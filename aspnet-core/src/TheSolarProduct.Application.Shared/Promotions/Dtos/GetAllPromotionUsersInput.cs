﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;

namespace TheSolarProduct.Promotions.Dtos
{
    public class GetAllPromotionUsersInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public int? OrganizationUnit { get; set; }
		public string ResponseMessageFilter { get; set; }

		public int? PromotionTitleFilter { get; set; }

		public string LeadCopanyNameFilter { get; set; }

	 	public string PromotionResponseStatusNameFilter { get; set; }
        
		public int? PromotionResponseSatusIdFilter { get; set; }
		 
		public List<int> JobStatusIDFilter { get; set; }

		public int? TeamId { get; set; }
		public int? SalesRepId { get; set; }
		public string  ResponseDateFilter { get; set; }
		public int?  PromoType { get; set; }
		
		public int? ReadUnreadsms { get; set; }

	}
}