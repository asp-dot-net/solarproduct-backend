﻿namespace TheSolarProduct.Promotions.Dtos
{
    public class GetPromotionForViewDto
    {
		public PromotionDto Promotion { get; set; }

		public string PromotionTypeName { get; set;}
        public string CreatorUserName { get; set; }
        public int InterestedCount { get; set; }
        public int NotInterestedCount { get; set; }
        public int OtherCount { get; set; }
        public int NoReply { get; set; }
        public int TotalLeads { get; set; }
        public int? TotalPromotion { get; set; }

        public int? NoReplay { get; set; }
        public int? summaryInterestedCount{ get; set; }
        public int? summaryNotInterestedCount { get; set; }
        public int? summaryOtherCount { get; set; }
        public int? Unhandle { get; set; }

        public int? PromotionProject { get; set; }
        public int? PromotionSales { get; set; }
        public int? PromotionSold { get; set; }

    }
}