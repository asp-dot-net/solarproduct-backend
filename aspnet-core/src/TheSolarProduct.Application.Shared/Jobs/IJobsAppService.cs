﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.EmailTemplates.Dtos;
using TheSolarProduct.Installer.Dtos;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Quotations.Dtos;
using TheSolarProduct.SmsTemplates.Dtos;
using GetAllForLookupTableInput = TheSolarProduct.Jobs.Dtos.GetAllForLookupTableInput;

namespace TheSolarProduct.Jobs
{
	public interface IJobsAppService : IApplicationService
	{
		Task<PagedResultDto<GetJobForViewDto>> GetAll(GetAllJobsInput input);

		Task<PagedResultDto<GetJobForViewDto>> GetAllForJobFinanceTracker(GetAllJobsInput input);

		Task<PagedResultDto<GetJobForViewDto>> GetAllForJobActiveTracker(GetAllJobsInput input);

		Task<PagedResultDto<GetJobForViewDto>> GetAllJobGrid(GetAllJobsInput input);

		Task<GetJobForEditOutput> GetJobForEdit(EntityDto input);
		
		Task<GetDetailForJobEditOutPut> GetJobDetailEditByJobID(EntityDto input);
		
		Task <JobApplicationViewDto> GetDataForQuickViewByJobID(EntityDto input);
		
		Task CreateOrEdit(CreateOrEditJobDto input);

		Task UpdateDataFromTracker(CreateOrEditJobDto input);

		Task ActiveJob(CreateOrEditJobDto input);

		Task<JobInstallationDto> GetJobInstallationDetailsForEdit(EntityDto input);

		Task<JobActiveStatusDto> GetJobActiveStatus(EntityDto input);

		string GetJobStatus(EntityDto input);

		Task UpdateJobStatus(UpdateJobStatusDto input);
		Task UpdateCanacelationRequest(CreateOrEditJobDto input);
		
		Task UpdateJobInstallationDetails(JobInstallationDto input);

		Task Delete(EntityDto input);

		//Task<FileDto> GetJobsToExcel(GetAllJobsForExcelInput input);

		Task<List<JobJobTypeLookupTableDto>> GetAllJobTypeForTableDropdown();

		Task<List<JobJobStatusLookupTableDto>> GetAllJobStatusForTableDropdown();

		Task<List<JobRoofTypeLookupTableDto>> GetAllRoofTypeForTableDropdown();

		Task<List<JobRoofAngleLookupTableDto>> GetAllRoofAngleForTableDropdown();

		Task<List<JobElecDistributorLookupTableDto>> GetAllElecDistributorForTableDropdown();

		Task<List<JobLeadLookupTableDto>> GetAllLeadForTableDropdown();

		Task<List<JobElecRetailerLookupTableDto>> GetAllElecRetailerForTableDropdown();

		Task<List<JobPaymentOptionLookupTableDto>> GetAllPaymentOptionForTableDropdown();

		Task<List<JobDepositOptionLookupTableDto>> GetAllDepositOptionForTableDropdown();

		Task<List<JobMeterUpgradeLookupTableDto>> GetAllMeterUpgradeForTableDropdown();

		Task<List<JobMeterPhaseLookupTableDto>> GetAllMeterPhaseForTableDropdown();

		Task<List<InstallerAvailableListDto>> GetAllInstallerAvailabilityList(DateTime AvailableDate,int jobid);
		
		Task<List<InstallerAvailableListDto>> GetAllElectricianAvailabilityList(DateTime AvailableDate, int jobid);
		
		Task<List<InstallerAvailableListDto>> GetAllDesignerAvailabilityList(DateTime AvailableDate, int jobid);

		Task<List<CommonLookupDto>> GetWareHouseDropdown(string stateid);

		Task<PagedResultDto<JobPromotionOfferLookupTableDto>> GetAllPromotionOfferForLookupTable(GetAllForLookupTableInput input);

		Task<List<JobHouseTypeLookupTableDto>> GetAllHouseTypeForTableDropdown();

		Task<List<JobFinanceOptionLookupTableDto>> GetAllFinanceOptionForTableDropdown();

		Task<List<STCZoneRatingDto>> GetSTCZoneRating();

		Task<List<STCPostalCodeDto>> GetSTCPostalCode();

		Task<List<STCYearWiseRateDto>> GetSTCYearWiseRate();

		Task<List<CommonLookupDto>> GetAllCancelReasonForTableDropdown();

		Task<List<CommonLookupDto>> GetAllHoldReasonForTableDropdown();

		string GetVariationsAction(int? Id);

		Task JobActive(int Id);

		Task JobActiveLoop();

		Task JobDeposite(int Id);
		
		Task Update_PostInstallationDetails(CreateOrEditJobDto input);
		
		Task Update_JOb_STCDetails(CreateOrEditJobDto input);
		
		Task<PagedResultDto<GetJobForViewDto>> GetAllForSTCTrackerTracker(GetAllJobsInput input);
		
		Task<List<JobLeadLookupTableDto>> GetPVDStatusList();

		Task<PagedResultDto<GetJobForViewDto>> GetGridConnectionTracker(GetAllJobsInput input);

		Task Update_MeterDetails(CreateOrEditJobDto input);

		Task<List<CommonLookupDto>> GetAllUsersTableDropdown();

		Task<Boolean> CheckExistJobsiteAddList(ExistJobSiteAddDto input);

		int checkjobcreatepermission(int leadid);

		Task<List<CommonLookupDto>> GetAllUsers();

		Task<TotalSammaryCountDto> getAllApplicationTrackerCount(int organizationid);

		Task<FileDto> getApplicationTrackerToExcel(GetAllJobsForExcelInput input);

		Task<FileDto> getFinanaceTrackerToExcel(GetAllJobsForExcelInput input);

		Task<FileDto> getJobActiveTrackerToExcel(GetAllJobsForExcelInput input);

		Task<FileDto> getJobGirdTrackerToExcel(GetAllJobsForExcelInput input);
		
		Task<FileDto> getSTCTrackerToExcel(GetAllJobsForExcelInput input);

		Task<FileDto> getGridConnectionTrackerToExcel(GetAllJobsForExcelInput input);

		Task<bool> CheckValidation(CreateOrEditJobDto input);

		Task SendSms(SmsEmailDto input);

		Task SendEmail(SmsEmailDto input);

		Task<List<string>> GetProductItemList(int productTypeId, string productItem, string state);

		Task<List<string>> getinvertmodel(string invertmodel);

		Task<List<string>> getpanelmodel(string panelmodel);

		Task<GetSmsTemplateForEditOutput> GetSmsTemplateForEditForSms(EntityDto input);

		Task<GetEmailTemplateForEdit> GetEmailTemplateForEditForEmail(EntityDto input);

		Task SendDocumentRequestLinkSMS(int input, int DocumentTypeId);

		Task<List<ProductItemAttachmentListDto>> ProductItemAttachmentList(int JobId);
		Task<List<PvdStatusDto>> GetAllPVDStatusDropdown();
		Task UpdateBookingManagerID(List<int> Jobids, int? userId);

		Task<List<CommonLookupDto>> GetUsers();
		Task<PagedResultDto<PvdStatusDto>> GetAllPvdStatusForGrid(GetAllRefundReasonsInput input);
		Task CreateOrEditStcStatus(CreateOrEditStcStatusDto input);
		Task<CreateOrEditStcStatusDto> GetstatusForEdit(EntityDto input);
		Task DeleteStcPvdStatus(EntityDto input);
		List<TotalStcCountDto> getStcTrackerCount(GetAllJobsInput input);
		GetJobForEditOutput getJobdetailbylead(int? leadid);
		Task<PagedResultDto<ReferralGridDto>> GetReferralTrackerList(ReferralInputDto input);
		Task<List<string>> GetAlljobNumberforAdd(string leadSourceName);
		 Task<PagedResultDto<WarrantyGridDto>> GetWarrantyTrackerList(WarrantyInputDto input);

		Task SaveDocument(UploadDocumentInput input);

	}
}