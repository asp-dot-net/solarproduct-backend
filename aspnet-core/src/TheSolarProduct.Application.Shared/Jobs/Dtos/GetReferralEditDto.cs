﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
   public  class GetReferralEditDto
    {
		public virtual int? RefferedJobId { get; set; }
		public virtual int? RefferedJobStatusId { get; set; }  
		public string AccountName { get; set; }
		public string BsbNo { get; set; }
		public string AccountNo { get; set; }
		public decimal? ReferralAmount { get; set; }
	}
}
