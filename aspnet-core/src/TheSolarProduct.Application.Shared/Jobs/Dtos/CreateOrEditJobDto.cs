﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditJobDto : EntityDto<int?>
    {

        public string Note { get; set; }
        public string OldSystemDetails { get; set; }

        public string InstallerNotes { get; set; }

        public int? PanelOnFlat { get; set; }

        public int? PanelOnPitched { get; set; }

        public string NMINumber { get; set; }

        public string RegPlanNo { get; set; }

        public string LotNumber { get; set; }

        public string PeakMeterNo { get; set; }

        public string OffPeakMeter { get; set; }

        public bool EnoughMeterSpace { get; set; }

        public bool IsSystemOffPeak { get; set; }

        public bool IsFinanceWithUs { get; set; }

        public decimal? BasicCost { get; set; }

        public decimal? SolarVLCRebate { get; set; }

        public decimal? SolarVLCLoan { get; set; }

        public decimal? TotalCost { get; set; }

        public string Suburb { get; set; }

        public int? SuburbId { get; set; }

        public string State { get; set; }

        public int? StateId { get; set; }

        public string PostalCode { get; set; }

        public string UnitNo { get; set; }

        public string UnitType { get; set; }

        public string StreetNo { get; set; }

        public string StreetName { get; set; }

        public string StreetType { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public decimal? SystemCapacity { get; set; }

        public decimal? STC { get; set; }

        public decimal? STCPrice { get; set; }

        public decimal? Rebate { get; set; }

        public string Address { get; set; }

        public string Country { get; set; }

        public int? JobTypeId { get; set; }

        public int? JobStatusId { get; set; }

        public int? RoofTypeId { get; set; }

        public int? RoofAngleId { get; set; }

        public int? ElecDistributorId { get; set; }

        public int? LeadId { get; set; }

        public int? ElecRetailerId { get; set; }

        public int? PaymentOptionId { get; set; }

        public int? DepositOptionId { get; set; }

        public int? MeterUpgradeId { get; set; }

        public int? MeterPhaseId { get; set; }

        public int? PromotionOfferId { get; set; }

        public int? HouseTypeId { get; set; }

        public int? FinanceOptionId { get; set; }

        public DateTime? DistAppliedDate { get; set; }

        public string MeterNumber { get; set; }

        public string ApplicationRefNo { get; set; }

        public string AdditionalComments { get; set; }

        public string JobNumber { get; set; }

        public bool? IsRefund { get; set; }

        public bool? RefundProcess { get; set; }

        public DateTime? InstalledcompleteDate { get; set; }
        
        public string IncompleteReason { get; set; }
        
        public string MeterApplyRef { get; set; }
        
        public string InspectorName { get; set; }
        
        public DateTime? InspectionDate { get; set; }
        
        public string ComplianceCertificate { get; set; }
        
        public int? PostInstallationStatus { get; set; }

        public DateTime? STCUploaddate { get; set; }
        
        public int? PVDStatus { get; set; }
        
        public DateTime? STCAppliedDate { get; set; }
        
        public string PVDNumber { get; set; }
        
        public string STCNotes { get; set; }
        
        public string STCUploadNumber { get; set; }
        
        public string Quickformid { get; set; }
        
        public TimeSpan? MeterTime { get; set; }

        public DateTime? FinanceApplicationDate { get; set; }
        
        public string FinancePurchaseNo { get; set; }
        
        public DateTime? FinanceDocSentDate { get; set; }
        
        public int? FinanceAppliedBy { get; set; }
        
        public int? FinanceDocSentBy { get; set; }
        
        public DateTime? FinanceDocReceivedDate { get; set; }
        
        public int? FinanceDocReceivedBy { get; set; }

        /// <summary>
        /// 1 = Verify
        /// 2 = Not Verify
        /// 3 = Query
        /// </summary>
        public int? FinanceDocumentVerified { get; set; }

        public bool? IsActive { get; set; }

        public DateTime? ActiveDate { get; set; }

        public string RebateAppRef { get; set; }

        public DateTime? ApprovalDate { get; set; }

        public DateTime? ExpiryDate { get; set; }

        public string VicRebate { get; set; }

        public decimal? SolarVICRebate { get; set; }

        public decimal? SolarVICLoanDiscont { get; set; }

        public int? SolarRebateStatus { get; set; }

        public string VicRebateNotes { get; set; }


        public  DateTime? DistApplied { get; set; }

        public  string ApprovalRef { get; set; }

        public  int? DistApproveBy { get; set; }

        public DateTime? DistExpiryDate { get; set; }

        public  int? AppliedBy { get; set; }

        public  DateTime? DistApproveDate { get; set; }

        public  int? EmpId { get; set; }

        public  decimal? PaidAmmount { get; set; }


        public  int? Paidby { get; set; }

        public  int? PaidStatus { get; set; }

        public  string InvRefNo { get; set; }

        public  DateTime? InvPaidDate { get; set; }

        public string ProjectNotes { get; set; }

        public string ApplicationNotes { get; set; }

        public DateTime? DocumentsVeridiedDate { get; set; }

        public int? DocumentsVeridiedBy { get; set; }

        public DateTime? CoolingoffPeriodEnd { get; set; }

        public string FinanceNotes { get; set; }

        public List<CreateOrEditJobProductItemDto> JobProductItems { get; set; }

        public List<CreateOrEditJobVariationDto> JobVariation { get; set; }

        public List<CreateOrEditJobPromotionDto> JobPromotion { get; set; }

        public string ManualQuote { get; set; }

        public decimal? ApprovedCapacityonExport { get; set; }

        public decimal? ApprovedCapacityonNonExport { get; set; }

        public  decimal? DepositRequired { get; set; }

        public  bool? SmsSend { get; set; }
        public  DateTime? SmsSendDate { get; set; }
        public  bool? EmailSend { get; set; }
        public  DateTime? EmailSendDate { get; set; }

        public  bool? FinanceSmsSend { get; set; }
        public  DateTime? FinanceSmsSendDate { get; set; }
        public  bool? FinanceEmailSend { get; set; }
        public  DateTime? FinanceEmailSendDate { get; set; }

        public  bool? JobActiveSmsSend { get; set; }
        public  DateTime? JobActiveSmsSendDate { get; set; }
        public  bool? JobActiveEmailSend { get; set; }
        public  DateTime? JobActiveEmailSendDate { get; set; }


        public  bool? GridConnectionSmsSend { get; set; }
        public  DateTime? GridConnectionSmsSendDate { get; set; }
        public  bool? GridConnectionEmailSend { get; set; }
        public  DateTime? GridConnectionEmailSendDate { get; set; }

        public  bool? StcSmsSend { get; set; }
        public  DateTime? StcSmsSendDate { get; set; }
        public  bool? StcEmailSend { get; set; }
        public  DateTime? StcEmailSendDate { get; set; }

        public  bool? JobGridSmsSend { get; set; }
        public  DateTime? JobGridSmsSendDate { get; set; }
        public  bool? JobGridEmailSend { get; set; }
        public  DateTime? JobGridEmailSendDate { get; set; }

        public string JobCancelRequestReason { get; set; }

        public bool? IsJobCancelRequest { get; set; }

        public string JobCancelReason { get; set; }

        public int? JobCancelReasonId { get; set; }
        public string JobCancelRejectReason { get; set; }
        public string InstallationNotes { get; set; }

        public string GridConnectionNotes { get; set; }
         
        public DateTime? DepositeRecceivedDate { get; set; }
        public virtual string FileName { get; set; }
        public virtual string FilePath { get; set; } 
        public string FileToken { get; set; }

        
             public virtual string ApprovalLetter_Filename { get; set; }
        public virtual string ApprovalLetter_FilePath { get; set; }

        public int? Applicationfeespaid { get; set; }
        public virtual string Applicationfeespaidstatus { get; set; }

        public bool? PendingInstallerSmsSend { get; set; }
        public DateTime? PendingInstallerSmsSendDate { get; set; }
        public bool? PendingInstallerEmailSend { get; set; }
        public DateTime? PendingInstallerEmailSendDate { get; set; }
        public virtual int? RefferedJobStatusId { get; set; }

        public virtual int? RefferedJobId { get; set; }

        public string AccountName { get; set; }
        public string BsbNo { get; set; }
        public string AccountNo { get; set; }
        public decimal? ReferralAmount { get; set; }
        public string refferedJobName { get; set; }
        public virtual DateTime? ReferralPayDate { get; set; }
        public virtual bool ReferralPayment { get; set; }
        public string BankReferenceNo { get; set; }
        public string ReferralRemark { get; set; }
    }
}