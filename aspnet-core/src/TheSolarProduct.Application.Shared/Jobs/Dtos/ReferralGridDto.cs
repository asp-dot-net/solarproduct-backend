﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
 public   class ReferralGridDto: EntityDto
    {

        public string ProjectName { get; set; }
        public string Customer { get; set; }

        public string Address { get; set; }

        public string PCode { get; set; }

        public DateTime? InstallDate { get; set; }
        public DateTime? InstallComplate
        { get; set; }
        public decimal? Ammount { get; set; }
        public DateTime? InvDate { get; set; }
        public DateTime? Issued { get; set; }
        public DateTime? PaymentDate { get; set; }
        public DateTime? PaymentStaus { get; set; }
        public string Suburb { get; set; }

        public string State { get; set; }

        public int? InvNo { get; set; }

        public int? id { get; set; }

        public string Total { get; set; }
        public string Verified { get; set; }
        public string Pending { get; set; }
        public string Awaiting { get; set; }
        public string installername { get; set; }
        public string invoiceType { get; set; }
        public string invoiceNotes { get; set; }
        public string InvoiceNO { get; set; }
        public int? invoiceinstallerId { get; set; }


        public string Filenameinv { get; set; }
        public string FilePathinv { get; set; }
        public string jobstatus { get; set; }
        public decimal? TotalCost { get; set; }
        public decimal? owningAmt { get; set; }
        public int? leadid { get; set; }
        public int? Refferedid { get; set; }
        public string companyName { get; set; }
        public string RefferedProjectName { get; set; }
        public string RefferedProjectStatus { get; set; }
        public decimal? RefferedTotalCost { get; set; }
        public decimal? RefferedowningAmt { get; set; }

        public string AccountName { get; set; }
        public string BsbNo { get; set; }
        public string AccountNo { get; set; }
        public decimal? ReferralAmount { get; set; }
        public DateTime? ActivityReminderTime { get; set; }
        public string ActivityDescription { get; set; }
        public string ActivityComment { get; set; }

      
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string panelName { get; set; }
        public string PanelModel { get; set; }
        public string PanelDocu { get; set; }
        public string InverterNname { get; set; }
        public string InverterModel { get; set; }
        public string InverterDoc { get; set; }
        public decimal? BalanceOwing { get; set; }
        public string SalesRep { get; set; }
        public string SMS { get; set; }
        //public string EMAIL { get; set; }
    }
}

    