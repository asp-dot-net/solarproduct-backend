﻿using TheSolarProduct.PostCodes;
using System.Collections.Generic;
using TheSolarProduct.States;
using TheSolarProduct.LeadSources;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Leads.Exporting;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.LeadStatuses;
using TheSolarProduct.UnitTypes;
using TheSolarProduct.StreetNames;
using TheSolarProduct.StreetTypes;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.LeadActions;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Runtime.Session;
using Abp;
using Abp.Notifications;
using TheSolarProduct.Notifications;
using PayPalCheckoutSdk.Orders;
using TheSolarProduct.TheSolarDemo;
using Microsoft.EntityFrameworkCore.Internal;
using System.Diagnostics;
using TheSolarProduct.RejectReasons;
using TheSolarProduct.CancelReasons;
using Castle.DynamicProxy.Internal;
using NPOI.SS.Formula.Functions;
using Castle.DynamicProxy.Contributors;
using NPOI.XSSF.Streaming.Values;
using Abp.Collections.Extensions;
using NUglify.Helpers;
using Abp.Domain.Uow;
using TheSolarProduct.Jobs;
using Abp.Organizations;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.ApplicationSettings;
using System.Net.Mail;
using Abp.Net.Mail;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.EmailTemplates;
using System.Text.RegularExpressions;
using TheSolarProduct.Quotations;
using System.IO;
using TheSolarProduct.SmsTemplates;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Invoices;
using TheSolarProduct.PreviousJobStatuses;
using Abp.UI;
using Abp.Timing.Timezone;
using TheSolarProduct.Promotions;
using TheSolarProduct.LeadHistory;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Notifications.Dto;
using TheSolarProduct.Sections;

namespace TheSolarProduct.Leads
{
    //[AbpAuthorize(AppPermissions.Pages_Leads, AppPermissions.Pages_LeadTracker, AppPermissions.Pages_Lead_Closed, AppPermissions.Pages_MyLeads, AppPermissions.Pages_Leads_Create,
    //	AppPermissions.Pages_Leads_Edit, AppPermissions.Pages_LeadSources_Delete, AppPermissions.Pages_Lead_Duplicate, AppPermissions.Pages_Leads_Assign, AppPermissions.Pages_Jobs)]

    public class LeadsAppService : TheSolarProductAppServiceBase, ILeadsAppService
    {
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<LeadAction, int> _lookup_leadActionRepository;
        private readonly ILeadsExcelExporter _leadsExcelExporter;
        private readonly IRepository<PostCode, int> _lookup_postCodeRepository;
        private readonly IRepository<State, int> _lookup_stateRepository;
        private readonly IRepository<LeadSource, int> _lookup_leadSourceRepository;
        private readonly IRepository<LeadStatus, int> _lookup_leadStatusRepository;
        private readonly IRepository<UnitType, int> _lookup_unitTypeRepository;
        private readonly IRepository<StreetName, int> _lookup_streetNameRepository;
        private readonly IRepository<StreetType, int> _lookup_streetTypeRepository;
        private readonly IRepository<RejectReason, int> _lookup_rejectReasonRepository;
        private readonly IRepository<CancelReason, int> _lookup_cancelReasonRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<JobStatus, int> _lookup_jobStatusRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<OrganizationUnitRole, long> _organizationUnitRoleRepository;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<Team> _teamRepository;
        private readonly IUserEmailer _userEmailer;
        private readonly IRepository<EmailTemplate> _emailTemplateRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<Quotation> _quotationRepository;
        private readonly IRepository<SmsTemplate> _smsTemplateRepository;
        private readonly IRepository<JobPromotion> _jobPromotionRepository;
        private readonly IRepository<FreebieTransport> _freebieTransportRepository;
        private readonly IRepository<JobRefund> _jobRefundRepository;
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
        private readonly IRepository<PreviousJobStatus> _previousJobStatusRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<PromotionUser> _PromotionUsersRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<JobVariation> _jobVariationRepository;
        private readonly IRepository<PostCode> _postCodeRepository;
        private readonly IRepository<LeadtrackerHistory> _LeadtrackerHistoryRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<ProductItem> _ProductItemRepository;
        private readonly NotificationAppService _NotificationAppService;
        private readonly IRepository<Section> _SectionAppService;
        private readonly IRepository<InvoiceImportData> _InvoiceImportDataRepository;
        public LeadsAppService(IRepository<Lead> leadRepository
            , ILeadsExcelExporter leadsExcelExporter
            , IRepository<PostCode, int> lookup_postCodeRepository
            , IRepository<State, int> lookup_stateRepository
            , IRepository<LeadSource, int> lookup_leadSourceRepository
            , IRepository<LeadStatus, int> lookup_leadStatusRepository
            , IRepository<UnitType, int> lookup_unitTypeRepository
            , IRepository<StreetName, int> lookup_streetNameRepository
            , IRepository<StreetType, int> lookup_streetTypeRepository
            , IRepository<RejectReason, int> lookup_rejectReasonRepository
            , IRepository<CancelReason, int> lookup_cancelReasonRepository
            , IRepository<LeadActivityLog> leadactivityRepository
            , IRepository<LeadAction, int> lookup_leadActionRepository
            , IRepository<User, long> userRepository
            , IRepository<UserRole, long> userroleRepository
            , IRepository<Role> roleRepository
            , IRepository<UserTeam> userTeamRepository
            , IAppNotifier appNotifier
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<OrganizationUnit, long> organizationUnitRepository
            , IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository
            , IRepository<OrganizationUnitRole, long> organizationUnitRoleRepository
            , IApplicationSettingsAppService applicationSettings
            , IEmailSender emailSender
            , IRepository<Team> teamRepository
            , IUserEmailer userEmailer
            , IRepository<EmailTemplate> emailTemplateRepository
            , UserManager userManager
            , IRepository<JobStatus, int> lookup_jobStatusRepository
            , IRepository<Job> jobRepository
            , IRepository<Quotation> quotationRepository,
              IRepository<SmsTemplate> smsTemplateRepository,
              IRepository<JobPromotion> jobPromotionRepository,
              IRepository<FreebieTransport> freebieTransportRepository,
              IRepository<JobRefund> jobRefundRepository,
              IRepository<InvoicePayment> invoicePaymentRepository,
              IRepository<PreviousJobStatus> previousJobStatusRepository
            , ITimeZoneConverter timeZoneConverter,
              IRepository<PromotionUser> PromotionUsersRepository
            , IRepository<JobVariation> jobVariationRepository
            , IRepository<JobProductItem> jobProductItemRepository,
               IRepository<PostCode> postCodeRepository,
               IRepository<LeadtrackerHistory> LeadtrackerHistoryRepository
            , IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
               IRepository<ProductItem> ProductItemRepository,
               NotificationAppService NotificationAppService,
               IRepository<Section> SectionAppService,
               IRepository<InvoiceImportData> InvoiceImportDataRepository
            )
        {
            _leadRepository = leadRepository;
            _leadsExcelExporter = leadsExcelExporter;
            _lookup_postCodeRepository = lookup_postCodeRepository;
            _lookup_stateRepository = lookup_stateRepository;
            _lookup_leadSourceRepository = lookup_leadSourceRepository;
            _lookup_leadStatusRepository = lookup_leadStatusRepository;
            _lookup_unitTypeRepository = lookup_unitTypeRepository;
            _lookup_streetNameRepository = lookup_streetNameRepository;
            _lookup_streetTypeRepository = lookup_streetTypeRepository;
            _lookup_rejectReasonRepository = lookup_rejectReasonRepository;
            _lookup_cancelReasonRepository = lookup_cancelReasonRepository;
            _leadactivityRepository = leadactivityRepository;
            _lookup_leadActionRepository = lookup_leadActionRepository;
            _userRepository = userRepository;
            _userroleRepository = userroleRepository;
            _roleRepository = roleRepository;
            _userTeamRepository = userTeamRepository;
            _appNotifier = appNotifier;
            _unitOfWorkManager = unitOfWorkManager;
            _organizationUnitRepository = organizationUnitRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _organizationUnitRoleRepository = organizationUnitRoleRepository;
            _applicationSettings = applicationSettings;
            _emailSender = emailSender;
            _teamRepository = teamRepository;
            _userEmailer = userEmailer;
            _emailTemplateRepository = emailTemplateRepository;
            _userManager = userManager;
            _lookup_jobStatusRepository = lookup_jobStatusRepository;
            _jobRepository = jobRepository;
            _quotationRepository = quotationRepository;
            _smsTemplateRepository = smsTemplateRepository;
            _jobPromotionRepository = jobPromotionRepository;
            _freebieTransportRepository = freebieTransportRepository;
            _jobRefundRepository = jobRefundRepository;
            _invoicePaymentRepository = invoicePaymentRepository;
            _previousJobStatusRepository = previousJobStatusRepository;
            _timeZoneConverter = timeZoneConverter;
            _PromotionUsersRepository = PromotionUsersRepository;
            _postCodeRepository = postCodeRepository;
            _LeadtrackerHistoryRepository = LeadtrackerHistoryRepository;
            _dbcontextprovider = dbcontextprovider;
            _ProductItemRepository = ProductItemRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _NotificationAppService = NotificationAppService;
            _SectionAppService = SectionAppService;
            _InvoiceImportDataRepository = InvoiceImportDataRepository;
        }

        /// <summary>
        /// Manage Leads API(Only UnAssigned Leads)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetLeadForViewDto>> GetAll(GetAllLeadsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var jobnumberlist = new List<int?>();
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

            var filteredLeads = _leadRepository.GetAll()
                        .Include(e => e.LeadStatusFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StreetNameFilter), e => e.StreetName == input.StreetNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadStatusId != null, e => e.LeadStatusId == input.LeadStatusId)
                        .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                        .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                        //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                        .WhereIf(input.DuplicateFilter == "web", o => (_leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.AssignToUserID == null && e.OrganizationId == input.OrganizationUnit && e.IsDuplicate != true && e.IsWebDuplicate != true).Count() > 0))
                        //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
                        .WhereIf(input.DuplicateFilter == "db", o => (_leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.OrganizationId == input.OrganizationUnit && e.AssignToUserID != null).Count() > 0))
                        //Not Duplicate Leads
                        .WhereIf(input.DuplicateFilter == "no", o => !(_leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.AssignToUserID == null && e.OrganizationId == input.OrganizationUnit && e.IsDuplicate != true && e.IsWebDuplicate != true).Count() > 0) && !(_leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.OrganizationId == input.OrganizationUnit && e.AssignToUserID != null).Count() > 0))
                        .Where(e => e.AssignToUserID == null)
                        .Where(e => e.IsDuplicate != true && e.IsWebDuplicate != true);

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var leads = (from o in pagedAndFilteredLeads
                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         join o2 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         select new GetLeadForViewDto()
                         {
                             Lead = new LeadDto
                             {
                                 CompanyName = o.CompanyName,
                                 Email = o.Email,
                                 Phone = o.Phone,
                                 Mobile = o.Mobile,
                                 Address = o.Address,
                                 Requirements = o.Requirements,
                                 Id = o.Id,
                                 PostCode = o.PostCode,
                                 LeadStatusID = o.LeadStatusId,
                                 Suburb = o.Suburb,
                                 LeadSource = o.LeadSource,
                                 State = o.State,
                                 AssignToUserID = o.AssignToUserID,
                                 IsExternalLead = o.IsExternalLead,
                                 OrganizationId = o.OrganizationId,
                                 OrganizationName = s2.DisplayName
                             },
                             Id = o.Id,
                             LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                             //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
                             Duplicate = (o.IsDuplicate == false) ? false : (o.IsDuplicate == true) ? true : _leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.OrganizationId == input.OrganizationUnit && e.AssignToUserID != null).Any(),
                             //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                             WebDuplicate = (o.IsWebDuplicate == false) ? false : (o.IsWebDuplicate == true) ? true : _leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.AssignToUserID == null && e.OrganizationId == input.OrganizationUnit && e.IsDuplicate != true && e.IsWebDuplicate != true).Any(),
                         });

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetLeadForViewDto>(
                totalCount,
                await leads.ToListAsync()
            );
        }

        /// <summary>
        /// Lead Tracker API(All Assign Leads - Except Web & Database Duplicate)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetLeadForViewDto>> GetLeadTrackerData(GetAllLeadsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            int UserId = (int)AbpSession.UserId;
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == UserId).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var jobnumberlist = new List<int?>();
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = _userTeamRepository.GetAll().Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }

            var FilterManagerList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                var ManagerTeamId = _userTeamRepository.GetAll().Where(e => TeamId.Contains(input.SalesManagerId)).Select(e => e.TeamId).ToList();
                FilterManagerList = _userTeamRepository.GetAll().Where(e => ManagerTeamId.Contains(input.SalesManagerId)).Select(e => e.UserId).ToList();
            }

            var CancelreqListLeadid = new List<int?>();
            if (input.Cancelrequestfilter != 0)
            {
                CancelreqListLeadid = _jobRepository.GetAll().Where(e => e.IsJobCancelRequest == true).Select(e => e.LeadId).Distinct().ToList();
            }

            var User_List = _userRepository
       .GetAll();

            var RoleName = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            where (us != null && user.Id == UserId)
                            select (us.DisplayName));

            var FollowupList = _leadactivityRepository.GetAll().WhereIf(input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
            var List = _leadactivityRepository.GetAll().Where(e => e.ActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).ToList();
            var NextFollowupList = List.WhereIf(input.StartDate != null && input.EndDate != null, e => e.ActivityDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActivityDate.Value.AddHours(10).Date <= EDate.Value.Date).Select(e => e.LeadId);

            var joblist = new List<int?>();
            if (input.JobStatusID != null && input.JobStatusID.Count() > 0 && input.LeadStatusId == 6)
            {
                joblist = _jobRepository.GetAll().Where(e => input.JobStatusID.Contains((int)e.JobStatusId)).Select(e => e.LeadId).ToList();
            }
            var filteredLeads = _leadRepository.GetAll()
                       .Include(e => e.LeadStatusFk)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) ||
                       e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                       .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                       .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => input.StateNameFilter.Contains(e.State) && !string.IsNullOrEmpty(e.State))
                       .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                       .WhereIf(input.LeadStatusId > 0, e => e.LeadStatusId == input.LeadStatusId)
                       .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                       .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                       .WhereIf(input.DateFilterType == "Assign" && input.StartDate != null && input.EndDate != null, e => e.LeadAssignDate.Value.AddHours(10).Date >= SDate.Value.Date && e.LeadAssignDate.Value.AddHours(10).Date <= EDate.Value.Date)
                       .WhereIf(input.DateFilterType == "ReAssign" && input.StartDate != null && input.EndDate != null, e => e.LeadAssignDate.Value.AddHours(10).Date >= SDate.Value.Date && e.LeadAssignDate.Value.AddHours(10).Date <= EDate.Value.Date && e.LeadStatusId == 11)
                       .WhereIf(input.DateFilterType == "Followup" && string.IsNullOrWhiteSpace(input.Filter) && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))
                       .WhereIf(input.DateFilterType == "NextFollowup" && string.IsNullOrWhiteSpace(input.Filter) && input.StartDate != null && input.EndDate != null, e => NextFollowupList.Contains(e.Id))
                       .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                       .WhereIf(input.SalesManagerId == null && input.SalesRepId == null && RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                       .WhereIf(input.SalesManagerId == null && input.SalesRepId == null && !RoleName.Contains("Admin"), e => UserList.Contains(e.AssignToUserID))
                       .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                       .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)
                       .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                       .WhereIf(input.JobStatusID != null && input.JobStatusID.Count() > 0, e => joblist.Contains(e.Id))
                       .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.AssignToUserID))
                       .WhereIf(input.Cancelrequestfilter != 0, e => CancelreqListLeadid.Contains(e.Id))
                       .Where(e => e.IsDuplicate != true && e.IsWebDuplicate != true);

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "LeadAssignDate desc")
                .PageBy(input);

            var LeadidList = _jobRepository.GetAll().Where(e => e.DepositeRecceivedDate != null).Select(e => e.LeadId).ToList();

            var leads = (from o in pagedAndFilteredLeads

                         join o1 in _lookup_rejectReasonRepository.GetAll() on o.RejectReasonId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _lookup_cancelReasonRepository.GetAll() on o.LeadStatusId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                         from s4 in j4.DefaultIfEmpty()

                         join o5 in _leadactivityRepository.GetAll() on o.Id equals o5.LeadId into j5
                         from s5 in j5.DefaultIfEmpty()

                         join o6 in _jobRepository.GetAll() on o.Id equals o6.LeadId into j6
                         from s6 in j6.DefaultIfEmpty()

                         select new GetLeadForViewDto()
                         {
                             Lead = new LeadDto
                             {
                                 CompanyName = o.CompanyName,
                                 Email = o.Email,
                                 Phone = o.Phone,
                                 Mobile = o.Mobile,
                                 Address = o.Address,
                                 Requirements = o.Requirements,
                                 Id = o.Id,
                                 PostCode = o.PostCode,
                                 LeadStatusID = o.LeadStatusId,
                                 Suburb = o.Suburb,
                                 LeadSource = o.LeadSource,
                                 State = o.State,
                                 AssignToUserID = o.AssignToUserID,
                                 RejectReason = o.RejectReason,
                                 RejectReasonId = o.RejectReasonId,
                                 CancelReasonId = o.CancelReasonId,
                                 OrganizationId = o.OrganizationId,
                                 OrganizationName = s4.DisplayName,
                                 CreationTime = o.CreationTime,
                                 FollowupDate = s5.CreationTime,
                                 ActionId = s5.ActionId,
                                 longitude = o.longitude,
                                 latitude = o.latitude,
                                 IsGoogle = o.IsGoogle,
                                 CurrentLeadOwner = _userRepository.GetAll().Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                                 LeadAssignDate = o.LeadAssignDate
                             },
                             LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                             Duplicate = (o.IsDuplicate == false) ? false : (o.IsDuplicate == true) ? true : _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID != null && e.OrganizationId == input.OrganizationUnit).Any(),
                             WebDuplicate = (o.IsWebDuplicate == false) ? false : (o.IsWebDuplicate == true) ? true : _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.OrganizationUnit).Any(),

                             LastComment = _leadactivityRepository.GetAll().Where(e => e.LeadId == o.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                             LastCommentDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == o.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),

                             ActivityReminderTime = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 14 || e.SectionId == 13) && e.ActionId == 8 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                             ActivityDescription = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 14 || e.SectionId == 13) && e.ActionId == 8 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                             ActivityComment = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 14 || e.SectionId == 13) && e.ActionId == 24 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                             JobStatusName = _lookup_jobStatusRepository.GetAll().Where(e => e.Id == s6.JobStatusId).Select(e => e.Name).FirstOrDefault(),

                             RejectReasonName = s1 == null || s1.RejectReasonName == null ? "" : s1.RejectReasonName.ToString(),
                             CancelReasonName = s2 == null || s2.CancelReasonName == null ? "" : s2.CancelReasonName.ToString(),

                             New = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 1).Count()),
                             UnHandled = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 2).Count()),
                             Hot = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 5).Count()),
                             Cold = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 3).Count()),
                             Warm = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 4).Count()),
                             Upgrade = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 6).Count()),
                             Rejected = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 7).Count()),
                             Cancelled = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 8).Count()),
                             Closed = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 9).Count()),
                             Total = Convert.ToString(filteredLeads.Count()),
                             Assigned = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 10).Count()),
                             ReAssigned = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 11).Count()),
                             Sold = Convert.ToString(filteredLeads.Where(e => LeadidList.Contains(e.Id)).Count()),
                         });

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetLeadForViewDto>(
                totalCount,
                leads.DistinctBy(e => e.Lead.Id).ToList()
            );
        }

        /// <summary>
        /// Lead Tracker Export To Excel API
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<FileDto> getLeadTrackerToExcel(LeadExcelExportTto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            int UserId = (int)AbpSession.UserId;
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == UserId).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var jobnumberlist = new List<int?>();
            //if (input.ProjectNumberFilter != null)
            //{
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            //}

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = _userTeamRepository.GetAll().Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }

            var FilterManagerList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                var ManagerTeamId = _userTeamRepository.GetAll().Where(e => TeamId.Contains(input.SalesManagerId)).Select(e => e.TeamId).ToList();
                FilterManagerList = _userTeamRepository.GetAll().Where(e => ManagerTeamId.Contains(input.SalesManagerId)).Select(e => e.UserId).ToList();
            }

            var User_List = _userRepository
       .GetAll();

            var RoleName = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            where (us != null && user.Id == UserId)
                            select (us.DisplayName));

            //if (RoleName.Contains("Sales Manager"))
            //{
            //	UserList.Remove(UserId);
            //}

            var joblist = new List<int?>();
            if (input.jobStatusIDFilter != null && input.jobStatusIDFilter != 0)
            {
                joblist = _jobRepository.GetAll().Where(e => e.JobStatusId == input.jobStatusIDFilter).Select(e => e.LeadId).ToList();
            }

            var FollowupList = _leadactivityRepository.GetAll().WhereIf(input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
            var List = _leadactivityRepository.GetAll().Where(e => e.ActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).ToList();
            var NextFollowupList = List.WhereIf(input.StartDate != null && input.EndDate != null, e => e.ActivityDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActivityDate.Value.AddHours(10).Date <= EDate.Value.Date).Select(e => e.LeadId);

            var filteredLeads = _leadRepository.GetAll()
                       .Include(e => e.LeadStatusFk)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter)
                       || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) ||
                       e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                       .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                       .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                       .WhereIf(input.LeadStatusId > 0, e => e.LeadStatusId == input.LeadStatusId)
                       .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                       //.WhereIf(input.LeadSourceIdFilter != 0 && input.LeadSourceIdFilter != null, e => e.LeadSourceId == input.LeadSourceIdFilter)
                       .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                       .WhereIf(input.DateFilterType == "Assign" && input.StartDate != null && input.EndDate != null, e => e.LeadAssignDate.Value.AddHours(10).Date >= SDate.Value.Date && e.LeadAssignDate.Value.AddHours(10).Date <= EDate.Value.Date)
                       .WhereIf(input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))
                       .WhereIf(input.DateFilterType == "NextFollowup" && input.StartDate != null && input.EndDate != null, e => NextFollowupList.Contains(e.Id)) //(_leadactivityRepository.GetAll().Where(e => e.ActivityDate >= input.StartDate && e.ActivityDate <= input.EndDate && e.ActionId == 8 && e.LeadId == l.Id).OrderByDescending(e => e.Id).Select(e => e.LeadId).FirstOrDefault() == l.Id))
                       .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                       //.WhereIf(RoleName.Contains("Sales Manager") && input.OnlyAssignLead == true, e => UserList.Contains(e.AssignToUserID))
                       //.WhereIf(RoleName.Contains("Sales Manager") && input.OnlyAssignLead != true, e => e.AssignToUserID == UserId)
                       .WhereIf(input.SalesManagerId == null && input.SalesRepId == null && RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                       .WhereIf(input.SalesManagerId == null && input.SalesRepId == null && !RoleName.Contains("Admin"), e => UserList.Contains(e.AssignToUserID))
                       .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                       .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)
                       .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                       .WhereIf(input.jobStatusIDFilter != null, e => joblist.Contains(e.Id))
                       .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.AssignToUserID))
                       .Where(e => e.IsDuplicate != true && e.IsWebDuplicate != true);

            var query = (from o in filteredLeads

                         join o2 in _jobRepository.GetAll() on o.Id equals o2.LeadId into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                         from s4 in j4.DefaultIfEmpty()

                             //join o5 in _leadactivityRepository.GetAll() on o.Id equals o5.LeadId into j5
                             //from s5 in j5.DefaultIfEmpty()

                         join o6 in _lookup_jobStatusRepository.GetAll() on s2.JobStatusId equals o6.Id into j6
                         from s6 in j6.DefaultIfEmpty()

                         select new GetLeadForViewDto()
                         {
                             Lead = new LeadDto
                             {
                                 CompanyName = o.CompanyName,
                                 Email = o.Email,
                                 Phone = o.Phone,
                                 Mobile = o.Mobile,
                                 Address = o.Address,
                                 Requirements = o.Requirements,
                                 Id = o.Id,
                                 PostCode = o.PostCode,
                                 LeadStatusID = o.LeadStatusId,
                                 Suburb = o.Suburb,
                                 LeadSource = o.LeadSource,
                                 State = o.State,
                                 AssignToUserID = o.AssignToUserID,
                                 RejectReason = o.RejectReason,
                                 RejectReasonId = o.RejectReasonId,
                                 CancelReasonId = o.CancelReasonId,
                                 OrganizationId = o.OrganizationId,
                                 OrganizationName = s4.DisplayName,
                                 CreationTime = o.CreationTime,
                                 FollowupDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == o.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                                 longitude = o.longitude,
                                 latitude = o.latitude,
                                 CurrentLeadOwner = _userRepository.GetAll().Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                                 LeadAssignDate = o.LeadAssignDate,
                                 JobNumber = s2.JobNumber,
                                 JobStatus = s6.Name
                             },
                             LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),

                         });

            var leadtrackerListDtos = await query.ToListAsync();

            return _leadsExcelExporter.LeadTrackerExportToFile(leadtrackerListDtos);
        }

        /// <summary>
        /// Main Search Data API
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetLeadForViewDto>> GetSearchData(GetAllLeadsInput input)
        {

            var filtertext = "";
            if (input.Filter != null && input.Filter.Contains("|"))
            {
                string[] splitPipeValue = input.Filter.Split('|');
                if (splitPipeValue.Length > 0)
                {
                    filtertext = splitPipeValue[0].ToString();
                }
            }
            else
            {
                filtertext = input.Filter;
            }

            var JobList = _jobRepository.GetAll().Where(j => j.JobNumber.Contains(filtertext)).Select(e => e.LeadId).ToList();
            var filteredLeads = _leadRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Email.Contains(filtertext) || e.Phone.Contains(filtertext) || e.Mobile.Contains(filtertext) || e.CompanyName.Contains(filtertext) || e.Address.Contains(filtertext) || JobList.Contains(e.Id))
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter),e => JobList.Contains(e.Id))
                        .Where(e => e.OrganizationId == input.OrganizationUnit);

            var pagedAndFilteredLeads = filteredLeads
                .PageBy(input);

            var leads = from o in pagedAndFilteredLeads

                        join o1 in _jobRepository.GetAll() on o.Id equals o1.LeadId into j1
                        from s1 in j1.DefaultIfEmpty()

                        join o2 in _lookup_jobStatusRepository.GetAll() on s1.JobStatusId equals o2.Id into j2
                        from s2 in j2.DefaultIfEmpty()

                        join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                        from s3 in j3.DefaultIfEmpty()

                        join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                        from s4 in j4.DefaultIfEmpty()

                        join o5 in _leadactivityRepository.GetAll() on o.Id equals o5.LeadId into j5
                        from s5 in j5.DefaultIfEmpty()

                        select new GetLeadForViewDto()
                        {
                            Lead = new LeadDto
                            {
                                CompanyName = o.CompanyName,
                                Email = o.Email,
                                Phone = o.Phone,
                                Mobile = o.Mobile,
                                Address = o.Address,
                                Requirements = o.Requirements,
                                Id = o.Id,
                                PostCode = o.PostCode,
                                LeadStatusID = o.LeadStatusId,
                                Suburb = o.Suburb,
                                LeadSource = o.LeadSource,
                                State = o.State,
                                OrganizationId = o.OrganizationId,
                                OrganizationName = s4.DisplayName,
                                longitude = o.longitude,
                                latitude = o.latitude,
                                FollowupDate = s5.CreationTime,
                                CreationTime = (DateTime)o.LeadAssignDate
                            },
                            JobNumber = s1.JobNumber,
                            JobStatus = s2.Name,
                            JobCreatedDate = s1.CreationTime,
                            OrganizationName = s4.DisplayName,
                            LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                            Duplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID != null && e.OrganizationId == input.OrganizationUnit).Any(),
                            WebDuplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.OrganizationUnit).Any(),
                            LastComment = _leadactivityRepository.GetAll().Where(e => e.LeadId == o.Id && e.ActionId == 8).OrderByDescending(x => x.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                            LastCommentDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == o.Id && e.ActionId == 8).OrderByDescending(x => x.Id).Select(e => e.CreationTime).FirstOrDefault(),
                            LastQuoteDate = _quotationRepository.GetAll().Where(e => e.JobFk.LeadFk.Id == o.Id).OrderByDescending(e => e.Id).Select(e => e.CreationTime).FirstOrDefault(),
                            ApplicationRefNo = _jobRepository.GetAll().Where(e => e.LeadFk.Id == o.Id).Select(e => e.ApplicationRefNo).FirstOrDefault(),
                            LeadOwaner = o.AssignToUserID == AbpSession.UserId
                        };
            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetLeadForViewDto>(
                totalCount,
                leads.DistinctBy(e => e.Lead.Id).ToList()
            );

        }

        /// <summary>
        /// Duplicate Leads API(Only Data & Web Duplicate Leads)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetDuplicateLeadForViewDto>> GetAllDuplicateLead(GetAllLeadsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var filteredLeads = _leadRepository.GetAll()
                        .Include(e => e.LeadStatusFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.StartDate != null && input.EndDate != null, e => e.ChangeStatusDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ChangeStatusDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                        .WhereIf(string.IsNullOrWhiteSpace(input.DuplicateFilter), e => e.IsDuplicate == true || e.IsWebDuplicate == true)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DuplicateFilter), e => input.DuplicateFilter == "web" ? e.IsWebDuplicate == true : e.IsDuplicate == true);

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "ChangeStatusDate desc")
                .PageBy(input);

            var leads = from o in pagedAndFilteredLeads

                        join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                        from s3 in j3.DefaultIfEmpty()

                        join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                        from s4 in j4.DefaultIfEmpty()

                        select new GetDuplicateLeadForViewDto()
                        {
                            DuplicateLead = new DuplicateLeadDto
                            {
                                CompanyName = o.CompanyName,
                                Email = o.Email,
                                Phone = o.Phone,
                                Mobile = o.Mobile,
                                Address = o.Address,
                                Requirements = o.Requirements,
                                Id = o.Id,
                                PostCode = o.PostCode,
                                LeadStatusID = o.LeadStatusId,
                                Suburb = o.Suburb,
                                LeadSource = o.LeadSource,
                                State = o.State,
                                OrganizationId = o.OrganizationId,
                                OrganizationName = s4.DisplayName,
                                CreationTime = o.CreationTime
                            },
                            //CurrentLeadOwner = _userRepository.GetAll().Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                            LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                            Duplicate = (o.IsDuplicate == false || string.IsNullOrEmpty(o.Email) || string.IsNullOrEmpty(o.Mobile) || o.IsWebDuplicate == true) ? false : (o.IsDuplicate == true) ? true : _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.OrganizationId == input.OrganizationUnit && e.AssignToUserID != null && e.IsWebDuplicate != true && e.CreationTime <= o.CreationTime).Any(),
                            WebDuplicate = (o.IsWebDuplicate == false || string.IsNullOrEmpty(o.Email) || string.IsNullOrEmpty(o.Mobile) || o.IsDuplicate == true) ? false : (o.IsWebDuplicate == true) ? true : _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.OrganizationId == input.OrganizationUnit && e.IsDuplicate != true && e.CreationTime <= o.CreationTime).Any(),
                            WebDuplicateCount = _leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.AssignToUserID == null && e.OrganizationId == input.OrganizationUnit && e.ChangeStatusDate <= o.ChangeStatusDate).Count(), //e.IsDuplicate != true && e.IsWebDuplicate != true &&
                            Facebook = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Facebook").Count()),
                            Online = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Google").Count()),
                            Others = Convert.ToString(filteredLeads.Where(e => e.LeadSource != "Facebook" && e.LeadSource != "Google").Count()),
                            Total = Convert.ToString(filteredLeads.Count()),
                            DuplicateDetail = (from item in _leadRepository.GetAll()
                                               .Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.OrganizationId == input.OrganizationUnit)

                                               select new DuplicateDetailDto
                                               {
                                                   Id = item.Id,
                                                   Address = item.Address,
                                                   PostCode = item.PostCode,
                                                   State = item.State,
                                                   Suburb = item.Suburb,
                                                   StreetName = item.StreetName,
                                                   StreetNo = item.StreetNo,
                                                   StreetType = item.StreetType,
                                                   UnitNo = item.UnitNo,
                                                   UnitType = item.UnitType,
                                                   CompanyName = item.CompanyName,
                                                   Email = item.Email,
                                                   Mobile = item.Mobile,
                                                   Phone = item.Phone,
                                                   Requirements = item.Requirements,
                                                   IsDuplicate = item.IsDuplicate,
                                                   IsWebDuplicate = item.IsWebDuplicate,
                                                   CreationTime = item.CreationTime,
                                                   LeadSourceName = _lookup_leadSourceRepository.GetAll().Where(e => e.Name == item.LeadSource).Select(e => e.Name).FirstOrDefault(),
                                                   OrganizationId = item.OrganizationId,
                                                   OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == item.OrganizationId).Select(e => e.DisplayName).FirstOrDefault(),
                                                   //CurrentLeadOwner = string.IsNullOrEmpty(Convert.ToString(item.AssignToUserID)) ? "-" : _userRepository.GetAll().Where(e => e.Id == item.AssignToUserID).Select(e => e.FullName).FirstOrDefault()
                                               }).ToList()
                        };

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetDuplicateLeadForViewDto>(
                totalCount,
                await leads.ToListAsync()
            );
        }

        /// <summary>
        /// Duplicate Lead Export to Excel
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<FileDto> getDublicateLeadsToExcel(LeadExcelExportTto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var filteredLeads = _leadRepository.GetAll()
                        .Include(e => e.LeadStatusFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.StartDate != null && input.EndDate != null, e => e.ChangeStatusDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ChangeStatusDate.Value.AddHours(10).Date <= EDate.Value.AddDays(1).Date)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                        .WhereIf(string.IsNullOrWhiteSpace(input.DuplicateFilter), e => e.IsDuplicate == true || e.IsWebDuplicate == true)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DuplicateFilter), e => input.DuplicateFilter == "web" ? e.IsWebDuplicate == true : e.IsDuplicate == true);

            var query = (from o in filteredLeads
                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()
                         join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                         from s4 in j4.DefaultIfEmpty()

                         select new GetDuplicateLeadForViewDto()
                         {
                             DuplicateLead = new DuplicateLeadDto
                             {
                                 CompanyName = o.CompanyName,
                                 LeadSource = o.LeadSource,
                                 OrganizationName = s4.DisplayName,
                                 Email = o.Email,
                                 Phone = o.Phone,
                                 Mobile = o.Mobile,
                                 Address = o.Address,
                                 Suburb = o.Suburb,
                                 State = o.State,
                                 PostCode = o.PostCode,
                                 CreationTime = o.CreationTime,
                             },
                             LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                             Duplicate = (o.IsDuplicate == false || string.IsNullOrEmpty(o.Email) || string.IsNullOrEmpty(o.Mobile)) ? false : (o.IsDuplicate == true) ? true : _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.OrganizationId == input.OrganizationUnit && e.AssignToUserID != null).Any(),
                             WebDuplicate = (o.IsWebDuplicate == false || string.IsNullOrEmpty(o.Email) || string.IsNullOrEmpty(o.Mobile)) ? false : (o.IsWebDuplicate == true) ? true : _leadRepository.GetAll().Where(e => ((e.Email == o.Email && e.Email != null && o.Email != null) || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.OrganizationId == input.OrganizationUnit && e.IsDuplicate != true && e.IsWebDuplicate != true).Any(),
                             Total = Convert.ToString(filteredLeads.Count()),
                         });

            var dublicateleadListDtos = await query.ToListAsync();

            return _leadsExcelExporter.DublicateLeadExportToFile(dublicateleadListDtos);
        }

        /// <summary>
        /// My Leads API(Only Assigned Leads - CUrrent User)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetLeadForViewDto>> GetAllForMyLead(GetAllLeadsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            int UserId = (int)AbpSession.UserId;
            var statusIds = new List<int>();
            statusIds.Add(1);
            statusIds.Add(7);
            statusIds.Add(8);
            statusIds.Add(9);

            //var joblist = new List<int?>();
            //if (input.jobStatusIDFilter != null && input.jobStatusIDFilter != 0)
            //{
            //	joblist = _jobRepository.GetAll().Where(e => e.JobStatusId == input.jobStatusIDFilter).Select(e => e.LeadId).ToList();
            //}
            var jobnumberlist = new List<int?>();
            //if (input.ProjectNumberFilter != null)
            //{
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            //}
            var FollowupList = _leadactivityRepository.GetAll().WhereIf(input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
            var List = _leadactivityRepository.GetAll().Where(e => e.ActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).ToList();
            var NextFollowupList = List.WhereIf(input.StartDate != null && input.EndDate != null, e => e.ActivityDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActivityDate.Value.AddHours(10).Date <= EDate.Value.Date).Select(e => e.LeadId);
           
            var joblist = new List<int?>();
            if (input.JobStatusID != null && input.JobStatusID.Count() > 0 && input.LeadStatusId == 6)
            {
                joblist = _jobRepository.GetAll().Where(e => input.JobStatusID.Contains((int)e.JobStatusId)).Select(e => e.LeadId).ToList();
            }

            var filteredLeads = _leadRepository.GetAll()
                        .Include(e => e.LeadStatusFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter)
                        .WhereIf(input.LeadStatusId > 0, e => e.LeadStatusId == input.LeadStatusId)
                        .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.LeadAssignDate.Value.AddHours(10).Date >= SDate.Value.Date && e.LeadAssignDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))
                        .WhereIf(input.DateFilterType == "NextFollowup" && input.StartDate != null && input.EndDate != null, e => NextFollowupList.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                        .Where(e => !statusIds.Contains(e.LeadStatusId))
                        .WhereIf(input.JobStatusID != null && input.JobStatusID.Count() > 0, e => joblist.Contains(e.Id))
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                        //.WhereIf(input.LeadSourceIdFilter != 0 && input.LeadSourceIdFilter != null, e => e.LeadSourceId == input.LeadSourceIdFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.ProjectNumberFilter), e => jobnumberlist.Contains(e.Id))
                        .Where(e => e.AssignToUserID == UserId);

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "LeadAssignDate desc")
                .PageBy(input);

            var LeadidList = _jobRepository.GetAll().Where(e=>e.DepositeRecceivedDate != null).Select(e => e.LeadId).ToList();
            var leads = from o in pagedAndFilteredLeads

                        join o1 in _lookup_rejectReasonRepository.GetAll() on o.RejectReasonId equals o1.Id into j1
                        from s1 in j1.DefaultIfEmpty()

                        join o2 in _lookup_cancelReasonRepository.GetAll() on o.LeadStatusId equals o2.Id into j2
                        from s2 in j2.DefaultIfEmpty()

                        join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                        from s3 in j3.DefaultIfEmpty()

                        join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                        from s4 in j4.DefaultIfEmpty()

                        join o5 in _leadactivityRepository.GetAll() on o.Id equals o5.LeadId into j5
                        from s5 in j5.DefaultIfEmpty()

                        join o6 in _jobRepository.GetAll() on o.Id equals o6.LeadId into j6
                        from s6 in j6.DefaultIfEmpty()

                        select new GetLeadForViewDto()
                        {
                            Lead = new LeadDto
                            {
                                CompanyName = o.CompanyName,
                                Email = o.Email,
                                Phone = o.Phone,
                                Mobile = o.Mobile,
                                Address = o.Address,
                                Requirements = o.Requirements,
                                Id = o.Id,
                                PostCode = o.PostCode,
                                LeadStatusID = o.LeadStatusId,
                                Suburb = o.Suburb,
                                LeadSource = o.LeadSource,
                                State = o.State,
                                OrganizationId = o.OrganizationId,
                                OrganizationName = s4.DisplayName,
                                longitude = o.longitude,
                                latitude = o.latitude,
                                FollowupDate = s5.CreationTime,
                                CreationTime = (DateTime)o.LeadAssignDate,
                                IsGoogle = o.IsGoogle
                            },
                            LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                            Duplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID != null && e.OrganizationId == input.OrganizationUnit).Any(),
                            WebDuplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.OrganizationUnit).Any(),
                            LastComment = _leadactivityRepository.GetAll().Where(e => e.LeadId == o.Id && e.ActionId == 8).OrderByDescending(x => x.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                            LastCommentDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == o.Id && e.ActionId == 8).OrderByDescending(x => x.Id).Select(e => e.ActivityDate).FirstOrDefault(),

                            ActivityReminderTime = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 13 || e.SectionId == 14 ) && e.ActionId == 8 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                            ActivityDescription = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 13 || e.SectionId == 14) && e.ActionId == 8 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                            ActivityComment = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 13 || e.SectionId == 14) && e.ActionId == 24 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                            JobStatusName = _lookup_jobStatusRepository.GetAll().Where(e => e.Id == s6.JobStatusId).Select(e => e.Name).FirstOrDefault(),

                            New = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 1).Count()),
                            UnHandled = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 2).Count()),
                            Hot = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 5).Count()),
                            Cold = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 3).Count()),
                            Warm = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 4).Count()),
                            Upgrade = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 6).Count()),
                            Rejected = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 7).Count()),
                            Cancelled = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 8).Count()),
                            Closed = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 9).Count()),
                            Total = Convert.ToString(filteredLeads.Count()),
                            Assigned = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 10).Count()),
                            ReAssigned = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 11).Count()),
                            Sold = Convert.ToString(filteredLeads.Where(e => LeadidList.Contains(e.Id)).Count()),
                        };

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetLeadForViewDto>(
                totalCount,
                leads.DistinctBy(e => e.Lead.Id).ToList()
            );
        }

        /// <summary>
        /// My Leads Export to Excel API
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<FileDto> getMyLeadToExcel(LeadExcelExportTto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            int UserId = (int)AbpSession.UserId;
            var statusIds = new List<int>();
            statusIds.Add(1);
            statusIds.Add(7);
            statusIds.Add(8);
            statusIds.Add(9);

            var jobnumberlist = new List<int?>();
            //if (input.ProjectNumberFilter != null)
            //{
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            //}

            var joblist = new List<int?>();
            if (input.jobStatusIDFilter != null && input.jobStatusIDFilter != 0)
            {
                joblist = _jobRepository.GetAll().Where(e => e.JobStatusId == input.jobStatusIDFilter).Select(e => e.LeadId).ToList();
            }

            var FollowupList = _leadactivityRepository.GetAll().WhereIf(input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
            var List = _leadactivityRepository.GetAll().Where(e => e.ActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).ToList();
            var NextFollowupList = List.WhereIf(input.StartDate != null && input.EndDate != null, e => e.ActivityDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActivityDate.Value.AddHours(10).Date <= EDate.Value.Date).Select(e => e.LeadId);

            var filteredLeads = _leadRepository.GetAll()
                        .Include(e => e.LeadStatusFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter)
                        .WhereIf(input.LeadStatusId > 0, e => e.LeadStatusId == input.LeadStatusId)
                        .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.LeadAssignDate.Value.AddHours(10).Date >= SDate.Value.Date && e.LeadAssignDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))
                        .WhereIf(input.DateFilterType == "NextFollowup" && input.StartDate != null && input.EndDate != null, e => NextFollowupList.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                        .Where(e => !statusIds.Contains(e.LeadStatusId))
                        .WhereIf(input.jobStatusIDFilter != null, e => joblist.Contains(e.Id))
                        .Where(e => e.AssignToUserID == UserId);

            var query = (from o in filteredLeads

                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                         from s4 in j4.DefaultIfEmpty()

                         join o5 in _leadactivityRepository.GetAll() on o.Id equals o5.LeadId into j5
                         from s5 in j5.DefaultIfEmpty()

                         select new GetLeadForViewDto()
                         {
                             Lead = new LeadDto
                             {
                                 CompanyName = o.CompanyName,
                                 Email = o.Email,
                                 Phone = o.Phone,
                                 Mobile = o.Mobile,
                                 Address = o.Address,
                                 Requirements = o.Requirements,
                                 Id = o.Id,
                                 PostCode = o.PostCode,
                                 LeadStatusID = o.LeadStatusId,
                                 Suburb = o.Suburb,
                                 LeadSource = o.LeadSource,
                                 State = o.State,
                                 OrganizationId = o.OrganizationId,
                                 OrganizationName = s4.DisplayName,
                                 longitude = o.longitude,
                                 latitude = o.latitude,
                                 FollowupDate = s5.CreationTime,
                                 CreationTime = (DateTime)o.LeadAssignDate
                             },
                             LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),

                         });

            var myleadListDtos = await query.ToListAsync();

            return _leadsExcelExporter.MyLeadExportToFile(myleadListDtos);
        }

        /// <summary>
        /// Closed Leads API (LeadStatusID = 9)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetLeadForViewDto>> GetAllForMyClosedLead(GetAllLeadsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var jobnumberlist = new List<int?>();
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

            var filteredLeads = _leadRepository.GetAll()
                        .Include(e => e.LeadStatusFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadStatusId != null, e => e.LeadStatusId == input.LeadStatusId)
                        .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                        .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                        .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .Where(e => e.LeadStatusId == 9);

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "ChangeStatusDate desc")
                .PageBy(input);

            var leads = from o in pagedAndFilteredLeads

                        join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                        from s3 in j3.DefaultIfEmpty()

                        join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                        from s4 in j4.DefaultIfEmpty()

                        select new GetLeadForViewDto()
                        {
                            Lead = new LeadDto
                            {
                                CompanyName = o.CompanyName,
                                Email = o.Email,
                                Phone = o.Phone,
                                Mobile = o.Mobile,
                                Address = o.Address,
                                Requirements = o.Requirements,
                                Id = o.Id,
                                PostCode = o.PostCode,
                                LeadStatusID = o.LeadStatusId,
                                Suburb = o.Suburb,
                                LeadSource = o.LeadSource,
                                State = o.State,
                                OrganizationId = o.OrganizationId,
                                OrganizationName = s4.DisplayName
                            },
                            LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                            Duplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID != null && e.OrganizationId == input.OrganizationUnit).Any(),
                            WebDuplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.OrganizationUnit).Any(),
                        };

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetLeadForViewDto>(
                totalCount,
                await leads.ToListAsync()
            );
        }

        /// <summary>
        /// Closed Lead Export to Excel API
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<FileDto> getClosedLeadsToExcel(LeadExcelExportTto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var jobnumberlist = new List<int?>();
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

            var filteredLeads = _leadRepository.GetAll()
                        .Include(e => e.LeadStatusFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadStatusId != null, e => e.LeadStatusId == input.LeadStatusId)
                    .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                        .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                        .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .Where(e => e.LeadStatusId == 9);

            var query = (from o in filteredLeads

                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                         from s4 in j4.DefaultIfEmpty()

                         select new GetLeadForViewDto()
                         {
                             Lead = new LeadDto
                             {
                                 CompanyName = o.CompanyName,
                                 Email = o.Email,
                                 Phone = o.Phone,
                                 Mobile = o.Mobile,
                                 Address = o.Address,
                                 Requirements = o.Requirements,
                                 Id = o.Id,
                                 PostCode = o.PostCode,
                                 LeadStatusID = o.LeadStatusId,
                                 Suburb = o.Suburb,
                                 LeadSource = o.LeadSource,
                                 State = o.State,
                                 OrganizationId = o.OrganizationId,
                                 OrganizationName = s4.DisplayName
                             },
                             LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                             Duplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID != null && e.OrganizationId == input.OrganizationUnit).Any(),
                             WebDuplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.OrganizationUnit).Any(),
                         });

            var closedleadListDtos = await query.ToListAsync();

            return _leadsExcelExporter.ClosedLeadExportToFile(closedleadListDtos);
        }

        /// <summary>
        /// Cancel Leads API (LeadStatusID = 8)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetLeadForViewDto>> GetAllForCancelLead(GetAllLeadsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = _userTeamRepository.GetAll().Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }

            var jobnumberlist = new List<int?>();
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

            var filteredLeads = _leadRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                        //.WhereIf(input.LeadSourceIdFilter != 0 && input.LeadSourceIdFilter != null, e => e.LeadSourceId == input.LeadSourceIdFilter)
                        .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateFilterType == "Cancel" && input.StartDate != null && input.EndDate != null, e => e.ChangeStatusDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ChangeStatusDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.OrganizationUnit != 0, e => e.OrganizationId == input.OrganizationUnit)
                        .WhereIf(input.CancelReasonId != 0, e => e.CancelReasonId == input.CancelReasonId)
                        .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.AssignToUserID))
                        .Where(e => e.LeadStatusId == 8);

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "ChangeStatusDate desc")
                .PageBy(input);

            var leads = from o in pagedAndFilteredLeads
                        join o1 in _userRepository.GetAll() on o.LastModifierUserId equals o1.Id into j1
                        from s1 in j1.DefaultIfEmpty()

                        join o2 in _lookup_cancelReasonRepository.GetAll() on o.LeadStatusId equals o2.Id into j2
                        from s2 in j2.DefaultIfEmpty()

                        join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                        from s3 in j3.DefaultIfEmpty()
                        join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                        from s4 in j4.DefaultIfEmpty()

                        select new GetLeadForViewDto()
                        {
                            Lead = new LeadDto
                            {
                                CompanyName = o.CompanyName,
                                Email = o.Email,
                                Phone = o.Phone,
                                Mobile = o.Mobile,
                                Address = o.Address,
                                Requirements = o.Requirements,
                                Id = o.Id,
                                PostCode = o.PostCode,
                                RejectReason = o.RejectReason,
                                RejectReasonId = o.RejectReasonId,
                                CancelReasonId = o.CancelReasonId,
                                LeadStatusID = o.LeadStatusId,
                                Suburb = o.Suburb,
                                LeadSource = o.LeadSource,
                                State = o.State,
                                OrganizationId = o.OrganizationId,
                                OrganizationName = s4.DisplayName,
                                ChangeStatusDate = o.ChangeStatusDate
                            },
                            LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                            LeadSourceName = _lookup_leadSourceRepository.GetAll().Where(e => (e.Id == o.LeadSourceId)).Select(e => e.Name).FirstOrDefault(),
                            CancelReasonName = _lookup_cancelReasonRepository.GetAll().Where(e => (e.Id == o.CancelReasonId)).Select(e => e.CancelReasonName).FirstOrDefault(),
                            LastModifiedUser = _userRepository.GetAll().Where(e => (e.Id == o.LastModifierUserId)).Select(e => e.Name).FirstOrDefault(),
                            Duplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID != null && e.OrganizationId == input.OrganizationUnit).Any(),
                            WebDuplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.OrganizationUnit).Any(),
                            Facebook = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Facebook").Count()),
                            Online = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Google").Count()),
                            Tv = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "TV").Count()),
                            Referral = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Referral").Count()),
                            Others = Convert.ToString(filteredLeads.Where(e => e.LeadSource != "Facebook" && e.LeadSource != "Google" && e.LeadSource != "Tv" && e.LeadSource != "Refeal").Count()),
                            Total = Convert.ToString(filteredLeads.Count())
                        };

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetLeadForViewDto>(
                totalCount,
                await leads.ToListAsync()
            );
        }

        /// <summary>
        /// Cancel Lead Export to Excel API
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<FileDto> getCancelLeadsToExcel(LeadExcelExportTto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var jobnumberlist = new List<int?>();
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

            var filteredLeads = _leadRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                        //.WhereIf(input.LeadSourceIdFilter != 0 && input.LeadSourceIdFilter != null, e => e.LeadSourceId == input.LeadSourceIdFilter)
                        .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateFilterType == "Cancel" && input.StartDate != null && input.EndDate != null, e => e.ChangeStatusDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ChangeStatusDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.OrganizationUnit != 0, e => e.OrganizationId == input.OrganizationUnit)
                        .WhereIf(input.CancelReasonId != 0, e => e.CancelReasonId == input.CancelReasonId)
                        .Where(e => e.LeadStatusId == 8);

            var query = (from o in filteredLeads
                         join o1 in _userRepository.GetAll() on o.LastModifierUserId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _lookup_cancelReasonRepository.GetAll() on o.LeadStatusId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()
                         join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                         from s4 in j4.DefaultIfEmpty()

                         select new GetLeadForViewDto()
                         {
                             Lead = new LeadDto
                             {
                                 CompanyName = o.CompanyName,
                                 Email = o.Email,
                                 Phone = o.Phone,
                                 Mobile = o.Mobile,
                                 Address = o.Address,
                                 Requirements = o.Requirements,
                                 Id = o.Id,
                                 PostCode = o.PostCode,
                                 RejectReason = o.RejectReason,
                                 RejectReasonId = o.RejectReasonId,
                                 CancelReasonId = o.CancelReasonId,
                                 LeadStatusID = o.LeadStatusId,
                                 Suburb = o.Suburb,
                                 LeadSource = o.LeadSource,
                                 State = o.State,
                                 OrganizationId = o.OrganizationId,
                                 OrganizationName = s4.DisplayName
                             },
                             LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                             LeadSourceName = _lookup_leadSourceRepository.GetAll().Where(e => (e.Id == o.LeadSourceId)).Select(e => e.Name).FirstOrDefault(),
                             CancelReasonName = _lookup_cancelReasonRepository.GetAll().Where(e => (e.Id == o.CancelReasonId)).Select(e => e.CancelReasonName).FirstOrDefault(),
                             LastModifiedUser = _userRepository.GetAll().Where(e => (e.Id == o.LastModifierUserId)).Select(e => e.Name).FirstOrDefault(),
                             Duplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID != null && e.OrganizationId == input.OrganizationUnit).Any(),
                             WebDuplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.OrganizationUnit).Any(),
                         });

            var cancelleadListDtos = await query.ToListAsync();

            return _leadsExcelExporter.CancelLeadExportToFile(cancelleadListDtos);
        }

        /// <summary>
        /// Cancel Leads API (LeadStatusID = 7)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetLeadForViewDto>> GetAllForRejectedLead(GetAllLeadsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var jobnumberlist = new List<int?>();
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = _userTeamRepository.GetAll().Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }

            var filteredLeads = _leadRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                        //.WhereIf(input.LeadSourceIdFilter != 0 && input.LeadSourceIdFilter != null, e => e.LeadSourceId == input.LeadSourceIdFilter)
                        .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateFilterType == "Rejected" && input.StartDate != null && input.EndDate != null, e => e.ChangeStatusDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ChangeStatusDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.OrganizationUnit != 0, e => e.OrganizationId == input.OrganizationUnit)
                        .WhereIf(input.RejectReasonId != 0, e => e.RejectReasonId == input.RejectReasonId)
                        .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.AssignToUserID))
                        .Where(e => e.LeadStatusId == 7);

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "ChangeStatusDate desc")
                .PageBy(input);

            var leads = from o in pagedAndFilteredLeads

                        join o2 in _userRepository.GetAll() on o.LastModifierUserId equals o2.Id into j2
                        from s2 in j2.DefaultIfEmpty()

                        join o1 in _lookup_rejectReasonRepository.GetAll() on o.RejectReasonId equals o1.Id into j1
                        from s1 in j1.DefaultIfEmpty()

                        join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                        from s3 in j3.DefaultIfEmpty()

                        join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                        from s4 in j4.DefaultIfEmpty()

                        select new GetLeadForViewDto()
                        {
                            Lead = new LeadDto
                            {
                                CompanyName = o.CompanyName,
                                Email = o.Email,
                                Phone = o.Phone,
                                Mobile = o.Mobile,
                                Address = o.Address,
                                Requirements = o.Requirements,
                                Id = o.Id,
                                PostCode = o.PostCode,
                                LeadStatusID = o.LeadStatusId,
                                Suburb = o.Suburb,
                                LeadSource = o.LeadSource,
                                State = o.State,
                                RejectReason = o.RejectReason,
                                RejectReasonId = o.RejectReasonId,
                                CancelReasonId = o.CancelReasonId,
                                OrganizationId = o.OrganizationId,
                                OrganizationName = s4.DisplayName,
                                ChangeStatusDate = o.ChangeStatusDate
                            },
                            LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                            LastModifiedUser = _userRepository.GetAll().Where(e => (e.Id == o.LastModifierUserId)).Select(e => e.Name).FirstOrDefault(),
                            LeadSourceName = _lookup_leadSourceRepository.GetAll().Where(e => (e.Id == o.LeadSourceId)).Select(e => e.Name).FirstOrDefault(),
                            RejectReasonName = _lookup_rejectReasonRepository.GetAll().Where(e => (e.Id == o.RejectReasonId)).Select(e => e.RejectReasonName).FirstOrDefault(),
                            Duplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID != null && e.OrganizationId == input.OrganizationUnit).Any(),
                            WebDuplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.OrganizationUnit).Any(),
                            Facebook = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Facebook").Count()),
                            Online = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Google").Count()),
                            Tv = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "TV").Count()),
                            Referral = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Referral").Count()),
                            Others = Convert.ToString(filteredLeads.Where(e => e.LeadSource != "Facebook" && e.LeadSource != "Google" && e.LeadSource != "Tv" && e.LeadSource != "Refeal").Count()),
                            Total = Convert.ToString(filteredLeads.Count())
                        };

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetLeadForViewDto>(
                totalCount,
                await leads.ToListAsync()
            );
        }

        /// <summary>
        /// Reject Lead Export to Excel API
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<FileDto> getRejectLeadsToExcel(LeadExcelExportTto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var jobnumberlist = new List<int?>();
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

            var filteredLeads = _leadRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                        //.WhereIf(input.LeadSourceIdFilter != 0 && input.LeadSourceIdFilter != null, e => e.LeadSourceId == input.LeadSourceIdFilter)
                        .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateFilterType == "Rejected" && input.StartDate != null && input.EndDate != null, e => e.ChangeStatusDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ChangeStatusDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.OrganizationUnit != 0, e => e.OrganizationId == input.OrganizationUnit)
                        .WhereIf(input.RejectReasonId != 0, e => e.RejectReasonId == input.RejectReasonId)
                        .Where(e => e.LeadStatusId == 7);

            var query = (from o in filteredLeads

                         join o2 in _userRepository.GetAll() on o.LastModifierUserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o1 in _lookup_rejectReasonRepository.GetAll() on o.RejectReasonId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                         from s4 in j4.DefaultIfEmpty()

                         select new GetLeadForViewDto()
                         {
                             Lead = new LeadDto
                             {
                                 CompanyName = o.CompanyName,
                                 Email = o.Email,
                                 Phone = o.Phone,
                                 Mobile = o.Mobile,
                                 Address = o.Address,
                                 Requirements = o.Requirements,
                                 Id = o.Id,
                                 PostCode = o.PostCode,
                                 LeadStatusID = o.LeadStatusId,
                                 Suburb = o.Suburb,
                                 LeadSource = o.LeadSource,
                                 State = o.State,
                                 RejectReason = o.RejectReason,
                                 RejectReasonId = o.RejectReasonId,
                                 CancelReasonId = o.CancelReasonId,
                                 OrganizationId = o.OrganizationId,
                                 OrganizationName = s4.DisplayName
                             },
                             LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                             LastModifiedUser = _userRepository.GetAll().Where(e => (e.Id == o.LastModifierUserId)).Select(e => e.Name).FirstOrDefault(),
                             LeadSourceName = _lookup_leadSourceRepository.GetAll().Where(e => (e.Id == o.LeadSourceId)).Select(e => e.Name).FirstOrDefault(),
                             RejectReasonName = _lookup_rejectReasonRepository.GetAll().Where(e => (e.Id == o.RejectReasonId)).Select(e => e.RejectReasonName).FirstOrDefault(),
                             Duplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID != null && e.OrganizationId == input.OrganizationUnit).Any(),
                             WebDuplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.OrganizationUnit).Any(),

                         });

            var rejectleadListDtos = await query.ToListAsync();

            return _leadsExcelExporter.RejectLeadExportToFile(rejectleadListDtos);
        }

        /// <summary>
        /// Lead Detail View API
        /// </summary>
        /// <param name="id">Lead Id</param>
        /// <returns></returns>
        public async Task<GetLeadForViewDto> GetLeadForView(int id)
        {
            var lead = await _leadRepository.GetAsync(id);

            var output = new GetLeadForViewDto { Lead = ObjectMapper.Map<LeadDto>(lead) };

            //if (output.Lead.Suburb != null)
            //{
            //	var _lookupPostCode = await _lookup_postCodeRepository.FirstOrDefaultAsync((int)output.Lead.Suburb);
            //	output.PostCodeSuburb = _lookupPostCode?.Suburb?.ToString();
            //}

            //if (output.Lead.State != null)
            //{
            //	var _lookupState = await _lookup_stateRepository.FirstOrDefaultAsync((int)output.Lead.State);
            //	output.StateName = _lookupState?.Name?.ToString();
            //}

            //if (output.Lead.LeadSource != null)
            //{
            //	var _lookupLeadSource = await _lookup_leadSourceRepository.FirstOrDefaultAsync((int)output.Lead.LeadSource);
            //	output.LeadSourceName = _lookupLeadSource?.Name?.ToString();
            //}

            var jobid = _jobRepository.GetAll().Where(e => e.LeadId == id).Select(e => e.Id).FirstOrDefault();
            if (jobid != null)
            {
                var StatusId = _jobRepository.GetAll().Where(e => e.Id == jobid).Select(e => e.JobStatusId).FirstOrDefault();
                output.JobStatusId = StatusId;
                output.JobStatusName = _lookup_jobStatusRepository.GetAll().Where(e => e.Id == StatusId).Select(e => e.Name).FirstOrDefault();
                output.JobNumber = _jobRepository.GetAll().Where(e => e.LeadId == id).Select(e => e.JobNumber).FirstOrDefault();
                output.SystemCapacity = _jobRepository.GetAll().Where(e => e.LeadId == id).Select(e => e.SystemCapacity).FirstOrDefault();
                output.TotalQuotaion = _jobRepository.GetAll().Where(e => e.LeadId == id).Select(e => e.TotalCost).FirstOrDefault();
                output.InstallationDate = _jobRepository.GetAll().Where(e => e.LeadId == id).Select(e => e.InstallationDate).FirstOrDefault();
                var InstallationId = _jobRepository.GetAll().Where(e => e.LeadId == id).Select(e => e.InstallerId).FirstOrDefault();
                if (InstallationId != null)
                {
                    var _lookupInstallerName = await _userRepository.FirstOrDefaultAsync((int)InstallationId);
                    output.InstallerName = _lookupInstallerName?.FullName?.ToString();
                }
                var JobProductIteam = _jobProductItemRepository.GetAll().Where(e => e.JobId == jobid).ToList();
                if (JobProductIteam.Count > 0)
                {
                    var QunityAndModelList = (from o in JobProductIteam
                                              join o1 in _ProductItemRepository.GetAll() on o.ProductItemId equals o1.Id into j1
                                              from s1 in j1.DefaultIfEmpty()
                                              select new
                                              {
                                                  Name = o.Quantity + " x " + s1.Name
                                              }).ToList();
                    output.QunityAndModelList = String.Join(",", QunityAndModelList.Select(p => p.Name));
                }

            }



            if (output.Lead.LeadStatusID != null)
            {
                var _lookupLeadStatus = await _lookup_leadStatusRepository.FirstOrDefaultAsync((int)output.Lead.LeadStatusID);
                output.LeadStatusName = _lookupLeadStatus?.Status?.ToString();
            }

            if (lead.CreatorUserId != null)
            {
                var _lookupUserStatus = await _userRepository.FirstOrDefaultAsync((int)lead.CreatorUserId);
                output.CreatedByName = _lookupUserStatus?.FullName?.ToString();
            }

            if (lead.AssignToUserID != null)
            {
                var _lookupUserStatus = await _userRepository.FirstOrDefaultAsync((int)lead.AssignToUserID);
                output.CurrentAssignUserName = _lookupUserStatus?.FullName?.ToString();
                output.CurrentAssignUserEmail = _lookupUserStatus?.EmailAddress?.ToString();
                output.CurrentAssignUserMobile = _lookupUserStatus?.PhoneNumber?.ToString();
            }
            else
            {
                var DupLead = _leadRepository.GetAll().Where(e => (e.Email == lead.Email || e.Mobile == lead.Mobile) && e.Id != lead.Id && e.OrganizationId == lead.OrganizationId && e.AssignToUserID != null).FirstOrDefault();
                if (DupLead != null)
                {
                    var _lookupUserStatus = await _userRepository.FirstOrDefaultAsync((int)DupLead.AssignToUserID);
                    output.CurrentAssignUserName = _lookupUserStatus?.FullName?.ToString();
                }
            }

            output.CreatedOn = lead.CreationTime;
            output.latitude = lead.latitude;
            output.longitude = lead.longitude;
            return output;
        }

        /// <summary>
        /// Check Lead Existancy (Address, Email, Phone)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<List<GetDuplicateLeadPopupDto>> CheckExistLeadList(CheckExistLeadDto input)
        {
            var User = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

            IList<string> role = await _userManager.GetRolesAsync(User);

            //if (role.Contains("Admin"))
            //{
            //	return new List<GetDuplicateLeadPopupDto>();
            //}
            var Mobile = input.Mobile.Substring(input.Mobile.Length - 9);

            var duplicateLead = _leadRepository.GetAll().Where(e => e.Mobile.Contains(Mobile) || e.Email == input.Email ||
                                (e.UnitNo == input.UnitNo && e.UnitType == input.UnitType && e.StreetNo == input.StreetNo && e.StreetName == input.StreetName
                                && e.StreetType == input.StreetType && e.Suburb == input.Suburb && e.State == input.State && e.PostCode == input.PostCode))
                                .Where(e => e.OrganizationId == input.OrganizationId)
                                .WhereIf(input.Id != null, e => e.Id != input.Id)
                                .Where(e => e.IsDuplicate != true && e.IsWebDuplicate != true);

            var leads = from o in duplicateLead

                        join o1 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o1.Id into j1
                        from s1 in j1.DefaultIfEmpty()

                        join o2 in _userRepository.GetAll() on o.CreatorUserId equals o2.Id into j2
                        from s2 in j2.DefaultIfEmpty()

                        select new GetDuplicateLeadPopupDto()
                        {
                            CreatedByName = s2.FullName,
                            CreationTime = o.CreationTime,
                            Address = o.Address,
                            CompanyName = o.CompanyName,
                            Email = o.Email,
                            Id = o.Id,
                            Requirements = o.Requirements,
                            Phone = o.Phone,
                            Mobile = o.Mobile,
                            LeadSource = o.LeadSource,
                            LeadStatus = s1.Status,
                            CurrentAssignUserName = _userRepository.GetAll().Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                            UnitNo = o.UnitNo,
                            UnitType = o.UnitType,
                            StreetNo = o.StreetNo,
                            StreetType = o.StreetType,
                            StreetName = o.StreetName,
                            Suburb = o.Suburb,
                            State = o.State,
                            Postcode = o.PostCode,
                            CurrentLeadOwaner = s2.FullName,
                            AddressExist = (o.UnitNo == input.UnitNo && o.UnitType == input.UnitType && o.StreetNo == input.StreetNo && o.StreetName == input.StreetName && o.StreetType == input.StreetType && o.Suburb == input.Suburb && o.State == input.State && o.PostCode == input.PostCode),
                            EmailExist = o.Email == input.Email,
                            MobileExist = o.Mobile == input.Mobile,
                            RoleName = role[0]
                        };

            return new List<GetDuplicateLeadPopupDto>(
                await leads.ToListAsync()
            );
        }

        /// <summary>
        /// Get Duplicate Lead Details (Email or Phone)
        /// </summary>
        /// <param name="id">Lead Id</param>
        /// <returns></returns>
        public async Task<List<GetDuplicateLeadPopupDto>> GetDuplicateLeadForView(int id)
        {
            var lead = await _leadRepository.GetAsync(id);

            var duplicateLead = _leadRepository.GetAll().Where(e => ((e.Email == lead.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == lead.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != lead.Id && e.AssignToUserID != null);

            var leads = from o in duplicateLead
                        join o1 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o1.Id into j1
                        from s1 in j1.DefaultIfEmpty()
                        join o2 in _userRepository.GetAll() on o.CreatorUserId equals o2.Id into j2
                        from s2 in j2.DefaultIfEmpty()

                        join o3 in _jobRepository.GetAll() on o.Id equals o3.LeadId into j3
                        from s3 in j3.DefaultIfEmpty()

                            //join o4 in _lookup_jobStatusRepository.GetAll() on s3.JobStatusId equals o4.Id into j4
                            //from s4 in j4.DefaultIfEmpty()

                        select new GetDuplicateLeadPopupDto()
                        {
                            CreatedByName = s2.FullName, //_userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                            CreationTime = o.CreationTime,
                            Address = o.Address,
                            CompanyName = o.CompanyName,
                            Email = o.Email,
                            Id = o.Id,
                            Requirements = o.Requirements,
                            Phone = o.Phone,
                            Mobile = o.Mobile,
                            LeadSource = o.LeadSource,
                            LeadStatus = s1.Status,
                            CurrentAssignUserName = _userRepository.GetAll().Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                            UnitNo = o.UnitNo,
                            UnitType = o.UnitType,
                            StreetNo = o.StreetNo,
                            StreetType = o.StreetType,
                            StreetName = o.StreetName,
                            Suburb = o.Suburb,
                            State = o.State,
                            Postcode = o.PostCode,
                            ProjectStatus = s3.JobStatusFk.Name,
                            ProjectOpenDate = s3.CreationTime,
                            ProjectNo = s3.JobNumber,
                            LastFollowupDate = _leadactivityRepository.GetAll().Where(e => e.ActionId == 8 && e.LeadId == o.Id).OrderByDescending(x => x.Id).Select(e => e.CreationTime).FirstOrDefault(),
                            Description = _leadactivityRepository.GetAll().Where(e => e.ActionId == 8 && e.LeadId == o.Id).OrderByDescending(x => x.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                            LastQuoteDate = _quotationRepository.GetAll().Where(e=>e.JobFk.LeadId == o.Id).OrderByDescending(x => x.Id).Select(e => e.CreationTime).FirstOrDefault()

                        };

            return new List<GetDuplicateLeadPopupDto>(
                await leads.ToListAsync()
            );
        }

        /// <summary>
        /// Get Lead Data for Edit
        /// </summary>
        /// <param name="input">Lead Id</param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Leads_Edit, AppPermissions.Pages_MyLeads)]
        public async Task<GetLeadForEditOutput> GetLeadForEdit(EntityDto input)
        {
            var lead = await _leadRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLeadForEditOutput { Lead = ObjectMapper.Map<CreateOrEditLeadDto>(lead) };

            //if (AbpSession.UserId != null)
            //	output.CurrentUserId = (int)AbpSession.UserId;

            var User = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            if (role.Contains("Admin"))
            {
                output.OwnTeamUser = true;
            }
            else if (role.Contains("Sales Manager"))
            {
                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
                var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
                output.OwnTeamUser = UserList.Contains(lead.AssignToUserID);
            }
            else
            {
                output.OwnTeamUser = (User.Id == lead.AssignToUserID);
            }

            //if (output.Lead.Suburb != null)
            //{
            //	var _lookupPostCode = await _lookup_postCodeRepository.FirstOrDefaultAsync((int)output.Lead.Suburb);
            //	output.PostCodeSuburb = _lookupPostCode?.Suburb?.ToString();
            //}

            //if (output.Lead.State != null)
            //{
            //	var _lookupState = await _lookup_stateRepository.FirstOrDefaultAsync((int)output.Lead.State);
            //	output.StateName = _lookupState?.Name?.ToString();
            //}

            //if (output.Lead.LeadSource != null)
            //{
            //	var _lookupLeadSource = await _lookup_leadSourceRepository.FirstOrDefaultAsync((int)output.Lead.LeadSource);
            //	output.LeadSourceName = _lookupLeadSource?.Name?.ToString();
            //}

            if (output.Lead.LeadStatusID != null)
            {
                var _lookupLeadStatus = await _lookup_leadStatusRepository.FirstOrDefaultAsync((int)output.Lead.LeadStatusID);
                output.LeadStatusName = _lookupLeadStatus?.Status?.ToString();
            }



            return output;
        }

        /// <summary>
        /// Create Or Edit Leads
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task CreateOrEdit(CreateOrEditLeadDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        /// <summary>
        /// Create Lead
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Leads_Create, AppPermissions.Pages_MyLeads)]
        protected virtual async Task Create(CreateOrEditLeadDto input)
        {
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

            IList<string> role = await _userManager.GetRolesAsync(assignedToUser);

            input.LeadSourceId = _lookup_leadSourceRepository.GetAll().Where(e => e.Name == input.LeadSource).Select(e => e.Id).FirstOrDefault();
            if (input.StateId == null)
            {
                input.StateId = _lookup_stateRepository.GetAll().Where(e => e.Name == input.State).Select(e => e.Id).FirstOrDefault();
            }
            if (input.SuburbId == null)
            {
                input.StateId = _lookup_postCodeRepository.GetAll().Where(e => e.PostalCode == input.PostCode).Select(e => e.Id).FirstOrDefault();
            }
            //if (input.PostalStateId == null)
            //{
            //	input.StateId = _lookup_stateRepository.GetAll().Where(e => e.Name == input.PostalState).Select(e => e.Id).FirstOrDefault();
            //}
            //if (input.PostalSuburbId == null)
            //{
            //	input.StateId = _lookup_postCodeRepository.GetAll().Where(e => e.PostalCode == input.PostalPostCode).Select(e => e.Id).FirstOrDefault();
            //}
            var lead = ObjectMapper.Map<Lead>(input);
            if (input.IsExternalLead == 3)
            {
                lead.Address = input.Address;
            }
            else
            {
                lead.Address = input.UnitNo + " " + input.UnitType + " " + input.StreetNo + " " + input.StreetName + " " + input.StreetType;
            }
            //lead.PostalAddress = lead.PostalUnitNo + " " + lead.PostalUnitType + " " + lead.PostalStreetNo + " " + lead.PostalStreetName + " " + lead.PostalStreetType;
            lead.IsPromotion = true;
            if (!string.IsNullOrWhiteSpace(input.Suburb))
            {
                string[] splitPipeValue = input.Suburb.Split('|');
                if (splitPipeValue.Length > 0)
                {
                    lead.Suburb = splitPipeValue[0].ToString();
                }
                else
                {
                    lead.Suburb = input.Suburb;
                }
            }

            //1. Create
            //2. Excel Import
            //3. External Link
            //4. Copy Lead
            lead.IsExternalLead = input.IsExternalLead;
            lead.LeadAssignDate = DateTime.UtcNow;

            if (input.from == "mylead")
            {
                lead.AssignToUserID = (int)AbpSession.UserId;
                lead.LeadStatusId = 2;
            }
            else if (input.from == "myleads")
            {
                lead.AssignToUserID = (int)AbpSession.UserId;
                lead.LeadStatusId = 2;
            }
            else
            {
                lead.LeadStatusId = 1;
            }
            await _leadRepository.InsertAndGetIdAsync(lead);

            PreviousJobStatus jobStstus = new PreviousJobStatus();
            jobStstus.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                jobStstus.TenantId = (int)AbpSession.TenantId;
            }
            jobStstus.CurrentID = lead.LeadStatusId;
            jobStstus.PreviousId = 0;
            await _previousJobStatusRepository.InsertAsync(jobStstus);

            //1. Create
            //2. Excel Import
            //3. External Link
            //4. Copy Lead
            if (lead.IsExternalLead == 3)
            {
                var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == lead.CreatorUserId).FirstOrDefault();
                string msg = string.Format("New Lead Created {0}.", lead.CompanyName);
                await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
            }

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == lead.CreatorUserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 1;
            leadactivity.SectionId = 0;
            leadactivity.ActionNote = "Lead Created";
            leadactivity.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);

        }

        /// <summary>
        /// Update Lead
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Leads_Edit, AppPermissions.Pages_MyLeads)]
        protected virtual async Task Update(CreateOrEditLeadDto input)
        {
            if (input.StateId == null)
            {
                input.StateId = _lookup_stateRepository.GetAll().Where(e => e.Name == input.State).Select(e => e.Id).FirstOrDefault();
            }
            if (input.SuburbId == null)
            {
                input.StateId = _lookup_postCodeRepository.GetAll().Where(e => e.PostalCode == input.PostCode).Select(e => e.Id).FirstOrDefault();
            }
            var lead = await _leadRepository.FirstOrDefaultAsync((int)input.Id);

            if (lead.LeadStatusId != input.LeadStatusID)
            {
                PreviousJobStatus jobStstus = new PreviousJobStatus();
                jobStstus.LeadId = lead.Id;
                if (AbpSession.TenantId != null)
                {
                    jobStstus.TenantId = (int)AbpSession.TenantId;
                }
                jobStstus.CurrentID = (int)input.LeadStatusID;
                jobStstus.PreviousId = lead.LeadStatusId;
                await _previousJobStatusRepository.InsertAsync(jobStstus);
                lead.ChangeStatusDate = DateTime.UtcNow;
            }
            lead.Address = input.UnitNo + " " + input.UnitType + " " + input.StreetNo + " " + input.StreetName + " " + input.StreetType;
            if (!string.IsNullOrWhiteSpace(input.Suburb))
            {
                string[] splitPipeValue = input.Suburb.Split('|');
                if (splitPipeValue.Length > 0)
                {
                    lead.Suburb = splitPipeValue[0].ToString();
                }
                else
                {
                    lead.Suburb = input.Suburb;
                }
            }

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 2;
            leadactivity.SectionId = 0;
            leadactivity.ActionNote = "Lead Modified";
            leadactivity.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);
            #region edithistory

            var List = new List<LeadtrackerHistory>();
            if (lead.CompanyName != input.CompanyName)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "CompanyName";
                leadStatus.PrevValue = lead.CompanyName;
                leadStatus.CurValue = input.CompanyName;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Email != input.Email)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Email";
                leadStatus.PrevValue = lead.Email;
                leadStatus.CurValue = input.Email;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Phone != input.Phone)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Phone";
                leadStatus.PrevValue = lead.Phone;
                leadStatus.CurValue = input.Phone;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Mobile != input.Mobile)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Mobile";
                leadStatus.PrevValue = lead.Mobile;
                leadStatus.CurValue = input.Mobile;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Address != input.Address)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Address";
                leadStatus.PrevValue = lead.Address;
                leadStatus.CurValue = input.Address;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            }
            if (lead.ExcelAddress != input.ExcelAddress)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "ExcelAddress";
                leadStatus.PrevValue = lead.ExcelAddress;
                leadStatus.CurValue = input.ExcelAddress;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            }
            if (lead.Requirements != input.Requirements)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Requirements";
                leadStatus.PrevValue = lead.Requirements;
                leadStatus.CurValue = input.Requirements;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Suburb != input.Suburb)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Suburb";
                leadStatus.PrevValue = lead.Suburb;
                leadStatus.CurValue = input.Suburb;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            }
            if (lead.State != input.State)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "State";
                leadStatus.PrevValue = lead.State;
                leadStatus.CurValue = input.State;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.PostCode != input.PostCode)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "PostCode";
                leadStatus.PrevValue = lead.PostCode;
                leadStatus.CurValue = input.PostCode;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.LeadSource != input.LeadSource)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "LeadSource";
                leadStatus.PrevValue = lead.LeadSource;
                leadStatus.CurValue = input.LeadSource;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.UnitNo != input.UnitNo)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "UnitNo";
                leadStatus.PrevValue = lead.UnitNo;
                leadStatus.CurValue = input.UnitNo;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.UnitType != input.UnitType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "UnitType";
                leadStatus.PrevValue = lead.UnitType;
                leadStatus.CurValue = input.UnitType;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.StreetNo != input.StreetNo)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "StreetNo";
                leadStatus.PrevValue = lead.StreetNo;
                leadStatus.CurValue = input.StreetNo;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.StreetName != input.StreetName)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "StreetName";
                leadStatus.PrevValue = lead.StreetName;
                leadStatus.CurValue = input.StreetName;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.StreetType != input.StreetType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "StreetType";
                leadStatus.PrevValue = lead.StreetType;
                leadStatus.CurValue = input.StreetType;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            }
            if (lead.LeadSourceId != input.LeadSourceId)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "LeadSourceId";
                leadStatus.PrevValue = Convert.ToString(from ld in _lookup_leadSourceRepository.GetAll() where ld.Id == lead.LeadSourceId select ld.Name);
                leadStatus.CurValue = Convert.ToString(from ld in _lookup_leadSourceRepository.GetAll() where ld.Id == input.LeadSourceId select ld.Name);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.SuburbId != input.SuburbId)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "SuburbId";
                leadStatus.PrevValue = Convert.ToString(lead.SuburbId);
                leadStatus.CurValue = Convert.ToString(input.SuburbId);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.StateId != input.StateId)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "StateId";
                leadStatus.PrevValue = Convert.ToString(lead.StateId);
                leadStatus.CurValue = Convert.ToString(input.StateId);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            }
            if (lead.LeadStatusId != input.LeadStatusID)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "LeadStatusId";
                leadStatus.PrevValue = Convert.ToString(from ld in _lookup_leadStatusRepository.GetAll() where ld.Id == lead.LeadStatusId select ld.Status);
                leadStatus.CurValue = Convert.ToString(from ld in _lookup_leadStatusRepository.GetAll() where ld.Id == input.LeadStatusID select ld.Status);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.AssignToUserID != input.AssignToUserID)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "AssignToUserID";
                leadStatus.PrevValue = Convert.ToString(from ld in _userRepository.GetAll() where ld.Id == lead.AssignToUserID select ld.Name);
                leadStatus.CurValue = Convert.ToString(from ld in _userRepository.GetAll() where ld.Id == input.AssignToUserID select ld.Name);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            }
            if (lead.latitude != input.latitude)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "latitude";
                leadStatus.PrevValue = Convert.ToString(lead.latitude);
                leadStatus.CurValue = Convert.ToString(input.latitude);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.longitude != input.longitude)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "longitude";
                leadStatus.PrevValue = Convert.ToString(lead.longitude);
                leadStatus.CurValue = Convert.ToString(input.longitude);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.IsGoogle != input.IsGoogle)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "IsGoogle";
                leadStatus.PrevValue = Convert.ToString(lead.IsGoogle);
                leadStatus.CurValue = Convert.ToString(input.IsGoogle);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            //if (lead.IsDuplicate != input.IsDuplicate)
            //{
            //	LeadtrackerHistory leadStatus = new LeadtrackerHistory();
            //	if (AbpSession.TenantId != null)
            //	{
            //		leadStatus.TenantId = (int)AbpSession.TenantId;
            //	}
            //	leadStatus.FieldName = "IsDuplicate";
            //	leadStatus.PrevValue = Convert.ToString(lead.IsDuplicate);
            //	leadStatus.CurValue = Convert.ToString(input.IsDuplicate);
            //	leadStatus.Action = "Edit";
            //	leadStatus.LastmodifiedDateTime = DateTime.Now;
            //	leadStatus.LeadId = lead.Id;
            //	leadStatus.LeadActionId = leadactid;
            //	List.Add(leadStatus);
            //	//await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            //}

            if (lead.AltPhone != input.AltPhone)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "AltPhone";
                leadStatus.PrevValue = Convert.ToString(lead.AltPhone);
                leadStatus.CurValue = Convert.ToString(input.AltPhone);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }


            if (lead.Type != input.Type)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Type";
                leadStatus.PrevValue = Convert.ToString(lead.Type);
                leadStatus.CurValue = Convert.ToString(input.Type);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Area != input.Area)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Area";
                leadStatus.PrevValue = Convert.ToString(lead.Area);
                leadStatus.CurValue = Convert.ToString(input.Area);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.ABN != input.ABN)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "ABN";
                leadStatus.PrevValue = Convert.ToString(lead.ABN);
                leadStatus.CurValue = Convert.ToString(input.ABN);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Fax != input.Fax)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Fax";
                leadStatus.PrevValue = Convert.ToString(lead.Fax);
                leadStatus.CurValue = Convert.ToString(input.Fax);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }

            if (lead.SystemType != input.SystemType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "SystemType";
                leadStatus.PrevValue = Convert.ToString(lead.SystemType);
                leadStatus.CurValue = Convert.ToString(input.SystemType);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now; leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.RoofType != input.RoofType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "RoofType";
                leadStatus.PrevValue = Convert.ToString(lead.RoofType);
                leadStatus.CurValue = Convert.ToString(input.RoofType);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now; leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }

            if (lead.AngleType != input.AngleType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "AngleType";
                leadStatus.PrevValue = Convert.ToString(lead.AngleType);
                leadStatus.CurValue = Convert.ToString(input.AngleType);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now; leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.StoryType != input.StoryType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "StoryType";
                leadStatus.PrevValue = Convert.ToString(lead.StoryType);
                leadStatus.CurValue = Convert.ToString(input.StoryType);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now; leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }

            if (lead.HouseAgeType != input.HouseAgeType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "HouseAgeType";
                leadStatus.PrevValue = Convert.ToString(lead.HouseAgeType);
                leadStatus.CurValue = Convert.ToString(input.HouseAgeType);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now; leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);


            }



            await _dbcontextprovider.GetDbContext().LeadtrackerHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            #endregion
            ObjectMapper.Map(input, lead);

        }

        /// <summary>
        /// Lead Status Change
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Leads_Edit, AppPermissions.Pages_MyLeads)]
        public virtual async Task ChangeStatus(GetLeadForChangeStatusOutput input)
        {
            var lead = await _leadRepository.FirstOrDefaultAsync((int)input.Id);

            PreviousJobStatus jobStstus = new PreviousJobStatus();
            jobStstus.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                jobStstus.TenantId = (int)AbpSession.TenantId;
            }
            jobStstus.CurrentID = (int)input.LeadStatusID;
            jobStstus.PreviousId = lead.LeadStatusId;
            await _previousJobStatusRepository.InsertAsync(jobStstus);

            lead.CancelReasonId = input.CancelReasonId;
            lead.RejectReasonId = input.RejectReasonId;
            lead.LeadStatusId = (int)input.LeadStatusID;
            lead.ChangeStatusDate = DateTime.UtcNow;
            if (!input.RejectReason.IsNullOrEmpty())
            {
                lead.RejectReason = input.RejectReason;
            }
            await _leadRepository.UpdateAsync(lead);

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 5;
            leadactivity.SectionId = 0;
            if (input.LeadStatusID == 7)
            {
                leadactivity.ActionNote = "Lead Rejected, Reason : " + input.RejectReason;
            }
            else
            {
                leadactivity.ActionNote = "Lead Status Changed";
            }

            leadactivity.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Leads_Edit, AppPermissions.Pages_MyLeads)]
        public async Task<GetLeadForChangeStatusOutput> GetLeadForChangeStatus(EntityDto input)
        {
            var lead = await _leadRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLeadForChangeStatusOutput();
            output.Id = lead.Id;
            output.LeadStatusID = lead.LeadStatusId;
            output.CompanyName = lead.CompanyName;

            if (output.LeadStatusID != null)
            {
                var _lookupLeadStatus = await _lookup_leadStatusRepository.FirstOrDefaultAsync((int)output.LeadStatusID);
                output.StatusName = _lookupLeadStatus?.Status?.ToString();
            }

            return output;
        }

        /// <summary>
        /// Delete Multiple Leads
        /// </summary>
        /// <param name="input">Lead Id[]</param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Leads_Delete, AppPermissions.Pages_MyLeads)]
        public async Task Delete(int[] input)
        {
            foreach (var item in input)
            {
                try
                {
                    var JobLeadId = _jobRepository.GetAll().Where(e => e.LeadId == item).Select(e => e.LeadId).FirstOrDefault();

                    if (JobLeadId > 0)
                    {
                        await _jobProductItemRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);
                        await _jobPromotionRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);
                        await _jobVariationRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);

                        await _jobRepository.DeleteAsync((int)JobLeadId);
                    }
                }
                catch (Exception e)
                {

                }

                await _leadRepository.DeleteAsync(item);
            }
        }

        /// <summary>
        /// Delete Lead
        /// </summary>
        /// <param name="input">Lead Id</param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Leads_Delete, AppPermissions.Pages_MyLeads)]
        public async Task DeleteLeads(EntityDto input)
        {
            try
            {
                var JobLeadId = _jobRepository.GetAll().Where(e => e.LeadId == input.Id).Select(e => e.LeadId).FirstOrDefault();

                if (JobLeadId > 0)
                {
                    await _jobProductItemRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);
                    await _jobPromotionRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);
                    await _jobVariationRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);

                    await _jobRepository.DeleteAsync((int)JobLeadId);
                }
            }
            catch (Exception e)
            {

            }

            await _leadRepository.DeleteAsync(input.Id);
        }

        /// <summary>
        /// Delete Duplicate Lead
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Leads_Delete, AppPermissions.Pages_MyLeads)]
        public async Task DeleteDuplicateLeads(EntityDto input)
        {
            var lead = await _leadRepository.GetAsync(input.Id);

            var duplicateLead = (_leadRepository.GetAll().Where(e => ((e.Email == lead.Email || e.Mobile == lead.Mobile) && (!string.IsNullOrEmpty(e.Mobile) && !string.IsNullOrEmpty(e.Email))) && e.Id != lead.Id && e.AssignToUserID != null && e.OrganizationId == lead.OrganizationId));
            //_leadRepository.GetAll().Where(e => (e.Phone == lead.Phone || e.Mobile == lead.Mobile || e.Email == lead.Email) && e.Id != lead.Id && e.AssignToUserID != null);


            foreach (var item in duplicateLead)
            {
                try
                {
                    var JobLeadId = _jobRepository.GetAll().Where(e => e.LeadId == item.Id).Select(e => e.LeadId).FirstOrDefault();

                    if (JobLeadId > 0)
                    {
                        await _jobProductItemRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);
                        await _jobPromotionRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);
                        await _jobVariationRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);

                        await _jobRepository.DeleteAsync((int)JobLeadId);
                    }
                }
                catch (Exception e)
                {

                }

                await _leadRepository.DeleteAsync(item.Id);
            }
        }

        /// <summary>
        /// Lead Excel Export
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<FileDto> GetLeadsToExcel(GetAllLeadsForExcelInput input)
        {

            var filteredLeads = _leadRepository.GetAll()
                        //.Include(e => e.SuburbFk)
                        //.Include(e => e.StateFk)
                        .Include(e => e.LeadStatusFk)
                        //.Include(e => e.LeadSourceFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.SuburbFk != null && e.SuburbFk.Suburb == input.PostCodeSuburbFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.StateFk != null && e.StateFk.Name == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StreetNameFilter), e => e.StreetName == input.StreetNameFilter);
            //.WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSourceFk != null && e.LeadSourceFk.Name == input.LeadSourceNameFilter);

            var query = (from o in filteredLeads
                             //join o1 in _lookup_postCodeRepository.GetAll() on o.Suburb equals o1.Id into j1
                             //from s1 in j1.DefaultIfEmpty()

                             //join o2 in _lookup_stateRepository.GetAll() on o.State equals o2.Id into j2
                             //from s2 in j2.DefaultIfEmpty()

                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                             //join o4 in _lookup_leadSourceRepository.GetAll() on o.LeadSource equals o4.Id into j4
                             //from s4 in j4.DefaultIfEmpty()

                         select new GetLeadForViewDto()
                         {
                             Lead = new LeadDto
                             {
                                 CompanyName = o.CompanyName,
                                 Email = o.Email,
                                 Phone = o.Phone,
                                 Mobile = o.Mobile,
                                 Address = o.Address,
                                 Requirements = o.Requirements,
                                 Id = o.Id,
                                 PostCode = o.PostCode
                             },
                             //PostCodeSuburb = s1 == null || s1.Suburb == null ? "" : s1.Suburb.ToString(),
                             //StateName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                             LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                             //LeadSourceName = s4 == null || s4.Name == null ? "" : s4.Name.ToString(),
                         });


            var leadListDtos = await query.ToListAsync();

            return _leadsExcelExporter.ExportToFile(leadListDtos);
        }

        /// <summary>
        /// Reject Reason Dropdown
        /// </summary>
        /// <returns></returns>
        public async Task<List<LookupTableDto>> GetAllRejectReasonForTableDropdown()
        {
            return await _lookup_rejectReasonRepository.GetAll()
                .Select(rejectReason => new LookupTableDto
                {
                    Id = rejectReason.Id,
                    DisplayName = rejectReason == null || rejectReason.RejectReasonName == null ? "" : rejectReason.RejectReasonName.ToString()
                }).ToListAsync();
        }

        /// <summary>
        /// Cancel Reason Dropdown
        /// </summary>
        /// <returns></returns>
        public async Task<List<LookupTableDto>> GetAllCancelReasonForTableDropdown()
        {
            return await _lookup_cancelReasonRepository.GetAll()
                .Select(cancelReason => new LookupTableDto
                {
                    Id = cancelReason.Id,
                    DisplayName = cancelReason == null || cancelReason.CancelReasonName == null ? "" : cancelReason.CancelReasonName.ToString()
                }).ToListAsync();
        }

        /// <summary>
        /// Lead Status Dropdown
        /// </summary>
        /// <param name="lead"></param>
        /// <returns></returns>
        public async Task<List<LeadStatusLookupTableDto>> GetAllLeadStatusForTableDropdown(string lead)
        {
            return await _lookup_leadStatusRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(lead), c => c.Status.ToLower().Contains(lead.ToLower()))
                .Select(leadStatus => new LeadStatusLookupTableDto
                {
                    Value = leadStatus.Id,
                    Name = leadStatus == null || leadStatus.Status == null ? "" : leadStatus.Status.ToString()
                }).ToListAsync();
        }

        /// <summary>
        /// Get Value for Auto Complete
        /// </summary>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        public NameValue<string> SendAndGetSelectedValue(NameValue<string> selectedValue)
        {
            return selectedValue;
        }

        /// <summary>
        /// Get Suburb Id from Postcode
        /// </summary>
        /// <param name="PostCode">Postcode</param>
        /// <returns></returns>
        public int GetSuburbId(string PostCode)
        {
            return _lookup_postCodeRepository.GetAll().Where(P => P.PostalCode == PostCode).Select(P => P.Id).FirstOrDefault();
        }

        /// <summary>
        /// Get State Id from State Name
        /// </summary>
        /// <param name="StateName">State Name</param>
        /// <returns></returns>
        public int StateId(string StateName)
        {
            return _lookup_stateRepository.GetAll().Where(P => P.Name == StateName).Select(P => P.Id).FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Leads_Assign)]
        public async Task<GetLeadForAssignOrTransferOutput> GetLeadForAssignOrTransfer(EntityDto input)
        {
            var lead = await _leadRepository.GetAsync(input.Id);
            var output = new GetLeadForAssignOrTransferOutput();
            output.CompanyName = lead.CompanyName;
            output.Id = lead.Id;

            if (output.AssignToUserID != null)
            {
                var _lookupUser = await _userRepository.FirstOrDefaultAsync((int)output.AssignToUserID);
                output.UserName = _lookupUser?.FullName?.ToString();
            }
            return output;
        }

        public async Task<List<LeadUsersLookupTableDto>> GetAllUserLeadAssignDropdown(int OId)
        {
            var User_List = _userRepository
           .GetAll();

            var UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Sales Manager" && uo != null && uo.OrganizationUnitId == OId) //|| us.DisplayName == "Sales Rep"
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            });

            return new List<LeadUsersLookupTableDto>(
                await UserList.ToListAsync()
            );
        }

        public async Task<List<LeadUsersLookupTableDto>> GetSalesManagerUser(int OId)
        {
            int UserId = (int)AbpSession.UserId;
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == UserId).Select(e => e.TeamId).ToList();
            var UsersList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).ToList();

            var User_List = _userRepository
           .GetAll();

            var RoleName = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            where (us != null && user.Id == UserId)
                            select (us.DisplayName));

            var CurrentRoleName = "Sales Manager";
            if (RoleName.Contains("Sales Manager"))
            {
                User_List.Where(e => UsersList.Contains(e.Id));
                CurrentRoleName = "Sales Rep";
            }

            var UserList = new List<LeadUsersLookupTableDto>();

            if (RoleName.Contains("Admin"))
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            where us != null && us.DisplayName == CurrentRoleName && uo != null && uo.OrganizationUnitId == OId
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).OrderBy(d => d.DisplayName).ToList();
            }
            else
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()
                            where us != null && us.DisplayName == CurrentRoleName && uo != null && uo.OrganizationUnitId == OId && TeamId.Contains(ut.TeamId)
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).OrderBy(d => d.DisplayName).ToList();
            }


            return new List<LeadUsersLookupTableDto>(
                UserList
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Leads_Assign)]
        public async Task AssignOrTransferLead(GetLeadForAssignOrTransferOutput input)
        {
            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == input.AssignToUserID).FirstOrDefault();



            IList<string> role = await _userManager.GetRolesAsync(assignedToUser);
            var roleName = "";
            if (role.Contains("Sales Manager"))
            {
                roleName = "Sales Manager";
            }
            else
            {
                roleName = "Sales Rep";
            }

            foreach (var leadid in input.LeadIds)
            {
                var lead = await _leadRepository.FirstOrDefaultAsync(leadid);

                if (lead.LeadStatusId == 7 || lead.LeadStatusId == 8)
                {
                    PreviousJobStatus jobStstus = new PreviousJobStatus();
                    jobStstus.LeadId = lead.Id;
                    if (AbpSession.TenantId != null)
                    {
                        jobStstus.TenantId = (int)AbpSession.TenantId;
                    }
                    jobStstus.CurrentID = 11;
                    jobStstus.PreviousId = lead.LeadStatusId;
                    await _previousJobStatusRepository.InsertAsync(jobStstus);
                }

                if (lead.AssignToUserID == null)
                {
                    lead.AssignToUserID = input.AssignToUserID;
                    if (lead.LeadAssignDate == null)
                    {
                        lead.LeadAssignDate = DateTime.UtcNow;
                    }
                    if (input.OrganizationID != null)
                    {
                        lead.OrganizationId = (int)input.OrganizationID;
                    }
                    await _leadRepository.UpdateAsync(lead);

                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 3;
                    leadactivity.SectionId = 0;
                    if (lead.LeadStatusId == 7)
                    {
                        leadactivity.ActionNote = "Rejected Lead Assign To " + assignedToUser.FullName;
                    }
                    else if (lead.LeadStatusId == 8)
                    {
                        leadactivity.ActionNote = "Canceled Lead Assign To " + assignedToUser.FullName;
                    }
                    else
                    {
                        leadactivity.ActionNote = "Lead Assign To " + assignedToUser.FullName;
                    }

                    leadactivity.LeadId = lead.Id;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    leadactivity.CreatorUserId = AbpSession.UserId;
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }
                else
                {
                    var assignedFromUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();

                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 4;
                    leadactivity.SectionId = 0;
                    if (lead.LeadStatusId == 7)
                    {
                        leadactivity.ActionNote = "Rejected Lead Transfer From " + assignedFromUser.FullName + " To " + assignedToUser.FullName;
                    }
                    else if (lead.LeadStatusId == 8)
                    {
                        leadactivity.ActionNote = "Canceled Lead Transfer From " + assignedFromUser.FullName + " To " + assignedToUser.FullName;
                    }
                    else
                    {
                        leadactivity.ActionNote = "Lead Transfer From " + assignedFromUser.FullName + " To " + assignedToUser.FullName;
                    }

                    leadactivity.LeadId = lead.Id;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    leadactivity.CreatorUserId = AbpSession.UserId;
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }

                if (lead.LeadStatusId == 1)
                {
                    PreviousJobStatus jobStstus = new PreviousJobStatus();
                    jobStstus.LeadId = lead.Id;
                    if (AbpSession.TenantId != null)
                    {
                        jobStstus.TenantId = (int)AbpSession.TenantId;
                    }
                    jobStstus.CurrentID = 10;
                    jobStstus.PreviousId = lead.LeadStatusId;
                    await _previousJobStatusRepository.InsertAsync(jobStstus);
                    lead.LeadStatusId = 10;
                }
                else if (lead.LeadStatusId == 10)
                {

                }
                else
                {
                    lead.LeadStatusId = 11;
                    lead.LeadAssignDate = DateTime.UtcNow;
                }

                lead.AssignToUserID = input.AssignToUserID;
                await _leadRepository.UpdateAsync(lead);
            }

            if (input.LeadIds.Count > 0)
            {
                var User_List = _userRepository.GetAll().ToList();
                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == input.AssignToUserID).Select(e => e.TeamId).FirstOrDefault();

                var UserList = (from user in User_List
                                join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                from ur in urJoined.DefaultIfEmpty()

                                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                from us in usJoined.DefaultIfEmpty()

                                join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                                from ut in utJoined.DefaultIfEmpty()

                                where us != null && us.DisplayName == roleName && ut != null && ut.TeamId == TeamId
                                select user).Distinct();

                foreach (var item in UserList)
                {
                    if (item.Id == assignedToUser.Id)
                    {
                        string msg = string.Format("{0} Lead Assigned To You", input.LeadIds.Count);
                        await _appNotifier.LeadAssiged(item, msg, NotificationSeverity.Info);

                        //Send Email
                        try
                        {
                            var OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationID).Select(e => e.DisplayName).FirstOrDefault();
                            await _userEmailer.SendEmailNewLeadAssign(item, Convert.ToString(input.LeadIds.Count));
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    else
                    {
                        if (roleName != "Sales Rep")
                        {
                            string msg = string.Format("{0} Lead Assigned To {1}", input.LeadIds.Count, assignedToUser.FullName);
                            await _appNotifier.LeadAssiged(item, msg, NotificationSeverity.Info);
                        }
                        if (roleName != "Sales Rep")
                        {
                            //Send Email
                            try
                            {
                                var OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationID).Select(e => e.DisplayName).FirstOrDefault();
                                await _userEmailer.SendEmailNewLeadToAssignTeamMember(item, Convert.ToString(input.LeadIds.Count), assignedToUser.FullName);
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Leads_Assign)]
        public async Task TransferLeadToOtherOrganization(GetLeadForAssignOrTransferOutput input)
        {
            var ChangedOrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationID).Select(e => e.DisplayName).FirstOrDefault();
            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == input.AssignToUserID).FirstOrDefault();

            foreach (var leadid in input.LeadIds)
            {
                var lead = await _leadRepository.FirstOrDefaultAsync(leadid);
                var assignedFromUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
                var PreviousOrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == lead.OrganizationId).Select(e => e.DisplayName).FirstOrDefault();
                if (lead.LeadAssignDate == null)
                {
                    lead.LeadAssignDate = DateTime.UtcNow;
                }
                lead.AssignToUserID = input.AssignToUserID;
                lead.OrganizationId = (int)input.OrganizationID;
                await _leadRepository.UpdateAsync(lead);

                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 4;
                leadactivity.SectionId = 0;
                leadactivity.ActionNote = "Lead Transfered To " + assignedToUser.FullName + " In " + ChangedOrganizationName + "Organization From " + assignedFromUser.FullName + ", " + PreviousOrganizationName + " Organization";
                leadactivity.LeadId = lead.Id;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                leadactivity.CreatorUserId = AbpSession.UserId;
                await _leadactivityRepository.InsertAsync(leadactivity);
            }

            var User_List = _userRepository.GetAll().ToList();
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == input.AssignToUserID).Select(e => e.TeamId).FirstOrDefault();

            var UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()

                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()

                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()

                            where us != null && us.DisplayName == "Sales Manager" && ut != null && ut.TeamId == TeamId
                            select user);

            foreach (var item in UserList)
            {
                if (item.Id == assignedToUser.Id)
                {
                    string msg = string.Format("{0} New Lead Assigned To You", input.LeadIds.Count);
                    await _appNotifier.LeadAssiged(item, msg, NotificationSeverity.Info);
                }
                else
                {
                    string msg = string.Format("{0} New Lead Assigned To {0}", input.LeadIds.Count, assignedToUser.FullName);
                    await _appNotifier.LeadAssiged(item, msg, NotificationSeverity.Info);
                }
            }
        }

        public async Task<List<GetActivityLogViewDto>> GetLeadActivityLog(GetActivityLogInput input)
        {

            List<int> leadActionids = new List<int>() { 1, 2, 3, 4, 5 };
            List<int> smsActionids = new List<int>() { 6, 20 };
            List<int> jobActionids = new List<int>() { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 };
            List<int> promotionActionids = new List<int>() { 21, 22 };
            var allactionids = _lookup_leadActionRepository.GetAll().Select(e => e.Id).ToList();
            var Result = _leadactivityRepository
                .GetAll()
                .Where(L => L.LeadId == input.LeadId)
                .WhereIf(input.ActionId == 0, L => allactionids.Contains(L.ActionId))
                .WhereIf(input.ActionId == 1, L => leadActionids.Contains(L.ActionId))
                .WhereIf(input.ActionId == 6, L => smsActionids.Contains(L.ActionId))
                .WhereIf(input.ActionId == 7, L => L.ActionId == 7)
                .WhereIf(input.ActionId == 8, L => L.ActionId == 8)
                .WhereIf(input.ActionId == 9, L => L.ActionId == 9)
                .WhereIf(input.ActionId == 10, L => jobActionids.Contains(L.ActionId))
                .WhereIf(input.ActionId == 11, L => promotionActionids.Contains(L.ActionId))
                .WhereIf(input.OnlyMy != false, L => L.CreatorUserId == AbpSession.UserId)
                .OrderByDescending(e => e.Id);

            var leads = from o in Result
                        join o1 in _lookup_leadActionRepository.GetAll() on o.ActionId equals o1.Id into j1
                        from s1 in j1.DefaultIfEmpty()

                        join o2 in _leadRepository.GetAll() on o.LeadId equals o2.Id into j2
                        from s2 in j2.DefaultIfEmpty()

                        join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                        from s3 in j3.DefaultIfEmpty()

                        select new GetActivityLogViewDto()
                        {
                            Id = o.Id,
                            ActionName = s1 == null || s1.ActionName == null ? "" : s1.ActionName.ToString(),
                            ActionId = o.ActionId,
                            ActionNote = o.ActionNote,
                            ActivityNote = o.ActionId == 6 ? o.Body : o.ActivityNote,
                            LeadId = o.LeadId,
                            CreationTime = o.CreationTime,
                            ActivityDate = o.ActivityDate,
                            CreatorUserName = s3 == null || s3.FullName == null ? "" : s3.FullName.ToString(),
                            LeadCompanyName = s2 == null || s2.CompanyName == null ? "" : s2.CompanyName.ToString()
                        };

            //return leads.Select(trip => new GetActivityLogViewDto()
            //{
            //    ActionName = trip.ActionName,
            //    ActionId = trip.ActionId,
            //    ActionNote1 = trip.ActionNote.Split("By").ToList(),
            //    ActionNote = trip.ActionNote1[0],
            //    ActivityNote = trip.ActivityNote,
            //    LeadId = trip.LeadId,
            //    CreationTime = trip.CreationTime,
            //    CreatorUserName = trip.CreatorUserName,
            //    LeadCompanyName = trip.LeadCompanyName
            //}).ToList();


            return new List<GetActivityLogViewDto>(
                await leads.ToListAsync()
            );
        }
        public async Task<List<LeadtrackerHistoryDto>> GetLeadActivityLogHistory(GetActivityLogInput input)
        {
            try
            {
                //List<int> leadActionids = new List<int>() { 1, 2, 3, 4, 5 };

                var allactionids = _lookup_leadActionRepository.GetAll().Select(e => e.Id).ToList();
                var Result = (from item in _LeadtrackerHistoryRepository.GetAll()
                              join ur in _userRepository.GetAll() on item.CreatorUserId equals ur.Id into urjoined
                              from ur in urjoined.DefaultIfEmpty()
                              join leadact in _leadactivityRepository.GetAll() on item.LeadActionId equals leadact.Id into leadjoined
                              from leadact in leadjoined.DefaultIfEmpty()
                              where (item.LeadActionId == input.LeadId)
                              select new LeadtrackerHistoryDto()
                              {
                                  Id = item.Id,
                                  FieldName = item.FieldName,
                                  PrevValue = item.PrevValue,
                                  CurValue = item.CurValue,
                                  Lastmodifiedbyuser = ur.Name + " " + ur.Surname
                              })
                    .OrderByDescending(e => e.Id).ToList();
                return Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task AddActivityLog(ActivityLogInput input)
        {
            var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var lead = await _leadRepository.FirstOrDefaultAsync(input.LeadId);
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
            
            var activity = ObjectMapper.Map<LeadActivityLog>(input);
            activity.ActionNote = input.ActionNote;
            activity.Subject = input.Subject;
            activity.SectionId = input.SectionId;
            var SectionName = _SectionAppService.GetAll().Where(e => e.SectionId == input.SectionId).Select(e => e.SectionName).FirstOrDefault();
            if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
            {
                activity.TemplateId = input.EmailTemplateId;
            }
            else if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
            {
                activity.TemplateId = input.SMSTemplateId;
            }
            else
            {
                activity.TemplateId = 0;
            }

            if (AbpSession.TenantId != null)
            {
                input.TenantId = (int)AbpSession.TenantId;
            }
            if (input.ActionId == 9)
            {
                string msg = input.ActionNote + " " + input.ActivityNote  + " For " + lead.CompanyName + " By " + CurrentUser.Name + " From " + SectionName;
                await _appNotifier.LeadComment(assignedToUser, msg, NotificationSeverity.Warn);
            }
            if (input.ActionId == 25)
            {
                var assignedToUserForTodo = _userRepository.GetAll().Where(u => u.Id == input.UserId).FirstOrDefault();
                string msg = input.ActionNote + " " + input.ActivityNote + " For " + lead.CompanyName + " By " + CurrentUser.Name + " From " + SectionName;
                await _appNotifier.LeadComment(assignedToUserForTodo, msg, NotificationSeverity.Warn);
            }
            if (input.ActionId == 8)
            {
                var CurrentTime = (_timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId));
                if (Convert.ToDateTime(input.Body) < CurrentTime)
                {
                    throw new UserFriendlyException(L("PleaseSelectGreaterDatethanCurrentDate"));
                }
                activity.ActivityDate = Convert.ToDateTime(input.Body);
                input.Body = "";
                string msg = string.Format("Reminder " + input.ActionNote + " " + input.ActivityNote + " For " + lead.CompanyName + " By "+ CurrentUser.Name + " From " + SectionName);
                await _appNotifier.LeadAssiged(CurrentUser, msg, NotificationSeverity.Info);
            }
            if (input.ActionId == 7)
            {
                //MailMessage mail = new MailMessage
                //{
                //    //From = new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                //    //To = { lead.Email },
                //    //Subject = input.Subject,//EmailBody.TemplateName + "The Solar Product Lead Notification",
                //    //Body = input.Body,
                //    //IsBodyHtml = true
                //};
                await _emailSender.SendAsync(new MailMessage
                {
                    To = { lead.Email }, ////{ "hiral.prajapati@meghtechnologies.com" },
                    Subject = input.Subject,//EmailBody.TemplateName + "The Solar Product Lead Notification",
                    Body = input.Body,
                    IsBodyHtml = true
                });
                //foreach (var item in input.AttachmentList)
                //{
                //    //Attach Data as Email Attachment
                //    //MemoryStream pdf = new MemoryStream(bytes);
                //    //Attachment data = new Attachment(pdf, fileName);
                //    //mail.Attachments.Add(data);
                //}

                //await this._emailSender.SendAsync(mail);
                //var EmailBody = _emailTemplateRepository.GetAll().Where(e => e.Id == input.EmailTemplateId).Select(e => new { e.Body, e.TemplateName }).FirstOrDefault();
                //await _emailSender.SendAsync(new MailMessage
                //{
                //	From = new MailAddress(assignedToUser.EmailAddress),
                //	To = { lead.Email },
                //	Subject = "Lead Update",//EmailBody.TemplateName + "The Solar Product Lead Notification",
                //	Body = input.Body,
                //	IsBodyHtml = true
                //});
            }
            activity.Body = Regex.Replace(input.Body, "<.*?>", String.Empty);
            activity.CreatorUserId = AbpSession.UserId;
            await _leadactivityRepository.InsertAndGetIdAsync(activity);
            if (input.ActionId == 6)
            {
                //Send SMS
                SendSMSInput sendSMSInput = new SendSMSInput();
                sendSMSInput.PhoneNumber = lead.Mobile;
                sendSMSInput.Text = input.Body;
                sendSMSInput.ActivityId = activity.Id;
                await _applicationSettings.SendSMS(sendSMSInput);
            }

            //GetUserNotificationsInput input1 = new GetUserNotificationsInput();
            //input1.State = null;
            //input1.StartDate = null;
            //input1.EndDate = null;
            //input1.MaxResultCount = 3;
            //input1.SkipCount = 0;
            //await _NotificationAppService.GetUserNotifications(input1);
        }

        public async Task AddActivityLogWithNotification(ActivityLogInput input)
        {
            var lead = await _leadRepository.FirstOrDefaultAsync(input.LeadId);
            var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var duplicateLead = _leadRepository.GetAll().Where(e => (e.Phone == lead.Phone || e.Mobile == lead.Mobile || e.Email == lead.Email) && e.Id != lead.Id && e.AssignToUserID != null).FirstOrDefault();
            var SectionName = _SectionAppService.GetAll().Where(e => e.SectionId == input.SectionId).Select(e => e.SectionName).FirstOrDefault();
            if (duplicateLead != null)
            {

                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == duplicateLead.AssignToUserID).FirstOrDefault();
                var activity = ObjectMapper.Map<LeadActivityLog>(input);
                activity.ActionNote = input.ActionNote;
                activity.Subject = input.Subject;
                activity.SectionId = input.SectionId;
                if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
                {
                    activity.TemplateId = input.EmailTemplateId;
                }
                else if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                {
                    activity.TemplateId = input.SMSTemplateId;
                }
                else
                {
                    activity.TemplateId = 0;
                }
                if (AbpSession.TenantId != null)
                {
                    input.TenantId = (int)AbpSession.TenantId;
                }
                activity.CreatorUserId = AbpSession.UserId;
                await _leadactivityRepository.InsertAndGetIdAsync(activity);
                if (input.ActionId == 25)
                {
                    var assignedToUserForTodo = _userRepository.GetAll().Where(u => u.Id == input.UserId).FirstOrDefault();
                    string msg = input.ActionNote + " " + input.ActivityNote + " For " + lead.CompanyName + " By " + CurrentUser.Name + " From " + SectionName;
                    await _appNotifier.LeadComment(assignedToUserForTodo, msg, NotificationSeverity.Warn);
                }
                if (input.ActionId == 8)
                {
                    string msg = string.Format("Reminder " + input.ActionNote + " " + input.ActivityNote + " For " + duplicateLead.CompanyName + " By " + CurrentUser.Name + " From " + SectionName);
                    await _appNotifier.LeadAssiged(CurrentUser, msg, NotificationSeverity.Info);
                }
                if (input.ActionId == 9)
                {
                    string msg = input.ActionNote + " " + input.ActivityNote  + " For " + duplicateLead.CompanyName + " By " + CurrentUser.Name + " From " + SectionName;
                    await _appNotifier.LeadComment(assignedToUser, msg, NotificationSeverity.Warn);

                    //Email
                    if (!string.IsNullOrEmpty(assignedToUser.EmailAddress))
                    {
                        await _emailSender.SendAsync(new MailMessage
                        {
                            To = { assignedToUser.EmailAddress },
                            Subject = input.Subject,
                            Body = input.Body.ToString(),
                            IsBodyHtml = true
                        });
                    }

                    //Send SMS
                    if (!string.IsNullOrEmpty(assignedToUser.Mobile))
                    {
                        SendSMSInput sendSMSInput = new SendSMSInput();
                        sendSMSInput.PhoneNumber = assignedToUser.Mobile;
                        sendSMSInput.Text = input.Body;
                        sendSMSInput.ActivityId = activity.Id;
                        await _applicationSettings.SendSMS(sendSMSInput);
                    }
                }
                //GetUserNotificationsInput input1 = new GetUserNotificationsInput();
                //input1.State = null;
                //input1.StartDate = null;
                //input1.EndDate = null;
                //input1.MaxResultCount = 3;
                //input1.SkipCount = 0;
                //await _NotificationAppService.GetUserNotifications(input1);

            }
            else
            {
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
                var activity = ObjectMapper.Map<LeadActivityLog>(input);
                activity.ActionNote = input.ActionNote;
                activity.Subject = input.Subject;
                activity.SectionId = input.SectionId;
                if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
                {
                    activity.TemplateId = input.EmailTemplateId;
                }
                else if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                {
                    activity.TemplateId = input.SMSTemplateId;
                }
                else
                {
                    activity.TemplateId = 0;
                }
                if (AbpSession.TenantId != null)
                {
                    input.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAndGetIdAsync(activity);
                if (input.ActionId == 25)
                {
                    var assignedToUserForTodo = _userRepository.GetAll().Where(u => u.Id == input.UserId).FirstOrDefault();
                    string msg = input.ActionNote + " " + input.ActivityNote + " For " + lead.CompanyName + " By " + CurrentUser.Name + " From " + SectionName;
                    await _appNotifier.LeadComment(assignedToUserForTodo, msg, NotificationSeverity.Warn);
                }
                if (input.ActionId == 8)
                {
                    string msg = string.Format("Reminder " + input.ActionNote + " " + input.ActivityNote + " For " + lead.CompanyName + " By " + CurrentUser.Name + " From " + SectionName);
                    await _appNotifier.LeadAssiged(CurrentUser, msg, NotificationSeverity.Info);
                }
                if (input.ActionId == 9)
                {
                    string msg = input.ActionNote + " " + input.ActivityNote + " For " + lead.CompanyName + " By " + CurrentUser.Name + " From " + SectionName;
                    await _appNotifier.LeadComment(assignedToUser, msg, NotificationSeverity.Warn);
                    //Email
                    if (!string.IsNullOrEmpty(assignedToUser.EmailAddress))
                    {
                        await _emailSender.SendAsync(new MailMessage
                        {
                            To = { assignedToUser.EmailAddress },
                            Subject = input.Subject,
                            Body = input.Body.ToString(),
                            IsBodyHtml = true
                        });
                    }


                    //Send SMS
                    if (!string.IsNullOrEmpty(assignedToUser.Mobile))
                    {
                        SendSMSInput sendSMSInput = new SendSMSInput();
                        sendSMSInput.PhoneNumber = assignedToUser.Mobile;
                        sendSMSInput.Text = input.Body;
                        sendSMSInput.ActivityId = activity.Id;
                        await _applicationSettings.SendSMS(sendSMSInput);
                    }
                }

                //GetUserNotificationsInput input1 = new GetUserNotificationsInput();
                //input1.State = null;
                //input1.StartDate = null;
                //input1.EndDate = null;
                //input1.MaxResultCount = 3;
                //input1.SkipCount = 0;

                //await _NotificationAppService.GetUserNotifications(input1);
            }


        }

        public GetLeadCountsDto GetLeadCounts()
        {
            var Result = new GetLeadCountsDto();

            Result.New = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 1).Count());
            Result.UnHandled = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 2).Count());
            Result.Cold = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 3).Count());
            Result.Warm = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 4).Count());
            Result.Hot = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 5).Count());
            Result.Upgrade = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 6).Count());
            Result.Rejected = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 7).Count());
            Result.Cancelled = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 8).Count());
            Result.Closed = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 9).Count());
            Result.Assigned = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 10).Count());
            Result.Total = Convert.ToString(_leadRepository.GetAll().Count());
            return Result;
        }

        public async Task<List<GetLeadForViewDto>> GetUserLeadForDashboard()
        {
            int UserId = (int)AbpSession.UserId;

            var Leads = _leadRepository.GetAll()
                        .Where(e => e.AssignToUserID == UserId);

            var listdata = Leads.MapTo<List<GetLeadForViewDto>>();

            return new List<GetLeadForViewDto>(listdata);
        }

        public virtual async Task ChangeDuplicateStatus(GetLeadForChangeDuplicateStatusOutput input)
        {
            var lead = await _leadRepository.FirstOrDefaultAsync((int)input.Id);
            lead.IsDuplicate = input.IsDuplicate;
            lead.ChangeStatusDate = DateTime.UtcNow;
            lead.IsWebDuplicate = input.IsWebDuplicate;
            await _leadRepository.UpdateAsync(lead);

            LeadActivityLog leadactivity = new LeadActivityLog();
            if (input.IsDuplicate != null)
            {
                leadactivity.ActionId = 5;
                if (input.IsDuplicate == true)
                {
                    leadactivity.ActionNote = "Lead Status Changed To Database Duplicate";
                }
                else
                {
                    leadactivity.ActionNote = "Lead Status Changed To Web Duplicate";
                }
            }
            else
            {
                leadactivity.ActionId = 5;
                if (input.IsDuplicate == true)
                {
                    leadactivity.ActionNote = "Lead Status Changed To Not Database Duplicate";
                }
                else
                {
                    leadactivity.ActionNote = "Lead Status Changed To Not Web Duplicate";
                }
            }
            leadactivity.SectionId = 0;
            leadactivity.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        /// <summary>
        /// Change Duplicate Status for multiple Leads
        /// </summary>
        /// <param name="input">Lead Id[], IsDuplicate - True(Web Dup) - False(DataBase Dup)</param>
        /// <returns></returns>
        public virtual async Task ChangeDuplicateStatusForMultipleLeads(GetLeadForChangeDuplicateStatusOutput input)
        {
            foreach (var item in input.LeadId)
            {
                var lead = await _leadRepository.FirstOrDefaultAsync((int)item);
                lead.IsDuplicate = input.IsDuplicate;
                lead.ChangeStatusDate = DateTime.UtcNow;
                lead.IsWebDuplicate = input.IsWebDuplicate;
                await _leadRepository.UpdateAsync(lead);

                LeadActivityLog leadactivity = new LeadActivityLog();
                if (input.IsDuplicate != null)
                {
                    leadactivity.ActionId = 5;
                    if (input.IsDuplicate == true)
                    {
                        leadactivity.ActionNote = "Lead Status Changed To Database Duplicate";
                    }
                    else
                    {
                        leadactivity.ActionNote = "Lead Status Changed To Web Duplicate";
                    }
                }
                else
                {
                    leadactivity.ActionId = 5;
                    if (input.IsDuplicate == true)
                    {
                        leadactivity.ActionNote = "Lead Status Changed To Not Database Duplicate";
                    }
                    else
                    {
                        leadactivity.ActionNote = "Lead Status Changed To Not Web Duplicate";
                    }
                }
                leadactivity.LeadId = lead.Id;
                leadactivity.SectionId = 0;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
        }

        [UnitOfWork]
        public async Task<List<string>> GetAllUnitType(string unitType)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                return await _lookup_unitTypeRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(unitType), c => c.Name.ToLower().Contains(unitType.ToLower()))
                .Select(unitTypes => unitTypes == null || unitTypes.Name == null ? "" : unitTypes.Name.ToString()).ToListAsync();
            }
        }

        [UnitOfWork]
        public async Task<List<string>> GetAllStreetName(string streetName)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                return await _lookup_streetNameRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(streetName), c => c.Name.ToLower().Contains(streetName.ToLower()))
                .Select(streetNames => streetNames == null || streetNames.Name == null ? "" : streetNames.Name.ToString()).ToListAsync();
            }
        }

        [UnitOfWork]
        public async Task<List<string>> GetAllStreetType(string streetType)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                return await _lookup_streetTypeRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(streetType), c => c.Name.ToLower().Contains(streetType.ToLower()))
                .Select(streetTypes => streetTypes == null || streetTypes.Name == null ? "" : streetTypes.Name.ToString()).ToListAsync();
            }
        }

        [UnitOfWork]
        public async Task<List<string>> GetAllSuburb(string suburb)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var result = _lookup_postCodeRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(suburb), c => c.Suburb.ToLower().Contains(suburb.ToLower()));

                var suburbs = from o in result
                              join o1 in _lookup_stateRepository.GetAll() on o.StateId equals o1.Id into j1
                              from s1 in j1.DefaultIfEmpty()
                              select new LeadStatusLookupTableDto()
                              {
                                  Value = o.Id,
                                  Name = o.Suburb.ToString() + " || " + s1.Name.ToString() + " || " + o.PostalCode
                              };

                return await suburbs
                    .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
            }
        }

        [UnitOfWork]
        public async Task<List<string>> GetOnlySuburb(string suburb)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var result = _lookup_postCodeRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(suburb), c => c.Suburb.ToLower().Contains(suburb.ToLower()));

                var suburbs = from o in result
                              select new LeadStatusLookupTableDto()
                              {
                                  Value = o.Id,
                                  Name = o.Suburb.ToString()
                              };

                return await suburbs
                    .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
            }
        }

        [UnitOfWork]
        public async Task<List<LeadStateLookupTableDto>> GetAllStateForTableDropdown()
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                return await _lookup_stateRepository.GetAll()
                .Select(state => new LeadStateLookupTableDto
                {
                    Id = state.Id,
                    DisplayName = state == null || state.Name == null ? "" : state.Name.ToString().ToUpper()
                }).ToListAsync();
            }
        }

        public async Task<List<LeadSourceLookupTableDto>> GetAllLeadSourceForTableDropdown()
        {
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            if (role.Contains("Admin"))
            {
                return await _lookup_leadSourceRepository.GetAll()
                            .Select(leadSource => new LeadSourceLookupTableDto
                            {
                                Id = leadSource.Id,
                                DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
                            }).ToListAsync();
            }
            else
            {
                return await _lookup_leadSourceRepository.GetAll().Where(e => e.IsActive == true)
                            .Select(leadSource => new LeadSourceLookupTableDto
                            {
                                Id = leadSource.Id,
                                DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
                            }).ToListAsync();
            }

        }

        public async Task<List<LeadSourceLookupTableDto>> GetAllLeadSourceDropdown()
        {
            return await _lookup_leadSourceRepository.GetAll()
                        .Select(leadSource => new LeadSourceLookupTableDto
                        {
                            Id = leadSource.Id,
                            DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
                        }).ToListAsync();
        }

        public async Task<List<JobStatusTableDto>> GetAllJobStatusTableDropdown()
        {
            return await _lookup_jobStatusRepository.GetAll()
            .Select(jobstaus => new JobStatusTableDto
            {
                Id = jobstaus.Id,
                DisplayName = jobstaus == null || jobstaus.Name == null ? "" : jobstaus.Name.ToString()
            }).ToListAsync();
        }

        public List<NameValue<string>> PromotionGetAllLeadStatus(string lead)
        {
            var exceptStatus = new[] { 6, 9, 7, 8, 1 }; //6=Closed, 9=upgrade, 7=Rejected  8=Canceled , 1=New
            var ListOfLeadStatus = _lookup_leadStatusRepository.GetAll()
                            //.Where(c => c.Id != 6 && c.Id != 9 && c.Id != 7 && c.Id != 8 && c.Id != 1)
                            .Where(c => c.Status.Contains(lead))
                            .Select(leadStatus =>
                                new LeadStatusLookupTableDto
                                {
                                    Value = leadStatus.Id,
                                    Name = leadStatus.Status.ToString()
                                });

            //Making NameValue Collection of Multi-Select DropDown

            var leadStatus = new List<NameValue<string>>();
            foreach (var item in ListOfLeadStatus)
                leadStatus.Add(new NameValue { Name = item.Name, Value = item.Value.ToString() });

            return
                lead.IsNullOrEmpty() ?
                leadStatus.ToList() :
                leadStatus.Where(c => c.Name.ToLower().Contains(lead.ToLower())).ToList();
        }

        public List<NameValue<string>> promotionGetAllJobStatus(string jobStatusName)
        {
            var ListOfJobStatus = _lookup_jobStatusRepository.GetAll()
                            .Where(c => c.Name.Contains(jobStatusName))
                            .Select(jobstatus =>
                                new LeadStatusLookupTableDto
                                {
                                    Value = jobstatus.Id,
                                    Name = jobstatus.Name.ToString()
                                });


            //Making NameValue Collection of Multi-Select DropDown

            var jobstatus = new List<NameValue<string>>();
            foreach (var item in ListOfJobStatus)
                jobstatus.Add(new NameValue { Name = item.Name, Value = item.Value.ToString() });


            return
                jobstatus.IsNullOrEmpty() ?
                jobstatus.ToList() :
                jobstatus.Where(c => c.Name.ToLower().Contains(jobStatusName.ToLower())).ToList();
        }

        public List<NameValue<string>> promotionGetAllLeadSource(string leadSourceName)
        {
            var ListOfLeadSource = _lookup_leadSourceRepository.GetAll()
                            .Where(c => c.Name.Contains(leadSourceName))
                            .Select(leadSource =>
                                new LeadStatusLookupTableDto
                                {
                                    Value = leadSource.Id,
                                    Name = leadSource.Name.ToString()
                                });


            //Making NameValue Collection of Multi-Select DropDown

            var leadSource = new List<NameValue<string>>();
            foreach (var item in ListOfLeadSource)
                leadSource.Add(new NameValue { Name = item.Name, Value = item.Value.ToString() });


            return
                leadSource.IsNullOrEmpty() ?
                leadSource.ToList() :
                leadSource.Where(c => c.Name.ToLower().Contains(leadSourceName.ToLower())).ToList();
        }

        public List<NameValue<string>> promotionGetAllState(string stateName)
        {
            var ListOfState = _lookup_stateRepository.GetAll()
                            .Where(c => c.Name.Contains(stateName))
                            .Select(obj =>
                                new LeadStatusLookupTableDto
                                {
                                    Value = obj.Id,
                                    Name = obj.Name.ToString()
                                });


            //Making NameValue Collection of Multi-Select DropDown

            var States = new List<NameValue<string>>();
            foreach (var item in ListOfState)
                States.Add(new NameValue { Name = item.Name, Value = item.Value.ToString() });


            return
                States.IsNullOrEmpty() ?
                States.ToList() :
                States.Where(c => c.Name.ToLower().Contains(stateName.ToLower())).ToList();
        }

        private int?[] getValuesFromNameValue(List<NameValue<int>> nameValues)
        {

            int?[] arr = new int?[nameValues.Count];


            if (nameValues != null)
            {
                int i = 0;

                foreach (var e in nameValues)
                {
                    arr[i] = e.Value;
                    i++;
                }
            }

            return arr;
        }

        public async Task<PagedResultDto<GetLeadForViewDto>> GetTotalLeadsCountForPromotion(GetAllLeadsInputForPromotion input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDateFilter, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDateFilter, (int)AbpSession.TenantId));

            if (input.TeamIdsFilter != null) Debug.Print(input.TeamIdsFilter != null ? input.TeamIdsFilter.Count().ToString() : "");
            if (input.LeadStatusIdsFilter != null) Debug.Print(input.LeadStatusIdsFilter != null ? input.LeadStatusIdsFilter.Count().ToString() : "");

            //var LeadStatusId = input.LeadStatusIdsFilter.Cast<int>().Select(e => input.LeadStatusIdsFilter[e].Value);


            //Making Int
            var arrLeadStatusIds = input.LeadStatusIdsFilter != null ? getValuesFromNameValue(input.LeadStatusIdsFilter) : null;
            var arrLeadSourceIds = input.LeadSourceIdsFilter != null ? getValuesFromNameValue(input.LeadSourceIdsFilter) : null;
            var arrJobStatusIds = input.JobStatusIdsFilter != null ? getValuesFromNameValue(input.JobStatusIdsFilter) : null;
            var arrTeamIds = input.TeamIdsFilter != null ? getValuesFromNameValue(input.TeamIdsFilter) : null;
            var arrStateIds = input.StateIdsFilter != null ? getValuesFromNameValue(input.StateIdsFilter) : null;

            int UserId = (int)AbpSession.UserId;

            var arrleads = new List<int?>();
            if (arrJobStatusIds != null && arrJobStatusIds.Count() > 0)
            {
                arrleads = _jobRepository.GetAll().Where(e => arrJobStatusIds.Contains(e.JobStatusId)).Select(e => e.LeadId).Distinct().ToList();
            }


            //////customer stop for promotion
            var stopresponceList = _PromotionUsersRepository.GetAll().Where(e => e.PromotionResponseStatusId == 2).Select(e => e.LeadId).ToList();

            /////customer not made payment
            //var Paidjobs = _invoicePaymentRepository.GetAll().Select(e => e.JobId).Distinct().ToList();
            //var paidleads = _jobRepository.GetAll().Where(e => Paidjobs.Contains(e.Id)).Select(e => e.LeadId).Distinct().ToList();

            //9=Closed
            var filteredLeads = _leadRepository.GetAll()
                .Where(e => e.IsPromotion)
                .Where(e => e.LeadStatusId != 9 && !stopresponceList.Contains(e.Id)) //&& !paidleads.Contains(e.Id))
                .WhereIf(arrLeadStatusIds != null && arrLeadStatusIds.Count() > 0, e => arrLeadStatusIds.Contains(e.LeadStatusId)) // LeadStatusID
                .WhereIf(arrLeadSourceIds != null && arrLeadSourceIds.Count() > 0, e => arrLeadSourceIds.Contains(e.LeadSourceId)) // LeadSourceID
                .WhereIf(arrleads != null && arrleads.Count() > 0, e => arrleads.Contains(e.Id)) /// JobStatusID
			.WhereIf(arrStateIds != null && arrStateIds.Count() > 0, e => arrStateIds.Contains(e.StateId))
                .WhereIf(input.StartDateFilter != null, e => e.CreationTime >= SDate.Value.Date)
                .WhereIf(input.EndDateFilter != null, e => e.CreationTime <= EDate.Value.Date)
                .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter);

            var pagedAndFilteredLeads = filteredLeads;

            if (arrTeamIds != null && arrTeamIds.Length > 0)
            {

                var leads = from o in filteredLeads
                            join ut in _userTeamRepository.GetAll() on o.AssignToUserID equals ut.UserId into j1
                            from ut1 in j1.DefaultIfEmpty()
                            where (arrTeamIds == null || arrTeamIds.Contains(ut1.TeamId)) && ut1.IsDeleted == false
                            //where (input.TeamIdFilter == null || ut1.TeamId == input.TeamIdFilter)  
                            select new GetLeadForViewDto
                            {
                                Lead = new LeadDto { Id = o.Id }
                            };

                var totalCount = await filteredLeads.CountAsync();

                return new PagedResultDto<GetLeadForViewDto>(
                    totalCount,
                    leads.ToList()
                );
            }
            else
            {


                var leads = from o in filteredLeads
                            select new GetLeadForViewDto
                            {
                                Lead = new LeadDto { Id = o.Id }
                            };
                var totalCount = await filteredLeads.CountAsync();

                return new PagedResultDto<GetLeadForViewDto>(
                    totalCount,
                    leads.ToList()
                );
            }
        }

        public async Task<List<LeadUsersLookupTableDto>> GetAllTeamsForFilter()
        {
            return await _teamRepository.GetAll()
            .Select(team => new LeadUsersLookupTableDto
            {
                Id = team.Id,
                DisplayName = team == null || team.Name == null ? "" : team.Name.ToString()
            }).ToListAsync();
        }

        public async Task<List<LeadUsersLookupTableDto>> GetSalesManagerForFilter(int OId, int? TeamId)
        {
            var User = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var Team = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            var User_List = _userRepository
           .GetAll();

            var UserList = new List<LeadUsersLookupTableDto>();
            if (role.Contains("Admin"))
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Sales Manager")
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).ToList();
            }
            else
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Sales Manager" && uo != null && uo.OrganizationUnitId == OId && ut.TeamId == Team) // 
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).ToList();
            }


            return UserList;
        }

        public async Task<List<LeadUsersLookupTableDto>> GetSalesRepForFilter(int OId, int? TeamId)
        {
            var User = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var Team = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            var User_List = _userRepository
           .GetAll();

            var UserList = new List<LeadUsersLookupTableDto>();
            if (role.Contains("Admin"))
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Sales Rep") //&& ut.TeamId == TeamId
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).OrderBy(d => d.DisplayName).ToList();
            }
            else
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Sales Rep" && uo != null && uo.OrganizationUnitId == OId && ut.TeamId == Team) // 
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).OrderBy(d => d.DisplayName).ToList();
            }


            return UserList;
        }

        public async Task CopyLead(int OId, int LeadId)
        {
            var OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == OId).Select(e => e.DisplayName).FirstOrDefault();
            var lead = _leadRepository.GetAll().Where(e => e.Id == LeadId).FirstOrDefault();
            lead.Id = 0;
            //1. Create
            //2. Excel Import
            //3. External Link
            //4. Copy Lead
            lead.IsExternalLead = 4;
            lead.OrganizationId = OId;
            lead.CreatorUserId = (int)AbpSession.UserId;
            await _leadRepository.InsertAndGetIdAsync(lead);

            var activity = new LeadActivityLog();
            activity.LeadId = lead.Id;
            activity.ActionId = 1;
            if (AbpSession.TenantId != null)
            {
                activity.TenantId = (int)AbpSession.TenantId;
            }
            activity.ActionNote = "Lead Copy From " + OrganizationName + " Organization";
            await _leadactivityRepository.InsertAsync(activity);

            var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == lead.CreatorUserId).FirstOrDefault();
            string msg = string.Format("New Lead Created {0}.", lead.CompanyName);
            await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
        }

        public bool CheckCopyExist(int OId, int LeadId)
        {
            var Result = false;

            var lead = _leadRepository.GetAll().Where(e => e.Id == LeadId).FirstOrDefault();
            Result = _leadRepository.GetAll().Where(e => e.IsExternalLead == 4 && e.OrganizationId == OId && e.Id != lead.Id && e.Email == lead.Email && e.Phone == lead.Phone && e.Mobile == lead.Mobile).Any();

            return Result;
        }

        public async Task<List<CommonLookupDto>> GetallEmailTemplates(int leadId)
        {
            var organizationId = _leadRepository.GetAll().Where(e => e.Id == leadId).Select(e => e.OrganizationId).FirstOrDefault();
            return await _emailTemplateRepository.GetAll().Where(e => e.OrganizationUnitId == organizationId)
            .Select(emailTemplate => new CommonLookupDto
            {
                Id = emailTemplate.Id,
                DisplayName = emailTemplate == null || emailTemplate.TemplateName == null ? "" : emailTemplate.TemplateName.ToString()
            }).ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetallSMSTemplates(int leadId)
        {
            var organizationId = _leadRepository.GetAll().Where(e => e.Id == leadId).Select(e => e.OrganizationId).FirstOrDefault();
            return await _smsTemplateRepository.GetAll().Where(e => e.OrganizationUnitId == organizationId)
            .Select(smsTemplate => new CommonLookupDto
            {
                Id = smsTemplate.Id,
                DisplayName = smsTemplate == null || smsTemplate.Name == null ? "" : smsTemplate.Name.ToString()
            }).ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetAllLeadAction()
        {
            return await _lookup_leadActionRepository.GetAll()
            .Select(leadAction => new CommonLookupDto
            {
                Id = leadAction.Id,
                DisplayName = leadAction.ActionName
            }).ToListAsync();

            //var TeamList = (from user in _lookup_leadActionRepository.GetAll()
            //                select new LeadUsersLookupTableDto()
            //                {
            //                    Id = user.Id,
            //                    DisplayName = user.ActionName == null || user.ActionName == null ? "" : user.ActionName.ToString()
            //                }).DistinctBy(d => d.Id).ToList();





        }

        public async Task<string> GetCurrentUserRole()
        {
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            return role[0];
        }
        public async Task<string> GetCurrentUserIdName()
        {
            return _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).Select(e => e.UserName).FirstOrDefault();
        }
        public async Task<int> GetCurrentUserId()
        {
            return (int)AbpSession.UserId;
        }

        public async Task<bool> CheckValidation(CreateOrEditLeadDto input)
        {
            var output = true;

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            if (!role.Contains("Admin"))
            {
                output = (!string.IsNullOrEmpty(input.CompanyName) && !string.IsNullOrEmpty(input.Area) && !string.IsNullOrEmpty(input.Type)
                    && !string.IsNullOrEmpty(input.StreetNo) && !string.IsNullOrEmpty(input.StreetName) && !string.IsNullOrEmpty(input.StreetType)
                    && !string.IsNullOrEmpty(input.Suburb) && !string.IsNullOrEmpty(input.State) && !string.IsNullOrEmpty(input.PostCode)
                    && !string.IsNullOrEmpty(input.LeadSource) && (!string.IsNullOrEmpty(input.Email) || !string.IsNullOrEmpty(input.Mobile)));
            }
            else
            {
                output = (!string.IsNullOrEmpty(input.CompanyName) && !string.IsNullOrEmpty(input.LeadSource) && (!string.IsNullOrEmpty(input.Email) || !string.IsNullOrEmpty(input.Mobile)));
            }

            return output;
        }

        /// SalesREpBySalesManagerId
        public async Task<List<LeadUsersLookupTableDto>> GetSalesRepBySalesManagerid(int? id, int? OId)
        {
            var User = _userRepository.GetAll().Where(u => u.Id == id).FirstOrDefault();
            var Team = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).FirstOrDefault();
            var User_List = _userRepository
           .GetAll();
            var UserList = new List<LeadUsersLookupTableDto>();
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Sales Rep" && uo != null && uo.OrganizationUnitId == OId && ut.TeamId == Team) // 
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).OrderBy(d => d.DisplayName).ToList();
            }

            return UserList;
        }

        /// login wise team display
        public async Task<List<LeadUsersLookupTableDto>> GetTeamForFilter()
        {
            var User = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var Team = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            var TeamList = new List<LeadUsersLookupTableDto>();
            if (role.Contains("Admin"))
            {
                TeamList = (from user in _teamRepository.GetAll()
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.Name == null || user.Name == null ? "" : user.Name.ToString()
                            }).DistinctBy(d => d.Id).ToList();
            }
            else
            {
                TeamList = (from team in _teamRepository.GetAll()
                            where (team.Id == Team)
                            select new LeadUsersLookupTableDto()
                            {
                                Id = team.Id,
                                DisplayName = team.Name == null || team.Name == null ? "" : team.Name.ToString()
                            }).DistinctBy(d => d.Id).ToList();
            }
            return TeamList;
        }


        /// SMS Reply Grid list
        public async Task<PagedResultDto<SmsReplyDto>> GetSmsReplyList(SmsReplyInputDto input)
        {
            int UserId = (int)AbpSession.UserId;
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == UserId).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var User_List = _userRepository
       .GetAll();
            var RoleName = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            where (us != null && user.Id == UserId)
                            select (us.DisplayName));


            var leadids = new List<int>();
            if (RoleName.Contains("Sales Manager"))
            {
                leadids = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationUnit && UserList.Contains(e.AssignToUserID)).Select(e => e.Id).ToList();
            }
            else if (RoleName.Contains("Sales Rep"))
            {
                leadids = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationUnit && e.AssignToUserID == UserId).Select(e => e.Id).ToList();
            }
            else
            {

                leadids = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationUnit).Select(e => e.Id).ToList();
            }



            var ids = _leadactivityRepository.GetAll().Where(e => e.ActionId == 6).Select(e => e.Id).ToList();




            var filteredLeads = _leadactivityRepository.GetAll()
                 .Include(e => e.LeadFk)
                 .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter))
                 .WhereIf(input.Markstatusid != 0 && input.Markstatusid == 1, e => ids.Contains((int)e.ReferanceId) && e.IsMark == true)
                 .WhereIf(input.Markstatusid != 0 && input.Markstatusid == 2, e => ids.Contains((int)e.ReferanceId) && e.IsMark == false)
                 // .WhereIf(input.DateNameFilter == "Creation", e => e.CreationTime >= input.StartDate && e.DistApplied <= input.EndDate)
                 //.WhereIf(input.DateNameFilter == "Receive", e => e.DistExpiryDate >= input.StartDate && e.DistExpiryDate <= input.EndDate)
                 .WhereIf(input.OrganizationUnit != null, e => leadids.Contains(e.LeadId))
                 .Where(e => ids.Contains((int)e.ReferanceId));

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var result = from o in pagedAndFilteredLeads
                         join o1 in _leadRepository.GetAll() on o.LeadId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _leadactivityRepository.GetAll() on o.ReferanceId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         select new SmsReplyDto
                         {
                             LeadCompanyName = s1.CompanyName,
                             ResponceActionNote = o.ActionNote,
                             ResponceActionId = o.ActionId,
                             ResponceActivityDate = o.ActivityDate,
                             ResponceActivityNote = o.ActivityNote,
                             ResponceCreationTime = o.CreationTime,
                             ResponceBody = o.Body,
                             ActionNote = s2.ActionNote,
                             ActionId = s2.ActionId,
                             ActivityDate = s2.ActivityDate,
                             ActivityNote = s2.ActivityNote,
                             CreationTime = s2.CreationTime,
                             Body = s2.Body,
                             IsMark = o.IsMark,
                             id = o.Id,
                             UserName = s3.Name

                         };


            var totalCount = filteredLeads.Count();

            return new PagedResultDto<SmsReplyDto>(
                totalCount, await result.ToListAsync()
            );
        }

        /// Email Reply Grid list
        public async Task<PagedResultDto<EmailReplyDto>> GetEmailReplyList(EmailReplyInputDto input)
        {
            int UserId = (int)AbpSession.UserId;
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == UserId).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var User_List = _userRepository
       .GetAll();
            var RoleName = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            where (us != null && user.Id == UserId)
                            select (us.DisplayName));


            var leadids = new List<int>();
            if (RoleName.Contains("Sales Manager"))
            {
                leadids = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationUnit && UserList.Contains(e.AssignToUserID)).Select(e => e.Id).ToList();
            }
            else if (RoleName.Contains("Sales Rep"))
            {
                leadids = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationUnit && e.AssignToUserID == UserId).Select(e => e.Id).ToList();
            }
            else
            {

                leadids = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationUnit).Select(e => e.Id).ToList();
            }

            var filteredLeads = _leadactivityRepository.GetAll()
                 .Include(e => e.LeadFk)
                 .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter))
                                .WhereIf(input.OrganizationUnit != null, e => leadids.Contains(e.LeadId))
                                .Where(e => e.ActionId == 7);

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var result = from o in pagedAndFilteredLeads
                         join o1 in _leadRepository.GetAll() on o.LeadId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _emailTemplateRepository.GetAll() on o.TemplateId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         select new EmailReplyDto
                         {
                             LeadCompanyName = s1.CompanyName,
                             ActionNote = o.ActionNote,
                             ActionId = o.ActionId,
                             ActivityDate = o.ActivityDate,
                             ActivityNote = o.ActivityNote,
                             CreationTime = o.CreationTime,
                             Body = o.Body,
                             Id = o.Id,
                             TemplateName = s2.TemplateName == null ? "Custome Template" : s2.TemplateName,
                             UserName = s3.Name,
                             Subject = o.Subject
                         };


            var totalCount = filteredLeads.Count();

            return new PagedResultDto<EmailReplyDto>(
                totalCount, await result.ToListAsync()
            );
        }

        /// mark same as read api 
        /// id use for activity id.. 0 means not read 1 means mark as read
        public async Task MarkReadSms(int id)
        {

            var leadactivity = await _leadactivityRepository.FirstOrDefaultAsync((int)id);
            leadactivity.IsMark = true;
            await _leadactivityRepository.UpdateAsync(leadactivity);

        }

        /// get leadactivitystatus by id
        /// id use for activity id.. 

        public async Task<SmsReplyDto> GetLeadActivitybyID(int id)
        {
            var output = new SmsReplyDto();

            var ids = _leadactivityRepository.GetAll().Where(L => L.Id == id).Select(e => e.ReferanceId).FirstOrDefault();
            output.ResponceBody = _leadactivityRepository.GetAll().Where(L => L.Id == id).Select(e => e.Body).FirstOrDefault();
            output.Body = _leadactivityRepository.GetAll().Where(e => e.Id == ids).Select(e => e.Body).FirstOrDefault();
            output.TemplateId = _leadactivityRepository.GetAll().Where(e => e.Id == id).Select(e => e.TemplateId).FirstOrDefault();
            return output;
        }

        //public async Task<List<GetActivityLogViewDto>> GetLeadActivitybyID(int id)
        //{
        //	return await _leadactivityRepository.GetAll().Where(L => L.Id == id)
        //		.Select(users => new GetActivityLogViewDto
        //		{
        //			ActionId = users.ActionId,
        //			ActionNote = users.ActionNote,
        //			LeadId = users.LeadId,
        //			CreationTime = users.CreationTime,
        //			Body = users.Body
        //		}).ToListAsync();
        //}

        /// <summary>
        /// Get UnRead SMS Count
        /// </summary>
        /// <returns></returns>
        public async Task<int> GetUnReadSMSCount()
        {
            var result = 0;

            int UserId = (int)AbpSession.UserId;
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == UserId).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var User_List = _userRepository
          .GetAll();
            var RoleName = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            where (us != null && user.Id == UserId)
                            select (us.DisplayName));

            var leadids = new List<int>();
            if (RoleName.Contains("Sales Manager"))
            {
                leadids = _leadRepository.GetAll().Where(e => UserList.Contains(e.AssignToUserID)).Select(e => e.Id).ToList();
            }
            else if (RoleName.Contains("Sales Rep"))
            {
                leadids = _leadRepository.GetAll().Where(e => e.AssignToUserID == UserId).Select(e => e.Id).ToList();
            }
            else
            {
                leadids = _leadRepository.GetAll().Select(e => e.Id).ToList();
            }

            var ids = _leadactivityRepository.GetAll().Where(e => e.ActionId == 6).Select(e => e.Id).ToList();

            result = _leadactivityRepository.GetAll()
                                .Where(e => leadids.Contains(e.LeadId))
                                .Where(e => e.IsMark == false && e.ActionId == 20).Count();

            return result;
        }

        public async Task<int> GetUnReadPromotionCount()
        {
            var result = 0;

            int UserId = (int)AbpSession.UserId;
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == UserId).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var User_List = _userRepository
          .GetAll();
            var RoleName = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            where (us != null && user.Id == UserId)
                            select (us.DisplayName));

            var leadids = new List<int>();
            if (RoleName.Contains("Sales Manager"))
            {
                leadids = _leadRepository.GetAll().Where(e => UserList.Contains(e.AssignToUserID)).Select(e => e.Id).ToList();
            }
            else if (RoleName.Contains("Sales Rep"))
            {
                leadids = _leadRepository.GetAll().Where(e => e.AssignToUserID == UserId).Select(e => e.Id).ToList();
            }
            else
            {
                leadids = _leadRepository.GetAll().Select(e => e.Id).ToList();
            }

            var ids = _leadactivityRepository.GetAll().Where(e => e.ActionId == 21).Select(e => e.Id).ToList();

            result = _leadactivityRepository.GetAll()
                                .Where(e => leadids.Contains(e.LeadId))
                                .Where(e => e.IsMark == false && e.ActionId == 22).Count();

            return result;
        }

        public async Task<PagedResultDto<GetLeadForViewDto>> GetMyReminderData(GetAllLeadsInput input)
        {
            int UserId = 9; // (int)AbpSession.UserId;

            var List = _leadactivityRepository.GetAll().Where(e => e.ActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).ToList();
            var NextFollowupList = List.Where(e => e.ActivityDate.Value.AddHours(10).Date >= input.StartDate.Value.Date && e.ActivityDate.Value.AddHours(10).Date <= input.EndDate.Value.Date).Select(e => e.LeadId);

            var filteredLeads = _leadRepository.GetAll()
                       .WhereIf(input.DateFilterType == "NextFollowup", e => NextFollowupList.Contains(e.Id))
                       .Where(e => e.AssignToUserID == UserId);

            var leads = (from o in filteredLeads
                         select new GetLeadForViewDto()
                         {
                             Lead = new LeadDto
                             {
                                 CompanyName = o.CompanyName,
                             },
                             LastComment = _leadactivityRepository.GetAll().Where(e => e.LeadId == o.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                             LastCommentDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == o.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),

                         });

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetLeadForViewDto>(
                totalCount,
                leads.DistinctBy(e => e.Lead.Id).ToList()
            );
        }


        public string GetareaBysuburbPostandstate(string suburb, string state, string postCode)
        {
            int stateid = _lookup_stateRepository.GetAll().Where(e => e.Name == state).Select(e => e.Id).FirstOrDefault();
            var Result = _postCodeRepository.GetAll().Where(e => e.Suburb == suburb.ToUpper() && e.StateId == stateid && e.PostalCode == postCode.ToUpper()).Select(e => e.Areas).FirstOrDefault();

            return Result;
        }

        /// ReAssign Cancel or Reject Lead api 
        /// id use for Leadid
        public async Task ReAssignCancelOrRejectLead(int leadid)
        {

            var lead = await _leadRepository.FirstOrDefaultAsync((int)leadid);
            PreviousJobStatus LeadStstus = new PreviousJobStatus();
            LeadStstus.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                LeadStstus.TenantId = (int)AbpSession.TenantId;
            }
            LeadStstus.CurrentID = 11;
            LeadStstus.PreviousId = lead.LeadStatusId;
            await _previousJobStatusRepository.InsertAsync(LeadStstus);
            lead.LeadStatusId = 11;
            await _leadRepository.UpdateAsync(lead);

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 11;
            leadactivity.SectionId = 0;
            leadactivity.ActionNote = "Lead Reassign";
            leadactivity.LeadId = (int)leadid;
            leadactivity.ActivityDate = DateTime.UtcNow;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);

        }
        public async Task<List<LeadUsersLookupTableDto>> GetUserByTeamId(int? teamid)
        {
            if (teamid != 0)
            {
                var UserList = new List<LeadUsersLookupTableDto>();
                {
                    UserList = (from user in _userRepository.GetAll()
                                join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                                from ut in utJoined.DefaultIfEmpty()
                                where (ut.TeamId == teamid)
                                select new LeadUsersLookupTableDto()
                                {
                                    Id = user.Id,
                                    DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                                }).DistinctBy(d => d.Id).ToList();
                }
                return UserList;
            }
            else
            {
                var UserList = new List<LeadUsersLookupTableDto>();
                {
                    UserList = (from user in _userRepository.GetAll()
                                select new LeadUsersLookupTableDto()
                                {
                                    Id = user.Id,
                                    DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                                }).DistinctBy(d => d.Id).ToList();
                }
                return UserList;
            }
        }

        public async Task UpdateIsMark()
        {
            var data = _leadactivityRepository.GetAll().Where(e => e.ActionId == 22 && e.IsMark != true).Select(e => e.Id).ToList().Take(10);

            foreach (var item in data)
            {
                var listdata = _leadactivityRepository.GetAll().Where(e => e.Id == item).FirstOrDefault();
                listdata.IsMark = true;
                await _leadactivityRepository.UpdateAsync(listdata);
            }
        }

        public async Task UpdateLeadActivityDetailForTodo(CreateEditActivityLogDto input)
        {
            var lead = await _leadactivityRepository.FirstOrDefaultAsync((int)input.Id);
            var email = _userRepository.GetAll().Where(e => e.Id == lead.CreatorUserId).Select(e => e.EmailAddress).FirstOrDefault();
            var user = _userRepository.GetAll().Where(e => e.Id == lead.CreatorUserId).FirstOrDefault();
            var logginuser = _userRepository.GetAll().Where(u => u.Id == lead.ReferanceId).Select(e => e.UserName).FirstOrDefault();
            MailMessage mail = new MailMessage
            {
                From = new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                To = { email },
                Subject = "Task Responce",
                Body = input.Body,
                IsBodyHtml = true
            };
            await this._emailSender.SendAsync(mail);
            string msg = string.Format("Task Responce" + input.Body + "by" + logginuser);
            await _appNotifier.LeadAssiged(user, msg, NotificationSeverity.Info);
            lead.Body = input.Body;
            lead.IsMark = true;
            await _leadactivityRepository.UpdateAsync(lead);
        }
    }
}