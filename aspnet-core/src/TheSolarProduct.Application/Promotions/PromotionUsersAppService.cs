﻿using TheSolarProduct.Leads;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheSolarProduct.Authorization;
using TheSolarProduct.Authorization.Users;
using Abp.Domain.Repositories;
using TheSolarProduct.Promotions.Exporting;
using TheSolarProduct.Promotions.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization.Users;
using TheSolarProduct.LeadStatuses;
using JetBrains.Annotations;
using TheSolarProduct.Jobs;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.ApplicationSettings;
using System;
using Abp.Timing.Timezone;
using TheSolarProduct.Invoices;

namespace TheSolarProduct.Promotions
{
	[AbpAuthorize(AppPermissions.Pages_PromotionUsers)]
	public class PromotionUsersAppService : TheSolarProductAppServiceBase, IPromotionUsersAppService
	{
		private readonly IRepository<PromotionUser> _promotionUserRepository;
		private readonly IPromotionUsersExcelExporter _promotionUsersExcelExporter;
		private readonly IRepository<Promotion, int> _lookup_promotionRepository;
		private readonly IRepository<Lead, int> _lookup_leadRepository;
		private readonly IRepository<LeadStatus, int> _leadStatusRepository;
		private readonly IRepository<PromotionResponseStatus, int> _lookup_promotionResponseStatusRepository;
		private readonly IRepository<PromotionType, int> _lookup_promotionTypeRepository;

		private readonly IRepository<User, long> _userRepository;
		private readonly IRepository<Job> _jobRepository;
		private readonly IRepository<UserTeam> _userTeamRepository;
		private readonly IRepository<JobStatus, int> _lookup_jobStatusRepository;
		private readonly IRepository<LeadActivityLog> _leadactivityRepository;
		private readonly IRepository<Team, int> _teamRepository;
		private readonly UserManager _userManager;
		private readonly IApplicationSettingsAppService _applicationSettings;
		private readonly ITimeZoneConverter _timeZoneConverter;
		private readonly IRepository<InvoicePayment> _invoicePaymentRepository;

		public PromotionUsersAppService(
			  IRepository<PromotionUser> promotionUserRepository,
			  IPromotionUsersExcelExporter promotionUsersExcelExporter,
			  IRepository<Promotion, int> lookup_promotionRepository,
			  IRepository<Lead, int> lookup_leadRepository,
			  IRepository<PromotionResponseStatus, int> lookup_promotionResponseStatusRepository,
			  IRepository<User, long> userRepository,
			  IRepository<LeadStatus, int> leadStatusRepository,
			  IRepository<PromotionType, int> lookup_promotionTypeRepository,
			  IRepository<Job> jobRepository,
			  IRepository<UserTeam> userTeamRepository,
			  IRepository<JobStatus, int> lookup_jobStatusRepository,
			  IRepository<LeadActivityLog> leadactivityRepository,
			  IRepository<Team, int> teamRepository,
			  UserManager userManager,
			  IApplicationSettingsAppService applicationSettings,
			  ITimeZoneConverter timeZoneConverter,
			  IRepository<InvoicePayment> invoicePaymentRepository

				)
		{
			_promotionUserRepository = promotionUserRepository;
			_promotionUsersExcelExporter = promotionUsersExcelExporter;
			_lookup_promotionRepository = lookup_promotionRepository;
			_lookup_leadRepository = lookup_leadRepository;
			_lookup_promotionResponseStatusRepository = lookup_promotionResponseStatusRepository;
			_userRepository = userRepository;
			_leadStatusRepository = leadStatusRepository;
			_lookup_promotionTypeRepository = lookup_promotionTypeRepository;
			_jobRepository = jobRepository;
			_userTeamRepository = userTeamRepository;
			_lookup_jobStatusRepository = lookup_jobStatusRepository;
			_leadactivityRepository = leadactivityRepository;
			_teamRepository = teamRepository;
			_userManager = userManager;
			_applicationSettings = applicationSettings;
			_timeZoneConverter = timeZoneConverter;
			_invoicePaymentRepository = invoicePaymentRepository;
		}

		public async Task<PagedResultDto<GetPromotionUserForViewDto>> GetAll(GetAllPromotionUsersInput input)
		{
			var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
			var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

			var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
			IList<string> role = await _userManager.GetRolesAsync(User);
			var TeamIds = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
			var UserList = _userTeamRepository.GetAll().Where(e => TeamIds.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

			var jobnumberlist = new List<int?>();
			jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

			var jobstatuss = new List<int?>();
			if (input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0)
			{
				jobstatuss = _jobRepository.GetAll().Where(e => input.JobStatusIDFilter.Contains((int)e.JobStatusId)).Select(e => e.LeadId).ToList();
			}

			var UserLists = new List<long?>();
			if (input.TeamId != 0 && input.TeamId != null)
			{
				UserLists = _userTeamRepository.GetAll().Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
			}


			
			var readfilterlist = new List<int?>();
			if (input.ReadUnreadsms == 1)
			{
				var readLedActivityids = _leadactivityRepository.GetAll().Where(e => e.ActionId == 22 && e.IsMark == true).Select(e => e.ReferanceId).Distinct().ToList();
				readfilterlist = _leadactivityRepository.GetAll().Where(e => readLedActivityids.Contains(e.Id)).Select(e => e.PromotionUserId).Distinct().ToList();
			}
			var unreadfilterlist = new List<int?>();
			if (input.ReadUnreadsms == 2)
			{
				var unreadLedActivityids = _leadactivityRepository.GetAll().Where(e => e.ActionId == 22 && e.IsMark == false).Select(e => e.ReferanceId).Distinct().ToList();
				unreadfilterlist = _leadactivityRepository.GetAll().Where(e => unreadLedActivityids.Contains(e.Id)).Select(e => e.PromotionUserId).Distinct().ToList();
			}
			var filteredPromotionUsers = _promotionUserRepository.GetAll()
						.Include(e => e.PromotionFk)
						.Include(e => e.LeadFk)
						.Include(e => e.PromotionResponseStatusFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.ResponseMessage.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter) || jobnumberlist.Contains(e.LeadId))
						.WhereIf(input.ResponseDateFilter == "Responce" && SDate != null && EDate != null, e => e.ResponseDate.AddHours(10).Date >= SDate.Value.Date && e.ResponseDate.AddHours(10).Date <= EDate.Value.Date)
						.WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => jobstatuss.Contains(e.LeadId))
						.WhereIf(input.PromotionTitleFilter != 0, e => e.PromotionId == input.PromotionTitleFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.LeadCopanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCopanyNameFilter)
						.WhereIf(input.PromotionResponseSatusIdFilter != null && input.PromotionResponseSatusIdFilter != 0, e => e.PromotionResponseStatusFk != null && e.PromotionResponseStatusFk.Id == input.PromotionResponseSatusIdFilter)
						.WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
						.WhereIf(input.TeamId != null && input.TeamId != 0, e => UserLists.Contains(e.LeadFk.AssignToUserID))
						.WhereIf(input.OrganizationUnit != null, e => e.PromotionFk.OrganizationID == input.OrganizationUnit)
						.WhereIf(input.PromoType != 0, e => e.PromotionFk.PromotionTypeId == input.PromoType)
						.WhereIf(input.ReadUnreadsms == 1, e => readfilterlist.Contains(e.Id))
						.WhereIf(input.ReadUnreadsms == 2, e => unreadfilterlist.Contains(e.Id))
						.WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
						.WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
						.WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
						.Where(e => e.PromotionResponseStatusId != null);

			var pagedAndFilteredPromotionUsers = filteredPromotionUsers
				.OrderBy(input.Sorting ?? "responseDate desc")
				.PageBy(input);

			var JobStatus = _jobRepository.GetAll().Where(e => e.JobStatusId == 1).Select(e => e.LeadId).ToList();
			var deposite = _invoicePaymentRepository.GetAll().Where(e => e.InvoicePaymentStatusId == 2).Select(e => e.JobFk.LeadId).Distinct().ToList();
			var promotionUsers = from o in pagedAndFilteredPromotionUsers
								 join o1 in _lookup_promotionRepository.GetAll() on o.PromotionId equals o1.Id into j1
								 from s1 in j1.DefaultIfEmpty()

								 join o2 in _lookup_leadRepository.GetAll() on o.LeadId equals o2.Id into j2
								 from s2 in j2.DefaultIfEmpty()


								 join o3 in _lookup_promotionResponseStatusRepository.GetAll() on o.PromotionResponseStatusId equals o3.Id into j3
								 from s3 in j3.DefaultIfEmpty()

								 join o4 in _userRepository.GetAll() on s2.AssignToUserID.ToString() equals o4.Id.ToString() into j4
								 from s4 in j4.DefaultIfEmpty()


								 join o5 in _leadStatusRepository.GetAll() on s2.LeadStatusId.ToString() equals o5.Id.ToString()

								 join o6 in _jobRepository.GetAll() on o.LeadId equals o6.LeadId into j6
								 from s6 in j6.DefaultIfEmpty()

								 join o7 in _lookup_jobStatusRepository.GetAll() on s6.JobStatusId equals o7.Id into j7
								 from s7 in j7.DefaultIfEmpty()


								 join o8 in _leadactivityRepository.GetAll() on s7.Id equals o8.PromotionUserId into j8
								 from s8 in j8.DefaultIfEmpty()
									 //join o6 in _lookup_promotionTypeRepository.GetAll() on o.PromotionTypeId equals o6.Id into j5
									 //from s6 in j1.DefaultIfEmpty()

								 select new GetPromotionUserForViewDto()
								 {
									 PromotionUser = new PromotionUserDto
									 {
										 ResponseDate = o.ResponseDate,
										 PromotionResponseStatusId = o.PromotionResponseStatusId,
										 ResponseMessage = o.ResponseMessage,
										 Id = o.Id

									 },

									 getPromotionForViewDto = new GetPromotionForViewDto
									 {
										 Promotion = new PromotionDto
										 {
											 Id = s1.Id,
											 Title = s1.Title,
											 PromoCharge = s1.PromoCharge,
											 PromotionTypeId = s1.PromotionTypeId,
											 Description = s1.Description,
											 CreationTime = s1.CreationTime,
										 },
										 PromotionTypeName = s1.PromotionTypeFk.Name

									 },

									 PromotionTitle = s1 == null || s1.Title == null ? "" : s1.Title.ToString(),
									 LeadCopanyName = s2 == null || s2.CompanyName == null ? "" : s2.CompanyName.ToString(),
									 Mobile = s2 == null || s2.Mobile == null ? "" : s2.Mobile.ToString(),
									 EMail = s2 == null || s2.Email == null ? "" : s2.Email.ToString(),
									 AssignedToName = s4 == null || s2.AssignToUserID == null ? "" : s4.Name,
									 PromotionResponseStatusName = s3 == null || s3.Name == null ? "" : s3.Name.ToString(),
									 LeadStatus = o5.Status,
									 ProjectStatus = s7.Name,
									 ProjectNumber = s6.JobNumber,
                                     Mark = _leadactivityRepository.GetAll().Where(e => e.ReferanceId == s8.Id).Select(e => e.IsMark).FirstOrDefault(),
                                     LeadId = s2.Id,
									 ActivityReminderTime = _leadactivityRepository.GetAll().Where(e => e.SectionId == 15 && e.ActionId == 8 && e.LeadId == s2.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
									 ActivityDescription = _leadactivityRepository.GetAll().Where(e => e.SectionId == 15 && e.ActionId == 8 && e.LeadId == s2.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
									 ActivityComment = _leadactivityRepository.GetAll().Where(e => e.SectionId == 15 && e.ActionId == 24 && e.LeadId == s2.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
									 TotalPromotion = filteredPromotionUsers.Select(e => e.Id).Count(),
									 InterestedCount = filteredPromotionUsers.Where(e => e.PromotionResponseStatusId == 1).Select(e => e.Id).Count(),
									 NotInterestedCount = filteredPromotionUsers.Where(e => e.PromotionResponseStatusId == 2).Select(e => e.Id).Count(),
									 OtherCount = filteredPromotionUsers.Where(e => e.PromotionResponseStatusId == 3).Select(e => e.Id).Count(),
									 Unhandle = filteredPromotionUsers.Where(e => e.LeadFk.LeadStatusId != 6).Select(e => e.Id).Count(),
									 PromotionSold = filteredPromotionUsers.Where(e => deposite.Contains((int)e.LeadId)).Select(e => e.Id).Count(),
									 PromotionProject = filteredPromotionUsers.Where(e => JobStatus.Contains((int)e.LeadId)).Select(e => e.Id).Count()
								 };

			var totalCount = await filteredPromotionUsers.CountAsync();

			return new PagedResultDto<GetPromotionUserForViewDto>(
				totalCount,
				await promotionUsers.ToListAsync()
			);
		}

		public async Task<GetPromotionUserForViewDto> GetPromotionUserForView(int id)
		{
			var promotionUser = await _promotionUserRepository.GetAsync(id);

			var output = new GetPromotionUserForViewDto { PromotionUser = ObjectMapper.Map<PromotionUserDto>(promotionUser) };

			if (output.PromotionUser.PromotionId != null)
			{
				var _lookupPromotion = await _lookup_promotionRepository.FirstOrDefaultAsync((int)output.PromotionUser.PromotionId);
				output.PromotionTitle = _lookupPromotion?.Title?.ToString();
			}

			if (output.PromotionUser.LeadId != null)
			{
				var _lookupLead = await _lookup_leadRepository.FirstOrDefaultAsync((int)output.PromotionUser.LeadId);
				output.LeadCopanyName = _lookupLead?.CompanyName?.ToString();
			}

			if (output.PromotionUser.PromotionResponseStatusId != null)
			{
				var _lookupPromotionResponseStatus = await _lookup_promotionResponseStatusRepository.FirstOrDefaultAsync((int)output.PromotionUser.PromotionResponseStatusId);
				output.PromotionResponseStatusName = _lookupPromotionResponseStatus?.Name?.ToString();
			}

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_PromotionUsers_Edit)]
		public async Task<GetPromotionUserForEditOutput> GetPromotionUserForEdit(EntityDto input)
		{
			var promotionUser = await _promotionUserRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetPromotionUserForEditOutput { PromotionUser = ObjectMapper.Map<CreateOrEditPromotionUserDto>(promotionUser) };

			if (output.PromotionUser.PromotionId != null)
			{
				var _lookupPromotion = await _lookup_promotionRepository.FirstOrDefaultAsync((int)output.PromotionUser.PromotionId);
				output.PromotionTitle = _lookupPromotion?.Title?.ToString();
			}

			if (output.PromotionUser.LeadId != null)
			{
				var _lookupLead = await _lookup_leadRepository.FirstOrDefaultAsync((int)output.PromotionUser.LeadId);
				output.LeadCopanyName = _lookupLead?.CompanyName?.ToString();
			}

			if (output.PromotionUser.PromotionResponseStatusId != null)
			{
				var _lookupPromotionResponseStatus = await _lookup_promotionResponseStatusRepository.FirstOrDefaultAsync((int)output.PromotionUser.PromotionResponseStatusId);
				output.PromotionResponseStatusName = _lookupPromotionResponseStatus?.Name?.ToString();
			}

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditPromotionUserDto input)
		{
			if (input.PromotionId != null && input.PromotionId != null)
			{
				//If Promotionid and leadid already exist it , shoult edit
				var qry = _promotionUserRepository.GetAll()
					.Where(e => e.PromotionId == input.PromotionId && e.LeadId == input.LeadId)
					.ToList();

				if (qry != null && qry.Count > 0) input.Id = qry[0].Id;
			}

			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_PromotionUsers_Create)]
		protected virtual async Task Create(CreateOrEditPromotionUserDto input)
		{
			var promotionUser = ObjectMapper.Map<PromotionUser>(input);


			if (AbpSession.TenantId != null)
			{
				promotionUser.TenantId = (int)AbpSession.TenantId;
			}


			await _promotionUserRepository.InsertAsync(promotionUser);
		}

		[AbpAuthorize(AppPermissions.Pages_PromotionUsers_Edit)]
		protected virtual async Task Update(CreateOrEditPromotionUserDto input)
		{
			var promotionUser = await _promotionUserRepository.FirstOrDefaultAsync((int)input.Id);
			ObjectMapper.Map(input, promotionUser);
		}

		public virtual async Task UpdatePromotionResponseStatus(int promotionUserId, int value)
		{
			var promotionUser = await _promotionUserRepository.FirstOrDefaultAsync(promotionUserId);
			var Lead = _lookup_leadRepository.GetAll().Where(e => e.Id == promotionUser.LeadId).FirstOrDefault();
			promotionUser.PromotionResponseStatusId = value;
			await _promotionUserRepository.UpdateAsync(promotionUser);

			var Activity = _leadactivityRepository.GetAll().Where(e => e.PromotionUserId == promotionUserId).FirstOrDefault();
			var LeadActivity = _leadactivityRepository.GetAll().Where(e => e.ReferanceId == Activity.Id && e.ActionId == 22).FirstOrDefault();
			LeadActivity.IsMark = true;
			await _leadactivityRepository.UpdateAsync(LeadActivity);

			if (value == 1)
			{
				if (!string.IsNullOrEmpty(Lead.Mobile))
				{
					SendSMSInput sendSMSInput = new SendSMSInput();
					sendSMSInput.PhoneNumber = Lead.Mobile;
					sendSMSInput.Text = "Thanks for the reply. We will get back to you soon.";
					await _applicationSettings.SendSMS(sendSMSInput);
				}
			}
			else if (value == 2)
			{
				if (!string.IsNullOrEmpty(Lead.Mobile))
				{
					SendSMSInput sendSMSInput = new SendSMSInput();
					sendSMSInput.PhoneNumber = Lead.Mobile;
					sendSMSInput.Text = "You have been successfully unsubscribed.";
					await _applicationSettings.SendSMS(sendSMSInput);
				}
			}
			if (!string.IsNullOrEmpty(Lead.Mobile))
			{
				var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
				LeadActivityLog leadactivity = new LeadActivityLog();
				leadactivity.ActionId = 6;
				leadactivity.SectionId = 0;
				leadactivity.ActionNote = "Sms Send From Promotion Tacker";
				leadactivity.LeadId = (int)promotionUser.LeadId;
				leadactivity.ActivityDate = DateTime.UtcNow;
				leadactivity.TemplateId = 0;

				if (AbpSession.TenantId != null)
				{
					leadactivity.TenantId = (int)AbpSession.TenantId;
				}
				if (value == 1)
				{
					leadactivity.Body = "Thanks for the reply. We will get back to you soon.";
				}
				else
				{
					leadactivity.Body = "You have been successfully unsubscribed.";
				}
				await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_PromotionUsers_Delete)]
		public async Task Delete(EntityDto input)
		{
			await _promotionUserRepository.DeleteAsync(input.Id);
		}

		public async Task<FileDto> GetPromotionUsersToExcel(GetAllPromotionUsersForExcelInput input)
		{
			var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
			var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

			var jobnumberlist = new List<int?>();
			jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

			var jobstatuss = new List<int?>();
			if (input.JobStatusIDFilter != 0 && input.JobStatusIDFilter != null)
			{
				jobstatuss = _jobRepository.GetAll().Where(e => e.JobStatusId == input.JobStatusIDFilter).Select(e => e.LeadId).ToList();
			}

			var UserList = new List<long?>();
			if (input.TeamId != 0 && input.TeamId != null)
			{
				UserList = _userTeamRepository.GetAll().Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
			}
			var filteredPromotionUsers = _promotionUserRepository.GetAll()
						.Include(e => e.PromotionFk)
						.Include(e => e.LeadFk)
						.Include(e => e.PromotionResponseStatusFk)
						.Where(e => e.PromotionResponseStatusId != null)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.ResponseMessage.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter) || jobnumberlist.Contains(e.LeadId))
						.WhereIf(input.ResponseDateFilter == "Responce" && SDate != null && EDate != null, e => e.ResponseDate.AddHours(10).Date >= SDate.Value.Date && e.ResponseDate.AddHours(10).Date <= EDate.Value.Date)
						.WhereIf(input.JobStatusIDFilter != 0, e => jobstatuss.Contains(e.LeadId))
						.WhereIf(input.PromotionTitleFilter != 0, e => e.PromotionId == input.PromotionTitleFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.LeadCopanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCopanyNameFilter)
						.WhereIf(input.PromotionResponseSatusIdFilter != null && input.PromotionResponseSatusIdFilter != 0, e => e.PromotionResponseStatusFk != null && e.PromotionResponseStatusFk.Id == input.PromotionResponseSatusIdFilter)
						.WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
						.WhereIf(input.TeamId != null && input.TeamId != 0, e => UserList.Contains(e.LeadFk.AssignToUserID))
						.WhereIf(input.OrganizationUnit != null, e => e.LeadFk.OrganizationId == input.OrganizationUnit);

			var query = (from o in filteredPromotionUsers
						 join o1 in _lookup_promotionRepository.GetAll() on o.PromotionId equals o1.Id into j1
						 from s1 in j1.DefaultIfEmpty()

						 join o2 in _lookup_leadRepository.GetAll() on o.LeadId equals o2.Id into j2
						 from s2 in j2.DefaultIfEmpty()

						 join o3 in _lookup_promotionResponseStatusRepository.GetAll() on o.PromotionResponseStatusId equals o3.Id into j3
						 from s3 in j3.DefaultIfEmpty()

						 join o5 in _leadStatusRepository.GetAll() on s2.LeadStatusId.ToString() equals o5.Id.ToString()

						 join o6 in _jobRepository.GetAll() on o.LeadId equals o6.LeadId into j6
						 from s6 in j6.DefaultIfEmpty()

						 join o7 in _lookup_jobStatusRepository.GetAll() on s6.JobStatusId equals o7.Id into j7
						 from s7 in j7.DefaultIfEmpty()

						 select new GetPromotionUserForViewDto()
						 {
							 PromotionUser = new PromotionUserDto
							 {
								 ResponseDate = o.ResponseDate,
								 ResponseMessage = o.ResponseMessage,
								 Id = o.Id,

							 },
							 PromotionTitle = s1 == null || s1.Title == null ? "" : s1.Title.ToString(),
							 LeadCopanyName = s2 == null || s2.CompanyName == null ? "" : s2.CompanyName.ToString(),
							 PromotionResponseStatusName = s3 == null || s3.Name == null ? "" : s3.Name.ToString(),
							 Mobile = s2 == null || s2.Mobile == null ? "" : s2.Mobile.ToString(),
							 EMail = s2 == null || s2.Email == null ? "" : s2.Email.ToString(),
							 CreationTime = o.CreationTime,
							 LeadStatus = o5.Status,
							 ProjectStatus = s7.Name,
							 ProjectNumber = s6.JobNumber,
							 CustEntered = s2.CreationTime,
							 LastComment = _leadactivityRepository.GetAll().Where(e => e.LeadId == o.LeadId && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
							 LastCommentDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == o.LeadId && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
							 CurrentLeadOwner = _userRepository.GetAll().Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
							 Interested = o.PromotionResponseStatusFk.Name,
							 SalesTeam = _teamRepository.GetAll().Where(e => e.Id == (_userTeamRepository.GetAll().Where(s => s.UserId == o.LeadFk.AssignToUserID).Select(s => s.TeamId).FirstOrDefault())).Select(e => e.Name).FirstOrDefault()
						 });


			var promotionUserListDtos = await query.ToListAsync();

			return _promotionUsersExcelExporter.ExportToFile(promotionUserListDtos);
		}

		public async Task MarkAsReadPromotionSms(int id)
		{
			var activityid = _leadactivityRepository.GetAll().Where(e => e.PromotionUserId == id).Select(e => e.Id).FirstOrDefault();
			var referenceLeadid = _leadactivityRepository.GetAll().Where(e => e.ReferanceId == activityid).Select(e => e.Id).FirstOrDefault();
			var leadactivity = await _leadactivityRepository.FirstOrDefaultAsync((int)referenceLeadid);
			leadactivity.IsMark = true;
			await _leadactivityRepository.UpdateAsync(leadactivity);

		}


		[AbpAuthorize(AppPermissions.Pages_PromotionUsers)]
		public async Task<List<PromotionUserPromotionLookupTableDto>> GetAllPromotionForTableDropdown()
		{
			return await _lookup_promotionRepository.GetAll()
				.Select(promotion => new PromotionUserPromotionLookupTableDto
				{
					Id = promotion.Id,
					DisplayName = promotion == null || promotion.Title == null ? "" : promotion.Title.ToString()
				}).ToListAsync();
		}

		[AbpAuthorize(AppPermissions.Pages_PromotionUsers)]
		public async Task<List<PromotionUserLeadLookupTableDto>> GetAllLeadForTableDropdown()
		{
			return await _lookup_leadRepository.GetAll()
				.Select(lead => new PromotionUserLeadLookupTableDto
				{
					Id = lead.Id,
					DisplayName = lead == null || lead.CompanyName == null ? "" : lead.CompanyName.ToString()
				}).ToListAsync();
		}

		[AbpAuthorize(AppPermissions.Pages_PromotionUsers)]
		public async Task<List<PromotionUserPromotionResponseStatusLookupTableDto>> GetAllPromotionResponseStatusForTableDropdown()
		{
			return await _lookup_promotionResponseStatusRepository.GetAll()
				.Select(promotionResponseStatus => new PromotionUserPromotionResponseStatusLookupTableDto
				{
					Id = promotionResponseStatus.Id,
					DisplayName = promotionResponseStatus == null || promotionResponseStatus.Name == null ? "" : promotionResponseStatus.Name.ToString()
				}).ToListAsync();
		}

	}
}