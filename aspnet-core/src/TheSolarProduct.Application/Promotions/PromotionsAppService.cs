﻿using TheSolarProduct.Promotions;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Promotions.Exporting;
using TheSolarProduct.Promotions.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Leads.Dtos;
using Abp.Authorization.Users;
using TheSolarProduct.Authorization.Users;
using Castle.Facilities.TypedFactory.Internal;
using Org.BouncyCastle.Crypto.Paddings;
using MimeKit.Cryptography;
using System.Runtime.CompilerServices;
using PayPalCheckoutSdk.Orders;
using System.Diagnostics;
using System.Net;
using Microsoft.VisualBasic;
using NUglify.Helpers;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.Leads;
using TheSolarProduct.ApplicationSettings;
using System.Net.Mail;
using Abp.Net.Mail;
using Abp.Runtime.Security;
using System.Text.RegularExpressions;
using Abp.Domain.Uow;
using TheSolarProduct.EmailTemplates;
using Microsoft.AspNetCore.Http;
using System.Web;
using System.Configuration;
using System.Transactions;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Promotions.SMSEmailSending;
using Abp.Runtime.Session;
using Abp.BackgroundJobs;
using Abp.Timing.Timezone;
using Abp.EntityFrameworkCore;
using Abp.Dependency;
using TheSolarProduct.SysJobs;
using TheSolarProduct.Notifications;
using Abp.Notifications;
using TheSolarProduct.Invoices;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.Promotions
{
	[AbpAuthorize(AppPermissions.Pages_Promotions)]
	public class PromotionsAppService : TheSolarProductAppServiceBase, IPromotionsAppService
	{
		private readonly IRepository<Promotion> _promotionRepository;
		private readonly IPromotionsExcelExporter _promotionsExcelExporter;
		private readonly IRepository<PromotionType, int> _lookup_promotionTypeRepository;
		private readonly IRepository<PromotionUser> _PromotionUsersRepository;
		private readonly IRepository<PromotionResponseStatus> _PromotionResponseStatusRepository;
		private readonly IRepository<User, long> _lookup_userRepository;
		private readonly IRepository<LeadActivityLog> _leadactivityRepository;
		private readonly IRepository<Lead> _leadRepository;
		private readonly IApplicationSettingsAppService _applicationSettings;
		private readonly IEmailSender _emailSender;
		private readonly IRepository<PromotionUser> _promotionUserRepository;
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private readonly IRepository<EmailTemplate> _emailTemplateRepository;
		protected readonly IBackgroundJobManager BackgroundJobManager;
		private readonly ITimeZoneConverter _timeZoneConverter;
		private TheSolarProductDbContext _database;
		private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
		private readonly IRepository<User, long> _userRepository;
		private readonly IAppNotifier _appNotifier;
		private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
		private readonly IRepository<Job> _jobRepository;
		public object URLEncoder { get; private set; }

		public PromotionsAppService(IRepository<Promotion> promotionRepository
			, IRepository<LeadActivityLog> leadactivityRepository
			, IPromotionsExcelExporter promotionsExcelExporter
			, IRepository<PromotionType, int> lookup_promotionTypeRepository
			, IRepository<PromotionResponseStatus> PromotionResponseStatusRepository
			, IRepository<PromotionUser> PromotionUsersRepository
			, IRepository<User, long> lookup_userRepository
			, IApplicationSettingsAppService applicationSettings
			, IRepository<Lead> leadRepository
			, IEmailSender emailSender
			, IRepository<PromotionUser> promotionUserRepository
			, IUnitOfWorkManager unitOfWorkManager
			, IRepository<EmailTemplate> emailTemplateRepository
			, IBackgroundJobManager backgroundJobManager
			, ITimeZoneConverter timeZoneConverter
			, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
			IRepository<User, long> userRepository,
			IAppNotifier appNotifier,
			IRepository<InvoicePayment> invoicePaymentRepository,
			IRepository<Job> jobRepository
			)
		{
			_promotionRepository = promotionRepository;
			_promotionsExcelExporter = promotionsExcelExporter;
			_lookup_promotionTypeRepository = lookup_promotionTypeRepository;
			_PromotionResponseStatusRepository = PromotionResponseStatusRepository;
			_PromotionUsersRepository = PromotionUsersRepository;
			_lookup_userRepository = lookup_userRepository;
			_leadactivityRepository = leadactivityRepository;
			_leadRepository = leadRepository;
			_applicationSettings = applicationSettings;
			_emailSender = emailSender;
			_promotionUserRepository = promotionUserRepository;
			_unitOfWorkManager = unitOfWorkManager;
			_emailTemplateRepository = emailTemplateRepository;
			BackgroundJobManager = backgroundJobManager;
			_timeZoneConverter = timeZoneConverter;
			_dbcontextprovider = dbcontextprovider;
			_userRepository = userRepository;
			_appNotifier = appNotifier;
			_invoicePaymentRepository = invoicePaymentRepository;
			_jobRepository = jobRepository;
		}

		/// <summary>
		/// Get Promotion List
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public async Task<PagedResultDto<GetPromotionForViewDto>> GetAll(GetAllPromotionsInput input)
		{
			var SDate = (_timeZoneConverter.Convert(input.FromDateFilter, (int)AbpSession.TenantId));
			var EDate = (_timeZoneConverter.Convert(input.ToDateFilter, (int)AbpSession.TenantId));

			var filteredPromotions = _promotionRepository.GetAll().OrderByDescending(e => e.CreationTime)
				.Include(e => e.PromotionTypeFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter), e => e.Title == input.TitleFilter)
						.WhereIf(input.FromDateFilter != null && input.ToDateFilter != null,
									e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date &&
									e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
						.WhereIf(input.PromotionTitleFilter != 0, e => e.Id == input.PromotionTitleFilter)
						.WhereIf(input.MinAmountFilter != null && input.MaxAmountFilter != null,
									e => e.PromoCharge >= input.MinAmountFilter &&
									e.PromoCharge <= input.MaxAmountFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.Contains(input.DescriptionFilter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.PromotionTypeNameFilter), e => e.PromotionTypeFk != null && e.PromotionTypeFk.Name == input.PromotionTypeNameFilter)
						.WhereIf(input.PromoType != 0, e => e.PromotionTypeId == input.PromoType)
						.WhereIf(input.OrganizationUnit != null && input.OrganizationUnit != 0, e => e.OrganizationID == input.OrganizationUnit);

			if (filteredPromotions != null && filteredPromotions.Count() > 0)
			{
				var JobStatus = _jobRepository.GetAll().Where(e => e.JobStatusId == 1).Select(e => e.LeadId).ToList();
				var deposite = _invoicePaymentRepository.GetAll().Where(e => e.InvoicePaymentStatusId == 2).Select(e => e.JobFk.LeadId).Distinct().ToList();
				var promotionids = filteredPromotions.Select(e => e.Id).Distinct().ToList();
				var promotions = from o in filteredPromotions
								 join o1 in _lookup_promotionTypeRepository.GetAll() on o.PromotionTypeId equals o1.Id into j1
								 from s1 in j1.DefaultIfEmpty()
								 join o2 in _lookup_userRepository.GetAll() on o.CreatorUserId equals o2.Id
								 select new GetPromotionForViewDto()
								 {
									 Promotion = new PromotionDto
									 {
										 Title = o.Title,
										 PromoCharge = o.PromoCharge,
										 Description = o.Description,
										 CreationTime = o.CreationTime
									 },
									 InterestedCount = _PromotionUsersRepository.GetAll().Where(s => s.PromotionId == o.Id && s.PromotionResponseStatusId == 1).Count(),
									 NotInterestedCount = _PromotionUsersRepository.GetAll().Where(s => s.PromotionId == o.Id && s.PromotionResponseStatusId == 2).Count(),
									 OtherCount = _PromotionUsersRepository.GetAll().Where(s => s.PromotionId == o.Id && s.PromotionResponseStatusId == 3).Count(),
									 NoReply = _PromotionUsersRepository.GetAll().Where(s => s.PromotionId == o.Id && s.ResponseMessage == null).Count(),
									 TotalLeads = _PromotionUsersRepository.GetAll().Where(s => s.PromotionId == o.Id).Count(),
									 PromotionTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
									 CreatorUserName = o2.UserName,

                                     TotalPromotion = _PromotionUsersRepository.GetAll().Where(e => promotionids.Contains((int)e.PromotionId)).Select(e => e.Id).Count(),
                                     summaryInterestedCount = _PromotionUsersRepository.GetAll().Where(e => promotionids.Contains((int)e.PromotionId) && e.PromotionResponseStatusId == 1).Select(e => e.Id).Count(),
                                     summaryNotInterestedCount = _PromotionUsersRepository.GetAll().Where(e => promotionids.Contains((int)e.PromotionId) && e.PromotionResponseStatusId == 2).Select(e => e.Id).Count(),
                                     summaryOtherCount = _PromotionUsersRepository.GetAll().Where(e => promotionids.Contains((int)e.PromotionId) && e.PromotionResponseStatusId == 3).Select(e => e.Id).Count(),
                                     Unhandle = _PromotionUsersRepository.GetAll().Where(e => promotionids.Contains((int)e.PromotionId) && e.LeadFk.LeadStatusId != 6).Select(e => e.Id).Count(),
                                     PromotionSold = _PromotionUsersRepository.GetAll().Where(e => promotionids.Contains((int)e.PromotionId) && deposite.Contains((int)e.LeadId)).Select(e => e.Id).Count(),
                                     PromotionProject = _PromotionUsersRepository.GetAll().Where(e => promotionids.Contains((int)e.PromotionId) && JobStatus.Contains((int)e.LeadId)).Select(e => e.Id).Count()

                                 };

				var pagedAndFilteredPromotions = promotions
					//.OrderBy(input.Sorting ?? "id desc")
					.PageBy(input);

				var totalCount = filteredPromotions.Count();

				return new PagedResultDto<GetPromotionForViewDto>(
					totalCount,
					await pagedAndFilteredPromotions.ToListAsync()

				);
			}
			else
			{
				return new PagedResultDto<GetPromotionForViewDto>(0, null);
			}
		}

		/// <summary>
		/// Promotion Detail View
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public async Task<GetPromotionForViewDto> GetPromotionForView(int id)
		{
			var promotion = await _promotionRepository.GetAsync(id);

			var output = new GetPromotionForViewDto { Promotion = ObjectMapper.Map<PromotionDto>(promotion) };

			if (output.Promotion.PromotionTypeId != null)
			{
				var _lookupPromotionType = await _lookup_promotionTypeRepository.FirstOrDefaultAsync((int)output.Promotion.PromotionTypeId);
				output.PromotionTypeName = _lookupPromotionType?.Name?.ToString();
				output.Promotion.Description = WebUtility.HtmlDecode(output.Promotion.Description);
			}

			return output;
		}

		/// <summary>
		/// Get Promotion Data For Edit ==> Not in Use
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		[AbpAuthorize(AppPermissions.Pages_Promotions_Edit)]
		public async Task<GetPromotionForEditOutput> GetPromotionForEdit(EntityDto input)
		{
			var promotion = await _promotionRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetPromotionForEditOutput { Promotion = ObjectMapper.Map<CreateOrEditPromotionDto>(promotion) };

			if (output.Promotion.PromotionTypeId != null)
			{
				var _lookupPromotionType = await _lookup_promotionTypeRepository.FirstOrDefaultAsync((int)output.Promotion.PromotionTypeId);
				output.PromotionTypeName = _lookupPromotionType?.Name?.ToString();
			}

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditPromotionDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
		}

		/// <summary>
		/// Create Promotion
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		[AbpAuthorize(AppPermissions.Pages_Promotions_Create)]
		protected virtual async Task Create(CreateOrEditPromotionDto input)
		{
			//SaveOrTest == 1 To Send Promotion To All Leads
			//SaveOrTest == 2 To Send Promotion To Only Test Users(Data Given in MobileNosList & Emails)

			if (input.SaveOrTest == 1)
			{
				//Send Promotion to all Leads
				var promotion = ObjectMapper.Map<Promotion>(input);

				if (AbpSession.TenantId != null)
				{
					promotion.TenantId = (int)AbpSession.TenantId;
				}

				string LeadStatusIdsFilter = "";
				string LeadSourceIdsFilter = "";
				string StateIdsFilter = "";
				string TeamIdsFilter = "";
				string JobStatusIdsFilter = "";

				if (input.LeadStatusIdsFilter != null)
				{
					foreach (var item in input.LeadStatusIdsFilter)
					{
						LeadStatusIdsFilter = LeadStatusIdsFilter + item;
					}
				}
				if (input.LeadSourceIdsFilter != null)
				{
					foreach (var item in input.LeadSourceIdsFilter)
					{
						LeadSourceIdsFilter = LeadSourceIdsFilter + item;
					}
				}
				if (input.StateIdsFilter != null)
				{
					foreach (var item in input.StateIdsFilter)
					{
						StateIdsFilter = StateIdsFilter + item;
					}
				}
				if (input.TeamIdsFilter != null)
				{
					foreach (var item in input.TeamIdsFilter)
					{
						TeamIdsFilter = TeamIdsFilter + item;
					}
				}
				if (input.JobStatusIdsFilter != null)
				{
					foreach (var item in input.JobStatusIdsFilter)
					{
						JobStatusIdsFilter = JobStatusIdsFilter + item;
					}
				}
				promotion.LeadStatusIdsFilter = LeadStatusIdsFilter;
				promotion.LeadSourceIdsFilter = LeadSourceIdsFilter;
				promotion.StateIdsFilter = StateIdsFilter;
				promotion.TeamIdsFilter = TeamIdsFilter;
				promotion.JobStatusIdsFilter = JobStatusIdsFilter;

				//Insert Data in Promotion Table With Promotion Details & Lead Count
				int newPromotionId = _promotionRepository.InsertAndGetId(promotion);

				string[] arrSelectedLeadIds = input.SelectedLeadIdsForPromotion.Split(',').Distinct().ToArray();

				var List = new List<PromotionUser>();

				var ActivityList = new List<LeadActivityLog>();

				var sendBulkSMSInput = new SendBulkSMSInput();

				var promotionSMSEmailList = new List<PromotionSMSEmailList>();

				List<SendSMSInput> msg = new List<SendSMSInput>();

				if (!string.IsNullOrEmpty(Convert.ToString(newPromotionId)))
				{
					//List for Promotion User Table for all leads
					foreach (string leadId in arrSelectedLeadIds)
					{
						if (!string.IsNullOrEmpty(leadId))
						{
							int LeadId = Convert.ToInt32(leadId);

							var ListItem = new PromotionUser();
							ListItem.CreationTime = promotion.CreationTime;
							ListItem.CreatorUserId = promotion.CreatorUserId;
							ListItem.IsDeleted = false;
							ListItem.PromotionId = newPromotionId;
							ListItem.LeadId = LeadId;
							ListItem.CreatorUserId = promotion.CreatorUserId;
							ListItem.TenantId = promotion.TenantId;
							List.Add(ListItem);
						}
					}

					//Bulk Insert Data in Promotion User Table
					await _dbcontextprovider.GetDbContext().PromotionUsers.AddRangeAsync(List);
					await _dbcontextprovider.GetDbContext().SaveChangesAsync();
				}

				if (!string.IsNullOrEmpty(Convert.ToString(newPromotionId)))
				{
					var PromotionUserList = await _promotionUserRepository.GetAll().Where(e => e.PromotionId == promotion.Id).Select(e => new { e.Id, e.LeadId }).ToListAsync();

					//List for Lead Activity Log Table for all leads
					foreach (var pu in PromotionUserList)
					{
						if (pu.LeadId > 0)
						{
							int LeadId = Convert.ToInt32(pu.LeadId);

							var ActivityListItem = new LeadActivityLog();
							ActivityListItem.ActionId = 21;
							ActivityListItem.ActionNote = "Promotion " + input.Description + " has been sent";
							ActivityListItem.LeadId = LeadId;
							ActivityListItem.Body = input.Description;
							ActivityListItem.TenantId = promotion.TenantId;
							ActivityListItem.CreatorUserId = promotion.CreatorUserId;
							ActivityListItem.CreationTime = promotion.CreationTime;
							ActivityListItem.IsDeleted = false;
							ActivityListItem.PromotionId = promotion.Id;
							ActivityListItem.PromotionUserId = pu.Id;
							ActivityList.Add(ActivityListItem);
						}
					}

					//Bulk Insert Data in Lead Activity Log Table
					await _dbcontextprovider.GetDbContext().LeadActivityLogs.AddRangeAsync(ActivityList);
					await _dbcontextprovider.GetDbContext().SaveChangesAsync();
				}

				if (!string.IsNullOrEmpty(Convert.ToString(newPromotionId)))
				{
					var ActivityLogList = await _leadactivityRepository.GetAll().Where(e => e.PromotionId == promotion.Id).Select(e => new { e.Id, e.LeadId, e.PromotionUserId }).ToListAsync();

					//List for Lead Activity Log Table for all leads
					foreach (var al in ActivityLogList)
					{
						if (al.LeadId > 0)
						{
							int LeadId = Convert.ToInt32(al.LeadId);
							var promotionSMSEmailListItem = new PromotionSMSEmailList();
							var LeadDetail = _leadRepository.GetAll().Where(e => e.Id == LeadId).Select(e => new { e.Mobile, e.Email }).FirstOrDefault();

							//Send Promotion to Only Test Users(Data Given in MobileNosList & Emails)
							if (input.PromotionTypeId == 1)
							{
								if (!string.IsNullOrEmpty(LeadDetail.Mobile))
								{
									var sendSMSInput = new SendSMSInput();
									sendSMSInput.PhoneNumber = LeadDetail.Mobile;
									sendSMSInput.Text = input.Description;
									sendSMSInput.ActivityId = al.Id;
									sendSMSInput.promoresponseID = (int)al.PromotionUserId;
									msg.Add(sendSMSInput);
								}
							}
							else
							{
								if (!string.IsNullOrEmpty(LeadDetail.Email))
								{
									var KeyValue = Convert.ToString(AbpSession.TenantId) + "," + (int)al.PromotionUserId;

									//Token With Tenant Id & Promotion User Primary Id for subscribe & UnSubscribe
									var token = HttpUtility.UrlEncode(SimpleStringCipher.Instance.Encrypt(KeyValue, AppConsts.DefaultPassPhrase));

									//Subscription Link
									string SubscribeLink = "http://thesolarproduct.com/account/customer-subscribe?STR=" + token;

									//UnSubscription Link
									string UnSubscribeLink = "http://thesolarproduct.com/account/customer-unsubscribe?STR=" + token;

									//Footer Design for subscribe & UnSubscribe
									var footer = "<div style=\"border:1px solid #ccc;padding:10px; background:#f2f2f2;\">If you are intrested in this offer, please click <a href=\"" + SubscribeLink + "\">" +
										"<img height=\"20px\" src=\"http://thesolarproduct.com/assets/common/images/like.png\"></a>.<br/>" +
										"If you do not wish to receive any further communications, please click <a href=\"" + UnSubscribeLink + "\">" +
										"<img height=\"20px\" src=\"http://thesolarproduct.com/assets/common/images/dislike.png\"></a>.</div>";

									//Merge Email Body And Footer for subscribe & UnSubscribe
									string FinalEmailBody = input.Description + footer;

									try
									{
										await _emailSender.SendAsync(new MailMessage
										{
											To = { LeadDetail.Email }, ////{ "hiral.prajapati@meghtechnologies.com" },
											Subject = input.Title,
											Body = FinalEmailBody,
											IsBodyHtml = true
										});
										//MailMessage mail = new MailMessage
										//{
										//    From = new MailAddress("info@solarproduct.com.au"), //new MailAddress(assignedToUser.EmailAddress),
										//    To = { "hiral.prajapati@meghtechnologies.com" },
										//    Subject = input.Title,
										//    Body = FinalEmailBody,
										//    IsBodyHtml = true
										//};
										//await this._emailSender.SendAsync(mail);
									}
									catch (Exception e)
									{

									}
								}
							}
						}

						////Resolver for HangfireJob(Must have to use before Job Call)
						//var leadReminderService = IocManager.Instance.Resolve<IClientDomainService>();

						////Hangfire Job(Fire And Forget) 
						//var jobId = Hangfire.BackgroundJob.Enqueue(() => leadReminderService.SendPromotion(promotion.TenantId, promotion.Id, (int)AbpSession.UserId));
					}
					sendBulkSMSInput.Messages = msg;
					await _applicationSettings.SendBulkSMS(sendBulkSMSInput);
				}
			}
			else
			{
				//Send Promotion to Only Test Users(Data Given in MobileNosList & Emails)
				if (input.PromotionTypeId == 1)
				{
					string[] MobileNosList = input.MobileNos.Split(',').Distinct().ToArray();
					foreach (string mobileno in MobileNosList)
					{
						if (!string.IsNullOrEmpty(mobileno))
						{
							try
							{
								SendSMSInput sendSMSInput = new SendSMSInput();
								sendSMSInput.PhoneNumber = mobileno;
								sendSMSInput.Text = input.Description;
								await _applicationSettings.SendSMS(sendSMSInput);
							}
							catch (Exception e)
							{

							}
						}
					}
				}
				else
				{

					if (!string.IsNullOrEmpty(input.Emails))
					{
						var KeyValue = Convert.ToString(AbpSession.TenantId);

						//Token With Tenant Id & Promotion User Primary Id for subscribe & UnSubscribe
						var token = HttpUtility.UrlEncode(SimpleStringCipher.Instance.Encrypt(KeyValue, AppConsts.DefaultPassPhrase));

						//Subscription Link
						string SubscribeLink = "http://thesolarproduct.com/account/customer-subscribe?STR=" + token;

						//UnSubscription Link
						string UnSubscribeLink = "http://thesolarproduct.com/account/customer-unsubscribe?STR=" + token;

						//Footer Design for subscribe & UnSubscribe
						var footer = "<div style=\"border:1px solid #ccc;padding:10px; background:#f2f2f2;\">If you are intrested in this offer, please click <a href=\"" + SubscribeLink + "\">" +
							"<img height=\"20px\" src=\"http://thesolarproduct.com/assets/common/images/like.png\"></a>.<br/>" +
							"If you do not wish to receive any further communications, please click <a href=\"" + UnSubscribeLink + "\">" +
							"<img height=\"20px\" src=\"http://thesolarproduct.com/assets/common/images/dislike.png\"></a>.</div>";

						//Merge Email Body And Footer for subscribe & UnSubscribe
						string FinalEmailBody = input.Description + footer;

						try
						{
							MailMessage mail = new MailMessage
							{
								From = new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
								To = { input.Emails },
								Subject = input.Title,
								Body = FinalEmailBody,
								IsBodyHtml = true
							};
							await this._emailSender.SendAsync(mail);
						}
						catch (Exception e)
						{

						}
					}
				}
			}

		}

		/// <summary>
		/// Edit Promotion Details ==> Not in Use
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		[AbpAuthorize(AppPermissions.Pages_Promotions_Edit)]
		protected virtual async Task Update(CreateOrEditPromotionDto input)
		{
			var promotion = await _promotionRepository.FirstOrDefaultAsync((int)input.Id);
			ObjectMapper.Map(input, promotion);
		}

		/// <summary>
		/// Delete Promotion ==> Not in Use
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		[AbpAuthorize(AppPermissions.Pages_Promotions_Delete)]
		public async Task Delete(EntityDto input)
		{
			await _promotionRepository.DeleteAsync(input.Id);
		}

		/// <summary>
		/// Export Promotion Data to Excel
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public async Task<FileDto> GetPromotionsToExcel(GetAllPromotionsForExcelInput input)
		{
			var SDate = (_timeZoneConverter.Convert(input.FromDateFilter, (int)AbpSession.TenantId));
			var EDate = (_timeZoneConverter.Convert(input.ToDateFilter, (int)AbpSession.TenantId));

			var orginization = new List<int?>();
			if (input.OrganizationUnit != null && input.OrganizationUnit != 0)
			{
				orginization = _PromotionUsersRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit).Select(e => e.PromotionId).ToList();
			}
			var filteredPromotions = _promotionRepository.GetAll().OrderByDescending(e => e.CreationTime)
				.Include(e => e.PromotionTypeFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter), e => e.Title == input.TitleFilter)
						.WhereIf(input.FromDateFilter != null && input.ToDateFilter != null,
									e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date &&
									e.CreationTime.AddHours(10).Date <= EDate.Value.Date)

						.WhereIf(input.MinAmountFilter != null && input.MaxAmountFilter != null,
									e => e.PromoCharge >= input.MinAmountFilter &&
									e.PromoCharge <= input.MaxAmountFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.Contains(input.DescriptionFilter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.PromotionTypeNameFilter), e => e.PromotionTypeFk != null && e.PromotionTypeFk.Name == input.PromotionTypeNameFilter)
						.WhereIf(input.OrganizationUnit != null && input.OrganizationUnit != 0, e => orginization.Contains(e.Id)).OrderByDescending(e => e.Id);


			var promotions = from o in filteredPromotions
							 join o1 in _lookup_promotionTypeRepository.GetAll() on o.PromotionTypeId equals o1.Id into j1
							 from s1 in j1.DefaultIfEmpty()
							 join o2 in _lookup_userRepository.GetAll() on o.CreatorUserId equals o2.Id

							 select new GetPromotionForViewDto()
							 {
								 Promotion = new PromotionDto
								 {
									 Title = o.Title,
									 PromoCharge = o.PromoCharge,
									 Description = o.Description,
									 //Id = o.Id,
									 CreationTime = o.CreationTime,
									 //TotalLeads = o.LeadCount
								 },
								 InterestedCount = _PromotionUsersRepository.GetAll().Where(s => s.PromotionId == o.Id && s.PromotionResponseStatusId == 1).Count(),
								 NotInterestedCount = _PromotionUsersRepository.GetAll().Where(s => s.PromotionId == o.Id && s.PromotionResponseStatusId == 2).Count(),
								 OtherCount = _PromotionUsersRepository.GetAll().Where(s => s.PromotionId == o.Id && s.PromotionResponseStatusId == 3).Count(),
								 NoReply = _PromotionUsersRepository.GetAll().Where(s => s.PromotionId == o.Id && s.ResponseMessage == null).Count(),
								 TotalLeads = _PromotionUsersRepository.GetAll().Where(s => s.PromotionId == o.Id).Count(),
								 PromotionTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
								 CreatorUserName = o2.UserName
							 };

			return _promotionsExcelExporter.ExportToFile(await promotions.ToListAsync());
		}

		/// <summary>
		/// Promotion Type Dropdown
		/// </summary>
		/// <returns></returns>
		[AbpAuthorize(AppPermissions.Pages_Promotions)]
		public async Task<List<PromotionPromotionTypeLookupTableDto>> GetAllPromotionTypeForTableDropdown()
		{
			return await _lookup_promotionTypeRepository.GetAll()
				.Select(promotionType => new PromotionPromotionTypeLookupTableDto
				{
					Id = promotionType.Id,
					DisplayName = promotionType == null || promotionType.Name == null ? "" : promotionType.Name.ToString()
				}).ToListAsync();
		}

		/// <summary>
		/// Subscribe And UnSubscribe for Promotion
		/// </summary>
		/// <param name="STR">Encrypted Link (Tenant Id & Promotion Id)</param>
		/// <param name="PromotionResponseStatusId">1 = Subscribe,2 = UnSubscribe</param>
		/// <returns></returns>
		public async Task SubScribeUnsubscribepromo(string STR, int PromotionResponseStatusId)
		{
			//Encrypt TenantId & PromotionId
			var IDs = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(STR, AppConsts.DefaultPassPhrase));
			var ID = IDs.Split(",");
			int TenantId = Convert.ToInt32(ID[0]);
			int Promoid = Convert.ToInt32(ID[1]);

			//Get Set from GIven Tenant Id
			using (_unitOfWorkManager.Current.SetTenantId(TenantId))
			{
				var promotionUser = await _promotionUserRepository.FirstOrDefaultAsync((int)Promoid);
				promotionUser.PromotionResponseStatusId = PromotionResponseStatusId;
				await _promotionUserRepository.UpdateAsync(promotionUser);

				var prmotionuserid = _leadactivityRepository.GetAll().Where(e => e.PromotionUserId == (int)Promoid).Select(e => e.Id).FirstOrDefault();


				var activity = new LeadActivityLog();
                activity.ActionNote = "Prmotion Email Reply Given By Customer";
                activity.TenantId = TenantId;
                activity.ActionId = 22;
                activity.CreatorUserId = 1;
                activity.LeadId = (int)promotionUser.LeadId;
                //activity.MessageId = smsID;
                activity.ReferanceId = prmotionuserid;
                activity.IsMark = false;
                await _leadactivityRepository.InsertAsync(activity);

				var AssignUserID = _leadRepository.GetAll().Where(e => e.Id == (int)promotionUser.LeadId).Select(e => e.AssignToUserID).FirstOrDefault();
                var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == AssignUserID).FirstOrDefault();
				string msg = string.Format("Promotion Reply Received");
				await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
			}
		}

		/// <summary>
		/// Export Excel With Mobile & Email 
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public async Task<FileDto> CreateWithExcel(CreateOrEditPromotionDto input)
		{
			//Send Promotion to all Leads
			var promotion = ObjectMapper.Map<Promotion>(input);

			if (AbpSession.TenantId != null)
			{
				promotion.TenantId = (int)AbpSession.TenantId;
			}

			string LeadStatusIdsFilter = "";
			string LeadSourceIdsFilter = "";
			string StateIdsFilter = "";
			string TeamIdsFilter = "";
			string JobStatusIdsFilter = "";

			if (input.LeadStatusIdsFilter != null)
			{
				foreach (var item in input.LeadStatusIdsFilter)
				{
					LeadStatusIdsFilter = LeadStatusIdsFilter + item;
				}
			}
			if (input.LeadSourceIdsFilter != null)
			{
				foreach (var item in input.LeadSourceIdsFilter)
				{
					LeadSourceIdsFilter = LeadSourceIdsFilter + item;
				}
			}
			if (input.StateIdsFilter != null)
			{
				foreach (var item in input.StateIdsFilter)
				{
					StateIdsFilter = StateIdsFilter + item;
				}
			}
			if (input.TeamIdsFilter != null)
			{
				foreach (var item in input.TeamIdsFilter)
				{
					TeamIdsFilter = TeamIdsFilter + item;
				}
			}
			if (input.JobStatusIdsFilter != null)
			{
				foreach (var item in input.JobStatusIdsFilter)
				{
					JobStatusIdsFilter = JobStatusIdsFilter + item;
				}
			}
			promotion.LeadStatusIdsFilter = LeadStatusIdsFilter;
			promotion.LeadSourceIdsFilter = LeadSourceIdsFilter;
			promotion.StateIdsFilter = StateIdsFilter;
			promotion.TeamIdsFilter = TeamIdsFilter;
			promotion.JobStatusIdsFilter = JobStatusIdsFilter;

			//Insert Data in Promotion Table With Promotion Details & Lead Count
			int newPromotionId = _promotionRepository.InsertAndGetId(promotion);

			string[] arrSelectedLeadIds = input.SelectedLeadIdsForPromotion.Split(',').Distinct().ToArray();

			var List = new List<PromotionUser>();

			var ActivityList = new List<LeadActivityLog>();

			var sendBulkSMSInput = new SendBulkSMSInput();

			var promotionSMSEmailList = new List<PromotionSMSEmailList>();

			//List for Promotion User Table for all leads
			foreach (string leadId in arrSelectedLeadIds)
			{
				if (!string.IsNullOrEmpty(leadId))
				{
					int LeadId = Convert.ToInt32(leadId);

					var ListItem = new PromotionUser();
					ListItem.CreationTime = promotion.CreationTime;
					ListItem.CreatorUserId = promotion.CreatorUserId;
					ListItem.IsDeleted = false;
					ListItem.PromotionId = newPromotionId;
					ListItem.LeadId = LeadId;
					ListItem.CreatorUserId = promotion.CreatorUserId;
					ListItem.TenantId = promotion.TenantId;
					List.Add(ListItem);

					var ActivityListItem = new LeadActivityLog();
					ActivityListItem.ActionId = 21;
					ActivityListItem.ActionNote = "Promotion " + input.Description + " has been sent";
					ActivityListItem.LeadId = LeadId;
					ActivityListItem.Body = input.Description;
					ActivityListItem.TenantId = promotion.TenantId;
					ActivityListItem.CreatorUserId = promotion.CreatorUserId;
					ActivityListItem.CreationTime = promotion.CreationTime;
					ActivityListItem.IsDeleted = false;
					ActivityList.Add(ActivityListItem);

					var promotionSMSEmailListItem = new PromotionSMSEmailList();
					var LeadData = _leadRepository.GetAll().Where(e => e.Id == LeadId).Select(e => new { e.Mobile, e.Email }).FirstOrDefault();
					promotionSMSEmailListItem.SMS = LeadData.Mobile;
					promotionSMSEmailListItem.Email = LeadData.Email;
					promotionSMSEmailList.Add(promotionSMSEmailListItem);
				}
			}

			//Bulk Insert Data in Promotion User Table
			await _dbcontextprovider.GetDbContext().PromotionUsers.AddRangeAsync(List);
			await _dbcontextprovider.GetDbContext().SaveChangesAsync();

			////Bulk Insert Data in Lead Activity Log Table
			await _dbcontextprovider.GetDbContext().LeadActivityLogs.AddRangeAsync(ActivityList);
			await _dbcontextprovider.GetDbContext().SaveChangesAsync();

			return _promotionsExcelExporter.ExportSMSEmailListFile(promotionSMSEmailList);
		}
	}
}