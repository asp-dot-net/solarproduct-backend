﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using PdfiumViewer;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Leads;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.Quotations.Dtos;
using TheSolarProduct.Storage;
using TheSolarProduct.TheSolarDemo;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using TheSolarProduct.Invoices;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using Abp;
using TheSolarProduct.LeadActivityLogs;
using Abp.Domain.Uow;
using Abp.Timing.Timezone;
using TheSolarProduct.Authorization;
using Abp.Authorization;

namespace TheSolarProduct.Jobs
{
    public class JobInstallerInvoicesAppService : TheSolarProductAppServiceBase, IJobInstallerInvoicesAppService
    {

        private readonly IRepository<InstallerInvoice> _jobInstallerInvoiceRepository;
        private readonly IRepository<Job> _jobJobRepository;
        private readonly IRepository<Lead> _jobLeadsRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<JobType, int> _lookup_jobTypeRepository;
        private readonly IRepository<JobStatus, int> _lookup_jobStatusRepository;
        private readonly IRepository<ProductItem> _productItemRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<InvoiceFile> _InvoiceFileRepository;
        private readonly IRepository<InvoiceImportData> _InvoiceImportDataRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
        public int MaxFileBytes { get; private set; }

        public JobInstallerInvoicesAppService(IRepository<InstallerInvoice> jobInstallerInvoiceRepository,
            IRepository<Job> jobJobRepository,
            IRepository<Lead> jobLeadsRepository,
             IRepository<User, long> userRepository,
             IRepository<UserRole, long> userroleRepository,
             IRepository<Role> roleRepository,
             IRepository<UserTeam> userTeamRepository,
             ITempFileCacheManager tempFileCacheManager,
              IWebHostEnvironment env,
              IRepository<Tenant> tenantRepository,
              UserManager userManager,
              IRepository<JobType, int> lookup_jobTypeRepository,
              IRepository<JobStatus, int> lookup_jobStatusRepository,
              IRepository<ProductItem> productItemRepository,
               IRepository<JobProductItem> jobProductItemRepository,
               IRepository<InvoiceFile> InvoiceFileRepository,
               IRepository<InvoiceImportData> InvoiceImportDataRepository,
               IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
               IRepository<LeadActivityLog> leadactivityRepository,
               IUnitOfWorkManager unitOfWorkManager,
               ITimeZoneConverter timeZoneConverter, IRepository<InvoicePayment> invoicePaymentRepository
            )
        {
            _jobInstallerInvoiceRepository = jobInstallerInvoiceRepository;
            _jobJobRepository = jobJobRepository;
            _jobLeadsRepository = jobLeadsRepository;
            _userRepository = userRepository;
            _userroleRepository = userroleRepository;
            _roleRepository = roleRepository;
            _userTeamRepository = userTeamRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _env = env;
            _tenantRepository = tenantRepository;
            _userManager = userManager;
            _lookup_jobTypeRepository = lookup_jobTypeRepository;
            _lookup_jobStatusRepository = lookup_jobStatusRepository;
            _productItemRepository = productItemRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _InvoiceFileRepository = InvoiceFileRepository;
            _InvoiceImportDataRepository = InvoiceImportDataRepository;
            _dbcontextprovider = dbcontextprovider;
            _leadactivityRepository = leadactivityRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _timeZoneConverter = timeZoneConverter;
            _invoicePaymentRepository = invoicePaymentRepository;
        }

        public async Task CreateOrEdit(CreateOrEditJobInstallerInvoiceDto input)
        {
            if (input.Id == null)
            {

                await Create(input);

            }
            else
            {
                await Update(input);
            }
        }

        //[AbpAuthorize(AppPermissions.Pages_JobTypes_Create)]

        protected virtual async Task Create(CreateOrEditJobInstallerInvoiceDto input)
        {
            try
            {
                var InstallerInvoice = ObjectMapper.Map<InstallerInvoice>(input);

                if (AbpSession.TenantId != null)
                {
                    InstallerInvoice.TenantId = (int)AbpSession.TenantId;
                }
                if (input.Jobdetails != null)
                {
                    int firstCommaIndex = input.Jobdetails.IndexOf('|');

                    string firstPart = input.Jobdetails.Substring(0, firstCommaIndex);

                    var jbid = _jobJobRepository.GetAll().Where(e => e.JobNumber == firstPart).Select(e => e.Id).FirstOrDefault();
                    InstallerInvoice.JobId = Convert.ToInt32(jbid);
                    input.JobId = Convert.ToInt32(jbid);

                }
                if (input.installers != null)
                {
                    int firstCommaIndex = input.installers.IndexOf('|');

                    string firstPart = input.installers.Substring(0, firstCommaIndex);

                    int FirstNameint = firstPart.IndexOf('.');

                    string FirstName = firstPart.Substring(0, FirstNameint);

                    int LastNameint = firstPart.IndexOf('.');

                    string LastName = firstPart.Substring(FirstNameint + 1);
                    var jbid = _userRepository.GetAll().Where(e => e.Name == FirstName && e.Surname == LastName).Select(e => e.Id).FirstOrDefault();
                    InstallerInvoice.InstallerId = Convert.ToInt32(jbid);
                    input.InstallerId = Convert.ToInt32(jbid);

                }
                var isdata = _jobInstallerInvoiceRepository.GetAll().Where(e => e.JobId == input.JobId && e.InstallerId == input.InstallerId && e.InvNo == input.InvNo).Any();
                if (input.FileName != null)
                {
                    var ByteArray = _tempFileCacheManager.GetFile(input.FileToken);

                    if (ByteArray == null)
                    {
                        throw new UserFriendlyException("There is no such file with the token: " + input.FileToken);
                    }

                    var ext = Path.GetExtension(input.FileName);
                    //var storedFile = new BinaryObject(AbpSession.TenantId, byteArray);
                    //await _binaryObjectManager.SaveAsync(storedFile);

                    var Path1 = _env.WebRootPath;
                    var MainFolder = Path1;
                    MainFolder = MainFolder + "\\Documents";

                    var JobNumber = _jobJobRepository.GetAll().Where(e => e.Id == input.JobId).FirstOrDefault();
                    var TenantName = _tenantRepository.GetAll().Where(e => e.Id == JobNumber.TenantId).Select(e => e.TenancyName).FirstOrDefault();

                    string TenantPath = MainFolder + "\\" + TenantName;
                    string JobPath = TenantPath + "\\" + JobNumber.JobNumber;
                    //string JobPath = TenantPath;
                    string SignaturePath = JobPath + "\\InstallerInvoices";
                    string filename = DateTime.Now.Ticks + "_" + "InstallerInv" + ext;

                    var FinalFilePath = SignaturePath + "\\" + filename;

                    if (System.IO.Directory.Exists(MainFolder))
                    {
                        if (System.IO.Directory.Exists(TenantPath))
                        {
                            if (System.IO.Directory.Exists(JobPath))
                            {
                                if (System.IO.Directory.Exists(SignaturePath))
                                {
                                    using (MemoryStream mStream = new MemoryStream(ByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(SignaturePath);
                                    using (MemoryStream mStream = new MemoryStream(ByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(JobPath);
                                if (System.IO.Directory.Exists(SignaturePath))
                                {
                                    using (MemoryStream mStream = new MemoryStream(ByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(SignaturePath);
                                    using (MemoryStream mStream = new MemoryStream(ByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                    }
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(TenantPath);
                            if (System.IO.Directory.Exists(JobPath))
                            {
                                if (System.IO.Directory.Exists(SignaturePath))
                                {
                                    using (MemoryStream mStream = new MemoryStream(ByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(SignaturePath);
                                    using (MemoryStream mStream = new MemoryStream(ByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(JobPath);
                                if (System.IO.Directory.Exists(SignaturePath))
                                {
                                    using (MemoryStream mStream = new MemoryStream(ByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(SignaturePath);
                                    using (MemoryStream mStream = new MemoryStream(ByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(MainFolder);
                        if (System.IO.Directory.Exists(TenantPath))
                        {
                            if (System.IO.Directory.Exists(JobPath))
                            {
                                if (System.IO.Directory.Exists(SignaturePath))
                                {
                                    using (MemoryStream mStream = new MemoryStream(ByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(SignaturePath);
                                    using (MemoryStream mStream = new MemoryStream(ByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(JobPath);
                                if (System.IO.Directory.Exists(SignaturePath))
                                {
                                    using (MemoryStream mStream = new MemoryStream(ByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(SignaturePath);
                                    using (MemoryStream mStream = new MemoryStream(ByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                    }
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(TenantPath);
                            if (System.IO.Directory.Exists(JobPath))
                            {
                                if (System.IO.Directory.Exists(SignaturePath))
                                {
                                    using (MemoryStream mStream = new MemoryStream(ByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(SignaturePath);
                                    using (MemoryStream mStream = new MemoryStream(ByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(JobPath);
                                if (System.IO.Directory.Exists(SignaturePath))
                                {
                                    using (MemoryStream mStream = new MemoryStream(ByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(SignaturePath);
                                    using (MemoryStream mStream = new MemoryStream(ByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                    }
                                }
                            }
                        }
                    }
                    //var JobNumber = _jobJobRepository.GetAll().Where(e => e.Id == input.JobId).Select(e => e.JobNumber).FirstOrDefault();
                    //var TenantName = _tenantRepository.GetAll().Where(e => e.Id == InstallerInvoice.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                    InstallerInvoice.FileName = filename;
                    InstallerInvoice.FilePath = "\\Documents" + "\\" + TenantName + "\\" + JobNumber.JobNumber + "\\" + "InstallerInvoices" + "\\";


                    await _jobInstallerInvoiceRepository.InsertAsync(InstallerInvoice);

                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 26;
                    leadactivity.SectionId = 0;
                    leadactivity.ActionNote = "InstallerInvoices Created";
                    leadactivity.LeadId = (int)(_jobJobRepository.GetAll().Where(e => e.Id == input.JobId).Select(e => e.LeadId).FirstOrDefault());

                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    else
                    {
                        leadactivity.TemplateId = 0;
                    }
                     ;
                    leadactivity.ActivityDate = DateTime.UtcNow;

                    await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);
                }
                else
                {
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 26;
                    leadactivity.SectionId = 0;
                    leadactivity.ActionNote = "InstallerInvoices Created";
                    leadactivity.LeadId = (int)(_jobJobRepository.GetAll().Where(e => e.Id == input.JobId).Select(e => e.LeadId).FirstOrDefault());

                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    else
                    {
                        leadactivity.TemplateId = 0;
                    }
                     ;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);
                    await _jobInstallerInvoiceRepository.InsertAsync(InstallerInvoice);
                }
            } catch (Exception e) { }
        }
        public bool CheckInstallerDetail(CreateOrEditJobInstallerInvoiceDto input)
        {

            var InstallerInvoice = ObjectMapper.Map<InstallerInvoice>(input);

            if (AbpSession.TenantId != null)
            {
                InstallerInvoice.TenantId = (int)AbpSession.TenantId;
            }
            if (input.Jobdetails != null)
            {
                int firstCommaIndex = input.Jobdetails.IndexOf('|');

                string firstPart = input.Jobdetails.Substring(0, firstCommaIndex);

                var jbid = _jobJobRepository.GetAll().Where(e => e.JobNumber == firstPart).Select(e => e.Id).FirstOrDefault();
                InstallerInvoice.JobId = Convert.ToInt32(jbid);
                input.JobId = Convert.ToInt32(jbid);

            }
            if (input.installers != null)
            {
                int firstCommaIndex = input.installers.IndexOf('|');

                string firstPart = input.installers.Substring(0, firstCommaIndex);

                int FirstNameint = firstPart.IndexOf('.');

                string FirstName = firstPart.Substring(0, FirstNameint);

                int LastNameint = firstPart.IndexOf('.');

                string LastName = firstPart.Substring(FirstNameint + 1);
                var jbid = _userRepository.GetAll().Where(e => e.Name == FirstName && e.Surname == LastName).Select(e => e.Id).FirstOrDefault();
                InstallerInvoice.InstallerId = Convert.ToInt32(jbid);
                input.InstallerId = Convert.ToInt32(jbid);

            }
            bool existdata = _jobInstallerInvoiceRepository.GetAll().Where(e => e.JobId == input.JobId && e.InstallerId == input.InstallerId && e.InvNo == input.InvNo).Any();
            return existdata;
        }
        public async Task SaveDocument(UploadDocumentInput input)
        {
            var ByteArray = _tempFileCacheManager.GetFile(input.FileToken);

            if (ByteArray == null)
            {
                throw new UserFriendlyException("There is no such file with the token: " + input.FileToken);
            }

            var ext = Path.GetExtension(input.FileName);
            //var storedFile = new BinaryObject(AbpSession.TenantId, byteArray);
            //await _binaryObjectManager.SaveAsync(storedFile);

            var Path1 = _env.WebRootPath;
            var MainFolder = Path1;
            MainFolder = MainFolder + "\\Documents";

            var JobNumber = _jobJobRepository.GetAll().Where(e => e.Id == input.JobId).FirstOrDefault();
            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == JobNumber.TenantId).Select(e => e.TenancyName).FirstOrDefault();

            string TenantPath = MainFolder + "\\" + TenantName;
            string JobPath = TenantPath + "\\" + JobNumber.JobNumber;
            //string JobPath = TenantPath;
            string SignaturePath = JobPath + "\\InstallerInvoices";
            string filename = DateTime.Now.Ticks + "_" + "InstallerInv" + ext;

            var FinalFilePath = SignaturePath + "\\" + filename;

            if (System.IO.Directory.Exists(MainFolder))
            {
                if (System.IO.Directory.Exists(TenantPath))
                {
                    if (System.IO.Directory.Exists(JobPath))
                    {
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(JobPath);
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(TenantPath);
                    if (System.IO.Directory.Exists(JobPath))
                    {
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(JobPath);
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
            }
            else
            {
                System.IO.Directory.CreateDirectory(MainFolder);
                if (System.IO.Directory.Exists(TenantPath))
                {
                    if (System.IO.Directory.Exists(JobPath))
                    {
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(JobPath);
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(TenantPath);
                    if (System.IO.Directory.Exists(JobPath))
                    {
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(JobPath);
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
            }


        }

        public async Task<CreateOrEditJobInstallerInvoiceDto> GetEditInstallerInvoice(EntityDto input)
        {
            var invoicePayment = await _jobInstallerInvoiceRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<CreateOrEditJobInstallerInvoiceDto>(invoicePayment);


            return output;
        }

        protected virtual async Task Update(CreateOrEditJobInstallerInvoiceDto input)
        {

            var jobType = await _jobInstallerInvoiceRepository.FirstOrDefaultAsync((int)input.Id);
            #region installerinvoice history


            var List = new List<InstInvoiceHistory>();
            if (jobType.JobId != input.JobId)
            {
                InstInvoiceHistory invoiceStatus = new InstInvoiceHistory();
                if (AbpSession.TenantId != null)
                {
                    invoiceStatus.TenantId = (int)AbpSession.TenantId;
                }
                invoiceStatus.FieldName = "JobNumber";
                invoiceStatus.PrevValue = Convert.ToString(jobType.JobId);
                invoiceStatus.CurValue = Convert.ToString(input.JobId);
                invoiceStatus.Action = "Edit";
                invoiceStatus.LastmodifiedDateTime = DateTime.Now;
                invoiceStatus.jobid = jobType.JobId;
                invoiceStatus.InvoiceInstallerId = jobType.Id;

                List.Add(invoiceStatus);


            }
            if (jobType.Installation_Maintenance_Inspection != input.Installation_Maintenance_Inspection)
            {
                InstInvoiceHistory invoiceStatus = new InstInvoiceHistory();
                if (AbpSession.TenantId != null)
                {
                    invoiceStatus.TenantId = (int)AbpSession.TenantId;
                }
                invoiceStatus.FieldName = "Email";
                invoiceStatus.PrevValue = jobType.Installation_Maintenance_Inspection;
                invoiceStatus.CurValue = input.Installation_Maintenance_Inspection;
                invoiceStatus.Action = "Select Type";
                invoiceStatus.LastmodifiedDateTime = DateTime.Now;
                invoiceStatus.jobid = jobType.JobId;
                invoiceStatus.InvoiceInstallerId = jobType.Id;
                List.Add(invoiceStatus);


            }
            if (jobType.InvNo != input.InvNo)
            {
                InstInvoiceHistory invoiceStatus = new InstInvoiceHistory();
                if (AbpSession.TenantId != null)
                {
                    invoiceStatus.TenantId = (int)AbpSession.TenantId;
                }
                invoiceStatus.FieldName = "Invoice No";
                invoiceStatus.PrevValue = Convert.ToString(jobType.InvNo);
                invoiceStatus.CurValue = Convert.ToString(input.InvNo);
                invoiceStatus.Action = "Edit";
                invoiceStatus.LastmodifiedDateTime = DateTime.Now;
                invoiceStatus.jobid = jobType.JobId;
                invoiceStatus.InvoiceInstallerId = jobType.Id;

                List.Add(invoiceStatus);

            }
            if (jobType.InvDate != input.InvDate)
            {
                InstInvoiceHistory invoiceStatus = new InstInvoiceHistory();
                if (AbpSession.TenantId != null)
                {
                    invoiceStatus.TenantId = (int)AbpSession.TenantId;
                }
                invoiceStatus.FieldName = "Invoice Date";
                invoiceStatus.PrevValue = Convert.ToString(jobType.InvDate);
                invoiceStatus.CurValue = Convert.ToString(input.InvDate);
                invoiceStatus.Action = "Edit";
                invoiceStatus.LastmodifiedDateTime = DateTime.Now;
                invoiceStatus.jobid = jobType.JobId;
                invoiceStatus.InvoiceInstallerId = jobType.Id;

                List.Add(invoiceStatus);


            }
            if (jobType.Amount != input.Amount)
            {
                InstInvoiceHistory invoiceStatus = new InstInvoiceHistory();
                if (AbpSession.TenantId != null)
                {
                    invoiceStatus.TenantId = (int)AbpSession.TenantId;
                }
                invoiceStatus.FieldName = "Amount";
                invoiceStatus.PrevValue = Convert.ToString(jobType.Amount);
                invoiceStatus.CurValue = Convert.ToString(input.Amount);
                invoiceStatus.Action = "Edit";
                invoiceStatus.LastmodifiedDateTime = DateTime.Now;
                invoiceStatus.jobid = jobType.JobId;
                invoiceStatus.InvoiceInstallerId = jobType.Id;

                List.Add(invoiceStatus);

            }
            if (jobType.InstallerId != input.InstallerId)
            {
                InstInvoiceHistory invoiceStatus = new InstInvoiceHistory();
                if (AbpSession.TenantId != null)
                {
                    invoiceStatus.TenantId = (int)AbpSession.TenantId;
                }
                invoiceStatus.FieldName = "InstallerId";
                invoiceStatus.PrevValue = Convert.ToString(jobType.InstallerId);
                invoiceStatus.CurValue = Convert.ToString(input.InstallerId);
                invoiceStatus.Action = "Edit";
                invoiceStatus.LastmodifiedDateTime = DateTime.Now;
                invoiceStatus.jobid = jobType.JobId;
                invoiceStatus.InvoiceInstallerId = jobType.Id;

                List.Add(invoiceStatus);

            }
            if (jobType.FileName != input.FileName)
            {
                InstInvoiceHistory invoiceStatus = new InstInvoiceHistory();
                if (AbpSession.TenantId != null)
                {
                    invoiceStatus.TenantId = (int)AbpSession.TenantId;
                }
                invoiceStatus.FieldName = "FileName";
                invoiceStatus.PrevValue = jobType.FileName;
                invoiceStatus.CurValue = input.FileName;
                invoiceStatus.Action = "Edit";
                invoiceStatus.LastmodifiedDateTime = DateTime.Now;
                invoiceStatus.jobid = jobType.JobId;
                invoiceStatus.InvoiceInstallerId = jobType.Id;
                List.Add(invoiceStatus);

            }
            if (jobType.FilePath != input.FilePath)
            {
                InstInvoiceHistory invoiceStatus = new InstInvoiceHistory();
                if (AbpSession.TenantId != null)
                {
                    invoiceStatus.TenantId = (int)AbpSession.TenantId;
                }
                invoiceStatus.FieldName = "FilePath";
                invoiceStatus.PrevValue = jobType.FilePath;
                invoiceStatus.CurValue = input.FilePath;
                invoiceStatus.Action = "Edit";
                invoiceStatus.LastmodifiedDateTime = DateTime.Now;
                invoiceStatus.jobid = jobType.JobId;
                invoiceStatus.InvoiceInstallerId = jobType.Id;
                List.Add(invoiceStatus);
                //await _InstInvoiceHistoryRepository.InsertAsync(invoiceStatus);
            }
            await _dbcontextprovider.GetDbContext().InstInvoiceHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();


            #endregion
            if (AbpSession.TenantId != null)
            {
                jobType.TenantId = (int)AbpSession.TenantId;
            }
            jobType.Installer_selfie = input.Installer_selfie;
            jobType.Front_of_property = input.Front_of_property;
            jobType.Inst_Des_Ele_sign = input.Inst_Des_Ele_sign;
            jobType.Cx_Sign = input.Cx_Sign;
            jobType.CES = input.CES;
            jobType.PanelsSerialNo = input.PanelsSerialNo;
            jobType.InstalaltionPic = input.InstalaltionPic;
            jobType.InverterSerialNo = input.InverterSerialNo;
            jobType.Wi_FiDongle = input.Wi_FiDongle;
            jobType.Traded = input.Traded;
            jobType.Remark_if_owing = input.Remark_if_owing;
            // jobType.Noof_Panels_Invoice = input.Noof_Panels_Invoice;
            jobType.ActualPanels_Installed = input.ActualPanels_Installed;
            jobType.Splits = input.Splits;
            jobType.TravelKMfromWarehouse = input.TravelKMfromWarehouse;
            jobType.otherExtraInvoiceNumber = input.otherExtraInvoiceNumber;
            jobType.Installation_Maintenance_Inspection = input.Installation_Maintenance_Inspection;
            jobType.AllGood_NotGood = input.AllGood_NotGood;
            jobType.NotesOrReasonforPending = input.NotesOrReasonforPending;
            jobType.EmailSent = input.EmailSent;
            jobType.InvoiceAmount = input.InvoiceAmount;
            jobType.InvoiceNo = input.InvoiceNo;
            jobType.Amount = input.Amount;
            jobType.Noof_Panels_invoice = input.Noof_Panels_invoice;
            //  jobType.Finance = input.Finance;
            jobType.IsVerify = input.IsVerify;
            jobType.IsPaid = input.IsPaid;
            jobType.Date = input.Date;
            jobType.Remarks = input.Remarks;
            jobType.ApproedDate = input.ApproedDate;
            jobType.ApproedAmount = input.ApproedAmount;
            jobType.ApprovedNotes = input.ApprovedNotes;
            jobType.BankRefNo = input.BankRefNo;
            await _jobInstallerInvoiceRepository.UpdateAsync(jobType);
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 26;
            leadactivity.SectionId = 0;
            leadactivity.ActionNote = "InstallerInvoices Updated";

            leadactivity.LeadId = (int)(_jobJobRepository.GetAll().Where(e => e.Id == input.JobId).Select(e => e.LeadId).FirstOrDefault());

            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            else
            {
                leadactivity.TemplateId = 0;
            }
                 ;
            leadactivity.ActivityDate = DateTime.UtcNow;
            await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);
            //ObjectMapper.Map(input, jobType);
        }

        //public async Task UpdatechangeDate(CreateOrEditJobInstallerInvoiceDto input)
        //{
        //	var jobType = await _jobInstallerInvoiceRepository.FirstOrDefaultAsync((int)input.Id);
        //	ObjectMapper.Map(input, jobType);
        //}
        public async Task<List<CreateOrEditJobInstallerInvoiceDto>> GetJobInstllationInvoicesByJobId(EntityDto input)
        {
            return await _jobInstallerInvoiceRepository.GetAll()
                .Where(e => e.JobId == input.Id)
            .Select(installerinvoicess => new CreateOrEditJobInstallerInvoiceDto
            {
                Id = installerinvoicess.Id,
                InvTypeId = installerinvoicess.InvTypeId,
                Amount = installerinvoicess.Amount,
                InvDate = installerinvoicess.InvDate,
                AdvanceAmount = installerinvoicess.AdvanceAmount,
                LessDeductAmount = installerinvoicess.LessDeductAmount,
                TotalAmount = installerinvoicess.TotalAmount,
                AdvancePayDate = installerinvoicess.AdvancePayDate,
                PayDate = installerinvoicess.PayDate,
                PaymentsTypeId = installerinvoicess.PaymentsTypeId,
                InstallerId = installerinvoicess.InstallerId,
                InvNo = installerinvoicess.InvNo,
                Notes = installerinvoicess.Notes,
                Remarks = installerinvoicess.Remarks,
                IsPaid = installerinvoicess.IsPaid,
                IsVerify = installerinvoicess.IsVerify,
                FilePath = installerinvoicess.FilePath,
                FileName = installerinvoicess.FileName,
            }).ToListAsync();
        }

        public async Task<PagedResultDto<InstallerNewDto>> GetInstallerNewList(InstallerNewInputDto input)
        {
            try {
                int installerid = 0;
                if (!string.IsNullOrEmpty(input.InstallerId))
                {
                    int firstCommaIndex = input.InstallerId.IndexOf('.');

                    string firstPart = input.InstallerId.Substring(0, firstCommaIndex);

                    var jbid = _userRepository.GetAll().Where(e => e.Name == firstPart).Select(e => e.Id).FirstOrDefault();

                    installerid = Convert.ToInt32(jbid);
                }
                var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
                IList<string> role = await _userManager.GetRolesAsync(User);
                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
                var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

                var filteredLeads = _jobInstallerInvoiceRepository.GetAll()
                    .Include(e => e.JobFk.LeadFk)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFk.JobNumber.Contains(input.Filter) ||
                            e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Mobile.Contains(input.Filter) || e.JobFk.LeadFk.Phone.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.InvoiceNo.Contains(input.Filter))
                            .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                            .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                    .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.Installation_Maintenance_Inspection == input.InvoiceType)
                    .WhereIf(installerid > 0, e => e.InstallerId == installerid).Distinct();

                var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

                var result = (from master in pagedAndFilteredLeads
                              join job in _jobJobRepository.GetAll() on master.JobId equals job.Id
                              join lead in _jobLeadsRepository.GetAll() on job.LeadId equals lead.Id
                              //join jie in _jobInstallerInvoiceRepository.GetAll() on job.Id equals jie.JobId
                              join user in _userRepository.GetAll() on (int)master.InstallerId equals (int)user.Id into userjoined
                              from user in userjoined.DefaultIfEmpty()
                              where master.IsPaid == false && master.IsVerify == false
                              select new InstallerNewDto
                              {
                                  ProjectName = job.JobNumber,
                                  Customer = lead.CompanyName,
                                  Address = job.Address,
                                  Suburb = job.Suburb,
                                  State = job.State,
                                  PCode = lead.PostCode,
                                  InstallDate = job.InstallationDate,
                                  InstallComplate = job.InstalledcompleteDate,
                                  Ammount = master.Amount,
                                  InvDate = master.InvDate,
                                  Issued = master.InvDate,
                                  PaymentDate = master.PayDate,
                                  PaymentStaus = master.AdvancePayDate,
                                  id = master.Id,
                                  installername = user.Name + "/" + user.CompanyName,
                                  invoiceType = master.Installation_Maintenance_Inspection,
                                  invoiceNotes = master.Notes,
                                  Total = Convert.ToString(filteredLeads.Count()),
                                  Pending = Convert.ToString(filteredLeads.Where(e => e.IsVerify != true).Count()),
                                  Verified = Convert.ToString(filteredLeads.Where(e => e.IsVerify != true).Count()),
                                  InvNo = master.InvNo,
                                  InvoiceNO = master.InvoiceNo,
                                  invoiceinstallerId = master.Id,
                                  Filenameinv = master.FileName,
                                  FilePathinv = master.FilePath
                              }).Distinct().ToList();


                var Count = result.Count();

                return new PagedResultDto<InstallerNewDto>(
                    Count, result.ToList()
                );
            }
            catch (Exception e) { throw e; }
        }
        //public async Task<PagedResultDto<InstallerNewDto>> GetInstallerNewList(InstallerNewDto input)
        //{

        //    var result = (from master in _jobLeadsRepository.GetAll()
        //                  join prg in _jobJobRepository.GetAll() on master.Id equals prg.LeadId
        //                  join jie in _jobInstallerInvoiceRepository.GetAll() on prg.Id equals jie.JobId
        //                  where jie.IsPaid == false && jie.IsVerify == false
        //                  select new InstallerNewDto
        //                  {
        //                      ProjectName = prg.JobNumber,
        //                      Customer = master.CompanyName,
        //                      Address = prg.Address,
        //                      Suburb = prg.Suburb,
        //                      State = prg.State,
        //                      PCode = master.PostCode,
        //                      InstallDate = prg.InstallationDate,
        //                      InstallComplate = prg.InstalledcompleteDate,
        //                      Ammount = jie.Amount,
        //                      Issued = jie.InvDate,
        //                      PaymentDate = jie.PayDate,
        //                      PaymentStaus = jie.AdvancePayDate,
        //                      id = jie.Id

        //                  }
        //                  ).ToList();


        //    var Count = result.Count();

        //    return new PagedResultDto<InstallerNewDto>(Count, result.MapTo<List<InstallerNewDto>>());

        //}

        public async Task<PagedResultDto<InstallerPaidDto>> GetInstallerPaidList(InstallerPaidInputDto input)
        {

            var filteredLeads = _jobInstallerInvoiceRepository.GetAll().WhereIf(input.PaymentStatus == 1, e => e.IsPaid == true)
                .WhereIf(input.PaymentStatus == 2, e => e.IsPaid == false)
                //.WhereIf(input.InstallerID != 0, e => e.InstallerId == input.InstallerID)
                .Where(e => e.ApproedAmount == null && e.ApproedDate == null);
            //.WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit);

            var pagedAndFilteredLeads = filteredLeads
            .OrderBy(input.Sorting ?? "id desc")
            .PageBy(input);

            var result = (from master in pagedAndFilteredLeads
                          join job in _jobJobRepository.GetAll() on master.JobId equals job.Id
                          join lead in _jobLeadsRepository.GetAll() on job.LeadId equals lead.Id
                          // join jie in _jobInstallerInvoiceRepository.GetAll() on job.Id equals jie.JobId
                          //  where jie.IsPaid == true && jie.IsVerify == true

                          where master.IsVerify == true && lead.OrganizationId == input.OrganizationUnit
                          select new InstallerPaidDto
                          {
                              ProjectName = job.JobNumber,
                              Customer = lead.CompanyName,
                              Address = job.Address,
                              Suburb = job.Suburb,
                              State = job.State,
                              PCode = lead.PostCode,
                              InstallDate = job.InstallationDate,
                              InstallComplate = job.InstalledcompleteDate,
                              Ammount = master.Amount,
                              InvDate = master.InvDate,
                              Issued = master.InvDate,
                              PaymentDate = master.PayDate,
                              PaymentStaus = master.AdvancePayDate,
                              //Id = job.Id,
                              LeadId = job.LeadId,
                              installerinvoiceId = master.Id,
                              Total = Convert.ToString(_jobInstallerInvoiceRepository.GetAll().Count()),
                              Pending = Convert.ToString(_jobInstallerInvoiceRepository.GetAll().Where(e => e.IsVerify != true).Count()),
                              Verified = Convert.ToString(_jobInstallerInvoiceRepository.GetAll().Where(e => e.IsVerify == true).Count()),
                              paidStatus = master.IsPaid
                          }).Distinct().ToList();


            var Count = result.Count();

            return new PagedResultDto<InstallerPaidDto>(Count, result.ToList());

        }


        public async Task<PagedResultDto<InstallerPaidDto>> GetInstallerApprovedList(InstallerPaidInputDto input)
        {
            try
            {
                int installerid = 0;
                if (!string.IsNullOrEmpty(input.InstallerId))
                {
                    int firstCommaIndex = input.InstallerId.IndexOf('.');

                    string firstPart = input.InstallerId.Substring(0, firstCommaIndex);

                    var jbid = _userRepository.GetAll().Where(e => e.Name == firstPart).Select(e => e.Id).FirstOrDefault();

                    installerid = Convert.ToInt32(jbid);
                }
                var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
                IList<string> role = await _userManager.GetRolesAsync(User);
                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
                var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

                var filteredLeads = _jobInstallerInvoiceRepository.GetAll().Include(e => e.JobFk.LeadFk)
           .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFk.JobNumber.Contains(input.Filter) ||
                   e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Mobile.Contains(input.Filter) || e.JobFk.LeadFk.Phone.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.InvoiceNo.Contains(input.Filter))
                   .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                   .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                   .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
           .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.Installation_Maintenance_Inspection == input.InvoiceType)
           .WhereIf(input.PaymentStatus==0 ,e=> e.ApproedAmount != null && e.ApproedDate != null && e.IsPaid == true && e.Date==null)
           .WhereIf(input.PaymentStatus==1,e=> e.ApproedAmount != null && e.ApproedDate != null && e.IsPaid == true &e.Date!=null)
                    .Where(e => e.ApproedAmount != null && e.ApproedDate != null && e.IsPaid == true);

                //.WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit);

                var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

                var result = (from master in pagedAndFilteredLeads
                              join job in _jobJobRepository.GetAll() on master.JobId equals job.Id
                              join lead in _jobLeadsRepository.GetAll() on job.LeadId equals lead.Id
                              // join jie in _jobInstallerInvoiceRepository.GetAll() on job.Id equals jie.JobId
                              //  where jie.IsPaid == true && jie.IsVerify == true
                              where master.IsVerify == true && lead.OrganizationId == input.OrganizationUnit
                              select new InstallerPaidDto
                              {
                                  ProjectName = job.JobNumber,
                                  Customer = lead.CompanyName,
                                  Address = job.Address,
                                  Suburb = job.Suburb,
                                  State = job.State,
                                  // PCode = lead.PostCode,
                                  InstallDate = job.InstallationDate,
                                  InstallComplate = job.InstalledcompleteDate,
                                  Ammount = master.Amount,
                                  InvDate = master.InvDate,
                                  Issued = master.InvDate,
                                  ApproedDate = master.ApproedDate,
                                  ApproedAmount = master.ApproedAmount,
                                  ApprovedNotes = master.ApprovedNotes,
                                  Id = job.Id,
                                  LeadId = job.LeadId,
                                  installerinvoiceId = master.Id,
                                  Total = Convert.ToString(_jobInstallerInvoiceRepository.GetAll().Count()),
                                  Pending = Convert.ToString(_jobInstallerInvoiceRepository.GetAll().Where(e => e.IsVerify != true).Count()),
                                  Verified = Convert.ToString(_jobInstallerInvoiceRepository.GetAll().Where(e => e.IsVerify == true).Count()),
                                  paidStatus = master.IsPaid,
                                  BankRefNo = master.BankRefNo,

                              }).Distinct().ToList();


                var Count = result.Count();

                return new PagedResultDto<InstallerPaidDto>(Count, result.ToList());
            } catch (Exception e) { throw e; }
        }
        //public async Task<PagedResultDto<InstallerNewDto>> GetInstallerPaidList(InstallerNewDto input)
        //{

        //    var result = (from master in _jobLeadsRepository.GetAll()
        //                  join prg in _jobJobRepository.GetAll() on master.Id equals prg.LeadId
        //                  join jie in _jobInstallerInvoiceRepository.GetAll() on prg.Id equals jie.JobId
        //                  where jie.IsPaid == true && jie.IsVerify == true
        //                  select new InstallerNewDto
        //                  {
        //                      ProjectName = prg.JobNumber,
        //                      Customer = master.CompanyName,
        //                      Address = prg.Address,
        //                      Suburb = prg.Suburb,
        //                      State = prg.State,
        //                      PCode = master.PostCode,
        //                      InstallDate = prg.InstallationDate,
        //                      InstallComplate = prg.InstalledcompleteDate,
        //                      Ammount = jie.Amount,
        //                      Issued = jie.InvDate,
        //                      PaymentDate = jie.PayDate,
        //                      PaymentStaus = jie.AdvancePayDate,

        //                  }
        //                  ).ToList();


        //    var Count = result.Count();

        //    return new PagedResultDto<InstallerNewDto>(Count, result.MapTo<List<InstallerNewDto>>());

        //}

        public async Task<List<CommonLookupDto>> GetAllJobNumberList()
        {
            //var jobnumber = _jobJobRepository.GetAll().Where(e=>e.JobStatusId>5);
            var jobnumber = _jobJobRepository.GetAll();
            var filterjobnumberList = (from item in jobnumber
                                       join o1 in _jobLeadsRepository.GetAll() on item.LeadId equals o1.Id into j1
                                       from s1 in j1.DefaultIfEmpty()
                                       where s1.IsDeleted == false
                                       select new CommonLookupDto()
                                       {
                                           Id = item.Id,
                                           DisplayName = item.JobNumber + "-" + s1.CompanyName,
                                       });
            return filterjobnumberList.ToList();
        }



        public List<NameValue<string>> promotionGetAllLeadSource(string leadSourceName)
        {
            var ListOfLeadSource = (from job in _jobJobRepository.GetAll()
                                    join lead in _jobLeadsRepository.GetAll() on job.LeadId equals lead.Id into leadjoined
                                    from lead in leadjoined
                                    where (job.JobNumber.Contains(leadSourceName) || lead.CompanyName.Contains(leadSourceName) || job.Address.Contains(leadSourceName))
                                    select
                                        new jobnoLookupTableDto
                                        {
                                            Value = job.Id,
                                            Name =Convert.ToString(job.JobNumber) + "| " + Convert.ToString(lead.CompanyName) + "| " + Convert.ToString(job.Address)
                                        }).ToList();


            //Making NameValue Collection of Multi-Select DropDown

            var leadSource = new List<NameValue<string>>();
            foreach (var item in ListOfLeadSource)
                if (item.Name != null)
                {
                    leadSource.Add(new NameValue { Name = item.Name, Value = item.Value.ToString() });
                }


            return
                leadSource.IsNullOrEmpty() ?
                leadSource.ToList() :
                leadSource.Where(c => c.Name.ToLower().Contains(leadSourceName.ToLower())).ToList();

        }
        [UnitOfWork]
        public async Task<List<string>> GetAlljobNumberforAdd(string leadSourceName)
        {
            var ListOfLeadSource = (from job in _jobJobRepository.GetAll()
                                    join lead in _jobLeadsRepository.GetAll() on job.LeadId equals lead.Id into leadjoined
                                    from lead in leadjoined
                                    where (job.JobNumber.Contains(leadSourceName) || lead.CompanyName.Contains(leadSourceName) || job.Address.Contains(leadSourceName))
                                    select new jobnoLookupTableDto
                                    {
                                        Value = job.Id,
                                        Name = Convert.ToString(job.JobNumber) + "| " + Convert.ToString(lead.CompanyName) + "| " + Convert.ToString(job.Address)
                                    });
            return await ListOfLeadSource
                    .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
        }
        public async Task<List<string>> GetInstallerForDropdown(string leadSourceName)
        {
            var User_List = _userRepository
             .GetAll().Where(e=>e.Name.Contains(leadSourceName) || e.Surname.Contains(leadSourceName) || e.CompanyName.Contains(leadSourceName) );
            var ListOfuser = (from user in User_List
                              join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                              from ur in urJoined.DefaultIfEmpty()
                              join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                              from us in usJoined.DefaultIfEmpty()
                              where (us != null && us.DisplayName == "Installer")
                              select new jobnoLookupTableDto()
                              {
                                  Value = (int)user.Id,
                                  Name = user.Name == null || user.FullName == null ? "" : user.Name.ToString() + "." + user.Surname.ToString() + "| " + user.CompanyName
                              });
            return await ListOfuser
                   .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();



        }
        [UnitOfWork]
        public async Task<List<string>> GetAlljobNumberforCommonFilter(string leadSourceName)
        {
            var ListOfLeadSource = (from job in _jobJobRepository.GetAll()
                                    join lead in _jobLeadsRepository.GetAll() on job.LeadId equals lead.Id into leadjoined
                                    from lead in leadjoined
                                    where (job.JobNumber.Contains(leadSourceName) || lead.CompanyName.Contains(leadSourceName) || job.Address.Contains(leadSourceName))
                                    select new jobnoLookupTableDto
                                    {
                                        Value = job.Id,
                                        Name = Convert.ToString(job.JobNumber) + "| " + Convert.ToString(lead.CompanyName) + "| " + Convert.ToString(lead.Mobile) +  "| " + Convert.ToString(lead.Email) + "| " + Convert.ToString(job.Address) 
                                    });
            return await ListOfLeadSource
                    .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
        }

       
        [UnitOfWork]
        public async Task<List<string>> GetOnlyprojectno(string leadSourceName)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var result = _jobJobRepository.GetAll()
                .Where(c => c.JobNumber.Contains(leadSourceName)); 
                var suburbs = from o in result
                              select new jobnoLookupTableDto()
                              {
                                  Value = o.Id,
                                  Name = o.Suburb.ToString()
                              };

                return await suburbs
                    .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
            }
        }
        public async Task<List<JobJobTypeLookupTableDto>> GetInstallerForTableDropdown()
        {
            var User_List = _userRepository
           .GetAll().Where(e => e.IsActive == true);

            var UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Installer")
                            select new JobJobTypeLookupTableDto()
                            {
                                Id = (int)user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            });

            return await UserList.ToListAsync();
        }

        

        public async Task<PagedResultDto<GetInstallerInvoiceForViewDto>> GetInstallerInvoiceTracker(GetAllInstallerInvoiceInput input)
        {
            int installerid = 0;
            if (!string.IsNullOrEmpty(input.InstallerId))
            {
                int firstCommaIndex = input.InstallerId.IndexOf('.');

                string firstPart = input.InstallerId.Substring(0, firstCommaIndex);

                var jbid = _userRepository.GetAll().Where(e => e.Name == firstPart).Select(e => e.Id).FirstOrDefault();

                installerid = Convert.ToInt32(jbid);
            }
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var invoicejobids = _jobInstallerInvoiceRepository.GetAll()
                .WhereIf(input.invoiceStatus == 0, e => e.IsVerify == false && e.IsPaid==false)
                .WhereIf(input.invoiceStatus == 1, e => e.IsVerify == true && e.ApproedDate==null && e.IsPaid == false)

                .Where(e => e.IsPaid == false).Select(e => e.JobId).ToList();
            var invoicename= _jobInstallerInvoiceRepository.GetAll()
                .WhereIf(input.invoiceStatus == 0, e => e.IsVerify == false && e.IsPaid == false)
                .WhereIf(input.invoiceStatus == 1, e => e.IsVerify == true && e.ApproedDate == null && e.IsPaid == false) 
                .Where(e => e.IsPaid == false).Select(e => e.InvoiceNo).Distinct().ToList();
            var invoiceList = _jobInstallerInvoiceRepository.GetAll().Select(e => new { e.JobId, e.IsVerify, e.IsPaid, e.Id }).ToList();

             
            var filteredLeads = _jobInstallerInvoiceRepository.GetAll()
           .Include(e => e.JobFk.LeadFk)
           .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFk.JobNumber.Contains(input.Filter) ||
                   e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Mobile.Contains(input.Filter) || e.JobFk.LeadFk.Phone.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.InvoiceNo.Contains(input.Filter))
                   .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                   .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                   .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
           .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.Installation_Maintenance_Inspection == input.InvoiceType)
           .WhereIf(installerid > 0, e => e.InstallerId == installerid).WhereIf(input.invoiceStatus == 0 ,e => e.IsVerify == false && e.IsPaid == false)
                .WhereIf(input.invoiceStatus == 1, e => e.IsVerify == true && e.ApproedDate == null && e.IsPaid == false).Distinct();

            var pagedAndFilteredJobs = filteredLeads
            .OrderBy(input.Sorting ?? "id desc")
            .PageBy(input);
            var Panels = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1).Select(e => e.Id).ToList();
            var Inverts = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2).Select(e => e.Id).ToList();

            var jobs = (from i1 in pagedAndFilteredJobs
                        join o in _jobJobRepository.GetAll() on i1.JobId equals o.Id into inv
                        from o in inv.DefaultIfEmpty()
                        join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                        from s1 in j1.DefaultIfEmpty() 
                        join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                        from s2 in j2.DefaultIfEmpty() 
                        join o6 in _jobLeadsRepository.GetAll() on o.LeadId equals o6.Id into j6
                        from s6 in j6.DefaultIfEmpty()
                            //join o7 in _jobInstallerInvoiceRepository.GetAll() on o.Id equals o7.JobId into j7
                            //from s7 in j7.DefaultIfEmpty()
                        join userreq in _userRepository.GetAll() on (int)i1.InstallerId equals userreq.Id into user
                        from userval in user.DefaultIfEmpty()
                        select new GetInstallerInvoiceForViewDto()
                        {
                            Job = new JobDto
                            {
                                RegPlanNo = o.RegPlanNo,
                                LotNumber = o.LotNumber,
                                Suburb = o.Suburb,
                                State = o.LeadFk.State,
                                UnitNo = o.UnitNo,
                                UnitType = o.UnitType,
                                NMINumber = o.NMINumber,
                                Id = o.Id,
                                ApplicationRefNo = o.ApplicationRefNo,
                                DistAppliedDate = o.DistAppliedDate,
                                Notes = o.Note,
                                InstallerNotes = o.InstallerNotes,
                                JobNumber = o.JobNumber,
                                MeterNumber = o.MeterNumber,
                                OldSystemDetails = o.OldSystemDetails,
                                PostalCode = o.PostalCode,
                                StreetName = o.StreetName,
                                StreetNo = o.StreetNo,
                                StreetType = o.StreetType,
                                LeadId = o.LeadId,
                                ElecDistributorId = o.ElecDistributorId,
                                ElecRetailerId = o.ElecRetailerId,
                                JobStatusId = o.JobStatusId,
                                JobTypeId = o.JobTypeId,
                                PVDStatus = o.PVDStatus,
                                STCAppliedDate = o.STCAppliedDate,
                                STCUploaddate = o.STCUploaddate,
                                STCUploadNumber = o.STCUploadNumber,
                                DistExpiryDate = o.DistExpiryDate,
                                InstallationDate = o.InstallationDate,
                                InstalledcompleteDate = o.InstalledcompleteDate,
                                PVDNumber = o.PVDNumber,
                                STCNotes = o.STCNotes,
                                TotalCost = o.TotalCost, 
                                owningAmt = (decimal)(o.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.PaidAmmount).Sum())),
                                Remark_if_owing = i1.Remark_if_owing,
                                FinanceName = o.PaymentOptionFk.Name,
                                SystemCapacity = o.SystemCapacity,
                                MeterApplyRef = o.MeterApplyRef,
                                InspectionDate = o.InspectionDate,
                                GridConnectionSmsSend = o.GridConnectionSmsSend,
                                GridConnectionEmailSendDate = o.GridConnectionSmsSendDate,
                                GridConnectionEmailSend = o.GridConnectionEmailSend,
                                GridConnectionSmsSendDate = o.GridConnectionEmailSendDate,
                                InstallerName = userval == null || userval.FullName == null ? "" : userval.FullName.ToString(),
                                InstallerCompanyName = userval == null || userval.CompanyName == null ? "" : userval.CompanyName.ToString(),
                                CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault()
                            },
                            invoiceinstallerId = i1.Id,
                            JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                            JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                            LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                            Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                            Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                            TotalNoOfPanel = _jobProductItemRepository.GetAll().Where(e => Panels.Contains((int)e.ProductItemId) && e.JobId == o.Id).Select(e => e.Quantity).Sum(),
                            TotalNoOfInvert = _jobProductItemRepository.GetAll().Where(e => Inverts.Contains((int)e.ProductItemId) && e.JobId == o.Id).Select(e => e.Quantity).Sum(),
                            Installer_selfie = i1.Installer_selfie == 0 || i1.Installer_selfie == null ? "No" : "Yes",
                            Front_of_property = i1.Front_of_property == 0 || i1.Front_of_property == null ? "No" : "Yes",
                            Inst_Des_Ele_sign = i1.Inst_Des_Ele_sign == 0 || i1.Inst_Des_Ele_sign == null ? "No" : "Yes",
                            Cx_Sign = i1.Cx_Sign == 0 || i1.Cx_Sign == null ? "No" : "Yes",
                            CES = i1.CES == 0 || i1.CES == null ? "No" : "Yes",
                            PanelsSerialNo = i1.PanelsSerialNo == 0 || i1.PanelsSerialNo == null ? "No" : "Yes",
                            InstalaltionPic = i1.InstalaltionPic == 0 || i1.InstalaltionPic == null ? "No" : "Yes",
                            InverterSerialNo = i1.InverterSerialNo == 0 || i1.InverterSerialNo == null ? "No" : "Yes",
                            Wi_FiDongle = i1.Wi_FiDongle == 0 || i1.Wi_FiDongle == null ? "No" : "Yes",
                            Traded = i1.Traded == 0 || i1.Traded == null ? "No" : "Yes",
                            Remark_if_owing = i1.Remark_if_owing,
                            ActualPanels_Installed = i1.ActualPanels_Installed,
                            Splits = i1.Splits == 0 || i1.Splits == null ? "No" : "Yes",
                            TravelKMfromWarehouse = i1.TravelKMfromWarehouse,
                            otherExtraInvoiceNumber = i1.otherExtraInvoiceNumber,
                            Installation_Maintenance_Inspection = i1.Installation_Maintenance_Inspection,
                            AllGood_NotGood = i1.AllGood_NotGood,
                            NotesOrReasonforPending = i1.NotesOrReasonforPending,
                            EmailSent = i1.EmailSent,
                            InvoiceNo = i1.InvoiceNo,
                            Amount = i1.Amount,
                            InvDate = i1.InvDate,
                            Total = Convert.ToString(_jobInstallerInvoiceRepository.GetAll().Count()),
                            Pending = Convert.ToString(_jobInstallerInvoiceRepository.GetAll().Where(e => e.IsVerify != true).Count()),
                            Verified = Convert.ToString(_jobInstallerInvoiceRepository.GetAll().Where(e => e.IsVerify == true).Count()),
                            Awaiting = "0",
                            IsVerify = i1.IsVerify,
                            paidStatus=i1.IsPaid,
                            InvoiceIssuedDate = i1.invoiceIssuedDate ,
                            ActivityReminderTime = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                            ActivityDescription = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                            ActivityComment = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                        }).WhereIf(input.invoiceStatus == 0, e => e.IsVerify == false)
                .WhereIf(input.invoiceStatus == 1, e => e.IsVerify == true );

            var totalCount = _jobInstallerInvoiceRepository.GetAll()
                .WhereIf(input.invoiceStatus == 0, e => e.IsVerify == false)
                .WhereIf(input.invoiceStatus == 1, e => e.IsVerify == true)
                 .Count();

            return new PagedResultDto<GetInstallerInvoiceForViewDto>(
                totalCount,
                await jobs.ToListAsync()
            );
        }
        [AbpAuthorize(AppPermissions.Pages_Installer_Delete)]
        public async Task DeleteEdition(EntityDto input)
        {
            
            var edition = await _jobInstallerInvoiceRepository.FirstOrDefaultAsync((int)input.Id);  
            await _jobInstallerInvoiceRepository.DeleteAsync(edition);
        }
        public async Task<CreateOrEditJobInstallerInvoiceDto> GetJobForEdit(EntityDto input)
        {
            try
            {
                var Inverts = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2).Select(e => e.Id).ToList();

                var Panels = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1).Select(e => e.Id).ToList();
                var job = (from item in _jobInstallerInvoiceRepository.GetAll()
                           join jb in _jobJobRepository.GetAll() on item.JobId equals jb.Id into jbjoined
                           from jb in jbjoined.DefaultIfEmpty()
                           join us in _userRepository.GetAll() on (int)jb.InstallerId equals us.Id into userjoined
                           from us in userjoined.DefaultIfEmpty()
                               //join userreq in _userRepository.GetAll() on (int)jb.InstallerId equals userreq.Id into userjoined
                               //from userreq in userjoined.DefaultIfEmpty()
                           where (item.Id == input.Id)
                           select new CreateOrEditJobInstallerInvoiceDto
                           {

                               JobNumber = jb.JobNumber,
                               JobId = item.JobId,
                               Id = item.Id,
                               State = jb.LeadFk.State,
                               InstalledcompleteDate = jb.InstalledcompleteDate,
                               InstallerName = us.FullName.ToString(),
                               InstallerCompanyName = Convert.ToString(us.CompanyName),
                               TotalCost = jb.TotalCost,
                               owningAmt = Convert.ToDecimal(jb.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == item.JobId).Select(e => e.PaidAmmount).Sum())),
                               TotalNoOfPanel = _jobProductItemRepository.GetAll().Where(e => Panels.Contains((int)e.ProductItemId) && e.JobId == jb.Id).Select(e => e.Quantity).Sum(),
                               totalNoOfInvert = _jobProductItemRepository.GetAll().Where(e => Inverts.Contains((int)e.ProductItemId) && e.JobId == jb.Id).Select(e => e.Quantity).Sum(),
                               Installer_selfie = item.Installer_selfie,
                               Front_of_property = item.Front_of_property,
                               Inst_Des_Ele_sign = item.Inst_Des_Ele_sign,
                               Cx_Sign = item.Cx_Sign,
                               CES = item.CES,
                               PanelsSerialNo = item.PanelsSerialNo,
                               InstalaltionPic = item.InstalaltionPic,
                               InverterSerialNo = item.InverterSerialNo,
                               Wi_FiDongle = item.Wi_FiDongle,
                               Traded = item.Traded,
                               Remark_if_owing = item.Remark_if_owing,
                               ActualPanels_Installed = item.ActualPanels_Installed,
                               Splits = item.Splits,
                               TravelKMfromWarehouse = item.TravelKMfromWarehouse,
                               otherExtraInvoiceNumber = item.otherExtraInvoiceNumber,
                               Installation_Maintenance_Inspection = item.Installation_Maintenance_Inspection,
                               AllGood_NotGood = item.AllGood_NotGood,
                               NotesOrReasonforPending = item.NotesOrReasonforPending,
                               EmailSent = item.EmailSent,
                               InvoiceNo = item.InvoiceNo,
                               InvoiceAmount = item.Amount,
                               FinanceName = jb.PaymentOptionFk.Name,
                               Noof_Panels_invoice = item.Noof_Panels_invoice,
                               Verifieddoublestory = jb.HouseTypeFk.Name,
                               threephase = Convert.ToString(jb.MeterPhaseId)
                           }).FirstOrDefault();


                return job;
            }
            catch (Exception e) { throw e; }
        }
        public async Task RevertVerifiedInvoice(int installerinvoiceId)
        {
            var jobType = await _jobInstallerInvoiceRepository.FirstOrDefaultAsync((int)installerinvoiceId);
            if (AbpSession.TenantId != null)
            {
                jobType.TenantId = (int)AbpSession.TenantId;
            }

            jobType.IsVerify = false;

            await _jobInstallerInvoiceRepository.UpdateAsync(jobType);
        }
        public async Task RevertApprovedInvoice(int installerinvoiceId)
        {
            var jobType = await _jobInstallerInvoiceRepository.FirstOrDefaultAsync((int)installerinvoiceId);
            if (AbpSession.TenantId != null)
            {
                jobType.TenantId = (int)AbpSession.TenantId;
            }

            jobType.ApproedDate = null;
            jobType.ApproedAmount = null;

            await _jobInstallerInvoiceRepository.UpdateAsync(jobType);
        }


        public async Task<PagedResultDto<InstallerInvoiceFilesViewDto>> GetAllInvoiceFiles(InstallerInvoiceFilesInput input)
        {
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var filteredLeads = _InvoiceFileRepository.GetAll();

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var fielist = (from o in pagedAndFilteredLeads
                           select new InstallerInvoiceFilesViewDto()
                           {
                               CreatedUser = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                               FileName = o.FileName,
                               FilePath = o.FilePath,
                               CreatedDate = o.CreationTime
                           });

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<InstallerInvoiceFilesViewDto>(
                totalCount,
                await fielist.ToListAsync()
            );
        }


        public async Task<PagedResultDto<InstallerInvoiceImportDataViewDto>> GetAllInvoiceImportData(InstallerInvoiceImportDataInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var filteredLeads = _InvoiceImportDataRepository.GetAll()
                               .WhereIf(!string.IsNullOrWhiteSpace(input.FilterText), e => false || e.JobFk.JobNumber.Contains(input.FilterText) || e.JobFk.LeadFk.Mobile.Contains(input.FilterText) || e.JobFk.LeadFk.CompanyName.Contains(input.FilterText) || e.JobFk.LeadFk.Mobile.Contains(input.FilterText))
                               .WhereIf(input.Paymenttypeid != 0, e => e.JobFk.PaymentOptionId == input.Paymenttypeid)
                               .WhereIf(input.Payby != 0, e => e.InvoicePaymentMethodFk.Id == input.Paymenttypeid)
                               .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.JobFk.LeadFk.State == input.State)
                               .WhereIf(input.DateFilterType == "CreationDate" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                               .WhereIf(input.DateFilterType == "BankDate" && input.StartDate != null && input.EndDate != null, e => e.Date.Value.AddHours(10).Date >= SDate.Value.Date && e.Date.Value.AddHours(10).Date <= EDate.Value.Date)
                               .WhereIf(input.DateFilterType == "InstallDate" && input.StartDate != null && input.EndDate != null, e => e.JobFk.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.JobFk.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
                               .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationID);
            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var Jobids = filteredLeads.Select(e => e.JobId).ToList();

            var fielist = (from o in pagedAndFilteredLeads
                           select new InstallerInvoiceImportDataViewDto()
                            {
                               Importdata = new ImportDataViewDto
                               {
                                   CreatedUser = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                                   Date = o.Date,
                                   JobNumber = o.JobNumber,
                                   PaymentMethodName = o.InvoicePaymentmothodName,
                                   Description = o.Description,
                                   PaidAmmount = o.PaidAmmount,
                                   AllotedBy = o.AllotedBy,
                                   InvoiceNotDescription = o.InvoiceNotesDescription,
                                   CreatedDate = o.CreationTime,
                                   LeadId = o.JobFk.LeadId,
                                   SSCharge = o.SSCharge,
                                   ReceiptNumber = o.ReceiptNumber,
                                   PurchaseNumber = o.PurchaseNumber
                               },

                               TotalInvoicePayment = _jobJobRepository.GetAll().Where(e => Jobids.Contains(e.Id)).Select(e=>e.TotalCost).Sum(),
                               AmmountRcv = filteredLeads.Select(e => e.PaidAmmount).Sum(),
                           });

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<InstallerInvoiceImportDataViewDto>(
                totalCount,
                await fielist.ToListAsync()
            );
        }
        public async Task<CreateOrEditJobInstallerInvoiceDto> GetDataForedit(EntityDto input)
        {


            var installerinvoice = (await _jobInstallerInvoiceRepository.GetAsync(input.Id)).MapTo<CreateOrEditJobInstallerInvoiceDto>();


            var job = await _jobJobRepository.FirstOrDefaultAsync((int)installerinvoice.JobId);
            if (installerinvoice != null)
            {
                installerinvoice.JobNumber = job.JobNumber;
                installerinvoice.Installation_Maintenance_Inspection = installerinvoice.Installation_Maintenance_Inspection;
                installerinvoice.Installer_selfie = installerinvoice.Installer_selfie;
                installerinvoice.InverterSerialNo = installerinvoice.InverterSerialNo;
                installerinvoice.InvoiceAmount = installerinvoice.InvoiceAmount;
                installerinvoice.InvoiceNo = installerinvoice.InvoiceNo;

                //output.NoofPanelsinvoice = installerinvoice.Noof_Panels_invoice;
                //output.NoofPanelsPortal = installerinvoice.Noof_Panels_Portal;
                //output.NotesOrReasonforPending = installerinvoice.NotesOrReasonforPending;
                //output.PanelsSerialNo = installerinvoice.PanelsSerialNo;
                //output.Remarkifowing = installerinvoice.Remark_if_owing;
                //output.Splits = installerinvoice.Splits;
                //output.Traded = installerinvoice.Traded;
                //output.TravelKMfromWarehouse = installerinvoice.TravelKMfromWarehouse;
                //output.WiFiDongle = installerinvoice.Wi_FiDongle;
                //output.otherExtraInvoiceNumber = installerinvoice.otherExtraInvoiceNumber;
                installerinvoice.FileName = installerinvoice.FileName;
                installerinvoice.FilePath = installerinvoice.FilePath;
            }

            return installerinvoice;

        }

    }

}
