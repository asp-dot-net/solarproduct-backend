﻿using TheSolarProduct.Jobs;
using System.Collections.Generic;


using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.Storage;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Transactions;
using Abp.Domain.Uow;
using TheSolarProduct.EntityFrameworkCore;
using Abp.EntityFrameworkCore;

namespace TheSolarProduct.Jobs
{
	[AbpAuthorize(AppPermissions.Pages_ProductItems, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
	public class ProductItemsAppService : TheSolarProductAppServiceBase, IProductItemsAppService
	{
		private const int MaxFileBytes = 5242880; //5MB
		private readonly IRepository<ProductItem> _productItemRepository;
		private readonly IProductItemsExcelExporter _productItemsExcelExporter;
		private readonly IRepository<ProductType, int> _lookup_productTypeRepository;
		private readonly IWebHostEnvironment _env;
		private readonly IRepository<Tenant> _tenantRepository;
		private readonly ITempFileCacheManager _tempFileCacheManager;
		private readonly IRepository<Warehouselocation> _warehouselocationRepository;
		private readonly IRepository<ProductItemLocation> _productiteamlocationRepository;
		private readonly DbContext _context;
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private TheSolarProductDbContext _database;
		private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

		public ProductItemsAppService(IRepository<ProductItem> productItemRepository
			, IProductItemsExcelExporter productItemsExcelExporter
			, IRepository<ProductType, int> lookup_productTypeRepository
			, IWebHostEnvironment env
			, IRepository<Tenant> tenantRepository
			, ITempFileCacheManager tempFileCacheManager,
			IRepository<Warehouselocation> warehouselocationRepository,
			IRepository<ProductItemLocation> productiteamlocationRepository
			//, DbContext context
			, IUnitOfWorkManager unitOfWorkManager
			, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
		{
			_productItemRepository = productItemRepository;
			_productItemsExcelExporter = productItemsExcelExporter;
			_lookup_productTypeRepository = lookup_productTypeRepository;
			_env = env;
			_tenantRepository = tenantRepository;
			_tempFileCacheManager = tempFileCacheManager;
			_warehouselocationRepository = warehouselocationRepository;
			_productiteamlocationRepository = productiteamlocationRepository;
			//_context = context;
			_unitOfWorkManager = unitOfWorkManager;
			_dbcontextprovider = dbcontextprovider;
		}

		public async Task<PagedResultDto<GetProductItemForViewDto>> GetAll(GetAllProductItemsInput input)
		{

			var filteredProductItems = _productItemRepository.GetAll()
						.Include(e => e.ProductTypeFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Manufacturer.Contains(input.Filter) || e.Model.Contains(input.Filter) || e.Series.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.ManufacturerFilter), e => e.Manufacturer == input.ManufacturerFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.ModelFilter), e => e.Model == input.ModelFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.SeriesFilter), e => e.Series == input.SeriesFilter)
						.WhereIf(input.MinSizeFilter != null, e => e.Size >= input.MinSizeFilter)
						.WhereIf(input.MaxSizeFilter != null, e => e.Size <= input.MaxSizeFilter)
						.WhereIf(input.MinCodeFilter != null, e => e.Code >= input.MinCodeFilter)
						.WhereIf(input.MaxCodeFilter != null, e => e.Code <= input.MaxCodeFilter)
						.WhereIf(input.MinExpiryDateFilter != null, e => e.ExpiryDate >= input.MinExpiryDateFilter)
						.WhereIf(input.MaxExpiryDateFilter != null, e => e.ExpiryDate <= input.MaxExpiryDateFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.ProductTypeNameFilter), e => e.ProductTypeFk != null && e.ProductTypeFk.Name == input.ProductTypeNameFilter)
						.WhereIf(input.ActiveOrDeactive != null && input.ActiveOrDeactive == "Active", e => e.Active == true)
						.WhereIf(input.ActiveOrDeactive != null && input.ActiveOrDeactive == "Deactive", e => e.Active != true)
						.WhereIf(input.ProductTypeId > 0, e => e.ProductTypeId == input.ProductTypeId);

			var pagedAndFilteredProductItems = filteredProductItems
				.OrderBy(input.Sorting ?? "id desc")
				.PageBy(input);

			var productItems = from o in pagedAndFilteredProductItems
							   join o1 in _lookup_productTypeRepository.GetAll() on o.ProductTypeId equals o1.Id into j1
							   from s1 in j1.DefaultIfEmpty()

							   select new GetProductItemForViewDto()
							   {
								   ProductItem = new ProductItemDto
								   {
									   Name = o.Name,
									   Manufacturer = o.Manufacturer,
									   Model = o.Model,
									   Series = o.Series,
									   Size = o.Size,
									   Code = o.Code,
									   Id = o.Id,
									   FileName = o.FileName,
									   FilePath = o.FilePath
								   },
								   ProductTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString()
							   };

			var totalCount = await filteredProductItems.CountAsync();

			return new PagedResultDto<GetProductItemForViewDto>(
				totalCount,
				await productItems.ToListAsync()
			);
		}

		public async Task<GetProductItemForViewDto> GetProductItemForView(int id)
		{
			var productItem = await _productItemRepository.GetAsync(id);

			var output = new GetProductItemForViewDto { ProductItem = ObjectMapper.Map<ProductItemDto>(productItem) };

			if (output.ProductItem.ProductTypeId != null)
			{
				var _lookupProductType = await _lookup_productTypeRepository.FirstOrDefaultAsync((int)output.ProductItem.ProductTypeId);
				output.ProductTypeName = _lookupProductType?.Name?.ToString();
			}

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_ProductItems_Edit)]
		public async Task<GetProductItemForEditOutput> GetProductItemForEdit(EntityDto input)
		{
			var productItem = await _productItemRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetProductItemForEditOutput { ProductItem = ObjectMapper.Map<CreateOrEditProductItemDto>(productItem) };
			output.ProductItem.IsActive = productItem.Active;
			if (output.ProductItem.ProductTypeId != null)
			{
				var _lookupProductType = await _lookup_productTypeRepository.FirstOrDefaultAsync((int)output.ProductItem.ProductTypeId);
				output.ProductTypeName = _lookupProductType?.Name?.ToString();
			}
			output.ProductItem.QLDSalesTag = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id && e.WarehouselocationId == 1).Select(e => e.SalesTag).FirstOrDefault();
			output.ProductItem.VICSalesTag = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id && e.WarehouselocationId == 2).Select(e => e.SalesTag).FirstOrDefault();
			output.ProductItem.NSWSalesTag = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id && e.WarehouselocationId == 3).Select(e => e.SalesTag).FirstOrDefault();
			output.ProductItem.WASalesTag = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id && e.WarehouselocationId == 4).Select(e => e.SalesTag).FirstOrDefault();
			output.ProductItem.NTSalesTag = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id && e.WarehouselocationId == 5).Select(e => e.SalesTag).FirstOrDefault();
			output.ProductItem.TASSalesTag = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id && e.WarehouselocationId == 6).Select(e => e.SalesTag).FirstOrDefault();
			output.ProductItem.SASalesTag = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id && e.WarehouselocationId == 7).Select(e => e.SalesTag).FirstOrDefault();
			output.ProductItem.AUSSalesTag = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id && e.WarehouselocationId == 8).Select(e => e.SalesTag).FirstOrDefault();
			return output;
		}

		public async Task CreateOrEdit(CreateOrEditProductItemDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_ProductItems_Create)]
		protected virtual async Task Create(CreateOrEditProductItemDto input)
		{
			var productItem = ObjectMapper.Map<ProductItem>(input);

			if (AbpSession.TenantId != null)
			{
				productItem.TenantId = (int)AbpSession.TenantId;
			}
			productItem.Active = input.IsActive;
			int Productiteamid = _productItemRepository.InsertAndGetId(productItem);

			var locationids = _warehouselocationRepository.GetAll().Select(e => e.Id).ToList();

			foreach (var warehouseid in locationids)
			{
				bool salestage = false;
				if (warehouseid == 1)
				{
					if (input.QLDSalesTag == true)
					{
						salestage = true;
					}
				}
				else if (warehouseid == 2)
				{
					if (input.VICSalesTag == true)
					{
						salestage = true;
					}
				}
				else if (warehouseid == 3)
				{
					if (input.NSWSalesTag == true)
					{
						salestage = true;
					}
				}
				else if (warehouseid == 4)
				{
					if (input.WASalesTag == true)
					{
						salestage = true;
					}
				}
				else if (warehouseid == 5)
				{
					if (input.NTSalesTag == true)
					{
						salestage = true;
					}
				}
				else if (warehouseid == 6)
				{
					if (input.TASSalesTag == true)
					{
						salestage = true;
					}
				}
				else if (warehouseid == 7)
				{
					if (input.SASalesTag == true)
					{
						salestage = true;
					}
				}
				else if (warehouseid == 8)
				{
					if (input.AUSSalesTag == true)
					{
						salestage = true;
					}
				}
				ProductItemLocation productiteamlocation = new ProductItemLocation();
				productiteamlocation.ProductItemId = Productiteamid;
				productiteamlocation.WarehouselocationId = warehouseid;
				productiteamlocation.SalesTag = salestage;
				if (AbpSession.TenantId != null)
				{
					productiteamlocation.TenantId = (int)AbpSession.TenantId;
				}
				await _productiteamlocationRepository.InsertAsync(productiteamlocation);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_ProductItems_Edit)]
		protected virtual async Task Update(CreateOrEditProductItemDto input)
		{
			var productItem = await _productItemRepository.FirstOrDefaultAsync((int)input.Id);
			productItem.Active = input.IsActive;
			ObjectMapper.Map(input, productItem);

			var locationids = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id).ToList();

			if (locationids.Count > 0)
			{
				foreach (var warehouseid in locationids)
				{
					///  bool salestage = false;
					if (warehouseid.WarehouselocationId == 1)
					{
						warehouseid.SalesTag = input.QLDSalesTag;
					}
					else if (warehouseid.WarehouselocationId == 2)
					{
						warehouseid.SalesTag = input.VICSalesTag;
					}
					else if (warehouseid.WarehouselocationId == 3)
					{
						warehouseid.SalesTag = input.NSWSalesTag;
					}
					else if (warehouseid.WarehouselocationId == 4)
					{
						warehouseid.SalesTag = input.WASalesTag;
					}
					else if (warehouseid.WarehouselocationId == 5)
					{
						warehouseid.SalesTag = input.NTSalesTag;
					}
					else if (warehouseid.WarehouselocationId == 6)
					{
						warehouseid.SalesTag = input.TASSalesTag;
					}
					else if (warehouseid.WarehouselocationId == 7)
					{
						warehouseid.SalesTag = input.SASalesTag;
					}
					else if (warehouseid.WarehouselocationId == 8)
					{
						warehouseid.SalesTag = input.AUSSalesTag;
					}
					await _productiteamlocationRepository.UpdateAsync(warehouseid);
				}
			}
			else
			{
				var warehousids = _warehouselocationRepository.GetAll().Select(e => e.Id).ToList();

				foreach (int warehouseid in warehousids)
				{
					bool salestage = false;
					if (warehouseid == 1)
					{
						if (input.QLDSalesTag == true)
						{
							salestage = true;
						}
					}
					else if (warehouseid == 2)
					{
						if (input.VICSalesTag == true)
						{
							salestage = true;
						}
					}
					else if (warehouseid == 3)
					{
						if (input.NSWSalesTag == true)
						{
							salestage = true;
						}
					}
					else if (warehouseid == 4)
					{
						if (input.WASalesTag == true)
						{
							salestage = true;
						}
					}
					else if (warehouseid == 5)
					{
						if (input.NTSalesTag == true)
						{
							salestage = true;
						}
					}
					else if (warehouseid == 6)
					{
						if (input.TASSalesTag == true)
						{
							salestage = true;
						}
					}
					else if (warehouseid == 7)
					{
						if (input.SASalesTag == true)
						{
							salestage = true;
						}
					}
					else if (warehouseid == 8)
					{
						if (input.AUSSalesTag == true)
						{
							salestage = true;
						}
					}
					ProductItemLocation productiteamlocation = new ProductItemLocation();
					productiteamlocation.ProductItemId = (int)input.Id;
					productiteamlocation.WarehouselocationId = warehouseid;
					productiteamlocation.SalesTag = salestage;
					if (AbpSession.TenantId != null)
					{
						productiteamlocation.TenantId = (int)AbpSession.TenantId;
					}
					await _productiteamlocationRepository.InsertAsync(productiteamlocation);
				}

			}
		}

		[AbpAuthorize(AppPermissions.Pages_ProductItems_Delete)]
		public async Task Delete(EntityDto input)
		{
			await _productItemRepository.DeleteAsync(input.Id);
		}

		public async Task<FileDto> GetProductItemsToExcel(GetAllProductItemsForExcelInput input)
		{

			var filteredProductItems = _productItemRepository.GetAll()
						.Include(e => e.ProductTypeFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Manufacturer.Contains(input.Filter) || e.Model.Contains(input.Filter) || e.Series.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.ManufacturerFilter), e => e.Manufacturer == input.ManufacturerFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.ModelFilter), e => e.Model == input.ModelFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.SeriesFilter), e => e.Series == input.SeriesFilter)
						.WhereIf(input.MinSizeFilter != null, e => e.Size >= input.MinSizeFilter)
						.WhereIf(input.MaxSizeFilter != null, e => e.Size <= input.MaxSizeFilter)
						.WhereIf(input.MinCodeFilter != null, e => e.Code >= input.MinCodeFilter)
						.WhereIf(input.MaxCodeFilter != null, e => e.Code <= input.MaxCodeFilter)

						.WhereIf(input.MinExpiryDateFilter != null, e => e.ExpiryDate >= input.MinExpiryDateFilter)
						.WhereIf(input.MaxExpiryDateFilter != null, e => e.ExpiryDate <= input.MaxExpiryDateFilter)

						.WhereIf(!string.IsNullOrWhiteSpace(input.ProductTypeNameFilter), e => e.ProductTypeFk != null && e.ProductTypeFk.Name == input.ProductTypeNameFilter);

			var query = (from o in filteredProductItems
						 join o1 in _lookup_productTypeRepository.GetAll() on o.ProductTypeId equals o1.Id into j1
						 from s1 in j1.DefaultIfEmpty()

						 select new GetProductItemForViewDto()
						 {
							 ProductItem = new ProductItemDto
							 {
								 Name = o.Name,
								 Manufacturer = o.Manufacturer,
								 Model = o.Model,
								 Series = o.Series,
								 Size = o.Size,
								 Code = o.Code,
								 Id = o.Id
							 },
							 ProductTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString()
						 });


			var productItemListDtos = await query.ToListAsync();

			return _productItemsExcelExporter.ExportToFile(productItemListDtos);
		}

		public async Task<List<ProductItemProductTypeLookupTableDto>> GetAllProductTypeForTableDropdown()
		{
			return await _lookup_productTypeRepository.GetAll()
				.Select(productType => new ProductItemProductTypeLookupTableDto
				{
					Id = productType.Id,
					DisplayName = productType == null || productType.Name == null ? "" : productType.Name.ToString()
				}).ToListAsync();
		}

		public async Task FetchAndInsert()
		{
			int ProductItemId = 0;
			int StockItemsLocationId = 0;

			using (_unitOfWorkManager.Current.SetTenantId(AbpSession.TenantId))
			{
				ProductItemId = _productItemRepository.GetAll().OrderByDescending(e => e.Id).Select(e => e.Id).FirstOrDefault();
				StockItemsLocationId = _productiteamlocationRepository.GetAll().OrderByDescending(e => e.Id).Select(e => e.Id).FirstOrDefault();
			}


			String SqlconString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=ariseadmin;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
			List<CreateOrEditProductItemDto> ProductItem = new List<CreateOrEditProductItemDto>();
			List<CreateOrEditProductItemDto> StockItemsLocation = new List<CreateOrEditProductItemDto>();


			using (SqlConnection sqlCon = new SqlConnection(SqlconString))
			{
				sqlCon.Open();
				SqlCommand sql_cmnd2 = new SqlCommand("GetData_tblStockItems", sqlCon);
				sql_cmnd2.CommandType = CommandType.StoredProcedure;
				sql_cmnd2.Parameters.AddWithValue("@Id", SqlDbType.Int).Value = ProductItemId;
				var tablePicklist = new DataTable();
				tablePicklist.Load(sql_cmnd2.ExecuteReader());
				var query = from pickItem in tablePicklist.AsEnumerable()
							select new CreateOrEditProductItemDto
							{
								Id = pickItem.Field<int>("StockItemID"),
								ProductTypeId = pickItem.Field<int>("StockCategoryID"),
								Name = pickItem.Field<string>("StockItem"),
								Manufacturer = pickItem.Field<string>("StockManufacturer"),
								Model = pickItem.Field<string>("StockModel"),
								Series = pickItem.Field<string>("StockSeries"),
								Size = pickItem.Field<decimal?>("Size"),
								Code = pickItem.Field<int?>("StockCode"),
								ShortName = pickItem.Field<string>("ShortName"),
								Description = pickItem.Field<string>("StockDescription"),
								InverterCert = pickItem.Field<string>("InverterCert"),
								IsActive = pickItem.Field<bool>("Active"),
								ApprovedDate = pickItem.Field<DateTime?>("ApprovedDate"),
								ExpiryDate = pickItem.Field<DateTime?>("ExpiryDate"),
								FireTested = pickItem.Field<string>("Firetested"),
								ACPower = pickItem.Field<string>("ACPower"),
								StockId = Convert.ToString(pickItem.Field<int>("StockItemID")),
							};
				sqlCon.Close();
				ProductItem = query.ToList();

				//SqlCommand sql_cmnd = new SqlCommand("GetData_tblStockItemsLocation", sqlCon);
				//sql_cmnd.CommandType = CommandType.StoredProcedure;
				//sql_cmnd.Parameters.AddWithValue("@Id", SqlDbType.Int).Value = StockItemsLocationId;
				//var tableStockItemsLocation = new DataTable();
				//tableStockItemsLocation.Load(sql_cmnd.ExecuteReader());

				//var query1 = from pickItem in tableStockItemsLocation.AsEnumerable()
				//			 select new CreateOrEditProductItemDto
				//			 {
				//				 Id = pickItem.Field<int>("id"),
				//				 ProductTypeId = pickItem.Field<int>("StockItemID"),
				//				 Name = pickItem.Field<string>("CompanyLocationID"),
				//				 Manufacturer = pickItem.Field<string>("SalesTag")
				//			 };
				//sqlCon.Close();
				//StockItemsLocation = query1.ToList();

			}

			foreach (var item in ProductItem)
			{
				var productItem = ObjectMapper.Map<ProductItem>(item);

				using (var uow = _unitOfWorkManager.Begin(TransactionScopeOption.RequiresNew))
				{
					_dbcontextprovider.GetDbContext().ProductItems.AddRange(productItem);

					await _dbcontextprovider.GetDbContext().Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.ProductItems ON");

					_dbcontextprovider.GetDbContext().SaveChanges();

					await _dbcontextprovider.GetDbContext().Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.ProductItems OFF");

					await uow.CompleteAsync();
				}
			}
		}


		public async Task SaveProductIteamDocument(string FileToken, string FileNames , int id)
		{
			var fileBytes = _tempFileCacheManager.GetFile(FileToken);

			if (fileBytes == null)
			{
				throw new UserFriendlyException("There is no such file with the token: " + FileToken);
			}

			if (fileBytes.Length > MaxFileBytes)
			{
				throw new UserFriendlyException("Document size exceeded");
			}


			var Path1 = _env.WebRootPath;
			var MainFolder = Path1;
			MainFolder = MainFolder + "\\Documents";

			var ProductItems = _productItemRepository.GetAll().Where(e => e.Id == id).FirstOrDefault();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			string FileName = ProductItems.Model + DateTime.Now.Ticks;
			string TenantPath = MainFolder + "\\" + TenantName + "\\";
			string SignaturePath = TenantPath + "ProductItem\\";
			var FinalFilePath = SignaturePath + "\\" + FileNames;

			if (System.IO.Directory.Exists(MainFolder))
			{
				if (System.IO.Directory.Exists(TenantPath))
				{
					if (System.IO.Directory.Exists(SignaturePath))
					{
						using (MemoryStream mStream = new MemoryStream(fileBytes))
						{
							System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(SignaturePath);
						using (MemoryStream mStream = new MemoryStream(fileBytes))
						{
							System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
						}
					}
				}
				else
				{
					System.IO.Directory.CreateDirectory(TenantPath);

					if (System.IO.Directory.Exists(SignaturePath))
					{
						using (MemoryStream mStream = new MemoryStream(fileBytes))
						{
							System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(SignaturePath);
						using (MemoryStream mStream = new MemoryStream(fileBytes))
						{
							System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
						}
					}
				}
			}
			else
			{
				System.IO.Directory.CreateDirectory(MainFolder);
				if (System.IO.Directory.Exists(TenantPath))
				{
					if (System.IO.Directory.Exists(SignaturePath))
					{
						using (MemoryStream mStream = new MemoryStream(fileBytes))
						{
							System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(SignaturePath);
						using (MemoryStream mStream = new MemoryStream(fileBytes))
						{
							System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
						}
					}
				}
				else
				{
					System.IO.Directory.CreateDirectory(TenantPath);

					if (System.IO.Directory.Exists(SignaturePath))
					{
						using (MemoryStream mStream = new MemoryStream(fileBytes))
						{
							System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(SignaturePath);
						using (MemoryStream mStream = new MemoryStream(fileBytes))
						{
							System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
						}
					}
				}
			}

			ProductItems.FileName = FileNames;
			ProductItems.FilePath = "\\Documents" + "\\" + TenantName + "\\ProductItem\\";

			await _productItemRepository.UpdateAsync(ProductItems);



		}
	}
}