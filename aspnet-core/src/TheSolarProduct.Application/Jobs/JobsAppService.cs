﻿using TheSolarProduct.Jobs;
using System.Collections.Generic;
using TheSolarProduct.Leads;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Installer;
using Abp.AutoMapper;
using TheSolarProduct.Invoices;
using TheSolarProduct.MultiTenancy;
using Abp.Organizations;
using TheSolarProduct.Installer.Dtos;
using GetAllForLookupTableInput = TheSolarProduct.Jobs.Dtos.GetAllForLookupTableInput;
using TheSolarProduct.HoldReasons;
using TheSolarProduct.PVDStatuses;
using TheSolarProduct.Organizations;
using Abp.Notifications;
using TheSolarProduct.Notifications;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Quotations.Dtos;
using TheSolarProduct.PickList;
using TheSolarProduct.Quotations;
using TheSolarProduct.Leads.Exporting;
using TheSolarProduct.LeadSources;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.SmsTemplates;
using TheSolarProduct.EmailTemplates;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.ApplicationSettings;
using System.Net.Mail;
using Abp.Net.Mail;
using TheSolarProduct.PreviousJobStatuses;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.SmsTemplates.Dtos;
using TheSolarProduct.EmailTemplates.Dtos;
using Abp.Runtime.Security;
using TheSolarProduct.Promotions;
using Abp.UI;
using TheSolarProduct.Authorization.Roles;
using Abp.Authorization.Users;
using System.Globalization;
using Abp.Timing.Timezone;
using Abp;
using TheSolarProduct.LeadStatuses;
using System.IO;
using TheSolarProduct.Storage;
using Microsoft.AspNetCore.Hosting;

namespace TheSolarProduct.Jobs
{
    //[AbpAuthorize(AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    public class JobsAppService : TheSolarProductAppServiceBase, IJobsAppService
    {

        private const int MaxFileBytes = 5242880; //5MB
        private readonly IRepository<Job> _jobRepository;
        
        private readonly IJobsExcelExporter _jobsExcelExporter;
        private readonly IRepository<JobType, int> _lookup_jobTypeRepository;
        private readonly IRepository<JobStatus, int> _lookup_jobStatusRepository;
        private readonly IRepository<RoofType, int> _lookup_roofTypeRepository;
        private readonly IRepository<RoofAngle, int> _lookup_roofAngleRepository;
        private readonly IRepository<ElecDistributor, int> _lookup_elecDistributorRepository;
        private readonly IRepository<Lead, int> _lookup_leadRepository;
        private readonly IRepository<ElecRetailer, int> _lookup_elecRetailerRepository;
        private readonly IRepository<PaymentOption, int> _lookup_paymentOptionRepository;
        private readonly IRepository<DepositOption, int> _lookup_depositOptionRepository;
        private readonly IRepository<MeterUpgrade, int> _lookup_meterUpgradeRepository;
        private readonly IRepository<MeterPhase, int> _lookup_meterPhaseRepository;
        private readonly IRepository<PromotionOffer, int> _lookup_promotionOfferRepository;
        private readonly IRepository<HouseType, int> _lookup_houseTypeRepository;
        private readonly IRepository<FinanceOption, int> _lookup_financeOptionRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<JobPromotion> _jobPromotionRepository;
        private readonly IRepository<JobVariation> _jobVariationRepository;
        private readonly IRepository<Variation> _variationRepository;
        private readonly IRepository<STCZoneRating> _stcZoneRatingRepository;
        private readonly IRepository<STCPostalCode> _stcPostalCodeRepository;
        private readonly IRepository<STCYearWiseRate> _stcYearWiseRateRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<InstallerDetail> _installerDetailRepository;
        private readonly IRepository<InstallerAvailability> _installerAvailabilityRepository;
        private readonly IRepository<JobCancellationReason> _jobCancellationReasonReasonRepository;
        private readonly IRepository<HoldReason> _jobHoldReasonRepository;
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
        private readonly IRepository<Quotation> _quotationRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendOrganizationUnitRepository;
        private readonly IRepository<Warehouselocation> _warehouselocationRepository;
        private readonly IRepository<PVDStatus, int> _pVDStatusRepository;
        private readonly UserManager _userManager;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<ProductItem> _productItemRepository;
        private readonly IRepository<Document> _documentRepository;
        private readonly IRepository<JobRefund> _jobRefundRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<DocumentType, int> _lookup_documentTypeRepository;
        private readonly ILeadsExcelExporter _leadsExcelExporter;
        private readonly IRepository<LeadSource, int> _lookup_leadSourceRepository;
        private readonly IRepository<FreebieTransport> _freebieTransportRepository;
        private readonly IRepository<SmsTemplate> _smsTemplateRepository;
        private readonly IRepository<EmailTemplate> _emailTemplateRepository;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<PreviousJobStatus> _previousJobStatusRepository;
        private readonly IRepository<FinanceOption> _financeOptionRepository;
        private readonly IRepository<DepositOption> _depositOptionRepository;
        private readonly IRepository<PaymentOption> _paymentOptionRepository;
        private readonly IRepository<PromotionUser> _PromotionUsersRepository;
        private readonly IRepository<Promotion, int> _lookup_promotionRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<ProductItemLocation> _productiteamlocationRepository;
        private readonly IRepository<InstallerInvoice> _installerInvoiceRepository;
        private readonly IRepository<InstInvoiceHistory> _InstInvoiceHistoryRepository;
        private readonly IRepository<InstallerInvoice> _jobInstallerInvoiceRepository;
        private readonly IRepository<LeadStatus, int> _lookup_leadStatusRepository;
        private readonly IRepository<InvoicePayment> _lookup_InvoicepaymentRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IRepository<JobApprovalFileHistory> _JobApprovalFileHistoryRepository;
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<InvoiceImportData> _InvoiceImportDataRepository;
        private readonly IRepository<Jobwarranty> _JobwarrantyRepository;
        public JobsAppService(IRepository<Job> jobRepository
            , IJobsExcelExporter jobsExcelExporter
            , IRepository<JobType, int> lookup_jobTypeRepository
            , IRepository<JobStatus, int> lookup_jobStatusRepository
            , IRepository<RoofType, int> lookup_roofTypeRepository
            , IRepository<RoofAngle, int> lookup_roofAngleRepository
            , IRepository<ElecDistributor, int> lookup_elecDistributorRepository
            , IRepository<Lead, int> lookup_leadRepository
            , IRepository<ElecRetailer, int> lookup_elecRetailerRepository
            , IRepository<PaymentOption, int> lookup_paymentOptionRepository
            , IRepository<DepositOption, int> lookup_depositOptionRepository
            , IRepository<MeterUpgrade, int> lookup_meterUpgradeRepository
            , IRepository<MeterPhase, int> lookup_meterPhaseRepository
            , IRepository<PromotionOffer, int> lookup_promotionOfferRepository
            , IRepository<HouseType, int> lookup_houseTypeRepository
            , IRepository<FinanceOption, int> lookup_financeOptionRepository
            , IRepository<JobProductItem> jobProductItemRepository
            , IRepository<JobPromotion> jobPromotionRepository
            , IRepository<JobVariation> jobVariationRepository
            , IRepository<STCZoneRating> stcZoneRatingRepository
            , IRepository<STCPostalCode> stcPostalCodeRepository
            , IRepository<STCYearWiseRate> stcYearWiseRateRepository
            , IRepository<Variation> variationRepository
            , IRepository<User, long> userRepository
            , IRepository<LeadActivityLog> leadactivityRepository
            , IRepository<InstallerDetail> installerDetailRepository
            , IRepository<InstallerAvailability> installerAvailabilityRepository
            , IRepository<JobCancellationReason> jobCancellationReasonReasonRepository
            , IRepository<HoldReason> jobHoldReasonRepository
            , IRepository<InvoicePayment> invoicePaymentRepository
            , IRepository<Quotation> quotationRepository
            , IRepository<Tenant> tenantRepository
            , IRepository<OrganizationUnit, long> organizationUnitRepository
            , IRepository<ExtendOrganizationUnit, long> extendOrganizationUnitRepository
            , IRepository<Warehouselocation> warehouselocationRepository
            , IRepository<PVDStatus, int> PVDStatusRepository
            , UserManager userManager
            , IAppNotifier appNotifier
            , IRepository<ProductItem> productItemRepository
            , IRepository<Document> documentRepository
            , IRepository<JobRefund> jobRefundRepository
            , IRepository<UserTeam> userTeamRepository
            , IRepository<DocumentType, int> lookup_documentTypeRepository
            , ILeadsExcelExporter leadsExcelExporter
            , IRepository<LeadSource, int> lookup_leadSourceRepository
            , IRepository<FreebieTransport> freebieTransportRepository
            , IRepository<SmsTemplate> smsTemplateRepository
            , IRepository<EmailTemplate> emailTemplateRepository
            , IApplicationSettingsAppService applicationSettings
            , IEmailSender emailSender
            , IRepository<PreviousJobStatus> previousJobStatusRepository
            , IRepository<FinanceOption> financeOptionRepository
            , IRepository<DepositOption> depositOptionRepository
            , IRepository<PaymentOption> paymentOptionRepository
            , IRepository<PromotionUser> PromotionUsersRepository
            , IRepository<Promotion, int> lookup_promotionRepository,
            IRepository<Role> roleRepository,
            IRepository<UserRole, long> userRoleRepository
            , ITimeZoneConverter timeZoneConverter,
           IRepository<ProductItemLocation> productiteamlocationRepository,
           IRepository<InstallerInvoice> installerInvoiceRepository,
           IRepository<InstallerInvoice> jobInstallerInvoiceRepository,
           IRepository<LeadStatus, int> lookup_leadStatusRepository,
           IRepository<InvoicePayment> lookup_InvoicepaymentRepository,
           IRepository<InstInvoiceHistory> InstInvoiceHistoryRepository,
           ITempFileCacheManager tempFileCacheManager,
           IWebHostEnvironment env, IRepository<JobApprovalFileHistory> JobApprovalFileHistoryRepository,
           IRepository<Team> teamRepository,
           IRepository<InvoiceImportData> InvoiceImportDataRepository,
           IRepository<Jobwarranty>  JobwarrantyRepository
            )
        {
            _env = env;
            _tempFileCacheManager = tempFileCacheManager;
            _jobRepository = jobRepository;
            _jobsExcelExporter = jobsExcelExporter;
            _lookup_jobTypeRepository = lookup_jobTypeRepository;
            _lookup_jobStatusRepository = lookup_jobStatusRepository;
            _lookup_roofTypeRepository = lookup_roofTypeRepository;
            _lookup_roofAngleRepository = lookup_roofAngleRepository;
            _lookup_elecDistributorRepository = lookup_elecDistributorRepository;
            _lookup_leadRepository = lookup_leadRepository;
            _lookup_elecRetailerRepository = lookup_elecRetailerRepository;
            _lookup_paymentOptionRepository = lookup_paymentOptionRepository;
            _lookup_depositOptionRepository = lookup_depositOptionRepository;
            _lookup_meterUpgradeRepository = lookup_meterUpgradeRepository;
            _lookup_meterPhaseRepository = lookup_meterPhaseRepository;
            _lookup_promotionOfferRepository = lookup_promotionOfferRepository;
            _lookup_houseTypeRepository = lookup_houseTypeRepository;
            _lookup_financeOptionRepository = lookup_financeOptionRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _jobPromotionRepository = jobPromotionRepository;
            _jobVariationRepository = jobVariationRepository;
            _stcZoneRatingRepository = stcZoneRatingRepository;
            _stcPostalCodeRepository = stcPostalCodeRepository;
            _stcYearWiseRateRepository = stcYearWiseRateRepository;
            _variationRepository = variationRepository;
            _userRepository = userRepository;
            _leadactivityRepository = leadactivityRepository;
            _installerDetailRepository = installerDetailRepository;
            _installerAvailabilityRepository = installerAvailabilityRepository;
            _jobCancellationReasonReasonRepository = jobCancellationReasonReasonRepository;
            _jobHoldReasonRepository = jobHoldReasonRepository;
            _invoicePaymentRepository = invoicePaymentRepository;
            _quotationRepository = quotationRepository;
            _tenantRepository = tenantRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _extendOrganizationUnitRepository = extendOrganizationUnitRepository;
            _warehouselocationRepository = warehouselocationRepository;
            _pVDStatusRepository = PVDStatusRepository;
            _userManager = userManager;
            _appNotifier = appNotifier;
            _productItemRepository = productItemRepository;
            _documentRepository = documentRepository;
            _jobRefundRepository = jobRefundRepository;
            _userTeamRepository = userTeamRepository;
            _lookup_documentTypeRepository = lookup_documentTypeRepository;
            _leadsExcelExporter = leadsExcelExporter;
            _lookup_leadSourceRepository = lookup_leadSourceRepository;
            _freebieTransportRepository = freebieTransportRepository;
            _smsTemplateRepository = smsTemplateRepository;
            _emailTemplateRepository = emailTemplateRepository;
            _applicationSettings = applicationSettings;
            _emailSender = emailSender;
            _previousJobStatusRepository = previousJobStatusRepository;
            _financeOptionRepository = financeOptionRepository;
            _depositOptionRepository = depositOptionRepository;
            _paymentOptionRepository = paymentOptionRepository;
            _PromotionUsersRepository = PromotionUsersRepository;
            _lookup_promotionRepository = lookup_promotionRepository;
            _roleRepository = roleRepository;
            _userRoleRepository = userRoleRepository;
            _timeZoneConverter = timeZoneConverter;
            _productiteamlocationRepository = productiteamlocationRepository;
            _installerInvoiceRepository = installerInvoiceRepository;
            _jobInstallerInvoiceRepository = jobInstallerInvoiceRepository;
            _lookup_leadStatusRepository = lookup_leadStatusRepository;
            _lookup_InvoicepaymentRepository = lookup_InvoicepaymentRepository;
            _InstInvoiceHistoryRepository = InstInvoiceHistoryRepository;
            _JobApprovalFileHistoryRepository = JobApprovalFileHistoryRepository;
            _teamRepository = teamRepository;
            _InvoiceImportDataRepository = InvoiceImportDataRepository;
            _JobwarrantyRepository = JobwarrantyRepository;
        }

        //Application Tracker
        public async Task<PagedResultDto<GetJobForViewDto>> GetAll(GetAllJobsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            var Doclist = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 2).Select(e => e.JobId).ToList();
            //var jobidstatus = input.testfilter != null ? input.testfilter : null;
            var filteredJobs = _jobRepository.GetAll()
                               .Include(e => e.LeadFk)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.projectFilter), e => false || e.JobNumber.Contains(input.projectFilter) || e.LeadFk.Mobile.Contains(input.projectFilter) || e.LeadFk.Email.Contains(input.projectFilter) || e.LeadFk.CompanyName.Contains(input.projectFilter))
                              .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.State == input.stateNameFilter)
                              .WhereIf(input.elecDistributorId != 0, e => e.ElecDistributorId == input.elecDistributorId)
                              .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
                              //.WhereIf(input.testfilter != null && input.testfilter.Count() > 0, e => input.testfilter.Contains((int)e.JobStatusId))
                              .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
                              .WhereIf(input.FinanceOptionNameFilter == "Application" && SDate != null && EDate != null, e => e.DistApplied.Value.AddHours(10).Date >= SDate.Value.Date && e.DistApplied.Value.AddHours(10).Date <= EDate.Value.Date)
                              .WhereIf(input.FinanceOptionNameFilter == "Exp" && SDate != null && EDate != null, e => e.DistExpiryDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DistExpiryDate.Value.AddHours(10).Date <= EDate.Value.Date)
                              .WhereIf(input.FinanceOptionNameFilter == "Dep" && SDate != null && EDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                              .WhereIf(input.FinanceOptionNameFilter == "Active" && SDate != null && EDate != null, e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
                              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                              .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                              .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                              .WhereIf(input.applicationstatus == 1, e => e.ApplicationRefNo != null)
                              .WhereIf(input.applicationstatus == 2, e => e.ApplicationRefNo == null)
                              .WhereIf(input.applicationstatus == 3, e => e.DistExpiryDate >= DateTime.UtcNow && e.DistExpiryDate <= DateTime.UtcNow.AddDays(30) && (e.JobStatusId >= 4 && e.JobStatusId <= 7) && e.MeterPhaseId != 0 && e.MeterPhaseId != null && e.PeakMeterNo != null && e.NMINumber != null && Doclist.Contains(e.Id) && e.ApplicationRefNo != null)
                              .WhereIf(input.applicationstatus == 4, e => e.JobStatusId >= 4 && (e.MeterPhaseId == 0 || e.MeterPhaseId == null || e.PeakMeterNo == null || string.IsNullOrEmpty(e.NMINumber) || !Doclist.Contains(e.Id)))
                              .WhereIf(input.applicationstatus != 4, e => e.JobStatusId >= 4 && e.MeterPhaseId != 0 && e.MeterPhaseId != null && e.PeakMeterNo != null && !string.IsNullOrEmpty(e.NMINumber) && Doclist.Contains(e.Id))
                              .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);

            var pagedAndFilteredJobs = filteredJobs
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var jobs = from o in pagedAndFilteredJobs
                       join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                       from s1 in j1.DefaultIfEmpty()

                       join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                       from s2 in j2.DefaultIfEmpty()

                           //join o3 in _lookup_roofTypeRepository.GetAll() on o.RoofTypeId equals o3.Id into j3
                           //from s3 in j3.DefaultIfEmpty()

                           //join o4 in _lookup_roofAngleRepository.GetAll() on o.RoofAngleId equals o4.Id into j4
                           //from s4 in j4.DefaultIfEmpty()

                       join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                       from s5 in j5.DefaultIfEmpty()

                       join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                       from s6 in j6.DefaultIfEmpty()

                       join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                       from s7 in j7.DefaultIfEmpty()

                           //join o8 in _lookup_paymentOptionRepository.GetAll() on o.PaymentOptionId equals o8.Id into j8
                           //from s8 in j8.DefaultIfEmpty()

                           //join o9 in _lookup_depositOptionRepository.GetAll() on o.DepositOptionId equals o9.Id into j9
                           //from s9 in j9.DefaultIfEmpty()

                           //join o10 in _lookup_meterUpgradeRepository.GetAll() on o.MeterUpgradeId equals o10.Id into j10
                           //from s10 in j10.DefaultIfEmpty()

                           //join o11 in _lookup_meterPhaseRepository.GetAll() on o.MeterPhaseId equals o11.Id into j11
                           //from s11 in j11.DefaultIfEmpty()

                           //join o12 in _lookup_promotionOfferRepository.GetAll() on o.PromotionOfferId equals o12.Id into j12
                           //from s12 in j12.DefaultIfEmpty()

                           //join o13 in _lookup_houseTypeRepository.GetAll() on o.HouseTypeId equals o13.Id into j13
                           //from s13 in j13.DefaultIfEmpty()

                           //join o14 in _lookup_financeOptionRepository.GetAll() on o.FinanceOptionId equals o14.Id into j14
                           //from s14 in j14.DefaultIfEmpty()

                       select new GetJobForViewDto()
                       {
                           Job = new JobDto
                           {
                               RegPlanNo = o.RegPlanNo,
                               LotNumber = o.LotNumber,
                               Suburb = o.Suburb,
                               State = o.State,
                               UnitNo = o.UnitNo,
                               UnitType = o.UnitType,
                               NMINumber = o.NMINumber,
                               Id = o.Id,
                               ApplicationRefNo = o.ApplicationRefNo,
                               DistAppliedDate = o.DistAppliedDate,
                               ExpiryDate = o.ExpiryDate,
                               Notes = o.Note,
                               InstallerNotes = o.InstallerNotes,
                               JobNumber = o.JobNumber,
                               MeterNumber = o.PeakMeterNo,
                               LeadId = o.LeadId,
                               OldSystemDetails = o.OldSystemDetails,
                               PostalCode = o.PostalCode,
                               StreetName = o.StreetName,
                               StreetNo = o.StreetNo,
                               StreetType = o.StreetType,
                               ElecDistributorId = o.ElecDistributorId,
                               ElecRetailerId = o.ElecRetailerId,
                               JobStatusId = o.JobStatusId,
                               JobTypeId = o.JobTypeId,
                               Address = o.Address,
                               SmsSend = o.SmsSend,
                               SmsSendDate = o.SmsSendDate,
                               EmailSend = o.EmailSend,
                               EmailSendDate = o.EmailSendDate,
                               DistApplied = o.DistApplied,
                               DistApproveDate = o.DistApproveDate,
                               DistExpiryDate = o.DistExpiryDate,
                               CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault()
                           },
                           JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                           JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                           ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                           LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                           Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                           Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                           ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
                           MeterPhadeIDs = _lookup_meterPhaseRepository.GetAll().Where(e => e.Id == o.MeterPhaseId).Any(),
                           PeakMeterNos = !string.IsNullOrEmpty(o.PeakMeterNo),
                           Nminumbers = !string.IsNullOrEmpty(o.NMINumber),
                           ActivityReminderTime = _leadactivityRepository.GetAll().Where(e => e.SectionId == 1 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                           ActivityDescription = _leadactivityRepository.GetAll().Where(e => e.SectionId == 1 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           ActivityComment = _leadactivityRepository.GetAll().Where(e => e.SectionId == 1 && e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                       };

            var totalCount = await filteredJobs.CountAsync();

            return new PagedResultDto<GetJobForViewDto>(
                totalCount,
                await jobs.ToListAsync()
            );
        }
        private int?[] getValuesFromNameValue(List<NameValue<int>> nameValues)
        {

            int?[] arr = new int?[nameValues.Count];


            if (nameValues != null)
            {
                int i = 0;

                foreach (var e in nameValues)
                {
                    arr[i] = e.Value;
                    i++;
                }
            }

            return arr;
        }
        //Application Tracker Excel Export
        public async Task<FileDto> getApplicationTrackerToExcel(GetAllJobsForExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredJobs = _jobRepository.GetAll()
                              .WhereIf(!string.IsNullOrWhiteSpace(input.projectFilter), e => false || e.JobNumber.Contains(input.projectFilter))
                              .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.State == input.stateNameFilter)
                              .WhereIf(input.elecDistributorId != 0, e => e.ElecDistributorId == input.elecDistributorId)
                              .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
                              .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
                              .WhereIf(input.FinanceOptionNameFilter == "Application" && SDate != null && EDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                              .WhereIf(input.FinanceOptionNameFilter == "Exp" && SDate != null && EDate != null, e => e.DistExpiryDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DistExpiryDate.Value.AddHours(10).Date <= EDate.Value.Date)
                              .WhereIf(input.FinanceOptionNameFilter == "Dep" && SDate != null && EDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                              .WhereIf(input.FinanceOptionNameFilter == "Active" && SDate != null && EDate != null, e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
                              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                              .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                              .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                              .WhereIf(input.applicationstatus != 0 && input.applicationstatus == 1, e => e.ApplicationRefNo != null)
                              .WhereIf(input.applicationstatus != 0 && input.applicationstatus == 2, e => e.ApplicationRefNo == null)
                              .Where(e => e.JobStatusId >= 4)
                              .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);

            var query = (from o in filteredJobs

                         join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                         from s5 in j5.DefaultIfEmpty()

                         join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                         from s6 in j6.DefaultIfEmpty()

                         join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                         from s7 in j7.DefaultIfEmpty()

                         select new GetJobForViewDto()
                         {
                             Job = new JobDto
                             {
                                 RegPlanNo = o.RegPlanNo,
                                 LotNumber = o.LotNumber,
                                 Suburb = o.Suburb,
                                 State = o.State,
                                 UnitNo = o.UnitNo,
                                 UnitType = o.UnitType,
                                 NMINumber = o.NMINumber,
                                 Id = o.Id,
                                 ApplicationRefNo = o.ApplicationRefNo,
                                 DistAppliedDate = o.DistAppliedDate,
                                 Notes = o.Note,
                                 InstallerNotes = o.InstallerNotes,
                                 JobNumber = o.JobNumber,
                                 MeterNumber = o.PeakMeterNo,
                                 LeadId = o.LeadId,
                                 OldSystemDetails = o.OldSystemDetails,
                                 PostalCode = o.PostalCode,
                                 StreetName = o.StreetName,
                                 StreetNo = o.StreetNo,
                                 StreetType = o.StreetType,
                                 ElecDistributorId = o.ElecDistributorId,
                                 ElecRetailerId = o.ElecRetailerId,
                                 JobStatusId = o.JobStatusId,
                                 JobTypeId = o.JobTypeId,
                                 Address = o.Address,
                                 CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault()
                             },
                             JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                             JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                             ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                             LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                             ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
                         });

            var applicationtrackerListDtos = await query.ToListAsync();

            return _leadsExcelExporter.ApplicationTrackerExportToFile(applicationtrackerListDtos);
        }

        //FinanceTracker
        public async Task<PagedResultDto<GetJobForViewDto>> GetAllForJobFinanceTracker(GetAllJobsInput input)
        {
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var Deposites = _invoicePaymentRepository.GetAll().Select(e => e.JobId).Distinct().ToList();

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobTypeFk)
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.LeadFk)
                        .Include(e => e.FinanceOptionFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.FinancePurchaseNo.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.LeadFk.Suburb == input.SuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.LeadFk.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.LeadFk.Mobile.Contains(input.Mobile))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Email), e => e.LeadFk.Email.Contains(input.Email))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.IsVerified == 3, e => e.FinanceDocumentVerified == 3)
                        .WhereIf(input.IsVerified == 2, e => e.FinanceDocumentVerified == 2 || e.FinanceDocumentVerified == null)
                        .WhereIf(input.IsVerified == 1, e => e.FinanceDocumentVerified == 1)
                        .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit)
                        .Where(e => e.PaymentOptionId != null && e.PaymentOptionId != 1)
                        .Where(e => Deposites.Contains(e.Id));

            var pagedAndFilteredJobs = filteredJobs
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var jobs = from o in pagedAndFilteredJobs
                       join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                       from s1 in j1.DefaultIfEmpty()

                       join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                       from s2 in j2.DefaultIfEmpty()

                           //join o3 in _lookup_roofTypeRepository.GetAll() on o.RoofTypeId equals o3.Id into j3
                           //from s3 in j3.DefaultIfEmpty()

                           //join o4 in _lookup_roofAngleRepository.GetAll() on o.RoofAngleId equals o4.Id into j4
                           //from s4 in j4.DefaultIfEmpty()

                       join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                       from s5 in j5.DefaultIfEmpty()

                       join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                       from s6 in j6.DefaultIfEmpty()

                       join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                       from s7 in j7.DefaultIfEmpty()

                           //join o8 in _lookup_paymentOptionRepository.GetAll() on o.PaymentOptionId equals o8.Id into j8
                           //from s8 in j8.DefaultIfEmpty()

                           //join o9 in _lookup_depositOptionRepository.GetAll() on o.DepositOptionId equals o9.Id into j9
                           //from s9 in j9.DefaultIfEmpty()

                           //join o10 in _lookup_meterUpgradeRepository.GetAll() on o.MeterUpgradeId equals o10.Id into j10
                           //from s10 in j10.DefaultIfEmpty()

                           //join o11 in _lookup_meterPhaseRepository.GetAll() on o.MeterPhaseId equals o11.Id into j11
                           //from s11 in j11.DefaultIfEmpty()

                           //join o12 in _lookup_promotionOfferRepository.GetAll() on o.PromotionOfferId equals o12.Id into j12
                           //from s12 in j12.DefaultIfEmpty()

                           //join o13 in _lookup_houseTypeRepository.GetAll() on o.HouseTypeId equals o13.Id into j13
                           //from s13 in j13.DefaultIfEmpty()

                           //join o14 in _lookup_financeOptionRepository.GetAll() on o.FinanceOptionId equals o14.Id into j14
                           //from s14 in j14.DefaultIfEmpty()

                       select new GetJobForViewDto()
                       {
                           Job = new JobDto
                           {
                               RegPlanNo = o.RegPlanNo,
                               LotNumber = o.LotNumber,
                               Address = o.Address,
                               Suburb = o.Suburb,
                               State = o.State,
                               UnitNo = o.UnitNo,
                               UnitType = o.UnitType,
                               NMINumber = o.NMINumber,
                               Id = o.Id,
                               ApplicationRefNo = o.ApplicationRefNo,
                               DistAppliedDate = o.DistAppliedDate,
                               ExpiryDate = o.ExpiryDate,
                               Notes = o.Note,
                               InstallerNotes = o.InstallerNotes,
                               JobNumber = o.JobNumber,
                               MeterNumber = o.PeakMeterNo,
                               OldSystemDetails = o.OldSystemDetails,
                               PostalCode = o.PostalCode,
                               StreetName = o.StreetName,
                               StreetNo = o.StreetNo,
                               StreetType = o.StreetType,
                               LeadId = o.LeadId,
                               ElecDistributorId = o.ElecDistributorId,
                               ElecRetailerId = o.ElecRetailerId,
                               JobStatusId = o.JobStatusId,
                               JobTypeId = o.JobTypeId,
                               FinancePurchaseNo = o.FinancePurchaseNo,
                               FinanceDocumentVerified = o.FinanceDocumentVerified,
                               FinanceSmsSend = o.FinanceSmsSend,
                               FinanceSmsSendDate = o.FinanceSmsSendDate,
                               FinanceEmailSend = o.FinanceEmailSend,
                               FinanceEmailSendDate = o.FinanceEmailSendDate,
                               CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault()
                           },
                           JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                           JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                           ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                           LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                           Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                           Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                           ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
                           ActivityReminderTime = _leadactivityRepository.GetAll().Where(e => e.SectionId == 3 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                           ActivityDescription = _leadactivityRepository.GetAll().Where(e => e.SectionId == 3 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           ActivityComment = _leadactivityRepository.GetAll().Where(e => e.SectionId == 3 && e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                       };

            var totalCount = await filteredJobs.CountAsync();

            return new PagedResultDto<GetJobForViewDto>(
                totalCount,
                await jobs.ToListAsync()
            );
        }

        //Finance Tracker Excel Export
        public async Task<FileDto> getFinanaceTrackerToExcel(GetAllJobsForExcelInput input)
        {

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobTypeFk)
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.LeadFk)
                        .Include(e => e.FinanceOptionFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.FinancePurchaseNo.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.LeadFk.Mobile.Contains(input.Mobile))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Email), e => e.LeadFk.Email.Contains(input.Email))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
                        .WhereIf(input.IsVerified != null, e => e.FinanceDocumentVerified == input.IsVerified)
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit)
                        .Where(e => e.PaymentOptionId != null && e.PaymentOptionId != 1);

            var query = (from o in filteredJobs

                         join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                         from s5 in j5.DefaultIfEmpty()

                         join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                         from s6 in j6.DefaultIfEmpty()

                         select new GetJobForViewDto()
                         {
                             Job = new JobDto
                             {
                                 RegPlanNo = o.RegPlanNo,
                                 LotNumber = o.LotNumber,
                                 Address = o.Address,
                                 Suburb = o.Suburb,
                                 State = o.State,
                                 UnitNo = o.UnitNo,
                                 UnitType = o.UnitType,
                                 NMINumber = o.NMINumber,
                                 Id = o.Id,
                                 ApplicationRefNo = o.ApplicationRefNo,
                                 DistAppliedDate = o.DistAppliedDate,
                                 Notes = o.Note,
                                 InstallerNotes = o.InstallerNotes,
                                 JobNumber = o.JobNumber,
                                 MeterNumber = o.PeakMeterNo,
                                 OldSystemDetails = o.OldSystemDetails,
                                 PostalCode = o.PostalCode,
                                 StreetName = o.StreetName,
                                 StreetNo = o.StreetNo,
                                 StreetType = o.StreetType,
                                 LeadId = o.LeadId,
                                 ElecDistributorId = o.ElecDistributorId,
                                 ElecRetailerId = o.ElecRetailerId,
                                 JobStatusId = o.JobStatusId,
                                 JobTypeId = o.JobTypeId,
                                 FinancePurchaseNo = o.FinancePurchaseNo,
                                 CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault()
                             },
                             JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                             JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                             ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                             LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                             Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                             Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                         });

            var financetrackerListDtos = await query.ToListAsync();

            return _leadsExcelExporter.FinanaceTrackerExportToFile(financetrackerListDtos);
        }

        //JobActiveTracker
        public async Task<PagedResultDto<GetJobForViewDto>> GetAllForJobActiveTracker(GetAllJobsInput input)
        {
            
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();


			var MeterPhoto = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 3).Select(e => e.JobId).Distinct().ToList();
			var Deposites = _invoicePaymentRepository.GetAll().Select(e => e.JobId).Distinct().ToList();
			var SignedQuote = _quotationRepository.GetAll().Where(e => e.IsSigned == true).Select(e => e.JobId).Distinct().ToList();
			var doc = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 5).Select(e => e.JobId).Distinct().ToList();
			
			var filteredJobs = _jobRepository.GetAll()
						.Include(e => e.JobTypeFk)
						.Include(e => e.JobStatusFk)
						.Include(e => e.RoofTypeFk)
						.Include(e => e.RoofAngleFk)
						.Include(e => e.ElecDistributorFk)
						.Include(e => e.LeadFk)
						.Include(e => e.ElecRetailerFk)
						.Include(e => e.PaymentOptionFk)
						.Include(e => e.DepositOptionFk)
						//.Include(e => e.MeterUpgradeFk)
						//.Include(e => e.MeterPhaseFk)
						.Include(e => e.PromotionOfferFk)
						.Include(e => e.HouseTypeFk)
						.Include(e => e.FinanceOptionFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofTypeNameFilter), e => e.RoofTypeFk != null && e.RoofTypeFk.Name == input.RoofTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofAngleNameFilter), e => e.RoofAngleFk != null && e.RoofAngleFk.Name == input.RoofAngleNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecDistributorNameFilter), e => e.ElecDistributorFk != null && e.ElecDistributorFk.Name == input.ElecDistributorNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecRetailerNameFilter), e => e.ElecRetailerFk != null && e.ElecRetailerFk.Name == input.ElecRetailerNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentOptionNameFilter), e => e.PaymentOptionFk != null && e.PaymentOptionFk.Name == input.PaymentOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DepositOptionNameFilter), e => e.DepositOptionFk != null && e.DepositOptionFk.Name == input.DepositOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionOfferNameFilter), e => e.PromotionOfferFk != null && e.PromotionOfferFk.Name == input.PromotionOfferNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.HouseTypeNameFilter), e => e.HouseTypeFk != null && e.HouseTypeFk.Name == input.HouseTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
                        .WhereIf(input.paymentid != 0, e => e.PaymentOptionId == input.paymentid)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.applicationstatus == 1, e => e.ActiveDate != null && e.JobStatusId == 5)
                         .WhereIf(input.applicationstatus == 2, e => e.IsActive != true && e.JobStatusId == 4 && Deposites.Contains(e.Id) && (e.NMINumber != null && e.ElecRetailerId != 0 && e.ApplicationRefNo != null && e.MeterUpgradeId != null && e.MeterUpgradeId != 0 && MeterPhoto.Contains(e.Id) && (SignedQuote.Contains(e.Id) || doc.Contains(e.Id))) && (e.JobStatusId != 3 && e.JobStatusId != 8))
                        //.WhereIf(input.applicationstatus == 3, e => e.IsActive != true && e.JobStatusId == 4 && Deposites.Contains(e.Id) && (e.NMINumber == null || e.ElecRetailerId != 0 || e.ApplicationRefNo == null || e.MeterUpgradeId == null || e.MeterUpgradeId == 0 || !MeterPhoto.Contains(e.Id) || ( e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false ) || (!SignedQuote.Contains(e.Id) || !doc.Contains(e.Id))) && (e.JobStatusId != 3 && e.JobStatusId != 8))
                        .WhereIf(input.applicationstatus == 3, e => e.IsActive != true && e.IsActive != null &&   Deposites.Contains(e.Id) && (e.NMINumber == null || e.ElecRetailerId != 0 || e.ApplicationRefNo == null || e.MeterUpgradeId == null || e.MeterUpgradeId == 0 || !MeterPhoto.Contains(e.Id) || (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) || (!SignedQuote.Contains(e.Id) || !doc.Contains(e.Id))) && (e.JobStatusId <5 && e.JobStatusId != 3 && e.JobStatusId != 1))
                         //.WhereIf(input.applicationstatus != 3 && input.applicationstatus != 2)
                        .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == j.LeadId).Select(e => e.OrganizationId).FirstOrDefault()) == input.OrganizationUnit && j.DepositeRecceivedDate != null);

            var pagedAndFilteredJobs = filteredJobs
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var jobs = from o in pagedAndFilteredJobs
                       join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                       from s1 in j1.DefaultIfEmpty()

                       join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                       from s2 in j2.DefaultIfEmpty()

                       join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                       from s5 in j5.DefaultIfEmpty()

                       join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                       from s6 in j6.DefaultIfEmpty()

                       join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                       from s7 in j7.DefaultIfEmpty()

                       select new GetJobForViewDto()
                       {
                           Job = new JobDto
                           {
                               RegPlanNo = o.RegPlanNo,
                               LotNumber = o.LotNumber,
                               Suburb = o.Suburb,
                               State = o.State,
                               UnitNo = o.UnitNo,
                               UnitType = o.UnitType,
                               NMINumber = o.NMINumber,
                               Id = o.Id,
                               ApplicationRefNo = o.ApplicationRefNo,
                               DistAppliedDate = o.DistAppliedDate,
                               Notes = o.Note,
                               InstallerNotes = o.InstallerNotes,
                               JobNumber = o.JobNumber,
                               MeterNumber = o.PeakMeterNo,
                               OldSystemDetails = o.OldSystemDetails,
                               PostalCode = o.PostalCode,
                               StreetName = o.StreetName,
                               StreetNo = o.StreetNo,
                               StreetType = o.StreetType,
                               LeadId = o.LeadId,
                               ElecDistributorId = o.ElecDistributorId,
                               ElecRetailerId = o.ElecRetailerId,
                               JobStatusId = o.JobStatusId,
                               JobTypeId = o.JobTypeId,
                               JobActiveSmsSend = o.JobActiveSmsSend,
                               JobActiveSmsSendDate = o.JobActiveSmsSendDate,
                               JobActiveEmailSend = o.JobActiveEmailSend,
                               JobActiveEmailSendDate = o.JobActiveEmailSendDate,
                               CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault()
                           },

                           JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                           JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                           ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                           LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                           Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                           Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                           ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
                           NMIs = !string.IsNullOrEmpty(o.NMINumber),
                           ApprovedReferenceNumbers = !string.IsNullOrEmpty(o.ApplicationRefNo),
                           MeterUpdgrades = _lookup_meterUpgradeRepository.GetAll().Where(e => e.Id == o.MeterUpgradeId).Any(),
                           MeterBoxPhotos = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 3 && e.JobId == o.Id).Any(),
                           SignedQuotes = _quotationRepository.GetAll().Where(e => e.JobId == o.Id && e.IsSigned == true).Any() || _documentRepository.GetAll().Where(e => e.JobId == o.Id && e.DocumentTypeId == 5).Any(),
                           Deposites = _invoicePaymentRepository.GetAll().Where(e => e.JobId == o.Id).Any(),
                           QuoteAcceptDates = _quotationRepository.GetAll().Where(e => e.JobId == o.Id && e.IsSigned == true && e.QuoteAcceptDate != null).Any(),
                           Finances = o.PaymentOptionId == 1 ? true : o.FinanceDocumentVerified == 1 ? true : false,
                           ActivityReminderTime = _leadactivityRepository.GetAll().Where(e => e.SectionId == 5 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                           ActivityDescription = _leadactivityRepository.GetAll().Where(e => e.SectionId == 5 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           ActivityComment = _leadactivityRepository.GetAll().Where(e => e.SectionId == 5 && e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           //payementoptionids = o.PaymentOptionId,

                       };

            var totalCount = await filteredJobs.CountAsync();

            return new PagedResultDto<GetJobForViewDto>(
                totalCount,
                await jobs.ToListAsync()
            );
        }

        //Active Tracker Excel Export
        public async Task<FileDto> getJobActiveTrackerToExcel(GetAllJobsForExcelInput input)
        {

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobTypeFk)
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.RoofTypeFk)
                        .Include(e => e.RoofAngleFk)
                        .Include(e => e.ElecDistributorFk)
                        .Include(e => e.LeadFk)
                        .Include(e => e.ElecRetailerFk)
                        .Include(e => e.PaymentOptionFk)
                        .Include(e => e.DepositOptionFk)
                        //.Include(e => e.MeterUpgradeFk)
                        //.Include(e => e.MeterPhaseFk)
                        .Include(e => e.PromotionOfferFk)
                        .Include(e => e.HouseTypeFk)
                        .Include(e => e.FinanceOptionFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofTypeNameFilter), e => e.RoofTypeFk != null && e.RoofTypeFk.Name == input.RoofTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofAngleNameFilter), e => e.RoofAngleFk != null && e.RoofAngleFk.Name == input.RoofAngleNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecDistributorNameFilter), e => e.ElecDistributorFk != null && e.ElecDistributorFk.Name == input.ElecDistributorNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecRetailerNameFilter), e => e.ElecRetailerFk != null && e.ElecRetailerFk.Name == input.ElecRetailerNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentOptionNameFilter), e => e.PaymentOptionFk != null && e.PaymentOptionFk.Name == input.PaymentOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DepositOptionNameFilter), e => e.DepositOptionFk != null && e.DepositOptionFk.Name == input.DepositOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionOfferNameFilter), e => e.PromotionOfferFk != null && e.PromotionOfferFk.Name == input.PromotionOfferNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.HouseTypeNameFilter), e => e.HouseTypeFk != null && e.HouseTypeFk.Name == input.HouseTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
                        .WhereIf(input.applicationstatus != 0 && input.applicationstatus == 1, e => e.ActiveDate != null)
                        .WhereIf(input.applicationstatus != 0 && input.applicationstatus == 2, e => e.ActiveDate == null)
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .Where(e => e.IsActive == true)
                        .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == j.LeadId).Select(e => e.OrganizationId).FirstOrDefault()) == input.OrganizationUnit);

            var query = (from o in filteredJobs

                         join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                         from s5 in j5.DefaultIfEmpty()

                         join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                         from s6 in j6.DefaultIfEmpty()

                         join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                         from s7 in j7.DefaultIfEmpty()

                         select new GetJobForViewDto()
                         {
                             Job = new JobDto
                             {
                                 RegPlanNo = o.RegPlanNo,
                                 LotNumber = o.LotNumber,
                                 Suburb = o.Suburb,
                                 State = o.State,
                                 UnitNo = o.UnitNo,
                                 UnitType = o.UnitType,
                                 NMINumber = o.NMINumber,
                                 Id = o.Id,
                                 ApplicationRefNo = o.ApplicationRefNo,
                                 ///  DistAppliedDate = o.DistAppliedDate,
                                 Notes = o.Note,
                                 InstallerNotes = o.InstallerNotes,
                                 JobNumber = o.JobNumber,
                                 MeterNumber = o.PeakMeterNo,
                                 OldSystemDetails = o.OldSystemDetails,
                                 PostalCode = o.PostalCode,
                                 StreetName = o.StreetName,
                                 StreetNo = o.StreetNo,
                                 StreetType = o.StreetType,
                                 LeadId = o.LeadId,
                                 ElecDistributorId = o.ElecDistributorId,
                                 ElecRetailerId = o.ElecRetailerId,
                                 JobStatusId = o.JobStatusId,
                                 JobTypeId = o.JobTypeId,
                                 CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault()
                             },

                             JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                             JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                             ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                             LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                             Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                             Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                             ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString()
                         });

            var activetrackerListDtos = await query.ToListAsync();

            return _leadsExcelExporter.JobActiveTrackerExportToFile(activetrackerListDtos);
        }

        //JobGrid
        public async Task<PagedResultDto<GetJobForViewDto>> GetAllJobGrid(GetAllJobsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = _userTeamRepository.GetAll().Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }


            var PanelDetails = new List<int?>();
            if (input.PanelModel != null)
            {
                var Panel = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Model.Contains(input.PanelModel)).Select(e => e.Id).FirstOrDefault();
                PanelDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Panel).Select(e => e.JobId).ToList();
            }

            var InvertDetails = new List<int?>();
            if (input.InvertModel != null)
            {
                var Inverter = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Model.Contains(input.InvertModel)).Select(e => e.Id).FirstOrDefault();
                InvertDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Inverter).Select(e => e.JobId).ToList();
            }
            var Panels = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1).Select(e => e.Id).ToList();
            var Inverts = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2).Select(e => e.Id).ToList();

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobTypeFk)
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.RoofTypeFk)
                        .Include(e => e.RoofAngleFk)
                        .Include(e => e.ElecDistributorFk)
                        .Include(e => e.LeadFk)
                        .Include(e => e.ElecRetailerFk)
                        .Include(e => e.PaymentOptionFk)
                        .Include(e => e.DepositOptionFk)
                        .Include(e => e.PromotionOfferFk)
                        .Include(e => e.HouseTypeFk)
                        .Include(e => e.FinanceOptionFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofTypeNameFilter), e => e.RoofTypeFk != null && e.RoofTypeFk.Name == input.RoofTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofAngleNameFilter), e => e.RoofAngleFk != null && e.RoofAngleFk.Name == input.RoofAngleNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecDistributorNameFilter), e => e.ElecDistributorFk != null && e.ElecDistributorFk.Name == input.ElecDistributorNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecRetailerNameFilter), e => e.ElecRetailerFk != null && e.ElecRetailerFk.Name == input.ElecRetailerNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentOptionNameFilter), e => e.PaymentOptionFk != null && e.PaymentOptionFk.Name == input.PaymentOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DepositOptionNameFilter), e => e.DepositOptionFk != null && e.DepositOptionFk.Name == input.DepositOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionOfferNameFilter), e => e.PromotionOfferFk != null && e.PromotionOfferFk.Name == input.PromotionOfferNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.HouseTypeNameFilter), e => e.HouseTypeFk != null && e.HouseTypeFk.Name == input.HouseTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.PostalCode, input.PostalCodeFrom) >= 0)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.PostalCode, input.PostalCodeTo) <= 0)
                        .WhereIf(input.paymentid != 0, e => e.PaymentOptionId == input.paymentid)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
                        .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
                        .WhereIf(input.JobDateFilter == "CreationDate" && SDate != null && EDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "FirstDeposite" && SDate != null && EDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "DepositeReceived" && SDate != null && EDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "Active" && SDate != null && EDate != null, e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "NextFollowUp" && SDate != null && EDate != null, e => e.NextFollowUpDate.Value.AddHours(10).Date >= SDate.Value.Date && e.NextFollowUpDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallBook" && SDate != null && EDate != null, e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallComplete" && SDate != null && EDate != null, e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => e.JobStatusId != 1)
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PanelModel), e => PanelDetails.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(input.Readytoactive != null && input.Readytoactive == 1, e => e.IsActive == true && (e.JobStatusId == 1 || e.JobStatusId == 4))
                        .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemEndRange != null && input.SystemEndRange != 0, e => e.SystemCapacity >= input.SystemStrtRange && e.SystemCapacity <= input.SystemEndRange)
                        .WhereIf(input.SystemStrtRange != null && input.SystemStrtRange != 0 && input.SystemEndRange == null && input.SystemEndRange == 0, e => e.SystemCapacity == input.SystemStrtRange)
                        .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemStrtRange == null && input.SystemStrtRange == 0, e => e.SystemCapacity == input.SystemEndRange)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadFk.LeadSourceId))
                        //.WhereIf(input.LeadSourceIdFilter != 0 && input.LeadSourceIdFilter != null, e => e.LeadFk.LeadSourceId == input.LeadSourceIdFilter)
                        .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == j.LeadId).Select(e => e.OrganizationId).FirstOrDefault()) == input.OrganizationUnit);

            var pagedAndFilteredJobs = filteredJobs
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var jobids = filteredJobs.Select(e => e.Id).ToList();

            var jobs = from o in pagedAndFilteredJobs
                       join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                       from s1 in j1.DefaultIfEmpty()

                       join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                       from s2 in j2.DefaultIfEmpty()

                           //join o3 in _lookup_roofTypeRepository.GetAll() on o.RoofTypeId equals o3.Id into j3
                           //from s3 in j3.DefaultIfEmpty()

                           //join o4 in _lookup_roofAngleRepository.GetAll() on o.RoofAngleId equals o4.Id into j4
                           //from s4 in j4.DefaultIfEmpty()

                       join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                       from s5 in j5.DefaultIfEmpty()

                       join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                       from s6 in j6.DefaultIfEmpty()

                       join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                       from s7 in j7.DefaultIfEmpty()

                           //join o8 in _lookup_paymentOptionRepository.GetAll() on o.PaymentOptionId equals o8.Id into j8
                           //from s8 in j8.DefaultIfEmpty()

                           //join o9 in _lookup_depositOptionRepository.GetAll() on o.DepositOptionId equals o9.Id into j9
                           //from s9 in j9.DefaultIfEmpty()

                           //join o10 in _lookup_meterUpgradeRepository.GetAll() on o.MeterUpgradeId equals o10.Id into j10
                           //from s10 in j10.DefaultIfEmpty()

                           //join o11 in _lookup_meterPhaseRepository.GetAll() on o.MeterPhaseId equals o11.Id into j11
                           //from s11 in j11.DefaultIfEmpty()

                           //join o12 in _lookup_promotionOfferRepository.GetAll() on o.PromotionOfferId equals o12.Id into j12
                           //from s12 in j12.DefaultIfEmpty()

                           //join o13 in _lookup_houseTypeRepository.GetAll() on o.HouseTypeId equals o13.Id into j13
                           //from s13 in j13.DefaultIfEmpty()

                           //join o14 in _lookup_financeOptionRepository.GetAll() on o.FinanceOptionId equals o14.Id into j14
                           //from s14 in j14.DefaultIfEmpty()

                       select new GetJobForViewDto()
                       {
                           Job = new JobDto
                           {
                               RegPlanNo = o.RegPlanNo,
                               LotNumber = o.LotNumber,
                               Address = o.Address,
                               Suburb = o.Suburb,
                               State = o.State,
                               UnitNo = o.UnitNo,
                               UnitType = o.UnitType,
                               NMINumber = o.NMINumber,
                               Id = o.Id,
                               ApplicationRefNo = o.ApplicationRefNo,
                               DistAppliedDate = o.DistAppliedDate,
                               Notes = o.Note,
                               InstallerNotes = o.InstallerNotes,
                               JobNumber = o.JobNumber,
                               MeterNumber = o.PeakMeterNo,
                               OldSystemDetails = o.OldSystemDetails,
                               PostalCode = o.PostalCode,
                               StreetName = o.StreetName,
                               StreetNo = o.StreetNo,
                               StreetType = o.StreetType,
                               ElecDistributorId = o.ElecDistributorId,
                               ElecRetailerId = o.ElecRetailerId,
                               JobStatusId = o.JobStatusId,
                               LeadId = o.LeadId,
                               JobTypeId = o.JobTypeId,
                               InstallationDate = o.InstallationDate,
                               JobGridSmsSend = o.JobGridSmsSend,
                               JobGridSmsSendDate = o.JobGridSmsSendDate,
                               JobGridEmailSend = o.JobGridEmailSend,
                               JobGridEmailSendDate = o.JobGridEmailSendDate,
                               CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                               DepositeRecceivedDate = o.DepositeRecceivedDate
                           },

                           JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                           JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                           ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                           LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                           Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                           Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                           ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
                           LastComment = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(x => x.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           LastCommentDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(x => x.Id).Select(e => e.CreationTime).FirstOrDefault(),
                           LeadSourceName = _lookup_leadSourceRepository.GetAll().Where(e => (e.Id == s6.LeadSourceId)).Select(e => e.Name).FirstOrDefault(),

                           TotalNoOfPanel = _jobProductItemRepository.GetAll().Where(e => Panels.Contains((int)e.ProductItemId) && jobids.Contains((int)e.JobId)).Select(e => e.Quantity).Sum(),
                           TotalNoOfInvert = _jobProductItemRepository.GetAll().Where(e => Inverts.Contains((int)e.ProductItemId) && jobids.Contains((int)e.JobId)).Select(e => e.Quantity).Sum(),
                           TotalSystemCapacity = filteredJobs.Select(e => e.SystemCapacity).Sum(),
                           TotalCost = filteredJobs.Select(e => e.TotalCost).Sum(),
                           DepositeReceived = filteredJobs.Where(e => e.JobStatusId == 4).Count(),
                           Active = filteredJobs.Where(e => e.JobStatusId == 5).Count(),
                           InstallJob = filteredJobs.Where(e => e.JobStatusId == 8).Count(),
                           ActivityReminderTime = _leadactivityRepository.GetAll().Where(e => e.SectionId == 8 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                           ActivityDescription = _leadactivityRepository.GetAll().Where(e => e.SectionId == 8 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           ActivityComment = _leadactivityRepository.GetAll().Where(e => e.SectionId == 8 && e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                       };

            var totalCount = await filteredJobs.CountAsync();

            return new PagedResultDto<GetJobForViewDto>(
                totalCount,
                await jobs.ToListAsync()
            );
        }

        //Job Tracker Excel Export
        public async Task<FileDto> getJobGirdTrackerToExcel(GetAllJobsForExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var PanelDetails = new List<int?>();
            if (input.PanelModel != null)
            {
                var Panel = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Model.Contains(input.PanelModel)).Select(e => e.Id).FirstOrDefault();
                PanelDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Panel).Select(e => e.JobId).ToList();
            }

            var InvertDetails = new List<int?>();
            if (input.InvertModel != null)
            {
                var Inverter = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Model.Contains(input.InvertModel)).Select(e => e.Id).FirstOrDefault();
                InvertDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Inverter).Select(e => e.JobId).ToList();
            }
            var Panels = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1).Select(e => e.Id).ToList();
            var Inverts = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2).Select(e => e.Id).ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = _userTeamRepository.GetAll().Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobTypeFk)
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.RoofTypeFk)
                        .Include(e => e.RoofAngleFk)
                        .Include(e => e.ElecDistributorFk)
                        .Include(e => e.LeadFk)
                        .Include(e => e.ElecRetailerFk)
                        .Include(e => e.PaymentOptionFk)
                        .Include(e => e.DepositOptionFk)
                        .Include(e => e.PromotionOfferFk)
                        .Include(e => e.HouseTypeFk)
                        .Include(e => e.FinanceOptionFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofTypeNameFilter), e => e.RoofTypeFk != null && e.RoofTypeFk.Name == input.RoofTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofAngleNameFilter), e => e.RoofAngleFk != null && e.RoofAngleFk.Name == input.RoofAngleNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecDistributorNameFilter), e => e.ElecDistributorFk != null && e.ElecDistributorFk.Name == input.ElecDistributorNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecRetailerNameFilter), e => e.ElecRetailerFk != null && e.ElecRetailerFk.Name == input.ElecRetailerNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentOptionNameFilter), e => e.PaymentOptionFk != null && e.PaymentOptionFk.Name == input.PaymentOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DepositOptionNameFilter), e => e.DepositOptionFk != null && e.DepositOptionFk.Name == input.DepositOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionOfferNameFilter), e => e.PromotionOfferFk != null && e.PromotionOfferFk.Name == input.PromotionOfferNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.HouseTypeNameFilter), e => e.HouseTypeFk != null && e.HouseTypeFk.Name == input.HouseTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
                        .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
                        .WhereIf(input.JobDateFilter == "CreationDate" && SDate != null && EDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "FirstDeposite" && SDate != null && EDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "DepositeReceived" && SDate != null && EDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "Active" && SDate != null && EDate != null, e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "FollowUp" && SDate != null && EDate != null, e => e.NextFollowUpDate.Value.AddHours(10).Date >= SDate.Value.Date && e.NextFollowUpDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "Install" && SDate != null && EDate != null, e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallComplete" && SDate != null && EDate != null, e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => e.JobStatusId != 1)
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PanelModel), e => PanelDetails.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        .WhereIf(input.Readytoactive != null && input.Readytoactive == 1, e => e.IsActive == true && (e.JobStatusId == 1 || e.JobStatusId == 4))
                        .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemEndRange != null && input.SystemEndRange != 0, e => e.SystemCapacity >= input.SystemStrtRange && e.SystemCapacity <= input.SystemEndRange)
                        .WhereIf(input.SystemStrtRange != null && input.SystemStrtRange != 0 && input.SystemEndRange == null && input.SystemEndRange == 0, e => e.SystemCapacity == input.SystemStrtRange)
                        .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemStrtRange == null && input.SystemStrtRange == 0, e => e.SystemCapacity == input.SystemEndRange)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadFk.LeadSourceId))
                        //.WhereIf(input.LeadSourceIdFilter != 0 && input.LeadSourceIdFilter != null, e => e.LeadFk.LeadSourceId == input.LeadSourceIdFilter)
                        .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == j.LeadId).Select(e => e.OrganizationId).FirstOrDefault()) == input.OrganizationUnit);

            var jobs = filteredJobs.Select(e => e.Id).ToList();
            var jobproductiteamlist = _jobProductItemRepository.GetAll().Where(e => jobs.Contains((int)e.JobId)).Select(e => e.ProductItemId).ToList();
            var query = (from o in filteredJobs

                         join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o3 in _lookup_roofTypeRepository.GetAll() on o.RoofTypeId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                         from s5 in j5.DefaultIfEmpty()

                         join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                         from s6 in j6.DefaultIfEmpty()

                         join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                         from s7 in j7.DefaultIfEmpty()

                         join o8 in _lookup_paymentOptionRepository.GetAll() on o.PaymentOptionId equals o8.Id into j8
                         from s8 in j8.DefaultIfEmpty()

                         join o13 in _lookup_houseTypeRepository.GetAll() on o.HouseTypeId equals o13.Id into j13
                         from s13 in j13.DefaultIfEmpty()

                         join o14 in _lookup_financeOptionRepository.GetAll() on o.FinanceOptionId equals o14.Id into j14
                         from s14 in j14.DefaultIfEmpty()



                         select new GetJobForViewDto()
                         {
                             Job = new JobDto
                             {
                                 RegPlanNo = o.RegPlanNo,
                                 LotNumber = o.LotNumber,
                                 Address = o.Address,
                                 Suburb = o.Suburb,
                                 State = o.State,
                                 UnitNo = o.UnitNo,
                                 UnitType = o.UnitType,
                                 NMINumber = o.NMINumber,
                                 Id = o.Id,
                                 ApplicationRefNo = o.ApplicationRefNo,
                                 DistAppliedDate = o.DistAppliedDate,
                                 Notes = o.Note,
                                 InstallerNotes = o.InstallerNotes,
                                 JobNumber = o.JobNumber,
                                 MeterNumber = o.MeterNumber,
                                 OldSystemDetails = o.OldSystemDetails,
                                 PostalCode = o.PostalCode,
                                 StreetName = o.StreetName,
                                 StreetNo = o.StreetNo,
                                 StreetType = o.StreetType,
                                 ElecDistributorId = o.ElecDistributorId,
                                 ElecRetailerId = o.ElecRetailerId,
                                 JobStatusId = o.JobStatusId,
                                 LeadId = o.LeadId,
                                 JobTypeId = o.JobTypeId,
                                 InstallationDate = o.InstallationDate,
                                 TotalCost = o.TotalCost,
                                 SystemCapacity = o.SystemCapacity,
                                 DistExpiryDate = o.DepositeRecceivedDate,
                                 CreationTime = o.CreationTime,
                                 CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                                 ManualQuote = o.ManualQuote,
                                 Note = o.Note
                             },

                             FinanceWith = s8 == null || s8.Name == null ? "" : s8.Name.ToString(),
                             HouseType = s13 == null || s13.Name == null ? "" : s13.Name.ToString(),
                             RoofType = s3 == null || s3.Name == null ? "" : s3.Name.ToString(),
                             JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                             JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                             ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                             LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                             Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                             Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                             ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
                             LastComment = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(x => x.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                             LastCommentDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(x => x.Id).Select(e => e.CreationTime).FirstOrDefault(),
                             LeadSourceName = _lookup_leadSourceRepository.GetAll().Where(e => (e.Id == s6.LeadSourceId)).Select(e => e.Name).FirstOrDefault(),
                             Team = _userTeamRepository.GetAll().Where(e => (e.UserId == s6.AssignToUserID)).Select(e => e.TeamFk.Name).FirstOrDefault(),
                             DateDiff = Convert.ToInt32((o.CreationTime - s6.CreationTime).TotalDays),
                             LeadCreationDate = s6.CreationTime,
                             TotalNoOfPanel = _jobProductItemRepository.GetAll().Where(e => e.JobId == o.Id && Panels.Contains((int)e.ProductItemId)).Select(e => e.Quantity).Sum(),
                             TotalNoOfInvert = _jobProductItemRepository.GetAll().Where(e => e.JobId == o.Id && Inverts.Contains((int)e.ProductItemId)).Select(e => e.Quantity).Sum(),
                             Panels = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Id == (_jobProductItemRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.ProductItemId).FirstOrDefault())).Select(e => e.Name).FirstOrDefault(),
                             Inverters = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == (_jobProductItemRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.ProductItemId).FirstOrDefault())).Select(e => e.Name).FirstOrDefault(),
                             //Inverters = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && jobproductiteamlist.Contains(e.Id)).OrderByDescending(e => e.Id).Select(e => e.Name).FirstOrDefault(),
                             LeadStatus = _lookup_leadStatusRepository.GetAll().Where(e => e.Id == s6.LeadStatusId).Select(e => e.Status).FirstOrDefault(),
                             FirstDepDate = _lookup_InvoicepaymentRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.InvoicePayDate).FirstOrDefault(),
                             FirstDepAmmount = _lookup_InvoicepaymentRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.InvoicePayTotal).FirstOrDefault(),

                         });

            var jobgridrListDtos = await query.ToListAsync();

            return _leadsExcelExporter.JobGridExportToFile(jobgridrListDtos);
        }

        /// <summary>
        /// Get Data For Edit Job From Lead Id
        /// </summary>
        /// <param name="input">Lead Id</param>
        /// <returns></returns>
        public async Task<GetJobForEditOutput> GetJobByLeadId(int LeadId)
        {
            var output = new GetJobForEditOutput();
            try
            {
                var job = await _jobRepository.GetAll().Where(x => x.LeadId == LeadId).FirstOrDefaultAsync();
                output = new GetJobForEditOutput { Job = ObjectMapper.Map<CreateOrEditJobDto>(job) };
                if (output.Job == null)
                {
                    return output;
                }
                if (output.Job.JobTypeId != null)
                {
                    var _lookupJobType = await _lookup_jobTypeRepository.FirstOrDefaultAsync((int)output.Job.JobTypeId);
                    output.JobTypeName = _lookupJobType?.Name?.ToString();
                }

                if (output.Job.JobStatusId != null)
                {
                    var _lookupJobStatus = await _lookup_jobStatusRepository.FirstOrDefaultAsync((int)output.Job.JobStatusId);
                    output.JobStatusName = _lookupJobStatus?.Name?.ToString();
                }

                if (output.Job.RoofTypeId != null)
                {
                    var _lookupRoofType = await _lookup_roofTypeRepository.FirstOrDefaultAsync((int)output.Job.RoofTypeId);
                    output.RoofTypeName = _lookupRoofType?.Name?.ToString();
                }

                if (output.Job.RoofAngleId != null)
                {
                    var _lookupRoofAngle = await _lookup_roofAngleRepository.FirstOrDefaultAsync((int)output.Job.RoofAngleId);
                    output.RoofAngleName = _lookupRoofAngle?.Name?.ToString();
                }

                if (output.Job.ElecDistributorId != null)
                {
                    var _lookupElecDistributor = await _lookup_elecDistributorRepository.FirstOrDefaultAsync((int)output.Job.ElecDistributorId);
                    output.ElecDistributorName = _lookupElecDistributor?.Name?.ToString();
                }

                if (output.Job.LeadId != null)
                {
                    var _lookupLead = await _lookup_leadRepository.FirstOrDefaultAsync((int)output.Job.LeadId);
                    output.LeadCompanyName = _lookupLead?.CompanyName?.ToString();
                }

                if (output.Job.ElecRetailerId != null)
                {
                    var _lookupElecRetailer = await _lookup_elecRetailerRepository.FirstOrDefaultAsync((int)output.Job.ElecRetailerId);
                    output.ElecRetailerName = _lookupElecRetailer?.Name?.ToString();
                }

                if (output.Job.PaymentOptionId != null)
                {
                    var _lookupPaymentOption = await _lookup_paymentOptionRepository.FirstOrDefaultAsync((int)output.Job.PaymentOptionId);
                    output.PaymentOptionName = _lookupPaymentOption?.Name?.ToString();
                }

                if (output.Job.DepositOptionId != null)
                {
                    var _lookupDepositOption = await _lookup_depositOptionRepository.FirstOrDefaultAsync((int)output.Job.DepositOptionId);
                    output.DepositOptionName = _lookupDepositOption?.Name?.ToString();
                }

                if (output.Job.MeterUpgradeId != null)
                {
                    var _lookupMeterUpgrade = await _lookup_meterUpgradeRepository.FirstOrDefaultAsync((int)output.Job.MeterUpgradeId);
                    output.MeterUpgradeName = _lookupMeterUpgrade?.Name?.ToString();
                }

                if (output.Job.MeterPhaseId != null)
                {
                    var _lookupMeterPhase = await _lookup_meterPhaseRepository.FirstOrDefaultAsync((int)output.Job.MeterPhaseId);
                    output.MeterPhaseName = _lookupMeterPhase?.Name?.ToString();
                }

                if (output.Job.PromotionOfferId != null)
                {
                    var _lookupPromotionOffer = await _lookup_promotionOfferRepository.FirstOrDefaultAsync((int)output.Job.PromotionOfferId);
                    output.PromotionOfferName = _lookupPromotionOffer?.Name?.ToString();
                }
            }
            catch (Exception ex)
            { };
            return output;
        }

        /// <summary>
        /// Get Data For Edit Job
        /// </summary>
        /// <param name="input">Job Id</param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Jobs_Edit, AppPermissions.Pages_ApplicationTracker_AddNotes, AppPermissions.Pages_FinanceTracker_AddDetails
            , AppPermissions.Pages_InvoiceTracker_Verify, AppPermissions.Pages_FreebiesTracker_AddTrackingNumber, AppPermissions.Pages_JobActiveTracker_AddDetails, AppPermissions.Pages_RefundTracker_Refund, AppPermissions.Pages_STCTracker_Edit)]
        public async Task<GetJobForEditOutput> GetJobForEdit(EntityDto input)
        {
            var job = await _jobRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetJobForEditOutput { Job = ObjectMapper.Map<CreateOrEditJobDto>(job) };

            output.TotalCost = output.Job.TotalCost;
            output.FinanceApplicationDates = output.Job.FinanceApplicationDate;
            output.DepPaid = _invoicePaymentRepository.GetAll().Where(e => e.JobId == job.Id).Select(e => e.InvoicePayTotal).Sum();
            output.BalanceDue = output.Job.TotalCost - output.DepPaid;
            output.AssignToUserID = _lookup_leadRepository.GetAll().Where(e => e.Id == job.LeadId).Select(e => e.AssignToUserID).FirstOrDefault();
            output.LeadCompanyName = _lookup_leadRepository.GetAll().Where(e => e.Id == job.LeadId).Select(e => e.CompanyName).FirstOrDefault();
            output.LeadEmail = _lookup_leadRepository.GetAll().Where(e => e.Id == job.LeadId).Select(e => e.Email).FirstOrDefault();
            if (output.Job.JobTypeId != null)
            {
                var _lookupJobType = await _lookup_jobTypeRepository.FirstOrDefaultAsync((int)output.Job.JobTypeId);
                output.JobTypeName = _lookupJobType?.Name?.ToString();
            }

            if (output.Job.JobStatusId != null)
            {
                var _lookupJobStatus = await _lookup_jobStatusRepository.FirstOrDefaultAsync((int)output.Job.JobStatusId);
                output.JobStatusName = _lookupJobStatus?.Name?.ToString();
            }

            if (output.Job.RoofTypeId != null)
            {
                var _lookupRoofType = await _lookup_roofTypeRepository.FirstOrDefaultAsync((int)output.Job.RoofTypeId);
                output.RoofTypeName = _lookupRoofType?.Name?.ToString();
            }

            if (output.Job.RoofAngleId != null)
            {
                var _lookupRoofAngle = await _lookup_roofAngleRepository.FirstOrDefaultAsync((int)output.Job.RoofAngleId);
                output.RoofAngleName = _lookupRoofAngle?.Name?.ToString();
            }

            if (output.Job.ElecDistributorId != null)
            {
                var _lookupElecDistributor = await _lookup_elecDistributorRepository.FirstOrDefaultAsync((int)output.Job.ElecDistributorId);
                output.ElecDistributorName = _lookupElecDistributor?.Name?.ToString();
            }

            if (output.Job.LeadId != null)
            {
                var _lookupLead = await _lookup_leadRepository.FirstOrDefaultAsync((int)output.Job.LeadId);
                output.LeadCompanyName = _lookupLead?.CompanyName?.ToString();
            }

            if (output.Job.ElecRetailerId != null)
            {
                var _lookupElecRetailer = await _lookup_elecRetailerRepository.FirstOrDefaultAsync((int)output.Job.ElecRetailerId);
                output.ElecRetailerName = _lookupElecRetailer?.Name?.ToString();
            }

            if (output.Job.PaymentOptionId != null)
            {
                var _lookupPaymentOption = await _lookup_paymentOptionRepository.FirstOrDefaultAsync((int)output.Job.PaymentOptionId);
                output.PaymentOptionName = _lookupPaymentOption?.Name?.ToString();
            }

            if (output.Job.DepositOptionId != null)
            {
                var _lookupDepositOption = await _lookup_depositOptionRepository.FirstOrDefaultAsync((int)output.Job.DepositOptionId);
                output.DepositOptionName = _lookupDepositOption?.Name?.ToString();
            }

            if (output.Job.MeterUpgradeId != null)
            {
                var _lookupMeterUpgrade = await _lookup_meterUpgradeRepository.FirstOrDefaultAsync((int)output.Job.MeterUpgradeId);
                output.MeterUpgradeName = _lookupMeterUpgrade?.Name?.ToString();
            }

            if (output.Job.MeterPhaseId != null)
            {
                var _lookupMeterPhase = await _lookup_meterPhaseRepository.FirstOrDefaultAsync((int)output.Job.MeterPhaseId);
                output.MeterPhaseName = _lookupMeterPhase?.Name?.ToString();
            }

            if (output.Job.PromotionOfferId != null)
            {
                var _lookupPromotionOffer = await _lookup_promotionOfferRepository.FirstOrDefaultAsync((int)output.Job.PromotionOfferId);
                output.PromotionOfferName = _lookupPromotionOffer?.Name?.ToString();
            }

            if (output.Job.HouseTypeId != null)
            {
                var _lookupHouseType = await _lookup_houseTypeRepository.FirstOrDefaultAsync((int)output.Job.HouseTypeId);
                output.HouseTypeName = _lookupHouseType?.Name?.ToString();
            }

            if (output.Job.FinanceOptionId != null)
            {
                var _lookupFinanceOption = await _lookup_financeOptionRepository.FirstOrDefaultAsync((int)output.Job.FinanceOptionId);
                output.FinanceOptionName = _lookupFinanceOption?.Name?.ToString();
            }
            if (output.Job.Applicationfeespaid == null || output.Job.Applicationfeespaid == 0)
            {
                output.Job.Applicationfeespaid = 0;
                output.Job.Applicationfeespaidstatus = "No";
            }
            else if (output.Job.Applicationfeespaid == 1)
            {

                output.Job.Applicationfeespaidstatus = "Yes";
            }
            output.RefferedJobId = output.Job.RefferedJobId;
            output.RefferedJobStatusId = output.Job.RefferedJobStatusId;
            output.AccountName = output.Job.AccountName;output.AccountNo = output.Job.AccountNo;
            output.BsbNo = output.Job.BsbNo;
            output.ReferralAmount = output.Job.ReferralAmount;
            output.refferedJobName = "";
            output.ReferralPayDate = output.Job.ReferralPayDate;
            output.ReferralPayment = output.Job.ReferralPayment;
            output.BankReferenceNo = output.Job.BankReferenceNo;
            output.ReferralRemark = output.Job.ReferralRemark;
            return output;
        }


        /// <summary>
        /// Create Or Edit Job 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task CreateOrEdit(CreateOrEditJobDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        /// <summary>
        /// Create Job
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Jobs_Create)]
        protected virtual async Task Create(CreateOrEditJobDto input)
        {
            var Lead = _lookup_leadRepository.GetAll().Where(e => e.Id == input.LeadId).FirstOrDefault();
            Lead.LeadStatusId = 6;
            await _lookup_leadRepository.UpdateAsync(Lead);
            var ProjectCode = _extendOrganizationUnitRepository.GetAll().Where(e => e.Id == Lead.OrganizationId).Select(e => e.ProjectId).FirstOrDefault();
            var OrganizationCode = _organizationUnitRepository.GetAll().Where(e => e.Id == Lead.OrganizationId).Select(e => e.Code).FirstOrDefault();
            var PrevJobNumber = (from j in _jobRepository.GetAll()
                                 where (j.LeadFk.OrganizationId == Lead.OrganizationId)
                                 select new { j.JobNumber, j.Id })
                                .OrderByDescending(e => e.Id).FirstOrDefault();
            if (input.PaymentOptionId == 1)
            {
                input.FinancePurchaseNo = null;
                input.DepositOptionId = null;
                input.FinanceOptionId = null;
            }
            var JobNumber = ProjectCode;
            if (PrevJobNumber != null)
            {
                if (PrevJobNumber.JobNumber != null)
                {
                    var JobNumberSplit = PrevJobNumber.JobNumber.Substring(PrevJobNumber.JobNumber.Length - 6); //PrevJobNumber.JobNumber.Split("_");
                    JobNumber = Convert.ToString(Convert.ToInt32(JobNumberSplit) + 1);
                }
            }

            if (JobNumber.Length <= 6)
            {
                JobNumber = JobNumber.ToString().PadLeft(6, '0');
            }

            var job = ObjectMapper.Map<Job>(input);
            if (AbpSession.TenantId != null)
            {
                job.TenantId = (int)AbpSession.TenantId;
            }
            job.Address = input.UnitNo + " " + input.UnitType + " " + input.StreetNo + " " + input.StreetName + " " + input.StreetType;
            job.JobNumber = OrganizationCode + JobNumber;
            job.JobStatusId = 1;
            var jobid = await _jobRepository.InsertAndGetIdAsync(job);

            if (input.JobProductItems != null)
            {
                foreach (var jobproduct in input.JobProductItems)
                {
                    var jobProductItem = ObjectMapper.Map<JobProductItem>(jobproduct);
                    jobProductItem.JobId = jobid;
                    if (AbpSession.TenantId != null)
                    {
                        jobProductItem.TenantId = (int)AbpSession.TenantId;
                    }
                    _jobProductItemRepository.Insert(jobProductItem);
                }
            }

            if (input.JobPromotion != null)
            {
                foreach (var jobpromotion in input.JobPromotion)
                {
                    var jobpromo = ObjectMapper.Map<JobPromotion>(jobpromotion);
                    jobpromo.JobId = jobid;
                    if (AbpSession.TenantId != null)
                    {
                        jobpromo.TenantId = (int)AbpSession.TenantId;
                    }
                    _jobPromotionRepository.Insert(jobpromo);
                }
            }

            if (input.JobVariation != null)
            {
                foreach (var jobvariation in input.JobVariation)
                {
                    var jobvar = ObjectMapper.Map<JobVariation>(jobvariation);
                    jobvar.JobId = jobid;
                    if (AbpSession.TenantId != null)
                    {
                        jobvar.TenantId = (int)AbpSession.TenantId;
                    }
                    _jobVariationRepository.Insert(jobvar);
                }
            }


            PreviousJobStatus jobStstus = new PreviousJobStatus();
            jobStstus.JobId = job.Id;
            if (AbpSession.TenantId != null)
            {
                jobStstus.TenantId = (int)AbpSession.TenantId;
            }
            jobStstus.CurrentID = (int)job.JobStatusId;
            jobStstus.PreviousId = 0;
            await _previousJobStatusRepository.InsertAsync(jobStstus);

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 10;
            leadactivity.SectionId = 0;
            leadactivity.ActionNote = "Job Created";
            leadactivity.LeadId = Convert.ToInt32(input.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);

        }

        /// <summary>
        /// Update Job
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Jobs_Edit, AppPermissions.Pages_ApplicationTracker_AddNotes, AppPermissions.Pages_FinanceTracker_AddDetails
            , AppPermissions.Pages_InvoiceTracker_Verify, AppPermissions.Pages_FreebiesTracker_AddTrackingNumber, AppPermissions.Pages_JobActiveTracker_AddDetails, AppPermissions.Pages_RefundTracker_Refund, AppPermissions.Pages_STCTracker_Edit)]
        protected virtual async Task Update(CreateOrEditJobDto input)
        {
            var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);
            
            if (input.refferedJobName != null)
            {
                int firstCommaIndex = input.refferedJobName.IndexOf('|');

                string firstPart = input.refferedJobName.Substring(0, firstCommaIndex);

                var jbid = _jobRepository.GetAll().Where(e => e.JobNumber == firstPart).FirstOrDefault();
                 
                input.RefferedJobId = Convert.ToInt32(jbid.Id);
                input.RefferedJobStatusId =  jbid.JobStatusId;

            }
            if (input.ReferralPayDate != null)
            { input.ReferralPayment = true; }
            ObjectMapper.Map(input, job);
            job.Address = input.UnitNo + " " + input.UnitType + " " + input.StreetNo + " " + input.StreetName + " " + input.StreetType;
            if(input.PaymentOptionId == 1)
            {
                job.FinancePurchaseNo = null;
                job.DepositOptionId = null;
                job.FinanceOptionId = null;
            } 
            await _jobRepository.UpdateAsync(job);
            await JobActive(job.Id);
            await JobDeposite(job.Id);

            if (input.JobProductItems != null)
            {
                if (input.JobProductItems[0].ProductItemId != null)
                {
                    _jobProductItemRepository.Delete(x => x.JobId == job.Id);
                }
                foreach (var jobproduct in input.JobProductItems)
                {
                    if (jobproduct.ProductItemId != null)
                    {
                        var jobProductItem = ObjectMapper.Map<JobProductItem>(jobproduct);
                        jobProductItem.JobId = job.Id;
                        if (AbpSession.TenantId != null)
                        {
                            jobProductItem.TenantId = (int)AbpSession.TenantId;
                        }
                        _jobProductItemRepository.Insert(jobProductItem);
                    }
                }
            }

            if (input.JobPromotion != null)
            {
                if (input.JobPromotion[0].PromotionMasterId != null)
                {
                    _jobPromotionRepository.Delete(x => x.JobId == job.Id);
                }
                foreach (var jobpromotion in input.JobPromotion)
                {
                    if (jobpromotion.PromotionMasterId != null)
                    {
                        var jobpromo = ObjectMapper.Map<JobPromotion>(jobpromotion);
                        jobpromo.JobId = job.Id;
                        if (AbpSession.TenantId != null)
                        {
                            jobpromo.TenantId = (int)AbpSession.TenantId;
                        }
                        _jobPromotionRepository.Insert(jobpromo);
                    }
                }
            }

            if (input.JobVariation != null)
            {
                if (input.JobVariation[0].VariationId != null)
                {
                    _jobVariationRepository.Delete(x => x.JobId == job.Id);
                }
                foreach (var jobvariation in input.JobVariation)
                {
                    if (jobvariation.VariationId != null)
                    {
                        var jobvar = ObjectMapper.Map<JobVariation>(jobvariation);
                        jobvar.JobId = job.Id;
                        if (AbpSession.TenantId != null)
                        {
                            jobvar.TenantId = (int)AbpSession.TenantId;
                        }
                        _jobVariationRepository.Insert(jobvar);
                    }
                }
            }

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 11;
            leadactivity.SectionId = 0;
            leadactivity.ActionNote = "Job Updated";
            leadactivity.LeadId = Convert.ToInt32(input.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        /// <summary>
        /// Update Job Data From Tracker
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Jobs_Edit, AppPermissions.Pages_ApplicationTracker_AddNotes, AppPermissions.Pages_FinanceTracker_AddDetails
            , AppPermissions.Pages_InvoiceTracker_Verify, AppPermissions.Pages_FreebiesTracker_AddTrackingNumber, AppPermissions.Pages_JobActiveTracker_AddDetails, AppPermissions.Pages_RefundTracker_Refund, AppPermissions.Pages_STCTracker_Edit)]
        public async Task UpdateDataFromTracker(CreateOrEditJobDto input)
        {
            var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);

            if (input.FileName != null)
            {
                var ByteArray = _tempFileCacheManager.GetFile(input.FileToken);

                if (ByteArray == null)
                {
                    throw new UserFriendlyException("There is no such file with the token: " + input.FileToken);
                }

                var ext = Path.GetExtension(input.FileName);
                //var storedFile = new BinaryObject(AbpSession.TenantId, byteArray);
                //await _binaryObjectManager.SaveAsync(storedFile);

                var Path1 = _env.WebRootPath;
                var MainFolder = Path1;
                MainFolder = MainFolder + "\\Documents";

                var JobNumber = _jobRepository.GetAll().Where(e => e.Id == input.Id).FirstOrDefault();
                var TenantName = _tenantRepository.GetAll().Where(e => e.Id == JobNumber.TenantId).Select(e => e.TenancyName).FirstOrDefault();

                string TenantPath = MainFolder + "\\" + TenantName;
                string JobPath = TenantPath + "\\" + JobNumber.JobNumber;
                //string JobPath = TenantPath;
                string SignaturePath = JobPath + "\\JobApprovalLetter";
                string filename = DateTime.Now.Ticks + "_" + "ApprovalLetter" + ext;

                var FinalFilePath = SignaturePath + "\\" + filename;

                if (System.IO.Directory.Exists(MainFolder))
                {
                    if (System.IO.Directory.Exists(TenantPath))
                    {
                        if (System.IO.Directory.Exists(JobPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(JobPath);
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(TenantPath);
                        if (System.IO.Directory.Exists(JobPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(JobPath);
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(MainFolder);
                    if (System.IO.Directory.Exists(TenantPath))
                    {
                        if (System.IO.Directory.Exists(JobPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(JobPath);
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(TenantPath);
                        if (System.IO.Directory.Exists(JobPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(JobPath);
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                    }
                }
                //var JobNumber = _jobJobRepository.GetAll().Where(e => e.Id == input.JobId).Select(e => e.JobNumber).FirstOrDefault();
                //var TenantName = _tenantRepository.GetAll().Where(e => e.Id == InstallerInvoice.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                //job.ApprovalLetter_Filename = filename;
                //job.ApprovalLetter_FilePath = "\\Documents" + "\\" + TenantName + "\\" + JobNumber.JobNumber + "\\" + "JobApprovalLetter" + "\\";

                input.ApprovalLetter_Filename = filename;
                input.ApprovalLetter_FilePath = "\\Documents" + "\\" + TenantName + "\\" + JobNumber.JobNumber + "\\" + "JobApprovalLetter" + "\\";

                if (job.ApprovalLetter_Filename != input.ApprovalLetter_Filename)
                {
                    JobApprovalFileHistory invoiceStatus = new JobApprovalFileHistory();
                    if (AbpSession.TenantId != null)
                    {
                        invoiceStatus.TenantId = (int)AbpSession.TenantId;
                    }
                    invoiceStatus.FieldName = "FileName";
                    invoiceStatus.PrevValue = job.ApprovalLetter_Filename;
                    invoiceStatus.CurValue = input.ApprovalLetter_Filename;
                    invoiceStatus.Action = "Edit";
                    invoiceStatus.LastmodifiedDateTime = DateTime.Now;
                    invoiceStatus.jobid = job.Id;
                    await _JobApprovalFileHistoryRepository.InsertAsync(invoiceStatus);
                }
                if (job.ApprovalLetter_FilePath != input.ApprovalLetter_FilePath)
                {
                    JobApprovalFileHistory invoiceStatus = new JobApprovalFileHistory();
                    if (AbpSession.TenantId != null)
                    {
                        invoiceStatus.TenantId = (int)AbpSession.TenantId;
                    }
                    invoiceStatus.FieldName = "FilePath";
                    invoiceStatus.PrevValue = job.ApprovalLetter_FilePath;
                    invoiceStatus.CurValue = input.ApprovalLetter_FilePath;
                    invoiceStatus.Action = "Edit";
                    invoiceStatus.LastmodifiedDateTime = DateTime.Now;
                    invoiceStatus.jobid = job.Id;
                    await _JobApprovalFileHistoryRepository.InsertAsync(invoiceStatus);
                }

                ObjectMapper.Map(input, job);
                //await _jobRepository.UpdateAsync(job);


            }
            else
            {
                ObjectMapper.Map(input, job);
                //await _jobRepository.UpdateAsync(job);

            }
            //ObjectMapper.Map(input, job);
            //await _jobRepository.UpdateAsync(job);
            await JobActive(job.Id);
            await JobDeposite(job.Id);


            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 11;
            leadactivity.SectionId = 0;
            leadactivity.ActionNote = "Job Data Updated From Tracker";
            leadactivity.LeadId = Convert.ToInt32(input.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        /// <summary>
        /// Update Application Tracker
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_ApplicationTracker_AddNotes)]
        public async Task UpdateApplicationTracker(CreateOrEditJobDto input)
        {
            var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, job);

            await _jobRepository.UpdateAsync(job);

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 11;
            leadactivity.SectionId = 0;
            leadactivity.ActionNote = "Job Application Details Updated";
            leadactivity.LeadId = Convert.ToInt32(input.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        /// <summary>
        /// Update Finance Tracker
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>		
        [AbpAuthorize(AppPermissions.Pages_FinanceTracker_AddDetails)]
        public async Task UpdateFinanceTracker(CreateOrEditJobDto input)
        {
            var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, job);

            await _jobRepository.UpdateAsync(job);

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 11;
            leadactivity.ActionNote = "Job Finance Details Updated";
            leadactivity.LeadId = Convert.ToInt32(input.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        [AbpAuthorize(AppPermissions.Pages_JobActiveTracker_AddDetails)]
        public async Task ActiveJob(CreateOrEditJobDto input)
        {
            var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);

            PreviousJobStatus jobStstus = new PreviousJobStatus();
            jobStstus.JobId = job.Id;
            if (AbpSession.TenantId != null)
            {
                jobStstus.TenantId = (int)AbpSession.TenantId;
            }
            if (input.ActiveDate != null)
            {
                jobStstus.CurrentID = 5;
            }
            else { jobStstus.CurrentID = 4; }

            jobStstus.PreviousId = (int)job.JobStatusId;
            await _previousJobStatusRepository.InsertAsync(jobStstus);

            ObjectMapper.Map(input, job);
            if (input.ActiveDate != null)
            {
                job.JobStatusId = 5;
            }
            else { job.JobStatusId = 4; }

            await _jobRepository.UpdateAsync(job);

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 14;
            leadactivity.SectionId = 0;
            leadactivity.ActionNote = "Job Status Change To Active";
            leadactivity.LeadId = Convert.ToInt32(input.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        [AbpAuthorize(AppPermissions.Pages_Jobs_Edit)]
        public async Task<JobInstallationDto> GetJobInstallationDetailsForEdit(EntityDto input)
        {
            var job = (await _jobRepository.GetAsync(input.Id)).MapTo<JobInstallationDto>();
            if (job.InstallationDate != null)
            {
                job.InstallationDate = job.InstallationDate;
            }
            return job;
        }

        public string GetJobStatus(EntityDto input)
        {
            var StatusId = _jobRepository.GetAll().Where(e => e.Id == input.Id).Select(e => e.JobStatusId).FirstOrDefault();

            return _lookup_jobStatusRepository.GetAll().Where(e => e.Id == StatusId).Select(e => e.Name).FirstOrDefault();
        }

        public async Task UpdateJobStatus(UpdateJobStatusDto input)
        {
            var job = await _jobRepository.GetAsync(input.Id);
            var JobStatusID = (int)job.JobStatusId;
            if (input.JobStatusId != 0)
            {
                PreviousJobStatus jobStstus = new PreviousJobStatus();
                jobStstus.JobId = job.Id;
                if (AbpSession.TenantId != null)
                {
                    jobStstus.TenantId = (int)AbpSession.TenantId;
                }
                jobStstus.CurrentID = (int)input.JobStatusId;
                jobStstus.PreviousId = (int)job.JobStatusId;
                await _previousJobStatusRepository.InsertAsync(jobStstus);
            }

            if (input.JobStatusId == 0) // No need to update Jobstatus. Only For Request of cancelation
            {
                job.JobCancelRequestReason = input.JobCancelRequestReason;
                job.IsJobCancelRequest = true;
                job.JobStatusId = JobStatusID;
                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 23;
                leadactivity.SectionId = 0;
                leadactivity.ActionNote = "Cancel Request";
                leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                leadactivity.CreatorUserId = AbpSession.UserId;
                await _leadactivityRepository.InsertAsync(leadactivity);

                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == UserDetail.Id).Select(e => e.TeamId).ToList();
                var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Distinct().ToList();

                var User_List = (from user in UserList
                                 join ur in _userRoleRepository.GetAll() on user.UserId equals ur.UserId into urJoined
                                 from ur in urJoined.DefaultIfEmpty()

                                 join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                 from us in usJoined.DefaultIfEmpty()

                                 where us != null && us.DisplayName == "Sales Manager"
                                 select user).Distinct();

                foreach (var item in User_List)
                {
                    var jobnumber = _jobRepository.GetAll().Where(e => e.Id == job.Id).Select(e => e.JobNumber).FirstOrDefault();
                    var user = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).Select(e => e.UserName).FirstOrDefault();
                    var canreq = "Cancel Request is Apply for" + jobnumber + "By" + user + ".";

                    var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == item.UserId).FirstOrDefault();
                    string msg = string.Format(canreq);
                    await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);

                }
            }

            if (input.JobStatusId == 2)
            {
                job.JobHoldReason = input.JobHoldReason;
                job.NextFollowUpDate = input.NextFollowUpDate;
                job.JobStatusId = input.JobStatusId;
                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 14;
                leadactivity.SectionId = 0;
                leadactivity.ActionNote = "Job Status Change To On Hold";
                leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
            if (input.JobStatusId == 3)
            {
                job.JobCancelReason = input.JobCancelReason;
                job.JobCancelReasonId = input.JobCancelReasonId;
                job.IsRefund = input.IsRefund;
                job.JobStatusId = input.JobStatusId;
                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 14;
                leadactivity.SectionId = 0;
                leadactivity.ActionNote = "Job Status Change To Cancel";
                leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
            if (input.JobStatusId == 4)
            {
                job.JobStatusId = input.JobStatusId;
                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 14;
                leadactivity.SectionId = 0;
                leadactivity.ActionNote = "Job Status Change To Deposite Received";
                leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
            if (input.JobStatusId == 5)
            {
                job.JobStatusId = input.JobStatusId;
                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 14;
                leadactivity.SectionId = 0;
                leadactivity.ActionNote = "Job Status Change To Active";
                leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
            await _jobRepository.UpdateAsync(job);
        }

        public async Task UpdateCanacelationRequest(CreateOrEditJobDto input)
        {
            var job = await _jobRepository.GetAsync((int)input.Id);
            var jobstatusid = (int)input.JobStatusId;
            if (input.IsJobCancelRequest == true)
            {
                jobstatusid = 3;
            }
            if (input.IsJobCancelRequest == false)
            {
                jobstatusid = (int)input.JobStatusId;
            }
            PreviousJobStatus jobStstus = new PreviousJobStatus();
            jobStstus.JobId = job.Id;
            if (AbpSession.TenantId != null)
            {
                jobStstus.TenantId = (int)AbpSession.TenantId;
            }
            jobStstus.CurrentID = jobstatusid;
            jobStstus.PreviousId = (int)job.JobStatusId;
            await _previousJobStatusRepository.InsertAsync(jobStstus);

            job.JobStatusId = jobstatusid;
            job.JobCancelReason = input.JobCancelReason;
            job.JobCancelReasonId = input.JobCancelReasonId;
            job.IsRefund = input.IsRefund;
            job.JobCancelRequestReason = input.JobCancelRequestReason;
            job.IsJobCancelRequest = input.IsJobCancelRequest;
            job.JobCancelRejectReason = input.JobCancelRejectReason;

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            if (job.IsJobCancelRequest == true)
            {
                leadactivity.ActionId = 14;
                
                leadactivity.ActionNote = "Job Status Change To Cancel";
                leadactivity.ActivityNote = "Cancelation Request Approved";
            }
            else if (job.IsJobCancelRequest == false)
            {
                leadactivity.ActionId = 23;
             
                leadactivity.ActionNote = "Cancel Request";
                leadactivity.ActivityNote = "Cancelation Request Reject";
            }

            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            leadactivity.SectionId = 0;
            leadactivity.CreatorUserId = AbpSession.UserId;
            await _leadactivityRepository.InsertAsync(leadactivity);

            await _jobRepository.UpdateAsync(job);
        }

        [AbpAuthorize(AppPermissions.Pages_Jobs_Edit)]
        public async Task UpdateJobInstallationDetails(JobInstallationDto input)
        {
            var Date = (_timeZoneConverter.Convert(input.InstallationDate, (int)AbpSession.TenantId));

            var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);

            if (job.JobStatusId >= 5)
            {
                if (Date != null)
                {
                    job.InstallationDate = Date.Value.Date;
                }
                else
                {
                    job.InstallationDate = null;
                }
                job.InstallationNotes = input.InstallationNotes;
                job.InstallationTime = input.InstallationTime;
                job.InstallerId = input.InstallerId;
                job.DesignerId = input.DesignerId;
                job.ElectricianId = input.ElectricianId;
                job.WarehouseLocation = input.WarehouseLocation;
                //await _jobRepository.UpdateAsync(job);

                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

                if (input.InstallationDate != null)
                {
                    job.JobStatusId = 6;
                    await _jobRepository.UpdateAsync(job);

                    LeadActivityLog leadactivity1 = new LeadActivityLog();
                    leadactivity1.ActionId = 14;
                    leadactivity1.ActionNote = "Job Status Change To Job Book";
                    leadactivity1.LeadId = Convert.ToInt32(job.LeadId);
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity1.TenantId = (int)AbpSession.TenantId;
                    }
                    leadactivity1.SectionId = 0;
                    await _leadactivityRepository.InsertAsync(leadactivity1);
                }
                else
                {
                    job.JobStatusId = 5;
                    await _jobRepository.UpdateAsync(job);

                    LeadActivityLog leadactivity2 = new LeadActivityLog();
                    leadactivity2.ActionId = 14;
                    leadactivity2.ActionNote = "Job Status Change To Active";
                    leadactivity2.LeadId = Convert.ToInt32(job.LeadId);
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity2.TenantId = (int)AbpSession.TenantId;
                    }
                    leadactivity2.SectionId = 0;
                    await _leadactivityRepository.InsertAsync(leadactivity2);
                }

                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 13;
                leadactivity.ActionNote = "Installation Details Updated";
                leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                leadactivity.SectionId = 0;
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
            else
            {

                throw new UserFriendlyException(L("JobIsNotActive"));
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Jobs_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _jobRepository.DeleteAsync(input.Id);
        }

        //public async Task<FileDto> v(GetAllJobsForExcelInput input)
        //{

        //    var filteredJobs = _jobRepository.GetAll()
        //                .Include(e => e.JobTypeFk)
        //                .Include(e => e.JobStatusFk)
        //                .Include(e => e.RoofTypeFk)
        //                .Include(e => e.RoofAngleFk)
        //                .Include(e => e.ElecDistributorFk)
        //                .Include(e => e.LeadFk)
        //                .Include(e => e.ElecRetailerFk)
        //                .Include(e => e.PaymentOptionFk)
        //                .Include(e => e.DepositOptionFk)
        //                //.Include(e => e.MeterUpgradeFk)
        //                //.Include(e => e.MeterPhaseFk)
        //                .Include(e => e.PromotionOfferFk)
        //                .Include(e => e.HouseTypeFk)
        //                .Include(e => e.FinanceOptionFk)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) || e.LotNumber.Contains(input.Filter) || e.PeakMeterNo.Contains(input.Filter) || e.OffPeakMeter.Contains(input.Filter) || e.Suburb.Contains(input.Filter) || e.State.Contains(input.Filter) || e.PostalCode.Contains(input.Filter) || e.UnitNo.Contains(input.Filter) || e.UnitType.Contains(input.Filter) || e.StreetNo.Contains(input.Filter) || e.StreetName.Contains(input.Filter) || e.StreetType.Contains(input.Filter) || e.Latitude.Contains(input.Filter) || e.Longitude.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Country.Contains(input.Filter))
        //                .WhereIf(input.MinPanelOnFlatFilter != null, e => e.PanelOnFlat >= input.MinPanelOnFlatFilter)
        //                .WhereIf(input.MaxPanelOnFlatFilter != null, e => e.PanelOnFlat <= input.MaxPanelOnFlatFilter)
        //                .WhereIf(input.MinPanelOnPitchedFilter != null, e => e.PanelOnPitched >= input.MinPanelOnPitchedFilter)
        //                .WhereIf(input.MaxPanelOnPitchedFilter != null, e => e.PanelOnPitched <= input.MaxPanelOnPitchedFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.RegPlanNoFilter), e => e.RegPlanNo == input.RegPlanNoFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.LotNumberFilter), e => e.LotNumber == input.LotNumberFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.PeakMeterNoFilter), e => e.PeakMeterNo == input.PeakMeterNoFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.OffPeakMeterFilter), e => e.OffPeakMeter == input.OffPeakMeterFilter)
        //                .WhereIf(input.EnoughMeterSpaceFilter > -1, e => (input.EnoughMeterSpaceFilter == 1 && e.EnoughMeterSpace) || (input.EnoughMeterSpaceFilter == 0 && !e.EnoughMeterSpace))
        //                .WhereIf(input.IsSystemOffPeakFilter > -1, e => (input.IsSystemOffPeakFilter == 1 && e.IsSystemOffPeak) || (input.IsSystemOffPeakFilter == 0 && !e.IsSystemOffPeak))
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.RoofTypeNameFilter), e => e.RoofTypeFk != null && e.RoofTypeFk.Name == input.RoofTypeNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.RoofAngleNameFilter), e => e.RoofAngleFk != null && e.RoofAngleFk.Name == input.RoofAngleNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.ElecDistributorNameFilter), e => e.ElecDistributorFk != null && e.ElecDistributorFk.Name == input.ElecDistributorNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.ElecRetailerNameFilter), e => e.ElecRetailerFk != null && e.ElecRetailerFk.Name == input.ElecRetailerNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentOptionNameFilter), e => e.PaymentOptionFk != null && e.PaymentOptionFk.Name == input.PaymentOptionNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.DepositOptionNameFilter), e => e.DepositOptionFk != null && e.DepositOptionFk.Name == input.DepositOptionNameFilter)
        //                //.WhereIf(!string.IsNullOrWhiteSpace(input.MeterUpgradeNameFilter), e => e.MeterUpgradeFk != null && e.MeterUpgradeFk.Name == input.MeterUpgradeNameFilter)
        //                //.WhereIf(!string.IsNullOrWhiteSpace(input.MeterPhaseNameFilter), e => e.MeterPhaseFk != null && e.MeterPhaseFk.Name == input.MeterPhaseNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionOfferNameFilter), e => e.PromotionOfferFk != null && e.PromotionOfferFk.Name == input.PromotionOfferNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.HouseTypeNameFilter), e => e.HouseTypeFk != null && e.HouseTypeFk.Name == input.HouseTypeNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter);

        //    var query = (from o in filteredJobs
        //                 join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
        //                 from s1 in j1.DefaultIfEmpty()

        //                 join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
        //                 from s2 in j2.DefaultIfEmpty()

        //                 join o3 in _lookup_roofTypeRepository.GetAll() on o.RoofTypeId equals o3.Id into j3
        //                 from s3 in j3.DefaultIfEmpty()

        //                 join o4 in _lookup_roofAngleRepository.GetAll() on o.RoofAngleId equals o4.Id into j4
        //                 from s4 in j4.DefaultIfEmpty()

        //                 join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
        //                 from s5 in j5.DefaultIfEmpty()

        //                 join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
        //                 from s6 in j6.DefaultIfEmpty()

        //                 join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
        //                 from s7 in j7.DefaultIfEmpty()

        //                 join o8 in _lookup_paymentOptionRepository.GetAll() on o.PaymentOptionId equals o8.Id into j8
        //                 from s8 in j8.DefaultIfEmpty()

        //                 join o9 in _lookup_depositOptionRepository.GetAll() on o.DepositOptionId equals o9.Id into j9
        //                 from s9 in j9.DefaultIfEmpty()

        //                 join o10 in _lookup_meterUpgradeRepository.GetAll() on o.MeterUpgradeId equals o10.Id into j10
        //                 from s10 in j10.DefaultIfEmpty()

        //                 join o11 in _lookup_meterPhaseRepository.GetAll() on o.MeterPhaseId equals o11.Id into j11
        //                 from s11 in j11.DefaultIfEmpty()

        //                 join o12 in _lookup_promotionOfferRepository.GetAll() on o.PromotionOfferId equals o12.Id into j12
        //                 from s12 in j12.DefaultIfEmpty()

        //                 join o13 in _lookup_houseTypeRepository.GetAll() on o.HouseTypeId equals o13.Id into j13
        //                 from s13 in j13.DefaultIfEmpty()

        //                 join o14 in _lookup_financeOptionRepository.GetAll() on o.FinanceOptionId equals o14.Id into j14
        //                 from s14 in j14.DefaultIfEmpty()

        //                 select new GetJobForViewDto()
        //                 {
        //                     Job = new JobDto
        //                     {
        //                         //PanelOnFlat = o.PanelOnFlat,
        //                         //PanelOnPitched = o.PanelOnPitched,
        //                         RegPlanNo = o.RegPlanNo,
        //                         LotNumber = o.LotNumber,
        //                         //PeakMeterNo = o.PeakMeterNo,
        //                         //OffPeakMeter = o.OffPeakMeter,
        //                         //EnoughMeterSpace = o.EnoughMeterSpace,
        //                         //IsSystemOffPeak = o.IsSystemOffPeak,
        //                         //BasicCost = o.BasicCost,
        //                         //SolarVLCRebate = o.SolarVLCRebate,
        //                         //SolarVLCLoan = o.SolarVLCLoan,
        //                         //TotalCost = o.TotalCost,
        //                         Suburb = o.Suburb,
        //                         State = o.State,
        //                         UnitNo = o.UnitNo,
        //                         UnitType = o.UnitType,
        //                         Id = o.Id
        //                     },
        //                     JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
        //                     JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
        //                     //RoofTypeName = s3 == null || s3.Name == null ? "" : s3.Name.ToString(),
        //                     //RoofAngleName = s4 == null || s4.Name == null ? "" : s4.Name.ToString(),
        //                     ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
        //                     LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
        //                     ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
        //                     //PaymentOptionName = s8 == null || s8.Name == null ? "" : s8.Name.ToString(),
        //                     //DepositOptionName = s9 == null || s9.Name == null ? "" : s9.Name.ToString(),
        //                     //MeterUpgradeName = s10 == null || s10.Name == null ? "" : s10.Name.ToString(),
        //                     //MeterPhaseName = s11 == null || s11.Name == null ? "" : s11.Name.ToString(),
        //                     //PromotionOfferName = s12 == null || s12.Name == null ? "" : s12.Name.ToString(),
        //                     //HouseTypeName = s13 == null || s13.Name == null ? "" : s13.Name.ToString(),
        //                     //FinanceOptionName = s14 == null || s14.Name == null ? "" : s14.Name.ToString()
        //                 });


        //    var jobListDtos = await query.ToListAsync();

        //    return _jobsExcelExporter.ExportToFile(jobListDtos);
        //}

        public async Task<List<JobJobTypeLookupTableDto>> GetAllJobTypeForTableDropdown()
        {
            return await _lookup_jobTypeRepository.GetAll()
                .Select(jobType => new JobJobTypeLookupTableDto
                {
                    Id = jobType.Id,
                    DisplayName = jobType == null || jobType.Name == null ? "" : jobType.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobJobStatusLookupTableDto>> GetAllJobStatusForTableDropdown()
        {
            return await _lookup_jobStatusRepository.GetAll()
                .Where(e => e.Name == "On Hold" || e.Name == "Cancel")
                .Select(jobStatus => new JobJobStatusLookupTableDto
                {
                    Id = jobStatus.Id,
                    DisplayName = jobStatus == null || jobStatus.Name == null ? "" : jobStatus.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobRoofTypeLookupTableDto>> GetAllRoofTypeForTableDropdown()
        {
            return await _lookup_roofTypeRepository.GetAll()
                .Select(roofType => new JobRoofTypeLookupTableDto
                {
                    Id = roofType.Id,
                    DisplayName = roofType == null || roofType.Name == null ? "" : roofType.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobRoofAngleLookupTableDto>> GetAllRoofAngleForTableDropdown()
        {
            return await _lookup_roofAngleRepository.GetAll()
                .Select(roofAngle => new JobRoofAngleLookupTableDto
                {
                    Id = roofAngle.Id,
                    DisplayName = roofAngle == null || roofAngle.Name == null ? "" : roofAngle.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobElecDistributorLookupTableDto>> GetAllElecDistributorForTableDropdown()
        {
            return await _lookup_elecDistributorRepository.GetAll()
                .Select(elecDistributor => new JobElecDistributorLookupTableDto
                {
                    Id = elecDistributor.Id,
                    DisplayName = elecDistributor == null || elecDistributor.Name == null ? "" : elecDistributor.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobLeadLookupTableDto>> GetAllLeadForTableDropdown()
        {
            return await _lookup_leadRepository.GetAll()
                .Select(lead => new JobLeadLookupTableDto
                {
                    Id = lead.Id,
                    DisplayName = lead == null || lead.CompanyName == null ? "" : lead.CompanyName.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobElecRetailerLookupTableDto>> GetAllElecRetailerForTableDropdown()
        {
            return await _lookup_elecRetailerRepository.GetAll()
                .Select(elecRetailer => new JobElecRetailerLookupTableDto
                {
                    Id = elecRetailer.Id,
                    DisplayName = elecRetailer == null || elecRetailer.Name == null ? "" : elecRetailer.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobPaymentOptionLookupTableDto>> GetAllPaymentOptionForTableDropdown()
        {
            return await _lookup_paymentOptionRepository.GetAll()
                .Select(paymentOption => new JobPaymentOptionLookupTableDto
                {
                    Id = paymentOption.Id,
                    DisplayName = paymentOption == null || paymentOption.Name == null ? "" : paymentOption.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobDepositOptionLookupTableDto>> GetAllDepositOptionForTableDropdown()
        {
            return await _lookup_depositOptionRepository.GetAll()
                .Select(depositOption => new JobDepositOptionLookupTableDto
                {
                    Id = depositOption.Id,
                    DisplayName = depositOption == null || depositOption.Name == null ? "" : depositOption.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobMeterUpgradeLookupTableDto>> GetAllMeterUpgradeForTableDropdown()
        {
            return await _lookup_meterUpgradeRepository.GetAll()
                .Select(meterUpgrade => new JobMeterUpgradeLookupTableDto
                {
                    Id = meterUpgrade.Id,
                    DisplayName = meterUpgrade == null || meterUpgrade.Name == null ? "" : meterUpgrade.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobMeterPhaseLookupTableDto>> GetAllMeterPhaseForTableDropdown()
        {
            return await _lookup_meterPhaseRepository.GetAll()
                .Select(meterPhase => new JobMeterPhaseLookupTableDto
                {
                    Id = meterPhase.Id,
                    DisplayName = meterPhase == null || meterPhase.Name == null ? "" : meterPhase.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<InstallerAvailableListDto>> GetAllInstallerAvailabilityList(DateTime AvailableDate, int jobid)
        {

            var InstAvailableDate = (_timeZoneConverter.Convert(AvailableDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.IsActive == true);
            ///DateTime date = Convert.ToDateTime(AvailableDate.ToString("yyyy-MM-dd"));
            var InstallerList = (from u in User
                                 join o1 in _installerDetailRepository.GetAll() on u.Id equals o1.UserId into j1
                                 from s1 in j1.DefaultIfEmpty()
                                 join o3 in _installerAvailabilityRepository.GetAll() on u.Id equals o3.UserId into j3
                                 from s3 in j3.DefaultIfEmpty()
                                     //where o3.AvailabilityDate.Date == InstAvailableDate.Value.Date
                                 let AvailableCount =
                                    (
                                      from o2 in _installerAvailabilityRepository.GetAll()
                                      where o2.UserId == u.Id && o2.AvailabilityDate.Date == InstAvailableDate.Value.Date
                                      select o2
                                    ).Count()
                                 let InstallBookCount =
                                     (
                                       from o2 in _jobRepository.GetAll()
                                       where o2.InstallerId == u.Id && o2.InstallationDate == InstAvailableDate.Value.Date && o2.Id != jobid
                                       select o2
                                     ).Count()
                                 where (s1 != null && s1.IsInst == true)
                                 select new InstallerAvailableListDto
                                 {
                                     Id = (int)u.Id,
                                     DisplayName = u.FullName,
                                     AvailabilityCount = AvailableCount,
                                     BookCount = InstallBookCount,
                                     availbale = true
                                 }).Where(x => x.AvailabilityCount > x.BookCount).ToList();
            //.Where(x => x.AvailabilityCount > x.BookCount).ToList();

            //if (InstallerList.Count == 0)
            //{
            //    var result = (from u in User
            //                  join o1 in _installerDetailRepository.GetAll() on u.Id equals o1.UserId into j1
            //                  from s1 in j1.DefaultIfEmpty()
            //                      //join o2 in _installerAvailabilityRepository.GetAll() on u.Id equals o2.UserId into j2
            //                      //from s2 in j2.DefaultIfEmpty()
            //                  let AvailableCount =
            //                     (
            //                       from o2 in _installerAvailabilityRepository.GetAll()
            //                       where o2.UserId == u.Id && o2.AvailabilityDate.Date == InstAvailableDate.Value.Date
            //                       select o2
            //                     ).Count()
            //                  let InstallBookCount =
            //                      (
            //                        from o2 in _jobRepository.GetAll()
            //                        where o2.InstallerId == u.Id && o2.InstallationDate == InstAvailableDate.Value.Date && o2.Id != jobid
            //                        select o2
            //                      ).Count()
            //                  where (s1 != null && s1.IsInst == true)
            //                  select new InstallerAvailableListDto
            //                  {
            //                      DisplayName = u.FullName,
            //                      availbale = false
            //                  }).ToList();
            //    return result;
            //}
            //else
            //{
            return InstallerList;
            //}
        }

        public async Task<List<InstallerAvailableListDto>> GetAllElectricianAvailabilityList(DateTime AvailableDate, int jobid)
        {
            var ElecAvailableDate = (_timeZoneConverter.Convert(AvailableDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll();

            var ElectricianList = (from u in User
                                   join o1 in _installerDetailRepository.GetAll() on u.Id equals o1.UserId into j1
                                   from s1 in j1.DefaultIfEmpty()
                                       //join o2 in _installerAvailabilityRepository.GetAll() on u.Id equals o2.UserId into j2
                                       //from s2 in j2.DefaultIfEmpty()
                                   let AvailableCount =
                                      (
                                        from o2 in _installerAvailabilityRepository.GetAll()
                                        where o2.UserId == u.Id && o2.AvailabilityDate.AddHours(10).Date == ElecAvailableDate.Value.Date
                                        select o2
                                      ).Count()
                                   let InstallBookCount =
                                       (
                                         from o2 in _jobRepository.GetAll()
                                         where o2.ElectricianId == u.Id && o2.InstallationDate == ElecAvailableDate.Value.Date && o2.Id != jobid
                                         select o2
                                       ).Count()
                                   where (s1 != null && s1.IsElec == true)
                                   select new InstallerAvailableListDto
                                   {
                                       Id = (int)u.Id,
                                       DisplayName = u.FullName,
                                       AvailabilityCount = AvailableCount,
                                       BookCount = InstallBookCount
                                   }).Where(x => x.AvailabilityCount > x.BookCount).ToList();
            return ElectricianList;
        }

        public async Task<List<InstallerAvailableListDto>> GetAllDesignerAvailabilityList(DateTime AvailableDate, int jobid)
        {
            var DescAvailableDate = (_timeZoneConverter.Convert(AvailableDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll();

            var DesignerList = (from u in User
                                join o1 in _installerDetailRepository.GetAll() on u.Id equals o1.UserId into j1
                                from s1 in j1.DefaultIfEmpty()
                                    //join o2 in _installerAvailabilityRepository.GetAll() on u.Id equals o2.UserId into j2
                                    //from s2 in j2.DefaultIfEmpty()
                                let AvailableCount =
                                   (
                                     from o2 in _installerAvailabilityRepository.GetAll()
                                     where o2.UserId == u.Id && o2.AvailabilityDate.AddHours(10).Date == DescAvailableDate.Value.Date
                                     select o2
                                   ).Count()
                                let InstallBookCount =
                                    (
                                      from o2 in _jobRepository.GetAll()
                                      where o2.DesignerId == u.Id && o2.InstallationDate == DescAvailableDate.Value.Date && o2.Id != jobid
                                      select o2
                                    ).Count()
                                where (s1 != null && s1.IsDesi == true)
                                select new InstallerAvailableListDto
                                {
                                    Id = (int)u.Id,
                                    DisplayName = u.FullName,
                                    AvailabilityCount = AvailableCount,
                                    BookCount = InstallBookCount
                                }).Where(x => x.AvailabilityCount > x.BookCount).ToList();
            return DesignerList;
        }

        public async Task<List<CommonLookupDto>> GetWareHouseDropdown(string stateid)
        {
            var warehOuselist = (from item in _warehouselocationRepository.GetAll()
                                     //where (item.state == stateid)
                                 select new CommonLookupDto
                                 {
                                     Id = item.Id,
                                     DisplayName = item.location,
                                 }).ToList();
            return warehOuselist;
        }

        public async Task<PagedResultDto<JobPromotionOfferLookupTableDto>> GetAllPromotionOfferForLookupTable(GetAllForLookupTableInput input)
        {
            var query = _lookup_promotionOfferRepository.GetAll().WhereIf(
                   !string.IsNullOrWhiteSpace(input.Filter),
                  e => e.Name != null && e.Name.Contains(input.Filter)
               );

            var totalCount = await query.CountAsync();

            var promotionOfferList = await query
                .PageBy(input)
                .ToListAsync();

            var lookupTableDtoList = new List<JobPromotionOfferLookupTableDto>();
            foreach (var promotionOffer in promotionOfferList)
            {
                lookupTableDtoList.Add(new JobPromotionOfferLookupTableDto
                {
                    Id = promotionOffer.Id,
                    DisplayName = promotionOffer.Name?.ToString()
                });
            }

            return new PagedResultDto<JobPromotionOfferLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
        }

        public async Task<List<JobHouseTypeLookupTableDto>> GetAllHouseTypeForTableDropdown()
        {
            return await _lookup_houseTypeRepository.GetAll()
                .Select(houseType => new JobHouseTypeLookupTableDto
                {
                    Id = houseType.Id,
                    DisplayName = houseType == null || houseType.Name == null ? "" : houseType.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobFinanceOptionLookupTableDto>> GetAllFinanceOptionForTableDropdown()
        {
            return await _lookup_financeOptionRepository.GetAll()
                .Select(financeOption => new JobFinanceOptionLookupTableDto
                {
                    Id = financeOption.Id,
                    DisplayName = financeOption == null || financeOption.Name == null ? "" : financeOption.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<STCZoneRatingDto>> GetSTCZoneRating()
        {
            return await _stcZoneRatingRepository.GetAll()
                .Select(stcZoneRating => new STCZoneRatingDto
                {
                    Id = stcZoneRating.Id,
                    Rating = stcZoneRating.Rating,
                    UpSizeTs = stcZoneRating.UpSizeTs
                }).ToListAsync();
        }

        public async Task<List<STCPostalCodeDto>> GetSTCPostalCode()
        {
            return await _stcPostalCodeRepository.GetAll()
                .Select(stcPostalCode => new STCPostalCodeDto
                {
                    Id = stcPostalCode.Id,
                    PostCodeFrom = stcPostalCode.PostCodeFrom,
                    PostCodeTo = stcPostalCode.PostCodeTo,
                    ZoneId = stcPostalCode.ZoneId
                }).ToListAsync();
        }

        public async Task<List<STCYearWiseRateDto>> GetSTCYearWiseRate()
        {
            return await _stcYearWiseRateRepository.GetAll()
                .Select(stcYearWise => new STCYearWiseRateDto
                {
                    Id = stcYearWise.Id,
                    Rate = stcYearWise.Rate,
                    Year = stcYearWise.Year
                }).ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetAllCancelReasonForTableDropdown()
        {
            return await _jobCancellationReasonReasonRepository.GetAll()
                .Select(cancelReason => new CommonLookupDto
                {
                    Id = cancelReason.Id,
                    DisplayName = cancelReason == null || cancelReason.Name == null ? "" : cancelReason.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetAllHoldReasonForTableDropdown()
        {
            return await _jobHoldReasonRepository.GetAll()
                .Select(holdReason => new CommonLookupDto
                {
                    Id = holdReason.Id,
                    DisplayName = holdReason == null || holdReason.JobHoldReason == null ? "" : holdReason.JobHoldReason.ToString()
                }).ToListAsync();
        }

        public string GetVariationsAction(int? Id)
        {
            var ActionName = _variationRepository.GetAll().Where(e => e.Id == Id).Select(e => e.Action).FirstOrDefault();

            return ActionName;
        }

        public async Task JobActive(int Id)
        {
            var Jobs = _jobRepository.GetAll().Where(e => e.Id == Id).FirstOrDefault();

            if (Jobs.JobStatusId == 1 || Jobs.JobStatusId == 4)
            {
                var NMI = !string.IsNullOrEmpty(Jobs.NMINumber);
                var ApprovedReferenceNumber = !string.IsNullOrEmpty(Jobs.ApplicationRefNo);
                var Meter = Jobs.MeterUpgradeId != null;
                var MeterPhoto = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 3 && e.JobId == Id).Any();
                var Deposite = _invoicePaymentRepository.GetAll().Where(e => e.JobId == Id).Any();
                var SignedQuote = _quotationRepository.GetAll().Where(e => e.JobId == Id && e.IsSigned == true).Any() || _documentRepository.GetAll().Where(e => e.JobId == Id && e.DocumentTypeId == 5).Any();
                //var QuoteAcceptDate = _quotationRepository.GetAll().Where(e => e.JobId == Id && e.IsSigned == true && e.QuoteAcceptDate != null).Any();
                var Finance = Jobs.PaymentOptionId == 1 ? true : Jobs.FinanceDocumentVerified == 1 ? true : false;

                if (NMI && Jobs.ElecRetailerId != null && Jobs.ElecRetailerId != 0 && Meter && Deposite && SignedQuote && Finance && ApprovedReferenceNumber && MeterPhoto)
                {
                    Jobs.IsActive = true;
                    await _jobRepository.UpdateAsync(Jobs);

                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 14;
                    leadactivity.SectionId = 0;
                    leadactivity.ActionNote = "Job Active Request Created";
                    leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);

                    //string msg = string.Format("Job Number {0} Requested To Active  ", Jobs.JobNumber);
                    //await _appNotifier.LeadAssiged(AssignedUser, msg, NotificationSeverity.Info);
                }
            }
        }

        public async Task JobActiveLoop()
        {
            var Job = _jobRepository.GetAll().Select(e => e.Id).ToList();

            foreach (var item in Job)
            {
                JobActive(item);
            }
        }

        public async Task<JobActiveStatusDto> GetJobActiveStatus(EntityDto input)
        {
            var Result = new JobActiveStatusDto();
            var job = _jobRepository.GetAll().Where(e => e.Id == input.Id).FirstOrDefault();
            ///// Active
            Result.NMINumber = !string.IsNullOrEmpty(job.NMINumber);
            Result.ApprovedReferenceNumber = !string.IsNullOrEmpty(job.ApplicationRefNo);
            Result.MeterUpdgrade = job.MeterUpgradeId != null;
            Result.MeterBoxPhoto = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 3 && e.JobId == job.Id).Any();
            Result.SignedQuote = _quotationRepository.GetAll().Where(e => e.JobId == job.Id && e.IsSigned == true).Any() || _documentRepository.GetAll().Where(e => e.JobId == job.Id && e.DocumentTypeId == 5).Any();
            Result.Deposite = _invoicePaymentRepository.GetAll().Where(e => e.JobId == input.Id).Any();
            //Result.QuoteAcceptDate = _quotationRepository.GetAll().Where(e => e.JobId == input.Id && e.IsSigned == true && e.QuoteAcceptDate != null).Any();
            Result.Finance = job.PaymentOptionId == 1 ? true : job.FinanceDocumentVerified == 1 ? true : false;
            Result.payementoptionid = job.PaymentOptionId;


            ///// deposite received
            Result.HouseTypeId = job.HouseTypeId != null;
            Result.ElecDistributorId = job.ElecDistributorId != null;
            Result.RoofAngleId = job.RoofAngleId != null;
            Result.RoofTypeId = job.RoofTypeId != null;
            Result.InstallerNotes = !string.IsNullOrEmpty(job.InstallerNotes);
            Result.Quote = _quotationRepository.GetAll().Where(e => e.JobId == input.Id).Any();
            Result.Invoice = _invoicePaymentRepository.GetAll().Where(e => e.JobId == input.Id).Any();
            Result.ProductDetail = _jobProductItemRepository.GetAll().Where(e => e.JobId == input.Id).Any();
            Result.JobStatusId = job.JobStatusId;

            ///// ApplicationTracker
            Result.MeterPhadeIDs = _lookup_meterPhaseRepository.GetAll().Where(e => e.Id == job.MeterPhaseId).Any();
            Result.PeakMeterNos = !string.IsNullOrEmpty(job.PeakMeterNo);
            Result.DocList = _documentRepository.GetAll().Where(e => e.JobId == job.Id).Any();
            Result.elecRetailerId = job.ElecRetailerId != null;
            return Result;
        }

        public async Task JobDeposite(int Id)
        {
            var Jobs = _jobRepository.GetAll().Where(e => e.Id == Id).FirstOrDefault();

            if ((Jobs.JobStatusId == 1 || Jobs.JobStatusId == 2 || Jobs.JobStatusId == 3) && Jobs.JobStatusId != 5)
            {
                var HouseTypeId = Jobs.HouseTypeId != null;
                var ElecDistributorId = Jobs.ElecDistributorId != null;
                var RoofAngleId = Jobs.RoofAngleId != null;
                var RoofTypeId = Jobs.RoofTypeId != null;
                var InstallerNOtes = !string.IsNullOrEmpty(Jobs.InstallerNotes);
                var Quote = _quotationRepository.GetAll().Where(e => e.JobId == Id).Any();
                var Invoice = _invoicePaymentRepository.GetAll().Where(e => e.JobId == Id).Any();
                var ProductDetail = _jobProductItemRepository.GetAll().Where(e => e.JobId == Id).Any();

                if (HouseTypeId && ElecDistributorId && RoofAngleId && RoofTypeId && InstallerNOtes && Quote && Invoice && ProductDetail)
                {
                    PreviousJobStatus jobStstus = new PreviousJobStatus();
                    jobStstus.JobId = Jobs.Id;
                    if (AbpSession.TenantId != null)
                    {
                        jobStstus.TenantId = (int)AbpSession.TenantId;
                    }
                    jobStstus.CurrentID = 4;
                    jobStstus.PreviousId = (int)Jobs.JobStatusId;
                    await _previousJobStatusRepository.InsertAsync(jobStstus);

                    Jobs.JobStatusId = 4;
                    Jobs.DepositeRecceivedDate = DateTime.UtcNow;
                    await _jobRepository.UpdateAsync(Jobs);

                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 14;
                    leadactivity.ActionNote = "Job Status Change To Job Deposite Received";
                    leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    leadactivity.SectionId = 0;
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }
            }
        }

        public async Task Update_PostInstallationDetails(CreateOrEditJobDto input)
        {
            var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);
            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 14;
            leadactivity.SectionId = 0;
            if (input.PostInstallationStatus == 1)
            {

                PreviousJobStatus jobStstus = new PreviousJobStatus();
                jobStstus.JobId = job.Id;
                if (AbpSession.TenantId != null)
                {
                    jobStstus.TenantId = (int)AbpSession.TenantId;
                }
                jobStstus.CurrentID = 8;
                jobStstus.PreviousId = (int)job.JobStatusId;
                await _previousJobStatusRepository.InsertAsync(jobStstus);


                job.JobStatusId = 8;
                leadactivity.ActionNote = "Job Status Change To Job Installed";

            }
            else if (input.PostInstallationStatus == 2)
            {
                PreviousJobStatus jobStstus = new PreviousJobStatus();
                jobStstus.JobId = job.Id;
                if (AbpSession.TenantId != null)
                {
                    jobStstus.TenantId = (int)AbpSession.TenantId;
                }
                jobStstus.CurrentID = 7;
                jobStstus.PreviousId = (int)job.JobStatusId;
                await _previousJobStatusRepository.InsertAsync(jobStstus);

                job.JobStatusId = 7;
                leadactivity.ActionNote = "Job Status Change To Job Incomplete";
            }
            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            job.MeterApplyRef = input.MeterApplyRef;
            job.InspectorName = input.InspectorName;
            job.InspectionDate = input.InspectionDate;
            job.ComplianceCertificate = input.ComplianceCertificate;
            job.IncompleteReason = input.IncompleteReason;
            job.InstalledcompleteDate = input.InstalledcompleteDate;
            job.PostInstallationStatus = input.PostInstallationStatus;
            await _jobRepository.UpdateAsync(job);
            // await JobActive(job.Id); 
            await _leadactivityRepository.InsertAsync(leadactivity);

        }

        //STC Tracker
        public async Task<PagedResultDto<GetJobForViewDto>> GetAllForSTCTrackerTracker(GetAllJobsInput input)
        {
            if (input.pvdStat != null)
            {
                if (input.pvdStat.Contains(11))
                {
                    input.pvdStat.Remove(11);
                    input.pvdnoStatus = 2;
                }
            }
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobTypeFk)
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.RoofTypeFk)
                        .Include(e => e.RoofAngleFk)
                        .Include(e => e.ElecDistributorFk)
                        .Include(e => e.LeadFk)
                        .Include(e => e.ElecRetailerFk)
                        .Include(e => e.PaymentOptionFk)
                        .Include(e => e.DepositOptionFk)
                        //.Include(e => e.MeterUpgradeFk)
                        //.Include(e => e.MeterPhaseFk)
                        .Include(e => e.PromotionOfferFk)
                        .Include(e => e.HouseTypeFk)
                        .Include(e => e.FinanceOptionFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) || e.LotNumber.Contains(input.Filter) || e.PeakMeterNo.Contains(input.Filter) || e.OffPeakMeter.Contains(input.Filter) || e.Suburb.Contains(input.Filter) || e.State.Contains(input.Filter) || e.PostalCode.Contains(input.Filter) || e.UnitNo.Contains(input.Filter) || e.UnitType.Contains(input.Filter) || e.StreetNo.Contains(input.Filter) || e.StreetName.Contains(input.Filter) || e.StreetType.Contains(input.Filter) || e.Latitude.Contains(input.Filter) || e.Longitude.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Country.Contains(input.Filter) || e.PVDNumber.Contains(input.Filter) || e.JobNumber.Contains(input.Filter))

                          .WhereIf(input.DateNameFilter == "StcAppliedDate" && SDate != null && EDate != null, e => e.STCAppliedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.STCAppliedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                          .WhereIf(input.DateNameFilter == "InstallDate" && SDate != null && EDate != null, e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
                          .WhereIf(input.DateNameFilter == "InstallCmpltDate" && SDate != null && EDate != null, e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
                          .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.LeadFk.State == input.stateNameFilter)

                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.InstallerID != null && input.InstallerID != 0, e => e.InstallerId == input.InstallerID)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
                         //.WhereIf(input.pvdStatus != null && input.pvdStatus != 0, e => e.PVDStatus == input.pvdStatus)
                         .WhereIf(input.pvdNo != null && input.pvdNo != "", e => e.PVDNumber == input.pvdNo)
                         .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
                         // .WhereIf(input.pvdStatus != null && input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId) 
                         .WhereIf(input.pvdnoStatus == 1, e => e.PVDNumber != null && e.PVDNumber != "")
                         .WhereIf(input.pvdnoStatus == 2, e => e.PVDNumber == null || e.PVDNumber == "")
                         .WhereIf(input.pvdStat != null && input.pvdStat.Count() > 0, e => input.pvdStat.Contains((int)e.PVDStatus))
                        //.WhereIf(input.pvdStat.Contains(1), e => e.PVDNumber != null && e.PVDNumber != "")
                        // .WhereIf(input.pvdStat.Contains(2), e => e.PVDNumber == null || e.PVDNumber == "")
                        .Where(e => e.JobStatusId >= 4 && e.PVDStatus != 1)
                        .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);

            var pagedAndFilteredJobs = filteredJobs
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var jobs = from o in pagedAndFilteredJobs
                       join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                       from s1 in j1.DefaultIfEmpty()

                       join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                       from o2 in j2.DefaultIfEmpty()

                       join pvdval in _pVDStatusRepository.GetAll() on o.PVDStatus equals pvdval.Id into pvd
                       from pvdval in pvd.DefaultIfEmpty()

                       join userreq in _userRepository.GetAll() on (int)o.InstallerId equals userreq.Id into user
                       from userval in user.DefaultIfEmpty()

                           //join o3 in _lookup_roofTypeRepository.GetAll() on o.RoofTypeId equals o3.Id into j3
                           //from s3 in j3.DefaultIfEmpty()

                           //join o4 in _lookup_roofAngleRepository.GetAll() on o.RoofAngleId equals o4.Id into j4
                           //from s4 in j4.DefaultIfEmpty()

                       join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                       from s5 in j5.DefaultIfEmpty()

                       join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                       from s6 in j6.DefaultIfEmpty()

                       join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                       from s7 in j7.DefaultIfEmpty()

                           //join o8 in _lookup_paymentOptionRepository.GetAll() on o.PaymentOptionId equals o8.Id into j8
                           //from s8 in j8.DefaultIfEmpty()

                           //join o9 in _lookup_depositOptionRepository.GetAll() on o.DepositOptionId equals o9.Id into j9
                           //from s9 in j9.DefaultIfEmpty()

                           //join o10 in _lookup_meterUpgradeRepository.GetAll() on o.MeterUpgradeId equals o10.Id into j10
                           //from s10 in j10.DefaultIfEmpty()

                           //join o11 in _lookup_meterPhaseRepository.GetAll() on o.MeterPhaseId equals o11.Id into j11
                           //from s11 in j11.DefaultIfEmpty()

                           //join o12 in _lookup_promotionOfferRepository.GetAll() on o.PromotionOfferId equals o12.Id into j12
                           //from s12 in j12.DefaultIfEmpty()

                           //join o13 in _lookup_houseTypeRepository.GetAll() on o.HouseTypeId equals o13.Id into j13
                           //from s13 in j13.DefaultIfEmpty()

                           //join o14 in _lookup_financeOptionRepository.GetAll() on o.FinanceOptionId equals o14.Id into j14
                           //from s14 in j14.DefaultIfEmpty()

                       select new GetJobForViewDto()
                       {
                           Job = new JobDto
                           {
                               RegPlanNo = o.RegPlanNo,
                               LotNumber = o.LotNumber,
                               Suburb = o.Suburb,
                               State = o.State,
                               UnitNo = o.UnitNo,
                               UnitType = o.UnitType,
                               NMINumber = o.NMINumber,
                               Id = o.Id,
                               ApplicationRefNo = o.ApplicationRefNo,
                               DistAppliedDate = o.DistAppliedDate,
                               Notes = o.Note,
                               InstallerNotes = o.InstallerNotes,
                               JobNumber = o.JobNumber,
                               MeterNumber = o.MeterNumber,
                               OldSystemDetails = o.OldSystemDetails,
                               PostalCode = o.PostalCode,
                               StreetName = o.StreetName,
                               StreetNo = o.StreetNo,
                               StreetType = o.StreetType,
                               LeadId = o.LeadId,
                               ElecDistributorId = o.ElecDistributorId,
                               ElecRetailerId = o.ElecRetailerId,
                               JobStatusId = o.JobStatusId,
                               JobTypeId = o.JobTypeId,
                               PVDStatus = o.PVDStatus,
                               STCAppliedDate = o.STCAppliedDate,
                               STCUploaddate = o.STCUploaddate,
                               STCUploadNumber = o.STCUploadNumber,
                               PVDNumber = o.PVDNumber == null || o.PVDNumber == "" ? "Blank" : o.PVDNumber,
                               STCNotes = o.STCNotes,
                               StcSmsSend = o.StcSmsSend,
                               StcSmsSendDate = o.StcSmsSendDate,
                               StcEmailSend = o.StcEmailSend,
                               StcEmailSendDate = o.StcEmailSendDate,
                               InstallerName = userval == null || userval.FullName == null ? "" : userval.FullName.ToString(),
                               PVDStatusName = pvdval == null || pvdval.Name == null ? "Blank" : pvdval.Name.ToString(),
                               CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                               Stc = o.STC,
                               StcTotalPrice = o.Rebate,
                               SystemCapacity = o.SystemCapacity,
                               InstallationDate = o.InstallationDate
                           },

                           LastComment = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           LastCommentDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),

                           JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                           JobStatusName = o2 == null || o2.Name == null ? "" : o2.Name.ToString(),
                           ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                           LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                           Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                           Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                           ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
                           ActivityReminderTime = _leadactivityRepository.GetAll().Where(e => e.SectionId == 7 && e.ActionId == 8 && e.LeadId == s6.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                           ActivityDescription = _leadactivityRepository.GetAll().Where(e => e.SectionId == 7 && e.ActionId == 8 && e.LeadId == s6.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           ActivityComment = _leadactivityRepository.GetAll().Where(e => e.SectionId == 7 && e.ActionId == 24 && e.LeadId == s6.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),


                       };

            var totalCount = await filteredJobs.CountAsync();

            return new PagedResultDto<GetJobForViewDto>(
                totalCount,
                await jobs.ToListAsync()
            );
        }

        //STC Excel Export
        public async Task<FileDto> getSTCTrackerToExcel(GetAllJobsForExcelInput input)
        {
            
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobTypeFk)
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.RoofTypeFk)
                        .Include(e => e.RoofAngleFk)
                        .Include(e => e.ElecDistributorFk)
                        .Include(e => e.LeadFk)
                        .Include(e => e.ElecRetailerFk)
                        .Include(e => e.PaymentOptionFk)
                        .Include(e => e.DepositOptionFk)
                        //.Include(e => e.MeterUpgradeFk)
                        //.Include(e => e.MeterPhaseFk)
                        .Include(e => e.PromotionOfferFk)
                        .Include(e => e.HouseTypeFk)
                        .Include(e => e.FinanceOptionFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) || e.LotNumber.Contains(input.Filter) || e.PeakMeterNo.Contains(input.Filter) || e.OffPeakMeter.Contains(input.Filter) || e.Suburb.Contains(input.Filter) || e.State.Contains(input.Filter) || e.PostalCode.Contains(input.Filter) || e.UnitNo.Contains(input.Filter) || e.UnitType.Contains(input.Filter) || e.StreetNo.Contains(input.Filter) || e.StreetName.Contains(input.Filter) || e.StreetType.Contains(input.Filter) || e.Latitude.Contains(input.Filter) || e.Longitude.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Country.Contains(input.Filter))

                          .WhereIf(input.DateNameFilter == "StcAppliedDate" && SDate != null && EDate != null, e => e.STCAppliedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.STCAppliedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                          .WhereIf(input.DateNameFilter == "InstallDate" && SDate != null && EDate != null, e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
                          .WhereIf(input.DateNameFilter == "InstallCmpltDate" && SDate != null && EDate != null, e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
                          .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.LeadFk.State == input.stateNameFilter)

                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.InstallerID != null && input.InstallerID != 0, e => e.InstallerId == input.InstallerID)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
                         //.WhereIf(input.pvdStatus != null && input.pvdStatus != 0, e => e.PVDStatus == input.pvdStatus)
                         .WhereIf(input.pvdNo != null && input.pvdNo != "", e => e.PVDNumber == input.pvdNo)
                         .WhereIf(input.pvdStat != null && input.pvdStat.Count() > 0, e => input.pvdStat.Contains((int)e.PVDStatus))
                         .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
                        .Where(e => e.JobStatusId >= 4 && e.PVDStatus != 1)
                        .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);
            var query = (from o in filteredJobs

                         join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o2 in _pVDStatusRepository.GetAll() on o.PVDStatus equals o2.Id into pvd
                         from pvdval in pvd.DefaultIfEmpty()

                         join userreq in _userRepository.GetAll() on (int)o.InstallerId equals userreq.Id into user
                         from userval in user.DefaultIfEmpty()


                         join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                         from s5 in j5.DefaultIfEmpty()

                         join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                         from s6 in j6.DefaultIfEmpty()

                         join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                         from s7 in j7.DefaultIfEmpty()



                         select new GetJobForViewDto()
                         {
                             Job = new JobDto
                             {
                                 RegPlanNo = o.RegPlanNo,
                                 LotNumber = o.LotNumber,
                                 Suburb = o.Suburb,
                                 State = o.State,
                                 UnitNo = o.UnitNo,
                                 UnitType = o.UnitType,
                                 NMINumber = o.NMINumber,
                                 Id = o.Id,
                                 ApplicationRefNo = o.ApplicationRefNo,
                                 DistAppliedDate = o.DistAppliedDate,
                                 Notes = o.Note,
                                 InstallerNotes = o.InstallerNotes,
                                 JobNumber = o.JobNumber,
                                 MeterNumber = o.MeterNumber,
                                 OldSystemDetails = o.OldSystemDetails,
                                 PostalCode = o.PostalCode,
                                 StreetName = o.StreetName,
                                 StreetNo = o.StreetNo,
                                 StreetType = o.StreetType,
                                 LeadId = o.LeadId,
                                 ElecDistributorId = o.ElecDistributorId,
                                 ElecRetailerId = o.ElecRetailerId,
                                 JobStatusId = o.JobStatusId,
                                 JobTypeId = o.JobTypeId,
                                 PVDStatus = o.PVDStatus,
                                 STCAppliedDate = o.STCAppliedDate,
                                 STCUploaddate = o.STCUploaddate,
                                 STCUploadNumber = o.STCUploadNumber,
                                 Stc = o.STC,
                                 StcTotalPrice = o.Rebate,
                                 SystemCapacity = o.SystemCapacity,
                                 PVDNumber = o.PVDNumber,
                                 STCNotes = o.STCNotes,
                                 InstallerName = userval == null || userval.FullName == null ? "" : userval.FullName.ToString(),
                                 PVDStatusName = pvdval == null || pvdval.Name == null ? "" : pvdval.Name.ToString(),
                                 CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                                 InstallationDate = o.InstallationDate
                             },
                             LastComment = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                             LastCommentDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),

                             JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                             JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                             ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                             LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                             Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                             Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                             ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),

                         });

            var stcListDtos = await query.ToListAsync();

            return _leadsExcelExporter.STCExportToFile(stcListDtos);
        }

        public async Task Update_JOb_STCDetails(CreateOrEditJobDto input)
        {
            var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);
            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

            job.STCNotes = input.STCNotes;
            job.STCAppliedDate = input.STCAppliedDate;
            job.STCUploaddate = input.STCUploaddate;
            job.STCUploadNumber = input.STCUploadNumber; 
            job.PVDNumber = input.PVDNumber;
            job.PVDStatus = input.PVDStatus;
            
            job.PVDNumber = input.PVDNumber;
            job.PVDStatus = input.PVDStatus;

            await _jobRepository.UpdateAsync(job);
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 18;
            leadactivity.SectionId = 0;
            leadactivity.ActionNote = "Add STC Details by for Project No: " + job.JobNumber;
            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            // await JobActive(job.Id); 
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        public async Task<List<JobLeadLookupTableDto>> GetPVDStatusList()
        {
            return await _pVDStatusRepository.GetAll()
                .Select(item => new JobLeadLookupTableDto
                {
                    Id = item.Id,
                    DisplayName = item.Name
                }).OrderBy(x => x.DisplayName).ToListAsync();
        }

        //GridConnectionTracker
        public async Task<PagedResultDto<GetJobForViewDto>> GetGridConnectionTracker(GetAllJobsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobTypeFk)
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.RoofTypeFk)
                        .Include(e => e.RoofAngleFk)
                        .Include(e => e.ElecDistributorFk)
                        .Include(e => e.LeadFk)
                        .Include(e => e.ElecRetailerFk)
                        .Include(e => e.PaymentOptionFk)
                        .Include(e => e.DepositOptionFk)
                        //.Include(e => e.MeterUpgradeFk)
                        //.Include(e => e.MeterPhaseFk)
                        .Include(e => e.PromotionOfferFk)
                        .Include(e => e.HouseTypeFk)
                        .Include(e => e.FinanceOptionFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) ||
                        e.LotNumber.Contains(input.Filter) || e.JobNumber.Contains(input.Filter) ||
                        e.OffPeakMeter.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.LeadFk.State == input.stateNameFilter)
                        .WhereIf(input.elecDistributorId != 0, e => e.ElecDistributorId == input.elecDistributorId)
                        //.WhereIf(input.jobStatusIDFilter != 0, e => e.JobStatusId == input.jobStatusIDFilter)
                        .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
                        .WhereIf(input.InstallerID != 0, e => e.InstallerId == input.InstallerID)
                        .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
                        .WhereIf(input.applicationstatus == 1, e => e.MeterApplyRef == null && e.InspectionDate == null)
                        .WhereIf(input.applicationstatus == 2, e => e.MeterApplyRef != null && e.InspectionDate != null)
                        .WhereIf(input.DateNameFilter == "DepositDate", e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateNameFilter == "ActiveDate", e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateNameFilter == "distAppliExpiryDate", e => e.DistExpiryDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DistExpiryDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateNameFilter == "InstallDate", e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateNameFilter == "InstallCmpltDate", e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .Where(e => e.JobStatusId == 8)
                        .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == j.LeadId).Select(e => e.OrganizationId).FirstOrDefault()) == input.OrganizationUnit);

            var pagedAndFilteredJobs = filteredJobs
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var jobs = from o in pagedAndFilteredJobs
                       join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                       from s1 in j1.DefaultIfEmpty()

                       join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                       from s2 in j2.DefaultIfEmpty()

                       join o2 in _pVDStatusRepository.GetAll() on o.PVDStatus equals o2.Id into pvd
                       from pvdval in pvd.DefaultIfEmpty()

                       join userreq in _userRepository.GetAll() on (int)o.InstallerId equals userreq.Id into user
                       from userval in user.DefaultIfEmpty()

                           //join o3 in _lookup_roofTypeRepository.GetAll() on o.RoofTypeId equals o3.Id into j3
                           //from s3 in j3.DefaultIfEmpty()

                           //join o4 in _lookup_roofAngleRepository.GetAll() on o.RoofAngleId equals o4.Id into j4
                           //from s4 in j4.DefaultIfEmpty()

                       join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                       from s5 in j5.DefaultIfEmpty()

                       join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                       from s6 in j6.DefaultIfEmpty()

                       join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                       from s7 in j7.DefaultIfEmpty()

                           //join o8 in _lookup_paymentOptionRepository.GetAll() on o.PaymentOptionId equals o8.Id into j8
                           //from s8 in j8.DefaultIfEmpty()

                           //join o9 in _lookup_depositOptionRepository.GetAll() on o.DepositOptionId equals o9.Id into j9
                           //from s9 in j9.DefaultIfEmpty()

                           //join o10 in _lookup_meterUpgradeRepository.GetAll() on o.MeterUpgradeId equals o10.Id into j10
                           //from s10 in j10.DefaultIfEmpty()

                           //join o11 in _lookup_meterPhaseRepository.GetAll() on o.MeterPhaseId equals o11.Id into j11
                           //from s11 in j11.DefaultIfEmpty()

                           //join o12 in _lookup_promotionOfferRepository.GetAll() on o.PromotionOfferId equals o12.Id into j12
                           //from s12 in j12.DefaultIfEmpty()

                           //join o13 in _lookup_houseTypeRepository.GetAll() on o.HouseTypeId equals o13.Id into j13
                           //from s13 in j13.DefaultIfEmpty()

                           //join o14 in _lookup_financeOptionRepository.GetAll() on o.FinanceOptionId equals o14.Id into j14
                           //from s14 in j14.DefaultIfEmpty()

                       select new GetJobForViewDto()
                       {
                           Job = new JobDto
                           {
                               RegPlanNo = o.RegPlanNo,
                               LotNumber = o.LotNumber,
                               Suburb = o.Suburb,
                               State = o.State,
                               UnitNo = o.UnitNo,
                               UnitType = o.UnitType,
                               NMINumber = o.NMINumber,
                               Id = o.Id,
                               ApplicationRefNo = o.ApplicationRefNo,
                               DistAppliedDate = o.DistAppliedDate,
                               Notes = o.Note,
                               InstallerNotes = o.InstallerNotes,
                               JobNumber = o.JobNumber,
                               MeterNumber = o.MeterNumber,
                               OldSystemDetails = o.OldSystemDetails,
                               PostalCode = o.PostalCode,
                               StreetName = o.StreetName,
                               StreetNo = o.StreetNo,
                               StreetType = o.StreetType,
                               LeadId = o.LeadId,
                               ElecDistributorId = o.ElecDistributorId,
                               ElecRetailerId = o.ElecRetailerId,
                               JobStatusId = o.JobStatusId,
                               JobTypeId = o.JobTypeId,
                               PVDStatus = o.PVDStatus,
                               STCAppliedDate = o.STCAppliedDate,
                               STCUploaddate = o.STCUploaddate,
                               STCUploadNumber = o.STCUploadNumber,
                               DistExpiryDate = o.DistExpiryDate,
                               InstallationDate = o.InstallationDate,
                               InstalledcompleteDate = o.InstalledcompleteDate,
                               PVDNumber = o.PVDNumber,
                               STCNotes = o.STCNotes,
                               SystemCapacity = o.SystemCapacity,
                               MeterApplyRef = o.MeterApplyRef,
                               InspectionDate = o.InspectionDate,
                               GridConnectionSmsSend = o.GridConnectionSmsSend,
                               GridConnectionEmailSendDate = o.GridConnectionSmsSendDate,
                               GridConnectionEmailSend = o.GridConnectionEmailSend,
                               GridConnectionSmsSendDate = o.GridConnectionEmailSendDate,
                               InstallerName = userval == null || userval.FullName == null ? "" : userval.FullName.ToString(),
                               PVDStatusName = pvdval == null || pvdval.Name == null ? "" : pvdval.Name.ToString(),
                               CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault()
                           },

                           LastComment = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           LastCommentDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),

                           JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                           JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                           ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                           LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                           Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                           Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                           ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
                           ActivityReminderTime = _leadactivityRepository.GetAll().Where(e => e.SectionId == 6 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                           ActivityDescription = _leadactivityRepository.GetAll().Where(e => e.SectionId == 6 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           ActivityComment = _leadactivityRepository.GetAll().Where(e => e.SectionId == 6 && e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                       };

            var totalCount = await filteredJobs.CountAsync();

            return new PagedResultDto<GetJobForViewDto>(
                totalCount,
                await jobs.ToListAsync()
            );
        }

        //STC Excel Export
        public async Task<FileDto> getGridConnectionTrackerToExcel(GetAllJobsForExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobTypeFk)
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.RoofTypeFk)
                        .Include(e => e.RoofAngleFk)
                        .Include(e => e.ElecDistributorFk)
                        .Include(e => e.LeadFk)
                        .Include(e => e.ElecRetailerFk)
                        .Include(e => e.PaymentOptionFk)
                        .Include(e => e.DepositOptionFk)
                        //.Include(e => e.MeterUpgradeFk)
                        //.Include(e => e.MeterPhaseFk)
                        .Include(e => e.PromotionOfferFk)
                        .Include(e => e.HouseTypeFk)
                        .Include(e => e.FinanceOptionFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) ||
                        e.LotNumber.Contains(input.Filter) || e.JobNumber.Contains(input.Filter) ||
                        e.OffPeakMeter.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.LeadFk.State == input.stateNameFilter)
                        .WhereIf(input.elecDistributorId != 0, e => e.ElecDistributorId == input.elecDistributorId)
                         .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
                        .WhereIf(input.InstallerID != 0, e => e.InstallerId == input.InstallerID)
                        .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
                        .WhereIf(input.applicationstatus == 1, e => e.MeterApplyRef == null && e.InspectionDate == null)
                        .WhereIf(input.applicationstatus == 2, e => e.MeterApplyRef != null && e.InspectionDate != null)
                        .WhereIf(input.DateNameFilter == "DepositDate", e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateNameFilter == "ActiveDate", e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateNameFilter == "distAppliExpiryDate", e => e.DistExpiryDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DistExpiryDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateNameFilter == "InstallDate", e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateNameFilter == "InstallCmpltDate", e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .Where(e => e.JobStatusId == 8)
                        .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == j.LeadId).Select(e => e.OrganizationId).FirstOrDefault()) == input.OrganizationUnit);

            var query = (from o in filteredJobs
                         join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o2 in _pVDStatusRepository.GetAll() on o.PVDStatus equals o2.Id into pvd
                         from pvdval in pvd.DefaultIfEmpty()

                         join userreq in _userRepository.GetAll() on (int)o.InstallerId equals userreq.Id into user
                         from userval in user.DefaultIfEmpty()

                             //join o3 in _lookup_roofTypeRepository.GetAll() on o.RoofTypeId equals o3.Id into j3
                             //from s3 in j3.DefaultIfEmpty()

                             //join o4 in _lookup_roofAngleRepository.GetAll() on o.RoofAngleId equals o4.Id into j4
                             //from s4 in j4.DefaultIfEmpty()

                         join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                         from s5 in j5.DefaultIfEmpty()

                         join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                         from s6 in j6.DefaultIfEmpty()

                         join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                         from s7 in j7.DefaultIfEmpty()

                             //join o8 in _lookup_paymentOptionRepository.GetAll() on o.PaymentOptionId equals o8.Id into j8
                             //from s8 in j8.DefaultIfEmpty()

                             //join o9 in _lookup_depositOptionRepository.GetAll() on o.DepositOptionId equals o9.Id into j9
                             //from s9 in j9.DefaultIfEmpty()

                             //join o10 in _lookup_meterUpgradeRepository.GetAll() on o.MeterUpgradeId equals o10.Id into j10
                             //from s10 in j10.DefaultIfEmpty()

                             //join o11 in _lookup_meterPhaseRepository.GetAll() on o.MeterPhaseId equals o11.Id into j11
                             //from s11 in j11.DefaultIfEmpty()

                             //join o12 in _lookup_promotionOfferRepository.GetAll() on o.PromotionOfferId equals o12.Id into j12
                             //from s12 in j12.DefaultIfEmpty()

                             //join o13 in _lookup_houseTypeRepository.GetAll() on o.HouseTypeId equals o13.Id into j13
                             //from s13 in j13.DefaultIfEmpty()

                             //join o14 in _lookup_financeOptionRepository.GetAll() on o.FinanceOptionId equals o14.Id into j14
                             //from s14 in j14.DefaultIfEmpty()

                         select new GetJobForViewDto()
                         {
                             Job = new JobDto
                             {
                                 RegPlanNo = o.RegPlanNo,
                                 LotNumber = o.LotNumber,
                                 Suburb = o.Suburb,
                                 State = o.State,
                                 UnitNo = o.UnitNo,
                                 UnitType = o.UnitType,
                                 NMINumber = o.NMINumber,
                                 Id = o.Id,
                                 ApplicationRefNo = o.ApplicationRefNo,
                                 DistAppliedDate = o.DistAppliedDate,
                                 Notes = o.Note,
                                 InstallerNotes = o.InstallerNotes,
                                 JobNumber = o.JobNumber,
                                 MeterNumber = o.MeterNumber,
                                 OldSystemDetails = o.OldSystemDetails,
                                 PostalCode = o.PostalCode,
                                 StreetName = o.StreetName,
                                 StreetNo = o.StreetNo,
                                 StreetType = o.StreetType,
                                 LeadId = o.LeadId,
                                 ElecDistributorId = o.ElecDistributorId,
                                 ElecRetailerId = o.ElecRetailerId,
                                 JobStatusId = o.JobStatusId,
                                 JobTypeId = o.JobTypeId,
                                 PVDStatus = o.PVDStatus,
                                 STCAppliedDate = o.STCAppliedDate,
                                 STCUploaddate = o.STCUploaddate,
                                 STCUploadNumber = o.STCUploadNumber,
                                 PVDNumber = o.PVDNumber,
                                 STCNotes = o.STCNotes,
                                 InstallerName = userval == null || userval.FullName == null ? "" : userval.FullName.ToString(),
                                 PVDStatusName = pvdval == null || pvdval.Name == null ? "" : pvdval.Name.ToString(),
                                 CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                                 SystemCapacity = o.SystemCapacity,
                                 MeterApplyRef = o.MeterApplyRef,
                             },
                             LastComment = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                             LastCommentDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),

                             JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                             JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                             ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                             LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                             Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                             Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                             ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
                         });

            var gridconnectionListDtos = await query.ToListAsync();

            return _leadsExcelExporter.GridConnectionExportToFile(gridconnectionListDtos);
        }

        public async Task Update_MeterDetails(CreateOrEditJobDto input)
        {
            var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, job);


            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 14;
            leadactivity.SectionId = 0;

            if (job.JobStatusId != input.JobStatusId)
            {
                PreviousJobStatus jobStstus = new PreviousJobStatus();
                jobStstus.JobId = job.Id;
                if (AbpSession.TenantId != null)
                {
                    jobStstus.TenantId = (int)AbpSession.TenantId;
                }
                jobStstus.CurrentID = 8;
                jobStstus.PreviousId = (int)job.JobStatusId;
                await _previousJobStatusRepository.InsertAsync(jobStstus);

                job.JobStatusId = 8;
            }
            leadactivity.ActionNote = "Meter Details are updated";

            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            //job.MeterTime = input.MeterTime;
            //job.MeterApplyRef = input.MeterApplyRef;
            //job.InspectorName = input.InspectorName;
            //job.InspectionDate = input.InspectionDate;
            await _jobRepository.UpdateAsync(job);
            // await JobActive(job.Id); 
            await _leadactivityRepository.InsertAsync(leadactivity);

        }

        public async Task<List<CommonLookupDto>> GetAllUsersTableDropdown()
        {
            return await _userRepository.GetAll()
                .Select(users => new CommonLookupDto
                {
                    Id = (int)users.Id,
                    DisplayName = users == null || users.FullName == null ? "" : users.FullName.ToString()
                }).ToListAsync();
        }

        public async Task<JobApplicationViewDto> GetDataForQuickViewByJobID(EntityDto input)
        {
            try
            {
                var job = await _jobRepository.FirstOrDefaultAsync(input.Id);

                var output = new JobApplicationViewDto { Job = ObjectMapper.Map<CreateOrEditJobDto>(job) };
                var ProductItemList = _jobProductItemRepository.GetAll().Where(j => j.JobId == input.Id).Select(e => e.ProductItemId).ToList();
                var ProductItemsList = _jobProductItemRepository.GetAll().Where(j => j.JobId == input.Id).ToList();
                var PanelDetail = _productItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 1).FirstOrDefault();
                var InverterDetail = _productItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 2).FirstOrDefault();
                var Map = _documentRepository.GetAll().Where(j => j.JobId == input.Id).ToList();
                var Finances = _financeOptionRepository.GetAll().Where(e => e.Id == job.FinanceOptionId).Select(e => e.Name).FirstOrDefault();
                var Dep = _depositOptionRepository.GetAll().Where(e => e.Id == job.DepositOptionId).Select(e => e.Name).FirstOrDefault();
                var Payment = _paymentOptionRepository.GetAll().Where(e => e.Id == job.PaymentOptionId).Select(e => e.Name).FirstOrDefault();

                var installerinvoice = _installerInvoiceRepository.GetAll().Where(e => e.JobId == input.Id).OrderByDescending(e => e.Id).FirstOrDefault();

                output.Doc = await GetJobDocumentsByJobId(job.Id);
                output.InvPaymentList = await GetInvoicesByJobId(job.Id);

                if (output.Job.JobTypeId != null)
                {
                    var _lookupJobType = await _lookup_jobTypeRepository.FirstOrDefaultAsync((int)output.Job.JobTypeId);
                    output.JobTypeName = _lookupJobType?.Name?.ToString();
                }

                if (output.Job.JobStatusId != null)
                {
                    var _lookupJobStatus = await _lookup_jobStatusRepository.FirstOrDefaultAsync((int)output.Job.JobStatusId);
                    output.JobStatusName = _lookupJobStatus?.Name?.ToString();
                }

                if (output.Job.RoofTypeId != null)
                {
                    var _lookupRoofType = await _lookup_roofTypeRepository.FirstOrDefaultAsync((int)output.Job.RoofTypeId);
                    output.RoofTypeName = _lookupRoofType?.Name?.ToString();
                }

                if (output.Job.RoofAngleId != null)
                {
                    var _lookupRoofAngle = await _lookup_roofAngleRepository.FirstOrDefaultAsync((int)output.Job.RoofAngleId);
                    output.RoofAngleName = _lookupRoofAngle?.Name?.ToString();
                }

                if (output.Job.ElecDistributorId != null)
                {
                    var _lookupElecDistributor = await _lookup_elecDistributorRepository.FirstOrDefaultAsync((int)output.Job.ElecDistributorId);
                    output.ElecDistributorName = _lookupElecDistributor?.Name?.ToString();
                }

                if (output.Job.LeadId != null)
                {
                    var _lookupLead = await _lookup_leadRepository.FirstOrDefaultAsync((int)output.Job.LeadId);
                    output.LeadCompanyName = _lookupLead?.CompanyName?.ToString();
                    output.Mobileno = _lookupLead?.Mobile?.ToString();
                    output.Phone = _lookupLead?.Phone?.ToString();
                    output.Email = _lookupLead?.Email?.ToString();
                }

                if (output.Job.ElecRetailerId != null)
                {
                    var _lookupElecRetailer = await _lookup_elecRetailerRepository.FirstOrDefaultAsync((int)output.Job.ElecRetailerId);
                    output.ElecRetailerName = _lookupElecRetailer?.Name?.ToString();
                }

                if (output.Job.PaymentOptionId != null)
                {
                    var _lookupPaymentOption = await _lookup_paymentOptionRepository.FirstOrDefaultAsync((int)output.Job.PaymentOptionId);
                    output.PaymentOptionName = _lookupPaymentOption?.Name?.ToString();
                }

                if (output.Job.DepositOptionId != null)
                {
                    var _lookupDepositOption = await _lookup_depositOptionRepository.FirstOrDefaultAsync((int)output.Job.DepositOptionId);
                    output.DepositOptionName = _lookupDepositOption?.Name?.ToString();
                }

                if (output.Job.MeterUpgradeId != null)
                {
                    var _lookupMeterUpgrade = await _lookup_meterUpgradeRepository.FirstOrDefaultAsync((int)output.Job.MeterUpgradeId);
                    output.MeterUpgradeName = _lookupMeterUpgrade?.Name?.ToString();
                }

                if (output.Job.MeterPhaseId != null)
                {
                    var _lookupMeterPhase = await _lookup_meterPhaseRepository.FirstOrDefaultAsync((int)output.Job.MeterPhaseId);
                    output.MeterPhaseName = _lookupMeterPhase?.Name?.ToString();
                }

                if (output.Job.PromotionOfferId != null)
                {
                    var _lookupPromotionOffer = await _lookup_promotionOfferRepository.FirstOrDefaultAsync((int)output.Job.PromotionOfferId);
                    output.PromotionOfferName = _lookupPromotionOffer?.Name?.ToString();
                }

                if (output.Job.HouseTypeId != null)
                {
                    var _lookupHouseType = await _lookup_houseTypeRepository.FirstOrDefaultAsync((int)output.Job.HouseTypeId);
                    output.HouseTypeName = _lookupHouseType?.Name?.ToString();
                }

                if (output.Job.FinanceOptionId != null)
                {
                    var _lookupFinanceOption = await _lookup_financeOptionRepository.FirstOrDefaultAsync((int)output.Job.FinanceOptionId);
                    output.FinanceOptionName = _lookupFinanceOption?.Name?.ToString();
                }

                if (output.Job.RefferedJobId != null)
                {
                    //var _lookupFinanceOption = await _lookup_financeOptionRepository.FirstOrDefaultAsync((int)output.Job.FinanceOptionId);
                    output.ReferralPayDate = job.ReferralPayDate;
                    output.ReferralPayment = job.ReferralPayment;
                    output.ReferralAmount = job.ReferralAmount;
                    output.ReferralRemark = job.ReferralRemark;
                }

                if (PanelDetail != null)
                {

                    var PanelQTY = ProductItemsList.Where(e => e.ProductItemId == PanelDetail.Id).Select(e => e.Quantity).FirstOrDefault();

                    output.Panel = PanelQTY + " x " + PanelDetail.Name;
                    output.PanelModelNo = PanelDetail.Model;
                    output.PanelQTY = PanelQTY;
                    output.PanelNAme = PanelDetail.Name;
                }
                if (InverterDetail != null)
                {
                    var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail.Id).Select(e => e.Quantity).FirstOrDefault();

                    output.Inverter = InverterQTY + " x " + InverterDetail.Name;
                    output.InverterModelNo = InverterDetail.Model;
                    output.InverterQTY = InverterQTY;
                    output.InverterNAme = InverterDetail.Name;
                    output.InverterOutPut = InverterDetail.Size;
                }
                //if (NearMap != null)
                //{

                //	//output.filepath = NearMap.FilePath;
                //	//output.filename = NearMap.FileName;
                //	//output.DocumentTypeId = NearMap.DocumentTypeId;
                //}

                if (job.PaymentOptionId == 1)
                {
                    output.Finance = "Cash";
                }
                else
                {
                    output.Finance = Payment + " with " + Dep + " for " + Finances;
                }
                output.RegPlanNo = job.RegPlanNo;
                output.LotNumber = job.LotNumber;
                output.Suburb = job.Suburb;
                output.State = job.State;
                output.UnitNo = job.UnitNo;
                output.UnitType = job.UnitType;
                output.NMINumber = job.NMINumber;
                output.ApplicationRefNo = job.ApplicationRefNo;
                output.DistAppliedDate = job.DistAppliedDate;
                output.Notes = job.Note;
                output.InstallerNotes = job.InstallerNotes;
                output.JobNumber = job.JobNumber;
                output.MeterNumber = job.PeakMeterNo;
                output.OldSystemDetails = job.OldSystemDetails;
                output.PostalCode = job.PostalCode;
                output.StreetName = job.StreetName;
                output.StreetNo = job.StreetNo;
                output.StreetType = job.StreetType;
                output.LeadId = job.LeadId;
                output.ElecDistributorId = job.ElecDistributorId;
                output.ElecRetailerId = job.ElecRetailerId;
                output.JobStatusId = job.JobStatusId;
                output.JobTypeId = job.JobTypeId;
                output.PVDStatus = job.PVDStatus;
                output.STCAppliedDate = job.STCAppliedDate;
                output.STCUploaddate = job.STCUploaddate;
                output.STCUploadNumber = job.STCUploadNumber;
                output.PVDNumber = job.PVDNumber;
                output.STCNotes = job.STCNotes;
                output.DistAppliedDate = job.DistAppliedDate;
                output.Note = job.Note;
                output.NMINumber = job.NMINumber;
                output.InstallerNotes = job.InstallerNotes;
                output.SystemCapacity = job.SystemCapacity;
                output.adress = job.Address;
                output.MeterNumber = job.PeakMeterNo;
                output.JobID = job.Id;
                output.DistApplied = job.DistApplied;
                output.DistExpiryDate = job.DistExpiryDate;
                output.ApplicationNotes = job.ApplicationNotes;
                output.InstallerDepartmentNotes = job.InstallationNotes;
                output.Price = job.TotalCost;
                output.FinanceNotes = job.FinanceNotes;
                output.RebateAppRef = job.RebateAppRef;
                output.ApprovalDate = job.ApprovalDate;
                output.ExpiryDate = job.ExpiryDate;
                output.VicRebate = job.VicRebate;
                output.SolarVICRebate = job.SolarVICRebate;
                output.SolarVICLoanDiscont = job.SolarVICLoanDiscont;
                output.SolarRebateStatus = job.SolarRebateStatus;
                output.vicRebateNotes = job.VicRebateNotes;
                output.FinanceApplicationDate = job.FinanceApplicationDate;
                output.FinancePurchaseNo = job.FinancePurchaseNo;
                output.DocumentsVeridiedDate = job.DocumentsVeridiedDate;
                output.CoolingoffPeriodEnd = job.CoolingoffPeriodEnd;
                if (output.Job.DocumentsVeridiedBy != null)
                {
                    var _lookupDocverify = await _userRepository.FirstOrDefaultAsync((int)output.Job.DocumentsVeridiedBy);
                    output.DocumentsVeridiedByName = _lookupDocverify?.Name?.ToString();
                }
                output.FinanceDocumentVerified = job.FinanceDocumentVerified;
                output.FinanceNotes = job.FinanceNotes;
                output.GridConnectionNotes = job.GridConnectionNotes;
                output.gridFilename = job.ApprovalLetter_Filename;
                output.gridFilePath = job.ApprovalLetter_FilePath;
                output.creationTime = job.CreationTime;
                output.createdBy = (_userRepository.GetAll().Where(e => e.Id == job.CreatorUserId).Select(e => e.UserName).FirstOrDefault());
                output.ApprovalFilelist = (from flist in _JobApprovalFileHistoryRepository.GetAll()
                                           where (flist.jobid == input.Id && flist.FieldName == "FileName" && flist.PrevValue != null && flist.CurValue != null)
                                           select new JobApprovalFileHistoryDto
                                           {
                                               fileName = flist.PrevValue,
                                               documentPath = job.ApprovalLetter_FilePath,
                                               creationTime = flist.CreationTime,
                                               createdBy = _userRepository.GetAll().Where(e => e.Id == flist.CreatorUserId).Select(e => e.UserName).FirstOrDefault(),
                                           }).OrderByDescending(e => e.creationTime).ToList();
                if (installerinvoice != null)
                {
                    output.ActualPanelsInstalled = installerinvoice.ActualPanels_Installed;
                    output.AllGoodNotGood = installerinvoice.AllGood_NotGood;
                    output.CES = installerinvoice.CES;
                    output.CxSign = installerinvoice.Cx_Sign;
                    output.EmailSent = installerinvoice.EmailSent;
                    output.Frontofproperty = installerinvoice.Front_of_property;
                    output.InstDesElesign = installerinvoice.Inst_Des_Ele_sign;
                    output.InstalaltionPic = installerinvoice.InstalaltionPic;
                    output.InstMaintInspPic = installerinvoice.Installation_Maintenance_Inspection;
                    output.Installerselfie = installerinvoice.Installer_selfie;
                    output.InverterSerialNo = installerinvoice.InverterSerialNo;
                    output.InvoiceAmount = installerinvoice.InvoiceAmount;
                    output.InvoiceNo = installerinvoice.InvoiceNo;
                    output.NoofPanelsinvoice = installerinvoice.Noof_Panels_invoice;
                    output.NoofPanelsPortal = installerinvoice.Noof_Panels_Portal;
                    output.NotesOrReasonforPending = installerinvoice.NotesOrReasonforPending;
                    output.PanelsSerialNo = installerinvoice.PanelsSerialNo;
                    output.Remarkifowing = installerinvoice.Remark_if_owing;
                    output.Splits = installerinvoice.Splits;
                    output.Traded = installerinvoice.Traded;
                    output.TravelKMfromWarehouse = installerinvoice.TravelKMfromWarehouse;
                    output.WiFiDongle = installerinvoice.Wi_FiDongle;
                    output.otherExtraInvoiceNumber = installerinvoice.otherExtraInvoiceNumber;
                    output.Filenameinv = installerinvoice.FileName;
                    output.FilePathinv = installerinvoice.FilePath; 
                    output.filelist = (from flist in _InstInvoiceHistoryRepository.GetAll()
                                       where (flist.InvoiceInstallerId == installerinvoice.Id && flist.FieldName == "FileName" && flist.PrevValue != null)
                                       select new installerinvoicefileDto
                                       {
                                           fileName = flist.PrevValue,
                                           documentPath = installerinvoice.FilePath,
                                           creationTime = flist.CreationTime,
                                           createdBy = _userRepository.GetAll().Where(e => e.Id == flist.CreatorUserId).Select(e => e.UserName).FirstOrDefault(),
                                       }).OrderByDescending(e => e.creationTime).ToList();
                }

                return output;
            }
            catch (Exception e) { throw e; }
        }

        public async Task<List<GetJobDocumentForView>> GetJobDocumentsByJobId(int jobId)
        {
            var jobdocuments = _documentRepository.GetAll().Include(e => e.JobFk)
                .Include(e => e.DocumentTypeFk)
                .OrderByDescending(e => e.Id)
                .Where(x => x.JobId == jobId);

            var filteredJobs = from o in jobdocuments
                               join o1 in _jobRepository.GetAll() on o.JobId equals o1.Id into j1
                               from s1 in j1.DefaultIfEmpty()

                               join o2 in _lookup_documentTypeRepository.GetAll() on o.DocumentTypeId equals o2.Id into j2
                               from s2 in j2.DefaultIfEmpty()

                               join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                               from s3 in j3.DefaultIfEmpty()

                               select new GetJobDocumentForView()
                               {
                                   Id = o.Id,
                                   DocumentTypeId = o.DocumentTypeId,
                                   MediaId = o.MediaId,
                                   JobId = o.JobId,
                                   FileName = o.FileName,
                                   FileType = o.FileType,
                                   DocumentPath = o.FilePath,
                                   DocumentTitle = s2 == null || s2.Title == null ? "" : s2.Title.ToString(),
                                   CreatedBy = s3.FullName,
                                   CreationTime = o.CreationTime
                               };

            return filteredJobs.ToList();
        }

        public async Task<Boolean> CheckExistJobsiteAddList(ExistJobSiteAddDto input)
        {
            var User = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

            IList<string> role = await _userManager.GetRolesAsync(User);

            if (role.Contains("Admin"))
            {
                return false;
            }

            if (input.jobTypeId == 2)
            {
                return false;
            }
            if (input.jobid == null)
            {
                if (!string.IsNullOrWhiteSpace(input.Suburb))
                {
                    string[] splitPipeValue = input.Suburb.Split('|');
                    if (splitPipeValue.Length > 0)
                    {
                        input.Suburbs = splitPipeValue[0].ToString().Trim();
                    }
                    else
                    {
                        input.Suburbs = input.Suburb;
                    }
                }
                var duplicateAdd = _jobRepository.GetAll().Where(e => (e.UnitNo == input.UnitNo && e.UnitType == input.UnitType && e.StreetNo == input.StreetNo && e.StreetName == input.StreetName
                               && e.StreetType == input.StreetType && e.Suburb == input.Suburb && e.State == input.State && e.PostalCode == input.PostCode) && e.LeadFk.OrganizationId == input.OrganizationId
                               ).Any();


                return duplicateAdd;

            }
            else
            {
                if (!string.IsNullOrWhiteSpace(input.Suburb))
                {
                    string[] splitPipeValue = input.Suburb.Split('|');
                    if (splitPipeValue.Length > 0)
                    {
                        input.Suburbs = splitPipeValue[0].ToString().Trim();
                    }
                    else
                    {
                        input.Suburbs = input.Suburb;
                    }
                }
                var duplicateAdd = _jobRepository.GetAll().Where(e => (e.UnitNo == input.UnitNo && e.UnitType == input.UnitType && e.StreetNo == input.StreetNo && e.StreetName == input.StreetName
                                && e.StreetType == input.StreetType && e.Suburb == input.Suburbs && e.State == input.State && e.PostalCode == input.PostCode) && e.LeadFk.OrganizationId == input.OrganizationId
                                && e.Id != input.jobid).Any();


                return duplicateAdd;

            }
        }

        /// <summary>
        /// Job Create Permission is only to Assigned User Only
        /// </summary>
        /// <param name="leadid">Lead Id</param>
        /// <returns></returns>
        public int checkjobcreatepermission(int leadid)
        {

            int? AssignUserid = _lookup_leadRepository.GetAll().Where(e => e.AssignToUserID == AbpSession.UserId && e.Id == leadid).Select(e => e.AssignToUserID).FirstOrDefault();
            if (AssignUserid > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Bind All User Dropdown For Admin For Other User Only User Will Bind
        /// </summary>
        /// <returns></returns>
        public async Task<List<CommonLookupDto>> GetAllUsers()
        {
            var User = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            if (role.Contains("Admin"))
            {
                return await _userRepository.GetAll()
                .Select(users => new CommonLookupDto
                {
                    Id = (int)users.Id,
                    DisplayName = users == null || users.FullName == null ? "" : users.FullName.ToString()
                }).ToListAsync();
            }
            else
            {
                return await _userRepository.GetAll()
                    .Where(e => e.Id == AbpSession.UserId)
                .Select(users => new CommonLookupDto
                {
                    Id = (int)users.Id,
                    DisplayName = users == null || users.FullName == null ? "" : users.FullName.ToString()
                }).ToListAsync();
            }

        }




        /// <summary>
        /// Get Total Count For All Tracker
        /// </summary>
        /// <param name="organizationid">Organization Id</param>
        /// <returns></returns>
        public async Task<TotalSammaryCountDto> getAllApplicationTrackerCount(int organizationid)
        {
            var output = new TotalSammaryCountDto();

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //Application Tracker Count
            var Doclist = _documentRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.DocumentTypeId == 2).Select(e => e.JobId).ToList();

            output.TotalAppTractorData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobStatusId >= 4 && e.MeterPhaseId != 0 && e.MeterPhaseId != null && e.PeakMeterNo != null && e.NMINumber != null && Doclist.Contains(e.Id) && e.LeadFk.OrganizationId == organizationid).Count();

            output.TotalAppTractorVerifyData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobStatusId >= 4 && e.MeterPhaseId != 0 && e.MeterPhaseId != null && e.PeakMeterNo != null && e.NMINumber != null && Doclist.Contains(e.Id) && e.LeadFk.OrganizationId == organizationid && e.ApplicationRefNo != null).Count();

            output.TotalAppTractornotVerifyData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobStatusId >= 4 && e.MeterPhaseId != 0 && e.MeterPhaseId != null && e.PeakMeterNo != null && e.NMINumber != null && Doclist.Contains(e.Id) && e.LeadFk.OrganizationId == organizationid && e.ApplicationRefNo == null).Count();

            output.TotalExpiryData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.DistExpiryDate != null && e.ActiveDate != null && e.DistExpiryDate >= DateTime.UtcNow && e.DistExpiryDate <= DateTime.UtcNow.AddDays(30) && (e.JobStatusId >= 4 && e.JobStatusId <= 7) && e.MeterPhaseId != 0 && e.MeterPhaseId != null && e.PeakMeterNo != null && e.NMINumber != null && Doclist.Contains(e.Id) && e.ApplicationRefNo != null && e.LeadFk.OrganizationId == organizationid).Count();

            output.TotalAppTractorAwaitingData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobStatusId >= 4 && (e.MeterPhaseId == 0 || e.MeterPhaseId == null || e.PeakMeterNo == null || e.NMINumber == null || !Doclist.Contains(e.Id)) && e.LeadFk.OrganizationId == organizationid).Count();

            //Job Active Tracker Count

            var MeterPhoto = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 3).Select(e => e.JobId).Distinct().ToList();
            var Deposites = _invoicePaymentRepository.GetAll().Select(e => e.JobId).Distinct().ToList();
            var SignedQuote = _quotationRepository.GetAll().Where(e => e.IsSigned == true).Select(e => e.JobId).Distinct().ToList();
            var doc = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 5).Select(e => e.JobId).Distinct().ToList();

            output.TotalActiveData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.IsActive == true && e.LeadFk.OrganizationId == organizationid).Count();
            output.TotalActiveVerifyData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.IsActive == true && e.ActiveDate != null && e.JobStatusId == 5 && e.LeadFk.OrganizationId == organizationid && e.ActiveDate != null).Count();
            output.TotalActiveDatanotVerifyData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.IsActive != true && e.JobStatusId == 4 && Deposites.Contains(e.Id) && (e.NMINumber != null && e.ElecRetailerId != 0 && e.ApplicationRefNo != null && e.MeterUpgradeId != null && e.MeterUpgradeId != 0 && MeterPhoto.Contains(e.Id) && (SignedQuote.Contains(e.Id) || doc.Contains(e.Id))) && (e.JobStatusId != 3 && e.JobStatusId != 8) && e.LeadFk.OrganizationId == organizationid && e.ActiveDate == null).Count();
            output.TotalActiveDatanotAwaitingData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.IsActive != true  && e.JobStatusId == 4 && Deposites.Contains(e.Id) && (e.NMINumber == null || e.ElecRetailerId == 0 || e.ApplicationRefNo == null || e.MeterUpgradeId == null || e.MeterUpgradeId == 0 || !MeterPhoto.Contains(e.Id) || !SignedQuote.Contains(e.Id) || !doc.Contains(e.Id)) && (e.JobStatusId != 3 && e.JobStatusId != 8) && e.LeadFk.OrganizationId == organizationid && Deposites.Contains(e.Id) && (e.NMINumber == null || e.ApplicationRefNo == null || e.MeterUpgradeId == null || e.MeterUpgradeId == 0 || !MeterPhoto.Contains(e.Id) || !SignedQuote.Contains(e.Id) || !doc.Contains(e.Id)  /*|| (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified)*/) && (e.JobStatusId != 3 && e.JobStatusId != 8)).Count();

            //Finance Tracker Count
            output.TotalfinanceData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.PaymentOptionId != null && e.PaymentOptionId != 1 && Deposites.Contains(e.Id) && e.LeadFk.OrganizationId == organizationid).Count();
            output.TotalfinanceVerifyData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.FinanceDocumentVerified == 1 && Deposites.Contains(e.Id) && e.LeadFk.OrganizationId == organizationid).Count();
            output.TotalfinanceDatanotVerifyData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.PaymentOptionId != null && e.PaymentOptionId != 1 && e.FinanceDocumentVerified != 1 && Deposites.Contains(e.Id) && e.LeadFk.OrganizationId == organizationid).Count();
            output.TotalfinanceQueryData = _jobRepository.GetAll()
               .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
               .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
               .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
               .Where(e => e.FinanceDocumentVerified == 3 && e.PaymentOptionId != null && e.PaymentOptionId != 1 && Deposites.Contains(e.Id) && e.LeadFk.OrganizationId == organizationid).Count();

            //Job Refund Tracker Count
            output.TotaljobrefundData = _jobRefundRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == organizationid).Count();
            output.TotaljobrefundVerifyData = _jobRefundRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == organizationid).Where(e => e.PaidDate != null).Count();
            output.TotaljobrefundnotVerifyData = _jobRefundRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == organizationid).Where(e => e.PaidDate == null).Count();

            //Invoice Payment Tracker Count
            output.TotalpaymentData = _invoicePaymentRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == organizationid).Count();
            output.TotalpaymentVerifyData = _invoicePaymentRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == organizationid).Where(e => e.IsVerified == true).Count();
            output.TotalpaymentnotVerifyData = _invoicePaymentRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(j => j.JobFk.LeadFk.OrganizationId == organizationid).Where(e => e.IsVerified == false).Count();


            output.TotalAmountOfInvoice = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.LeadFk.OrganizationId == organizationid && _invoicePaymentRepository.GetAll().Select(e => e.JobId).Contains(e.Id)).Select(e => e.TotalCost).Sum();



            output.totalrefundamount = _jobRefundRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(j => j.JobFk.LeadFk.OrganizationId == organizationid).Select(e => e.Amount).Sum();


            output.TotalAmountReceived = (_invoicePaymentRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(j => j.JobFk.LeadFk.OrganizationId == organizationid).Select(e => e.InvoicePayTotal).Sum()) - (output.totalrefundamount);
            output.BalanceOwning = (output.TotalAmountOfInvoice - output.TotalAmountReceived);

            output.TotalCancelAmount = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.LeadFk.OrganizationId == organizationid && e.JobStatusId == 3 && _invoicePaymentRepository.GetAll().Select(e => e.JobId).Contains(e.Id)).Select(e => e.TotalCost).Sum();

            output.TotalVICAmountOfInvoice = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.LeadFk.OrganizationId == organizationid && e.LeadFk.State == "VIC" && _invoicePaymentRepository.GetAll().Select(e => e.JobId).Contains(e.Id)).Select(e => e.SolarVICRebate).Sum();

            output.TotalVICAmountReceived = 0;
            output.TotalVICBAlOwning = (output.TotalVICAmountOfInvoice - output.TotalVICAmountReceived);

            output.TotalCustAmountOfInvoice = (_jobRepository.GetAll()
            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            .Where(e => e.LeadFk.OrganizationId == organizationid && e.LeadFk.State == "VIC" && _invoicePaymentRepository.GetAll().Select(e => e.JobId).Contains(e.Id)).Select(e => e.TotalCost).Sum()) - (output.TotalVICAmountOfInvoice);

            output.TotalCustAmountReceived = _invoicePaymentRepository.GetAll()
            .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
            .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
            .Where(j => j.JobFk.LeadFk.OrganizationId == organizationid && j.JobFk.LeadFk.State == "VIC").Select(e => e.InvoicePayTotal).Sum();

            output.TotalCustBAlOwning = (output.TotalCustAmountOfInvoice - output.TotalCustAmountReceived);



            //Freebies Tracker Count
            output.TotalfreebieData = _jobPromotionRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid && e.JobFk.TotalCost <= (_invoicePaymentRepository.GetAll().Where(i => i.JobId == e.JobFk.Id).Select(e => e.InvoicePayTotal).Sum())).Count();
            output.TotalAwaitingData = _jobPromotionRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid && e.JobFk.TotalCost > (_invoicePaymentRepository.GetAll().Where(i => i.JobId == e.JobFk.Id).Select(e => e.InvoicePayTotal).Sum())).Count();
            output.TotalfreebieVerifyData = _jobPromotionRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid && e.TrackingNumber != null && e.DispatchedDate != null && e.JobFk.TotalCost <= (_invoicePaymentRepository.GetAll().Where(i => i.JobId == e.JobFk.Id).Select(e => e.InvoicePayTotal).Sum())).Count();
            output.TotalfreebienotVerifyData = _jobPromotionRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid && e.TrackingNumber == null && e.DispatchedDate == null && e.JobFk.TotalCost <= (_invoicePaymentRepository.GetAll().Where(i => i.JobId == e.JobFk.Id).Select(e => e.InvoicePayTotal).Sum())).Count();

            /// Job grid
            var Panels = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1).Select(e => e.Id).ToList();
            var Inverts = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2).Select(e => e.Id).ToList();
            output.TotalNoOfPanel = _jobProductItemRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid && Panels.Contains((int)e.ProductItemId)).Select(e => e.Quantity).Sum();

            output.TotalNoOfInvert = _jobProductItemRepository.GetAll()
               .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
               .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
               .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
               .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid && Inverts.Contains((int)e.ProductItemId)).Select(e => e.Quantity).Sum();

            output.TotalSystemCapacity = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.LeadFk.OrganizationId == organizationid).Select(e => e.SystemCapacity).Sum();

            output.TotalCost = _jobRepository.GetAll()
               .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
               .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
               .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
               .Where(e => e.LeadFk.OrganizationId == organizationid).Select(e => e.TotalCost).Sum();

            output.Price = Math.Round(Convert.ToDecimal(output.TotalCost / output.TotalSystemCapacity));

            output.DepositeReceived = _jobRepository.GetAll()
               .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
               .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
               .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
               .Where(e => e.LeadFk.OrganizationId == organizationid && e.JobStatusId == 4).Select(e => e.Id).Count();

            output.Active = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.LeadFk.OrganizationId == organizationid && e.JobStatusId == 5).Select(e => e.Id).Count();

            output.InstallJob = _jobRepository.GetAll()
               .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
               .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
               .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
               .Where(e => e.LeadFk.OrganizationId == organizationid && e.JobStatusId == 8).Select(e => e.Id).Count();

            /////Promotion
            //var ids = _lookup_promotionRepository.GetAll().Where(e => e.OrganizationID == organizationid).Select(e => e.Id).ToList();
            //var JobStatus = _jobRepository.GetAll().Where(e => e.JobStatusId == 6).Select(e => e.LeadId).ToList();
            //var Leads = _lookup_leadRepository.GetAll().Select(e => e.Id).ToList();
            //var promostionids = _PromotionUsersRepository.GetAll().Select(e => e.PromotionId).ToList();

            //output.TotalPromotion = _lookup_promotionRepository.GetAll().Where(e => e.OrganizationID == organizationid && promostionids.Contains(e.Id)).Select(e => e.LeadCount).Sum();
            //output.NoReplay = _PromotionUsersRepository.GetAll().Where(e => e.ResponseMessage == null && ids.Contains((int)e.PromotionId)).Select(e => (int)e.Id).Count();
            //output.InterestedCount = _PromotionUsersRepository.GetAll().Where(e => e.PromotionResponseStatusId == 1 && ids.Contains((int)e.PromotionId)).Select(e => (int)e.Id).Count();
            //output.NotInterestedCount = _PromotionUsersRepository.GetAll().Where(e => e.PromotionResponseStatusId == 2 && ids.Contains((int)e.PromotionId)).Select(e => (int)e.Id).Count();
            //output.OtherCount = _PromotionUsersRepository.GetAll().Where(e => e.PromotionResponseStatusId == 3 && ids.Contains((int)e.PromotionId)).Select(e => (int)e.Id).Count();
            //output.Unhandle = _PromotionUsersRepository.GetAll().Where(e => e.LeadFk.LeadStatusId == 2 && ids.Contains((int)e.PromotionId)).Select(e => (int)e.Id).Count();
            //output.PromotionSold = _PromotionUsersRepository.GetAll().Where(e => e.LeadFk.LeadStatusId == 9 && ids.Contains((int)e.PromotionId)).Select(e => (int)e.Id).Count();
            //output.PromotionProject = _PromotionUsersRepository.GetAll().Where(e => JobStatus.Contains((int)e.LeadId) && ids.Contains((int)e.PromotionId)).Select(e => (int)e.Id).Count();

            ///STC
            output.PendingStc = _jobRepository.GetAll()
              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
              .Where(e => e.LeadFk.OrganizationId == organizationid && e.PVDStatus == 5).Select(e => e.STC).Sum();
            output.AprrovedStc = _jobRepository.GetAll()
             .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
             .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
             .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
             .Where(e => e.LeadFk.OrganizationId == organizationid && e.PVDStatus == 4).Select(e => e.STC).Sum();
            output.TotalStc = _jobRepository.GetAll()
             .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
             .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
             .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
             .Where(e => e.LeadFk.OrganizationId == organizationid && e.PVDStatus != null).Select(e => e.STC).Sum();
            output.Compliancestc = _jobRepository.GetAll()
             .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
             .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
             .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
             .Where(e => e.LeadFk.OrganizationId == organizationid && e.PVDStatus == 2).Select(e => e.STC).Sum();

            output.FailedStc = _jobRepository.GetAll()
             .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
             .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
             .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
             .Where(e => e.LeadFk.OrganizationId == organizationid && e.PVDStatus == 3).Select(e => e.STC).Sum();

            output.TradedStc = _jobRepository.GetAll()
             .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
             .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
             .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
             .Where(e => e.LeadFk.OrganizationId == organizationid && e.PVDStatus == 6).Select(e => e.STC).Sum();



            ///InstallerInvoice

            output.TotalinsInvoice = _jobInstallerInvoiceRepository.GetAll()
            .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
            .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
            .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid).Select(e => e.Id).Count();

            output.PendinginsInvoice = _jobInstallerInvoiceRepository.GetAll()
           .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
           .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
           .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
           .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid && e.IsVerify == false).Select(e => e.Id).Count();

            output.VerifiedinsInvoice = _jobInstallerInvoiceRepository.GetAll()
           .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
           .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
           .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
           .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid && e.IsVerify == true).Select(e => e.Id).Count();

            //grid connection tracker count
            output.PendingGrid = _jobRepository.GetAll()
              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
              .Where(e => e.LeadFk.OrganizationId == organizationid && e.MeterApplyRef == null && e.InspectionDate == null && e.JobStatusId == 8).Select(e => e.Id).Count();

            output.AprrovedGrid = _jobRepository.GetAll()
             .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
             .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
             .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
             .Where(e => e.LeadFk.OrganizationId == organizationid && e.MeterApplyRef != null && e.InspectionDate != null && e.JobStatusId == 8).Select(e => e.Id).Count();

            output.TotalGrid = _jobRepository.GetAll()
             .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
             .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
             .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
             .Where(e => e.LeadFk.OrganizationId == organizationid && (e.JobStatusId == 8)).Select(e => e.Id).Count();
            //.Where(e => e.LeadFk.OrganizationId == organizationid && ( e.JobStatusId == 8) || (e.JobStatusId == 6)).Select(e => e.Id).Count();

            output.AwaitingGrid = _jobRepository.GetAll()
             .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
             .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
             .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
             .Where(e => e.LeadFk.OrganizationId == organizationid && e.JobStatusId == 6).Select(e => e.Id).Count();


            output.TotalReferral = _jobRepository.GetAll()
            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            .Where(e => e.LeadFk.OrganizationId == organizationid && e.LeadFk.LeadSource == "Referral" && e.LeadFk.LeadSourceId == 2 && e.JobStatusId >= 4).Select(e => e.Id).Count();

            output.AwaitingReferral = _jobRepository.GetAll()
            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            .Where(e => e.LeadFk.OrganizationId == organizationid && e.LeadFk.LeadSource == "Referral" && e.LeadFk.LeadSourceId == 2 && e.ReferralPayment == false && e.BsbNo != null && e.AccountNo != null && ((_invoicePaymentRepository.GetAll().Where(e => e.JobId == e.Id).Select(e => e.InvoicePayTotal).Sum()) != e.TotalCost) && e.JobStatusId >= 4).Select(e => e.Id).Count();

            output.readytopayReferral = _jobRepository.GetAll()
            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            .Where(e => e.LeadFk.OrganizationId == organizationid && e.LeadFk.LeadSource == "Referral" && e.LeadFk.LeadSourceId == 2 && e.ReferralPayment == false && e.BsbNo != null && e.AccountNo != null && ((_invoicePaymentRepository.GetAll().Where(e => e.JobId == e.Id).Select(e => e.InvoicePayTotal).Sum()) == e.TotalCost)).Select(e => e.Id).Count();

            output.paidReferral = _jobRepository.GetAll()
            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            .Where(e => e.LeadFk.OrganizationId == organizationid && e.LeadFk.LeadSource == "Referral" && e.LeadFk.LeadSourceId == 2 && e.ReferralPayment == true && e.ReferralPayDate != null).Select(e => e.Id).Count();

             
            return output;
        }

        public async Task<GetDetailForJobEditOutPut> GetJobDetailEditByJobID(EntityDto input)
        {
            var job = await _jobRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetDetailForJobEditOutPut { Job = ObjectMapper.Map<CreateOrEditJobDto>(job) };
            output.DistApplied = job.DistApplied;
            output.ApplicationRefNo = job.ApplicationRefNo;
            output.DistApproveBy = job.DistApproveBy;
            output.ExpiryDate = job.ExpiryDate;
            output.AppliedBy = job.AppliedBy;
            output.ApprovalDate = job.ApprovalDate;
            output.EmpId = job.EmpId;
            output.PaidAmmount = job.PaidAmmount;
            output.Paidby = job.Paidby;
            output.PaidStatus = job.PaidStatus;
            output.InvRefNo = job.InvRefNo;
            output.InvPaidDate = job.InvPaidDate;
            output.FinanceApplicationDate = job.FinanceApplicationDate;
            output.FinancePurchaseNo = job.FinancePurchaseNo;
            output.DocumentsVeridiedDate = job.DocumentsVeridiedDate;
            output.CoolingoffPeriodEnd = job.CoolingoffPeriodEnd;
            output.FinanceDocumentVerified = job.FinanceDocumentVerified;
            output.DocumentsVeridiedBy = job.DocumentsVeridiedBy;
            output.FinanceNotes = job.FinanceNotes;
            output.StcUploaddate = job.STCUploaddate;
            output.StcUploadNumber = job.STCUploadNumber;
            output.StcAppliedDate = job.STCAppliedDate;
            output.PvdStatus = job.PVDStatus;
            output.PvdNumber = job.PVDNumber;
            output.StcNotes = job.STCNotes;
            output.MeterTime = job.MeterTime;
            output.MeterApplyRef = job.MeterApplyRef;
            output.InspectorName = job.InspectorName;
            output.InspectionDate = job.InspectionDate;
            output.ActiveDate = job.ActiveDate;
            if (output.PvdStatus != null)
            {
                var _lookupFinanceOption = await _pVDStatusRepository.FirstOrDefaultAsync((int)output.PvdStatus);
                output.PvdStatusName = _lookupFinanceOption?.Name?.ToString();
            }
            if (output.DocumentsVeridiedBy != null)
            {
                var _lookupdocverify = await _userRepository.FirstOrDefaultAsync((int)output.DocumentsVeridiedBy);
                output.DocumentsVeridiedByName = _lookupdocverify?.Name?.ToString();
            }

            if (output.DistApproveBy != null)
            {
                var _lookupdistaapprove = await _userRepository.FirstOrDefaultAsync((int)output.DistApproveBy);
                output.DistApproveByName = _lookupdistaapprove?.Name?.ToString();
            }
            if (output.EmpId != null)
            {
                var _lookupemployee = await _userRepository.FirstOrDefaultAsync((int)output.EmpId);
                output.EployeeByName = _lookupemployee?.Name?.ToString();
            }
            if (output.AppliedBy != null)
            {
                var _lookupapplyby = await _userRepository.FirstOrDefaultAsync((int)output.AppliedBy);
                output.ApplyByName = _lookupapplyby?.Name?.ToString();
            }
            output.Refund = await GetJobRefundByJobId(input.Id);
            output.InvoicePayment = await GetInvoicesByJobId(input.Id);
            output.JobPromotion = await GetjobpromotionByJobId(input.Id);

            return output;
        }

        public async Task<List<CreateOrEditJobRefundDto>> GetJobRefundByJobId(int jobid)
        {
            var filteredInvoicePayments = _jobRefundRepository.GetAll()
                        .Where(e => e.JobId == jobid);
            var refunds = from o in filteredInvoicePayments
                          select new CreateOrEditJobRefundDto()
                          {
                              Id = o.Id,
                              PaidDate = o.PaidDate,
                              PaymentOptionId = o.PaymentOptionId,
                              RefundReasonId = o.RefundReasonId,
                              Remarks = o.Remarks,
                              PaymentOptionName = _lookup_paymentOptionRepository.GetAll().Where(e => e.Id == o.PaymentOptionId).Select(e => e.Name).FirstOrDefault(),
                          };
            return refunds.ToList();
        }

        public async Task<List<DetailDto>> GetInvoicesByJobId(int jobid)
        {
            var filteredInvoicePayments = _invoicePaymentRepository.GetAll()
                        .Include(e => e.JobFk)
                        .Include(e => e.UserFk)
                        .Include(e => e.VerifiedByFk)
                        .Include(e => e.RefundByFk)
                        .Include(e => e.InvoicePaymentMethodFk)
                        .OrderByDescending(e => e.Id)
                        .Where(e => e.JobId == jobid);

            var invoicePayments = from o in filteredInvoicePayments
                                  select new DetailDto()
                                  {
                                      InvoicePayExGST = o.InvoicePayExGST,
                                      InvoicePayGST = o.InvoicePayGST,
                                      InvoicePayTotal = o.InvoicePayTotal,
                                      InvoicePayDate = o.InvoicePayDate,
                                      CCSurcharge = o.CCSurcharge,
                                      VerifiedOn = o.VerifiedOn,
                                      PaymentNumber = o.PaymentNumber,
                                      IsVerified = o.IsVerified,
                                      ReceiptNumber = o.ReceiptNumber,
                                      ActualPayDate = o.ActualPayDate,
                                      PaidComment = o.PaidComment,
                                      PaymentNote = o.PaymentNote,
                                      CreationTime = o.CreationTime,
                                      CreatedBy = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault()
                                  };
            return invoicePayments.ToList();
        }

        public async Task<List<DetailJobPromotionDto>> GetjobpromotionByJobId(int jobid)
        {
            var filteredInvoicePayments = _jobPromotionRepository.GetAll()
                        .Where(e => e.JobId == jobid);

            var invoicePayments = from o in filteredInvoicePayments
                                  select new DetailJobPromotionDto()
                                  {
                                      FreebieTransportId = o.FreebieTransportId,
                                      DispatchedDate = o.DispatchedDate,
                                      Description = o.Description,
                                      TrackingNumber = o.TrackingNumber,
                                      JobId = o.JobId,
                                      Name = _freebieTransportRepository.GetAll().Where(e => e.Id == o.FreebieTransportId).Select(e => e.Name).FirstOrDefault()
                                  };
            return invoicePayments.ToList();
        }

        public async Task<bool> CheckValidation(CreateOrEditJobDto input)
        {
            if (input.JobProductItems[0].Quantity != null && (input.BasicCost > 0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// Sms send From allTrackers
        public async Task SendSms(SmsEmailDto input)
        {
            if (input.TrackerId == 2)
            {
                var jobPromotion = await _jobPromotionRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditJobPromotionDto>(jobPromotion);
                var link = _freebieTransportRepository.GetAll().Where(e => e.Id == output.FreebieTransportId).Select(e => e.TransportLink).FirstOrDefault();
                var leadid = _jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                var mobilenumber = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Mobile).FirstOrDefault();
                var Body = input.Body + "You TransportLink Is: " + link + "/" + output.TrackingNumber + ".";
                if (!string.IsNullOrEmpty(mobilenumber))
                {
                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 6;
                    leadactivity.ActionNote = "SMS Send From Freebies";
                    leadactivity.LeadId = (int)leadid;
                    leadactivity.Subject = Body;
                    if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                    {
                        leadactivity.TemplateId = input.SMSTemplateId;
                    }
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    else
                    {
                        leadactivity.TemplateId = 0;
                    }
                    leadactivity.Body = Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                    SendSMSInput sendSMSInput = new SendSMSInput();
                    sendSMSInput.PhoneNumber = mobilenumber;
                    sendSMSInput.Text = Body;
                    sendSMSInput.ActivityId = leadactivity.Id;
                    await _applicationSettings.SendSMS(sendSMSInput);

                    output.SmsSend = true;
                    output.SmsSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobPromotion);
                }
            }
            else if (input.TrackerId == 4)
            {
                var jobRefund = await _jobRefundRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditJobRefundDto>(jobRefund);
                var leadid = _jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                var mobilenumber = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Mobile).FirstOrDefault();
                if (!string.IsNullOrEmpty(mobilenumber))
                {
                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 6;
                    leadactivity.ActionNote = "Sms Send From JobRefund";
                    leadactivity.LeadId = (int)leadid;
                    leadactivity.Subject = input.Body;
                    if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                    {
                        leadactivity.TemplateId = input.SMSTemplateId;
                    }
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    else
                    {
                        leadactivity.TemplateId = 0;
                    }
                    leadactivity.Body = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                    SendSMSInput sendSMSInput = new SendSMSInput();
                    sendSMSInput.PhoneNumber = mobilenumber;
                    sendSMSInput.Text = input.Body;
                    sendSMSInput.ActivityId = leadactivity.Id;
                    await _applicationSettings.SendSMS(sendSMSInput);

                    output.JobRefundSmsSend = true;
                    output.JobRefundSmsSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobRefund);
                }
            }
            else if (input.TrackerId == 6)
            {
                var jobinvoice = await _invoicePaymentRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditInvoicePaymentDto>(jobinvoice);
                var leadid = _jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                var mobilenumber = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Mobile).FirstOrDefault();
                if (!string.IsNullOrEmpty(mobilenumber))
                {
                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 6;
                    leadactivity.ActionNote = "Sms Send From Invoice";
                    leadactivity.LeadId = (int)leadid;
                    leadactivity.Subject = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                    {
                        leadactivity.TemplateId = input.SMSTemplateId;
                    }
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    else
                    {
                        leadactivity.TemplateId = 0;
                    }
                    leadactivity.Body = input.Body;
                    await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                    SendSMSInput sendSMSInput = new SendSMSInput();
                    sendSMSInput.PhoneNumber = mobilenumber;
                    sendSMSInput.Text = input.Body;
                    sendSMSInput.ActivityId = leadactivity.Id;
                    await _applicationSettings.SendSMS(sendSMSInput);

                    output.InvoiceSmsSend = true;
                    output.InvoiceSmsSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobinvoice);
                }
            }
            else if (input.TrackerId == 11)
            {
                var Jobid = _jobRepository.GetAll().Where(e => e.LeadId == input.LeadId).Select(e => e.Id).FirstOrDefault();
                var job = await _jobRepository.FirstOrDefaultAsync(Jobid);
                var output = ObjectMapper.Map<CreateOrEditJobDto>(job);
                var mobilenumber = _lookup_leadRepository.GetAll().Where(e => e.Id == job.LeadId).Select(e => e.Mobile).FirstOrDefault();
                if (!string.IsNullOrEmpty(mobilenumber))
                {
                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 6;
                    leadactivity.ActionNote = "Sms Send From PendingInstallation";
                    leadactivity.LeadId = (int)job.LeadId;
                    leadactivity.Subject = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                    {
                        leadactivity.TemplateId = input.SMSTemplateId;
                    }
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    else
                    {
                        leadactivity.TemplateId = 0;
                    }
                    leadactivity.Body = input.Body;
                    await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                    SendSMSInput sendSMSInput = new SendSMSInput();
                    sendSMSInput.PhoneNumber = mobilenumber;
                    sendSMSInput.Text = input.Body;
                    sendSMSInput.ActivityId = leadactivity.Id;
                    await _applicationSettings.SendSMS(sendSMSInput);

                    output.PendingInstallerSmsSend = true;
                    output.PendingInstallerSmsSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, job);
                }
            }
            else if (input.TrackerId == 26)  // InstallerInvoice
            {
                var jobinvoice = await _jobInstallerInvoiceRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditInvoicePaymentDto>(jobinvoice);
                var leadid = _jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                var mobilenumber = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Mobile).FirstOrDefault();
                if (!string.IsNullOrEmpty(mobilenumber))
                {
                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 26;
                    leadactivity.ActionNote = "Sms Send From InstallerInvoice";
                    leadactivity.LeadId = (int)leadid;
                    leadactivity.Subject = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                    {
                        leadactivity.TemplateId = input.SMSTemplateId;
                    }
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    else
                    {
                        leadactivity.TemplateId = 0;
                    }
                    leadactivity.Body = input.Body;
                    await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                    SendSMSInput sendSMSInput = new SendSMSInput();
                    sendSMSInput.PhoneNumber = mobilenumber;
                    sendSMSInput.Text = input.Body;
                    sendSMSInput.ActivityId = leadactivity.Id;
                    await _applicationSettings.SendSMS(sendSMSInput);

                    output.InvoiceSmsSend = true;
                    output.InvoiceSmsSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobinvoice);
                }
            }
            else
            {
                var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)input.LeadId);
                if (!string.IsNullOrEmpty(lead.Mobile))
                {
                    var Jobid = _jobRepository.GetAll().Where(e => e.LeadId == input.LeadId).Select(e => e.Id).FirstOrDefault();
                    var job = await _jobRepository.FirstOrDefaultAsync(Jobid);
                    var output = ObjectMapper.Map<CreateOrEditJobDto>(job);
                    if (input.TrackerId == 1 && job.DistApproveDate != null) // ApplicarionTrackerid
                    {
                        output.SmsSend = true;
                        output.SmsSendDate = DateTime.UtcNow;
                    }

                    if (input.TrackerId == 3) // FinanceTrackerid
                    {
                        output.FinanceSmsSend = true;
                        output.FinanceSmsSendDate = DateTime.UtcNow;
                    }

                    if (input.TrackerId == 5)  // JobActiveTrackerid
                    {
                        output.JobActiveSmsSend = true;
                        output.JobActiveSmsSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 7)  // GridConnectionid
                    {
                        output.GridConnectionSmsSend = true;
                        output.GridConnectionSmsSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 8)  // StcTrackerid
                    {
                        output.StcSmsSend = true;
                        output.StcSmsSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 9)  // JobGridId
                    {
                        output.JobGridSmsSend = true;
                        output.JobGridSmsSendDate = DateTime.UtcNow;
                    }
                    ObjectMapper.Map(output, job);

                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 6;
                    leadactivity.ActionNote = "Sms Send From Tacker";
                    leadactivity.LeadId = (int)input.LeadId;
                    leadactivity.Subject = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                    {
                        leadactivity.TemplateId = input.SMSTemplateId;
                    }
                    else
                    {
                        leadactivity.TemplateId = 0;
                    }
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    leadactivity.Body = input.Body;
                    await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                    SendSMSInput sendSMSInput = new SendSMSInput();
                    sendSMSInput.PhoneNumber = lead.Mobile;
                    sendSMSInput.Text = input.Body;
                    sendSMSInput.ActivityId = leadactivity.Id;
                    await _applicationSettings.SendSMS(sendSMSInput);
                }
            }
        }

        /// Email send From allTrackers
        public async Task SendEmail(SmsEmailDto input)
        {

            if (input.TrackerId == 2)
            {
                var jobPromotion = await _jobPromotionRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditJobPromotionDto>(jobPromotion);
                var link = _freebieTransportRepository.GetAll().Where(e => e.Id == output.FreebieTransportId).Select(e => e.TransportLink).FirstOrDefault();
                var leadid = _jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                var Email = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Email).FirstOrDefault();
                var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)leadid);
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();

                int? TemplateId;
                if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
                {
                    TemplateId = input.EmailTemplateId;
                }
                else
                {
                    TemplateId = 0;

                }

                if (!string.IsNullOrEmpty(Email))
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                        To = { Email },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                    output.EmailSend = true;
                    output.EmailSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobPromotion);


                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 7;
                    leadactivity.ActionNote = "Email Send From Freebies";
                    leadactivity.LeadId = (int)leadid;
                    leadactivity.TemplateId = TemplateId;
                    leadactivity.Subject = input.Subject;
                    leadactivity.Body = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }
            }
            else if (input.TrackerId == 4)
            {
                var jobRefund = await _jobRefundRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditJobRefundDto>(jobRefund);
                var leadid = _jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                var Email = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Email).FirstOrDefault();
                var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)leadid);
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();


                int? TemplateId;
                if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
                {
                    TemplateId = input.EmailTemplateId;
                }
                else
                {
                    TemplateId = 0;
                }

                if (!string.IsNullOrEmpty(Email))
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                        To = { Email },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                    output.JobRefundEmailSend = true;
                    output.JobRefundSmsSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobRefund);

                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 7;
                    leadactivity.ActionNote = "Email Send From Refund";
                    leadactivity.LeadId = (int)leadid;
                    leadactivity.TemplateId = TemplateId;
                    leadactivity.Subject = input.Subject;
                    leadactivity.Body = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }
            }
            else if (input.TrackerId == 6)
            {
                var jobinvoice = await _invoicePaymentRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditInvoicePaymentDto>(jobinvoice);
                var leadid = _jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                var Email = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Email).FirstOrDefault();
                var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)leadid);
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();


                int? TemplateId;
                if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
                {
                    TemplateId = input.EmailTemplateId;
                }
                else
                {
                    TemplateId = 0;
                }

                if (!string.IsNullOrEmpty(Email))
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                        To = { Email },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                    output.InvoiceEmailSend = true;
                    output.InvoiceEmailSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobinvoice);

                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 7;
                    leadactivity.ActionNote = "Email Send From Invoice";
                    leadactivity.LeadId = (int)leadid;
                    leadactivity.TemplateId = TemplateId;
                    leadactivity.Subject = input.Subject;
                    leadactivity.Body = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }
            }
            else if (input.TrackerId == 11)
            {
                var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)input.LeadId);
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();

                var Jobid = _jobRepository.GetAll().Where(e => e.LeadId == input.LeadId).Select(e => e.Id).FirstOrDefault();
                var job = await _jobRepository.FirstOrDefaultAsync(Jobid);
                var output = ObjectMapper.Map<CreateOrEditJobDto>(job);

                int? TemplateId;
                if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
                {
                    TemplateId = input.EmailTemplateId;
                }
                else
                {
                    TemplateId = 0;
                }

                if (!string.IsNullOrEmpty(lead.Email))
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                        To = { lead.Email },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                    output.PendingInstallerEmailSend = true;
                    output.PendingInstallerEmailSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, job);

                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 7;
                    leadactivity.ActionNote = "Email Send From PendingInstalltion";
                    leadactivity.LeadId = (int)job.LeadId;
                    leadactivity.TemplateId = TemplateId;
                    leadactivity.Subject = input.Subject;
                    leadactivity.Body = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }
            }
            else if (input.TrackerId == 26)
            {
                var jobinvoice = await _jobInstallerInvoiceRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditInvoicePaymentDto>(jobinvoice);
                var leadid = _jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                var Email = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Email).FirstOrDefault();
                var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)leadid);
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();


                int? TemplateId;
                if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
                {
                    TemplateId = input.EmailTemplateId;
                }
                else
                {
                    TemplateId = 0;
                }

                if (!string.IsNullOrEmpty(Email))
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                        To = { Email },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                    output.InvoiceEmailSend = true;
                    output.InvoiceEmailSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobinvoice);

                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 7;
                    leadactivity.ActionNote = "Email Send From InstallerInvoice";
                    leadactivity.LeadId = (int)leadid;
                    leadactivity.TemplateId = TemplateId;
                    leadactivity.Subject = input.Subject;
                    leadactivity.Body = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }
            }
            else
            {
                var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)input.LeadId);
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
                int? TemplateId;
                if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
                {
                    TemplateId = input.EmailTemplateId;
                }
                else
                {
                    TemplateId = 0;
                }

                if (!string.IsNullOrEmpty(lead.Email))
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                        To = { lead.Email },
                        Subject = input.Subject,//EmailBody.TemplateName + "The Solar Product Lead Notification",
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);

                    var Jobid = _jobRepository.GetAll().Where(e => e.LeadId == input.LeadId).Select(e => e.Id).FirstOrDefault();
                    var job = await _jobRepository.FirstOrDefaultAsync(Jobid);
                    var output = ObjectMapper.Map<CreateOrEditJobDto>(job);
                    if (input.TrackerId == 1 && job.DistApproveDate != null)
                    {
                        output.EmailSend = true;
                        output.EmailSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 3)
                    {
                        output.FinanceEmailSend = true;
                        output.FinanceEmailSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 5)
                    {
                        output.JobActiveEmailSend = true;
                        output.JobActiveEmailSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 7)
                    {
                        output.GridConnectionEmailSend = true;
                        output.GridConnectionEmailSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 8)
                    {
                        output.StcEmailSend = true;
                        output.StcEmailSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 9)
                    {
                        output.JobGridEmailSend = true;
                        output.JobGridEmailSendDate = DateTime.UtcNow;
                    }
                    ObjectMapper.Map(output, job);

                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 7;
                    leadactivity.ActionNote = "Email Send From Invoice";
                    leadactivity.LeadId = (int)input.LeadId;
                    leadactivity.TemplateId = TemplateId;
                    leadactivity.Subject = input.Subject;
                    leadactivity.Body = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }
            }
        }

        /// get smsTemplateData By ID for send sms Functionality
        public async Task<GetSmsTemplateForEditOutput> GetSmsTemplateForEditForSms(EntityDto input)
        {
            var smsTemplate = await _smsTemplateRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetSmsTemplateForEditOutput { SmsTemplate = ObjectMapper.Map<CreateOrEditSmsTemplateDto>(smsTemplate) };

            return output;
        }

        /// get EmailTemplateData By ID for send email Functionality
        public async Task<GetEmailTemplateForEdit> GetEmailTemplateForEditForEmail(EntityDto input)
        {
            var Email = await _emailTemplateRepository.FirstOrDefaultAsync(input.Id);
            var output = new GetEmailTemplateForEdit { EmailTemplate = ObjectMapper.Map<CreateOrEditEmailTemplateDto>(Email) };
            return output;
        }

        /// <summary>
        /// AutoComplete For Product Names
        /// </summary>
        /// <param name="productTypeId">Product Type Master Id</param>
        /// <param name="productItem">Product Name</param>
        /// <returns></returns>
        public async Task<List<string>> GetProductItemList(int productTypeId, string productItem, string state)
        {

            var warehouseid = _warehouselocationRepository.GetAll().Where(e => e.state == state).Select(e => e.Id).FirstOrDefault();
            var productiteamids = _productiteamlocationRepository.GetAll().Where(e => e.WarehouselocationId == warehouseid && e.SalesTag == true).Select(e => e.ProductItemId).Distinct().ToList();
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            if (!role.Contains("Admin"))
            {
                return await _productItemRepository.GetAll()
            .WhereIf(!string.IsNullOrWhiteSpace(productItem), c => c.Name.ToLower().Contains(productItem.ToLower()))
            .Where(e => e.ProductTypeId == productTypeId && e.Active == true && productiteamids.Contains(e.Id))
            .Select(productItems => productItems == null || productItems.Name == null ? "" : productItems.Name.ToString()).ToListAsync();
            }
            else
            {

                return await _productItemRepository.GetAll()
            .WhereIf(!string.IsNullOrWhiteSpace(productItem), c => c.Name.ToLower().Contains(productItem.ToLower()))
            .Where(e => e.ProductTypeId == productTypeId && e.Active == true)
            .Select(productItems => productItems == null || productItems.Name == null ? "" : productItems.Name.ToString()).ToListAsync();

            }


        }

        public async Task<List<string>> getinvertmodel(string invertmodel)
        {

            var result = _productItemRepository.GetAll()
            .WhereIf(!string.IsNullOrWhiteSpace(invertmodel), c => c.Model.ToLower().Contains(invertmodel.ToLower()) && c.ProductTypeId == 2);

            var invertmodels = from o in result
                               select new LeadStatusLookupTableDto()
                               {
                                   Value = o.Id,
                                   Name = o.Model.ToString()
                               };

            return await invertmodels
                .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
        }

        public async Task<List<string>> getpanelmodel(string panelmodel)
        {
            var result = _productItemRepository.GetAll()
            .WhereIf(!string.IsNullOrWhiteSpace(panelmodel), c => c.Model.ToLower().Contains(panelmodel.ToLower()) && c.ProductTypeId == 1);

            var invertmodels = from o in result
                               select new LeadStatusLookupTableDto()
                               {
                                   Value = o.Id,
                                   Name = o.Model.ToString()
                               };

            return await invertmodels
                .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();

        }

        /// <summary>
        /// Send Document Request for Document Upload Using Camera/Gallary 
        /// </summary>
        /// <param name="JobId">Job Id</param>
        /// <param name="DocumentTypeId">Requested Document Type Id</param>
        /// <returns></returns>
        public async Task SendDocumentRequestLinkSMS(int JobId, int DocumentTypeId)
        {
            var lead = _lookup_leadRepository.GetAll().Where(e => e.Id == _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.LeadId).FirstOrDefault()).FirstOrDefault();

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 6;
            leadactivity.ActionNote = "Document Request Created";
            leadactivity.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);

            if (!string.IsNullOrEmpty(lead.Mobile))
            {
                string STR = Convert.ToString(DocumentTypeId);
                var token = SimpleStringCipher.Instance.Encrypt(STR, AppConsts.DefaultPassPhrase);

                SendSMSInput sendSMSInput = new SendSMSInput();
                sendSMSInput.PhoneNumber = "+919974242686"; //lead.Mobile;
                sendSMSInput.Text = "http://thesolarproduct.com/account/document-request?STR=" + token; //input.Body;
                await _applicationSettings.SendSMS(sendSMSInput);
            }

        }

        /// <summary>
        /// Get Attached Documents With Product Items Used in Job
        /// </summary>
        /// <param name="JobId">Job Id</param>
        /// <returns></returns>
        public async Task<List<ProductItemAttachmentListDto>> ProductItemAttachmentList(int JobId)
        {
            var List = _jobProductItemRepository.GetAll().Where(e => e.JobId == JobId).Select(e => e.ProductItemId).ToList();
            return await _productItemRepository.GetAll()
                .Where(e => List.Contains(e.Id))
                .Select(item => new ProductItemAttachmentListDto
                {
                    FileName = item.FileName,
                    FilePath = item.FilePath
                }).OrderBy(x => x.FileName).ToListAsync();
        }

        public async Task<List<PvdStatusDto>> GetAllPVDStatusDropdown()
        {
            return await _pVDStatusRepository.GetAll()
                        .Select(pVDStatus => new PvdStatusDto
                        {
                            Id = pVDStatus.Id,
                            DisplayName = pVDStatus == null || pVDStatus.Name == null ? "" : pVDStatus.Name.ToString()
                        }).ToListAsync();
        }

        public async Task UpdateBookingManagerID(List<int> Jobids, int? userId)
        {
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == userId).FirstOrDefault();
            var assignedFromUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).Select(e=>e.FullName).FirstOrDefault();

            foreach (var id in Jobids)
            {
                var job = await _jobRepository.FirstOrDefaultAsync((int)id);
                job.BookingManagerId = userId;
                job.JobAssignDate = DateTime.UtcNow;
                await _jobRepository.UpdateAsync(job);

                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionNote = "JobAssign To" + assignedToUser.FullName;
                leadactivity.ActionId = 27;
                leadactivity.SectionId = 0;
                leadactivity.LeadId = (int)job.LeadId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                leadactivity.CreatorUserId = AbpSession.UserId;
                await _leadactivityRepository.InsertAsync(leadactivity);
            }

            

            string msg = string.Format(Jobids.Count + "Job Assign" + " BY " + assignedFromUser);
            await _appNotifier.LeadAssiged(assignedToUser, msg, NotificationSeverity.Info);


           

        }

        public async Task<List<CommonLookupDto>> GetUsers()
        {
            return await _userRepository.GetAll()
            .Select(users => new CommonLookupDto
            {
                Id = (int)users.Id,
                DisplayName = users == null || users.FullName == null ? "" : users.FullName.ToString()
            }).ToListAsync();
        }

        public async Task<PagedResultDto<PvdStatusDto>> GetAllPvdStatusForGrid(GetAllRefundReasonsInput input)
        {

            //return await _pVDStatusRepository.GetAll()
            //			.Select(pVDStatus => new PvdStatusDto
            //			{
            //				Id = pVDStatus.Id,
            //				DisplayName = pVDStatus == null || pVDStatus.Name == null ? "" : pVDStatus.Name.ToString()
            //			}).ToListAsync();

            var filteredRefundReasons = _pVDStatusRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

            var pagedAndFilteredRefundReasons = filteredRefundReasons
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var refundReasons = from o in pagedAndFilteredRefundReasons
                                select new PvdStatusDto
                                {

                                    DisplayName = o.Name,
                                    Id = o.Id

                                };

            var totalCount = await filteredRefundReasons.CountAsync();

            return new PagedResultDto<PvdStatusDto>(
                totalCount,
                await refundReasons.ToListAsync()
            );
        }

        public async Task CreateOrEditStcStatus(CreateOrEditStcStatusDto input)
        {
            if (input.Id == null)
            {
                await CreateStcStatus(input);
            }
            else
            {
                await UpdateStcStatus(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_StcPvdStatus_Create)]
        protected virtual async Task CreateStcStatus(CreateOrEditStcStatusDto input)
        {
            var refundReason = ObjectMapper.Map<PVDStatus>(input);

            //if (AbpSession.TenantId != null)
            //{
            //	refundReason.TenantId = (int)AbpSession.TenantId;
            //}

            await _pVDStatusRepository.InsertAsync(refundReason);
        }

        [AbpAuthorize(AppPermissions.Pages_StcPvdStatus_Edit)]
        protected virtual async Task UpdateStcStatus(CreateOrEditStcStatusDto input)
        {
            var refundReason = await _pVDStatusRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, refundReason);
        }


        [AbpAuthorize(AppPermissions.Pages_StcPvdStatus_Edit)]
        public async Task<CreateOrEditStcStatusDto> GetstatusForEdit(EntityDto input)
        {
            var refundReason = await _pVDStatusRepository.FirstOrDefaultAsync(input.Id);

            var output = new CreateOrEditStcStatusDto { Name = refundReason.Name, Id = refundReason.Id };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_StcPvdStatus_Delete)]
        public async Task DeleteStcPvdStatus(EntityDto input)
        {
            var pvddata = _jobRepository.GetAll().Where(e => e.PVDStatus == input.Id).Any();
            if (!pvddata)
            {
                await _pVDStatusRepository.DeleteAsync(input.Id);
            }
        }

        public List<TotalStcCountDto> getStcTrackerCount(GetAllJobsInput input)
        {
            if (input.pvdStat != null)
            {
                if (input.pvdStat.Contains(11))
                {
                    input.pvdStat.Remove(11);
                    input.pvdnoStatus = 2;
                }
            }
            var output = new List<TotalStcCountDto>();
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            // IList<string> role = await  _userManager.GetRolesAsync(User);
            var role = (from user in _userRepository.GetAll()
                        join ur in _userRoleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                        from ur in urJoined.DefaultIfEmpty()
                        join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                        from us in usJoined.DefaultIfEmpty()
                        where (us != null && user.Id == User.Id)
                        select (us.DisplayName));
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var pvdstatus = _pVDStatusRepository.GetAll().Distinct().ToList();
            var stctotaldata = new TotalStcCountDto();
            stctotaldata.Stc = "Total";
            stctotaldata.totalstcamount = _jobRepository.GetAll()
           .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
           .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
           .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
           .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) || e.LotNumber.Contains(input.Filter) || e.PeakMeterNo.Contains(input.Filter) || e.OffPeakMeter.Contains(input.Filter) || e.Suburb.Contains(input.Filter) || e.State.Contains(input.Filter) || e.PostalCode.Contains(input.Filter) || e.UnitNo.Contains(input.Filter) || e.UnitType.Contains(input.Filter) || e.StreetNo.Contains(input.Filter) || e.StreetName.Contains(input.Filter) || e.StreetType.Contains(input.Filter) || e.Latitude.Contains(input.Filter) || e.Longitude.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Country.Contains(input.Filter) || e.PVDNumber.Contains(input.Filter))

                          .WhereIf(input.DateNameFilter == "StcAppliedDate" && SDate != null && EDate != null, e => e.STCAppliedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.STCAppliedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                          .WhereIf(input.DateNameFilter == "InstallDate" && SDate != null && EDate != null, e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
                          .WhereIf(input.DateNameFilter == "InstallCmpltDate" && SDate != null && EDate != null, e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
                             .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.LeadFk.State == input.stateNameFilter)

                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.InstallerID != null && input.InstallerID != 0, e => e.InstallerId == input.InstallerID)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
                         //.WhereIf(input.pvdStatus != null && input.pvdStatus != 0, e => e.PVDStatus == input.pvdStatus)
                         .WhereIf(input.pvdNo != null && input.pvdNo != "", e => e.PVDNumber == input.pvdNo)
                         .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
                         // .WhereIf(input.pvdStatus != null && input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId) 
                         .WhereIf(input.pvdnoStatus == 1, e => e.PVDNumber != null && e.PVDNumber != "")
                         .WhereIf(input.pvdnoStatus == 2, e => e.PVDNumber == null || e.PVDNumber == "")
                         .WhereIf(input.pvdStat != null && input.pvdStat.Count() > 0, e => input.pvdStat.Contains((int)e.PVDStatus))
           .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit  && e.JobStatusId >= 4 ||  e.PVDStatus != null).Select(e => e.STC).Sum();
            output.Add(stctotaldata);
            var stcBlankdata = new TotalStcCountDto();
            stcBlankdata.Stc = "Blank";
            stcBlankdata.totalstcamount = _jobRepository.GetAll()
           .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
           .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
           .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
           .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) || e.LotNumber.Contains(input.Filter) || e.PeakMeterNo.Contains(input.Filter) || e.OffPeakMeter.Contains(input.Filter) || e.Suburb.Contains(input.Filter) || e.State.Contains(input.Filter) || e.PostalCode.Contains(input.Filter) || e.UnitNo.Contains(input.Filter) || e.UnitType.Contains(input.Filter) || e.StreetNo.Contains(input.Filter) || e.StreetName.Contains(input.Filter) || e.StreetType.Contains(input.Filter) || e.Latitude.Contains(input.Filter) || e.Longitude.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Country.Contains(input.Filter) || e.PVDNumber.Contains(input.Filter))

                          .WhereIf(input.DateNameFilter == "StcAppliedDate" && SDate != null && EDate != null, e => e.STCAppliedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.STCAppliedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                          .WhereIf(input.DateNameFilter == "InstallDate" && SDate != null && EDate != null, e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
                          .WhereIf(input.DateNameFilter == "InstallCmpltDate" && SDate != null && EDate != null, e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
                             .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.LeadFk.State == input.stateNameFilter)

                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.InstallerID != null && input.InstallerID != 0, e => e.InstallerId == input.InstallerID)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
                         //.WhereIf(input.pvdStatus != null && input.pvdStatus != 0, e => e.PVDStatus == input.pvdStatus)
                         .WhereIf(input.pvdNo != null && input.pvdNo != "", e => e.PVDNumber == input.pvdNo)
                         .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
                         // .WhereIf(input.pvdStatus != null && input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId) 
                         .WhereIf(input.pvdnoStatus == 1, e => e.PVDNumber != null && e.PVDNumber != "")
                         .WhereIf(input.pvdnoStatus == 2, e => e.PVDNumber == null || e.PVDNumber == "")
                         .WhereIf(input.pvdStat != null && input.pvdStat.Count() > 0, e => input.pvdStat.Contains((int)e.PVDStatus))
           .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.PVDStatus == null && e.JobStatusId >= 4).Select(e => e.STC).Sum();
            output.Add(stcBlankdata);
            for (int k = 0; k < pvdstatus.Count; k++)
            {
                var stcdata = new TotalStcCountDto();
                if (pvdstatus[k].Id != 11)
                {
                    stcdata.Stc = pvdstatus[k].Name;
                
                int? pvdid = pvdstatus[k].Id;
                stcdata.totalstcamount = _jobRepository.GetAll()
              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
              .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) || e.LotNumber.Contains(input.Filter) || e.PeakMeterNo.Contains(input.Filter) || e.OffPeakMeter.Contains(input.Filter) || e.Suburb.Contains(input.Filter) || e.State.Contains(input.Filter) || e.PostalCode.Contains(input.Filter) || e.UnitNo.Contains(input.Filter) || e.UnitType.Contains(input.Filter) || e.StreetNo.Contains(input.Filter) || e.StreetName.Contains(input.Filter) || e.StreetType.Contains(input.Filter) || e.Latitude.Contains(input.Filter) || e.Longitude.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Country.Contains(input.Filter) || e.PVDNumber.Contains(input.Filter) || e.JobNumber.Contains(input.Filter))

                          .WhereIf(input.DateNameFilter == "StcAppliedDate" && SDate != null && EDate != null, e => e.STCAppliedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.STCAppliedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                          .WhereIf(input.DateNameFilter == "InstallDate" && SDate != null && EDate != null, e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
                          .WhereIf(input.DateNameFilter == "InstallCmpltDate" && SDate != null && EDate != null, e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
                         .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.LeadFk.State == input.stateNameFilter)
                         .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                         .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                         .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                         .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                         .WhereIf(input.InstallerID != null && input.InstallerID != 0, e => e.InstallerId == input.InstallerID)
                         .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                         .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
                         //.WhereIf(input.pvdStatus != null && input.pvdStatus != 0, e => e.PVDStatus == input.pvdStatus)
                         .WhereIf(input.pvdNo != null && input.pvdNo != "", e => e.PVDNumber == input.pvdNo)
                         .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
                         // .WhereIf(input.pvdStatus != null && input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId) 
                         .WhereIf(input.pvdnoStatus == 1, e => e.PVDNumber != null && e.PVDNumber != "")
                         .WhereIf(input.pvdnoStatus == 2, e => e.PVDNumber == null || e.PVDNumber == "")
                         .WhereIf(input.pvdStat != null && input.pvdStat.Count() > 0, e => input.pvdStat.Contains((int)e.PVDStatus))
                        .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.PVDStatus == pvdid && e.PVDStatus!=11).Select(e => e.STC).Sum();



                output.Add(stcdata);
                }
            }

            return output;
        }

        public GetJobForEditOutput getJobdetailbylead(int? leadid) {

            var Installerid = _jobRepository.GetAll().Where(e => e.LeadId == leadid).Select(e => e.InstallerId).FirstOrDefault();
            if (Installerid != null && Installerid != 0)
            {


                ///  var InstallerEmailPhone = GetJobForEditOutput();
                var InstallerEmailPhone = (from o in _userRepository.GetAll()
                                           join o2 in _installerDetailRepository.GetAll() on o.Id equals o2.UserId
                                           where (o2.UserId == Installerid)
                                           select new GetJobForEditOutput
                                           {
                                              InstallerEmail = o.EmailAddress,
                                              InstallerMobile = o.Mobile,
                                              InstallerID = Installerid,
                                           }).FirstOrDefault();

                return InstallerEmailPhone;
            }
            else {
                return null;
            }
        }


        public async Task<PagedResultDto<ReferralGridDto>> GetReferralTrackerList(ReferralInputDto input)
        {
            try {
                //int installerid = 0;
                //if (!string.IsNullOrEmpty(input.InstallerId))
                //{
                //    int firstCommaIndex = input.InstallerId.IndexOf('.');

                //    string firstPart = input.InstallerId.Substring(0, firstCommaIndex);

                //    var jbid = _userRepository.GetAll().Where(e => e.Name == firstPart).Select(e => e.Id).FirstOrDefault();

                //    installerid = Convert.ToInt32(jbid);
                //}
                var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
                IList<string> role = await _userManager.GetRolesAsync(User);
                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
                var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
                //var paidjobid = _jobInstallerInvoiceRepository.GetAll().WhereIf(input.PaymentStatus == 1, e => e.IsPaid == true && e.ApproedDate != null && e.PayDate != null)
                //    .WhereIf(input.PaymentStatus == 2, e => e.IsPaid == true && e.ApproedDate != null && e.PayDate == null).Select(e => e.JobId).ToList();


                var filteredLeads = _jobRepository.GetAll()
                    .Include(e => e.LeadFk)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) ||
                            e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                    .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                            .WhereIf(input.PaymentStatus == 1, e => e.ReferralPayment == true  )
                             .WhereIf(input.PaymentStatus == 2, e => e.ReferralPayment == false && e.BsbNo != null && e.AccountNo!=null && ((_invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.InvoicePayTotal).Sum()) == e.TotalCost))
                             .WhereIf(input.PaymentStatus == 3, e => e.ReferralPayment == false && e.BsbNo == null && e.AccountNo == null && ((_invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.InvoicePayTotal).Sum()) != e.TotalCost ))
                             .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                             .WhereIf(!input.StateId.IsNullOrEmpty(), e => e.LeadFk.State == input.StateId)
                            .Where(e => e.LeadFk.LeadSource == "Referral" && e.LeadFk.LeadSourceId == 2 && e.JobStatusId >= 4)
                    .Distinct();

                var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

                var result = (from master in pagedAndFilteredLeads
                              join lead in _lookup_leadRepository.GetAll() on master.LeadId equals lead.Id
                              //join jie in _jobInstallerInvoiceRepository.GetAll() on job.Id equals jie.JobId
                              join user in _userRepository.GetAll() on (int)master.InstallerId equals (int)user.Id into userjoined
                              from user in userjoined.DefaultIfEmpty()
                              let refid = master.RefferedJobId == null ? 0 : master.RefferedJobId
                              let refstatusid = master.RefferedJobStatusId == null ? 0 : master.RefferedJobStatusId
                              select new ReferralGridDto
                              {
                                  ProjectName = master.JobNumber,
                                  //Customer = user.Name +" " + user.FullName,
                                  Address = master.Address,
                                  Suburb = master.Suburb,
                                  State = master.State,
                                  PCode = lead.PostCode,
                                  InstallDate = master.InstallationDate,
                                  InstallComplate = master.InstalledcompleteDate,

                                  id = master.Id,
                                  companyName = lead.CompanyName,
                                  jobstatus = master.JobStatusFk.Name,
                                  TotalCost = master.TotalCost,
                                  owningAmt = Convert.ToDecimal(master.TotalCost - (_invoicePaymentRepository.GetAll().Where(e => e.JobId == master.Id).Select(e => e.InvoicePayTotal).Sum())),
                                  leadid = master.LeadId,

                                  RefferedProjectName = _jobRepository.GetAll().Where(e => e.Id == refid).Select(e => e.JobNumber).FirstOrDefault(),
                                  RefferedProjectStatus = _lookup_jobStatusRepository.GetAll().Where(e => e.Id == refstatusid).Select(e => e.Name).FirstOrDefault(),
                                  RefferedTotalCost = _jobRepository.GetAll().Where(e => e.Id == refid).Select(e => e.TotalCost).FirstOrDefault(),
                                  RefferedowningAmt = Convert.ToDecimal(_jobRepository.GetAll().Where(e => e.Id == refid).Select(e => e.TotalCost).FirstOrDefault() - (_invoicePaymentRepository.GetAll().Where(e => e.JobId == refid).Select(e => e.InvoicePayTotal).Sum())),

                                  AccountName = master.AccountName,

                                  BsbNo = master.BsbNo,
                                  AccountNo = master.AccountNo,
                                  ReferralAmount = master.ReferralAmount,
                                  ActivityReminderTime = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 8 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                                  ActivityDescription = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 8 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                  ActivityComment = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 24 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                              }).Distinct().ToList();


                var Count = result.Count();

                return new PagedResultDto<ReferralGridDto>(
                    Count, result.ToList()
                );
            } catch (Exception e) { throw e; }

            }
        
        public async Task<List<string>> GetAlljobNumberforAdd(string leadSourceName)
        {
            var ListOfLeadSource = (from job in _jobRepository.GetAll()
                                    join lead in _lookup_leadRepository.GetAll() on job.LeadId equals lead.Id into leadjoined
                                    from lead in leadjoined
                                    where (job.JobNumber.Contains(leadSourceName) || lead.CompanyName.Contains(leadSourceName) || job.Address.Contains(leadSourceName))
                                    select new jobnoLookupTableDto
                                    {
                                        Value = job.Id,
                                        Name = Convert.ToString(job.JobNumber) + "| " + Convert.ToString(lead.CompanyName) + "| " + Convert.ToString(job.Address)
                                    });
            return await ListOfLeadSource
                    .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
        }
        //public async Task<GetReferralEditDto> GetReferralForEdit(EntityDto input)
        //{
        //    //var job = await _jobRepository.FirstOrDefaultAsync(input.Id);
        //    //return job;
        //}
        public async Task<PagedResultDto<WarrantyGridDto>> GetWarrantyTrackerList(WarrantyInputDto input)
        {

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();


            var filteredLeads = _jobRepository.GetAll()
                .Include(e => e.LeadFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) ||
                                e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id) 
                                 .WhereIf(!input.StateId.IsNullOrEmpty(), e => e.LeadFk.State == input.StateId)
                        .Where(e => e.InstalledcompleteDate != null && ((_invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.InvoicePayTotal).Sum()) == e.TotalCost))
                .Distinct();

            var pagedAndFilteredLeads = filteredLeads
            .OrderBy(input.Sorting ?? "id desc")
            .PageBy(input);

            var result = (from master in pagedAndFilteredLeads
                          join lead in _lookup_leadRepository.GetAll() on master.LeadId equals lead.Id
                          //join jie in _jobInstallerInvoiceRepository.GetAll() on job.Id equals jie.JobId
                          join user in _userRepository.GetAll() on (int)lead.AssignToUserID equals (int)user.Id into userjoined
                          from user in userjoined.DefaultIfEmpty()
                              //let refid = master.RefferedJobId == null ? 0 : master.RefferedJobId
                              //let refstatusid = master.RefferedJobStatusId == null ? 0 : master.RefferedJobStatusId
                          select new WarrantyGridDto
                          {
                              ProjectName = master.JobNumber,
                              Customer = lead.CompanyName,
                              Address = master.Address,
                              Mobile = lead.Mobile,
                              Email = lead.Email,


                              panelName = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Id == (_jobProductItemRepository.GetAll().Where(e => e.JobId == master.Id).Select(e => e.ProductItemId).FirstOrDefault())).Select(e => e.Name).FirstOrDefault(),
                              PanelModel = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Id == (_jobProductItemRepository.GetAll().Where(e => e.JobId == master.Id).Select(e => e.ProductItemId).FirstOrDefault())).Select(e => e.Model).FirstOrDefault(),
                              PanelDocu = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Id == (_jobProductItemRepository.GetAll().Where(e => e.JobId == master.Id).Select(e => e.ProductItemId).FirstOrDefault())).Select(e => e.FileName).FirstOrDefault(),
                              InverterNname = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == (_jobProductItemRepository.GetAll().Where(e => e.JobId == master.Id).Select(e => e.ProductItemId).FirstOrDefault())).Select(e => e.Name).FirstOrDefault(),
                              InverterModel = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == (_jobProductItemRepository.GetAll().Where(e => e.JobId == master.Id).Select(e => e.ProductItemId).FirstOrDefault())).Select(e => e.Model).FirstOrDefault(),
                              InverterDoc = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == (_jobProductItemRepository.GetAll().Where(e => e.JobId == master.Id).Select(e => e.ProductItemId).FirstOrDefault())).Select(e => e.FileName).FirstOrDefault(),
                              BalanceOwing = Convert.ToDecimal(master.TotalCost - (_invoicePaymentRepository.GetAll().Where(e => e.JobId == master.Id).Select(e => e.InvoicePayTotal).Sum())),
                              SalesRep = user.Name,
                              Sms = "N",
                              Emailsend = "N",

                              ActivityReminderTime = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 8 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                              ActivityDescription = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 8 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              ActivityComment = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 24 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              jobid=master.Id,Leadid=master.LeadId,
                          }).Distinct().ToList();


            var Count = result.Count();

            return new PagedResultDto<WarrantyGridDto>(
                Count, result.ToList()
            );


        }

        public async Task SaveDocument(UploadDocumentInput input)
        {
            var ByteArray = _tempFileCacheManager.GetFile(input.FileToken);

            if (ByteArray == null)
            {
                throw new UserFriendlyException("There is no such file with the token: " + input.FileToken);
            }

            if (ByteArray.Length > MaxFileBytes)
            {
                throw new UserFriendlyException("Document size exceeded");
            }

             

            var Path1 = _env.WebRootPath;
            var MainFolder = Path1;
            MainFolder = MainFolder + "\\Documents";

            var JobNumber = _jobRepository.GetAll().Where(e => e.Id == input.JobId).FirstOrDefault();
            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == JobNumber.TenantId).Select(e => e.TenancyName).FirstOrDefault();


            string TenantPath = MainFolder + "\\" + TenantName;
            string JobPath = TenantPath + "\\" + JobNumber.JobNumber;
            string SignaturePath = "";
          
                SignaturePath = JobPath + "\\Warrantys";
             
            var FinalFilePath = SignaturePath + "\\" + input.FileName;

            if (System.IO.Directory.Exists(MainFolder))
            {
                if (System.IO.Directory.Exists(TenantPath))
                {
                    if (System.IO.Directory.Exists(JobPath))
                    {
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(JobPath);
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(TenantPath);
                    if (System.IO.Directory.Exists(JobPath))
                    {
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(JobPath);
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
            }
            else
            {
                System.IO.Directory.CreateDirectory(MainFolder);
                if (System.IO.Directory.Exists(TenantPath))
                {
                    if (System.IO.Directory.Exists(JobPath))
                    {
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(JobPath);
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(TenantPath);
                    if (System.IO.Directory.Exists(JobPath))
                    {
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(JobPath);
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
            }

            

            
            var document = new Jobwarranty()
            {
                Id = input.Id,
                ProductTypeId = input.DocumentTypeId,
                JobId = input.JobId,
                Filename = input.FileName,
                FileType = input.FileType,
                Filepath = input.DocumentTypeId == 1 ? "\\Documents" + "\\" + TenantName + "\\" + JobNumber.JobNumber + "\\NearMap\\" : "\\Documents" + "\\" + TenantName + "\\" + JobNumber.JobNumber + "\\Documents\\",
               
            };
            await _JobwarrantyRepository.InsertAsync(document);
            //}

           // await _jobAppService.JobActive((int)input.JobId);

            var Jobs = _jobRepository.GetAll().Where(e => e.Id == input.JobId).FirstOrDefault();
            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.SectionId = 0;
            leadactivity.ActionId = 17;
            leadactivity.ActionNote = "Job Document Uploaded";
            leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

    }
}


