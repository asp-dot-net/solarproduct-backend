﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Invoices;
using TheSolarProduct.Jobs;
using TheSolarProduct.Leads;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.PickList;
using TheSolarProduct.Quotations;
using TheSolarProduct.TheSolarDemo;

namespace TheSolarProduct.Reports
{
	public class ReportAppService : TheSolarProductAppServiceBase, IReportAppService
	{
		private readonly IRepository<Job> _jobRepository;
		private readonly IRepository<RoofType, int> _roofTypeRepository;
		private readonly IRepository<RoofAngle, int> _roofAngleRepository;
		private readonly IRepository<HouseType, int> _houseTypeRepository;
		private readonly IRepository<ElecDistributor, int> _elecDistributorRepository;
		private readonly IRepository<ElecRetailer, int> _elecRetailerRepository;
		private readonly IRepository<ProductItem> _ProductItemRepository;
		private readonly IRepository<JobProductItem> _jobProductItemRepository;
		private readonly IRepository<Lead> _leadRepository;
		private readonly IRepository<JobStatus> _jobStatusRepository;
		private readonly IRepository<User, long> _userRepository;
		private readonly IRepository<UserRole, long> _userroleRepository;
		private readonly IRepository<Role> _roleRepository;
		private readonly IRepository<UserTeam> _userTeamRepository;
		private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
		private readonly IRepository<InvoicePaymentMethod> _invoicePaymentMethodRepository;
		private readonly IRepository<JobRefund> _jobRefundRepository;
		private readonly IRepository<RefundReason> _refundReasonRepository;
		private readonly IAppFolders _appFolders;
		private readonly IWebHostEnvironment _env;
		private readonly IRepository<Tenant> _tenantRepository;
		private readonly IRepository<Document> _documentRepository;
		private readonly IRepository<Variation> _variationRepository;
		private readonly IRepository<JobVariation> _jobVariationRepository;
		private readonly IRepository<Quotation> _quotationRepository;
		private readonly IRepository<Warehouselocation> _wareHouseLocationRepository;
		private readonly IPickListAppService _pickListAppService;
		private readonly IRepository<FinanceOption> _financeOptionRepository;
		private readonly IRepository<DepositOption> _depositOptionRepository;
		private readonly IRepository<PaymentOption> _paymentOptionRepository;
		private readonly UserManager _userManager;

		public ReportAppService(IRepository<Job> jobRepository
			, IRepository<RoofType, int> roofTypeRepository
			, IRepository<RoofAngle, int> roofAngleRepository
			, IRepository<HouseType, int> houseTypeRepository
			, IRepository<ElecDistributor, int> elecDistributorRepository
			, IRepository<ElecRetailer, int> elecRetailerRepository
			, IRepository<ProductItem> ProductItemRepository
			, IRepository<JobProductItem> jobProductItemRepository
			, IRepository<Lead> leadRepository
			, IRepository<JobStatus> jobStatusRepository
			, IRepository<User, long> userRepository
			, IRepository<UserRole, long> userroleRepository
			, IRepository<Role> roleRepository
			, IRepository<UserTeam> userTeamRepository
			, IRepository<InvoicePayment> invoicePaymentRepository
			, IRepository<InvoicePaymentMethod> invoicePaymentMethodRepository
			, IRepository<JobRefund> jobRefundRepository
			, IRepository<RefundReason> refundReasonRepository
			, IAppFolders appFolders
			, IWebHostEnvironment env
			, IRepository<Tenant> tenantRepository
			, IRepository<Document> documentRepository
			, IRepository<Variation> variationRepository
			, IRepository<JobVariation> jobVariationRepository
			, IRepository<Quotation> quotationRepository
			, IPickListAppService pickListAppService
			, IRepository<Warehouselocation> wareHouseLocationRepository
			, IRepository<FinanceOption> financeOptionRepository
			, IRepository<DepositOption> depositOptionRepository
			, IRepository<PaymentOption> paymentOptionRepository
			, UserManager userManager)
		{
			_jobRepository = jobRepository;
			_roofTypeRepository = roofTypeRepository;
			_roofAngleRepository = roofAngleRepository;
			_houseTypeRepository = houseTypeRepository;
			_elecDistributorRepository = elecDistributorRepository;
			_elecRetailerRepository = elecRetailerRepository;
			_ProductItemRepository = ProductItemRepository;
			_jobProductItemRepository = jobProductItemRepository;
			_leadRepository = leadRepository;
			_jobStatusRepository = jobStatusRepository;
			_userRepository = userRepository;
			_userroleRepository = userroleRepository;
			_roleRepository = roleRepository;
			_userTeamRepository = userTeamRepository;
			_invoicePaymentRepository = invoicePaymentRepository;
			_invoicePaymentMethodRepository = invoicePaymentMethodRepository;
			_jobRefundRepository = jobRefundRepository;
			_refundReasonRepository = refundReasonRepository;
			_appFolders = appFolders;
			_env = env;
			_tenantRepository = tenantRepository;
			_documentRepository = documentRepository;
			_variationRepository = variationRepository;
			_jobVariationRepository = jobVariationRepository;
			_quotationRepository = quotationRepository;
			_pickListAppService = pickListAppService;
			_wareHouseLocationRepository = wareHouseLocationRepository;
			_financeOptionRepository = financeOptionRepository;
			_depositOptionRepository = depositOptionRepository;
			_paymentOptionRepository = paymentOptionRepository;
			_userManager = userManager;
		}

		public string Quotation(int JobId, int? QuoteId, string ImagePath, string QuoteNo, int OrgId)
		{
			if (OrgId == 1)
			{

				var report_parent = new Report.NewQuote();
				var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
				var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
				var RoofType = _roofTypeRepository.GetAll().Where(e => e.Id == JobDetail.RoofTypeId).Select(e => e.Name).FirstOrDefault();
				var RoofAngle = _roofAngleRepository.GetAll().Where(e => e.Id == JobDetail.RoofAngleId).Select(e => e.Name).FirstOrDefault();
				var HouseType = _houseTypeRepository.GetAll().Where(e => e.Id == JobDetail.HouseTypeId).Select(e => e.Name).FirstOrDefault();
				var Dist = _elecDistributorRepository.GetAll().Where(e => e.Id == JobDetail.ElecDistributorId).Select(e => e.Name).FirstOrDefault();
				var Retailer = _elecRetailerRepository.GetAll().Where(e => e.Id == JobDetail.ElecRetailerId).Select(e => e.Name).FirstOrDefault();
				var ProductItemList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).Select(e => e.ProductItemId).ToList();
				var ProductItemsList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).ToList();
				var PanelDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 1).FirstOrDefault();
				var InverterDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 2).ToList();
				var NearMap = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 1 && e.JobId == JobId).OrderByDescending(e => e.Id).FirstOrDefault();
				var MinusList = _variationRepository.GetAll().Where(e => e.Action == "Minus").Select(e => e.Id).ToList();
				var PlusList = _variationRepository.GetAll().Where(e => e.Action == "Plus").Select(e => e.Id).ToList();
				var Addition = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && PlusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
				var Discount = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && MinusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
				var SalesRep = _userRepository.GetAll().Where(e => e.Id == Lead.AssignToUserID).FirstOrDefault();
				var TotalDeposite = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id && e.InvoicePaymentStatusId == 2).Select(e => e.InvoicePayTotal).FirstOrDefault();

				var Finance = _financeOptionRepository.GetAll().Where(e => e.Id == JobDetail.FinanceOptionId).Select(e => e.Name).FirstOrDefault();
				var Dep = _depositOptionRepository.GetAll().Where(e => e.Id == JobDetail.DepositOptionId).Select(e => e.Name).FirstOrDefault();
				var Payment = _paymentOptionRepository.GetAll().Where(e => e.Id == JobDetail.PaymentOptionId).Select(e => e.Name).FirstOrDefault();

				var QunityAndModelList = (from o in _jobProductItemRepository.GetAll()
										  join o1 in _ProductItemRepository.GetAll() on o.ProductItemId equals o1.Id into j1
										  from s1 in j1.DefaultIfEmpty()
										  where (o.JobId == JobDetail.Id)
										  select new
										  {
											  Name = o.Quantity + " x " + s1.Name
										  }).ToList();

				//	var QunityAndModel = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id)).Select(e => e.Name).ToList();


				Telerik.Reporting.TextBox QuoteNumber1 = report_parent.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Date1 = report_parent.Items.Find("textBox2", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox QuoteNumber2 = report_parent.Items.Find("textBox124", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Date2 = report_parent.Items.Find("textBox123", true)[0] as Telerik.Reporting.TextBox;

				Telerik.Reporting.TextBox Name = report_parent.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Mobile = report_parent.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Email = report_parent.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Address1 = report_parent.Items.Find("textBox6", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Address2 = report_parent.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox MeaterPhase = report_parent.Items.Find("textBox8", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox MeaterUpgrade = report_parent.Items.Find("textBox9", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox NoofStory = report_parent.Items.Find("textBox11", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox RoofPinch = report_parent.Items.Find("textBox12", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox RoofTypes = report_parent.Items.Find("textBox10", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox EnergyDist = report_parent.Items.Find("textBox13", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox EnergyRetailer = report_parent.Items.Find("textBox14", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox NMINumber = report_parent.Items.Find("textBox97", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox PaymentMethod = report_parent.Items.Find("textBox98", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox InstDate = report_parent.Items.Find("textBox15", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox InstName = report_parent.Items.Find("textBox33", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox InstMobile = report_parent.Items.Find("textBox34", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox InstEmail = report_parent.Items.Find("textBox35", true)[0] as Telerik.Reporting.TextBox;

				Telerik.Reporting.TextBox Capecity = report_parent.Items.Find("textBox15", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Total = report_parent.Items.Find("textBox23", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox SubTotal = report_parent.Items.Find("textBox24", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox STC = report_parent.Items.Find("textBox25", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox STCDesc = report_parent.Items.Find("textBox26", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox GrandTotal = report_parent.Items.Find("textBox27", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox FinalQuote = report_parent.Items.Find("textBox28", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox SpecialDiscount = report_parent.Items.Find("textBox28", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox AdditionalCharges = report_parent.Items.Find("textBox29", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Deposite = report_parent.Items.Find("textBox30", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox DepositeText = report_parent.Items.Find("textBox92", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Balance = report_parent.Items.Find("textBox31", true)[0] as Telerik.Reporting.TextBox;

				//Telerik.Reporting.TextBox Panel = report_parent.Items.Find("textBox18", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox Inverter1 = report_parent.Items.Find("textBox17", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox Inverter2 = report_parent.Items.Find("textBox172", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox Inverter3 = report_parent.Items.Find("textBox171", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox Inverter4 = report_parent.Items.Find("textBox177", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox InverterModelNo2 = report_parent.Items.Find("textBox176", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox Inverter3 = report_parent.Items.Find("textBox179", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox InverterModelNo3 = report_parent.Items.Find("textBox178", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Notes = report_parent.Items.Find("textBox32", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox RefNo = report_parent.Items.Find("textBox175", true)[0] as Telerik.Reporting.TextBox;

				Telerik.Reporting.TextBox SignCustName = report_parent.Items.Find("textBox174", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox SignCustDate = report_parent.Items.Find("textBox173", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.PictureBox CustSignature = report_parent.Items.Find("pictureBox6", true)[0] as Telerik.Reporting.PictureBox;
				Telerik.Reporting.PictureBox NearMap1 = report_parent.Items.Find("pictureBox7", true)[0] as Telerik.Reporting.PictureBox;
				Telerik.Reporting.PictureBox NearMap2 = report_parent.Items.Find("pictureBox8", true)[0] as Telerik.Reporting.PictureBox;
				Telerik.Reporting.PictureBox NearMap3 = report_parent.Items.Find("pictureBox9", true)[0] as Telerik.Reporting.PictureBox;

				Telerik.Reporting.PictureBox IMG1 = report_parent.Items.Find("pictureBox1", true)[0] as Telerik.Reporting.PictureBox;
				Telerik.Reporting.PictureBox IMG2 = report_parent.Items.Find("pictureBox2", true)[0] as Telerik.Reporting.PictureBox;
				Telerik.Reporting.PictureBox IMG3 = report_parent.Items.Find("pictureBox3", true)[0] as Telerik.Reporting.PictureBox;
				Telerik.Reporting.PictureBox IMG4 = report_parent.Items.Find("pictureBox4", true)[0] as Telerik.Reporting.PictureBox;
				Telerik.Reporting.PictureBox IMG5 = report_parent.Items.Find("pictureBox5", true)[0] as Telerik.Reporting.PictureBox;

				Telerik.Reporting.Table SyatemDetail = report_parent.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
				SyatemDetail.DataSource = QunityAndModelList;
				var Path = _env.WebRootPath;

				Image Url1 = Image.FromFile(Path + "/PDFImages/Quote1.png");
				Image Url2 = Image.FromFile(Path + "/PDFImages/Quote2.png");
				Image Url3 = Image.FromFile(Path + "/PDFImages/Quote3.png");
				Image Url4 = Image.FromFile(Path + "/PDFImages/Quote4.png");
				Image Url5 = Image.FromFile(Path + "/PDFImages/Quote5.png");

				IMG1.Value = Url1;
				IMG2.Value = Url2;
				IMG3.Value = Url3;
				IMG4.Value = Url4;
				IMG5.Value = Url5;

				var test = 0;
				int i = 0;
				foreach (var item in Addition)
				{
					if (i == 0)
					{
						test = Convert.ToInt32(item);
					}
					else
					{
						test = test + Convert.ToInt32(item);
					}
					i++;
				}
				var test1 = 0;
				int j = 0;
				foreach (var item in Discount)
				{
					if (j == 0)
					{
						test1 = Convert.ToInt32(item);
					}
					else
					{
						test1 = test1 + Convert.ToInt32(item);
					}
					j++;
				}

				QuoteNumber1.Value = JobDetail.JobNumber;
				QuoteNumber2.Value = JobDetail.JobNumber;
				Date1.Value = DateTime.Now.AddHours(15).ToString("MMM dd, yyyy");
				Date2.Value = DateTime.Now.AddHours(15).ToString("MMM dd, yyyy");
				Name.Value = Lead.CompanyName;
				Mobile.Value = ": " + Lead.Mobile;
				Email.Value = ": " + Lead.Email;
				if (JobDetail.Address != null)
				{
					Address1.Value = ": " + Convert.ToString(JobDetail.Address).Trim();
				}
				Address2.Value = JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode;
				Capecity.Value = Convert.ToString(JobDetail.SystemCapacity) + " KW SOLAR";
				Notes.Value = JobDetail.Note;
				if (SalesRep != null)
				{
					InstName.Value = SalesRep.FullName;
					if (SalesRep.Mobile != null)
					{
						InstMobile.Value = SalesRep.PhoneNumber + "/" + SalesRep.Mobile;
					}
					else
					{
						InstMobile.Value = SalesRep.PhoneNumber;
					}
					InstEmail.Value = SalesRep.EmailAddress;
				}
				RefNo.Value = JobDetail.JobNumber;
				SubTotal.Value = Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate);
				STC.Value = Convert.ToString(JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.Rebate);
				STCDesc.Value = JobDetail.STC + " * " + JobDetail.STCPrice;
				GrandTotal.Value = Convert.ToString((JobDetail.BasicCost + JobDetail.Rebate) - (JobDetail.Rebate)) == null ? "$ 0.00" : "$ " + Convert.ToString((JobDetail.BasicCost + JobDetail.Rebate) - (JobDetail.Rebate));

				if (TotalDeposite == 0 || TotalDeposite == null)
				{
					DepositeText.Value = "Deposit Required";
					Deposite.Value = JobDetail.DepositRequired == 0 ? "$ " + Math.Round((double)JobDetail.BasicCost * 10 / 100) + ".00" : "$ " + Math.Round((double)JobDetail.DepositRequired) + ".00";
				}
				else
				{
					DepositeText.Value = "Deposit Paid";
					Deposite.Value = "$ " + TotalDeposite;
				}

				Total.Value = Convert.ToString(JobDetail.BasicCost) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost);
				if (TotalDeposite == 0 || TotalDeposite == null)
				{
					Balance.Value = Convert.ToString((JobDetail.BasicCost) + test - test1 - JobDetail.DepositRequired) == null ? "$ 0.00" : "$ " + Convert.ToString((JobDetail.BasicCost) + test - test1 - JobDetail.DepositRequired);
				}
				else
				{
					Balance.Value = Convert.ToString((JobDetail.BasicCost) + test - test1 - TotalDeposite) == null ? "$ 0.00" : "$ " + Convert.ToString((JobDetail.BasicCost) + test - test1 - TotalDeposite);
				}

				MeaterPhase.Value = ": " + Convert.ToString(JobDetail.MeterPhaseId);

				if (JobDetail.PaymentOptionId == 1)
				{
					PaymentMethod.Value = "Cash";
				}
				else
				{
					PaymentMethod.Value = Payment + " with " + Dep + " for " + Finance;
				}

				SpecialDiscount.Value = test1 == 0 ? "$ 0.00" : "$ " + test1 + ".00";
				AdditionalCharges.Value = test == 0 ? "$ 0.00" : "$ " + test + ".00";
				if (JobDetail.MeterUpgradeId == 1)
				{
					MeaterUpgrade.Value = ": " + "Yes";
				}
				else
				{
					MeaterUpgrade.Value = ": " + "No";
				}
				NoofStory.Value = ": " + HouseType;
				RoofTypes.Value = ": " + RoofType;
				RoofPinch.Value = ": " + RoofAngle;
				EnergyDist.Value = ": " + Dist;
				EnergyRetailer.Value = ": " + Retailer;
				NMINumber.Value = ": " + JobDetail.NMINumber;
				//if (JobDetail.InstallationDate != null)
				//{
				//	InstDate.Value = JobDetail.InstallationDate.Value.ToString("DD/MM/yyyy");
				//}
				//if (PanelDetail != null)
				//{
				//	var PanelQTY = ProductItemsList.Where(e => e.ProductItemId == PanelDetail.Id).Select(e => e.Quantity).FirstOrDefault();

				//	//Panel.Value = PanelQTY + " x " + PanelDetail.Name;
				//	//PanelModelNo.Value = PanelDetail.Model;
				//}
				//for (int k = 0; k < InverterDetail.Count; k++)
				//{
				//	if(k == 0)
				//	{
				//		var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail[k].Id).Select(e => e.Quantity).FirstOrDefault();

				//		//Inverter1.Value = InverterQTY + " x " + InverterDetail[k].Name;
				//		//InverterModelNo1.Value = InverterDetail[k].Model;
				//	}
				//	if (k == 1)
				//	{
				//		var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail[k].Id).Select(e => e.Quantity).FirstOrDefault();

				//		//Inverter2.Value = InverterQTY + " x " + InverterDetail[k].Name;
				//		//InverterModelNo2.Value = InverterDetail[k].Model;
				//	}
				//	if (k == 2)
				//	{
				//		var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail[k].Id).Select(e => e.Quantity).FirstOrDefault();

				//		//Inverter3.Value = InverterQTY + " x " + InverterDetail[k].Name;
				//		//InverterModelNo3.Value = InverterDetail[k].Model;
				//	}
				//	if (k == 3)
				//	{
				//		var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail[k].Id).Select(e => e.Quantity).FirstOrDefault();

				//		//Inverter4.Value = InverterQTY + " x " + InverterDetail[k].Name;
				//		//InverterModelNo4.Value = InverterDetail[k].Model;
				//	}
				//}
				//if (InverterDetail != null)
				//{
				//	var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail.Id).Select(e => e.Quantity).FirstOrDefault();

				//	Inverter.Value = InverterQTY + " x " + InverterDetail.Name;
				//	InverterModelNo.Value = InverterDetail.Model;
				//}
				SignCustName.Value = Lead.CompanyName;
				SignCustDate.Value = DateTime.Now.AddHours(15).ToString("dd/MM/yyyy");
				if (ImagePath != null)
				{
					CustSignature.Value = ImagePath;
				}

                if (NearMap != null)
                {
                    if (!string.IsNullOrEmpty(NearMap.NearMapImage1))
                    {
                        Image image1 = Image.FromFile(Path + NearMap.FilePath + "\\" + NearMap.NearMapImage1);
                        NearMap1.Value = image1;
                    }
                    if (!string.IsNullOrEmpty(NearMap.NearMapImage2))
                    {
                        Image image2 = Image.FromFile(Path + NearMap.FilePath + "\\" + NearMap.NearMapImage2);
                        NearMap2.Value = image2;
                    }
                    if (!string.IsNullOrEmpty(NearMap.NearMapImage3))
                    {
                        Image image3 = Image.FromFile(Path + NearMap.FilePath + "\\" + NearMap.NearMapImage3);
                        NearMap3.Value = image3;
                    }
                }

                var Filename = DateTime.Now.Ticks + "_Quotation";
				SaveTelerikPDF(report_parent, JobDetail.Id, JobDetail.TenantId, Filename);
				return Filename;
			}
			else if (OrgId == 2)
			{
				var report_parent = new Report.SavvyQuote();
				var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
				var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
				var RoofType = _roofTypeRepository.GetAll().Where(e => e.Id == JobDetail.RoofTypeId).Select(e => e.Name).FirstOrDefault();
				var RoofAngle = _roofAngleRepository.GetAll().Where(e => e.Id == JobDetail.RoofAngleId).Select(e => e.Name).FirstOrDefault();
				var HouseType = _houseTypeRepository.GetAll().Where(e => e.Id == JobDetail.HouseTypeId).Select(e => e.Name).FirstOrDefault();
				var Dist = _elecDistributorRepository.GetAll().Where(e => e.Id == JobDetail.ElecDistributorId).Select(e => e.Name).FirstOrDefault();
				var Retailer = _elecRetailerRepository.GetAll().Where(e => e.Id == JobDetail.ElecRetailerId).Select(e => e.Name).FirstOrDefault();
				var ProductItemList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).Select(e => e.ProductItemId).ToList();
				var ProductItemsList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).ToList();
				var PanelDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 1).OrderByDescending(e => e.Id).FirstOrDefault();
				var Inverter = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 2).OrderByDescending(e => e.Id).FirstOrDefault();
				var InverterDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 2).ToList();
				var NearMap = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 1 && e.JobId == JobId).OrderByDescending(e => e.Id).FirstOrDefault();
				var MinusList = _variationRepository.GetAll().Where(e => e.Action == "Minus").Select(e => e.Id).ToList();
				var PlusList = _variationRepository.GetAll().Where(e => e.Action == "Plus").Select(e => e.Id).ToList();
				var Addition = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && PlusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
				var Discount = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && MinusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
				var SalesRep = _userRepository.GetAll().Where(e => e.Id == Lead.AssignToUserID).FirstOrDefault();
				var TotalDeposite = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id && e.InvoicePaymentStatusId == 2).Select(e => e.InvoicePayTotal).FirstOrDefault();

				var Finance = _financeOptionRepository.GetAll().Where(e => e.Id == JobDetail.FinanceOptionId).Select(e => e.Name).FirstOrDefault();
				var Dep = _depositOptionRepository.GetAll().Where(e => e.Id == JobDetail.DepositOptionId).Select(e => e.Name).FirstOrDefault();
				var Payment = _paymentOptionRepository.GetAll().Where(e => e.Id == JobDetail.PaymentOptionId).Select(e => e.Name).FirstOrDefault();

				var QunityAndModelList = (from o in _jobProductItemRepository.GetAll()
										  join o1 in _ProductItemRepository.GetAll() on o.ProductItemId equals o1.Id into j1
										  from s1 in j1.DefaultIfEmpty()
										  where (o.JobId == JobDetail.Id)
										  select new
										  {
											  Name = o.Quantity + " x " + s1.Name
										  }).ToList();

				//	var QunityAndModel = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id)).Select(e => e.Name).ToList();


				Telerik.Reporting.TextBox QuoteNumber1 = report_parent.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Date1 = report_parent.Items.Find("textBox2", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox QuoteNumber2 = report_parent.Items.Find("textBox124", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox Date2 = report_parent.Items.Find("textBox123", true)[0] as Telerik.Reporting.TextBox;

				Telerik.Reporting.TextBox Name = report_parent.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Mobile = report_parent.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Email = report_parent.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Address1 = report_parent.Items.Find("textBox6", true)[0] as Telerik.Reporting.TextBox;
				///Telerik.Reporting.TextBox Address2 = report_parent.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox MeaterPhase = report_parent.Items.Find("textBox8", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox MeaterUpgrade = report_parent.Items.Find("textBox9", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox NoofStory = report_parent.Items.Find("textBox11", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox RoofPinch = report_parent.Items.Find("textBox12", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox RoofTypes = report_parent.Items.Find("textBox10", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox EnergyDist = report_parent.Items.Find("textBox13", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox EnergyRetailer = report_parent.Items.Find("textBox14", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox NMINumber = report_parent.Items.Find("textBox97", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox PaymentMethod = report_parent.Items.Find("textBox98", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox InstDate = report_parent.Items.Find("textBox15", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox InstName = report_parent.Items.Find("textBox33", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox InstMobile = report_parent.Items.Find("textBox34", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox InstEmail = report_parent.Items.Find("textBox35", true)[0] as Telerik.Reporting.TextBox;

				Telerik.Reporting.TextBox Capecity = report_parent.Items.Find("textBox15", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox Total = report_parent.Items.Find("textBox23", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Total = report_parent.Items.Find("textBox24", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox SubTotal = report_parent.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox STC = report_parent.Items.Find("textBox25", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox STCDesc = report_parent.Items.Find("textBox26", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox GrandTotal = report_parent.Items.Find("textBox27", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox STCIncetive = report_parent.Items.Find("textBox19", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox FinalQuote = report_parent.Items.Find("textBox28", true)[0] as Telerik.Reporting.TextBox;
				//	Telerik.Reporting.TextBox SpecialDiscount = report_parent.Items.Find("textBox28", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox SolarVICRebate = report_parent.Items.Find("textBox28", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox SolarVICLoanDiscount = report_parent.Items.Find("textBox20", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox AdditionalCharges = report_parent.Items.Find("textBox29", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Deposite = report_parent.Items.Find("textBox30", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox DepositeText = report_parent.Items.Find("textBox92", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Balance = report_parent.Items.Find("textBox31", true)[0] as Telerik.Reporting.TextBox;

				Telerik.Reporting.TextBox Panel = report_parent.Items.Find("textBox18", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Inverters = report_parent.Items.Find("textBox17", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox Inverter2 = report_parent.Items.Find("textBox172", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox Inverter3 = report_parent.Items.Find("textBox171", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox Inverter4 = report_parent.Items.Find("textBox177", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox InverterModelNo2 = report_parent.Items.Find("textBox176", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox Inverter3 = report_parent.Items.Find("textBox179", true)[0] as Telerik.Reporting.TextBox;
				//Telerik.Reporting.TextBox InverterModelNo3 = report_parent.Items.Find("textBox178", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Notes = report_parent.Items.Find("textBox32", true)[0] as Telerik.Reporting.TextBox;
				///by hiralTelerik.Reporting.TextBox RefNo = report_parent.Items.Find("textBox175", true)[0] as Telerik.Reporting.TextBox;

				Telerik.Reporting.TextBox SignCustName = report_parent.Items.Find("textBox174", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox SignCustDate = report_parent.Items.Find("textBox173", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.PictureBox CustSignature = report_parent.Items.Find("pictureBox6", true)[0] as Telerik.Reporting.PictureBox;
				Telerik.Reporting.PictureBox NearMap1 = report_parent.Items.Find("pictureBox7", true)[0] as Telerik.Reporting.PictureBox;
				Telerik.Reporting.PictureBox NearMap2 = report_parent.Items.Find("pictureBox8", true)[0] as Telerik.Reporting.PictureBox;
				Telerik.Reporting.PictureBox NearMap3 = report_parent.Items.Find("pictureBox9", true)[0] as Telerik.Reporting.PictureBox;

				Telerik.Reporting.PictureBox IMG1 = report_parent.Items.Find("pictureBox1", true)[0] as Telerik.Reporting.PictureBox;
				Telerik.Reporting.PictureBox IMG2 = report_parent.Items.Find("pictureBox2", true)[0] as Telerik.Reporting.PictureBox;
				Telerik.Reporting.PictureBox IMG3 = report_parent.Items.Find("pictureBox3", true)[0] as Telerik.Reporting.PictureBox;
				Telerik.Reporting.PictureBox IMG4 = report_parent.Items.Find("pictureBox4", true)[0] as Telerik.Reporting.PictureBox;
				Telerik.Reporting.PictureBox IMG5 = report_parent.Items.Find("pictureBox5", true)[0] as Telerik.Reporting.PictureBox;

				//Telerik.Reporting.Table SyatemDetail = report_parent.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
				//SyatemDetail.DataSource = QunityAndModelList;
				var Path = _env.WebRootPath;

				Image Url1 = Image.FromFile(Path + "/PDFImages/SavvyQuote1.png");
				Image Url2 = Image.FromFile(Path + "/PDFImages/SavvyQuote2.png");
				Image Url3 = Image.FromFile(Path + "/PDFImages/SavvyQuote3.png");
				Image Url4 = Image.FromFile(Path + "/PDFImages/SavvyQuote4.png");
				Image Url5 = Image.FromFile(Path + "/PDFImages/SavvyQuote5.png");

				IMG1.Value = Url1;
				IMG2.Value = Url2;
				IMG3.Value = Url3;
				IMG4.Value = Url4;
				IMG5.Value = Url5;

				var test = 0;
				int i = 0;
				foreach (var item in Addition)
				{
					if (i == 0)
					{
						test = Convert.ToInt32(item);
					}
					else
					{
						test = test + Convert.ToInt32(item);
					}
					i++;
				}
				var test1 = 0;
				int j = 0;
				foreach (var item in Discount)
				{
					if (j == 0)
					{
						test1 = Convert.ToInt32(item);
					}
					else
					{
						test1 = test1 + Convert.ToInt32(item);
					}
					j++;
				}

				QuoteNumber1.Value = JobDetail.JobNumber;
				//QuoteNumber2.Value = JobDetail.JobNumber;
				Date1.Value = DateTime.Now.AddHours(15).ToString("MMM dd, yyyy");
				//Date2.Value = DateTime.Now.AddHours(15).ToString("MMM dd, yyyy");
				Name.Value = Lead.CompanyName;
				Mobile.Value = Lead.Mobile;
				Email.Value = Lead.Email;
				if (JobDetail.Address != null)
				{
					Address1.Value = Convert.ToString(JobDetail.Address).Trim() + " " + JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode; ;
				}
				///Address2.Value = JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode;
				Capecity.Value = Convert.ToString(JobDetail.SystemCapacity) + " KW SOLAR";
				Notes.Value = JobDetail.Note;
				//if (SalesRep != null)
				//{
				//	InstName.Value = SalesRep.FullName;
				//	if (SalesRep.Mobile != null)
				//	{
				//		InstMobile.Value = SalesRep.PhoneNumber + "/" + SalesRep.Mobile;
				//	}
				//	else
				//	{
				//		InstMobile.Value = SalesRep.PhoneNumber;
				//	}
				//	InstEmail.Value = SalesRep.EmailAddress;
				//}
				//RefNo.Value = JobDetail.JobNumber;
				SubTotal.Value = Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate);
				Total.Value = Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate);
				STC.Value = Convert.ToString(JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.Rebate);
				STCDesc.Value = JobDetail.STC + " * " + JobDetail.STCPrice;
				GrandTotal.Value = Convert.ToString((JobDetail.BasicCost + JobDetail.Rebate) - (JobDetail.Rebate)) == null ? "$ 0.00" : "$ " + Convert.ToString((JobDetail.BasicCost + JobDetail.Rebate) - (JobDetail.Rebate));
				STCIncetive.Value = Convert.ToString((JobDetail.BasicCost + JobDetail.Rebate) - (JobDetail.Rebate)) == null ? "$ 0.00" : "$ " + Convert.ToString((JobDetail.BasicCost + JobDetail.Rebate) - (JobDetail.Rebate));
				SolarVICRebate.Value = Convert.ToString(JobDetail.SolarVICRebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.SolarVICRebate);
				SolarVICLoanDiscount.Value = Convert.ToString(JobDetail.SolarVICLoanDiscont) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.SolarVICRebate);
				decimal? FinalDeposite;
				if (TotalDeposite == 0 || TotalDeposite == null)
				{
					//DepositeText.Value = "Deposit Required";
					decimal? finalcost = ((JobDetail.BasicCost) - (JobDetail.SolarVICRebate + JobDetail.SolarVICLoanDiscont));
					Deposite.Value = JobDetail.DepositRequired == 0 ? "$ " + Math.Round((double)finalcost * 10 / 100) + ".00" : "$ " + Math.Round((double)JobDetail.DepositRequired) + ".00";
					FinalDeposite = Convert.ToDecimal(JobDetail.DepositRequired == 0 ? Math.Round((double)finalcost * 10 / 100) : Math.Round((double)JobDetail.DepositRequired));
				}
				else
				{
					//DepositeText.Value = "Deposit Paid";
					Deposite.Value = "$ " + TotalDeposite;
					FinalDeposite = TotalDeposite;
				}

				//Total.Value = Convert.ToString(JobDetail.BasicCost) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost);
				if (TotalDeposite == 0 || TotalDeposite == null)
				{
					Balance.Value = Convert.ToString((JobDetail.BasicCost) - (JobDetail.SolarVICRebate + JobDetail.SolarVICLoanDiscont) - JobDetail.DepositRequired) == null ? "$ 0.00" : "$ " + Convert.ToString((JobDetail.BasicCost) - (JobDetail.SolarVICRebate + JobDetail.SolarVICLoanDiscont) - FinalDeposite);
				}
				else
				{
					Balance.Value = Convert.ToString((JobDetail.BasicCost) - (JobDetail.SolarVICRebate + JobDetail.SolarVICLoanDiscont) - TotalDeposite) == null ? "$ 0.00" : "$ " + Convert.ToString((JobDetail.BasicCost) - (JobDetail.SolarVICRebate + JobDetail.SolarVICLoanDiscont) - TotalDeposite);
				}

				MeaterPhase.Value = Convert.ToString(JobDetail.MeterPhaseId);

				//if (JobDetail.PaymentOptionId == 1)
				//{
				//	PaymentMethod.Value = "Cash";
				//}
				//else
				//{
				//	PaymentMethod.Value = Payment + " with " + Dep + " for " + Finance;
				//}

				//SpecialDiscount.Value = test1 == 0 ? "$ 0.00" : "$ " + test1 + ".00";
				///AdditionalCharges.Value = test == 0 ? "$ 0.00" : "$ " + test + ".00";
				if (JobDetail.MeterUpgradeId == 1)
				{
					MeaterUpgrade.Value = "Yes";
				}
				else
				{
					MeaterUpgrade.Value = "No";
				}
				NoofStory.Value = HouseType;
				RoofTypes.Value = RoofType;
				RoofPinch.Value = RoofAngle;
				EnergyDist.Value = Dist;
				EnergyRetailer.Value = Retailer;
				//NMINumber.Value = ": " + JobDetail.NMINumber;
				//if (JobDetail.InstallationDate != null)
				//{
				//	InstDate.Value = JobDetail.InstallationDate.Value.ToString("DD/MM/yyyy");
				//}
				if (PanelDetail != null)
				{
					var PanelQTY = ProductItemsList.Where(e => e.ProductItemId == PanelDetail.Id).Select(e => e.Quantity).FirstOrDefault();

					Panel.Value = PanelQTY + " x " + PanelDetail.Name;
					//PanelModelNo.Value = PanelDetail.Model;
				}
				//for (int k = 0; k < InverterDetail.Count; k++)
				//{
				//	if(k == 0)
				//	{
				//		var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail[k].Id).Select(e => e.Quantity).FirstOrDefault();

				//		//Inverter1.Value = InverterQTY + " x " + InverterDetail[k].Name;
				//		//InverterModelNo1.Value = InverterDetail[k].Model;
				//	}
				//	if (k == 1)
				//	{
				//		var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail[k].Id).Select(e => e.Quantity).FirstOrDefault();

				//		//Inverter2.Value = InverterQTY + " x " + InverterDetail[k].Name;
				//		//InverterModelNo2.Value = InverterDetail[k].Model;
				//	}
				//	if (k == 2)
				//	{
				//		var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail[k].Id).Select(e => e.Quantity).FirstOrDefault();

				//		//Inverter3.Value = InverterQTY + " x " + InverterDetail[k].Name;
				//		//InverterModelNo3.Value = InverterDetail[k].Model;
				//	}
				//	if (k == 3)
				//	{
				//		var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail[k].Id).Select(e => e.Quantity).FirstOrDefault();

				//		//Inverter4.Value = InverterQTY + " x " + InverterDetail[k].Name;
				//		//InverterModelNo4.Value = InverterDetail[k].Model;
				//	}
				//}
				if (Inverter != null)
				{
					var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == Inverter.Id).Select(e => e.Quantity).FirstOrDefault();

					Inverters.Value = InverterQTY + " x " + Inverter.Name;
					//InverterModelNo.Value = InverterDetail.Model;
				}
				SignCustName.Value = Lead.CompanyName;
				SignCustDate.Value = DateTime.Now.AddHours(15).ToString("dd/MM/yyyy");
				if (ImagePath != null)
				{
					CustSignature.Value = ImagePath;
				}

				if (NearMap != null)
				{
					if (!string.IsNullOrEmpty(NearMap.NearMapImage1))
					{
						Image image1 = Image.FromFile(Path + NearMap.FilePath + "\\" + NearMap.NearMapImage1);
						NearMap1.Value = image1;
					}
					if (!string.IsNullOrEmpty(NearMap.NearMapImage2))
					{
						Image image2 = Image.FromFile(Path + NearMap.FilePath + "\\" + NearMap.NearMapImage2);
						NearMap2.Value = image2;
					}
					if (!string.IsNullOrEmpty(NearMap.NearMapImage3))
					{
						Image image3 = Image.FromFile(Path + NearMap.FilePath + "\\" + NearMap.NearMapImage3);
						NearMap3.Value = image3;
					}
				}

				var Filename = DateTime.Now.Ticks + "_Quotation";
				SaveTelerikPDF(report_parent, JobDetail.Id, JobDetail.TenantId, Filename);
				return Filename;
			}
			else
			{
				return null;
			}
		}

		public async Task<string> Refund(int JobId, int Id)
		{
			var report_parent = new Report.NewRefundReceipt();
			var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
			var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
			var ProductItemList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).Select(e => e.ProductItemId).ToList();
			var ProductItemsList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).ToList();
			var PanelDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 1).FirstOrDefault();
			var PanelQTY = ProductItemsList.Where(e => e.ProductItemId == PanelDetail.Id).Select(e => e.Quantity).FirstOrDefault();
			var JobStatus = _jobStatusRepository.GetAll().Where(e => e.Id == JobDetail.JobStatusId).Select(e => e.Name).FirstOrDefault();
			var CurrentUser = _userRepository.GetAll().Where(e => e.Id == Lead.AssignToUserID).FirstOrDefault();
			var DepDateDetail = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id).Select(e => e.InvoicePayDate).FirstOrDefault();
			var JobRefundDetails = _jobRefundRepository.GetAll().Where(e => e.JobId == JobDetail.Id).FirstOrDefault();
			var RefundReason = _refundReasonRepository.GetAll().Where(e => e.Id == JobRefundDetails.RefundReasonId).Select(e => e.Name).FirstOrDefault();
			IList<string> role = await _userManager.GetRolesAsync(CurrentUser);

			Telerik.Reporting.PictureBox IMG1 = report_parent.Items.Find("pictureBox1", true)[0] as Telerik.Reporting.PictureBox;
			Telerik.Reporting.TextBox Date = report_parent.Items.Find("textBox22", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Name = report_parent.Items.Find("textBox45", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Email = report_parent.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Mobile = report_parent.Items.Find("textBox39", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox ProjectNo = report_parent.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Status = report_parent.Items.Find("textBox8", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox SalesPerson = report_parent.Items.Find("textBox9", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox DepDate = report_parent.Items.Find("textBox10", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox EnterDate = report_parent.Items.Find("textBox11", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox ManagerName = report_parent.Items.Find("textBox12", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox NoofPanel = report_parent.Items.Find("textBox14", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Reason = report_parent.Items.Find("textBox48", true)[0] as Telerik.Reporting.TextBox;

			Telerik.Reporting.TextBox OName = report_parent.Items.Find("textBox34", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox OEmail = report_parent.Items.Find("textBox32", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox OMobile = report_parent.Items.Find("textBox33", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox OProjectNo = report_parent.Items.Find("textBox31", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox BankName = report_parent.Items.Find("textBox24", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox AccountName = report_parent.Items.Find("textBox20", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox BSENo = report_parent.Items.Find("textBox23", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox ACNo = report_parent.Items.Find("textBox21", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Amount = report_parent.Items.Find("textBox19", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox RefundDate = report_parent.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox ReasonNotRefund = report_parent.Items.Find("textBox49", true)[0] as Telerik.Reporting.TextBox;
			//Telerik.Reporting.TextBox OReason = report_parent.Items.Find("textBox58", true)[0] as Telerik.Reporting.TextBox;
			//Telerik.Reporting.TextBox SpecialNotes = report_parent.Items.Find("textBox59", true)[0] as Telerik.Reporting.TextBox;

			var Path = _env.WebRootPath;
			Image Url1 = Image.FromFile(Path + "/PDFImages/Refund.png");
			IMG1.Value = Url1;

			Date.Value = DateTime.Now.AddHours(15).ToString("MMMM dd, yyyy");
			Name.Value = Lead.CompanyName;
			Email.Value = Lead.Email;
			Mobile.Value = Lead.Mobile;
			ProjectNo.Value = ": " + JobDetail.JobNumber;
			Status.Value = ": " + JobStatus;
			if (role.Contains("Sales Manager"))
			{
				SalesPerson.Value = ": ";
				ManagerName.Value = ": " + CurrentUser.FullName;
			}
			else
			{
				var User_List = _userRepository.GetAll().ToList();
				var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == Lead.AssignToUserID).Select(e => e.TeamId).FirstOrDefault();

				var UserList = (from user in User_List
								join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
								from ur in urJoined.DefaultIfEmpty()

								join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
								from us in usJoined.DefaultIfEmpty()

								join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
								from ut in utJoined.DefaultIfEmpty()

								where us != null && us.DisplayName == "Sales Rep" && ut != null && ut.TeamId == TeamId
								select user).Distinct().ToList();

				SalesPerson.Value = ": " + CurrentUser.FullName;
				ManagerName.Value = ": " + UserList[0].FullName;
			}

			DepDate.Value = ": " + DepDateDetail.Value.ToString("dd/MM/yyyy");
			EnterDate.Value = ": " + JobDetail.CreationTime.ToString("dd/MM/yyyy");
			NoofPanel.Value = ": " + Convert.ToString(PanelQTY);
			Reason.Value = RefundReason;

			OName.Value = Lead.CompanyName;
			OEmail.Value = Lead.Email;
			OMobile.Value = Lead.Mobile;
			OProjectNo.Value = ": " + JobDetail.JobNumber;
			//OReason.Value = RefundReason;
			BankName.Value = ": " + JobRefundDetails.BankName;
			AccountName.Value = ": " + JobRefundDetails.AccountName;
			BSENo.Value = ": " + JobRefundDetails.BSBNo;
			ACNo.Value = ": " + JobRefundDetails.AccountNo;
			Amount.Value = ": " + Convert.ToString(JobRefundDetails.Amount);
			if (JobRefundDetails.PaidDate != null)
			{
				RefundDate.Value = ": " + JobRefundDetails.PaidDate.Value.ToString("DD/MM/yyyy");
			}
			ReasonNotRefund.Value = JobRefundDetails.Remarks;

			var Filename = DateTime.Now.Ticks + "_Refund";
			SaveTelerikPDF(report_parent, JobDetail.Id, JobDetail.TenantId, Filename);
			return Filename;
		}

		public string TaxInvoice(int JobId)
		{
			var report_parent = new Report.NewTaxInvoice();
			var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
			var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
			var ProductItemList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).Select(e => e.ProductItemId).ToList();
			var ProductItemsList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).ToList();
			var PanelDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 1).FirstOrDefault();
			var InverterDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 2).FirstOrDefault();
			var DepositeDetail = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id).FirstOrDefault();
			var DepositeDetailList = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id).Select(e => e.InvoicePayTotal).Sum();

			Telerik.Reporting.PictureBox IMG1 = report_parent.Items.Find("pictureBox1", true)[0] as Telerik.Reporting.PictureBox;

			Telerik.Reporting.TextBox CustName = report_parent.Items.Find("textBox2", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox CustMobile = report_parent.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox CustEmail = report_parent.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox CustAddress1 = report_parent.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
			//Telerik.Reporting.TextBox CustAddress2 = report_parent.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox BalanceDue = report_parent.Items.Find("textBox54", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox InvoiceNo = report_parent.Items.Find("textBox6", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Date = report_parent.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Panel = report_parent.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Inverter = report_parent.Items.Find("textBox9", true)[0] as Telerik.Reporting.TextBox;

			Telerik.Reporting.TextBox TotalCost = report_parent.Items.Find("textBox8", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox LessSTC = report_parent.Items.Find("textBox10", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox NetCost = report_parent.Items.Find("textBox11", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox DepositePaid = report_parent.Items.Find("textBox12", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Balance = report_parent.Items.Find("textBox13", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox HeaderDate = report_parent.Items.Find("textBox16", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox FinalBalance = report_parent.Items.Find("textBox14", true)[0] as Telerik.Reporting.TextBox;

			var Path = _env.WebRootPath;
			Image Url1 = Image.FromFile(Path + "/PDFImages/TaxInvoice.png");
			IMG1.Value = Url1;

			CustName.Value = Lead.CompanyName;
			CustMobile.Value = ": " + Lead.Mobile;
			CustEmail.Value = ": " + Lead.Email;
			if (JobDetail.Address != null)
			{
				CustAddress1.Value = ": " + JobDetail.Address.Trim() + " " + JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode;
			}
			//CustAddress2.Value = JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode;

			InvoiceNo.Value = JobDetail.JobNumber;
			TotalCost.Value = Convert.ToString(JobDetail.TotalCost + JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.TotalCost + JobDetail.Rebate);
			LessSTC.Value = Convert.ToString(JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.Rebate);
			NetCost.Value = Convert.ToString(JobDetail.TotalCost) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.TotalCost);
			if (DepositeDetail != null)
			{
				DepositePaid.Value = Convert.ToString(DepositeDetail.InvoicePayTotal) == null ? "$ 0.00" : "$ " + Convert.ToString(DepositeDetail.InvoicePayTotal);
			}
			Balance.Value = Convert.ToString(JobDetail.TotalCost - DepositeDetailList) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.TotalCost - DepositeDetailList);
			FinalBalance.Value = Convert.ToString(JobDetail.TotalCost - DepositeDetailList) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.TotalCost - DepositeDetailList);
			BalanceDue.Value = Convert.ToString(JobDetail.TotalCost - DepositeDetailList) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.TotalCost - DepositeDetailList);
			Date.Value = DateTime.Now.AddHours(15).ToString("MMM dd, yyyy");
			HeaderDate.Value = DateTime.Now.AddHours(15).ToString("MMMM dd, yyyy");
			//GST.Value = Convert.ToString(JobDetail.BasicCost * 10 / 100) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost * 10 / 100);
			if (PanelDetail != null)
			{
				var PanelQTY = ProductItemsList.Where(e => e.ProductItemId == PanelDetail.Id).Select(e => e.Quantity).FirstOrDefault();

				Panel.Value = PanelQTY + " x " + PanelDetail.Name + "(" + PanelDetail.Model + ")";
			}
			if (InverterDetail != null)
			{
				var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail.Id).Select(e => e.Quantity).FirstOrDefault();

				Inverter.Value = InverterQTY + " x " + InverterDetail.Name + "(" + InverterDetail.Model + ") KW Inverter";
			}

			var Filename = DateTime.Now.Ticks + "_TaxInvoice";
			SaveTelerikPDF(report_parent, JobDetail.Id, JobDetail.TenantId, Filename);
			return Filename;
		}

		public string PaymentReceipt(int JobId)
		{
			var report_parent = new Report.NewPaymentReceipt();
			var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
			var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
			var TotalDeposite = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id).Select(e => e.InvoicePayTotal).Sum();
			var MinusList = _variationRepository.GetAll().Where(e => e.Action == "Minus").Select(e => e.Id).ToList();
			var Discount = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && MinusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
			var PlusList = _variationRepository.GetAll().Where(e => e.Action == "Plus").Select(e => e.Id).ToList();
			var Addition = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && PlusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();

			var DepositeDetailList = from o in _invoicePaymentRepository.GetAll()
									 join o1 in _invoicePaymentMethodRepository.GetAll() on o.InvoicePaymentMethodId equals o1.Id into j1
									 from s1 in j1.DefaultIfEmpty()
									 where (o.JobId == JobDetail.Id)
									 select new
									 {
										 o.InvoicePayDate,
										 o.InvoicePayTotal,
										 o.CCSurcharge,
										 s1.PaymentMethod
									 };

			Telerik.Reporting.PictureBox IMG1 = report_parent.Items.Find("pictureBox1", true)[0] as Telerik.Reporting.PictureBox;

			Telerik.Reporting.TextBox Name = report_parent.Items.Find("textBox8", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Mobile = report_parent.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Email = report_parent.Items.Find("textBox2", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Address1 = report_parent.Items.Find("textBox13", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Address2 = report_parent.Items.Find("textBox14", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox InvoiceTotal = report_parent.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox TotalCost = report_parent.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox LessDiscount = report_parent.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox LessSTCRebate = report_parent.Items.Find("textBox6", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox NetCost = report_parent.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox BalanceDue = report_parent.Items.Find("textBox15", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox HeaderDate = report_parent.Items.Find("textBox16", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox CustName = report_parent.Items.Find("textBox39", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Date = report_parent.Items.Find("textBox35", true)[0] as Telerik.Reporting.TextBox;

			Telerik.Reporting.Table Payment = report_parent.Items.Find("table1", true)[0] as Telerik.Reporting.Table;

			var Path = _env.WebRootPath;
			Image Url1 = Image.FromFile(Path + "/PDFImages/PaymentReceipt.png");
			IMG1.Value = Url1;

			if (JobDetail.Address != null)
			{
				Address1.Value = JobDetail.Address.Trim();
			}
			Address2.Value = JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode;
			Name.Value = Lead.CompanyName;
			Mobile.Value = ": " + Lead.Mobile;
			Email.Value = ": " + Lead.Email;
			InvoiceTotal.Value = Convert.ToString(JobDetail.BasicCost) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost);
			decimal Add = 0;
			foreach (var item in Addition)
			{
				Add = (decimal)+item;
			}
			TotalCost.Value = Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate + Add) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate + Add);
			LessSTCRebate.Value = Convert.ToString(JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.Rebate);
			decimal Disc = 0;
			foreach (var item in Discount)
			{
				Disc = (decimal)+item;
			}
			LessDiscount.Value = Convert.ToString(Disc) == null ? "0.00" : "$ " + Convert.ToString(Disc);
			NetCost.Value = Convert.ToString(JobDetail.BasicCost - Disc) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost - Disc);
			Payment.DataSource = DepositeDetailList.ToList();
			var Due = JobDetail.TotalCost - TotalDeposite;
			var GST = Due * 10 / 100;
			BalanceDue.Value = ": " + Convert.ToString(Due) == null ? "$ 0.00" : "$ " + Convert.ToString(Due);
			CustName.Value = Lead.CompanyName;
			Date.Value = DateTime.Now.AddHours(15).ToString("dd MMM, yyyy");
			HeaderDate.Value = DateTime.Now.AddHours(15).ToString("MMM dd, yyyy");
			var Filename = DateTime.Now.Ticks + "_PaymentReceipt";
			SaveTelerikPDF(report_parent, JobDetail.Id, JobDetail.TenantId, Filename);
			return Filename;
		}

		public async Task<string> PickList(int JobId, int PicklistId)
		{
			var report_parent = new Report.NewPickList();
			var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
			var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
			var Installer = _userRepository.GetAll().Where(e => e.Id == JobDetail.InstallerId).FirstOrDefault();
			var RoofTypeName = _roofTypeRepository.GetAll().Where(e => e.Id == JobDetail.RoofTypeId).Select(e => e.Name).FirstOrDefault();
			var RoofAngle = _roofAngleRepository.GetAll().Where(e => e.Id == JobDetail.RoofTypeId).Select(e => e.Name).FirstOrDefault();
			var WareHouse = _wareHouseLocationRepository.GetAll().Where(e => e.Id == JobDetail.WarehouseLocation).Select(e => e.state).FirstOrDefault();
			var PickList = await _pickListAppService.GetPickListForReport(PicklistId);

			Telerik.Reporting.PictureBox IMG1 = report_parent.Items.Find("pictureBox1", true)[0] as Telerik.Reporting.PictureBox;

			Telerik.Reporting.TextBox Name = report_parent.Items.Find("textBox45", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Mobile = report_parent.Items.Find("textBox39", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Email = report_parent.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox CustAddress1 = report_parent.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox CustAddress2 = report_parent.Items.Find("textBox6", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox InstallDate = report_parent.Items.Find("textBox13", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox InstallerName = report_parent.Items.Find("textBox46", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox PickedBy = report_parent.Items.Find("textBox48", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox PickedDate = report_parent.Items.Find("textBox47", true)[0] as Telerik.Reporting.TextBox;

			Telerik.Reporting.TextBox HouseType = report_parent.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox RoofType = report_parent.Items.Find("textBox8", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Angle = report_parent.Items.Find("textBox9", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox ManualQuote = report_parent.Items.Find("textBox10", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox ProjectNo = report_parent.Items.Find("textBox22", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Store = report_parent.Items.Find("textBox12", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Notes = report_parent.Items.Find("textBox18", true)[0] as Telerik.Reporting.TextBox;

			Telerik.Reporting.Table PickListTable = report_parent.Items.Find("table1", true)[0] as Telerik.Reporting.Table;

			var Path = _env.WebRootPath;
			Image Url1 = Image.FromFile(Path + "/PDFImages/PickList.png");
			IMG1.Value = Url1;

			Name.Value = Lead.CompanyName;
			Mobile.Value = Lead.Mobile;
			Email.Value = Lead.Email;
			CustAddress1.Value = JobDetail.Address.Trim();
			CustAddress2.Value = JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode;
			InstallDate.Value = JobDetail.InstallationDate.Value.ToString("dd MMM yyyy");
			InstallerName.Value = ": " + Installer.FullName;
			PickedBy.Value = ": " + Installer.FullName;
			PickedDate.Value = ": " + DateTime.Now.AddHours(15).ToString("dd MMM yyyy");
			if (JobDetail.HouseTypeId == 1)
			{
				HouseType.Value = ": " + "Single";
			}
			else if (JobDetail.HouseTypeId == 2)
			{
				HouseType.Value = ": " + "Double";
			}
			else
			{
				HouseType.Value = ": " + "Multiple";
			}
			RoofType.Value = ": " + RoofTypeName;
			Angle.Value = ": " + RoofAngle;
			ProjectNo.Value = ": " + JobDetail.JobNumber;
			Store.Value = ": " + WareHouse;
			ManualQuote.Value = ": " + JobDetail.ManualQuote;
			Notes.Value = JobDetail.InstallationNotes;
			PickListTable.DataSource = PickList;

			var Filename = DateTime.Now.Ticks + "_PickList";
			SaveTelerikPDF(report_parent, JobDetail.Id, JobDetail.TenantId, Filename);
			return Filename;
		}

		private void SaveTelerikPDF(Telerik.Reporting.Report rpt, int JobId, int TenantId, string FileName)
		{
			Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
			Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

			instanceReportSource.ReportDocument = rpt;

			Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

			string fileName = FileName + "." + result.Extension;

			//string fullPath = Path.Combine(_appFolders.Report, fileName);

			var Path = _env.WebRootPath;
			var MainFolder = Path;
			MainFolder = MainFolder + "\\Documents";
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == TenantId).Select(e => e.TenancyName).FirstOrDefault();
			var JobNumber = _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.JobNumber).FirstOrDefault();

			string TenantPath = MainFolder + "\\" + TenantName;
			string JobPath = TenantPath + "\\" + JobNumber;
			string SignaturePath = JobPath + "\\Reports";

			if (System.IO.Directory.Exists(MainFolder))
			{
				if (System.IO.Directory.Exists(TenantPath))
				{
					if (System.IO.Directory.Exists(JobPath))
					{
						if (System.IO.Directory.Exists(SignaturePath))
						{
							var FinalFilePath = SignaturePath + "\\" + fileName;

							FileStream fs = new FileStream(FinalFilePath, FileMode.Create, FileAccess.Write);
							fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
							fs.Close();
						}
						else
						{
							System.IO.Directory.CreateDirectory(SignaturePath);
							var FinalFilePath = SignaturePath + "\\" + fileName;

							FileStream fs = new FileStream(FinalFilePath, FileMode.Create, FileAccess.Write);
							fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
							fs.Close();
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(JobPath);
						if (System.IO.Directory.Exists(SignaturePath))
						{
							var FinalFilePath = SignaturePath + "\\" + fileName;

							FileStream fs = new FileStream(FinalFilePath, FileMode.Create, FileAccess.Write);
							fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
							fs.Close();
						}
						else
						{
							System.IO.Directory.CreateDirectory(SignaturePath);
							var FinalFilePath = SignaturePath + "\\" + fileName;

							FileStream fs = new FileStream(FinalFilePath, FileMode.Create, FileAccess.Write);
							fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
							fs.Close();
						}
					}
				}
				else
				{
					System.IO.Directory.CreateDirectory(TenantPath);
					if (System.IO.Directory.Exists(JobPath))
					{
						if (System.IO.Directory.Exists(SignaturePath))
						{
							var FinalFilePath = SignaturePath + "\\" + fileName;

							FileStream fs = new FileStream(FinalFilePath, FileMode.Create, FileAccess.Write);
							fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
							fs.Close();
						}
						else
						{
							System.IO.Directory.CreateDirectory(SignaturePath);
							var FinalFilePath = SignaturePath + "\\" + fileName;

							FileStream fs = new FileStream(FinalFilePath, FileMode.Create, FileAccess.Write);
							fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
							fs.Close();
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(JobPath);
						if (System.IO.Directory.Exists(SignaturePath))
						{
							var FinalFilePath = SignaturePath + "\\" + fileName;

							FileStream fs = new FileStream(FinalFilePath, FileMode.Create, FileAccess.Write);
							fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
							fs.Close();
						}
						else
						{
							System.IO.Directory.CreateDirectory(SignaturePath);
							var FinalFilePath = SignaturePath + "\\" + fileName;

							FileStream fs = new FileStream(FinalFilePath, FileMode.Create, FileAccess.Write);
							fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
							fs.Close();
						}
					}
				}
			}
			else
			{
				System.IO.Directory.CreateDirectory(MainFolder);
				if (System.IO.Directory.Exists(TenantPath))
				{
					if (System.IO.Directory.Exists(JobPath))
					{
						if (System.IO.Directory.Exists(SignaturePath))
						{
							var FinalFilePath = SignaturePath + "\\" + fileName;

							FileStream fs = new FileStream(FinalFilePath, FileMode.Create, FileAccess.Write);
							fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
							fs.Close();
						}
						else
						{
							System.IO.Directory.CreateDirectory(SignaturePath);
							var FinalFilePath = SignaturePath + "\\" + fileName;

							FileStream fs = new FileStream(FinalFilePath, FileMode.Create, FileAccess.Write);
							fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
							fs.Close();
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(JobPath);
						if (System.IO.Directory.Exists(SignaturePath))
						{
							var FinalFilePath = SignaturePath + "\\" + fileName;

							FileStream fs = new FileStream(FinalFilePath, FileMode.Create, FileAccess.Write);
							fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
							fs.Close();
						}
						else
						{
							System.IO.Directory.CreateDirectory(SignaturePath);
							var FinalFilePath = SignaturePath + "\\" + fileName;

							FileStream fs = new FileStream(FinalFilePath, FileMode.Create, FileAccess.Write);
							fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
							fs.Close();
						}
					}
				}
				else
				{
					System.IO.Directory.CreateDirectory(TenantPath);
					if (System.IO.Directory.Exists(JobPath))
					{
						if (System.IO.Directory.Exists(SignaturePath))
						{
							var FinalFilePath = SignaturePath + "\\" + fileName;

							FileStream fs = new FileStream(FinalFilePath, FileMode.Create, FileAccess.Write);
							fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
							fs.Close();
						}
						else
						{
							System.IO.Directory.CreateDirectory(SignaturePath);
							var FinalFilePath = SignaturePath + "\\" + fileName;

							FileStream fs = new FileStream(FinalFilePath, FileMode.Create, FileAccess.Write);
							fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
							fs.Close();
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(JobPath);
						if (System.IO.Directory.Exists(SignaturePath))
						{
							var FinalFilePath = SignaturePath + "\\" + fileName;

							FileStream fs = new FileStream(FinalFilePath, FileMode.Create, FileAccess.Write);
							fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
							fs.Close();
						}
						else
						{
							System.IO.Directory.CreateDirectory(SignaturePath);
							var FinalFilePath = SignaturePath + "\\" + fileName;

							FileStream fs = new FileStream(FinalFilePath, FileMode.Create, FileAccess.Write);
							fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
							fs.Close();
						}
					}
				}
			}
		}
	}
}