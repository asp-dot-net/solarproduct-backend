﻿using Abp.Localization;
using Abp.Localization.Sources;
using NPOI.HSSF.Record;
using NPOI.SS.UserModel;
using NPOI.XSSF.Streaming.Values;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Invoices.Importing.Dto;

namespace TheSolarProduct.Invoices.Importing
{
    public class InvoicesListExcelDataReader : NpoiExcelImporterBase<ImportInvoicesDto>, IInvoicesListExcelDataReader
    {
        private readonly ILocalizationSource _localizationSource;

        public InvoicesListExcelDataReader(ILocalizationManager localizationManager)
        {
            _localizationSource = localizationManager.GetSource(TheSolarProductConsts.LocalizationSourceName);
        }

        public List<ImportInvoicesDto> GetInvoicesFromExcel(byte[] fileBytes)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow);
        }

        private ImportInvoicesDto ProcessExcelRow(ISheet worksheet, int row)
        {
            //if (IsRowEmpty(worksheet, row))
            //{
            //    return null;
            //}

            var exceptionMessage = new StringBuilder();
            var invoice = new ImportInvoicesDto();

            IRow roww = worksheet.GetRow(row);
            List<ICell> cells = roww.Cells;
            List<string> rowData = new List<string>();
            List<DateTime> rowDataDate = new List<DateTime>();

            for (int colNumber = 0; colNumber <= 9; colNumber++)
            {
                ICell cell = roww.GetCell(colNumber, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                rowData.Add(cell.ToString());
            }

            try
            {
                //var time = "00:00:00";
                //invoice.Date = Convert.ToDateTime(rowData[0]);
                //invoice.Date = DateTime.Parse(rowData[0], CultureInfo.CurrentCulture.DateTimeFormat);
                invoice.Date = DateTime.Parse(rowData[0], CultureInfo.CreateSpecificCulture("en-AU"));
                //invoice.Date = DateTime.ParseExact(rowData[0] + " " + time, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                invoice.ProjectNo = rowData[1];
                invoice.PayBy = rowData[2];
                invoice.Description = rowData[3];
                invoice.AmountPaid = rowData[4] == null || rowData[4] == "" ? 0 : Convert.ToDecimal(rowData[4]);
                invoice.SSCharge = rowData[5] == null || rowData[5] == "" ? 0 : Convert.ToDecimal(rowData[5]);
                invoice.AllocatedBy = rowData[6];
                invoice.ReceiptNumber = rowData[7];
                invoice.PurchaseNumber = rowData[8];
                invoice.InvoiceNotesDescription = rowData[9];
              }

            catch (System.Exception exception)
            {
                invoice.Exception = exception.Message;
            }

            return invoice;
        }

        private string GetLocalizedExceptionMessagePart(string parameter)
        {
            return _localizationSource.GetString("{0}IsInvalid", _localizationSource.GetString(parameter)) + "; ";
        }

        private string GetRequiredValueFromRowOrNull(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].StringCellValue;
            if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue))
            {
                return cellValue;
            }

            exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
            return null;
        }

        private double GetValue(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].NumericCellValue;

            return cellValue;

        }

        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
            return cell == null || string.IsNullOrWhiteSpace(cell.StringCellValue);
        }
    }
}
