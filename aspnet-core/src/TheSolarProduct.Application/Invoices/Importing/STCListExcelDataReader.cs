﻿using Abp.Localization;
using Abp.Localization.Sources;
using NPOI.HSSF.Record;
using NPOI.SS.UserModel;
using NPOI.XSSF.Streaming.Values;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Invoices.Importing.Dto;

namespace TheSolarProduct.Invoices.Importing
{
    public class STCListExcelDataReader : NpoiExcelImporterBase<ImportSTCDto>, ISTCListExcelDataReader
    {
        private readonly ILocalizationSource _localizationSource;

        public STCListExcelDataReader(ILocalizationManager localizationManager)
        {
            _localizationSource = localizationManager.GetSource(TheSolarProductConsts.LocalizationSourceName);
        }

        public List<ImportSTCDto> GetSTCFromExcel(byte[] fileBytes)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow);
        }

        private ImportSTCDto ProcessExcelRow(ISheet worksheet, int row)
        {
            //if (IsRowEmpty(worksheet, row))
            //{
            //    return null;
            //}

            var exceptionMessage = new StringBuilder();
            var stc = new ImportSTCDto();

            IRow roww = worksheet.GetRow(row);
            List<ICell> cells = roww.Cells;
            List<string> rowData = new List<string>();
            List<DateTime> rowDataDate = new List<DateTime>();

            for (int colNumber = 0; colNumber <= 5; colNumber++)
            {
                ICell cell = roww.GetCell(colNumber, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                rowData.Add(cell.ToString());
            }

            try
            {
                stc.PVDNumber = Convert.ToString(rowData[0]);
                stc.ProjectNumber = rowData[1];
                stc.ManualQuote = rowData[2];
                stc.PVDStatus = Convert.ToInt32(rowData[4]);
                stc.STCAppliedDate = DateTime.Parse(rowData[5], CultureInfo.CreateSpecificCulture("en-AU"));
                //stc.STCAppliedDate = DateTime.Parse(rowData[5], CultureInfo.CurrentCulture.DateTimeFormat);/*Convert.ToDateTime(rowData[5]);*/
            }

            catch (System.Exception exception)
            {
                stc.Exception = exception.Message;
            }

            return stc;
        }

        private string GetLocalizedExceptionMessagePart(string parameter)
        {
            return _localizationSource.GetString("{0}IsInvalid", _localizationSource.GetString(parameter)) + "; ";
        }

        private string GetRequiredValueFromRowOrNull(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].StringCellValue;
            if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue))
            {
                return cellValue;
            }

            exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
            return null;
        }

        private double GetValue(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].NumericCellValue;

            return cellValue;

        }

        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
            return cell == null || string.IsNullOrWhiteSpace(cell.StringCellValue);
        }
    }
}
