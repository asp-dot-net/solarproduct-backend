﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Add_referralDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AccountName",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AccountNo",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BsbNo",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ReferralAmount",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RefferedJobStatusId",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountName",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "AccountNo",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "BsbNo",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ReferralAmount",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "RefferedJobStatusId",
                table: "Jobs");
        }
    }
}
