﻿using TheSolarProduct.SmsTemplates;
using TheSolarProduct.HoldReasons;

using TheSolarProduct.Installer;
using TheSolarProduct.Invoices;
using TheSolarProduct.CancelReasons;
using TheSolarProduct.RejectReasons;
using TheSolarProduct.Jobs;
using TheSolarProduct.Promotions;
using TheSolarProduct.Departments;
using TheSolarProduct.Leads;
using TheSolarProduct.LeadSources;
using TheSolarProduct.PostalTypes;
using TheSolarProduct.PostCodes;
using TheSolarProduct.States;
using TheSolarProduct.StreetNames;
using TheSolarProduct.StreetTypes;
using TheSolarProduct.UnitTypes;
using Abp.IdentityServer4;
using Abp.Organizations;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Authorization.Delegation;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Chat;
using TheSolarProduct.Editions;
using TheSolarProduct.Friendships;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.MultiTenancy.Accounting;
using TheSolarProduct.MultiTenancy.Payments;
using TheSolarProduct.Storage;
using TheSolarProduct.LeadStatuses;
using TheSolarProduct.LeadActions;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Expense;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Quotations;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.EmailTemplates;
using TheSolarProduct.Organizations;
using TheSolarProduct.PVDStatuses;
using TheSolarProduct.CasualMaintenances;
using TheSolarProduct.PreviousJobStatuses;
using TheSolarProduct.LeadHistory;
using TheSolarProduct.Sections;

namespace TheSolarProduct.EntityFrameworkCore
{
	public class TheSolarProductDbContext : AbpZeroDbContext<Tenant, Role, User, TheSolarProductDbContext>, IAbpPersistedGrantDbContext
	{
		public virtual DbSet<SmsTemplate> SmsTemplates { get; set; }

		public virtual DbSet<FreebieTransport> FreebieTransports { get; set; }

		public virtual DbSet<InvoiceStatus> InvoiceStatuses { get; set; }

		public virtual DbSet<casualmaintenance> CasualMaintenances { get; set; }
		public virtual DbSet<QuotationLinkHistory> QuotationLinkHistorys { get; set; }

		public virtual DbSet<PVDStatus> PVDStatus { get; set; }

		public virtual DbSet<ExtendOrganizationUnit> ExtendOrganizationUnits { get; set; }

		public virtual DbSet<HoldReason> HoldReasons { get; set; }

		public virtual DbSet<ProductItemLocation> ProductItemLocations { get; set; }

		public virtual DbSet<Warehouselocation> Warehouselocations { get; set; }

		public virtual DbSet<JobCancellationReason> JobCancellationReasons { get; set; }

		public virtual DbSet<EmailTemplate> EmailTemplates { get; set; }

		public virtual DbSet<RefundReason> RefundReasons { get; set; }

		public virtual DbSet<JobRefund> JobRefunds { get; set; }

		public virtual DbSet<InstallerDetail> InstallerDetails { get; set; }
		public virtual DbSet<InstallerContract> InstallerContracts { get; set; }
		public virtual DbSet<InstallerAddress> InstallerAddresses { get; set; }

		public virtual DbSet<Quotation> Quotations { get; set; }

		public virtual DbSet<DocumentType> DocumentTypes { get; set; }

		public virtual DbSet<Document> Documents { get; set; }

		public virtual DbSet<InvoicePaymentMethod> InvoicePaymentMethods { get; set; }

		public virtual DbSet<InvoicePayment> InvoicePayments { get; set; }

		public virtual DbSet<STCZoneRating> STCZoneRatings { get; set; }

		public virtual DbSet<STCPostalCode> STCPostalCodes { get; set; }

		public virtual DbSet<STCYearWiseRate> STCYearWiseRates { get; set; }

		public virtual DbSet<JobProductItem> JobProductItems { get; set; }

		public virtual DbSet<PromotionMaster> PromotionMasters { get; set; }

		public virtual DbSet<FinanceOption> FinanceOptions { get; set; }

		public virtual DbSet<HouseType> HouseTypes { get; set; }

		public virtual DbSet<CancelReason> CancelReasons { get; set; }

		public virtual DbSet<RejectReason> RejectReasons { get; set; }

		public virtual DbSet<MeterPhase> MeterPhases { get; set; }

		public virtual DbSet<MeterUpgrade> MeterUpgrades { get; set; }

		public virtual DbSet<ElecRetailer> ElecRetailers { get; set; }

		public virtual DbSet<DepositOption> DepositOptions { get; set; }

		public virtual DbSet<PaymentOption> PaymentOptions { get; set; }

		public virtual DbSet<PromotionOffer> PromotionOffers { get; set; }

		public virtual DbSet<JobVariation> JobVariations { get; set; }

		public virtual DbSet<Variation> Variations { get; set; }

		public virtual DbSet<JobPromotion> JobPromotions { get; set; }

		public virtual DbSet<Job> Jobs { get; set; }

		public virtual DbSet<JobStatus> JobStatuses { get; set; }

		public virtual DbSet<ProductItem> ProductItems { get; set; }

		public virtual DbSet<ElecDistributor> ElecDistributors { get; set; }

		public virtual DbSet<RoofAngle> RoofAngles { get; set; }

		public virtual DbSet<RoofType> RoofTypes { get; set; }

		public virtual DbSet<ProductType> ProductTypes { get; set; }

		public virtual DbSet<JobType> JobTypes { get; set; }

		public virtual DbSet<PromotionUser> PromotionUsers { get; set; }

		public virtual DbSet<Promotion> Promotions { get; set; }

		public virtual DbSet<PromotionResponseStatus> PromotionResponseStatuses { get; set; }

		public virtual DbSet<PromotionType> PromotionTypes { get; set; }

		public virtual DbSet<Department> Departments { get; set; }

		public virtual DbSet<LeadExpense> LeadExpenses { get; set; }

		public virtual DbSet<UserTeam> UserTeams { get; set; }
		public virtual DbSet<PreviousJobStatus> PreviousJobStatuses { get; set; }
		public virtual DbSet<Category> Categories { get; set; }

		public virtual DbSet<Team> Teams { get; set; }

		public virtual DbSet<LeadAction> LeadActions { get; set; }
		public virtual DbSet<Section> Sections { get; set; }

		public virtual DbSet<LeadActivityLog> LeadActivityLogs { get; set; }

		public virtual DbSet<LeadStatus> LeadStatuses { get; set; }

		public virtual DbSet<Lead> Leads { get; set; }

		public virtual DbSet<LeadSource> LeadSources { get; set; }

		public virtual DbSet<PostalType> PostalTypes { get; set; }

		public virtual DbSet<PostCode> PostCodes { get; set; }

		public virtual DbSet<State> States { get; set; }

		public virtual DbSet<StreetName> StreetNames { get; set; }

		public virtual DbSet<StreetType> StreetTypes { get; set; }

		public virtual DbSet<UnitType> UnitTypes { get; set; }

		public virtual DbSet<ApplicationSetting> ApplicationSettings { get; set; }
		/* Define an IDbSet for each entity of the application */

		public virtual DbSet<BinaryObject> BinaryObjects { get; set; }

		public virtual DbSet<Friendship> Friendships { get; set; }

		public virtual DbSet<ChatMessage> ChatMessages { get; set; }

		public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }

		public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }

		public virtual DbSet<Invoice> Invoices { get; set; }

		public virtual DbSet<PersistedGrantEntity> PersistedGrants { get; set; }

		public virtual DbSet<SubscriptionPaymentExtensionData> SubscriptionPaymentExtensionDatas { get; set; }

		public virtual DbSet<UserDelegation> UserDelegations { get; set; }
		public virtual DbSet<InstallerAvailability> InstallerAvailabilities { get; set; }

		public virtual DbSet<InstallerInvoice> InstallerInvoices { get; set; }
		public virtual DbSet<DocumentLibrary> DocumentLibrarys { get; set; }

		public virtual DbSet<LeadtrackerHistory> LeadtrackerHistorys { get; set; }

		public virtual DbSet<InvoiceFile> InvoiceFiles { get; set; }
		public virtual DbSet<InvoiceImportData> InvoiceImportDatas { get; set; }
		public virtual DbSet<InstInvoiceHistory> InstInvoiceHistory { get; set; }
		public virtual DbSet<JobApprovalFileHistory> JobApprovalFileHistory { get; set; }

		public virtual DbSet<Jobwarranty> Jobwarranty { get; set; }
		
		public TheSolarProductDbContext(DbContextOptions<TheSolarProductDbContext> options)
			: base(options)
		{

		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<SmsTemplate>(s =>
			{
				s.HasIndex(e => new { e.TenantId });
			});
			modelBuilder.Entity<FreebieTransport>(f =>
						{
							f.HasIndex(e => new { e.TenantId });
						});
			modelBuilder.Entity<InvoiceStatus>(i =>
					   {
						   i.HasIndex(e => new { e.TenantId });
					   });
			modelBuilder.Entity<RefundReason>(r =>
					   {
						   r.HasIndex(e => new { e.TenantId });
					   });
			modelBuilder.Entity<HoldReason>(h =>
					   {
						   h.HasIndex(e => new { e.TenantId });
					   });
			modelBuilder.Entity<RefundReason>(r =>
					{
						r.HasIndex(e => new { e.TenantId });
					});
			modelBuilder.Entity<JobRefund>(j =>
					   {
						   j.HasIndex(e => new { e.TenantId });
						   modelBuilder.Entity<ProductItem>(p =>
				{
					p.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<InstallerDetail>(i =>
						   {
							   i.HasIndex(e => new { e.TenantId });
						   });

						   modelBuilder.Entity<InstallerAddress>(i =>
						   {
							   i.HasIndex(e => new { e.TenantId });
						   });

						   modelBuilder.Entity<InvoicePaymentMethod>(i =>
						   {
							   i.HasIndex(e => new { e.TenantId });
						   });
						   modelBuilder.Entity<InvoicePayment>(i =>
						   {
							   i.HasIndex(e => new { e.TenantId });
						   });
						   modelBuilder.Entity<JobProductItem>(j =>
				{
					j.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<PromotionMaster>(p =>
				{
					p.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<FinanceOption>(f =>
				{
					f.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<CancelReason>(c =>
				{
					c.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<RejectReason>(r =>
				{
					r.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<DepositOption>(d =>
				{
					d.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<PaymentOption>(p =>
				{
					p.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<JobVariation>(j =>
				{
					j.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<Variation>(v =>
				{
					v.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<JobPromotion>(j =>
				{
					j.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<Job>(j =>
				{
					j.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<ProductItem>(p =>
				{
					p.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<PromotionUser>(p =>
				{
					p.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<Promotion>(p =>
				{
					p.HasIndex(e => new { e.TenantId });
				});
						   //modelBuilder.Entity<PromotionResponseStatus>(p =>
						   //           {
						   //               p.HasIndex(e => new { e.TenantId });
						   //           });
						   //modelBuilder.Entity<PromotionType>(p =>
						   //           {
						   //               p.HasIndex(e => new { e.TenantId });
						   //           });
						   modelBuilder.Entity<Department>(d =>
												 {
													 d.HasIndex(e => new { e.TenantId });
												 });
						   modelBuilder.Entity<Lead>(l =>
			 {
				 l.HasIndex(e => new { e.TenantId });
			 });

						   //modelBuilder.Entity<LeadSource>(l =>
						   //		   {
						   //			   l.HasIndex(e => new { e.TenantId });
						   //		   });
						   modelBuilder.Entity<BinaryObject>(b =>
									  {
										  b.HasIndex(e => new { e.TenantId });
									  });

						   modelBuilder.Entity<ChatMessage>(b =>
						   {
							   b.HasIndex(e => new { e.TenantId, e.UserId, e.ReadState });
							   b.HasIndex(e => new { e.TenantId, e.TargetUserId, e.ReadState });
							   b.HasIndex(e => new { e.TargetTenantId, e.TargetUserId, e.ReadState });
							   b.HasIndex(e => new { e.TargetTenantId, e.UserId, e.ReadState });
						   });

						   modelBuilder.Entity<Friendship>(b =>
						   {
							   b.HasIndex(e => new { e.TenantId, e.UserId });
							   b.HasIndex(e => new { e.TenantId, e.FriendUserId });
							   b.HasIndex(e => new { e.FriendTenantId, e.UserId });
							   b.HasIndex(e => new { e.FriendTenantId, e.FriendUserId });
						   });

						   modelBuilder.Entity<Tenant>(b =>
						   {
							   b.HasIndex(e => new { e.SubscriptionEndDateUtc });
							   b.HasIndex(e => new { e.CreationTime });
						   });

						   modelBuilder.Entity<SubscriptionPayment>(b =>
						   {
							   b.HasIndex(e => new { e.Status, e.CreationTime });
							   b.HasIndex(e => new { PaymentId = e.ExternalPaymentId, e.Gateway });
						   });

						   modelBuilder.Entity<SubscriptionPaymentExtensionData>(b =>
						   {
							   b.HasQueryFilter(m => !m.IsDeleted)
						.HasIndex(e => new { e.SubscriptionPaymentId, e.Key, e.IsDeleted })
						.IsUnique();
						   });

						   modelBuilder.Entity<UserDelegation>(b =>
						   {
							   b.HasIndex(e => new { e.TenantId, e.SourceUserId });
							   b.HasIndex(e => new { e.TenantId, e.TargetUserId });
						   });

						   modelBuilder.Entity<Section>(s =>
						   {
							   s.HasIndex(e => new { e.SectionId }).IsUnique();
						   });

						   modelBuilder.ConfigurePersistedGrantEntity();
					   });
		}

	}
}