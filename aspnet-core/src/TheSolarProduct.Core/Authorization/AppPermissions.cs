﻿namespace TheSolarProduct.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission nam
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        public const string Pages_SmsTemplates = "Pages.SmsTemplates";
        public const string Pages_SmsTemplates_Create = "Pages.SmsTemplates.Create";
        public const string Pages_SmsTemplates_Edit = "Pages.SmsTemplates.Edit";
        public const string Pages_SmsTemplates_Delete = "Pages.SmsTemplates.Delete";

        public const string Pages_FreebieTransports = "Pages.FreebieTransports";
        public const string Pages_FreebieTransports_Create = "Pages.FreebieTransports.Create";
        public const string Pages_FreebieTransports_Edit = "Pages.FreebieTransports.Edit";
        public const string Pages_FreebieTransports_Delete = "Pages.FreebieTransports.Delete";

        public const string Pages_InvoiceStatuses = "Pages.InvoiceStatuses";
        public const string Pages_InvoiceStatuses_Create = "Pages.InvoiceStatuses.Create";
        public const string Pages_InvoiceStatuses_Edit = "Pages.InvoiceStatuses.Edit";
        public const string Pages_InvoiceStatuses_Delete = "Pages.InvoiceStatuses.Delete";

        public const string Pages_Tracker = "Pages.Tracker";

        public const string Pages_ApplicationTracker = "Pages.Tracker.ApplicationTracker";
        public const string Pages_ApplicationTracker_AddNotes = "Pages.Tracker.ApplicationTracker.AddNotes";
        public const string Pages_ApplicationTracker_JobDetail = "Pages.Tracker.ApplicationTracker.JobDetail";
        public const string Pages_ApplicationTracker_Notify = "Pages.Tracker.ApplicationTracker.Notify";
        public const string Pages_ApplicationTracker_SMS = "Pages.Tracker.ApplicationTracker.SMS";
        public const string Pages_ApplicationTracker_Email = "Pages.Tracker.ApplicationTracker.Email";
        public const string Pages_ApplicationTracker_ToDo = "Pages.Tracker.ApplicationTracker.ToDo";
        public const string Pages_ApplicationTracker_Reminder = "Pages.Tracker.ApplicationTracker.Reminder";
        public const string Pages_ApplicationTracker_Comment = "Pages.Tracker.ApplicationTracker.Comment";
        public const string Pages_ApplicationTracker_QuickView = "Pages.Tracker.ApplicationTracker.QuickView";
        public const string Pages_ApplicationTracker_DocumentRequest = "Pages.Tracker.ApplicationTracker.DocumentRequest";
        public const string Pages_ApplicationTracker_Job_Edit = "Pages.Tracker.ApplicationTracker.JobEdit";
        public const string Pages_ApplicationTracker_Export = "Pages.Tracker.ApplicationTracker.Export";
        public const string Pages_ApplicationTracker_Import = "Pages.Tracker.ApplicationTracker.Import";

        public const string Pages_FreebiesTracker = "Pages.Tracker.FreebiesTracker";
        public const string Pages_FreebiesTracker_AddTrackingNumber = "Pages.Tracker.FreebiesTracker.AddTrackingNumber";
        public const string Pages_FreebiesTracker_Notify = "Pages.Tracker.FreebiesTracker.Notify";
        public const string Pages_FreebiesTracker_SMS = "Pages.Tracker.FreebiesTracker.SMS";
        public const string Pages_FreebiesTracker_Email = "Pages.Tracker.FreebiesTracker.Email";
        public const string Pages_FreebiesTracker_QuickView = "Pages.Tracker.FreebiesTracker.QuickView";
        public const string Pages_FreebiesTracker_Job_Edit = "Pages.Tracker.FreebiesTracker.JobEdit";
        public const string Pages_FreebiesTracker_Export = "Pages.Tracker.FreebiesTracker.Export";
        public const string Pages_FreebiesTracker_Import = "Pages.Tracker.FreebiesTracker.Import";
        public const string Pages_FreebiesTracker_ToDo = "Pages.Tracker.FreebiesTracker.ToDo";
        public const string Pages_FreebiesTracker_Reminder = "Pages.Tracker.FreebiesTracker.Reminder";
        public const string Pages_FreebiesTracker_Comment = "Pages.Tracker.FreebiesTracker.Comment";

        public const string Pages_FinanceTracker = "Pages.Tracker.FinanceTracker";
        public const string Pages_FinanceTracker_AddDetails = "Pages.Tracker.FinanceTracker.AddDetails";
        public const string Pages_FinanceTracker_Notify = "Pages.Tracker.FinanceTracker.Notify";
        public const string Pages_FinanceTracker_SMS = "Pages.Tracker.FinanceTracker.SMS";
        public const string Pages_FinanceTracker_Email = "Pages.Tracker.FinanceTracker.Email";
        public const string Pages_FinanceTracker_QuickView = "Pages.Tracker.FinanceTracker.QuickView";
        public const string Pages_FinanceTracker_Job_Edit = "Pages.Tracker.FinanceTracker.JobEdit";
        public const string Pages_FinanceTracker_Export = "Pages.Tracker.FinanceTracker.Export";
        public const string Pages_FinanceTracker_Import = "Pages.Tracker.FinanceTracker.Import";
        public const string Pages_FinanceTracker_ToDo = "Pages.Tracker.FinanceTracker.ToDo";
        public const string Pages_FinanceTracker_Reminder = "Pages.Tracker.FinanceTracker.Reminder";
        public const string Pages_FinanceTracker_Comment = "Pages.Tracker.FinanceTracker.Comment";

        public const string Pages_RefundTracker = "Pages.Tracker.RefundTracker";
        public const string Pages_RefundTracker_Refund = "Pages.Tracker.RefundTracker.Refund";
        public const string Pages_RefundTracker_Notify = "Pages.Tracker.RefundTracker.Notify";
        public const string Pages_RefundTracker_SMS = "Pages.Tracker.RefundTracker.SMS";
        public const string Pages_RefundTracker_Email = "Pages.Tracker.RefundTracker.Email";
        public const string Pages_RefundTracker_QuickView = "Pages.Tracker.RefundTracker.QuickView";
        public const string Pages_RefundTracker_Delete = "Pages.Tracker.RefundTracker.Delete";
        public const string Pages_RefundTracker_Job_Edit = "Pages.Tracker.RefundTracker.JobEdit";
        public const string Pages_RefundTracker_Export = "Pages.Tracker.RefundTracker.Export";
        public const string Pages_RefundTracker_Import = "Pages.Tracker.RefundTracker.Import";
        public const string Pages_RefundTracker_ToDo = "Pages.Tracker.RefundTracker.ToDo";
        public const string Pages_RefundTracker_Reminder = "Pages.Tracker.RefundTracker.Reminder";
        public const string Pages_RefundTracker_Comment = "Pages.Tracker.RefundTracker.Comment";

        public const string Pages_JobActiveTracker = "Pages.Tracker.JobActiveTracker";
        public const string Pages_JobActiveTracker_AddDetails = "Pages.Tracker.JobActiveTracker.AddDetails";
        public const string Pages_JobActiveTracker_Notify = "Pages.Tracker.JobActiveTracker.Notify";
        public const string Pages_JobActiveTracker_SMS = "Pages.Tracker.JobActiveTracker.SMS";
        public const string Pages_JobActiveTracker_Email = "Pages.Tracker.JobActiveTracker.Email";
        public const string Pages_JobActiveTracker_QuickView = "Pages.Tracker.JobActiveTracker.QuickView";
        public const string Pages_JobActiveTracker_Job_Edit = "Pages.Tracker.JobActiveTracker.JobEdit";
        public const string Pages_JobActiveTracker_Export = "Pages.Tracker.JobActiveTracker.Export";
        public const string Pages_JobActiveTracker_Import = "Pages.Tracker.JobActiveTracker.Import";
        public const string Pages_JobActiveTracker_ToDo = "Pages.Tracker.JobActiveTracker.ToDo";
        public const string Pages_JobActiveTracker_Reminder = "Pages.Tracker.JobActiveTracker.Reminder";
        public const string Pages_JobActiveTracker_Comment = "Pages.Tracker.JobActiveTracker.Comment";


        public const string Pages_Invoice = "Pages.Invoice"; 
        public const string Pages_InvoiceTracker = "Pages.Tracker.InvoiceTracker";
        public const string Pages_InvoiceTracker_Verify = "Pages.Tracker.InvoiceTracker.Verify";
        public const string Pages_InvoiceTracker_Notify = "Pages.Tracker.InvoiceTracker.Notify";
        public const string Pages_InvoiceTracker_SMS = "Pages.Tracker.InvoiceTracker.SMS";
        public const string Pages_InvoiceTracker_Email = "Pages.Tracker.InvoiceTracker.Email";
        public const string Pages_InvoiceTracker_QuickView = "Pages.Tracker.InvoiceTracker.QuickView";
        public const string Pages_InvoiceTracker_Job_Edit = "Pages.Tracker.InvoiceTracker.JobEdit";
        public const string Pages_InvoiceTracker_Export = "Pages.Tracker.InvoiceTracker.Export";
        public const string Pages_InvoiceTracker_Import = "Pages.Tracker.InvoiceTracker.Import";
        public const string Pages_InvoiceTracker_ToDo = "Pages.Tracker.InvoiceTracker.ToDo";
        public const string Pages_InvoiceTracker_Reminder = "Pages.Tracker.InvoiceTracker.Reminder";
        public const string Pages_InvoiceTracker_Comment = "Pages.Tracker.InvoiceTracker.Comment";

        public const string Pages_InvoiceIssuedTracker = "Pages.Tracker.InvoiceIssuedTracker";
        public const string Pages_InvoiceIssuedTracker_Notify = "Pages.Tracker.InvoiceIssuedTracker.Notify";
        public const string Pages_InvoiceIssuedTracker_SMS = "Pages.Tracker.InvoiceIssuedTracker.SMS";
        public const string Pages_InvoiceIssuedTracker_Email = "Pages.Tracker.InvoiceIssuedTracker.Email";
        public const string Pages_InvoiceIssuedTracker_QuickView = "Pages.Tracker.InvoiceIssuedTracker.QuickView";
        public const string Pages_InvoiceIssuedTracker_Job_Edit = "Pages.Tracker.InvoiceIssuedTracker.JobEdit";
        public const string Pages_InvoiceIssuedTracker_Export = "Pages.Tracker.InvoiceIssuedTracker.Export";
        public const string Pages_InvoiceIssuedTracker_Import = "Pages.Tracker.InvoiceIssuedTracker.Import";
        public const string Pages_InvoiceIssuedTracker_ToDo = "Pages.Tracker.InvoiceIssuedTracker.ToDo";
        public const string Pages_InvoiceIssuedTracker_Reminder = "Pages.Tracker.InvoiceIssuedTracker.Reminder";
        public const string Pages_InvoiceIssuedTracker_Comment = "Pages.Tracker.InvoiceIssuedTracker.Comment";

        public const string Pages_Gridconnectiontracker = "Pages.Tracker.Gridconnectiontracker";
        public const string Pages_Gridconnectiontracker_Detail = "Pages.Tracker.Gridconnectiontracker.Detail";
        public const string Pages_Gridconnectiontracker_AddMeterDetail = "Pages.Tracker.Gridconnectiontracker.AddMeterDetail";
        public const string Pages_Gridconnectiontracker_Job_Edit = "Pages.Tracker.Gridconnectiontracker.JobEdit";
        public const string Pages_Gridconnectiontracker_Delete = "Pages.Gridconnectiontracker.Delete";
        public const string Pages_Gridconnectiontracker_Export = "Pages.Gridconnectiontracker.Export";
        public const string Pages_Gridconnectiontracker_Import = "Pages.Gridconnectiontracker.Import";
        public const string Pages_Gridconnectiontracker_ToDo = "Pages.Gridconnectiontracker.ToDo";
        public const string Pages_Gridconnectiontracker_Reminder = "Pages.Gridconnectiontracker.Reminder";
        public const string Pages_Gridconnectiontracker_Comment = "Pages.Gridconnectiontracker.Comment";
        public const string Pages_Gridconnectiontracker_JobDetail = "Pages.Gridconnectiontracker.JobDetail";
        public const string Pages_Gridconnectiontracker_Notify = "Pages.Gridconnectiontracker.Notify";
        public const string Pages_Gridconnectiontracker_QuickView = "Pages.Gridconnectiontracker.QuickView";
        public const string Pages_Gridconnectiontracker_SMS = "Pages.Gridconnectiontracker.SMS";
        public const string Pages_Gridconnectiontracker_Email = "Pages.Gridconnectiontracker.Email";
        public const string Pages_Gridconnectiontracker_AddNote = "Pages.Gridconnectiontracker.AddNote";


        public const string Pages_STCTracker = "Pages.Tracker.STCTracker";
        public const string Pages_STCTracker_Edit = "Pages.Tracker.STCTracker.Edit";
        public const string Pages_STCTracker_Job_Edit = "Pages.Tracker.STCTracker.JobEdit"; 
        public const string Pages_STCTracker_Delete = "Pages.STCTracker.Delete";
        public const string Pages_STCTracker_Export = "Pages.STCTracker.Export";
        public const string Pages_STCTracker_Import = "Pages.STCTracker.Import";
        public const string Pages_STCTracker_JobDetail = "Pages.STCTracker.JobDetail";
        public const string Pages_STCTracker_Notify = "Pages.STCTracker.Notify";
        public const string Pages_STCTracker_QuickView = "Pages.STCTracker.QuickView";
        public const string Pages_STCTracker_SMS = "Pages.STCTracker.SMS";
        public const string Pages_STCTracker_Email = "Pages.STCTracker.Email";
        public const string Pages_STCTracker_AddSTC = "Pages.STCTracker.AddSTC";
        public const string Pages_STCTracker_ToDo = "Pages.STCTracker.ToDo";
        public const string Pages_STCTracker_Reminder = "Pages.STCTracker.Reminder";
        public const string Pages_STCTracker_Comment = "Pages.STCTracker.Comment";

        public const string Pages_Tracker_Referral = "Pages.Tracker.Referral";
        public const string Pages_Tracker_Referral_Payment = "Pages.Tracker.Referral.Payment";
        public const string Pages_Tracker_Referral_QuickView = "Pages.Tracker.Referral.Payment.QuickView";
        public const string Pages_JobGrid = "Pages.JobGrid";
        public const string Pages_JobGrid_Edit = "Pages.JobGrid.JobEdit";
        public const string Pages_JobGrid_Export = "Pages.JobGrid.Export";
        public const string Pages_JobGrid_Import = "Pages.JobGrid.Import";
        public const string Pages_JobGrid_JobDetail = "Pages.JobGrid.JobDetail";
        public const string Pages_JobGrid_Notify = "Pages.JobGrid.Notify";
        public const string Pages_JobGrid_QuickView = "Pages.JobGrid.QuickView";
        public const string Pages_JobGrid_SMS = "Pages.JobGrid.SMS";
        public const string Pages_JobGrid_Email = "Pages.JobGrid.Email";
        public const string Pages_JobGrid_ToDo = "Pages.JobGrid.ToDo";
        public const string Pages_JobGrid_Comment = "Pages.JobGrid.Comment";
        public const string Pages_JobGrid_Reminder = "Pages.JobGrid.Reminder";



        public const string Pages_HoldReasons = "Pages.HoldReasons";
        public const string Pages_HoldReasons_Create = "Pages.HoldReasons.Create";
        public const string Pages_HoldReasons_Edit = "Pages.HoldReasons.Edit";
        public const string Pages_HoldReasons_Delete = "Pages.HoldReasons.Delete";

        //For Document TYpe 
        public const string Pages_Tenant_datavault_DocumentType = "Pages.Tenant.datavault.DocumentType";
        public const string Pages_Tenant_datavault_DocumentType_Create = "Pages.Tenant.datavault.DocumentType.Create";
        public const string Pages_Tenant_datavault_DocumentType_Edit = "Pages.Tenant.datavault.DocumentType.Edit";
        public const string Pages_Tenant_datavault_DocumentType_Delete = "Pages.Tenant.datavault.DocumentType.Delete";

        //For Document Library 
        public const string Pages_Tenant_datavault_DocumentLibrary = "Pages.Tenant.datavault.DocumentLibrary";
        public const string Pages_Tenant_datavault_DocumentLibrary_Create = "Pages.Tenant.datavault.DocumentLibrary.Create";
        public const string Pages_Tenant_datavault_DocumentLibrary_Edit = "Pages.Tenant.datavault.DocumentLibrary.Edit";
        public const string Pages_Tenant_datavault_DocumentLibrary_Delete = "Pages.Tenant.datavault.DocumentLibrary.Delete";


        //For JOb Cancellation Reason
        public const string Pages_Tenant_datavault_JobCancellationReason = "Pages.Tenant.datavault.JobCancellationReason";
        public const string Pages_Tenant_datavault_JobCancellationReason_Create = "Pages.Tenant.datavault.JobCancellationReason.Create";
        public const string Pages_Tenant_datavault_JobCancellationReason_Edit = "Pages.Tenant.datavault.JobCancellationReason.Edit";
        public const string Pages_Tenant_datavault_JobCancellationReason_Delete = "Pages.Tenant.datavault.JobCancellationReason.Delete";

        public const string Pages_Tenant_Installer = "Pages.Tenant.Installer";
        public const string Pages_Tenant_Installer_Invoice = "Pages.Tenant.InstallerInvoice";
        public const string Pages_Tenant_Installer_Availability = "Pages.Tenant.Installer.Availability";
        public const string Pages_Tenant_InstallerManager_InstallerCalendar = "Pages.Tenant.InstallerManager.InstallerCalendar";

        public const string Pages_EmailTemplates = "Pages.Tenant.EmailTemplates";

        public const string Pages_RefundReasons = "Pages.RefundReasons";
        public const string Pages_RefundReasons_Create = "Pages.RefundReasons.Create";
        public const string Pages_RefundReasons_Edit = "Pages.RefundReasons.Edit";
        public const string Pages_RefundReasons_Delete = "Pages.RefundReasons.Delete";

        public const string Pages_JobRefunds = "Pages.JobRefunds";
        public const string Pages_JobRefunds_Create = "Pages.JobRefunds.Create";
        public const string Pages_JobRefunds_Edit = "Pages.JobRefunds.Edit";
        public const string Pages_JobRefunds_Delete = "Pages.JobRefunds.Delete";

        public const string Pages_InstallerDetails = "Pages.InstallerDetails";
        public const string Pages_InstallerDetails_Create = "Pages.InstallerDetails.Create";
        public const string Pages_InstallerDetails_Edit = "Pages.InstallerDetails.Edit";
        public const string Pages_InstallerDetails_Delete = "Pages.InstallerDetails.Delete";

        public const string Pages_InstallerAddresses = "Pages.InstallerAddresses";
        public const string Pages_InstallerAddresses_Create = "Pages.InstallerAddresses.Create";
        public const string Pages_InstallerAddresses_Edit = "Pages.InstallerAddresses.Edit";
        public const string Pages_InstallerAddresses_Delete = "Pages.InstallerAddresses.Delete";

        public const string Pages_InstallationSection = "Pages.InstallationSection";
        public const string Pages_Calenders = "Pages.Calenders";
        public const string Pages_Map = "Pages.Map";
        public const string Pages_Installation = "Pages.Installation";
        public const string Pages_PendingInstallation = "Pages.PendingInstallation";
        public const string Pages_PendingInstallation_View = "Pages.PendingInstallation.View";
        public const string Pages_PendingInstallation_ToDo = "Pages.PendingInstallation.ToDo";
        public const string Pages_PendingInstallation_Email = "Pages.PendingInstallation.Email";
        public const string Pages_PendingInstallation_Sms = "Pages.PendingInstallation.SMS";
        public const string Pages_PendingInstallation_Comment = "Pages.PendingInstallation.Comment";
        public const string Pages_PendingInstallation_Notify = "Pages.PendingInstallation.Notify";
        public const string Pages_PendingInstallation_Reminder = "Pages.PendingInstallation.Reminder";

        public const string Pages_JobBooking_Notify = "Pages.JobBooking.Notify";
        public const string Pages_JobBooking_ToDo = "Pages.JobBooking.ToDo";
        public const string Pages_JobBooking_Comment = "Pages.JobBooking.Comment";
        public const string Pages_JobBooking_Reminder = "Pages.JobBooking.Reminder";

        public const string Pages_JobBooking = "Pages.JobBooking";
        public const string Pages_InvoicePaymentMethods = "Pages.InvoicePaymentMethods";
        public const string Pages_InvoicePaymentMethods_Create = "Pages.InvoicePaymentMethods.Create";
        public const string Pages_InvoicePaymentMethods_Edit = "Pages.InvoicePaymentMethods.Edit";
        public const string Pages_InvoicePaymentMethods_Delete = "Pages.InvoicePaymentMethods.Delete";

        public const string Pages_InvoicePayments = "Pages.InvoicePayments";
        public const string Pages_InvoicePayments_Create = "Pages.InvoicePayments.Create";
        public const string Pages_InvoicePayments_Edit = "Pages.InvoicePayments.Edit";
        public const string Pages_InvoicePayments_Delete = "Pages.InvoicePayments.Delete";
        public const string Pages_InvoicePayments_Verify = "Pages.InvoicePayments.Verify";

        public const string Pages_JobProductItems = "Pages.JobProductItems";
        public const string Pages_JobProductItems_Create = "Pages.JobProductItems.Create";
        public const string Pages_JobProductItems_Edit = "Pages.JobProductItems.Edit";
        public const string Pages_JobProductItems_Delete = "Pages.JobProductItems.Delete";

        public const string Pages_PromotionMasters = "Pages.PromotionMasters";
        public const string Pages_PromotionMasters_Create = "Pages.PromotionMasters.Create";
        public const string Pages_PromotionMasters_Edit = "Pages.PromotionMasters.Edit";
        public const string Pages_PromotionMasters_Delete = "Pages.PromotionMasters.Delete";

        public const string Pages_FinanceOptions = "Pages.FinanceOptions";
        public const string Pages_FinanceOptions_Create = "Pages.FinanceOptions.Create";
        public const string Pages_FinanceOptions_Edit = "Pages.FinanceOptions.Edit";
        public const string Pages_FinanceOptions_Delete = "Pages.FinanceOptions.Delete";

        public const string Pages_HouseTypes = "Pages.HouseTypes";
        public const string Pages_HouseTypes_Create = "Pages.HouseTypes.Create";
        public const string Pages_HouseTypes_Edit = "Pages.HouseTypes.Edit";
        public const string Pages_HouseTypes_Delete = "Pages.HouseTypes.Delete";

        public const string Pages_CancelReasons = "Pages.CancelReasons";
        public const string Pages_CancelReasons_Create = "Pages.CancelReasons.Create";
        public const string Pages_CancelReasons_Edit = "Pages.CancelReasons.Edit";
        public const string Pages_CancelReasons_Delete = "Pages.CancelReasons.Delete";

        public const string Pages_RejectReasons = "Pages.RejectReasons";
        public const string Pages_RejectReasons_Create = "Pages.RejectReasons.Create";
        public const string Pages_RejectReasons_Edit = "Pages.RejectReasons.Edit";
        public const string Pages_RejectReasons_Delete = "Pages.RejectReasons.Delete";

        public const string Pages_MeterPhases = "Pages.MeterPhases";
        public const string Pages_MeterPhases_Create = "Pages.MeterPhases.Create";
        public const string Pages_MeterPhases_Edit = "Pages.MeterPhases.Edit";
        public const string Pages_MeterPhases_Delete = "Pages.MeterPhases.Delete";

        public const string Pages_MeterUpgrades = "Pages.MeterUpgrades";
        public const string Pages_MeterUpgrades_Create = "Pages.MeterUpgrades.Create";
        public const string Pages_MeterUpgrades_Edit = "Pages.MeterUpgrades.Edit";
        public const string Pages_MeterUpgrades_Delete = "Pages.MeterUpgrades.Delete";

        public const string Pages_ElecRetailers = "Pages.ElecRetailers";
        public const string Pages_ElecRetailers_Create = "Pages.ElecRetailers.Create";
        public const string Pages_ElecRetailers_Edit = "Pages.ElecRetailers.Edit";
        public const string Pages_ElecRetailers_Delete = "Pages.ElecRetailers.Delete";

        public const string Pages_DepositOptions = "Pages.DepositOptions";
        public const string Pages_DepositOptions_Create = "Pages.DepositOptions.Create";
        public const string Pages_DepositOptions_Edit = "Pages.DepositOptions.Edit";
        public const string Pages_DepositOptions_Delete = "Pages.DepositOptions.Delete";

        public const string Pages_Quotations = "Pages.Quotations";
        public const string Pages_Quotations_Create = "Pages.Quotations.Create";
        public const string Pages_Quotations_Edit = "Pages.Quotations.Edit";
        public const string Pages_Quotations_Delete = "Pages.Quotations.Delete";

        public const string Pages_PaymentOptions = "Pages.PaymentOptions";
        public const string Pages_PaymentOptions_Create = "Pages.PaymentOptions.Create";
        public const string Pages_PaymentOptions_Edit = "Pages.PaymentOptions.Edit";
        public const string Pages_PaymentOptions_Delete = "Pages.PaymentOptions.Delete";

        public const string Pages_JobVariations = "Pages.JobVariations";
        public const string Pages_JobVariations_Create = "Pages.JobVariations.Create";
        public const string Pages_JobVariations_Edit = "Pages.JobVariations.Edit";
        public const string Pages_JobVariations_Delete = "Pages.JobVariations.Delete";

        public const string Pages_Variations = "Pages.Variations";
        public const string Pages_Variations_Create = "Pages.Variations.Create";
        public const string Pages_Variations_Edit = "Pages.Variations.Edit";
        public const string Pages_Variations_Delete = "Pages.Variations.Delete";

        public const string Pages_JobPromotions = "Pages.JobPromotions";
        public const string Pages_JobPromotions_Create = "Pages.JobPromotions.Create";
        public const string Pages_JobPromotions_Edit = "Pages.JobPromotions.Edit";
        public const string Pages_JobPromotions_Delete = "Pages.JobPromotions.Delete";

        public const string Pages_JobProducts = "Pages.JobProducts";
        public const string Pages_JobProducts_Create = "Pages.JobProducts.Create";
        public const string Pages_JobProducts_Edit = "Pages.JobProducts.Edit";
        public const string Pages_JobProducts_Delete = "Pages.JobProducts.Delete";

        public const string Pages_JobPickList = "Pages.JobPickList";
        public const string Pages_JobPickList_Create = "Pages.JobPickList.Create";
        public const string Pages_JobPickList_Edit = "Pages.JobPickList.Edit";
        public const string Pages_JobPickList_Delete = "Pages.JobPickList.Delete";

        public const string Pages_Jobs = "Pages.Jobs";
        public const string Pages_Jobs_Create = "Pages.Jobs.Create";
        public const string Pages_Jobs_Edit = "Pages.Jobs.Edit";
        public const string Pages_Jobs_Delete = "Pages.Jobs.Delete";
        public const string Pages_Jobs_Sales = "Pages.Jobs.Sales";
        public const string Pages_Jobs_Quotation = "Pages.Jobs.Quotation";
        public const string Pages_Jobs_Installation = "Pages.Jobs.Installation";
        public const string Pages_Jobs_PostInstallation = "Pages.Jobs.PostInstallation";
        public const string Pages_Jobs_Refund = "Pages.Jobs.Refund";
        public const string Pages_Jobs_StatusChange = "Pages.Jobs.StatusChange";
        public const string Pages_Jobs_Detail = "Pages.Jobs.Detail";

       
        public const string Pages_JobStatuses = "Pages.JobStatuses";
        public const string Pages_JobStatuses_Create = "Pages.JobStatuses.Create";
        public const string Pages_JobStatuses_Edit = "Pages.JobStatuses.Edit";
        public const string Pages_JobStatuses_Delete = "Pages.JobStatuses.Delete";

        public const string Pages_ProductItems = "Pages.ProductItems";
        public const string Pages_ProductItems_Create = "Pages.ProductItems.Create";
        public const string Pages_ProductItems_Edit = "Pages.ProductItems.Edit";
        public const string Pages_ProductItems_Delete = "Pages.ProductItems.Delete";

        public const string Pages_ElecDistributors = "Pages.ElecDistributors";
        public const string Pages_ElecDistributors_Create = "Pages.ElecDistributors.Create";
        public const string Pages_ElecDistributors_Edit = "Pages.ElecDistributors.Edit";
        public const string Pages_ElecDistributors_Delete = "Pages.ElecDistributors.Delete";

        public const string Pages_RoofAngles = "Pages.RoofAngles";
        public const string Pages_RoofAngles_Create = "Pages.RoofAngles.Create";
        public const string Pages_RoofAngles_Edit = "Pages.RoofAngles.Edit";
        public const string Pages_RoofAngles_Delete = "Pages.RoofAngles.Delete";

        public const string Pages_RoofTypes = "Pages.RoofTypes";
        public const string Pages_RoofTypes_Create = "Pages.RoofTypes.Create";
        public const string Pages_RoofTypes_Edit = "Pages.RoofTypes.Edit";
        public const string Pages_RoofTypes_Delete = "Pages.RoofTypes.Delete";

        public const string Pages_ProductTypes = "Pages.ProductTypes";
        public const string Pages_ProductTypes_Create = "Pages.ProductTypes.Create";
        public const string Pages_ProductTypes_Edit = "Pages.ProductTypes.Edit";
        public const string Pages_ProductTypes_Delete = "Pages.ProductTypes.Delete";

        public const string Pages_JobTypes = "Pages.JobTypes";
        public const string Pages_JobTypes_Create = "Pages.JobTypes.Create";
        public const string Pages_JobTypes_Edit = "Pages.JobTypes.Edit";
        public const string Pages_JobTypes_Delete = "Pages.JobTypes.Delete";

        public const string Pages_PromotionGroup = "Pages.PromotionGroup";
        public const string Pages_PromotionUsers = "Pages.PromotionUsers";
        public const string Pages_PromotionUsers_Create = "Pages.PromotionUsers.Create";
        public const string Pages_PromotionUsers_Edit = "Pages.PromotionUsers.Edit";
        public const string Pages_PromotionUsers_Delete = "Pages.PromotionUsers.Delete";
        public const string Pages_PromotionUsers_Excel = "Pages.PromotionUsers.Excel";

        public const string Pages_Promotions = "Pages.Promotions";
        public const string Pages_Promotions_Create = "Pages.Promotions.Create";
        public const string Pages_Promotions_Edit = "Pages.Promotions.Edit";
        public const string Pages_Promotions_Delete = "Pages.Promotions.Delete";

        public const string Pages_PromotionResponseStatuses = "Pages.PromotionResponseStatuses";
        public const string Pages_PromotionResponseStatuses_Create = "Pages.PromotionResponseStatuses.Create";
        public const string Pages_PromotionResponseStatuses_Edit = "Pages.PromotionResponseStatuses.Edit";
        public const string Pages_PromotionResponseStatuses_Delete = "Pages.PromotionResponseStatuses.Delete";

        public const string Pages_PromotionTypes = "Pages.PromotionTypes";
        public const string Pages_PromotionTypes_Create = "Pages.PromotionTypes.Create";
        public const string Pages_PromotionTypes_Edit = "Pages.PromotionTypes.Edit";
        public const string Pages_PromotionTypes_Delete = "Pages.PromotionTypes.Delete";

        public const string Pages_Departments = "Pages.Departments";
        public const string Pages_Departments_Create = "Pages.Departments.Create";
        public const string Pages_Departments_Edit = "Pages.Departments.Edit";
        public const string Pages_Departments_Delete = "Pages.Departments.Delete";

        public const string Pages_Tenant_DataVaults_EmailTemplates = "Pages.Tenant.DataVaults.EmailTemplates";
        public const string Pages_Tenant_DataVaults_EmailTemplates_Create = "Pages.Tenant.DataVaults.EmailTemplates.Create";
        public const string Pages_Tenant_DataVaults_EmailTemplates_Edit = "Pages.Tenant.DataVaults.EmailTemplates.Edit";
        public const string Pages_Tenant_DataVaults_EmailTemplates_Delete = "Pages.Tenant.DataVaults.EmailTemplates.Delete";

        public const string Pages_LeadExpenses = "Pages.LeadExpenses";
        public const string Pages_LeadExpenses_Create = "Pages.LeadExpenses.Create";
        public const string Pages_LeadExpenses_Edit = "Pages.LeadExpenses.Edit";
        public const string Pages_LeadExpenses_Delete = "Pages.LeadExpenses.Delete";

        public const string Pages_UserTeams = "Pages.UserTeams";
        public const string Pages_UserTeams_Create = "Pages.UserTeams.Create";
        public const string Pages_UserTeams_Edit = "Pages.UserTeams.Edit";
        public const string Pages_UserTeams_Delete = "Pages.UserTeams.Delete";

        public const string Pages_Categories = "Pages.Categories";
        public const string Pages_Categories_Create = "Pages.Categories.Create";
        public const string Pages_Categories_Edit = "Pages.Categories.Edit";
        public const string Pages_Categories_Delete = "Pages.Categories.Delete";

        public const string Pages_Teams = "Pages.Teams";
        public const string Pages_Teams_Create = "Pages.Teams.Create";
        public const string Pages_Teams_Edit = "Pages.Teams.Edit";
        public const string Pages_Teams_Delete = "Pages.Teams.Delete";

        public const string Pages_Leads = "Pages.Leads";
        public const string Pages_LeadTracker = "Pages.LeadTracker";
        public const string Pages_ManageLeads = "Pages.ManageLeads";
        public const string Pages_Leads_Create = "Pages.Leads.Create";
        public const string Pages_Leads_Edit = "Pages.Leads.Edit";
        public const string Pages_Leads_Delete = "Pages.Leads.Delete";
        public const string Pages_Leads_Export = "Pages.Leads.Export";
        public const string Pages_Leads_Import = "Pages.Leads.Import";

        public const string Pages_MyLeads = "Pages.MyLeads";

        public const string Pages_Leads_Assign = "Pages.Leads.Assign";

        public const string Pages_Lead_Duplicate = "Pages.Lead.Duplicate";

        public const string Pages_Lead_Closed = "Pages.Lead.Close";

        public const string Pages_Lead_Calcel = "Pages.Lead.Calcel";
        public const string Pages_Lead_Rejects = "Pages.Lead.Rejects";
        public const string Pages_Leads_ChangeOrganization = "Pages.Leads.ChangeOrganization";

        public const string Pages_LeadSources = "Pages.LeadSources";
        public const string Pages_LeadSources_Create = "Pages.LeadSources.Create";
        public const string Pages_LeadSources_Edit = "Pages.LeadSources.Edit";
        public const string Pages_LeadSources_Delete = "Pages.LeadSources.Delete";

        public const string Pages_PostalTypes = "Pages.PostalTypes";
        public const string Pages_PostalTypes_Create = "Pages.PostalTypes.Create";
        public const string Pages_PostalTypes_Edit = "Pages.PostalTypes.Edit";
        public const string Pages_PostalTypes_Delete = "Pages.PostalTypes.Delete";

        public const string Pages_PostCodes = "Pages.PostCodes";
        public const string Pages_PostCodes_Create = "Pages.PostCodes.Create";
        public const string Pages_PostCodes_Edit = "Pages.PostCodes.Edit";
        public const string Pages_PostCodes_Delete = "Pages.PostCodes.Delete";

        public const string Pages_StreetNames = "Pages.StreetNames";
        public const string Pages_StreetNames_Create = "Pages.StreetNames.Create";
        public const string Pages_StreetNames_Edit = "Pages.StreetNames.Edit";
        public const string Pages_StreetNames_Delete = "Pages.StreetNames.Delete";

        public const string Pages_StreetTypes = "Pages.StreetTypes";
        public const string Pages_StreetTypes_Create = "Pages.StreetTypes.Create";
        public const string Pages_StreetTypes_Edit = "Pages.StreetTypes.Edit";
        public const string Pages_StreetTypes_Delete = "Pages.StreetTypes.Delete";

        public const string Pages_UnitTypes = "Pages.UnitTypes";
        public const string Pages_UnitTypes_Create = "Pages.UnitTypes.Create";
        public const string Pages_UnitTypes_Edit = "Pages.UnitTypes.Edit";
        public const string Pages_UnitTypes_Delete = "Pages.UnitTypes.Delete";

        public const string Pages_DataVaults = "Pages.DataVaults";

        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";

        public const string Pages_DemoUiComponents = "Pages.DemoUiComponents";
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";
        public const string Pages_Administration_Users_Unlock = "Pages.Administration.Users.Unlock";

        public const string Pages_Administration_SalesRep = "Pages.Administration.SalesRep";
        public const string Pages_Administration_SalesRep_Create = "Pages.Administration.SalesRep.Create";
        public const string Pages_Administration_SalesRep_Edit = "Pages.Administration.SalesRep.Edit";
        public const string Pages_Administration_SalesRep_Delete = "Pages.Administration.SalesRep.Delete";
        public const string Pages_Administration_SalesRep_ChangePermissions = "Pages.Administration.SalesRep.ChangePermissions";
        public const string Pages_Administration_SalesRep_Impersonation = "Pages.Administration.SalesRep.Impersonation";
        public const string Pages_Administration_SalesRep_Unlock = "Pages.Administration.SalesRep.Unlock";

        public const string Pages_Administration_SalesManager = "Pages.Administration.SalesManager";
        public const string Pages_Administration_SalesManager_Create = "Pages.Administration.SalesManager.Create";
        public const string Pages_Administration_SalesManager_Edit = "Pages.Administration.SalesManager.Edit";
        public const string Pages_Administration_SalesManager_Delete = "Pages.Administration.SalesManager.Delete";
        public const string Pages_Administration_SalesManager_ChangePermissions = "Pages.Administration.SalesManager.ChangePermissions";
        public const string Pages_Administration_SalesManager_Impersonation = "Pages.Administration.SalesManager.Impersonation";
        public const string Pages_Administration_SalesManager_Unlock = "Pages.Administration.SalesManager.Unlock";

        public const string Pages_Administration_Installer = "Pages.Administration.Installer";
        public const string Pages_Administration_Installer_Create = "Pages.Administration.Installer.Create";
        public const string Pages_Administration_Installer_Edit = "Pages.Administration.Installer.Edit";
        public const string Pages_Administration_Installer_Delete = "Pages.Administration.Installer.Delete";
        public const string Pages_Administration_Installer_ChangePermissions = "Pages.Administration.Installer.ChangePermissions";
        public const string Pages_Administration_Installer_Impersonation = "Pages.Administration.Installer.Impersonation";
        public const string Pages_Administration_Installer_Unlock = "Pages.Administration.Installer.Unlock";

        public const string Pages_Administration_InstallerInvitation = "Pages.Installer.InstallerInvitation";
        public const string Pages_Installer_New = "Pages.Installer.New";
        public const string Pages_Installer_Paid = "Pages.Installer.Paid";
        public const string Pages_Installer_QuickView = "Pages.Installer.QuickView";
        public const string Pages_Installer_RevertVerify = "Pages.Installer.RevertVerify";
        public const string Pages_Installer_Notify = "Pages.Installer.Notify";
        public const string Pages_Installer_SMS = "Pages.Installer.SMS";
        public const string Pages_Installer_Email = "Pages.Installer.Email";
        public const string Pages_Installer_Invoice = "Pages.Installer.Invoice";
        public const string Pages_Installer_ReadyToPay = "Pages.Installer.ReadyToPay";
        public const string Pages_Installer_Delete = "Pages.Installer.Delete";
        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
		public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
		public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
		public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
		public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

		public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";
        public const string Pages_Administration_OrganizationUnits_ManageRoles = "Pages.Administration.OrganizationUnits.ManageRoles";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

        public const string Pages_Administration_UiCustomization = "Pages.Administration.UiCustomization";

        public const string Pages_Administration_WebhookSubscription = "Pages.Administration.WebhookSubscription";
        public const string Pages_Administration_WebhookSubscription_Create = "Pages.Administration.WebhookSubscription.Create";
        public const string Pages_Administration_WebhookSubscription_Edit = "Pages.Administration.WebhookSubscription.Edit";
        public const string Pages_Administration_WebhookSubscription_ChangeActivity = "Pages.Administration.WebhookSubscription.ChangeActivity";
        public const string Pages_Administration_WebhookSubscription_Detail = "Pages.Administration.WebhookSubscription.Detail";
        public const string Pages_Administration_Webhook_ListSendAttempts = "Pages.Administration.Webhook.ListSendAttempts";
        public const string Pages_Administration_Webhook_ResendWebhook = "Pages.Administration.Webhook.ResendWebhook";

        public const string Pages_Administration_DynamicParameters = "Pages.Administration.DynamicParameters";
        public const string Pages_Administration_DynamicParameters_Create = "Pages.Administration.DynamicParameters.Create";
        public const string Pages_Administration_DynamicParameters_Edit = "Pages.Administration.DynamicParameters.Edit";
        public const string Pages_Administration_DynamicParameters_Delete = "Pages.Administration.DynamicParameters.Delete";

        public const string Pages_Administration_DynamicParameterValue = "Pages.Administration.DynamicParameterValue";
        public const string Pages_Administration_DynamicParameterValue_Create = "Pages.Administration.DynamicParameterValue.Create";
        public const string Pages_Administration_DynamicParameterValue_Edit = "Pages.Administration.DynamicParameterValue.Edit";
        public const string Pages_Administration_DynamicParameterValue_Delete = "Pages.Administration.DynamicParameterValue.Delete";

        public const string Pages_Administration_EntityDynamicParameters = "Pages.Administration.EntityDynamicParameters";
        public const string Pages_Administration_EntityDynamicParameters_Create = "Pages.Administration.EntityDynamicParameters.Create";
        public const string Pages_Administration_EntityDynamicParameters_Edit = "Pages.Administration.EntityDynamicParameters.Edit";
        public const string Pages_Administration_EntityDynamicParameters_Delete = "Pages.Administration.EntityDynamicParameters.Delete";

        public const string Pages_Administration_EntityDynamicParameterValue = "Pages.Administration.EntityDynamicParameterValue";
        public const string Pages_Administration_EntityDynamicParameterValue_Create = "Pages.Administration.EntityDynamicParameterValue.Create";
        public const string Pages_Administration_EntityDynamicParameterValue_Edit = "Pages.Administration.EntityDynamicParameterValue.Edit";
        public const string Pages_Administration_EntityDynamicParameterValue_Delete = "Pages.Administration.EntityDynamicParameterValue.Delete";
        //TENANT-SPECIFIC PERMISSIONS
        public const string Pages_Tenant_Dashboard_UserLeads = "Pages.Tenant.Dashboard.UserLeads";
        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";
        //public const string Pages_Tenant_Dashboard_FilterSalesRepUsers = "Pages.Tenant.Dashboard.FilterSalesRepUsers";
        public const string Pages_Tenant_Dashboard_ManagerLeads = "Pages.Tenant.Dashboard.ManagerLeads";
        /*Widgets Permission Start*/
        public const string Pages_Tenant_Dashboard_OrganizationWiseLeads = "Pages.Tenant.Dashboard.OrganizationWiseLeads";
        public const string Pages_Tenant_Dashboard_OrganizationWiseLeadDetails = "Pages.Tenant.Dashboard.OrganizationWiseLeadDetails";
        public const string Pages_Tenant_Dashboard_OrganizationWiseTracker = "Pages.Tenant.Dashboard.OrganizationWiseTracker";
        public const string Pages_Tenant_Dashboard_MyReminders = "Pages.Tenant.Dashboard.MyReminders";
        public const string Pages_Tenant_Dashboard_SalesDetails = "Pages.Tenant.Dashboard.SalesDetails";
        public const string Pages_Tenant_Dashboard_InvoiceStatus = "Pages.Tenant.Dashboard.InvoiceStatus";
        public const string Pages_Tenant_Dashboard_ToDoDetails = "Pages.Tenant.Dashboard.ToDoDetails";
        public const string Pages_Tenant_Dashboard_ManagerToDoDetails = "Pages.Tenant.Dashboard.ManagerToDoDetails";
        public const string Pages_Tenant_Dashboard_ManagerSalesDetails = "Pages.Tenant.Dashboard.ManagerSalesDetails";

        /*Widgets Permission End*/
        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Administration_Tenant_SubscriptionManagement = "Pages.Administration.Tenant.SubscriptionManagement";

        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";
        public const string Pages_Editions_MoveTenantsToAnotherEdition = "Pages.Editions.MoveTenantsToAnotherEdition";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
        public const string Pages_Administration_Host_Dashboard = "Pages.Administration.Host.Dashboard";


        public const string Pages_Notification = "Pages.Notification";
        public const string Pages_Sms = "Pages.Sms";
        public const string Pages_Email = "Pages.Email";


        public const string Pages_StcPvdStatus = "Pages.StcPvdStatus";
        public const string Pages_StcPvdStatus_Create = "Pages.StcPvdStatus.Create";
        public const string Pages_StcPvdStatus_Edit = "Pages.StcPvdStatus.Edit";
        public const string Pages_StcPvdStatus_Delete = "Pages.StcPvdStatus.Delete";


        public const string Pages_report = "Pages.report";
        public const string Pages_ActivityReport = "Pages.ActivityReport";
        public const string Pages_ToDoActivityReport = "Pages.ToDoActivityReport";

        public const string Pages_Tracker_Warranty = "Pages.Tracker.Warranty";
        public const string Pages_Tracker_Warranty_Email  = "Pages.Tracker.Warranty.Email";
        public const string Pages_Tracker_Warranty_Sms = "Pages.Tracker.Warranty.Sms";
        public const string Pages_Tracker_Warranty_QuickView = "Pages.Tracker.Warranty.QuickView";
        public const string Pages_Tracker_Warranty_Comment = "Pages.Tracker.Warranty.Comment";
        public const string Pages_Tracker_Warranty_Notify = "Pages.Tracker.Warranty.Notify";
        public const string Pages_Tracker_Warranty_DocUpload = "Pages.Tracker.Warranty.DocUpload";

    }
}

   