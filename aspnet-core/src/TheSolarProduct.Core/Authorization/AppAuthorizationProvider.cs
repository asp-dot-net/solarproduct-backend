﻿using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;

namespace TheSolarProduct.Authorization
{
	/// <summary>
	/// Application's authorization provider.
	/// Defines permissions for the application.
	/// See <see cref="AppPermissions"/> for all permission names.
	/// </summary>
	public class AppAuthorizationProvider : AuthorizationProvider
	{
		private readonly bool _isMultiTenancyEnabled;

		public AppAuthorizationProvider(bool isMultiTenancyEnabled)
		{
			_isMultiTenancyEnabled = isMultiTenancyEnabled;
		}

		public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
		{
			_isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
		}

		public override void SetPermissions(IPermissionDefinitionContext context)
		{
			//COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

			var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));


			//TENANT-SPECIFIC PERMISSIONS

			//pages.CreateChildPermission(AppPermissions.Pages_DemoUiComponents, L("DemoUiComponents"));

			var dashboard = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);
			#region Dashboard
			/// public const string Pages_Tenant_Dashboard_FilterSalesRepUsers = "Pages.Tenant.Dashboard.FilterSalesRepUsers";
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_UserLeads, L("UserLeadsWidget"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_ManagerLeads, L("ManagerLeadsWidget"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_OrganizationWiseLeads, L("OrganizationWiseLeads"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_OrganizationWiseLeadDetails, L("OrganizationWiseLeadDetails"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_OrganizationWiseTracker, L("OrganizationWiseTracker"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_MyReminders, L("MyReminders"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_SalesDetails, L("SalesDetails"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_InvoiceStatus, L("InvoiceStatus"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_ToDoDetails, L("ToDoDetails"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_ManagerSalesDetails, L("ManagerSalesDetails"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_ManagerToDoDetails, L("ManagerToDoDetails"), multiTenancySides: MultiTenancySides.Tenant);

			#endregion

			var datavaults = pages.CreateChildPermission(AppPermissions.Pages_DataVaults, L("DataVaults"));

			#region DataVaults

			var postalTypes = datavaults.CreateChildPermission(AppPermissions.Pages_PostalTypes, L("PostalTypes"), multiTenancySides: MultiTenancySides.Host);
			postalTypes.CreateChildPermission(AppPermissions.Pages_PostalTypes_Create, L("CreateNewPostalType"), multiTenancySides: MultiTenancySides.Host);
			postalTypes.CreateChildPermission(AppPermissions.Pages_PostalTypes_Edit, L("EditPostalType"), multiTenancySides: MultiTenancySides.Host);
			postalTypes.CreateChildPermission(AppPermissions.Pages_PostalTypes_Delete, L("DeletePostalType"), multiTenancySides: MultiTenancySides.Host);

			var postCodes = datavaults.CreateChildPermission(AppPermissions.Pages_PostCodes, L("PostCodes"), multiTenancySides: MultiTenancySides.Host);
			postCodes.CreateChildPermission(AppPermissions.Pages_PostCodes_Create, L("CreateNewPostCode"), multiTenancySides: MultiTenancySides.Host);
			postCodes.CreateChildPermission(AppPermissions.Pages_PostCodes_Edit, L("EditPostCode"), multiTenancySides: MultiTenancySides.Host);
			postCodes.CreateChildPermission(AppPermissions.Pages_PostCodes_Delete, L("DeletePostCode"), multiTenancySides: MultiTenancySides.Host);

			var streetNames = datavaults.CreateChildPermission(AppPermissions.Pages_StreetNames, L("StreetNames"), multiTenancySides: MultiTenancySides.Host);
			streetNames.CreateChildPermission(AppPermissions.Pages_StreetNames_Create, L("CreateNewStreetName"), multiTenancySides: MultiTenancySides.Host);
			streetNames.CreateChildPermission(AppPermissions.Pages_StreetNames_Edit, L("EditStreetName"), multiTenancySides: MultiTenancySides.Host);
			streetNames.CreateChildPermission(AppPermissions.Pages_StreetNames_Delete, L("DeleteStreetName"), multiTenancySides: MultiTenancySides.Host);

			var streetTypes = datavaults.CreateChildPermission(AppPermissions.Pages_StreetTypes, L("StreetTypes"), multiTenancySides: MultiTenancySides.Host);
			streetTypes.CreateChildPermission(AppPermissions.Pages_StreetTypes_Create, L("CreateNewStreetType"), multiTenancySides: MultiTenancySides.Host);
			streetTypes.CreateChildPermission(AppPermissions.Pages_StreetTypes_Edit, L("EditStreetType"), multiTenancySides: MultiTenancySides.Host);
			streetTypes.CreateChildPermission(AppPermissions.Pages_StreetTypes_Delete, L("DeleteStreetType"), multiTenancySides: MultiTenancySides.Host);

			var unitTypes = datavaults.CreateChildPermission(AppPermissions.Pages_UnitTypes, L("UnitTypes"), multiTenancySides: MultiTenancySides.Host);
			unitTypes.CreateChildPermission(AppPermissions.Pages_UnitTypes_Create, L("CreateNewUnitType"), multiTenancySides: MultiTenancySides.Host);
			unitTypes.CreateChildPermission(AppPermissions.Pages_UnitTypes_Edit, L("EditUnitType"), multiTenancySides: MultiTenancySides.Host);
			unitTypes.CreateChildPermission(AppPermissions.Pages_UnitTypes_Delete, L("DeleteUnitType"), multiTenancySides: MultiTenancySides.Host);

			var emailTemplates = datavaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates, L("EmailTemplate"), multiTenancySides: MultiTenancySides.Tenant);
			emailTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Create, L("CreateNewEmailTemplate"), multiTenancySides: MultiTenancySides.Tenant);
			emailTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Edit, L("EditEmailTemplate"), multiTenancySides: MultiTenancySides.Tenant);
			emailTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Delete, L("DeleteEmailTemplate"), multiTenancySides: MultiTenancySides.Tenant);

			var documenttypes = datavaults.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_DocumentType, L("DocumentType"), multiTenancySides: MultiTenancySides.Tenant);
			documenttypes.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_DocumentType_Create, L("CreateNewDocumentType"), multiTenancySides: MultiTenancySides.Tenant);
			documenttypes.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_DocumentType_Edit, L("EditDocumentType"), multiTenancySides: MultiTenancySides.Tenant);
			documenttypes.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_DocumentType_Delete, L("DeleteDocumentType"), multiTenancySides: MultiTenancySides.Tenant);

			var documentlibrary = datavaults.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_DocumentLibrary, L("DocumentLibrary"), multiTenancySides: MultiTenancySides.Tenant);
			documentlibrary.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_DocumentLibrary_Create, L("CreateNewDocumentLibrary"), multiTenancySides: MultiTenancySides.Tenant);
			documentlibrary.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_DocumentLibrary_Edit, L("EditDocumentLibrary"), multiTenancySides: MultiTenancySides.Tenant);
			documentlibrary.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_DocumentLibrary_Delete, L("DeleteDocumentLibrary"), multiTenancySides: MultiTenancySides.Tenant);

			var JobCancellationReason = datavaults.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_JobCancellationReason, L("JobCancellationReason"), multiTenancySides: MultiTenancySides.Tenant);
			JobCancellationReason.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_JobCancellationReason_Create, L("CreateNewJobCancellationReason"), multiTenancySides: MultiTenancySides.Tenant);
			JobCancellationReason.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_JobCancellationReason_Edit, L("EditJobCancellationReason"), multiTenancySides: MultiTenancySides.Tenant);
			JobCancellationReason.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_JobCancellationReason_Delete, L("DeleteJobCancellationReason"), multiTenancySides: MultiTenancySides.Tenant);

			var leadExpenses = datavaults.CreateChildPermission(AppPermissions.Pages_LeadExpenses, L("LeadExpenses"), multiTenancySides: MultiTenancySides.Tenant);
			leadExpenses.CreateChildPermission(AppPermissions.Pages_LeadExpenses_Create, L("CreateNewLeadExpense"), multiTenancySides: MultiTenancySides.Tenant);
			leadExpenses.CreateChildPermission(AppPermissions.Pages_LeadExpenses_Edit, L("EditLeadExpense"), multiTenancySides: MultiTenancySides.Tenant);
			leadExpenses.CreateChildPermission(AppPermissions.Pages_LeadExpenses_Delete, L("DeleteLeadExpense"), multiTenancySides: MultiTenancySides.Tenant);

			var leadSources = datavaults.CreateChildPermission(AppPermissions.Pages_LeadSources, L("LeadSources"), multiTenancySides: MultiTenancySides.Tenant);
			leadSources.CreateChildPermission(AppPermissions.Pages_LeadSources_Create, L("CreateNewLeadSource"), multiTenancySides: MultiTenancySides.Tenant);
			leadSources.CreateChildPermission(AppPermissions.Pages_LeadSources_Edit, L("EditLeadSource"), multiTenancySides: MultiTenancySides.Tenant);
			leadSources.CreateChildPermission(AppPermissions.Pages_LeadSources_Delete, L("DeleteLeadSource"), multiTenancySides: MultiTenancySides.Tenant);

			var teams = datavaults.CreateChildPermission(AppPermissions.Pages_Teams, L("Teams"), multiTenancySides: MultiTenancySides.Tenant);
			teams.CreateChildPermission(AppPermissions.Pages_Teams_Create, L("CreateNewTeam"), multiTenancySides: MultiTenancySides.Tenant);
			teams.CreateChildPermission(AppPermissions.Pages_Teams_Edit, L("EditTeam"), multiTenancySides: MultiTenancySides.Tenant);
			teams.CreateChildPermission(AppPermissions.Pages_Teams_Delete, L("DeleteTeam"), multiTenancySides: MultiTenancySides.Tenant);

			var userTeams = datavaults.CreateChildPermission(AppPermissions.Pages_UserTeams, L("UserTeams"), multiTenancySides: MultiTenancySides.Tenant);
			userTeams.CreateChildPermission(AppPermissions.Pages_UserTeams_Create, L("CreateNewUserTeam"), multiTenancySides: MultiTenancySides.Tenant);
			userTeams.CreateChildPermission(AppPermissions.Pages_UserTeams_Edit, L("EditUserTeam"), multiTenancySides: MultiTenancySides.Tenant);
			userTeams.CreateChildPermission(AppPermissions.Pages_UserTeams_Delete, L("DeleteUserTeam"), multiTenancySides: MultiTenancySides.Tenant);

			var categories = datavaults.CreateChildPermission(AppPermissions.Pages_Categories, L("Categories"), multiTenancySides: MultiTenancySides.Tenant);
			categories.CreateChildPermission(AppPermissions.Pages_Categories_Create, L("CreateNewCategory"), multiTenancySides: MultiTenancySides.Tenant);
			categories.CreateChildPermission(AppPermissions.Pages_Categories_Edit, L("EditCategory"), multiTenancySides: MultiTenancySides.Tenant);
			categories.CreateChildPermission(AppPermissions.Pages_Categories_Delete, L("DeleteCategory"), multiTenancySides: MultiTenancySides.Tenant);

			var cancelReasons = datavaults.CreateChildPermission(AppPermissions.Pages_CancelReasons, L("CancelReasons"), multiTenancySides: MultiTenancySides.Tenant);
			cancelReasons.CreateChildPermission(AppPermissions.Pages_CancelReasons_Create, L("CreateNewCancelReason"), multiTenancySides: MultiTenancySides.Tenant);
			cancelReasons.CreateChildPermission(AppPermissions.Pages_CancelReasons_Edit, L("EditCancelReason"), multiTenancySides: MultiTenancySides.Tenant);
			cancelReasons.CreateChildPermission(AppPermissions.Pages_CancelReasons_Delete, L("DeleteCancelReason"), multiTenancySides: MultiTenancySides.Tenant);

			var rejectReasons = datavaults.CreateChildPermission(AppPermissions.Pages_RejectReasons, L("RejectReasons"), multiTenancySides: MultiTenancySides.Tenant);
			rejectReasons.CreateChildPermission(AppPermissions.Pages_RejectReasons_Create, L("CreateNewRejectReason"), multiTenancySides: MultiTenancySides.Tenant);
			rejectReasons.CreateChildPermission(AppPermissions.Pages_RejectReasons_Edit, L("EditRejectReason"), multiTenancySides: MultiTenancySides.Tenant);
			rejectReasons.CreateChildPermission(AppPermissions.Pages_RejectReasons_Delete, L("DeleteRejectReason"), multiTenancySides: MultiTenancySides.Tenant);

			var elecDistributors = datavaults.CreateChildPermission(AppPermissions.Pages_ElecDistributors, L("ElecDistributors"), multiTenancySides: MultiTenancySides.Tenant);
			elecDistributors.CreateChildPermission(AppPermissions.Pages_ElecDistributors_Create, L("CreateNewElecDistributor"), multiTenancySides: MultiTenancySides.Tenant);
			elecDistributors.CreateChildPermission(AppPermissions.Pages_ElecDistributors_Edit, L("EditElecDistributor"), multiTenancySides: MultiTenancySides.Tenant);
			elecDistributors.CreateChildPermission(AppPermissions.Pages_ElecDistributors_Delete, L("DeleteElecDistributor"), multiTenancySides: MultiTenancySides.Tenant);

			var elecRetailers = datavaults.CreateChildPermission(AppPermissions.Pages_ElecRetailers, L("ElecRetailers"), multiTenancySides: MultiTenancySides.Tenant);
			elecRetailers.CreateChildPermission(AppPermissions.Pages_ElecRetailers_Create, L("CreateNewElecRetailer"), multiTenancySides: MultiTenancySides.Tenant);
			elecRetailers.CreateChildPermission(AppPermissions.Pages_ElecRetailers_Edit, L("EditElecRetailer"), multiTenancySides: MultiTenancySides.Tenant);
			elecRetailers.CreateChildPermission(AppPermissions.Pages_ElecRetailers_Delete, L("DeleteElecRetailer"), multiTenancySides: MultiTenancySides.Tenant);

			var jobTypes = datavaults.CreateChildPermission(AppPermissions.Pages_JobTypes, L("JobTypes"), multiTenancySides: MultiTenancySides.Tenant);
			jobTypes.CreateChildPermission(AppPermissions.Pages_JobTypes_Create, L("CreateNewJobType"), multiTenancySides: MultiTenancySides.Tenant);
			jobTypes.CreateChildPermission(AppPermissions.Pages_JobTypes_Edit, L("EditJobType"), multiTenancySides: MultiTenancySides.Tenant);
			jobTypes.CreateChildPermission(AppPermissions.Pages_JobTypes_Delete, L("DeleteJobType"), multiTenancySides: MultiTenancySides.Tenant);

			var jobStatuses = datavaults.CreateChildPermission(AppPermissions.Pages_JobStatuses, L("JobStatuses"), multiTenancySides: MultiTenancySides.Tenant);
			jobStatuses.CreateChildPermission(AppPermissions.Pages_JobStatuses_Create, L("CreateNewJobStatus"), multiTenancySides: MultiTenancySides.Tenant);
			jobStatuses.CreateChildPermission(AppPermissions.Pages_JobStatuses_Edit, L("EditJobStatus"), multiTenancySides: MultiTenancySides.Tenant);
			jobStatuses.CreateChildPermission(AppPermissions.Pages_JobStatuses_Delete, L("DeleteJobStatus"), multiTenancySides: MultiTenancySides.Tenant);

			var productTypes = datavaults.CreateChildPermission(AppPermissions.Pages_ProductTypes, L("ProductTypes"), multiTenancySides: MultiTenancySides.Tenant);
			productTypes.CreateChildPermission(AppPermissions.Pages_ProductTypes_Create, L("CreateNewProductType"), multiTenancySides: MultiTenancySides.Tenant);
			productTypes.CreateChildPermission(AppPermissions.Pages_ProductTypes_Edit, L("EditProductType"), multiTenancySides: MultiTenancySides.Tenant);
			productTypes.CreateChildPermission(AppPermissions.Pages_ProductTypes_Delete, L("DeleteProductType"), multiTenancySides: MultiTenancySides.Tenant);

			var productItems = datavaults.CreateChildPermission(AppPermissions.Pages_ProductItems, L("ProductItems"), multiTenancySides: MultiTenancySides.Tenant);
			productItems.CreateChildPermission(AppPermissions.Pages_ProductItems_Create, L("CreateNewProductItem"), multiTenancySides: MultiTenancySides.Tenant);
			productItems.CreateChildPermission(AppPermissions.Pages_ProductItems_Edit, L("EditProductItem"), multiTenancySides: MultiTenancySides.Tenant);
			productItems.CreateChildPermission(AppPermissions.Pages_ProductItems_Delete, L("DeleteProductItem"), multiTenancySides: MultiTenancySides.Tenant);

			var houseTypes = datavaults.CreateChildPermission(AppPermissions.Pages_HouseTypes, L("HouseTypes"), multiTenancySides: MultiTenancySides.Tenant);
			houseTypes.CreateChildPermission(AppPermissions.Pages_HouseTypes_Create, L("CreateNewHouseType"), multiTenancySides: MultiTenancySides.Tenant);
			houseTypes.CreateChildPermission(AppPermissions.Pages_HouseTypes_Edit, L("EditHouseType"), multiTenancySides: MultiTenancySides.Tenant);
			houseTypes.CreateChildPermission(AppPermissions.Pages_HouseTypes_Delete, L("DeleteHouseType"), multiTenancySides: MultiTenancySides.Tenant);

			var roofTypes = datavaults.CreateChildPermission(AppPermissions.Pages_RoofTypes, L("RoofTypes"), multiTenancySides: MultiTenancySides.Tenant);
			roofTypes.CreateChildPermission(AppPermissions.Pages_RoofTypes_Create, L("CreateNewRoofType"), multiTenancySides: MultiTenancySides.Tenant);
			roofTypes.CreateChildPermission(AppPermissions.Pages_RoofTypes_Edit, L("EditRoofType"), multiTenancySides: MultiTenancySides.Tenant);
			roofTypes.CreateChildPermission(AppPermissions.Pages_RoofTypes_Delete, L("DeleteRoofType"), multiTenancySides: MultiTenancySides.Tenant);

			var roofAngles = datavaults.CreateChildPermission(AppPermissions.Pages_RoofAngles, L("RoofAngles"), multiTenancySides: MultiTenancySides.Tenant);
			roofAngles.CreateChildPermission(AppPermissions.Pages_RoofAngles_Create, L("CreateNewRoofAngle"), multiTenancySides: MultiTenancySides.Tenant);
			roofAngles.CreateChildPermission(AppPermissions.Pages_RoofAngles_Edit, L("EditRoofAngle"), multiTenancySides: MultiTenancySides.Tenant);
			roofAngles.CreateChildPermission(AppPermissions.Pages_RoofAngles_Delete, L("DeleteRoofAngle"), multiTenancySides: MultiTenancySides.Tenant);

			var meterPhases = datavaults.CreateChildPermission(AppPermissions.Pages_MeterPhases, L("MeterPhases"), multiTenancySides: MultiTenancySides.Tenant);
			meterPhases.CreateChildPermission(AppPermissions.Pages_MeterPhases_Create, L("CreateNewMeterPhase"), multiTenancySides: MultiTenancySides.Tenant);
			meterPhases.CreateChildPermission(AppPermissions.Pages_MeterPhases_Edit, L("EditMeterPhase"), multiTenancySides: MultiTenancySides.Tenant);
			meterPhases.CreateChildPermission(AppPermissions.Pages_MeterPhases_Delete, L("DeleteMeterPhase"), multiTenancySides: MultiTenancySides.Tenant);

			var meterUpgrades = datavaults.CreateChildPermission(AppPermissions.Pages_MeterUpgrades, L("MeterUpgrades"), multiTenancySides: MultiTenancySides.Tenant);
			meterUpgrades.CreateChildPermission(AppPermissions.Pages_MeterUpgrades_Create, L("CreateNewMeterUpgrade"), multiTenancySides: MultiTenancySides.Tenant);
			meterUpgrades.CreateChildPermission(AppPermissions.Pages_MeterUpgrades_Edit, L("EditMeterUpgrade"), multiTenancySides: MultiTenancySides.Tenant);
			meterUpgrades.CreateChildPermission(AppPermissions.Pages_MeterUpgrades_Delete, L("DeleteMeterUpgrade"), multiTenancySides: MultiTenancySides.Tenant);

			var financeOptions = datavaults.CreateChildPermission(AppPermissions.Pages_FinanceOptions, L("FinanceOptions"), multiTenancySides: MultiTenancySides.Tenant);
			financeOptions.CreateChildPermission(AppPermissions.Pages_FinanceOptions_Create, L("CreateNewFinanceOption"), multiTenancySides: MultiTenancySides.Tenant);
			financeOptions.CreateChildPermission(AppPermissions.Pages_FinanceOptions_Edit, L("EditFinanceOption"), multiTenancySides: MultiTenancySides.Tenant);
			financeOptions.CreateChildPermission(AppPermissions.Pages_FinanceOptions_Delete, L("DeleteFinanceOption"), multiTenancySides: MultiTenancySides.Tenant);

			var depositOptions = datavaults.CreateChildPermission(AppPermissions.Pages_DepositOptions, L("DepositOptions"), multiTenancySides: MultiTenancySides.Tenant);
			depositOptions.CreateChildPermission(AppPermissions.Pages_DepositOptions_Create, L("CreateNewDepositOption"), multiTenancySides: MultiTenancySides.Tenant);
			depositOptions.CreateChildPermission(AppPermissions.Pages_DepositOptions_Edit, L("EditDepositOption"), multiTenancySides: MultiTenancySides.Tenant);
			depositOptions.CreateChildPermission(AppPermissions.Pages_DepositOptions_Delete, L("DeleteDepositOption"), multiTenancySides: MultiTenancySides.Tenant);

			var paymentOptions = datavaults.CreateChildPermission(AppPermissions.Pages_PaymentOptions, L("PaymentOptions"), multiTenancySides: MultiTenancySides.Tenant);
			paymentOptions.CreateChildPermission(AppPermissions.Pages_PaymentOptions_Create, L("CreateNewPaymentOption"), multiTenancySides: MultiTenancySides.Tenant);
			paymentOptions.CreateChildPermission(AppPermissions.Pages_PaymentOptions_Edit, L("EditPaymentOption"), multiTenancySides: MultiTenancySides.Tenant);
			paymentOptions.CreateChildPermission(AppPermissions.Pages_PaymentOptions_Delete, L("DeletePaymentOption"), multiTenancySides: MultiTenancySides.Tenant);

			var promotionTypes = datavaults.CreateChildPermission(AppPermissions.Pages_PromotionTypes, L("PromotionTypes"), multiTenancySides: MultiTenancySides.Host);
			promotionTypes.CreateChildPermission(AppPermissions.Pages_PromotionTypes_Create, L("CreateNewPromotionType"), multiTenancySides: MultiTenancySides.Host);
			promotionTypes.CreateChildPermission(AppPermissions.Pages_PromotionTypes_Edit, L("EditPromotionType"), multiTenancySides: MultiTenancySides.Host);
			promotionTypes.CreateChildPermission(AppPermissions.Pages_PromotionTypes_Delete, L("DeletePromotionType"), multiTenancySides: MultiTenancySides.Host);

			var variations = datavaults.CreateChildPermission(AppPermissions.Pages_Variations, L("Variations"), multiTenancySides: MultiTenancySides.Tenant);
			variations.CreateChildPermission(AppPermissions.Pages_Variations_Create, L("CreateNewVariation"), multiTenancySides: MultiTenancySides.Tenant);
			variations.CreateChildPermission(AppPermissions.Pages_Variations_Edit, L("EditVariation"), multiTenancySides: MultiTenancySides.Tenant);
			variations.CreateChildPermission(AppPermissions.Pages_Variations_Delete, L("DeleteVariation"), multiTenancySides: MultiTenancySides.Tenant);

			var jobVariations = datavaults.CreateChildPermission(AppPermissions.Pages_JobVariations, L("JobVariations"), multiTenancySides: MultiTenancySides.Tenant);
			jobVariations.CreateChildPermission(AppPermissions.Pages_JobVariations_Create, L("CreateNewJobVariation"), multiTenancySides: MultiTenancySides.Tenant);
			jobVariations.CreateChildPermission(AppPermissions.Pages_JobVariations_Edit, L("EditJobVariation"), multiTenancySides: MultiTenancySides.Tenant);
			jobVariations.CreateChildPermission(AppPermissions.Pages_JobVariations_Delete, L("DeleteJobVariation"), multiTenancySides: MultiTenancySides.Tenant);

			var promotionMasters = datavaults.CreateChildPermission(AppPermissions.Pages_PromotionMasters, L("PromotionMasters"), multiTenancySides: MultiTenancySides.Tenant);
			promotionMasters.CreateChildPermission(AppPermissions.Pages_PromotionMasters_Create, L("CreateNewPromotionMaster"), multiTenancySides: MultiTenancySides.Tenant);
			promotionMasters.CreateChildPermission(AppPermissions.Pages_PromotionMasters_Edit, L("EditPromotionMaster"), multiTenancySides: MultiTenancySides.Tenant);
			promotionMasters.CreateChildPermission(AppPermissions.Pages_PromotionMasters_Delete, L("DeletePromotionMaster"), multiTenancySides: MultiTenancySides.Tenant);

			var jobPromotions = datavaults.CreateChildPermission(AppPermissions.Pages_JobPromotions, L("JobPromotions"), multiTenancySides: MultiTenancySides.Tenant);
			jobPromotions.CreateChildPermission(AppPermissions.Pages_JobPromotions_Create, L("CreateNewJobPromotion"), multiTenancySides: MultiTenancySides.Tenant);
			jobPromotions.CreateChildPermission(AppPermissions.Pages_JobPromotions_Edit, L("EditJobPromotion"), multiTenancySides: MultiTenancySides.Tenant);
			jobPromotions.CreateChildPermission(AppPermissions.Pages_JobPromotions_Delete, L("DeleteJobPromotion"), multiTenancySides: MultiTenancySides.Tenant);

			var refundReasons = datavaults.CreateChildPermission(AppPermissions.Pages_RefundReasons, L("RefundReasons"), multiTenancySides: MultiTenancySides.Tenant);
			refundReasons.CreateChildPermission(AppPermissions.Pages_RefundReasons_Create, L("CreateNewRefundReason"), multiTenancySides: MultiTenancySides.Tenant);
			refundReasons.CreateChildPermission(AppPermissions.Pages_RefundReasons_Edit, L("EditRefundReason"), multiTenancySides: MultiTenancySides.Tenant);
			refundReasons.CreateChildPermission(AppPermissions.Pages_RefundReasons_Delete, L("DeleteRefundReason"), multiTenancySides: MultiTenancySides.Tenant);

			var holdReasons = datavaults.CreateChildPermission(AppPermissions.Pages_HoldReasons, L("HoldReasons"), multiTenancySides: MultiTenancySides.Tenant);
			holdReasons.CreateChildPermission(AppPermissions.Pages_HoldReasons_Create, L("CreateNewHoldReason"), multiTenancySides: MultiTenancySides.Tenant);
			holdReasons.CreateChildPermission(AppPermissions.Pages_HoldReasons_Edit, L("EditHoldReason"), multiTenancySides: MultiTenancySides.Tenant);
			holdReasons.CreateChildPermission(AppPermissions.Pages_HoldReasons_Delete, L("DeleteHoldReason"), multiTenancySides: MultiTenancySides.Tenant);

			var invoicePaymentMethods = datavaults.CreateChildPermission(AppPermissions.Pages_InvoicePaymentMethods, L("InvoicePaymentMethods"), multiTenancySides: MultiTenancySides.Tenant);
			invoicePaymentMethods.CreateChildPermission(AppPermissions.Pages_InvoicePaymentMethods_Create, L("CreateNewInvoicePaymentMethod"), multiTenancySides: MultiTenancySides.Tenant);
			invoicePaymentMethods.CreateChildPermission(AppPermissions.Pages_InvoicePaymentMethods_Edit, L("EditInvoicePaymentMethod"), multiTenancySides: MultiTenancySides.Tenant);
			invoicePaymentMethods.CreateChildPermission(AppPermissions.Pages_InvoicePaymentMethods_Delete, L("DeleteInvoicePaymentMethod"), multiTenancySides: MultiTenancySides.Tenant);

			var invoicePayments = datavaults.CreateChildPermission(AppPermissions.Pages_InvoicePayments, L("InvoicePayments"), multiTenancySides: MultiTenancySides.Tenant);
			invoicePayments.CreateChildPermission(AppPermissions.Pages_InvoicePayments_Create, L("CreateNewInvoicePayment"), multiTenancySides: MultiTenancySides.Tenant);
			invoicePayments.CreateChildPermission(AppPermissions.Pages_InvoicePayments_Edit, L("EditInvoicePayment"), multiTenancySides: MultiTenancySides.Tenant);
			invoicePayments.CreateChildPermission(AppPermissions.Pages_InvoicePayments_Delete, L("DeleteInvoicePayment"), multiTenancySides: MultiTenancySides.Tenant);
			invoicePayments.CreateChildPermission(AppPermissions.Pages_InvoicePayments_Verify, L("InvoicePaymentsVerify"), multiTenancySides: MultiTenancySides.Tenant);

			var invoiceStatuses = datavaults.CreateChildPermission(AppPermissions.Pages_InvoiceStatuses, L("InvoiceStatuses"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceStatuses.CreateChildPermission(AppPermissions.Pages_InvoiceStatuses_Create, L("CreateNewInvoiceStatus"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceStatuses.CreateChildPermission(AppPermissions.Pages_InvoiceStatuses_Edit, L("EditInvoiceStatus"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceStatuses.CreateChildPermission(AppPermissions.Pages_InvoiceStatuses_Delete, L("DeleteInvoiceStatus"), multiTenancySides: MultiTenancySides.Tenant);

			var smsTemplates = datavaults.CreateChildPermission(AppPermissions.Pages_SmsTemplates, L("SmsTemplates"), multiTenancySides: MultiTenancySides.Tenant);
			smsTemplates.CreateChildPermission(AppPermissions.Pages_SmsTemplates_Create, L("CreateNewSmsTemplate"), multiTenancySides: MultiTenancySides.Tenant);
			smsTemplates.CreateChildPermission(AppPermissions.Pages_SmsTemplates_Edit, L("EditSmsTemplate"), multiTenancySides: MultiTenancySides.Tenant);
			smsTemplates.CreateChildPermission(AppPermissions.Pages_SmsTemplates_Delete, L("DeleteSmsTemplate"), multiTenancySides: MultiTenancySides.Tenant);

			var freebieTransports = datavaults.CreateChildPermission(AppPermissions.Pages_FreebieTransports, L("FreebieTransports"), multiTenancySides: MultiTenancySides.Tenant);
			freebieTransports.CreateChildPermission(AppPermissions.Pages_FreebieTransports_Create, L("CreateNewFreebieTransport"), multiTenancySides: MultiTenancySides.Tenant);
			freebieTransports.CreateChildPermission(AppPermissions.Pages_FreebieTransports_Edit, L("EditFreebieTransport"), multiTenancySides: MultiTenancySides.Tenant);
			freebieTransports.CreateChildPermission(AppPermissions.Pages_FreebieTransports_Delete, L("DeleteFreebieTransport"), multiTenancySides: MultiTenancySides.Tenant);

			var stcPvdStatus = datavaults.CreateChildPermission(AppPermissions.Pages_StcPvdStatus, L("StcPvdStatus"), multiTenancySides: MultiTenancySides.Tenant);
			stcPvdStatus.CreateChildPermission(AppPermissions.Pages_StcPvdStatus_Create, L("CreateStcPvdStatus"), multiTenancySides: MultiTenancySides.Tenant);
			stcPvdStatus.CreateChildPermission(AppPermissions.Pages_StcPvdStatus_Edit, L("EditStcPvdStatus"), multiTenancySides: MultiTenancySides.Tenant);
			stcPvdStatus.CreateChildPermission(AppPermissions.Pages_StcPvdStatus_Delete, L("DeleteStcPvdStatus"), multiTenancySides: MultiTenancySides.Tenant);

			#endregion

			var lead = pages.CreateChildPermission(AppPermissions.Pages_Leads, L("Leads"), multiTenancySides: MultiTenancySides.Tenant);

			#region
			lead.CreateChildPermission(AppPermissions.Pages_ManageLeads, L("ManageLeads"), multiTenancySides: MultiTenancySides.Tenant);
			lead.CreateChildPermission(AppPermissions.Pages_Leads_Create, L("CreateNewLead"), multiTenancySides: MultiTenancySides.Tenant);
			lead.CreateChildPermission(AppPermissions.Pages_Leads_Edit, L("EditLead"), multiTenancySides: MultiTenancySides.Tenant);
			lead.CreateChildPermission(AppPermissions.Pages_Leads_Delete, L("DeleteLead"), multiTenancySides: MultiTenancySides.Tenant);
			lead.CreateChildPermission(AppPermissions.Pages_Leads_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
			lead.CreateChildPermission(AppPermissions.Pages_Leads_Import, L("ImportFromExcel"), multiTenancySides: MultiTenancySides.Tenant);
			lead.CreateChildPermission(AppPermissions.Pages_Lead_Duplicate, L("DuplicateLead"), multiTenancySides: MultiTenancySides.Tenant);
			lead.CreateChildPermission(AppPermissions.Pages_Lead_Closed, L("ClosedLead"), multiTenancySides: MultiTenancySides.Tenant);
			lead.CreateChildPermission(AppPermissions.Pages_Lead_Calcel, L("CancelLead"), multiTenancySides: MultiTenancySides.Tenant);
			lead.CreateChildPermission(AppPermissions.Pages_Lead_Rejects, L("RejectLead"), multiTenancySides: MultiTenancySides.Tenant);
			lead.CreateChildPermission(AppPermissions.Pages_MyLeads, L("MyLead"), multiTenancySides: MultiTenancySides.Tenant);
			lead.CreateChildPermission(AppPermissions.Pages_Leads_Assign, L("AssignOrTransferLead"), multiTenancySides: MultiTenancySides.Tenant);
			lead.CreateChildPermission(AppPermissions.Pages_LeadTracker, L("LeadTracker"), multiTenancySides: MultiTenancySides.Tenant);
			lead.CreateChildPermission(AppPermissions.Pages_Leads_ChangeOrganization, L("ChangeOrganization"), multiTenancySides: MultiTenancySides.Tenant);
			#endregion

			var promotionsGroup = pages.CreateChildPermission(AppPermissions.Pages_PromotionGroup, L("Promotions"));

			#region Promotions
			var promotionUsers = promotionsGroup.CreateChildPermission(AppPermissions.Pages_PromotionUsers, L("PromotionUsers"), multiTenancySides: MultiTenancySides.Tenant);
			promotionUsers.CreateChildPermission(AppPermissions.Pages_PromotionUsers_Create, L("CreateNewPromotionUser"), multiTenancySides: MultiTenancySides.Tenant);
			promotionUsers.CreateChildPermission(AppPermissions.Pages_PromotionUsers_Edit, L("EditPromotionUser"), multiTenancySides: MultiTenancySides.Tenant);
			promotionUsers.CreateChildPermission(AppPermissions.Pages_PromotionUsers_Delete, L("DeletePromotionUser"), multiTenancySides: MultiTenancySides.Tenant);
			promotionUsers.CreateChildPermission(AppPermissions.Pages_PromotionUsers_Excel, L("PromotionUsersExcel"), multiTenancySides: MultiTenancySides.Tenant);

			var promotions = promotionsGroup.CreateChildPermission(AppPermissions.Pages_Promotions, L("Promotions"), multiTenancySides: MultiTenancySides.Tenant);
			promotions.CreateChildPermission(AppPermissions.Pages_Promotions_Create, L("CreateNewPromotion"), multiTenancySides: MultiTenancySides.Tenant);
			promotions.CreateChildPermission(AppPermissions.Pages_Promotions_Edit, L("EditPromotion"), multiTenancySides: MultiTenancySides.Tenant);
			promotions.CreateChildPermission(AppPermissions.Pages_Promotions_Delete, L("DeletePromotion"), multiTenancySides: MultiTenancySides.Tenant);

			var promotionResponseStatuses = promotionsGroup.CreateChildPermission(AppPermissions.Pages_PromotionResponseStatuses, L("PromotionResponseStatuses"), multiTenancySides: MultiTenancySides.Tenant);
			promotionResponseStatuses.CreateChildPermission(AppPermissions.Pages_PromotionResponseStatuses_Create, L("CreateNewPromotionResponseStatus"), multiTenancySides: MultiTenancySides.Tenant);
			promotionResponseStatuses.CreateChildPermission(AppPermissions.Pages_PromotionResponseStatuses_Edit, L("EditPromotionResponseStatus"), multiTenancySides: MultiTenancySides.Tenant);
			promotionResponseStatuses.CreateChildPermission(AppPermissions.Pages_PromotionResponseStatuses_Delete, L("DeletePromotionResponseStatus"), multiTenancySides: MultiTenancySides.Tenant);

			#endregion


			var Tracker = pages.CreateChildPermission(AppPermissions.Pages_Tracker, L("Tracker"), multiTenancySides: MultiTenancySides.Tenant);

			#region Tracker
			var applicationTracker = Tracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker, L("ApplicationTracker"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_AddNotes, L("ApplicationTrackerAddNotes"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_DocumentRequest, L("ApplicationTrackerDocumentRequest"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_Email, L("ApplicationTrackerEmail"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_JobDetail, L("ApplicationTrackerJobDetail"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_Notify, L("ApplicationTrackerNotify"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_QuickView, L("ApplicationTrackerQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_SMS, L("ApplicationTrackerSMS"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_Job_Edit, L("ApplicationTrackerJobEdit"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_Export, L("ApplicationTrackerExport"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_Import, L("ApplicationTrackerImport"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_ToDo, L("ApplicationTrackerToDo"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_Reminder, L("ApplicationTrackerReminder"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_Comment, L("ApplicationTrackerComment"), multiTenancySides: MultiTenancySides.Tenant);

			var freebiesTracker = Tracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker, L("FreebiesTracker"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_AddTrackingNumber, L("FreebiesTrackerAddTrackingNumber"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_Email, L("FreebiesTrackerEmail"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_Notify, L("FreebiesTrackerNotify"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_QuickView, L("FreebiesTrackerQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_SMS, L("FreebiesTrackerSMS"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_Job_Edit, L("FreebiesTrackerJobEdit"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_Export, L("FreebiesTrackerExport"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_Import, L("FreebiesTrackerImport"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_ToDo, L("FreebiesTrackerToDo"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_Reminder, L("FreebiesTrackerReminder"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_Comment, L("FreebiesTrackerComment"), multiTenancySides: MultiTenancySides.Tenant);

			var financeTracker = Tracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker, L("FinanceTracker"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_AddDetails, L("FinanceTrackerAddDetails"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_Email, L("FinanceTrackerEmail"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_Notify, L("FinanceTrackerNotify"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_QuickView, L("FinanceTrackerQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_SMS, L("FinanceTrackerSMS"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_Job_Edit, L("FinanceTrackerJobEdit"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_Export, L("FinanceTrackerExport"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_Import, L("FinanceTrackerImport"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_ToDo, L("FinanceTrackerToDo"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_Reminder, L("FinanceTrackerReminder"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_Comment, L("FinanceTrackerComment"), multiTenancySides: MultiTenancySides.Tenant);

			var refundTracker = Tracker.CreateChildPermission(AppPermissions.Pages_RefundTracker, L("RefundTracker"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Delete, L("RefundTrackerDelete"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Email, L("RefundTrackerEmail"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Notify, L("RefundTrackerNotify"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_QuickView, L("RefundTrackerQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Refund, L("RefundTrackerRefund"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_SMS, L("RefundTrackerSMS"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Job_Edit, L("RefundTrackerJobEdit"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Export, L("RefundTrackerExport"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Import, L("RefundTrackerImport"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_ToDo, L("RefundTrackerToDo"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Reminder, L("RefundTrackerReminder"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Comment, L("RefundTrackerComment"), multiTenancySides: MultiTenancySides.Tenant);

			var JobActive = Tracker.CreateChildPermission(AppPermissions.Pages_JobActiveTracker, L("JobActiveList"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_AddDetails, L("ActiveJobAddDetails"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_Email, L("ActiveJobEmail"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_Notify, L("ActiveJobNotify"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_QuickView, L("ActiveJobQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_SMS, L("ActiveJobSMS"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_Job_Edit, L("ActiveJobEdit"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_Export, L("ActiveJobExport"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_Import, L("ActiveJobImport"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_ToDo, L("ActiveJobToDo"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_Reminder, L("ActiveJobReminder"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_Comment, L("ActiveJobComment"), multiTenancySides: MultiTenancySides.Tenant);


			

			var Gridconnection = Tracker.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker, L("gridconnectiontraker"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Detail, L("Gridconntiondetail"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_AddMeterDetail, L("GridConnectionAddMeter"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Job_Edit, L("GridConnectionJobEdit"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Export, L("GridconnectiontrackerJobExport"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Import, L("GridconnectiontrackerJobImport"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_JobDetail, L("GridconnectiontrackerJobDetail"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Notify, L("GridconnectiontrackerNotify"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_QuickView, L("GridconnectiontrackerQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_SMS, L("GridconnectiontrackerSMS"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_AddNote, L("GridconnectiontrackerAddNote"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Email, L("GridconnectiontrackerEmail"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Delete, L("GridconnectiontrackerDelete"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_ToDo, L("GridconnectiontrackerToDo"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Reminder, L("GridconnectiontrackerReminder"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Comment, L("GridconnectiontrackerComment"), multiTenancySides: MultiTenancySides.Tenant);

			var stcTracker = Tracker.CreateChildPermission(AppPermissions.Pages_STCTracker, L("STCTracker"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Edit, L("STCTrackerEdit"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Job_Edit, L("STCTrackerJobEdit"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Delete, L("STCTrackerJobDelete"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Export, L("STCTrackerJobExport"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Import, L("STCTrackerJobImport"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_JobDetail, L("STCTrackerJobDetail"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Notify, L("STCTrackerNotify"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_QuickView, L("STCTrackerQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_SMS, L("STCTrackerSMS"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_AddSTC, L("STCTrackerAddSTC"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Email, L("STCTrackerEmail"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_ToDo, L("STCTrackerToDo"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Reminder, L("STCTrackerReminder"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Comment, L("STCTrackerComment"), multiTenancySides: MultiTenancySides.Tenant);

			var jobGrid = Tracker.CreateChildPermission(AppPermissions.Pages_JobGrid, L("JobGrid"), multiTenancySides: MultiTenancySides.Tenant);
			jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_Edit, L("JobGridEdit"), multiTenancySides: MultiTenancySides.Tenant);
			jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_Export, L("JobGridExport"), multiTenancySides: MultiTenancySides.Tenant);
			jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_Import, L("JobGridImport"), multiTenancySides: MultiTenancySides.Tenant);
			jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_JobDetail, L("JobGridJobDetail"), multiTenancySides: MultiTenancySides.Tenant);
			jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_Notify, L("JobGridNotify"), multiTenancySides: MultiTenancySides.Tenant);
			jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_QuickView, L("JobGridQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_SMS, L("JobGridSMS"), multiTenancySides: MultiTenancySides.Tenant);
			jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_Email, L("JobGridEmail"), multiTenancySides: MultiTenancySides.Tenant);
			jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_ToDo, L("JobGridToDo"), multiTenancySides: MultiTenancySides.Tenant);
			jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_Comment, L("JobGridComment"), multiTenancySides: MultiTenancySides.Tenant);
			jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_Reminder, L("JobGridReminder"), multiTenancySides: MultiTenancySides.Tenant);

			var Referral = Tracker.CreateChildPermission(AppPermissions.Pages_Tracker_Referral, L("Referral"), multiTenancySides: MultiTenancySides.Tenant);
			Referral.CreateChildPermission(AppPermissions.Pages_Tracker_Referral_Payment, L("ReferralPayment"), multiTenancySides: MultiTenancySides.Tenant);
			Referral.CreateChildPermission(AppPermissions.Pages_Tracker_Referral_QuickView, L("ReferralQuickView"), multiTenancySides: MultiTenancySides.Tenant);

			var Warranty = Tracker.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty, L("Warranty"), multiTenancySides: MultiTenancySides.Tenant);
			Warranty.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty_DocUpload, L("WarrantyDocUpload"), multiTenancySides: MultiTenancySides.Tenant);
			Warranty.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty_QuickView, L("WarrantyQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			Warranty.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty_Email, L("WarrantyEmail"), multiTenancySides: MultiTenancySides.Tenant);
			Warranty.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty_Sms, L("WarrantySms"), multiTenancySides: MultiTenancySides.Tenant);

			Warranty.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty_Comment, L("WarrantyComment"), multiTenancySides: MultiTenancySides.Tenant);
			Warranty.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty_Notify, L("WarrantyNotify"), multiTenancySides: MultiTenancySides.Tenant);
			#endregion



			var jobs = pages.CreateChildPermission(AppPermissions.Pages_Jobs, L("Jobs"), multiTenancySides: MultiTenancySides.Tenant);

			#region
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_Create, L("CreateNewJob"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_Edit, L("EditJob"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_Delete, L("DeleteJob"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_Sales, L("Sales"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_Quotation, L("Quotation"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_Installation, L("Installation"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_PostInstallation, L("PostInstallation"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_Refund, L("Refund"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_StatusChange, L("JobStatusChange"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_Detail, L("Detail"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_JobPickList_Create, L("CreateNewPickList"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_JobPickList_Edit, L("EditPickList"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_JobPickList_Delete, L("DeletePickList"), multiTenancySides: MultiTenancySides.Tenant);


			var jobRefunds = jobs.CreateChildPermission(AppPermissions.Pages_JobRefunds, L("JobRefunds"), multiTenancySides: MultiTenancySides.Tenant);
			jobRefunds.CreateChildPermission(AppPermissions.Pages_JobRefunds_Create, L("CreateNewJobRefund"), multiTenancySides: MultiTenancySides.Tenant);
			jobRefunds.CreateChildPermission(AppPermissions.Pages_JobRefunds_Edit, L("EditJobRefund"), multiTenancySides: MultiTenancySides.Tenant);
			jobRefunds.CreateChildPermission(AppPermissions.Pages_JobRefunds_Delete, L("DeleteJobRefund"), multiTenancySides: MultiTenancySides.Tenant);

			var quotations = jobs.CreateChildPermission(AppPermissions.Pages_Quotations, L("Quotations"), multiTenancySides: MultiTenancySides.Tenant);
			quotations.CreateChildPermission(AppPermissions.Pages_Quotations_Create, L("CreateNewQuotation"), multiTenancySides: MultiTenancySides.Tenant);
			quotations.CreateChildPermission(AppPermissions.Pages_Quotations_Edit, L("EditQuotations"), multiTenancySides: MultiTenancySides.Tenant);
			quotations.CreateChildPermission(AppPermissions.Pages_Quotations_Delete, L("DeleteQuotations"), multiTenancySides: MultiTenancySides.Tenant);

			var jobProductItems = jobs.CreateChildPermission(AppPermissions.Pages_JobProductItems, L("JobProductItems"), multiTenancySides: MultiTenancySides.Tenant);
			jobProductItems.CreateChildPermission(AppPermissions.Pages_JobProductItems_Create, L("CreateNewJobProductItem"), multiTenancySides: MultiTenancySides.Tenant);
			jobProductItems.CreateChildPermission(AppPermissions.Pages_JobProductItems_Edit, L("EditJobProductItem"), multiTenancySides: MultiTenancySides.Tenant);
			jobProductItems.CreateChildPermission(AppPermissions.Pages_JobProductItems_Delete, L("DeleteJobProductItem"), multiTenancySides: MultiTenancySides.Tenant);

			var installerDetails = jobs.CreateChildPermission(AppPermissions.Pages_InstallerDetails, L("InstallerDetails"), multiTenancySides: MultiTenancySides.Tenant);
			installerDetails.CreateChildPermission(AppPermissions.Pages_InstallerDetails_Create, L("CreateNewInstallerDetail"), multiTenancySides: MultiTenancySides.Tenant);
			installerDetails.CreateChildPermission(AppPermissions.Pages_InstallerDetails_Edit, L("EditInstallerDetail"), multiTenancySides: MultiTenancySides.Tenant);
			installerDetails.CreateChildPermission(AppPermissions.Pages_InstallerDetails_Delete, L("DeleteInstallerDetail"), multiTenancySides: MultiTenancySides.Tenant);


			var installerGroup = jobs.CreateChildPermission(AppPermissions.Pages_Tenant_Installer, L("Installer"), multiTenancySides: MultiTenancySides.Tenant);
			installerGroup.CreateChildPermission(AppPermissions.Pages_Tenant_Installer_Availability, L("InstallerAvailability"), multiTenancySides: MultiTenancySides.Tenant);
			installerGroup.CreateChildPermission(AppPermissions.Pages_Tenant_Installer_Invoice, L("InstallerInvoice"), multiTenancySides: MultiTenancySides.Tenant);


			var installerAddresses = jobs.CreateChildPermission(AppPermissions.Pages_InstallerAddresses, L("InstallerAddresses"), multiTenancySides: MultiTenancySides.Tenant);
			installerAddresses.CreateChildPermission(AppPermissions.Pages_InstallerAddresses_Create, L("CreateNewInstallerAddress"), multiTenancySides: MultiTenancySides.Tenant);
			installerAddresses.CreateChildPermission(AppPermissions.Pages_InstallerAddresses_Edit, L("EditInstallerAddress"), multiTenancySides: MultiTenancySides.Tenant);
			installerAddresses.CreateChildPermission(AppPermissions.Pages_InstallerAddresses_Delete, L("DeleteInstallerAddress"), multiTenancySides: MultiTenancySides.Tenant);

			#endregion

			var invoice = pages.CreateChildPermission(AppPermissions.Pages_Invoice, L("Invoice"), multiTenancySides: MultiTenancySides.Tenant);
			#region Invoice

			var invoiceTracker = invoice.CreateChildPermission(AppPermissions.Pages_InvoiceTracker, L("Invoice"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Email, L("InvoiceTrackerEmail"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Notify, L("InvoiceTrackerNotify"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_QuickView, L("InvoiceTrackerQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_SMS, L("InvoiceTrackerSMS"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Verify, L("InvoiceTrackerVerify"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Job_Edit, L("InvoiceTrackerJobEdit"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Export, L("InvoiceTrackerExport"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Import, L("InvoiceTrackerImport"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_ToDo, L("InvoiceTrackerToDo"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Reminder, L("InvoiceTrackerReminder"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Comment, L("InvoiceTrackerComment"), multiTenancySides: MultiTenancySides.Tenant);

			var invoiceIssuedTracker = invoice.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker, L("InvoiceIssuedTracker"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_Email, L("InvoiceIssuedTrackerEmail"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_Notify, L("InvoiceIssuedTrackerNotify"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_QuickView, L("InvoiceIssuedTrackerQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_SMS, L("InvoiceIssuedTrackerSMS"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_Job_Edit, L("InvoiceIssuedTrackerJobEdit"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_Export, L("InvoiceIssuedTrackerExport"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_Import, L("InvoiceIssuedTrackerImport"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_ToDo, L("InvoiceIssuedTrackerToDo"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_Reminder, L("InvoiceIssuedTrackerReminder"), multiTenancySides: MultiTenancySides.Tenant);
			invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_Comment, L("InvoiceIssuedTrackerComment"), multiTenancySides: MultiTenancySides.Tenant);
			#endregion

			var Installation = pages.CreateChildPermission(AppPermissions.Pages_InstallationSection, L("InstallationSection"), multiTenancySides: MultiTenancySides.Tenant);
			#region Installation
			var installerCalendar  = Installation.CreateChildPermission(AppPermissions.Pages_Calenders, L("Calenders"), multiTenancySides: MultiTenancySides.Tenant);
			installerCalendar.CreateChildPermission(AppPermissions.Pages_Tenant_InstallerManager_InstallerCalendar, L("InstallerCalendar"), multiTenancySides: MultiTenancySides.Tenant);

			Installation.CreateChildPermission(AppPermissions.Pages_Installation, L("Installation"), multiTenancySides: MultiTenancySides.Tenant);
			Installation.CreateChildPermission(AppPermissions.Pages_Map, L("Map"), multiTenancySides: MultiTenancySides.Tenant);

			var PenddingInstallation = Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation, L("pendinginstallation"), multiTenancySides: MultiTenancySides.Tenant);
			PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_View, L("pendinginstallationView"), multiTenancySides: MultiTenancySides.Tenant);
			PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_ToDo, L("pendinginstallationToDo"), multiTenancySides: MultiTenancySides.Tenant);
			PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Sms, L("pendinginstallationSMS"), multiTenancySides: MultiTenancySides.Tenant);
			PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Email, L("pendinginstallationEmail"), multiTenancySides: MultiTenancySides.Tenant);
			PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Comment, L("pendinginstallationComment"), multiTenancySides: MultiTenancySides.Tenant);
			PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Notify, L("pendinginstallationNotify"), multiTenancySides: MultiTenancySides.Tenant);
			PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Reminder, L("pendinginstallationReminder"), multiTenancySides: MultiTenancySides.Tenant);

			//Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation, L("pendinginstallation"), multiTenancySides: MultiTenancySides.Tenant);
			//Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_View, L("pendinginstallationView"), multiTenancySides: MultiTenancySides.Tenant);
			//Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_ToDo, L("pendinginstallationToDo"), multiTenancySides: MultiTenancySides.Tenant);
			//Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_ToDo, L("pendinginstallationSMS"), multiTenancySides: MultiTenancySides.Tenant);
			//Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Email, L("pendinginstallationEmail"), multiTenancySides: MultiTenancySides.Tenant);
			//Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Comment, L("pendinginstallationComment"), multiTenancySides: MultiTenancySides.Tenant);
			//Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Notify, L("pendinginstallationNotify"), multiTenancySides: MultiTenancySides.Tenant);
			//Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Reminder, L("pendinginstallationReminder"), multiTenancySides: MultiTenancySides.Tenant);

			var JobBooking = Installation.CreateChildPermission(AppPermissions.Pages_JobBooking, L("JobBooking"), multiTenancySides: MultiTenancySides.Tenant);
			JobBooking.CreateChildPermission(AppPermissions.Pages_JobBooking_Notify, L("JobBookingNotify"), multiTenancySides: MultiTenancySides.Tenant);
			JobBooking.CreateChildPermission(AppPermissions.Pages_JobBooking_ToDo, L("JobBookingToDo"), multiTenancySides: MultiTenancySides.Tenant);
			JobBooking.CreateChildPermission(AppPermissions.Pages_JobBooking_Comment, L("JobBookingComment"), multiTenancySides: MultiTenancySides.Tenant);
			JobBooking.CreateChildPermission(AppPermissions.Pages_JobBooking_Reminder, L("JobBookingReminder"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

			
			var InstallerInvoice = pages.CreateChildPermission(AppPermissions.Pages_Administration_InstallerInvitation, L("InstallerInvoice"), multiTenancySides: MultiTenancySides.Tenant);
			#region InstallerInvoice
			InstallerInvoice.CreateChildPermission(AppPermissions.Pages_Installer_New, L("NewInstallerInvoice"), multiTenancySides: MultiTenancySides.Tenant);
			InstallerInvoice.CreateChildPermission(AppPermissions.Pages_Installer_Paid, L("PaidInstallerInvoice"), multiTenancySides: MultiTenancySides.Tenant);
			InstallerInvoice.CreateChildPermission(AppPermissions.Pages_Installer_QuickView, L("InstallerInvoiceQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			InstallerInvoice.CreateChildPermission(AppPermissions.Pages_Installer_RevertVerify, L("InstallerInvoiceRevertVerify"), multiTenancySides: MultiTenancySides.Tenant);


			InstallerInvoice.CreateChildPermission(AppPermissions.Pages_Installer_Notify, L("InstallerInvoiceNotify"), multiTenancySides: MultiTenancySides.Tenant);
			InstallerInvoice.CreateChildPermission(AppPermissions.Pages_Installer_SMS, L("InstallerInvoiceSMS"), multiTenancySides: MultiTenancySides.Tenant);
			InstallerInvoice.CreateChildPermission(AppPermissions.Pages_Installer_Email, L("InstallerInvoiceEmail"), multiTenancySides: MultiTenancySides.Tenant);
			InstallerInvoice.CreateChildPermission(AppPermissions.Pages_Installer_Invoice, L("InvoiceInstaller"), multiTenancySides: MultiTenancySides.Tenant);
			InstallerInvoice.CreateChildPermission(AppPermissions.Pages_Installer_ReadyToPay, L("ReadyToPayInvoice"), multiTenancySides: MultiTenancySides.Tenant);
			InstallerInvoice.CreateChildPermission(AppPermissions.Pages_Installer_Delete, L("InstallerInvoiceDelete"), multiTenancySides: MultiTenancySides.Tenant);
			#endregion

			var Reply = pages.CreateChildPermission(AppPermissions.Pages_Notification, L("Notification"), multiTenancySides: MultiTenancySides.Tenant);

			#region Reply
			Reply.CreateChildPermission(AppPermissions.Pages_Sms, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
			Reply.CreateChildPermission(AppPermissions.Pages_Email, L("Email"), multiTenancySides: MultiTenancySides.Tenant);
			#endregion

			var report = pages.CreateChildPermission(AppPermissions.Pages_report, L("Report"), multiTenancySides: MultiTenancySides.Tenant);
			#region Report
			report.CreateChildPermission(AppPermissions.Pages_ActivityReport, L("ActivityReport"), multiTenancySides: MultiTenancySides.Tenant);
			report.CreateChildPermission(AppPermissions.Pages_ToDoActivityReport, L("ToDoActivityReport"), multiTenancySides: MultiTenancySides.Tenant);
			#endregion

			var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

			#region Administrator
			

			var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
			roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
			roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
			roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

			var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
			users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
			users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
			users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
			users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
			users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));
			users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Unlock, L("Unlock"));

			var salesRep = administration.CreateChildPermission(AppPermissions.Pages_Administration_SalesRep, L("SalesRep"), multiTenancySides: MultiTenancySides.Tenant);
			salesRep.CreateChildPermission(AppPermissions.Pages_Administration_SalesRep_Create, L("CreateNewSalesRep"), multiTenancySides: MultiTenancySides.Tenant);
			salesRep.CreateChildPermission(AppPermissions.Pages_Administration_SalesRep_Edit, L("EditSalesRep"), multiTenancySides: MultiTenancySides.Tenant);
			salesRep.CreateChildPermission(AppPermissions.Pages_Administration_SalesRep_Delete, L("DeleteSalesRep"), multiTenancySides: MultiTenancySides.Tenant);
			salesRep.CreateChildPermission(AppPermissions.Pages_Administration_SalesRep_ChangePermissions, L("ChangingPermissions"));
			salesRep.CreateChildPermission(AppPermissions.Pages_Administration_SalesRep_Impersonation, L("LoginForUsers"));
			salesRep.CreateChildPermission(AppPermissions.Pages_Administration_SalesRep_Unlock, L("Unlock"));

			var salesManager = administration.CreateChildPermission(AppPermissions.Pages_Administration_SalesManager, L("SalesManager"), multiTenancySides: MultiTenancySides.Tenant);
			salesManager.CreateChildPermission(AppPermissions.Pages_Administration_SalesManager_Create, L("CreateNewSalesManager"), multiTenancySides: MultiTenancySides.Tenant);
			salesManager.CreateChildPermission(AppPermissions.Pages_Administration_SalesManager_Edit, L("EditSalesManager"), multiTenancySides: MultiTenancySides.Tenant);
			salesManager.CreateChildPermission(AppPermissions.Pages_Administration_SalesManager_Delete, L("DeleteSalesManager"), multiTenancySides: MultiTenancySides.Tenant);
			salesManager.CreateChildPermission(AppPermissions.Pages_Administration_SalesManager_ChangePermissions, L("ChangingPermissions"));
			salesManager.CreateChildPermission(AppPermissions.Pages_Administration_SalesManager_Impersonation, L("LoginForUsers"));
			salesManager.CreateChildPermission(AppPermissions.Pages_Administration_SalesManager_Unlock, L("Unlock"));

			var Installer = administration.CreateChildPermission(AppPermissions.Pages_Administration_Installer, L("Installer"), multiTenancySides: MultiTenancySides.Tenant);
			Installer.CreateChildPermission(AppPermissions.Pages_Administration_Installer_Create, L("CreateNewInstaller"), multiTenancySides: MultiTenancySides.Tenant);
			Installer.CreateChildPermission(AppPermissions.Pages_Administration_Installer_Edit, L("EditInstaller"), multiTenancySides: MultiTenancySides.Tenant);
			Installer.CreateChildPermission(AppPermissions.Pages_Administration_Installer_Delete, L("DeleteInstaller"), multiTenancySides: MultiTenancySides.Tenant);
			Installer.CreateChildPermission(AppPermissions.Pages_Administration_Installer_ChangePermissions, L("ChangingPermissions"));
			Installer.CreateChildPermission(AppPermissions.Pages_Administration_Installer_Impersonation, L("LoginForUsers"));
			Installer.CreateChildPermission(AppPermissions.Pages_Administration_Installer_Unlock, L("Unlock"));



			//var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
			//languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
			//languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
			//languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
			//languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

			administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

			var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"), multiTenancySides: MultiTenancySides.Tenant);
			organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"), multiTenancySides: MultiTenancySides.Tenant);
			organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"), multiTenancySides: MultiTenancySides.Tenant);
			organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageRoles, L("ManagingRoles"), multiTenancySides: MultiTenancySides.Tenant);

			//administration.CreateChildPermission(AppPermissions.Pages_Administration_UiCustomization, L("VisualSettings"));

			var webhooks = administration.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription, L("Webhooks"));
			webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Create, L("CreatingWebhooks"));
			webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Edit, L("EditingWebhooks"));
			webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_ChangeActivity, L("ChangingWebhookActivity"));
			webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Detail, L("DetailingSubscription"));
			webhooks.CreateChildPermission(AppPermissions.Pages_Administration_Webhook_ListSendAttempts, L("ListingSendAttempts"));
			webhooks.CreateChildPermission(AppPermissions.Pages_Administration_Webhook_ResendWebhook, L("ResendingWebhook"));

			//var dynamicParameters = administration.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters, L("DynamicParameters"));
			//dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters_Create, L("CreatingDynamicParameters"));
			//dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters_Edit, L("EditingDynamicParameters"));
			//dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters_Delete, L("DeletingDynamicParameters"));

			//var dynamicParameterValues = dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue, L("DynamicParameterValue"));
			//dynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue_Create, L("CreatingDynamicParameterValue"));
			//dynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue_Edit, L("EditingDynamicParameterValue"));
			//dynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue_Delete, L("DeletingDynamicParameterValue"));

			//var entityDynamicParameters = dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameters, L("EntityDynamicParameters"));
			//entityDynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameters_Create, L("CreatingEntityDynamicParameters"));
			//entityDynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameters_Edit, L("EditingEntityDynamicParameters"));
			//entityDynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameters_Delete, L("DeletingEntityDynamicParameters"));

			//var entityDynamicParameterValues = dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameterValue, L("EntityDynamicParameterValue"));
			//entityDynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameterValue_Create, L("CreatingEntityDynamicParameterValue"));
			//entityDynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameterValue_Edit, L("EditingEntityDynamicParameterValue"));
			//entityDynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameterValue_Delete, L("DeletingEntityDynamicParameterValue"));

			administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);
			administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement, L("Subscription"), multiTenancySides: MultiTenancySides.Tenant);

			var departments = administration.CreateChildPermission(AppPermissions.Pages_Departments, L("Departments"));
			departments.CreateChildPermission(AppPermissions.Pages_Departments_Create, L("CreateNewDepartment"));
			departments.CreateChildPermission(AppPermissions.Pages_Departments_Edit, L("EditDepartment"));
			departments.CreateChildPermission(AppPermissions.Pages_Departments_Delete, L("DeleteDepartment"));
			#endregion

			administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
			administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
			administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
			administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Host);

			

			//HOST-SPECIFIC PERMISSIONS
			var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
			editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
			editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
			editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);
			editions.CreateChildPermission(AppPermissions.Pages_Editions_MoveTenantsToAnotherEdition, L("MoveTenantsToAnotherEdition"), multiTenancySides: MultiTenancySides.Host);

			var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
			tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
			tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
			tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
			tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
			tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);
		}

		private static ILocalizableString L(string name)
		{
			return new LocalizableString(name, TheSolarProductConsts.LocalizationSourceName);
		}
	}
}