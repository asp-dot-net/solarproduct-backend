﻿using TheSolarProduct.Leads;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Auditing;
using TheSolarProduct.Authorization.Users;

namespace TheSolarProduct.Jobs
{
	[Table("Jobs")]
	[Audited]
	public class Job : FullAuditedEntity, IMustHaveTenant
	{
		public int TenantId { get; set; }
		
		public virtual string Note { get; set; }
		
		public virtual string OldSystemDetails { get; set; }
	
		public virtual string InstallerNotes { get; set; }

		public virtual int? PanelOnFlat { get; set; }

		public virtual int? PanelOnPitched { get; set; }

		public virtual string NMINumber { get; set; }

		public virtual string RegPlanNo { get; set; }

		public virtual string LotNumber { get; set; }

		public virtual string PeakMeterNo { get; set; }

		public virtual string OffPeakMeter { get; set; }

		public virtual bool EnoughMeterSpace { get; set; }

		public virtual bool IsSystemOffPeak { get; set; }

		public virtual bool IsFinanceWithUs { get; set; }

		public virtual decimal? BasicCost { get; set; }

		public virtual decimal? SolarVLCRebate { get; set; }

		public virtual decimal? SolarVLCLoan { get; set; }

		public virtual decimal? TotalCost { get; set; }

		public virtual string Suburb { get; set; }

		public virtual int? SuburbId { get; set; }

		public virtual string State { get; set; }

		public virtual int? StateId { get; set; }

		public virtual string PostalCode { get; set; }

		public virtual string UnitNo { get; set; }

		public virtual string UnitType { get; set; }

		public virtual string StreetNo { get; set; }

		public virtual string StreetName { get; set; }

		public virtual string StreetType { get; set; }

		public virtual string Latitude { get; set; }

		public virtual string Longitude { get; set; }

		public virtual decimal? SystemCapacity { get; set; }

		public virtual decimal? STC { get; set; }

		public virtual decimal? STCPrice { get; set; }

		public virtual decimal? Rebate { get; set; }

		public virtual string Address { get; set; }

		public virtual string Country { get; set; }

		public virtual int? JobTypeId { get; set; }

		[ForeignKey("JobTypeId")]
		public JobType JobTypeFk { get; set; }

		public virtual int? JobStatusId { get; set; }

		[ForeignKey("JobStatusId")]
		public JobStatus JobStatusFk { get; set; }

		public virtual int? RoofTypeId { get; set; }

		[ForeignKey("RoofTypeId")]
		public RoofType RoofTypeFk { get; set; }

		public virtual int? RoofAngleId { get; set; }

		[ForeignKey("RoofAngleId")]
		public RoofAngle RoofAngleFk { get; set; }

		public virtual int? ElecDistributorId { get; set; }

		public virtual DateTime? DistAppliedDate { get; set; }

		public virtual string MeterNumber { get; set; }

		public virtual string ApplicationRefNo { get; set; }

		public virtual string AdditionalComments { get; set; }

		[ForeignKey("ElecDistributorId")]
		public ElecDistributor ElecDistributorFk { get; set; }

		public virtual int? LeadId { get; set; }

		[ForeignKey("LeadId")]
		public Lead LeadFk { get; set; }

		public virtual int? ElecRetailerId { get; set; }

		[ForeignKey("ElecRetailerId")]
		public ElecRetailer ElecRetailerFk { get; set; }

		public virtual int? PaymentOptionId { get; set; }

		[ForeignKey("PaymentOptionId")]
		public PaymentOption PaymentOptionFk { get; set; }

		public virtual int? DepositOptionId { get; set; }

		[ForeignKey("DepositOptionId")]
		public DepositOption DepositOptionFk { get; set; }

		public virtual int? MeterUpgradeId { get; set; }

		public virtual int? MeterPhaseId { get; set; }

		public virtual int? PromotionOfferId { get; set; }

		[ForeignKey("PromotionOfferId")]
		public PromotionOffer PromotionOfferFk { get; set; }

		public virtual int? HouseTypeId { get; set; }

		[ForeignKey("HouseTypeId")]
		public HouseType HouseTypeFk { get; set; }

		public virtual int? FinanceOptionId { get; set; }

		[ForeignKey("FinanceOptionId")]
		public FinanceOption FinanceOptionFk { get; set; }

		//Installation Details
		public virtual DateTime? InstallationDate { get; set; }

		public virtual string InstallationTime { get; set; }

		public virtual string GridConnectionNotes { get; set; }

		public virtual int? InstallerId { get; set; }

		public virtual int? ElectricianId { get; set; }

		public virtual int? DesignerId { get; set; }

		public virtual int? WarehouseLocation { get; set; }

		public virtual string InstallationNotes { get; set; }

		public virtual DateTime? NextFollowUpDate { get; set; }

		public virtual string JobHoldReason { get; set; }

		public virtual string JobCancelReason { get; set; }

		public virtual int? JobCancelReasonId { get; set; }

		public virtual string JobNumber { get; set; }

		public virtual int? JobHoldReasonId { get; set; }

		public virtual bool? IsRefund { get; set; }

		public virtual bool? RefundProcess { get; set; }

		public virtual DateTime? InstalledcompleteDate { get; set; }

		public virtual string IncompleteReason { get; set; }

		public virtual string MeterApplyRef { get; set; }

		public virtual string InspectorName { get; set; }

		public virtual DateTime? InspectionDate { get; set; }

		public virtual string ComplianceCertificate { get; set; }

		public virtual int? PostInstallationStatus { get; set; }

		public virtual DateTime? STCUploaddate { get; set; }

		public virtual string STCUploadNumber { get; set; }

		public virtual int? PVDStatus { get; set; }

		public virtual DateTime? STCAppliedDate { get; set; }

		public virtual string PVDNumber { get; set; }

		public virtual int? Quickformid { get; set; }

		public virtual string STCNotes { get; set; }

		public virtual TimeSpan? MeterTime { get; set; }

		public virtual DateTime? FinanceApplicationDate { get; set; }

		public virtual string FinancePurchaseNo { get; set; }

		public virtual DateTime? FinanceDocSentDate { get; set; }

		public virtual int? FinanceAppliedBy { get; set; }

		public virtual int? FinanceDocSentBy { get; set; }

		public virtual DateTime? FinanceDocReceivedDate { get; set; }

		public virtual int? FinanceDocReceivedBy { get; set; }

		/// <summary>
		/// 1 = Verify
		/// 2 = Not Verify
		/// 3 = Query
		/// </summary>
		public virtual int? FinanceDocumentVerified { get; set; }

		public virtual bool? IsActive { get; set; }

		public virtual DateTime? ActiveDate { get; set; }

		public virtual string RebateAppRef { get; set; }

		public virtual DateTime? ApprovalDate { get; set; }

		public virtual DateTime? ExpiryDate { get; set; }

		public virtual string VicRebate { get; set; }

		public virtual decimal? SolarVICRebate { get; set; }

		public virtual decimal? SolarVICLoanDiscont { get; set; }

		public virtual int? SolarRebateStatus { get; set; }

		public virtual string VicRebateNotes { get; set; }

		public virtual DateTime? DistApplied { get; set; }

		public virtual string ApprovalRef { get; set; }

		public virtual int? DistApproveBy { get; set; }

		public virtual DateTime? DistExpiryDate { get; set; }

		public virtual int? AppliedBy { get; set; }

		public virtual DateTime? DistApproveDate { get; set; }

		public virtual int? EmpId { get; set; }

		public virtual decimal? PaidAmmount { get; set; }

		public virtual int? Paidby { get; set; }

		public virtual int? PaidStatus { get; set; }

		public virtual string InvRefNo { get; set; }

		public virtual DateTime? InvPaidDate { get; set; }

		public virtual string ProjectNotes { get; set; }

		public virtual string ApplicationNotes { get; set; }

		public virtual DateTime? DocumentsVeridiedDate { get; set; }

		public virtual int? DocumentsVeridiedBy { get; set; }

		public virtual DateTime? CoolingoffPeriodEnd { get; set; }

		public virtual string FinanceNotes { get; set; }

		public virtual string ManualQuote { get; set; }

		public virtual decimal? ApprovedCapacityonExport { get; set; }

		public virtual decimal? ApprovedCapacityonNonExport { get; set; }

		public virtual DateTime? DepositeRecceivedDate { get; set; }

		public virtual decimal? DepositRequired { get; set; }
		public virtual bool? SmsSend { get; set; }
		public virtual DateTime? SmsSendDate { get; set; }
		public virtual bool? EmailSend { get; set; }
		public virtual DateTime? EmailSendDate { get; set; }

		public virtual bool? FinanceSmsSend { get; set; }
		public virtual DateTime? FinanceSmsSendDate { get; set; }
		public virtual bool? FinanceEmailSend { get; set; }
		public virtual DateTime? FinanceEmailSendDate { get; set; }

		public virtual bool? JobActiveSmsSend { get; set; }
		public virtual DateTime? JobActiveSmsSendDate { get; set; }
		public virtual bool? JobActiveEmailSend { get; set; }
		public virtual DateTime? JobActiveEmailSendDate { get; set; }


		public virtual bool? GridConnectionSmsSend { get; set; }
		public virtual DateTime? GridConnectionSmsSendDate { get; set; }
		public virtual bool? GridConnectionEmailSend { get; set; }
		public virtual DateTime? GridConnectionEmailSendDate { get; set; }

		public virtual bool? StcSmsSend { get; set; }
		public virtual DateTime? StcSmsSendDate { get; set; }
		public virtual bool? StcEmailSend { get; set; }
		public virtual DateTime? StcEmailSendDate { get; set; }

		public virtual bool? JobGridSmsSend { get; set; }
		public virtual DateTime? JobGridSmsSendDate { get; set; }
		public virtual bool? JobGridEmailSend { get; set; }
		public virtual DateTime? JobGridEmailSendDate { get; set; }
		public virtual string JobCancelRequestReason { get; set; }

		public virtual bool? IsJobCancelRequest { get; set; }

		public virtual string JobCancelRejectReason { get; set; }
		public virtual int? BookingManagerId { get; set; }
		public virtual DateTime? JobAssignDate { get; set; }
		public virtual string ApprovalLetter_Filename { get; set; }
		public virtual string ApprovalLetter_FilePath { get; set; }

		public int? Applicationfeespaid { get; set; }

		public virtual bool? PendingInstallerSmsSend { get; set; }
		public virtual DateTime? PendingInstallerSmsSendDate { get; set; }
		public virtual bool? PendingInstallerEmailSend { get; set; }
		public virtual DateTime? PendingInstallerEmailSendDate { get; set; }

		public virtual int? RefferedJobStatusId { get; set; }



		public string AccountName { get; set; }
		public string BsbNo { get; set; }
		public string AccountNo { get; set; }
		public decimal? ReferralAmount { get; set; }
		public virtual int? RefferedJobId { get; set; }

		public virtual DateTime? ReferralPayDate { get; set; }
		public virtual bool ReferralPayment { get; set; }
		public string BankReferenceNo { get; set; }
		public string ReferralRemark { get; set; }



	}

}