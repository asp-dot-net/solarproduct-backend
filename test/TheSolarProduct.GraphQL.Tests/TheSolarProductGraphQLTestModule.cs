﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Castle.Windsor.MsDependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using TheSolarProduct.Configure;
using TheSolarProduct.Startup;
using TheSolarProduct.Test.Base;

namespace TheSolarProduct.GraphQL.Tests
{
    [DependsOn(
        typeof(TheSolarProductGraphQLModule),
        typeof(TheSolarProductTestBaseModule))]
    public class TheSolarProductGraphQLTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            IServiceCollection services = new ServiceCollection();
            
            services.AddAndConfigureGraphQL();

            WindsorRegistrationHelper.CreateServiceProvider(IocManager.IocContainer, services);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TheSolarProductGraphQLTestModule).GetAssembly());
        }
    }
}