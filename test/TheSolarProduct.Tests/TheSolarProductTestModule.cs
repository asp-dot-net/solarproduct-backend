﻿using Abp.Modules;
using TheSolarProduct.Test.Base;

namespace TheSolarProduct.Tests
{
    [DependsOn(typeof(TheSolarProductTestBaseModule))]
    public class TheSolarProductTestModule : AbpModule
    {
       
    }
}
