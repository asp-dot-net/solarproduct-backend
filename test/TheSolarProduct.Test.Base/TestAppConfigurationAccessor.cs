﻿using Abp.Dependency;
using Abp.Reflection.Extensions;
using Microsoft.Extensions.Configuration;
using TheSolarProduct.Configuration;

namespace TheSolarProduct.Test.Base
{
    public class TestAppConfigurationAccessor : IAppConfigurationAccessor, ISingletonDependency
    {
        public IConfigurationRoot Configuration { get; }

        public TestAppConfigurationAccessor()
        {
            Configuration = AppConfigurations.Get(
                typeof(TheSolarProductTestBaseModule).GetAssembly().GetDirectoryPathOrNull()
            );
        }
    }
}
