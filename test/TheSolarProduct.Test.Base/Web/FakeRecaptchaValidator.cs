﻿using System.Threading.Tasks;
using TheSolarProduct.Security.Recaptcha;

namespace TheSolarProduct.Test.Base.Web
{
    public class FakeRecaptchaValidator : IRecaptchaValidator
    {
        public Task ValidateAsync(string captchaResponse)
        {
            return Task.CompletedTask;
        }
    }
}
