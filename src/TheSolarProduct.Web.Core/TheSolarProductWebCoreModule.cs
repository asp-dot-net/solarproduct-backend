﻿using System;
using System.IO;
using System.Text;
using Abp.AspNetCore;
using Abp.AspNetCore.Configuration;
using Abp.AspNetCore.SignalR;
using Abp.AspNetZeroCore.Licensing;
using Abp.AspNetZeroCore.Web;
using Abp.Configuration.Startup;
using Abp.Dependency;
using Abp.EntityFrameworkCore;
using Abp.Hangfire;
using Abp.Hangfire.Configuration;
using Abp.IO;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Runtime.Caching.Redis;
using Abp.Threading;
using Abp.Zero.Configuration;
using Castle.Core.Internal;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Hangfire;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using TheSolarProduct.Chat;
using TheSolarProduct.Configuration;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Jobs;
using TheSolarProduct.Startup;
using TheSolarProduct.SysJobs;
using TheSolarProduct.Web.Authentication.JwtBearer;
using TheSolarProduct.Web.Authentication.TwoFactor;
using TheSolarProduct.Web.Chat.SignalR;
using TheSolarProduct.Web.Configuration;
using TheSolarProduct.Web.Controllers;
using TheSolarProduct.Web.DashboardCustomization;

namespace TheSolarProduct.Web
{
    [DependsOn(
        typeof(TheSolarProductApplicationModule),
        typeof(TheSolarProductEntityFrameworkCoreModule),
        typeof(AbpAspNetZeroCoreWebModule),
        typeof(AbpAspNetCoreSignalRModule),
        typeof(TheSolarProductGraphQLModule),
        typeof(AbpRedisCacheModule), //AbpRedisCacheModule dependency (and Abp.RedisCache nuget package) can be removed if not using Redis cache
        typeof(AbpHangfireAspNetCoreModule) //AbpHangfireModule dependency (and Abp.Hangfire.AspNetCore nuget package) can be removed if not using Hangfire
    )]
    public class TheSolarProductWebCoreModule : AbpModule
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public TheSolarProductWebCoreModule(IWebHostEnvironment env
            )
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            //Set default connection string
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
                TheSolarProductConsts.ConnectionStringName
            );

            //Use database for language management
            Configuration.Modules.Zero().LanguageManagement.EnableDbLocalization();

            Configuration.Modules.AbpAspNetCore()
                .CreateControllersForAppServices(
                    typeof(TheSolarProductApplicationModule).GetAssembly()
                );

            Configuration.Caching.Configure(TwoFactorCodeCacheItem.CacheName, cache =>
            {
                cache.DefaultAbsoluteExpireTime = TimeSpan.FromMinutes(2);
            });

            if (_appConfiguration["Authentication:JwtBearer:IsEnabled"] != null && bool.Parse(_appConfiguration["Authentication:JwtBearer:IsEnabled"]))
            {
                ConfigureTokenAuth();
            }
            
            IocManager.Register<DashboardViewConfiguration>();

            Configuration.ReplaceService<IAppConfigurationAccessor, AppConfigurationAccessor>();

            Configuration.ReplaceService<ISysJobsAppService, SysJobsAppService>();

            Configuration.ReplaceService<IAppConfigurationWriter, AppConfigurationWriter>();

            //Uncomment this line to use Hangfire instead of default background job manager (remember also to uncomment related lines in Startup.cs file(s)).
            Configuration.BackgroundJobs.UseHangfire();
            
            //Uncomment this line to use Redis cache instead of in-memory cache.
            //See app.config for Redis configuration and connection string
            //Configuration.Caching.UseRedis(options =>
            //{
            //    options.ConnectionString = _appConfiguration["Abp:RedisCache:ConnectionString"];
            //    options.DatabaseId = _appConfiguration.GetValue<int>("Abp:RedisCache:DatabaseId");
            //});
            Configuration.ReplaceService<ICancellationTokenProvider, NullCancellationTokenProvider>(DependencyLifeStyle.Transient);

        }

        private void ConfigureTokenAuth()
        {
            IocManager.Register<TokenAuthConfiguration>();
            var tokenAuthConfig = IocManager.Resolve<TokenAuthConfiguration>();

            tokenAuthConfig.SecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_appConfiguration["Authentication:JwtBearer:SecurityKey"]));
            tokenAuthConfig.Issuer = _appConfiguration["Authentication:JwtBearer:Issuer"];
            tokenAuthConfig.Audience = _appConfiguration["Authentication:JwtBearer:Audience"];
            tokenAuthConfig.SigningCredentials = new SigningCredentials(tokenAuthConfig.SecurityKey, SecurityAlgorithms.HmacSha256);
            tokenAuthConfig.AccessTokenExpiration = AppConsts.AccessTokenExpiration;
            tokenAuthConfig.RefreshTokenExpiration = AppConsts.RefreshTokenExpiration;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TheSolarProductWebCoreModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            SetAppFolders();
            
            IocManager.Resolve<ApplicationPartManager>().AddApplicationPartsIfNotAddedBefore(typeof(TheSolarProductWebCoreModule).Assembly);

            //RecurringJob.RemoveIfExists("LeadReminder");
            //RecurringJob.RemoveIfExists("GetGreenbotData");
            //RecurringJob.RemoveIfExists("UpdateGreenbotData");

            //var leadReminderService = IocManager.Resolve<IClientDomainService>();
            //RecurringJob.AddOrUpdate("LeadReminder", () => leadReminderService.NotifyLeadAsync(), "*/30 * * * *", null, "default");

            #region Uncomment after Testing 
            var australianEasternTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
            var sysJobsAppService = IocManager.Resolve<ISysJobsAppService>();
            RecurringJob.AddOrUpdate<ISysJobsAppService>("GetGreenbotData", _ => _.GetGreenbotDetails(), "0 0 0 * * *", australianEasternTimeZoneInfo, "default");

            RecurringJob.AddOrUpdate<ISysJobsAppService>("GetGreenDealJobInfo", _ => _.GetGreenDealJobInformation(), "0 0 1 * * *", australianEasternTimeZoneInfo, "default");

            RecurringJob.AddOrUpdate<ISysJobsAppService>("GetAllRECDetail", _ => _.GetAllRECDetails(australianEasternTimeZoneInfo), "0 0 2 * * *", australianEasternTimeZoneInfo, "default");
            
            RecurringJob.AddOrUpdate<ISysJobsAppService>("UpdateGreenbotData", _ => _.UpdateGreenbotDetails(), "0 40 2 * * *", australianEasternTimeZoneInfo, "default");

            RecurringJob.AddOrUpdate<ISysJobsAppService>("UpdateRECDetails", _ => _.UpdateRECDetails(), "0 20 3 * * *", australianEasternTimeZoneInfo, "default");

            RecurringJob.AddOrUpdate<ISysJobsAppService>("UpdateFacebookLeads", _ => _.UpdateFbLeads(), "*/15 * * * *", australianEasternTimeZoneInfo, "default");

            RecurringJob.AddOrUpdate<ISysJobsAppService>("AutoAssignLeads", _ => _.AutoAssignLeads(), "*/15 * * * *", australianEasternTimeZoneInfo, "default");

            RecurringJob.AddOrUpdate<ISysJobsAppService>("GetCimetDetails", _ => _.GetCimetDetails(), "0 0 4 * * *", australianEasternTimeZoneInfo, "default");
            #endregion

            //var FoolowUpReminderService = IocManager.Resolve<IClientDomainService>();
            //RecurringJob.AddOrUpdate("FollowUpReminder", () => FoolowUpReminderService.ReminderForFolloupAsync(), "*/15 * * * *", null, "default");
        }

        private void SetAppFolders()
        {
            var appFolders = IocManager.Resolve<AppFolders>();

            appFolders.SampleProfileImagesFolder = Path.Combine(_env.WebRootPath, $"Common{Path.DirectorySeparatorChar}Images{Path.DirectorySeparatorChar}SampleProfilePics");
            appFolders.WebLogsFolder = Path.Combine(_env.ContentRootPath, $"App_Data{Path.DirectorySeparatorChar}Logs");
            appFolders.Report = Path.Combine(_env.WebRootPath, $"Common{Path.DirectorySeparatorChar}Reports");
            
        }
    }
}
