﻿namespace TheSolarProduct.Web.Authentication.JwtBearer
{
    public enum TokenType
    {
        AccessToken,
        RefreshToken
    }
}
