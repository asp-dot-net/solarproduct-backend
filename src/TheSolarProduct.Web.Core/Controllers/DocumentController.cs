﻿using Abp.IO.Extensions;
using Abp.Runtime.Security;
using Microsoft.AspNetCore.Mvc;
using System.Web;
using System;
using TheSolarProduct.Common;
using Abp.Domain.Uow;
using Abp.Notifications;
using NPOI.HPSF;
using System.Collections;
using TheSolarProduct.Quotations;
using System.Windows.Forms;
using Abp.Domain.Repositories;
using System.Linq;
using System.Linq.Dynamic.Core;
using TheSolarProduct.DocumentRequests;
using TheSolarProduct.Notifications;
using TheSolarProduct.Authorization.Users;
using System.IO;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Abp.Web.Models;
using Abp.UI;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Jobs;
using TheSolarProduct.Migrations;
using TheSolarProduct.Businesses;
using TheSolarProduct.MultiTenancy;
using Microsoft.VisualBasic;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.ECommerces.Client.Dto;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct.PurchaseDocumentListes;
using TheSolarProduct.StockOrders;
using TheSolarProduct.QuickStocks;
using TheSolarProduct.Common.Dto;
using Abp.Organizations;
using Abp.Runtime.Session;
using TheSolarProduct.StockPayments;
using TheSolarProduct.Jobs.Dtos;

namespace TheSolarProduct.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    public class DocumentController : TheSolarProductControllerBase
    {
        private readonly ICommonLookupAppService _commonDocumentSaveRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<DocumentRequests.DocumentRequest> _documentRequestRepository;
        private readonly IRepository<PurchaseOrderDocument> _purchasedocumentRepository;
        private readonly IRepository<DocumentRequests.DocumentRequestLinkHistory> _documentRequestLinkRepository;
        private readonly IRepository<Document> _documentRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Job, int> _jobRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<Business> _businessRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<STCRegister> _STCRegisterImageRepository;
        private readonly IRepository<QuickStockActivityLog> _quickStockActivityLogRepository;
        private readonly IRepository<PurchaseOrder> _purchaseOrderRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<PurchaseDocumentList> _purchaseDocumentListRepository;
        private readonly IRepository<StockPayment> _stockPaymentRepository;
        private readonly IRepository<JobReview> _jobReviewRepository;
        private readonly IRepository<Job, int> _lookup_JobRepository;
        public DocumentController(ICommonLookupAppService commonDocumentSaveRepository,
            IRepository<PurchaseDocumentList> purchaseDocumentListRepository,
            IRepository<QuickStockActivityLog> quickStockActivityLogRepository, 
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<PurchaseOrderDocument> purchasedocumentRepository,
            IRepository<DocumentRequests.DocumentRequest> documentRequestRepository,
            IRepository<Document> documentRepository,
            IRepository<DocumentRequestLinkHistory> documentRequestLinkRepository,
            IAppNotifier appNotifier,
            IRepository<User, long> userRepository,
            IRepository<Job, int> jobRepository,
            IRepository<LeadActivityLog> leadactivityRepository,
            IRepository<Business> businessRepository,
            IRepository<Tenant> tenantRepository, IRepository<STCRegister> STCRegisterImageRepository,
            IRepository<PurchaseOrder> purchaseOrderRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRepository<StockPayment> stockPaymentRepository,
            IRepository<JobReview> jobReviewRepository,
            IRepository<Job, int> lookup_JobRepository

            )
        {
            _commonDocumentSaveRepository = commonDocumentSaveRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _documentRequestRepository = documentRequestRepository;
            _documentRepository = documentRepository;
            _documentRequestLinkRepository = documentRequestLinkRepository;
            _appNotifier = appNotifier;
            _userRepository = userRepository;
            _jobRepository = jobRepository;
            _leadactivityRepository = leadactivityRepository;
            _businessRepository = businessRepository;
            _tenantRepository = tenantRepository;
            _STCRegisterImageRepository = STCRegisterImageRepository;
            _purchasedocumentRepository = purchasedocumentRepository;
            _quickStockActivityLogRepository = quickStockActivityLogRepository;
            _purchaseOrderRepository = purchaseOrderRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _purchaseDocumentListRepository = purchaseDocumentListRepository;
            _stockPaymentRepository = stockPaymentRepository;
            _jobReviewRepository = jobReviewRepository; 
            _lookup_JobRepository = lookup_JobRepository;
        }
       
        [HttpPost]
        public async Task<JsonResult> UploadDocuments(string STR)
        {
            try
            {
                if (!string.IsNullOrEmpty(STR))
                {
                    var Ids = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(STR, AppConsts.DefaultPassPhrase));

                    var Id = Ids.Split(",");
                    int TenantId = Convert.ToInt32(Id[0]);
                    int JobId = Convert.ToInt32(Id[1]);
                    int DocRequestId = Convert.ToInt32(Id[2]);

                    var files = Request.Form.Files;

                    using (var uow = _unitOfWorkManager.Begin())
                    {
                        using (_unitOfWorkManager.Current.SetTenantId(TenantId))
                        {
                            var documentRequest = _documentRequestRepository.GetAll().Include(e => e.DocTypeFk).Include(e => e.JobFk).Where(e => e.Id == DocRequestId).FirstOrDefault();

                            foreach (var file in files)
                            {
                                byte[] fileBytes;
                                using (var stream = file.OpenReadStream())
                                {
                                    fileBytes = stream.GetAllBytes();
                                }

                                FileInfo fi = new FileInfo(file.FileName);
                                var fileName = DateTime.UtcNow.Ticks + "_" + file.Name + fi.Extension;

                                var filepath = _commonDocumentSaveRepository.SaveCommonDocument(fileBytes, fileName, "Documents", JobId, 0, AbpSession.TenantId);

                                var document = new Document()
                                {
                                    DocumentTypeId = documentRequest.DocTypeId,
                                    JobId = JobId,
                                    FileName = filepath.filename,
                                    FileType = file.ContentType,
                                    FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\" + filepath.JobNumber + "\\Documents\\",
                                    CreatorUserId = 1
                                };
                                await _documentRepository.InsertAsync(document);
                            }

                            documentRequest.IsSubmitted = true;
                            await _documentRequestRepository.UpdateAsync(documentRequest);
                            var Link = _documentRequestLinkRepository.GetAll().Where(e => e.DocumentRequestId == DocRequestId);
                            foreach (var item in Link)
                            {
                                item.Expired = true;
                                await _documentRequestLinkRepository.UpdateAsync(item);
                            }

                            var AssignedUser = _userRepository.GetAll().Where(e => e.Id == documentRequest.CreatorUserId).FirstOrDefault();
                            string msg = string.Format(documentRequest.DocTypeFk.Title + " Received For {0} Job.", documentRequest.JobFk.JobNumber);
                            await _appNotifier.LeadAssiged(AssignedUser, msg, NotificationSeverity.Info);

                            uow.Complete();
                            LeadActivityLog leadactivity = new LeadActivityLog();

                            leadactivity.ActionId = 42;
                            var job = _jobRepository.GetAll().Where(e => e.Id == documentRequest.JobId).FirstOrDefault();
                            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                            leadactivity.CreatorUserId = 1;
                            leadactivity.TenantId = TenantId;
                            leadactivity.ActionNote = files.Count + " Documents Received For " + documentRequest.DocTypeFk.Title;

                            await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);


                            return Json(new AjaxResponse(new { success = true }));
                        }
                    }
                }
                else
                {
                    return Json(new AjaxResponse(new { success = false }));
                }

            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [HttpPost]
        public async Task<JsonResult> UploadLicencePassportDoc(string STR)
        {
            try
            {
                if (!string.IsNullOrEmpty(STR))
                {
                    // var Ids = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(STR, AppConsts.DefaultPassPhrase));

                    var Id = STR.Split(",");
                    int TenantId = Convert.ToInt32(Id[0]);
                    int CreditId = Convert.ToInt32(Id[1]);

                    var file = Request.Form.Files[0];

                    using (var uow = _unitOfWorkManager.Begin())
                    {
                        using (_unitOfWorkManager.Current.SetTenantId(TenantId))
                        {
                            byte[] fileBytes;
                            using (var stream = file.OpenReadStream())
                            {
                                fileBytes = stream.GetAllBytes();
                            }

                            FileInfo fi = new FileInfo(file.FileName);

                            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == TenantId).Select(e => e.TenancyName).FirstOrDefault();
                            var filename = DateTime.Now.Ticks + "_" + file.FileName.Replace(" ", "");

                            var MainFolder = ApplicationSettingConsts.DocumentPath; //From Const
                            MainFolder = MainFolder + "\\Documents";
                            var TenantPath = MainFolder + "\\" + TenantName;
                            var CreditApplicationPath = TenantPath + "\\" + "CreditApplication";
                            var FolderPath = CreditApplicationPath + "\\" + "LicencePassportDoc";
                            var FinalFilePath = FolderPath + "\\" + filename;

                            if (System.IO.Directory.Exists(MainFolder))
                            {
                                if (System.IO.Directory.Exists(TenantPath))
                                {
                                    if (System.IO.Directory.Exists(CreditApplicationPath))
                                    {
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        System.IO.Directory.CreateDirectory(CreditApplicationPath);
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(TenantPath);
                                    if (System.IO.Directory.Exists(CreditApplicationPath))
                                    {
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        System.IO.Directory.CreateDirectory(CreditApplicationPath);
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(MainFolder);
                                if (System.IO.Directory.Exists(TenantPath))
                                {
                                    if (System.IO.Directory.Exists(CreditApplicationPath))
                                    {
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        System.IO.Directory.CreateDirectory(CreditApplicationPath);
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(TenantPath);
                                    if (System.IO.Directory.Exists(CreditApplicationPath))
                                    {
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        System.IO.Directory.CreateDirectory(CreditApplicationPath);
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                }
                            }

                            var creditApplication = await _businessRepository.GetAsync(CreditId);
                            creditApplication.LicencePassportDoc = "\\Documents\\" + TenantName + "\\" + "CreditApplication" + "\\LicencePassportDoc" + "\\" + filename;

                            await _businessRepository.UpdateAsync(creditApplication);
                            uow.Complete();

                            return Json(new AjaxResponse(new { success = true, filePath = creditApplication.LicencePassportDoc }));

                        }
                    }
                }
                else
                {
                    return Json(new AjaxResponse(new { success = false }));

                }

            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));

            }
        }

        [HttpPost]
        public async Task<JsonResult> UploadAddressDoc(string STR)
        {
            try
            {
                if (!string.IsNullOrEmpty(STR))
                {

                    var Id = STR.Split(",");
                    int TenantId = Convert.ToInt32(Id[0]);
                    int CreditId = Convert.ToInt32(Id[1]);

                    var file = Request.Form.Files[0];

                    using (var uow = _unitOfWorkManager.Begin())
                    {
                        using (_unitOfWorkManager.Current.SetTenantId(TenantId))
                        {
                            byte[] fileBytes;
                            using (var stream = file.OpenReadStream())
                            {
                                fileBytes = stream.GetAllBytes();
                            }

                            FileInfo fi = new FileInfo(file.FileName);

                            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == TenantId).Select(e => e.TenancyName).FirstOrDefault();
                            var filename = DateTime.Now.Ticks + "_" + file.FileName.Replace(" ", "");
                            var Path = ApplicationSettingConsts.DocumentPath;

                            var MainFolder = ApplicationSettingConsts.DocumentPath; //From Const
                            MainFolder = MainFolder + "\\Documents";
                            var TenantPath = MainFolder + "\\" + TenantName;
                            var CreditApplicationPath = TenantPath + "\\" + "CreditApplication";
                            var FolderPath = CreditApplicationPath + "\\" + "AddressDoc";
                            var FinalFilePath = FolderPath + "\\" + filename;

                            if (System.IO.Directory.Exists(MainFolder))
                            {
                                if (System.IO.Directory.Exists(TenantPath))
                                {
                                    if (System.IO.Directory.Exists(CreditApplicationPath))
                                    {
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        System.IO.Directory.CreateDirectory(CreditApplicationPath);
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(TenantPath);
                                    if (System.IO.Directory.Exists(CreditApplicationPath))
                                    {
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        System.IO.Directory.CreateDirectory(CreditApplicationPath);
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(MainFolder);
                                if (System.IO.Directory.Exists(TenantPath))
                                {
                                    if (System.IO.Directory.Exists(CreditApplicationPath))
                                    {
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        System.IO.Directory.CreateDirectory(CreditApplicationPath);
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(TenantPath);
                                    if (System.IO.Directory.Exists(CreditApplicationPath))
                                    {
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        System.IO.Directory.CreateDirectory(CreditApplicationPath);
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                }
                            }

                            var creditApplication = await _businessRepository.GetAsync(CreditId);
                            creditApplication.AddressDoc = "\\Documents\\" + TenantName + "\\" + "CreditApplication" + "\\AddressDoc" + "\\" + filename;

                            await _businessRepository.UpdateAsync(creditApplication);

                            uow.Complete();

                            return Json(new AjaxResponse(new { success = true, filePath = creditApplication.AddressDoc }));

                        }
                    }
                }
                else
                {
                    return Json(new AjaxResponse(new { success = false }));

                }

            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));

            }
        }


        [HttpPost]
        public async Task<JsonResult> AddtoStcRegister([FromForm] STCRegisterDto sTCRegister)
        {
            try
            {
                var files = Request.Form.Files;

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (_unitOfWorkManager.Current.SetTenantId(sTCRegister.TenantId))
                    {
                        foreach (var file in files)
                        {
                            byte[] fileBytes;
                            using (var stream = file.OpenReadStream())
                            {
                                fileBytes = stream.GetAllBytes();
                            }

                            FileInfo fi = new FileInfo(file.FileName);
                            var fileName = DateTime.UtcNow.Ticks + "_" + sTCRegister.TradingName.Trim() + fi.Extension;
                            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == sTCRegister.TenantId).Select(e => e.TenancyName).FirstOrDefault();

                            var MainFolder = ApplicationSettingConsts.DocumentPath; //From Const
                            MainFolder = MainFolder + "\\Documents";
                            var TenantPath = MainFolder + "\\" + TenantName;
                            var CreditApplicationPath = TenantPath + "\\" + "STCRegister";
                            var FolderPath = CreditApplicationPath + "\\" + file.Name;
                            var FinalFilePath = FolderPath + "\\" + fileName;

                            if (System.IO.Directory.Exists(MainFolder))
                            {
                                if (System.IO.Directory.Exists(TenantPath))
                                {
                                    if (System.IO.Directory.Exists(CreditApplicationPath))
                                    {
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        System.IO.Directory.CreateDirectory(CreditApplicationPath);
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(TenantPath);
                                    if (System.IO.Directory.Exists(CreditApplicationPath))
                                    {
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        System.IO.Directory.CreateDirectory(CreditApplicationPath);
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(MainFolder);
                                if (System.IO.Directory.Exists(TenantPath))
                                {
                                    if (System.IO.Directory.Exists(CreditApplicationPath))
                                    {
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        System.IO.Directory.CreateDirectory(CreditApplicationPath);
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(TenantPath);
                                    if (System.IO.Directory.Exists(CreditApplicationPath))
                                    {
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        System.IO.Directory.CreateDirectory(CreditApplicationPath);
                                        if (System.IO.Directory.Exists(FolderPath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(FolderPath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                }
                            }

                            if(file.Name == "PublicLiabilityCertificate")
                            {
                                sTCRegister.PublicLiabilityCertificate = "\\Documents\\" + TenantName + "\\" + "STCRegister" + "\\PublicLiabilityCertificate" + "\\" + fileName;
                            }
                            else if (file.Name == "ProductLiabilityCertificate")
                            {
                                sTCRegister.ProductLiabilityCertificate = "\\Documents\\" + TenantName + "\\" + "STCRegister" + "\\ProductLiabilityCertificate" + "\\" + fileName;
                            }
                            else if (file.Name == "PhotoId")
                            {
                                sTCRegister.PhotoId = "\\Documents\\" + TenantName + "\\" + "STCRegister" + "\\PhotoId" + "\\" + fileName;
                            }
                            else if (file.Name == "ASICCertificate")
                            {
                                sTCRegister.ASICCertificate = "\\Documents\\" + TenantName + "\\" + "STCRegister" + "\\ASICCertificate" + "\\" + fileName;
                            }


                        }

                        var request = ObjectMapper.Map<STCRegister>(sTCRegister);

                        await _STCRegisterImageRepository.InsertAsync(request);

                        uow.Complete();

                        return Json(new AjaxResponse(new { success = true, filePath = "" }));

                    }
                }


            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));

            }
        }
        [HttpPost]
        public async Task<JsonResult> UploadPurchaseOrderDocuments(string STR)
        {
            try
            {
                if (!string.IsNullOrEmpty(STR))
                {
                    //var Ids = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(STR, AppConsts.DefaultPassPhrase));

                    var Id = STR.Split(",");
                    int TenantId = Convert.ToInt32(Id[0]);
                    int PurhaseOrderId = Convert.ToInt32(Id[1]);
                    int DocumentId = Convert.ToInt32(Id[2]);
                    int OrgId = Convert.ToInt32(Id[3]);
                    int UserId = Convert.ToInt32(Id[4]);
                    string DocName = Id[5];
                    var files = Request.Form.Files;

                    using (var uow = _unitOfWorkManager.Begin())
                    {
                        using (_unitOfWorkManager.Current.SetTenantId(TenantId))
                        {
                            var documentRequest = _purchaseDocumentListRepository.GetAll().Where(e => e.Id == DocumentId).FirstOrDefault();

                            foreach (var file in files)
                            {
                                byte[] fileBytes;
                                using (var stream = file.OpenReadStream())
                                {
                                    fileBytes = stream.GetAllBytes();
                                }
                                var TenantName = _tenantRepository.GetAll().Where(e => e.Id == TenantId).Select(e => e.TenancyName.Replace(" ", "")).FirstOrDefault();
                                var OrgName = _organizationUnitRepository.GetAll().Where(e => e.Id == OrgId).Select(e => e.DisplayName.Replace(" ", "")).FirstOrDefault();
                                var MainFolder = ApplicationSettingConsts.DocumentPath;
                                var OrderNO = _purchaseOrderRepository.GetAll().Where(e => e.Id == PurhaseOrderId).Select(e => e.OrderNo).FirstOrDefault();
                                FileInfo fi = new FileInfo(file.FileName);
                                var fileName = DateTime.UtcNow.Ticks + "_" + DocName.Replace(" ","") + fi.Extension;
                                var fileepath = MainFolder + "/" + TenantName + "/" + OrgName + "/" + "Inventory" + "/"  + "StockOrder" + "/" + OrderNO ; 
                                var filepath = SavePurchaseOrderCommonDocument(fileBytes, fileepath, fileName);

                                var document = new PurchaseOrderDocument()
                                {
                                    PurchaseDocumentId = documentRequest.Id,
                                    PurchaseOrderId = PurhaseOrderId,
                                    FileName = fileName,
                                    FilePath = "/" + TenantName + "/" + OrgName + "/" + "Inventory" + "/" + "StockOrder" + "/" + OrderNO,
                                    CreatorUserId = UserId,
                                    DocumentName = DocName

                                };
                                await _purchasedocumentRepository.InsertAsync(document);
                            }


                            var purchaseOrder = _purchaseOrderRepository.GetAll().Where( e => e.Id == PurhaseOrderId).Select(e=>e.OrderNo).FirstOrDefault();
                            var AssignedUser = _userRepository.GetAll().Where(e => e.Id == documentRequest.CreatorUserId).FirstOrDefault();
                            string msg = string.Format(documentRequest.Name + " Received For {0} OrderNo.", purchaseOrder);
                            await _appNotifier.LeadAssiged(AssignedUser, msg, NotificationSeverity.Info);

                            uow.Complete();
                            QuickStockActivityLog activity = new QuickStockActivityLog();

                            activity.ActionId = 84;
                            activity.Action = "Document Uploaded";
                            activity.PurchaseOrderId = Convert.ToInt32(PurhaseOrderId);
                            activity.SectionId = 1;
                            activity.TenantId = TenantId;
                            activity.Type = "Purchase Order";
                            activity.ActionNote = "Documents Received For : " + documentRequest.Name;
                            activity.CreatorUserId = UserId;
                            await _quickStockActivityLogRepository.InsertAndGetIdAsync(activity);


                            return Json(new AjaxResponse(new { success = true }));
                        }
                    }
                }
                else
                {
                    return Json(new AjaxResponse(new { success = false }));
                }

            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
        private PurchaseOrderDoucmentReturnDto SavePurchaseOrderCommonDocument(byte[]? ByteArray, string filepath,string filename)
        {
            if (ByteArray == null)
            {
                throw new UserFriendlyException("There is no such file");
            }

            if (System.IO.Directory.Exists(filepath))
            {
                using (MemoryStream mStream = new MemoryStream(ByteArray))
                {
                    System.IO.File.WriteAllBytes(string.Format("{0}", filepath+"/"+filename), ByteArray);
                }
            }
            else
            {
                System.IO.Directory.CreateDirectory(filepath);
                using (MemoryStream mStream = new MemoryStream(ByteArray))
                {
                    System.IO.File.WriteAllBytes(string.Format("{0}", filepath+ "/"+ filename), ByteArray);
                }
            }

            var doc = new PurchaseOrderDoucmentReturnDto();
            doc.FinalFilePath = filepath;

            return doc;
        }

        [HttpPost]
        public async Task<JsonResult> UploadPaymentRecipt(string STR)
        {
            try
            {
                if (!string.IsNullOrEmpty(STR))
                {

                    var Id = STR.Split(",");
                    int TenantId = Convert.ToInt32(Id[0]);
                    int StockPaymentId = Convert.ToInt32(Id[1]);
                    int OrgId = Convert.ToInt32(Id[2]);
                    int InvoiceNO = Convert.ToInt32(Id[3]);
                    var file = Request.Form.Files[0];

                    using (var uow = _unitOfWorkManager.Begin())
                    {
                        using (_unitOfWorkManager.Current.SetTenantId(TenantId))
                        {
                            byte[] fileBytes;
                            using (var stream = file.OpenReadStream())
                            {
                                fileBytes = stream.GetAllBytes();
                            }


                            FileInfo fi = new FileInfo(file.FileName);
                            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == TenantId).Select(e => e.TenancyName).FirstOrDefault();
                            var fileName = DateTime.UtcNow.Ticks + "_" + file.FileName.Replace(" ", "");
                            //var fileName = DateTime.UtcNow.Ticks + "_" + file.FileName.Replace(" ", "") + fi.Extension;
                            var MainFolder = ApplicationSettingConsts.DocumentPath;
                            
                            var OrgName = _organizationUnitRepository.GetAll().Where(e => e.Id == OrgId).Select(e => e.DisplayName.Replace(" ", "")).FirstOrDefault();
                            var fileepath = MainFolder + "/" + TenantName + "/" + OrgName + "/" + "Inventory" + "/" + "StockPayment" + "/" + InvoiceNO;
                            var filepath = SavePaymentReciptDocument(fileBytes, fileepath, fileName);

                            //var stockPayment = await _stockPaymentRepository.GetAsync(InvoiceNO);
                            //stockPayment.FilePath =  "/" + TenantName + "/" + OrgName + "/" + "Inventory" + "/" + "StockPayment" + "/" + InvoiceNO;
                            //stockPayment.FileName= fileName;
                            //await _stockPaymentRepository.UpdateAsync(stockPayment);
                            uow.Complete();

                            return Json(new AjaxResponse(new
                            {
                                success = true,
                                filePath = "/" + TenantName + "/" + OrgName + "/" + "Inventory" + "/" + "StockPayment" + "/" + InvoiceNO,
                                fileName = fileName
                            }));

                        }
                    }
                }
                else
                {
                    return Json(new AjaxResponse(new { success = false }));

                }

            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));

            }
        }

        private StockPaymentReciptDocumentReturnDto SavePaymentReciptDocument(byte[]? ByteArray, string filepath, string filename)
        {
            if (ByteArray == null)
            {
                throw new UserFriendlyException("There is no such file");
            }

            if (System.IO.Directory.Exists(filepath))
            {
                using (MemoryStream mStream = new MemoryStream(ByteArray))
                {
                    System.IO.File.WriteAllBytes(string.Format("{0}", filepath + "/" + filename), ByteArray);
                }
            }
            else
            {
                System.IO.Directory.CreateDirectory(filepath);
                using (MemoryStream mStream = new MemoryStream(ByteArray))
                {
                    System.IO.File.WriteAllBytes(string.Format("{0}", filepath + "/" + filename), ByteArray);
                }
            }

            var doc = new StockPaymentReciptDocumentReturnDto();
            doc.FinalFilePath = filepath;

            return doc;
        }
        [HttpPost]
        public async Task<JsonResult> UploadReviewNoteDocuments(string STR)
        {
            try
            {
                if (!string.IsNullOrEmpty(STR))
                {
                    //var Ids = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(STR, AppConsts.DefaultPassPhrase));

                    var Id = STR.Split(",");
                  
                    int TenantId = Convert.ToInt32(Id[0]);
                    int JobId = Convert.ToInt32(Id[1]);
                    int reviewTypeId = Convert.ToInt32(Id[2]);
                    int rating = Convert.ToInt32(Id[3]);
                    string reviewNotes = (Id[4]);
                    int OrgId = Convert.ToInt32(Id[5]);
                    int createId = Convert.ToInt32(Id[6]);
                    var files = Request.Form.Files;

                    using (var uow = _unitOfWorkManager.Begin())
                    {
                        using (_unitOfWorkManager.Current.SetTenantId(TenantId))
                        {
                            

                            foreach (var file in files)
                            {
                                byte[] fileBytes;
                                using (var stream = file.OpenReadStream())
                                {
                                    fileBytes = stream.GetAllBytes();
                                }
                                var TenantName = _tenantRepository.GetAll().Where(e => e.Id == TenantId).Select(e => e.TenancyName.Replace(" ", "")).FirstOrDefault();
                                var OrgName = _organizationUnitRepository.GetAll().Where(e => e.Id == OrgId).Select(e => e.DisplayName.Replace(" ", "")).FirstOrDefault();
                                var MainFolder = ApplicationSettingConsts.DocumentPath;
                               
                                FileInfo fi = new FileInfo(file.FileName);
                                //var fileName = DateTime.UtcNow.Ticks + "_" + reviewNotes.Trim() + fi.Extension;
                                var fileName = DateTime.UtcNow.Ticks + "_" + reviewNotes.Replace(" ", "") + fi.Extension;
                                var fileepath = MainFolder + "/" + TenantName + "/" + OrgName + "/" + "Retail" + "/" + "Review" + "/" + JobId;
                                var filepath = SavePurchaseOrderCommonDocument(fileBytes, fileepath, fileName);

                                var document = new JobReview()
                                {
                                    JobId = JobId,
                                    ReviewTypeId =reviewTypeId,
                                    ReviewNotes=reviewNotes,
                                    Rating=rating,
                                    FileName = fileName,
                                    FilePath = "/" + TenantName + "/" + OrgName + "/" + "Retail" + "/" + "Review" + "/" + JobId,
                                   CreatorUserId= createId,

                                };
                                await _jobReviewRepository.InsertAsync(document);
                            }


                            uow.Complete();
                            //var AssignedUser = _userRepository.GetAll().Where(e => e.Id == documentRequest.CreatorUserId).FirstOrDefault();
                            //string msg = string.Format(documentRequest.Name + " Received For {0} OrderNo.", purchaseOrder);
                            //await _appNotifier.LeadAssiged(AssignedUser, msg, NotificationSeverity.Info);
                            var job = await _lookup_JobRepository.FirstOrDefaultAsync((int)JobId);
                            
                            LeadActivityLog leadactivity = new LeadActivityLog();

                            leadactivity.ActionId = 28;
                            leadactivity.SectionId = 23;
                            leadactivity.ActionNote = "Review Added";
                            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                            if (AbpSession.TenantId != null)
                            {
                                leadactivity.TenantId = (int)AbpSession.TenantId;
                            }
                            var jobactionid = _leadactivityRepository.InsertAndGetId(leadactivity);


                            return Json(new AjaxResponse(new { success = true }));
                        }
                    }
                }
                else
                {
                    return Json(new AjaxResponse(new { success = false }));
                }

            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
    
        [HttpPost]
        public async Task<JsonResult> UploadImages()
        {
            try
            {
                
               var files = Request.Form.Files;
               return Json(new AjaxResponse(new { success = true }));
              

            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

    }
}
