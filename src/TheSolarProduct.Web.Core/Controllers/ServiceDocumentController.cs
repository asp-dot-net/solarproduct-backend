﻿using Abp.Domain.Uow;
using Abp.IO.Extensions;
using Abp.Notifications;
using Abp.Runtime.Security;
using Abp.UI;
using Abp.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System;
using TheSolarProduct.LeadActivityLogs;
using Abp.Domain.Repositories;
using TheSolarProduct.ServiceDocumentRequests;
using System.Data.Entity;
using System.Linq;
using TheSolarProduct.Common;
using TheSolarProduct.ServiceDocs;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Notifications;
using TheSolarProduct.Jobs;
using TheSolarProduct.Services;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.Common.Dto;

namespace TheSolarProduct.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ServiceDocumentController : TheSolarProductControllerBase
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<ServiceDocumentRequest> _serviceDocumentRequestRepository;
        private readonly ICommonLookupAppService _commonDocumentSaveRepository;
        private readonly IRepository<ServiceDoc> _serviceDocumentRepository;
        private readonly IRepository<ServiceDocumentRequestLinkHistory> _serviceDocumentRequestLinkHistory;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Job, int> _jobRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<ServiceDocumentType> _serviceDocumentTypeRepository;
        private readonly IRepository<Service> _serviceRepository;

        public ServiceDocumentController(
             IUnitOfWorkManager unitOfWorkManager
            , IRepository<ServiceDocumentRequest> serviceDocumentRequestRepository
            , ICommonLookupAppService commonDocumentSaveRepository
            , IRepository<ServiceDoc> serviceDocumentRepository
            , IRepository<ServiceDocumentRequestLinkHistory> serviceDocumentRequestLinkHistory
            , IAppNotifier appNotifier
            , IRepository<User, long> userRepository
            , IRepository<Job, int> jobRepository
            , IRepository<LeadActivityLog> leadactivityRepository
            , IRepository<ServiceDocumentType> serviceDocumentTypeRepository
            , IRepository<Service> serviceRepository

            )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _serviceDocumentRequestRepository = serviceDocumentRequestRepository;
            _commonDocumentSaveRepository = commonDocumentSaveRepository;
            _serviceDocumentRequestLinkHistory = serviceDocumentRequestLinkHistory;
            _appNotifier = appNotifier;
            _userRepository = userRepository;
            _jobRepository = jobRepository;
            _leadactivityRepository = leadactivityRepository;
            _serviceDocumentRepository = serviceDocumentRepository;
            _serviceDocumentTypeRepository = serviceDocumentTypeRepository;
            _serviceRepository = serviceRepository;
        }

        [HttpPost]
        public async Task<JsonResult> UploadDocuments(string STR)
        {
            try
            {
                if (!string.IsNullOrEmpty(STR))
                {
                    var Ids = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(STR, AppConsts.DefaultPassPhrase));

                    var Id = Ids.Split(",");
                    int TenantId = Convert.ToInt32(Id[0]);
                    int JobId = Convert.ToInt32(Id[1]);
                    int serviceId = Convert.ToInt32(Id[2]);
                    int DocRequestId = Convert.ToInt32(Id[3]);

                    var files = Request.Form.Files;
                    var jobnumber = "";
                    using (var uow = _unitOfWorkManager.Begin())
                    {
                        using (_unitOfWorkManager.Current.SetTenantId(TenantId))
                        {
                            var documentRequest = _serviceDocumentRequestRepository.GetAll().Include(e => e.ServiceDocTypeFk).Include(e => e.JobFk).Where(e => e.Id == DocRequestId).FirstOrDefault();
                            var type = _serviceDocumentTypeRepository.GetAll().Where(e => e.Id == documentRequest.ServiceDocTypeId).Select(e => e.Title).FirstOrDefault();
                            foreach (var file in files)
                            {
                                byte[] fileBytes;
                                using (var stream = file.OpenReadStream())
                                {
                                    fileBytes = stream.GetAllBytes();
                                }

                                FileInfo fi = new FileInfo(file.FileName);
                                var fileName = DateTime.UtcNow.Ticks + "_" + file.Name + fi.Extension;

                                var filepath = _commonDocumentSaveRepository.SaveCommonDocument(fileBytes, fileName, "ServiceDocuments", JobId, 0, AbpSession.TenantId);

                                var document = new ServiceDoc()
                                {
                                    ServiceDocumentTypeId = documentRequest.ServiceDocTypeId,
                                    ServiceId = serviceId,
                                    FileName = filepath.filename,
                                    FileType = file.ContentType,
                                    FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\" + filepath.JobNumber + "\\ServiceDocuments\\",
                                    CreatorUserId = 1
                                };
                                await _serviceDocumentRepository.InsertAsync(document);
                                jobnumber = filepath.JobNumber;
                            }

                            documentRequest.IsSubmitted = true;
                            await _serviceDocumentRequestRepository.UpdateAsync(documentRequest);
                            var Link = _serviceDocumentRequestLinkHistory.GetAll().Where(e => e.ServiceDocumentRequestId == DocRequestId);
                            foreach (var item in Link)
                            {
                                item.Expired = true;
                                await _serviceDocumentRequestLinkHistory.UpdateAsync(item);
                            }

                            var AssignedUser = _userRepository.GetAll().Where(e => e.Id == documentRequest.CreatorUserId).FirstOrDefault();
                            string msg = string.Format(type + " Received For {0} Job.", jobnumber);
                            //await _appNotifier.LeadAssiged(AssignedUser, msg, NotificationSeverity.Info);

                            uow.Complete();
                            LeadActivityLog leadactivity = new LeadActivityLog();

                            leadactivity.ActionId = 42;
                            var job = _jobRepository.GetAll().Where(e => e.Id == documentRequest.JobId).FirstOrDefault();
                            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                            leadactivity.CreatorUserId = 1;
                            leadactivity.TenantId = TenantId;
                            leadactivity.SectionId = Convert.ToInt32(Id[4]);
                            leadactivity.ActionNote = files.Count + "Service Documents Received For " + type;

                            await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);


                            return Json(new AjaxResponse(new { success = true }));
                        }
                    }
                }
                else
                {
                    return Json(new AjaxResponse(new { success = false }));
                }

            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [HttpPost]
        public async Task<JsonResult> AttachFiletoMail(string serviceId, int tenentId)
        {
            try
            {
                int jobID = 0;
                var filepath = new DocumentReturnValuesDto();
                var MainFolder = ApplicationSettingConsts.DocumentPath; //From Const

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (_unitOfWorkManager.Current.SetTenantId(tenentId))
                    { 
                        jobID = (int)_serviceRepository.GetAll().Where(e => e.Id == Convert.ToInt32(serviceId)).Select(e => e.JobId).FirstOrDefault();
                        var files = Request.Form.Files;
                        byte[] fileBytes;
                        using (var stream = files[0].OpenReadStream())
                        {
                            fileBytes = stream.GetAllBytes();
                        }
                        FileInfo fi = new FileInfo(files[0].FileName);

                        var fileName = DateTime.UtcNow.Ticks + "_" + files[0].Name + fi.Extension;

                        filepath = _commonDocumentSaveRepository.SaveCommonDocument(fileBytes, fileName, "ServiceTempDoc", jobID, 0, AbpSession.TenantId);

                        uow.Complete();

                    }
                }
                     
                return Json(new AjaxResponse(new { fileName = filepath.filename, filepath = MainFolder + "\\Documents" + "\\" + filepath.TenantName + "\\" + filepath.JobNumber + "\\ServiceTempDoc\\" }));

            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
    }
}
