using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.IO.Extensions;
using Abp.Notifications;
using Abp.UI;
using Abp.Web.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using TheSolarProduct.DataVaults;
using TheSolarProduct.DemoUiComponents.Dto;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Jobs;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.Quotations;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Web.Controllers
{
   // [AbpMvcAuthorize]
    public class DemoUiComponentsController : TheSolarProductControllerBase
    {
        private const int MaxFileBytes = 5242880; //5MB
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<ProductItem> _productItemRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IRepository<DocumentLibrary> _documentlibraryRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public DemoUiComponentsController(IBinaryObjectManager binaryObjectManager
            , IWebHostEnvironment env
            , IRepository<ProductItem> productItemRepository
            , IRepository<Tenant> tenantRepository
            , ITempFileCacheManager tempFileCacheManager,
            IRepository<DocumentLibrary> documentlibraryRepository
            , IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider, IUnitOfWorkManager unitOfWorkManager)
        {
            _binaryObjectManager = binaryObjectManager;
            _env = env;
            _productItemRepository = productItemRepository;
            _tenantRepository = tenantRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _documentlibraryRepository = documentlibraryRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [HttpPost]
        public async Task<JsonResult> UploadFiles()
        {
            try
            {
                var files = Request.Form.Files;

                //Check input
                if (files == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }

                List<UploadFileOutput> filesOutput = new List<UploadFileOutput>();

                foreach (var file in files)
                {
                    if (file.Length > 1048576) //1MB
                    {
                        throw new UserFriendlyException(L("File_SizeLimit_Error"));
                    }

                    byte[] fileBytes;
                    using (var stream = file.OpenReadStream())
                    {
                        fileBytes = stream.GetAllBytes();
                    }

                    var Path = _env.WebRootPath;
                    var FinalFilePath = Path + "\\EmailAttachments\\" + file.FileName;
                    using (MemoryStream mStream = new MemoryStream(fileBytes))
                    {
                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                    }

                    var fileObject = new BinaryObject(AbpSession.TenantId, fileBytes);
                    await _binaryObjectManager.SaveAsync(fileObject);

                    filesOutput.Add(new UploadFileOutput
                    {
                        Id = fileObject.Id,
                        FileName = file.FileName
                    });
                }

                return Json(new AjaxResponse(filesOutput));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [HttpPost]
        public async Task<JsonResult> UploadAttachments()
        {
            try
            {
                var files = Request.Form.Files;

                //Check input
                if (files == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }

                List<Guid> filesOutput = new List<Guid>();

                foreach (var file in files)
                {
                    if (file.Length > 1048576) //1MB
                    {
                        throw new UserFriendlyException(L("File_SizeLimit_Error"));
                    }

                    byte[] fileBytes;
                    using (var stream = file.OpenReadStream())
                    {
                        fileBytes = stream.GetAllBytes();
                    }

                    var fileObject = new BinaryObject(AbpSession.TenantId, fileBytes);
                    await _binaryObjectManager.SaveAsync(fileObject);

                    filesOutput.Add(fileObject.Id);
                }

                return Json(new AjaxResponse(filesOutput));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [HttpPost]
        public async Task<JsonResult> UploadProductItem(int id)
        {
            try
            {
                var files = Request.Form.Files;

                //Check input
                if (files == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }

                List<Guid> filesOutput = new List<Guid>();

                foreach (var file in files)
                {
                    if (file.Length > 1048576) //1MB
                    {
                        throw new UserFriendlyException(L("File_SizeLimit_Error"));
                    }

                    byte[] fileBytes;
                    using (var stream = file.OpenReadStream())
                    {
                        fileBytes = stream.GetAllBytes();
                    }



                    //var ByteArray = _tempFileCacheManager.GetFile(fileBytes);

                    //if (ByteArray == null)
                    //{
                    //	throw new UserFriendlyException("There is no such file with the token: " + input.FileToken);
                    //}

                    //if (ByteArray.Length > MaxFileBytes)
                    //{
                    //	throw new UserFriendlyException("Document size exceeded");
                    //}

                    var Path1 = _env.WebRootPath;
                    var MainFolder = Path1;
                    MainFolder = MainFolder + "\\Documents";

					var ProductItems = _productItemRepository.GetAll().Where(e => e.Id == id).FirstOrDefault();
					var TenantName = _tenantRepository.GetAll().Where(e => e.Id == ProductItems.TenantId).Select(e => e.TenancyName).FirstOrDefault();

					string FileName = ProductItems.Model + DateTime.Now.Ticks;
					string TenantPath = MainFolder + "\\" + TenantName + "\\";
					string SignaturePath = TenantPath + "ProductItem\\";
					var FinalFilePath = SignaturePath + "\\" + file.FileName;

                    if (System.IO.Directory.Exists(MainFolder))
                    {
                        if (System.IO.Directory.Exists(TenantPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(TenantPath);

                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(MainFolder);
                        if (System.IO.Directory.Exists(TenantPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(TenantPath);

                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                    }

					ProductItems.FileName = file.FileName;
					ProductItems.FileType = file.ContentType;
					ProductItems.FilePath = "\\Documents" + "\\" + TenantName + "\\ProductItem\\";

                     _productItemRepository.Update(ProductItems);
                    //var fileObject = new BinaryObject(AbpSession.TenantId, fileBytes);
                    //await _binaryObjectManager.SaveAsync(fileObject);

                    //filesOutput.Add(fileObject.Id);
                }

                return Json(new AjaxResponse(filesOutput));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }


        [HttpPost]
        public async Task<JsonResult> UploadDocumentLibrary(int? id, string title, int tenantId)
        {
            try
            {
                var files = Request.Form.Files;

                //Check input
                if (files == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }

                List<Guid> filesOutput = new List<Guid>();
                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (_unitOfWorkManager.Current.SetTenantId(tenantId))
                    {
                        foreach (var file in files)
                        {
                            if (file.Length > 1048576) //1MB
                            {
                                throw new UserFriendlyException(L("File_SizeLimit_Error"));
                            }

                            byte[] fileBytes;
                            using (var stream = file.OpenReadStream())
                            {
                                fileBytes = stream.GetAllBytes();
                            }



                            //var ByteArray = _tempFileCacheManager.GetFile(fileBytes);

                            //if (ByteArray == null)
                            //{
                            //	throw new UserFriendlyException("There is no such file with the token: " + input.FileToken);
                            //}

                            //if (ByteArray.Length > MaxFileBytes)
                            //{
                            //	throw new UserFriendlyException("Document size exceeded");
                            //}




                           
                                var Path1 = _env.WebRootPath;
                                var MainFolder = Path1;
                                MainFolder = MainFolder + "\\Documents";


                                var TenantName = _tenantRepository.GetAll().Where(e => e.Id == tenantId).Select(e => e.TenancyName).FirstOrDefault();
                                string FileName = DateTime.Now.Ticks + "_" + file.FileName;
                                string TenantPath = MainFolder + "\\" + TenantName + "\\";
                                string SignaturePath = TenantPath + "DocumentLibrary\\";
                                var FinalFilePath = SignaturePath + "\\" + FileName;

                                if (System.IO.Directory.Exists(MainFolder))
                                {
                                    if (System.IO.Directory.Exists(TenantPath))
                                    {
                                        if (System.IO.Directory.Exists(SignaturePath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(SignaturePath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        System.IO.Directory.CreateDirectory(TenantPath);

                                        if (System.IO.Directory.Exists(SignaturePath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(SignaturePath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(MainFolder);
                                    if (System.IO.Directory.Exists(TenantPath))
                                    {
                                        if (System.IO.Directory.Exists(SignaturePath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(SignaturePath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        System.IO.Directory.CreateDirectory(TenantPath);

                                        if (System.IO.Directory.Exists(SignaturePath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(SignaturePath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                }

                                if (id == 0)
                                {
                                    DocumentLibrary doc = new DocumentLibrary();
                                    doc.Title = title;
                                    doc.FileName = FileName;
                                    doc.FilePath = "\\Documents" + "\\" + TenantName + "\\DocumentLibrary\\" + FileName;
                                    doc.FileType = file.ContentType;
                                    if (AbpSession.TenantId != null)
                                    {
                                        doc.TenantId = (int)AbpSession.TenantId;
                                    }
                                    await _documentlibraryRepository.InsertAsync(doc);
                                    var dataVaultLog = new DataVaultActivityLog();
                                    dataVaultLog.Action = "Created";
                                    dataVaultLog.SectionId = 30;
                                    dataVaultLog.ActionNote = "Document Library Created : " + doc.Title;
                                    await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);
                                }
                                if (id > 0)
                                {
                                    var Doc = _documentlibraryRepository.GetAll().Where(e => e.Id == id).FirstOrDefault();

                                    var dataVaultLog = new DataVaultActivityLog();
                                    dataVaultLog.Action = "Updated";
                                    dataVaultLog.SectionId = 30;
                                    dataVaultLog.ActionNote = "Document Library Updated : " + Doc.Title;

                                    var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
                                    var List = new List<DataVaultActivityLogHistory>();

                                    if (title != Doc.Title)
                                    {
                                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                                        history.FieldName = "Title";
                                        history.PrevValue = Doc.Title;
                                        history.CurValue = title;
                                        history.Action = "Update";
                                        history.ActivityLogId = dataVaultLogId;
                                        List.Add(history);
                                    }

                                    if (FileName != Doc.FileName)
                                    {
                                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                                        history.FieldName = "FileName";
                                        history.PrevValue = Doc.FileName;
                                        history.CurValue = FileName;
                                        history.Action = "Update";
                                        history.ActivityLogId = dataVaultLogId;
                                        List.Add(history);
                                    }

                                    await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
                                    await _dbcontextprovider.GetDbContext().SaveChangesAsync();

                                    Doc.FileName = FileName;
                                    Doc.FileType = file.ContentType;
                                    Doc.FilePath = "\\Documents" + "\\" + TenantName + "\\DocumentLibrary\\" + FileName;
                                    Doc.Title = title;
                                    if (AbpSession.TenantId != null)
                                    {
                                        Doc.TenantId = (int)AbpSession.TenantId;
                                    }
                                    await _documentlibraryRepository.UpdateAsync(Doc);

                                }
                                uow.Complete();


                        }

                        return Json(new AjaxResponse(new { success = true }));
                    }
                }

                
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
    }
}