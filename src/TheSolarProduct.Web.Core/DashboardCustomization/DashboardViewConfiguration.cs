﻿using System.Collections.Generic;


namespace TheSolarProduct.Web.DashboardCustomization
{
    public class DashboardViewConfiguration
    {
        public Dictionary<string, WidgetViewDefinition> WidgetViewDefinitions { get; } = new Dictionary<string, WidgetViewDefinition>();

        public Dictionary<string, WidgetFilterViewDefinition> WidgetFilterViewDefinitions { get; } = new Dictionary<string, WidgetFilterViewDefinition>();

        public DashboardViewConfiguration()
        {
            var jsAndCssFileRoot = "/Areas/App/Views/CustomizableDashboard/Widgets/";
            var viewFileRoot = "~/Areas/App/Views/Shared/Components/CustomizableDashboard/Widgets/";

            #region FilterViewDefinitions

            WidgetFilterViewDefinitions.Add(TheSolarProductDashboardCustomizationConsts.Filters.FilterDateRangePicker,
                new WidgetFilterViewDefinition(
                    TheSolarProductDashboardCustomizationConsts.Filters.FilterDateRangePicker,
                    viewFileRoot + "DateRangeFilter.cshtml",
                    jsAndCssFileRoot + "DateRangeFilter/DateRangeFilter.min.js",
                    jsAndCssFileRoot + "DateRangeFilter/DateRangeFilter.min.css")
            );
			WidgetFilterViewDefinitions.Add(TheSolarProductDashboardCustomizationConsts.Filters.FilterSalesRepUsers,
			  new WidgetFilterViewDefinition(
				  TheSolarProductDashboardCustomizationConsts.Filters.FilterSalesRepUsers,
				  viewFileRoot + "FilterSalesRepUsers.cshtml",
				  jsAndCssFileRoot + "DateRangeFilter/DateRangeFilter.min.js",
				  jsAndCssFileRoot + "DateRangeFilter/DateRangeFilter.min.css")
		  );
			//add your filters iew definitions here
			#endregion

			#region WidgetViewDefinitions

			#region TenantWidgets

			//WidgetViewDefinitions.Add(TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.DailySales,
			//	new WidgetViewDefinition(
			//		TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.DailySales,
			//		viewFileRoot + "DailySales.cshtml",
			//		jsAndCssFileRoot + "DailySales/DailySales.min.js",
			//		jsAndCssFileRoot + "DailySales/DailySales.min.css"));

			//WidgetViewDefinitions.Add(TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.GeneralStats,
			//	new WidgetViewDefinition(
			//		TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.GeneralStats,
			//		viewFileRoot + "GeneralStats.cshtml",
			//		jsAndCssFileRoot + "GeneralStats/GeneralStats.min.js",
			//		jsAndCssFileRoot + "GeneralStats/GeneralStats.min.css"));

			//WidgetViewDefinitions.Add(TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.ProfitShare,
			//	new WidgetViewDefinition(
			//		TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.ProfitShare,
			//		viewFileRoot + "ProfitShare.cshtml",
			//		jsAndCssFileRoot + "ProfitShare/ProfitShare.min.js",
			//		jsAndCssFileRoot + "ProfitShare/ProfitShare.min.css"));

			//WidgetViewDefinitions.Add(TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.MemberActivity,
			//	new WidgetViewDefinition(
			//		TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.MemberActivity,
			//		viewFileRoot + "MemberActivity.cshtml",
			//		jsAndCssFileRoot + "MemberActivity/MemberActivity.min.js",
			//		jsAndCssFileRoot + "MemberActivity/MemberActivity.min.css"));

			//WidgetViewDefinitions.Add(TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.RegionalStats,
			//	new WidgetViewDefinition(
			//		TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.RegionalStats,
			//		viewFileRoot + "RegionalStats.cshtml",
			//		jsAndCssFileRoot + "RegionalStats/RegionalStats.min.js",
			//		jsAndCssFileRoot + "RegionalStats/RegionalStats.min.css",
			//		12,
			//		10));

			//WidgetViewDefinitions.Add(TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.SalesSummary,
			//	new WidgetViewDefinition(
			//		TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.SalesSummary,
			//		viewFileRoot + "SalesSummary.cshtml",
			//		jsAndCssFileRoot + "SalesSummary/SalesSummary.min.js",
			//		jsAndCssFileRoot + "SalesSummary/SalesSummary.min.css",
			//		6,
			//		10));

			//WidgetViewDefinitions.Add(TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.TopStats,
			//	new WidgetViewDefinition(
			//		TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.TopStats,
			//		viewFileRoot + "TopStats.cshtml",
			//		jsAndCssFileRoot + "TopStats/TopStats.min.js",
			//		jsAndCssFileRoot + "TopStats/TopStats.min.css",
			//		12,
			//		10));

			WidgetViewDefinitions.Add(TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.UserLeads,
               new WidgetViewDefinition(
                   TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.UserLeads,
                   viewFileRoot + "UserLeads.cshtml",
                   jsAndCssFileRoot + "UserLeads/UserLeads.min.js",
                   jsAndCssFileRoot + "UserLeads/UserLeads.min.css",
                   12,
                   15));

			WidgetViewDefinitions.Add(TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.ManagerLeads,
			   new WidgetViewDefinition(
				   TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.ManagerLeads,
				   viewFileRoot + "UserLeads.cshtml",
				   jsAndCssFileRoot + "UserLeads/UserLeads.min.js",
				   jsAndCssFileRoot + "UserLeads/UserLeads.min.css",
				   12,
				   15));
			//add your tenant side widget definitions here
			#endregion

			#region HostWidgets

			WidgetViewDefinitions.Add(TheSolarProductDashboardCustomizationConsts.Widgets.Host.IncomeStatistics,
				new WidgetViewDefinition(
					TheSolarProductDashboardCustomizationConsts.Widgets.Host.IncomeStatistics,
					viewFileRoot + "IncomeStatistics.cshtml",
					jsAndCssFileRoot + "IncomeStatistics/IncomeStatistics.min.js",
					jsAndCssFileRoot + "IncomeStatistics/IncomeStatistics.min.css"));

			WidgetViewDefinitions.Add(TheSolarProductDashboardCustomizationConsts.Widgets.Host.TopStats,
				new WidgetViewDefinition(
					TheSolarProductDashboardCustomizationConsts.Widgets.Host.TopStats,
					viewFileRoot + "HostTopStats.cshtml",
					jsAndCssFileRoot + "HostTopStats/HostTopStats.min.js",
					jsAndCssFileRoot + "HostTopStats/HostTopStats.min.css"));

			WidgetViewDefinitions.Add(TheSolarProductDashboardCustomizationConsts.Widgets.Host.EditionStatistics,
				new WidgetViewDefinition(
					TheSolarProductDashboardCustomizationConsts.Widgets.Host.EditionStatistics,
					viewFileRoot + "EditionStatistics.cshtml",
					jsAndCssFileRoot + "EditionStatistics/EditionStatistics.min.js",
					jsAndCssFileRoot + "EditionStatistics/EditionStatistics.min.css"));

			WidgetViewDefinitions.Add(TheSolarProductDashboardCustomizationConsts.Widgets.Host.SubscriptionExpiringTenants,
				new WidgetViewDefinition(
					TheSolarProductDashboardCustomizationConsts.Widgets.Host.SubscriptionExpiringTenants,
					viewFileRoot + "SubscriptionExpiringTenants.cshtml",
					jsAndCssFileRoot + "SubscriptionExpiringTenants/SubscriptionExpiringTenants.min.js",
					jsAndCssFileRoot + "SubscriptionExpiringTenants/SubscriptionExpiringTenants.min.css",
					6,
					10));

			WidgetViewDefinitions.Add(TheSolarProductDashboardCustomizationConsts.Widgets.Host.RecentTenants,
				new WidgetViewDefinition(
					TheSolarProductDashboardCustomizationConsts.Widgets.Host.RecentTenants,
					viewFileRoot + "RecentTenants.cshtml",
					jsAndCssFileRoot + "RecentTenants/RecentTenants.min.js",
					jsAndCssFileRoot + "RecentTenants/RecentTenants.min.css"));

			//add your host side widgets definitions here
			#endregion

			#endregion
		}
    }
}
