﻿using System.Threading.Tasks;
using TheSolarProduct.Sessions.Dto;

namespace TheSolarProduct.Web.Session
{
    public interface IPerRequestSessionCache
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformationsAsync();
    }
}
