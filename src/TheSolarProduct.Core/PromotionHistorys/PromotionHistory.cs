﻿using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PromotionHistorys
{
    public class PromotionHistory : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get ; set; }

        public string LeadIds { get; set; }

        public string PromotionType { get; set; }

        public int PromotionId { get; set; }

        public int PromotionSendingId { get; set; }

        public int OrganizationId { get; set; }

        public string Title { get; set; }

        public string LeadStatuses { get; set; }

        public string States { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public decimal? Charges { get; set; }

        public string Description { get; set; }

        public int? TotalCharCount { get; set; }

        public int? TotalCredit { get; set;}

        public string LeadSources { get; set; }

        public string Teams { get; set; }

        public string Area { get; set; }

        public string Type { get; set; }

    }
}
