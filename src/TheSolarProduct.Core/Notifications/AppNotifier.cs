using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp;
using Abp.Collections.Extensions;
using Abp.Localization;
using Abp.Notifications;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.MultiTenancy;

namespace TheSolarProduct.Notifications
{
    public class AppNotifier : TheSolarProductDomainServiceBase, IAppNotifier, IRealTimeNotifier
    {
        private readonly INotificationPublisher _notificationPublisher;
        private readonly UserManager _userManager;
        public AppNotifier(INotificationPublisher notificationPublisher,
            UserManager userManager)
        {
            _notificationPublisher = notificationPublisher;
            _userManager = userManager;
        }

        public async Task WelcomeToTheApplicationAsync(User user)
        {
            await _notificationPublisher.PublishAsync(
                AppNotificationNames.WelcomeToTheApplication,
                new MessageNotificationData(L("WelcomeToTheApplicationNotificationMessage")),
                severity: NotificationSeverity.Success,
                userIds: new[] { user.ToUserIdentifier() }
            );
        }

        public async Task NewUserRegisteredAsync(User user)
        {
            var notificationData = new LocalizableMessageNotificationData(
                new LocalizableString(
                    "NewUserRegisteredNotificationMessage",
                    TheSolarProductConsts.LocalizationSourceName
                )
            );

            notificationData["userName"] = user.UserName;
            notificationData["emailAddress"] = user.EmailAddress;

            await _notificationPublisher.PublishAsync(AppNotificationNames.NewUserRegistered, notificationData,
                tenantIds: new[] { user.TenantId });
        }

        public async Task NewTenantRegisteredAsync(Tenant tenant)
        {
            var notificationData = new LocalizableMessageNotificationData(
                new LocalizableString(
                    "NewTenantRegisteredNotificationMessage",
                    TheSolarProductConsts.LocalizationSourceName
                )
            );

            notificationData["tenancyName"] = tenant.TenancyName;
            await _notificationPublisher.PublishAsync(AppNotificationNames.NewTenantRegistered, notificationData);
        }

        public async Task GdprDataPrepared(UserIdentifier user, Guid binaryObjectId)
        {
            var notificationData = new LocalizableMessageNotificationData(
                new LocalizableString(
                    "GdprDataPreparedNotificationMessage",
                    TheSolarProductConsts.LocalizationSourceName
                )
            );

            notificationData["binaryObjectId"] = binaryObjectId;

            await _notificationPublisher.PublishAsync(AppNotificationNames.GdprDataPrepared, notificationData,
                userIds: new[] { user });
        }

        //This is for test purposes
        public async Task SendMessageAsync(UserIdentifier user, string message,
            NotificationSeverity severity = NotificationSeverity.Info)
        {
            await _notificationPublisher.PublishAsync(
                AppNotificationNames.SimpleMessage,
                new MessageNotificationData(message),
                severity: severity,
                userIds: new[] { user }
            );
        }

        public Task SendMessageAsync(UserIdentifier user, LocalizableString localizableMessage,
            IDictionary<string, object> localizableMessageData = null,
            NotificationSeverity severity = NotificationSeverity.Info)
        {
            return SendNotificationAsync(AppNotificationNames.SimpleMessage, user, localizableMessage,
                localizableMessageData, severity);
        }

        protected async Task SendNotificationAsync(string notificationName, UserIdentifier user,
            LocalizableString localizableMessage, IDictionary<string, object> localizableMessageData = null,
            NotificationSeverity severity = NotificationSeverity.Info)
        {
            var notificationData = new LocalizableMessageNotificationData(localizableMessage);
            if (localizableMessageData != null)
            {
                foreach (var pair in localizableMessageData)
                {
                    notificationData[pair.Key] = pair.Value;
                }
            }

            await _notificationPublisher.PublishAsync(notificationName, notificationData, severity: severity,
                userIds: new[] { user });
        }

        public Task TenantsMovedToEdition(UserIdentifier user, string sourceEditionName, string targetEditionName)
        {
            return SendNotificationAsync(AppNotificationNames.TenantsMovedToEdition, user,
                new LocalizableString(
                    "TenantsMovedToEditionNotificationMessage",
                    TheSolarProductConsts.LocalizationSourceName
                ),
                new Dictionary<string, object>
                {
                    {"sourceEditionName", sourceEditionName},
                    {"targetEditionName", targetEditionName}
                });
        }

        public Task<TResult> TenantsMovedToEdition<TResult>(UserIdentifier argsUser, int sourceEditionId,
            int targetEditionId)
        {
            throw new NotImplementedException();
        }

        public Task SomeUsersCouldntBeImported(UserIdentifier user, string fileToken, string fileType, string fileName)
        {
            return SendNotificationAsync(AppNotificationNames.DownloadInvalidImportUsers, user,
                new LocalizableString(
                    "ClickToSeeInvalidUsers",
                    TheSolarProductConsts.LocalizationSourceName
                ),
                new Dictionary<string, object>
                {
                    { "fileToken", fileToken },
                    { "fileType", fileType },
                    { "fileName", fileName }
                });
        }

        public Task SomeLeadsCouldntBeImported(UserIdentifier user, string fileToken, string fileType, string fileName)
        {
            return SendNotificationAsync(AppNotificationNames.DownloadInvalidImportUsers, user,
                new LocalizableString(
                    "ClickToSeeInvalidLeads",
                    TheSolarProductConsts.LocalizationSourceName
                ),
                new Dictionary<string, object>
                {
                    { "fileToken", fileToken },
                    { "fileType", fileType },
                    { "fileName", fileName }
                });
        }

        public Task SomeProductItemsCouldntBeImported(UserIdentifier user, string fileToken, string fileType, string fileName)
        {
            return SendNotificationAsync(AppNotificationNames.DownloadInvalidImportUsers, user,
                new LocalizableString(
                    "ClickToSeeInvalidProductItems",
                    TheSolarProductConsts.LocalizationSourceName
                ),
                new Dictionary<string, object>
                {
                    { "fileToken", fileToken },
                    { "fileType", fileType },
                    { "fileName", fileName }
                });
        }

        public async Task LeadExpenseAdded(User user, string message, NotificationSeverity notificationSeverity)
        {
            
            await _notificationPublisher.PublishAsync(
                AppNotificationNames.LeadAssigned,
                new MessageNotificationData(message),
                severity: NotificationSeverity.Info,
                userIds: new[] { user.ToUserIdentifier() }
                );

        }

        public async Task LeadAssiged(User user, string message, NotificationSeverity notificationSeverity)
        {
            await _notificationPublisher.PublishAsync(
                AppNotificationNames.LeadAssigned,
                new MessageNotificationData(message),
                severity: NotificationSeverity.Info,
                userIds: new[] { user.ToUserIdentifier() }
                );
        }

        public async Task LeadAssigedList(List<User> user, string message, NotificationSeverity notificationSeverity)
        {
			foreach (var item in user)
			{
                await _notificationPublisher.PublishAsync(
                AppNotificationNames.LeadAssigned,
                new MessageNotificationData(message),
                severity: NotificationSeverity.Info,
                userIds: new[] { item.ToUserIdentifier() }
                );
            }
        }

        public async Task LeadComment(User user, string message, NotificationSeverity notificationSeverity)
        {
            await _notificationPublisher.PublishAsync(
                AppNotificationNames.LeadAssigned,
                new MessageNotificationData(message),
                severity: NotificationSeverity.Info,
                userIds: new[] { user.ToUserIdentifier() }
                );
        }        

        public async Task SendNotificationsAsync(UserNotification[] userNotifications)
        {
            //await signalRClient.SendAsync("getFriendshipRequest", friendshipRequest, isOwnRequest);
            //foreach (var userNotification in userNotifications)
            //{
            //    if (userNotification.Notification.Data is MessageNotificationData data)
            //    {
            //        var user = await _userManager.GetUserByIdAsync(userNotification.UserId);
            //        await _notificationPublisher.PublishAsync(
            //        AppNotificationNames.LeadAssigned,
            //        new MessageNotificationData(data.Message),
            //        severity: NotificationSeverity.Info,
            //        userIds: new[] { user.ToUserIdentifier() }
            //        );
            //    }
            //}
        }

        public void SendNotifications(UserNotification[] userNotifications)
        {
            //foreach (var userNotification in userNotifications)
            //{
            //    if (userNotification.Notification.Data is MessageNotificationData data)
            //    {
            //        var user = _userManager.GetUserById(userNotification.UserId);
            //         _notificationPublisher.Publish(
            //        AppNotificationNames.LeadAssigned,
            //        new MessageNotificationData(data.Message),
            //        severity: NotificationSeverity.Info,
            //        userIds: new[] { user.ToUserIdentifier() }
            //        );
            //    }
            //}
        }

		public async Task NewLead(User user, string message, NotificationSeverity notificationSeverity)
		{
            await _notificationPublisher.PublishAsync(
                AppNotificationNames.LeadAssigned,
                new MessageNotificationData(message),
                severity: NotificationSeverity.Info,
                userIds: new[] { user.ToUserIdentifier() }
                );
        }
        public Task SomeSTCCouldntBeImported(UserIdentifier user, string fileToken, string fileType, string fileName)
        {
            return SendNotificationAsync(AppNotificationNames.DownloadInvalidImportUsers, user,
                new LocalizableString(
                    "ClickToSeeInvalidProductItems",
                    TheSolarProductConsts.LocalizationSourceName
                ),
                new Dictionary<string, object>
                {
                    { "fileToken", fileToken },
                    { "fileType", fileType },
                    { "fileName", fileName }
                });
        }
        
        
        public Task SomeEnfornicaCallDetailCouldntBeImported(UserIdentifier user, string fileToken, string fileType, string fileName)
        {
            return SendNotificationAsync(AppNotificationNames.DownloadInvalidImportUsers, user,
                new LocalizableString(
                    "ClickToSeeInvalidProductItems",
                    TheSolarProductConsts.LocalizationSourceName
                ),
                new Dictionary<string, object>
                {
                    { "fileToken", fileToken },
                    { "fileType", fileType },
                    { "fileName", fileName }
                });
        }
        public Task SomeInstallationCouldntBeImported(UserIdentifier user, string fileToken, string fileType, string fileName)
        {
            return SendNotificationAsync(AppNotificationNames.DownloadInvalidImportUsers, user,
                new LocalizableString(
                    "ClickToSeeInvalidProductItems",
                    TheSolarProductConsts.LocalizationSourceName
                ),
                new Dictionary<string, object>
                {
                    { "fileToken", fileToken },
                    { "fileType", fileType },
                    { "fileName", fileName }
                });
        }

    }
}
