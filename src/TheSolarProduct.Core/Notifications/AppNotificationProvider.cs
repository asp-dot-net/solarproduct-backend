﻿using Abp.Authorization;
using Abp.Localization;
using Abp.Notifications;
using TheSolarProduct.Authorization;

namespace TheSolarProduct.Notifications
{
    public class AppNotificationProvider : NotificationProvider
    {
        public override void SetNotifications(INotificationDefinitionContext context)
        {
            context.Manager.Add(
                new NotificationDefinition(
                    AppNotificationNames.NewUserRegistered,
                    displayName: L("NewUserRegisteredNotificationDefinition"),
                    permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Users)
                    )
                );

            context.Manager.Add(
                new NotificationDefinition(
                    AppNotificationNames.NewTenantRegistered,
                    displayName: L("NewTenantRegisteredNotificationDefinition"),
                    permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Tenants)
                    )
                );

            context.Manager.Add(
                new NotificationDefinition(
                    AppNotificationNames.LeadAssigned,
                    displayName: L("NewLeadAssignedNotificationDefinition"),
                    permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_MyLeads)
                    )
                );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, TheSolarProductConsts.LocalizationSourceName);
        }
    }
}
