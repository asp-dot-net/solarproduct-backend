﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.GreenBoatDatatable
{
    [Table("GreenBoatDatas")]
    public class GreenBoatData : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; } 
        public int? CompanyId { get; set; } 
        public int ProjectNo { get; set; }
        public string VendorJobId { get; set; }
        public int? JobID { get; set; }
        public string Title { get; set; }
        public string RefNumber  { get; set; }
        public string Description{ get; set; }
        public int?  JobType { get; set; }
        public int?  JobStage { get; set; }
        public int?  Priority { get; set; }
        public string InstallationDate { get; set; }
        public string CreatedDate { get; set; }
        public   bool  IsGst   { get; set; }
        public string OwnerType_JOD { get; set; }
        public string CompanyName_JOD { get; set; }
        public string FirstName_JOD { get; set; }
        public string LastName_JOD { get; set; }
        public string Phone_JOD { get; set; }
        public string Mobile_JOD { get; set; }
        public string Email_JOD { get; set; }
        public bool IsPostalAddress_JOD  { get; set; }
        public string UnitTypeID_JOD { get; set; }
        public string UnitNumber_JOD  { get; set; }
        public string StreetNumber_JOD    { get; set; }
        public string StreetName_JOD   { get; set; }
        public int? StreetTypeID_JOD  { get; set; }
        public string Town_JOD { get; set; }
        public string State_JOD { get; set; }
        public string PostCode_JOD { get; set; }
        public string DistributorID_JID  { get; set; }
        public string MeterNumber_JID { get; set; }
        public string PhaseProperty_JID { get; set; }
        public string ElectricityProviderID_JID { get; set; }
        public bool IsSameAsOwnerAddress_JID { get; set; }
        public bool IsPostalAddress_JID { get; set; }
        public string UnitTypeID_JID  { get; set; }
        public string UnitNumber_JID { get; set; }
        public string  StreetNumber_JID
        { get; set; }
        public string StreetName_JID
        { get; set; }
        public int? StreetTypeID_JID { get; set; }
        public string Town_JID { get; set; }
        public string State_JID { get; set; }
        public string PostCode_JID { get; set; }
        public string NMI_JID { get; set; }
        public string SystemSize_JID { get; set; }
        public string PropertyType_JID { get; set; }
        public string SingleMultipleStory_JID { get; set; }
        public string InstallingNewPanel_JID { get; set; }
        public string Location_JID { get; set; }
        public bool ExistingSystem_JID  { get; set; }
        public string ExistingSystemSize_JID { get; set; }
        public string SystemLocation_JID  { get; set; }
        public string NoOfPanels_JID { get; set; }
        public string AdditionalInstallationInformation_JID { get; set; }
        public string SystemSize_JSD { get; set; }
        public string SerialNumbers_JSD { get; set; }
        public string NoOfPanel_JSD { get; set; }
        public string AdditionalCapacityNotes_JSTC { get; set; }
        public string TypeOfConnection_JSTC { get; set; }
        public string SystemMountingType_JSTC { get; set; }
        public string DeemingPeriod_JSTC { get; set; }
        public string CertificateCreated_JSTC { get; set; }
        public string FailedAccreditationCode_JSTC { get; set; }
        public string FailedReason_JSTC { get; set; }
        public string MultipleSGUAddress_JSTC { get; set; }
        public string Location_JSTC { get; set; }
        public string AdditionalLocationInformation_JSTC { get; set; }
        public string AdditionalSystemInformation_JSTC { get; set; }
        public string FirstName_IV { get; set; }
        public string LastName_IV { get; set; }
        public string Email_IV { get; set; }
        public string Phone_IV { get; set; }
        public string Mobile_IV { get; set; }
        public string CECAccreditationNumber_IV { get; set; }
        public string ElectricalContractorsLicenseNumber_IV { get; set; }
        public bool IsPostalAddress_IV { get; set; }
        public int? UnitTypeID_IV { get; set; }
        public string UnitNumber_IV { get; set; }
        public string StreetNumber_IV { get; set; }
        public string StreetName_IV { get; set; }
        public int? StreetTypeID_IV { get; set; }
        public string Town_IV { get; set; }
        public string State_IV { get; set; }
        public string PostCode_IV { get; set; }
        public string FirstName_DV { get; set; }
        public string LastName_DV { get; set; }
        public string Email_DV { get; set; }
        public string Phone_DV { get; set; }
        public string Mobile_DV { get; set; }
        public string CECAccreditationNumber_DV { get; set; }
        public string ElectricalContractorsLicenseNumber_DV { get; set; }
        public bool IsPostalAddress_DV { get; set; }
        public int? UnitTypeID_DV { get; set; }
        public string UnitNumber_DV { get; set; }
        public string StreetNumber_DV { get; set; }
        public string StreetName_DV { get; set; }
        public int? StreetTypeID_DV { get; set; }
        public string Town_DV { get; set; }
        public string State_DV { get; set; }
        public string PostCode_DV { get; set; }
        public string CompanyName_JE { get; set; }
        public string FirstName_JE { get; set; }
        public string LastName_JE { get; set; }
        public string Email_JE { get; set; }
        public string Phone_JE { get; set; }
        public string Mobile_JE { get; set; }
        public string LicenseNumber_JE { get; set; }
        public bool IsPostalAddress_JE { get; set; }
        public string UnitTypeID_JE { get; set; }
        public string UnitNumber_JE { get; set; }
        public string StreetNumber_JE { get; set; }
        public string StreetName_JE { get; set; }
        public string StreetTypeID_JE { get; set; }
        public string Town_JE { get; set; }
        public string State_JE { get; set; }
        public string PostCode_JE { get; set; }
        public string Brand_LstJID { get; set; }
        public string Model_LstJID { get; set; }
        public string Series_LstJID { get; set; }
        public string NoOfInverter_LstJID { get; set; }
        public string Brand_LstJPD { get; set; }
        public string Model_LstJPD { get; set; }
        public int? NoOfPanel_LstJPD { get; set; }
        public string STCStatus_JSTCD { get; set; }
        public string CalculatedSTC_JSTCD { get; set; }
        public string STCPrice_JSTCD { get; set; }
        public string FailureNotice_JSTCD { get; set; }
        public string ComplianceNotes_JSTCD { get; set; }
        public string STCSubmissionDate_JSTCD { get; set; }
        public string STCInvoiceStatus_JSTCD { get; set; }
        public bool IsInvoiced_JSTCD { get; set; }
        public string FieldName_LstCD { get; set; }
        public string FieldValue_LstCD { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int? OrganizationId { get; set; }

    }
}
