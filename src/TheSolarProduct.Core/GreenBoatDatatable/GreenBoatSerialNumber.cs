﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.GreenBoatDatatable
{
    [Table("GreenBoatSerialNumbers")]
   public class GreenBoatSerialNumber : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public int? GreenBoatId { get; set; }
        
        public string SerialNumbers { get; set; }
    }
}
