﻿using System.Globalization;

namespace TheSolarProduct.Localization
{
    public interface IApplicationCulturesProvider
    {
        CultureInfo[] GetAllCultures();
    }
}