﻿using System.Reflection;
using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace TheSolarProduct.Localization
{
    public static class TheSolarProductLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(
                    TheSolarProductConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(TheSolarProductLocalizationConfigurer).GetAssembly(),
                        "TheSolarProduct.Localization.TheSolarProduct"
                    )
                )
            );
        }
    }
}