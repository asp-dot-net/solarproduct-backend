﻿using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheSolarProduct.ECommerceSliders
{
	[Table("ECommerceSliders")]
    public class ECommerceSlider : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public string Name { get; set;}

        public string Image { get; set;}

        public string Header { get; set;}

        public string SubHeader { get; set;}

        public string button { get; set;}

        public bool active { get; set;}

        public string ImageUrl { get; set;}
    }
}
