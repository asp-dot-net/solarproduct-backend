﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Auditing;

namespace TheSolarProduct.Promotions
{
	[Table("PromotionResponseStatuses")]
    [Audited]
    public class PromotionResponseStatus : Entity
    {		

		[Required]
		[StringLength(PromotionResponseStatusConsts.MaxNameLength, MinimumLength = PromotionResponseStatusConsts.MinNameLength)]
		public virtual string Name { get; set; }
		

    }
}