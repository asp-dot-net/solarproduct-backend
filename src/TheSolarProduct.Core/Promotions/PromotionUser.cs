﻿using TheSolarProduct.Promotions;
using TheSolarProduct.Leads;
using TheSolarProduct.Promotions;
using TheSolarProduct.Promotions;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Promotions
{
    [Table("PromotionUsers")]
    public class PromotionUser : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }


        public virtual DateTime ResponseDate { get; set; }

        public virtual string ResponseMessage { get; set; }


        public virtual int? PromotionId { get; set; }

        [ForeignKey("PromotionId")]
        public Promotion PromotionFk { get; set; }

        public virtual int? LeadId { get; set; }

        [ForeignKey("LeadId")]
        public Lead LeadFk { get; set; }

        public virtual int? PromotionResponseStatusId { get; set; }

        [ForeignKey("PromotionResponseStatusId")]
        public PromotionResponseStatus PromotionResponseStatusFk { get; set; }
    }
}