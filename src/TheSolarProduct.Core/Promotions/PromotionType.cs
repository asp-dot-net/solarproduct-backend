﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Auditing;

namespace TheSolarProduct.Promotions
{
	[Table("PromotionTypes")]
    [Audited]
    public class PromotionType : Entity 
    {

		[Required]
		[StringLength(PromotionTypeConsts.MaxNameLength, MinimumLength = PromotionTypeConsts.MinNameLength)]
		public virtual string Name { get; set; }

		public virtual string IconClass { get; set; }
		
    }
}