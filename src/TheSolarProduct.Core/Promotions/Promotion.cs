﻿using TheSolarProduct.Promotions;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Promotions
{
	[Table("Promotions")]
	public class Promotion : FullAuditedEntity, IMustHaveTenant
	{
		public int TenantId { get; set; }

		[Required]
		public virtual string Title { get; set; }

		public virtual decimal PromoCharge { get; set; }

		public virtual string Description { get; set; }

		public virtual int? PromotionTypeId { get; set; }

		[ForeignKey("PromotionTypeId")]
		public PromotionType PromotionTypeFk { get; set; }

		public int LeadCount { get; set; }

		public virtual int OrganizationID { get; set; }



		//Filter Value
		public DateTime? StartDateFilter { get; set; }

		public DateTime? EndDateFilter { get; set; }

		public string LeadStatusIdsFilter { get; set; }

		public string LeadSourceIdsFilter { get; set; }

		public string StateIdsFilter { get; set; }

		public string TeamIdsFilter { get; set; }

		public string JobStatusIdsFilter { get; set; }

		public string AreaNameFilter { get; set; }

		public string TypeNameFilter { get; set; }
	}
}