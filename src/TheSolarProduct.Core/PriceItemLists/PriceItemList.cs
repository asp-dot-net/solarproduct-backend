﻿using TheSolarProduct.States;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.PriceItemLists
{
    [Table("PriceItemLists")]
    public class PriceItemList : FullAuditedEntity
    {
        public virtual string Name { get; set; }
        public virtual Boolean IsActive { get; set; }
    }
}
