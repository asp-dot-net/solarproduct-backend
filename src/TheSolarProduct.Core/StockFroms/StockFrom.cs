﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.StockFroms
{
    [Table("StockFroms")]
    public class StockFrom : FullAuditedEntity
    {
        public string Name { get; set; }

        public bool? IsActive { get; set; }
    }
}
