﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.StreetTypes
{
	[Table("StreetTypes")]
    public class StreetType : FullAuditedEntity 
    {

		[Required]
		[StringLength(StreetTypeConsts.MaxNameLength, MinimumLength = StreetTypeConsts.MinNameLength)]
		public virtual string Name { get; set; }
		
		[Required]
		[StringLength(StreetTypeConsts.MaxCodeLength, MinimumLength = StreetTypeConsts.MinCodeLength)]
		public virtual string Code { get; set; }

		public virtual int? StreetTypeID { get; set; }
        public virtual Boolean IsActive { get; set; }
    }
}