﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.LeadActions;
using TheSolarProduct.WholeSaleLeadActions;
using TheSolarProduct.WholeSaleLeads;

namespace TheSolarProduct.WholeSaleLeadActivityLogs
{
	[Table("WholeSaleLeadActivityLog")]
    public class WholeSaleLeadActivityLog : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual int WholeSaleLeadId { get; set; }

        [ForeignKey("WholeSaleLeadId")]
        public WholeSaleLead WholeSaleLeadFk { get; set; }

        public virtual int ActionId { get; set; }

        [ForeignKey("ActionId")]
        public LeadAction LeadActionIdFk { get; set; }

        public virtual string ActionNote { get; set; }

        public virtual DateTime? ActivityDate { get; set; }

        public virtual string Body { get; set; }

        public virtual string ActivityNote { get; set; }

        public virtual string MessageId { get; set; }

        public virtual int? ReferanceId { get; set; }

        public virtual bool? IsMark { get; set; }

        public virtual int? TemplateId { get; set; }

        public virtual string Subject { get; set; }

        public virtual int? PromotionId { get; set; }

        public virtual int? PromotionUserId { get; set; }

        public virtual int? SectionId { get; set; }

        //[ForeignKey("SectionId")]
        //public Section SectionIdFk { get; set; }

        public virtual int? TodopriorityId { get; set; }
        public virtual string Todopriority { get; set; }
        public virtual bool? IsTodoComplete { get; set; }

        public virtual string TodoResponse { get; set; }
        public virtual string TodoTag { get; set; }
        public virtual DateTime? TodoDueDate { get; set; }
        public virtual DateTime? TodoresponseTime { get; set; }

        //public virtual int? ServiceId { get; set; }

        //[ForeignKey("ServiceId")]
        //public virtual Service ServiceFK { get; set; }
    }
}
