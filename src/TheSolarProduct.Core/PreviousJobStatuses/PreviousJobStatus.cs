﻿using Abp.Auditing;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;
using TheSolarProduct.Leads;

namespace TheSolarProduct.PreviousJobStatuses
{
    [Table("PreviousJobStatuses")]
    public class PreviousJobStatus : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual int PreviousId { get; set; }
        public virtual int CurrentID { get; set; }

        public virtual int? LeadId { get; set; }
        [ForeignKey("LeadId")]
        public Lead LeadFk { get; set; }

        public virtual int? JobId { get; set; }

        [ForeignKey("JobId")]
        public Job JobFk { get; set; }

    }
}
