﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.VoucherMasters
{
 
    [Table("VoucherMasters")]
    public class VoucherMaster : FullAuditedEntity
    {
        public virtual string VoucherName { get; set; }

        public virtual string FilePath { get; set; }

        public virtual string FileName { get; set; }

        public virtual bool? IsActive { get; set; }

    }
}
