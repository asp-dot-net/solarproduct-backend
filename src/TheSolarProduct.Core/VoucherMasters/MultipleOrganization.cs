﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Vendors;

namespace TheSolarProduct.VoucherMasters
{
    [Table("MultipleOrganizations")]
    public class MultipleOrganization : FullAuditedEntity
    {
        public int? VoucherId { get; set; }

        [ForeignKey("VoucherId")]
        public VoucherMaster VoucherMasterFK { get; set; }
        public int OrgId {  get; set; }
    }
}
