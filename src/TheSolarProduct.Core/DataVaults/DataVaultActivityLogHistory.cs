﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.DataVaults
{
	[Table("DataVaultActivityLogHistory")]
    public class DataVaultActivityLogHistory : FullAuditedEntity
    {
        public virtual string FieldName { get; set; }

        public virtual string PrevValue { get; set; }

        public virtual string CurValue { get; set; }

        public virtual string Action { get; set; }

        public int? ActivityLogId { get; set; }

		[ForeignKey("SectionId")]
        public DataVaultActivityLog DataVaultActivityLogFK { get; set; }


    }
}
