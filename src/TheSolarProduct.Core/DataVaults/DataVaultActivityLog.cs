﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.DataVaults
{
	[Table("DataVaultActivityLogs")]
    public class DataVaultActivityLog : FullAuditedEntity
    {
        public string Action { get; set; }

        public virtual string ActionNote { get; set; }

        public int? SectionId { get; set; }

		[ForeignKey("SectionId")]
        public DataVaultSection SectionFK { get; set; }

        public bool IsInventory { get; set; }
    }
}
