﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.DataVaults
{
    [Table("DataVaultSections")]
    public class DataVaultSection : FullAuditedEntity
    {
        public virtual string SectionName { get; set; }
    }
}
