﻿using TheSolarProduct.Jobs;
using TheSolarProduct.Authorization.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;


namespace TheSolarProduct.Invoices
{
    [Table("InvoiceImportDatas")]
    public class InvoiceImportData : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual string JobNumber { get; set; }
        public virtual int JobId { get; set; }

        [ForeignKey("JobId")]
        public Job JobFk { get; set; }

        public virtual string InvoicePaymentmothodName { get; set; }
        public virtual int? InvoicePaymentMethodId { get; set; }

        [ForeignKey("InvoicePaymentMethodId")]
        public InvoicePaymentMethod InvoicePaymentMethodFk { get; set; }

        public virtual string Description { get; set; }
        public virtual Decimal? PaidAmmount { get; set; }
        public virtual Decimal? SSCharge { get; set; }
        public virtual string AllotedBy { get; set; }

        public virtual string ReceiptNumber {get;set;}
        public virtual string PurchaseNumber {get;set;}
        public virtual string InvoiceNotesDescription { get; set; }

        public virtual bool? IsManual { get; set; }
    }
}
