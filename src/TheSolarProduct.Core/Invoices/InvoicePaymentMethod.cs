﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Invoices
{
	[Table("InvoicePaymentMethods")]
    public class InvoicePaymentMethod : FullAuditedEntity , IMustHaveTenant
    {
			public int TenantId { get; set; }
			

		public virtual string PaymentMethod { get; set; }
		
		public virtual string ShortCode { get; set; }
		
		public virtual bool Active { get; set; }
		

    }
}