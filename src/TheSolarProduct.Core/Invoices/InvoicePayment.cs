﻿using TheSolarProduct.Jobs;
using TheSolarProduct.Authorization.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Invoices
{
	[Table("InvoicePayments")]
	public class InvoicePayment : FullAuditedEntity, IMustHaveTenant
	{
		public int TenantId { get; set; }

		public virtual decimal? InvoicePayExGST { get; set; }

		public virtual decimal? InvoicePayGST { get; set; }

		public virtual decimal? InvoicePayTotal { get; set; }

		public virtual DateTime? InvoicePayDate { get; set; }

		public virtual decimal? CCSurcharge { get; set; }

		public virtual DateTime? VerifiedOn { get; set; }

		[StringLength(InvoicePaymentConsts.MaxPaymentNoteLength, MinimumLength = InvoicePaymentConsts.MinPaymentNoteLength)]
		public virtual string PaymentNote { get; set; }

		public virtual bool Refund { get; set; }

		public virtual int? RefundType { get; set; }

		public virtual DateTime? RefundDate { get; set; }

		public virtual int? PaymentNumber { get; set; }

		public virtual bool IsVerified { get; set; }

		public virtual string ReceiptNumber { get; set; }

		public virtual string TransactionCode { get; set; }

		public virtual DateTime? ActualPayDate { get; set; }

		public virtual string PaidComment { get; set; }

		public virtual int? InvoiceNo { get; set; }
		public virtual int? JobId { get; set; }

		[ForeignKey("JobId")]
		public Job JobFk { get; set; }

		public virtual long? UserId { get; set; }

		[ForeignKey("UserId")]
		public User UserFk { get; set; }

		public virtual long? VerifiedBy { get; set; }

		[ForeignKey("VerifiedBy")]
		public User VerifiedByFk { get; set; }

		public virtual long? RefundBy { get; set; }

		[ForeignKey("RefundBy")]
		public User RefundByFk { get; set; }

		public virtual int? InvoicePaymentMethodId { get; set; }

		[ForeignKey("InvoicePaymentMethodId")]
		public InvoicePaymentMethod InvoicePaymentMethodFk { get; set; }

		public virtual int? InvoicePaymentStatusId { get; set; }

		[ForeignKey("InvoicePaymentStatusId")]
		public InvoiceStatus InvoicePaymentStatusFk { get; set; }
		public virtual Boolean? InvoiceSmsSend { get; set; }

		public virtual Boolean? InvoiceEmailSend { get; set; }

		public virtual DateTime? InvoiceSmsSendDate { get; set; }

		public virtual DateTime? InvoiceEmailSendDate { get; set; }
	}
}