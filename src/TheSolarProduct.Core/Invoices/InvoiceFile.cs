﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Invoices
{
    [Table("InvoiceFiles")]
    public class InvoiceFile : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual string FileName { get; set; }
        public virtual string FilePath { get; set; }
    }
}
