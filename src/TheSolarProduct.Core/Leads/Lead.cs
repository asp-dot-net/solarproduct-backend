﻿using TheSolarProduct.PostCodes;
using TheSolarProduct.States;
using TheSolarProduct.LeadSources;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using TheSolarProduct.LeadStatuses;
using TheSolarProduct.Authorization.Users;

namespace TheSolarProduct.Leads
{
	[Table("Leads")]
	public class Lead : FullAuditedEntity, IMustHaveTenant
	{
		public int TenantId { get; set; }

		public virtual string CompanyName { get; set; }

		public virtual string Email { get; set; }

		public virtual string Phone { get; set; }

		public virtual string Mobile { get; set; }

		public virtual string Address { get; set; }

		public virtual string ExcelAddress { get; set; }

		public virtual string Requirements { get; set; }

		public virtual string LeadSource { get; set; }

		public virtual int? LeadSourceId { get; set; }

		public virtual string Suburb { get; set; }

		public virtual int? SuburbId { get; set; }

		public virtual string State { get; set; }

		public virtual int? StateId { get; set; }

		public virtual string PostCode { get; set; }

		public virtual string UnitNo { get; set; }

		public virtual string UnitType { get; set; }

		public virtual string StreetNo { get; set; }

		public virtual string StreetName { get; set; }

		public virtual string StreetType { get; set; }

		public virtual int LeadStatusId { get; set; }

		[ForeignKey("LeadStatusId")]
		public LeadStatus LeadStatusFk { get; set; }

		public virtual int? AssignToUserID { get; set; }

		public virtual string latitude { get; set; }

		public virtual string longitude { get; set; }

		//public virtual string Postallatitude { get; set; }

		//public virtual string Postallongitude { get; set; }

		public virtual string IsGoogle { get; set; }

		//public virtual string PostalIsGoogle { get; set; }

		public virtual bool? IsDuplicate { get; set; }

		public virtual bool? IsWebDuplicate { get; set; }

		public virtual bool IsPromotion { get; set; }

		public virtual string AltPhone { get; set; }

		public virtual string Type { get; set; }

		public virtual string SolarType { get; set; }

		//public virtual bool? IsSameAddress { get; set; }

		//public virtual string PostalAddress { get; set; }

		//public virtual string PostalUnitNo { get; set; }

		//public virtual string PostalUnitType { get; set; }

		//public virtual string PostalStreetNo { get; set; }

		//public virtual string PostalStreetName { get; set; }

		//public virtual string PostalStreetType { get; set; }

		//public virtual string PostalSuburb { get; set; }

		//public virtual int? PostalSuburbId { get; set; }

		//public virtual string PostalState { get; set; }

		//public virtual int? PostalStateId { get; set; }

		//public virtual string PostalPostCode { get; set; }

		public virtual string Area { get; set; }

		//public virtual string Country { get; set; }

		public virtual string ABN { get; set; }

		public virtual string Fax { get; set; }

		public virtual string SystemType { get; set; }

		public virtual string RoofType { get; set; }

		public virtual string AngleType { get; set; }

		public virtual string StoryType { get; set; }

		public virtual string HouseAgeType { get; set; }

		public virtual string RejectReason { get; set; }

		public virtual int? RejectReasonId { get; set; }

		public virtual int? CancelReasonId { get; set; }

		//1. Create
		//2. Excel Import
		//3. External Link
		public virtual int IsExternalLead { get; set; }

		public virtual int OrganizationId { get; set; }

		public virtual DateTime? LeadAssignDate { get; set; }

		public virtual DateTime? ChangeStatusDate { get; set; }

		public virtual DateTime? ActivityDate { get; set; }

		public virtual string ActivityDescription { get; set; }

		public virtual string ActivityNote { get; set; }

		public virtual DateTime? AssignDate { get; set; }

        public virtual string ReferralName { get; set; }

        public virtual int? ReferralLeadId { get; set; }

        public virtual string GCLId { get; set; }

        public virtual string FormName { get; set; }

        public virtual string UserIP { get; set; }

        public virtual int? DublicateLeadId { get; set; }

        public virtual bool? HideDublicate { get; set; }

        public virtual DateTime? ReAssignDate { get; set; }

        public virtual DateTime? LeadGenTransferDate { get; set; }

        public virtual DateTime? FirstAssignDate { get; set; }

        public virtual long? FirstAssignUserId { get; set; }

        public virtual bool? IsFakeLead { get; set; }

		public int? SMSEmailCIMAT { get; set; }

    }
}