﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.CommmonActivityLogs
{
    [Table("CommonSections")]
    public class CommonSection : FullAuditedEntity
    {
        public virtual string SectionName { get; set; }

    }
}
