﻿using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.CommissionRanges
{
    [Table("CommissionRanges")]
    public class CommissionRange : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public string Range { get; set; }

        public decimal? Value { get; set; }

        public int? OrgId { get; set; }
        public Boolean IsActive {  get; set; }

    }
}
