﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.FreightCompanies
{
    [Table("FreightCompanies")]
    public class FreightCompany : FullAuditedEntity
    {
        public string Name { get; set; }

        public bool? IsActive { get; set; }
    }
}
