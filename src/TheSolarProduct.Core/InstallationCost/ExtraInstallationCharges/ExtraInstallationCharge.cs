﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.States;

namespace TheSolarProduct.InstallationCost.ExtraInstallationCharges
{
    [Table("ExtraInstallationCharges")]
    public class ExtraInstallationCharge : FullAuditedEntity
    {
        public virtual string Category { get; set; }

        public virtual int NameId { get; set; }

        [ForeignKey("StateId")]
        public State StateFk { get; set; }

        public int StateId { get; set; }

        public string Type { get; set; }

        public decimal? MetroAmount { get; set; }

        public decimal? RegionalAmount { get; set; }
    }
}
