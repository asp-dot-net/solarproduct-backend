﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.InstallationCost.BatteryInstallationCost
{
    [Table("BatteryInstallationCosts")]
    public class BatteryInstallationCost : FullAuditedEntity
    {
        public decimal FixCost { get; set; }
        
        public decimal Kw { get; set; }
        
        public decimal ExtraCost { get; set; }
    }
}
