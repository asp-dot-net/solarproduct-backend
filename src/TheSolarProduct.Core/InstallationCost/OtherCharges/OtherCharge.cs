﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheSolarProduct.InstallationCost.OtherCharges
{
    [Table("OtherCharges")]
    public class OtherCharge : FullAuditedEntity
    {
        [Required]
        public virtual string Name { get; set; }
    }
}
