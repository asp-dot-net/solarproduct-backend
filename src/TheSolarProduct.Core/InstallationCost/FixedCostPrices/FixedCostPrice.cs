﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheSolarProduct.InstallationCost.FixedCostPrices
{
    [Table("FixedCostPrice")]
    public class FixedCostPrice : FullAuditedEntity
    {
        public virtual string Name { get; set; }

        public virtual string Type { get; set; }

        public virtual decimal? Cost { get; set; }
    }
}
