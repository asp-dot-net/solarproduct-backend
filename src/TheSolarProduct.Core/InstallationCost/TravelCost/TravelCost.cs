﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.InstallationCost.TravelCost
{
    [Table("TravelCosts")]
    public class TravelCost : FullAuditedEntity
    {
        public decimal StartKm { get; set; }

        public decimal EndKm { get; set; }

        public decimal ExtraCostPerKm { get; set; }
    }
}
