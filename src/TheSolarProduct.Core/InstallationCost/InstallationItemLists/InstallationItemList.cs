﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.InstallationCost.InstallationItemLists
{
    [Table("InstallationItemLists")]
    public class InstallationItemList : FullAuditedEntity
    {
        [ForeignKey("InstallationItemPeriodId")]
        public InstallationItemPeriod InstallationItemPeriodFk { get; set; }

        public virtual int InstallationItemPeriodId { get; set; }

        [ForeignKey("ProductItemId")]
        public ProductItem ProductItemFk { get; set; }

        public virtual int ProductItemId { get; set; }

        public virtual decimal? PricePerWatt { get; set; }

        public virtual decimal? UnitPrice { get; set; }
    }
}
