﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheSolarProduct.InstallationCost.InstallationItemLists
{
    [Table("InstallationItemPeriods")]
    public class InstallationItemPeriod : FullAuditedEntity
    {
        [ForeignKey("OrganizationUnit")]
        public OrganizationUnit OrganizationUnitFk { get; set; }

        public virtual long OrganizationUnit { get; set; }

        public virtual string Name { get; set; }

        public virtual DateTime StartDate { get; set; }

        public virtual DateTime EndDate { get; set; }
    }
}
