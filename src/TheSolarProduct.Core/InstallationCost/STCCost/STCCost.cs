﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.InstallationCost.STCCost
{
    [Table("STCCosts")]
    public class STCCost : FullAuditedEntity
    {
        public decimal Cost { get; set; }
    }
}
