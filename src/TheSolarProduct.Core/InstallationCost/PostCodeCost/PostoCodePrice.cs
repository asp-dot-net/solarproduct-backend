﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.InstallationCost.PostCodeCost
{
    [Table("PostoCodePrices")]
    public class PostoCodePrice : FullAuditedEntity
    {
        public string PostCode { get; set; }

        public decimal? Price { get; set; }

        public int? PostCodePricePeriodId { get; set; }

        [ForeignKey("PostCodePricePeriodId")]
        public PostCodePricePeriod PostCodePricePeriodFk { get; set; }
    }
}
