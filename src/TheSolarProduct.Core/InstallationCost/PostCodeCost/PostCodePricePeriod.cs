﻿using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.InstallationCost.PostCodeCost
{
    [Table("PostCodePricePeriods")]
    public class PostCodePricePeriod : FullAuditedEntity
    {
        public virtual string Name { get; set; }

        public virtual int Month { get; set; }

        public virtual int Year { get; set; }
    }
}