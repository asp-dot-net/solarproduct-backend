﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;
using TheSolarProduct.States;

namespace TheSolarProduct.InstallationCost.StateWiseInstallationCosts
{
    [Table("StateWiseInstallationCosts")]
    public class StateWiseInstallationCost : FullAuditedEntity
    {
        [ForeignKey("StateId")]
        public State StateFk { get; set; }

        [Required]
        public int StateId { get; set; }

        public decimal? Metro { get; set; }

        public decimal? Regional { get; set; }
    }
}
