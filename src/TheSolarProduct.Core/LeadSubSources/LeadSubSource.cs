﻿using TheSolarProduct.LeadSources;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.LeadSubSources
{
	[Table("LeadSubSources")]
    public class LeadSubSource : FullAuditedEntity , IMustHaveTenant
    {
			public int TenantId { get; set; }
			

		[Required]
		[StringLength(LeadSubSourceConsts.MaxNameLength, MinimumLength = LeadSubSourceConsts.MinNameLength)]
		public virtual string Name { get; set; }
		

		public virtual int LeadSourceId { get; set; }
		
        [ForeignKey("LeadSourceId")]
		public LeadSource LeadSourceFk { get; set; }
		
    }
}