﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.RECDatas 
{ 
    public class RECData : FullAuditedEntity
    {
        public string PVDNumber { get; set; }

        public string JobNumber { get; set; }

        public int NoOfPanels { get; set; }

        public DateTime CreationDate { get; set; }

        public int RECCreated { get; set; }

        public int RECPassed { get; set; }

        public int RECPanding { get; set; }

        public int RECFailed { get; set; }

        public int RECRegistered { get; set; }

        public DateTime? LastAuditedDate { get; set; }

        public long BlukUploadId { get; set; }

        public string Exception { get; set; }

        public bool CanBeImported()
        {
            return string.IsNullOrEmpty(Exception);
        }
    }
}
