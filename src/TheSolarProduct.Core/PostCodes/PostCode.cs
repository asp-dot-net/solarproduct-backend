﻿using TheSolarProduct.States;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.PostCodes
{
	[Table("PostCodes")]
    public class PostCode : FullAuditedEntity 
    {

		[Required]
		[RegularExpression(PostCodeConsts.PostalCodeRegex)]
		[StringLength(PostCodeConsts.MaxPostalCodeLength, MinimumLength = PostCodeConsts.MinPostalCodeLength)]
		public virtual string PostalCode { get; set; }
		
		[Required]
		[StringLength(PostCodeConsts.MaxSuburbLength, MinimumLength = PostCodeConsts.MinSuburbLength)]
		public virtual string Suburb { get; set; }
		
		//[Required]
		[StringLength(PostCodeConsts.MaxPOBoxesLength, MinimumLength = PostCodeConsts.MinPOBoxesLength)]
		public virtual string POBoxes { get; set; }
		
		[StringLength(PostCodeConsts.MaxAreaLength, MinimumLength = PostCodeConsts.MinAreaLength)]
		public virtual string Area { get; set; }
		
		public virtual int StateId { get; set; }
		
        [ForeignKey("StateId")]
		public State StateFk { get; set; }

		public virtual string Areas { get; set; }
		public virtual Boolean IsActive {  get; set; }
	}
}