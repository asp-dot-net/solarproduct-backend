﻿using System.Threading.Tasks;
using TheSolarProduct.Authorization.Users;

namespace TheSolarProduct.WebHooks
{
    public interface IAppWebhookPublisher
    {
        Task PublishTestWebhook();
    }
}
