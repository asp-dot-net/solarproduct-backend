﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.ServiceCategorys;

namespace TheSolarProduct.ServiceCategoryDocs
{
    [Table("ServiceCategoryDocs")]
    public class ServiceCategoryDoc : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual int? SubCategoryId { get; set; }
        public virtual string FileName { get; set; }

        public virtual string FileType { get; set; }

        public virtual string FilePath { get; set; }
    }
}
