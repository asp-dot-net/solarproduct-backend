﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.EmailSetting
{
    public class EmailProviders : FullAuditedEntity
    {
        public string Name { get; set; }
    }
}
