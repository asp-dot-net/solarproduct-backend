﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.PurchaseDocumentListes
{
    [Table("PurchaseDocumentListes")]
    public class PurchaseDocumentList : FullAuditedEntity
    {
        public string Name { get; set; }
        public int Size { get; set; }
        public string Format { get; set; }
        public bool? IsActive { get; set; }
    }
}
