﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.WholeSaleLeads;

namespace TheSolarProduct.Vendors
{
    [Table("VendorContacts")]
    public class VendorContact : FullAuditedEntity
    {
        public int? VendorId { get; set; }
       
        [ForeignKey("VendorId")]
        public Vendor VendorFK { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
