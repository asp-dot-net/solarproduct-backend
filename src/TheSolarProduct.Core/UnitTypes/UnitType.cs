﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.UnitTypes
{
	[Table("UnitTypes")]
    public class UnitType : FullAuditedEntity 
    {

		[Required]
		[StringLength(UnitTypeConsts.MaxNameLength, MinimumLength = UnitTypeConsts.MinNameLength)]
		public virtual string Name { get; set; }
        public virtual Boolean IsActive { get; set; }
    }
}