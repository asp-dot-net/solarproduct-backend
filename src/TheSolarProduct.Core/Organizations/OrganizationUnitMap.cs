﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Organizations
{
    [Table("OrganizationUnitMaps")]
    public class OrganizationUnitMap : FullAuditedEntity
    {
        public long OrganizationUnitId { get; set; }

        public string MapProvider { get; set; }

        public string MapApiKey { get; set; }
    }
}
