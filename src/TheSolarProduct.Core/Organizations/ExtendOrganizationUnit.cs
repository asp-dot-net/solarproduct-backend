﻿using Abp.Organizations;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Organizations
{
	public class ExtendOrganizationUnit : OrganizationUnit
	{
		//public ExtendOrganizationUnit(int? tenantId, string projectId, string displayName, string organizationCode, long? parentId = null)
		//{
		//}

		public virtual string OrganizationCode { get; set; }

		public virtual string ProjectId { get; set; }

		public virtual string Logo { get; set; }

		public virtual string ABN { get; set; }

		public virtual string Address { get; set; }

		public virtual string ContactNo { get; set; }

		public virtual string GreenBoatUsername { get; set; }
		public virtual string GreenBoatPassword { get; set; }
		public virtual string defaultFromAddress { get; set; }
		public virtual string defaultFromDisplayName { get; set; }
		public virtual string FoneDynamicsPhoneNumber { get; set; }
		public virtual string FoneDynamicsPropertySid { get; set; }
		public virtual string FoneDynamicsAccountSid { get; set; }
		public virtual string FoneDynamicsToken { get; set; }
		public virtual string Mobile { get; set; }
		public virtual string Email { get; set; }
		public virtual string LogoFileName { get; set; }
		public virtual string LogoFilePath { get; set; } 
		public string paymentMethod { get; set; }
		public string cardNumber { get; set; }
		public string expiryDateMonth { get; set; }
		public string expiryDateYear { get; set; }
		public string cvn { get; set; }
		public string cardholderName { get; set; }
		public string SecrateKey { get; set; }
		public string PublishKey { get; set; }
		public string MerchantId { get; set; }
		public string businesscode { get; set; }
		public virtual string AuthorizationKey { get; set; }
		public virtual string WestPacSecreteKey { get; set; }
		public virtual string WestPacPublishKey { get; set; }
		public virtual string SurchargeAuthorizationKey { get; set; }
		public virtual string SurchargeSecrateKey { get; set; }
		public virtual string SurchargePublishKey { get; set; }
		public virtual string GreenBoatUsernameForFetch { get; set; }
		public virtual string GreenBoatPasswordForFetch { get; set; }
		public virtual string ABNNumber { get; set; }

		public bool IsDefault { get; set; }
        //public virtual string MapProvider { get; set; }

        //public virtual string MapApiKey { get; set; }

        public virtual string AccountId { get; set; }

        public virtual string GateWayAccountEmail { get; set; }

        public virtual string GateWayAccountPassword { get; set; }

        public virtual string smsFrom { get; set; }

		public virtual int? AutoAssignLeadUserId { get; set; }

		public virtual bool? EnableAutoAssignLead { get; set; }

		public long? ProjectOrderNo { get; set; }
    }
}
