﻿using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Organizations
{
    [Table("STCProviders")]
    public class STCProvider : FullAuditedEntity
    {
        public long? OrganizationUnitId { get; set; }

        public string Provider { get; set; }

        public string Name { get; set; }

        public string UserId { get; set; }

        public string Password { get; set; }
    }
}
