﻿using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Organizations;

namespace TheSolarProduct.WholeSaleEmailTemplates
{
    [Table("WholeSaleEmailTemplates")]
    public class WholeSaleEmailTemplate : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public long OrganizationUnitId { get; set; }

        [ForeignKey("OrganizationUnitId")]
        public OrganizationUnit OrganizationUnitFk { get; set; }

        public string TemplateName { get; set; }

        public string ViewHtml { get; set; }

        public bool IsActive { get; set; }
    }
}
