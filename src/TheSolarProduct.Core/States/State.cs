﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.States
{
    [Table("State")]
    public class State : FullAuditedEntity
    {
        [Required]
        [MaxLength(StateConsts.MaxStateNameLength)]
        public virtual string Name { get; set; }
        public virtual Boolean IsReport { get; set; }

        public decimal? BatteryRebate { get; set; }

        public bool IsActiveBatteryRebate { get; set; }

        public decimal? BatteryBess { get; set; }
    }
}
