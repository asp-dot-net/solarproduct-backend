﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.TransportationCosts
{
	[Table("TransportationCosts")]
    public class TransportationCost : FullAuditedEntity
    {
        public int? TransportCompanyId { get; set; }

        [ForeignKey("TransportCompanyId")]
        public TransportCompany TransportCompanyFK { get; set; }

        public string Mobile { get; set; }

        public string TrackingNo { get; set; }

        public decimal? Amount { get; set; }

        public string Notes { get; set; }

    }
}
