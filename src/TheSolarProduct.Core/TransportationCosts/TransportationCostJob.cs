﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.TransportationCosts
{
	[Table("TransportationCostJobs")]
    public class TransportationCostJob : FullAuditedEntity
    {

        public int? TransportationCostId { get; set; }

        [ForeignKey("TransportationCostId")]
        public TransportationCost TransportationCostFK { get; set; }

        public int? JobId { get; set; }

        [ForeignKey("JobId")]
        public Job JobFK { get; set; }


    }
}
