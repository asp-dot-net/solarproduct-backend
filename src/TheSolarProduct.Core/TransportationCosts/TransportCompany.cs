﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.TransportationCosts
{
	[Table("TransportCompanies")]
    public class TransportCompany : FullAuditedEntity
    {
          public string CompanyName { get; set; }
       
    }
}
