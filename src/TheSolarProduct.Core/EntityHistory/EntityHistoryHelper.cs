﻿using TheSolarProduct.Jobs;
using TheSolarProduct.Promotions;
using System;
using System.Linq;
using Abp.Organizations;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.MultiTenancy;

namespace TheSolarProduct.EntityHistory
{
    public static class EntityHistoryHelper
    {
        public const string EntityHistoryConfigurationName = "EntityHistory";

        public static readonly Type[] HostSideTrackedTypes =
        {
            typeof(OrganizationUnit), typeof(Role), typeof(Tenant)
        };

        public static readonly Type[] TenantSideTrackedTypes =
        {
            typeof(Job),
            typeof(PromotionResponseStatus),
            typeof(PromotionType),
            typeof(OrganizationUnit), typeof(Role)
        };

        public static readonly Type[] TrackedTypes =
            HostSideTrackedTypes
                .Concat(TenantSideTrackedTypes)
                .GroupBy(type => type.FullName)
                .Select(types => types.First())
                .ToArray();
    }
}
