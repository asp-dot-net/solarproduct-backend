﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Leads;

namespace TheSolarProduct.LeadGenAppointments
{
    [Table("LeadGenAppointments")]
    public class LeadGenAppointment : FullAuditedEntity
    {
        public DateTime AppointmentDate { get; set; }

        public long AppointmentFor { get; set; }

        [ForeignKey("AppointmentFor")]
        public User UserFK { get; set; }

        public string Notes { get; set; }

        public int LeadId { get; set; }

        [ForeignKey("LeadId")]
        public Lead LeadFK { get; set; }

        public DateTime CommitionDate { get; set; } 

        public decimal CommitionAmount { get; set;}

        public string CommissionNote { get; set; }  
    }
}
