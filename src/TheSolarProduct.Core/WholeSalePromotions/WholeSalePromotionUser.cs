﻿using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Leads;
using TheSolarProduct.WholeSaleLeads;

namespace TheSolarProduct.WholeSalePromotions
{
    [Table("WholeSalePromotionUsers")]
    public class WholeSalePromotionUser : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }


        public virtual DateTime ResponseDate { get; set; }

        public virtual string ResponseMessage { get; set; }


        public virtual int? WholeSalePromotionId { get; set; }

        [ForeignKey("WholeSalePromotionId")]
        public WholeSalePromotion WholeSalePromotionFk { get; set; }

        public virtual int? WholeSaleLeadId { get; set; }

        [ForeignKey("WholeSaleLeadId")]
        public WholeSaleLead WholeSaleLeadFk { get; set; }

        public virtual int? WholeSalePromotionResponseStatusId { get; set; }

        [ForeignKey("WholeSalePromotionResponseStatusId")]
        public WholeSalePromotionResponseStatus WholeSalePromotionResponseStatusFk { get; set; }
    }
}
