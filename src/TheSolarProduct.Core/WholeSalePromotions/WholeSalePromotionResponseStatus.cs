﻿using Abp.Auditing;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheSolarProduct.WholeSalePromotions
{
    [Table("WholeSalePromotionResponseStatuses")]
    [Audited]
    public class WholeSalePromotionResponseStatus : Entity
    {	
        [Required]
        [StringLength(WholeSalePromotionResponseStatusConsts.MaxNameLength, MinimumLength = WholeSalePromotionResponseStatusConsts.MinNameLength)]
        public virtual string Name { get; set; }
    
    }
}
