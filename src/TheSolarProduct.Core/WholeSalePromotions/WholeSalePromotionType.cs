﻿using Abp.Auditing;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheSolarProduct.WholeSalePromotions
{
    [Table("WholeSalePromotionTypes")]
    [Audited]
    public class WholeSalePromotionType : Entity
    {

        [Required]
        [StringLength(WholeSalePromotionTypeConsts.MaxNameLength, MinimumLength = WholeSalePromotionTypeConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public virtual string IconClass { get; set; }

    }
}
