﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Wholesales.DataVault
{
    [Table("WholeSaleDataVaultSections")]
    public class WholeSaleDataVaultSection : FullAuditedEntity
    {
        public virtual string SectionName { get; set; }
    }
}
