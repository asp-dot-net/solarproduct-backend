﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.DataVaults;

namespace TheSolarProduct.Wholesales.DataVault
{
    [Table("WholeSaleDataVaultActivityLogs")]
    public class WholeSaleDataVaultActivityLogs : FullAuditedEntity
    {
        public string Action { get; set; }

        public virtual string ActionNote { get; set; }
        public virtual string Type { get; set; }

        public int? SectionId { get; set; }

        [ForeignKey("SectionId")]
        public WholeSaleDataVaultSection SectionFK { get; set; }
    }
}
