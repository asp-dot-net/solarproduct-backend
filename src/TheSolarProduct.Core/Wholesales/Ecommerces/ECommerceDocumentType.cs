﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("ECommerceDocumentTypes")]
    public class ECommerceDocumentType : FullAuditedEntity
    {
        public string DocumentType { get; set; }

        public bool IsActive { get; set; }
    }
}
