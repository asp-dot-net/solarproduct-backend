﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("EcommerceProductItemSpecifications")]
    public class EcommerceProductItemSpecification : FullAuditedEntity
    {

        public int? SpecificationId { get; set; }

        [ForeignKey("SpecificationId")]
        public EcommerceSpecification EcommerceSpecificationfk { get; set; }

        public int? EcommerceProductItemId { get; set; }

        [ForeignKey("EcommerceProductItemId")]
        public EcommerceProductItem EcommerceProductItemFK { get; set; }

        public string Value { get; set; }





    }
}
