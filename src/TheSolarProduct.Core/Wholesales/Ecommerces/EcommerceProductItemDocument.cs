﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("EcommerceProductItemDocuments")]
    public class EcommerceProductItemDocument : FullAuditedEntity
    {
        public int? EcommerceProductItemId { get; set; }

        [ForeignKey("EcommerceProductItemId")]
        public EcommerceProductItem EcommerceProductItemFK { get; set; }

        public int? DocumentTypeId { get; set; }

        [ForeignKey("DocumentTypeId")]
        public ECommerceDocumentType DocumentTypeFK { get; set; }

        public string DocumentName { get; set; }    

        public string Documemt { get; set; }


    }
}
