﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("SpecialOfferProducts")]
    public class SpecialOfferProduct : FullAuditedEntity
    {
        public int? SpecialOfferId { get; set; }

        [ForeignKey("SpecialOfferId")]
        public SpecialOffer SpecialOfferFK { get; set; }

        public int? ProductItemId { get; set; }

        [ForeignKey("ProductItemId")]
        public EcommerceProductItem ProductItemFK { get; set; }

        public decimal? OldPrice { get; set; }

        public decimal? NewPrice { get; set;}
    }
}
