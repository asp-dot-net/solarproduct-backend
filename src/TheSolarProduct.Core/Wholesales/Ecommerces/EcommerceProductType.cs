﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("EcommerceProductTypes")]
    public class EcommerceProductType : FullAuditedEntity
    {
        public string Type { get; set; }
    }
}
