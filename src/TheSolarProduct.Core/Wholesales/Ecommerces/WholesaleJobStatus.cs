﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("WholesaleJobStatuses")]
    public class WholesaleJobStatus : FullAuditedEntity
    {
        public string Name { get; set; }
        public bool? IsActive { get; set; }
    }
}
