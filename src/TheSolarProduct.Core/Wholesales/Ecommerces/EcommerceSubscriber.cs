﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("EcommerceSubscribers")]
    public class EcommerceSubscriber : FullAuditedEntity
    {
        public string SubscriberEmail { get; set; }
    }
}
