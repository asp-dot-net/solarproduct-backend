﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("EcommerceSolarPackages")]
    public class EcommerceSolarPackage : FullAuditedEntity
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public bool IsActive { get; set; }

        public string Banner { get; set; }
    }
}
