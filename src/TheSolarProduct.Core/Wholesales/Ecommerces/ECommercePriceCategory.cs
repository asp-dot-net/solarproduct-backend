﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("ECommercePriceCategorys")]
    public class ECommercePriceCategory : FullAuditedEntity
    {
        public string PriceCategory { get; set; }

        public bool IsActive { get; set; }
    }
}
