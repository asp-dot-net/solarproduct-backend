﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("EcomerceSpecialStatuses")]
    public class EcomerceSpecialStatus : FullAuditedEntity
    {
        public string Status { get; set; }

        public bool IsActive { get; set; }
    }
}
