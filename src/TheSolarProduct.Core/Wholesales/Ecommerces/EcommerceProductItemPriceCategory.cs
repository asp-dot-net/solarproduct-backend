﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("EcommerceProductItemPriceCategorys")]
    public class EcommerceProductItemPriceCategory : FullAuditedEntity
    {
        public int? EcommerceProductItemId { get; set; }

        [ForeignKey("EcommerceProductItemId")]
        public EcommerceProductItem EcommerceProductItemFK { get; set; }

        public int? ECommercePriceCategoryId { get; set; }

        [ForeignKey("ECommercePriceCategoryId")]
        public ECommercePriceCategory ECommercePriceCategoryFk { get; set; }

        public decimal? Price { get; set; }
    }
}
