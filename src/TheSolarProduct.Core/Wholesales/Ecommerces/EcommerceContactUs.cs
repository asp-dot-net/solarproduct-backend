﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.WholeSaleLeads;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("EcommerceContactUses")]
    public class EcommerceContactUs : FullAuditedEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }   

        public string Subject { get; set; }

        public string Message { get; set; }

        public int? WholesaleLeadId { get; set; }

		[ForeignKey("WholesaleLeadId")]
        public WholeSaleLead WholeSaleLeadFk { get; set; }
    }
}
