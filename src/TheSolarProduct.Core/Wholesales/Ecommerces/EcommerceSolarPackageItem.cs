﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("EcommerceSolarPackageItems")]
    public class EcommerceSolarPackageItem : FullAuditedEntity
    {
        public int EcommerceSolarPackageId { get; set; }

        [ForeignKey("EcommerceSolarPackageId")]
        public EcommerceSolarPackage EcommerceSolarPackagFK { get; set; }

        public int ProductItemId { get; set; }

        [ForeignKey("ProductItemId")]
        public ProductItem ProductItemFk { get; set; }

        public int Quantity { get; set; }
    }
}
