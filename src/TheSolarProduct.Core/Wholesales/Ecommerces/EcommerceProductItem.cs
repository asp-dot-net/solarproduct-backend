﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("EcommerceProductItems")]
    public class EcommerceProductItem : FullAuditedEntity
    {
        public int? ProductCategoryId { get; set; }

        [ForeignKey("ProductCategoryId")]
        public ProductType ProductTypeFK { get; set; }

        public int? ProductItemID { get; set; }

        [ForeignKey("ProductItemID")]
        public ProductItem ProductItemFk { get; set; }

        public string ProductDescription { get; set; }

        public bool IsDiscounted { get; set; }

        public bool IsNewArrival { get; set; }

        public bool IsFeatured { get; set; }

        public int? Rating { get; set; }

        public int? SpecialStatusId { get; set; }

        [ForeignKey("SpecialStatusId")]
        public EcomerceSpecialStatus SpecialStatusFk { get; set; }

        public bool IsActive { get; set; }

        public int? BrandPartnerId { get; set; }

        [ForeignKey("BrandPartnerId")]
        public BrandingPartner BrandPartnerFK { get; set; }

        public int? SeriesId { get; set; }

        [ForeignKey("SeriesId")]
        public Series SeriesFK { get; set; }

        public string ProductShortDescription { get; set; }

        public string Specification { get; set; }

        public int? EcommerceProductTypeId { get; set; }

        [ForeignKey("EcommerceProductTypeId")]
        public EcommerceProductType EcommerceProductTypeFK { get; set; }

    }
}
