﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("BrandingPartners")]
    public class BrandingPartner : FullAuditedEntity
    {
        public int? ProductCategoryId { get; set; }

        [ForeignKey("ProductCategoryId")]
        public ProductType ProductTypeFK { get; set; }

        public string BrandName { get; set; }   

        public string Logo { get; set; }
    }
}
