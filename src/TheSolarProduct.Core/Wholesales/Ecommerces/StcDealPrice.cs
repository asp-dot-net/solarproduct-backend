﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("StcDealPrices")]
    public class StcDealPrice : FullAuditedEntity    
    {
     public decimal? Value {  get; set; }
    }
}
