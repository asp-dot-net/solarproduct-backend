﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("EcommerceCartProductItems")]
    public class EcommerceCartProductItem : FullAuditedEntity
    {
        public int? UserId { get; set; }

        public int? EcommerceProductItemId { get; set; }

        [ForeignKey("EcommerceProductItemId")]
        public EcommerceProductItem EcommerceProductItemFK { get; set; }

        public int? QTY { get; set; }

        public bool Purcahse { get; set; }

        public decimal? Price { get; set; }

        public int? WholesaleInvoiceId { get; set; }

        [ForeignKey("WholesaleInvoiceId")]
        public WholesaleInvoice WholesaleInvoiceFK { get; set; }
    }
}
