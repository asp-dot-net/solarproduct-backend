﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("Serieses")]
    public class Series : FullAuditedEntity
    {
        public string SeriesName { get; set; }

        public int? ProductCategoryId { get; set; }

        [ForeignKey("ProductCategoryId")]
        public ProductType ProductTypeFK { get; set; }
    }
}
