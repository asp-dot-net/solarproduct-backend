﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    public class EcommerceProductItemImage : FullAuditedEntity
    {
        public int? EcommerceProductItemId { get; set; }

        [ForeignKey("EcommerceProductItemId")]
        public EcommerceProductItem EcommerceProductItemFK { get; set; }

        public string ImageName { get; set; }

        public string Image { get; set; }
    }
}
