﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Wholesales.Ecommerces
{
    [Table("SpecialOffers")]
    public class SpecialOffer : FullAuditedEntity
    {
        public string OfferTitle { get; set; }

        public DateTime? DealStarts { get; set; }

        public DateTime? DealEnds { get; set; }

        public bool IsOfferActive { get; set; }
    }
}
