﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;
using TheSolarProduct.WholeSaleLeads;
using TheSolarProduct.Wholesales.Ecommerces;

namespace TheSolarProduct.Wholesales
{
    [Table("WholesaleInvoices")]
    public class WholesaleInvoice : FullAuditedEntity
    {
        public int? InvoiceTypeId { get; set; }

        [ForeignKey("InvoiceTypeId")]
        public WholesaleInvoicetype WholesaleInvoicetypeFK { get; set; }

        public string InvoiceNo { get; set; }

        public int? JobTypeId { get; set; }

        [ForeignKey("JobTypeId")]
        public WholesaleJobType WholesaleJobTypeFK { get; set; }

        public int? WholesaleLeadId { get; set; }

        [ForeignKey("WholesaleLeadId")]
        public WholeSaleLead WholeSaleLeadFK { get; set; }

        public decimal? CreditAmount { get; set; }

        public string Reference { get; set;  }

        public string DeliveryOptions { get; set; }

        public int? JobStatusId { get; set; }

        [ForeignKey("JobStatusId")]
        public WholesaleJobStatus WholesaleJobStatusFK { get; set; }

        public int? StoreLoactionId { get; set; }   

        [ForeignKey("StoreLoactionId")]
        public Warehouselocation StoreLocationFK { get; set; }

        public decimal? InvoiceAmount { get; set; }

        public decimal? InvoiceAmountWithGST { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public string DeliveryTime { get; set; }

        public int? TansportTypeId { get; set; }

        [ForeignKey("TansportTypeId")]
        public WholesaleTransportType WholesaleTransportTypeFK { get; set; }

        public string Consign { get; set; }

        public decimal? ActualFreigthCharges { get; set; }
        
        public decimal? CustFreigthCharges { get; set; }

        public int? PropertyTypeId { get; set; }

        [ForeignKey("PropertyTypeId")]
        public WholesalePropertyType WholesalePropertyTypeFK { get; set; }

        public int? PVDStatusId { get; set; }

        [ForeignKey("PVDStatusId")]
        public WholesalePVDStatus WholesalePVDStatusFK { get; set; }

        public string STCID { get; set; }

        public int? NoOfSTC { get; set; }

        public decimal? STCRate { get; set; }

        public string PVDNumber { get; set; }

    }
}
