﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;
using TheSolarProduct.PriceItemLists;
using TheSolarProduct.WholeSaleLeads;

namespace TheSolarProduct.BonusList
{
  
    [Table("BonusCommissionItems")]
    public class BonusCommissionItem : FullAuditedEntity
    {
        public int? BonusId { get; set; }

        [ForeignKey("BonusId")]
        public Bonus BonusItemFK { get; set; }

        public decimal? Amount { get; set; }

        public int? JobId { get; set; }

        [ForeignKey("JobId")]
        public Job JobFK { get; set; }

        public DateTime? CommitionDate { get; set; }

        public string CommissionNote { get; set; }

        public int? UserId { get; set; }

        public string Notes { get; set; }
    }


}
