﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.BonusList
{

    [Table("Bonuses")]
    public class Bonus : FullAuditedEntity
    {
        public virtual string Name { get; set; }
        public virtual Boolean IsActive { get; set; }
    }
}
