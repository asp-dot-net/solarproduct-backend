﻿using Abp.Domain.Entities.Auditing;
using IdentityServer4.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.PostCodes;

namespace TheSolarProduct.PostCodeRange
{
    [Table("PostCodeRanges")]
    public class PostCodeRange : FullAuditedEntity
    {
        [Required]
        [RegularExpression(PostCodeConsts.PostalCodeRegex)]
        [StringLength(PostCodeConsts.MaxPostalCodeLength, MinimumLength = PostCodeConsts.MinPostalCodeLength)]
        public virtual long StartPostCode { get; set; }

        [Required]
        [RegularExpression(PostCodeConsts.PostalCodeRegex)]
        [StringLength(PostCodeConsts.MaxPostalCodeLength, MinimumLength = PostCodeConsts.MinPostalCodeLength)]
        public virtual long EndPostCode { get; set; }

        public virtual string Area { get; set; }
        public virtual Boolean IsActive { get; set; }
    }
}