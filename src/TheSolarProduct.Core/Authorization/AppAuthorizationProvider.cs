﻿using Abp.Application.Features;
using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;
using TheSolarProduct.Features;
using TheSolarProduct.PostCodes;
using TheSolarProduct.StockOrderStatuses;
using TheSolarProduct.Wholesales.Ecommerces;

namespace TheSolarProduct.Authorization
{
	/// <summary>
	/// Application's authorization provider.
	/// Defines permissions for the application.
	/// See <see cref="AppPermissions"/> for all permission names.
	/// </summary>
	public class AppAuthorizationProvider : AuthorizationProvider
	{
		private readonly bool _isMultiTenancyEnabled;

		public AppAuthorizationProvider(bool isMultiTenancyEnabled)
		{
			_isMultiTenancyEnabled = isMultiTenancyEnabled;
		}

		public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
		{
			_isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
		}

		public override void SetPermissions(IPermissionDefinitionContext context)
		{
			//COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

			var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));


            //TENANT-SPECIFIC PERMISSIONS

            //IP Address Based Login
            var IpAddresss = context.GetPermissionOrNull(AppPermissions.LoginWithIPAddress) ?? context.CreatePermission(AppPermissions.LoginWithIPAddress, L("LoginWithIPAddress"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.IPAddressFeature));
            
            //User IN Out Time Login
            var UserInOutTime = context.GetPermissionOrNull(AppPermissions.UserInOutTime) ?? context.CreatePermission(AppPermissions.UserInOutTime, L("UserInOutTime"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.UserInOutTimeFeature));

            //pages.CreateChildPermission(AppPermissions.Pages_DemoUiComponents, L("DemoUiComponents"));

            var dashboard = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);
			#region Dashboard
			/// public const string Pages_Tenant_Dashboard_FilterSalesRepUsers = "Pages.Tenant.Dashboard.FilterSalesRepUsers";
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_UserLeads, L("UserLeadsWidget"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_ManagerLeads, L("ManagerLeadsWidget"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_OrganizationWiseLeads, L("OrganizationWiseLeads"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_OrganizationWiseLeadDetails, L("OrganizationWiseLeadDetails"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_OrganizationWiseTracker, L("OrganizationWiseTracker"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_MyReminders, L("MyReminders"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_SalesDetails, L("SalesDetails"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_InvoiceStatus, L("InvoiceStatus"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_ToDoDetails, L("ToDoDetails"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_ManagerSalesDetails, L("ManagerSalesDetails"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_ManagerToDoDetails, L("ManagerToDoDetails"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_LeadsSalesRank, L("LeadsSalesRank"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_LeadsDetails, L("LeadsDetails"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_LeadsStatus, L("LeadsStatus"), multiTenancySides: MultiTenancySides.Tenant);
			dashboard.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_LeadsStatusDetail, L("LeadsStatusDetail"), multiTenancySides: MultiTenancySides.Tenant);

			
            #endregion

            #region Retail
            var retail = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Retail, L("Retail"), multiTenancySides: MultiTenancySides.Tenant);

            var dashboardRetail = retail.CreateChildPermission(AppPermissions.Pages_Tenant_Retail_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);
            #region DashboardRetail
            dashboardRetail.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_Sales, L("Sales"), multiTenancySides: MultiTenancySides.Tenant);
            dashboardRetail.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_Invoice, L("Invoice"), multiTenancySides: MultiTenancySides.Tenant);
            dashboardRetail.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_Service, L("Service"), multiTenancySides: MultiTenancySides.Tenant);
            dashboardRetail.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_Dashboard2, L("Dashboard2"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion


            var datavaults = retail.CreateChildPermission(AppPermissions.Pages_DataVaults, L("DataVaults"));
            #region DataVaults

            //ActivityLog
            var ActivityLog = datavaults.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_ActivityLog, L("DataVaultActivityLog"), multiTenancySides: MultiTenancySides.Tenant);
            ActivityLog.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_ActivityLog_Detail, L("DataVaultActivityLogDetail"), multiTenancySides: MultiTenancySides.Tenant);
            ActivityLog.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_ActivityLog_History, L("DataVaultActivityLogHistory"), multiTenancySides: MultiTenancySides.Tenant);

            // CallFlowQueues Permissions
            var callFlowQueues = datavaults.CreateChildPermission(AppPermissions.Pages_CallFlowQueues, L("CallFlowQueues"), multiTenancySides: MultiTenancySides.Tenant);
            callFlowQueues.CreateChildPermission(AppPermissions.Pages_CallFlowQueues_Create, L("CreateNewCallFlowQueues"), multiTenancySides: MultiTenancySides.Tenant);
            callFlowQueues.CreateChildPermission(AppPermissions.Pages_CallFlowQueues_Edit, L("EditCallFlowQueues"), multiTenancySides: MultiTenancySides.Tenant);
            callFlowQueues.CreateChildPermission(AppPermissions.Pages_CallFlowQueues_Delete, L("DeleteCallFlowQueues"), multiTenancySides: MultiTenancySides.Tenant);
            callFlowQueues.CreateChildPermission(AppPermissions.Pages_CallFlowQueues_View, L("ViewCallFlowQueues"), multiTenancySides: MultiTenancySides.Tenant);

            //Cancel Reasons
            var cancelReasons = datavaults.CreateChildPermission(AppPermissions.Pages_CancelReasons, L("CancelReasons"), multiTenancySides: MultiTenancySides.Tenant);
            cancelReasons.CreateChildPermission(AppPermissions.Pages_CancelReasons_Create, L("CreateCancelReason"), multiTenancySides: MultiTenancySides.Tenant);
            cancelReasons.CreateChildPermission(AppPermissions.Pages_CancelReasons_Edit, L("EditCancelReason"), multiTenancySides: MultiTenancySides.Tenant);
            cancelReasons.CreateChildPermission(AppPermissions.Pages_CancelReasons_Delete, L("DeleteCancelReason"), multiTenancySides: MultiTenancySides.Tenant);

            //CheckActive
            var CheckActive = datavaults.CreateChildPermission(AppPermissions.Pages_CheckActive, L("CheckActive"), multiTenancySides: MultiTenancySides.Tenant);
            CheckActive.CreateChildPermission(AppPermissions.Pages_CheckActive_Create, L("CreateNewCheckActive"), multiTenancySides: MultiTenancySides.Tenant);
            CheckActive.CreateChildPermission(AppPermissions.Pages_CheckActive_Edit, L("EditCheckActive"), multiTenancySides: MultiTenancySides.Tenant);
            CheckActive.CreateChildPermission(AppPermissions.Pages_CheckActive_Delete, L("DeleteCheckActive"), multiTenancySides: MultiTenancySides.Tenant);
            CheckActive.CreateChildPermission(AppPermissions.Pages_CheckActive_Export, L("ExportCheckActive"), multiTenancySides: MultiTenancySides.Tenant);

            // CheckApplication
            var CheckApplication = datavaults.CreateChildPermission(AppPermissions.Pages_ChcekApplication, L("CheckApplication"), multiTenancySides: MultiTenancySides.Tenant);
            CheckApplication.CreateChildPermission(AppPermissions.Pages_CheckApplication_Create, L("CreateNewCheckApplication"), multiTenancySides: MultiTenancySides.Tenant);
            CheckApplication.CreateChildPermission(AppPermissions.Pages_CheckApplication_Edit, L("EditCheckApplication"), multiTenancySides: MultiTenancySides.Tenant);
            CheckApplication.CreateChildPermission(AppPermissions.Pages_CheckApplication_Delete, L("DeleteCheckApplication"), multiTenancySides: MultiTenancySides.Tenant);
            CheckApplication.CreateChildPermission(AppPermissions.Pages_CheckApplication_Export, L("ExportCheckApplication"), multiTenancySides: MultiTenancySides.Tenant);

            //Check DepositReceived
            var checkDepositReceived = datavaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_CheckDepositReceived, L("CheckDepositReceived"), multiTenancySides: MultiTenancySides.Tenant);
            checkDepositReceived.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_CheckDepositReceived_Create, L("CreateCheckDepositReceived"), multiTenancySides: MultiTenancySides.Tenant);
            checkDepositReceived.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_CheckDepositReceived_Edit, L("EditCheckDepositReceived"), multiTenancySides: MultiTenancySides.Tenant);
            checkDepositReceived.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_CheckDepositReceived_Delete, L("DeleteCheckDepositReceived"), multiTenancySides: MultiTenancySides.Tenant);
            checkDepositReceived.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_CheckDepositReceived_Export, L("ExportCheckDepositReceived"), multiTenancySides: MultiTenancySides.Tenant);

            //commissionRanges
            var commissionRanges = datavaults.CreateChildPermission(AppPermissions.Pages_CommissionRanges, L("CommissionRanges"), multiTenancySides: MultiTenancySides.Tenant);
            commissionRanges.CreateChildPermission(AppPermissions.Pages_CommissionRanges_Create, L("CreateNewCommissionRanges"), multiTenancySides: MultiTenancySides.Tenant);
            commissionRanges.CreateChildPermission(AppPermissions.Pages_CommissionRanges_Edit, L("EditCommissionRanges"), multiTenancySides: MultiTenancySides.Tenant);
            commissionRanges.CreateChildPermission(AppPermissions.Pages_CommissionRanges_Delete, L("DeleteCommissionRanges"), multiTenancySides: MultiTenancySides.Tenant);

            //DashboardMessage
            var DashboardMessage = datavaults.CreateChildPermission(AppPermissions.Pages_DashboardMessage, L("DashboardMessage"), multiTenancySides: MultiTenancySides.Tenant);
            DashboardMessage.CreateChildPermission(AppPermissions.Pages_DashboardMessage_Create, L("CreateNewDashboardMessage"), multiTenancySides: MultiTenancySides.Tenant);
            DashboardMessage.CreateChildPermission(AppPermissions.Pages_DashboardMessage_Edit, L("EditDashboardMessage"), multiTenancySides: MultiTenancySides.Tenant);
            DashboardMessage.CreateChildPermission(AppPermissions.Pages_DashboardMessage_Delete, L("DeleteDashboardMessage"), multiTenancySides: MultiTenancySides.Tenant);

            //documentlibrary
            var documentlibrary = datavaults.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_DocumentLibrary, L("DocumentLibrary"), multiTenancySides: MultiTenancySides.Tenant);
            documentlibrary.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_DocumentLibrary_Create, L("CreateNewDocumentLibrary"), multiTenancySides: MultiTenancySides.Tenant);
            documentlibrary.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_DocumentLibrary_Edit, L("EditDocumentLibrary"), multiTenancySides: MultiTenancySides.Tenant);
            documentlibrary.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_DocumentLibrary_Delete, L("DeleteDocumentLibrary"), multiTenancySides: MultiTenancySides.Tenant);

            //documenttypes
            var documenttypes = datavaults.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_DocumentType, L("DocumentType"), multiTenancySides: MultiTenancySides.Tenant);
            documenttypes.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_DocumentType_Create, L("CreateNewDocumentType"), multiTenancySides: MultiTenancySides.Tenant);
            documenttypes.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_DocumentType_Edit, L("EditDocumentType"), multiTenancySides: MultiTenancySides.Tenant);
            documenttypes.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_DocumentType_Delete, L("DeleteDocumentType"), multiTenancySides: MultiTenancySides.Tenant);

            //elecDistributors
            var elecDistributors = datavaults.CreateChildPermission(AppPermissions.Pages_ElecDistributors, L("ElecDistributors"), multiTenancySides: MultiTenancySides.Tenant);
            elecDistributors.CreateChildPermission(AppPermissions.Pages_ElecDistributors_Create, L("CreateElecDistributor"), multiTenancySides: MultiTenancySides.Tenant);
            elecDistributors.CreateChildPermission(AppPermissions.Pages_ElecDistributors_Edit, L("EditElecDistributor"), multiTenancySides: MultiTenancySides.Tenant);
            elecDistributors.CreateChildPermission(AppPermissions.Pages_ElecDistributors_Delete, L("DeleteElecDistributor"), multiTenancySides: MultiTenancySides.Tenant);

            //elecRetailers
            var elecRetailers = datavaults.CreateChildPermission(AppPermissions.Pages_ElecRetailers, L("ElecRetailers"), multiTenancySides: MultiTenancySides.Tenant);
            elecRetailers.CreateChildPermission(AppPermissions.Pages_ElecRetailers_Create, L("CreateElecRetailer"), multiTenancySides: MultiTenancySides.Tenant);
            elecRetailers.CreateChildPermission(AppPermissions.Pages_ElecRetailers_Edit, L("EditElecRetailer"), multiTenancySides: MultiTenancySides.Tenant);
            elecRetailers.CreateChildPermission(AppPermissions.Pages_ElecRetailers_Delete, L("DeleteElecRetailer"), multiTenancySides: MultiTenancySides.Tenant);

            //emailTemplates
            var emailTemplates = datavaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates, L("EmailTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            emailTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Create, L("CreateNewEmailTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            emailTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Edit, L("EditEmailTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            emailTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Delete, L("DeleteEmailTemplate"), multiTenancySides: MultiTenancySides.Tenant);

            //categories
            var categories = datavaults.CreateChildPermission(AppPermissions.Pages_Categories, L("Categories"), multiTenancySides: MultiTenancySides.Tenant);
            categories.CreateChildPermission(AppPermissions.Pages_Categories_Create, L("CreateCategory"), multiTenancySides: MultiTenancySides.Tenant);
            categories.CreateChildPermission(AppPermissions.Pages_Categories_Edit, L("EditCategory"), multiTenancySides: MultiTenancySides.Tenant);
            categories.CreateChildPermission(AppPermissions.Pages_Categories_Delete, L("DeleteCategory"), multiTenancySides: MultiTenancySides.Tenant);


            //freebieTransports
            var freebieTransports = datavaults.CreateChildPermission(AppPermissions.Pages_FreebieTransports, L("FreebiTransport"), multiTenancySides: MultiTenancySides.Tenant);
            freebieTransports.CreateChildPermission(AppPermissions.Pages_FreebieTransports_Create, L("CreateFreebieTransport"), multiTenancySides: MultiTenancySides.Tenant);
            freebieTransports.CreateChildPermission(AppPermissions.Pages_FreebieTransports_Edit, L("EditFreebiTransport"), multiTenancySides: MultiTenancySides.Tenant);
            freebieTransports.CreateChildPermission(AppPermissions.Pages_FreebieTransports_Delete, L("DeleteFreebieTransport"), multiTenancySides: MultiTenancySides.Tenant);


            //holdReasons
            var holdReasons = datavaults.CreateChildPermission(AppPermissions.Pages_HoldReasons, L("HoldReasons"), multiTenancySides: MultiTenancySides.Tenant);
            holdReasons.CreateChildPermission(AppPermissions.Pages_HoldReasons_Create, L("CreateNewHoldReason"), multiTenancySides: MultiTenancySides.Tenant);
            holdReasons.CreateChildPermission(AppPermissions.Pages_HoldReasons_Edit, L("EditHoldReason"), multiTenancySides: MultiTenancySides.Tenant);
            holdReasons.CreateChildPermission(AppPermissions.Pages_HoldReasons_Delete, L("DeleteHoldReason"), multiTenancySides: MultiTenancySides.Tenant);

            //houseTypes
            var houseTypes = datavaults.CreateChildPermission(AppPermissions.Pages_HouseTypes, L("HouseTyp"), multiTenancySides: MultiTenancySides.Tenant);
            houseTypes.CreateChildPermission(AppPermissions.Pages_HouseTypes_Create, L("CreateHouseType"), multiTenancySides: MultiTenancySides.Tenant);
            houseTypes.CreateChildPermission(AppPermissions.Pages_HouseTypes_Edit, L("EditHouseTyp"), multiTenancySides: MultiTenancySides.Tenant);
            houseTypes.CreateChildPermission(AppPermissions.Pages_HouseTypes_Delete, L("DeleteHouseTyp"), multiTenancySides: MultiTenancySides.Tenant);

            // Quotation Template Permissions
            var quotationTemplate = datavaults.CreateChildPermission(AppPermissions.Pages_QuotationTemplate, L("QuotationTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            quotationTemplate.CreateChildPermission(AppPermissions.Pages_QuotationTemplate_Create, L("CreateNewQuotationTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            quotationTemplate.CreateChildPermission(AppPermissions.Pages_QuotationTemplate_Edit, L("EditQuotationTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            quotationTemplate.CreateChildPermission(AppPermissions.Pages_QuotationTemplate_Delete, L("DeleteQuotationTemplate"), multiTenancySides: MultiTenancySides.Tenant);

            //installationCost
            var installationCost = datavaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost, L("InstallationCost"), multiTenancySides: MultiTenancySides.Tenant);
            //Battery Cost
            var batteryCost = installationCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_BatteryInstallationCost, L("BatteryInstallationCost"), multiTenancySides: MultiTenancySides.Tenant);
            batteryCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_BatteryInstallationCost_Create, L("CreateBatteryInstallationCost"), multiTenancySides: MultiTenancySides.Tenant);
            batteryCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_BatteryInstallationCost_Edit, L("EditBatteryInstallationCost"), multiTenancySides: MultiTenancySides.Tenant);
            batteryCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_BatteryInstallationCost_Delete, L("DeleteBatteryInstallationCost"), multiTenancySides: MultiTenancySides.Tenant);
            //CategoryInstallationItemList
            var CategoryInsItem = installationCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_CategoryInstallationItemList, L("CategoryInstallationItemList"), multiTenancySides: MultiTenancySides.Tenant);
            CategoryInsItem.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_CategoryInstallationItemList_Create, L("CreateCategoryInstallationItemList"), multiTenancySides: MultiTenancySides.Tenant);
            CategoryInsItem.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_CategoryInstallationItemList_Edit, L("EditCategoryInstallationItemList"), multiTenancySides: MultiTenancySides.Tenant);
            CategoryInsItem.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_CategoryInstallationItemList_Delete, L("DeleteCategoryInstallationItemList"), multiTenancySides: MultiTenancySides.Tenant);
            CategoryInsItem.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_CategoryInstallationItemList_View, L("ViewCategoryInstallationItemList"), multiTenancySides: MultiTenancySides.Tenant);
            CategoryInsItem.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_CategoryInstallationItemList_LastMonthEdit, L("LastMonthEdit"), multiTenancySides: MultiTenancySides.Tenant);
            //ExtraInstallationCharges
            var extraIns = installationCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_ExtraInstallationCharges, L("ExtraInstallationCharges"), multiTenancySides: MultiTenancySides.Tenant);
            extraIns.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_ExtraInstallationCharges_Create, L("CreateExtraInstallationCharges"), multiTenancySides: MultiTenancySides.Tenant);
            extraIns.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_ExtraInstallationCharges_Edit, L("EditExtraInstallationCharges"), multiTenancySides: MultiTenancySides.Tenant);
            extraIns.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_ExtraInstallationCharges_Delete, L("DeleteExtraInstallationCharges"), multiTenancySides: MultiTenancySides.Tenant);
            //Fixed Cost Price
            var fixedCost = installationCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_FixedCostPrice, L("FixedCostPrice"), multiTenancySides: MultiTenancySides.Tenant);
            fixedCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_FixedCostPrice_Create, L("CreateFixedCostPrice"), multiTenancySides: MultiTenancySides.Tenant);
            fixedCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_FixedCostPrice_Edit, L("EditFixedCostPrice"), multiTenancySides: MultiTenancySides.Tenant);
            fixedCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_FixedCostPrice_Delete, L("DeleteFixedCostPrice"), multiTenancySides: MultiTenancySides.Tenant);
            //Installation Item List
            var InsItem = installationCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_InstallationItemList, L("InstallationItemList"), multiTenancySides: MultiTenancySides.Tenant);
            InsItem.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_InstallationItemList_Create, L("CreateInstallationItemList"), multiTenancySides: MultiTenancySides.Tenant);
            InsItem.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_InstallationItemList_Edit, L("EditInstallationItemList"), multiTenancySides: MultiTenancySides.Tenant);
            InsItem.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_InstallationItemList_Delete, L("DeleteInstallationItemList"), multiTenancySides: MultiTenancySides.Tenant);
            InsItem.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_InstallationItemList_View, L("ViewInstallationItemList"), multiTenancySides: MultiTenancySides.Tenant);
            InsItem.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_InstallationItemList_LastMonthEdit, L("LastMonthEdit"), multiTenancySides: MultiTenancySides.Tenant);
            //Job Cost Fix Expense
            var JobCostFixExpense = installationCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_JobCostFixExpense, L("JobCostFixExpense"), multiTenancySides: MultiTenancySides.Tenant);
            JobCostFixExpense.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_JobCostFixExpense_Create, L("CreateJobCostFixExpense"), multiTenancySides: MultiTenancySides.Tenant);
            JobCostFixExpense.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_JobCostFixExpense_Edit, L("EditJobCostFixExpense"), multiTenancySides: MultiTenancySides.Tenant);
            JobCostFixExpense.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_JobCostFixExpense_Delete, L("DeleteJobCostFixExpense"), multiTenancySides: MultiTenancySides.Tenant);
            JobCostFixExpense.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_JobCostFixExpense_View, L("ViewJobCostFixExpense"), multiTenancySides: MultiTenancySides.Tenant);
            JobCostFixExpense.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_JobCostFixExpense_LastMonthEdit, L("LastMonthEdit"), multiTenancySides: MultiTenancySides.Tenant);
            //Other Charges
            var otherCharges = installationCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_OtherCharges, L("OtherCharges"), multiTenancySides: MultiTenancySides.Tenant);
            otherCharges.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_OtherCharges_Create, L("CreateOtherCharges"), multiTenancySides: MultiTenancySides.Tenant);
            otherCharges.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_OtherCharges_Edit, L("EditOtherCharges"), multiTenancySides: MultiTenancySides.Tenant);
            otherCharges.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_OtherCharges_Delete, L("DeleteOtherCharges"), multiTenancySides: MultiTenancySides.Tenant);
            //postcodeCost
            var postcodeCost = installationCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_PostCodeCost, L("PostCodeCost"), multiTenancySides: MultiTenancySides.Tenant);
            postcodeCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_PostCodeCost_Create, L("CreatePostCodeCost"), multiTenancySides: MultiTenancySides.Tenant);
            postcodeCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_PostCodeCost_Edit, L("EditPostCodeCost"), multiTenancySides: MultiTenancySides.Tenant);
            postcodeCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_PostCodeCost_Delete, L("DeletePostCodeCost"), multiTenancySides: MultiTenancySides.Tenant);
            postcodeCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_PostCodeCost_LastMonthEdit, L("LastMonthEdit"), multiTenancySides: MultiTenancySides.Tenant);
            postcodeCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_PostCodeCost_View, L("ViewPostCodeCost"), multiTenancySides: MultiTenancySides.Tenant);
			//State Wise Installation Cost
            var steteWiseInsCost = installationCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_StateWiseInstallationCost, L("StateWiseInstallationCost"), multiTenancySides: MultiTenancySides.Tenant);
            steteWiseInsCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_StateWiseInstallationCost_Create, L("CreateStateWiseInstallationCost"), multiTenancySides: MultiTenancySides.Tenant);
            steteWiseInsCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_StateWiseInstallationCost_Edit, L("EditStateWiseInstallationCost"), multiTenancySides: MultiTenancySides.Tenant);
            steteWiseInsCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_StateWiseInstallationCost_Delete, L("DeleteStateWiseInstallationCost"), multiTenancySides: MultiTenancySides.Tenant);
            //STC Cost
            var stcCost = installationCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_STCCost, L("STCCost"), multiTenancySides: MultiTenancySides.Tenant);
            stcCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_STCCost_Create, L("CreateNewSTCCost"), multiTenancySides: MultiTenancySides.Tenant);
            stcCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_STCCost_Edit, L("EditSTCCost"), multiTenancySides: MultiTenancySides.Tenant);
            stcCost.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_STCCost_Delete, L("DeleteSTCCost"), multiTenancySides: MultiTenancySides.Tenant);

            //invoicePaymentMethods
            var invoicePaymentMethods = datavaults.CreateChildPermission(AppPermissions.Pages_InvoicePaymentMethods, L("InvoicPaymentMethod"), multiTenancySides: MultiTenancySides.Tenant);
            invoicePaymentMethods.CreateChildPermission(AppPermissions.Pages_InvoicePaymentMethods_Create, L("CreateInvoicPaymentMethod"), multiTenancySides: MultiTenancySides.Tenant);
            invoicePaymentMethods.CreateChildPermission(AppPermissions.Pages_InvoicePaymentMethods_Edit, L("EditInvoicPaymentMethod"), multiTenancySides: MultiTenancySides.Tenant);
            invoicePaymentMethods.CreateChildPermission(AppPermissions.Pages_InvoicePaymentMethods_Delete, L("DeleteInvoicPaymentMethod"), multiTenancySides: MultiTenancySides.Tenant);

            //invoiceStatuses
            var invoiceStatuses = datavaults.CreateChildPermission(AppPermissions.Pages_InvoiceStatuses, L("InvoiceStatuses"), multiTenancySides: MultiTenancySides.Tenant);
            invoiceStatuses.CreateChildPermission(AppPermissions.Pages_InvoiceStatuses_Create, L("CreateNewInvoiceStatus"), multiTenancySides: MultiTenancySides.Tenant);
            invoiceStatuses.CreateChildPermission(AppPermissions.Pages_InvoiceStatuses_Edit, L("EditInvoiceStatus"), multiTenancySides: MultiTenancySides.Tenant);
            invoiceStatuses.CreateChildPermission(AppPermissions.Pages_InvoiceStatuses_Delete, L("DeleteInvoiceStatus"), multiTenancySides: MultiTenancySides.Tenant);

            //JobCancellationReason
            var JobCancellationReason = datavaults.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_JobCancellationReason, L("JobCancellationReason"), multiTenancySides: MultiTenancySides.Tenant);
            JobCancellationReason.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_JobCancellationReason_Create, L("CreateNewJobCancellationReason"), multiTenancySides: MultiTenancySides.Tenant);
            JobCancellationReason.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_JobCancellationReason_Edit, L("EditJobCancellationReason"), multiTenancySides: MultiTenancySides.Tenant);
            JobCancellationReason.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_JobCancellationReason_Delete, L("DeleteJobCancellationReason"), multiTenancySides: MultiTenancySides.Tenant);

            //jobStatuses
            var jobStatuses = datavaults.CreateChildPermission(AppPermissions.Pages_JobStatuses, L("JobStatuses"), multiTenancySides: MultiTenancySides.Tenant);
            jobStatuses.CreateChildPermission(AppPermissions.Pages_JobStatuses_Create, L("CreateNewJobStatus"), multiTenancySides: MultiTenancySides.Tenant);
            jobStatuses.CreateChildPermission(AppPermissions.Pages_JobStatuses_Edit, L("EditJobStatus"), multiTenancySides: MultiTenancySides.Tenant);
            jobStatuses.CreateChildPermission(AppPermissions.Pages_JobStatuses_Delete, L("DeleteJobStatus"), multiTenancySides: MultiTenancySides.Tenant);

           // jobPromotions
            var jobPromotions = datavaults.CreateChildPermission(AppPermissions.Pages_JobPromotions, L("JobPromotions"), multiTenancySides: MultiTenancySides.Tenant);
            jobPromotions.CreateChildPermission(AppPermissions.Pages_JobPromotions_Create, L("CreateNewJobPromotion"), multiTenancySides: MultiTenancySides.Tenant);
            jobPromotions.CreateChildPermission(AppPermissions.Pages_JobPromotions_Edit, L("EditJobPromotion"), multiTenancySides: MultiTenancySides.Tenant);
            jobPromotions.CreateChildPermission(AppPermissions.Pages_JobPromotions_Delete, L("DeleteJobPromotion"), multiTenancySides: MultiTenancySides.Tenant);


            //jobTypes
            var jobTypes = datavaults.CreateChildPermission(AppPermissions.Pages_JobTypes, L("JobTypes"), multiTenancySides: MultiTenancySides.Tenant);
            jobTypes.CreateChildPermission(AppPermissions.Pages_JobTypes_Create, L("CreateNewJobType"), multiTenancySides: MultiTenancySides.Tenant);
            jobTypes.CreateChildPermission(AppPermissions.Pages_JobTypes_Edit, L("EditJobType"), multiTenancySides: MultiTenancySides.Tenant);
            jobTypes.CreateChildPermission(AppPermissions.Pages_JobTypes_Delete, L("DeleteJobType"), multiTenancySides: MultiTenancySides.Tenant);

            //jobVariations
            var jobVariations = datavaults.CreateChildPermission(AppPermissions.Pages_JobVariations, L("JobVariations"), multiTenancySides: MultiTenancySides.Tenant);
            jobVariations.CreateChildPermission(AppPermissions.Pages_JobVariations_Create, L("CreateNewJobVariation"), multiTenancySides: MultiTenancySides.Tenant);
            jobVariations.CreateChildPermission(AppPermissions.Pages_JobVariations_Edit, L("EditJobVariation"), multiTenancySides: MultiTenancySides.Tenant);
            jobVariations.CreateChildPermission(AppPermissions.Pages_JobVariations_Delete, L("DeleteJobVariation"), multiTenancySides: MultiTenancySides.Tenant);

            //leadExpenses
            var leadExpenses = datavaults.CreateChildPermission(AppPermissions.Pages_LeadExpenses, L("LeadExpenses"), multiTenancySides: MultiTenancySides.Tenant);
            leadExpenses.CreateChildPermission(AppPermissions.Pages_LeadExpenses_Create, L("CreateLeadExpense"), multiTenancySides: MultiTenancySides.Tenant);
            leadExpenses.CreateChildPermission(AppPermissions.Pages_LeadExpenses_Edit, L("EditLeadExpense"), multiTenancySides: MultiTenancySides.Tenant);
            leadExpenses.CreateChildPermission(AppPermissions.Pages_LeadExpenses_Delete, L("DeleteLeadExpense"), multiTenancySides: MultiTenancySides.Tenant);
            leadExpenses.CreateChildPermission(AppPermissions.Pages_LeadExpenses_LastMonthEdit, L("LastMonthEdit"), multiTenancySides: MultiTenancySides.Tenant);

            //leadSources
            var leadSources = datavaults.CreateChildPermission(AppPermissions.Pages_LeadSources, L("LeadSources"), multiTenancySides: MultiTenancySides.Tenant);
            leadSources.CreateChildPermission(AppPermissions.Pages_LeadSources_Create, L("CreateNewLeadSource"), multiTenancySides: MultiTenancySides.Tenant);
            leadSources.CreateChildPermission(AppPermissions.Pages_LeadSources_Edit, L("EditLeadSource"), multiTenancySides: MultiTenancySides.Tenant);
            leadSources.CreateChildPermission(AppPermissions.Pages_LeadSources_Delete, L("DeleteLeadSource"), multiTenancySides: MultiTenancySides.Tenant);

            //MeterPhases
            var meterPhases = datavaults.CreateChildPermission(AppPermissions.Pages_MeterPhases, L("MeterPhases"), multiTenancySides: MultiTenancySides.Tenant);
            meterPhases.CreateChildPermission(AppPermissions.Pages_MeterPhases_Create, L("CreateNewMeterPhase"), multiTenancySides: MultiTenancySides.Tenant);
            meterPhases.CreateChildPermission(AppPermissions.Pages_MeterPhases_Edit, L("EditMeterPhase"), multiTenancySides: MultiTenancySides.Tenant);
            meterPhases.CreateChildPermission(AppPermissions.Pages_MeterPhases_Delete, L("DeleteMeterPhase"), multiTenancySides: MultiTenancySides.Tenant);

            //meterUpgrades
            var meterUpgrades = datavaults.CreateChildPermission(AppPermissions.Pages_MeterUpgrades, L("MeterUpgrades"), multiTenancySides: MultiTenancySides.Tenant);
            meterUpgrades.CreateChildPermission(AppPermissions.Pages_MeterUpgrades_Create, L("CreateNewMeterUpgrade"), multiTenancySides: MultiTenancySides.Tenant);
            meterUpgrades.CreateChildPermission(AppPermissions.Pages_MeterUpgrades_Edit, L("EditMeterUpgrade"), multiTenancySides: MultiTenancySides.Tenant);
            meterUpgrades.CreateChildPermission(AppPermissions.Pages_MeterUpgrades_Delete, L("DeleteMeterUpgrade"), multiTenancySides: MultiTenancySides.Tenant);


            //payment Options
            var paymentOptions = datavaults.CreateChildPermission(AppPermissions.Pages_PaymentOptions, L("PaymentOptions"), multiTenancySides: MultiTenancySides.Tenant);
            paymentOptions.CreateChildPermission(AppPermissions.Pages_PaymentOptions_Create, L("CreateNewPaymentOption"), multiTenancySides: MultiTenancySides.Tenant);
            paymentOptions.CreateChildPermission(AppPermissions.Pages_PaymentOptions_Edit, L("EditPaymentOption"), multiTenancySides: MultiTenancySides.Tenant);
            paymentOptions.CreateChildPermission(AppPermissions.Pages_PaymentOptions_Delete, L("DeletePaymentOption"), multiTenancySides: MultiTenancySides.Tenant);
            paymentOptions.CreateChildPermission(AppPermissions.Pages_PaymentOptions_Export, L("ExportPaymentOption"), multiTenancySides: MultiTenancySides.Tenant);


            //payment option
            var depositOptions = datavaults.CreateChildPermission(AppPermissions.Pages_DepositOptions, L("DepositOptions"), multiTenancySides: MultiTenancySides.Tenant);
            depositOptions.CreateChildPermission(AppPermissions.Pages_DepositOptions_Create, L("CreateNewDepositOption"), multiTenancySides: MultiTenancySides.Tenant);
            depositOptions.CreateChildPermission(AppPermissions.Pages_DepositOptions_Edit, L("EditDepositOption"), multiTenancySides: MultiTenancySides.Tenant);
            depositOptions.CreateChildPermission(AppPermissions.Pages_DepositOptions_Delete, L("DeleteDepositOption"), multiTenancySides: MultiTenancySides.Tenant);
            depositOptions.CreateChildPermission(AppPermissions.Pages_DepositOptions_Export, L("ExportDepositOption"), multiTenancySides: MultiTenancySides.Tenant);

            //payment Term
            var financeOptions = datavaults.CreateChildPermission(AppPermissions.Pages_FinanceOptions, L("FinanceOptions"), multiTenancySides: MultiTenancySides.Tenant);
            financeOptions.CreateChildPermission(AppPermissions.Pages_FinanceOptions_Create, L("CreateNewFinanceOption"), multiTenancySides: MultiTenancySides.Tenant);
            financeOptions.CreateChildPermission(AppPermissions.Pages_FinanceOptions_Edit, L("EditFinanceOption"), multiTenancySides: MultiTenancySides.Tenant);
            financeOptions.CreateChildPermission(AppPermissions.Pages_FinanceOptions_Delete, L("DeleteFinanceOption"), multiTenancySides: MultiTenancySides.Tenant);

            //postCodes
            var postCodes = datavaults.CreateChildPermission(AppPermissions.Pages_PostCodes, L("PostCodes"), multiTenancySides: MultiTenancySides.Tenant);
            postCodes.CreateChildPermission(AppPermissions.Pages_PostCodes_Create, L("CreateNewPostCode"), multiTenancySides: MultiTenancySides.Tenant);
            postCodes.CreateChildPermission(AppPermissions.Pages_PostCodes_Edit, L("EditPostCode"), multiTenancySides: MultiTenancySides.Tenant);
            postCodes.CreateChildPermission(AppPermissions.Pages_PostCodes_Delete, L("DeletePostCode"), multiTenancySides: MultiTenancySides.Tenant);

            //PostCodeRange
            var postCodeRange = datavaults.CreateChildPermission(AppPermissions.Pages_PostCodeRange, L("PostCodeRange"), multiTenancySides: MultiTenancySides.Tenant);
            postCodeRange.CreateChildPermission(AppPermissions.Pages_PostCodeRange_Create, L("CreateNewPostCodeRange"), multiTenancySides: MultiTenancySides.Tenant);
            postCodeRange.CreateChildPermission(AppPermissions.Pages_PostCodeRange_Edit, L("EditPostCodeRange"), multiTenancySides: MultiTenancySides.Tenant);
            postCodeRange.CreateChildPermission(AppPermissions.Pages_PostCodeRange_Delete, L("DeletePostCodeRange"), multiTenancySides: MultiTenancySides.Tenant);

            //PostalTypes
            var postalTypes = datavaults.CreateChildPermission(AppPermissions.Pages_PostalTypes, L("PostalTypes"), multiTenancySides: MultiTenancySides.Tenant);
            postalTypes.CreateChildPermission(AppPermissions.Pages_PostalTypes_Create, L("CreateNewPostalType"), multiTenancySides: MultiTenancySides.Tenant);
            postalTypes.CreateChildPermission(AppPermissions.Pages_PostalTypes_Edit, L("EditPostalType"), multiTenancySides: MultiTenancySides.Tenant);
            postalTypes.CreateChildPermission(AppPermissions.Pages_PostalTypes_Delete, L("DeletePostalType"), multiTenancySides: MultiTenancySides.Tenant);

            //priceitemlists
            var priceitemlists = datavaults.CreateChildPermission(AppPermissions.Pages_PriceItemLists, L("PriceItemLists"), multiTenancySides: MultiTenancySides.Tenant);
            priceitemlists.CreateChildPermission(AppPermissions.Pages_PriceItemLists_Create, L("CreateNewPriceItemLists"), multiTenancySides: MultiTenancySides.Tenant);
            priceitemlists.CreateChildPermission(AppPermissions.Pages_PriceItemLists_Edit, L("EditPriceItemLists"), multiTenancySides: MultiTenancySides.Tenant);
            priceitemlists.CreateChildPermission(AppPermissions.Pages_PriceItemLists_Delete, L("DeletePriceItemLists"), multiTenancySides: MultiTenancySides.Tenant);
                       
            //Price variations
            var variations = datavaults.CreateChildPermission(AppPermissions.Pages_Variations, L("Variations"), multiTenancySides: MultiTenancySides.Tenant);
            variations.CreateChildPermission(AppPermissions.Pages_Variations_Create, L("CreateNewVariation"), multiTenancySides: MultiTenancySides.Tenant);
            variations.CreateChildPermission(AppPermissions.Pages_Variations_Edit, L("EditVariation"), multiTenancySides: MultiTenancySides.Tenant);
            variations.CreateChildPermission(AppPermissions.Pages_Variations_Delete, L("DeleteVariation"), multiTenancySides: MultiTenancySides.Tenant);
            
			//productItems
			var productItems = datavaults.CreateChildPermission(AppPermissions.Pages_ProductItems, L("ProductItems"), multiTenancySides: MultiTenancySides.Tenant);
            productItems.CreateChildPermission(AppPermissions.Pages_ProductItems_Create, L("CreateNewProductItem"), multiTenancySides: MultiTenancySides.Tenant);
            productItems.CreateChildPermission(AppPermissions.Pages_ProductItems_Edit, L("EditProductItem"), multiTenancySides: MultiTenancySides.Tenant);
            productItems.CreateChildPermission(AppPermissions.Pages_ProductItems_Delete, L("DeleteProductItem"), multiTenancySides: MultiTenancySides.Tenant);
            productItems.CreateChildPermission(AppPermissions.Pages_ProductItems_View, L("ViewProductItem"), multiTenancySides: MultiTenancySides.Tenant);
            productItems.CreateChildPermission(AppPermissions.Pages_ProductItems_Upload, L("UploadProductItem"), multiTenancySides: MultiTenancySides.Tenant);
            productItems.CreateChildPermission(AppPermissions.Pages_ProductItems_UploadImage, L("UploadImageProductItem"), multiTenancySides: MultiTenancySides.Tenant);
            productItems.CreateChildPermission(AppPermissions.Pages_ProductItems_Download, L("DownloadProductItem"), multiTenancySides: MultiTenancySides.Tenant);

            //ProductPackages
            var package = datavaults.CreateChildPermission(AppPermissions.Pages_ProductPackages, L("ProductPackages"), multiTenancySides: MultiTenancySides.Tenant);
            package.CreateChildPermission(AppPermissions.Pages_ProductPackages_Create, L("CreateProductPackagese"), multiTenancySides: MultiTenancySides.Tenant);
            package.CreateChildPermission(AppPermissions.Pages_ProductPackages_Edit, L("EditProductPackagese"), multiTenancySides: MultiTenancySides.Tenant);
            package.CreateChildPermission(AppPermissions.Pages_ProductPackages_Delete, L("DeleteProductPackagese"), multiTenancySides: MultiTenancySides.Tenant);

            //productTypes
            var productTypes = datavaults.CreateChildPermission(AppPermissions.Pages_ProductTypes, L("ProductTypes"), multiTenancySides: MultiTenancySides.Tenant);
            productTypes.CreateChildPermission(AppPermissions.Pages_ProductTypes_Create, L("CreateNewProductType"), multiTenancySides: MultiTenancySides.Tenant);
            productTypes.CreateChildPermission(AppPermissions.Pages_ProductTypes_Edit, L("EditProductType"), multiTenancySides: MultiTenancySides.Tenant);
            productTypes.CreateChildPermission(AppPermissions.Pages_ProductTypes_Delete, L("DeleteProductType"), multiTenancySides: MultiTenancySides.Tenant);

            //promotionMasters
            var promotionMasters = datavaults.CreateChildPermission(AppPermissions.Pages_PromotionMasters, L("PromotionMasters"), multiTenancySides: MultiTenancySides.Tenant);
            promotionMasters.CreateChildPermission(AppPermissions.Pages_PromotionMasters_Create, L("CreateNewPromotionMaster"), multiTenancySides: MultiTenancySides.Tenant);
            promotionMasters.CreateChildPermission(AppPermissions.Pages_PromotionMasters_Edit, L("EditPromotionMaster"), multiTenancySides: MultiTenancySides.Tenant);
            promotionMasters.CreateChildPermission(AppPermissions.Pages_PromotionMasters_Delete, L("DeletePromotionMaster"), multiTenancySides: MultiTenancySides.Tenant);

            //promotionTypes
            var promotionTypes = datavaults.CreateChildPermission(AppPermissions.Pages_PromotionTypes, L("PromotionTypes"), multiTenancySides: MultiTenancySides.Host);
            promotionTypes.CreateChildPermission(AppPermissions.Pages_PromotionTypes_Create, L("CreateNewPromotionType"), multiTenancySides: MultiTenancySides.Host);
            promotionTypes.CreateChildPermission(AppPermissions.Pages_PromotionTypes_Edit, L("EditPromotionType"), multiTenancySides: MultiTenancySides.Host);
            promotionTypes.CreateChildPermission(AppPermissions.Pages_PromotionTypes_Delete, L("DeletePromotionType"), multiTenancySides: MultiTenancySides.Host);
          
            //refundReasons
            var refundReasons = datavaults.CreateChildPermission(AppPermissions.Pages_RefundReasons, L("RefundReasons"), multiTenancySides: MultiTenancySides.Tenant);
            refundReasons.CreateChildPermission(AppPermissions.Pages_RefundReasons_Create, L("CreateNewRefundReason"), multiTenancySides: MultiTenancySides.Tenant);
            refundReasons.CreateChildPermission(AppPermissions.Pages_RefundReasons_Edit, L("EditRefundReason"), multiTenancySides: MultiTenancySides.Tenant);
            refundReasons.CreateChildPermission(AppPermissions.Pages_RefundReasons_Delete, L("DeleteRefundReason"), multiTenancySides: MultiTenancySides.Tenant);

            //rejectReasons
            var rejectReasons = datavaults.CreateChildPermission(AppPermissions.Pages_RejectReasons, L("RejectReasons"), multiTenancySides: MultiTenancySides.Tenant);
            rejectReasons.CreateChildPermission(AppPermissions.Pages_RejectReasons_Create, L("CreateNewRejectReason"), multiTenancySides: MultiTenancySides.Tenant);
            rejectReasons.CreateChildPermission(AppPermissions.Pages_RejectReasons_Edit, L("EditRejectReason"), multiTenancySides: MultiTenancySides.Tenant);
            rejectReasons.CreateChildPermission(AppPermissions.Pages_RejectReasons_Delete, L("DeleteRejectReason"), multiTenancySides: MultiTenancySides.Tenant);

            //reviewType
            var reviewType = datavaults.CreateChildPermission(AppPermissions.Pages_ReviewType, L("ReviewType"), multiTenancySides: MultiTenancySides.Tenant);
            reviewType.CreateChildPermission(AppPermissions.Pages_ReviewType_Create, L("CreateNewReviewType"), multiTenancySides: MultiTenancySides.Tenant);
            reviewType.CreateChildPermission(AppPermissions.Pages_ReviewType_Edit, L("EditReviewType"), multiTenancySides: MultiTenancySides.Tenant);
            reviewType.CreateChildPermission(AppPermissions.Pages_ReviewType_Delete, L("DeleteReviewType"), multiTenancySides: MultiTenancySides.Tenant);

            //RoofAngles
            var roofAngles = datavaults.CreateChildPermission(AppPermissions.Pages_RoofAngles, L("RoofAngles"), multiTenancySides: MultiTenancySides.Tenant);
            roofAngles.CreateChildPermission(AppPermissions.Pages_RoofAngles_Create, L("CreateNewRoofAngle"), multiTenancySides: MultiTenancySides.Tenant);
            roofAngles.CreateChildPermission(AppPermissions.Pages_RoofAngles_Edit, L("EditRoofAngle"), multiTenancySides: MultiTenancySides.Tenant);
            roofAngles.CreateChildPermission(AppPermissions.Pages_RoofAngles_Delete, L("DeleteRoofAngle"), multiTenancySides: MultiTenancySides.Tenant);
            
            //RoofTypes
            var roofTypes = datavaults.CreateChildPermission(AppPermissions.Pages_RoofTypes, L("RoofTypes"), multiTenancySides: MultiTenancySides.Tenant);
            roofTypes.CreateChildPermission(AppPermissions.Pages_RoofTypes_Create, L("CreateNewRoofType"), multiTenancySides: MultiTenancySides.Tenant);
            roofTypes.CreateChildPermission(AppPermissions.Pages_RoofTypes_Edit, L("EditRoofType"), multiTenancySides: MultiTenancySides.Tenant);
            roofTypes.CreateChildPermission(AppPermissions.Pages_RoofTypes_Delete, L("DeleteRoofType"), multiTenancySides: MultiTenancySides.Tenant);

            //teams
            var teams = datavaults.CreateChildPermission(AppPermissions.Pages_Teams, L("Teams"), multiTenancySides: MultiTenancySides.Tenant);
            teams.CreateChildPermission(AppPermissions.Pages_Teams_Create, L("CreateNewTeam"), multiTenancySides: MultiTenancySides.Tenant);
            teams.CreateChildPermission(AppPermissions.Pages_Teams_Edit, L("EditTeam"), multiTenancySides: MultiTenancySides.Tenant);
            teams.CreateChildPermission(AppPermissions.Pages_Teams_Delete, L("DeleteTeam"), multiTenancySides: MultiTenancySides.Tenant);

            //ServiceCategory
            var serviceCategory = datavaults.CreateChildPermission(AppPermissions.Pages_ServiceCategory, L("ServiceCategory"), multiTenancySides: MultiTenancySides.Tenant);
            serviceCategory.CreateChildPermission(AppPermissions.Pages_ServiceCategory_Create, L("CreateNewServiceCategory"), multiTenancySides: MultiTenancySides.Tenant);
            serviceCategory.CreateChildPermission(AppPermissions.Pages_ServiceCategory_Edit, L("EditServiceCategory"), multiTenancySides: MultiTenancySides.Tenant);
            serviceCategory.CreateChildPermission(AppPermissions.Pages_ServiceCategory_Delete, L("DeleteServiceCategory"), multiTenancySides: MultiTenancySides.Tenant);
           
            //ServiceDocumentType
            var serviceDocumenttypes = datavaults.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_ServiceDocumentType, L("ServiceDocumentType"), multiTenancySides: MultiTenancySides.Tenant);
            serviceDocumenttypes.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_ServiceDocumentType_Create, L("CreateNewServiceDocumentType"), multiTenancySides: MultiTenancySides.Tenant);
            serviceDocumenttypes.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_ServiceDocumentType_Edit, L("EditServiceDocumentType"), multiTenancySides: MultiTenancySides.Tenant);
            serviceDocumenttypes.CreateChildPermission(AppPermissions.Pages_Tenant_datavault_ServiceDocumentType_Delete, L("DeleteServiceDocumentType"), multiTenancySides: MultiTenancySides.Tenant);
            
            // Service Sources Permissions
            var serviceSources = datavaults.CreateChildPermission(AppPermissions.Pages_ServiceSources, L("ServiceSources"), multiTenancySides: MultiTenancySides.Tenant);
            serviceSources.CreateChildPermission(AppPermissions.Pages_ServiceSources_Create, L("CreateNewServiceSources"), multiTenancySides: MultiTenancySides.Tenant);
            serviceSources.CreateChildPermission(AppPermissions.Pages_ServiceSources_Edit, L("EditServiceSources"), multiTenancySides: MultiTenancySides.Tenant);
            serviceSources.CreateChildPermission(AppPermissions.Pages_ServiceSources_Delete, L("DeleteServiceSources"), multiTenancySides: MultiTenancySides.Tenant);
            
            //ServiceStatus
            var servicePriority = datavaults.CreateChildPermission(AppPermissions.Pages_ServiceStatus, L("ServiceStatus"), multiTenancySides: MultiTenancySides.Tenant);
            servicePriority.CreateChildPermission(AppPermissions.Pages_ServiceStatus_Create, L("CreateNewServiceStatus"), multiTenancySides: MultiTenancySides.Tenant);
            servicePriority.CreateChildPermission(AppPermissions.Pages_ServiceStatus_Edit, L("EditServiceStatus"), multiTenancySides: MultiTenancySides.Tenant);
            servicePriority.CreateChildPermission(AppPermissions.Pages_ServiceStatus_Delete, L("DeleteServiceStatus"), multiTenancySides: MultiTenancySides.Tenant);
            
            //ServiceSubCategory
            var serviceSubCategory = datavaults.CreateChildPermission(AppPermissions.Pages_ServiceSubCategory, L("ServiceSubCategory"), multiTenancySides: MultiTenancySides.Tenant);
            serviceSubCategory.CreateChildPermission(AppPermissions.Pages_ServiceSubCategory_Create, L("CreateNewServiceSubCategory"), multiTenancySides: MultiTenancySides.Tenant);
            serviceSubCategory.CreateChildPermission(AppPermissions.Pages_ServiceSubCategory_Edit, L("EditServiceSubCategory"), multiTenancySides: MultiTenancySides.Tenant);
            serviceSubCategory.CreateChildPermission(AppPermissions.Pages_ServiceSubCategory_Delete, L("DeleteServiceSubCategory"), multiTenancySides: MultiTenancySides.Tenant);
            
            //ServiceType
            var serviceType = datavaults.CreateChildPermission(AppPermissions.Pages_ServiceType, L("ServiceType"), multiTenancySides: MultiTenancySides.Tenant);
            serviceType.CreateChildPermission(AppPermissions.Pages_ServiceType_Create, L("CreateNewServiceType"), multiTenancySides: MultiTenancySides.Tenant);
            serviceType.CreateChildPermission(AppPermissions.Pages_ServiceType_Edit, L("EditServiceType"), multiTenancySides: MultiTenancySides.Tenant);
            serviceType.CreateChildPermission(AppPermissions.Pages_ServiceType_Delete, L("DeleteServiceType"), multiTenancySides: MultiTenancySides.Tenant);
            
            // SmsTemplates
            var smsTemplates = datavaults.CreateChildPermission(AppPermissions.Pages_SmsTemplates, L("SmsTemplates"), multiTenancySides: MultiTenancySides.Tenant);
            smsTemplates.CreateChildPermission(AppPermissions.Pages_SmsTemplates_Create, L("CreateNewSmsTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            smsTemplates.CreateChildPermission(AppPermissions.Pages_SmsTemplates_Edit, L("EditSmsTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            smsTemplates.CreateChildPermission(AppPermissions.Pages_SmsTemplates_Delete, L("DeleteSmsTemplate"), multiTenancySides: MultiTenancySides.Tenant);

            //state
            var state = datavaults.CreateChildPermission(AppPermissions.Pages_DataVaults_State, L("State"), multiTenancySides: MultiTenancySides.Tenant);
            state.CreateChildPermission(AppPermissions.Pages_DataVaults_State_Edit, L("UpdateBatteryRebate"), multiTenancySides: MultiTenancySides.Tenant);

            //StcPvdStatus
            var stcPvdStatus = datavaults.CreateChildPermission(AppPermissions.Pages_StcPvdStatus, L("StcPvdStatus"), multiTenancySides: MultiTenancySides.Tenant);
            stcPvdStatus.CreateChildPermission(AppPermissions.Pages_StcPvdStatus_Create, L("CreateStcPvdStatus"), multiTenancySides: MultiTenancySides.Tenant);
            stcPvdStatus.CreateChildPermission(AppPermissions.Pages_StcPvdStatus_Edit, L("EditStcPvdStatus"), multiTenancySides: MultiTenancySides.Tenant);
            stcPvdStatus.CreateChildPermission(AppPermissions.Pages_StcPvdStatus_Delete, L("DeleteStcPvdStatus"), multiTenancySides: MultiTenancySides.Tenant);
                        
            //streetNames
            var streetNames = datavaults.CreateChildPermission(AppPermissions.Pages_StreetNames, L("StreetNames"), multiTenancySides: MultiTenancySides.Tenant);
            streetNames.CreateChildPermission(AppPermissions.Pages_StreetNames_Create, L("CreateNewStreetName"), multiTenancySides: MultiTenancySides.Tenant);
            streetNames.CreateChildPermission(AppPermissions.Pages_StreetNames_Edit, L("EditStreetName"), multiTenancySides: MultiTenancySides.Tenant);
            streetNames.CreateChildPermission(AppPermissions.Pages_StreetNames_Delete, L("DeleteStreetName"), multiTenancySides: MultiTenancySides.Tenant);

            //StreetTypes
            var streetTypes = datavaults.CreateChildPermission(AppPermissions.Pages_StreetTypes, L("StreetTypes"), multiTenancySides: MultiTenancySides.Tenant);
            streetTypes.CreateChildPermission(AppPermissions.Pages_StreetTypes_Create, L("CreateNewStreetType"), multiTenancySides: MultiTenancySides.Tenant);
            streetTypes.CreateChildPermission(AppPermissions.Pages_StreetTypes_Edit, L("EditStreetType"), multiTenancySides: MultiTenancySides.Tenant);
            streetTypes.CreateChildPermission(AppPermissions.Pages_StreetTypes_Delete, L("DeleteStreetType"), multiTenancySides: MultiTenancySides.Tenant);

            
           
            
            //UnitTypes
            var unitTypes = datavaults.CreateChildPermission(AppPermissions.Pages_UnitTypes, L("UnitTypes"), multiTenancySides: MultiTenancySides.Tenant);
            unitTypes.CreateChildPermission(AppPermissions.Pages_UnitTypes_Create, L("CreateNewUnitType"), multiTenancySides: MultiTenancySides.Tenant);
            unitTypes.CreateChildPermission(AppPermissions.Pages_UnitTypes_Edit, L("EditUnitType"), multiTenancySides: MultiTenancySides.Tenant);
            unitTypes.CreateChildPermission(AppPermissions.Pages_UnitTypes_Delete, L("DeleteUnitType"), multiTenancySides: MultiTenancySides.Tenant);

            //userTeams
            var userTeams = datavaults.CreateChildPermission(AppPermissions.Pages_UserTeams, L("UserTeams"), multiTenancySides: MultiTenancySides.Tenant);
            userTeams.CreateChildPermission(AppPermissions.Pages_UserTeams_Create, L("CreateNewUserTeam"), multiTenancySides: MultiTenancySides.Tenant);
            userTeams.CreateChildPermission(AppPermissions.Pages_UserTeams_Edit, L("EditUserTeam"), multiTenancySides: MultiTenancySides.Tenant);
            userTeams.CreateChildPermission(AppPermissions.Pages_UserTeams_Delete, L("DeleteUserTeam"), multiTenancySides: MultiTenancySides.Tenant);

            //WarehouseLocation
            var location = datavaults.CreateChildPermission(AppPermissions.Pages_WarehouseLocation, L("WarehouseLocation"), multiTenancySides: MultiTenancySides.Tenant);
            location.CreateChildPermission(AppPermissions.Pages_WarehouseLocation_Edit, L("EditWarehouseLocation"), multiTenancySides: MultiTenancySides.Tenant);

            //Voucher
            var voucher = datavaults.CreateChildPermission(AppPermissions.Pages_Voucher, L("voucher"), multiTenancySides: MultiTenancySides.Tenant);
            voucher.CreateChildPermission(AppPermissions.Pages_Vouchers_Create, L("CreateNewvoucher"), multiTenancySides: MultiTenancySides.Tenant);
            voucher.CreateChildPermission(AppPermissions.Pages_Vouchers_Edit, L("Editvoucher"), multiTenancySides: MultiTenancySides.Tenant);
            voucher.CreateChildPermission(AppPermissions.Pages_Vouchers_Delete, L("Deletevoucher"), multiTenancySides: MultiTenancySides.Tenant);

            //var postCodes = datavaults.CreateChildPermission(AppPermissions.Pages_PostCodes, L("PostCodes"), multiTenancySides: MultiTenancySides.Host);
            //postCodes.CreateChildPermission(AppPermissions.Pages_PostCodes_Create, L("CreateNewPostCode"), multiTenancySides: MultiTenancySides.Host);
            //postCodes.CreateChildPermission(AppPermissions.Pages_PostCodes_Edit, L("EditPostCode"), multiTenancySides: MultiTenancySides.Host);
            //postCodes.CreateChildPermission(AppPermissions.Pages_PostCodes_Delete, L("DeletePostCode"), multiTenancySides: MultiTenancySides.Host);



            #endregion

            var lead = retail.CreateChildPermission(AppPermissions.Pages_Leads, L("Leads"), multiTenancySides: MultiTenancySides.Tenant);

            #region lead
            var managelead = lead.CreateChildPermission(AppPermissions.Pages_ManageLeads, L("ManageLeads"), multiTenancySides: MultiTenancySides.Tenant);
			managelead.CreateChildPermission(AppPermissions.Pages_Leads_Create, L("CreateNewLead"), multiTenancySides: MultiTenancySides.Tenant);
			managelead.CreateChildPermission(AppPermissions.Pages_Leads_Edit, L("EditLead"), multiTenancySides: MultiTenancySides.Tenant);
			managelead.CreateChildPermission(AppPermissions.Pages_Leads_Delete, L("DeleteLead"), multiTenancySides: MultiTenancySides.Tenant);
			managelead.CreateChildPermission(AppPermissions.Pages_Leads_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
			managelead.CreateChildPermission(AppPermissions.Pages_Leads_Import, L("Import"), multiTenancySides: MultiTenancySides.Tenant);
			managelead.CreateChildPermission(AppPermissions.Pages_Leads_Assign, L("Assign"), multiTenancySides: MultiTenancySides.Tenant);
			managelead.CreateChildPermission(AppPermissions.Pages_Leads_AssignToDifferentOrganization, L("AssignToDifferentOrganization"), multiTenancySides: MultiTenancySides.Tenant);
			managelead.CreateChildPermission(AppPermissions.Pages_Leads_CopyToDifferentOrganization, L("CopyToDifferentOrganization"), multiTenancySides: MultiTenancySides.Tenant);
			managelead.CreateChildPermission(AppPermissions.Pages_Leads_MarkAsDuplicate, L("MarkAsDuplicate"), multiTenancySides: MultiTenancySides.Tenant);
			managelead.CreateChildPermission(AppPermissions.Pages_Leads_UpdateFacebookLeads, L("UpdateFacebookLeads"), multiTenancySides: MultiTenancySides.Tenant);
			managelead.CreateChildPermission(AppPermissions.Pages_Leads_FakeLeads, L("MarkAsFakeLead"), multiTenancySides: MultiTenancySides.Tenant);
            managelead.CreateChildPermission(AppPermissions.Pages_ManageLeads_Export_MobileEmail, L("ExportToExcelMobileEmail"), multiTenancySides: MultiTenancySides.Tenant);

            var leadduplicate = lead.CreateChildPermission(AppPermissions.Pages_Lead_Duplicate, L("DuplicateLead"), multiTenancySides: MultiTenancySides.Tenant);
            leadduplicate.CreateChildPermission(AppPermissions.Pages_Lead_Duplicate_Export_MobileEmail, L("ExportToExcelMobileEmail"), multiTenancySides: MultiTenancySides.Tenant);

            var mylead = lead.CreateChildPermission(AppPermissions.Pages_MyLeads, L("MyLead"), multiTenancySides: MultiTenancySides.Tenant);
          
            mylead.CreateChildPermission(AppPermissions.Pages_MyLeads_Create, L("CreateNewLead"), multiTenancySides: MultiTenancySides.Tenant);
            mylead.CreateChildPermission(AppPermissions.Pages_MyLeads_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            //mylead.CreateChildPermission(AppPermissions.Pages_MyLeads_ShowLeadSource, L("ShowLeadSource"), multiTenancySides: MultiTenancySides.Tenant);
            mylead.CreateChildPermission(AppPermissions.Pages_MyLeads_AssignUserId, L("ShowAssignUserId"), multiTenancySides: MultiTenancySides.Tenant);

            var leadtracker = lead.CreateChildPermission(AppPermissions.Pages_LeadTracker, L("LeadTracker"), multiTenancySides: MultiTenancySides.Tenant);
            leadtracker.CreateChildPermission(AppPermissions.Pages_LeadTracker_Export_MobileEmail, L("ExportToExcelMobileEmail"), multiTenancySides: MultiTenancySides.Tenant);
            leadtracker.CreateChildPermission(AppPermissions.Pages_LeadTracker_Edit, L("EditLead"), multiTenancySides: MultiTenancySides.Tenant);
            leadtracker.CreateChildPermission(AppPermissions.Pages_LeadTracker_Delete, L("DeleteLead"), multiTenancySides: MultiTenancySides.Tenant);
            

            var action = leadtracker.CreateChildPermission(AppPermissions.Pages_Leads_LeadTracker_Action, L("LeadAction"), multiTenancySides: MultiTenancySides.Tenant);
            action.CreateChildPermission(AppPermissions.Pages_Leads_LeadTracker_Action_ChangeOrganization, L("ChangeOrganization"), multiTenancySides: MultiTenancySides.Tenant);
            action.CreateChildPermission(AppPermissions.Pages_Leads_LeadTracker_Action_AssignToSalesRep, L("AssignToSalesRep"), multiTenancySides: MultiTenancySides.Tenant);
            action.CreateChildPermission(AppPermissions.Pages_Leads_LeadTracker_Action_AssignToSalesManager, L("AssignToSalesManager"), multiTenancySides: MultiTenancySides.Tenant);
            action.CreateChildPermission(AppPermissions.Pages_Leads_LeadTracker_Action_AssignToUsers, L("AssignToUsers"), multiTenancySides: MultiTenancySides.Tenant);
            action.CreateChildPermission(AppPermissions.Pages_Leads_LeadTracker_Action_TransferToLeadGen, L("TransferToLeadGen"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.LeadGeneration));
            action.CreateChildPermission(AppPermissions.Pages_Leads_LeadTracker_Action_MarkAsFakeLead, L("MarkAsFakeLead"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.LeadGeneration));
            action.CreateChildPermission(AppPermissions.Pages_Leads_LeadTracker_Action_TransferLeftSalesRepLead, L("TransferLeftSalesRepLead"), multiTenancySides: MultiTenancySides.Tenant);

            //End Lead Tracker Permission

            var leadCalcel = lead.CreateChildPermission(AppPermissions.Pages_Lead_Calcel, L("CancelLead"), multiTenancySides: MultiTenancySides.Tenant);
            var leadRejects = lead.CreateChildPermission(AppPermissions.Pages_Lead_Rejects, L("RejectLead"), multiTenancySides: MultiTenancySides.Tenant);
			var leadClosed = lead.CreateChildPermission(AppPermissions.Pages_Lead_Closed, L("ClosedLead"), multiTenancySides: MultiTenancySides.Tenant);
            leadClosed.CreateChildPermission(AppPermissions.Pages_Lead_Closed_Export_MobileEmail, L("ExportToExcelMobileEmail"), multiTenancySides: MultiTenancySides.Tenant);
            var _3rdPartyLeadTracker = lead.CreateChildPermission(AppPermissions.Pages_3rdPartyLeadTracker, L("3rdPartyLeadTracker"), multiTenancySides: MultiTenancySides.Tenant);
            _3rdPartyLeadTracker.CreateChildPermission(AppPermissions.Pages_3rdPartyLeadTracker_Create, L("Create3rdPartyLeadTracker"), multiTenancySides: MultiTenancySides.Tenant);
            _3rdPartyLeadTracker.CreateChildPermission(AppPermissions.Pages_3rdPartyLeadTracker_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);

           
            var ShowLeadSource = lead.CreateChildPermission(AppPermissions.Pages_MyLeads_ShowLeadSource, L("ShowLeadSource"), multiTenancySides: MultiTenancySides.Tenant);
            #region LeadDetails
            var leadDetailsFeatureDependency = new SimpleFeatureDependency(AppFeatures.LeadDetails);
            var leadDetails = lead.CreateChildPermission(AppPermissions.Pages_LeadDetails, L("LeadDetails"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: leadDetailsFeatureDependency);
            leadDetails.CreateChildPermission(AppPermissions.Pages_LeadDetails_InvoicePaymentVerifiedEdit, L("InvoicePaymentVerifiedEdit"), multiTenancySides: MultiTenancySides.Tenant);

            var callHistoryFeatureDependency = new SimpleFeatureDependency(AppFeatures.LeadDetails_CallHistory);
            leadDetails.CreateChildPermission(AppPermissions.Pages_LeadDetails_CallHistory, L("CallHistory"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: callHistoryFeatureDependency);
            leadDetails.CreateChildPermission(AppPermissions.Pages_LeadDetails_InvoicePaymentDelete, L("DeleteInvoicePayment"), multiTenancySides: MultiTenancySides.Tenant);
            leadDetails.CreateChildPermission(AppPermissions.Pages_LeadDetails_Calculator, L("Calculator"), multiTenancySides: MultiTenancySides.Tenant);
            var documents = leadDetails.CreateChildPermission(AppPermissions.Pages_LeadDetails_Documents, L("Documents"), multiTenancySides: MultiTenancySides.Tenant);
            documents.CreateChildPermission(AppPermissions.Pages_LeadDetails_Documents_Delete, L("DeleteDocument"), multiTenancySides: MultiTenancySides.Tenant);
            var Acknowledgement = leadDetails.CreateChildPermission(AppPermissions.Pages_LeadDetails_Acknowledgement, L("Acknowledgement"), multiTenancySides: MultiTenancySides.Tenant);
            Acknowledgement.CreateChildPermission(AppPermissions.Pages_LeadDetails_Acknowledgement_Delete, L("DeleteAcknowledgement"), multiTenancySides: MultiTenancySides.Tenant);
            var Declaration = leadDetails.CreateChildPermission(AppPermissions.Pages_LeadDetails_Declaration, L("Declaration"), multiTenancySides: MultiTenancySides.Tenant);
            Declaration.CreateChildPermission(AppPermissions.Pages_LeadDetails_Declaration_Delete, L("DeleteDeclaration"), multiTenancySides: MultiTenancySides.Tenant);

            var checkAutualCostFeatureDependency = new SimpleFeatureDependency(AppFeatures.LeadDetails_Job_ActualCost);
            leadDetails.CreateChildPermission(AppPermissions.Pages_LeadDetails_CheckActualCost, L("ActualCost"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: checkAutualCostFeatureDependency);
            leadDetails.CreateChildPermission(AppPermissions.Pages_LeadDetails_ShowActualCostDetails, L("ShowActualCostDetails"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: checkAutualCostFeatureDependency);
            leadDetails.CreateChildPermission(AppPermissions.Pages_LeadDetails_CategoryD, L("ShowCategoryDDiscount"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: checkAutualCostFeatureDependency);

            leadDetails.CreateChildPermission(AppPermissions.Pages_LeadDetails_EditJobAddress, L("EditJobAddress"), multiTenancySides: MultiTenancySides.Tenant);

            var checkLiveStockFeatureDependency = new SimpleFeatureDependency(AppFeatures.StockManagement_ProductItems_NetSituation);
            leadDetails.CreateChildPermission(AppPermissions.Pages_LeadDetails_CheckLiveStock, L("CheckLiveStock"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: checkLiveStockFeatureDependency);
            leadDetails.CreateChildPermission(AppPermissions.Pages_LeadDetails_SkipActualCost, L("SkipActualCost"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: checkAutualCostFeatureDependency);

            var checkWarrantyFeatureDependency = new SimpleFeatureDependency(AppFeatures.LeadDetails_Job_ProductItems_CheckWarranty);
            leadDetails.CreateChildPermission(AppPermissions.Pages_LeadDetails_CheckWarranty, L("CheckWarranty"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: checkWarrantyFeatureDependency);

            #endregion

            #endregion lead

            var promotionsGroup = retail.CreateChildPermission(AppPermissions.Pages_PromotionGroup, L("Promotions"));

			#region Promotions
			//var promotionResponseStatuses = promotionsGroup.CreateChildPermission(AppPermissions.Pages_PromotionResponseStatuses, L("PromotionResponseStatuses"), multiTenancySides: MultiTenancySides.Tenant);
			//promotionResponseStatuses.CreateChildPermission(AppPermissions.Pages_PromotionResponseStatuses_Create, L("CreateNewPromotionResponseStatus"), multiTenancySides: MultiTenancySides.Tenant);
			//promotionResponseStatuses.CreateChildPermission(AppPermissions.Pages_PromotionResponseStatuses_Edit, L("EditPromotionResponseStatus"), multiTenancySides: MultiTenancySides.Tenant);
			//promotionResponseStatuses.CreateChildPermission(AppPermissions.Pages_PromotionResponseStatuses_Delete, L("DeletePromotionResponseStatus"), multiTenancySides: MultiTenancySides.Tenant);

			var promotions = promotionsGroup.CreateChildPermission(AppPermissions.Pages_Promotions, L("Promotions"), multiTenancySides: MultiTenancySides.Tenant);
			promotions.CreateChildPermission(AppPermissions.Pages_Promotions_Create, L("CreateNewPromotion"), multiTenancySides: MultiTenancySides.Tenant);
			promotions.CreateChildPermission(AppPermissions.Pages_Promotions_View, L("ViewPromotion"), multiTenancySides: MultiTenancySides.Tenant);
            //promotions.CreateChildPermission(AppPermissions.Pages_Promotions_Edit, L("EditPromotion"), multiTenancySides: MultiTenancySides.Tenant);
            promotions.CreateChildPermission(AppPermissions.Pages_Promotions_Delete, L("DeletePromotion"), multiTenancySides: MultiTenancySides.Tenant);
			promotions.CreateChildPermission(AppPermissions.Pages_Promotions_ExportExcel, L("ExportPromotionExcel"), multiTenancySides: MultiTenancySides.Tenant);

			var promotionUsers = promotionsGroup.CreateChildPermission(AppPermissions.Pages_PromotionUsers, L("PromotionUsers"), multiTenancySides: MultiTenancySides.Tenant);
			//promotionUsers.CreateChildPermission(AppPermissions.Pages_PromotionUsers_Create, L("CreateNewPromotionUser"), multiTenancySides: MultiTenancySides.Tenant);
			//promotionUsers.CreateChildPermission(AppPermissions.Pages_PromotionUsers_Edit, L("EditPromotionUser"), multiTenancySides: MultiTenancySides.Tenant);
			//promotionUsers.CreateChildPermission(AppPermissions.Pages_PromotionUsers_Delete, L("DeletePromotionUser"), multiTenancySides: MultiTenancySides.Tenant);
			promotionUsers.CreateChildPermission(AppPermissions.Pages_PromotionUsers_View, L("ViewPromotionUser"), multiTenancySides: MultiTenancySides.Tenant);
			promotionUsers.CreateChildPermission(AppPermissions.Pages_PromotionUsers_Excel, L("PromotionUsersExcel"), multiTenancySides: MultiTenancySides.Tenant);
            promotionUsers.CreateChildPermission(AppPermissions.Pages_PromotionUsers_Excel_MobileEmail, L("ExportToExcelMobileEmail"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion
            var Tracker = retail.CreateChildPermission(AppPermissions.Pages_Tracker, L("Tracker"), multiTenancySides: MultiTenancySides.Tenant);

            #region Tracker
            var essentialTracker = Tracker.CreateChildPermission(AppPermissions.Pages_EssentialTracker, L("EssentialTracker"), multiTenancySides: MultiTenancySides.Tenant);
            essentialTracker.CreateChildPermission(AppPermissions.Pages_EssentialTracker_AddNotes, L("EssentialTrackerAddNotes"), multiTenancySides: MultiTenancySides.Tenant);
            essentialTracker.CreateChildPermission(AppPermissions.Pages_EssentialTracker_DocumentRequest, L("EssentialTrackerDocumentRequest"), multiTenancySides: MultiTenancySides.Tenant);
            essentialTracker.CreateChildPermission(AppPermissions.Pages_EssentialTracker_Email, L("EssentialTrackerEmail"), multiTenancySides: MultiTenancySides.Tenant);
            essentialTracker.CreateChildPermission(AppPermissions.Pages_EssentialTracker_JobDetail, L("EssentialTrackerJobDetail"), multiTenancySides: MultiTenancySides.Tenant);
            essentialTracker.CreateChildPermission(AppPermissions.Pages_EssentialTracker_Notify, L("EssentialTrackerNotify"), multiTenancySides: MultiTenancySides.Tenant);
            essentialTracker.CreateChildPermission(AppPermissions.Pages_EssentialTracker_QuickView, L("EssentialTrackerQuickView"), multiTenancySides: MultiTenancySides.Tenant);
            essentialTracker.CreateChildPermission(AppPermissions.Pages_EssentialTracker_SMS, L("EssentialTrackerSMS"), multiTenancySides: MultiTenancySides.Tenant);
            essentialTracker.CreateChildPermission(AppPermissions.Pages_EssentialTracker_Job_Edit, L("EssentialTrackerJobEdit"), multiTenancySides: MultiTenancySides.Tenant);
            essentialTracker.CreateChildPermission(AppPermissions.Pages_EssentialTracker_Export, L("EssentialTrackerExport"), multiTenancySides: MultiTenancySides.Tenant);
            //applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_Import, L("ApplicationTrackerImport"), multiTenancySides: MultiTenancySides.Tenant);
            essentialTracker.CreateChildPermission(AppPermissions.Pages_EssentialTracker_ToDo, L("EssentialTrackerToDo"), multiTenancySides: MultiTenancySides.Tenant);
            essentialTracker.CreateChildPermission(AppPermissions.Pages_EssentialTracker_Reminder, L("EssentialTrackerReminder"), multiTenancySides: MultiTenancySides.Tenant);
            essentialTracker.CreateChildPermission(AppPermissions.Pages_EssentialTracker_Comment, L("EssentialTrackerComment"), multiTenancySides: MultiTenancySides.Tenant);


            var applicationTracker = Tracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker, L("ApplicationTracker"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_AddNotes, L("ApplicationTrackerAddNotes"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_DocumentRequest, L("ApplicationTrackerDocumentRequest"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_Email, L("ApplicationTrackerEmail"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_JobDetail, L("ApplicationTrackerJobDetail"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_Notify, L("ApplicationTrackerNotify"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_QuickView, L("ApplicationTrackerQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_SMS, L("ApplicationTrackerSMS"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_Job_Edit, L("ApplicationTrackerJobEdit"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_Export, L("ApplicationTrackerExport"), multiTenancySides: MultiTenancySides.Tenant);
			//applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_Import, L("ApplicationTrackerImport"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_ToDo, L("ApplicationTrackerToDo"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_Reminder, L("ApplicationTrackerReminder"), multiTenancySides: MultiTenancySides.Tenant);
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_Comment, L("ApplicationTrackerComment"), multiTenancySides: MultiTenancySides.Tenant);

            var applicationFeeTracker = Tracker.CreateChildPermission(AppPermissions.Pages_ApplicationFeeTracker, L("ApplicationFeeTracker"), multiTenancySides: MultiTenancySides.Tenant);


            var freebiesTracker = Tracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker, L("FreebiesTracker"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_AddTrackingNumber, L("FreebiesTrackerAddTrackingNumber"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_Email, L("FreebiesTrackerEmail"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_Notify, L("FreebiesTrackerNotify"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_QuickView, L("FreebiesTrackerQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_SMS, L("FreebiesTrackerSMS"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_Job_Edit, L("FreebiesTrackerJobEdit"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
			//freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_Import, L("FreebiesTrackerImport"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
			freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);

			var financeTracker = Tracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker, L("FinanceTracker"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_AddDetails, L("FinanceTrackerAddDetails"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_Email, L("FinanceTrackerEmail"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_Notify, L("FinanceTrackerNotify"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_QuickView, L("FinanceTrackerQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_SMS, L("FinanceTrackerSMS"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_Job_Edit, L("FinanceTrackerJobEdit"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
			//financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_Import, L("FinanceTrackerImport"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
			financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);

			var refundTracker = Tracker.CreateChildPermission(AppPermissions.Pages_RefundTracker, L("RefundTracker"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Delete, L("RefundTrackerDelete"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Email, L("RefundTrackerEmail"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Notify, L("RefundTrackerNotify"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_QuickView, L("RefundTrackerQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Refund, L("RefundTrackerRefund"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_SMS, L("RefundTrackerSMS"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Job_Edit, L("RefundTrackerJobEdit"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
			//refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Import, L("RefundTrackerImport"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Verify, L("RefundTrackerVerify"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_NotVerify, L("RefundTrackerNotVerify"), multiTenancySides: MultiTenancySides.Tenant);
			refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_Download, L("RefundPDF"), multiTenancySides: MultiTenancySides.Tenant);

            var Referral = Tracker.CreateChildPermission(AppPermissions.Pages_Tracker_Referral, L("ReferralTracker"), multiTenancySides: MultiTenancySides.Tenant);
            Referral.CreateChildPermission(AppPermissions.Pages_Tracker_Referral_Payment, L("ReferralPayment"), multiTenancySides: MultiTenancySides.Tenant);
            Referral.CreateChildPermission(AppPermissions.Pages_Tracker_Referral_QuickView, L("QuickView"), multiTenancySides: MultiTenancySides.Tenant);
            Referral.CreateChildPermission(AppPermissions.Pages_Tracker_Referral_AddNotes, L("AddNotes"), multiTenancySides: MultiTenancySides.Tenant);
            Referral.CreateChildPermission(AppPermissions.Pages_Tracker_Referral_Notify, L("Notify"), multiTenancySides: MultiTenancySides.Tenant);
            Referral.CreateChildPermission(AppPermissions.Pages_Tracker_Referral_SMS, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
            Referral.CreateChildPermission(AppPermissions.Pages_Tracker_Referral_Email, L("Email"), multiTenancySides: MultiTenancySides.Tenant);
            Referral.CreateChildPermission(AppPermissions.Pages_Tracker_Referral_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
            Referral.CreateChildPermission(AppPermissions.Pages_Tracker_Referral_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);
            Referral.CreateChildPermission(AppPermissions.Pages_Tracker_Referral_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
            Referral.CreateChildPermission(AppPermissions.Pages_Tracker_Referral_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            Referral.CreateChildPermission(AppPermissions.Pages_Tracker_Referral_Verify, L("ReferralVerify"), multiTenancySides: MultiTenancySides.Tenant);
            Referral.CreateChildPermission(AppPermissions.Pages_Tracker_Referral_NotVerify, L("ReferralNotVerify"), multiTenancySides: MultiTenancySides.Tenant);
            Referral.CreateChildPermission(AppPermissions.Pages_Tracker_Referral_RevertNotes, L("RevertNotes"), multiTenancySides: MultiTenancySides.Tenant);

            var JobActive = Tracker.CreateChildPermission(AppPermissions.Pages_JobActiveTracker, L("JobActiveList"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_AddDetails, L("ActiveJobAddDetails"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_Email, L("ActiveJobEmail"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_Notify, L("ActiveJobNotify"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_QuickView, L("ActiveJobQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_SMS, L("ActiveJobSMS"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_Job_Edit, L("ActiveJobEdit"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
			//JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_Import, L("ActiveJobImport"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);
			JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_Export_CheckActive, L("CheckActiveExcel"), multiTenancySides: MultiTenancySides.Tenant);


			

			var Gridconnection = Tracker.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker, L("gridconnectiontraker"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Detail, L("Gridconntiondetail"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_AddMeterDetail, L("GridConnectionAddMeter"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Job_Edit, L("GridConnectionJobEdit"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Export, L("GridconnectiontrackerJobExport"), multiTenancySides: MultiTenancySides.Tenant);
			//Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Import, L("GridconnectiontrackerJobImport"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_JobDetail, L("GridconnectiontrackerJobDetail"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Notify, L("GridconnectiontrackerNotify"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_QuickView, L("GridconnectiontrackerQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_SMS, L("GridconnectiontrackerSMS"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_AddNote, L("GridconnectiontrackerAddNote"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Email, L("GridconnectiontrackerEmail"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Delete, L("GridconnectiontrackerDelete"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
			Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);

			var stcTracker = Tracker.CreateChildPermission(AppPermissions.Pages_STCTracker, L("STCTracker"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Edit, L("STCTrackerEdit"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Job_Edit, L("STCTrackerJobEdit"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Delete, L("STCTrackerJobDelete"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Export, L("STCTrackerJobExport"), multiTenancySides: MultiTenancySides.Tenant);
			//stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Import, L("STCTrackerJobImport"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_JobDetail, L("STCTrackerJobDetail"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Notify, L("STCTrackerNotify"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_QuickView, L("STCTrackerQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_SMS, L("STCTrackerSMS"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_AddSTC, L("STCTrackerAddSTC"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Email, L("STCTrackerEmail"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
			stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);

			
			
			

			var Warranty = Tracker.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty, L("Warranty"), multiTenancySides: MultiTenancySides.Tenant);
			Warranty.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty_DocUpload, L("WarrantyDocUpload"), multiTenancySides: MultiTenancySides.Tenant);
			Warranty.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty_QuickView, L("WarrantyQuickView"), multiTenancySides: MultiTenancySides.Tenant);
			Warranty.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty_Email, L("Email"), multiTenancySides: MultiTenancySides.Tenant);
			Warranty.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty_Sms, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
			Warranty.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);
			Warranty.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty_Notify, L("Notify"), multiTenancySides: MultiTenancySides.Tenant);
			Warranty.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
			Warranty.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
			Warranty.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);

			var holdjobtracker = Tracker.CreateChildPermission(AppPermissions.Pages_HoldJobTracker, L("HoldJob"), multiTenancySides: MultiTenancySides.Tenant);
			holdjobtracker.CreateChildPermission(AppPermissions.Pages_HoldJobTracker_Edit, L("HoldJobTrackerEdit"), multiTenancySides: MultiTenancySides.Tenant);
			holdjobtracker.CreateChildPermission(AppPermissions.Pages_HoldJobTracker_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
			holdjobtracker.CreateChildPermission(AppPermissions.Pages_HoldJobTracker_JobDetail, L("JobDetail"), multiTenancySides: MultiTenancySides.Tenant);
			holdjobtracker.CreateChildPermission(AppPermissions.Pages_HoldJobTracker_Notify, L("Notify"), multiTenancySides: MultiTenancySides.Tenant);
			holdjobtracker.CreateChildPermission(AppPermissions.Pages_HoldJobTracker_QuickView, L("QuickView"), multiTenancySides: MultiTenancySides.Tenant);
			holdjobtracker.CreateChildPermission(AppPermissions.Pages_HoldJobTracker_SMS, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
			holdjobtracker.CreateChildPermission(AppPermissions.Pages_HoldJobTracker_Email, L("Email"), multiTenancySides: MultiTenancySides.Tenant);
			holdjobtracker.CreateChildPermission(AppPermissions.Pages_HoldJobTracker_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
			holdjobtracker.CreateChildPermission(AppPermissions.Pages_HoldJobTracker_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);
			holdjobtracker.CreateChildPermission(AppPermissions.Pages_HoldJobTracker_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);

            var transportCost = Tracker.CreateChildPermission(AppPermissions.Pages_TransportCost, L("TransportCost"), multiTenancySides: MultiTenancySides.Tenant);
            transportCost.CreateChildPermission(AppPermissions.Pages_TransportCost_Create, L("CreateTransportCost"), multiTenancySides: MultiTenancySides.Tenant);
            transportCost.CreateChildPermission(AppPermissions.Pages_TransportCost_Edit, L("EditTransportCost"), multiTenancySides: MultiTenancySides.Tenant);
            transportCost.CreateChildPermission(AppPermissions.Pages_TransportCost_Delete, L("DeleteTransportCost"), multiTenancySides: MultiTenancySides.Tenant);

            var CIMETTracker = Tracker.CreateChildPermission(AppPermissions.Pages_CIMETTracker, L("CIMETTracker"), multiTenancySides: MultiTenancySides.Tenant);


            #endregion

            #region jobGrid

            var jobGrid = retail.CreateChildPermission(AppPermissions.Pages_JobGrid, L("JobGrid"), multiTenancySides: MultiTenancySides.Tenant);
            jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_Edit, L("JobGridEdit"), multiTenancySides: MultiTenancySides.Tenant);
            jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_Export, L("JobGridExport"), multiTenancySides: MultiTenancySides.Tenant);
            jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_Export_ProductDetailItemWise, L("JobGridExportWithProductItemDetail"), multiTenancySides: MultiTenancySides.Tenant);
            jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_Export_ProductDetail, L("JobGridExportWithProductDetail"), multiTenancySides: MultiTenancySides.Tenant);
            //jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_Import, L("JobGridImport"), multiTenancySides: MultiTenancySides.Tenant);
            jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_JobDetail, L("JobGridJobDetail"), multiTenancySides: MultiTenancySides.Tenant);
            jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_Notify, L("JobGridNotify"), multiTenancySides: MultiTenancySides.Tenant);
            jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_QuickView, L("JobGridQuickView"), multiTenancySides: MultiTenancySides.Tenant);
            jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_SMS, L("JobGridSMS"), multiTenancySides: MultiTenancySides.Tenant);
            jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_Email, L("JobGridEmail"), multiTenancySides: MultiTenancySides.Tenant);
            jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
            jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);
            jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
            jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_ProjectOpenDate, L("ProjectOpenDate"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion
            var jobs = retail.CreateChildPermission(AppPermissions.Pages_Jobs, L("Jobs"), multiTenancySides: MultiTenancySides.Tenant);

			#region Jobs
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_Create, L("CreateNewJob"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_Edit, L("EditJob"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_Delete, L("DeleteJob"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_Sales, L("Sales"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_Quotation, L("Quotation"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_Installation, L("Installation"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_PostInstallation, L("PostInstallation"), multiTenancySides: MultiTenancySides.Tenant);

            var jobStatus = jobs.CreateChildPermission(AppPermissions.Pages_Jobs_StatusChange, L("JobStatusChange"), multiTenancySides: MultiTenancySides.Tenant);
            jobStatus.CreateChildPermission(AppPermissions.Pages_Jobs_StatusChange_Planned, L("Planned"), multiTenancySides: MultiTenancySides.Tenant);
            jobStatus.CreateChildPermission(AppPermissions.Pages_Jobs_StatusChange_Hold, L("Hold"), multiTenancySides: MultiTenancySides.Tenant);
            jobStatus.CreateChildPermission(AppPermissions.Pages_Jobs_StatusChange_Cancel, L("Cancel"), multiTenancySides: MultiTenancySides.Tenant);
            jobStatus.CreateChildPermission(AppPermissions.Pages_Jobs_StatusChange_DepositReceived, L("DepositReceived"), multiTenancySides: MultiTenancySides.Tenant);
            jobStatus.CreateChildPermission(AppPermissions.Pages_Jobs_StatusChange_Active, L("Active"), multiTenancySides: MultiTenancySides.Tenant);

			//Jobs Details
            jobs.CreateChildPermission(AppPermissions.Pages_Jobs_Detail, L("Detail"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_JobPickList_Create, L("CreateNewPickList"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_JobPickList_Edit, L("EditPickList"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_JobPickList_Delete, L("DeletePickList"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_WithSurchargePayWay, L("WithSurchargePayWay"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_WithoutSurchargePayWay, L("WithoutSurchargePayWay"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_Variation, L("VariationManagerDiscount"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_CheckProductStock, L("CheckProductStock"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_SecondPicklistWithPanelInverter, L("SecondPicklistWithPanelInverter"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_Jobs_CheckActualAudit, L("CheckActualAudit"), multiTenancySides: MultiTenancySides.Tenant);
			jobs.CreateChildPermission(AppPermissions.Pages_JobPickList_NagativeStock, L("NagativeStockPickList"), multiTenancySides: MultiTenancySides.Tenant);
            var bonus = jobs.CreateChildPermission(AppPermissions.Pages_Jobs_ShowIsBonus, L("ShowIsBonus"), multiTenancySides: MultiTenancySides.Tenant);
            bonus.CreateChildPermission(AppPermissions.Pages_Jobs_ShowIsBonus_Amount, L("Amount"), multiTenancySides: MultiTenancySides.Tenant);
            jobs.CreateChildPermission(AppPermissions.Pages_Jobs_EditOnHold, L("EditOnHold"), multiTenancySides: MultiTenancySides.Tenant);
            //Refund Tab
            var jobRefunds = jobs.CreateChildPermission(AppPermissions.Pages_Job_Refunds, L("JobRefunds"), multiTenancySides: MultiTenancySides.Tenant);
			jobRefunds.CreateChildPermission(AppPermissions.Pages_Job_Refunds_Create, L("CreateNewJobRefund"), multiTenancySides: MultiTenancySides.Tenant);
			jobRefunds.CreateChildPermission(AppPermissions.Pages_Job_Refunds_Edit, L("EditJobRefund"), multiTenancySides: MultiTenancySides.Tenant);
			jobRefunds.CreateChildPermission(AppPermissions.Pages_Job_Refunds_Delete, L("DeleteJobRefund"), multiTenancySides: MultiTenancySides.Tenant);

			var quotations = jobs.CreateChildPermission(AppPermissions.Pages_Quotations, L("Quotations"), multiTenancySides: MultiTenancySides.Tenant);
			quotations.CreateChildPermission(AppPermissions.Pages_Quotations_Create, L("CreateNewQuotation"), multiTenancySides: MultiTenancySides.Tenant);
			quotations.CreateChildPermission(AppPermissions.Pages_Quotations_Edit, L("EditQuotations"), multiTenancySides: MultiTenancySides.Tenant);
			quotations.CreateChildPermission(AppPermissions.Pages_Quotations_Delete, L("DeleteQuotations"), multiTenancySides: MultiTenancySides.Tenant);

			var jobProductItems = jobs.CreateChildPermission(AppPermissions.Pages_JobProductItems, L("JobProductItems"), multiTenancySides: MultiTenancySides.Tenant);
			jobProductItems.CreateChildPermission(AppPermissions.Pages_JobProductItems_Create, L("CreateNewJobProductItem"), multiTenancySides: MultiTenancySides.Tenant);
			jobProductItems.CreateChildPermission(AppPermissions.Pages_JobProductItems_Edit, L("EditJobProductItem"), multiTenancySides: MultiTenancySides.Tenant);
			jobProductItems.CreateChildPermission(AppPermissions.Pages_JobProductItems_Delete, L("DeleteJobProductItem"), multiTenancySides: MultiTenancySides.Tenant);

			var installerDetails = jobs.CreateChildPermission(AppPermissions.Pages_InstallerDetails, L("InstallerDetails"), multiTenancySides: MultiTenancySides.Tenant);
			installerDetails.CreateChildPermission(AppPermissions.Pages_InstallerDetails_Create, L("CreateNewInstallerDetail"), multiTenancySides: MultiTenancySides.Tenant);
			installerDetails.CreateChildPermission(AppPermissions.Pages_InstallerDetails_Edit, L("EditInstallerDetail"), multiTenancySides: MultiTenancySides.Tenant);
			installerDetails.CreateChildPermission(AppPermissions.Pages_InstallerDetails_Delete, L("DeleteInstallerDetail"), multiTenancySides: MultiTenancySides.Tenant);


			var installerGroup = jobs.CreateChildPermission(AppPermissions.Pages_Tenant_Installer, L("Installer"), multiTenancySides: MultiTenancySides.Tenant);
			installerGroup.CreateChildPermission(AppPermissions.Pages_Tenant_Installer_Availability, L("InstallerAvailability"), multiTenancySides: MultiTenancySides.Tenant);
			installerGroup.CreateChildPermission(AppPermissions.Pages_Tenant_Installer_Invoice, L("InstallerInvoice"), multiTenancySides: MultiTenancySides.Tenant);


			var installerAddresses = jobs.CreateChildPermission(AppPermissions.Pages_InstallerAddresses, L("InstallerAddresses"), multiTenancySides: MultiTenancySides.Tenant);
			installerAddresses.CreateChildPermission(AppPermissions.Pages_InstallerAddresses_Create, L("CreateNewInstallerAddress"), multiTenancySides: MultiTenancySides.Tenant);
			installerAddresses.CreateChildPermission(AppPermissions.Pages_InstallerAddresses_Edit, L("EditInstallerAddress"), multiTenancySides: MultiTenancySides.Tenant);
			installerAddresses.CreateChildPermission(AppPermissions.Pages_InstallerAddresses_Delete, L("DeleteInstallerAddress"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion

            #region Service Permission
            var serviceFeatureDependency = new SimpleFeatureDependency(AppFeatures.Service);
            var service = retail.CreateChildPermission(AppPermissions.Pages_Service, L("Service"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: serviceFeatureDependency);

            var manageServiceFeatureDependency = new SimpleFeatureDependency(AppFeatures.Service_ManageService);
            var ManageService = service.CreateChildPermission(AppPermissions.Pages_Lead_ManageService, L("ManageService"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: manageServiceFeatureDependency);

            var myserviceFeatureDependency = new SimpleFeatureDependency(AppFeatures.Service_MyService);
            var myservice = service.CreateChildPermission(AppPermissions.Pages_MyService, L("MyService"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: myserviceFeatureDependency);
            myservice.CreateChildPermission(AppPermissions.Pages_MyService_AddServiceInstaller, L("AddServiceInstaller"), multiTenancySides: MultiTenancySides.Tenant);
            myservice.CreateChildPermission(AppPermissions.Pages_MyService_Create, L("MyServiceCreate"), multiTenancySides: MultiTenancySides.Tenant);
            myservice.CreateChildPermission(AppPermissions.Pages_MyService_Edit, L("Edit"), multiTenancySides: MultiTenancySides.Tenant);
            myservice.CreateChildPermission(AppPermissions.Pages_MyService_SMS, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
            myservice.CreateChildPermission(AppPermissions.Pages_MyService_Email, L("Email"), multiTenancySides: MultiTenancySides.Tenant);
            myservice.CreateChildPermission(AppPermissions.Pages_MyService_Notify, L("Notify"), multiTenancySides: MultiTenancySides.Tenant);
            myservice.CreateChildPermission(AppPermissions.Pages_MyService_QuickView, L("QuickView"), multiTenancySides: MultiTenancySides.Tenant);
            myservice.CreateChildPermission(AppPermissions.Pages_MyService_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
            myservice.CreateChildPermission(AppPermissions.Pages_MyService_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
            myservice.CreateChildPermission(AppPermissions.Pages_MyService_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);
            myservice.CreateChildPermission(AppPermissions.Pages_MyService_AssignUser, L("AssignUser"), multiTenancySides: MultiTenancySides.Tenant);
            myservice.CreateChildPermission(AppPermissions.Pages_MyService_Delete, L("Delete"), multiTenancySides: MultiTenancySides.Tenant);
            myservice.CreateChildPermission(AppPermissions.Pages_MyService_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);

            var mapFeatureDependency = new SimpleFeatureDependency(AppFeatures.Service_Map);
            var ServiceMap = service.CreateChildPermission(AppPermissions.Pages_ServiceMap, L("ServiceMap"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: mapFeatureDependency);

            var serviceInstallationFeatureDependency = new SimpleFeatureDependency(AppFeatures.Service_Map);
            var Serviceinstalltion = service.CreateChildPermission(AppPermissions.Pages_ServiceInstallation, L("ServiceInstallation"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: serviceInstallationFeatureDependency);
            Serviceinstalltion.CreateChildPermission(AppPermissions.Pages_ServiceInstaller, L("ServiceInstaller"), multiTenancySides: MultiTenancySides.Tenant);
            Serviceinstalltion.CreateChildPermission(AppPermissions.Pages_ServiceInstallationJobView, L("JobView"), multiTenancySides: MultiTenancySides.Tenant);

            
            var warrantyClaim = service.CreateChildPermission(AppPermissions.Pages_WarrantyClaim, L("WarrantyClaim"), multiTenancySides: MultiTenancySides.Tenant);
            warrantyClaim.CreateChildPermission(AppPermissions.Pages_WarrantyClaim_AddServiceInstaller, L("AddServiceInstaller"), multiTenancySides: MultiTenancySides.Tenant);
            warrantyClaim.CreateChildPermission(AppPermissions.Pages_WarrantyClaim_Edit, L("Edit"), multiTenancySides: MultiTenancySides.Tenant);
            warrantyClaim.CreateChildPermission(AppPermissions.Pages_WarrantyClaim_SMS, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
            warrantyClaim.CreateChildPermission(AppPermissions.Pages_WarrantyClaim_Email, L("Email"), multiTenancySides: MultiTenancySides.Tenant);
            warrantyClaim.CreateChildPermission(AppPermissions.Pages_WarrantyClaim_Notify, L("Notify"), multiTenancySides: MultiTenancySides.Tenant);
            warrantyClaim.CreateChildPermission(AppPermissions.Pages_WarrantyClaim_QuickView, L("QuickView"), multiTenancySides: MultiTenancySides.Tenant);
            warrantyClaim.CreateChildPermission(AppPermissions.Pages_WarrantyClaim_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
            warrantyClaim.CreateChildPermission(AppPermissions.Pages_WarrantyClaim_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
            warrantyClaim.CreateChildPermission(AppPermissions.Pages_WarrantyClaim_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);
            warrantyClaim.CreateChildPermission(AppPermissions.Pages_WarrantyClaim_AssignUser, L("AssignUser"), multiTenancySides: MultiTenancySides.Tenant);
            warrantyClaim.CreateChildPermission(AppPermissions.Pages_WarrantyClaim_Delete, L("Delete"), multiTenancySides: MultiTenancySides.Tenant);
            warrantyClaim.CreateChildPermission(AppPermissions.Pages_WarrantyClaim_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            warrantyClaim.CreateChildPermission(AppPermissions.Pages_WarrantyClaim_AddInvoice, L("AddInvoice"), multiTenancySides: MultiTenancySides.Tenant);
            warrantyClaim.CreateChildPermission(AppPermissions.Pages_WarrantyClaim_AddNotes, L("AddNotes"), multiTenancySides: MultiTenancySides.Tenant);
            warrantyClaim.CreateChildPermission(AppPermissions.Pages_WarrantyClaim_PickupItem, L("PickupItem"), multiTenancySides: MultiTenancySides.Tenant);
            var serviceInvoice = service.CreateChildPermission(AppPermissions.Pages_ServiceInvoice, L("ServiceInvoice"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            var invoice = retail.CreateChildPermission(AppPermissions.Pages_Invoice, L("Invoice"), multiTenancySides: MultiTenancySides.Tenant);
            #region Invoice

            var invoiceIssuedTracker = invoice.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker, L("InvoiceIssuedTracker"), multiTenancySides: MultiTenancySides.Tenant);
            invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_Email, L("Email"), multiTenancySides: MultiTenancySides.Tenant);
            invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_Notify, L("Notify"), multiTenancySides: MultiTenancySides.Tenant);
            invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_QuickView, L("QuickView"), multiTenancySides: MultiTenancySides.Tenant);
            invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_SMS, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
            invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_Job_Edit, L("JobEdit"), multiTenancySides: MultiTenancySides.Tenant);
            invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_Export_MobileEmail, L("ExportToExcelMobileEmail"), multiTenancySides: MultiTenancySides.Tenant);
            invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_Import, L("ImportFromExcel"), multiTenancySides: MultiTenancySides.Tenant);
            invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
            invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
            invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);


            var invoicepaidTracker = invoice.CreateChildPermission(AppPermissions.Pages_InvoiceTracker, L("InvoicePaid"), multiTenancySides: MultiTenancySides.Tenant);
            invoicepaidTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Email, L("Email"), multiTenancySides: MultiTenancySides.Tenant);
            invoicepaidTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Notify, L("Notify"), multiTenancySides: MultiTenancySides.Tenant);
            invoicepaidTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_QuickView, L("QuickView"), multiTenancySides: MultiTenancySides.Tenant);
            invoicepaidTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_SMS, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
            invoicepaidTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Verify, L("InvoiceTrackerVerify"), multiTenancySides: MultiTenancySides.Tenant);
            invoicepaidTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Job_Edit, L("JobEdit"), multiTenancySides: MultiTenancySides.Tenant);
            invoicepaidTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            invoicepaidTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Import, L("ImportFromExcel"), multiTenancySides: MultiTenancySides.Tenant);
            invoicepaidTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
            invoicepaidTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
            invoicepaidTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);
            invoicepaidTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Edit, L("InvoiceTrackerEdit"), multiTenancySides: MultiTenancySides.Tenant);
            invoicepaidTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_Delete, L("InvoiceTrackerDelete"), multiTenancySides: MultiTenancySides.Tenant);
            var createManualInvoice = new SimpleFeatureDependency(AppFeatures.InvoicePaid_CreateManually);
            invoicepaidTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_CreateManually, L("CreateManuallyInvoice"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: createManualInvoice);

            var invoicePayWay = invoice.CreateChildPermission(AppPermissions.Pages_InvoicePayWay, L("InvoicePayWay"), multiTenancySides: MultiTenancySides.Tenant);
            invoicePayWay.CreateChildPermission(AppPermissions.Pages_InvoicePayWay_Email, L("Email"), multiTenancySides: MultiTenancySides.Tenant);
            invoicePayWay.CreateChildPermission(AppPermissions.Pages_InvoicePayWay_Notify, L("Notify"), multiTenancySides: MultiTenancySides.Tenant);
            invoicePayWay.CreateChildPermission(AppPermissions.Pages_InvoicePayWay_QuickView, L("QuickView"), multiTenancySides: MultiTenancySides.Tenant);
            invoicePayWay.CreateChildPermission(AppPermissions.Pages_InvoicePayWay_SMS, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
            invoicePayWay.CreateChildPermission(AppPermissions.Pages_InvoicePayWay_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
            invoicePayWay.CreateChildPermission(AppPermissions.Pages_InvoicePayWay_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
            invoicePayWay.CreateChildPermission(AppPermissions.Pages_InvoicePayWay_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);

            var invoicePayments = invoice.CreateChildPermission(AppPermissions.Pages_InvoicePayments, L("InvoicePayments"), multiTenancySides: MultiTenancySides.Tenant);
            invoicePayments.CreateChildPermission(AppPermissions.Pages_InvoicePayments_Create, L("CreateNewInvoicePayment"), multiTenancySides: MultiTenancySides.Tenant);
            invoicePayments.CreateChildPermission(AppPermissions.Pages_InvoicePayments_Edit, L("EditInvoicePayment"), multiTenancySides: MultiTenancySides.Tenant);
            invoicePayments.CreateChildPermission(AppPermissions.Pages_InvoicePayments_Delete, L("DeleteInvoicePayment"), multiTenancySides: MultiTenancySides.Tenant);
            invoicePayments.CreateChildPermission(AppPermissions.Pages_InvoicePayments_Verify, L("InvoicePaymentsVerify"), multiTenancySides: MultiTenancySides.Tenant);
          
            var InvoiceFileList = invoice.CreateChildPermission(AppPermissions.Pages_InvoiceFileList, L("InvoiceFileList"), multiTenancySides: MultiTenancySides.Tenant);
            
            #endregion

            var Installation = retail.CreateChildPermission(AppPermissions.Pages_InstallationSection, L("InstallationSection"), multiTenancySides: MultiTenancySides.Tenant);
            #region Installation
            var myinstaller = Installation.CreateChildPermission(AppPermissions.Pages_MyInstaller, L("MyInstaller"), multiTenancySides: MultiTenancySides.Tenant);
            myinstaller.CreateChildPermission(AppPermissions.Pages_MyInstaller_Create, L("MyInstallerCreate"), multiTenancySides: MultiTenancySides.Tenant);
            myinstaller.CreateChildPermission(AppPermissions.Pages_MyInstaller_Edit, L("MyInstallerEdit"), multiTenancySides: MultiTenancySides.Tenant);
            myinstaller.CreateChildPermission(AppPermissions.Pages_MyInstaller_View, L("MyInstallerView"), multiTenancySides: MultiTenancySides.Tenant);
            myinstaller.CreateChildPermission(AppPermissions.Pages_MyInstaller_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);
            myinstaller.CreateChildPermission(AppPermissions.Pages_MyInstaller_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
            myinstaller.CreateChildPermission(AppPermissions.Pages_MyInstaller_Sms, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
            myinstaller.CreateChildPermission(AppPermissions.Pages_MyInstaller_Email, L("Email"), multiTenancySides: MultiTenancySides.Tenant);
            myinstaller.CreateChildPermission(AppPermissions.Pages_MyInstaller_Notify, L("Notify"), multiTenancySides: MultiTenancySides.Tenant);
            myinstaller.CreateChildPermission(AppPermissions.Pages_MyInstaller_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
            myinstaller.CreateChildPermission(AppPermissions.Pages_MyInstaller_UploadDoc, L("UploadDoc"), multiTenancySides: MultiTenancySides.Tenant);
            myinstaller.CreateChildPermission(AppPermissions.Pages_MyInstaller_PriceList, L("PriceList"), multiTenancySides: MultiTenancySides.Tenant);
            myinstaller.CreateChildPermission(AppPermissions.Pages_MyInstaller_SignContract, L("SignContract"), multiTenancySides: MultiTenancySides.Tenant);
            myinstaller.CreateChildPermission(AppPermissions.Pages_MyInstaller_AssignUser, L("AssignUser"), multiTenancySides: MultiTenancySides.Tenant);
            myinstaller.CreateChildPermission(AppPermissions.Pages_MyInstaller_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);

            var PenddingInstallation = Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation, L("JobAssign"), multiTenancySides: MultiTenancySides.Tenant);
            PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_View, L("QuickView"), multiTenancySides: MultiTenancySides.Tenant);
            PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
            PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Sms, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
            PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Email, L("Email"), multiTenancySides: MultiTenancySides.Tenant);
            PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);
            PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Notify, L("Notify"), multiTenancySides: MultiTenancySides.Tenant);
            PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
            PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_AssignUser, L("AssignUser"), multiTenancySides: MultiTenancySides.Tenant);
            PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);

            //Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation, L("pendinginstallation"), multiTenancySides: MultiTenancySides.Tenant);
            //Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_View, L("pendinginstallationView"), multiTenancySides: MultiTenancySides.Tenant);
            //Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_ToDo, L("pendinginstallationToDo"), multiTenancySides: MultiTenancySides.Tenant);
            //Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_ToDo, L("pendinginstallationSMS"), multiTenancySides: MultiTenancySides.Tenant);
            //Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Email, L("pendinginstallationEmail"), multiTenancySides: MultiTenancySides.Tenant);
            //Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Comment, L("pendinginstallationComment"), multiTenancySides: MultiTenancySides.Tenant);
            //Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Notify, L("pendinginstallationNotify"), multiTenancySides: MultiTenancySides.Tenant);
            //Installation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_Reminder, L("pendinginstallationReminder"), multiTenancySides: MultiTenancySides.Tenant);
            var Map = Installation.CreateChildPermission(AppPermissions.Pages_Map, L("Map"), multiTenancySides: MultiTenancySides.Tenant);
            Map.CreateChildPermission(AppPermissions.Pages_Map_JobView, L("JobView"), multiTenancySides: MultiTenancySides.Tenant);


            var JobBooking = Installation.CreateChildPermission(AppPermissions.Pages_JobBooking, L("JobBooking"), multiTenancySides: MultiTenancySides.Tenant);
            JobBooking.CreateChildPermission(AppPermissions.Pages_JobBooking_Notify, L("Notify"), multiTenancySides: MultiTenancySides.Tenant);
            JobBooking.CreateChildPermission(AppPermissions.Pages_JobBooking_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
            JobBooking.CreateChildPermission(AppPermissions.Pages_JobBooking_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);
            JobBooking.CreateChildPermission(AppPermissions.Pages_JobBooking_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
            JobBooking.CreateChildPermission(AppPermissions.Pages_JobBooking_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            JobBooking.CreateChildPermission(AppPermissions.Pages_JobBooking_ViewJobDetail, L("JobDetail"), multiTenancySides: MultiTenancySides.Tenant);
            JobBooking.CreateChildPermission(AppPermissions.Pages_JobBooking_Sms, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
            JobBooking.CreateChildPermission(AppPermissions.Pages_JobBookingn_Email, L("Email"), multiTenancySides: MultiTenancySides.Tenant);

            
            var installerCalendar = Installation.CreateChildPermission(AppPermissions.Pages_Calenders, L("Calenders"), multiTenancySides: MultiTenancySides.Tenant);
            installerCalendar.CreateChildPermission(AppPermissions.Pages_Tenant_InstallerManager_InstallerCalendar, L("InstallerCalendar"), multiTenancySides: MultiTenancySides.Tenant);

            var installerInstallation = Installation.CreateChildPermission(AppPermissions.Pages_Installation, L("Installation"), multiTenancySides: MultiTenancySides.Tenant);
            installerInstallation.CreateChildPermission(AppPermissions.Pages_Installation_JobView, L("JobView"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion


            var InstallerInvoice = retail.CreateChildPermission(AppPermissions.Pages_Administration_InstallerInvitation, L("InstallerInvoice"), multiTenancySides: MultiTenancySides.Tenant);

            #region InstallerInvoice
            var newinstallerinvoice = InstallerInvoice.CreateChildPermission(AppPermissions.Pages_Installer_New, L("NewInstallerInvoice"), multiTenancySides: MultiTenancySides.Tenant);
            newinstallerinvoice.CreateChildPermission(AppPermissions.Pages_Installer_New_Create, L("NewInstallerInvoiceCreate"), multiTenancySides: MultiTenancySides.Tenant);
            newinstallerinvoice.CreateChildPermission(AppPermissions.Pages_Installer_New_Edit, L("NewInstallerInvoiceEdit"), multiTenancySides: MultiTenancySides.Tenant);
            newinstallerinvoice.CreateChildPermission(AppPermissions.Pages_Installer_New_Delete, L("NewInstallerInvoiceDelete"), multiTenancySides: MultiTenancySides.Tenant);
            newinstallerinvoice.CreateChildPermission(AppPermissions.Pages_Installer_New_QuickView, L("NewInstallerInvoiceQuickView"), multiTenancySides: MultiTenancySides.Tenant);
            newinstallerinvoice.CreateChildPermission(AppPermissions.Pages_Installer_New_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);

            var installerinvoices = InstallerInvoice.CreateChildPermission(AppPermissions.Pages_Installer_Invoice, L("PendingInvoice"), multiTenancySides: MultiTenancySides.Tenant);
            installerinvoices.CreateChildPermission(AppPermissions.Pages_Installer_Delete, L("PendingInvoiceDelete"), multiTenancySides: MultiTenancySides.Tenant);
            installerinvoices.CreateChildPermission(AppPermissions.Pages_Installer_Edit, L("PendingInvoiceEdit"), multiTenancySides: MultiTenancySides.Tenant);
            installerinvoices.CreateChildPermission(AppPermissions.Pages_Installer_Notify, L("Notify"), multiTenancySides: MultiTenancySides.Tenant);
            installerinvoices.CreateChildPermission(AppPermissions.Pages_Installer_SMS, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
            installerinvoices.CreateChildPermission(AppPermissions.Pages_Installer_Email, L("Email"), multiTenancySides: MultiTenancySides.Tenant);
            installerinvoices.CreateChildPermission(AppPermissions.Pages_Installer_QuickView, L("QuickView"), multiTenancySides: MultiTenancySides.Tenant);
            installerinvoices.CreateChildPermission(AppPermissions.Pages_Installer_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
            installerinvoices.CreateChildPermission(AppPermissions.Pages_Installer_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);
            installerinvoices.CreateChildPermission(AppPermissions.Pages_Installer_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
            installerinvoices.CreateChildPermission(AppPermissions.Pages_Installer_RevertVerify, L("PendingInvoiceRevertVerify"), multiTenancySides: MultiTenancySides.Tenant);
            installerinvoices.CreateChildPermission(AppPermissions.Pages_Installer_ReadyToPay, L("PendingInvoiceReadyToPayInvoice"), multiTenancySides: MultiTenancySides.Tenant);
            installerinvoices.CreateChildPermission(AppPermissions.Pages_Installer_Verify, L("PendingInvoiceVerify"), multiTenancySides: MultiTenancySides.Tenant);
            installerinvoices.CreateChildPermission(AppPermissions.Pages_Installer_ExcelExport, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            installerinvoices.CreateChildPermission(AppPermissions.Pages_Installer_View, L("View"), multiTenancySides: MultiTenancySides.Tenant);


            var readytopay = InstallerInvoice.CreateChildPermission(AppPermissions.Pages_ReadyToPay, L("ReadyToPayInvoice"), multiTenancySides: MultiTenancySides.Tenant);
            readytopay.CreateChildPermission(AppPermissions.Pages_ReadyToPay_Sms, L("Delete"), multiTenancySides: MultiTenancySides.Tenant);
            readytopay.CreateChildPermission(AppPermissions.Pages_ReadyToPay_Email, L("Edit"), multiTenancySides: MultiTenancySides.Tenant);
            readytopay.CreateChildPermission(AppPermissions.Pages_ReadyToPay_Unapproved, L("ReadyToPayUnapproved"), multiTenancySides: MultiTenancySides.Tenant);
            readytopay.CreateChildPermission(AppPermissions.Pages_ReadyToPay_QuickView, L("QuickView"), multiTenancySides: MultiTenancySides.Tenant);
            readytopay.CreateChildPermission(AppPermissions.Pages_ReadyToPay_Payement, L("ReadyToPayPayement"), multiTenancySides: MultiTenancySides.Tenant);
            readytopay.CreateChildPermission(AppPermissions.Pages_ReadyToPay_Payement_Revert, L("ReadyToPayPayementRevert"), multiTenancySides: MultiTenancySides.Tenant);
            readytopay.CreateChildPermission(AppPermissions.Pages_ReadyToPay_Notify, L("Notify"), multiTenancySides: MultiTenancySides.Tenant);
            readytopay.CreateChildPermission(AppPermissions.Pages_ReadyToPay_ExcelExport, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            readytopay.CreateChildPermission(AppPermissions.Pages_ReadyToPay_View, L("View"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #region Leads Generation Permission
            var leadGenerationFeature = new SimpleFeatureDependency(AppFeatures.LeadGeneration);
            var leadGen = retail.CreateChildPermission(AppPermissions.Pages_LeadGeneration, L("LeadGeneration"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: leadGenerationFeature);

            var leadGenMyFeature = new SimpleFeatureDependency(AppFeatures.LeadGeneration_MyLeadsGeneration);
            var leadGenActionFetures = leadGen.CreateChildPermission(AppPermissions.Pages_LeadGeneration_MyLeadsGeneration, L("MyLeadsGeneration"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: leadGenMyFeature);

            var leadGenMapFeature = new SimpleFeatureDependency(AppFeatures.LeadGeneration_Map);
            leadGen.CreateChildPermission(AppPermissions.Pages_LeadGeneration_Map, L("Map"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: leadGenMapFeature);

            var leadGenInstallationFeature = new SimpleFeatureDependency(AppFeatures.LeadGeneration_Installation);
            var leadgeninstalltion = leadGen.CreateChildPermission(AppPermissions.Pages_LeadGeneration_Installation, L("Appointment"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: leadGenInstallationFeature);

            leadGenActionFetures.CreateChildPermission(AppPermissions.Pages_LeadGeneration_MyLeadsGeneration_SMS, L("MyLeadsGenerationSMS"), multiTenancySides: MultiTenancySides.Tenant);
            leadGenActionFetures.CreateChildPermission(AppPermissions.Pages_LeadGeneration_MyLeadsGeneration_Email, L("MyLeadsGenerationEmail"), multiTenancySides: MultiTenancySides.Tenant);
            leadGenActionFetures.CreateChildPermission(AppPermissions.Pages_LeadGeneration_MyLeadsGeneration_Appointment, L("MyLeadsGenerationAppointment"), multiTenancySides: MultiTenancySides.Tenant);
            leadGenActionFetures.CreateChildPermission(AppPermissions.Pages_LeadGeneration_MyLeadsGeneration_Comment, L("MyLeadsGenerationComment"), multiTenancySides: MultiTenancySides.Tenant);
            leadGenActionFetures.CreateChildPermission(AppPermissions.Pages_LeadGeneration_MyLeadsGeneration_ToDo, L("MyLeadsGenerationToDo"), multiTenancySides: MultiTenancySides.Tenant);
            leadGenActionFetures.CreateChildPermission(AppPermissions.Pages_LeadGeneration_MyLeadsGeneration_Reminder, L("MyLeadsGenerationReminder"), multiTenancySides: MultiTenancySides.Tenant);
            leadGenActionFetures.CreateChildPermission(AppPermissions.Pages_LeadGeneration_MyLeadsGeneration_Notify, L("MyLeadsGenerationNotify"), multiTenancySides: MultiTenancySides.Tenant);

            var Commission = leadGen.CreateChildPermission(AppPermissions.Pages_LeadGeneration_Commission, L("Commission"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: leadGenMyFeature);
            Commission.CreateChildPermission(AppPermissions.Pages_LeadGeneration_Commission_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: leadGenMyFeature);
            Commission.CreateChildPermission(AppPermissions.Pages_LeadGeneration_Commission_Payment, L("CommissionPayment"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: leadGenMyFeature);
            Commission.CreateChildPermission(AppPermissions.Pages_LeadGeneration_Commission_QuickView, L("JobGridQuickView"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: leadGenMyFeature);


            #endregion

            #region Review
            var Review = retail.CreateChildPermission(AppPermissions.Pages_Review, L("Review"), multiTenancySides: MultiTenancySides.Tenant);
            Review.CreateChildPermission(AppPermissions.Pages_Review_Email, L("Email"), multiTenancySides: MultiTenancySides.Tenant);
            Review.CreateChildPermission(AppPermissions.Pages_Review_Notify, L("Notify"), multiTenancySides: MultiTenancySides.Tenant);
            Review.CreateChildPermission(AppPermissions.Pages_Review_QuickView, L("QuickView"), multiTenancySides: MultiTenancySides.Tenant);
            Review.CreateChildPermission(AppPermissions.Pages_Review_SMS, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
            Review.CreateChildPermission(AppPermissions.Pages_Review_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
            Review.CreateChildPermission(AppPermissions.Pages_Review_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
            Review.CreateChildPermission(AppPermissions.Pages_Review_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);
            Review.CreateChildPermission(AppPermissions.Pages_Review_AddReview, L("ReviewAddReview"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #region Notification
            var Reply = retail.CreateChildPermission(AppPermissions.Pages_Notification, L("Notification"), multiTenancySides: MultiTenancySides.Tenant);
			var sms = Reply.CreateChildPermission(AppPermissions.Pages_Sms, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
			Reply.CreateChildPermission(AppPermissions.Pages_Email, L("Email"), multiTenancySides: MultiTenancySides.Tenant);
           
            #endregion
            var report = retail.CreateChildPermission(AppPermissions.Pages_report, L("Report"), multiTenancySides: MultiTenancySides.Tenant);
			
			
            #region Report
			var activityReport = report.CreateChildPermission(AppPermissions.Pages_ActivityReport, L("LeadActivityDetail"), multiTenancySides: MultiTenancySides.Tenant);
			var ToDoActivityReport = report.CreateChildPermission(AppPermissions.Pages_ToDoActivityReport, L("TodoActivityDetail"), multiTenancySides: MultiTenancySides.Tenant);
			var LeadexpenseReport = report.CreateChildPermission(AppPermissions.Pages_LeadexpenseReport, L("LeadExpenseReport"), multiTenancySides: MultiTenancySides.Tenant);
			var OutStandingReport = report.CreateChildPermission(AppPermissions.Pages_OutStandingReport, L("OutStandingReport"), multiTenancySides: MultiTenancySides.Tenant);

			var jobCost = report.CreateChildPermission(AppPermissions.Pages_Report_JobCost, L("JobCost"), multiTenancySides: MultiTenancySides.Tenant);
			jobCost.CreateChildPermission(AppPermissions.Pages_Report_JobCost_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
			jobCost.CreateChildPermission(AppPermissions.Pages_Report_JobCost_QuickView, L("QuickView"), multiTenancySides: MultiTenancySides.Tenant);

            var leadAssign = report.CreateChildPermission(AppPermissions.Pages_Report_LeadAssign, L("LeadAssign"), multiTenancySides: MultiTenancySides.Tenant);
            leadAssign.CreateChildPermission(AppPermissions.Pages_Report_LeadAssign_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);

            var leadSold = report.CreateChildPermission(AppPermissions.Pages_Report_LeadSold, L("LeadSold"), multiTenancySides: MultiTenancySides.Tenant);
            leadAssign.CreateChildPermission(AppPermissions.Pages_Report_LeadSold_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);

            var empJobCost = report.CreateChildPermission(AppPermissions.Pages_Report_EmpJobCost, L("EmpJobCost"), multiTenancySides: MultiTenancySides.Tenant);
            empJobCost.CreateChildPermission(AppPermissions.Pages_Report_EmpJobCost_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            empJobCost.CreateChildPermission(AppPermissions.Pages_Report_EmpJobCost_QuickView, L("QuickView"), multiTenancySides: MultiTenancySides.Tenant);

            var JobCommission = report.CreateChildPermission(AppPermissions.Pages_Report_JobCommission, L("JobCommission"), multiTenancySides: MultiTenancySides.Tenant);
            JobCommission.CreateChildPermission(AppPermissions.Pages_Report_JobCommission_Create, L("JobCommissionCreate"), multiTenancySides: MultiTenancySides.Tenant);
            JobCommission.CreateChildPermission(AppPermissions.Pages_Report_JobCommission_Edit, L("JobCommissionEdit"), multiTenancySides: MultiTenancySides.Tenant);
            JobCommission.CreateChildPermission(AppPermissions.Pages_Report_JobCommission_ShowIsVerified, L("ShowIsVerified"), multiTenancySides: MultiTenancySides.Tenant);
            JobCommission.CreateChildPermission(AppPermissions.Pages_Report_JobCommission_ShowIsApproved, L("ShowIsApproved"), multiTenancySides: MultiTenancySides.Tenant);

            var JobCommissionPaid = report.CreateChildPermission(AppPermissions.Pages_Report_JobCommissionPaid, L("JobCommissionPaid"), multiTenancySides: MultiTenancySides.Tenant);

            var ProductSold = report.CreateChildPermission(AppPermissions.Pages_Report_ProductSold, L("ProductSold"), multiTenancySides: MultiTenancySides.Tenant);
            ProductSold.CreateChildPermission(AppPermissions.Pages_Report_ProductSold_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);

			var SmsCount = report.CreateChildPermission(AppPermissions.Pages_Report_SmsCount, L("ProductSold"), multiTenancySides: MultiTenancySides.Tenant);
            SmsCount.CreateChildPermission(AppPermissions.Pages_Report_SmsCount_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);


            var userdetails = report.CreateChildPermission(AppPermissions.Pages_Report_UserDetails, L("UserDetails"), multiTenancySides: MultiTenancySides.Tenant);
            userdetails.CreateChildPermission(AppPermissions.Pages_Report_UserDetails_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);

            var Comparison = report.CreateChildPermission(AppPermissions.Pages_Report_Comparison, L("Comparison"), multiTenancySides: MultiTenancySides.Tenant);
            var LeadComparison = report.CreateChildPermission(AppPermissions.Pages_Report_LeadComparison, L("LeadComparison"), multiTenancySides: MultiTenancySides.Tenant);
            var monthlyComparison = report.CreateChildPermission(AppPermissions.Pages_Report_MonthlyComparison, L("MonthlyComparison"), multiTenancySides: MultiTenancySides.Tenant);


            var InstallerInvoicePayment = report.CreateChildPermission(AppPermissions.Pages_Report_InstallerInvoicePayment, L("InstallerInvoicePayment"), multiTenancySides: MultiTenancySides.Tenant);

            InstallerInvoicePayment.CreateChildPermission(AppPermissions.Pages_Report_InstallerInvoicePayment_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);


            var InstallerWiseInvoicePayment = report.CreateChildPermission(AppPermissions.Pages_Report_InstallerWiseInvoicePayment, L("InstallerWiseInvoicePayment"), multiTenancySides: MultiTenancySides.Tenant);
            InstallerWiseInvoicePayment.CreateChildPermission(AppPermissions.Pages_Report_InstallerJobWiseInvoicePayment, L("InstallerPaymentinstallerDetailsJobWise"), multiTenancySides: MultiTenancySides.Tenant);
            InstallerWiseInvoicePayment.CreateChildPermission(AppPermissions.Pages_Report_InstallerWiseInvoicePayment_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);

            //var transportCost = pages.CreateChildPermission(AppPermissions.Pages_TransportCost, L("TransportCost"), multiTenancySides: MultiTenancySides.Tenant);
            //transportCost.CreateChildPermission(AppPermissions.Pages_TransportCost_Create, L("CreateTransportCost"), multiTenancySides: MultiTenancySides.Tenant);
            //transportCost.CreateChildPermission(AppPermissions.Pages_TransportCost_Edit, L("EditTransportCost"), multiTenancySides: MultiTenancySides.Tenant);
            //transportCost.CreateChildPermission(AppPermissions.Pages_TransportCost_Delete, L("DeleteTransportCost"), multiTenancySides: MultiTenancySides.Tenant);

            var jobCostMonthwise = report.CreateChildPermission(AppPermissions.Pages_Report_JobCostMonthWise, L("JobCostMonthWise"), multiTenancySides: MultiTenancySides.Tenant);
            jobCostMonthwise.CreateChildPermission(AppPermissions.Pages_Report_JobCostMonthWise_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            jobCostMonthwise.CreateChildPermission(AppPermissions.Pages_Report_JobCostMonthWise_QuickView, L("QuickView"), multiTenancySides: MultiTenancySides.Tenant);
            jobCostMonthwise.CreateChildPermission(AppPermissions.Pages_Report_JobCostMonthWise_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant);

            var VariationReport = report.CreateChildPermission(AppPermissions.Pages_Report_JobVariation, L("Variations"), multiTenancySides: MultiTenancySides.Tenant);
            VariationReport.CreateChildPermission(AppPermissions.Pages_Report_JobVariation_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            VariationReport.CreateChildPermission(AppPermissions.Pages_Report_JobVariation_StateWiseVariationAmount, L("StateWiseVariationAmount"), multiTenancySides: MultiTenancySides.Tenant);

            var rescheduleInstallation = report.CreateChildPermission(AppPermissions.Pages_Report_RescheduleInstallation, L("RescheduleInstallation"), multiTenancySides: MultiTenancySides.Tenant);
            rescheduleInstallation.CreateChildPermission(AppPermissions.Pages_Report_RescheduleInstallation_History, L("RescheduleInstallationHistory"), multiTenancySides: MultiTenancySides.Tenant);
           
            var ReviewReport = report.CreateChildPermission(AppPermissions.Pages_Report_ReviewReport, L("ReviewReport"), multiTenancySides: MultiTenancySides.Tenant);
            var ProgressReport = report.CreateChildPermission(AppPermissions.Pages_Report_ProgressReport, L("ProgressReport"), multiTenancySides: MultiTenancySides.Tenant);
            ProgressReport.CreateChildPermission(AppPermissions.Pages_Report_ProgressReport_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            var EmployeeProfit = report.CreateChildPermission(AppPermissions.Pages_Report_EmployeeProfit, L("EmployeeProfit"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #region Call History
            var callHistory = retail.CreateChildPermission(AppPermissions.Pages_CallHistory, L("CallHistory"), multiTenancySides: MultiTenancySides.Tenant);

            // User Call History
            var userCallHistory = callHistory.CreateChildPermission(AppPermissions.Pages_CallHistory_UserCallHistory, L("UserCallHistory"), multiTenancySides: MultiTenancySides.Tenant);
            userCallHistory.CreateChildPermission(AppPermissions.Pages_CallHistory_UserCallHistory_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            userCallHistory.CreateChildPermission(AppPermissions.Pages_CallHistory_UserCallHistory_Detail, L("UserCallHistoryDetail"), multiTenancySides: MultiTenancySides.Tenant);
            userCallHistory.CreateChildPermission(AppPermissions.Pages_CallHistory_UserCallHistory_ShowMobileNo, L("ShowMobileNo"), multiTenancySides: MultiTenancySides.Tenant);

            // State Wise Call History
            var stateCallHistory = callHistory.CreateChildPermission(AppPermissions.Pages_CallHistory_StateWiseCallHistory, L("StateWiseCallHistory"), multiTenancySides: MultiTenancySides.Tenant);
            stateCallHistory.CreateChildPermission(AppPermissions.Pages_CallHistory_StateWiseCallHistory_Import, L("ImportFromExcel"), multiTenancySides: MultiTenancySides.Tenant);
            stateCallHistory.CreateChildPermission(AppPermissions.Pages_CallHistory_StateWiseCallHistory_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            stateCallHistory.CreateChildPermission(AppPermissions.Pages_CallHistory_StateWiseCallHistory_HeaderDetail, L("StateWiseCallHistoryHeaderDeatil"), multiTenancySides: MultiTenancySides.Tenant);
            stateCallHistory.CreateChildPermission(AppPermissions.Pages_CallHistory_StateWiseCallHistory_HeaderDetail_Export, L("HeaderDeatilExport"), multiTenancySides: MultiTenancySides.Tenant);

            // 3CX Call Queue
            var cXCallQueue = callHistory.CreateChildPermission(AppPermissions.Pages_CallHistory_CallFlowQueueCallHistory, L("3CXCallQueue"), multiTenancySides: MultiTenancySides.Tenant);
            cXCallQueue.CreateChildPermission(AppPermissions.Pages_CallHistory_CallFlowQueueCallHistory_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
          
            var cXDetails = cXCallQueue.CreateChildPermission(AppPermissions.Pages_CallHistory_CallFlowQueueCallHistory_Detail, L("3CXCallQueueDetails"), multiTenancySides: MultiTenancySides.Tenant);
            cXDetails.CreateChildPermission(AppPermissions.Pages_CallHistory_CallFlowQueueCallHistory_Detail_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            var cdCallDetails = cXDetails.CreateChildPermission(AppPermissions.Pages_CallHistory_CallFlowQueueCallHistory_Detail_UserCallDetails, L("3CXCallQueueUserDetails"), multiTenancySides: MultiTenancySides.Tenant);
            cdCallDetails.CreateChildPermission(AppPermissions.Pages_CallHistory_CallFlowQueueCallHistory_Detail_UserCallDetails_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            cdCallDetails.CreateChildPermission(AppPermissions.Pages_CallHistory_CallFlowQueueCallHistory_Detail_UserCallDetails_ShowMobileNo, L("ShowMobileNo"), multiTenancySides: MultiTenancySides.Tenant);

            var cXCallQueueWeekly = callHistory.CreateChildPermission(AppPermissions.Pages_CallHistory_CallFlowQueueWeeklyCallHistory, L("3CXCallQueueWeekly"), multiTenancySides: MultiTenancySides.Tenant);

            var cXCallQueueUserWise = cXCallQueue.CreateChildPermission(AppPermissions.Pages_CallHistory_CallFlowQueueUserWiseCallHistory, L("3CXCallQueueUserWise"), multiTenancySides: MultiTenancySides.Tenant);
            cXCallQueueUserWise.CreateChildPermission(AppPermissions.Pages_CallHistory_CallFlowQueueUserWiseCallHistory_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            var cXUserWiseDetails = cXCallQueue.CreateChildPermission(AppPermissions.Pages_CallHistory_CallFlowQueueUserWiseCallHistory_Detail, L("3CXCallQueueUserWiseDetails"), multiTenancySides: MultiTenancySides.Tenant);
            cXUserWiseDetails.CreateChildPermission(AppPermissions.Pages_CallHistory_CallFlowQueueUserWiseCallHistory_Detail_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion

            #endregion

            #region WholeSale

            var wholeSale = pages.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale, L("WholeSale"), multiTenancySides: MultiTenancySides.Tenant);

            var dashboardWholeSale = wholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

            var datavaultsWholeSale = wholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults, L("DataVaults"));

            #region DataVault

            var emailTemplatesWholeSale = datavaultsWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_EmailTemplates, L("EmailTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            emailTemplatesWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_EmailTemplates_Create, L("CreateNewEmailTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            emailTemplatesWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_EmailTemplates_Edit, L("EditEmailTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            emailTemplatesWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_EmailTemplates_Delete, L("DeleteEmailTemplate"), multiTenancySides: MultiTenancySides.Tenant);

            var smsTemplatesWholeSale = datavaultsWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_SmsTemplates, L("SmsTemplates"), multiTenancySides: MultiTenancySides.Tenant);
            smsTemplatesWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_SmsTemplates_Create, L("CreateNewSmsTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            smsTemplatesWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_SmsTemplates_Edit, L("EditSmsTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            smsTemplatesWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_SmsTemplates_Delete, L("DeleteSmsTemplate"), multiTenancySides: MultiTenancySides.Tenant);

            var wholeSaleStatusWholeSale = datavaultsWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholeSaleStatus, L("WholeSaleStatus"), multiTenancySides: MultiTenancySides.Tenant);
            wholeSaleStatusWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholeSaleStatus_Create, L("CreateNewWholeSaleStatus"), multiTenancySides: MultiTenancySides.Tenant);
            wholeSaleStatusWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholeSaleStatus_Edit, L("EditWholeSaleStatus"), multiTenancySides: MultiTenancySides.Tenant);
            wholeSaleStatusWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholeSaleStatus_Delete, L("DeleteWholeSaleStatus"), multiTenancySides: MultiTenancySides.Tenant);

            var documenttypesWholeSale = datavaultsWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_DocumentType, L("DocumentType"), multiTenancySides: MultiTenancySides.Tenant);
            documenttypesWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_DocumentType_Create, L("CreateNewDocumentType"), multiTenancySides: MultiTenancySides.Tenant);
            documenttypesWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_DocumentType_Edit, L("EditDocumentType"), multiTenancySides: MultiTenancySides.Tenant);
            documenttypesWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_DocumentType_Delete, L("DeleteDocumentType"), multiTenancySides: MultiTenancySides.Tenant);
            var seriesWholeSale = datavaultsWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_Serieses, L("Serieses"), multiTenancySides: MultiTenancySides.Tenant);
            seriesWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_Serieses_Create, L("CreateNewSeries"), multiTenancySides: MultiTenancySides.Tenant);
            seriesWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_Serieses_Edit, L("EditSeries"), multiTenancySides: MultiTenancySides.Tenant);
            seriesWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_Serieses_Delete, L("DeleteSeries"), multiTenancySides: MultiTenancySides.Tenant);

            var invoicetype = datavaultsWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleInvoicetypes, L("WholesaleInvoiceTypes"), multiTenancySides: MultiTenancySides.Tenant);
            invoicetype.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleInvoicetypes_Create, L("CreateWholesaleInvoiceTypes"), multiTenancySides: MultiTenancySides.Tenant);
            invoicetype.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleInvoicetypes_Edit, L("EditWholesaleInvoiceTypes"), multiTenancySides: MultiTenancySides.Tenant);
            invoicetype.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleInvoicetypes_Delete, L("DeleteWholesaleInvoiceTypes"), multiTenancySides: MultiTenancySides.Tenant);
            invoicetype.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleInvoicetypes_Export, L("ExportWholesaleInvoiceTypes"), multiTenancySides: MultiTenancySides.Tenant);


            var deliveryoption = datavaultsWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleDeliveryOptions, L("WholesaleDeliveryOptions"), multiTenancySides: MultiTenancySides.Tenant);
            deliveryoption.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleDeliveryOptions_Create, L("CreateWholesaleDeliveryOptions"), multiTenancySides: MultiTenancySides.Tenant);
            deliveryoption.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleDeliveryOptions_Edit, L("EditWholesaleDeliveryOptions"), multiTenancySides: MultiTenancySides.Tenant);
            deliveryoption.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleDeliveryOptions_Delete, L("DeleteWholesaleDeliveryOptions"), multiTenancySides: MultiTenancySides.Tenant);
            deliveryoption.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleDeliveryOptions_Export, L("ExportWholesaleDeliveryOptions"), multiTenancySides: MultiTenancySides.Tenant);


            var TransportType = datavaultsWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleTransportTypes, L("WholesaleTransportTypes"), multiTenancySides: MultiTenancySides.Tenant);
            TransportType.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleTransportTypes_Create, L("CreateWholesaleTransportTypes"), multiTenancySides: MultiTenancySides.Tenant);
            TransportType.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleTransportTypes_Edit, L("EditWholesaleTransportTypes"), multiTenancySides: MultiTenancySides.Tenant);
            TransportType.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleTransportTypes_Delete, L("DeleteWholesaleTransportTypes"), multiTenancySides: MultiTenancySides.Tenant);

            var JobType = datavaultsWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleJobTypes, L("WholesaleJobTypes"), multiTenancySides: MultiTenancySides.Tenant);
            JobType.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleJobTypes_Create, L("CreateWholesaleJobTypes"), multiTenancySides: MultiTenancySides.Tenant);
            JobType.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleJobTypes_Edit, L("EditWholesaleJobTypes"), multiTenancySides: MultiTenancySides.Tenant);
            JobType.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleJobTypes_Delete, L("DeleteWholesaleJobTypes"), multiTenancySides: MultiTenancySides.Tenant);


            var PropertyType = datavaultsWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesalePropertyTypes, L("WholesalePropertyTypes"), multiTenancySides: MultiTenancySides.Tenant);
            PropertyType.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesalePropertyTypes_Create, L("CreateWholesalePropertyTypes"), multiTenancySides: MultiTenancySides.Tenant);
            PropertyType.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesalePropertyTypes_Edit, L("EditWholesalePropertyTypes"), multiTenancySides: MultiTenancySides.Tenant);
            PropertyType.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesalePropertyTypes_Delete, L("DeleteWholesalePropertyTypes"), multiTenancySides: MultiTenancySides.Tenant);

            var Pwdstatus = datavaultsWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesalePVDStatuses, L("WholesalePVDStatuses"), multiTenancySides: MultiTenancySides.Tenant);
            Pwdstatus.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesalePVDStatuses_Create, L("CreateWholesalePVDStatuses"), multiTenancySides: MultiTenancySides.Tenant);
            Pwdstatus.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesalePVDStatuses_Edit, L("EditWholesalePVDStatuses"), multiTenancySides: MultiTenancySides.Tenant);
            Pwdstatus.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesalePVDStatuses_Delete, L("DeleteWholesalePVDStatuses"), multiTenancySides: MultiTenancySides.Tenant);


            var Jobstatus = datavaultsWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleJobStatuses, L("WholesaleJobStatuses"), multiTenancySides: MultiTenancySides.Tenant);
            Jobstatus.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleJobStatuses_Create, L("CreateWholesaleJobStatuses"), multiTenancySides: MultiTenancySides.Tenant);
            Jobstatus.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleJobStatuses_Edit, L("EditWholesaleJobStatuses"), multiTenancySides: MultiTenancySides.Tenant);
            Jobstatus.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholesaleJobStatuses_Delete, L("DeleteWholesaleJobStatuses"), multiTenancySides: MultiTenancySides.Tenant);
            var WholeSaleActivityLog = datavaultsWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_datavault_ActivityLog, L("DataVaultActivityLog"), multiTenancySides: MultiTenancySides.Tenant);
            WholeSaleActivityLog.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_datavault_ActivityLog_Detail, L("DataVaultActivityLogDetail"), multiTenancySides: MultiTenancySides.Tenant);
            WholeSaleActivityLog.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_datavault_ActivityLog_History, L("DataVaultActivityLogHistory"), multiTenancySides: MultiTenancySides.Tenant);

            var ECommerceProductType = datavaultsWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_ProductType, L("ProductTypes"), multiTenancySides: MultiTenancySides.Tenant);
            ECommerceProductType.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_ProductType_Create, L("CreateNewProductType"), multiTenancySides: MultiTenancySides.Tenant);
            ECommerceProductType.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_ProductType_Edit, L("EditProductType"), multiTenancySides: MultiTenancySides.Tenant);
            ECommerceProductType.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_DataVaults_ProductType_Delete, L("DeleteProductType"), multiTenancySides: MultiTenancySides.Tenant);

            var EcommerceActivityLog = datavaultsWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_Ecommerce_datavault_ActivityLog, L("EcommerceActivityLog"), multiTenancySides: MultiTenancySides.Tenant);
            EcommerceActivityLog.CreateChildPermission(AppPermissions.Pages_Tenant_Ecommerce_datavault_ActivityLog_Detail, L("EcommerceActivityLogDetail"), multiTenancySides: MultiTenancySides.Tenant);
            EcommerceActivityLog.CreateChildPermission(AppPermissions.Pages_Tenant_Ecommerce_datavault_ActivityLog_History, L("EcommerceActivityLogHistory"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #region ECommerce
            var ECommerceWholeSale = wholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce, L("ECommerce"), multiTenancySides: MultiTenancySides.Tenant);
            var SliderECommerceWholeSale = ECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Slider, L("Slider"), multiTenancySides: MultiTenancySides.Tenant);
            SliderECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Slider_Create, L("CreateSlider"), multiTenancySides: MultiTenancySides.Tenant);
            SliderECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Slider_Edit, L("EditSlider"), multiTenancySides: MultiTenancySides.Tenant);
            SliderECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Slider_Delete, L("DeleteSlider"), multiTenancySides: MultiTenancySides.Tenant);
            SliderECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Slider_View, L("ViewSlider"), multiTenancySides: MultiTenancySides.Tenant);

            var ProductECommerceWholeSale = ECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Products, L("Product"), multiTenancySides: MultiTenancySides.Tenant);
            ProductECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Products_Create, L("CreateProduct"), multiTenancySides: MultiTenancySides.Tenant);
            ProductECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Products_Edit, L("EditProduct"), multiTenancySides: MultiTenancySides.Tenant);
            ProductECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Products_Delete, L("DeleteProduct"), multiTenancySides: MultiTenancySides.Tenant);
            ProductECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Products_View, L("ViewProduct"), multiTenancySides: MultiTenancySides.Tenant);
            ProductECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Products_Documents, L("ProductDocument"), multiTenancySides: MultiTenancySides.Tenant);

            ProductECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Products_EcommerceProductItemSpecifications, L("EcommerceProductItemSpecifications"), multiTenancySides: MultiTenancySides.Tenant);


            ProductECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Products_Images, L("ProductImage"), multiTenancySides: MultiTenancySides.Tenant);
            ProductECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Products_Imagesdownload, L("ProductImagedownload"), multiTenancySides: MultiTenancySides.Tenant);
            ProductECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Products_Imagesdelete, L("ProductImagedelete"), multiTenancySides: MultiTenancySides.Tenant);
            ProductECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Products_Documents_Delete, L("DeleteProductDocument"), multiTenancySides: MultiTenancySides.Tenant);
            var BrandingPartnerECommerceWholeSale = ECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_BrandingPartner, L("BrandingPartner"), multiTenancySides: MultiTenancySides.Tenant);
            BrandingPartnerECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_BrandingPartner_Create, L("CreateBrandingPartner"), multiTenancySides: MultiTenancySides.Tenant);
            BrandingPartnerECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_BrandingPartner_Edit, L("EditBrandingPartner"), multiTenancySides: MultiTenancySides.Tenant);
            BrandingPartnerECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_BrandingPartner_Delete, L("DeleteBrandingPartner"), multiTenancySides: MultiTenancySides.Tenant);
            BrandingPartnerECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_BrandingPartner_View, L("ViewBrandingPartner"), multiTenancySides: MultiTenancySides.Tenant);

            var SpecialOfferECommerceWholeSale = ECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_SpecialOffer, L("SpecialOffer"), multiTenancySides: MultiTenancySides.Tenant);
            SpecialOfferECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_SpecialOffer_Create, L("CreateSpecialOffer"), multiTenancySides: MultiTenancySides.Tenant);
            SpecialOfferECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_SpecialOffer_Edit, L("EditSpecialOffer"), multiTenancySides: MultiTenancySides.Tenant);
            SpecialOfferECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_SpecialOffer_Delete, L("DeleteSpecialOffer"), multiTenancySides: MultiTenancySides.Tenant);
            SpecialOfferECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_SpecialOffer_View, L("ViewSpecialOffer"), multiTenancySides: MultiTenancySides.Tenant);

            var EcoummerceUserRequest = ECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_UserRequest, L("UserRequest"), multiTenancySides: MultiTenancySides.Tenant);
            EcoummerceUserRequest.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_UserRequest_Create, L("CreateUserRequest"), multiTenancySides: MultiTenancySides.Tenant);
            EcoummerceUserRequest.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_UserRequest_Edit, L("EditUserRequest"), multiTenancySides: MultiTenancySides.Tenant);
            EcoummerceUserRequest.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_UserRequest_Delete, L("DeleteUserRequest"), multiTenancySides: MultiTenancySides.Tenant);
            EcoummerceUserRequest.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_UserRequest_Approve, L("ApproveUserRequest"), multiTenancySides: MultiTenancySides.Tenant);

            var ecommerceSolarPackages = ECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceSolarPackages, L("EcommerceSolarPackages"), multiTenancySides: MultiTenancySides.Tenant);
            ecommerceSolarPackages.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceSolarPackages_Create, L("CreateEcommerceSolarPackages"), multiTenancySides: MultiTenancySides.Tenant);
            ecommerceSolarPackages.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceSolarPackages_Edit, L("EditEcommerceSolarPackages"), multiTenancySides: MultiTenancySides.Tenant);
            ecommerceSolarPackages.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceSolarPackages_Delete, L("DeleteEcommerceSolarPackages"), multiTenancySides: MultiTenancySides.Tenant);
            ecommerceSolarPackages.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceSolarPackages_View, L("ViewEcommerceSolarPackages"), multiTenancySides: MultiTenancySides.Tenant);

            
            var ESpecifications = ECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceSpecifications, L("EcommerceSpecifications"), multiTenancySides: MultiTenancySides.Tenant);
            ESpecifications.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceSpecifications_Create, L("CreateNewEcommerceSpecifications"), multiTenancySides: MultiTenancySides.Tenant);
            ESpecifications.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceSpecifications_Edit, L("EditEcommerceSpecifications"), multiTenancySides: MultiTenancySides.Tenant);
            ESpecifications.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceSpecifications_Delete, L("DeleteEcommerceSpecifications"), multiTenancySides: MultiTenancySides.Tenant);

            var EStcDealPrice = ECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceStcDealPrice, L("EcommerceStcDealPrice"), multiTenancySides: MultiTenancySides.Tenant);
            EStcDealPrice.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceStcDealPrice_Create, L("CreateNewEcommerceStcDealPrice"), multiTenancySides: MultiTenancySides.Tenant);
            EStcDealPrice.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceStcDealPrice_Edit, L("EditEcommerceStcDealPrice"), multiTenancySides: MultiTenancySides.Tenant);
            EStcDealPrice.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceStcDealPrice_Delete, L("DeleteEcommerceStcDealPrice"), multiTenancySides: MultiTenancySides.Tenant);

            var SubscribeECommerceWholeSale = ECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Subscribe, L("Subscribe"), multiTenancySides: MultiTenancySides.Tenant);
            SubscribeECommerceWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_Subscribe_SendEmail, L("SendEmail"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion


            var promotionsGroupWholeSale = wholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_PromotionGroup, L("Promotions"));

            #region Promotions
            var promotionsWholeSale = promotionsGroupWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Promotions, L("Promotions"), multiTenancySides: MultiTenancySides.Tenant);
            promotionsWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Promotions_Create, L("CreateNewPromotion"), multiTenancySides: MultiTenancySides.Tenant);
            promotionsWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Promotions_Delete, L("DeletePromotion"), multiTenancySides: MultiTenancySides.Tenant);
            promotionsWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Promotions_View, L("ViewPromotion"), multiTenancySides: MultiTenancySides.Tenant);
            promotionsWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Promotions_ExportExcel, L("ExportPromotionExcel"), multiTenancySides: MultiTenancySides.Tenant);

            var promotionUsersWholeSale = promotionsGroupWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_PromotionUsers, L("PromotionUsers"), multiTenancySides: MultiTenancySides.Tenant);
            promotionUsersWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_PromotionUsers_Excel, L("PromotionUsersExcel"), multiTenancySides: MultiTenancySides.Tenant);
            promotionUsersWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_PromotionUsers_View, L("ViewPromotionUser"), multiTenancySides: MultiTenancySides.Tenant);
            promotionUsersWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_PromotionUsers_Excel_MobileEmail, L("ExportToExcelMobileEmail"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion

            #region Leads
            var leadWholeSale = wholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Leads, L("Leads"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Leads_Create, L("CreateNewLead"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Leads_View, L("ViewNewLead"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Leads_Edit, L("EditLead"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Leads_Delete, L("DeleteLead"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Leads_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Leads_SMS, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Leads_Email, L("Email"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Leads_Notify, L("Notify"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Leads_Comment, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Leads_ToDo, L("ToDo"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Leads_Reminder, L("ReminderWholeSale"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Leads_UploadDoc, L("UploadDoc"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Leads_AddToUserRequest, L("AddToUserRequest"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Leads_RequestToMove, L("RequestToMove"), multiTenancySides: MultiTenancySides.Tenant);
            var leadWholeSaleaction = leadWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Action, L("LeadAction"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSaleaction.CreateChildPermission(AppPermissions.Pages_WholeSale_Action_AssignToSalesRep, L("AssignToSalesRep"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSaleaction.CreateChildPermission(AppPermissions.Pages_WholeSale_Action_AssignToManager, L("AssignToSalesManager"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSaleaction.CreateChildPermission(AppPermissions.Pages_WholeSale_Action_Delete, L("DeleteWholeSaleLead"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #region RequestToTransfer
            var leadWholeSaleTransferRequest = wholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_RequestToTransferLeads, L("RequestToTransfer"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSaleTransferRequest.CreateChildPermission(AppPermissions.Pages_WholeSale_RequestToTransferLeads_Approve, L("ApproveRequestToTransfer"), multiTenancySides: MultiTenancySides.Tenant);
            leadWholeSaleTransferRequest.CreateChildPermission(AppPermissions.Pages_WholeSale_RequestToMove_Approve, L("ApproveRequestToMove"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #region Invoice
            var invoiceWholeSale = wholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Invoice, L("WholeSaleInvoice"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #region Credit
            var creditWholeSale = wholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_Credit, L("Credit"), multiTenancySides: MultiTenancySides.Tenant);
            var CreditApplicationWholeSale = creditWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_Credit_CreditApplication, L("CreditApplication"), multiTenancySides: MultiTenancySides.Tenant);
            CreditApplicationWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_Credit_CreditApplication_View, L("ViewCreditApplication"), multiTenancySides: MultiTenancySides.Tenant);
            CreditApplicationWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_Credit_CreditApplication_SMS, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
            CreditApplicationWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_Credit_CreditApplication_Email, L("Email"), multiTenancySides: MultiTenancySides.Tenant);
            CreditApplicationWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_Credit_CreditApplication_Comments, L("Comment"), multiTenancySides: MultiTenancySides.Tenant);
            CreditApplicationWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_Credit_CreditApplication_Reminder, L("ActionReminder"), multiTenancySides: MultiTenancySides.Tenant);
            CreditApplicationWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_Credit_CreditApplication_AddNotes, L("AddNotes"), multiTenancySides: MultiTenancySides.Tenant);
            CreditApplicationWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_Credit_CreditApplication_Delete, L("DeleteCreditApplication"), multiTenancySides: MultiTenancySides.Tenant);
            CreditApplicationWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_Credit_CreditApplication_Approve, L("ApproveCreditApplication"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #region ContactUs
            var EcommerceContactUs = wholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceContactUs, L("EcommerceContactUs"), multiTenancySides: MultiTenancySides.Tenant);
            EcommerceContactUs.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceContactUs_Edit, L("EcommerceContactUsEdit"), multiTenancySides: MultiTenancySides.Tenant);
            EcommerceContactUs.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceContactUs_Delete, L("EcommerceContactUsDelete"), multiTenancySides: MultiTenancySides.Tenant);
            EcommerceContactUs.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceContactUs_SMS, L("EcommerceContactUsSMS"), multiTenancySides: MultiTenancySides.Tenant);
            EcommerceContactUs.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceContactUs_Email, L("EcommerceContactUsEmail"), multiTenancySides: MultiTenancySides.Tenant);
            EcommerceContactUs.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_ECommerce_EcommerceContactUs_AddtoLead, L("EcommerceContactUsAddtoLead"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion

            #region Notification
            var WholesaleReply = wholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_Notification, L("Notification"), multiTenancySides: MultiTenancySides.Tenant);
            var Wholesalesms = Reply.CreateChildPermission(AppPermissions.Pages_Tenant_WholeSale_Sms, L("Sms"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #region WholeSaleHeaderSearch
            var wholeSaleHeaderSearch = wholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_HeaderSearch_WholeSaleLead, L("WholeSaleHeaderSearch"), multiTenancySides: MultiTenancySides.Tenant);
            wholeSaleHeaderSearch.CreateChildPermission(AppPermissions.Pages_Tenant_HeaderSearch_WholeSaleLead_ViewDetail, L("QuickView"), multiTenancySides: MultiTenancySides.Tenant);
            wholeSaleHeaderSearch.CreateChildPermission(AppPermissions.Pages_Tenant_HeaderSearch_WholeSaleLead_RequestToTransfer, L("RequestToTransfer"), multiTenancySides: MultiTenancySides.Tenant);
            wholeSaleHeaderSearch.CreateChildPermission(AppPermissions.Pages_Tenant_HeaderSearch_WholeSaleLead_RequestToMove, L("RequestToMove"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #endregion

            #region QuickStock
            var QuickStock = pages.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock, L("QuickStock"), multiTenancySides: MultiTenancySides.Tenant);

            var QuickStockDataVault = QuickStock.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults, L("DataVaults"));
            var QuickStockReports = QuickStock.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_Reports, L("Reports"));
            //QuickStockActivityLog
            var QuickStockActivityLog = datavaultsWholeSale.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_datavault_ActivityLog, L("DataVaultActivityLog"), multiTenancySides: MultiTenancySides.Tenant);
            QuickStockActivityLog.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_datavault_ActivityLog_Detail, L("DataVaultActivityLogDetail"), multiTenancySides: MultiTenancySides.Tenant);
            QuickStockActivityLog.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_datavault_ActivityLog_History, L("DataVaultActivityLogHistory"), multiTenancySides: MultiTenancySides.Tenant);

            //Crrency 
            var currency = QuickStockDataVault.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_Currencies, L("Currencies"), multiTenancySides: MultiTenancySides.Tenant);
            currency.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_Currencies_Create, L("CreateCurrencies"), multiTenancySides: MultiTenancySides.Tenant);
            currency.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_Currencies_Edit, L("EditCurrencies"), multiTenancySides: MultiTenancySides.Tenant);
            currency.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_Currencies_Delete, L("DeleteCurrencies"), multiTenancySides: MultiTenancySides.Tenant);
            currency.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_Currencies_Export, L("ExportCurrencies"), multiTenancySides: MultiTenancySides.Tenant);

            //Delivery Types 
            var deliverytypes = QuickStockDataVault.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_DeliveryTypes, L("DeliveryTypes"), multiTenancySides: MultiTenancySides.Tenant);
            deliverytypes.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_DeliveryTypes_Create, L("CreateDeliveryTypes"), multiTenancySides: MultiTenancySides.Tenant);
            deliverytypes.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_DeliveryTypes_Edit, L("EditDeliveryTypes"), multiTenancySides: MultiTenancySides.Tenant);
            deliverytypes.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_DeliveryTypes_Delete, L("DeleteDeliveryTypes"), multiTenancySides: MultiTenancySides.Tenant);
            deliverytypes.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_DeliveryTypes_Export, L("ExportDeliveryTypes"), multiTenancySides: MultiTenancySides.Tenant);

            //FreightCompany
            var FreightCompany = QuickStockDataVault.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_FreightCompany, L("FreightCompany"), multiTenancySides: MultiTenancySides.Tenant);
            FreightCompany.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_FreightCompany_Create, L("CreateNewFreightCompany"), multiTenancySides: MultiTenancySides.Tenant);
            FreightCompany.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_FreightCompany_Edit, L("EditFreightCompany"), multiTenancySides: MultiTenancySides.Tenant);
            FreightCompany.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_FreightCompany_Delete, L("DeleteFreightCompany"), multiTenancySides: MultiTenancySides.Tenant);
            FreightCompany.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_FreightCompany_Export, L("ExportFreightCompany"), multiTenancySides: MultiTenancySides.Tenant);

            //IncoTerm
            var IncoTerm = QuickStockDataVault.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_IncoTerm, L("IncoTerm"), multiTenancySides: MultiTenancySides.Tenant);
            IncoTerm.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_IncoTerm_Create, L("CreateNewIncoTerm"), multiTenancySides: MultiTenancySides.Tenant);
            IncoTerm.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_IncoTerm_Edit, L("EditIncoTerm"), multiTenancySides: MultiTenancySides.Tenant);
            IncoTerm.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_IncoTerm_Delete, L("DeleteIncoTerm"), multiTenancySides: MultiTenancySides.Tenant);
            IncoTerm.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_IncoTerm_Export, L("ExportIncoTerm"), multiTenancySides: MultiTenancySides.Tenant);

            //PaymentMethod Quickstock
            var PaymentMethod = QuickStockDataVault.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentMethod, L("PaymentMethod"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentMethod.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentMethod_Create, L("CreateNewPaymentMethod"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentMethod.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentMethod_Edit, L("EditPaymentMethod"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentMethod.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentMethod_Delete, L("DeletePaymentMethod"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentMethod.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentMethod_Export, L("ExportPaymentMethod"), multiTenancySides: MultiTenancySides.Tenant);

            //PaymentStatus
            var PaymentStatus = QuickStockDataVault.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentStatus, L("PaymentStatus"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentStatus.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentStatus_Create, L("CreateNewPaymentStatus"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentStatus.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentStatus_Edit, L("EditPaymentStatus"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentStatus.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentStatus_Delete, L("DeletePaymentStatus"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentStatus.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentStatus_Export, L("ExportPaymentStatus"), multiTenancySides: MultiTenancySides.Tenant);

            //payment Type
            var PaymentType = QuickStockDataVault.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentType, L("PaymentTypee"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentType.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentType_Create, L("CreateNewPaymentType"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentType.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentType_Edit, L("EditPaymentType"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentType.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentType_Delete, L("DeletePaymentType"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentType.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentType_Export, L("ExportPaymentType"), multiTenancySides: MultiTenancySides.Tenant);

            //PurchaseCompany
            var PurchaseCompany = QuickStockDataVault.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseCompany, L("PurchaseCompany"), multiTenancySides: MultiTenancySides.Tenant);
            PurchaseCompany.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseCompany_Create, L("CreateNewPurchaseCompany"), multiTenancySides: MultiTenancySides.Tenant);
            PurchaseCompany.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseCompany_Edit, L("EditPurchaseCompany"), multiTenancySides: MultiTenancySides.Tenant);
            PurchaseCompany.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseCompany_Delete, L("DeletePurchaseCompany"), multiTenancySides: MultiTenancySides.Tenant);
            PurchaseCompany.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseCompany_Export, L("ExportPurchaseCompany"), multiTenancySides: MultiTenancySides.Tenant);

            //PurchaseDocumentList
            var PurchaseDocumentList = QuickStockDataVault.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseDocumentList, L("PurchaseDocumentList"), multiTenancySides: MultiTenancySides.Tenant);
            PurchaseDocumentList.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseDocumentList_Create, L("CreateNewPurchaseDocumentList"), multiTenancySides: MultiTenancySides.Tenant);
            PurchaseDocumentList.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseDocumentList_Edit, L("EditPurchaseDocumentList"), multiTenancySides: MultiTenancySides.Tenant);
            PurchaseDocumentList.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseDocumentList_Delete, L("DeletePurchaseDocumentList"), multiTenancySides: MultiTenancySides.Tenant);
            PurchaseDocumentList.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseDocumentList_Export, L("ExportPurchaseDocumentList"), multiTenancySides: MultiTenancySides.Tenant);

            //StockFroms
            var stockfrom = QuickStockDataVault.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockFroms, L("StockFroms"), multiTenancySides: MultiTenancySides.Tenant);
            stockfrom.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockFroms_Create, L("CreateStockFroms"), multiTenancySides: MultiTenancySides.Tenant);
            stockfrom.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockFroms_Edit, L("EditStockFroms"), multiTenancySides: MultiTenancySides.Tenant);
            stockfrom.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockFroms_Delete, L("DeleteStockFroms"), multiTenancySides: MultiTenancySides.Tenant);
            stockfrom.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockFroms_Export, L("ExportStockFroms"), multiTenancySides: MultiTenancySides.Tenant);

            //StockOrderFors
            var stockorderfors = QuickStockDataVault.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockOrderFors, L("StockOrderFors"), multiTenancySides: MultiTenancySides.Tenant);
            stockorderfors.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockOrderFors_Create, L("CreateStockOrderFors"), multiTenancySides: MultiTenancySides.Tenant);
            stockorderfors.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockOrderFors_Edit, L("EditStockOrderFors"), multiTenancySides: MultiTenancySides.Tenant);
            stockorderfors.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockOrderFors_Delete, L("DeleteStockOrderFors"), multiTenancySides: MultiTenancySides.Tenant);
            stockorderfors.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockOrderFors_Export, L("ExportStockOrderFors"), multiTenancySides: MultiTenancySides.Tenant);

            //StockOrderStatus
            var StockOrderStatus = QuickStockDataVault.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockOrderStatus, L("StockOrderStatus"), multiTenancySides: MultiTenancySides.Tenant);
            StockOrderStatus.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockOrderStatus_Create, L("CreateNewStockOrderStatus"), multiTenancySides: MultiTenancySides.Tenant);
            StockOrderStatus.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockOrderStatus_Edit, L("EditStockOrderStatus"), multiTenancySides: MultiTenancySides.Tenant);
            StockOrderStatus.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockOrderStatus_Delete, L("DeleteStockOrderStatus"), multiTenancySides: MultiTenancySides.Tenant);
            StockOrderStatus.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockOrderStatus_Export, L("ExportStockOrderStatus"), multiTenancySides: MultiTenancySides.Tenant);

            //Transport Company
            var transportcompany = QuickStockDataVault.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_TransportCompanies, L("TransportCompanies"), multiTenancySides: MultiTenancySides.Tenant);
            transportcompany.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_TransportCompanies_Create, L("CreateTransportCompanies"), multiTenancySides: MultiTenancySides.Tenant);
            transportcompany.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_TransportCompanies_Edit, L("EditTransportCompanies"), multiTenancySides: MultiTenancySides.Tenant);
            transportcompany.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_TransportCompanies_Delete, L("DeleteTransportCompanies"), multiTenancySides: MultiTenancySides.Tenant);
            transportcompany.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_TransportCompanies_Export, L("ExportTransportCompanies"), multiTenancySides: MultiTenancySides.Tenant);

            //Vendor
            var Vendor = QuickStockDataVault.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_Vendor, L("Vendor"), multiTenancySides: MultiTenancySides.Tenant);
            Vendor.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_Vendor_Create, L("CreateNewVendor"), multiTenancySides: MultiTenancySides.Tenant);
            Vendor.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_Vendor_Edit, L("EditVendor"), multiTenancySides: MultiTenancySides.Tenant);
            Vendor.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_Vendor_Delete, L("DeleteVendor"), multiTenancySides: MultiTenancySides.Tenant);
            Vendor.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_Vendor_Export, L("ExportVendor"), multiTenancySides: MultiTenancySides.Tenant);


            var StockOrder = QuickStock.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_PurchaseOrder, L("PurchaseOrder"), multiTenancySides: MultiTenancySides.Tenant);
            StockOrder.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_PurchaseOrder_Create, L("PurchaseOrderCreate"), multiTenancySides: MultiTenancySides.Tenant);
            StockOrder.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_PurchaseOrder_Edit, L("PurchaseOrderEdit"), multiTenancySides: MultiTenancySides.Tenant);
            StockOrder.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_PurchaseOrder_Delete, L("PurchaseOrderDelete"), multiTenancySides: MultiTenancySides.Tenant);
            StockOrder.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_PurchaseOrder_Import, L("SerialNoImport"), multiTenancySides: MultiTenancySides.Tenant);
            StockOrder.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_PurchaseOrder_Export, L("PurchaseOrderExport"), multiTenancySides: MultiTenancySides.Tenant);

            var StockTransfer = QuickStock.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_StockTransfer, L("StockTransfer"), multiTenancySides: MultiTenancySides.Tenant);
            StockTransfer.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_StockTransfer_Create, L("StockTransferCreate"), multiTenancySides: MultiTenancySides.Tenant);
            StockTransfer.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_StockTransfer_Edit, L("StockTransferEdit"), multiTenancySides: MultiTenancySides.Tenant);
            StockTransfer.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_StockTransfer_Delete, L("StockTransferDelete"), multiTenancySides: MultiTenancySides.Tenant);
            StockTransfer.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_StockTransfer_Export, L("StockTransferExport"), multiTenancySides: MultiTenancySides.Tenant);

            var StockPayment = QuickStock.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_StockPayment, L("StockPayment"), multiTenancySides: MultiTenancySides.Tenant);
            StockPayment.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_StockPayment_Create, L("StockPaymentCreate"), multiTenancySides: MultiTenancySides.Tenant);
            StockPayment.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_StockPayment_Edit, L("StockPaymentEdit"), multiTenancySides: MultiTenancySides.Tenant);
            StockPayment.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_StockPayment_Delete, L("StockPaymentDelete"), multiTenancySides: MultiTenancySides.Tenant);
            StockPayment.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_StockPayment_Export, L("StockPaymentExport"), multiTenancySides: MultiTenancySides.Tenant);

            var stockVariations = QuickStockDataVault.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockVariations, L("Variations"), multiTenancySides: MultiTenancySides.Tenant);
            stockVariations.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockVariations_Create, L("CreateNewVariation"), multiTenancySides: MultiTenancySides.Tenant);
            stockVariations.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockVariations_Edit, L("EditVariation"), multiTenancySides: MultiTenancySides.Tenant);
            stockVariations.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockVariations_Delete, L("DeleteVariation"), multiTenancySides: MultiTenancySides.Tenant);

            var serialNoStatus = QuickStockDataVault.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_SerialNoStatus, L("SerialNoStatus"), multiTenancySides: MultiTenancySides.Tenant);
            serialNoStatus.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_SerialNoStatus_Edit, L("EditSerialNoStatus"), multiTenancySides: MultiTenancySides.Tenant);
            serialNoStatus.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_DataVaults_SerialNoStatus_Delete, L("DeleteSerialNoStatus"), multiTenancySides: MultiTenancySides.Tenant);
           


            var StockPaymentTracker = QuickStock.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_StockPaymentTracker, L("StockPaymentTracker"), multiTenancySides: MultiTenancySides.Tenant);
            StockPaymentTracker.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_StockPaymentTracker_Export, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant);

            var StockReceivedReports = QuickStockReports.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_Reports_StockReceivedReport, L("StockReceivedReports"), multiTenancySides: MultiTenancySides.Tenant);
            StockReceivedReports.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_Reports_StockReceivedReport_Export, L("StockReceivedReportsExport"), multiTenancySides: MultiTenancySides.Tenant);
            var SerialNoHistoryReport = QuickStockReports.CreateChildPermission(AppPermissions.Pages_Tenant_QuickStock_Reports_SerialNoHistoryReport, L("SerialNoHistoryReport"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion


            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

			#region Administrator
			

			var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
			roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
			roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
			roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

			var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
			users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
			users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
			users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
			users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
			users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));
			users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Unlock, L("Unlock"));
			users.CreateChildPermission(AppPermissions.Pages_Administration_Users_PermissionReport, L("PermissionReportExcel"));

			var salesRep = administration.CreateChildPermission(AppPermissions.Pages_Administration_SalesRep, L("SalesRep"), multiTenancySides: MultiTenancySides.Tenant);
			salesRep.CreateChildPermission(AppPermissions.Pages_Administration_SalesRep_Create, L("CreateNewSalesRep"), multiTenancySides: MultiTenancySides.Tenant);
			salesRep.CreateChildPermission(AppPermissions.Pages_Administration_SalesRep_Edit, L("EditSalesRep"), multiTenancySides: MultiTenancySides.Tenant);
			salesRep.CreateChildPermission(AppPermissions.Pages_Administration_SalesRep_Delete, L("DeleteSalesRep"), multiTenancySides: MultiTenancySides.Tenant);
			salesRep.CreateChildPermission(AppPermissions.Pages_Administration_SalesRep_ChangePermissions, L("ChangingPermissions"));
			salesRep.CreateChildPermission(AppPermissions.Pages_Administration_SalesRep_Impersonation, L("LoginForUsers"));
			salesRep.CreateChildPermission(AppPermissions.Pages_Administration_SalesRep_Unlock, L("Unlock"));

			var salesManager = administration.CreateChildPermission(AppPermissions.Pages_Administration_SalesManager, L("SalesManager"), multiTenancySides: MultiTenancySides.Tenant);
			salesManager.CreateChildPermission(AppPermissions.Pages_Administration_SalesManager_Create, L("CreateNewSalesManager"), multiTenancySides: MultiTenancySides.Tenant);
			salesManager.CreateChildPermission(AppPermissions.Pages_Administration_SalesManager_Edit, L("EditSalesManager"), multiTenancySides: MultiTenancySides.Tenant);
			salesManager.CreateChildPermission(AppPermissions.Pages_Administration_SalesManager_Delete, L("DeleteSalesManager"), multiTenancySides: MultiTenancySides.Tenant);
			salesManager.CreateChildPermission(AppPermissions.Pages_Administration_SalesManager_ChangePermissions, L("ChangingPermissions"));
			salesManager.CreateChildPermission(AppPermissions.Pages_Administration_SalesManager_Impersonation, L("LoginForUsers"));
			salesManager.CreateChildPermission(AppPermissions.Pages_Administration_SalesManager_Unlock, L("Unlock"));

			var Installer = administration.CreateChildPermission(AppPermissions.Pages_Administration_Installer, L("Installer"), multiTenancySides: MultiTenancySides.Tenant);
			Installer.CreateChildPermission(AppPermissions.Pages_Administration_Installer_Create, L("CreateNewInstaller"), multiTenancySides: MultiTenancySides.Tenant);
			Installer.CreateChildPermission(AppPermissions.Pages_Administration_Installer_Edit, L("EditInstaller"), multiTenancySides: MultiTenancySides.Tenant);
			Installer.CreateChildPermission(AppPermissions.Pages_Administration_Installer_Delete, L("DeleteInstaller"), multiTenancySides: MultiTenancySides.Tenant);
			Installer.CreateChildPermission(AppPermissions.Pages_Administration_Installer_ChangePermissions, L("ChangingPermissions"));
			Installer.CreateChildPermission(AppPermissions.Pages_Administration_Installer_Impersonation, L("LoginForUsers"));
			Installer.CreateChildPermission(AppPermissions.Pages_Administration_Installer_Unlock, L("Unlock"));



			//var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
			//languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
			//languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
			//languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
			//languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

			administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

			var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"), multiTenancySides: MultiTenancySides.Tenant);
			organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"), multiTenancySides: MultiTenancySides.Tenant);
			organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"), multiTenancySides: MultiTenancySides.Tenant);
			organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageRoles, L("ManagingRoles"), multiTenancySides: MultiTenancySides.Tenant);

			//administration.CreateChildPermission(AppPermissions.Pages_Administration_UiCustomization, L("VisualSettings"));

			var webhooks = administration.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription, L("Webhooks"));
			webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Create, L("CreatingWebhooks"));
			webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Edit, L("EditingWebhooks"));
			webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_ChangeActivity, L("ChangingWebhookActivity"));
			webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Detail, L("DetailingSubscription"));
			webhooks.CreateChildPermission(AppPermissions.Pages_Administration_Webhook_ListSendAttempts, L("ListingSendAttempts"));
			webhooks.CreateChildPermission(AppPermissions.Pages_Administration_Webhook_ResendWebhook, L("ResendingWebhook"));

			//var dynamicParameters = administration.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters, L("DynamicParameters"));
			//dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters_Create, L("CreatingDynamicParameters"));
			//dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters_Edit, L("EditingDynamicParameters"));
			//dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters_Delete, L("DeletingDynamicParameters"));

			//var dynamicParameterValues = dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue, L("DynamicParameterValue"));
			//dynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue_Create, L("CreatingDynamicParameterValue"));
			//dynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue_Edit, L("EditingDynamicParameterValue"));
			//dynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue_Delete, L("DeletingDynamicParameterValue"));

			//var entityDynamicParameters = dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameters, L("EntityDynamicParameters"));
			//entityDynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameters_Create, L("CreatingEntityDynamicParameters"));
			//entityDynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameters_Edit, L("EditingEntityDynamicParameters"));
			//entityDynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameters_Delete, L("DeletingEntityDynamicParameters"));

			//var entityDynamicParameterValues = dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameterValue, L("EntityDynamicParameterValue"));
			//entityDynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameterValue_Create, L("CreatingEntityDynamicParameterValue"));
			//entityDynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameterValue_Edit, L("EditingEntityDynamicParameterValue"));
			//entityDynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameterValue_Delete, L("DeletingEntityDynamicParameterValue"));

			administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);
			administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement, L("Subscription"), multiTenancySides: MultiTenancySides.Tenant);

			var departments = administration.CreateChildPermission(AppPermissions.Pages_Departments, L("Departments"));
			departments.CreateChildPermission(AppPermissions.Pages_Departments_Create, L("CreateNewDepartment"));
			departments.CreateChildPermission(AppPermissions.Pages_Departments_Edit, L("EditDepartment"));
			departments.CreateChildPermission(AppPermissions.Pages_Departments_Delete, L("DeleteDepartment"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Host);

            #endregion

            #region Quick Stock App Permission
            var multTenant = MultiTenancySides.Tenant;
            var qsApp = context.GetPermissionOrNull(AppPermissions.QuickStockApp) ?? context.CreatePermission(AppPermissions.QuickStockApp, L("QuickStockApp"), multiTenancySides: multTenant);

            // Stock Order
            var stockOrder = qsApp.CreateChildPermission(AppPermissions.QuickStockApp_StockOrder, L("StockOrder"), multiTenancySides: multTenant);

            // Stock Transfer
            var stockTransfer = qsApp.CreateChildPermission(AppPermissions.QuickStockApp_StockTransfer, L("StockTransfer"), multiTenancySides: multTenant);

            // Retail Job
            var retailJob = qsApp.CreateChildPermission(AppPermissions.QuickStockApp_RetailJob, L("RetailJob"), multiTenancySides: multTenant);

            // Wholesale Invoice
            var wholesaleInvoice = qsApp.CreateChildPermission(AppPermissions.QuickStockApp_WholesaleInvoice, L("WholesaleInvoice"), multiTenancySides: multTenant);

            // Revert Items
            var revertItems = qsApp.CreateChildPermission(AppPermissions.QuickStockApp_RevertItems, L("RevertItems"), multiTenancySides: multTenant);

            // Faulty Items
            var faultyItems = qsApp.CreateChildPermission(AppPermissions.QuickStockApp_FaultyItems, L("FaultyItems"), multiTenancySides: multTenant);

            // Live Stock Qty
            var liveStockQty = qsApp.CreateChildPermission(AppPermissions.QuickStockApp_LiveStockQty, L("LiveStockQty"), multiTenancySides: multTenant);

            // Stock Audit
            var stockAudit = qsApp.CreateChildPermission(AppPermissions.QuickStockApp_StockAudit, L("StockAudit"), multiTenancySides: multTenant);

            // Reports
            var reports = qsApp.CreateChildPermission(AppPermissions.QuickStockApp_Reports, L("Reports"), multiTenancySides: multTenant);

            // Jobs
            var jobs_qs = qsApp.CreateChildPermission(AppPermissions.QuickStockApp_Jobs, L("Jobs"), multiTenancySides: multTenant);

            // Availability
            var availability = qsApp.CreateChildPermission(AppPermissions.QuickStockApp_Availability, L("Availability"), multiTenancySides: multTenant);




            #endregion

            //HOST-SPECIFIC PERMISSIONS
            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
			editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
			editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
			editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);
			editions.CreateChildPermission(AppPermissions.Pages_Editions_MoveTenantsToAnotherEdition, L("MoveTenantsToAnotherEdition"), multiTenancySides: MultiTenancySides.Host);

			var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
			tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
			tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
			tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
			tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
			tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);

			
			var headerSearch =pages.CreateChildPermission(AppPermissions.Pages_Tenant_HeaderSearch, L("HeaderSearch"), multiTenancySides: MultiTenancySides.Tenant);
            headerSearch.CreateChildPermission(AppPermissions.Pages_Tenant_HeaderSearch_ViewDetail, L("QuickView"), multiTenancySides: MultiTenancySides.Tenant);
           

            #region
            mylead.CreateChildPermission(AppPermissions.Pages_MyLeads_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            managelead.CreateChildPermission(AppPermissions.Pages_ManageLeads_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            leadtracker.CreateChildPermission(AppPermissions.Pages_LeadTracker_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            leadduplicate.CreateChildPermission(AppPermissions.Pages_Lead_Duplicate_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            leadCalcel.CreateChildPermission(AppPermissions.Pages_Leads_Calcel_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            leadRejects.CreateChildPermission(AppPermissions.Pages_Leads_Rejects_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            leadClosed.CreateChildPermission(AppPermissions.Pages_Lead_Closed_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            
			promotions.CreateChildPermission(AppPermissions.Pages_Promotions_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            promotionUsers.CreateChildPermission(AppPermissions.Pages_PromotionUsers_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            
			applicationTracker.CreateChildPermission(AppPermissions.Pages_ApplicationTracker_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            freebiesTracker.CreateChildPermission(AppPermissions.Pages_FreebiesTracker_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            financeTracker.CreateChildPermission(AppPermissions.Pages_FinanceTracker_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            refundTracker.CreateChildPermission(AppPermissions.Pages_RefundTracker_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            Referral.CreateChildPermission(AppPermissions.Pages_Tracker_Referral_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            JobActive.CreateChildPermission(AppPermissions.Pages_JobActiveTracker_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            Gridconnection.CreateChildPermission(AppPermissions.Pages_Gridconnectiontracker_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            stcTracker.CreateChildPermission(AppPermissions.Pages_STCTracker_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            Warranty.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            holdjobtracker.CreateChildPermission(AppPermissions.Pages_HoldJobTracker_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            
			jobGrid.CreateChildPermission(AppPermissions.Pages_JobGrid_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
           
			invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            invoicepaidTracker.CreateChildPermission(AppPermissions.Pages_InvoiceTracker_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            invoicePayWay.CreateChildPermission(AppPermissions.Pages_InvoicePayWay_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));

            myinstaller.CreateChildPermission(AppPermissions.Pages_MyInstaller_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            JobBooking.CreateChildPermission(AppPermissions.Pages_JobBooking_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));

            newinstallerinvoice.CreateChildPermission(AppPermissions.Pages_Installer_New_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            installerinvoices.CreateChildPermission(AppPermissions.Pages_Installer_Invoice_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            readytopay.CreateChildPermission(AppPermissions.Pages_ReadyToPay_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            
			leadGenActionFetures.CreateChildPermission(AppPermissions.Pages_LeadGeneration_MyLeadsGeneration_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            leadgeninstalltion.CreateChildPermission(AppPermissions.Pages_LeadGeneration_Installation_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            Commission.CreateChildPermission(AppPermissions.Pages_LeadGeneration_Commission_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));

            ManageService.CreateChildPermission(AppPermissions.Pages_Lead_ManageService_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            myservice.CreateChildPermission(AppPermissions.Pages_MyService_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            warrantyClaim.CreateChildPermission(AppPermissions.Pages_WarrantyClaim_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            serviceInvoice.CreateChildPermission(AppPermissions.Pages_ServiceInvoice_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));

            reviewType.CreateChildPermission(AppPermissions.Pages_Review_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            
            sms.CreateChildPermission(AppPermissions.Pages_Sms_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));

            activityReport.CreateChildPermission(AppPermissions.Pages_ActivityReport_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            ToDoActivityReport.CreateChildPermission(AppPermissions.Pages_ToDoActivityReport_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            LeadexpenseReport.CreateChildPermission(AppPermissions.Pages_LeadexpenseReport_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            OutStandingReport.CreateChildPermission(AppPermissions.Pages_OutStandingReport_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            jobCost.CreateChildPermission(AppPermissions.Pages_Report_JobCost_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            leadAssign.CreateChildPermission(AppPermissions.Pages_Report_LeadAssign_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            leadSold.CreateChildPermission(AppPermissions.Pages_Report_LeadSold_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            empJobCost.CreateChildPermission(AppPermissions.Pages_Report_EmpJobCost_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));

            userCallHistory.CreateChildPermission(AppPermissions.Pages_CallHistory_UserCallHistory_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            stateCallHistory.CreateChildPermission(AppPermissions.Pages_CallHistory_StateWiseCallHistory_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            cXCallQueue.CreateChildPermission(AppPermissions.Pages_CallHistory_CallFlowQueueCallHistory_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));


            leadWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Leads_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            
			promotionsWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Promotions_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            promotionUsersWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_PromotionUsers_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));

            invoiceWholeSale.CreateChildPermission(AppPermissions.Pages_WholeSale_Invoice_EnableCopyPaste, L("EnableCopyPaste"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));


            
			leadCalcel.CreateChildPermission(AppPermissions.Pages_Lead_Calcel_ExcelExport, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            leadRejects.CreateChildPermission(AppPermissions.Pages_Lead_Rejects_ExcelExport, L("ExportToExcel"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: new SimpleFeatureDependency(AppFeatures.TenantEnableCopyPaste));
            #endregion

          

            managelead.CreateChildPermission(AppPermissions.Pages_ManageLeads_ShowEmailPhone, L("ShowEmailPhone"), multiTenancySides: MultiTenancySides.Tenant);
            leadduplicate.CreateChildPermission(AppPermissions.Pages_Lead_Duplicate_ShowEmailPhone, L("ShowEmailPhone"), multiTenancySides: MultiTenancySides.Tenant);
            leadtracker.CreateChildPermission(AppPermissions.Pages_LeadTracker_ShowEmailPhone, L("ShowEmailPhone"), multiTenancySides: MultiTenancySides.Tenant);
            leadClosed.CreateChildPermission(AppPermissions.Pages_Lead_Closed_ShowEmailPhone, L("ShowEmailPhone"), multiTenancySides: MultiTenancySides.Tenant);
            _3rdPartyLeadTracker.CreateChildPermission(AppPermissions.Pages_3rdPartyLeadTracker_ShowEmailPhone, L("ShowEmailPhone"), multiTenancySides: MultiTenancySides.Tenant);
            Warranty.CreateChildPermission(AppPermissions.Pages_Tracker_Warranty_ShowEmailPhone, L("ShowEmailPhone"), multiTenancySides: MultiTenancySides.Tenant);
            transportCost.CreateChildPermission(AppPermissions.Pages_TransportCost_ShowEmailPhone, L("ShowEmailPhone"), multiTenancySides: MultiTenancySides.Tenant);
            invoiceIssuedTracker.CreateChildPermission(AppPermissions.Pages_InvoiceIssuedTracker_ShowEmailPhone, L("ShowEmailPhone"), multiTenancySides: MultiTenancySides.Tenant);
            myinstaller.CreateChildPermission(AppPermissions.Pages_MyInstaller_ShowEmailPhone, L("ShowEmailPhone"), multiTenancySides: MultiTenancySides.Tenant);
            PenddingInstallation.CreateChildPermission(AppPermissions.Pages_PendingInstallation_ShowEmailPhone, L("ShowEmailPhone"), multiTenancySides: MultiTenancySides.Tenant);
            leadGenActionFetures.CreateChildPermission(AppPermissions.Pages_LeadGeneration_MyLeadsGeneration_ShowEmailPhone, L("ShowEmailPhone"), multiTenancySides: MultiTenancySides.Tenant);
            Commission.CreateChildPermission(AppPermissions.Pages_LeadGeneration_Commission_ShowEmailPhone, L("ShowEmailPhone"), multiTenancySides: MultiTenancySides.Tenant);
            ManageService.CreateChildPermission(AppPermissions.Pages_Lead_ManageService_ShowEmailPhone, L("ShowEmailPhone"), multiTenancySides: MultiTenancySides.Tenant);
            myservice.CreateChildPermission(AppPermissions.Pages_MyService_ShowEmailPhone, L("ShowEmailPhone"), multiTenancySides: MultiTenancySides.Tenant);
            warrantyClaim.CreateChildPermission(AppPermissions.Pages_WarrantyClaim_ShowEmailPhone, L("ShowEmailPhone"), multiTenancySides: MultiTenancySides.Tenant);
            

            
            myinstaller.CreateChildPermission(AppPermissions.Pages_MyInstaller_Export_MobileEmail, L("ExportToExcelMobileEmail"), multiTenancySides: MultiTenancySides.Tenant);
            
            
            Commission.CreateChildPermission(AppPermissions.Pages_LeadGeneration_Commission_Export_MobileEmail, L("ExportToExcelMobileEmail"), multiTenancySides: MultiTenancySides.Tenant);

                      
        }

        private static ILocalizableString L(string name)
		{
			return new LocalizableString(name, TheSolarProductConsts.LocalizationSourceName);
		}
	}
}