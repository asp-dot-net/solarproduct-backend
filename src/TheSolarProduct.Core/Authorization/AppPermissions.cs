﻿namespace TheSolarProduct.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission nam
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        public const string LoginWithIPAddress = "LoginWithIPAddress";
        public const string UserInOutTime = "UserInOutTime";
        public const string Pages_SmsTemplates = "Pages.SmsTemplates";
        public const string Pages_SmsTemplates_Create = "Pages.SmsTemplates.Create";
        public const string Pages_SmsTemplates_Edit = "Pages.SmsTemplates.Edit";
        public const string Pages_SmsTemplates_Delete = "Pages.SmsTemplates.Delete";
       

        public const string Pages_FreebieTransports = "Pages.FreebieTransports";
        public const string Pages_FreebieTransports_Create = "Pages.FreebieTransports.Create";
        public const string Pages_FreebieTransports_Edit = "Pages.FreebieTransports.Edit";
        public const string Pages_FreebieTransports_Delete = "Pages.FreebieTransports.Delete";

        public const string Pages_InvoiceStatuses = "Pages.InvoiceStatuses";
        public const string Pages_InvoiceStatuses_Create = "Pages.InvoiceStatuses.Create";
        public const string Pages_InvoiceStatuses_Edit = "Pages.InvoiceStatuses.Edit";
        public const string Pages_InvoiceStatuses_Delete = "Pages.InvoiceStatuses.Delete";

        public const string Pages_Tracker = "Pages.Tracker";

        public const string Pages_ApplicationTracker = "Pages.Tracker.ApplicationTracker";
        public const string Pages_ApplicationTracker_AddNotes = "Pages.Tracker.ApplicationTracker.AddNotes";
        public const string Pages_ApplicationTracker_JobDetail = "Pages.Tracker.ApplicationTracker.JobDetail";
        public const string Pages_ApplicationTracker_Notify = "Pages.Tracker.ApplicationTracker.Notify";
        public const string Pages_ApplicationTracker_SMS = "Pages.Tracker.ApplicationTracker.SMS";
        public const string Pages_ApplicationTracker_Email = "Pages.Tracker.ApplicationTracker.Email";
        public const string Pages_ApplicationTracker_ToDo = "Pages.Tracker.ApplicationTracker.ToDo";
        public const string Pages_ApplicationTracker_Reminder = "Pages.Tracker.ApplicationTracker.Reminder";
        public const string Pages_ApplicationTracker_Comment = "Pages.Tracker.ApplicationTracker.Comment";
        public const string Pages_ApplicationTracker_QuickView = "Pages.Tracker.ApplicationTracker.QuickView";
        public const string Pages_ApplicationTracker_DocumentRequest = "Pages.Tracker.ApplicationTracker.DocumentRequest";
        public const string Pages_ApplicationTracker_Job_Edit = "Pages.Tracker.ApplicationTracker.JobEdit";
        public const string Pages_ApplicationTracker_Export = "Pages.Tracker.ApplicationTracker.Export";
        //public const string Pages_ApplicationTracker_Import = "Pages.Tracker.ApplicationTracker.Import";

        public const string Pages_ApplicationFeeTracker = "Pages.Tracker.ApplicationFeeTracker";

        public const string Pages_EssentialTracker = "Pages.Tracker.EssentialTracker";
        public const string Pages_EssentialTracker_AddNotes = "Pages.Tracker.EssentialTracker.AddNotes";
        public const string Pages_EssentialTracker_JobDetail = "Pages.Tracker.EssentialTracker.JobDetail";
        public const string Pages_EssentialTracker_Notify = "Pages.Tracker.EssentialTracker.Notify";
        public const string Pages_EssentialTracker_SMS = "Pages.Tracker.EssentialTracker.SMS";
        public const string Pages_EssentialTracker_Email = "Pages.Tracker.EssentialTracker.Email";
        public const string Pages_EssentialTracker_ToDo = "Pages.Tracker.EssentialTracker.ToDo";
        public const string Pages_EssentialTracker_Reminder = "Pages.Tracker.EssentialTracker.Reminder";
        public const string Pages_EssentialTracker_Comment = "Pages.Tracker.EssentialTracker.Comment";
        public const string Pages_EssentialTracker_QuickView = "Pages.Tracker.EssentialTracker.QuickView";
        public const string Pages_EssentialTracker_DocumentRequest = "Pages.Tracker.EssentialTracker.DocumentRequest";
        public const string Pages_EssentialTracker_Job_Edit = "Pages.Tracker.EssentialTracker.JobEdit";
        public const string Pages_EssentialTracker_Export = "Pages.Tracker.EssentialTracker.Export";

        public const string Pages_FreebiesTracker = "Pages.Tracker.FreebiesTracker";
        public const string Pages_FreebiesTracker_AddTrackingNumber = "Pages.Tracker.FreebiesTracker.AddTrackingNumber";
        public const string Pages_FreebiesTracker_Notify = "Pages.Tracker.FreebiesTracker.Notify";
        public const string Pages_FreebiesTracker_SMS = "Pages.Tracker.FreebiesTracker.SMS";
        public const string Pages_FreebiesTracker_Email = "Pages.Tracker.FreebiesTracker.Email";
        public const string Pages_FreebiesTracker_QuickView = "Pages.Tracker.FreebiesTracker.QuickView";
        public const string Pages_FreebiesTracker_Job_Edit = "Pages.Tracker.FreebiesTracker.JobEdit";
        public const string Pages_FreebiesTracker_Export = "Pages.Tracker.FreebiesTracker.Export";
        //public const string Pages_FreebiesTracker_Import = "Pages.Tracker.FreebiesTracker.Import";
        public const string Pages_FreebiesTracker_ToDo = "Pages.Tracker.FreebiesTracker.ToDo";
        public const string Pages_FreebiesTracker_Reminder = "Pages.Tracker.FreebiesTracker.Reminder";
        public const string Pages_FreebiesTracker_Comment = "Pages.Tracker.FreebiesTracker.Comment";

        public const string Pages_FinanceTracker = "Pages.Tracker.FinanceTracker";
        public const string Pages_FinanceTracker_AddDetails = "Pages.Tracker.FinanceTracker.AddDetails";
        public const string Pages_FinanceTracker_Notify = "Pages.Tracker.FinanceTracker.Notify";
        public const string Pages_FinanceTracker_SMS = "Pages.Tracker.FinanceTracker.SMS";
        public const string Pages_FinanceTracker_Email = "Pages.Tracker.FinanceTracker.Email";
        public const string Pages_FinanceTracker_QuickView = "Pages.Tracker.FinanceTracker.QuickView";
        public const string Pages_FinanceTracker_Job_Edit = "Pages.Tracker.FinanceTracker.JobEdit";
        public const string Pages_FinanceTracker_Export = "Pages.Tracker.FinanceTracker.Export";
        //public const string Pages_FinanceTracker_Import = "Pages.Tracker.FinanceTracker.Import";
        public const string Pages_FinanceTracker_ToDo = "Pages.Tracker.FinanceTracker.ToDo";
        public const string Pages_FinanceTracker_Reminder = "Pages.Tracker.FinanceTracker.Reminder";
        public const string Pages_FinanceTracker_Comment = "Pages.Tracker.FinanceTracker.Comment";

        public const string Pages_RefundTracker = "Pages.Tracker.RefundTracker";
        public const string Pages_RefundTracker_Refund = "Pages.Tracker.RefundTracker.Refund";
        public const string Pages_RefundTracker_Notify = "Pages.Tracker.RefundTracker.Notify";
        public const string Pages_RefundTracker_SMS = "Pages.Tracker.RefundTracker.SMS";
        public const string Pages_RefundTracker_Email = "Pages.Tracker.RefundTracker.Email";
        public const string Pages_RefundTracker_QuickView = "Pages.Tracker.RefundTracker.QuickView";
        public const string Pages_RefundTracker_Delete = "Pages.Tracker.RefundTracker.Delete";
        public const string Pages_RefundTracker_Job_Edit = "Pages.Tracker.RefundTracker.JobEdit";
        public const string Pages_RefundTracker_Export = "Pages.Tracker.RefundTracker.Export";
        //public const string Pages_RefundTracker_Import = "Pages.Tracker.RefundTracker.Import";
        public const string Pages_RefundTracker_ToDo = "Pages.Tracker.RefundTracker.ToDo";
        public const string Pages_RefundTracker_Reminder = "Pages.Tracker.RefundTracker.Reminder";
        public const string Pages_RefundTracker_Comment = "Pages.Tracker.RefundTracker.Comment";
        public const string Pages_RefundTracker_Verify = "Pages.Tracker.RefundTracker.Verify";
        public const string Pages_RefundTracker_NotVerify = "Pages.Tracker.RefundTracker.NotVerify";
        public const string Pages_RefundTracker_Download = "Pages.Tracker.RefundTracker.Download";

        public const string Pages_JobActiveTracker = "Pages.Tracker.JobActiveTracker";
        public const string Pages_JobActiveTracker_AddDetails = "Pages.Tracker.JobActiveTracker.AddDetails";
        public const string Pages_JobActiveTracker_Notify = "Pages.Tracker.JobActiveTracker.Notify";
        public const string Pages_JobActiveTracker_SMS = "Pages.Tracker.JobActiveTracker.SMS";
        public const string Pages_JobActiveTracker_Email = "Pages.Tracker.JobActiveTracker.Email";
        public const string Pages_JobActiveTracker_QuickView = "Pages.Tracker.JobActiveTracker.QuickView";
        public const string Pages_JobActiveTracker_Job_Edit = "Pages.Tracker.JobActiveTracker.JobEdit";
        public const string Pages_JobActiveTracker_Export = "Pages.Tracker.JobActiveTracker.Export";
        //public const string Pages_JobActiveTracker_Import = "Pages.Tracker.JobActiveTracker.Import";
        public const string Pages_JobActiveTracker_ToDo = "Pages.Tracker.JobActiveTracker.ToDo";
        public const string Pages_JobActiveTracker_Reminder = "Pages.Tracker.JobActiveTracker.Reminder";
        public const string Pages_JobActiveTracker_Comment = "Pages.Tracker.JobActiveTracker.Comment";
        public const string Pages_JobActiveTracker_Export_CheckActive = "Pages.Tracker.JobActiveTracker.Export.CheckActive";


        public const string Pages_Invoice = "Pages.Invoice";
        public const string Pages_InvoiceTracker = "Pages.Tracker.InvoiceTracker";
        public const string Pages_InvoiceTracker_Verify = "Pages.Tracker.InvoiceTracker.Verify";
        public const string Pages_InvoiceTracker_Notify = "Pages.Tracker.InvoiceTracker.Notify";
        public const string Pages_InvoiceTracker_SMS = "Pages.Tracker.InvoiceTracker.SMS";
        public const string Pages_InvoiceTracker_Email = "Pages.Tracker.InvoiceTracker.Email";
        public const string Pages_InvoiceTracker_QuickView = "Pages.Tracker.InvoiceTracker.QuickView";
        public const string Pages_InvoiceTracker_Job_Edit = "Pages.Tracker.InvoiceTracker.JobEdit";
        public const string Pages_InvoiceTracker_Export = "Pages.Tracker.InvoiceTracker.Export";
        public const string Pages_InvoiceTracker_Import = "Pages.Tracker.InvoiceTracker.Import";
        public const string Pages_InvoiceTracker_ToDo = "Pages.Tracker.InvoiceTracker.ToDo";
        public const string Pages_InvoiceTracker_Reminder = "Pages.Tracker.InvoiceTracker.Reminder";
        public const string Pages_InvoiceTracker_Comment = "Pages.Tracker.InvoiceTracker.Comment";
        public const string Pages_InvoiceTracker_Edit = "Pages.Tracker.InvoiceTracker.Edit";
        public const string Pages_InvoiceTracker_Delete = "Pages.Tracker.InvoiceTracker.Delete";
        public const string Pages_InvoiceTracker_CreateManually = "Pages.Tracker.InvoiceTracker.CreateManually";


        public const string Pages_InvoiceIssuedTracker = "Pages.Tracker.InvoiceIssuedTracker";
        public const string Pages_InvoiceIssuedTracker_Notify = "Pages.Tracker.InvoiceIssuedTracker.Notify";
        public const string Pages_InvoiceIssuedTracker_SMS = "Pages.Tracker.InvoiceIssuedTracker.SMS";
        public const string Pages_InvoiceIssuedTracker_Email = "Pages.Tracker.InvoiceIssuedTracker.Email";
        public const string Pages_InvoiceIssuedTracker_QuickView = "Pages.Tracker.InvoiceIssuedTracker.QuickView";
        public const string Pages_InvoiceIssuedTracker_Job_Edit = "Pages.Tracker.InvoiceIssuedTracker.JobEdit";
        public const string Pages_InvoiceIssuedTracker_Export = "Pages.Tracker.InvoiceIssuedTracker.Export";
        public const string Pages_InvoiceIssuedTracker_Export_MobileEmail = "Pages.Tracker.InvoiceIssuedTracker.Export.MobileEmail";
        public const string Pages_InvoiceIssuedTracker_Import = "Pages.Tracker.InvoiceIssuedTracker.Import";
        public const string Pages_InvoiceIssuedTracker_ToDo = "Pages.Tracker.InvoiceIssuedTracker.ToDo";
        public const string Pages_InvoiceIssuedTracker_Reminder = "Pages.Tracker.InvoiceIssuedTracker.Reminder";
        public const string Pages_InvoiceIssuedTracker_Comment = "Pages.Tracker.InvoiceIssuedTracker.Comment";

        public const string Pages_InvoicePayWay = "Pages.InvoicePayWay";
        public const string Pages_InvoicePayWay_Email = "Pages.InvoicePayWay.Email";
        public const string Pages_InvoicePayWay_Notify = "Pages.InvoicePayWay.Notify";
        public const string Pages_InvoicePayWay_QuickView = "Pages.InvoicePayWay.QuickView";
        public const string Pages_InvoicePayWay_SMS = "Pages.InvoicePayWay.SMS";
        public const string Pages_InvoicePayWay_ToDo = "Pages.InvoicePayWay.ToDo";
        public const string Pages_InvoicePayWay_Reminder = "Pages.InvoicePayWay.Reminder";
        public const string Pages_InvoicePayWay_Comment = "Pages.InvoicePayWay.Comment";
        public const string Pages_InvoiceFileList = "Pages.InvoiceFileList";

        public const string Pages_Gridconnectiontracker = "Pages.Tracker.Gridconnectiontracker";
        public const string Pages_Gridconnectiontracker_Detail = "Pages.Tracker.Gridconnectiontracker.Detail";
        public const string Pages_Gridconnectiontracker_AddMeterDetail = "Pages.Tracker.Gridconnectiontracker.AddMeterDetail";
        public const string Pages_Gridconnectiontracker_Job_Edit = "Pages.Tracker.Gridconnectiontracker.JobEdit";
        public const string Pages_Gridconnectiontracker_Delete = "Pages.Gridconnectiontracker.Delete";
        public const string Pages_Gridconnectiontracker_Export = "Pages.Gridconnectiontracker.Export";
        //public const string Pages_Gridconnectiontracker_Import = "Pages.Gridconnectiontracker.Import";
        public const string Pages_Gridconnectiontracker_ToDo = "Pages.Gridconnectiontracker.ToDo";
        public const string Pages_Gridconnectiontracker_Reminder = "Pages.Gridconnectiontracker.Reminder";
        public const string Pages_Gridconnectiontracker_Comment = "Pages.Gridconnectiontracker.Comment";
        public const string Pages_Gridconnectiontracker_JobDetail = "Pages.Gridconnectiontracker.JobDetail";
        public const string Pages_Gridconnectiontracker_Notify = "Pages.Gridconnectiontracker.Notify";
        public const string Pages_Gridconnectiontracker_QuickView = "Pages.Gridconnectiontracker.QuickView";
        public const string Pages_Gridconnectiontracker_SMS = "Pages.Gridconnectiontracker.SMS";
        public const string Pages_Gridconnectiontracker_Email = "Pages.Gridconnectiontracker.Email";
        public const string Pages_Gridconnectiontracker_AddNote = "Pages.Gridconnectiontracker.AddNote";


        public const string Pages_STCTracker = "Pages.Tracker.STCTracker";
        public const string Pages_STCTracker_Edit = "Pages.Tracker.STCTracker.Edit";
        public const string Pages_STCTracker_Job_Edit = "Pages.Tracker.STCTracker.JobEdit";
        public const string Pages_STCTracker_Delete = "Pages.STCTracker.Delete";
        public const string Pages_STCTracker_Export = "Pages.STCTracker.Export";
        //public const string Pages_STCTracker_Import = "Pages.STCTracker.Import";
        public const string Pages_STCTracker_JobDetail = "Pages.STCTracker.JobDetail";
        public const string Pages_STCTracker_Notify = "Pages.STCTracker.Notify";
        public const string Pages_STCTracker_QuickView = "Pages.STCTracker.QuickView";
        public const string Pages_STCTracker_SMS = "Pages.STCTracker.SMS";
        public const string Pages_STCTracker_Email = "Pages.STCTracker.Email";
        public const string Pages_STCTracker_AddSTC = "Pages.STCTracker.AddSTC";
        public const string Pages_STCTracker_ToDo = "Pages.STCTracker.ToDo";
        public const string Pages_STCTracker_Reminder = "Pages.STCTracker.Reminder";
        public const string Pages_STCTracker_Comment = "Pages.STCTracker.Comment";

        public const string Pages_Tracker_Referral = "Pages.Tracker.Referral";
        public const string Pages_Tracker_Referral_Payment = "Pages.Tracker.Referral.Payment";
        public const string Pages_Tracker_Referral_QuickView = "Pages.Tracker.Referral.Payment.QuickView";
        public const string Pages_Tracker_Referral_AddNotes = "Pages.Tracker.Referral.Payment.AddNotes";
        public const string Pages_Tracker_Referral_Notify = "Pages.Tracker.Referral.Payment.Notify";
        public const string Pages_Tracker_Referral_SMS = "Pages.Tracker.Referral.Payment.SMS";
        public const string Pages_Tracker_Referral_Email = "Pages.Tracker.Referral.Payment.Email";
        public const string Pages_Tracker_Referral_ToDo = "Pages.Tracker.Referral.Payment.ToDo";
        public const string Pages_Tracker_Referral_Comment = "Pages.Tracker.Referral.Payment.Comment";
        public const string Pages_Tracker_Referral_Reminder = "Pages.Tracker.Referral.Payment.Reminder";
        public const string Pages_Tracker_Referral_Export = "Pages.Tracker.Referral.Payment.Export";
        public const string Pages_Tracker_Referral_Verify = "Pages.Tracker.Referral.Payment.Verify";
        public const string Pages_Tracker_Referral_NotVerify = "Pages.Tracker.Referral.Payment.NotVerify";
        public const string Pages_Tracker_Referral_RevertNotes = "Pages.Tracker.Referral.Payment.RevertNotes";

        // Refund Tab Permission
        public const string Pages_Job_Refunds = "Pages.Job.Refunds";
        public const string Pages_Job_Refunds_Create = "Pages.Job.Refunds.Create";
        public const string Pages_Job_Refunds_Edit = "Pages.Job.Refunds.Edit";
        public const string Pages_Job_Refunds_Delete = "Pages.Job.Refunds.Delete";


        public const string Pages_JobGrid = "Pages.JobGrid";
        public const string Pages_JobGrid_Edit = "Pages.JobGrid.JobEdit";
        public const string Pages_JobGrid_Export = "Pages.JobGrid.Export";
        public const string Pages_JobGrid_Export_ProductDetail = "Pages.JobGrid.Export.ProductDetail";
        public const string Pages_JobGrid_Export_ProductDetailItemWise = "Pages.JobGrid.Export.ProductDetailItemWise";
        //public const string Pages_JobGrid_Import = "Pages.JobGrid.Import";
        public const string Pages_JobGrid_JobDetail = "Pages.JobGrid.JobDetail";
        public const string Pages_JobGrid_Notify = "Pages.JobGrid.Notify";
        public const string Pages_JobGrid_QuickView = "Pages.JobGrid.QuickView";
        public const string Pages_JobGrid_SMS = "Pages.JobGrid.SMS";
        public const string Pages_JobGrid_Email = "Pages.JobGrid.Email";
        public const string Pages_JobGrid_ToDo = "Pages.JobGrid.ToDo";
        public const string Pages_JobGrid_Comment = "Pages.JobGrid.Comment";
        public const string Pages_JobGrid_Reminder = "Pages.JobGrid.Reminder";
        public const string Pages_JobGrid_ProjectOpenDate = "Pages.JobGrid.ProjectOpenDate";


        public const string Pages_HoldJobTracker = "Pages.HoldJobTracker";
        public const string Pages_HoldJobTracker_Edit = "Pages.HoldJobTracker.JobEdit";
        public const string Pages_HoldJobTracker_Export = "Pages.HoldJobTracker.Export";
        public const string Pages_HoldJobTracker_JobDetail = "Pages.HoldJobTracker.JobDetail";
        public const string Pages_HoldJobTracker_Notify = "Pages.HoldJobTracker.Notify";
        public const string Pages_HoldJobTracker_QuickView = "Pages.HoldJobTracker.QuickView";
        public const string Pages_HoldJobTracker_SMS = "Pages.HoldJobTracker.SMS";
        public const string Pages_HoldJobTracker_Email = "Pages.HoldJobTracker.Email";
        public const string Pages_HoldJobTracker_ToDo = "Pages.HoldJobTracker.ToDo";
        public const string Pages_HoldJobTracker_Comment = "Pages.HoldJobTracker.Comment";
        public const string Pages_HoldJobTracker_Reminder = "Pages.HoldJobTracker.Reminder";

        public const string Pages_HoldReasons = "Pages.HoldReasons";
        public const string Pages_HoldReasons_Create = "Pages.HoldReasons.Create";
        public const string Pages_HoldReasons_Edit = "Pages.HoldReasons.Edit";
        public const string Pages_HoldReasons_Delete = "Pages.HoldReasons.Delete";

        //For Document TYpe 
        public const string Pages_Tenant_datavault_DocumentType = "Pages.Tenant.datavault.DocumentType";
        public const string Pages_Tenant_datavault_DocumentType_Create = "Pages.Tenant.datavault.DocumentType.Create";
        public const string Pages_Tenant_datavault_DocumentType_Edit = "Pages.Tenant.datavault.DocumentType.Edit";
        public const string Pages_Tenant_datavault_DocumentType_Delete = "Pages.Tenant.datavault.DocumentType.Delete";

        //For Document Library 
        public const string Pages_Tenant_datavault_DocumentLibrary = "Pages.Tenant.datavault.DocumentLibrary";
        public const string Pages_Tenant_datavault_DocumentLibrary_Create = "Pages.Tenant.datavault.DocumentLibrary.Create";
        public const string Pages_Tenant_datavault_DocumentLibrary_Edit = "Pages.Tenant.datavault.DocumentLibrary.Edit";
        public const string Pages_Tenant_datavault_DocumentLibrary_Delete = "Pages.Tenant.datavault.DocumentLibrary.Delete";


        //For JOb Cancellation Reason
        public const string Pages_Tenant_datavault_JobCancellationReason = "Pages.Tenant.datavault.JobCancellationReason";
        public const string Pages_Tenant_datavault_JobCancellationReason_Create = "Pages.Tenant.datavault.JobCancellationReason.Create";
        public const string Pages_Tenant_datavault_JobCancellationReason_Edit = "Pages.Tenant.datavault.JobCancellationReason.Edit";
        public const string Pages_Tenant_datavault_JobCancellationReason_Delete = "Pages.Tenant.datavault.JobCancellationReason.Delete";

        public const string Pages_Tenant_Installer = "Pages.Tenant.Installer";
        public const string Pages_Tenant_Installer_Invoice = "Pages.Tenant.InstallerInvoice";
        public const string Pages_Tenant_Installer_Availability = "Pages.Tenant.Installer.Availability";
        public const string Pages_Tenant_InstallerManager_InstallerCalendar = "Pages.Tenant.InstallerManager.InstallerCalendar";

        public const string Pages_EmailTemplates = "Pages.Tenant.EmailTemplates";

        public const string Pages_RefundReasons = "Pages.RefundReasons";
        public const string Pages_RefundReasons_Create = "Pages.RefundReasons.Create";
        public const string Pages_RefundReasons_Edit = "Pages.RefundReasons.Edit";
        public const string Pages_RefundReasons_Delete = "Pages.RefundReasons.Delete";




        public const string Pages_InstallerDetails = "Pages.InstallerDetails";
        public const string Pages_InstallerDetails_Create = "Pages.InstallerDetails.Create";
        public const string Pages_InstallerDetails_Edit = "Pages.InstallerDetails.Edit";
        public const string Pages_InstallerDetails_Delete = "Pages.InstallerDetails.Delete";

        public const string Pages_InstallerAddresses = "Pages.InstallerAddresses";
        public const string Pages_InstallerAddresses_Create = "Pages.InstallerAddresses.Create";
        public const string Pages_InstallerAddresses_Edit = "Pages.InstallerAddresses.Edit";
        public const string Pages_InstallerAddresses_Delete = "Pages.InstallerAddresses.Delete";

        public const string Pages_InstallationSection = "Pages.InstallationSection";
        public const string Pages_Calenders = "Pages.Calenders";
        public const string Pages_Map = "Pages.Map";
        public const string Pages_Map_JobView = "Pages.Map.JobView";

        public const string Pages_Installation = "Pages.Installation";
        public const string Pages_Installation_JobView = "Pages.Installation.JobView";

        public const string Pages_MyInstaller = "Pages.MyInstaller";
        public const string Pages_MyInstaller_Create = "Pages.MyInstaller.Create";
        public const string Pages_MyInstaller_Edit = "Pages.MyInstaller.Edit";
        public const string Pages_MyInstaller_View = "Pages.MyInstaller.View";
        public const string Pages_MyInstaller_Comment = "Pages.MyInstaller.Comment";
        public const string Pages_MyInstaller_Reminder = "Pages.MyInstaller.Reminder";
        public const string Pages_MyInstaller_Sms = "Pages.MyInstaller.SMS";
        public const string Pages_MyInstaller_Email = "Pages.MyInstaller.Email";
        public const string Pages_MyInstaller_Notify = "Pages.MyInstaller.Notify";
        public const string Pages_MyInstaller_ToDo = "Pages.MyInstaller.ToDo";
        public const string Pages_MyInstaller_UploadDoc = "Pages.MyInstaller.UploadDoc";
        public const string Pages_MyInstaller_PriceList = "Pages.MyInstaller.PriceList";
        public const string Pages_MyInstaller_SignContract = "Pages.MyInstaller.SignContract";
        public const string Pages_MyInstaller_AssignUser = "Pages.MyInstaller.AssignUser";
        public const string Pages_MyInstaller_Export = "Pages.MyInstaller.Export";

        public const string Pages_PendingInstallation = "Pages.PendingInstallation";
        public const string Pages_PendingInstallation_View = "Pages.PendingInstallation.View";
        public const string Pages_PendingInstallation_ToDo = "Pages.PendingInstallation.ToDo";
        public const string Pages_PendingInstallation_Email = "Pages.PendingInstallation.Email";
        public const string Pages_PendingInstallation_Sms = "Pages.PendingInstallation.SMS";
        public const string Pages_PendingInstallation_Comment = "Pages.PendingInstallation.Comment";
        public const string Pages_PendingInstallation_Notify = "Pages.PendingInstallation.Notify";
        public const string Pages_PendingInstallation_Reminder = "Pages.PendingInstallation.Reminder";
        public const string Pages_PendingInstallation_AssignUser = "Pages.PendingInstallation.AssignUser";
        public const string Pages_PendingInstallation_Export = "Pages.PendingInstallation.Export";



        public const string Pages_JobBooking_Notify = "Pages.JobBooking.Notify";
        public const string Pages_JobBooking_ToDo = "Pages.JobBooking.ToDo";
        public const string Pages_JobBooking_Comment = "Pages.JobBooking.Comment";
        public const string Pages_JobBooking_Reminder = "Pages.JobBooking.Reminder";
        public const string Pages_JobBooking_Export = "Pages.JobBooking.Export";
        public const string Pages_JobBooking_ViewJobDetail = "Pages.JobBooking.ViewJobDetail";
        public const string Pages_JobBooking_Sms = "Pages.JobBooking.SMS";
        public const string Pages_JobBookingn_Email = "Pages.JobBooking.Email";
        public const string Pages_JobBooking = "Pages.JobBooking";
        public const string Pages_InvoicePaymentMethods = "Pages.InvoicePaymentMethods";
        public const string Pages_InvoicePaymentMethods_Create = "Pages.InvoicePaymentMethods.Create";
        public const string Pages_InvoicePaymentMethods_Edit = "Pages.InvoicePaymentMethods.Edit";
        public const string Pages_InvoicePaymentMethods_Delete = "Pages.InvoicePaymentMethods.Delete";

        public const string Pages_InvoicePayments = "Pages.InvoicePayments";
        public const string Pages_InvoicePayments_Create = "Pages.InvoicePayments.Create";
        public const string Pages_InvoicePayments_Edit = "Pages.InvoicePayments.Edit";
        public const string Pages_InvoicePayments_Delete = "Pages.InvoicePayments.Delete";
        public const string Pages_InvoicePayments_Verify = "Pages.InvoicePayments.Verify";

        public const string Pages_JobProductItems = "Pages.JobProductItems";
        public const string Pages_JobProductItems_Create = "Pages.JobProductItems.Create";
        public const string Pages_JobProductItems_Edit = "Pages.JobProductItems.Edit";
        public const string Pages_JobProductItems_Delete = "Pages.JobProductItems.Delete";

        public const string Pages_PromotionMasters = "Pages.PromotionMasters";
        public const string Pages_PromotionMasters_Create = "Pages.PromotionMasters.Create";
        public const string Pages_PromotionMasters_Edit = "Pages.PromotionMasters.Edit";
        public const string Pages_PromotionMasters_Delete = "Pages.PromotionMasters.Delete";

        public const string Pages_FinanceOptions = "Pages.FinanceOptions";
        public const string Pages_FinanceOptions_Create = "Pages.FinanceOptions.Create";
        public const string Pages_FinanceOptions_Edit = "Pages.FinanceOptions.Edit";
        public const string Pages_FinanceOptions_Delete = "Pages.FinanceOptions.Delete";

        public const string Pages_HouseTypes = "Pages.HouseTypes";
        public const string Pages_HouseTypes_Create = "Pages.HouseTypes.Create";
        public const string Pages_HouseTypes_Edit = "Pages.HouseTypes.Edit";
        public const string Pages_HouseTypes_Delete = "Pages.HouseTypes.Delete";

        public const string Pages_CancelReasons = "Pages.CancelReasons";
        public const string Pages_CancelReasons_Create = "Pages.CancelReasons.Create";
        public const string Pages_CancelReasons_Edit = "Pages.CancelReasons.Edit";
        public const string Pages_CancelReasons_Delete = "Pages.CancelReasons.Delete";

        public const string Pages_RejectReasons = "Pages.RejectReasons";
        public const string Pages_RejectReasons_Create = "Pages.RejectReasons.Create";
        public const string Pages_RejectReasons_Edit = "Pages.RejectReasons.Edit";
        public const string Pages_RejectReasons_Delete = "Pages.RejectReasons.Delete";

        public const string Pages_MeterPhases = "Pages.MeterPhases";
        public const string Pages_MeterPhases_Create = "Pages.MeterPhases.Create";
        public const string Pages_MeterPhases_Edit = "Pages.MeterPhases.Edit";
        public const string Pages_MeterPhases_Delete = "Pages.MeterPhases.Delete";

        public const string Pages_MeterUpgrades = "Pages.MeterUpgrades";
        public const string Pages_MeterUpgrades_Create = "Pages.MeterUpgrades.Create";
        public const string Pages_MeterUpgrades_Edit = "Pages.MeterUpgrades.Edit";
        public const string Pages_MeterUpgrades_Delete = "Pages.MeterUpgrades.Delete";

        public const string Pages_ElecRetailers = "Pages.ElecRetailers";
        public const string Pages_ElecRetailers_Create = "Pages.ElecRetailers.Create";
        public const string Pages_ElecRetailers_Edit = "Pages.ElecRetailers.Edit";
        public const string Pages_ElecRetailers_Delete = "Pages.ElecRetailers.Delete";

        public const string Pages_DepositOptions = "Pages.DepositOptions";
        public const string Pages_DepositOptions_Create = "Pages.DepositOptions.Create";
        public const string Pages_DepositOptions_Edit = "Pages.DepositOptions.Edit";
        public const string Pages_DepositOptions_Delete = "Pages.DepositOptions.Delete";
        public const string Pages_DepositOptions_Export = "Pages.DepositOptions.Export";

        public const string Pages_Quotations = "Pages.Quotations";
        public const string Pages_Quotations_Create = "Pages.Quotations.Create";
        public const string Pages_Quotations_Edit = "Pages.Quotations.Edit";
        public const string Pages_Quotations_Delete = "Pages.Quotations.Delete";

        public const string Pages_PaymentOptions = "Pages.PaymentOptions";
        public const string Pages_PaymentOptions_Create = "Pages.PaymentOptions.Create";
        public const string Pages_PaymentOptions_Edit = "Pages.PaymentOptions.Edit";
        public const string Pages_PaymentOptions_Delete = "Pages.PaymentOptions.Delete";
        public const string Pages_PaymentOptions_Export = "Pages.PaymentOptions.Export";

        public const string Pages_JobVariations = "Pages.JobVariations";
        public const string Pages_JobVariations_Create = "Pages.JobVariations.Create";
        public const string Pages_JobVariations_Edit = "Pages.JobVariations.Edit";
        public const string Pages_JobVariations_Delete = "Pages.JobVariations.Delete";

        public const string Pages_Variations = "Pages.Variations";
        public const string Pages_Variations_Create = "Pages.Variations.Create";
        public const string Pages_Variations_Edit = "Pages.Variations.Edit";
        public const string Pages_Variations_Delete = "Pages.Variations.Delete";

        public const string Pages_JobPromotions = "Pages.JobPromotions";
        public const string Pages_JobPromotions_Create = "Pages.JobPromotions.Create";
        public const string Pages_JobPromotions_Edit = "Pages.JobPromotions.Edit";
        public const string Pages_JobPromotions_Delete = "Pages.JobPromotions.Delete";

        public const string Pages_JobProducts = "Pages.JobProducts";
        public const string Pages_JobProducts_Create = "Pages.JobProducts.Create";
        public const string Pages_JobProducts_Edit = "Pages.JobProducts.Edit";
        public const string Pages_JobProducts_Delete = "Pages.JobProducts.Delete";

        public const string Pages_JobPickList = "Pages.JobPickList";
        public const string Pages_JobPickList_Create = "Pages.JobPickList.Create";
        public const string Pages_JobPickList_Edit = "Pages.JobPickList.Edit";
        public const string Pages_JobPickList_Delete = "Pages.JobPickList.Delete";
        public const string Pages_Jobs_WithSurchargePayWay = "Pages.Jobs.WithSurchargePayWay";
        public const string Pages_Jobs_WithoutSurchargePayWay = "Pages.Jobs.WithoutSurchargePayWay";
        public const string Pages_JobPickList_NagativeStock = "Pages.JobPickList.NagativeStock";

        public const string Pages_Jobs = "Pages.Jobs";
        public const string Pages_Jobs_Create = "Pages.Jobs.Create";
        public const string Pages_Jobs_Edit = "Pages.Jobs.Edit";
        public const string Pages_Jobs_Delete = "Pages.Jobs.Delete";
        public const string Pages_Jobs_Sales = "Pages.Jobs.Sales";
        public const string Pages_Jobs_Quotation = "Pages.Jobs.Quotation";
        public const string Pages_Jobs_Installation = "Pages.Jobs.Installation";
        public const string Pages_Jobs_PostInstallation = "Pages.Jobs.PostInstallation";
        //public const string Pages_Jobs_Refund = "Pages.Jobs.Refund";

        public const string Pages_Jobs_StatusChange = "Pages.Jobs.StatusChange";
        public const string Pages_Jobs_StatusChange_Planned = "Pages.Jobs.StatusChange.Planned";
        public const string Pages_Jobs_StatusChange_Hold = "Pages.Jobs.StatusChange.Hold";
        public const string Pages_Jobs_StatusChange_Cancel = "Pages.Jobs.StatusChange.Cancel";
        public const string Pages_Jobs_StatusChange_DepositReceived = "Pages.Jobs.StatusChange.DepositReceived";
        public const string Pages_Jobs_StatusChange_Active = "Pages.Jobs.StatusChange.Active";

        public const string Pages_Jobs_Detail = "Pages.Jobs.Detail";
        public const string Pages_Jobs_Variation = "Pages.Jobs.Variation";
        public const string Pages_Jobs_CheckProductStock = "Pages.Jobs.CheckProductStock";
        public const string Pages_Jobs_SecondPicklistWithPanelInverter = "Pages.Jobs.SecondPicklistWithPanelInverter";
        public const string Pages_Jobs_CheckActualAudit = "Pages.Jobs.CheckActualAudit";
        public const string Pages_Jobs_ShowIsBonus = "Pages.Jobs.ShowIsBonus";
        public const string Pages_Jobs_EditOnHold = "Pages.Jobs.EditOnHold";
        public const string Pages_Jobs_ShowIsBonus_Amount = "Pages.Jobs.ShowIsBonus.Amount";

        public const string Pages_Review = "Pages.Review";
        public const string Pages_Review_Email = "Pages.Review.Email";
        public const string Pages_Review_Notify = "Pages.Review.Notify";
        public const string Pages_Review_QuickView = "Pages.Review.QuickView";
        public const string Pages_Review_SMS = "Pages.Review.SMS";
        public const string Pages_Review_ToDo = "Pages.Review.ToDo";
        public const string Pages_Review_Reminder = "Pages.Review.Reminder";
        public const string Pages_Review_Comment = "Pages.Review.Comment";
        public const string Pages_Review_AddReview = "Pages.Review.AddReview";
        public const string Pages_Service = "Pages.Service";
        public const string Pages_MyService = "Pages.Service.MyService";
        public const string Pages_MyService_AddServiceInstaller = "Pages.Service.MyService.AddServiceInstaller";
        public const string Pages_MyService_Create = "Pages.Service.MyService.Create";
        public const string Pages_MyService_Edit = "Pages.Service.MyService.Edit";
        public const string Pages_MyService_SMS = "Pages.Service.MyService.SMS";
        public const string Pages_MyService_Email = "Pages.Service.MyService.Email";
        public const string Pages_MyService_Notify = "Pages.Service.MyService.Notify";
        public const string Pages_MyService_QuickView = "Pages.Service.MyService.QuickView";
        public const string Pages_MyService_ToDo = "Pages.Service.MyService.ToDo";
        public const string Pages_MyService_Reminder = "Pages.Service.MyService.Reminder";
        public const string Pages_MyService_Comment = "Pages.Service.MyService.Comment";
        public const string Pages_MyService_AssignUser = "Pages.Service.MyService.AssignUser";
        public const string Pages_MyService_Delete = "Pages.Service.MyService.Delete";
        public const string Pages_MyService_Export = "Pages.Service.MyService.Export";
        public const string Pages_ServiceMap = "Pages.Service.ServiceMap";
        public const string Pages_ServiceInstallation = "Pages.Service.ServiceInstallation";
        public const string Pages_ServiceInstaller = "Pages.Service.ServiceInstaller";
        public const string Pages_ServiceInstallationJobView = "Pages.Service.ServiceInstallationJobView";
        public const string Pages_JobStatuses = "Pages.JobStatuses";
        public const string Pages_JobStatuses_Create = "Pages.JobStatuses.Create";
        public const string Pages_JobStatuses_Edit = "Pages.JobStatuses.Edit";
        public const string Pages_JobStatuses_Delete = "Pages.JobStatuses.Delete";

        public const string Pages_ProductItems = "Pages.ProductItems";
        public const string Pages_ProductItems_Create = "Pages.ProductItems.Create";
        public const string Pages_ProductItems_Edit = "Pages.ProductItems.Edit";
        public const string Pages_ProductItems_Delete = "Pages.ProductItems.Delete";
        public const string Pages_ProductItems_View = "Pages.ProductItems.View";
        public const string Pages_ProductItems_Upload = "Pages.ProductItems.Upload";
        public const string Pages_ProductItems_UploadImage = "Pages.ProductItems.UploadImage";
        public const string Pages_ProductItems_Download = "Pages.ProductItems.Download";

        public const string Pages_ElecDistributors = "Pages.ElecDistributors";
        public const string Pages_ElecDistributors_Create = "Pages.ElecDistributors.Create";
        public const string Pages_ElecDistributors_Edit = "Pages.ElecDistributors.Edit";
        public const string Pages_ElecDistributors_Delete = "Pages.ElecDistributors.Delete";

        public const string Pages_RoofAngles = "Pages.RoofAngles";
        public const string Pages_RoofAngles_Create = "Pages.RoofAngles.Create";
        public const string Pages_RoofAngles_Edit = "Pages.RoofAngles.Edit";
        public const string Pages_RoofAngles_Delete = "Pages.RoofAngles.Delete";

        public const string Pages_RoofTypes = "Pages.RoofTypes";
        public const string Pages_RoofTypes_Create = "Pages.RoofTypes.Create";
        public const string Pages_RoofTypes_Edit = "Pages.RoofTypes.Edit";
        public const string Pages_RoofTypes_Delete = "Pages.RoofTypes.Delete";

        public const string Pages_ProductTypes = "Pages.ProductTypes";
        public const string Pages_ProductTypes_Create = "Pages.ProductTypes.Create";
        public const string Pages_ProductTypes_Edit = "Pages.ProductTypes.Edit";
        public const string Pages_ProductTypes_Delete = "Pages.ProductTypes.Delete";

        public const string Pages_JobTypes = "Pages.JobTypes";
        public const string Pages_JobTypes_Create = "Pages.JobTypes.Create";
        public const string Pages_JobTypes_Edit = "Pages.JobTypes.Edit";
        public const string Pages_JobTypes_Delete = "Pages.JobTypes.Delete";

        public const string Pages_PromotionGroup = "Pages.PromotionGroup";
        public const string Pages_PromotionUsers = "Pages.PromotionUsers";
        public const string Pages_PromotionUsers_View = "Pages.PromotionUsers.View";
        //public const string Pages_PromotionUsers_Create = "Pages.PromotionUsers.Create";
        //public const string Pages_PromotionUsers_Edit = "Pages.PromotionUsers.Edit";
        //public const string Pages_PromotionUsers_Delete = "Pages.PromotionUsers.Delete";
        public const string Pages_PromotionUsers_Excel = "Pages.PromotionUsers.Excel";
        public const string Pages_PromotionUsers_Excel_MobileEmail = "Pages.PromotionUsers.Excel.MobileEmail";

        public const string Pages_Promotions = "Pages.Promotions";
        public const string Pages_Promotions_Create = "Pages.Promotions.Create";
        public const string Pages_Promotions_View = "Pages.Promotions.View";
        //public const string Pages_Promotions_Edit = "Pages.Promotions.Edit";
        public const string Pages_Promotions_Delete = "Pages.Promotions.Delete";
        public const string Pages_Promotions_ExportExcel = "Pages.Promotions.ExportExcel";

        //public const string Pages_PromotionResponseStatuses = "Pages.PromotionResponseStatuses";
        //public const string Pages_PromotionResponseStatuses_Create = "Pages.PromotionResponseStatuses.Create";
        //public const string Pages_PromotionResponseStatuses_Edit = "Pages.PromotionResponseStatuses.Edit";
        //public const string Pages_PromotionResponseStatuses_Delete = "Pages.PromotionResponseStatuses.Delete";

        public const string Pages_PromotionTypes = "Pages.PromotionTypes";
        public const string Pages_PromotionTypes_Create = "Pages.PromotionTypes.Create";
        public const string Pages_PromotionTypes_Edit = "Pages.PromotionTypes.Edit";
        public const string Pages_PromotionTypes_Delete = "Pages.PromotionTypes.Delete";

        public const string Pages_Departments = "Pages.Departments";
        public const string Pages_Departments_Create = "Pages.Departments.Create";
        public const string Pages_Departments_Edit = "Pages.Departments.Edit";
        public const string Pages_Departments_Delete = "Pages.Departments.Delete";

        public const string Pages_Tenant_DataVaults_EmailTemplates = "Pages.Tenant.DataVaults.EmailTemplates";
        public const string Pages_Tenant_DataVaults_EmailTemplates_Create = "Pages.Tenant.DataVaults.EmailTemplates.Create";
        public const string Pages_Tenant_DataVaults_EmailTemplates_Edit = "Pages.Tenant.DataVaults.EmailTemplates.Edit";
        public const string Pages_Tenant_DataVaults_EmailTemplates_Delete = "Pages.Tenant.DataVaults.EmailTemplates.Delete";

        public const string Pages_Tenant_DataVaults_InstallationCost = "Pages.Tenant.DataVaults.InstallationCost";

        public const string Pages_Tenant_DataVaults_InstallationCost_STCCost = "Pages.Tenant.DataVaults.InstallationCost.STCCost";
        public const string Pages_Tenant_DataVaults_InstallationCost_STCCost_Create = "Pages.Tenant.DataVaults.InstallationCost.STCCost.Create";
        public const string Pages_Tenant_DataVaults_InstallationCost_STCCost_Edit = "Pages.Tenant.DataVaults.InstallationCost.STCCost.Edit";
        public const string Pages_Tenant_DataVaults_InstallationCost_STCCost_Delete = "Pages.Tenant.DataVaults.InstallationCost.STCCost.Delete";

        public const string Pages_Tenant_DataVaults_CheckDepositReceived = "Pages.Tenant.DataVaults.CheckDepositReceived";
        public const string Pages_Tenant_DataVaults_CheckDepositReceived_Create = "Pages.Tenant.DataVaults.CheckDepositReceived.Create";
        public const string Pages_Tenant_DataVaults_CheckDepositReceived_Edit = "Pages.Tenant.DataVaults.CheckDepositReceived.Edit";
        public const string Pages_Tenant_DataVaults_CheckDepositReceived_Delete = "Pages.Tenant.DataVaults.CheckDepositReceived.Delete";
        public const string Pages_Tenant_DataVaults_CheckDepositReceived_Export = "Pages.Tenant.DataVaults.CheckDepositReceived.Export";

        public const string Pages_Tenant_DataVaults_InstallationCost_BatteryInstallationCost = "Pages.Tenant.DataVaults.InstallationCost.BatteryInstallationCost";
        public const string Pages_Tenant_DataVaults_InstallationCost_BatteryInstallationCost_Create = "Pages.Tenant.DataVaults.InstallationCost.BatteryInstallationCost.Create";
        public const string Pages_Tenant_DataVaults_InstallationCost_BatteryInstallationCost_Edit = "Pages.Tenant.DataVaults.InstallationCost.BatteryInstallationCost.Edit";
        public const string Pages_Tenant_DataVaults_InstallationCost_BatteryInstallationCost_Delete = "Pages.Tenant.DataVaults.InstallationCost.BatteryInstallationCost.Delete";


        public const string Pages_Tenant_DataVaults_InstallationCost_OtherCharges = "Pages.Tenant.DataVaults.InstallationCost.OtherCharges";
        public const string Pages_Tenant_DataVaults_InstallationCost_OtherCharges_Create = "Pages.Tenant.DataVaults.InstallationCost.OtherCharges.Create";
        public const string Pages_Tenant_DataVaults_InstallationCost_OtherCharges_Edit = "Pages.Tenant.DataVaults.InstallationCost.OtherCharges.Edit";
        public const string Pages_Tenant_DataVaults_InstallationCost_OtherCharges_Delete = "Pages.Tenant.DataVaults.InstallationCost.OtherCharges.Delete";

        public const string Pages_Tenant_DataVaults_InstallationCost_StateWiseInstallationCost = "Pages.Tenant.DataVaults.InstallationCost.StateWiseInstallationCost";
        public const string Pages_Tenant_DataVaults_InstallationCost_StateWiseInstallationCost_Create = "Pages.Tenant.DataVaults.InstallationCost.StateWiseInstallationCost.Create";
        public const string Pages_Tenant_DataVaults_InstallationCost_StateWiseInstallationCost_Edit = "Pages.Tenant.DataVaults.InstallationCost.StateWiseInstallationCost.Edit";
        public const string Pages_Tenant_DataVaults_InstallationCost_StateWiseInstallationCost_Delete = "Pages.Tenant.DataVaults.InstallationCost.StateWiseInstallationCost.Delete";

        public const string Pages_Tenant_DataVaults_InstallationCost_FixedCostPrice = "Pages.Tenant.DataVaults.InstallationCost.FixedCostPrice";
        public const string Pages_Tenant_DataVaults_InstallationCost_FixedCostPrice_Create = "Pages.Tenant.DataVaults.InstallationCost.FixedCostPrice.Create";
        public const string Pages_Tenant_DataVaults_InstallationCost_FixedCostPrice_Edit = "Pages.Tenant.DataVaults.InstallationCost.FixedCostPrice.Edit";
        public const string Pages_Tenant_DataVaults_InstallationCost_FixedCostPrice_Delete = "Pages.Tenant.DataVaults.InstallationCost.FixedCostPrice.Delete";

        public const string Pages_Tenant_DataVaults_InstallationCost_InstallationItemList = "Pages.Tenant.DataVaults.InstallationCost.InstallationItemList";
        public const string Pages_Tenant_DataVaults_InstallationCost_InstallationItemList_View = "Pages.Tenant.DataVaults.InstallationCost.InstallationItemList.View";
        public const string Pages_Tenant_DataVaults_InstallationCost_InstallationItemList_Create = "Pages.Tenant.DataVaults.InstallationCost.InstallationItemList.Create";
        public const string Pages_Tenant_DataVaults_InstallationCost_InstallationItemList_Edit = "Pages.Tenant.DataVaults.InstallationCost.InstallationItemList.Edit";
        public const string Pages_Tenant_DataVaults_InstallationCost_InstallationItemList_Delete = "Pages.Tenant.DataVaults.InstallationCost.InstallationItemList.Delete";
        public const string Pages_Tenant_DataVaults_InstallationCost_InstallationItemList_LastMonthEdit = "Pages.Tenant.DataVaults.InstallationCost.InstallationItemList.LastMonthEdit";

        public const string Pages_Tenant_DataVaults_InstallationCost_ExtraInstallationCharges = "Pages.Tenant.DataVaults.InstallationCost.ExtraInstallationCharges";
        public const string Pages_Tenant_DataVaults_InstallationCost_ExtraInstallationCharges_Create = "Pages.Tenant.DataVaults.InstallationCost.ExtraInstallationCharges.Create";
        public const string Pages_Tenant_DataVaults_InstallationCost_ExtraInstallationCharges_Edit = "Pages.Tenant.DataVaults.InstallationCost.ExtraInstallationCharges.Edit";
        public const string Pages_Tenant_DataVaults_InstallationCost_ExtraInstallationCharges_Delete = "Pages.Tenant.DataVaults.InstallationCost.ExtraInstallationCharges.Delete";

        public const string Pages_LeadExpenses = "Pages.LeadExpenses";
        public const string Pages_LeadExpenses_Create = "Pages.LeadExpenses.Create";
        public const string Pages_LeadExpenses_Edit = "Pages.LeadExpenses.Edit";
        public const string Pages_LeadExpenses_Delete = "Pages.LeadExpenses.Delete";
        public const string Pages_LeadExpenses_LastMonthEdit = "Pages.LeadExpenses.LastMonthEdit";

        public const string Pages_UserTeams = "Pages.UserTeams";
        public const string Pages_UserTeams_Create = "Pages.UserTeams.Create";
        public const string Pages_UserTeams_Edit = "Pages.UserTeams.Edit";
        public const string Pages_UserTeams_Delete = "Pages.UserTeams.Delete";

        public const string Pages_Categories = "Pages.Categories";
        public const string Pages_Categories_Create = "Pages.Categories.Create";
        public const string Pages_Categories_Edit = "Pages.Categories.Edit";
        public const string Pages_Categories_Delete = "Pages.Categories.Delete";

        public const string Pages_Teams = "Pages.Teams";
        public const string Pages_Teams_Create = "Pages.Teams.Create";
        public const string Pages_Teams_Edit = "Pages.Teams.Edit";
        public const string Pages_Teams_Delete = "Pages.Teams.Delete";

        public const string Pages_Leads = "Pages.Leads";

        public const string Pages_ManageLeads = "Pages.ManageLeads";
        public const string Pages_Leads_Create = "Pages.Leads.Create";
        public const string Pages_Leads_Edit = "Pages.Leads.Edit";
        public const string Pages_Leads_Delete = "Pages.Leads.Delete";
        public const string Pages_Leads_Export = "Pages.Leads.Export";
        public const string Pages_Leads_Import = "Pages.Leads.Import";
        public const string Pages_Leads_Assign = "Pages.Leads.Assign";
        public const string Pages_Leads_AssignToDifferentOrganization = "Pages.Leads.AssignToDifferentOrganization";
        public const string Pages_Leads_CopyToDifferentOrganization = "Pages.Leads.CopyToDifferentOrganization";
        public const string Pages_Leads_MarkAsDuplicate = "Pages.Leads.MarkAsDuplicate";
        public const string Pages_Leads_UpdateFacebookLeads = "Pages.Leads.UpdateFacebookLeads";
        public const string Pages_Leads_FakeLeads = "Pages.Leads.FakeLeads";

        public const string Pages_LeadTracker = "Pages.LeadTracker";
        public const string Pages_LeadTracker_Export_MobileEmail = "Pages.LeadTracker.Export.MobileEmail";
        public const string Pages_LeadTracker_Edit = "Pages.LeadTracker.Edit";
        public const string Pages_LeadTracker_Delete = "Pages.LeadTracker.Delete";

        //Lead Tracker Permission
        public const string Pages_Leads_LeadTracker_Action = "Pages.Leads.LeadTracker.Action";
        public const string Pages_Leads_LeadTracker_Action_ChangeOrganization = "Pages.Leads.LeadTracker.ChangeOrganization";
        public const string Pages_Leads_LeadTracker_Action_AssignToSalesRep = "Pages.Leads.LeadTracker.Action.AssignToSalesRep";
        public const string Pages_Leads_LeadTracker_Action_AssignToSalesManager = "Pages.Leads.LeadTracker.Action.AssignToManager";
        public const string Pages_Leads_LeadTracker_Action_AssignToUsers = "Pages.Leads.LeadTracker.Action.AssignToUsers";
        public const string Pages_Leads_LeadTracker_Action_TransferToLeadGen = "Pages.Leads.LeadTracker.Action.TransferToLeadGen";
        public const string Pages_Leads_LeadTracker_Action_MarkAsFakeLead = "Pages.Leads.LeadTracker.Action.MarkAsFakeLead";
        public const string Pages_Leads_LeadTracker_Action_TransferLeftSalesRepLead = "Pages.Leads.LeadTracker.Action.TransferLeftSalesRepLead";
        //End Lead Tracker Permission

        public const string Pages_MyLeads = "Pages.MyLeads";
        public const string Pages_MyLeads_Create = "Pages.MyLeads.Create";
        public const string Pages_MyLeads_Export = "Pages.MyLeads.Export";
        public const string Pages_MyLeads_ShowLeadSource = "Pages.MyLeads.ShowLeadSource";
        public const string Pages_MyLeads_AssignUserId = "Pages.MyLeads.AssignUserId";
        public const string Pages_Lead_ManageService = "Pages.Lead.ManageService";
        public const string Pages_Lead_Duplicate = "Pages.Lead.Duplicate";

        public const string Pages_Lead_Closed = "Pages.Lead.Close";

        public const string Pages_Lead_Calcel = "Pages.Lead.Calcel";
        public const string Pages_Lead_Calcel_ExcelExport = "Pages.Lead.Calcel.ExcelExport";
        public const string Pages_Lead_Rejects = "Pages.Lead.Rejects";
        public const string Pages_Lead_Rejects_ExcelExport = "Pages.Lead.Rejects.ExcelExport";


        public const string Pages_LeadSources = "Pages.LeadSources";
        public const string Pages_LeadSources_Create = "Pages.LeadSources.Create";
        public const string Pages_LeadSources_Edit = "Pages.LeadSources.Edit";
        public const string Pages_LeadSources_Delete = "Pages.LeadSources.Delete";

        public const string Pages_PostalTypes = "Pages.PostalTypes";
        public const string Pages_PostalTypes_Create = "Pages.PostalTypes.Create";
        public const string Pages_PostalTypes_Edit = "Pages.PostalTypes.Edit";
        public const string Pages_PostalTypes_Delete = "Pages.PostalTypes.Delete";

        public const string Pages_PostCodes = "Pages.PostCodes";
        public const string Pages_PostCodes_Create = "Pages.PostCodes.Create";
        public const string Pages_PostCodes_Edit = "Pages.PostCodes.Edit";
        public const string Pages_PostCodes_Delete = "Pages.PostCodes.Delete";

        public const string Pages_StreetNames = "Pages.StreetNames";
        public const string Pages_StreetNames_Create = "Pages.StreetNames.Create";
        public const string Pages_StreetNames_Edit = "Pages.StreetNames.Edit";
        public const string Pages_StreetNames_Delete = "Pages.StreetNames.Delete";

        public const string Pages_StreetTypes = "Pages.StreetTypes";
        public const string Pages_StreetTypes_Create = "Pages.StreetTypes.Create";
        public const string Pages_StreetTypes_Edit = "Pages.StreetTypes.Edit";
        public const string Pages_StreetTypes_Delete = "Pages.StreetTypes.Delete";

        public const string Pages_UnitTypes = "Pages.UnitTypes";
        public const string Pages_UnitTypes_Create = "Pages.UnitTypes.Create";
        public const string Pages_UnitTypes_Edit = "Pages.UnitTypes.Edit";
        public const string Pages_UnitTypes_Delete = "Pages.UnitTypes.Delete";

        // Post Code Range
        public const string Pages_PostCodeRange = "Pages.PostCodeRange";
        public const string Pages_PostCodeRange_Create = "Pages.PostCodeRange.Create";
        public const string Pages_PostCodeRange_Edit = "Pages.PostCodeRange.Edit";
        public const string Pages_PostCodeRange_Delete = "Pages.PostCodeRange.Delete";

        public const string Pages_DataVaults = "Pages.DataVaults";

        public const string Pages_DataVaults_State = "Pages.DataVaults.State";
        public const string Pages_DataVaults_State_Edit = "Pages.DataVaults.State.Edit";

        public const string Pages_Voucher = "Pages.Voucher";
        public const string Pages_Vouchers_Create = "Pages.Voucher.Create";
        public const string Pages_Vouchers_Edit = "Pages.Voucher.Edit";
        public const string Pages_Vouchers_Delete = "Pages.Voucher.Delete";
        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";

        public const string Pages_DemoUiComponents = "Pages.DemoUiComponents";
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";
        public const string Pages_Administration_Users_Unlock = "Pages.Administration.Users.Unlock";
        public const string Pages_Administration_Users_PermissionReport = "Pages.Administration.Users.PermissionReport";


        public const string Pages_Administration_SalesRep = "Pages.Administration.SalesRep";
        public const string Pages_Administration_SalesRep_Create = "Pages.Administration.SalesRep.Create";
        public const string Pages_Administration_SalesRep_Edit = "Pages.Administration.SalesRep.Edit";
        public const string Pages_Administration_SalesRep_Delete = "Pages.Administration.SalesRep.Delete";
        public const string Pages_Administration_SalesRep_ChangePermissions = "Pages.Administration.SalesRep.ChangePermissions";
        public const string Pages_Administration_SalesRep_Impersonation = "Pages.Administration.SalesRep.Impersonation";
        public const string Pages_Administration_SalesRep_Unlock = "Pages.Administration.SalesRep.Unlock";

        public const string Pages_Administration_SalesManager = "Pages.Administration.SalesManager";
        public const string Pages_Administration_SalesManager_Create = "Pages.Administration.SalesManager.Create";
        public const string Pages_Administration_SalesManager_Edit = "Pages.Administration.SalesManager.Edit";
        public const string Pages_Administration_SalesManager_Delete = "Pages.Administration.SalesManager.Delete";
        public const string Pages_Administration_SalesManager_ChangePermissions = "Pages.Administration.SalesManager.ChangePermissions";
        public const string Pages_Administration_SalesManager_Impersonation = "Pages.Administration.SalesManager.Impersonation";
        public const string Pages_Administration_SalesManager_Unlock = "Pages.Administration.SalesManager.Unlock";

        public const string Pages_Administration_Installer = "Pages.Administration.Installer";
        public const string Pages_Administration_Installer_Create = "Pages.Administration.Installer.Create";
        public const string Pages_Administration_Installer_Edit = "Pages.Administration.Installer.Edit";
        public const string Pages_Administration_Installer_Delete = "Pages.Administration.Installer.Delete";
        public const string Pages_Administration_Installer_ChangePermissions = "Pages.Administration.Installer.ChangePermissions";
        public const string Pages_Administration_Installer_Impersonation = "Pages.Administration.Installer.Impersonation";
        public const string Pages_Administration_Installer_Unlock = "Pages.Administration.Installer.Unlock";

        public const string Pages_Administration_InstallerInvitation = "Pages.Installer.InstallerInvitation";
        public const string Pages_Installer_New = "Pages.Installer.New";
        public const string Pages_Installer_New_Create = "Pages.Installer.New.Create";
        public const string Pages_Installer_New_Edit = "Pages.Installer.New.Edit";
        public const string Pages_Installer_New_Delete = "Pages.Installer.New.Delete";
        public const string Pages_Installer_New_QuickView = "Pages.Installer.New.QuickView";
        public const string Pages_Installer_New_Export = "Pages.Installer.New.Export";


        public const string Pages_Installer_Paid = "Pages.Installer.Paid";
        public const string Pages_Installer_QuickView = "Pages.Installer.QuickView";
        public const string Pages_Installer_RevertVerify = "Pages.Installer.RevertVerify";
        public const string Pages_Installer_Notify = "Pages.Installer.Notify";
        public const string Pages_Installer_SMS = "Pages.Installer.SMS";
        public const string Pages_Installer_Email = "Pages.Installer.Email";
        public const string Pages_Installer_Invoice = "Pages.Installer.Invoice";
        public const string Pages_Installer_ReadyToPay = "Pages.Installer.ReadyToPay";
        public const string Pages_Installer_Verify = "Pages.Installer.Verify";
        public const string Pages_Installer_Delete = "Pages.Installer.Delete";
        public const string Pages_Installer_Edit = "Pages.Installer.Edit";
        public const string Pages_Installer_Reminder = "Pages.Installer.Reminder";
        public const string Pages_Installer_Comment = "Pages.Installer.Comment";
        public const string Pages_Installer_ToDo = "Pages.Installer.ToDo";
        public const string Pages_Installer_ExcelExport = "Pages.Installer.ExcelExport";
        public const string Pages_Installer_View = "Pages.Installer.View";


        public const string Pages_ReadyToPay = "Pages.ReadyToPay";
        public const string Pages_ReadyToPay_Sms = "Pages.ReadyToPay.Sms";
        public const string Pages_ReadyToPay_Email = "Pages.ReadyToPay.Email";
        public const string Pages_ReadyToPay_Unapproved = "Pages.ReadyToPay.Unapproved";
        public const string Pages_ReadyToPay_QuickView = "Pages.ReadyToPay.QuickView";
        public const string Pages_ReadyToPay_Payement = "Pages.ReadyToPay.Payement";
        public const string Pages_ReadyToPay_Payement_Revert = "Pages.ReadyToPay.Payement.Revert";
        public const string Pages_ReadyToPay_Notify = "Pages.ReadyToPay.Notify";
        public const string Pages_ReadyToPay_ExcelExport = "Pages.ReadyToPay.ExcelExport";
        public const string Pages_ReadyToPay_View = "Pages.ReadyToPay.View";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";
        public const string Pages_Administration_OrganizationUnits_ManageRoles = "Pages.Administration.OrganizationUnits.ManageRoles";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

        public const string Pages_Administration_UiCustomization = "Pages.Administration.UiCustomization";

        public const string Pages_Administration_WebhookSubscription = "Pages.Administration.WebhookSubscription";
        public const string Pages_Administration_WebhookSubscription_Create = "Pages.Administration.WebhookSubscription.Create";
        public const string Pages_Administration_WebhookSubscription_Edit = "Pages.Administration.WebhookSubscription.Edit";
        public const string Pages_Administration_WebhookSubscription_ChangeActivity = "Pages.Administration.WebhookSubscription.ChangeActivity";
        public const string Pages_Administration_WebhookSubscription_Detail = "Pages.Administration.WebhookSubscription.Detail";
        public const string Pages_Administration_Webhook_ListSendAttempts = "Pages.Administration.Webhook.ListSendAttempts";
        public const string Pages_Administration_Webhook_ResendWebhook = "Pages.Administration.Webhook.ResendWebhook";

        public const string Pages_Administration_DynamicParameters = "Pages.Administration.DynamicParameters";
        public const string Pages_Administration_DynamicParameters_Create = "Pages.Administration.DynamicParameters.Create";
        public const string Pages_Administration_DynamicParameters_Edit = "Pages.Administration.DynamicParameters.Edit";
        public const string Pages_Administration_DynamicParameters_Delete = "Pages.Administration.DynamicParameters.Delete";

        public const string Pages_Administration_DynamicParameterValue = "Pages.Administration.DynamicParameterValue";
        public const string Pages_Administration_DynamicParameterValue_Create = "Pages.Administration.DynamicParameterValue.Create";
        public const string Pages_Administration_DynamicParameterValue_Edit = "Pages.Administration.DynamicParameterValue.Edit";
        public const string Pages_Administration_DynamicParameterValue_Delete = "Pages.Administration.DynamicParameterValue.Delete";

        public const string Pages_Administration_EntityDynamicParameters = "Pages.Administration.EntityDynamicParameters";
        public const string Pages_Administration_EntityDynamicParameters_Create = "Pages.Administration.EntityDynamicParameters.Create";
        public const string Pages_Administration_EntityDynamicParameters_Edit = "Pages.Administration.EntityDynamicParameters.Edit";
        public const string Pages_Administration_EntityDynamicParameters_Delete = "Pages.Administration.EntityDynamicParameters.Delete";

        public const string Pages_Administration_EntityDynamicParameterValue = "Pages.Administration.EntityDynamicParameterValue";
        public const string Pages_Administration_EntityDynamicParameterValue_Create = "Pages.Administration.EntityDynamicParameterValue.Create";
        public const string Pages_Administration_EntityDynamicParameterValue_Edit = "Pages.Administration.EntityDynamicParameterValue.Edit";
        public const string Pages_Administration_EntityDynamicParameterValue_Delete = "Pages.Administration.EntityDynamicParameterValue.Delete";
        //TENANT-SPECIFIC PERMISSIONS
        public const string Pages_Tenant_Dashboard_UserLeads = "Pages.Tenant.Dashboard.UserLeads";
        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";
        //public const string Pages_Tenant_Dashboard_FilterSalesRepUsers = "Pages.Tenant.Dashboard.FilterSalesRepUsers";
        public const string Pages_Tenant_Dashboard_ManagerLeads = "Pages.Tenant.Dashboard.ManagerLeads";
        /*Widgets Permission Start*/
        public const string Pages_Tenant_Dashboard_OrganizationWiseLeads = "Pages.Tenant.Dashboard.OrganizationWiseLeads";
        public const string Pages_Tenant_Dashboard_OrganizationWiseLeadDetails = "Pages.Tenant.Dashboard.OrganizationWiseLeadDetails";
        public const string Pages_Tenant_Dashboard_OrganizationWiseTracker = "Pages.Tenant.Dashboard.OrganizationWiseTracker";
        public const string Pages_Tenant_Dashboard_MyReminders = "Pages.Tenant.Dashboard.MyReminders";
        public const string Pages_Tenant_Dashboard_SalesDetails = "Pages.Tenant.Dashboard.SalesDetails";
        public const string Pages_Tenant_Dashboard_InvoiceStatus = "Pages.Tenant.Dashboard.InvoiceStatus";
        public const string Pages_Tenant_Dashboard_ToDoDetails = "Pages.Tenant.Dashboard.ToDoDetails";
        public const string Pages_Tenant_Dashboard_ManagerToDoDetails = "Pages.Tenant.Dashboard.ManagerToDoDetails";
        public const string Pages_Tenant_Dashboard_ManagerSalesDetails = "Pages.Tenant.Dashboard.ManagerSalesDetails";
        public const string Pages_Tenant_Dashboard_LeadsSalesRank = "Pages.Tenant.Dashboard.LeadsSalesRank";
        public const string Pages_Tenant_Dashboard_LeadsDetails = "Pages.Tenant.Dashboard.LeadsDetails";
        public const string Pages_Tenant_Dashboard_LeadsStatus = "Pages.Tenant.Dashboard.LeadsStatus";
        public const string Pages_Tenant_Dashboard_LeadsStatusDetail = "Pages.Tenant.Dashboard.LeadsStatusDetail";

        /*Widgets Permission End*/
        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Administration_Tenant_SubscriptionManagement = "Pages.Administration.Tenant.SubscriptionManagement";

        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";
        public const string Pages_Editions_MoveTenantsToAnotherEdition = "Pages.Editions.MoveTenantsToAnotherEdition";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
        public const string Pages_Administration_Host_Dashboard = "Pages.Administration.Host.Dashboard";


        public const string Pages_Notification = "Pages.Notification";
        public const string Pages_Sms = "Pages.Sms";
        public const string Pages_Email = "Pages.Email";

        public const string Pages_Tenant_Retail = "Pages.Tenant.Retail";
        public const string Pages_Tenant_Retail_Dashboard = "Pages.Tenant.Retail.Dashboard";

        public const string Pages_StcPvdStatus = "Pages.StcPvdStatus";
        public const string Pages_StcPvdStatus_Create = "Pages.StcPvdStatus.Create";
        public const string Pages_StcPvdStatus_Edit = "Pages.StcPvdStatus.Edit";
        public const string Pages_StcPvdStatus_Delete = "Pages.StcPvdStatus.Delete";


        public const string Pages_ServiceCategory = "Pages.ServiceCategory";
        public const string Pages_ServiceCategory_Create = "Pages.ServiceCategory.Create";
        public const string Pages_ServiceCategory_Edit = "Pages.ServiceCategory.Edit";
        public const string Pages_ServiceCategory_Delete = "Pages.ServiceCategory.Delete";

        public const string Pages_ServiceStatus = "Pages.ServiceStatus";
        public const string Pages_ServiceStatus_Create = "Pages.ServiceStatus.Create";
        public const string Pages_ServiceStatus_Edit = "Pages.ServiceStatus.Edit";
        public const string Pages_ServiceStatus_Delete = "Pages.ServiceStatus.Delete";

        public const string Pages_ServiceType = "Pages.ServiceType";
        public const string Pages_ServiceType_Create = "Pages.ServiceType.Create";
        public const string Pages_ServiceType_Edit = "Pages.ServiceType.Edit";
        public const string Pages_ServiceType_Delete = "Pages.ServiceType.Delete";

        public const string Pages_ReviewType = "Pages.ReviewType";
        public const string Pages_ReviewType_Create = "Pages.ReviewType.Create";
        public const string Pages_ReviewType_Edit = "Pages.ReviewType.Edit";
        public const string Pages_ReviewType_Delete = "Pages.ReviewType.Delete";

        public const string Pages_PriceItemLists = "Pages.PriceItemLists";
        public const string Pages_PriceItemLists_Create = "Pages.PriceItemLists.Create";
        public const string Pages_PriceItemLists_Edit = "Pages.PriceItemLists.Edit";
        public const string Pages_PriceItemLists_Delete = "Pages.PriceItemLists.Delete";

        public const string Pages_ServiceSubCategory = "Pages.ServiceSubCategory";
        public const string Pages_ServiceSubCategory_Create = "Pages.ServiceSubCategory.Create";
        public const string Pages_ServiceSubCategory_Edit = "Pages.ServiceSubCategory.Edit";
        public const string Pages_ServiceSubCategory_Delete = "Pages.ServiceSubCategory.Delete";

        public const string Pages_report = "Pages.report";
        public const string Pages_ActivityReport = "Pages.ActivityReport";
        public const string Pages_ToDoActivityReport = "Pages.ToDoActivityReport";
        public const string Pages_OutStandingReport = "Pages.OutStandingReport";
        public const string Pages_LeadexpenseReport = "Pages.LeadexpenseReport";
        public const string Pages_Tracker_Warranty = "Pages.Tracker.Warranty";
        public const string Pages_Tracker_Warranty_Email = "Pages.Tracker.Warranty.Email";
        public const string Pages_Tracker_Warranty_Sms = "Pages.Tracker.Warranty.Sms";
        public const string Pages_Tracker_Warranty_QuickView = "Pages.Tracker.Warranty.QuickView";
        public const string Pages_Tracker_Warranty_Comment = "Pages.Tracker.Warranty.Comment";
        public const string Pages_Tracker_Warranty_Notify = "Pages.Tracker.Warranty.Notify";
        public const string Pages_Tracker_Warranty_DocUpload = "Pages.Tracker.Warranty.DocUpload";
        public const string Pages_Tracker_Warranty_ToDo = "Pages.Tracker.Warranty.ToDo";
        public const string Pages_Tracker_Warranty_Reminder = "Pages.Tracker.Warranty.Reminder";
        public const string Pages_Tracker_Warranty_Export = "Pages.Tracker.Warranty.Export";
        public const string Pages_Report_JobCost = "Pages.Report.JobCost";
        public const string Pages_Report_JobCost_Export = "Pages.Report.JobCost.Export";
        public const string Pages_Report_JobCommission = "Pages.Report.JobCommission";
        public const string Pages_Report_JobCommission_Create = "Pages.Report.JobCommission.Create";
        public const string Pages_Report_JobCommission_Edit = "Pages.Report.JobCommission.Edit";
        public const string Pages_Report_JobCommission_ShowIsVerified = "Pages.Report.JobCommission.ShowIsVerified";
        public const string Pages_Report_JobCommission_ShowIsApproved = "Pages.Report.JobCommission.ShowIsApproved";
        public const string Pages_Report_JobCost_QuickView = "Pages.Report.JobCost.QuickView";
        public const string Pages_Report_ProductSold = "Pages.Report.ProductSold";
        public const string Pages_Report_ProductSold_Export = "Pages.Report.ProductSold.Export";
        public const string Pages_Report_JobCommissionPaid = "Pages.Report.JobCommissionPaid";
        public const string Pages_Report_JobVariation = "Pages.Report.JobVariation";
        public const string Pages_Report_JobVariation_StateWiseVariationAmount = "Pages.Report.JobVariation.StateWiseVariationAmount";
        public const string Pages_Report_JobVariation_Export = "Pages.Report.JobVariation.Export";



        public const string Pages_Report_LeadAssign = "Pages.Report.LeadAssign";
        public const string Pages_Report_LeadAssign_Export = "Pages.Report.LeadAssign.Export";

        // Service Sources Permissions
        public const string Pages_ServiceSources = "Pages.ServiceSources";
        public const string Pages_ServiceSources_Create = "Pages.ServiceSources.Create";
        public const string Pages_ServiceSources_Edit = "Pages.ServiceSources.Edit";
        public const string Pages_ServiceSources_Delete = "Pages.ServiceSources.Delete";

        public const string Pages_Tenant_HeaderSearch = "Pages.Tenant.HeaderSearch";
        public const string Pages_Tenant_HeaderSearch_WholeSaleLead = "Pages.Tenant.HeaderSearch.WholeSaleLead";
        public const string Pages_Tenant_HeaderSearch_WholeSaleLead_ViewDetail = "Pages.Tenant.HeaderSearch.WholeSaleLead.ViewDetail";
        public const string Pages_Tenant_HeaderSearch_WholeSaleLead_RequestToTransfer = "Pages.Tenant.HeaderSearch.WholeSaleLead.RequestToTransfer";
        public const string Pages_Tenant_HeaderSearch_WholeSaleLead_RequestToMove = "Pages.Tenant.HeaderSearch.WholeSaleLead.RequestToMove";

        public const string Pages_QuotationTemplate = "Pages.QuotationTemplate";
        public const string Pages_QuotationTemplate_Create = "Pages.QuotationTemplate.Create";
        public const string Pages_QuotationTemplate_Edit = "Pages.QuotationTemplate.Edit";
        public const string Pages_QuotationTemplate_Delete = "Pages.QuotationTemplate.Delete";

        public const string Pages_WarehouseLocation = "Pages.WarehouseLocation";
        public const string Pages_WarehouseLocation_Edit = "Pages.WarehouseLocation.Edit";

        public const string Pages_ProductPackages = "Pages.ProductPackages";
        public const string Pages_ProductPackages_Create = "Pages.ProductPackages.Create";
        public const string Pages_ProductPackages_Edit = "Pages.ProductPackages.Edit";
        public const string Pages_ProductPackages_Delete = "Pages.ProductPackages.Delete";

        public const string Pages_LeadDetails = "Pages.LeadDetails";
        public const string Pages_LeadDetails_CallHistory = "Pages.LeadDetails.CallHistory";
        public const string Pages_LeadDetails_InvoicePaymentDelete = "Pages.LeadDetails.InvoicePaymentDelete";
        public const string Pages_LeadDetails_Calculator = "Pages.LeadDetails.Calculator";
        public const string Pages_LeadDetails_Documents = "Pages.LeadDetails.Documents";
        public const string Pages_LeadDetails_Documents_Delete = "Pages.LeadDetails.Documents.Delete";
        public const string Pages_LeadDetails_CheckActualCost = "Pages.LeadDetails.CheckActualCost";
        public const string Pages_LeadDetails_ShowActualCostDetails = "Pages.LeadDetails.ShowActualCostDetails";
        public const string Pages_LeadDetails_SkipActualCost = "Pages.LeadDetails.SkipActualCost";
        public const string Pages_LeadDetails_EditJobAddress = "Pages.LeadDetails.EditJobAddress";
        public const string Pages_LeadDetails_CheckLiveStock = "Pages.LeadDetails.CheckLiveStock";
        public const string Pages_LeadDetails_CategoryD = "Pages.LeadDetails.CategoryD";
        public const string Pages_LeadDetails_Acknowledgement = "Pages.LeadDetails.Acknowledgement";
        public const string Pages_LeadDetails_Acknowledgement_Delete = "Pages.LeadDetails.Acknowledgement.Delete";
        public const string Pages_LeadDetails_Declaration= "Pages.LeadDetails.Declaration";
        public const string Pages_LeadDetails_Declaration_Delete = "Pages.LeadDetails.Declaration.Delete";
        public const string Pages_LeadDetails_CheckWarranty = "Pages.LeadDetails.CheckWarranty";

        public const string Pages_LeadGeneration = "Pages.LeadGeneration";
        public const string Pages_LeadGeneration_MyLeadsGeneration = "Pages.LeadGeneration.MyLeadsGeneration";
        public const string Pages_LeadGeneration_Map = "Pages.LeadGeneration.Map";
        public const string Pages_LeadGeneration_Installation = "Pages.LeadGeneration.Installation";

        public const string Pages_LeadGeneration_Commission = "Pages.LeadGeneration.Commission";
        public const string Pages_LeadGeneration_Commission_Export = "Pages.LeadGeneration.Commission.Export";
        public const string Pages_LeadGeneration_Commission_Payment = "Pages.LeadGeneration.Commission.Payment";
        public const string Pages_LeadGeneration_Commission_QuickView = "Pages.LeadGeneration.Commission.QuickView";

        #region QS App Permiaaion
        public const string QuickStockApp = "QuickStockApp";
        public const string QuickStockApp_StockOrder = "QuickStockApp.StockOrder";
        public const string QuickStockApp_StockTransfer = "QuickStockApp.StockTransfer";
        public const string QuickStockApp_RetailJob = "QuickStockApp.RetailJob";
        public const string QuickStockApp_WholesaleInvoice = "QuickStockApp.WholesaleInvoice";
        public const string QuickStockApp_RevertItems = "QuickStockApp.RevertItems";
        public const string QuickStockApp_FaultyItems = "QuickStockApp.FaultyItems";
        public const string QuickStockApp_LiveStockQty = "QuickStockApp.LiveStockQty";
        public const string QuickStockApp_StockAudit = "QuickStockApp.StockAudit";
        public const string QuickStockApp_Reports = "QuickStockApp.Reports";
        public const string QuickStockApp_Jobs = "QuickStockApp.Jobs";
        public const string QuickStockApp_Availability = "QuickStockApp.Availability";
        #endregion

        public const string Pages_CallFlowQueues = "Pages.CallFlowQueues";
        public const string Pages_CallFlowQueues_Create = "Pages.CallFlowQueues.Create";
        public const string Pages_CallFlowQueues_Edit = "Pages.CallFlowQueues.Edit";
        public const string Pages_CallFlowQueues_Delete = "Pages.CallFlowQueues.Delete";
        public const string Pages_CallFlowQueues_View = "Pages.CallFlowQueues.View";




        public const string Pages_LeadGeneration_MyLeadsGeneration_SMS = "Pages.LeadGeneration.MyLeadsGeneration.Sms";
        public const string Pages_LeadGeneration_MyLeadsGeneration_Email = "Pages.LeadGeneration.MyLeadsGeneration.Email";
        public const string Pages_LeadGeneration_MyLeadsGeneration_Appointment = "Pages.LeadGeneration.MyLeadsGeneration.Appointment";
        public const string Pages_LeadGeneration_MyLeadsGeneration_Comment = "Pages.LeadGeneration.MyLeadsGeneration.Comment";
        public const string Pages_LeadGeneration_MyLeadsGeneration_ToDo = "Pages.LeadGeneration.MyLeadsGeneration.ToDo";
        public const string Pages_LeadGeneration_MyLeadsGeneration_Reminder = "Pages.LeadGeneration.MyLeadsGeneration.Reminder";
        public const string Pages_LeadGeneration_MyLeadsGeneration_Notify = "Pages.LeadGeneration.MyLeadsGeneration.Notify";

        public const string Pages_Tenant_datavault_ServiceDocumentType = "Pages.Tenant.datavault.ServiceDocumentType";
        public const string Pages_Tenant_datavault_ServiceDocumentType_Create = "Pages.Tenant.datavault.ServiceDocumentType.Create";
        public const string Pages_Tenant_datavault_ServiceDocumentType_Edit = "Pages.Tenant.datavault.ServiceDocumentType.Edit";
        public const string Pages_Tenant_datavault_ServiceDocumentType_Delete = "Pages.Tenant.datavault.ServiceDocumentType.Delete";

        public const string Pages_WarrantyClaim = "Pages.Service.WarrantyClaim";
        public const string Pages_WarrantyClaim_AddServiceInstaller = "Pages.Service.WarrantyClaim.AddServiceInstaller";
        public const string Pages_WarrantyClaim_Edit = "Pages.Service.WarrantyClaim.Edit";
        public const string Pages_WarrantyClaim_SMS = "Pages.Service.WarrantyClaim.SMS";
        public const string Pages_WarrantyClaim_Email = "Pages.Service.WarrantyClaim.Email";
        public const string Pages_WarrantyClaim_Notify = "Pages.Service.WarrantyClaim.Notify";
        public const string Pages_WarrantyClaim_QuickView = "Pages.Service.WarrantyClaim.QuickView";
        public const string Pages_WarrantyClaim_ToDo = "Pages.Service.WarrantyClaim.ToDo";
        public const string Pages_WarrantyClaim_Reminder = "Pages.Service.WarrantyClaim.Reminder";
        public const string Pages_WarrantyClaim_Comment = "Pages.Service.WarrantyClaim.Comment";
        public const string Pages_WarrantyClaim_AssignUser = "Pages.Service.WarrantyClaim.AssignUser";
        public const string Pages_WarrantyClaim_Delete = "Pages.Service.WarrantyClaim.Delete";
        public const string Pages_WarrantyClaim_Export = "Pages.Service.WarrantyClaim.Export";


        public const string Pages_Report_EmpJobCost = "Pages.Report.EmpJobCost";
        public const string Pages_Report_EmpJobCost_Export = "Pages.Report.EmpJobCost.Export";
        public const string Pages_Report_EmpJobCost_QuickView = "Pages.Report.EmpJobCost.QuickView";

        public const string Pages_WarrantyClaim_AddInvoice = "Pages.Service.WarrantyClaim.AddInvoice";
        public const string Pages_WarrantyClaim_AddNotes = "Pages.Service.WarrantyClaim.AddNotes";
        public const string Pages_WarrantyClaim_PickupItem = "Pages.Service.WarrantyClaim.PickupItem";

        public const string Pages_ServiceInvoice = "Pages.Service.ServiceInvoice";

        public const string Pages_Report_SmsCount = "Pages.Report.SmsCount";
        public const string Pages_Report_SmsCount_Export = "Pages.Report.SmsCount.Export";


        public const string Pages_ChcekApplication = "Pages.CheckApplication";
        public const string Pages_CheckApplication_Edit = "Pages.CheckApplication.Edit";
        public const string Pages_CheckApplication_Create = "Pages.CheckApplication.Create";
        public const string Pages_CheckApplication_Delete = "Pages.CheckApplication.Delete";
        public const string Pages_CheckApplication_Export = "Pages.CheckApplication.Export";

        public const string Pages_CheckActive = "Pages.CheckActive";
        public const string Pages_CheckActive_Edit = "Pages.CheckActive.Edit";
        public const string Pages_CheckActive_Create = "Pages.CheckActive.Create";
        public const string Pages_CheckActive_Delete = "Pages.CheckActive.Delete";
        public const string Pages_CheckActive_Export = "Pages.CheckActive.Export";

        public const string Pages_Report_JobCostMonthWise = "Pages.Report.JobCostMonthWise";
        public const string Pages_Report_JobCostMonthWise_Export = "Pages.Report.JobCostMonthWise.Export";
        public const string Pages_Report_JobCostMonthWise_QuickView = "Pages.Report.JobCostMonthWise.QuickView";
        public const string Pages_Report_JobCostMonthWise_EnableCopyPaste = "Pages.Report.JobCostMonthWise.EnableCopyPaste";
              

        #region Call History
        public const string Pages_CallHistory = "Pages.CallHistory";

        public const string Pages_CallHistory_UserCallHistory = "Pages.CallHistory.UserCallHistory";
        public const string Pages_CallHistory_UserCallHistory_Export = "Pages.CallHistory.UserCallHistory.Export";
        public const string Pages_CallHistory_UserCallHistory_Detail = "Pages.CallHistory.UserCallHistory.Detail";
        public const string Pages_CallHistory_UserCallHistory_ShowMobileNo = "Pages.CallHistory.UserCallHistory.ShowMobileNo";

        public const string Pages_CallHistory_StateWiseCallHistory = "Pages.CallHistory.StateWiseCallHistory";
        public const string Pages_CallHistory_StateWiseCallHistory_Import = "Pages.CallHistory.StateWiseCallHistory.Import";
        public const string Pages_CallHistory_StateWiseCallHistory_Export = "Pages.CallHistory.StateWiseCallHistory.Export";
        public const string Pages_CallHistory_StateWiseCallHistory_HeaderDetail = "Pages.CallHistory.StateWiseCallHistory.HeaderDetail";
        public const string Pages_CallHistory_StateWiseCallHistory_HeaderDetail_Export = "Pages.CallHistory.StateWiseCallHistory.HeaderDetail.Export";

        public const string Pages_CallHistory_CallFlowQueueCallHistory = "Pages.CallHistory.CallFlowQueueCallHistory";
        public const string Pages_CallHistory_CallFlowQueueCallHistory_Export = "Pages.CallHistory.CallFlowQueueCallHistory.Export";
        public const string Pages_CallHistory_CallFlowQueueCallHistory_Detail = "Pages.CallHistory.CallFlowQueueCallHistory.Detail";
        public const string Pages_CallHistory_CallFlowQueueCallHistory_Detail_Export = "Pages.CallHistory.CallFlowQueueCallHistory.Detail.Export";
        public const string Pages_CallHistory_CallFlowQueueCallHistory_Detail_UserCallDetails = "Pages.CallHistory.CallFlowQueueCallHistory.Detail.UserCallDetails";
        public const string Pages_CallHistory_CallFlowQueueCallHistory_Detail_UserCallDetails_Export = "Pages.CallHistory.CallFlowQueueCallHistory.Detail.UserCallDetails.Export";
        public const string Pages_CallHistory_CallFlowQueueCallHistory_Detail_UserCallDetails_ShowMobileNo = "Pages.CallHistory.CallFlowQueueCallHistory.Detail.UserCallDetails.ShowMobileNo";

        public const string Pages_CallHistory_CallFlowQueueWeeklyCallHistory = "Pages.CallHistory.CallFlowQueueWeeklyCallHistory";

        public const string Pages_CallHistory_CallFlowQueueUserWiseCallHistory = "Pages.CallHistory.CallFlowQueueUserWiseCallHistory";
        public const string Pages_CallHistory_CallFlowQueueUserWiseCallHistory_Export = "Pages.CallHistory.CallFlowQueueUserWiseCallHistory.Export";
        public const string Pages_CallHistory_CallFlowQueueUserWiseCallHistory_Detail = "Pages.CallHistory.CallFlowQueueUserWiseCallHistory.Detail";
        public const string Pages_CallHistory_CallFlowQueueUserWiseCallHistory_Detail_Export = "Pages.CallHistory.CallFlowQueueUserWiseCallHistory.Detail.Export";

        #endregion


        #region WholeSale

        public const string Pages_Tenant_WholeSale = "Pages.Tenant.WholeSale";

        public const string Pages_Tenant_WholeSale_Dashboard = "Pages.Tenant.WholeSale.Dashboard";

        public const string Pages_Tenant_WholeSale_DataVaults = "Pages.Tenant.WholeSale.DataVaults";

        public const string Pages_Tenant_WholeSale_DataVaults_SmsTemplates = "Pages.Tenant.WholeSale.DataVaults.SmsTemplates";
        public const string Pages_Tenant_WholeSale_DataVaults_SmsTemplates_Create = "Pages.Tenant.WholeSale.DataVaults.SmsTemplates.Create";
        public const string Pages_Tenant_WholeSale_DataVaults_SmsTemplates_Edit = "Pages.Tenant.WholeSale.DataVaults.SmsTemplates.Edit";
        public const string Pages_Tenant_WholeSale_DataVaults_SmsTemplates_Delete = "Pages.Tenant.WholeSale.DataVaults.SmsTemplates.Delete";
        public const string Pages_Tenant_WholeSale_DataVaults_SmsTemplates_View = "Pages.Tenant.WholeSale.DataVaults.SmsTemplates.View";
        public const string Pages_Tenant_WholeSale_DataVaults_SmsTemplates_Export = "Pages.Tenant.WholeSale.DataVaults.SmsTemplates.Export";




        public const string Pages_Tenant_WholeSale_DataVaults_EmailTemplates = "Pages.Tenant.WholeSale.DataVaults.EmailTemplates";
        public const string Pages_Tenant_WholeSale_DataVaults_EmailTemplates_Create = "Pages.Tenant.WholeSale.DataVaults.EmailTemplates.Create";
        public const string Pages_Tenant_WholeSale_DataVaults_EmailTemplates_Edit = "Pages.Tenant.WholeSale.DataVaults.EmailTemplates.Edit";
        public const string Pages_Tenant_WholeSale_DataVaults_EmailTemplates_Delete = "Pages.Tenant.WholeSale.DataVaults.EmailTemplates.Delete";

        public const string Pages_Tenant_WholeSale_DataVaults_WholeSaleStatus = "Pages.Tenant.WholeSale.DataVaults.WholeSaleStatus";
        public const string Pages_Tenant_WholeSale_DataVaults_WholeSaleStatus_Create = "Pages.Tenant.WholeSale.DataVaults.WholeSaleStatus.Create";
        public const string Pages_Tenant_WholeSale_DataVaults_WholeSaleStatus_Edit = "Pages.Tenant.WholeSale.DataVaults.WholeSaleStatus.Edit";
        public const string Pages_Tenant_WholeSale_DataVaults_WholeSaleStatus_Delete = "Pages.Tenant.WholeSale.DataVaults.WholeSaleStatus.Delete";

        public const string Pages_Tenant_WholeSale_DataVaults_DocumentType = "Pages.Tenant.WholeSale.DataVaults.DocumentType";
        public const string Pages_Tenant_WholeSale_DataVaults_DocumentType_Create = "Pages.Tenant.WholeSale.DataVaults.DocumentType.Create";
        public const string Pages_Tenant_WholeSale_DataVaults_DocumentType_Edit = "Pages.Tenant.WholeSale.DataVaults.DocumentType.Edit";
        public const string Pages_Tenant_WholeSale_DataVaults_DocumentType_Delete = "Pages.Tenant.WholeSale.DataVaults.DocumentType.Delete";

        public const string Pages_WholeSale_Leads = "Pages.WholeSale.Leads";
        public const string Pages_WholeSale_Leads_Create = "Pages.WholeSale.Leads.Create";
        public const string Pages_WholeSale_Leads_Edit = "Pages.WholeSale.Leads.Edit";
        public const string Pages_WholeSale_Leads_View = "Pages.WholeSale.Leads.CreditApplicationView";
        public const string Pages_WholeSale_Leads_Delete = "Pages.WholeSale.Leads.Delete";
        public const string Pages_WholeSale_Leads_Export = "Pages.WholeSale.Leads.Export";
        public const string Pages_WholeSale_Leads_SMS = "Pages.WholeSale.Leads.SMS";
        public const string Pages_WholeSale_Leads_Email = "Pages.WholeSale.Leads.Email";
        public const string Pages_WholeSale_Leads_Notify = "Pages.WholeSale.Leads.Notify";
        public const string Pages_WholeSale_Leads_Comment = "Pages.WholeSale.Leads.Comment";
        public const string Pages_WholeSale_Leads_ToDo = "Pages.WholeSale.Leads.ToDo";
        public const string Pages_WholeSale_Leads_Reminder = "Pages.WholeSale.Leads.Reminder";
        public const string Pages_WholeSale_Leads_UploadDoc = "Pages.WholeSale.Leads.UploadDoc";
        public const string Pages_WholeSale_Leads_AddToUserRequest = "Pages.WholeSale.Leads.AddToUserRequest";
        public const string Pages_WholeSale_Leads_RequestToMove = "Pages.WholeSale.Leads.RequestToMove";

        public const string Pages_WholeSale_PromotionGroup = "Pages.WholeSale.PromotionGroup";
        public const string Pages_WholeSale_PromotionUsers = "Pages.WholeSale.PromotionUsers";

        public const string Pages_WholeSale_Promotions = "Pages.WholeSale.Promotions";
        public const string Pages_WholeSale_Promotions_Create = "Pages.WholeSale.Promotions.Create";
        public const string Pages_WholeSale_Promotions_View = "Pages.WholeSale.Promotions.View";
        //public const string Pages_WholeSale_Promotions_Edit = "Pages.Promotions.Edit";
        public const string Pages_WholeSale_Promotions_Delete = "Pages.WholeSale.Promotions.Delete";
        public const string Pages_WholeSale_Promotions_ExportExcel = "Pages.WholeSale.Promotions.ExportExcel";

        public const string Pages_WholeSale_PromotionUsers_Excel = "Pages.WholeSale.PromotionUsers.Excel";
        public const string Pages_WholeSale_PromotionUsers_View = "Pages.WholeSale.PromotionUsers.View";
        public const string Pages_WholeSale_PromotionUsers_Excel_MobileEmail = "Pages.WholeSale.PromotionUsers.Excel.MobileEmail";

        public const string Pages_WholeSale_Invoice = "Pages.WholeSale.Invoice";

        public const string Pages_WholeSale_Action = "Pages.WholeSale.Action";
        public const string Pages_WholeSale_Action_AssignToManager = "Pages.WholeSale.Action.AssignToManager";
        public const string Pages_WholeSale_Action_AssignToSalesRep = "Pages.WholeSale.Action.AssignToSalesRep";
        public const string Pages_WholeSale_Action_Delete = "Pages.WholeSale.Action.Delete";

        public const string Pages_Tenant_HeaderSearch_ViewDetail = "Pages.Tenant.HeaderSearch.ViewDetail";

        public const string Pages_Report_LeadSold = "Pages.Report.LeadSold";
        public const string Pages_Report_LeadSold_Export = "Pages.Report.LeadSold.Export";

        public const string Pages_MyLeads_EnableCopyPaste = "Pages.MyLeads.EnableCopyPaste";
        public const string Pages_ManageLeads_EnableCopyPaste = "Pages.ManageLeads.EnableCopyPaste";
        public const string Pages_Lead_Duplicate_EnableCopyPaste = "Pages.Lead.Duplicate.EnableCopyPaste";
        public const string Pages_LeadTracker_EnableCopyPaste = "Pages.LeadTracker.EnableCopyPaste";
        public const string Pages_Leads_Calcel_EnableCopyPaste = "Pages.Lead.Calcel.EnableCopyPaste";
        public const string Pages_Leads_Rejects_EnableCopyPaste = "Pages.Lead.Rejects.EnableCopyPaste";
        public const string Pages_Lead_Closed_EnableCopyPaste = "Pages.Lead.Close.EnableCopyPaste";
        public const string Pages_Promotions_EnableCopyPaste = "Pages.Promotions.EnableCopyPaste";
        public const string Pages_PromotionUsers_EnableCopyPaste = "Pages.PromotionUsers.EnableCopyPaste";
        public const string Pages_ApplicationTracker_EnableCopyPaste = "Pages.Tracker.ApplicationTracker.EnableCopyPaste";
        public const string Pages_EssentialTracker_EnableCopyPaste = "Pages.Tracker.EssentialTracker.EnableCopyPaste";
        public const string Pages_FreebiesTracker_EnableCopyPaste = "Pages.Tracker.FreebiesTracker.EnableCopyPaste";
        public const string Pages_FinanceTracker_EnableCopyPaste = "Pages.Tracker.FinanceTracker.EnableCopyPaste";
        public const string Pages_RefundTracker_EnableCopyPaste = "Pages.Tracker.RefundTracker.EnableCopyPaste";
        public const string Pages_Tracker_Referral_EnableCopyPaste = "Pages.Tracker.Referral.EnableCopyPaste";
        public const string Pages_JobActiveTracker_EnableCopyPaste = "Pages.Tracker.JobActiveTracker.EnableCopyPaste";
        public const string Pages_Gridconnectiontracker_EnableCopyPaste = "Pages.Tracker.Gridconnectiontracker.EnableCopyPaste";
        public const string Pages_STCTracker_EnableCopyPaste = "Pages.Tracker.STCTracker.EnableCopyPaste";
        public const string Pages_Tracker_Warranty_EnableCopyPaste = "Pages.Tracker.Warranty.EnableCopyPaste";
        public const string Pages_HoldJobTracker_EnableCopyPaste = "Pages.HoldJobTracker.EnableCopyPaste";
        public const string Pages_JobGrid_EnableCopyPaste = "Pages.JobGrid.EnableCopyPaste";
        public const string Pages_InvoiceIssuedTracker_EnableCopyPaste = "Pages.Tracker.InvoiceIssuedTracker.EnableCopyPaste";
        public const string Pages_InvoiceTracker_EnableCopyPaste = "Pages.Tracker.InvoiceTracker.EnableCopyPaste";
        public const string Pages_InvoicePayWay_EnableCopyPaste = "Pages.InvoicePayWay.EnableCopyPaste";
        public const string Pages_MyInstaller_EnableCopyPaste = "Pages.MyInstaller.EnableCopyPaste";
        public const string Pages_PendingInstallation_EnableCopyPaste = "Pages.PendingInstallation.EnableCopyPaste";
        public const string Pages_JobBooking_EnableCopyPaste = "Pages.JobBooking.EnableCopyPaste";
        public const string Pages_Installer_New_EnableCopyPaste = "Pages.Installer.New.EnableCopyPaste";
        public const string Pages_Installer_Invoice_EnableCopyPaste = "Pages.Installer.Invoice.EnableCopyPaste";
        public const string Pages_ReadyToPay_EnableCopyPaste = "Pages.ReadyToPay.EnableCopyPaste";
        public const string Pages_LeadGeneration_MyLeadsGeneration_EnableCopyPaste = "Pages.LeadGeneration.MyLeadsGeneration.EnableCopyPaste";
        public const string Pages_LeadGeneration_Installation_EnableCopyPaste = "Pages.LeadGeneration.Installation.EnableCopyPaste";
        public const string Pages_LeadGeneration_Commission_EnableCopyPaste = "Pages.LeadGeneration.Commission.EnableCopyPaste";
        public const string Pages_Lead_ManageService_EnableCopyPaste = "Pages.Lead.ManageService.EnableCopyPaste";
        public const string Pages_MyService_EnableCopyPaste = "Pages.Service.MyService.EnableCopyPaste";
        public const string Pages_WarrantyClaim_EnableCopyPaste = "Pages.Service.WarrantyClaim.EnableCopyPaste";
        public const string Pages_ServiceInvoice_EnableCopyPaste = "Pages.Service.ServiceInvoice.EnableCopyPaste";
        public const string Pages_Review_EnableCopyPaste = "Pages.Review.EnableCopyPaste";
        public const string Pages_Sms_EnableCopyPaste = "Pages.Sms.EnableCopyPaste";
        public const string Pages_ActivityReport_EnableCopyPaste = "Pages.ActivityReport.EnableCopyPaste";
        public const string Pages_ToDoActivityReport_EnableCopyPaste = "Pages.ToDoActivityReport.EnableCopyPaste";
        public const string Pages_LeadexpenseReport_EnableCopyPaste = "Pages.LeadexpenseReport.EnableCopyPaste";
        public const string Pages_OutStandingReport_EnableCopyPaste = "Pages.OutStandingReport.EnableCopyPaste";
        public const string Pages_Report_JobCost_EnableCopyPaste = "Pages.Report.JobCost.EnableCopyPaste";
        public const string Pages_Report_LeadAssign_EnableCopyPaste = "Pages.Report.LeadAssign.EnableCopyPaste";
        public const string Pages_Report_LeadSold_EnableCopyPaste = "Pages.Report.LeadSold.EnableCopyPaste";
        public const string Pages_Report_EmpJobCost_EnableCopyPaste = "Pages.Report.EmpJobCost.EnableCopyPaste";
        public const string Pages_CallHistory_UserCallHistory_EnableCopyPaste = "Pages.CallHistory.UserCallHistory.EnableCopyPaste";
        public const string Pages_CallHistory_StateWiseCallHistory_EnableCopyPaste = "Pages.CallHistory.StateWiseCallHistory.EnableCopyPaste";
        public const string Pages_CallHistory_CallFlowQueueCallHistory_EnableCopyPaste = "Pages.CallHistory.CallFlowQueueCallHistory.EnableCopyPaste";

        public const string Pages_WholeSale_Leads_EnableCopyPaste = "Pages.WholeSale.Leads.EnableCopyPaste";
        public const string Pages_WholeSale_Promotions_EnableCopyPaste = "Pages.WholeSale.Promotions.EnableCopyPaste";
        public const string Pages_WholeSale_PromotionUsers_EnableCopyPaste = "Pages.WholeSale.PromotionUsers.EnableCopyPaste";
        public const string Pages_WholeSale_Invoice_EnableCopyPaste = "Pages.WholeSale.Invoice.EnableCopyPaste";

        public const string Pages_Tenant_Dashboard_Sales = "Pages.Tenant.Dashboard.Sales";
        public const string Pages_Tenant_Dashboard_Invoice = "Pages.Tenant.Dashboard.Invoice";
        public const string Pages_Tenant_Dashboard_Service = "Pages.Tenant.Dashboard.Service";
        public const string Pages_Tenant_Dashboard_Dashboard2 = "Pages.Tenant.Dashboard.Dashboard2";

        public const string Pages_CommissionRanges = "Pages.CommissionRanges";
        public const string Pages_CommissionRanges_Create = "Pages.CommissionRanges.Create";
        public const string Pages_CommissionRanges_Edit = "Pages.CommissionRanges.Edit";
        public const string Pages_CommissionRanges_Delete = "Pages.CommissionRanges.Delete";

        public const string Pages_DashboardMessage = "Pages.DashboardMessage";
        public const string Pages_DashboardMessage_Create = "Pages.DashboardMessage.Create";
        public const string Pages_DashboardMessage_Edit = "Pages.DashboardMessage.Edit";
        public const string Pages_DashboardMessage_Delete = "Pages.DashboardMessage.Delete";

        public const string Pages_Tenant_DataVaults_InstallationCost_CategoryInstallationItemList = "Pages.Tenant.DataVaults.InstallationCost.CategoryInstallationItemList";
        public const string Pages_Tenant_DataVaults_InstallationCost_CategoryInstallationItemList_View = "Pages.Tenant.DataVaults.InstallationCost.CategoryInstallationItemList.View";
        public const string Pages_Tenant_DataVaults_InstallationCost_CategoryInstallationItemList_Create = "Pages.Tenant.DataVaults.InstallationCost.CategoryInstallationItemList.Create";
        public const string Pages_Tenant_DataVaults_InstallationCost_CategoryInstallationItemList_Edit = "Pages.Tenant.DataVaults.InstallationCost.CategoryInstallationItemList.Edit";
        public const string Pages_Tenant_DataVaults_InstallationCost_CategoryInstallationItemList_Delete = "Pages.Tenant.DataVaults.InstallationCost.CategoryInstallationItemList.Delete";
        public const string Pages_Tenant_DataVaults_InstallationCost_CategoryInstallationItemList_LastMonthEdit = "Pages.Tenant.DataVaults.InstallationCost.CategoryInstallationItemList.LastMonthEdit";


        public const string Pages_Tenant_WholeSale_ECommerce_ActivityLog = "Pages.Tenant.WholeSale.ECommerce.ActivityLog";
        public const string Pages_Tenant_WholeSale_ECommerce_ActivityLog_Detail = "Pages.Tenant.WholeSale.ECommerce.ActivityLog.Detail";
        public const string Pages_Tenant_WholeSale_ECommerce_ActivityLog_History = "Pages.Tenant.WholeSale.ECommerce.ActivityLog.History";




        public const string Pages_Tenant_WholeSale_ECommerce = "Pages.Tenant.WholeSale.ECommerce";
        public const string Pages_Tenant_WholeSale_ECommerce_Slider = "Pages.Tenant.WholeSale.ECommerce.Slider";
        public const string Pages_Tenant_WholeSale_ECommerce_Slider_Create = "Pages.Tenant.WholeSale.ECommerce.Slider.Create";
        public const string Pages_Tenant_WholeSale_ECommerce_Slider_Edit = "Pages.Tenant.WholeSale.ECommerce.Slider.Edit";
        public const string Pages_Tenant_WholeSale_ECommerce_Slider_Delete = "Pages.Tenant.WholeSale.ECommerce.Slider.Delete";
        public const string Pages_Tenant_WholeSale_ECommerce_Slider_View = "Pages.Tenant.WholeSale.ECommerce.Slider.View";

        public const string Pages_Tenant_WholeSale_ECommerce_Products = "Pages.Tenant.WholeSale.ECommerce.Products";
        public const string Pages_Tenant_WholeSale_ECommerce_Products_Create = "Pages.Tenant.WholeSale.ECommerce.Products.Create";
        public const string Pages_Tenant_WholeSale_ECommerce_Products_Edit = "Pages.Tenant.WholeSale.ECommerce.Products.Edit";
        public const string Pages_Tenant_WholeSale_ECommerce_Products_Delete = "Pages.Tenant.WholeSale.ECommerce.Products.Delete";
        public const string Pages_Tenant_WholeSale_ECommerce_Products_View = "Pages.Tenant.WholeSale.ECommerce.Products.View";
        public const string Pages_Tenant_WholeSale_ECommerce_Products_Documents = "Pages.Tenant.WholeSale.ECommerce.Products.Documents";
        public const string Pages_Tenant_WholeSale_ECommerce_Products_Images = "Pages.Tenant.WholeSale.ECommerce.Products.Images";
        public const string Pages_Tenant_WholeSale_ECommerce_Products_Imagesdownload = "Pages.Tenant.WholeSale.ECommerce.Products.Imagesdownload";
        public const string Pages_Tenant_WholeSale_ECommerce_Products_Imagesdelete = "Pages.Tenant.WholeSale.ECommerce.Products.Imagesdelete";
        public const string Pages_Tenant_WholeSale_ECommerce_Products_Documents_Delete = "Pages.Tenant.WholeSale.ECommerce.Products.Documents.Delete";

        public const string Pages_Tenant_WholeSale_Credit = "Pages.Tenant.WholeSale.Credit";
        public const string Pages_Tenant_WholeSale_Credit_CreditApplication = "Pages.Tenant.WholeSale.Credit.CreditApplication";
        public const string Pages_Tenant_WholeSale_Credit_CreditApplication_View = "Pages.Tenant.WholeSale.Credit.CreditApplication.View";
        public const string Pages_Tenant_WholeSale_Credit_CreditApplication_SMS = "Pages.Tenant.WholeSale.Credit.CreditApplication.SMS";
        public const string Pages_Tenant_WholeSale_Credit_CreditApplication_Email = "Pages.Tenant.WholeSale.Credit.CreditApplication.Email";
        public const string Pages_Tenant_WholeSale_Credit_CreditApplication_Comments = "Pages.Tenant.WholeSale.Credit.CreditApplication.Comments";
        public const string Pages_Tenant_WholeSale_Credit_CreditApplication_Reminder = "Pages.Tenant.WholeSale.Credit.CreditApplication.Reminder";
        public const string Pages_Tenant_WholeSale_Credit_CreditApplication_AddNotes = "Pages.Tenant.WholeSale.Credit.CreditApplication.AddNotes";
        public const string Pages_Tenant_WholeSale_Credit_CreditApplication_Delete = "Pages.Tenant.WholeSale.Credit.CreditApplication.Delete";
        public const string Pages_Tenant_WholeSale_Credit_CreditApplication_Approve = "Pages.Tenant.WholeSale.Credit.CreditApplication.Approve";


        public const string Pages_Tenant_DataVaults_InstallationCost_PostCodeCost = "Pages.Tenant.DataVaults.InstallationCost.PostCodeCost";
        public const string Pages_Tenant_DataVaults_InstallationCost_PostCodeCost_Create = "Pages.Tenant.DataVaults.InstallationCost.PostCodeCost.Create";
        public const string Pages_Tenant_DataVaults_InstallationCost_PostCodeCost_Edit = "Pages.Tenant.DataVaults.InstallationCost.PostCodeCost.Edit";
        public const string Pages_Tenant_DataVaults_InstallationCost_PostCodeCost_Delete = "Pages.Tenant.DataVaults.InstallationCost.PostCodeCost.Delete";
        public const string Pages_Tenant_DataVaults_InstallationCost_PostCodeCost_LastMonthEdit = "Pages.Tenant.DataVaults.InstallationCost.PostCodeCost.LastMonthEdit";
        public const string Pages_Tenant_DataVaults_InstallationCost_PostCodeCost_View = "Pages.Tenant.DataVaults.InstallationCost.PostCodeCost.View";

        public const string Pages_Tenant_WholeSale_ECommerce_BrandingPartner = "Pages.Tenant.WholeSale.ECommerce.BrandingPartner";
        public const string Pages_Tenant_WholeSale_ECommerce_BrandingPartner_Create = "Pages.Tenant.WholeSale.ECommerce.BrandingPartner.Create";
        public const string Pages_Tenant_WholeSale_ECommerce_BrandingPartner_Edit = "Pages.Tenant.WholeSale.ECommerce.BrandingPartner.Edit";
        public const string Pages_Tenant_WholeSale_ECommerce_BrandingPartner_Delete = "Pages.Tenant.WholeSale.ECommerce.BrandingPartner.Delete";
        public const string Pages_Tenant_WholeSale_ECommerce_BrandingPartner_View = "Pages.Tenant.WholeSale.ECommerce.BrandingPartner.View";

        public const string Pages_Tenant_WholeSale_ECommerce_SpecialOffer = "Pages.Tenant.WholeSale.ECommerce.SpecialOffer";
        public const string Pages_Tenant_WholeSale_ECommerce_SpecialOffer_Create = "Pages.Tenant.WholeSale.ECommerce.SpecialOffer.Create";
        public const string Pages_Tenant_WholeSale_ECommerce_SpecialOffer_Edit = "Pages.Tenant.WholeSale.ECommerce.SpecialOffer.Edit";
        public const string Pages_Tenant_WholeSale_ECommerce_SpecialOffer_Delete = "Pages.Tenant.WholeSale.ECommerce.SpecialOffer.Delete";
        public const string Pages_Tenant_WholeSale_ECommerce_SpecialOffer_View = "Pages.Tenant.WholeSale.ECommerce.SpecialOffer.View";

        public const string Pages_Tenant_WholeSale_ECommerce_UserRequest = "Pages.Tenant.WholeSale.ECommerce.UserRequest";
        public const string Pages_Tenant_WholeSale_ECommerce_UserRequest_Create = "Pages.Tenant.WholeSale.ECommerce.UserRequest.Create";
        public const string Pages_Tenant_WholeSale_ECommerce_UserRequest_Edit = "Pages.Tenant.WholeSale.ECommerce.UserRequest.Edit";
        public const string Pages_Tenant_WholeSale_ECommerce_UserRequest_Delete = "Pages.Tenant.WholeSale.ECommerce.UserRequest.Delete";
        public const string Pages_Tenant_WholeSale_ECommerce_UserRequest_Approve = "Pages.Tenant.WholeSale.ECommerce.UserRequest.Approve";


        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceSolarPackages = "Pages.Tenant.WholeSale.ECommerce.EcommerceSolarPackages";
        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceSolarPackages_Create = "Pages.Tenant.WholeSale.ECommerce.EcommerceSolarPackages.Create";
        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceSolarPackages_Edit = "Pages.Tenant.WholeSale.ECommerce.EcommerceSolarPackages.Edit";
        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceSolarPackages_Delete = "Pages.Tenant.WholeSale.ECommerce.EcommerceSolarPackages.Delete";
        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceSolarPackages_View = "Pages.Tenant.WholeSale.ECommerce.EcommerceSolarPackages.View";

        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceContactUs = "Pages.Tenant.WholeSale.ECommerce.EcommerceContactUs";
        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceContactUs_Edit = "Pages.Tenant.WholeSale.ECommerce.EcommerceContactUs.Edit";
        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceContactUs_Delete = "Pages.Tenant.WholeSale.ECommerce.EcommerceContactUs.Delete";
        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceContactUs_SMS = "Pages.Tenant.WholeSale.ECommerce.EcommerceContactUs.SMS";
        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceContactUs_Email = "Pages.Tenant.WholeSale.ECommerce.EcommerceContactUs.Email";
        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceContactUs_AddtoLead = "Pages.Tenant.WholeSale.ECommerce.EcommerceContactUs.AddtoLead";

        public const string Pages_Report_UserDetails = "Pages.Report.UserDetails";
        public const string Pages_Report_UserDetails_Export = "Pages.Report.UserDetails.Export";

        public const string Pages_Report_Comparison = "Pages.Report.Comparison";
        public const string Pages_Report_LeadComparison = "Pages.Report.LeadComparison";
        public const string Pages_Report_MonthlyComparison = "Pages.Report.MonthlyComparison";

        public const string Pages_Report_ReviewReport = "Pages.Report.ReviewReport";
        public const string Pages_Report_ProgressReport = "Pages.Report.ProgressReport";
        public const string Pages_Report_ProgressReport_Export = "Pages.Report.ProgressReport.Export";
        public const string Pages_Report_EmployeeProfit = "Pages.Report.EmployeeProfit";

        public const string Pages_3rdPartyLeadTracker = "Pages.3rdPartyLeadTracker";
        public const string Pages_3rdPartyLeadTracker_Create = "Pages.3rdPartyLeadTracker.Create";
        public const string Pages_3rdPartyLeadTracker_Export = "Pages.3rdPartyLeadTracker.Export";

        public const string Pages_Tenant_datavault_ActivityLog = "Pages.Tenant.datavault.ActivityLog";
        public const string Pages_Tenant_datavault_ActivityLog_Detail = "Pages.Tenant.datavault.ActivityLog.Detail";
        public const string Pages_Tenant_datavault_ActivityLog_History = "Pages.Tenant.datavault.ActivityLog.History";

        public const string Pages_Report_InstallerInvoicePayment = "Pages.Report.InstallerInvoicePayment";
        public const string Pages_Report_InstallerInvoicePayment_Export = "Pages.Report.InstallerInvoicePayment.Export";

        public const string Pages_Report_InstallerWiseInvoicePayment = "Pages.Report.InstallerWiseInvoicePayment";
        public const string Pages_Report_InstallerJobWiseInvoicePayment = "Pages.Report.InstallerJobWiseInvoicePayment";
        public const string Pages_Report_InstallerWiseInvoicePayment_Export = "Pages.Report.InstallerWiseInvoicePayment.Export";

        public const string Pages_TransportCost = "Pages.TransportCost";
        public const string Pages_TransportCost_Create = "Pages.TransportCost.Create";
        public const string Pages_TransportCost_Edit = "Pages.TransportCost.Edit";
        public const string Pages_TransportCost_Delete = "Pages.TransportCost.Delete";


        public const string Pages_Tenant_WholeSale_ECommerce_Subscribe = "Pages.Tenant.WholeSale.ECommerce.Subscribe";
        public const string Pages_Tenant_WholeSale_ECommerce_Subscribe_SendEmail = "Pages.Tenant.WholeSale.ECommerce.Subscribe.SendEmail";


        #endregion

        public const string Pages_ManageLeads_ShowEmailPhone = "Pages.ManageLeads.ShowEmailPhone";
        public const string Pages_Lead_Duplicate_ShowEmailPhone = "Pages.Lead.Duplicate.ShowEmailPhone";
        public const string Pages_LeadTracker_ShowEmailPhone = "Pages.LeadTracker.ShowEmailPhone";
        public const string Pages_Lead_Closed_ShowEmailPhone = "Pages.Lead.Close.ShowEmailPhone";
        public const string Pages_3rdPartyLeadTracker_ShowEmailPhone = "Pages.3rdPartyLeadTracker.ShowEmailPhone";
        public const string Pages_Tracker_Warranty_ShowEmailPhone = "Pages.Tracker.Warranty.ShowEmailPhone";
        public const string Pages_TransportCost_ShowEmailPhone = "Pages.TransportCost.ShowEmailPhone";
        public const string Pages_InvoiceIssuedTracker_ShowEmailPhone = "Pages.Tracker.InvoiceIssuedTracker.ShowEmailPhone";
        public const string Pages_MyInstaller_ShowEmailPhone = "Pages.MyInstaller.ShowEmailPhone";
        public const string Pages_PendingInstallation_ShowEmailPhone = "Pages.PendingInstallation.ShowEmailPhone";
        public const string Pages_LeadGeneration_MyLeadsGeneration_ShowEmailPhone = "Pages.LeadGeneration.MyLeadsGeneration.ShowEmailPhone";
        public const string Pages_LeadGeneration_Commission_ShowEmailPhone = "Pages.LeadGeneration.Commission.ShowEmailPhone";
        public const string Pages_Lead_ManageService_ShowEmailPhone = "Pages.Lead.ManageService.ShowEmailPhone";
        public const string Pages_MyService_ShowEmailPhone = "Pages.Service.MyService.ShowEmailPhone";
        public const string Pages_WarrantyClaim_ShowEmailPhone = "Pages.Service.WarrantyClaim.ShowEmailPhone";

        public const string Pages_ManageLeads_Export_MobileEmail = "Pages.ManageLeads.Export.MobileEmail";
        public const string Pages_MyInstaller_Export_MobileEmail = "Pages.MyInstaller.Export.MobileEmail";
        public const string Pages_Lead_Duplicate_Export_MobileEmail = "Pages.Lead.Duplicate.Export.MobileEmail";
        public const string Pages_Lead_Closed_Export_MobileEmail = "Pages.Lead.Close.Export.MobileEmail";
        public const string Pages_LeadGeneration_Commission_Export_MobileEmail = "Pages.LeadGeneration.Commission.Export.MobileEmail";

        public const string Pages_Tenant_WholeSale_DataVaults_Serieses = "Pages.Tenant.WholeSale.DataVaults.Serieses";
        public const string Pages_Tenant_WholeSale_DataVaults_Serieses_Create = "Pages.Tenant.WholeSale.DataVaults.Serieses.Create";
        public const string Pages_Tenant_WholeSale_DataVaults_Serieses_Edit = "Pages.Tenant.WholeSale.DataVaults.Serieses.Edit";
        public const string Pages_Tenant_WholeSale_DataVaults_Serieses_Delete = "Pages.Tenant.WholeSale.DataVaults.Serieses.Delete";

        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleInvoicetypes = "Pages.Tenant.WholeSale.DataVaults.Wholesaleinvoicetypes";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleInvoicetypes_Create = "Pages.Tenant.WholeSale.DataVaults.Wholesaleinvoicetypes.Create";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleInvoicetypes_Edit = "Pages.Tenant.WholeSale.DataVaults.Wholesaleinvoicetypes.Edit";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleInvoicetypes_Delete = "Pages.Tenant.WholeSale.DataVaults.Wholesaleinvoicetypes.Delete";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleInvoicetypes_Export = "Pages.Tenant.WholeSale.DataVaults.Wholesaleinvoicetypes.Export";

        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleDeliveryOptions = "Pages.Tenant.WholeSale.DataVaults.WholesaleDeliveryOptions";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleDeliveryOptions_Create = "Pages.Tenant.WholeSale.DataVaults.WholesaleDeliveryOptions.Create";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleDeliveryOptions_Edit = "Pages.Tenant.WholeSale.DataVaults.WholesaleDeliveryOptions.Edit";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleDeliveryOptions_Delete = "Pages.Tenant.WholeSale.DataVaults.WholesaleDeliveryOptions.Delete";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleDeliveryOptions_Export = "Pages.Tenant.WholeSale.DataVaults.WholesaleDeliveryOptions.Export";

        public const string Pages_WholeSale_RequestToTransferLeads = "Pages.WholeSale.RequestToTransferLeads";
        public const string Pages_WholeSale_RequestToTransferLeads_Approve = "Pages.WholeSale.RequestToTransferLeads.Approve";
        public const string Pages_WholeSale_RequestToMove_Approve = "Pages.WholeSale.RequestToMove.Approve";

        //public const string Pages_WholeSale_RequestToTransferLeads_Dropdown_RequestToTransfer = "Pages.WholeSale.RequestToTransferLeads.Approve.RequestToTransfer";
        //public const string Pages_WholeSale_RequestToTransferLeads_Dropdown_RequestToMove = "Pages.WholeSale.RequestToTransferLeads.Dropdown.RequestToMove";



        public const string Pages_Tenant_WholeSale_datavault_ActivityLog = "Pages.Tenant.WholeSale.datavault.ActivityLog";
        public const string Pages_Tenant_WholeSale_datavault_ActivityLog_Detail = "Pages.Tenant.WholeSale.datavault.ActivityLog.Detail";
        public const string Pages_Tenant_WholeSale_datavault_ActivityLog_History = "Pages.Tenant.WholeSale.datavault.ActivityLog.History";

        public const string Pages_Tenant_Ecommerce_datavault_ActivityLog = "Pages.Tenant.Ecommerce.datavault.ActivityLog";
        public const string Pages_Tenant_Ecommerce_datavault_ActivityLog_Detail = "Pages.Tenant.Ecommerce.datavault.ActivityLog.Detail";
        public const string Pages_Tenant_Ecommerce_datavault_ActivityLog_History = "Pages.Tenant.Ecommerce.datavault.ActivityLog.History";

        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleTransportTypes = "Pages.Tenant.WholeSale.DataVaults.WholesaleTransportTypes";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleTransportTypes_Create = "Pages.Tenant.WholeSale.DataVaults.WholesaleTransportTypes.Create";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleTransportTypes_Edit = "Pages.Tenant.WholeSale.DataVaults.WholesaleTransportTypes.Edit";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleTransportTypes_Delete = "Pages.Tenant.WholeSale.DataVaults.WholesaleTransportTypes.Delete";

        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleJobTypes = "Pages.Tenant.WholeSale.DataVaults.WholesaleJobTypes";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleJobTypes_Create = "Pages.Tenant.WholeSale.DataVaults.WholesaleJobTypes.Create";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleJobTypes_Edit = "Pages.Tenant.WholeSale.DataVaults.WholesaleJobTypes.Edit";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleJobTypes_Delete = "Pages.Tenant.WholeSale.DataVaults.WholesaleJobTypes.Delete";

        public const string Pages_Tenant_WholeSale_DataVaults_WholesalePropertyTypes = "Pages.Tenant.WholeSale.DataVaults.WholesalePropertyTypes";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesalePropertyTypes_Create = "Pages.Tenant.WholeSale.DataVaults.WholesalePropertyTypes.Create";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesalePropertyTypes_Edit = "Pages.Tenant.WholeSale.DataVaults.WholesalePropertyTypes.Edit";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesalePropertyTypes_Delete = "Pages.Tenant.WholeSale.DataVaults.WholesalePropertyTypes.Delete";

        public const string Pages_Tenant_WholeSale_DataVaults_WholesalePVDStatuses = "Pages.Tenant.WholeSale.DataVaults.WholesalePVDStatuses";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesalePVDStatuses_Create = "Pages.Tenant.WholeSale.DataVaults.WholesalePVDStatuses.Create";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesalePVDStatuses_Edit = "Pages.Tenant.WholeSale.DataVaults.WholesalePVDStatuses.Edit";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesalePVDStatuses_Delete = "Pages.Tenant.WholeSale.DataVaults.WholesalePVDStatuses.Delete";

        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleJobStatuses = "Pages.Tenant.WholeSale.DataVaults.WholesaleJobStatuses";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleJobStatuses_Create = "Pages.Tenant.WholeSale.DataVaults.WholesaleJobStatuses.Create";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleJobStatuses_Edit = "Pages.Tenant.WholeSale.DataVaults.WholesaleJobStatuses.Edit";
        public const string Pages_Tenant_WholeSale_DataVaults_WholesaleJobStatuses_Delete = "Pages.Tenant.WholeSale.DataVaults.WholesaleJobStatuses.Delete";

        public const string Pages_Tenant_WholeSale_Notification = "Pages.Tenant.WholeSale.Notification";
        public const string Pages_Tenant_WholeSale_Sms = "Pages.Tenant.WholeSale.Sms";

        public const string Pages_Tenant_WholeSale_DataVaults_ProductType = "Pages.Tenant.WholeSale.DataVaults.ProductType";
        public const string Pages_Tenant_WholeSale_DataVaults_ProductType_Create = "Pages.Tenant.WholeSale.DataVaults.ProductType.Create";
        public const string Pages_Tenant_WholeSale_DataVaults_ProductType_Edit = "Pages.Tenant.WholeSale.DataVaults.ProductType.Edit";
        public const string Pages_Tenant_WholeSale_DataVaults_ProductType_Delete = "Pages.Tenant.WholeSale.DataVaults.ProductType.Delete";

        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceSpecifications = "Pages.Tenant.WholeSale.ECommerce.EcommerceSpecifications";
        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceSpecifications_Create = "Pages.Tenant.WholeSale.ECommerce.EcommerceSpecifications.Create";
        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceSpecifications_Edit = "Pages.Tenant.WholeSale.ECommerce.EcommerceSpecifications.Edit";
        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceSpecifications_Delete = "Pages.Tenant.WholeSale.ECommerce.EcommerceSpecifications.Delete";

        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceStcDealPrice = "Pages.Tenant.WholeSale.ECommerce.EcommerceStcDealPrice";
        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceStcDealPrice_Create = "Pages.Tenant.WholeSale.ECommerce.EcommerceStcDealPrice.Create";
        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceStcDealPrice_Edit = "Pages.Tenant.WholeSale.ECommerce.EcommerceStcDealPrice.Edit";
        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceStcDealPrice_Delete = "Pages.Tenant.WholeSale.ECommerce.EcommerceStcDealPrice.Delete";

        public const string Pages_Tenant_WholeSale_ECommerce_EcommerceStcRegister = "Pages.Tenant.WholeSale.ECommerce.EcommerceStcRegister";
        

        public const string Pages_Tenant_WholeSale_ECommerce_Products_EcommerceProductItemSpecifications = "Pages.Tenant.WholeSale.ECommerce.Products.EcommerceProductItemSpecification";
        public const string Pages_Report_RescheduleInstallation = "Pages.Report.RescheduleInstallation";
        public const string Pages_Report_RescheduleInstallation_History = "Pages.Report.RescheduleInstallation.History";

        public const string Pages_LeadDetails_InvoicePaymentVerifiedEdit = "Pages.LeadDetails.InvoicePaymentVerifiedEdit";

       

        public const string Pages_Tenant_DataVaults_InstallationCost_JobCostFixExpense = "Pages.Tenant.DataVaults.InstallationCost.JobCostFixExpense";
        public const string Pages_Tenant_DataVaults_InstallationCost_JobCostFixExpense_View = "Pages.Tenant.DataVaults.InstallationCost.JobCostFixExpense.View";
        public const string Pages_Tenant_DataVaults_InstallationCost_JobCostFixExpense_Create = "Pages.Tenant.DataVaults.InstallationCost.JobCostFixExpense.Create";
        public const string Pages_Tenant_DataVaults_InstallationCost_JobCostFixExpense_Edit = "Pages.Tenant.DataVaults.InstallationCost.JobCostFixExpense.Edit";
        public const string Pages_Tenant_DataVaults_InstallationCost_JobCostFixExpense_Delete = "Pages.Tenant.DataVaults.InstallationCost.JobCostFixExpense.Delete";
        public const string Pages_Tenant_DataVaults_InstallationCost_JobCostFixExpense_LastMonthEdit = "Pages.Tenant.DataVaults.InstallationCost.JobCostFixExpense.LastMonthEdit";

        public const string Pages_CIMETTracker = "Pages.Tracker.CIMETTracker";


        #region Quick Stock

        public const string Pages_Tenant_QuickStock = "Pages.Tenant.QuickStock";

        public const string Pages_Tenant_QuickStock_PurchaseOrder = "Pages.Tenant.QuickStock.PurchaseOrder";
        public const string Pages_Tenant_QuickStock_PurchaseOrder_Create = "Pages.Tenant.QuickStock.PurchaseOrder.Create";
        public const string Pages_Tenant_QuickStock_PurchaseOrder_Edit = "Pages.Tenant.QuickStock.PurchaseOrder.Edit";
        public const string Pages_Tenant_QuickStock_PurchaseOrder_Delete = "Pages.Tenant.QuickStock.PurchaseOrder.Delete";
        public const string Pages_Tenant_QuickStock_PurchaseOrder_Import = "Pages.Tenant.QuickStock.PurchaseOrder.Import";
        public const string Pages_Tenant_QuickStock_PurchaseOrder_Export = "Pages.Tenant.QuickStock.PurchaseOrder.Export";

        public const string Pages_Tenant_QuickStock_StockTransfer = "Pages.Tenant.QuickStock.StockTransfer";
        public const string Pages_Tenant_QuickStock_StockTransfer_Create = "Pages.Tenant.QuickStock.StockTransfer.Create";
        public const string Pages_Tenant_QuickStock_StockTransfer_Edit = "Pages.Tenant.QuickStock.StockTransfer.Edit";
        public const string Pages_Tenant_QuickStock_StockTransfer_Delete = "Pages.Tenant.QuickStock.StockTransfer.Delete";
        public const string Pages_Tenant_QuickStock_StockTransfer_Export = "Pages.Tenant.QuickStock.StockTransfer.Export";

        public const string Pages_Tenant_QuickStock_StockPayment = "Pages.Tenant.QuickStock.StockPayment";
        public const string Pages_Tenant_QuickStock_StockPayment_Create = "Pages.Tenant.QuickStock.StockPayment.Create";
        public const string Pages_Tenant_QuickStock_StockPayment_Edit = "Pages.Tenant.QuickStock.StockPayment.Edit";
        public const string Pages_Tenant_QuickStock_StockPayment_Delete = "Pages.Tenant.QuickStock.StockPayment.Delete";
        public const string Pages_Tenant_QuickStock_StockPayment_Export = "Pages.Tenant.QuickStock.StockPayment.Export";

        public const string Pages_Tenant_QuickStock_StockPaymentTracker = "Pages.Tenant.QuickStock.StockPaymentTracker";
        public const string Pages_Tenant_QuickStock_StockPaymentTracker_Export = "Pages.Tenant.QuickStock.StockPaymentTracker.Export";

        public const string Pages_Tenant_QuickStock_DataVaults = "Pages.Tenant.QuickStock.DataVaults";

        public const string Pages_Tenant_QuickStock_DataVaults_Currencies = "Pages.Tenant.QuickStock.DataVaults.Currencies";
        public const string Pages_Tenant_QuickStock_DataVaults_Currencies_Create = "Pages.Tenant.QuickStock.DataVaults.Currencies.Create";
        public const string Pages_Tenant_QuickStock_DataVaults_Currencies_Edit = "Pages.Tenant.QuickStock.DataVaults.Currencies.Edit";
        public const string Pages_Tenant_QuickStock_DataVaults_Currencies_Delete = "Pages.Tenant.QuickStock.DataVaults.Currencies.Delete";
        public const string Pages_Tenant_QuickStock_DataVaults_Currencies_Export = "Pages.Tenant.QuickStock.DataVaults.Currencies.Export";

        public const string Pages_Tenant_QuickStock_DataVaults_DeliveryTypes = "Pages.Tenant.QuickStock.DataVaults.DeliveryTypes";
        public const string Pages_Tenant_QuickStock_DataVaults_DeliveryTypes_Create = "Pages.Tenant.QuickStock.DataVaults.DeliveryTypes.Create";
        public const string Pages_Tenant_QuickStock_DataVaults_DeliveryTypes_Edit = "Pages.Tenant.QuickStock.DataVaults.DeliveryTypes.Edit";
        public const string Pages_Tenant_QuickStock_DataVaults_DeliveryTypes_Delete = "Pages.Tenant.QuickStock.DataVaults.DeliveryTypes.Delete";
        public const string Pages_Tenant_QuickStock_DataVaults_DeliveryTypes_Export = "Pages.Tenant.QuickStock.DataVaults.DeliveryTypes.Export";

        public const string Pages_Tenant_QuickStock_DataVaults_FreightCompany = "Pages.Tenant.QuickStock.DataVaults.FreightCompany";
        public const string Pages_Tenant_QuickStock_DataVaults_FreightCompany_Edit = "Pages.Tenant.QuickStock.DataVaults.FreightCompany.Edit";
        public const string Pages_Tenant_QuickStock_DataVaults_FreightCompany_Create = "Pages.Tenant.QuickStock.DataVaults.FreightCompany.Create";
        public const string Pages_Tenant_QuickStock_DataVaults_FreightCompany_Delete = "Pages.Tenant.QuickStock.DataVaults.FreightCompany.Delete";
        public const string Pages_Tenant_QuickStock_DataVaults_FreightCompany_Export = "Pages.Tenant.QuickStock.DataVaults.FreightCompany.Export";

        public const string Pages_Tenant_QuickStock_DataVaults_IncoTerm = "Pages.Tenant.QuickStock.DataVaults.IncoTerm";
        public const string Pages_Tenant_QuickStock_DataVaults_IncoTerm_Edit = "Pages.Tenant.QuickStock.DataVaults.IncoTerm.Edit";
        public const string Pages_Tenant_QuickStock_DataVaults_IncoTerm_Create = "Pages.Tenant.QuickStock.DataVaults.IncoTerm.Create";
        public const string Pages_Tenant_QuickStock_DataVaults_IncoTerm_Delete = "Pages.Tenant.QuickStock.DataVaults.IncoTerm.Delete";
        public const string Pages_Tenant_QuickStock_DataVaults_IncoTerm_Export = "Pages.Tenant.QuickStock.DataVaults.IncoTerm.Export";

        public const string Pages_Tenant_QuickStock_DataVaults_PaymentMethod = "Pages.Tenant.QuickStock.DataVaults.PaymentMethod";
        public const string Pages_Tenant_QuickStock_DataVaults_PaymentMethod_Edit = "Pages.Tenant.QuickStock.DataVaults.PaymentMethod.Edit";
        public const string Pages_Tenant_QuickStock_DataVaults_PaymentMethod_Create = "Pages.Tenant.QuickStock.DataVaults.PaymentMethod.Create";
        public const string Pages_Tenant_QuickStock_DataVaults_PaymentMethod_Delete = "Pages.Tenant.QuickStock.DataVaults.PaymentMethod.Delete";
        public const string Pages_Tenant_QuickStock_DataVaults_PaymentMethod_Export = "Pages.Tenant.QuickStock.DataVaults.PaymentMethod.Export";

        public const string Pages_Tenant_QuickStock_DataVaults_PaymentStatus = "Pages.Tenant.QuickStock.DataVaults.PaymentStatus";
        public const string Pages_Tenant_QuickStock_DataVaults_PaymentStatus_Edit = "Pages.Tenant.QuickStock.DataVaults.PaymentStatus.Edit";
        public const string Pages_Tenant_QuickStock_DataVaults_PaymentStatus_Create = "Pages.Tenant.QuickStock.DataVaults.PaymentStatus.Create";
        public const string Pages_Tenant_QuickStock_DataVaults_PaymentStatus_Delete = "Pages.Tenant.QuickStock.DataVaults.PaymentStatus.Delete";
        public const string Pages_Tenant_QuickStock_DataVaults_PaymentStatus_Export = "Pages.Tenant.QuickStock.DataVaults.PaymentStatus.Export";

        public const string Pages_Tenant_QuickStock_DataVaults_PaymentType = "Pages.Tenant.QuickStock.DataVaults.PaymentType";
        public const string Pages_Tenant_QuickStock_DataVaults_PaymentType_Edit = "Pages.Tenant.QuickStock.DataVaults.PaymentType.Edit";
        public const string Pages_Tenant_QuickStock_DataVaults_PaymentType_Create = "Pages.Tenant.QuickStock.DataVaults.PaymentType.Create";
        public const string Pages_Tenant_QuickStock_DataVaults_PaymentType_Delete = "Pages.Tenant.QuickStock.DataVaults.PaymentType.Delete";
        public const string Pages_Tenant_QuickStock_DataVaults_PaymentType_Export = "Pages.Tenant.QuickStock.DataVaults.PaymentType.Export";

        public const string Pages_Tenant_QuickStock_DataVaults_PurchaseCompany = "Pages.Tenant.QuickStock.DataVaults.PurchaseCompany";
        public const string Pages_Tenant_QuickStock_DataVaults_PurchaseCompany_Edit = "Pages.Tenant.QuickStock.DataVaults.PurchaseCompany.Edit";
        public const string Pages_Tenant_QuickStock_DataVaults_PurchaseCompany_Create = "Pages.Tenant.QuickStock.DataVaults.PurchaseCompany.Create";
        public const string Pages_Tenant_QuickStock_DataVaults_PurchaseCompany_Delete = "Pages.Tenant.QuickStock.DataVaults.PurchaseCompany.Delete";
        public const string Pages_Tenant_QuickStock_DataVaults_PurchaseCompany_Export = "Pages.Tenant.QuickStock.DataVaults.PurchaseCompany.Export";

        public const string Pages_Tenant_QuickStock_DataVaults_StockFroms = "Pages.Tenant.QuickStock.DataVaults.StockFroms";
        public const string Pages_Tenant_QuickStock_DataVaults_StockFroms_Create = "Pages.Tenant.QuickStock.DataVaults.StockFroms.Create";
        public const string Pages_Tenant_QuickStock_DataVaults_StockFroms_Edit = "Pages.Tenant.QuickStock.DataVaults.StockFroms.Edit";
        public const string Pages_Tenant_QuickStock_DataVaults_StockFroms_Delete = "Pages.Tenant.QuickStock.DataVaults.StockFroms.Delete";
        public const string Pages_Tenant_QuickStock_DataVaults_StockFroms_Export = "Pages.Tenant.QuickStock.DataVaults.StockFroms.Export";

        public const string Pages_Tenant_QuickStock_DataVaults_StockOrderFors = "Pages.Tenant.QuickStock.DataVaults.StockOrderFors";
        public const string Pages_Tenant_QuickStock_DataVaults_StockOrderFors_Create = "Pages.Tenant.QuickStock.DataVaults.StockOrderFors.Create";
        public const string Pages_Tenant_QuickStock_DataVaults_StockOrderFors_Edit = "Pages.Tenant.QuickStock.DataVaults.StockOrderFors.Edit";
        public const string Pages_Tenant_QuickStock_DataVaults_StockOrderFors_Delete = "Pages.Tenant.QuickStock.DataVaults.StockOrderFors.Delete";
        public const string Pages_Tenant_QuickStock_DataVaults_StockOrderFors_Export = "Pages.Tenant.QuickStock.DataVaults.StockOrderFors.Export";

        public const string Pages_Tenant_QuickStock_DataVaults_StockOrderStatus = "Pages.Tenant.QuickStock.DataVaults.StockOrderStatus";
        public const string Pages_Tenant_QuickStock_DataVaults_StockOrderStatus_Edit = "Pages.Tenant.QuickStock.DataVaults.StockOrderStatus.Edit";
        public const string Pages_Tenant_QuickStock_DataVaults_StockOrderStatus_Create = "Pages.Tenant.QuickStock.DataVaults.StockOrderStatus.Create";
        public const string Pages_Tenant_QuickStock_DataVaults_StockOrderStatus_Delete = "Pages.Tenant.QuickStock.DataVaults.StockOrderStatus.Delete";
        public const string Pages_Tenant_QuickStock_DataVaults_StockOrderStatus_Export = "Pages.Tenant.QuickStock.DataVaults.StockOrderStatus.Export";

        public const string Pages_Tenant_QuickStock_DataVaults_TransportCompanies = "Pages.Tenant.QuickStock.DataVaults.TransportCompanies";
        public const string Pages_Tenant_QuickStock_DataVaults_TransportCompanies_Create = "Pages.Tenant.QuickStock.DataVaults.TransportCompanies.Create";
        public const string Pages_Tenant_QuickStock_DataVaults_TransportCompanies_Edit = "Pages.Tenant.QuickStock.DataVaults.TransportCompanies.Edit";
        public const string Pages_Tenant_QuickStock_DataVaults_TransportCompanies_Delete = "Pages.Tenant.QuickStock.DataVaults.TransportCompanies.Delete";
        public const string Pages_Tenant_QuickStock_DataVaults_TransportCompanies_Export = "Pages.Tenant.QuickStock.DataVaults.TransportCompanies.Export";

        public const string Pages_Tenant_QuickStock_DataVaults_Vendor = "Pages.Tenant.QuickStock.DataVaults.Vendor";
        public const string Pages_Tenant_QuickStock_DataVaults_Vendor_Edit = "Pages.Tenant.QuickStock.DataVaults.Vendor.Edit";
        public const string Pages_Tenant_QuickStock_DataVaults_Vendor_Create = "Pages.Tenant.QuickStock.DataVaults.Vendor.Create";
        public const string Pages_Tenant_QuickStock_DataVaults_Vendor_Delete = "Pages.Tenant.QuickStock.DataVaults.Vendor.Delete";
        public const string Pages_Tenant_QuickStock_DataVaults_Vendor_Export = "Pages.Tenant.QuickStock.DataVaults.Vendor.Export";


        public const string Pages_Tenant_QuickStock_DataVaults_PurchaseDocumentList = "Pages.Tenant.QuickStock.DataVaults.PurchaseDocumentList";
        public const string Pages_Tenant_QuickStock_DataVaults_PurchaseDocumentList_Edit = "Pages.Tenant.QuickStock.DataVaults.PurchaseDocumentList.Edit";
        public const string Pages_Tenant_QuickStock_DataVaults_PurchaseDocumentList_Create = "Pages.Tenant.QuickStock.DataVaults.PurchaseDocumentList.Create";
        public const string Pages_Tenant_QuickStock_DataVaults_PurchaseDocumentList_Delete = "Pages.Tenant.QuickStock.DataVaults.PurchaseDocumentList.Delete";
        public const string Pages_Tenant_QuickStock_DataVaults_PurchaseDocumentList_Export = "Pages.Tenant.QuickStock.DataVaults.PurchaseDocumentList.Export";

        public const string Pages_Tenant_QuickStock_DataVaults_StockVariations = "Pages.Tenant.QuickStock.DataVaults.StockVariations";
        public const string Pages_Tenant_QuickStock_DataVaults_StockVariations_Create = "Pages.Tenant.QuickStock.DataVaults.StockVariations.Create";
        public const string Pages_Tenant_QuickStock_DataVaults_StockVariations_Edit = "Pages.Tenant.QuickStock.DataVaults.StockVariations.Edit";
        public const string Pages_Tenant_QuickStock_DataVaults_StockVariations_Delete = "Pages.Tenant.QuickStock.DataVaults.StockVariations.Delete";

        public const string Pages_Tenant_QuickStock_DataVaults_SerialNoStatus = "Pages.Tenant.QuickStock.DataVaults.SerialNoStatus";
        public const string Pages_Tenant_QuickStock_DataVaults_SerialNoStatus_Edit = "Pages.Tenant.QuickStock.DataVaults.SerialNoStatus.Edit";
        public const string Pages_Tenant_QuickStock_DataVaults_SerialNoStatus_Delete = "Pages.Tenant.QuickStock.DataVaults.SerialNoStatus.Delete";

        public const string Pages_Tenant_QuickStock_datavault_ActivityLog = "Pages.Tenant.QuickStock.DataVaults.ActivityLog";
        public const string Pages_Tenant_QuickStock_datavault_ActivityLog_Detail = "Pages.Tenant.QuickStock.DataVaults.ActivityLog.Detail";
        public const string Pages_Tenant_QuickStock_datavault_ActivityLog_History = "Pages.Tenant.QuickStock.DataVaults.ActivityLog.History";

        public const string Pages_Tenant_QuickStock_Reports = "Pages.Tenant.QuickStock.Reports";

        public const string Pages_Tenant_QuickStock_Reports_StockReceivedReport = "Pages.Tenant.QuickStock.Reports.StockReceivedReport";
        public const string Pages_Tenant_QuickStock_Reports_StockReceivedReport_Export = "Pages.Tenant.QuickStock.Reports.StockReceivedReport.Export";
        public const string Pages_Tenant_QuickStock_Reports_SerialNoHistoryReport = "Pages.Tenant.QuickStock.Reports.SerialNoHistoryReport";
        #endregion
    }
}

