﻿using System.Threading.Tasks;
using Abp.Domain.Policies;

namespace TheSolarProduct.Authorization.Users
{
    public interface IUserPolicy : IPolicy
    {
        Task CheckMaxUserCountAsync(int tenantId);
    }
}
