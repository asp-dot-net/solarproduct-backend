﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.Authorization.Users
{
    [Table("UserWarehouseLocations")]
    public class UserWarehouseLocation : FullAuditedEntity
    {
        public int? WarehouseLocationId { get; set; }

        [ForeignKey("WarehouseLocationId")]
        public Warehouselocation WarehouseLocationFk { get; set; }

        public long? UserId { get; set; }

        [ForeignKey("UserId")]
        public User UserFk { get; set; }
    }
}
