﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Authorization.Users
{
    [Table("UserIPAddress")]
    public class UserIPAddress : FullAuditedEntity
    {
        public string IPAdress { get; set; }

        public bool IsActive { get; set; }

        public long UserId { get; set; }
    }
}
