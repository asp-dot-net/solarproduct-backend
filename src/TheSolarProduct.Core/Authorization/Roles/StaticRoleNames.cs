﻿namespace TheSolarProduct.Authorization.Roles
{
    public static class StaticRoleNames
    {
        public static class Host
        {
            public const string Admin = "Admin";

        }

        public static class Tenants
        {
            public const string Admin = "Admin";

            public const string User = "User";

            public const string SalesRep = "Sales Rep";

            public const string SalesManager = "Sales Manager";

            public const string Installer = "Installer";

            public const string InstallerManager = "Installation Manager";

            public const string PreInstallation = "Pre Installation";

            public const string PostInstallation = "Post Installation";

            public const string LeadOperator = "Lead Operator";

            public const string WholesaleClient = "Wholesale Client";
        }
    }
}