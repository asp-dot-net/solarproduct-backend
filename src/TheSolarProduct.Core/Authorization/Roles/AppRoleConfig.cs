﻿using Abp.MultiTenancy;
using Abp.Zero.Configuration;

namespace TheSolarProduct.Authorization.Roles
{
	public static class AppRoleConfig
	{
		public static void Configure(IRoleManagementConfig roleManagementConfig)
		{
			//Static host roles

			roleManagementConfig.StaticRoles.Add(
				new StaticRoleDefinition(
					StaticRoleNames.Host.Admin,
					MultiTenancySides.Host,
					grantAllPermissionsByDefault: true)
				);
			//roleManagementConfig.StaticRoles.Add(
			//	new StaticRoleDefinition(
			//		StaticRoleNames.Host.SalesRep,
			//		MultiTenancySides.Host)
			//	);
			//roleManagementConfig.StaticRoles.Add(
			//	new StaticRoleDefinition(
			//		StaticRoleNames.Host.SalesManager,
			//		MultiTenancySides.Host)
			//	);
			//roleManagementConfig.StaticRoles.Add(
			//	new StaticRoleDefinition(
			//		StaticRoleNames.Host.LeadOperator,
			//		MultiTenancySides.Host)
			//	);
			//roleManagementConfig.StaticRoles.Add(
			//	new StaticRoleDefinition(
			//		StaticRoleNames.Host.Installer,
			//		MultiTenancySides.Host)
			//	);

			//Static tenant roles

			roleManagementConfig.StaticRoles.Add(
				new StaticRoleDefinition(
					StaticRoleNames.Tenants.Admin,
					MultiTenancySides.Tenant,
					grantAllPermissionsByDefault: true)
				);

			roleManagementConfig.StaticRoles.Add(
				new StaticRoleDefinition(
					StaticRoleNames.Tenants.User,
					MultiTenancySides.Tenant)
				);

			roleManagementConfig.StaticRoles.Add(
				new StaticRoleDefinition(
					StaticRoleNames.Tenants.SalesRep,
					MultiTenancySides.Tenant)
				{
					GrantedPermissions = { "Pages.Tenant.Dashboard"
					,"Pages.Tenant.Dashboard.UserLeads"
					,"Pages.Leads"
					,"Pages.Leads.Create"
					,"Pages.Leads.Edit"
					,"Pages.MyLeads"
					,"Pages.Lead.Close"
					,"Pages.Leads.Export"
					,"Pages.Quotations"
					,"Pages.Quotations.Create"
					,"Pages.Quotations.Edit"
					,"Pages.Quotations.Delete"
					,"Pages.Jobs"
					,"Pages.Jobs.Create"
					,"Pages.Jobs.Edit"
					,"Pages.Jobs.Delete"
					,"Pages.ApplicationTracker"
					,"Pages.ApplicationTracker.Edit"
					,"Pages.FreebiesTracker"
					,"Pages.InvoiceTracker"
					,"Pages.RefundTracker"
					,"Pages.JobGrid"
					,"Pages.Jobs.Sales"
					,"Pages.Jobs.Quotation"
					,"Pages.Jobs.Refund"
					,"Pages.Tenant.Dashboard.SalesDetails"
					,"Pages.Tenant.Dashboard.ToDoDetails"
					,"Pages_Tenant_Dashboard_MyReminders"}
				}
				);

			roleManagementConfig.StaticRoles.Add(
				new StaticRoleDefinition(
					StaticRoleNames.Tenants.SalesManager,
					MultiTenancySides.Tenant)
				{
					GrantedPermissions = { "Pages.Tenant.Dashboard"
					,"Pages.Tenant.Dashboard.UserLeads"
					,"Pages.Leads"
					,"Pages.Leads.Create"
					,"Pages.Leads.Edit"
					,"Pages.Leads.Delete"
					,"Pages.Leads.Assign"
					,"Pages.MyLeads"
					,"Pages.Lead.Close"
					,"Pages.LeadTracker"
					,"Pages.Leads.Export"
					,"Pages.Quotations"
					,"Pages.Quotations.Create"
					,"Pages.Quotations.Edit"
					,"Pages.Quotations.Delete"
					,"Pages.Jobs"
					,"Pages.Jobs.Create"
					,"Pages.Jobs.Edit"
					,"Pages.Jobs.Delete"
					,"Pages.ApplicationTracker"
					,"Pages.ApplicationTracker.Edit"
					,"Pages.FreebiesTracker"
					,"Pages.InvoiceTracker"
					,"Pages.RefundTracker"
					,"Pages.JobGrid"
					,"Pages.Jobs.Sales"
					,"Pages.Jobs.Quotation"
					,"Pages.Jobs.Refund"
					, "Pages.Jobs.StatusChange"
					,"Pages.Tenant.Dashboard.ManagerSalesDetails"
					,"Pages.Tenant.Dashboard.SalesDetails"
					,"Pages.Tenant.Dashboard.ToDoDetails"
					,"Pages.Tenant.Dashboard.ManagerToDoDetails"
					,"Pages.Tenant.Dashboard.ManagerLeads"
					,"Pages_Tenant_Dashboard_MyReminders"}
				}
				);

			roleManagementConfig.StaticRoles.Add(
				new StaticRoleDefinition(
					StaticRoleNames.Tenants.LeadOperator,
					MultiTenancySides.Tenant)
				{
					GrantedPermissions = { "Pages.Tenant.Dashboard"
					,"Pages.Tenant.Dashboard.UserLeads"
					,"Pages.Leads"
					,"Pages.Leads.Create"
					,"Pages.Leads.Edit"
					,"Pages.Leads.Delete"
					,"Pages.Leads.Assign"
					,"Pages.Lead.Close"
					,"Pages.Leads.Export"
					,"Pages.ManageLeads"
					,"Pages.Lead.Duplicate"}
				});

			roleManagementConfig.StaticRoles.Add(
				new StaticRoleDefinition(
					StaticRoleNames.Tenants.Installer,
					MultiTenancySides.Tenant)
				{
					GrantedPermissions = { "Pages.Tenant.Dashboard"
					,"Pages.Calenders"
					,"Pages.Installation"
					,"Pages.Tenant.Installer"
					,"Pages.Tenant.Installer.Availability"
					,"Pages.Tenant.InstallerInvoice"}
				}
				);

			roleManagementConfig.StaticRoles.Add(
				new StaticRoleDefinition(
					StaticRoleNames.Tenants.InstallerManager,
					MultiTenancySides.Tenant)
				{
					GrantedPermissions = { "Pages.Tenant.Dashboard"
					,"Pages.Calenders"
					,"Pages.Installation"
					,"Pages.Tenant.Installer"
					,"Pages.Tenant.Installer.Availability"}
				}
				);

			roleManagementConfig.StaticRoles.Add(
				new StaticRoleDefinition(
					StaticRoleNames.Tenants.PreInstallation,
					MultiTenancySides.Tenant)
				{
					GrantedPermissions = { "Pages.Tenant.Dashboard"
					,"Pages.Calenders"
					,"Pages.Installation"
					,"Pages.Tenant.Installer"
					,"Pages.Tenant.Installer.Availability"}
				}
				);

			roleManagementConfig.StaticRoles.Add(
				new StaticRoleDefinition(
					StaticRoleNames.Tenants.PostInstallation,
					MultiTenancySides.Tenant)
				{
					GrantedPermissions = { "Pages.Tenant.Dashboard"
					,"Pages.Calenders"
					,"Pages.Installation"
					,"Pages.Tenant.Installer"
					,"Pages.Tenant.Installer.Availability"}
				}
				);
		}
	}
}
