﻿using Abp.Authorization;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;

namespace TheSolarProduct.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
