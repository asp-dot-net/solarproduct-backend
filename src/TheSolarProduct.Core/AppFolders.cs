﻿using Abp.Dependency;

namespace TheSolarProduct
{
	public class AppFolders : IAppFolders, ISingletonDependency
	{
		public string SampleProfileImagesFolder { get; set; }

		public string WebLogsFolder { get; set; }

		public string Report { get; set; }
	}
}