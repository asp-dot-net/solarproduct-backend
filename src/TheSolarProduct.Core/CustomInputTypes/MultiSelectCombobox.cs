﻿using System;
using Abp.UI.Inputs;

namespace TheSolarProduct.CustomInputTypes
{
    /// <summary>
    ///Multi Select Combobox value UI type.
    /// </summary>
    [Serializable]
    [InputType("MULTISELECTCOMBOBOX")]
    public class MultiSelectComboboxInputType : InputTypeBase
    {
    }
}
