﻿namespace TheSolarProduct.Features
{
    public static class AppFeatures
    {
        public const string MaxUserCount = "App.MaxUserCount";
        public const string ChatFeature = "App.ChatFeature";
        public const string TenantToTenantChatFeature = "App.ChatFeature.TenantToTenant";
        public const string TenantToHostChatFeature = "App.ChatFeature.TenantToHost";
        public const string TestCheckFeature = "App.TestCheckFeature";
        public const string TestCheckFeature2 = "App.TestCheckFeature2";
        public const string PromotionFeature = "App.PromotionFeature";
        public const string MaxSMSCount = "App.MaxSMSCount";

        //For Host Features for Tanant Login With IP Setting
        public const string IPAddressFeature = "App.IPAddressFeature";

        //Service Features
        public const string Service = "App.Service";
        public const string Service_ManageService = "App.Service.ManageService";
        public const string Service_MyService = "App.Service.MyService";
        public const string Service_Map = "App.Service.Map";
        public const string Service_Installation = "App.Service.Installation";

        //Stock Management Features
        public const string StockManagement = "App.StockManagement";
        public const string StockManagement_Picklist = "App.StockManagement.Picklist";
        public const string StockManagement_Picklist_CheckStockOnHand = "App.StockManagement.Picklist.CheckStockOnHand";

        public const string StockManagement_ProductItems = "App.StockManagement.ProductItems";
        public const string StockManagement_ProductItems_NetSituation = "App.StockManagement.ProductItems.NetSituation";
        
        //Lead Details Features
        public const string LeadDetails = "App.LeadDetails";
        public const string LeadDetails_CallHistory = "App.LeadDetails.CallHistory";
        public const string LeadDetails_Job = "App.LeadDetails.Job";
        public const string LeadDetails_Job_ActualCost = "App.LeadDetails.Job.ActualCost";
        public const string LeadDetails_Job_ProductItems_CheckWarranty = "App.LeadDetails.Job.ProductItems.CheckWarranty";
        //public const string LeadDetails_Job_DepositReceivedDateExcelSheet = "App.LeadDetails.Job.DepositReceivedDateExcelSheet";

        //Leads Generation
        public const string LeadGeneration = "App.LeadGeneration";
        public const string LeadGeneration_MyLeadsGeneration = "App.LeadGeneration.MyLeadsGeneration";
        public const string LeadGeneration_Map = "App.LeadGeneration.Map";
        public const string LeadGeneration_Installation = "App.LeadGeneration.Installation";

        // Create Manual Invoice
        public const string InvoicePaid_CreateManually = "App.InvoicePaid.CreateManually";

        public const string TenantEnableCopyPaste = "App.TenantEnableCopyPaste";
        public const string AutoAssignLeads = "App.AutoAssignLeads";

        public const string UserInOutTimeFeature = "App.UserInOutTimeFeature";

    }
}