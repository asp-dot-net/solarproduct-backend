﻿using Abp.Application.Features;
using Abp.Localization;
using Abp.Runtime.Validation;
using Abp.UI.Inputs;

namespace TheSolarProduct.Features
{
    public class AppFeatureProvider : FeatureProvider
    {
        public override void SetFeatures(IFeatureDefinitionContext context)
        {
            context.Create(
                AppFeatures.MaxUserCount,
                defaultValue: "0", //0 = unlimited
                displayName: L("MaximumUserCount"),
                description: L("MaximumUserCount_Description"),
                inputType: new SingleLineStringInputType(new NumericValueValidator(0, int.MaxValue))
            )[FeatureMetadata.CustomFeatureKey] = new FeatureMetadata
            {
                ValueTextNormalizer = value => value == "0" ? L("Unlimited") : new FixedLocalizableString(value),
                IsVisibleOnPricingTable = true
            };

            #region ######## Example Features - You can delete them #########

            //context.Create("TestTenantScopeFeature", "false", L("TestTenantScopeFeature"), scope: FeatureScopes.Tenant);
            //context.Create("TestEditionScopeFeature", "false", L("TestEditionScopeFeature"), scope: FeatureScopes.Edition);

            //context.Create(
            //    AppFeatures.TestCheckFeature,
            //    defaultValue: "false",
            //    displayName: L("TestCheckFeature"),
            //    inputType: new CheckboxInputType()
            //)[FeatureMetadata.CustomFeatureKey] = new FeatureMetadata
            //{
            //    IsVisibleOnPricingTable = true,
            //    TextHtmlColor = value => value == "true" ? "#5cb85c" : "#d9534f"
            //};

            //context.Create(
            //    AppFeatures.TestCheckFeature2,
            //    defaultValue: "true",
            //    displayName: L("TestCheckFeature2"),
            //    inputType: new CheckboxInputType()
            //)[FeatureMetadata.CustomFeatureKey] = new FeatureMetadata
            //{
            //    IsVisibleOnPricingTable = true,
            //    TextHtmlColor = value => value == "true" ? "#5cb85c" : "#d9534f"
            //};

            #endregion

            var ipAddressFeature = context.Create(
                AppFeatures.IPAddressFeature,
                defaultValue: "false",
                displayName: L("IPAddressFeature"),
                inputType: new CheckboxInputType()
            );

            var promotionFeature = context.Create(
                AppFeatures.PromotionFeature,
                defaultValue: "false",
                displayName: L("PromotionFeature"),
                inputType: new CheckboxInputType()
            );

            promotionFeature.CreateChildFeature(
                AppFeatures.MaxSMSCount,
                defaultValue: "0", //0 = unlimited
                displayName: L("MaximumSMSCount"),
                description: L("MaximumSMSCount_Description"),
                inputType: new SingleLineStringInputType(new NumericValueValidator(0, int.MaxValue))
            );

            var chatFeature = context.Create(
                AppFeatures.ChatFeature,
                defaultValue: "false",
                displayName: L("ChatFeature"),
                inputType: new CheckboxInputType()
            );

            chatFeature.CreateChildFeature(
                AppFeatures.TenantToTenantChatFeature,
                defaultValue: "false",
                displayName: L("TenantToTenantChatFeature"),
                inputType: new CheckboxInputType()
            );

            chatFeature.CreateChildFeature(
                AppFeatures.TenantToHostChatFeature,
                defaultValue: "false",
                displayName: L("TenantToHostChatFeature"),
                inputType: new CheckboxInputType()
            );

            #region Service Features
            var service = context.Create(
                AppFeatures.Service,
                defaultValue: "false",
                displayName: L("Service"),
                inputType: new CheckboxInputType()
            );

            service.CreateChildFeature(
                AppFeatures.Service_ManageService,
                defaultValue: "false",
                displayName: L("ManageService"),
                inputType: new CheckboxInputType()
            );

            service.CreateChildFeature(
                AppFeatures.Service_MyService,
                defaultValue: "false",
                displayName: L("MyService"),
                inputType: new CheckboxInputType()
            );

            service.CreateChildFeature(
                AppFeatures.Service_Map,
                defaultValue: "false",
                displayName: L("Map"),
                inputType: new CheckboxInputType()
            );

            service.CreateChildFeature(
                AppFeatures.Service_Installation,
                defaultValue: "false",
                displayName: L("Installation"),
                inputType: new CheckboxInputType()
            );
            #endregion

            #region Stock Management Features
            var stockManagement = context.Create(
                AppFeatures.StockManagement,
                defaultValue: "false",
                displayName: L("StockManagement"),
                inputType: new CheckboxInputType()
            );

            var picklist = stockManagement.CreateChildFeature(
                AppFeatures.StockManagement_Picklist,
                defaultValue: "false",
                displayName: L("Picklist"),
                inputType: new CheckboxInputType()
            );

            picklist.CreateChildFeature(
                AppFeatures.StockManagement_Picklist_CheckStockOnHand,
                defaultValue: "false",
                displayName: L("CheckStockOnHand"),
                inputType: new CheckboxInputType()
            );

            var productItem = stockManagement.CreateChildFeature(
                AppFeatures.StockManagement_ProductItems,
                defaultValue: "false",
                displayName: L("ProductItems"),
                inputType: new CheckboxInputType()
            );

            productItem.CreateChildFeature(
                AppFeatures.StockManagement_ProductItems_NetSituation,
                defaultValue: "false",
                displayName: L("NetSituation"),
                inputType: new CheckboxInputType()
            );
            #endregion

            #region Lead Details Features
            var leadDetails = context.Create(
                AppFeatures.LeadDetails,
                defaultValue: "false",
                displayName: L("LeadDetails"),
                inputType: new CheckboxInputType()
            );

            leadDetails.CreateChildFeature(
                AppFeatures.LeadDetails_CallHistory,
                defaultValue: "false",
                displayName: L("CallHistory"),
                inputType: new CheckboxInputType()
            );

            var jobs = leadDetails.CreateChildFeature(
                AppFeatures.LeadDetails_Job,
                defaultValue: "false",
                displayName: L("Jobs"),
                inputType: new CheckboxInputType()
            );

            jobs.CreateChildFeature(
                AppFeatures.LeadDetails_Job_ActualCost,
                defaultValue: "false",
                displayName: L("ActualCost"),
                inputType: new CheckboxInputType()
            );
            
            jobs.CreateChildFeature(
                AppFeatures.LeadDetails_Job_ProductItems_CheckWarranty,
                defaultValue: "false",
                displayName: L("CheckWarranty"),
                inputType: new CheckboxInputType()
            );

            //jobs.CreateChildFeature(
            //    AppFeatures.LeadDetails_Job_DepositReceivedDateExcelSheet,
            //    defaultValue: "false",
            //    displayName: L("DepositReceivedDateExcelSheet"),
            //    inputType: new CheckboxInputType()
            //);
            #endregion

            #region Lead Generation Features
            var leadGeneration = context.Create(
                AppFeatures.LeadGeneration,
                defaultValue: "false",
                displayName: L("LeadGeneration"),
                inputType: new CheckboxInputType()
            );

            leadGeneration.CreateChildFeature(
                AppFeatures.LeadGeneration_MyLeadsGeneration,
                defaultValue: "false",
                displayName: L("MyLeadsGeneration"),
                inputType: new CheckboxInputType()
            );

            leadGeneration.CreateChildFeature(
                AppFeatures.LeadGeneration_Map,
                defaultValue: "false",
                displayName: L("Map"),
                inputType: new CheckboxInputType()
            );

            leadGeneration.CreateChildFeature(
                AppFeatures.LeadGeneration_Installation,
                defaultValue: "false",
                displayName: L("Installation"),
                inputType: new CheckboxInputType()
            );
            #endregion

            #region Create Manual in Invoice Paid
            var manualInvoice = context.Create(
                AppFeatures.InvoicePaid_CreateManually,
                defaultValue: "false",
                displayName: L("CreateManuallyInvoice"),
                inputType: new CheckboxInputType()
            );
            #endregion

            var EnableCopyPaste = context.Create(
                   AppFeatures.TenantEnableCopyPaste,
                   defaultValue: "false",
                   displayName: L("EnableCopyPaste"),
                   inputType: new CheckboxInputType()
               );

            var AutoAssignLeads = context.Create(
                   AppFeatures.AutoAssignLeads,
                   defaultValue: "false",
                   displayName: L("EnableAutoAssignLeads"),
                   inputType: new CheckboxInputType()
               );

            var userInOutTimeFeature = context.Create(
                AppFeatures.UserInOutTimeFeature,
                defaultValue: "false",
                displayName: L("UserInOutTimeFeature"),
                inputType: new CheckboxInputType()
            );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, TheSolarProductConsts.LocalizationSourceName);
        }
    }
}
