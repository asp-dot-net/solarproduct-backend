﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using TheSolarProduct.ServiceCategorys;

namespace TheSolarProduct.ServiceSubCategorys
{
    [Table("ServiceSubCategorys")]
    public class ServiceSubCategory : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual int ServiceCategoryId { get; set; }

        [Required]
        public virtual string Name { get; set; }
        public virtual Boolean IsActive { get; set; }
    }
}

