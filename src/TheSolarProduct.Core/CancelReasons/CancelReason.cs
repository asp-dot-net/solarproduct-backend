﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.CancelReasons
{
	[Table("CancelReasons")]
    public class CancelReason : FullAuditedEntity , IMustHaveTenant
    {
			public int TenantId { get; set; }
			

		[Required]
		[StringLength(CancelReasonConsts.MaxCancelReasonNameLength, MinimumLength = CancelReasonConsts.MinCancelReasonNameLength)]
		public virtual string CancelReasonName { get; set; }

        public virtual Boolean IsActive { get; set; }
    }
}