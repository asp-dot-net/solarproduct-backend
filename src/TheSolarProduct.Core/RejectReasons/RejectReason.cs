﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.RejectReasons
{
	[Table("RejectReasons")]
    public class RejectReason : FullAuditedEntity , IMustHaveTenant
    {
	    public int TenantId { get; set; }

		[Required]
		[StringLength(RejectReasonConsts.MaxRejectReasonNameLength, MinimumLength = RejectReasonConsts.MinRejectReasonNameLength)]
		public virtual string RejectReasonName { get; set; }

        public virtual Boolean IsActive { get; set; }
    }
}