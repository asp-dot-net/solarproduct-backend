﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.JobHistory
{
    [Table("JobTrackerHistory")]
    public class JobTrackerHistory : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual string FieldName { get; set; }
        public virtual string PrevValue { get; set; }
        public virtual string CurValue { get; set; }
        public virtual string Action { get; set; }
        public virtual DateTime LastmodifiedDateTime { get; set; }
        public int JobIDId { get; set; }
        public int JobActionId { get; set; }
    }
}
