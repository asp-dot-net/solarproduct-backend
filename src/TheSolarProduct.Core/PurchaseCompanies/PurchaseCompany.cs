﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.PurchaseCompanies
{
	[Table("PurchaseCompanies")]
    public class PurchaseCompany : FullAuditedEntity
    {
        public string Name { get; set; }

        public bool? IsActive { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public string Logo { get; set; }
    }
}
