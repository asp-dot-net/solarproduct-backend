﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;
using TheSolarProduct.ProductPackages;

namespace TheSolarProduct.ProductPackages
{
    [Table("ProductPackageItems")]
    public class ProductPackageItem : FullAuditedEntity
    {
		public int ProductPackageId { get; set; }

		public int ProductItemId { get; set; }

		[ForeignKey("ProductItemId")]
		public ProductItem ProductItemFk { get; set; }

		public int Quantity { get; set; }
	}
}
