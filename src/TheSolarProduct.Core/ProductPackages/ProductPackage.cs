﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.ProductPackages;

namespace TheSolarProduct.ProductPackages
{
    [Table("ProductPackages")]
    public class ProductPackage : FullAuditedEntity
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public bool IsActive { get; set; }

        public string Organizations { get; set; }

        public string States { get; set; }

    }
}
