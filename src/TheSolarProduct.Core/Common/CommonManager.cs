﻿using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using System.Linq;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Quotations;
using System;
using System.Collections.Generic;
using Abp.Authorization;
using Abp.Configuration;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.Organizations;
using Abp.Runtime.Caching;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.Common
{
    /// <summary>
    /// Common manager.
    /// Used to implement common method logic.
    /// </summary>
    public class CommonManager : AbpUserManager<Role, User>
    {
        private readonly IRepository<QuotationTemplate> _quotationTemplateRepqository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ILocalizationManager _localizationManager;
        private readonly ISettingManager _settingManager;

        public CommonManager(
            UserStore userStore,
            IOptions<IdentityOptions> optionsAccessor,
            IPasswordHasher<User> passwordHasher,
            IEnumerable<IUserValidator<User>> userValidators,
            IEnumerable<IPasswordValidator<User>> passwordValidators,
            ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber errors,
            IServiceProvider services,
            ILogger<UserManager> logger,
            RoleManager roleManager,
            IPermissionManager permissionManager,
            IUnitOfWorkManager unitOfWorkManager,
            ICacheManager cacheManager,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IOrganizationUnitSettings organizationUnitSettings,
            ISettingManager settingManager,
            ILocalizationManager localizationManager,
            IRepository<QuotationTemplate> quotationTemplateRepqository
            )
            : base(
                  roleManager,
                  userStore,
                  optionsAccessor,
                  passwordHasher,
                  userValidators,
                  passwordValidators,
                  keyNormalizer,
                  errors,
                  services,
                  logger,
                  permissionManager,
                  unitOfWorkManager,
                  cacheManager,
                  organizationUnitRepository,
                  userOrganizationUnitRepository,
                  organizationUnitSettings,
                  settingManager)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _settingManager = settingManager;
            _localizationManager = localizationManager;
            _quotationTemplateRepqository = quotationTemplateRepqository;
        }

        public virtual QuotationTemplate GetTemplateAsync(int OrganazationId, int TemplateTypeId)
        {
            var quoteTemplate = _quotationTemplateRepqository.GetAll().Where(e => e.OrganizationUnitId == OrganazationId && e.TemplateTypeId == TemplateTypeId).FirstOrDefault();

            return quoteTemplate;
        }
    }
}
