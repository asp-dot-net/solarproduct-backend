﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Departments
{
	[Table("Departments")]
    public class Department : Entity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			

		[Required]
		[StringLength(DepartmentConsts.MaxNameLength, MinimumLength = DepartmentConsts.MinNameLength)]
		public virtual string Name { get; set; }
		

    }
}