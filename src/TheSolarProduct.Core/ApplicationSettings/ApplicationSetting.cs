﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheSolarProduct.ApplicationSettings
{
    [Table("ApplicationSettings")]
    public class ApplicationSetting : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }
        public virtual bool FoneDynamicsEnabled { get; set; }
        [MaxLength(ApplicationSettingConsts.MaxPhoneNumberLength)]
        public virtual string FoneDynamicsPhoneNumber { get; set; }
        [MaxLength(ApplicationSettingConsts.MaxPropertySidLength)]
        public virtual string FoneDynamicsPropertySid { get; set; }
        [MaxLength(ApplicationSettingConsts.MaxAccountSidLength)]
        public virtual string FoneDynamicsAccountSid { get; set; }
        public virtual string FoneDynamicsToken { get; set; }
    }
}
