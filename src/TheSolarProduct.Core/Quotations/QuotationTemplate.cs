﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheSolarProduct.Quotations
{
    [Table("QuotationTemplates")]
    public class QuotationTemplate : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        
        public long OrganizationUnitId { get; set; }

        [ForeignKey("OrganizationUnitId")]
        public OrganizationUnit OrganizationUnitFk { get; set; }

        public string TemplateName { get; set; }

        public string ViewHtml { get; set; }

        public string ApiHtml { get; set; }

        public bool IsActive { get; set; }

        public int TemplateTypeId { get; set; }

        public string Subject { get; set; }
    }
}
