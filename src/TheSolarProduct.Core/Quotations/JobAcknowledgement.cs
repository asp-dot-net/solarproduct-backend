﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.Quotations
{
	[Table("JobAcknowledgements")]
	public class JobAcknowledgement : FullAuditedEntity, IMustHaveTenant
    {
		public int TenantId { get; set; }

		public virtual string DocType { get; set; }

		public virtual int? JobId { get; set; }

		[ForeignKey("JobId")]
		public Job JobFk { get; set; }

		public virtual string Name { get; set; }

		public virtual string Address { get; set; }

		public virtual string Mobile { get; set; }

		public virtual string Email { get; set; }

		public decimal? Kw { get; set; }
		
		public virtual bool? YN { get; set; }

		public virtual bool IsSigned { get; set; }

		public virtual string SignFilePath { get; set; }

		public virtual string CustSignFileName { get; set; }

		public virtual string CustSignLongitude { get; set; }

		public virtual string CustSignLatitude { get; set; }

		public virtual string CustSignIP { get; set; }

		public virtual DateTime? SignDate { get; set; }
	}
}
