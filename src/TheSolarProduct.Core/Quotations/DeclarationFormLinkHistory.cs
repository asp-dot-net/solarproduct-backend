﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Quotations
{
    [Table("DeclarationFormLinkHistorys")]
    public class DeclarationFormLinkHistory : FullAuditedEntity
    {
        public virtual string Token { get; set; }

        public virtual int? DeclarationFormId { get; set; }

        public virtual bool? Expired { get; set; }
    }
}
