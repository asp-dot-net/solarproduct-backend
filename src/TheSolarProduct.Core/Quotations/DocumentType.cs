﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheSolarProduct.Quotations
{
    [Table("DocumentTypes")]
    public class DocumentType : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual string Title { get; set; }
         public bool IsActive { get; set; }
    }
}
