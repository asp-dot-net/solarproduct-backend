﻿using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.Quotations
{
    [Table("Documents")]
    public class Document : FullAuditedEntity
    {
        public virtual int? DocumentTypeId { get; set; }

        [ForeignKey("DocumentTypeId")]
        public DocumentType DocumentTypeFk { get; set; }
        public Guid? MediaId { get; set; }
        public virtual int? JobId { get; set; }
        public virtual string FileName { get; set; }
        public virtual string FileType { get; set; }
        public virtual string FilePath { get; set; }

        public virtual string NearMapImage1 { get; set; }
        public virtual string NearMapImage2 { get; set; }
        public virtual string NearMapImage3 { get; set; }

        [ForeignKey("JobId")]
        public Job JobFk { get; set; }
    }
}
