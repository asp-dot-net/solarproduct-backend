﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Quotations
{
    [Table("JobAcknowledgementLinkHistorys")]
    public class JobAcknowledgementLinkHistory : FullAuditedEntity
    {
        public virtual string Token { get; set; }

        public virtual int? JobAcknowledgementId { get; set; }
        
        public virtual bool? Expired { get; set; }
    }
}
