﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.Quotations
{
    [Table("QuotationDetails")]
    public class QuotationDetail : FullAuditedEntity
    {
        public int JobId { get; set; }

        [ForeignKey("JobId")]
        public Job JobFk { get; set; }

        public int? QuotationId { get; set; }

        [ForeignKey("QuotationId")]
        public Quotation QuotationFk { get; set; }

        public virtual string QuoteNumber { get; set; }

        public virtual string Date { get; set; }

        public virtual string Name { get; set; }

        public virtual string Mobile { get; set; }

        public virtual string Email { get; set; }

        public virtual string Address1 { get; set; }

        public virtual string Address2 { get; set; }

        public string SystemWith { get; set; }

        public virtual string Capecity { get; set; }

        public virtual string BatteryCapecity { get; set; }

        public virtual string Notes { get; set; }

        public virtual string InstName { get; set; }

        public virtual string InstMobile { get; set; }

        public virtual string InstEmail { get; set; }

        public virtual string SubTotal { get; set; }

        public virtual string STC { get; set; }

        public virtual string STCDesc { get; set; }

        public virtual string GrandTotal { get; set; }

        public virtual string DepositeText { get; set; }

        public virtual string Deposite { get; set; }

        public virtual string Total { get; set; }

        public virtual string Balance { get; set; }

        public virtual string MeaterPhase { get; set; }

        public virtual string PaymentMethod { get; set; }

        public virtual string SpecialDiscount { get; set; }

        public virtual string AdditionalCharges { get; set; }

        public virtual string MeaterUpgrade { get; set; }

        public virtual string NoofStory { get; set; }

        public virtual string RoofTypes { get; set; }

        public virtual string RoofPinch { get; set; }

        public virtual string EnergyDist { get; set; }

        public virtual string EnergyRetailer { get; set; }

        public virtual string NMINumber { get; set; }

        public virtual string MeterNo { get; set; }

        public virtual string NearMap1 { get; set; }

        public virtual string NearMap2 { get; set; }

        public virtual string NearMap3 { get; set; }

        public virtual string DepositRequired { get; set; }

        public virtual string STCIncetive { get; set; }

        public virtual string SolarVICRebate { get; set; }

        public virtual string SolarVICLoanDiscount { get; set; }

        public virtual string FinalPrice { get; set; }

        public virtual string InverterLocation { get; set; }

        public virtual string VICRebate { get; set; }

        public virtual string TotalCost { get; set; }

		public decimal? BatteryRebate { get; set; }
    }

    [Table("QuotationQunityAndModelDetails")]
    public class QuotationQunityAndModelDetail : FullAuditedEntity
    {
        public int QuotationDetailId { get; set; }

        public virtual string Name { get; set; }
    }
}