﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.Quotations
{
	[Table("Quotations")]
	public class Quotation : FullAuditedEntity, IMustHaveTenant
	{
		public virtual DateTime QuoteDate { get; set; }

		public virtual DateTime? QuoteAcceptDate { get; set; }

		public virtual bool IsSigned { get; set; }

		public virtual Guid? DocumentId { get; set; }

		public virtual int? JobId { get; set; }

		[ForeignKey("JobId")]
		public Job JobFk { get; set; }

		public virtual string QuoteFileName { get; set; }

		public virtual string SignedQuoteFileName { get; set; }

		public virtual string CustSignFileName { get; set; }

        public virtual string CustSignFilePath { get; set; }

        public virtual string CustSignLongitude { get; set; }

		public virtual string CustSignLatitude { get; set; }

		public virtual string CustSignIP { get; set; }

		public virtual string QuoteFilePath { get; set; }

		public int TenantId { get; set; }

		public virtual string QuoteNumber { get; set; }

		public decimal? BatteryRebate { get; set; }
	}
}
