﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Quotations
{
    [Table("DocumentLibrarys")]
    public class DocumentLibrary : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual string Title { get; set; }
        public virtual string FileName { get; set; }

        public virtual string FileType { get; set; }

        public virtual string FilePath { get; set; }
    }
    
}
