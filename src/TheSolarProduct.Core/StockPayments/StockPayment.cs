﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Currencies;
using TheSolarProduct.PaymentMethods;

namespace TheSolarProduct.StockPayments
{
    [Table("StockPayments")]
    public class StockPayment : FullAuditedEntity
    {
        public int OrganizationId { get; set; }

        public long StockNo { get; set; }

        public int? PaymentMethodId { get; set; }

        [ForeignKey("PaymentMethodId")]
        public PaymentMethod PaymentMethodFK { get; set; }    

        public DateTime? PaymentDate { get; set; }

        public int? CurrencyId { get; set; }

        [ForeignKey("CurrencyId")]
        public Currency CurrencyFk { get; set; }

        public int? GSTType { get; set; }

        public decimal? Rate { get; set; }

        public decimal? PaymentAmount { get; set; }

        public decimal? AUD { get; set; }

        public decimal? USD { get; set; }

        public  string FileName { get; set; }

        public  string FilePath { get; set; }
    }
}
