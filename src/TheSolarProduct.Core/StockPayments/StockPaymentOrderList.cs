﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.StockOrders;

namespace TheSolarProduct.StockPayments
{
    [Table("StockPaymentOrderLists")]
    public class StockPaymentOrderList : FullAuditedEntity
    {
        public int? StockPaymentId { get; set; }

        [ForeignKey("StockPaymentId")]
        public StockPayment StockPaymentFK { get; set; }

        public int? PurchaseOrderId { get; set; }
        [ForeignKey("PurchaseOrderId")]
        public PurchaseOrder PurchaseOrderFK { get; set; }

        public decimal Amount { get; set; }

    }
}
