﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.CallHistory
{
    [Table("CallTypes")]
    public class CallType : FullAuditedEntity
    {
        public string Name { get; set; }

        public string ColorClass { get; set; }

        public string IconClass { get; set; }
    }
}
