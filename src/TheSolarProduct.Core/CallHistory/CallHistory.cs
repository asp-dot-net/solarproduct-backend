﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.CallHistory
{
    [Table("CallHistory")]
    public class CallHistory : FullAuditedEntity
    {
        public string CallType { get; set; }

        public string Number { get; set; }

        public string CallDirection { get; set; }

        public string Name { get; set; }

        public string EntityId { get; set; }

        public string  EntityType { get; set; }

        public string  Agent { get; set; }
        
        public string AgentFirstName { get; set; }
        
        public string AgentLastName { get; set; }
        
        public string AgentEmail { get; set; }
        
        public string Duration { get; set; }
        
        public string DurationTimeSpan { get; set; }
        
        public DateTime DateTime { get; set; }

        public DateTime CallStartTimeLocal { get; set; }
        
        public DateTime CallStartTimeUTC { get; set; }
        
        public DateTime CallEstablishedTimeLocal { get; set; }
        
        public DateTime CallEstablishedTimeUTC { get; set; }
        
        public DateTime CallEndTimeLocal { get; set; }
        
        public DateTime CallEndTimeUTC { get; set; }
        
        public string CallStartTimeLocalMillis { get; set; }

        public string CallStartTimeUTCMillis { get; set; }

        public string CallEstablishedTimeLocalMillis { get; set; }

        public string CallEstablishedTimeUTCMillis { get; set; }

        public string CallEndTimeLocalMillis { get; set; }

        public string CallEndTimeUTCMillis { get; set; }

        public string QueueExtension { get; set; }
    }
}
