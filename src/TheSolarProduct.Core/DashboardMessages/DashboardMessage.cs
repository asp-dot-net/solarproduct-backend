﻿using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheSolarProduct.DashboardMessages
{
	[Table("DashboardMessages")]
    public class DashboardMessage : FullAuditedEntity, IMustHaveTenant
    {
		public int TenantId { get; set; }

        public int? OrgId { get; set; }

        public string Message { get; set;}

        public bool IsActive { get; set; }

        public string DashboardType { get; set; }


    }
}
