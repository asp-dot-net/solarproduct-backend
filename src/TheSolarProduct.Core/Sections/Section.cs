﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Sections
{
    [Table("Section")]
    public class Section : FullAuditedEntity
    {
        public virtual string SectionName { get; set; }
        public virtual int SectionId { get; set; }
    }
    
}
