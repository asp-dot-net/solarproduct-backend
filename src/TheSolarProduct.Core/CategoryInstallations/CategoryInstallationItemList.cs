﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.InstallationCost.InstallationItemLists;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.CategoryInstallations
{
    [Table("CategoryInstallationItemLists")]
    public class CategoryInstallationItemList : FullAuditedEntity
    {
        [ForeignKey("CategoryInstallationItemPeriodId")]
        public CategoryInstallationItemPeriod CategoryInstallationItemPeriodFk { get; set; }

        public virtual int CategoryInstallationItemPeriodId { get; set; }

        public virtual string CategoryName { get; set; }

        public virtual int? StartValue { get; set; }

        public virtual int? EndValue { get; set; }
    }
}
