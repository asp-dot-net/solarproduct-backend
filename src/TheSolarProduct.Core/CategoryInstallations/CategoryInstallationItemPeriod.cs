﻿using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.CategoryInstallations
{
    [Table("CategoryInstallationItemPeriods")]
    public class CategoryInstallationItemPeriod : FullAuditedEntity
    {
        [ForeignKey("OrganizationUnit")]
        public OrganizationUnit OrganizationUnitFk { get; set; }

        public virtual long OrganizationUnit { get; set; }

        public virtual string Name { get; set; }

        public virtual DateTime StartDate { get; set; }

        public virtual DateTime EndDate { get; set; }
    }
}
