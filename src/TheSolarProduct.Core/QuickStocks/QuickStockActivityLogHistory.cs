﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.DataVaults;

namespace TheSolarProduct.QuickStocks
{
    
    [Table("QuickStockActivityLogHistory")]
    public class QuickStockActivityLogHistory : FullAuditedEntity
    {
        public virtual string FieldName { get; set; }

        public virtual string PrevValue { get; set; }

        public virtual string CurValue { get; set; }

        public virtual string Action { get; set; }

        public int? ActivityLogId { get; set; }

        [ForeignKey("SectionId")]
        public QuickStockActivityLog QuickStockActivityLogFK { get; set; }


    }
}
