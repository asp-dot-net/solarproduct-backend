﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.StockOrders;
using TheSolarProduct.StockOrders.SerialNoStatuses;

namespace TheSolarProduct.QuickStocks
{
    [Table("StockSerialNoLogHistorys")]
    public class StockSerialNoLogHistory : FullAuditedEntity
    {
        public int? StockSerialNoId { get; set; }

        [ForeignKey("StockSerialNoId")]
        public StockSerialNo StockSerialNoFk { get; set; }

        public int? SerialNoStatusId { get; set; }

        [ForeignKey("SerialNoStatusId")]
        public SerialNoStatus SerialNoStatusFk { get; set; }

        public virtual string ActionNote { get; set; }

        public bool IsApp {  get; set; }
    }
}
