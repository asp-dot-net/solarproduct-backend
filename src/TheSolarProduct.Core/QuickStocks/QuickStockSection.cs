﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.QuickStocks
{
   
    [Table("QuickStockSections")]
    public class QuickStockSection : FullAuditedEntity
    {
        public virtual string SectionName { get; set; }
    }
}
