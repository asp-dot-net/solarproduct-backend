﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.DataVaults;
using TheSolarProduct.LeadActions;
using TheSolarProduct.StockOrders;
using TheSolarProduct.WholeSaleLeads;
using Twilio.TwiML.Messaging;

namespace TheSolarProduct.QuickStocks
{
    
    [Table("QuickStockActivityLogs")]
    public class QuickStockActivityLog : FullAuditedEntity
    {
        public int? PurchaseOrderId { get; set; }

        [ForeignKey("PurchaseOrderId")]
        public PurchaseOrder PurchaseOrderFk { get; set; }

        public int? StockTransferId { get; set; }

        [ForeignKey("StockTransferId")]
        public StockTransfer StockTransferFK { get; set; }

        public string Type { get; set; }

        public string Action { get; set; }

        public virtual string ActionNote { get; set; }

        public int? SectionId { get; set; }

        [ForeignKey("SectionId")]
        public QuickStockSection SectionFK { get; set; }

        public int? ActionId { get; set; }

        [ForeignKey("ActionId")]
        public LeadAction ActionFk { get; set; }

        public virtual DateTime? ActivityDate { get; set; }

        public virtual string Body { get; set; }

        public virtual string MessageId { get; set; }

        public virtual int? ReferanceId { get; set; }

        public virtual int? TemplateId { get; set; }

        public virtual string Subject { get; set; }
        public int TenantId { get; set; }
        public bool IsApp {  get; set; }
      


    }
}
