﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.QuickStocks
{
    [Table("AppDocuments")]
    public class AppDocument : FullAuditedEntity
    {
        public int DocumentTypeId { get; set; }

        public string DocumentName { get; set; }

        public string DocumentPath { get; set; }

        public int DocumentId { get; set; }
    }
}
