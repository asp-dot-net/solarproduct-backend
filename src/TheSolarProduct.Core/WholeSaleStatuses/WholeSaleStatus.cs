﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.WholeSaleStatuses
{
	[Table("WholeSaleStatuses")]
    public class WholeSaleStatus : FullAuditedEntity
    {
        public string Name { get; set; }    
    }
}
