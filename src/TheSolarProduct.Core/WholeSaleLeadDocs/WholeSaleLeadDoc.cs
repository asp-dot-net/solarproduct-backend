﻿using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Services;
using TheSolarProduct.WholeSaleLeads;

namespace TheSolarProduct.WholeSaleLeadDocs
{
    [Table("WholeSaleLeadDocs")]
    public class WholeSaleLeadDoc : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual int? WholeSaleLeadId { get; set; }

        [ForeignKey("WholeSaleLeadId")]
        public WholeSaleLead WholeSaleLeadFk { get; set; }
        public virtual string FileName { get; set; }

        public virtual string FileType { get; set; }

        public virtual string FilePath { get; set; }

        public virtual int? WholeSaleLeadDocumentTypeId { get; set; }

        [ForeignKey("WholeSaleLeadDocumentTypeId")]
        public WholeSaleLeadDocumentType WholeSaleDocumentTypeFk { get; set; }

    }
}
