﻿using System.Collections.Generic;

namespace TheSolarProduct.MultiTenancy.Payments
{
    public interface IPaymentGatewayStore
    {
        List<PaymentGatewayModel> GetActiveGateways();
    }
}
