﻿using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Notifications;
using Abp.Organizations;
using Abp.Runtime.Security;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Leads;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.Notifications;
using TheSolarProduct.Promotions;
using TheSolarProduct.Organizations;
using TheSolarProduct.GreenBoatDatatable;
using TheSolarProduct.Jobs;
using TheSolarProduct.PVDStatuses;
using Abp.Timing.Timezone;

namespace TheSolarProduct.SysJobs
{
    public class ClientDomainService : TheSolarProductServiceBase, IClientDomainService
    {
        private readonly IRepository<Job> _jobRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly UserManager _userManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IUserEmailer _userEmailer;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<ApplicationSetting> _applicationSettingRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<Promotion> _promotionRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<PromotionUser> _promotionUserRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendOrganizationUnitRepository;
        private readonly IRepository<OrganizationUnit, long> _OrganizationUnitRepository;
        private readonly IRepository<GreenBoatData> _greenBoatDataRepository;
        private readonly IRepository<GreenBoatSerialNumber> _GreenBoatSerialNumberRepository;
        private readonly IRepository<PVDStatus, int> _pVDStatusRepository;
        private readonly IRepository<WestPackData> _westpackdataRepository;
        private readonly IRepository<WestPackPrincipalAmount> _westpackprincipleRepository;
        private readonly IRepository<WestPackSurchargeAmount> _westpacksurchargeRepository;
        private readonly IRepository<WestPackTotalAmount> _westpacktotalRepository;
        
        public ClientDomainService(IAppNotifier appNotifier, IRepository<Job> jobRepository
            , UserManager userManager
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<Lead> leadRepository
            , IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository
            , IRepository<User, long> userRepository
            , IRepository<UserRole, long> userroleRepository
            , IRepository<Role> roleRepository
            , IUserEmailer userEmailer
            , IRepository<Tenant> tenantRepository
            , IRepository<ApplicationSetting> applicationSettingRepository
            , IRepository<OrganizationUnit, long> organizationUnitRepository
            , IRepository<Promotion> promotionRepository
            , IRepository<LeadActivityLog> leadactivityRepository
            , IRepository<PromotionUser> promotionUserRepository
            , IRepository<ExtendOrganizationUnit, long> extendOrganizationUnitRepository
            , IRepository<GreenBoatData> GreenBoatDataRepository
            , IRepository<OrganizationUnit, long> OrganizationUnitRepository
            , IRepository<GreenBoatSerialNumber> GreenBoatSerialNumberRepository
            , IRepository<PVDStatus, int> pVDStatusRepository
            , IRepository<WestPackData> westpackdataRepository
            , IRepository<WestPackPrincipalAmount> westpackprincipleRepository
            , IRepository<WestPackSurchargeAmount> westpacksurchargeRepository
            , IRepository<WestPackTotalAmount> westpacktotalRepository,
            ITimeZoneConverter timeZoneConverter)
        {
            _appNotifier = appNotifier;
            _jobRepository = jobRepository;
            _userManager = userManager;
            _unitOfWorkManager = unitOfWorkManager;
            _leadRepository = leadRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _userRepository = userRepository;
            _userroleRepository = userroleRepository;
            _roleRepository = roleRepository;
            _userEmailer = userEmailer;
            _tenantRepository = tenantRepository;
            _applicationSettingRepository = applicationSettingRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _promotionRepository = promotionRepository;
            _leadactivityRepository = leadactivityRepository;
            _promotionUserRepository = promotionUserRepository;
            _extendOrganizationUnitRepository = extendOrganizationUnitRepository;
            _OrganizationUnitRepository = OrganizationUnitRepository;
            _greenBoatDataRepository = GreenBoatDataRepository;
            _GreenBoatSerialNumberRepository = GreenBoatSerialNumberRepository;
            _pVDStatusRepository = pVDStatusRepository;
            _westpackdataRepository = westpackdataRepository;
            _westpackprincipleRepository = westpackprincipleRepository;
            _westpacksurchargeRepository = westpacksurchargeRepository;
            _westpacktotalRepository = westpacktotalRepository;
            
        }

        public async Task NotifyLeadAsync()
        {
            using (_unitOfWorkManager.Begin())
            {
                var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
                for (int i = 0; i < tenant.Count; i++)
                {
                    using (_unitOfWorkManager.Current.SetTenantId(tenant[i].Id))
                    {
                        var OrganizationList = _leadRepository.GetAll()
                                    .Where(e => e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true)
                                    .Select(e => e.OrganizationId).Distinct().ToList();

                        foreach (var item in OrganizationList)
                        {
                            var OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == item).Select(e => e.DisplayName).FirstOrDefault();
                            var LeadList = _leadRepository.GetAll()
                                    .Where(e => e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == item)
                                    .ToList();

                            var User_List = _userRepository.GetAll();

                            var UserList = (from user in User_List
                                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                            from ur in urJoined.DefaultIfEmpty()
                                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                            from us in usJoined.DefaultIfEmpty()
                                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                                            from uo in uoJoined.DefaultIfEmpty()
                                            where (us != null && us.DisplayName == "Lead Operator" && uo != null && uo.OrganizationUnitId == item)
                                            select user).ToList();

                            //Send Notification
                            try
                            {
                                string msg = string.Format("{0} Lead Pending To Assigne From {1} Organization, Please Take Action on it.", LeadList.Count, OrganizationName);
                                await _appNotifier.LeadAssigedList(UserList, msg, NotificationSeverity.Info);
                            }
                            catch (Exception ex)
                            {

                            }

                            //Send Email
                            //try
                            //{
                            //    //await _userEmailer.SendEmailPendingLeadReminderList(UserList, LeadList.Count, OrganizationName);
                            //}
                            //catch (Exception ex)
                            //{

                            //}

                            //Send SMS
                            try
                            {
                                string msg = string.Format("{0} Lead Pending To Assigne From {1} Organization, Please Take Action on it.", LeadList.Count, OrganizationName);

                                //foreach (var item1 in UserList)
                                //{
                                //    try
                                //    {
                                //        SendSMSRequestInput smsReq = new SendSMSRequestInput();

                                //        var IsSettingAvailable = _applicationSettingRepository.GetAll().Where(e => e.TenantId == tenant[i].Id).FirstOrDefault();
                                //        if (IsSettingAvailable != null)
                                //        {
                                //            var applicationSetting = _applicationSettingRepository.FirstOrDefault(X => X.TenantId == tenant[i].Id);
                                //            string authCode = applicationSetting.FoneDynamicsAccountSid + ":" + applicationSetting.FoneDynamicsToken;
                                //            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(authCode);
                                //            string headerToken = "BASIC " + System.Convert.ToBase64String(data);
                                //            smsReq.From = applicationSetting.FoneDynamicsPhoneNumber;
                                //            smsReq.To = item1.PhoneNumber;
                                //            smsReq.Text = msg;
                                //            string restURL = "https://api.fonedynamics.com/v2/Properties/" + applicationSetting.FoneDynamicsPropertySid + "/Messages";
                                //            using (var httpClient = new HttpClient())
                                //            {
                                //                StringContent content = new StringContent(JsonConvert.SerializeObject(smsReq), Encoding.UTF8, "application/json");
                                //                httpClient.DefaultRequestHeaders.Add("Authorization", headerToken);
                                //                using (var response = await httpClient.PostAsync(restURL, content))
                                //                {
                                //                    string apiResponse = await response.Content.ReadAsStringAsync();
                                //                    //test
                                //                }
                                //            }
                                //        }
                                //    }
                                //    catch (Exception)
                                //    {

                                //        throw;
                                //    }
                                //}
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
            }
        }

        public async Task SendPromotion(int TenantId, int PromotionId, int UserId)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (_unitOfWorkManager.Current.SetTenantId(TenantId))
                {
                    int i = 0;
                    var PromotionDetail = _promotionRepository.GetAll().Where(e => e.Id == PromotionId).FirstOrDefault();
                    var PromotionUserData = _promotionUserRepository.GetAll().Where(e => e.PromotionId == PromotionId).ToList();

                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == UserId).FirstOrDefault();

                    foreach (var Promotion in PromotionUserData)
                    {
                        if (Promotion.LeadId > 0)
                        {
                            try
                            {
                                // Add Activity								
                                LeadActivityLog leadactivity = new LeadActivityLog();
                                leadactivity.ActionId = 21;
                                leadactivity.SectionId = 0;
                                leadactivity.ActionNote = "Promotion " + PromotionDetail.Title + " has been sent";
                                leadactivity.LeadId = (int)Promotion.LeadId;
                                leadactivity.Body = PromotionDetail.Description;
                                leadactivity.TenantId = TenantId;
                                leadactivity.CreatorUserId = UserId;
                                int promoActivityID = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                                var mobilenumber = _leadRepository.GetAll().Where(e => e.Id == Promotion.LeadId).FirstOrDefault().Mobile;
                                var Email = _leadRepository.GetAll().Where(e => e.Id == Promotion.LeadId).FirstOrDefault().Email;

                                //Send SMS
                                if (PromotionDetail.PromotionTypeId == 1)
                                {
                                    if (!string.IsNullOrEmpty(mobilenumber))
                                    {
                                        try
                                        {
                                            //SendSMSInput sendSMSInput = new SendSMSInput();
                                            //sendSMSInput.PhoneNumber = mobilenumber;
                                            //sendSMSInput.Text = PromotionDetail.Description;
                                            //sendSMSInput.ActivityId = promoActivityID;
                                            //sendSMSInput.promoresponseID = Promotion.Id;
                                            //await _applicationSettings.SendSMS(sendSMSInput);
                                        }
                                        catch (Exception e)
                                        {

                                        }
                                    }
                                }
                                //Send Emails
                                if (PromotionDetail.PromotionTypeId == 2)
                                {
                                    if (!string.IsNullOrEmpty(Email))
                                    {
                                        var KeyValue = TenantId + "," + Promotion.Id;

                                        //Token With Tenant Id & Promotion User Primary Id for subscribe & UnSubscribe
                                        var token = HttpUtility.UrlEncode(SimpleStringCipher.Instance.Encrypt(KeyValue, "gsKxGZ012HLL3MI5"));

                                        //Subscription Link
                                        string SubscribeLink = "http://thesolarproduct.com/account/customer-subscribe?STR=" + token;

                                        //UnSubscription Link
                                        string UnSubscribeLink = "http://thesolarproduct.com/account/customer-unsubscribe?STR=" + token;

                                        //Footer Design for subscribe & UnSubscribe
                                        var footer = "<div style=\"border:1px solid #ccc;padding:10px; background:#f2f2f2;\">If you are intrested in this offer, please click <a href=\"" + SubscribeLink + "\">" +
                                            "<img height=\"20px\" src=\"http://thesolarproduct.com/assets/common/images/like.png\"></a>.<br/>" +
                                            "If you do not wish to receive any further communications, please click <a href=\"" + UnSubscribeLink + "\">" +
                                            "<img height=\"20px\" src=\"http://thesolarproduct.com/assets/common/images/dislike.png\"></a>.</div>";

                                        //Merge Email Body And Footer for subscribe & UnSubscribe
                                        string FinalEmailBody = PromotionDetail.Description + footer;
                                        try
                                        {
                                            //MailMessage mail = new MailMessage
                                            //{
                                            //	From = new MailAddress(UserDetail.EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                                            //	To = { Email },
                                            //	Subject = PromotionDetail.Title,
                                            //	Body = FinalEmailBody,
                                            //	IsBodyHtml = true
                                            //};
                                            //await this._emailSender.SendAsync(mail);
                                        }
                                        catch (Exception e)
                                        {

                                        }
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                i = i++;
                            }
                        }
                    }
                }
                uow.Complete();
            }
        }

        //public async Task getGreenBoatData()
        //{
        //    try
        //    {

        //        #region Old COde
        //        //using (var uow = _unitOfWorkManager.Begin())
        //        //{
        //        //    using (_unitOfWorkManager.Current.SetTenantId(6))
        //        //    {
        //        //        var oldgbdata = _GreenBoatDataRepository.GetAll().OrderBy(p => p.Id).ToList();

        //        //        foreach (var detaildt in oldgbdata)
        //        //        {
        //        //            using (var uow2 = _unitOfWorkManager.Begin())
        //        //            {
        //        //                await _GreenBoatDataRepository.HardDeleteAsync(detaildt);
        //        //                uow2.Complete();
        //        //            }
        //        //        }

        //        //        var organizationdetail = _organizationUnitRepository.GetAll().ToList();
        //        //        if (organizationdetail.Count > 0)
        //        //        {
        //        //            foreach (var dt in organizationdetail)
        //        //            {
        //        //                var item = _extendOrganizationUnitRepository.GetAll().Where(e => e.Id == dt.Id).FirstOrDefault();
        //        //                if (item.GreenBoatUsernameForFetch != null)
        //        //                {
        //        //                    var client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/Login");
        //        //                    var request = new RestRequest(Method.POST);
        //        //                    request.AddParameter("Username", item.GreenBoatUsernameForFetch);
        //        //                    request.AddParameter("Password", item.GreenBoatPasswordForFetch);
        //        //                    var response = client.Execute(request);
        //        //                    RootObject JobDetails = new RootObject();
        //        //                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
        //        //                    {
        //        //                        RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(response.Content);
        //        //                        string AccessToken = RootObject.TokenData.access_token; // get Login Access Token
        //        //                        string str = "01/01/2021";
        //        //                        string date = DateTime.Now.Date.ToString("dd/MM/yyyy");

        //        //                        client = new RestClient("https://api.greenbot.com.au/api/Account/GetJobs?FromDate=" + str + "&ToDate=" + date);
        //        //                        request = new RestRequest(Method.GET);
        //        //                        client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", AccessToken));
        //        //                        client.AddDefaultHeader("Content-Type", "application/json");
        //        //                        client.AddDefaultHeader("Accept", "application/json");

        //        //                        IRestResponse<Job_Response.RootObject> JsonData = client.Execute<Job_Response.RootObject>(request);
        //        //                        if (JsonData.StatusCode == System.Net.HttpStatusCode.OK)
        //        //                        {
        //        //                            JobDetails = JsonConvert.DeserializeObject<Job_Response.RootObject>(JsonData.Content);

        //        //                            if (JobDetails.lstJobData.Count > 0)
        //        //                            {
        //        //                                //  var oldgbdata = _GreenBoatDataRepository.GetAll().OrderBy(p => p.Id).ToList();

        //        //                                foreach (var detaildt in JobDetails.lstJobData)
        //        //                                {
        //        //                                    using (var uow1 = _unitOfWorkManager.Begin())
        //        //                                    {
        //        //                                        using (CurrentUnitOfWork.SetTenantId(6))
        //        //                                        {
        //        //                                            GreenBoatData gbdata = new GreenBoatData();
        //        //                                            gbdata.TenantId = 6;

        //        //                                            gbdata.CreationTime = DateTime.Now;
        //        //                                            gbdata.IsDeleted = false;
        //        //                                            gbdata.ProjectNo = detaildt.BasicDetails.JobID;
        //        //                                            gbdata.VendorJobId = detaildt.BasicDetails.VendorJobId;
        //        //                                            gbdata.JobID = detaildt.BasicDetails.JobID;
        //        //                                            gbdata.Title = detaildt.BasicDetails.Title;
        //        //                                            gbdata.RefNumber = detaildt.BasicDetails.RefNumber.Trim();
        //        //                                            gbdata.Description = detaildt.BasicDetails.Description;
        //        //                                            gbdata.JobType = detaildt.BasicDetails.JobType;
        //        //                                            gbdata.JobStage = detaildt.BasicDetails.JobStage;
        //        //                                            gbdata.Priority = detaildt.BasicDetails.Priority;
        //        //                                            gbdata.InstallationDate = detaildt.BasicDetails.InstallationDate;
        //        //                                            gbdata.CreatedDate = detaildt.BasicDetails.CreatedDate;


        //        //                                            gbdata.OwnerType_JOD = detaildt.JobOwnerDetails.OwnerType;
        //        //                                            gbdata.CompanyName_JOD = detaildt.JobOwnerDetails.CompanyName;
        //        //                                            gbdata.FirstName_JOD = detaildt.JobOwnerDetails.FirstName;
        //        //                                            gbdata.LastName_JOD = detaildt.JobOwnerDetails.LastName;
        //        //                                            gbdata.Phone_JOD = detaildt.JobOwnerDetails.Phone;
        //        //                                            gbdata.Mobile_JOD = detaildt.JobOwnerDetails.Mobile;
        //        //                                            gbdata.Email_JOD = detaildt.JobOwnerDetails.Email;
        //        //                                            gbdata.IsPostalAddress_JOD = detaildt.JobOwnerDetails.IsPostalAddress;
        //        //                                            gbdata.UnitTypeID_JOD = detaildt.JobOwnerDetails.UnitTypeID;
        //        //                                            gbdata.UnitNumber_JOD = detaildt.JobOwnerDetails.UnitNumber;
        //        //                                            gbdata.StreetNumber_JOD = detaildt.JobOwnerDetails.StreetNumber;
        //        //                                            gbdata.StreetName_JOD = detaildt.JobOwnerDetails.StreetName;
        //        //                                            gbdata.StreetTypeID_JOD = detaildt.JobOwnerDetails.StreetTypeID;
        //        //                                            gbdata.Town_JOD = detaildt.JobOwnerDetails.Town;
        //        //                                            gbdata.State_JOD = detaildt.JobOwnerDetails.State;
        //        //                                            gbdata.PostCode_JOD = detaildt.JobOwnerDetails.PostCode;

        //        //                                            gbdata.DistributorID_JID = detaildt.JobInstallationDetails.DistributorID;
        //        //                                            gbdata.MeterNumber_JID = detaildt.JobInstallationDetails.MeterNumber;
        //        //                                            gbdata.PhaseProperty_JID = detaildt.JobInstallationDetails.PhaseProperty;
        //        //                                            gbdata.ElectricityProviderID_JID = detaildt.JobInstallationDetails.ElectricityProviderID;
        //        //                                            gbdata.IsSameAsOwnerAddress_JID = detaildt.JobInstallationDetails.IsSameAsOwnerAddress;
        //        //                                            gbdata.IsPostalAddress_JID = detaildt.JobInstallationDetails.IsPostalAddress;
        //        //                                            gbdata.UnitTypeID_JID = detaildt.JobInstallationDetails.UnitTypeID;
        //        //                                            gbdata.UnitNumber_JID = detaildt.JobInstallationDetails.UnitNumber;
        //        //                                            gbdata.StreetNumber_JID = detaildt.JobInstallationDetails.StreetNumber;
        //        //                                            gbdata.StreetName_JID = detaildt.JobInstallationDetails.StreetName;
        //        //                                            gbdata.StreetTypeID_JID = detaildt.JobInstallationDetails.StreetTypeID;
        //        //                                            gbdata.Town_JID = detaildt.JobInstallationDetails.Town;
        //        //                                            gbdata.State_JID = detaildt.JobInstallationDetails.State;
        //        //                                            gbdata.PostCode_JID = detaildt.JobInstallationDetails.PostCode;
        //        //                                            gbdata.NMI_JID = detaildt.JobInstallationDetails.NMI;
        //        //                                            gbdata.SystemSize_JID = detaildt.JobInstallationDetails.SystemSize;
        //        //                                            gbdata.PropertyType_JID = detaildt.JobInstallationDetails.PropertyType;
        //        //                                            gbdata.SingleMultipleStory_JID = detaildt.JobInstallationDetails.SingleMultipleStory;
        //        //                                            gbdata.InstallingNewPanel_JID = detaildt.JobInstallationDetails.InstallingNewPanel;
        //        //                                            gbdata.Location_JID = detaildt.JobInstallationDetails.Location;
        //        //                                            gbdata.ExistingSystem_JID = detaildt.JobInstallationDetails.ExistingSystem;
        //        //                                            gbdata.ExistingSystemSize_JID = detaildt.JobInstallationDetails.ExistingSystemSize;
        //        //                                            gbdata.SystemLocation_JID = detaildt.JobInstallationDetails.SystemLocation;
        //        //                                            gbdata.NoOfPanels_JID = detaildt.JobInstallationDetails.NoOfPanels;
        //        //                                            gbdata.AdditionalInstallationInformation_JID = detaildt.JobInstallationDetails.AdditionalInstallationInformation;

        //        //                                            gbdata.SystemSize_JSD = detaildt.JobSystemDetails.SystemSize;
        //        //                                            gbdata.SerialNumbers_JSD = detaildt.JobSystemDetails.SerialNumbers;
        //        //                                            gbdata.NoOfPanel_JSD = detaildt.JobSystemDetails.NoOfPanel;
        //        //                                            gbdata.AdditionalCapacityNotes_JSTC = detaildt.JobSTCDetails.AdditionalCapacityNotes;
        //        //                                            gbdata.TypeOfConnection_JSTC = detaildt.JobSTCDetails.TypeOfConnection;
        //        //                                            gbdata.SystemMountingType_JSTC = detaildt.JobSTCDetails.SystemMountingType;
        //        //                                            gbdata.DeemingPeriod_JSTC = detaildt.JobSTCDetails.DeemingPeriod;
        //        //                                            gbdata.CertificateCreated_JSTC = detaildt.JobSTCDetails.CertificateCreated;
        //        //                                            gbdata.FailedAccreditationCode_JSTC = detaildt.JobSTCDetails.FailedAccreditationCode;
        //        //                                            gbdata.FailedReason_JSTC = detaildt.JobSTCDetails.FailedReason;
        //        //                                            gbdata.MultipleSGUAddress_JSTC = detaildt.JobSTCDetails.MultipleSGUAddress;
        //        //                                            gbdata.Location_JSTC = detaildt.JobSTCDetails.Location;
        //        //                                            gbdata.AdditionalLocationInformation_JSTC = detaildt.JobSTCDetails.AdditionalLocationInformation;
        //        //                                            gbdata.AdditionalSystemInformation_JSTC = detaildt.JobSTCDetails.AdditionalSystemInformation;

        //        //                                            gbdata.FirstName_IV = detaildt.InstallerView.FirstName;
        //        //                                            gbdata.LastName_IV = detaildt.InstallerView.LastName;
        //        //                                            gbdata.Email_IV = detaildt.InstallerView.Email;
        //        //                                            gbdata.Phone_IV = detaildt.InstallerView.Phone;
        //        //                                            gbdata.Mobile_IV = detaildt.InstallerView.Mobile;
        //        //                                            gbdata.CECAccreditationNumber_IV = detaildt.InstallerView.CECAccreditationNumber;
        //        //                                            gbdata.ElectricalContractorsLicenseNumber_IV = detaildt.InstallerView.ElectricalContractorsLicenseNumber;
        //        //                                            gbdata.IsPostalAddress_IV = detaildt.InstallerView.IsPostalAddress;
        //        //                                            gbdata.UnitTypeID_IV = detaildt.InstallerView.UnitTypeID;
        //        //                                            gbdata.UnitNumber_IV = detaildt.InstallerView.UnitNumber;
        //        //                                            gbdata.StreetNumber_IV = detaildt.InstallerView.StreetNumber;
        //        //                                            gbdata.StreetName_IV = detaildt.InstallerView.StreetName;
        //        //                                            gbdata.StreetTypeID_IV = detaildt.InstallerView.StreetTypeID;
        //        //                                            gbdata.Town_IV = detaildt.InstallerView.Town;
        //        //                                            gbdata.State_IV = detaildt.InstallerView.State;
        //        //                                            gbdata.PostCode_IV = detaildt.InstallerView.PostCode;


        //        //                                            gbdata.FirstName_DV = detaildt.DesignerView.FirstName;
        //        //                                            gbdata.LastName_DV = detaildt.DesignerView.LastName;
        //        //                                            gbdata.Email_DV = detaildt.DesignerView.Email;
        //        //                                            gbdata.Phone_DV = detaildt.DesignerView.Phone;
        //        //                                            gbdata.Mobile_DV = detaildt.DesignerView.Mobile;
        //        //                                            gbdata.CECAccreditationNumber_DV = detaildt.DesignerView.CECAccreditationNumber;
        //        //                                            gbdata.ElectricalContractorsLicenseNumber_DV = detaildt.DesignerView.ElectricalContractorsLicenseNumber;
        //        //                                            gbdata.IsPostalAddress_DV = detaildt.DesignerView.IsPostalAddress;
        //        //                                            gbdata.UnitTypeID_DV = detaildt.DesignerView.UnitTypeID;
        //        //                                            gbdata.UnitNumber_DV = detaildt.DesignerView.UnitNumber;
        //        //                                            gbdata.StreetNumber_DV = detaildt.DesignerView.StreetNumber;
        //        //                                            gbdata.StreetName_DV = detaildt.DesignerView.StreetName;
        //        //                                            gbdata.StreetTypeID_DV = detaildt.DesignerView.StreetTypeID;
        //        //                                            gbdata.Town_DV = detaildt.DesignerView.Town;
        //        //                                            gbdata.State_DV = detaildt.DesignerView.State;
        //        //                                            gbdata.PostCode_DV = detaildt.DesignerView.PostCode;

        //        //                                            gbdata.CompanyName_JE = detaildt.JobElectricians.CompanyName;
        //        //                                            gbdata.FirstName_JE = detaildt.JobElectricians.FirstName;
        //        //                                            gbdata.LastName_JE = detaildt.JobElectricians.LastName;
        //        //                                            gbdata.Email_JE = detaildt.JobElectricians.Email;
        //        //                                            gbdata.Phone_JE = detaildt.JobElectricians.Phone;
        //        //                                            gbdata.Mobile_JE = detaildt.JobElectricians.Mobile;
        //        //                                            gbdata.LicenseNumber_JE = detaildt.JobElectricians.LicenseNumber;
        //        //                                            gbdata.IsPostalAddress_JE = detaildt.JobElectricians.IsPostalAddress;
        //        //                                            gbdata.UnitTypeID_JE = detaildt.JobElectricians.UnitTypeID;
        //        //                                            gbdata.UnitNumber_JE = detaildt.JobElectricians.UnitNumber;
        //        //                                            gbdata.StreetNumber_JE = detaildt.JobElectricians.StreetNumber;
        //        //                                            gbdata.StreetName_JE = detaildt.JobElectricians.StreetName;
        //        //                                            gbdata.StreetTypeID_JE = detaildt.JobElectricians.StreetTypeID;
        //        //                                            gbdata.Town_JE = detaildt.JobElectricians.Town;
        //        //                                            gbdata.State_JE = detaildt.JobElectricians.State;
        //        //                                            gbdata.PostCode_JE = detaildt.JobElectricians.PostCode;

        //        //                                            //gbdata.Brand_LstJID = detaildt.lstJobInverterDetails[0].Brand;
        //        //                                            //gbdata.Model_LstJID = detaildt.lstJobInverterDetails[0].Model;
        //        //                                            //gbdata.Series_LstJID = detaildt.lstJobInverterDetails[0].Series;
        //        //                                            //gbdata.NoOfInverter_LstJID = detaildt.lstJobInverterDetails[0].NoOfInverter;

        //        //                                            //gbdata.Brand_LstJPD = detaildt.lstJobPanelDetails[0].Brand;
        //        //                                            //gbdata.Model_LstJPD = detaildt.lstJobPanelDetails[0].Model;

        //        //                                            //gbdata.NoOfPanel_LstJPD = detaildt.lstJobPanelDetails[0].NoOfPanel;

        //        //                                            gbdata.STCStatus_JSTCD = detaildt.JobSTCStatusData.STCStatus;
        //        //                                            gbdata.CalculatedSTC_JSTCD = detaildt.JobSTCStatusData.STCStatus;
        //        //                                            gbdata.STCPrice_JSTCD = detaildt.JobSTCStatusData.STCPrice;
        //        //                                            gbdata.FailureNotice_JSTCD = detaildt.JobSTCStatusData.FailureNotice;
        //        //                                            gbdata.ComplianceNotes_JSTCD = detaildt.JobSTCStatusData.ComplianceNotes;
        //        //                                            gbdata.STCSubmissionDate_JSTCD = detaildt.JobSTCStatusData.STCSubmissionDate;
        //        //                                            gbdata.STCInvoiceStatus_JSTCD = detaildt.JobSTCStatusData.STCInvoiceStatus;
        //        //                                            gbdata.IsInvoiced_JSTCD = detaildt.JobSTCStatusData.IsInvoiced;
        //        //                                            gbdata.FieldName_LstCD = detaildt.JobSTCStatusData.STCStatus;
        //        //                                            gbdata.FieldValue_LstCD = detaildt.JobSTCStatusData.STCStatus;
        //        //                                            gbdata.UpdatedOn = DateTime.UtcNow;
        //        //                                            gbdata.IsGst = false;
        //        //                                            gbdata.OrganizationId = (int?)dt.Id;
        //        //                                            var id = await _GreenBoatDataRepository.InsertAndGetIdAsync(gbdata);
        //        //                                            //if (detaildt.JobSystemDetails.SerialNumbers != "" && detaildt.JobSystemDetails.SerialNumbers != null)
        //        //                                            //{


        //        //                                            //    List<string> idList = detaildt.JobSystemDetails.SerialNumbers.Split(new[] { "\r\n" }, StringSplitOptions.None).ToList();
        //        //                                            //    if (idList.Count > 0)
        //        //                                            //    {

        //        //                                            //        GreenBoatSerialNumber obj = new GreenBoatSerialNumber();
        //        //                                            //        for (int k = 0; k < idList.Count; k++)
        //        //                                            //        {
        //        //                                            //            obj.GreenBoatId = id;
        //        //                                            //            obj.SerialNumbers = idList[k];
        //        //                                            //            _GreenBoatSerialNumberRepository.Insert(obj);
        //        //                                            //        }
        //        //                                            //    }
        //        //                                            //}

        //        //                                        }
        //        //                                        uow1.Complete();
        //        //                                    }
        //        //                                }
        //        //                            }
        //        //                        }
        //        //                    }
        //        //                }
        //        //            }
        //        //        }
        //        //    }

        //        //    uow.Complete();
        //        //}
        //        #endregion
        //    }
        //    catch (Exception e) { throw e; }
        //}

        //public async Task updateGreenBoatData()
        //{
        //    using (var uow = _unitOfWorkManager.Begin())
        //    {
        //        //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
        //        //for (int i = 0; i < tenant.Count; i++)
        //        //{
        //        //using (_unitOfWorkManager.Current.SetTenantId(tenant[i].Id))
        //        using (_unitOfWorkManager.Current.SetTenantId(6))
        //        {
        //            var organizationdetail = _organizationUnitRepository.GetAll().ToList();
        //            var pvdstatus = _pVDStatusRepository.GetAll();
        //            if (organizationdetail.Count > 0)
        //            {
        //                foreach (var dt in organizationdetail)
        //                { 
        //                    using (var uow1 = _unitOfWorkManager.Begin())
        //                    {
        //                        var stclist = _jobRepository.GetAll().Where(e => e.JobStatusId >= 4 && e.PVDStatus != 4 & e.InstallationDate != null && e.LeadFk.OrganizationId == dt.Id).ToList();
        //                        foreach (var item in stclist)
        //                        {
        //                            string JobNumber = item.JobNumber.Substring(2, item.JobNumber.Length - 2);
        //                            var gbdata = _greenBoatDataRepository.GetAll().Where(e => e.RefNumber == item.JobNumber || e.RefNumber == JobNumber).FirstOrDefault();
        //                            if (gbdata != null)
        //                            {
        //                                if (gbdata.STCSubmissionDate_JSTCD != "" && gbdata.STCSubmissionDate_JSTCD != null)
        //                                {
        //                                    item.STCAppliedDate = Convert.ToDateTime(gbdata.STCSubmissionDate_JSTCD);
        //                                }
        //                                if (gbdata.STCStatus_JSTCD != "" && gbdata.STCStatus_JSTCD != null)
        //                                {
        //                                    item.PVDStatus = pvdstatus.Where(e => e.Name == gbdata.STCStatus_JSTCD).Select(e => e.Id).FirstOrDefault();
        //                                }
        //                                item.PVDNumber = gbdata.FailedAccreditationCode_JSTC;
        //                                await _jobRepository.UpdateAsync(item);
        //                            }
        //                        }
        //                        uow1.Complete();
        //                    }
        //                }
        //            }
        //        }
        //        uow.Complete();
        //    }
        //}
        
        //public async Task getWestPackData()
        //{
        //    using (var uow = _unitOfWorkManager.Begin())
        //    {
        //        using (_unitOfWorkManager.Current.SetTenantId(6))
        //        {
        //            //    var client = new RestClient("https://api.quickstream.support.qvalent.com/rest/v1/transactions?supplierBusinessCode=C00585");
        //            //    client.Timeout = -1;
        //            //    var request = new RestRequest(Method.GET);
        //            //    request.AddHeader("Authorization", "Basic QzAwNTg1X1NFQ190ZG5ja3F0N3V2a3dyczhiYXFyZGRwcjZwbXZyejVkamR4aWNxYXVrajl1aXF3dG52eXNpejVld2NjazI6");
        //            //    IRestResponse response = client.Execute(request);
        //            try
        //            {
        //                var organizationdetail = _organizationUnitRepository.GetAll().ToList();
        //                if (organizationdetail.Count > 0)
        //                {
        //                    foreach (var dts in organizationdetail)
        //                    {
        //                        var orgwestpacksecretkey = _extendOrganizationUnitRepository.GetAll().Where(e => e.Id == dts.Id).Select(e => e.WestPacSecreteKey).FirstOrDefault();
        //                        if (orgwestpacksecretkey != null)
        //                        {
        //                            var currentdate = DateTime.Now.Date.ToString("yyyy-MM-dd");
        //                            var date = Convert.ToDateTime(currentdate);
        //                            var client1 = new RestClient("https://api.quickstream.support.qvalent.com/rest/v1/transactions/for-settlement-date?settlementDate="+ currentdate);  ///Currentdate
        //                            client1.Timeout = -1;

        //                            var request1 = new RestRequest(Method.GET);
        //                            /// secretekay
        //                            var plainscrtKeyBytes = System.Text.Encoding.UTF8.GetBytes(orgwestpacksecretkey);
        //                            string scrtKeyval = System.Convert.ToBase64String(plainscrtKeyBytes);
        //                            request1.AddHeader("Authorization", "Basic " + scrtKeyval);
        //                            IRestResponse response1 = client1.Execute(request1);
        //                            var jsonobject = JsonConvert.DeserializeObject(response1.Content);
        //                            if (jsonobject != null)
        //                            {
        //                                string dt = jsonobject.ToString();
        //                                string name = dt.Split("data")[1];
        //                                string data = "{" + "\"data\": [{ " + name.Split("],")[1];

        //                               // var temp = JsonConvert.SerializeObject(name);
        //                                //var objs = JArray.Parse(temp).ToObject<List<object>>();
        //                                //JObject json = JObject.Parse(objs);
        //                               //string abc= objs.ToString();

        //                                ///dynamic json = JsonConvert.DeserializeObject(name);
        //                                var transactiondata = JsonConvert.DeserializeObject<Root>(dt);

        //                                foreach (var item in transactiondata.data)
        //                                {
        //                                    WestPackData westpackdata = new WestPackData();
        //                                    westpackdata.TenantId = 6;
        //                                    westpackdata.ReceiptNumber = item.receiptNumber;
        //                                    westpackdata.Status = item.status;
        //                                    westpackdata.ResponseCode = item.responseCode;
        //                                    westpackdata.ResponseDescription = item.responseDescription;
        //                                    westpackdata.SummaryCode = item.summaryCode;
        //                                    westpackdata.TransactionType = item.transactionType;
        //                                    westpackdata.FraudGuardResult = item.fraudGuardResult;
        //                                    westpackdata.TransactionTime = Convert.ToDateTime(item.transactionTime);
        //                                    westpackdata.SettlementDate = Convert.ToDateTime(item.settlementDate);
        //                                    westpackdata.CustomerReferenceNumber = item.customerReferenceNumber;
        //                                    westpackdata.PaymentReferenceNumber = item.paymentReferenceNumber;
        //                                    westpackdata.User = item.user;
        //                                    westpackdata.Voidable = Convert.ToBoolean(item.voidable);
        //                                    westpackdata.Refundable = Convert.ToBoolean(item.refundable);
        //                                    westpackdata.Comment = item.comment;
        //                                    westpackdata.IpAddress = item.ipAddress;
        //                                    westpackdata.OrganaizationId = (int?)dts.Id;
        //                                    int westpackdataId = await _westpackdataRepository.InsertAndGetIdAsync(westpackdata);

        //                                    WestPackPrincipalAmount westpackprincdata = new WestPackPrincipalAmount();
        //                                    westpackprincdata.TenantId = 6;
        //                                    westpackprincdata.WestPackId = westpackdataId;
        //                                    westpackprincdata.Currency = item.principalAmount.currency;
        //                                    westpackprincdata.Amount = float.Parse(item.principalAmount.amount);
        //                                    westpackprincdata.DisplayAmount = item.principalAmount.displayAmount;
        //                                    if (westpackprincdata != null)
        //                                    {
        //                                        await _westpackprincipleRepository.InsertAsync(westpackprincdata);
        //                                    }

        //                                    WestPackSurchargeAmount westpacksurchargedata = new WestPackSurchargeAmount();
        //                                    westpacksurchargedata.WestPackId = westpackdataId;
        //                                    westpacksurchargedata.Currency = item.principalAmount.currency;
        //                                    westpacksurchargedata.Amount = float.Parse(item.principalAmount.amount);
        //                                    westpacksurchargedata.DisplayAmount = item.principalAmount.displayAmount;
        //                                    westpacksurchargedata.TenantId = 6;
        //                                    if (westpacksurchargedata != null)
        //                                    {
        //                                        await _westpacksurchargeRepository.InsertAsync(westpacksurchargedata);
        //                                    }

        //                                    WestPackTotalAmount westpacktotaldata = new WestPackTotalAmount();
        //                                    westpacktotaldata.WestPackId = westpackdataId;
        //                                    westpacktotaldata.Currency = item.principalAmount.currency;
        //                                    westpacktotaldata.Amount = float.Parse(item.principalAmount.amount);
        //                                    westpacktotaldata.DisplayAmount = item.principalAmount.displayAmount;
        //                                    westpacktotaldata.TenantId = 6;
        //                                    if (westpacktotaldata != null)
        //                                    {
        //                                        await _westpacktotalRepository.InsertAsync(westpacktotaldata);
        //                                    }
        //                                }
        //                            }
        //                            Console.WriteLine(response1.Content);
        //                            Console.WriteLine(response1.Content);
        //                        }
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {

        //                throw ex;
        //            }
        //        }
        //        uow.Complete();
        //    }
           
        //}

        //public async Task ReminderForFolloupAsync()
        //{
        //    using (_unitOfWorkManager.Begin())
        //    {
        //        var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
        //        for (int i = 0; i < tenant.Count; i++)
        //        {
        //            using (_unitOfWorkManager.Current.SetTenantId(tenant[i].Id))
        //            {
        //                var LeadActivity_List = _leadactivityRepository.GetAll();
        //                DateTime firstoriginal = Convert.ToDateTime((DateTime.Now.AddMinutes(14)).ToString("yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture));
        //                DateTime original = Convert.ToDateTime((DateTime.Now.AddMinutes(16)).ToString("yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture));
        //                var LeadList = LeadActivity_List
        //                            .Where(e => e.ActionId == 8 && e.ActivityDate.Value >= firstoriginal && e.ActivityDate.Value <= original)
        //                            .Select(e => e.LeadId).Distinct().ToList();
        //                var OrganizationList = _leadRepository.GetAll().Where(e=> LeadList.Contains(e.Id)).Select(e=>e.OrganizationId).Distinct().ToList();
        //                foreach (var item in OrganizationList)
        //                {
        //                    var OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == item).Select(e => e.DisplayName).FirstOrDefault();
        //                    var LeadIDList = LeadActivity_List
        //                                     .Where(e => e.ActionId == 8 && e.ActivityDate.Value >= firstoriginal && e.ActivityDate.Value <= original && e.LeadFk.OrganizationId == item).Select(e => e.Id).ToList();

        //                    foreach (var items in LeadIDList)
        //                    {
        //                        var LeadActivityList = LeadActivity_List.Where(e => e.Id == items).FirstOrDefault();
        //                        var NotifyToUser = _userRepository.GetAll().Where(E => E.Id == LeadActivityList.CreatorUserId).FirstOrDefault();
        //                        var User_List = _userRepository.GetAll();

        //                        var UserList = (from user in User_List
        //                                        where (user.Id == LeadActivityList.CreatorUserId)
        //                                        select user).ToList();
        //                        //Send Notification
        //                        try
        //                        {
        //                            string msg = string.Format("Next Reminder Set For " + LeadActivityList.ActivityNote + " For " + OrganizationName);
        //                            await _appNotifier.LeadAssigedList(UserList, msg, NotificationSeverity.Info);
        //                        }
        //                        catch (Exception ex)
        //                        {

        //                        }
        //                    }
        //                }
        //            }
        //        }

        //    }
        //}

        public class SendSMSRequestInput
        {
            public string From { get; set; }
            public string To { get; set; }
            public string Text { get; set; }
        }

    }


}