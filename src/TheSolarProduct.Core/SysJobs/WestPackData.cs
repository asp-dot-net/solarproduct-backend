﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.SysJobs
{
    [Table("WestPackData")]
    public class WestPackData : FullAuditedEntity, IMustHaveTenant
    {
		public int TenantId { get; set; }
		public virtual string ReceiptNumber { get; set; }
		public virtual string Status { get; set; }
		public virtual string ResponseCode { get; set; }
		public virtual string ResponseDescription { get; set; }
		public virtual string SummaryCode { get; set; }
		public virtual string TransactionType { get; set; }
		public virtual string FraudGuardResult { get; set; }
		public virtual DateTime TransactionTime { get; set; }
		public virtual DateTime SettlementDate { get; set; }
		public virtual string CustomerReferenceNumber { get; set; }
		public virtual string PaymentReferenceNumber { get; set; }
		public virtual string User { get; set; }
		public virtual bool Voidable { get; set; }
		public virtual bool Refundable { get; set; }
		public virtual string Comment { get; set; }
		public virtual string IpAddress { get; set; }
		public virtual int? OrganaizationId { get; set; }
    }
}
