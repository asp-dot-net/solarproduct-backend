﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.SysJobs
{
    public class Job_Response
    {
        public Job_Response()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public class TokenData
        {
            public string access_token { get; set; }
            public string token_type { get; set; }
        }

        public class RootObject
        {
            public TokenData TokenData { get; set; }
            public bool Status { get; set; }
            public string StatusCode { get; set; }
            public string Message { get; set; }
            public string access_token { get; set; }
            public List<lstJobData> lstJobData { get; set; }
        }
        public class TokenObject
        {
            public string access_token { get; set; }
        }


        public class lstJobData
        {
            public BasicDetails BasicDetails { get; set; }
            public JobOwnerDetails JobOwnerDetails { get; set; }
            public JobInstallationDetails JobInstallationDetails { get; set; }
            public JobSystemDetails JobSystemDetails { get; set; }
            public JobSTCDetails JobSTCDetails { get; set; }
            public JobSTCStatusData JobSTCStatusData { get; set; }

            public InstallerView InstallerView { get; set; }
            public DesignerView DesignerView { get; set; }
            public JobElectricians JobElectricians { get; set; }
            public lstJobInverterDetails[] lstJobInverterDetails { get; set; }
            public lstJobPanelDetails[] lstJobPanelDetails { get; set; }


        }

        public class BasicDetails
        {
            public string VendorJobId { get; set; }
            public int JobID { get; set; }
            public string Title { get; set; }
            public string RefNumber { get; set; }
            public string Description { get; set; }
            public int JobType { get; set; }
            public int JobStage { get; set; }
            public int Priority { get; set; }
            public string InstallationDate { get; set; }
            public string CreatedDate { get; set; }

        }
        public class JobOwnerDetails {
            public string OwnerType { get; set; }
            public string CompanyName { get; set; }

            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Phone { get; set; }

            public string Mobile { get; set; }

            public string Email { get; set; }

            public bool IsPostalAddress { get; set; }

            public string UnitTypeID { get; set; }

            public string UnitNumber { get; set; }

            public string StreetNumber { get; set; }

            public string StreetName { get; set; }
            public int? StreetTypeID { get; set; }

            public string Town { get; set; }

            public string State { get; set; }
            public string PostCode { get; set; }

        }
        public class JobInstallationDetails
        {
            public string DistributorID { get; set; }

            public string MeterNumber { get; set; }

            public string PhaseProperty { get; set; }

            public string ElectricityProviderID { get; set; }

            public bool IsSameAsOwnerAddress { get; set; }

            public bool IsPostalAddress { get; set; }

            public string UnitTypeID { get; set; }

            public string UnitNumber { get; set; }

            public string StreetNumber { get; set; }

            public string StreetName { get; set; }

            public int? StreetTypeID { get; set; }

            public string Town { get; set; }

            public string State { get; set; }

            public string PostCode { get; set; }

            public string NMI { get; set; }

            public string SystemSize { get; set; }

            public string PropertyType { get; set; }

            public string SingleMultipleStory { get; set; }

            public string InstallingNewPanel { get; set; }

            public string Location { get; set; }

            public bool ExistingSystem { get; set; }

            public string ExistingSystemSize { get; set; }

            public string SystemLocation { get; set; }

            public string NoOfPanels { get; set; }

            public string AdditionalInstallationInformation { get; set; }

        }
        public class JobSystemDetails
        {
            public string SystemSize { get; set; }
            public string SerialNumbers { get; set; }
            public string CalculatedSTC { get; set; }
            public string NoOfPanel { get; set; }
        }

        public class JobSTCDetails
        {

            public string AdditionalCapacityNotes { get; set; }

            public string TypeOfConnection { get; set; }

            public string SystemMountingType { get; set; }
            public string DeemingPeriod { get; set; }

            public string CertificateCreated { get; set; }

            public string FailedAccreditationCode { get; set; }
            public string FailedReason { get; set; }

            public string MultipleSGUAddress { get; set; }
            public string Location { get; set; }

            public string AdditionalLocationInformation { get; set; }

            public string AdditionalSystemInformation { get; set; }
        }
        public class InstallerView
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
            public string Mobile { get; set; }
            public string CECAccreditationNumber { get; set; }
            public string ElectricalContractorsLicenseNumber { get; set; }
            public bool IsPostalAddress { get; set; }
            public int? UnitTypeID { get; set; }
            public string UnitNumber { get; set; }
            public string StreetNumber { get; set; }
            public string StreetName { get; set; }
            public int? StreetTypeID { get; set; }
            public string Town { get; set; }
            public string State { get; set; }
            public string PostCode { get; set; }

        }
        public class DesignerView
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
            public string Mobile { get; set; }
            public string CECAccreditationNumber { get; set; }
            public string ElectricalContractorsLicenseNumber { get; set; }
            public bool IsPostalAddress { get; set; }
            public int? UnitTypeID { get; set; }
            public string UnitNumber { get; set; }
            public string StreetNumber { get; set; }
            public string StreetName { get; set; }
            public int? StreetTypeID { get; set; }
            public string Town { get; set; }
            public string State { get; set; }
            public string PostCode { get; set; }
        }
        public class JobElectricians
        {
            public string CompanyName { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
            public string Mobile { get; set; }
            public string LicenseNumber { get; set; }
            public bool IsPostalAddress { get; set; }
            public string UnitTypeID { get; set; }
            public string UnitNumber { get; set; }
            public string StreetNumber { get; set; }
            public string StreetName { get; set; }
            public string StreetTypeID { get; set; }
            public string Town { get; set; }
            public string State { get; set; }
            public string PostCode { get; set; }
        }

        public class lstJobInverterDetails
        {
            public string Brand { get; set; }

            public string Model { get; set; }
            public string Series { get; set; }
            public string NoOfInverter { get; set; }

        }
        public class JobSTCStatusData
        {
            public string STCStatus { get; set; }

            public string CalculatedSTC { get; set; }

            public string STCPrice { get; set; }

            public string FailureNotice { get; set; }

            public string ComplianceNotes { get; set; }

            public string STCSubmissionDate { get; set; }
            public string STCInvoiceStatus { get; set; }

            public bool IsInvoiced { get; set; }

            public string STCInvoiceCreatedDate { get; set; }

            public string SettlementDate { get; set; }

        }
        public class lstJobPanelDetails
        {

            public string Brand { get; set; }
            public string Model { get; set; }
            public int? NoOfPanel { get; set; }
            public string Supplier { get; set; }

        }
       

    }
}
