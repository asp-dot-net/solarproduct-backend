﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.SysJobs
{
    [Table("WestPackTotalAmount")]
    public class WestPackTotalAmount : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public int WestPackId { get; set; }
        public virtual string Currency { get; set; }
        public virtual float Amount { get; set; }
        public virtual string DisplayAmount { get; set; }
    }
}
