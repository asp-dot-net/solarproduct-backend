﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.SysJobs
{
  public  class LinkData
    {
        public LinkData()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public class Root
        {
            public List<links> links { get; set; }
            public List<data> data { get; set; }
        }
        public class data
        {
            public List<links> links { get; set; }
            public string receiptNumber { get; set; }
            public PrincipalAmount principalAmount { get; set; }
            public surchargeAmount surchargeAmount { get; set; }
            public totalAmount totalAmount { get; set; }

            public string status { get; set; }
            public string responseCode { get; set; }
            public string responseDescription { get; set; }
            public string summaryCode { get; set; }
            public string transactionType { get; set; }
            public string fraudGuardResult { get; set; }
            public string transactionTime { get; set; }
            public string settlementDate { get; set; }
            public string source { get; set; }
            public string customerReferenceNumber { get; set; }
            public string paymentReferenceNumber { get; set; }
            public string user { get; set; }
            public string voidable { get; set; }
            public string refundable { get; set; }
            public string comment { get; set; }
            public string ipAddress { get; set; }


        }
        //public class links {
        //    public string rel { get; set; }
        //    public string href { get; set; }
        //    public string requestMethod { get; set; }
        //    public List< data> data { get; set; }
        //}
        public class links
        {
            public string rel { get; set; }
            public string href { get; set; }
            public string requestMethod { get; set; }

        }
        public class PrincipalAmount
        {
            public string currency { get; set; }
            public string amount { get; set; }
            public string displayAmount { get; set; }
        }
        public class surchargeAmount
        {
            public string currency { get; set; }
            public string amount { get; set; }
            public string displayAmount { get; set; }
        }
        public class totalAmount
        {
            public string currency { get; set; }
            public string amount { get; set; }
            public string displayAmount { get; set; }
        }
    }
}
