﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TheSolarProduct.SysJobs
{
    public interface IClientDomainService : IDomainService
    {
        Task NotifyLeadAsync();

        Task SendPromotion(int TenantId, int PromotionId, int UserId);

        //Task getGreenBoatData();
        //Task updateGreenBoatData();
        //Task getWestPackData();
        //Task ReminderForFolloupAsync();
    }
}
