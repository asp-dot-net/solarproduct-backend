﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.CommmonActivityLogs;
using TheSolarProduct.DataVaults;
using TheSolarProduct.LeadActions;

namespace TheSolarProduct.UserActivityLogs
{
	[Table("UserActivityLogs")]
    public class UserActivityLog : FullAuditedEntity
    {
        public int? ActionId { get; set; }

        [ForeignKey("ActionId")]
        public LeadAction LeadAction { get; set; }

        public string ActionNote { get; set; }

        public int? SectionId { get; set; }

        [ForeignKey("SectionId")]
        public CommonSection SectionFK { get; set; }
    }
}
