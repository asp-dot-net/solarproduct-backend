﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.LeadStatuses
{
	[Table("LeadStatus")]
	public class LeadStatus : FullAuditedEntity
	{
		[Required]
		public virtual string Status { get; set; }

		public virtual string ColorClass { get; set; }
	}
}
