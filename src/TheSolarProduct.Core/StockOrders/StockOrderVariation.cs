﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.StockOrders
{
	[Table("StockOrderVariations")]
    public class StockOrderVariation : FullAuditedEntity
    {
        public virtual decimal? Cost { get; set; }

        public virtual int? StockVariationId { get; set; }

        [ForeignKey("StockVariationId")]
        public StockVariation VariationFk { get; set; }

        public virtual int? PurchaseOrderId { get; set; }

        [ForeignKey("PurchaseOrderId")]
        public PurchaseOrder PurchaseOrderFk { get; set; }

        public string Notes { get; set; }

    }
}
