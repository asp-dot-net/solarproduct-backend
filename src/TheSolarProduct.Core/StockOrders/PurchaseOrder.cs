﻿using Abp.Domain.Entities.Auditing;
using Castle.MicroKernel.Registration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Currencies;
using TheSolarProduct.DeliveryTypes;
using TheSolarProduct.IncoTerms;
using TheSolarProduct.Jobs;
using TheSolarProduct.PaymentMethods;
using TheSolarProduct.PaymentStatuses;
using TheSolarProduct.PaymentTypes;
using TheSolarProduct.PurchaseCompanies;
using TheSolarProduct.StockFroms;
using TheSolarProduct.StockOrderFors;
using TheSolarProduct.StockOrderStatuses;
using TheSolarProduct.TransportationCosts;
using TheSolarProduct.Vendors;

namespace TheSolarProduct.StockOrders
{
    [Table("PurchaseOrders")]
    public class PurchaseOrder : FullAuditedEntity
    {
        public virtual int OrganizationId { get; set; }

        public virtual long OrderNo { get; set; }

        public virtual int? VendorId { get; set; }

        [ForeignKey("VendorId")]
        public Vendor VendorFK { get; set; }

        public virtual int? PurchaseCompanyId { get; set; }

        [ForeignKey("PurchaseCompanyId")]
        public PurchaseCompany PurchaseCompanyFk { get; set; }

        public virtual string ManualOrderNo { get; set; }

        public virtual string ProductNotes { get; set; }

        public virtual string VendorInoviceNo { get; set; }

        public virtual string ContainerNo { get; set; }
        
        public virtual int? StockOrderStatusId { get; set; }

        [ForeignKey("StockOrderStatusId")]
        public StockOrderStatus StockOrderStatusFk { get; set; }
        
        public virtual int? StockFromId { get; set; }

        [ForeignKey("StockFromId")]
        public StockFrom StockFromFk { get; set; }

        public virtual int? DeliveryTypeId { get; set; }

        [ForeignKey("DeliveryTypeId")]
        public DeliveryType DeliveryTypeFk { get; set; }

        public virtual int? PaymentStatusId { get; set; }

        [ForeignKey("PaymentStatusId")]
        public PaymentStatus PaymentStatusFk { get; set; }

        public virtual int? PaymentTypeId { get; set; }

        [ForeignKey("PaymentTypeId")]
        public PaymentType PaymentTypeFk { get; set; }

        public virtual int? WarehouseLocationId { get; set; }

        [ForeignKey("WarehouseLocationId")]
        public Warehouselocation WarehouseLocationFk { get; set; }

        public virtual int? IncoTermId { get; set; }

        [ForeignKey("IncoTermId")]
        public IncoTerm IncoTermFk { get; set; }

        public virtual int? TransportCompanyId { get; set; }

        [ForeignKey("TransportCompanyId")]
        public TransportCompany TransportCompanyFk { get; set; }

        public virtual DateTime? DocSentDate { get; set; }

        public virtual DateTime? TargetArrivalDate { get; set; }

        public virtual DateTime? ETD { get; set; }
        
        public virtual DateTime? ETA { get; set; }

        public virtual DateTime? PaymentDueDate { get; set; }
        
        public virtual DateTime? PhysicalDeliveryDate { get; set; }

        public virtual decimal? DiscountPrice { get; set; }

        public virtual decimal? CreditNotePrice { get; set; }

        public virtual decimal? Amount { get; set; }

        public virtual int? CurrencyId { get; set; }

        [ForeignKey("CurrencyId")]
        public Currency CurrencyFk { get; set; }

        public virtual int? GSTType { get; set; }

        public virtual int? StockOrderForId { get; set; }

        [ForeignKey("StockOrderForId")]
        public StockOrderFor StockOrderForFk { get; set; }

        public virtual string AdditionalNotes { get; set; }
        public virtual DateTime? ReminderTime { get; set; }

        public virtual string ReminderDes { get; set; }

        public virtual string Comment { get; set; }

        public virtual DateTime? IsSubmitDate { get; set; }

        public virtual string signature_image { get; set; }
        public virtual string user_image { get; set; }
        public virtual string Latitude { get; set; }

        public virtual string Longitude { get; set; }
    }
}