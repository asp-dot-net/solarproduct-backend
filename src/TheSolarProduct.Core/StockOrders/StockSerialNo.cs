﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;
using TheSolarProduct.StockOrders.SerialNoStatuses;

namespace TheSolarProduct.StockOrders
{
    [Table("StockSerialNo")]
    public class StockSerialNo : FullAuditedEntity
    {
        public virtual int PurchaseOrderId { get; set; }

        [ForeignKey("PurchaseOrderId")]
        public PurchaseOrder PurchaseOrderFk { get; set; }

        public virtual int ProductItemId { get; set; }

        [ForeignKey("ProductItemId")]
        public ProductItem ProductItemFk { get; set; }

        public virtual int WarehouseLocationId { get; set; }

        [ForeignKey("WarehouseLocationId")]
        public Warehouselocation WarehouseLocationFk { get; set; }

        public virtual string PalletNo { get; set; }

        public virtual string SerialNo { get; set; }

        public virtual bool IsApp { get; set; }

        public virtual bool IsVerify { get; set; }

        public virtual int? SerialNoStatusId { get; set; }

        [ForeignKey("SerialNoStatusId")]
        public SerialNoStatus SerialNoStatusFk { get; set; }
    }
}
