﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.StockOrders
{
    [Table("OtherStockItemQty")]
    public class OtherStockItemQty : FullAuditedEntity
    {
        public virtual int PurchaseOrderId { get; set; }

        [ForeignKey("PurchaseOrderId")]
        public PurchaseOrder PurchaseOrderFk { get; set; }

        public virtual int ProductItemId { get; set; }

        [ForeignKey("ProductItemId")]
        public ProductItem ProductItemFk { get; set; }

        public virtual int Quantity { get; set; }
    }
}