﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;
using TheSolarProduct.PurchaseDocumentListes;

namespace TheSolarProduct.StockOrders
{
    [Table("PurchaseOrderDocuments")]
    public class PurchaseOrderDocument : FullAuditedEntity
    {
        public virtual int PurchaseOrderId { get; set; }

        [ForeignKey("PurchaseOrderId")]
        public PurchaseOrder PurchaseOrderFk { get; set; }

        public virtual int PurchaseDocumentId { get; set; }

        [ForeignKey("PurchaseDocumentId")]
        public PurchaseDocumentList PurchaseDocumentListFk { get; set; }

        public string DocumentName { get; set; }

        public virtual string FileName { get; set; }

        public virtual string FilePath { get; set; }
    }
}
