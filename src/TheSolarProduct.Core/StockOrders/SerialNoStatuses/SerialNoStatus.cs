﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.StockOrders.SerialNoStatuses
{
    [Table("SerialNoStatuses")]
    public class SerialNoStatus : FullAuditedEntity
    {
        public string Status { get; set; }

        public bool IsActive { get; set; }
    }
}
