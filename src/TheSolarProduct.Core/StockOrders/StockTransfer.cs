﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Currencies;
using TheSolarProduct.DeliveryTypes;
using TheSolarProduct.IncoTerms;
using TheSolarProduct.Jobs;
using TheSolarProduct.PaymentStatuses;
using TheSolarProduct.PaymentTypes;
using TheSolarProduct.PurchaseCompanies;
using TheSolarProduct.StockFroms;
using TheSolarProduct.StockOrderFors;
using TheSolarProduct.StockOrderStatuses;
using TheSolarProduct.TransportationCosts;
using TheSolarProduct.Vendors;

namespace TheSolarProduct.StockOrders
{
    [Table("StockTransfers")]
    public class StockTransfer : FullAuditedEntity
    {
        public virtual int? TransportCompanyId { get; set; }

        [ForeignKey("TransportCompanyId")]
        public TransportCompany TransportCompanyFk { get; set; }

        public virtual int? WarehouseLocationFromId { get; set; }

        [ForeignKey("WarehouseLocationFromId")]
        public Warehouselocation WarehouseLocationFromFK { get; set; }

        public virtual int? WarehouseLocationToId { get; set; }

        [ForeignKey("WarehouseLocationToId")]
        public Warehouselocation WarehouseLocationToFK { get; set; }

        public virtual int OrganizationId { get; set; }

        public virtual string VehicalNo { get; set; }

        public virtual string TrackingNumber { get; set; }

        public virtual decimal? Amount { get; set; }

        public virtual string AdditionalNotes { get; set; }

        public virtual string CornetNo { get; set; }

        public virtual int Transferd { get; set; }
        public virtual DateTime TransferByDate { get; set; }
        public virtual int Received { get; set; }
        public virtual DateTime ReceivedByDate { get; set; }

        public virtual long? TransferNo { get; set; }

    }
}
