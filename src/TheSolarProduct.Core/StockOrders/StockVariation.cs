﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.StockOrders
{
	[Table("StockVariations")]
    public class StockVariation : FullAuditedEntity
    {
        public virtual string Name { get; set; }
		public virtual string Action { get; set; }
    }
}
