﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.StockOrders
{
    [Table("PurchaseOrderItems")]
    public class PurchaseOrderItem : FullAuditedEntity
    {
        public virtual int? PurchaseOrderId { get; set; }

        [ForeignKey("PurchaseOrderId")]
        public PurchaseOrder PurchaseOrderFk { get; set; }

        public virtual int? ProductItemId { get; set; }

        [ForeignKey("ProductItemId")]
        public ProductItem ProductItemFk { get; set; }

        public virtual int? Quantity { get; set; }

        public virtual decimal? PricePerWatt { get; set; }

        public virtual decimal? UnitRate { get; set; }

        public virtual decimal? EstRate { get; set; }

        public virtual decimal? Amount { get; set; }
        
        public virtual string ModelNo { get; set; }
        
        public virtual DateTime? ExpiryDate { get; set; }
    }
}