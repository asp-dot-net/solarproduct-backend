﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.StockOrders
{
    [Table("StockTransferProductItems")]
    public class StockTransferProductItem : FullAuditedEntity
    {
        public virtual int? StockTransferId { get; set; }

        [ForeignKey("StockTransferId")]
        public StockTransfer StockTransferFk { get; set; }

        public virtual int? ProductItemId { get; set; }

        [ForeignKey("ProductItemId")]
        public ProductItem ProductItemFk { get; set; }

        public virtual int? Quantity { get; set; }
    }
}
