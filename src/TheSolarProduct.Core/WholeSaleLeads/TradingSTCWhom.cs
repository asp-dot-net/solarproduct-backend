﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.WholeSaleLeads
{
    [Table("TradingSTCWhoms")]
    public class TradingSTCWhom : FullAuditedEntity
    {
        public string Name { get; set; }
    }
}
