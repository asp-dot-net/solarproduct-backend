﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Leads;

namespace TheSolarProduct.WholeSaleLeads
{
	[Table("RequestWholesaleLeadTransfers")]
    public class RequestWholesaleLeadTransfer : FullAuditedEntity
    {
        public string Approval { get; set; }

        public string Type { get; set; }

        public int? LeadId { get; set; }

       // public int? Id { get; set; }

        [ForeignKey("LeadId")]
        public WholeSaleLead LeadFk { get; set; }
        
    }
}
