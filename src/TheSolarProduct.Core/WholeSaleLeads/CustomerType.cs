﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.WholeSaleLeads
{
    [Table("CustomerTypes")]
    public class CustomerType : FullAuditedEntity
    {
        public string Type { get; set; }
    }
}
