﻿using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using TheSolarProduct.WholeSaleStatuses;
using TheSolarProduct.States;
using TheSolarProduct.Wholesales.Ecommerces;
using PayPalCheckoutSdk.Orders;

namespace TheSolarProduct.WholeSaleLeads
{
	[Table("WholeSaleLeads")]
    public class WholeSaleLead : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public string ABNNumber { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CompanyName { get; set; }

        public string Mobile { get; set; }

        public string Talephone { get; set; }

        public string CustomerType { get; set; }

      

        public int? WholeSaleStatusId { get; set; }

		[ForeignKey("WholeSaleStatusId")]
        public WholeSaleStatus WholeSaleStatusFK { get; set; }

        public virtual string PostalUnitNo { get; set; }

        public virtual string PostalUnitType { get; set; }

        public virtual string PostalStreetNo { get; set; }

        public virtual string PostalStreetName { get; set; }

        public virtual string PostalStreetType { get; set; }

        public int? PostalStateId { get; set; }

		[ForeignKey("PostalStateId")]
        public State PostalStateFK { get; set; }

        public virtual string PostalPostCode { get; set; }

        public virtual string PostalLatitude { get; set; }

        public virtual string PostalLongitude { get; set; }

        public virtual string DelivaryUnitNo { get; set; }

        public virtual string DelivaryUnitType { get; set; }

        public virtual string DelivaryStreetNo { get; set; }

        public virtual string DelivaryStreetName { get; set; }

        public virtual string DelivaryStreetType { get; set; }

        public int? DelivaryStateId { get; set; }

		[ForeignKey("DelivaryStateId")]
        public State DelivaryStateFK { get; set; }

        public virtual string DelivaryPostCode { get; set; }

        public virtual string DelivaryLatitude { get; set; }

        public virtual string DelivaryLongitude { get; set; }

        public virtual string BankName { get; set; }

        public virtual string AccountName { get; set; }

        public virtual string BSBNo { get; set; }

        public virtual string AccoutNo { get; set; }

        public virtual decimal? CreditAmount { get; set; }

        public virtual int? CreditDays { get; set; }

        public virtual int? AssignUserId { get; set; }

        public virtual DateTime? AssignDate { get; set; }

        public virtual int? FirstAssignUserId { get; set; }

        public virtual DateTime? FirstAssignDate { get; set; }

        public virtual int? BDMId { get; set; }

        public string AditionalNotes { get; set; }

        public string Email { get; set; }

        public virtual int OrganizationId { get; set; }

        public virtual int? SalesRapId { get; set; }

        public virtual DateTime? ActivityDate { get; set; }

        public virtual string ActivityDescription { get; set; }

        public virtual string ActivityComment { get; set; }

        public virtual DateTime? ReAssignDate { get; set; }

        public bool? TradinSTC { get; set; }

        public decimal? TradinSTCPrice { get; set; }

        public int? TradinSTCWhomId { get; set; }

        [ForeignKey("TradinSTCWhomId")]
        public TradingSTCWhom TradingSTCWhomFK { get; set; }

        public string TradinSTCNote { get; set; }

        public int? PriceCategoryId { get; set; }   

        [ForeignKey("PriceCategoryId")]
        public ECommercePriceCategory PriceCategoryFK { get; set; }

        public int? WholesaleClientId { get; set; }

        [ForeignKey("WholesaleClientId")]
        public EcommerceUserRegisterRequest EcommerceUserRegisterRequestFk { get; set; }

        public string PostalSuburb { get; set; }

        public int? PostalSuburbId { get; set; }

        public string DelivarySuburb { get; set; }

        public int? DelivarySuburbId { get; set; }

        public int? IsTransfer { get; set; }
    }
}
