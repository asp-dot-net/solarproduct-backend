﻿using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheSolarProduct.WholeSaleLeads
{
    [Table("WholeSaleLeadDocumentTypes")]
    public class WholeSaleLeadDocumentType : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual string Title { get; set; }
        public bool IsDocumentRequest { get; set; }
    }
}
