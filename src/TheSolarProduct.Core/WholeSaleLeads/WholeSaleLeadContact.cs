﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.WholeSaleLeads
{
	[Table("WholeSaleLeadContacts")]
    public class WholeSaleLeadContact : FullAuditedEntity
    {
        public int? WholesaleLeadId { get; set; }

		[ForeignKey("WholesaleLeadId")]
        public WholeSaleLead WholeSaleLeadFK { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public string Phone { get; set; }

        public bool? IsWhatsapp { get; set; }


    }
}
