﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleSmsTemplates
{
    public class WholeSaleSmsTemplate : FullAuditedEntity, IMustHaveTenant
    {
        public string Name { get; set; }

        public string Text { get; set; }

        public virtual int? OrganizationUnitId { get; set; }

        public int TenantId { get; set; }
    }
}
