﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.LeadActions
{
	[Table("LeadAction")]
	public class LeadAction : FullAuditedEntity
	{
		public virtual string ActionName { get; set; }
	}
}
