﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Quotations;
using TheSolarProduct.Services;

namespace TheSolarProduct.ServiceDocs
{
    [Table("ServiceDocs")]
    public class ServiceDoc : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual int? ServiceId { get; set; }

        [ForeignKey("ServiceId")]
        public Service ServiceFk { get; set; }
        public virtual string FileName { get; set; }

        public virtual string FileType { get; set; }

        public virtual string FilePath { get; set; }

        public virtual int? ServiceDocumentTypeId { get; set; }

        [ForeignKey("ServiceDocumentTypeId")]
        public ServiceDocumentType ServiceDocumentTypeFk { get; set; }

    }
}
