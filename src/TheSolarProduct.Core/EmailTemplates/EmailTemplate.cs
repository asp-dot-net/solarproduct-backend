﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.EmailTemplates
{
	[Table("EmailTemplate")]
	public class EmailTemplate : FullAuditedEntity, IMustHaveTenant
	{
		[Required]
		public virtual string TemplateName { get; set; }

		[Required]
		public virtual string Body { get; set; }

		public virtual string Subject { get; set; }

		public int TenantId { get; set; }
		
		public virtual int? OrganizationUnitId { get; set; }

	}
}
