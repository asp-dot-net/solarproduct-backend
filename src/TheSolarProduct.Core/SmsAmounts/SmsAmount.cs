﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.SmsAmounts
{
    [Table("SmsAmounts")]
    public class SmsAmount : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual decimal? Amount { get; set; }
    }
}
