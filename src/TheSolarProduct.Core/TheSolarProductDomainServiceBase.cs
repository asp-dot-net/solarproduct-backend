﻿using Abp.Domain.Services;

namespace TheSolarProduct
{
    public abstract class TheSolarProductDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected TheSolarProductDomainServiceBase()
        {
            LocalizationSourceName = TheSolarProductConsts.LocalizationSourceName;
        }
    }
}
