﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore.Metadata;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.ThirdPartyApis.RECData
{
    [Table("RECDetails")]
    public class RECDetail : FullAuditedEntity
    {
        public string ActionType { get; set; }

        public DateTime CompletedTime { get; set; }

        public string CertificateType { get; set; }

        public long RegisteredPersonNumber { get; set; }

        public string AccreditationCode { get; set; }

        public long GenerationYear { get; set; }

        public string GenerationState { get; set; }

        public long StartSerialNumber { get; set; }

        public long EndSerialNumber { get; set; }

        public string FuelSource { get; set; }

        public string OwnerAccount { get; set; }

        public long OwnerAccountId { get; set; }

        public string Status { get; set; }

        public string GM_DisplayName { get; set; }

        public long GM_MonthCode { get; set; }

        public string GM_MonthName { get; set; }

        public string GM_Name { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
