﻿using Abp.Domain.Entities.Auditing;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.ThirdPartyApis.Credit
{
    [Table("CimetDetails")]
    public class CimetDetail : FullAuditedEntity
    {
        public string ProviderName { get; set; }

        public string PlanName { get; set; }

        public string AffiliateName { get; set; }

        public string SubaffiliateName { get; set; }

        public string Address { get; set; }

        public string StatusErrorDescription { get; set; }

        public DateTime? StatusCreatedAt { get; set; }

        public DateTime? StatusUpdatedAt { get; set; }

        public string SaleSubStatus { get; set; }

        public long? SaleProductsEnergyServiceId { get; set; }

        public string CommissionStatus { get; set; }

        public string ReferenceNo { get; set; }

        public long? ProductType { get; set; }

        public long? SaleStatus { get; set; }

        public DateTime? CreatedAt { get; set; }

        public long? ProductId { get; set; }

        public long? LeadId { get; set; }

        public long? VisitorId { get; set; }

        public long? Status { get; set; }

        public long? VisitorSource { get; set; }

        public string AgentName { get; set; }

        public string AgentId { get; set; }

        public string AgentEmail { get; set; }

        public DateTime? SaleCreated { get; set; }

        public long? Rc { get; set; }

        public DateTime? SaleCreatedAt { get; set; }

        public DateTime? ProductUpdatedAt { get; set; }

        public long? ProductStatus { get; set; }

        public long? BundleId { get; set; }

        public long? Cui { get; set; }

        public string UtmSource { get; set; }

        public string UtmMedium { get; set; }

        public string UtmCampaign { get; set; }

        public string UtmTerm { get; set; }

        public string UtmContent { get; set; }

        public string Gclid { get; set; }

        public string Fbclid { get; set; }

        public string Msclkid { get; set; }

        public string ServiceId { get; set; }

        public string SaleStatusDescription { get; set; }

        public string SubStatusDescription { get; set; }

        public string ServiceName { get; set; }

        public string StatusTitle { get; set; }

        public string AffiliateCommissionStatus { get; set; }

        public DateTime? SaleStatusUpdatedAt { get; set; }

        public DateTime? SaleStatusCreatedAt { get; set; }

        public string PostalAddress { get; set; }

        public string SaleType { get; set; }

        public string PropertyType { get; set; }

        public string MoveinType { get; set; }

        public string MovinDate { get; set; }

        public string SolarType { get; set; }

        public string MedicalSupport { get; set; }

        public string EnergyType { get; set; }

        public string NmiMirnNumber { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public string TransfersStatus { get; set; }

        public string ConnectionName { get; set; }

        public string ConnectionType { get; set; }

        public string SaleProductsMobileServiceId { get; set; }

        public string Provider { get; set; }

        public string PlanMobile { get; set; }

        public string ReferalCode { get; set; }

        public string SaleProductsBroadbandServiceId { get; set; }

        public string PlanBroadband { get; set; }
    }
}
