﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.ThirdPartyApis.GreenDeal
{
    [Table("GreenDealJobSerialNo")]
    public class GreenDealJobSerialNo : FullAuditedEntity
    {
        public string GwtId { get; set; }

        public string SerialNo { get; set; }

        public int ProductItemTypeId { get; set; }
    }
}
