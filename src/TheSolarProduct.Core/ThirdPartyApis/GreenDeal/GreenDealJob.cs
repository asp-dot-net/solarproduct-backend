﻿using Abp.Domain.Entities.Auditing;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.ThirdPartyApis.GreenDeal
{
    [Table("GreenDealJobs")]
    public class GreenDealJob : FullAuditedEntity
    {
        public long OrganizationId { get; set; }

        public string GwtId { get; set; }

        public string PvdNumber { get; set; }

        public string TradeMode { get; set; }

        public int ClaimedQuantity { get; set; }

        public Decimal? Price { get; set; }

        public bool Gst { get; set; }

        public string Status { get; set; }

        public string PaymentStatus { get; set; }

        public DateTime? SubmitDate { get; set; }

        public DateTime? PaidDate { get; set; }

        public string CreditMemo { get; set; }

        public string UnqualifiedReasons { get; set; }

        public string FailedReasons { get; set; }

        public string CreatedVia { get; set; }

        public string Note { get; set; }

        public DateTime? ReceivedDate { get; set; }

        public string Reference { get; set; }

        public decimal? Amount { get; set; }

        public DateTime? CreatedAt { get; set; }

        public DateTime? FinishedAt { get; set; }

        public string FinishedLocation { get; set; }

        public int? RecQuantity { get; set; }

        public string PoReference { get; set; }

        public string OrderReference { get; set; }

        #region Install Details
        public string ID_UnitType { get; set; }

        public string ID_UnitNumber { get; set; }

        public string ID_PostCode { get; set; }

        public string ID_StreetNumber { get; set; }

        public string ID_StreetName { get; set; }

        public string ID_Suburb { get; set; }

        public string ID_State { get; set; }

        public string ID_PropertyType { get; set; }

        public string ID_Latitude { get; set; }

        public string ID_Longitude { get; set; }

        public string ID_SpecialAddress { get; set; }

        public string ID_Story { get; set; }

        public string ID_NMINumber { get; set; }

        public string ID_JobNumber { get; set; }

        public string ID_InstallationAddress { get; set; }

        public string ID_SingleOrMultiStory { get; set; }

        public bool ID_SameAsPostal { get; set; }

        public string ID_StreetType { get; set; }
        #endregion

        #region Owner Details
        public string OD_FirstName { get; set; }

        public string OD_LastName { get; set; }

        public string OD_Phone { get; set; }

        public string OD_DeliveryType { get; set; }

        public string OD_DeliveryNumber { get; set; }

        public string OD_UnitType { get; set; }

        public string OD_UnitNumber { get; set; }

        public string OD_PostCode { get; set; }

        public string OD_StreetNumber { get; set; }

        public string OD_StreetName { get; set; }

        public string OD_Suburb { get; set; }

        public string OD_State { get; set; }

        public string OD_Email { get; set; }

        public string OD_PriceToTheHomeOwner { get; set; }

        public string OD_HomeOwnerAbn { get; set; }

        public string OD_Type { get; set; }

        public string OD_TypeName { get; set; }

        public string OD_AddressType { get; set; }

        public string OD_Address { get; set; }

        public string OD_StreetType { get; set; }

        public string OD_OwnerRepresentativeName { get; set; }

        public string OD_OwnerRepresentativePosition { get; set; }
        #endregion

        #region System Details
        public int SD_DeemedYears { get; set; }

        public DateTime? SD_InstallationDate { get; set; }

        public string SD_TypeOfConnection { get; set; }

        public string SD_ConnectedType { get; set; }

        public bool SD_IsBattery { get; set; }

        public string SD_SystemMountingType { get; set; }

        public double? SD_RatedOutput { get; set; }

        public string SD_CompleteUnit { get; set; }

        public string SD_AdditionalUpgradeSystemDetails { get; set; }

        public string SD_InstallAdditionalInformation { get; set; }

        public string SD_PanelBrand { get; set; }

        public string SD_PanelModel { get; set; }

        public int? SD_NumberPanels { get; set; }

        public int? SD_NumberInverters { get; set; }

        public int? SD_WattsPerPanel { get; set; }

        public string SD_InverterManufacture { get; set; }

        public string SD_InverterSeries { get; set; }

        public string SD_InverterModel { get; set; }

        public string SD_BatteryManufacture { get; set; }

        public string SD_BatterySeries { get; set; }

        public string SD_BatteryModel { get; set; }
        #endregion

        #region Installer Details
        public string I_FirstName { get; set; }

        public string I_LastName { get; set; }

        public string I_Accreditation { get; set; }

        public string I_UnitType { get; set; }

        public string I_UnitNumber { get; set; }

        public string I_StreetNumber { get; set; }

        public string I_StreetName { get; set; }

        public string I_StreetType { get; set; }

        public string I_Suburb { get; set; }

        public string I_State { get; set; }

        public string I_AddressType { get; set; }

        public string I_DeliveryType { get; set; }

        public string I_DeliveryNumber { get; set; }

        public string I_PostCode { get; set; }

        public string I_Phone { get; set; }

        public string I_PlumberLicenses { get; set; }
        #endregion

        #region Designer Details
        public string D_FirstName { get; set; }

        public string D_LastName { get; set; }

        public string D_Accreditation { get; set; }

        public string D_UnitType { get; set; }

        public string D_UnitNumber { get; set; }

        public string D_StreetNumber { get; set; }

        public string D_StreetName { get; set; }

        public string D_StreetType { get; set; }

        public string D_Suburb { get; set; }

        public string D_State { get; set; }

        public string D_AddressType { get; set; }

        public string D_DeliveryType { get; set; }

        public string D_DeliveryNumber { get; set; }

        public string D_PostCode { get; set; }

        public string D_Phone { get; set; }
        #endregion

        #region Electrician Details
        public string E_FirstName { get; set; }

        public string E_LastName { get; set; }

        public string E_License { get; set; }

        public string E_UnitType { get; set; }

        public string E_UnitNumber { get; set; }

        public string E_StreetNumber { get; set; }

        public string E_StreetName { get; set; }

        public string E_StreetType { get; set; }

        public string E_Suburb { get; set; }

        public string E_State { get; set; }

        public string E_AddressType { get; set; }

        public string E_DeliveryType { get; set; }

        public string E_DeliveryNumber { get; set; }

        public string E_PostCode { get; set; }

        public string E_Phone { get; set; }
        #endregion

        public string InstallerSignedAt { get; set; }

        public string InstallerSignedSignature { get; set; }

        public string DesignerSignedAt { get; set; }

        public string DesignerSignedSignature { get; set; }

        public string OwnerSignedAt { get; set; }

        public string OwnerSignedSignature { get; set; }

        public string WitnessSignedAt { get; set; }

        public string WitnessSignedSignature { get; set; }

        public string RetailerSignedAt { get; set; }

        public string RetailerSignedSignature { get; set; }

        public string RetailerWitnessSignedAt { get; set; }

        public string RetailerWitnessSignedSignature { get; set; }

        public string InstallerWitnessSignedAt { get; set; }

        public string InstallerWitnessSignedSignature { get; set; }

        public string DesignerWitnessSignedAt { get; set; }

        public string DesignerWitnessSignedSignature { get; set; }
    }
}
