﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;





namespace TheSolarProduct.CheckDepositReceived
{
    [Table("CheckDepositReceived")]
    public class Checkdeposite : FullAuditedEntity
    {
        public virtual string Name { get; set; }
        public virtual bool IsActive { get; set; }
    }
}
