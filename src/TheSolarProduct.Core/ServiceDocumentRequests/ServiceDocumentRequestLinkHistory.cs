﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.ServiceDocumentRequests
{
    [Table("ServiceDocumentRequestLinkHistorys")]
    public class ServiceDocumentRequestLinkHistory : FullAuditedEntity
    {
        public virtual string Token { get; set; }

        public virtual int? ServiceDocumentRequestId { get; set; }

        public virtual bool? Expired { get; set; }
    }
}
