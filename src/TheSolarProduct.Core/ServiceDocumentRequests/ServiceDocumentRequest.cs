﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;
using TheSolarProduct.Quotations;
using TheSolarProduct.Services;

namespace TheSolarProduct.ServiceDocumentRequests
{

    [Table("ServiceDocumentRequests")]
    public class ServiceDocumentRequest : FullAuditedEntity
    {
        public int ServiceId { get; set; }

        [ForeignKey("ServiceId")]
        public Service ServiceFk { get; set; }

        public int JobId { get; set; }

        [ForeignKey("JobId")]
        public Job JobFk { get; set; }

        public int ServiceDocTypeId { get; set; }

        [ForeignKey("ServiceDocTypeId")]
        public ServiceDocumentType ServiceDocTypeFk { get; set; }

        public bool IsSubmitted { get; set; }
    }
}
