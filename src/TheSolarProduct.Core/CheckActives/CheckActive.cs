﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.CheckActives
{
    
        [Table("CheckActive")]
        public class CheckActive : FullAuditedEntity
        {

            public virtual string Name { get; set; }
            public virtual Boolean IsActive { get; set; }
        }
    
}
