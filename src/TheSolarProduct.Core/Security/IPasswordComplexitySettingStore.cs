﻿using System.Threading.Tasks;

namespace TheSolarProduct.Security
{
    public interface IPasswordComplexitySettingStore
    {
        Task<PasswordComplexitySetting> GetSettingsAsync();
    }
}
