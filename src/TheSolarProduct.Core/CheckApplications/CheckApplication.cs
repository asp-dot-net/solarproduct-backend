﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TheSolarProduct.States;

namespace TheSolarProduct.CheckApplications
{
    [Table("CheckApplication")]
    public class CheckApplication : FullAuditedEntity
    {
        
        public virtual string Name { get; set; }
        public virtual Boolean IsActive { get; set; }
    }
}
