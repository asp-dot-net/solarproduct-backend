﻿using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheSolarProduct.WholeSaleLeadHistory
{
    [Table("WholeSaleLeadtrackerHistory")]
    public class WholeSaleLeadtrackerHistory : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual string FieldName { get; set; }
        public virtual string PrevValue { get; set; }
        public virtual string CurValue { get; set; }
        public virtual string Action { get; set; }
        // public virtual string DisplayField { get; set; }
        public virtual DateTime LastmodifiedDateTime { get; set; }
        public int WholeSaleLeadId { get; set; }
        public int WholeSaleLeadActivityId { get; set; }
    }
}
