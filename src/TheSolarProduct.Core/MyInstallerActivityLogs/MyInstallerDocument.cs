﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Authorization.Users;

namespace TheSolarProduct.MyInstallerActivityLogs
{
    [Table("MyInstallerDocuments")]
    public class MyInstallerDocument : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual long UserId { get; set; }

        [ForeignKey("UserId")]
        public User UserFk { get; set; }

        public virtual int OrganizationId { get; set; }
        public virtual string FileName { get; set; }
        public virtual string FilePath { get; set; }
    }
}
