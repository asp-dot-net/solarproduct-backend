﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.VppConections
{
	[Table("VppConections")]
    public class VppConection : FullAuditedEntity
    {
		public virtual string Name { get; set; }
    }
}
