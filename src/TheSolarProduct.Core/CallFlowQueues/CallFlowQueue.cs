﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.LeadSources;

namespace TheSolarProduct.CallFlowQueues
{
    [Table("CallFlowQueues")]
    public class CallFlowQueue : FullAuditedEntity
    {

        public  string Name { get; set; }

        public  int ExtentionNumber { get; set; }
        public  Boolean IsActive { get; set; }
    }
}
