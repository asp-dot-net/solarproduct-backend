﻿using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheSolarProduct.CallFlowQueues
{
    [Table("CallFlowQueueOrganizationUnits")]
    public class CallFlowQueueOrganizationUnit : FullAuditedEntity
    {
        public virtual int? CallFlowQueueId { get; set; }

        [ForeignKey("CallFlowQueueId")]
        public CallFlowQueue CallFlowQueueFk { get; set; }

        public virtual long? OrganizationUnitId { get; set; }

        [ForeignKey("OrganizationUnitId")]
        public OrganizationUnit OrganizationUnitFk { get; set; }
    }
}
