﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.LeadSources
{
    [Table("LeadSources")]
    public class LeadSource : FullAuditedEntity//, IMustHaveTenant
    {
        //public int TenantId { get; set; }

        [Required]
        [StringLength(LeadSourceConsts.MaxNameLength, MinimumLength = LeadSourceConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public virtual bool? IsActive { get; set; }

        public virtual bool? IsActiveSalesRep { get; set; }

        public virtual string ColorClass { get; set; }

        public decimal MissingPer { get; set; }



    }
}