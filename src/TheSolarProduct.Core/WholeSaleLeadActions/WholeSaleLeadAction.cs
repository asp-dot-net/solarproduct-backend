﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.WholeSaleLeadActions
{
    [Table("WholeSaleLeadAction")]
    public class WholeSaleLeadAction : FullAuditedEntity
    {
        public virtual string ActionName { get; set; }
    }
}
