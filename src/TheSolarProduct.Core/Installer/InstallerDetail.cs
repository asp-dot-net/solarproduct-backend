﻿using TheSolarProduct.Authorization.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Installer
{
	[Table("InstallerDetails")]
	public class InstallerDetail : Entity, IMustHaveTenant
	{
		public int TenantId { get; set; }

		public virtual long UserId { get; set; }

		[ForeignKey("UserId")]
		public User UserFk { get; set; }

		public virtual string InstallerAccreditationNumber { get; set; }

		public virtual DateTime InstallerAccreditationExpiryDate { get; set; }

		public virtual string DesignerLicenseNumber { get; set; }

		public virtual DateTime DesignerLicenseExpiryDate { get; set; }

		public virtual string ElectricianLicenseNumber { get; set; }

		public virtual DateTime ElectricianLicenseExpiryDate { get; set; }

		public virtual string DocInstaller { get; set; }

		public virtual string DocDesigner { get; set; }

		public virtual string DocElectrician { get; set; }

		public virtual string OtherDocsCSV { get; set; }

		public virtual bool? IsInst { get; set; }

		public virtual bool? IsDesi { get; set; }

		public virtual bool? IsElec { get; set; }

		//Address Detail
		public virtual string Address { get; set; }

		public virtual string IsGoogle { get; set; }

		public virtual string latitude { get; set; }

		public virtual string longitude { get; set; }

		public virtual string Unit { get; set; }

		public virtual string UnitType { get; set; }

		public virtual string StreetNo { get; set; }

		public virtual string StreetType { get; set; }

		public virtual string StreetName { get; set; }

		public virtual string Suburb { get; set; }

		public virtual int? StateId { get; set; }

		public virtual string PostCode { get; set; }
		
		public int? SourceTypeId { get; set; }
		
		public string AreaName { get; set; }
        
		public string Notes { get; set; }
		
		public bool? SmsSend { get; set; }
		
		public DateTime? SmsSendDate { get; set; }
		
		public bool? EmailSend { get; set; }
		
		public DateTime? EmailSendDate { get; set; }

		public virtual string Name { get; set; }

		public virtual string Surname { get; set; }

		public virtual string SAANumber { get; set; }
	}
}