﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Installer
{
    [Table("InstallerAvailability")]
    public class InstallerAvailability : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual long? UserId { get; set; }
        public DateTime AvailabilityDate { get; set; }
    }
}
