﻿using TheSolarProduct.Authorization.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;


namespace TheSolarProduct.Installer
{
    [Table("InstallerContracts")]
    public class InstallerContract : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual long UserId { get; set; }

        [ForeignKey("UserId")]
        public User UserFk { get; set; }

        public virtual int OrganizationId { get; set; }
        public virtual string FileName { get; set; }
        public virtual string FilePath { get; set; }
    }
}
