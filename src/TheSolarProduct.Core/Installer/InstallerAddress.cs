﻿using TheSolarProduct.Authorization.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Installer
{
	[Table("InstallerAddresses")]
	public class InstallerAddress : Entity, IMustHaveTenant
	{
		public int TenantId { get; set; }

		public virtual long? UserId { get; set; }

		[ForeignKey("UserId")]
		public User UserFk { get; set; }

		public virtual string Address { get; set; }

		public virtual string IsGoogle { get; set; }

		public virtual string latitude { get; set; }

		public virtual string longitude { get; set; }

		public virtual string Unit { get; set; }

		public virtual string UnitType { get; set; }

		public virtual string StreetNo { get; set; }

		public virtual string StreetType { get; set; }

		public virtual string StreetName { get; set; }

		public virtual string Suburb { get; set; }

		public virtual int? StateId { get; set; }

		//public string StateName { get; set; }

		public virtual string PostCode { get; set; }

	}
}