﻿using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using TheSolarProduct.VoucherMasters;

namespace TheSolarProduct.Jobs
{
	[Table("JobPromotions")]
    public class JobPromotion : FullAuditedEntity , IMustHaveTenant
    {
		public int TenantId { get; set; }
		
		public virtual int JobId { get; set; }
		
		public virtual string TrackingNumber { get; set; }
		
		public virtual string Description { get; set; }
		
		[ForeignKey("JobId")]
		public Job JobFk { get; set; }
		
		public virtual int? PromotionMasterId { get; set; }
        
		[ForeignKey("PromotionMasterId")]
		public PromotionMaster PromotionMasterFk { get; set; }

		public virtual DateTime? DispatchedDate { get; set; }

		public virtual int? FreebieTransportId { get; set; }

		[ForeignKey("FreebieTransportId")]
		public FreebieTransport FreebieTransportIdFk { get; set; }

		public virtual Boolean? SmsSend { get; set; }

		public virtual Boolean? EmailSend { get; set; }

		public virtual DateTime? SmsSendDate { get; set; }

		public virtual DateTime? EmailSendDate { get; set; }

		public string SerialNo { get; set; }
        public virtual int? VoucherId { get; set; }

        [ForeignKey("VoucherId")]
        public VoucherMaster VoucherMasterFk { get; set; }
    }
}