﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.ReviewTypes;

namespace TheSolarProduct.Jobs
{
    [Table("JobReview")]
    public class JobReview : FullAuditedEntity, IMustHaveTenant
    {
		public int TenantId { get; set; }

		public virtual int? Rating { get; set; }
		public virtual string ReviewNotes { get; set; }

		public virtual int? ReviewTypeId { get; set; }

		[ForeignKey("ReviewTypeId")]
		public ReviewType ReviewType { get; set; }

		public virtual int? JobId { get; set; }

		[ForeignKey("JobId")]
		public Job JobFk { get; set; }

        public virtual string FileName { get; set; }

        public virtual string FilePath { get; set; }


    }
}
