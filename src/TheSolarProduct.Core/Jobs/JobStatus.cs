﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
	[Table("JobStatuses")]
    public class JobStatus : Entity//, IMustHaveTenant
	{

		[Required]
		[StringLength(JobStatusConsts.MaxNameLength, MinimumLength = JobStatusConsts.MinNameLength)]
		public virtual string Name { get; set; }

		public virtual string ColorClass { get; set; }

        public virtual Boolean IsActive { get; set; }
    }
}