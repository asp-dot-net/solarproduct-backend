﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Jobs
{
    [Table("JobSuggestedProducts")]
    public class JobSuggestedProduct : FullAuditedEntity
    {
        public virtual int? JobId { get; set; }

        [ForeignKey("JobId")]
        public Job JobFk { get; set; }

        public virtual string Name { get; set; }
        public virtual decimal? Amount { get; set; }

        
    }
}
