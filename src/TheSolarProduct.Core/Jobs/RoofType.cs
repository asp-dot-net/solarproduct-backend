﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
	[Table("RoofTypes")]
    public class RoofType : FullAuditedEntity//, IMustHaveTenant
	{

		[Required]
		[StringLength(RoofTypeConsts.MaxNameLength, MinimumLength = RoofTypeConsts.MinNameLength)]
		public virtual string Name { get; set; }
		
		public virtual int? DisplayOrder { get; set; }
        public virtual Boolean IsActive { get; set; }
    }
}