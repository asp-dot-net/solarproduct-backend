﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
	[Table("PromotionOffers")]
    public class PromotionOffer : Entity , IMustHaveTenant
    {
			public int TenantId { get; set; }
			

		public virtual string Name { get; set; }
		

    }
}