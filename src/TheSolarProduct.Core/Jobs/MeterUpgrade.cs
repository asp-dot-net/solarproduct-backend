﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
	[Table("MeterUpgrades")]
    public class MeterUpgrade : FullAuditedEntity//, IMustHaveTenant
	{

		public virtual string Name { get; set; }
		
		public virtual int? DisplayOrder { get; set; }
		//public int TenantId { get; set; }
	}
}