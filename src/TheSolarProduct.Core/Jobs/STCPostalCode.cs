﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
    [Table("STCPostalCodes")]
	public class STCPostalCode : Entity
	{
		
		public string PostCodeFrom { get; set; }
		public string PostCodeTo { get; set; }
		public int ZoneId { get; set; }
	}
}