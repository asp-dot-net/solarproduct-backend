﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Jobs
{
    [Table("QLDBatteryRebates")]
    public class QLDBatteryRebate : FullAuditedEntity
    {
        public int? ItemId { get; set; }

        public string ItemName { get; set; }

        public int? Qty { get; set; }

        public string RefNo { get; set; }
    }
}
