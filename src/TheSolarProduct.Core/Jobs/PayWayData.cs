﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Jobs
{
	[Table("PayWayDatas")]

	public class PayWayData : FullAuditedEntity //,IMustHaveTenant
	{

        public int jobId { get; set; }
        [ForeignKey("jobId")]
        public Job JobFk { get; set; }
        public string transactionId { get; set; }
        public string receiptNumber { get; set; }
        public string status { get; set; }
        public string responseCode { get; set; }
        public string responseText { get; set; }
        public string transactionType { get; set; }
        public string customerNumber { get; set; }
        public string customerName { get; set; }
        public string currency { get; set; }
        public decimal? principalAmount { get; set; }
        public decimal? surchargeAmount { get; set; }
        public decimal? paymentAmount { get; set; }
        public string paymentMethod { get; set; }
        public int? PaymentType { get; set; }
        public int? OrganizationId { get; set; }

    }
}

 