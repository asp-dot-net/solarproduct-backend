﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
	[Table("ProductTypes")]
    public class ProductType : FullAuditedEntity , IMustHaveTenant
    {
        public int TenantId { get; set; }
		[Required]
		[StringLength(ProductTypeConsts.MaxNameLength, MinimumLength = ProductTypeConsts.MinNameLength)]
		public virtual string Name { get; set; }

        public virtual string Image { get; set; }

    }
}