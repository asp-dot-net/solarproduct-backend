﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Jobs
{
    [Table("PylonDocuments")]
    public class PylonDocument : FullAuditedEntity
    {
        public int JobId { get; set; }

        [ForeignKey("JobId")]
        public Job JobFk { get; set; }

        public string Url { get; set; }
    }
}
