﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
	[Table("JobProductItems")]
    public class JobProductItem : FullAuditedEntity , IMustHaveTenant
    {
		public int TenantId { get; set; }
			
		public virtual int? Quantity { get; set; }
		
		public virtual int? JobId { get; set; }
		
        [ForeignKey("JobId")]
		public Job JobFk { get; set; }
		
		public virtual int? ProductItemId { get; set; }
		
        [ForeignKey("ProductItemId")]
		public ProductItem ProductItemFk { get; set; }

		public string RefNo { get; set; }
		
    }
}