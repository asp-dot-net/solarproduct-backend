﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Jobs
{
	[Table("InstallerInvoiceTypes")]
    public class InstallerInvoiceType : FullAuditedEntity
    {
		public virtual string Type { get; set; }
    }
}
