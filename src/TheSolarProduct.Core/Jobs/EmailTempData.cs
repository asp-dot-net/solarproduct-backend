﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Jobs
{
    [Table("EmailTempDatas")]
  public  class EmailTempData : FullAuditedEntity
    {
        public virtual int? DocumentTypeId { get; set; } 
        public virtual int? JobId { get; set; }
        public virtual string FileName { get; set; }
        public virtual string FileType { get; set; }
        public virtual string FilePath { get; set; }

        
    }
}

