﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Jobs
{
    [Table("ProductItemLocation")]
    public class ProductItemLocation : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual int ProductItemId { get; set; }

        [ForeignKey("ProductItemId")]
        public ProductItem ProductItemIdFk { get; set; }

        public virtual int WarehouselocationId { get; set; }

        [ForeignKey("WarehouselocationId")]
        public Warehouselocation WarehouselocationIdFk { get; set; }

        public virtual bool SalesTag { get; set; }
    }
}
