﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
	[Table("ElecDistributors")]
    public class ElecDistributor : FullAuditedEntity//, IMustHaveTenant
	{

		[Required]
		[StringLength(ElecDistributorConsts.MaxNameLength, MinimumLength = ElecDistributorConsts.MinNameLength)]
		public virtual string Name { get; set; }
		
		public virtual int? Seq { get; set; }
		
		public virtual bool NSW { get; set; }
		
		public virtual bool SA { get; set; }
		
		public virtual bool QLD { get; set; }
		
		public virtual bool VIC { get; set; }
		
		public virtual bool WA { get; set; }
		
		public virtual bool ACT { get; set; }
		
		public virtual string TAS { get; set; }
		
		public virtual bool NT { get; set; }
		
		public virtual string ElectDistABB { get; set; }
		
		public virtual bool ElecDistAppReq { get; set; }
		
		public virtual int? GreenBoatDistributor { get; set; }
	    public virtual Boolean IsActive { get; set; }
	}
}