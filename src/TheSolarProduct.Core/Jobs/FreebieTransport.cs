﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using IdentityServer4.Models;

namespace TheSolarProduct.Jobs
{
    [Table("FreebieTransports")]
    public class FreebieTransport : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual string Name { get; set; }

        public virtual string TransportLink { get; set; }

       public Boolean IsActive { get; set; }

    }
}