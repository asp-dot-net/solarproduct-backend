﻿using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Jobs
{
    [Table("JobSuggestedProductItemLists")]
    public class JobSuggestedProductItemList : FullAuditedEntity
    
    {
        public virtual int? SuggestedProductId { get; set; }

        [ForeignKey("SuggestedProductId")]
        public JobSuggestedProduct JobSuggestedProductFk { get; set; }

        public virtual int? ProductItemId { get; set; }

        [ForeignKey("ProductItemId")]
        public ProductItem ProductItemFk { get; set; }

        public virtual int? Quantity { get; set; }
    }
}
