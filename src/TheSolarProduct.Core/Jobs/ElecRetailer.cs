﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
	[Table("ElecRetailers")]
    public class ElecRetailer : FullAuditedEntity//, IMustHaveTenant
	{

		public virtual string Name { get; set; }
		
		public virtual bool NSW { get; set; }
		
		public virtual bool SA { get; set; }
		
		public virtual bool QLD { get; set; }
		
		public virtual bool VIC { get; set; }
		
		public virtual bool WA { get; set; }
		
		public virtual bool ACT { get; set; }
		
		public virtual bool TAS { get; set; }
		
		public virtual bool NT { get; set; }
		
		public virtual int? ElectricityProviderId { get; set; }
        public virtual Boolean IsActive { get; set; }
    }
}