﻿using Abp.Auditing;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Jobs
{
    [Table("Jobswarranty")]
    [Audited]
    public class Jobwarranty : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }


        public virtual int? JobId { get; set; }

        [ForeignKey("JobId")]
        public JobType JobFk { get; set; }

        public virtual int? ProductTypeId { get; set; }

        public virtual string Filename { get; set; }
        public virtual string Filepath { get; set; }
        public virtual string FileType { get; set; }

    }
}
