﻿using Abp.Domain.Entities.Auditing;
using Org.BouncyCastle.Asn1.Mozilla;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.PriceItemLists;

namespace TheSolarProduct.Jobs
{
    [Table("InstallerInvoicePriceLists")]
    public class InstallerInvoicePriceList : FullAuditedEntity
    {
        public int? PriceItemId { get; set; }

        [ForeignKey("PriceItemId")]
        public PriceItemList PriceItemListFK { get; set; } 

        public decimal? Amount { get; set; }

        public int? InstallerInvoId { get; set; }

        [ForeignKey("InstallerInvoId")]
        public InstallerInvoice InstallerInvoiceFK { get; set; }

        public string Notes { get; set; }

    }
}
