﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
	[Table("MeterPhases")]
    public class MeterPhase : FullAuditedEntity//, IMustHaveTenant
	{

		public virtual string Name { get; set; }
		
		public virtual int? DisplayOrder { get; set; }
		//public int TenantId { get; set; }
	}
}