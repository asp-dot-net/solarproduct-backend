﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
	[Table("JobTypes")]
    public class JobType : FullAuditedEntity //,IMustHaveTenant
    {

		[Required]
		[StringLength(JobTypeConsts.MaxNameLength, MinimumLength = JobTypeConsts.MinNameLength)]
		public virtual string Name { get; set; }
		
		public virtual int? DisplayOrder { get; set; }

		public Boolean IsActive { get; set; }
	}
}