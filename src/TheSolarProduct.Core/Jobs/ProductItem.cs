﻿using TheSolarProduct.Jobs;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
    [Table("ProductItems")]
    public class ProductItem : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(ProductItemConsts.MaxNameLength, MinimumLength = ProductItemConsts.MinNameLength)]
        public virtual string Name { get; set; }

        [StringLength(ProductItemConsts.MaxManufacturerLength, MinimumLength = ProductItemConsts.MinManufacturerLength)]
        public virtual string Manufacturer { get; set; }

        [StringLength(ProductItemConsts.MaxModelLength, MinimumLength = ProductItemConsts.MinModelLength)]
        public virtual string Model { get; set; }

        //[StringLength(ProductItemConsts.MaxSeriesLength, MinimumLength = ProductItemConsts.MinSeriesLength)]
        public virtual string Series { get; set; }

        public virtual decimal? Size { get; set; }
        
        public virtual int? Code { get; set; }

        public virtual DateTime? ApprovedDate { get; set; }

        public virtual DateTime? ExpiryDate { get; set; }
        
        public virtual int ProductTypeId { get; set; }

        [ForeignKey("ProductTypeId")]
        public ProductType ProductTypeFk { get; set; }
        
        public virtual bool Active { get; set; }

        public virtual string FileName { get; set; }
        
        public virtual string FileType { get; set; }
        
        public virtual string FilePath { get; set; }

        public int? PerformanceWarranty { get; set; }

        public int? ProductWarranty { get; set; }

        public int? ExtendedWarranty { get; set; }

        public int? WorkmanshipWarranty { get; set; }

        public decimal? PricePerWatt { get; set; }

        public decimal? Amount { get; set; }

        public virtual bool ECommerce { get; set; }

        public virtual string ImageName { get; set; }

        public virtual string ImagePath { get; set; }

        public virtual int? GreenDealBrandId { get; set; }

        public string ManuPhone { get; set; }

        public string ManuMobile { get; set; }

        public string ManuEmail { get; set; }

        public string ManuWebsite { get; set; }

        public bool IsScanned { get; set; }

    }
}