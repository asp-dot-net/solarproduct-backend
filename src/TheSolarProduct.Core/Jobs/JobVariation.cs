﻿using TheSolarProduct.Jobs;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
	[Table("JobVariations")]
    public class JobVariation : FullAuditedEntity, IMustHaveTenant
    {
		public int TenantId { get; set; }

		public virtual decimal? Cost { get; set; }

		public virtual int? VariationId { get; set; }
		
        [ForeignKey("VariationId")]
		public Variation VariationFk { get; set; }
		
		public virtual int? JobId { get; set; }
		
        [ForeignKey("JobId")]
		public Job JobFk { get; set; }

        public string Notes { get; set; }

    }
}