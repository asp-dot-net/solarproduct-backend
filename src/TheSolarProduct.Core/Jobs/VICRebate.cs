﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Jobs
{
    [Table("VICRebate")]
    public class VICRebate : FullAuditedEntity
    {
        public string Status { get; set; }
    }
}
