﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
    [Table("STCZoneRatings")]
	public class STCZoneRating : Entity
	{
		[Column(TypeName = "decimal(8,3)")]
		public decimal Rating { get; set; }
		public string UpSizeTs { get; set; }
	}
}