﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
    [Table("STCYearWiseRates")]
	public class STCYearWiseRate : Entity
	{
		public int Year { get; set; }
		public int Rate { get; set; }
	}
}