﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
	[Table("Variations")]
    public class Variation : Entity , IMustHaveTenant
    {
			public int TenantId { get; set; }
			

		public virtual string Name { get; set; }
		
		[Required]
		[StringLength(VariationConsts.MaxActionLength, MinimumLength = VariationConsts.MinActionLength)]
		public virtual string Action { get; set; }
        public virtual Boolean IsActive { get; set; }

    }
}