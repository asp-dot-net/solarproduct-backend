﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
	[Table("PaymentOptions")]
    public class PaymentOption : FullAuditedEntity , IMustHaveTenant
    {
			public int TenantId { get; set; }
			

		public virtual string Name { get; set; }
		
		public virtual int? DisplayOrder { get; set; }
		
		public decimal? EstablishmentFee { get; set; }

		public decimal? MonthlyAccFee { get; set; }

		public Boolean IsActive { get; set; }	


    }
}