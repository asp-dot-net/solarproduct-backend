﻿using TheSolarProduct.Jobs;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using TheSolarProduct.Invoices;

namespace TheSolarProduct.Jobs
{
	[Table("JobRefunds")]
    public class JobRefund : FullAuditedEntity , IMustHaveTenant
    {
		public int TenantId { get; set; }

		public virtual decimal? Amount { get; set; }

		public virtual decimal? PaidAmount { get; set; }
		
		//[Required]
		public virtual string BankName { get; set; }
		
		public virtual string AccountName { get; set; }
		
		public virtual string BSBNo { get; set; }
		
		//[Required]
		public virtual string AccountNo { get; set; }
		
		public virtual string Notes { get; set; }
		
		public virtual DateTime? PaidDate { get; set; }
		
		public virtual string Remarks { get; set; }
		

		public virtual int? PaymentOptionId { get; set; }
		
        [ForeignKey("PaymentOptionId")]
		//public PaymentMethods PaymentOptionFk { get; set; }
		public InvoicePaymentMethod PaymentOptionFk { get; set; }
		
		public virtual int? JobId { get; set; }
		
        [ForeignKey("JobId")]
		public Job JobFk { get; set; }
		
		public virtual int? RefundReasonId { get; set; }
		
        [ForeignKey("RefundReasonId")]
		public RefundReason RefundReasonFk { get; set; }

		public virtual string ReceiptNo { get; set; }

		public virtual Boolean? JobRefundSmsSend { get; set; }

		public virtual Boolean? JobRefundEmailSend { get; set; }

		public virtual DateTime? JobRefundSmsSendDate { get; set; }

		public virtual DateTime? JobRefundEmailSendDate { get; set; }

		public virtual Boolean? IsVerify { get; set; }
        public virtual Boolean? XeroInvCreated { get; set; }
        public virtual string RefundPaymentType { get; set; }
	}
}