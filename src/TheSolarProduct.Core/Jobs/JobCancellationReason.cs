﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Jobs
{
    [Table("JobCancellationReason")]
    public class JobCancellationReason : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual string Name { get; set; }
        public virtual Boolean IsActive { get; set; }
    }
}
