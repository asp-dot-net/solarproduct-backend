﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
	[Table("FinanceOptions")]
    public class FinanceOption : FullAuditedEntity , IMustHaveTenant
    {
		public int TenantId { get; set; }

		public virtual string Name { get; set; }
		
		public virtual int? DisplayOrder { get; set; }

		public virtual decimal? Percentage { get; set; }
		public Boolean IsActive {  get; set; }

    }
}