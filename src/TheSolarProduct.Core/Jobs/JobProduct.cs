﻿using TheSolarProduct.Jobs;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.Jobs
{
	[Table("JobProducts")]
	public class JobProduct : FullAuditedEntity, IMustHaveTenant
	{
		public int TenantId { get; set; }

		public virtual int? Quantity { get; set; }

		public virtual int? ProductItemId { get; set; }

		[ForeignKey("ProductItemId")]
		public ProductItem ProductItemFk { get; set; }

		public virtual int? JobId { get; set; }

		[ForeignKey("JobId")]
		public Job JobFk { get; set; }

		public virtual string Model { get; set; }
	}
}