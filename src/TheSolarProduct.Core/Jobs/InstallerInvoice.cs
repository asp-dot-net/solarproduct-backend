﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Jobs
{
    [Table("InstallerInvoices")]
    public class InstallerInvoice : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual int? JobId { get; set; }
        [ForeignKey("JobId")]
        public Job JobFk { get; set; }
        public virtual int? InvTypeId { get; set; }

        public virtual decimal? Amount { get; set; }

        public virtual DateTime? InvDate { get; set; }

        public virtual decimal? AdvanceAmount { get; set; }

        public virtual decimal? LessDeductAmount { get; set; }

        public virtual decimal? TotalAmount { get; set; }

        public virtual DateTime? AdvancePayDate { get; set; }

        public virtual DateTime? PayDate { get; set; }

        public virtual DateTime? PaymentTypeId { get; set; }

        public virtual int? InstallerId { get; set; }

        public virtual string Notes { get; set; }

        public virtual int? InvNo { get; set; }

        public virtual DateTime? Date { get; set; }

        public virtual string Remarks { get; set; }

        public virtual bool IsPaid { get; set; }

        public virtual bool IsVerify { get; set; }

        public virtual int? PaymentsTypeId { get; set; }
        public virtual string FileName { get; set; }
        public virtual string FilePath { get; set; }
        public int? Installer_selfie { get; set; }
        public int? Front_of_property { get; set; }
        public int? Inst_Des_Ele_sign { get; set; }
        public int? Cx_Sign { get; set; }
        public int? CES { get; set; }
        public int? PanelsSerialNo { get; set; }
        public int? InstalaltionPic { get; set; }
        public int? InverterSerialNo { get; set; }
        public int? Wi_FiDongle { get; set; }
        public int? Traded { get; set; }
        public string Remark_if_owing { get; set; }
        public int? Noof_Panels_Portal { get; set; }
        public int? ActualPanels_Installed { get; set; }
        public int? Splits { get; set; }
        public string TravelKMfromWarehouse { get; set; }
        public string otherExtraInvoiceNumber { get; set; }
        public string Installation_Maintenance_Inspection { get; set; }
        public string AllGood_NotGood { get; set; }
        public string NotesOrReasonforPending { get; set; }
        public string EmailSent { get; set; }
        public string InvoiceNo { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public int? Noof_Panels_invoice { get; set; }
        public virtual DateTime? ApproedDate { get; set; }
        public virtual decimal? ApproedAmount { get; set; }
        public string ApprovedNotes { get; set; }

        public string BankRefNo { get; set; }
        public virtual DateTime? invoiceIssuedDate { get; set; }
        public virtual DateTime? SmsSendDate { get; set; }
        public virtual DateTime? EmaiSendDate { get; set; }
        public virtual Boolean? SmsSend { get; set; }
        public virtual Boolean? installerEmailSend { get; set; }
        public virtual DateTime? DueDate { get; set; }
        public virtual int? PriorityId { get; set; }

        public bool? IsGSTInclude { get; set; }
    }
}
