﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Jobs
{
    [Table("SolarRebateStatus")]
    public class SolarRebateStatus : FullAuditedEntity
    {
        public string Status { get; set; }

        public string ColorClass { get; set; }
    }
}