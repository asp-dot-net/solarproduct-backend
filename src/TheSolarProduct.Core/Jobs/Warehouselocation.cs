﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Jobs
{
    [Table("WareHouseLocation")]
    public class Warehouselocation : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual string location { get; set; }
        
        public virtual string state { get; set; }

        public virtual string Address { get; set; }

        public virtual string UnitNo { get; set; }

        public virtual string UnitType { get; set; }

        public virtual string StreetNo { get; set; }

        public virtual string StreetName { get; set; }

        public virtual string StreetType { get; set; }

        public virtual string PostCode { get; set; }

        public virtual string Latitude { get; set; }

        public virtual string Longitude { get; set; }
        public virtual string Suburb { get; set; }
    }
}
