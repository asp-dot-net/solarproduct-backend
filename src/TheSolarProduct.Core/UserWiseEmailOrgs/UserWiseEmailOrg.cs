﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.UserWiseEmailOrgs
{
    [Table("UserWiseEmailOrgs")]
    public class UserWiseEmailOrg : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        public virtual long UserId { get; set; }

        public virtual long OrganizationUnitId { get; set; }
        public virtual string EmailFromAdress { get; set; }
    }
}
