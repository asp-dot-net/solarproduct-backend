﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.TheSolarDemo
{
	[Table("Categories")]
    public class Category : FullAuditedEntity //, IMayHaveTenant
    {
			//public int? TenantId { get; set; }
			

		[StringLength(CategoryConsts.MaxNameLength, MinimumLength = CategoryConsts.MinNameLength)]
		public virtual string Name { get; set; }

		public Boolean IsActive {  get; set; }
		

    }
}