﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.TheSolarDemo
{
	[Table("Teams")]
    public class Team : FullAuditedEntity //, IMayHaveTenant
    {
			//public int? TenantId { get; set; }
			

		[Required]
		[StringLength(TeamConsts.MaxNameLength, MinimumLength = TeamConsts.MinNameLength)]
		public virtual string Name { get; set; }
        public virtual Boolean IsActive { get; set; }

    }
}