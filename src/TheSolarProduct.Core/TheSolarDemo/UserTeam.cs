﻿using TheSolarProduct.Authorization.Users; 
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.TheSolarDemo
{
	[Table("UserTeams")]
    public class UserTeam : FullAuditedEntity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			


		public virtual long? UserId { get; set; }
		
        [ForeignKey("UserId")]
		public User UserFk { get; set; }
		
		public virtual int? TeamId { get; set; }
		
        [ForeignKey("TeamId")]
		public Team TeamFk { get; set; }
		
    }
}