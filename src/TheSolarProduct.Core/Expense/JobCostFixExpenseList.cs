﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.InstallationCost.InstallationItemLists;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.Expense
{
    [Table("JobCostFixExpenseLists")]
    public class JobCostFixExpenseList : FullAuditedEntity
    {
        [ForeignKey("JobCostFixExpensePeriodId")]
        public JobCostFixExpensePeriod JobCostFixExpensePeriodFK { get; set; }

        public virtual int JobCostFixExpensePeriodId { get; set; }

        [ForeignKey("JobCostFixExpenseTypeId")]
        public JobCostFixExpenseType JobCostFixExpenseTypeFK { get; set; }

        public virtual int JobCostFixExpenseTypeId { get; set; }

        public virtual decimal? Amount { get; set; }

    }
}
