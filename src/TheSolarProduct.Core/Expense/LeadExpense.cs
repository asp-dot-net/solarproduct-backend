﻿using TheSolarProduct.TheSolarDemo;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using System.Collections.Generic;
using TheSolarProduct.LeadSources;
using TheSolarProduct.States;

namespace TheSolarProduct.Expense
{
	[Table("LeadExpenses")]
    public class LeadExpense : FullAuditedEntity , IMayHaveTenant //, IValidatableObject
    {
		 public int? TenantId { get; set; } 
		public virtual DateTime FromDate { get; set; }
		
		public virtual DateTime ToDate { get; set; }
		
		public virtual decimal Amount { get; set; }

		public virtual int? LeadSourceId { get; set; }
		
        [ForeignKey("LeadSourceId")]
		public LeadSource LeadSourceFk { get; set; }

		public virtual int? LeadinvestmentId { get; set; }

		[ForeignKey("LeadinvestmentId")]
		public LeadExpenseInvestment LeadinvestmentFk { get; set; }

        public virtual int? StateId { get; set; }
        [ForeignKey("StateId")]
        public State StatusFk { get; set; }

        public virtual decimal dailyamount { get; set; }
        //     public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        //     {
        //if (ToDate < FromDate) { 

        //	yield return
        //		new ValidationResult(
        //		 errorMessage: "To Date must be greater than From Date",
        //                               memberNames: new[] { "EndDate" });
        //}
        //         throw new NotImplementedException();
        //     }
    }

    public class statedetail
    {
        public int StateId { get; set; }
        public decimal Amount { get; set; }
        public virtual decimal dailyamount { get; set; }
    }
}