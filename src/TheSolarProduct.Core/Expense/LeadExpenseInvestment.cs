﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Expense
{
	[Table("LeadExpenseInvestments")]
	public class LeadExpenseInvestment : FullAuditedEntity, IMayHaveTenant //, IValidatableObject
	{
		public int? TenantId { get; set; }
		public virtual int OrganizationId { get; set; } 
		public virtual DateTime FromDate { get; set; } 
		public virtual DateTime ToDate { get; set; } 
		public virtual string MonthYear { get; set; }
		public virtual float Investment { get; set; }

		
	}
}
