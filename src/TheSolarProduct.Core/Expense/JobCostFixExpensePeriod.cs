﻿using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Expense
{
    [Table("JobCostFixExpensePeriods")]
    public class JobCostFixExpensePeriod : FullAuditedEntity
    {
        [ForeignKey("OrganizationUnit")]
        public OrganizationUnit OrganizationUnitFk { get; set; }

        public virtual long OrganizationUnit { get; set; }

        public virtual string Name { get; set; }

        public virtual int Month { get; set; }

        public virtual int Year { get; set; }
    }
}
