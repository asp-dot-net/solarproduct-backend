﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Expense
{
	[Table("JobCostFixExpenseTypes")]
    public class JobCostFixExpenseType : FullAuditedEntity
    {
        public string Type { get; set; }
    }
}
