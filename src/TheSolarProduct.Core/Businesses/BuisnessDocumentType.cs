﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Businesses
{
    [Table("BuisnessDocumentTypes")]
    public class BuisnessDocumentType : FullAuditedEntity
    {
        public string DocType { get; set; }

        public bool IsActive { get; set; }
    }
}
