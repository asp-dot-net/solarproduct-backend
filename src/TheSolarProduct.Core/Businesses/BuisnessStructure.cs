﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Businesses
{
    [Table("BuisnessStructures")]
    public class BuisnessStructure : FullAuditedEntity
    {
        public string Structure { get; set; }

        public bool IsActive { get; set; }
    }
}
