﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Castle.MicroKernel.Registration;
using Microsoft.VisualBasic;
using PayPalCheckoutSdk.Orders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.States;
using TheSolarProduct.WholeSaleLeads;

namespace TheSolarProduct.Businesses
{
    [Table("Businesses")]
    public class Business : FullAuditedEntity
    {
        public string ABNNumber { get; set; }

        public int? BuisnessStructureId { get; set; }

        public int? UserId { get; set; }

        [ForeignKey("BuisnessStructureId")]
        public BuisnessStructure BuisnessStructureFk { get; set; }

        public DateTime? YearOfInspaction { get; set; }

        public decimal? LastFinancialYearTurnover { get; set; }

        public decimal? AverageMonthlyTurnover { get; set; } 

        public decimal? NetProfit { get; set; }

        public string CECRegistrationNumber { get; set; }

        public string ElectricalLicence { get; set; }

        public int? NumberOfEmployees { get; set; }

        public int? NumberOfOffices { get; set; }

        public string LegalEntityName { get; set; }

        public string MainTradingName { get; set; }

        public string OtherTradingNames { get; set; }

        public decimal? CreditLimitRequested { get; set; }

        public string PrimaryFullName { get; set; }

        public string PrimaryPosition { get; set; }

        public string PrimaryEmail { get; set; }

        public string PrimaryContactNo { get; set; }

        public string PrimaryFAX { get; set; }

        public string APCFullName { get; set; }

        public string APCEmail { get; set; }

        public string APCContactNo { get; set; }

        public string APCFAX { get; set; }

        public string DP_Name { get; set; }

        public DateTime? DP_DOB { get; set; }

        public string DP_Email { get; set; }
        
        public string DP_Phone { get; set; }
        
        public string DP_Mobile { get; set; }

        public string DP_PassportNo { get; set; }

        public string DP_PassportCountry { get; set; }

        public int? DP_BuisnessDocumentTypeId { get; set; }

        [ForeignKey("DP_BuisnessDocumentTypeId")]
        public BuisnessDocumentType DP_BuisnessDocumentTypeFK { get; set; }

        public  string DP_UnitNo { get; set; }

        public  string DP_UnitType { get; set; }

        public  string DP_StreetNo { get; set; }

        public  string DP_StreetName { get; set; }

        public  string DP_StreetType { get; set; }

        public int? DP_StateId { get; set; }

        [ForeignKey("DP_StateId")]
        public State DP_StateFK { get; set; }

        public  string DP_PostCode { get; set; }

        public  string DP_Latitude { get; set; }

        public  string DP_Longitude { get; set; }

        public string DP_Name2 { get; set; }

        public DateTime? DP_DOB2 { get; set; }

        public string DP_Email2 { get; set; }

        public string DP_Phone2 { get; set; }

        public string DP_Mobile2 { get; set; }

        public string DP_PassportNo2 { get; set; }

        public string DP_PassportCountry2 { get; set; }
        
        public int? DP_BuisnessDocumentTypeId2 { get; set; }

        [ForeignKey("DP_BuisnessDocumentTypeId2")]
        public BuisnessDocumentType DP_BuisnessDocumentTypeFK2 { get; set; }

        public  string DP_UnitNo2 { get; set; }

        public  string DP_UnitType2 { get; set; }

        public  string DP_StreetNo2 { get; set; }

        public  string DP_StreetName2 { get; set; }

        public  string DP_StreetType2 { get; set; }

        public int? DP_StateId2 { get; set; }

        [ForeignKey("DP_StateId2")]
        public State DP_StateFK2 { get; set; }

        public  string DP_PostCode2 { get; set; }

        public  string DP_Latitude2 { get; set; }

        public  string DP_Longitude2 { get; set; }

        public string R_UnitNo { get; set; }

        public string R_UnitType { get; set; }

        public string R_StreetNo { get; set; }

        public string R_StreetName { get; set; }

        public string R_StreetType { get; set; }

        public int? R_StateId { get; set; }

        [ForeignKey("R_StateId")]
        public State R_StateFK { get; set; }

        public string R_PostCode { get; set; }

        public string R_Latitude { get; set; }

        public string R_Longitude { get; set; }

        public string B_UnitNo { get; set; }

        public string B_UnitType { get; set; }

        public string B_StreetNo { get; set; }

        public string B_StreetName { get; set; }

        public string B_StreetType { get; set; }

        public int? B_StateId { get; set; }

        [ForeignKey("B_StateId")]
        public State B_StateFK { get; set; }

        public string B_PostCode { get; set; }

        public string B_Latitude { get; set; }

        public string B_Longitude { get; set; }

        public string TradeFullName { get; set; }

        public string TradeCompanyName { get; set; }

        public string TradePhone { get; set; }

        public string TradeEmail { get; set; }

        public string TradeFullName2 { get; set; }

        public string TradeCompanyName2 { get; set; }

        public string TradePhone2 { get; set; }

        public string TradeEmail2 { get; set; }

        public string TradeFullName3 { get; set; }

        public string TradeCompanyName3 { get; set; }

        public string TradePhone3 { get; set; }

        public string TradeEmail3 { get; set; }

        public string GuarantorName { get; set; }

        public string GuarantorCompanyName { get; set; }

        public string GuarantorPhone { get; set; }

        public string GuarantorEmail { get; set; }

        public string GuarantorName2 { get; set; }

        public string GuarantorCompanyName2 { get; set; }

        public string GuarantorPhone2 { get; set; }

        public string GuarantorEmail2 { get; set; }

        public string LicencePassportDoc { get; set; }

        public string AddressDoc { get; set; }

        public string Notes { get; set; }

        public string Status { get; set; }

        public decimal? ApprovedAmount { get; set; }

        public int? WholeSaleLeadId { get; set; }   

        [ForeignKey("WholeSaleLeadId")]
        public WholeSaleLead WholeSaleLeadFK { get; set; }

        public bool IsConfirm { get; set; }

        public bool IsApproved { get; set; }

    }
}
