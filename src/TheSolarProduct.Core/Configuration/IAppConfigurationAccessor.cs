﻿using Microsoft.Extensions.Configuration;

namespace TheSolarProduct.Configuration
{
    public interface IAppConfigurationAccessor
    {
        IConfigurationRoot Configuration { get; }
    }
}
