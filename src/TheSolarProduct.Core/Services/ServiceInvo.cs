﻿using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.Services
{
    [Table("ServiceInvos")]
    public class ServiceInvo : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public int ServiceID { get; set;}

        [ForeignKey("ServiceID")]
        public Service ServiceFk { get; set;}

        public int JobID { get; set;}

        [ForeignKey("JobID")]
        public Job JobFk { get; set;}
        
        public string InvoiceNo { get; set; }
        
        public DateTime? InvoiceDate { get; set; }

        public decimal Amount { get; set; }

        public DateTime? PaidDate { get; set; }

        public string PaidReferenceNo { get; set; }

    }
}
