﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using TheSolarProduct.Leads;
using TheSolarProduct.Jobs;
using TheSolarProduct.ServiceStatuses;
using TheSolarProduct.ServicePrioritys;
using TheSolarProduct.ServiceCategorys;
using TheSolarProduct.ServiceSubCategorys;

namespace TheSolarProduct.Services
{
    [Table("Services")]
    public class Service : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public int OrganizationId { get; set; }
        //public int? serviceid { get; set; }
        public string ServiceSource { get; set; }
        public string State { get; set; }
        public int? ServicePriorityId { get; set; }

        [ForeignKey("ServicePriorityId")]
        public ServicePriority PriorityFk { get; set; }

        public int? ServiceCategoryId { get; set; }

        
        public string ServiceCategoryName { get; set; }
        public int? ServiceStatusId { get; set; }

        [ForeignKey("ServiceStatusId")]
        public ServiceStatus StatusFk { get; set; }

        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string JobNumber { get; set; }
        public string Notes { get; set; }
        public DateTime? ServiceAssignDate { get; set; }
        public virtual int? ServiceAssignId { get; set; }
        public int? InstallerId { get; set; }
        public DateTime? InstallerDate { get; set; }
        public string InstallerNotes { get; set; }
        public string ServiceLine{ get; set; }

        public virtual int? LeadId { get; set; }

        [ForeignKey("LeadId")]
        public Lead LeadFk { get; set; }

        public virtual int? JobId { get; set; }

        [ForeignKey("JobId")]
        public Job JobFk { get; set; }

        public virtual int? IsExternalLead { get; set; }
        public virtual bool? IsDuplicate { get; set; }
        public virtual bool? IsMergeRecord { get; set; }
        public virtual int? IsMergeCount { get; set; }
        public virtual int? ServiceSubCategoryId { get; set; }

        [ForeignKey("ServiceSubCategoryId")]
        public ServiceSubCategory ServiceSubCategoryFk { get; set; }
        public virtual string OtherEmail { get; set; }

        public virtual bool IsWarrantyClaim { get; set; }

        public virtual string CaseNo { get; set; }

        public virtual string CaseNotes { get; set; }

        public virtual string CaseStatus { get; set; }

        public virtual string TrackingNo { get; set; }

        public virtual string TrackingNotes { get; set; }

        public string TicketNo { get; set; }

        public string Pickby { get; set; }

        public DateTime? PickUpDate { get; set; }

        public string PickupMethod { get; set; }

        public string PickupNotes { get; set; }

        public DateTime StockArrivalDate { get; set; }

    }
}
