﻿using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Services
{
    [Table("ServiceDocumentTypes")]
    public class ServiceDocumentType : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual string Title { get; set; }
        public bool IsDocumentRequest { get; set; }
    }
}
