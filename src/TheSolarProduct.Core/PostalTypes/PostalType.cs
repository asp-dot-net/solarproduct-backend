﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.PostalTypes
{
	[Table("PostalTypes")]
    public class PostalType : FullAuditedEntity 
    {

		[Required]
		[StringLength(PostalTypeConsts.MaxNameLength, MinimumLength = PostalTypeConsts.MinNameLength)]
		public virtual string Name { get; set; }
		
		[Required]
		[StringLength(PostalTypeConsts.MaxCodeLength, MinimumLength = PostalTypeConsts.MinCodeLength)]
		public virtual string Code { get; set; }
        public virtual Boolean IsActive { get; set; }

    }
}