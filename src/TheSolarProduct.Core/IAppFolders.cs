﻿namespace TheSolarProduct
{
    public interface IAppFolders
    {
        string SampleProfileImagesFolder { get; }

        string Report { get; }

        string WebLogsFolder { get; set; }
    }
}