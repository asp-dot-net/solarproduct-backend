﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.StreetNames
{
	[Table("StreetNames")]
    public class StreetName : FullAuditedEntity 
    {

		[Required]
		[StringLength(StreetNameConsts.MaxNameLength, MinimumLength = StreetNameConsts.MinNameLength)]
		public virtual string Name { get; set; }
        public virtual Boolean IsActive { get; set; }

    }
}