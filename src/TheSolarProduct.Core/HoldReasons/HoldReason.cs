﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace TheSolarProduct.HoldReasons
{
    [Table("HoldReasons")]
    public class HoldReason : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual string JobHoldReason { get; set; }
        public virtual Boolean IsActive { get; set; }

    }
}