﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.InstallerInvoices
{
    [Table("InstallerInvoice")]
    public class InstallerInvoice
    {
        public int? Typeid { get; set; }
        public int Jobid { get; set; }
        public int InvoiceNo { get; set; }
        public Decimal Invoice_Amount { get; set; }
        public DateTime? Invoice_Date { get; set; }
        public Decimal? AdvanceAmount { get; set; }
        public Decimal? LessDeductAmount { get; set; }
        public Decimal? TotalAmount { get; set; }
        public DateTime Adv_PayDate { get; set; }
        public DateTime PayDate { get; set; }

        public string PaymentType { get; set; }
        public int? InstallerId { get; set; }

        public string Notes { get; set; }
        public string UploadInv_doc { get; set; }
    }
}
