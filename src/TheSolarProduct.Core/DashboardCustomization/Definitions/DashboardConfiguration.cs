using System.Collections.Generic;
using System.Linq;
using Abp.MultiTenancy;
using TheSolarProduct.Authorization;

namespace TheSolarProduct.DashboardCustomization.Definitions
{
	public class DashboardConfiguration
	{
		public List<DashboardDefinition> DashboardDefinitions { get; } = new List<DashboardDefinition>();

		public List<WidgetDefinition> WidgetDefinitions { get; } = new List<WidgetDefinition>();

		public List<WidgetFilterDefinition> WidgetFilterDefinitions { get; } = new List<WidgetFilterDefinition>();

		public DashboardConfiguration()
		{
			#region FilterDefinitions

			// These are global filter which all widgets can use
			var dateRangeFilter = new WidgetFilterDefinition(
				TheSolarProductDashboardCustomizationConsts.Filters.FilterDateRangePicker,
				"FilterDateRangePicker"
			);
			var salesrepuserFilter = new WidgetFilterDefinition(
			  TheSolarProductDashboardCustomizationConsts.Filters.FilterSalesRepUsers,
			  "FilterSalesRepUsers"
		  );



			WidgetFilterDefinitions.Add(dateRangeFilter);
			WidgetFilterDefinitions.Add(salesrepuserFilter);

			// Add your filters here

			#endregion

			#region WidgetDefinitions

			// Define Widgets

			#region TenantWidgets

			var tenantWidgetsDefaultPermission = new List<string>
			{
				AppPermissions.Pages_Tenant_Dashboard
			};

			//var dailySales = new WidgetDefinition(
			//    TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.DailySales,
			//    "WidgetDailySales",
			//    side: MultiTenancySides.Tenant,
			//    usedWidgetFilters: new List<string> { dateRangeFilter.Id },
			//    permissions: tenantWidgetsDefaultPermission
			//);

			//var generalStats = new WidgetDefinition(
			//    TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.GeneralStats,
			//    "WidgetGeneralStats",
			//    side: MultiTenancySides.Tenant,
			//    permissions: tenantWidgetsDefaultPermission.Concat(new List<string>{ AppPermissions.Pages_Administration_AuditLogs }).ToList());

			//var profitShare = new WidgetDefinition(
			//    TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.ProfitShare,
			//    "WidgetProfitShare",
			//    side: MultiTenancySides.Tenant,
			//    permissions: tenantWidgetsDefaultPermission);

			//var memberActivity = new WidgetDefinition(
			//    TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.MemberActivity,
			//    "WidgetMemberActivity",
			//    side: MultiTenancySides.Tenant,
			//    permissions: tenantWidgetsDefaultPermission);

			//var regionalStats = new WidgetDefinition(
			//    TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.RegionalStats,
			//    "WidgetRegionalStats",
			//    side: MultiTenancySides.Tenant,
			//    permissions: tenantWidgetsDefaultPermission);

			//var salesSummary = new WidgetDefinition(
			//    TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.SalesSummary,
			//    "WidgetSalesSummary",
			//    usedWidgetFilters: new List<string>() { dateRangeFilter.Id },
			//    side: MultiTenancySides.Tenant,
			//    permissions: tenantWidgetsDefaultPermission);

			//var topStats = new WidgetDefinition(
			//    TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.TopStats,
			//    "WidgetTopStats",
			//    side: MultiTenancySides.Tenant,
			//    permissions: tenantWidgetsDefaultPermission);

			var userLeads = new WidgetDefinition(
				TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.UserLeads,
				"WidgetUserLeads",
				side: MultiTenancySides.Tenant,
				usedWidgetFilters: new List<string>() { salesrepuserFilter.Id },
				permissions: tenantWidgetsDefaultPermission.Concat(new List<string> { AppPermissions.Pages_Tenant_Dashboard_UserLeads }).ToList());

			var managerLeads = new WidgetDefinition(
			   TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.ManagerLeads,
			   "WidgetManagerLeads",
			   side: MultiTenancySides.Tenant,
			   permissions: tenantWidgetsDefaultPermission.Concat(new List<string> { AppPermissions.Pages_Tenant_Dashboard_ManagerLeads }).ToList());

			var organizationWiseLead = new WidgetDefinition(
			   id: TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.OrganizationWiseLead,
			   name: "OrganizationWiseLead",//localized string key
			   side: MultiTenancySides.Tenant,
			   usedWidgetFilters: new List<string>() { dateRangeFilter.Id },// you can use any filter you need
			   permissions: tenantWidgetsDefaultPermission.Concat(new List<string> { AppPermissions.Pages_Tenant_Dashboard_OrganizationWiseLeads }).ToList());
			var organizationWiseLeaddetails = new WidgetDefinition(
			   id: TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.OrganizationWiseLeaddetails,
			   name: "OrganizationWiseLeaddetails",//localized string key
			   side: MultiTenancySides.Tenant,
			   usedWidgetFilters: new List<string>() { dateRangeFilter.Id },// you can use any filter you need
			   permissions: tenantWidgetsDefaultPermission.Concat(new List<string> { AppPermissions.Pages_Tenant_Dashboard_OrganizationWiseLeadDetails }).ToList());
			var myReminders = new WidgetDefinition(
			   id: TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.MyReminders,
			   name: "MyReminders",//localized string key
			   side: MultiTenancySides.Tenant,
			   usedWidgetFilters: new List<string>() { salesrepuserFilter.Id },// you can use any filter you need
			   permissions: tenantWidgetsDefaultPermission.Concat(new List<string> { AppPermissions.Pages_Tenant_Dashboard_MyReminders }).ToList());
			var organizationWiseTrackerCount = new WidgetDefinition(
			   id: TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.OrganizationWiseTrackerCount,
			   name: "OrganizationWiseTrackerCount",//localized string key
			   side: MultiTenancySides.Tenant,
			   //usedWidgetFilters: new List<string>() { dateRangeFilter.Id },// you can use any filter you need
			   permissions: tenantWidgetsDefaultPermission.Concat(new List<string> { AppPermissions.Pages_Tenant_Dashboard_OrganizationWiseTracker }).ToList());

			var saleDetails = new WidgetDefinition(
			  id: TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.SalesDetails,
			   name: "SalesDetails",
			   side: MultiTenancySides.Tenant,
			   usedWidgetFilters: new List<string>() { salesrepuserFilter.Id },// you can use any filter you need
			   permissions: tenantWidgetsDefaultPermission.Concat(new List<string> { AppPermissions.Pages_Tenant_Dashboard_SalesDetails }).ToList());


			var managersaleDetails = new WidgetDefinition(
		   id: TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.ManagerSalesDetails,
			name: "ManagerSalesDetails",
			side: MultiTenancySides.Tenant,
			//usedWidgetFilters: new List<string>() { salesrepuserFilter.Id },// you can use any filter you need
			permissions: tenantWidgetsDefaultPermission.Concat(new List<string> { AppPermissions.Pages_Tenant_Dashboard_ManagerSalesDetails }).ToList());

			var invoiceStatus = new WidgetDefinition(
			id: TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.InvoiceStatus,
			 name: "InvoiceStatus",
			 side: MultiTenancySides.Tenant,
			 //usedWidgetFilters: new List<string>() { dateRangeFilter.Id },// you can use any filter you need
			 permissions: tenantWidgetsDefaultPermission.Concat(new List<string> { AppPermissions.Pages_Tenant_Dashboard_InvoiceStatus }).ToList());

			var todoDetails = new WidgetDefinition(
			 id: TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.ToDoDetails,
			  name: "ToDoDetails",
			  side: MultiTenancySides.Tenant,
			  usedWidgetFilters: new List<string>() { salesrepuserFilter.Id },// you can use any filter you need
			  permissions: tenantWidgetsDefaultPermission.Concat(new List<string> { AppPermissions.Pages_Tenant_Dashboard_ToDoDetails }).ToList());

			var managerToDoDetails = new WidgetDefinition(
			id: TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.ManagerToDoDetails,
			 name: "ManagerToDoDetails",
			 side: MultiTenancySides.Tenant,
			 //usedWidgetFilters: new List<string>() { dateRangeFilter.Id },// you can use any filter you need
			 permissions: tenantWidgetsDefaultPermission.Concat(new List<string> { AppPermissions.Pages_Tenant_Dashboard_ManagerToDoDetails }).ToList());


			var leadsSalesRank = new WidgetDefinition(
			id: TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.LeadsSalesRank,
			 name: "LeadsSalesRank",
			 side: MultiTenancySides.Tenant,
			 //usedWidgetFilters: new List<string>() { dateRangeFilter.Id },// you can use any filter you need
			 permissions: tenantWidgetsDefaultPermission.Concat(new List<string> { AppPermissions.Pages_Tenant_Dashboard_LeadsSalesRank }).ToList());
			var LeadsDetails = new WidgetDefinition(
			id: TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.LeadsDetails,
			 name: "LeadsDetails",
			 side: MultiTenancySides.Tenant,
			 //usedWidgetFilters: new List<string>() { dateRangeFilter.Id },// you can use any filter you need
			 permissions: tenantWidgetsDefaultPermission.Concat(new List<string> { AppPermissions.Pages_Tenant_Dashboard_LeadsDetails }).ToList());
			var LeadsStatus = new WidgetDefinition(
			id: TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.LeadsStatus,
			 name: "LeadsStatus",
			 side: MultiTenancySides.Tenant,
			 //usedWidgetFilters: new List<string>() { dateRangeFilter.Id },// you can use any filter you need
			 permissions: tenantWidgetsDefaultPermission.Concat(new List<string> { AppPermissions.Pages_Tenant_Dashboard_LeadsStatus }).ToList());

			var LeadsStatusDetail = new WidgetDefinition(
			id: TheSolarProductDashboardCustomizationConsts.Widgets.Tenant.LeadsStatusDetail,
			 name: "LeadsStatusDetail",
			 side: MultiTenancySides.Tenant,
			 //usedWidgetFilters: new List<string>() { dateRangeFilter.Id },// you can use any filter you need
			 permissions: tenantWidgetsDefaultPermission.Concat(new List<string> { AppPermissions.Pages_Tenant_Dashboard_LeadsStatusDetail }).ToList());


			
			//WidgetDefinitions.Add(generalStats);
			//WidgetDefinitions.Add(dailySales);
			//WidgetDefinitions.Add(profitShare);
			//WidgetDefinitions.Add(memberActivity);
			//WidgetDefinitions.Add(regionalStats);
			//WidgetDefinitions.Add(topStats);
			//WidgetDefinitions.Add(salesSummary);
			WidgetDefinitions.Add(userLeads);
			WidgetDefinitions.Add(organizationWiseLead);
			WidgetDefinitions.Add(organizationWiseLeaddetails);
			WidgetDefinitions.Add(myReminders);
			WidgetDefinitions.Add(organizationWiseTrackerCount);
			WidgetDefinitions.Add(saleDetails);
			WidgetDefinitions.Add(invoiceStatus);
			WidgetDefinitions.Add(invoiceStatus);
			WidgetDefinitions.Add(todoDetails);
			WidgetDefinitions.Add(managersaleDetails);
			WidgetDefinitions.Add(managerToDoDetails);
			WidgetDefinitions.Add(managerLeads);
			WidgetDefinitions.Add(leadsSalesRank);
			WidgetDefinitions.Add(LeadsDetails);
			WidgetDefinitions.Add(LeadsStatus);
			WidgetDefinitions.Add(LeadsStatusDetail);

			// Add your tenant side widgets here

			#endregion

			#region HostWidgets

			var hostWidgetsDefaultPermission = new List<string>
			{
				AppPermissions.Pages_Administration_Host_Dashboard
			};

			var incomeStatistics = new WidgetDefinition(
				TheSolarProductDashboardCustomizationConsts.Widgets.Host.IncomeStatistics,
				"WidgetIncomeStatistics",
				side: MultiTenancySides.Host,
				permissions: hostWidgetsDefaultPermission);

			var hostTopStats = new WidgetDefinition(
				TheSolarProductDashboardCustomizationConsts.Widgets.Host.TopStats,
				"WidgetTopStats",
				side: MultiTenancySides.Host,
				permissions: hostWidgetsDefaultPermission);

			var editionStatistics = new WidgetDefinition(
				TheSolarProductDashboardCustomizationConsts.Widgets.Host.EditionStatistics,
				"WidgetEditionStatistics",
				side: MultiTenancySides.Host,
				permissions: hostWidgetsDefaultPermission);

			var subscriptionExpiringTenants = new WidgetDefinition(
				TheSolarProductDashboardCustomizationConsts.Widgets.Host.SubscriptionExpiringTenants,
				"WidgetSubscriptionExpiringTenants",
				side: MultiTenancySides.Host,
				permissions: hostWidgetsDefaultPermission);

			var recentTenants = new WidgetDefinition(
				TheSolarProductDashboardCustomizationConsts.Widgets.Host.RecentTenants,
				"WidgetRecentTenants",
				side: MultiTenancySides.Host,
				usedWidgetFilters: new List<string>() { dateRangeFilter.Id },
				permissions: hostWidgetsDefaultPermission);

			WidgetDefinitions.Add(incomeStatistics);
			WidgetDefinitions.Add(hostTopStats);
			WidgetDefinitions.Add(editionStatistics);
			WidgetDefinitions.Add(subscriptionExpiringTenants);
			WidgetDefinitions.Add(recentTenants);

			// Add your host side widgets here

			#endregion

			#endregion

			#region DashboardDefinitions

			// Create dashboard
			var defaultTenantDashboard = new DashboardDefinition(
				TheSolarProductDashboardCustomizationConsts.DashboardNames.DefaultTenantDashboard,
				new List<string>
				{
                    //generalStats.Id, dailySales.Id, profitShare.Id, memberActivity.Id, regionalStats.Id, topStats.Id, salesSummary.Id, 
                    userLeads.Id,organizationWiseLead.Id,organizationWiseLeaddetails.Id,myReminders.Id,organizationWiseTrackerCount.Id,saleDetails.Id,invoiceStatus.Id,todoDetails.Id, managersaleDetails.Id,managerToDoDetails.Id,managerLeads.Id,leadsSalesRank.Id,LeadsDetails.Id,LeadsStatus.Id,LeadsStatusDetail.Id
				});

			DashboardDefinitions.Add(defaultTenantDashboard);

			var defaultHostDashboard = new DashboardDefinition(
				TheSolarProductDashboardCustomizationConsts.DashboardNames.DefaultHostDashboard,
				new List<string>
				{
					incomeStatistics.Id,
					hostTopStats.Id,
					editionStatistics.Id,
					subscriptionExpiringTenants.Id,
					recentTenants.Id
				});

			DashboardDefinitions.Add(defaultHostDashboard);

			// Add your dashboard definiton here

			#endregion

		}

	}
}
