﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.Jobs;
using TheSolarProduct.Quotations;

namespace TheSolarProduct.DocumentRequests
{
    [Table("DocumentRequests")]
    public class DocumentRequest : FullAuditedEntity
    {
        public int JobId { get; set; }

        [ForeignKey("JobId")]
        public Job JobFk { get; set; }

        public int DocTypeId { get; set; }

        [ForeignKey("DocTypeId")]
        public DocumentType DocTypeFk { get; set; }

        public bool IsSubmitted { get; set; }

    }
}
