﻿using Abp.AspNetCore.Mvc.Views;

namespace TheSolarProduct.Web.Views
{
    public abstract class TheSolarProductRazorPage<TModel> : AbpRazorPage<TModel>
    {
        protected TheSolarProductRazorPage()
        {
            LocalizationSourceName = TheSolarProductConsts.LocalizationSourceName;
        }
    }
}
