﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Authorization;
using TheSolarProduct.Authorization.Users.Profile;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Web.Controllers
{
    [Authorize]
    public class ProfileController : ProfileControllerBase
    {
        public ProfileController(
            ITempFileCacheManager tempFileCacheManager,
            IProfileAppService profileAppService) :
            base(tempFileCacheManager, profileAppService)
        {
        }
    }
}