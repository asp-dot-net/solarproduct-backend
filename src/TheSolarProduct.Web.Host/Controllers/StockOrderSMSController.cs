﻿using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Notifications;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;
using System;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Notifications;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.WholeSaleLeadActivityLogs;
using TheSolarProduct.WholeSaleLeads;
using TheSolarProduct.WholeSalePromotions;
using TheSolarProduct.QuickStocks;
using System.Linq.Dynamic.Core;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.StockOrders;
using System.Linq;
using NPOI.HPSF;


namespace TheSolarProduct.Web.Controllers
{

    public class StockOrderSMSController : TheSolarProductControllerBase
    {
        private readonly IRepository<QuickStockActivityLog> _quickStockActivityLogRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<PurchaseOrder> _purchaseOrderRepository;
        private readonly IRepository<WholeSalePromotionUser> _wholeSalePromotionUsersRepository;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<Tenant> _tenantRepository;

        public StockOrderSMSController(IRepository<QuickStockActivityLog> quickStockActivityLogRepository
            , IRepository<User, long> userRepository
            , IAppNotifier appNotifier
            , IRepository<WholeSalePromotionUser> wholeSalePromotionUsersRepository
            , IRepository<PurchaseOrder> purchaseOrderRepository
            , IApplicationSettingsAppService applicationSettings
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<UserTeam> userTeamRepository
            , IRepository<UserRole, long> userRoleRepository
            , IRepository<Role> roleRepository
            , IRepository<Tenant> tenantRepository
            )
        {
            _quickStockActivityLogRepository = quickStockActivityLogRepository;
            _userRepository = userRepository;
            _appNotifier = appNotifier;
            _purchaseOrderRepository = purchaseOrderRepository;
            _wholeSalePromotionUsersRepository = wholeSalePromotionUsersRepository;
            _applicationSettings = applicationSettings;
            _unitOfWorkManager = unitOfWorkManager;
            _userTeamRepository = userTeamRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _tenantRepository = tenantRepository;
        }
        public async Task<IActionResult> IndexAsync(int tenantid, int actionId, int sectionid = 0, int promoresid = 0)
        {

            string smsReply = Request.Form["Message.Text"];
            string smsID = Request.Form["Message.Sid"];

            //string smsReply = "yes";
            //string smsID = "123";

            using (CurrentUnitOfWork.SetTenantId(tenantid))
            {
                var Activity = _quickStockActivityLogRepository.GetAll().Where(e => e.Id == actionId).FirstOrDefault();
                var PurchaseOrder = _purchaseOrderRepository.GetAll().Where(e => e.Id == Activity.PurchaseOrderId).FirstOrDefault();
                var PurchaseOrderno = PurchaseOrder.Id;
                var strnotify = "";
                if (PurchaseOrderno != null)
                {
                    strnotify = " For " + PurchaseOrderno + " Job";
                }


                int actID = 20;
                var promouserid = 0;
                if (promoresid > 0)
                {
                    int resid = 3;
                    actID = 22;
                    if (smsReply.ToLower().Trim() == "yes")
                    {
                        resid = 1;

                        SendSMSInput sendSMSInput = new SendSMSInput();
                        sendSMSInput.PhoneNumber = PurchaseOrder.VendorFK.Phone;
                        sendSMSInput.Text = "Thanks for the reply. We will get back to you soon.";
                        await _applicationSettings.SendStockOrderSMS(sendSMSInput);
                    }
                    else if (smsReply.ToLower().Trim() == "stop")
                    {
                        resid = 2;

                        SendSMSInput sendSMSInput = new SendSMSInput();
                        sendSMSInput.PhoneNumber = PurchaseOrder.VendorFK.Phone;
                        sendSMSInput.Text = "You have been successfully unsubscribed.";
                        await _applicationSettings.SendStockOrderSMS(sendSMSInput);
                    }
                    var resdate = DateTime.UtcNow;
                  
                }
                using (var uow = _unitOfWorkManager.Begin(TransactionScopeOption.RequiresNew))
                {
                    {
                        var activity = new QuickStockActivityLog();
                        activity.ActionNote = "Reply :" + smsReply;
                        activity.Body = smsReply;
                        activity.TenantId = tenantid;
                        activity.ActionId = actID;
                        activity.CreatorUserId = Activity.CreatorUserId;
                        if (sectionid != null)
                        {
                            activity.SectionId = sectionid;
                        }
                        else { activity.SectionId = 0; }

                        activity.PurchaseOrderId = Activity.PurchaseOrderId;
                        activity.MessageId = smsID;
                        activity.ReferanceId = actionId;
                        //activity.IsMark = false;
                        await _quickStockActivityLogRepository.InsertAsync(activity);

                    }


                    await uow.CompleteAsync();
                }

                
                    if (Activity.CreatorUserId != null)
                    {
                        var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == Activity.CreatorUserId).FirstOrDefault();
                        string msg = string.Format("SMS Reply Received" + strnotify);
                        await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
                    }
                

            }
            return View();
        }
    }
}