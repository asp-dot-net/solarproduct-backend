﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using System.Transactions;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.IO.Extensions;
using Abp.Notifications;
using Abp.UI;
using Abp.Web.Models;
using Microsoft.AspNetCore.Mvc;
using NPOI.HPSF;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.DemoUiComponents.Dto;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Leads;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.MyInstallerActivityLogs;
using TheSolarProduct.Notifications;
using TheSolarProduct.Promotions;
using TheSolarProduct.Storage;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.WholeSaleLeadActivityLogs;
using TheSolarProduct.WholeSaleLeads;
using TheSolarProduct.WholeSalePromotions;

namespace TheSolarProduct.Web.Controllers
{
    public class WholeSaleSMSController : TheSolarProductControllerBase
    {
        protected readonly IRepository<WholeSaleLeadActivityLog> _wholeSaleLeadactivityRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<WholeSaleLead> _wholeSaleLeadRepository;
        private readonly IRepository<WholeSalePromotionUser> _wholeSalePromotionUsersRepository;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<Tenant> _tenantRepository;

        public WholeSaleSMSController(IRepository<WholeSaleLeadActivityLog> wholeSaleLeadactivityRepository
            , IRepository<User, long> userRepository
            , IAppNotifier appNotifier
            , IRepository<WholeSalePromotionUser> wholeSalePromotionUsersRepository
            , IRepository<WholeSaleLead> wholeSaleLeadRepository
            , IApplicationSettingsAppService applicationSettings
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<UserTeam> userTeamRepository
            , IRepository<UserRole, long> userRoleRepository
            , IRepository<Role> roleRepository
            , IRepository<Tenant> tenantRepository
            )
        {
            _wholeSaleLeadactivityRepository = wholeSaleLeadactivityRepository;
            _userRepository = userRepository;
            _appNotifier = appNotifier;
            _wholeSaleLeadRepository = wholeSaleLeadRepository;
            _wholeSalePromotionUsersRepository = wholeSalePromotionUsersRepository;
            _applicationSettings = applicationSettings;
            _unitOfWorkManager = unitOfWorkManager;
            _userTeamRepository = userTeamRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _tenantRepository = tenantRepository;
        }
        public async Task<IActionResult> IndexAsync(int tenantid, int actionId, int sectionid = 0, int promoresid = 0)
        {

            string smsReply = Request.Form["Message.Text"];
            string smsID = Request.Form["Message.Sid"];

            //string smsReply = "yes";
            //string smsID = "123";

            using (CurrentUnitOfWork.SetTenantId(tenantid))
            {
                var Activity = _wholeSaleLeadactivityRepository.GetAll().Where(e => e.Id == actionId).FirstOrDefault();
                var Lead = _wholeSaleLeadRepository.GetAll().Where(e => e.Id == Activity.WholeSaleLeadId).FirstOrDefault();
                int actID = 20;
                var mobile = Lead.Mobile;
                var promouserid = 0;
                if (promoresid > 0)
                {
                    int resid = 3;
                    actID = 51;
                    var sendBulkSMSInput = new SendBulkSMSInput();
                    List<SendSMSInput> msg = new List<SendSMSInput>();

                    if (smsReply.ToLower().Trim() == "yes")
                    {
                        resid = 1;

                        SendSMSInput sendSMSInput = new SendSMSInput();
                        sendSMSInput.PhoneNumber = mobile;
                        sendSMSInput.Text = "Thanks for the reply. We will get back to you soon.";
                        sendSMSInput.ActivityId = actionId;

                        msg.Add(sendSMSInput);
                        sendBulkSMSInput.Messages = msg;

                        await _applicationSettings.SendWholeSaleBulkSMS(sendBulkSMSInput);
                    }
                    else if (smsReply.ToLower().Trim() == "stop")
                    {
                        resid = 2;

                        SendSMSInput sendSMSInput = new SendSMSInput();
                        sendSMSInput.PhoneNumber = mobile;
                        sendSMSInput.Text = "You have been successfully unsubscribed.";
                        sendSMSInput.ActivityId = actionId;

                        msg.Add(sendSMSInput);
                        sendBulkSMSInput.Messages = msg;

                        await _applicationSettings.SendWholeSaleBulkSMS(sendBulkSMSInput);
                    }
                    var resdate = DateTime.UtcNow;
                    try
                    {
                        using (var uow = _unitOfWorkManager.Begin(TransactionScopeOption.RequiresNew))
                        {
                            var promoresponse = _wholeSalePromotionUsersRepository.GetAll().Where(e => e.Id == promoresid).FirstOrDefault();
                            promoresponse.ResponseDate = resdate;
                            promoresponse.ResponseMessage = smsReply;
                            promoresponse.WholeSalePromotionResponseStatusId = resid;
                            await _wholeSalePromotionUsersRepository.UpdateAsync(promoresponse);

                            await uow.CompleteAsync();
                        }
                    }
                    catch
                    {

                    }
                }
                using (var uow = _unitOfWorkManager.Begin(TransactionScopeOption.RequiresNew))
                {
                    var activity = new WholeSaleLeadActivityLog();
                    activity.ActionNote = "Reply :" + smsReply;
                    activity.Body = smsReply;
                    activity.TenantId = tenantid;
                    activity.ActionId = actID;
                    activity.CreatorUserId = Activity.CreatorUserId;
                    if (sectionid != null)
                    {
                        activity.SectionId = sectionid;
                    }
                    else { activity.SectionId = 0; }

                    activity.WholeSaleLeadId = Activity.WholeSaleLeadId;
                    activity.MessageId = smsID;
                    activity.ReferanceId = actionId;
                    activity.IsMark = false;
                    await _wholeSaleLeadactivityRepository.InsertAsync(activity);
                    

                    await uow.CompleteAsync();
                }

                if (actID == 51)
                {
                    if (Lead.AssignUserId != null)
                    {
                       var assId = Lead.AssignUserId;
                        string msg = string.Format("WholeSale Promotion Reply Received");

                        var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == assId).FirstOrDefault();
                        await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);



                        //var roleids = _roleRepository.GetAll().Where(e => e.Name == "Admin").Select(e => e.Id).ToList();
                        //var userids = _userRoleRepository.GetAll().Where(e => roleids.Contains(e.RoleId)).Select(e => e.UserId).ToList();

                        //foreach (var id in userids)
                        //{
                        //	var NotifyToUsertoadmin = _userRepository.GetAll().Where(u => u.Id == id).FirstOrDefault();
                        //	await _appNotifier.NewLead(NotifyToUsertoadmin, msg, NotificationSeverity.Info);
                        //}

                        //var roleids = _roleRepository.GetAll().Where(e => e.Name == "Sales Manager").Select(e => e.Id).ToList();
                        //var uRole = _userRoleRepository.GetAll().Where(e => roleids.Contains(e.RoleId)).FirstOrDefault();

                        //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == Lead.AssignUserId).Select(e => e.TeamId).ToList();
                        //var ManagerUserid = _userTeamRepository.GetAll().Include(e => e.UserFk).Where(e => TeamId.Contains(e.TeamId) && e.UserId == uRole.UserId).Select(e => e.UserId).Distinct().ToList();
                        //foreach (var id in ManagerUserid)
                        //{
                        //    var NotifyToManager = _userRepository.GetAll().Where(u => u.Id == id).FirstOrDefault();
                        //    await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
                        //}

                    }
                }
                else
                {
                    if (Activity.CreatorUserId != null)
                    {
                        var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == Activity.CreatorUserId).FirstOrDefault();
                        string msg = string.Format("WholeSale SMS Reply Received");
                        await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
                    }
                }

            }
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> UploadFiles()
        {
            try
            {
                var files = Request.Form.Files;

                //Check input
                if (files == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }

                UploadFileOutput filesOutput = new UploadFileOutput();

                foreach (var file in files)
                {
                    if (file.Length > 1048576) //1MB
                    {
                        throw new UserFriendlyException(L("File_SizeLimit_Error"));
                    }

                    byte[] fileBytes;
                    using (var stream = file.OpenReadStream())
                    {
                        fileBytes = stream.GetAllBytes();
                    }
                    var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                    var filename = DateTime.Now.Ticks + "_" + file.FileName.Replace(" ", "");
                    var Path = ApplicationSettingConsts.DocumentPath;
                    //var FinalFilePath = Path + TenantName + "\\CreditApplication\\LicencePassportDoc\\" + filename;

                    var MainFolder = ApplicationSettingConsts.DocumentPath; //From Const
                    MainFolder = MainFolder + "\\Documents";
                    var TenantPath = MainFolder + "\\" + TenantName;
                    var CreditApplicationPath = TenantPath + "\\" + "CreditApplication";
                    var FolderPath = CreditApplicationPath + "\\" + "LicencePassportDoc";
                    var FinalFilePath = FolderPath + "\\" + filename;

                    if (System.IO.Directory.Exists(MainFolder))
                    {
                        if (System.IO.Directory.Exists(TenantPath))
                        {
                            if (System.IO.Directory.Exists(CreditApplicationPath))
                            {
                                if (System.IO.Directory.Exists(FolderPath))
                                {
                                    using (MemoryStream mStream = new MemoryStream(fileBytes))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(FolderPath);
                                    using (MemoryStream mStream = new MemoryStream(fileBytes))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(CreditApplicationPath);
                                if (System.IO.Directory.Exists(FolderPath))
                                {
                                    using (MemoryStream mStream = new MemoryStream(fileBytes))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(FolderPath);
                                    using (MemoryStream mStream = new MemoryStream(fileBytes))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                    }
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(TenantPath);
                            if (System.IO.Directory.Exists(CreditApplicationPath))
                            {
                                if (System.IO.Directory.Exists(FolderPath))
                                {
                                    using (MemoryStream mStream = new MemoryStream(fileBytes))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(FolderPath);
                                    using (MemoryStream mStream = new MemoryStream(fileBytes))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(CreditApplicationPath);
                                if (System.IO.Directory.Exists(FolderPath))
                                {
                                    using (MemoryStream mStream = new MemoryStream(fileBytes))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(FolderPath);
                                    using (MemoryStream mStream = new MemoryStream(fileBytes))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(MainFolder);
                        if (System.IO.Directory.Exists(TenantPath))
                        {
                            if (System.IO.Directory.Exists(CreditApplicationPath))
                            {
                                if (System.IO.Directory.Exists(FolderPath))
                                {
                                    using (MemoryStream mStream = new MemoryStream(fileBytes))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(FolderPath);
                                    using (MemoryStream mStream = new MemoryStream(fileBytes))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(CreditApplicationPath);
                                if (System.IO.Directory.Exists(FolderPath))
                                {
                                    using (MemoryStream mStream = new MemoryStream(fileBytes))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(FolderPath);
                                    using (MemoryStream mStream = new MemoryStream(fileBytes))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                    }
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(TenantPath);
                            if (System.IO.Directory.Exists(CreditApplicationPath))
                            {
                                if (System.IO.Directory.Exists(FolderPath))
                                {
                                    using (MemoryStream mStream = new MemoryStream(fileBytes))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(FolderPath);
                                    using (MemoryStream mStream = new MemoryStream(fileBytes))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(CreditApplicationPath);
                                if (System.IO.Directory.Exists(FolderPath))
                                {
                                    using (MemoryStream mStream = new MemoryStream(fileBytes))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(FolderPath);
                                    using (MemoryStream mStream = new MemoryStream(fileBytes))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                    }
                                }
                            }
                        }
                    }

                    //using (MemoryStream mStream = new MemoryStream(fileBytes))
                    //{
                    //    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                    //}

                    var fileObject = new BinaryObject(AbpSession.TenantId, fileBytes);
                    //await _binaryObjectManager.SaveAsync(fileObject);

                    filesOutput = (new UploadFileOutput
                    {
                        Id = fileObject.Id,
                        FileName = file.FileName,
                        FilePath = "\\Documents\\" + TenantName + "\\" + "CreditApplication" + "\\LicencePassportDoc" + "\\" + filename
                    });
                }

                return Json(new AjaxResponse(filesOutput));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }


    }
}
