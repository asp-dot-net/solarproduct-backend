﻿using Abp.AspNetCore.Mvc.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.Storage;
using Abp.BackgroundJobs;
using Microsoft.AspNetCore.Authorization;

namespace TheSolarProduct.Web.Controllers
{
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_Users)]
    //[AllowAnonymous]
    public class UsersController : UsersControllerBase
    {
        public UsersController(IBinaryObjectManager binaryObjectManager, IBackgroundJobManager backgroundJobManager)
            : base(binaryObjectManager, backgroundJobManager)
        {
        }
    }
}