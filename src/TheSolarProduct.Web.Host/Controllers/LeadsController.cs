﻿using Abp.AspNetCore.Mvc.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.Storage;
using Abp.BackgroundJobs;

namespace TheSolarProduct.Web.Controllers
{
    [AbpMvcAuthorize(AppPermissions.Pages_Leads)]
    public class LeadsController : LeadsControllerBase
    {
        public LeadsController(IBinaryObjectManager binaryObjectManager, IBackgroundJobManager backgroundJobManager)
            : base(binaryObjectManager, backgroundJobManager)
        {
        }
    }
}