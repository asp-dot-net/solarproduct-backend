﻿using Microsoft.AspNetCore.Antiforgery;

namespace TheSolarProduct.Web.Controllers
{
    public class AntiForgeryController : TheSolarProductControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
