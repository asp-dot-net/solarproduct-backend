﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using System.Transactions;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Notifications;
using Microsoft.AspNetCore.Mvc;
using NPOI.HPSF;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Jobs;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Leads;
using TheSolarProduct.MyInstallerActivityLogs;
using TheSolarProduct.Notifications;
using TheSolarProduct.Promotions;
using TheSolarProduct.TheSolarDemo;

namespace TheSolarProduct.Web.Controllers
{
	public class SMSController : TheSolarProductControllerBase
	{
		protected readonly IRepository<LeadActivityLog> _leadactivityRepository;
		private readonly IRepository<User, long> _userRepository;
		private readonly IAppNotifier _appNotifier;
		private readonly IRepository<Lead> _leadRepository;
		private readonly IRepository<PromotionUser> _PromotionUsersRepository;
		private readonly IApplicationSettingsAppService _applicationSettings;
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private readonly IRepository<UserTeam> _userTeamRepository;
		private readonly IRepository<UserRole, long> _userRoleRepository;
		private readonly IRepository<Role> _roleRepository;
		private readonly IRepository<MyInstallerActivityLog> _myinstalleractivityRepository;
		private readonly IRepository<Job> _jobRepository;

		public SMSController(IRepository<LeadActivityLog> leadactivityRepository
			, IRepository<User, long> userRepository
			, IAppNotifier appNotifier
			, IRepository<PromotionUser> PromotionUsersRepository
			, IRepository<Lead> leadRepository
			, IApplicationSettingsAppService applicationSettings
			, IUnitOfWorkManager unitOfWorkManager,
			IRepository<UserTeam> userTeamRepository,
			IRepository<UserRole, long> userRoleRepository,
			IRepository<Role> roleRepository,
			IRepository<MyInstallerActivityLog> myinstalleractivityRepository
            , IRepository<Job> jobRepository)
		{
			_leadactivityRepository = leadactivityRepository;
			_userRepository = userRepository;
			_appNotifier = appNotifier;
			_leadRepository = leadRepository;
			_PromotionUsersRepository = PromotionUsersRepository;
			_applicationSettings = applicationSettings;
			_unitOfWorkManager = unitOfWorkManager;
			_userTeamRepository = userTeamRepository;
			_userRoleRepository = userRoleRepository;
			_roleRepository = roleRepository;
			_myinstalleractivityRepository = myinstalleractivityRepository;
			_jobRepository = jobRepository;
		}
		public async Task<IActionResult> IndexAsync(int tenantid, int actionId, int sectionid, int promoresid = 0, bool isMyInstallerUser = false)
		{
			string smsReply = Request.Form["Message.Text"];
			string smsID = Request.Form["Message.Sid"];

			//string smsReply = "Test";
			//string smsID = "123";

			using (CurrentUnitOfWork.SetTenantId(tenantid))
			{
				var Activity = _leadactivityRepository.GetAll().Where(e => e.Id == actionId).FirstOrDefault();
				var Lead = _leadRepository.GetAll().Where(e => e.Id == Activity.LeadId).FirstOrDefault();
				var jobnumber = _jobRepository.GetAll().Where(e => e.LeadId == Lead.Id).Select(e => e.JobNumber).FirstOrDefault();
				var strnotify = "";
				if (jobnumber != null)
				{
					strnotify = " For " + jobnumber + " Job";
				}
				else
				{
                    strnotify = " For " + Lead.CompanyName;
                }

				int actID = 20;
				var promouserid = 0;
				if (promoresid > 0)
				{
					int resid = 3;
					actID = 22;
					if (smsReply.ToLower().Trim() == "yes")
					{
						resid = 1;

						SendSMSInput sendSMSInput = new SendSMSInput();
						sendSMSInput.PhoneNumber = Lead.Mobile;
						sendSMSInput.Text = "Thanks for the reply. We will get back to you soon.";
						await _applicationSettings.SendSMS(sendSMSInput);
					}
					else if (smsReply.ToLower().Trim() == "stop")
					{
						resid = 2;

						SendSMSInput sendSMSInput = new SendSMSInput();
						sendSMSInput.PhoneNumber = Lead.Mobile;
						sendSMSInput.Text = "You have been successfully unsubscribed.";
						await _applicationSettings.SendSMS(sendSMSInput);
					}
					var resdate = DateTime.UtcNow;
					try
					{
						using (var uow = _unitOfWorkManager.Begin(TransactionScopeOption.RequiresNew))
						{
							var promoresponse = _PromotionUsersRepository.GetAll().Where(e => e.Id == promoresid).FirstOrDefault();
							promoresponse.ResponseDate = resdate;
							promoresponse.ResponseMessage = smsReply;
							promoresponse.PromotionResponseStatusId = resid;
							await _PromotionUsersRepository.UpdateAsync(promoresponse);

							await uow.CompleteAsync();
						}
					}
					catch
					{

					}
				}
				using (var uow = _unitOfWorkManager.Begin(TransactionScopeOption.RequiresNew))
				{
					if (isMyInstallerUser == true)
					{
						var myActivity = _myinstalleractivityRepository.GetAll().Where(e => e.Id == actionId).FirstOrDefault();
						MyInstallerActivityLog myinsalleractivity = new MyInstallerActivityLog();
						myinsalleractivity.ActionNote = "Reply :" + smsReply;
						myinsalleractivity.Body = smsReply;
						myinsalleractivity.TenantId = tenantid;
						myinsalleractivity.ActionId = actID;
						myinsalleractivity.CreatorUserId = myActivity.CreatorUserId;
						myinsalleractivity.MyInstallerId = myActivity.MyInstallerId;
						myinsalleractivity.MessageId = smsID;
						myinsalleractivity.ReferanceId = actionId;
						myinsalleractivity.IsMark = false;

						await _myinstalleractivityRepository.InsertAsync(myinsalleractivity);
					}
					else
					{
						var activity = new LeadActivityLog();
						activity.ActionNote = "Reply :" + smsReply;
						activity.Body = smsReply;
						activity.TenantId = tenantid;
						activity.ActionId = actID;
						activity.CreatorUserId = Activity.CreatorUserId;
						if (sectionid != null)
						{
							activity.SectionId = sectionid;
						}
						else { activity.SectionId = 0; }

						activity.LeadId = Activity.LeadId;
						activity.MessageId = smsID;
						activity.ReferanceId = actionId;
						activity.IsMark = false;
						await _leadactivityRepository.InsertAsync(activity);

					}


					await uow.CompleteAsync();
				}

				if (actID == 22)
				{
					if (Lead.AssignToUserID != null)
					{
						string msg = string.Format("Promotion Reply Received");

						var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == Lead.AssignToUserID).FirstOrDefault();
						await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);

						var roleids = _roleRepository.GetAll().Where(e => e.Name == "Sales Manager").Select(e => e.Id).ToList();
						var uRole = _userRoleRepository.GetAll().Where(e => roleids.Contains(e.RoleId)).FirstOrDefault();

						var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == Lead.AssignToUserID).Select(e => e.TeamId).ToList();
						var ManagerUserid = _userTeamRepository.GetAll().Include(e => e.UserFk).Where(e => TeamId.Contains(e.TeamId) && e.UserId == uRole.UserId).Select(e => e.UserId).Distinct().ToList();
						foreach (var id in ManagerUserid)
						{
							var NotifyToManager = _userRepository.GetAll().Where(u => u.Id == id).FirstOrDefault();
							await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
						}
					}
				}
				else
				{
					if (Activity.CreatorUserId != null)
					{
						var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == Activity.CreatorUserId).FirstOrDefault();
						string msg = string.Format("SMS Reply Received" + strnotify);
						await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
					}
				}

			}
			return View();
		}
	}
}