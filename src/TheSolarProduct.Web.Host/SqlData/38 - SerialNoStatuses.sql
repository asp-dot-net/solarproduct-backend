﻿--SET IDENTITY_INSERT SerialNoStatuses ON

--Insert Into SerialNoStatuses(Id, CreationTime, CreatorUserId, IsDeleted, Status, IsActive)
--Values (1, '2025-01-17 00:00:00.00', 7, 0, 'Upload From Excal', 1)
--Values (2, '2025-01-17 00:00:00.00', 7, 0, 'Stock In', 1)
--Values (3, '2025-01-17 00:00:00.00', 7, 0, 'Ready To Transfer', 1)
--Values (4, '2025-01-17 00:00:00.00', 7, 0, 'Transfer Out', 1)
--Values (5, '2025-01-17 00:00:00.00', 7, 0, 'Transfer Revert', 1)
--Values (6, '2025-01-17 00:00:00.00', 7, 0, 'Transfer In', 1)
--Values (7, '2025-01-17 00:00:00.00', 7, 0, 'Broken', 1)
--Values (8, '2025-01-17 00:00:00.00', 7, 0, 'Faulty', 1)

--SET IDENTITY_INSERT SerialNoStatuses OFF