SET IDENTITY_INSERT [dbo].[STCPostalCodes] ON 
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (1, 0, 799, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (2, 800, 869, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (3, 870, 879, 1)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (4, 880, 1000, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (5, 1001, 2356, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (6, 2357, 2357, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (7, 2358, 2384, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (8, 2385, 2393, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (9, 2394, 2395, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (10, 2396, 2398, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (11, 2399, 2399, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (12, 2400, 2400, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (13, 2401, 2404, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (14, 2405, 2407, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (15, 2408, 2410, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (16, 2411, 2414, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (17, 2415, 2536, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (18, 2537, 2537, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (19, 2538, 2544, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (20, 2545, 2557, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (21, 2558, 2626, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (22, 2627, 2629, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (23, 2630, 2630, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (24, 2631, 2639, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (25, 2640, 2820, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (26, 2821, 2842, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (27, 2843, 2872, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (28, 2873, 2874, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (29, 2875, 2876, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (30, 2877, 2889, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (31, 2890, 2897, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (32, 2898, 2899, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (33, 2900, 2999, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (34, 3000, 3390, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (35, 3391, 3398, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (36, 3399, 3413, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (37, 3414, 3426, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (38, 3427, 3474, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (39, 3475, 3514, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (40, 3515, 3516, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (41, 3517, 3520, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (42, 3521, 3524, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (43, 3525, 3538, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (44, 3539, 3539, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (45, 3540, 3549, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (46, 3550, 3560, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (47, 3561, 3569, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (48, 3570, 3570, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (49, 3571, 3606, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (50, 3607, 3617, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (51, 3618, 3622, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (52, 3623, 3628, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (53, 3629, 3657, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (54, 3658, 3684, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (55, 3685, 3687, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (56, 3688, 3724, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (57, 3725, 3731, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (58, 3732, 3999, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (59, 4000, 4416, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (60, 4417, 4417, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (61, 4418, 4422, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (62, 4423, 4423, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (63, 4424, 4426, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (64, 4427, 4473, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (65, 4474, 4476, 1)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (66, 4477, 4478, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (67, 4479, 4485, 1)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (68, 4486, 4490, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (69, 4491, 4492, 1)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (70, 4493, 4499, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (71, 4500, 4721, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (72, 4722, 4722, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (73, 4723, 4723, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (74, 4724, 4735, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (75, 4736, 4736, 1)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (76, 4737, 4824, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (77, 4825, 4827, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (78, 4828, 4828, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (79, 4829, 4829, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (80, 4830, 5261, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (81, 5262, 5263, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (82, 5264, 5270, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (83, 5271, 5300, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (84, 5301, 5429, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (85, 5430, 5450, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (86, 5451, 5653, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (87, 5654, 5669, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (88, 5670, 5679, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (89, 5680, 5699, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (90, 5700, 5709, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (91, 5710, 5722, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (92, 5723, 5724, 1)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (93, 5725, 5733, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (94, 5734, 5799, 1)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (95, 5800, 6243, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (96, 6244, 6250, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (97, 6251, 6254, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (98, 6255, 6270, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (99, 6271, 6315, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (100, 6316, 6357, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (101, 6358, 6393, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (102, 6394, 6400, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (103, 6401, 6430, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (104, 6431, 6431, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (105, 6432, 6433, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (106, 6434, 6439, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (107, 6440, 6441, 1)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (108, 6442, 6444, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (109, 6445, 6459, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (110, 6460, 6467, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (111, 6468, 6469, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (112, 6470, 6471, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (113, 6472, 6474, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (114, 6475, 6506, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (115, 6507, 6555, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (116, 6556, 6573, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (117, 6574, 6602, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (118, 6603, 6607, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (119, 6608, 6641, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (120, 6642, 6724, 1)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (121, 6725, 6750, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (122, 6751, 6797, 1)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (123, 6798, 6799, 2)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (124, 6800, 6999, 3)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (125, 7000, 8999, 4)
GO
INSERT [dbo].[STCPostalCodes] ([Id], [PostCodeFrom], [PostCodeTo], [ZoneId]) VALUES (126, 9000, 9999, 3)
GO
SET IDENTITY_INSERT [dbo].[STCPostalCodes] OFF