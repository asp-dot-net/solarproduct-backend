SET IDENTITY_INSERT [dbo].[JobStatuses] ON 

INSERT [dbo].[JobStatuses]([Id],[Name])VALUES(1,'Planned')
INSERT [dbo].[JobStatuses]([Id],[Name])VALUES(2,'On Hold')
INSERT [dbo].[JobStatuses]([Id],[Name])VALUES(3,'Cancel')
INSERT [dbo].[JobStatuses]([Id],[Name])VALUES(4,'Deposite Received')
INSERT [dbo].[JobStatuses]([Id],[Name])VALUES(5,'Active')
INSERT [dbo].[JobStatuses]([Id],[Name])VALUES(6,'Job Book')
INSERT [dbo].[JobStatuses]([Id],[Name])VALUES(7,'Job Incomplete')
INSERT [dbo].[JobStatuses]([Id],[Name])VALUES(8,'Job Installed')

SET IDENTITY_INSERT [dbo].[JobStatuses] OFF