SET IDENTITY_INSERT [dbo].[ProductTypes] ON 

INSERT [dbo].[ProductTypes]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Name])VALUES(1,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Module')
INSERT [dbo].[ProductTypes]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Name])VALUES(2,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Inverters')
INSERT [dbo].[ProductTypes]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Name])VALUES(3,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Mounting')
INSERT [dbo].[ProductTypes]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Name])VALUES(4,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Electrical')
INSERT [dbo].[ProductTypes]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Name])VALUES(5,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Battery')
INSERT [dbo].[ProductTypes]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Name])VALUES(6,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Others')

SET IDENTITY_INSERT [dbo].[ProductTypes] OFF