SET IDENTITY_INSERT [dbo].[SolarRebateStatus] ON 

INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(1,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Blank')
INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(2,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'After Install')
INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(3,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Cancelled')
INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(4,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Installation Completed')

INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(5,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Installation Scheduled')

INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(6,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Payment Approved')

INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(7,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Payment Claim Under Review')

INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(8,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Payment Processed')

INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(9,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Quote Submitted')

INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(10,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Appl Rejected')

INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(11,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Additional Info Required')
INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(12,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'App Expired')
INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(13,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Draft quote')

INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(14,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Quote expired')

INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(15,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Appl Started')
INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(16,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Appl under review')
INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(17,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Appl approved')
INSERT [dbo].[SolarRebateStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(18,N'2021-01-17 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Quote expired')
SET IDENTITY_INSERT [dbo].[SolarRebateStatus] OFF