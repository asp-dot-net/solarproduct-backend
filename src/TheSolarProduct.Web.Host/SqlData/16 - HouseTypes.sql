SET IDENTITY_INSERT [dbo].[HouseTypes] ON 

INSERT [dbo].[HouseTypes]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Name],[DisplayOrder])VALUES(1,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Single Story',1)
INSERT [dbo].[HouseTypes]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Name],[DisplayOrder])VALUES(2,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Double Story',2)
INSERT [dbo].[HouseTypes]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Name],[DisplayOrder])VALUES(3,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Multi Story',3)
INSERT [dbo].[HouseTypes]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Name],[DisplayOrder])VALUES(4,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Shed',4)

SET IDENTITY_INSERT [dbo].[HouseTypes] OFF