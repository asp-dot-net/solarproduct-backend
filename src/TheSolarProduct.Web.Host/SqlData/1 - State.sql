SET IDENTITY_INSERT [dbo].[State] ON 

INSERT [dbo].[State] ([Id], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [IsDeleted], [DeleterUserId], [DeletionTime], [Name]) VALUES (1, CAST(N'2020-06-16T17:35:24.8627817' AS DateTime2), NULL, NULL, NULL, 0, NULL, NULL, N'ACT')
INSERT [dbo].[State] ([Id], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [IsDeleted], [DeleterUserId], [DeletionTime], [Name]) VALUES (2, CAST(N'2020-06-16T17:35:24.9880316' AS DateTime2), NULL, NULL, NULL, 0, NULL, NULL, N'NSW')
INSERT [dbo].[State] ([Id], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [IsDeleted], [DeleterUserId], [DeletionTime], [Name]) VALUES (3, CAST(N'2020-06-16T17:35:25.0661342' AS DateTime2), NULL, NULL, NULL, 0, NULL, NULL, N'NT')
INSERT [dbo].[State] ([Id], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [IsDeleted], [DeleterUserId], [DeletionTime], [Name]) VALUES (4, CAST(N'2020-06-16T17:35:25.1479971' AS DateTime2), NULL, NULL, NULL, 0, NULL, NULL, N'QLD')
INSERT [dbo].[State] ([Id], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [IsDeleted], [DeleterUserId], [DeletionTime], [Name]) VALUES (5, CAST(N'2020-06-16T17:35:25.2529416' AS DateTime2), NULL, NULL, NULL, 0, NULL, NULL, N'SA')
INSERT [dbo].[State] ([Id], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [IsDeleted], [DeleterUserId], [DeletionTime], [Name]) VALUES (6, CAST(N'2020-06-16T17:35:25.3406386' AS DateTime2), NULL, NULL, NULL, 0, NULL, NULL, N'TAS')
INSERT [dbo].[State] ([Id], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [IsDeleted], [DeleterUserId], [DeletionTime], [Name]) VALUES (7, CAST(N'2020-06-16T17:35:25.4279037' AS DateTime2), NULL, NULL, NULL, 0, NULL, NULL, N'VIC')
INSERT [dbo].[State] ([Id], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [IsDeleted], [DeleterUserId], [DeletionTime], [Name]) VALUES (8, CAST(N'2020-06-16T17:35:25.5060755' AS DateTime2), NULL, NULL, NULL, 0, NULL, NULL, N'WA')
SET IDENTITY_INSERT [dbo].[State] OFF
