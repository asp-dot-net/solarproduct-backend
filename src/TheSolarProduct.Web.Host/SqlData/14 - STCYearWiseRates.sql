SET IDENTITY_INSERT [dbo].[STCYearWiseRates] ON 
GO
INSERT [dbo].[STCYearWiseRates] ([Id], [Year], [Rate]) VALUES (1, 2030, 1)
GO
INSERT [dbo].[STCYearWiseRates] ([Id], [Year], [Rate]) VALUES (2, 2029, 2)
GO
INSERT [dbo].[STCYearWiseRates] ([Id], [Year], [Rate]) VALUES (3, 2028, 3)
GO
INSERT [dbo].[STCYearWiseRates] ([Id], [Year], [Rate]) VALUES (4, 2027, 4)
GO
INSERT [dbo].[STCYearWiseRates] ([Id], [Year], [Rate]) VALUES (5, 2026, 5)
GO
INSERT [dbo].[STCYearWiseRates] ([Id], [Year], [Rate]) VALUES (6, 2025, 6)
GO
INSERT [dbo].[STCYearWiseRates] ([Id], [Year], [Rate]) VALUES (7, 2024, 7)
GO
INSERT [dbo].[STCYearWiseRates] ([Id], [Year], [Rate]) VALUES (8, 2023, 8)
GO
INSERT [dbo].[STCYearWiseRates] ([Id], [Year], [Rate]) VALUES (9, 2022, 9)
GO
INSERT [dbo].[STCYearWiseRates] ([Id], [Year], [Rate]) VALUES (10, 2021, 10)
GO
INSERT [dbo].[STCYearWiseRates] ([Id], [Year], [Rate]) VALUES (11, 2020, 11)
GO
INSERT [dbo].[STCYearWiseRates] ([Id], [Year], [Rate]) VALUES (12, 2019, 12)
GO
SET IDENTITY_INSERT [dbo].[STCYearWiseRates] OFF
