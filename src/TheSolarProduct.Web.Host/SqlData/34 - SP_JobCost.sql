Create Procedure [dbo].[GetJobCost] --'', 7    
(    
@JobNumber varchar(10),    
@OrganizationId int,    
@DateType varchar(100),    
@DiffHour Float,    
@StartDate Datetime,    
@EndDate Datetime, 
@CurruntDate varchar(100),
@State varchar(10),
@Installation int,
@StartSysCapRange decimal(10, 2),
@EndSysCapRange decimal(10, 2)
)     
AS    
Begin    
 Declare @FixBatteryCost decimal;    
 Declare @UptoBatteryKw decimal;    
 Declare @ExtraBatteryCost decimal;    
    
 Declare @RackingCost decimal;    
 Declare @RackingType varchar(10);    
 Declare @AccessoriesCost decimal;    
 Declare @AccessoriesType varchar(10);    
 Declare @AdCost decimal;    
 Declare @AdType varchar(10);    
 Declare @FixExpenseCost decimal;    
 Declare @FixExpenseType varchar(10);    
    
 Select @FixBatteryCost = FixCost, @UptoBatteryKw = Kw, @ExtraBatteryCost = ExtraCost from BatteryInstallationCosts;    
 Select @RackingCost = Cost, @RackingType = [Type] from FixedCostPrice Where [Name] = 'Racking'    
 Select @AccessoriesCost = Cost, @AccessoriesType = [Type] from FixedCostPrice Where [Name] = 'Accessories'    
 Select @AdCost = Cost, @AdType = [Type] from FixedCostPrice Where [Name] = 'Ad Cost'    
 Select @FixExpenseCost = Cost, @FixExpenseType = [Type] from FixedCostPrice Where [Name] = 'Fix Expense'      
 
 Declare @DateTypeNew varchar(100);  Set @DateTypeNew = (Case When @CurruntDate = 'CurruntDate' Then @CurruntDate else @DateType End);  
    
 Select J.Id, J.JobNumber, J.[State], J.PostalCode, L.Area    
     
 , (Select top 1 T.[Name] from UserTeams UT Join Teams T on T.Id = UT.TeamId Where UT.IsDeleted = 0 and T.IsDeleted = 0 and UT.UserId = U.Id) as TeamName    
     
 , Concat(U.[Name], ' ', U.Surname) as SalesRep    
    
 , (Select top 1 [PI].Size from JobProductItems JPI join ProductItems [PI] on JPI.ProductItemId = [PI].Id Where JPI.IsDeleted = 0 and [PI].IsDeleted = 0 and JobId = J.Id and [PI].ProductTypeId = 1) as PanelSize    
     
 , (Select SUM(JPI.Quantity) from JobProductItems JPI join ProductItems [PI] on JPI.ProductItemId = [PI].Id Where JPI.IsDeleted = 0 and [PI].IsDeleted = 0 and JobId = J.Id and [PI].ProductTypeId = 1) as NoOfPanel    
     
 , J.SystemCapacity    
    
 , (Select SUM(ISNULL((IL.UnitPrice * JPI.Quantity), 0)) from JobProductItems JPI Join ProductItems [PI] on JPI.ProductItemId = [PI].ID     
 Join InstallationItemLists IL on IL.ProductItemId = JPI.ProductItemId Join InstallationItemPeriods [IP] on IL.InstallationItemPeriodId = [IP].Id    
    
 Where --J.CreationTime between [IP].StartDate and [IP].EndDate and   
 [IP].OrganizationUnit = L.OrganizationId    
 and JPI.IsDeleted = 0 and [PI].IsDeleted = 0 and [IL].IsDeleted = 0 and [PI].ProductTypeId = 1 and JobId = J.Id  and (Case   
 When @DateTypeNew = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, J.CreationTime))  
 WHEN @DateTypeNew = 'FirstDeposite' then Convert(date, J.FirstDepositDate)   
 WHEN @DateTypeNew = 'DepositeReceived' then Convert(date, J.DepositeRecceivedDate)   
 WHEN @DateTypeNew = 'Active' then Convert(date, DateAdd(hour, @DiffHour, J.ActiveDate))  
 WHEN @DateTypeNew = 'InstallBook' then Convert(date, DateAdd(hour, @DiffHour, J.InstallationDate))  
 WHEN @DateTypeNew = 'InstallComplete' then Convert(date, DateAdd(hour, @DiffHour, J.InstalledcompleteDate))  
 WHEN @DateTypeNew = 'CurruntDate' then Convert(date, DateAdd(hour, @DiffHour, GETDATE()))  
 End) between [IP].StartDate and [IP].EndDate  
 ) as PanelCost    
    
 , (Select SUM(ISNULL((IL.UnitPrice * JPI.Quantity), 0)) from JobProductItems JPI Join ProductItems [PI] on JPI.ProductItemId = [PI].ID     
 Join InstallationItemLists IL on IL.ProductItemId = JPI.ProductItemId Join InstallationItemPeriods [IP] on IL.InstallationItemPeriodId = [IP].Id    
    
 Where --J.CreationTime between [IP].StartDate and [IP].EndDate and   
 [IP].OrganizationUnit = L.OrganizationId    
 and JPI.IsDeleted = 0 and [PI].IsDeleted = 0 and [IL].IsDeleted = 0 and [PI].ProductTypeId = 2 and JobId = J.Id  and (Case   
 When @DateTypeNew = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, J.CreationTime))  
 WHEN @DateTypeNew = 'FirstDeposite' then Convert(date, J.FirstDepositDate)   
 WHEN @DateTypeNew = 'DepositeReceived' then Convert(date, J.DepositeRecceivedDate)   
 WHEN @DateTypeNew = 'Active' then Convert(date, DateAdd(hour, @DiffHour, J.ActiveDate))  
 WHEN @DateTypeNew = 'InstallBook' then Convert(date, DateAdd(hour, @DiffHour, J.InstallationDate))  
 WHEN @DateTypeNew = 'InstallComplete' then Convert(date, DateAdd(hour, @DiffHour, J.InstalledcompleteDate))  
 WHEN @DateTypeNew = 'CurruntDate' then Convert(date, DateAdd(hour, @DiffHour, GETDATE()))  
 End) between [IP].StartDate and [IP].EndDate  ) as InverterCost    
    
 , (Select SUM(ISNULL((IL.UnitPrice * JPI.Quantity), 0)) from JobProductItems JPI Join ProductItems [PI] on JPI.ProductItemId = [PI].ID     
 Join InstallationItemLists IL on IL.ProductItemId = JPI.ProductItemId Join InstallationItemPeriods [IP] on IL.InstallationItemPeriodId = [IP].Id    
    
 Where --J.CreationTime between [IP].StartDate and [IP].EndDate and   
 [IP].OrganizationUnit = L.OrganizationId    
 and JPI.IsDeleted = 0 and [PI].IsDeleted = 0 and [IL].IsDeleted = 0 and [PI].ProductTypeId = 5 and JobId = J.Id  and (Case   
 When @DateTypeNew = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, J.CreationTime))  
 WHEN @DateTypeNew = 'FirstDeposite' then Convert(date, J.FirstDepositDate)   
 WHEN @DateTypeNew = 'DepositeReceived' then Convert(date, J.DepositeRecceivedDate)   
 WHEN @DateTypeNew = 'Active' then Convert(date, DateAdd(hour, @DiffHour, J.ActiveDate))  
 WHEN @DateTypeNew = 'InstallBook' then Convert(date, DateAdd(hour, @DiffHour, J.InstallationDate))  
 WHEN @DateTypeNew = 'InstallComplete' then Convert(date, DateAdd(hour, @DiffHour, J.InstalledcompleteDate))  
 WHEN @DateTypeNew = 'CurruntDate' then Convert(date, DateAdd(hour, @DiffHour, GETDATE()))  
 End) between [IP].StartDate and [IP].EndDate  
 ) as BatteryCost    
      
 --, ISNULL((Case When J.InstallCost is not null then J.InstallCost Else (Select ISNULL((PCP.Price * J.SystemCapacity) , 0)    
 --from PostoCodePrices PCP Where IsDeleted = 0 and PCP.PostCode = J.PostalCode) end), 0) as InstallationCost 
 
 , ISNULL(
 (Case When Exists(select * from InstallerInvoices Where InstallerInvoices.JobId = J.Id and IsDeleted = 0 and Installation_Maintenance_Inspection = 'Installation') Then 
	(select Sum(InstallerInvoices.Amount) from InstallerInvoices Where InstallerInvoices.JobId = J.Id and IsDeleted = 0) 
	else
	(Select ISNULL((PCP.Price * J.SystemCapacity) , 0) from PostoCodePrices PCP Join PostCodePricePeriods PCPP on PCP.PostCodePricePeriodId = PCPP.Id 
	Where PCPP.IsDeleted = 0 and PCP.IsDeleted = 0 and PCP.PostCode = J.PostalCode
	and PCPP.[Month] = (Case   
						 When @DateTypeNew = 'CreationDate' then Month(Convert(date, DateAdd(hour, @DiffHour, J.CreationTime)))
						 WHEN @DateTypeNew = 'FirstDeposite' then Month(Convert(date, J.FirstDepositDate))
						 WHEN @DateTypeNew = 'DepositeReceived' then Month(Convert(date, J.DepositeRecceivedDate))   
						 WHEN @DateTypeNew = 'Active' then Month(Convert(date, DateAdd(hour, @DiffHour, J.ActiveDate)))  
						 WHEN @DateTypeNew = 'InstallBook' then Month(Convert(date, DateAdd(hour, @DiffHour, J.InstallationDate)))  
						 WHEN @DateTypeNew = 'InstallComplete' then Month(Convert(date, DateAdd(hour, @DiffHour, J.InstalledcompleteDate)))
						 WHEN @DateTypeNew = 'CurruntDate' then Month(Convert(date, DateAdd(hour, @DiffHour, GETDATE())))  
						 End)
	and PCPP.[Year] = (Case   
						 When @DateTypeNew = 'CreationDate' then Year(Convert(date, DateAdd(hour, @DiffHour, J.CreationTime)))
						 WHEN @DateTypeNew = 'FirstDeposite' then Year(Convert(date, J.FirstDepositDate))
						 WHEN @DateTypeNew = 'DepositeReceived' then Year(Convert(date, J.DepositeRecceivedDate))   
						 WHEN @DateTypeNew = 'Active' then Year(Convert(date, DateAdd(hour, @DiffHour, J.ActiveDate)))  
						 WHEN @DateTypeNew = 'InstallBook' then Year(Convert(date, DateAdd(hour, @DiffHour, J.InstallationDate)))  
						 WHEN @DateTypeNew = 'InstallComplete' then Year(Convert(date, DateAdd(hour, @DiffHour, J.InstalledcompleteDate)))
						 WHEN @DateTypeNew = 'CurruntDate' then Year(Convert(date, DateAdd(hour, @DiffHour, GETDATE())))  
						 End)
	)
 end), 0) as InstallationCost
    
 , (Select (Case When Exists(select * from JobProductItems Join ProductItems on JobProductItems.ProductItemId = ProductItems.Id
 Where JobProductItems.JobId = J.Id and JobProductItems.IsDeleted = 0 and ProductItems.IsDeleted = 0 and ProductItems.ProductTypeId = 5) then
	 (@FixBatteryCost + (Case When (ISNULL((SUM([PI].Size)), 0) > @UptoBatteryKw) then 
	 ((ISNULL((SUM([PI].Size)), 0) - @UptoBatteryKw) * @ExtraBatteryCost) else 0 end))
 Else 0 End) from JobProductItems JPI Join ProductItems [PI] on JPI.ProductItemId = [PI].ID     
 Join InstallationItemLists IL on IL.ProductItemId = JPI.ProductItemId Join InstallationItemPeriods [IP] on IL.InstallationItemPeriodId = [IP].Id    
    
 Where   
 [IP].OrganizationUnit = L.OrganizationId    
 and JPI.IsDeleted = 0 and [PI].IsDeleted = 0 and [IL].IsDeleted = 0 and [PI].ProductTypeId = 5 and JobId = J.Id  and (Case   
 When @DateTypeNew = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, J.CreationTime))  
 WHEN @DateTypeNew = 'FirstDeposite' then Convert(date, J.FirstDepositDate)   
 WHEN @DateTypeNew = 'DepositeReceived' then Convert(date, J.DepositeRecceivedDate)   
 WHEN @DateTypeNew = 'Active' then Convert(date, DateAdd(hour, @DiffHour, J.ActiveDate))  
 WHEN @DateTypeNew = 'InstallBook' then Convert(date, DateAdd(hour, @DiffHour, J.InstallationDate))  
 WHEN @DateTypeNew = 'InstallComplete' then Convert(date, DateAdd(hour, @DiffHour, J.InstalledcompleteDate))  
 WHEN @DateTypeNew = 'CurruntDate' then Convert(date, DateAdd(hour, @DiffHour, GETDATE()))  
 End) between [IP].StartDate and [IP].EndDate  
 ) as BatteryInstallation    
    
 , (Select (Case When @RackingType = 'Panel' then (@RackingCost * SUM(ISNULL(JPI.Quantity, 0))) When @RackingType = 'Kw' then (@RackingCost * ISNULL(J.SystemCapacity, 0))     
 When @RackingType = 'Job' then (@RackingCost * 1) else 0 end)    
 from JobProductItems JPI Join ProductItems [PI] on JPI.ProductItemId = [PI].ID     
 Join InstallationItemLists IL on IL.ProductItemId = JPI.ProductItemId Join InstallationItemPeriods [IP] on IL.InstallationItemPeriodId = [IP].Id    
    
 Where --J.CreationTime between [IP].StartDate and [IP].EndDate and   
 [IP].OrganizationUnit = L.OrganizationId    
 and JPI.IsDeleted = 0 and [PI].IsDeleted = 0 and [IL].IsDeleted = 0 and [PI].ProductTypeId = 1 and JobId = J.Id  and (Case   
 When @DateTypeNew = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, J.CreationTime))  
 WHEN @DateTypeNew = 'FirstDeposite' then Convert(date, J.FirstDepositDate)   
 WHEN @DateTypeNew = 'DepositeReceived' then Convert(date, J.DepositeRecceivedDate)   
 WHEN @DateTypeNew = 'Active' then Convert(date, DateAdd(hour, @DiffHour, J.ActiveDate))  
 WHEN @DateTypeNew = 'InstallBook' then Convert(date, DateAdd(hour, @DiffHour, J.InstallationDate))  
 WHEN @DateTypeNew = 'InstallComplete' then Convert(date, DateAdd(hour, @DiffHour, J.InstalledcompleteDate))  
 WHEN @DateTypeNew = 'CurruntDate' then Convert(date, DateAdd(hour, @DiffHour, GETDATE()))  
 End) between [IP].StartDate and [IP].EndDate  
 ) as Racking    
    
 , (Select (Case When @AccessoriesType = 'Panel' then (@AccessoriesCost * SUM(ISNULL(JPI.Quantity, 0))) When @AccessoriesType = 'Kw' then (@AccessoriesCost * ISNULL(J.SystemCapacity, 0))     
 When @AccessoriesType = 'Job' then (@AccessoriesCost * 1) else 0 end)    
 from JobProductItems JPI Join ProductItems [PI] on JPI.ProductItemId = [PI].ID     
 Join InstallationItemLists IL on IL.ProductItemId = JPI.ProductItemId Join InstallationItemPeriods [IP] on IL.InstallationItemPeriodId = [IP].Id    
    
 Where --J.CreationTime between [IP].StartDate and [IP].EndDate and   
 [IP].OrganizationUnit = L.OrganizationId    
 and JPI.IsDeleted = 0 and [PI].IsDeleted = 0 and [IL].IsDeleted = 0 and [PI].ProductTypeId = 1 and JobId = J.Id  and (Case   
 When @DateTypeNew = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, J.CreationTime))  
 WHEN @DateTypeNew = 'FirstDeposite' then Convert(date, J.FirstDepositDate)   
 WHEN @DateTypeNew = 'DepositeReceived' then Convert(date, J.DepositeRecceivedDate)   
 WHEN @DateTypeNew = 'Active' then Convert(date, DateAdd(hour, @DiffHour, J.ActiveDate))  
 WHEN @DateTypeNew = 'InstallBook' then Convert(date, DateAdd(hour, @DiffHour, J.InstallationDate))  
 WHEN @DateTypeNew = 'InstallComplete' then Convert(date, DateAdd(hour, @DiffHour, J.InstalledcompleteDate))  
 WHEN @DateTypeNew = 'CurruntDate' then Convert(date, DateAdd(hour, @DiffHour, GETDATE()))  
 End) between [IP].StartDate and [IP].EndDate  
 ) as Accessories    
    
 , (Select (Case When @AdType = 'Panel' then (@AdCost * SUM(ISNULL(JPI.Quantity, 0))) When @AdType = 'Kw' then (@AdCost * ISNULL(J.SystemCapacity, 0))     
 When @AdType = 'Job' then (@AdCost * 1) else 0 end)    
 from JobProductItems JPI Join ProductItems [PI] on JPI.ProductItemId = [PI].ID     
 Join InstallationItemLists IL on IL.ProductItemId = JPI.ProductItemId Join InstallationItemPeriods [IP] on IL.InstallationItemPeriodId = [IP].Id    
    
 Where --J.CreationTime between [IP].StartDate and [IP].EndDate and   
 [IP].OrganizationUnit = L.OrganizationId    
 and JPI.IsDeleted = 0 and [PI].IsDeleted = 0 and [IL].IsDeleted = 0 and [PI].ProductTypeId = 1 and JobId = J.Id  and (Case   
 When @DateTypeNew = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, J.CreationTime))  
 WHEN @DateTypeNew = 'FirstDeposite' then Convert(date, J.FirstDepositDate)   
 WHEN @DateTypeNew = 'DepositeReceived' then Convert(date, J.DepositeRecceivedDate)   
 WHEN @DateTypeNew = 'Active' then Convert(date, DateAdd(hour, @DiffHour, J.ActiveDate))  
 WHEN @DateTypeNew = 'InstallBook' then Convert(date, DateAdd(hour, @DiffHour, J.InstallationDate))  
 WHEN @DateTypeNew = 'InstallComplete' then Convert(date, DateAdd(hour, @DiffHour, J.InstalledcompleteDate))  
 WHEN @DateTypeNew = 'CurruntDate' then Convert(date, DateAdd(hour, @DiffHour, GETDATE()))  
 End) between [IP].StartDate and [IP].EndDate  
 ) as AdCost    
    
 , (Select (Case When @FixExpenseType = 'Panel' then (@FixExpenseCost * SUM(ISNULL(JPI.Quantity, 0))) When @FixExpenseType = 'Kw' then (@FixExpenseCost * ISNULL(J.SystemCapacity, 0))     
 When @FixExpenseType = 'Job' then (@FixExpenseCost * 1) else 0 end)    
 from JobProductItems JPI Join ProductItems [PI] on JPI.ProductItemId = [PI].ID     
 Join InstallationItemLists IL on IL.ProductItemId = JPI.ProductItemId Join InstallationItemPeriods [IP] on IL.InstallationItemPeriodId = [IP].Id    
    
 Where --J.CreationTime between [IP].StartDate and [IP].EndDate and   
 [IP].OrganizationUnit = L.OrganizationId    
 and JPI.IsDeleted = 0 and [PI].IsDeleted = 0 and [IL].IsDeleted = 0 and [PI].ProductTypeId = 1 and JobId = J.Id  and (Case   
 When @DateTypeNew = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, J.CreationTime))  
 WHEN @DateTypeNew = 'FirstDeposite' then Convert(date, J.FirstDepositDate)   
 WHEN @DateTypeNew = 'DepositeReceived' then Convert(date, J.DepositeRecceivedDate)   
 WHEN @DateTypeNew = 'Active' then Convert(date, DateAdd(hour, @DiffHour, J.ActiveDate))  
 WHEN @DateTypeNew = 'InstallBook' then Convert(date, DateAdd(hour, @DiffHour, J.InstallationDate))  
 WHEN @DateTypeNew = 'InstallComplete' then Convert(date, DateAdd(hour, @DiffHour, J.InstalledcompleteDate))  
 WHEN @DateTypeNew = 'CurruntDate' then Convert(date, DateAdd(hour, @DiffHour, GETDATE()))  
 End) between [IP].StartDate and [IP].EndDate  
 ) as FixedExpense    
    
 , (Select top 1    
 (Case When EIC.[Type] = 'Panel' then     
  (     
    (Case When L.Area = 'Metro' then EIC.MetroAmount When L.Area = 'Regional' then EIC.RegionalAmount else 0 end)     
    *    
    ISNULL((Select SUM(ISNULL(JPI.Quantity, 0)) from JobProductItems JPI Join ProductItems [PI] on JPI.ProductItemId = [PI].ID     
    Join InstallationItemLists IL on IL.ProductItemId = JPI.ProductItemId Join InstallationItemPeriods [IP] on IL.InstallationItemPeriodId = [IP].Id    
    Where --J.CreationTime between [IP].StartDate and [IP].EndDate and   
	[IP].OrganizationUnit = L.OrganizationId    
    and JPI.IsDeleted = 0 and [PI].IsDeleted = 0 and [IL].IsDeleted = 0 and [PI].ProductTypeId = 1 and JobId = J.Id   and (Case   
	  When @DateTypeNew = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, J.CreationTime))  
	  WHEN @DateTypeNew = 'FirstDeposite' then Convert(date, J.FirstDepositDate)   
	  WHEN @DateTypeNew = 'DepositeReceived' then Convert(date, J.DepositeRecceivedDate)   
	  WHEN @DateTypeNew = 'Active' then Convert(date, DateAdd(hour, @DiffHour, J.ActiveDate))  
	  WHEN @DateTypeNew = 'InstallBook' then Convert(date, DateAdd(hour, @DiffHour, J.InstallationDate))  
	  WHEN @DateTypeNew = 'InstallComplete' then Convert(date, DateAdd(hour, @DiffHour, J.InstalledcompleteDate))   
	  WHEN @DateTypeNew = 'CurruntDate' then Convert(date, DateAdd(hour, @DiffHour, GETDATE()))  
	  End) between [IP].StartDate and [IP].EndDate  ), 0)    
  )    
 When EIC.[Type] = 'Kw' then    
     (    
   (Case When L.Area = 'Metro' then EIC.MetroAmount When L.Area = 'Regional' then EIC.RegionalAmount else 0 end)     
   * J.SystemCapacity    
     )    
 When EIC.[Type] = 'Job' then    
     (    
   (Case When L.Area = 'Metro' then EIC.MetroAmount When L.Area = 'Regional' then EIC.RegionalAmount else 0 end)     
   * 1    
     )    
 else 0 end)    
 from ExtraInstallationCharges EIC Join [State] S on EIC.StateId = S.Id Where EIC.IsDeleted = 0 and S.[Name] = J.[State]    
 and EIC.Category = 'House Type' and EIC.NameId = J.HouseTypeId) as HouseType    
    
 , (Select top 1    
 (Case When EIC.[Type] = 'Panel' then     
  (     
    (Case When L.Area = 'Metro' then EIC.MetroAmount When L.Area = 'Regional' then EIC.RegionalAmount else 0 end)     
    *    
    ISNULL((Select SUM(ISNULL(JPI.Quantity, 0)) from JobProductItems JPI Join ProductItems [PI] on JPI.ProductItemId = [PI].ID     
    Join InstallationItemLists IL on IL.ProductItemId = JPI.ProductItemId Join InstallationItemPeriods [IP] on IL.InstallationItemPeriodId = [IP].Id    
    Where --J.CreationTime between [IP].StartDate and [IP].EndDate and   
	[IP].OrganizationUnit = L.OrganizationId    
    and JPI.IsDeleted = 0 and [PI].IsDeleted = 0 and [IL].IsDeleted = 0 and [PI].ProductTypeId = 1 and JobId = J.Id   and (Case   
  When @DateTypeNew = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, J.CreationTime))  
  WHEN @DateTypeNew = 'FirstDeposite' then Convert(date, J.FirstDepositDate)   
  WHEN @DateTypeNew = 'DepositeReceived' then Convert(date, J.DepositeRecceivedDate)   
  WHEN @DateTypeNew = 'Active' then Convert(date, DateAdd(hour, @DiffHour, J.ActiveDate))  
  WHEN @DateTypeNew = 'InstallBook' then Convert(date, DateAdd(hour, @DiffHour, J.InstallationDate))  
  WHEN @DateTypeNew = 'InstallComplete' then Convert(date, DateAdd(hour, @DiffHour, J.InstalledcompleteDate))   
  WHEN @DateTypeNew = 'CurruntDate' then Convert(date, DateAdd(hour, @DiffHour, GETDATE()))  
  End) between [IP].StartDate and [IP].EndDate  ), 0)    
  )    
 When EIC.[Type] = 'Kw' then    
     (    
   (Case When L.Area = 'Metro' then EIC.MetroAmount When L.Area = 'Regional' then EIC.RegionalAmount else 0 end)     
   * J.SystemCapacity    
     )    
 When EIC.[Type] = 'Job' then    
     (    
   (Case When L.Area = 'Metro' then EIC.MetroAmount When L.Area = 'Regional' then EIC.RegionalAmount else 0 end)     
   * 1    
     )    
 else 0 end)    
 from ExtraInstallationCharges EIC Join [State] S on EIC.StateId = S.Id Where EIC.IsDeleted = 0 and S.[Name] = J.[State]    
 and EIC.Category = 'Roof Type' and EIC.NameId = J.RoofTypeId) as RoofType    
    
 , (Select top 1    
 (Case When EIC.[Type] = 'Panel' then     
  (     
    (Case When L.Area = 'Metro' then EIC.MetroAmount When L.Area = 'Regional' then EIC.RegionalAmount else 0 end)     
    *    
    ISNULL((Select SUM(ISNULL(JPI.Quantity, 0)) from JobProductItems JPI Join ProductItems [PI] on JPI.ProductItemId = [PI].ID     
    Join InstallationItemLists IL on IL.ProductItemId = JPI.ProductItemId Join InstallationItemPeriods [IP] on IL.InstallationItemPeriodId = [IP].Id    
    Where --J.CreationTime between [IP].StartDate and [IP].EndDate and   
	[IP].OrganizationUnit = L.OrganizationId    
    and JPI.IsDeleted = 0 and [PI].IsDeleted = 0 and [IL].IsDeleted = 0 and [PI].ProductTypeId = 1 and JobId = J.Id   and (Case   
  When @DateTypeNew = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, J.CreationTime))  
  WHEN @DateTypeNew = 'FirstDeposite' then Convert(date, J.FirstDepositDate)   
  WHEN @DateTypeNew = 'DepositeReceived' then Convert(date, J.DepositeRecceivedDate)   
  WHEN @DateTypeNew = 'Active' then Convert(date, DateAdd(hour, @DiffHour, J.ActiveDate))  
  WHEN @DateTypeNew = 'InstallBook' then Convert(date, DateAdd(hour, @DiffHour, J.InstallationDate))  
  WHEN @DateTypeNew = 'InstallComplete' then Convert(date, DateAdd(hour, @DiffHour, J.InstalledcompleteDate))   
  WHEN @DateTypeNew = 'CurruntDate' then Convert(date, DateAdd(hour, @DiffHour, GETDATE()))  
  End) between [IP].StartDate and [IP].EndDate  ), 0)    
  )    
 When EIC.[Type] = 'Kw' then    
     (    
   (Case When L.Area = 'Metro' then EIC.MetroAmount When L.Area = 'Regional' then EIC.RegionalAmount else 0 end)     
   * J.SystemCapacity    
     )    
 When EIC.[Type] = 'Job' then    
     (    
   (Case When L.Area = 'Metro' then EIC.MetroAmount When L.Area = 'Regional' then EIC.RegionalAmount else 0 end)     
   * 1    
     )    
 else 0 end)    
 from ExtraInstallationCharges EIC Join [State] S on EIC.StateId = S.Id Where EIC.IsDeleted = 0 and S.[Name] = J.[State]    
 and EIC.Category = 'Roof Angle' and EIC.NameId = J.RoofAngleId) as RoofAngle    
    
 , (Select SUM(ISNULL(Cost, 0)) from (Select     
 (Case When EIC.[Type] = 'Panel' then     
  (     
    (Case When L.Area = 'Metro' then EIC.MetroAmount When L.Area = 'Regional' then EIC.RegionalAmount else 0 end)     
    *    
    ISNULL((Select SUM(ISNULL(JPI.Quantity, 0)) from JobProductItems JPI Join ProductItems [PI] on JPI.ProductItemId = [PI].ID     
    Join InstallationItemLists IL on IL.ProductItemId = JPI.ProductItemId Join InstallationItemPeriods [IP] on IL.InstallationItemPeriodId = [IP].Id    
    Where --J.CreationTime between [IP].StartDate and [IP].EndDate and   
	[IP].OrganizationUnit = L.OrganizationId    
    and JPI.IsDeleted = 0 and [PI].IsDeleted = 0 and [IL].IsDeleted = 0 and [PI].ProductTypeId = 1 and JobId = J.Id   and (Case   
  When @DateTypeNew = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, J.CreationTime))  
  WHEN @DateTypeNew = 'FirstDeposite' then Convert(date, J.FirstDepositDate)   
  WHEN @DateTypeNew = 'DepositeReceived' then Convert(date, J.DepositeRecceivedDate)   
  WHEN @DateTypeNew = 'Active' then Convert(date, DateAdd(hour, @DiffHour, J.ActiveDate))  
  WHEN @DateTypeNew = 'InstallBook' then Convert(date, DateAdd(hour, @DiffHour, J.InstallationDate))  
  WHEN @DateTypeNew = 'InstallComplete' then Convert(date, DateAdd(hour, @DiffHour, J.InstalledcompleteDate))   
  WHEN @DateTypeNew = 'CurruntDate' then Convert(date, DateAdd(hour, @DiffHour, GETDATE()))  
  End) between [IP].StartDate and [IP].EndDate  ), 0)    
  )    
 When EIC.[Type] = 'Kw' then    
     (    
   (Case When L.Area = 'Metro' then EIC.MetroAmount When L.Area = 'Regional' then EIC.RegionalAmount else 0 end)     
   * J.SystemCapacity    
     )    
 When EIC.[Type] = 'Job' then    
     (    
   (Case When L.Area = 'Metro' then EIC.MetroAmount When L.Area = 'Regional' then EIC.RegionalAmount else 0 end)     
   * 1    
     )    
 else 0 end) as Cost    
      
 from JobVariations JV Join ExtraInstallationCharges EIC on JV.VariationId = EIC.NameId Join [State] S on EIC.StateId = S.Id Where EIC.IsDeleted = 0 and S.[Name] = J.[State]    
    
 and EIC.Category = 'Price Variation' and JobId = J.Id) as tbl    
 ) as Variation    
    
 , (Select Cost * J.STC from STCCosts Where IsDeleted = 0) as STCRebate    
    
 , J.TotalCost    
    
 , JobStatusId, L.AssignToUserID    
    
 , ((Case   
	When ISNUll(J.PaymentOptionId, 0) = 1 then 0.0   
	Else ISNULL((Select [Percentage] from FinanceOptions Where Id = J.FinanceOptionId), 0)  
	End)  
	) as FinancePercentage 
 
 , J.BatteryKw
   
 , (Case   
 When @DateTypeNew = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, J.CreationTime))  
 WHEN @DateTypeNew = 'FirstDeposite' then Convert(date, J.FirstDepositDate)   
 WHEN @DateTypeNew = 'DepositeReceived' then Convert(date, J.DepositeRecceivedDate)   
 WHEN @DateTypeNew = 'Active' then Convert(date, DateAdd(hour, @DiffHour, J.ActiveDate))  
 WHEN @DateTypeNew = 'InstallBook' then Convert(date, DateAdd(hour, @DiffHour, J.InstallationDate))  
 WHEN @DateTypeNew = 'InstallComplete' then Convert(date, DateAdd(hour, @DiffHour, J.InstalledcompleteDate))  
 WHEN @DateTypeNew = 'CurruntDate' then Convert(date, DateAdd(hour, @DiffHour, GETDATE()))  
 End) as CategoryDate
 
 ,(Case When Exists(select * from InstallerInvoices Where InstallerInvoices.JobId = J.Id and IsDeleted = 0 and Installation_Maintenance_Inspection = 'Installation') 
 then 'I' else 'P' end) as InstallationCostType
 
 from Jobs J     
 Join Leads L on J.LeadId = L.Id    
 Join AbpUsers U on L.AssignToUserID = U.Id    
     
 Where J.IsDeleted = 0 and L.IsDeleted = 0 and OrganizationId = @OrganizationId    
 and ((J.JobNumber Like '%'+ @JobNumber +'%') or ISNULL(@JobNumber, '')='')
 and ((J.[State] = @State) or ISNULL(@State, '')='')

 and ((
 (Case When Exists(select * from InstallerInvoices Where InstallerInvoices.JobId = J.Id and IsDeleted = 0 and Installation_Maintenance_Inspection = 'Installation') then 1 else 2 end)
 = @Installation) or ISNULL(@Installation, 0)=0)
 
 and ((J.SystemCapacity >= @StartSysCapRange) or ISNULL(@StartSysCapRange, 0)=0)
 and ((J.SystemCapacity <= @EndSysCapRange) or ISNULL(@EndSysCapRange, 0)=0)

 and isnull(@StartDate,0) <= CASE     
  WHEN @DateType = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, J.CreationTime))     
  WHEN @DateType = 'FirstDeposite' then Convert(date, J.FirstDepositDate)     
  WHEN @DateType = 'DepositeReceived' then Convert(date, J.DepositeRecceivedDate)     
  WHEN @DateType = 'Active' then Convert(date, DateAdd(hour, @DiffHour, J.ActiveDate))     
  WHEN @DateType = 'InstallBook' then Convert(date, DateAdd(hour, @DiffHour, J.InstallationDate))     
  WHEN @DateType = 'InstallComplete' then Convert(date, DateAdd(hour, @DiffHour, J.InstalledcompleteDate))     
 else isnull(@StartDate,0) END                                                                                                                                                 
 and ((@EndDate >= CASE     
  WHEN @DateType = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, J.CreationTime))     
  WHEN @DateType = 'FirstDeposite' then Convert(date, J.FirstDepositDate)     
  WHEN @DateType = 'DepositeReceived' then Convert(date, J.DepositeRecceivedDate)     
  WHEN @DateType = 'Active' then Convert(date, DateAdd(hour, @DiffHour, J.ActiveDate))     
  WHEN @DateType = 'InstallBook' then Convert(date, DateAdd(hour, @DiffHour, J.InstallationDate))     
  WHEN @DateType = 'InstallComplete' then Convert(date, DateAdd(hour, @DiffHour, J.InstalledcompleteDate))                                                                        
 else isnull(@EndDate,0) END) or (isnull(@EndDate,0)=0))        
End 