SET IDENTITY_INSERT [dbo].[PromotionResponseStatuses] ON 

INSERT [dbo].[PromotionResponseStatuses] ([Id], [Name]) VALUES (1, N'Interested')
INSERT [dbo].[PromotionResponseStatuses] ([Id], [Name]) VALUES (2, N'Stop')
INSERT [dbo].[PromotionResponseStatuses] ([Id], [Name]) VALUES (3, N'Not Sure')

SET IDENTITY_INSERT [dbo].[PromotionResponseStatuses] OFF
