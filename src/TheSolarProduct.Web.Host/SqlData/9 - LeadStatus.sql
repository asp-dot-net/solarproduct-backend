SET IDENTITY_INSERT [dbo].[LeadStatus] ON 

INSERT [dbo].[LeadStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(1,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'New')
INSERT [dbo].[LeadStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(2,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'UnHandled')
INSERT [dbo].[LeadStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(3,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Cold')
INSERT [dbo].[LeadStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(4,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Warm')
INSERT [dbo].[LeadStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(5,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Hot')
INSERT [dbo].[LeadStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(6,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Upgrade')
INSERT [dbo].[LeadStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(7,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Rejected')
INSERT [dbo].[LeadStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(8,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Canceled')
INSERT [dbo].[LeadStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(9,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Closed')
INSERT [dbo].[LeadStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(10,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'Assigned')
INSERT [dbo].[LeadStatus]([Id],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[IsDeleted],[DeleterUserId],[DeletionTime],[Status])VALUES(11,N'2020-06-16 17:35:24.8627817',NULL,NULL,NULL,0,NULL,NULL,'ReAssigned')

SET IDENTITY_INSERT [dbo].[LeadStatus] OFF