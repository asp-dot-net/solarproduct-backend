﻿SET IDENTITY_INSERT [dbo].[WholeSalePromotionResponseStatuses] ON 

INSERT [dbo].[WholeSalePromotionResponseStatuses] ([Id], [Name]) VALUES (1, N'Interested')
INSERT [dbo].[WholeSalePromotionResponseStatuses] ([Id], [Name]) VALUES (2, N'Stop')
INSERT [dbo].[WholeSalePromotionResponseStatuses] ([Id], [Name]) VALUES (3, N'Not Sure')

SET IDENTITY_INSERT [dbo].[WholeSalePromotionResponseStatuses] OFF
