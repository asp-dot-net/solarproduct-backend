CREATE Procedure [dbo].[GetLeadAssign] 
( 
@OrganizationUnitId int, 
@UserId int, 
@DiffHour Float, 
@StartDate datetime, 
@EndDate datetime 
)  
AS  
Begin Select * from (
	  Select U.Id, Concat(U.[Name], ' ', U.Surname) as [Name]

	  , (Select Count(*) from Leads L Where L.IsDeleted = 0 and L.OrganizationId = @OrganizationUnitId and L.FirstAssignUserId = U.Id
		and L.LeadSourceId = 3
		and isnull(@StartDate,0) <= CASE 
			WHEN 'CreationDate' = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, L.CreationTime)) 
		else isnull(@StartDate,0) END                                                                                                                                             
		and ((@EndDate >= CASE 
			WHEN 'CreationDate' = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, L.CreationTime)) 
		else isnull(@EndDate,0) END) or (isnull(@EndDate,0)=0))
	  ) as TV
	  
	  , (select count(*) from jobs j where leadid in (Select l.id from Leads L Where L.IsDeleted = 0 and L.OrganizationId = @OrganizationUnitId and L.FirstAssignUserId = U.Id
		and L.LeadSourceId = 3) 
		and isnull(@StartDate,0) <= CASE 
			WHEN 'FirstDepositDate' = 'FirstDepositDate' then Convert(date,  j.FirstDepositDate)
		else isnull(@StartDate,0) END                                                                                                                                             
		and ((@EndDate >= CASE 
			WHEN 'FirstDepositDate' = 'FirstDepositDate' then Convert(date,  j.FirstDepositDate) 
		else isnull(@EndDate,0) END) or (isnull(@EndDate,0)=0)) 

	  ) as TVSold

	  , (Select Count(*) from Leads L Where L.IsDeleted = 0 and L.OrganizationId = @OrganizationUnitId and L.FirstAssignUserId = U.Id
		and L.LeadSourceId In (1,19,23,25)
		and isnull(@StartDate,0) <= CASE 
			WHEN 'CreationDate' = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, L.CreationTime)) 
		else isnull(@StartDate,0) END                                                                                                                                             
		and ((@EndDate >= CASE 
			WHEN 'CreationDate' = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, L.CreationTime)) 
		else isnull(@EndDate,0) END) or (isnull(@EndDate,0)=0))
	  ) as Google

	  ,(select count(*) from jobs j where leadid in(Select l.id from Leads L Where L.IsDeleted = 0 and L.OrganizationId = @OrganizationUnitId and L.FirstAssignUserId = U.Id
		and L.LeadSourceId In (1,19,23,25)) 
		and isnull(@StartDate,0) <= CASE 
			WHEN 'FirstDepositDate' = 'FirstDepositDate' then Convert(date, j.FirstDepositDate)
		else isnull(@StartDate,0) END                                                                                                                                             
		and ((@EndDate >= CASE 
			WHEN 'FirstDepositDate' = 'FirstDepositDate' then Convert(date, j.FirstDepositDate)
		else isnull(@EndDate,0) END) or (isnull(@EndDate,0)=0)) 
	  ) as GoogleSold

	  , (Select Count(*) from Leads L Where L.IsDeleted = 0 and L.OrganizationId = @OrganizationUnitId and L.FirstAssignUserId = U.Id
		and L.LeadSourceId In (4,18,24,26)
		and isnull(@StartDate,0) <= CASE 
			WHEN 'CreationDate' = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, L.CreationTime)) 
		else isnull(@StartDate,0) END                                                                                                                                             
		and ((@EndDate >= CASE 
			WHEN 'CreationDate' = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, L.CreationTime)) 
		else isnull(@EndDate,0) END) or (isnull(@EndDate,0)=0))
	  ) as Facebook

	   , (select count(*) from jobs j where leadid in(Select  l.id from Leads L Where L.IsDeleted = 0 and L.OrganizationId = @OrganizationUnitId and L.FirstAssignUserId = U.Id
		and L.LeadSourceId In (4,18,24,26)) 
		and isnull(@StartDate,0) <= CASE 
			WHEN 'FirstDepositDate' = 'FirstDepositDate' then Convert(date, j.FirstDepositDate)
		else isnull(@StartDate,0) END                                                                                                                                             
		and ((@EndDate >= CASE 
			WHEN 'FirstDepositDate' = 'FirstDepositDate' then Convert(date, j.FirstDepositDate) 
		else isnull(@EndDate,0) END) or (isnull(@EndDate,0)=0))
	  ) as FacebookSold

	  , (Select Count(*) from Leads L Where L.IsDeleted = 0 and L.OrganizationId = @OrganizationUnitId and L.FirstAssignUserId = U.Id
	  and L.LeadSourceId = 2
		and isnull(@StartDate,0) <= CASE 
			WHEN 'CreationDate' = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, L.CreationTime)) 
		else isnull(@StartDate,0) END                                                                                                                                             
		and ((@EndDate >= CASE 
			WHEN 'CreationDate' = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, L.CreationTime)) 
		else isnull(@EndDate,0) END) or (isnull(@EndDate,0)=0))
	  ) as Refferal

	  , (select count(*) from jobs j where leadid in(Select  l.id from Leads L Where L.IsDeleted = 0 and L.OrganizationId = @OrganizationUnitId and L.FirstAssignUserId = U.Id
	  and L.LeadSourceId = 2) 
		and isnull(@StartDate,0) <= CASE 
			WHEN 'FirstDepositDate' = 'FirstDepositDate' then Convert(date, j.FirstDepositDate)
		else isnull(@StartDate,0) END                                                                                                                                             
		and ((@EndDate >= CASE 
			WHEN 'FirstDepositDate' = 'FirstDepositDate' then Convert(date, j.FirstDepositDate)
		else isnull(@EndDate,0) END) or (isnull(@EndDate,0)=0))
	  ) as RefferalSold

	  , (Select Count(*) from Leads L Where L.IsDeleted = 0 and L.OrganizationId = @OrganizationUnitId and L.FirstAssignUserId = U.Id
		and L.LeadSourceId not In (2, 3, 1,19,23,25,4,18,24,26)
		and isnull(@StartDate,0) <= CASE 
			WHEN 'CreationDate' = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, L.CreationTime)) 
		else isnull(@StartDate,0) END                                                                                                                                             
		and ((@EndDate >= CASE 
			WHEN 'CreationDate' = 'CreationDate' then Convert(date, DateAdd(hour, @DiffHour, L.CreationTime)) 
		else isnull(@EndDate,0) END) or (isnull(@EndDate,0)=0))
	  ) as Others
	  
	   , (select count(*) from jobs j where leadid in(Select  l.id from Leads L Where L.IsDeleted = 0 and L.OrganizationId = @OrganizationUnitId and L.FirstAssignUserId = U.Id
		and L.LeadSourceId not In (2, 3, 1,19,23,25,4,18,24,26)) 
		and isnull(@StartDate,0) <= CASE 
			WHEN 'FirstDepositDate' = 'FirstDepositDate' then Convert(date, j.FirstDepositDate)
		else isnull(@StartDate,0) END                                                                                                                                             
		and ((@EndDate >= CASE 
			WHEN 'FirstDepositDate' = 'FirstDepositDate' then Convert(date, j.FirstDepositDate) 
		else isnull(@EndDate,0) END) or (isnull(@EndDate,0)=0))
	  ) as OthersSold

	  from AbpUsers U Join AbpUserRoles UR on U.Id = UR.UserId
	  Join UserWiseEmailOrgs UO on U.Id = UO.UserId
	  
	  Where U.IsDeleted = 0 and UO.IsDeleted = 0 and UO.OrganizationUnitId = @OrganizationUnitId

	  and ((U.Id = @UserId) or ISNULL(@UserId, 0)=0)

) as tbl Where TV != 0 or Google != 0 or Facebook != 0 or Refferal != 0 or Others != 0

End