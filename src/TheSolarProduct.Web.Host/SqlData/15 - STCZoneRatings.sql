SET IDENTITY_INSERT [dbo].[STCZoneRatings] ON 
GO
INSERT [dbo].[STCZoneRatings] ([Id], [Rating], [UpSizeTs]) VALUES (1, 1.622, N'0x00000000003DCC51')
GO
INSERT [dbo].[STCZoneRatings] ([Id], [Rating], [UpSizeTs]) VALUES (2, 1.536, N'0x00000000003DCC52')
GO
INSERT [dbo].[STCZoneRatings] ([Id], [Rating], [UpSizeTs]) VALUES (3, 1.382, N'0x00000000003DCC53')
GO
INSERT [dbo].[STCZoneRatings] ([Id], [Rating], [UpSizeTs]) VALUES (4, 1.185, N'0x00000000003DCC54')
GO
SET IDENTITY_INSERT [dbo].[STCZoneRatings] OFF
