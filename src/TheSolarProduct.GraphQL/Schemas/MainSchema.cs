﻿using Abp.Dependency;
using GraphQL;
using GraphQL.Types;
using TheSolarProduct.Queries.Container;

namespace TheSolarProduct.Schemas
{
    public class MainSchema : Schema, ITransientDependency
    {
        public MainSchema(IDependencyResolver resolver) :
            base(resolver)
        {
            Query = resolver.Resolve<QueryContainer>();
        }
    }
}