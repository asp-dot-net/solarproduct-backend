﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace TheSolarProduct
{
    public class TheSolarProductClientModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TheSolarProductClientModule).GetAssembly());
        }
    }
}
