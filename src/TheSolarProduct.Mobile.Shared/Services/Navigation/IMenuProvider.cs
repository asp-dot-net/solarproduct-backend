using System.Collections.Generic;
using MvvmHelpers;
using TheSolarProduct.Models.NavigationMenu;

namespace TheSolarProduct.Services.Navigation
{
    public interface IMenuProvider
    {
        ObservableRangeCollection<NavigationMenuItem> GetAuthorizedMenuItems(Dictionary<string, string> grantedPermissions);
    }
}