﻿using System.Threading.Tasks;
using TheSolarProduct.Views;
using Xamarin.Forms;

namespace TheSolarProduct.Services.Modal
{
    public interface IModalService
    {
        Task ShowModalAsync(Page page);

        Task ShowModalAsync<TView>(object navigationParameter) where TView : IXamarinView;

        Task<Page> CloseModalAsync();
    }
}
