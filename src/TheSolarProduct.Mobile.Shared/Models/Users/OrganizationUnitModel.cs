﻿using Abp.AutoMapper;
using TheSolarProduct.Organizations.Dto;

namespace TheSolarProduct.Models.Users
{
    [AutoMapFrom(typeof(OrganizationUnitDto))]
    public class OrganizationUnitModel : OrganizationUnitDto
    {
        public bool IsAssigned { get; set; }
    }
}