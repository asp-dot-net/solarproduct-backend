﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace TheSolarProduct
{
    [DependsOn(typeof(TheSolarProductClientModule), typeof(AbpAutoMapperModule))]
    public class TheSolarProductXamarinSharedModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Localization.IsEnabled = false;
            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TheSolarProductXamarinSharedModule).GetAssembly());
        }
    }
}