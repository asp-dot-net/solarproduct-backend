﻿using Xamarin.Forms.Internals;

namespace TheSolarProduct.Behaviors
{
    [Preserve(AllMembers = true)]
    public interface IAction
    {
        bool Execute(object sender, object parameter);
    }
}