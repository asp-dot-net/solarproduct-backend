﻿using TheSolarProduct.SmsTemplates;
using TheSolarProduct.HoldReasons;

using TheSolarProduct.Installer;
using TheSolarProduct.Invoices;
using TheSolarProduct.CancelReasons;
using TheSolarProduct.RejectReasons;
using TheSolarProduct.Jobs;
using TheSolarProduct.Promotions;
using TheSolarProduct.Departments;
using TheSolarProduct.Leads;
using TheSolarProduct.LeadSources;
using TheSolarProduct.PostalTypes;
using TheSolarProduct.PostCodes;
using TheSolarProduct.States;
using TheSolarProduct.StreetNames;
using TheSolarProduct.StreetTypes;
using TheSolarProduct.UnitTypes;
using Abp.IdentityServer4;
using Abp.Organizations;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Authorization.Delegation;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Chat;
using TheSolarProduct.Editions;
using TheSolarProduct.Friendships;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.MultiTenancy.Accounting;
using TheSolarProduct.MultiTenancy.Payments;
using TheSolarProduct.Storage;
using TheSolarProduct.LeadStatuses;
using TheSolarProduct.LeadActions;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Expense;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Quotations;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.EmailTemplates;
using TheSolarProduct.Organizations;
using TheSolarProduct.PVDStatuses;
using TheSolarProduct.CasualMaintenances;
using TheSolarProduct.PreviousJobStatuses;
using TheSolarProduct.LeadHistory;
using TheSolarProduct.Sections;
using TheSolarProduct.JobHistory;
using TheSolarProduct.UserWiseEmailOrgs;
using TheSolarProduct.GreenBoatDatatable;
using TheSolarProduct.ServiceCategorys;
using TheSolarProduct.ServiceSources;
using TheSolarProduct.ServiceStatuses;
using TheSolarProduct.Services;
using TheSolarProduct.ServicePrioritys;
using TheSolarProduct.ServiceTypes;
using TheSolarProduct.ServiceSubCategorys;
using TheSolarProduct.ReviewTypes;
using TheSolarProduct.ServiceCategoryDocs;
using TheSolarProduct.ServiceDocs;
using TheSolarProduct.SysJobs;
using TheSolarProduct.MyInstallerActivityLogs;
using TheSolarProduct.PriceItemLists;
using TheSolarProduct.MyInstallerPriceItemLists;
using TheSolarProduct.ProductPackages;
using TheSolarProduct.EmailSetting;
using TheSolarProduct.InstallationCost.OtherCharges;
using TheSolarProduct.InstallationCost.StateWiseInstallationCosts;
using TheSolarProduct.InstallationCost.InstallationItemLists;
using TheSolarProduct.InstallationCost.FixedCostPrices;
using TheSolarProduct.InstallationCost.ExtraInstallationCharges;
using TheSolarProduct.InstallationCost.STCCost;
using TheSolarProduct.InstallationCost.BatteryInstallationCost;
using TheSolarProduct.InstallationCost.TravelCost;
using TheSolarProduct.DocumentRequests;
using TheSolarProduct.RECDatas;
using TheSolarProduct.LeadGenAppointments;
using TheSolarProduct.CallHistory;
using TheSolarProduct.CallFlowQueues;
using TheSolarProduct.ServiceDocumentRequests;
using TheSolarProduct.WholeSaleStatuses;
using TheSolarProduct.WholeSaleLeads;
using TheSolarProduct.WholeSalePromotions;
using TheSolarProduct.WholeSaleLeadActivityLogs;
using TheSolarProduct.WholeSaleLeadHistory;
using TheSolarProduct.WholeSaleLeadDocs;
using TheSolarProduct.WholeSaleSmsTemplates;
using TheSolarProduct.WholeSaleEmailTemplates;
using TheSolarProduct.PromotionHistorys;
using TheSolarProduct.VppConections;
using TheSolarProduct.CommissionRanges;
using TheSolarProduct.DashboardMessages;
using TheSolarProduct.CategoryInstallations;
using TheSolarProduct.SmsAmounts;
using TheSolarProduct.ECommerceSliders;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct.Businesses;
using TheSolarProduct.InstallationCost.PostCodeCost;
using TheSolarProduct.DataVaults;
using TheSolarProduct.CheckDepositReceived;

using TheSolarProduct.CheckApplications;
using TheSolarProduct.CheckActives;


using TheSolarProduct.TransportationCosts;
using TheSolarProduct.PurchaseCompanies;
using TheSolarProduct.StockOrderStatuses;
using TheSolarProduct.DeliveryTypes;
using TheSolarProduct.PaymentStatuses;
using TheSolarProduct.PaymentMethods;
using TheSolarProduct.PaymentTypes;
using TheSolarProduct.Currencies;
using TheSolarProduct.FreightCompanies;
using TheSolarProduct.StockFroms;
using TheSolarProduct.StockOrderFors;
using TheSolarProduct.PurchaseDocumentListes;
using TheSolarProduct.Vendors;
using TheSolarProduct.IncoTerms;
using TheSolarProduct.StockOrders;
using TheSolarProduct.QuickStocks;
using TheSolarProduct.Wholesales.DataVault;
using TheSolarProduct.BonusList;
using TheSolarProduct.Wholesales;
using TheSolarProduct.CommmonActivityLogs;
using TheSolarProduct.UserActivityLogs;
using TheSolarProduct.ThirdPartyApis.GreenDeal;
using TheSolarProduct.ThirdPartyApis.RECData;
using TheSolarProduct.StockPayments;
using TheSolarProduct.ThirdPartyApis.Credit;
using TheSolarProduct.StockOrders.SerialNoStatuses;
using TheSolarProduct.VoucherMasters;

namespace TheSolarProduct.EntityFrameworkCore
{
	public class TheSolarProductDbContext : AbpZeroDbContext<Tenant, Role, User, TheSolarProductDbContext>, IAbpPersistedGrantDbContext
	{
		public virtual DbSet<SmsTemplate> SmsTemplates { get; set; }

		public virtual DbSet<FreebieTransport> FreebieTransports { get; set; }

		public virtual DbSet<InvoiceStatus> InvoiceStatuses { get; set; }

		public virtual DbSet<casualmaintenance> CasualMaintenances { get; set; }
		public virtual DbSet<QuotationLinkHistory> QuotationLinkHistorys { get; set; }

		public virtual DbSet<PVDStatus> PVDStatus { get; set; }

		public virtual DbSet<ExtendOrganizationUnit> ExtendOrganizationUnits { get; set; }
		 
		public virtual DbSet<HoldReason> HoldReasons { get; set; }
		public virtual DbSet<ServiceCategory> ServiceCategorys { get; set; }
		public virtual DbSet<ServiceSource> ServiceSources { get; set; }
		public virtual DbSet<ServiceStatus> ServiceStatuses { get; set; }
		public virtual DbSet<ServicePriority> ServicePrioritys { get; set; }

		public virtual DbSet<ReviewType> ReviewTypes { get; set; }
		public virtual DbSet<Service> Services { get; set; }
		public virtual DbSet<ServiceType> ServiceTypes { get; set; }
		public virtual DbSet<ServiceSubCategory> ServiceSubCategorys { get; set; }
		public virtual DbSet<ProductItemLocation> ProductItemLocations { get; set; }

		public virtual DbSet<Warehouselocation> Warehouselocations { get; set; }

		public virtual DbSet<JobCancellationReason> JobCancellationReasons { get; set; }

		public virtual DbSet<EmailTemplate> EmailTemplates { get; set; }

		public virtual DbSet<RefundReason> RefundReasons { get; set; }

		public virtual DbSet<JobRefund> JobRefunds { get; set; }

		public virtual DbSet<InstallerDetail> InstallerDetails { get; set; }
		public virtual DbSet<InstallerContract> InstallerContracts { get; set; }
		public virtual DbSet<InstallerAddress> InstallerAddresses { get; set; }

		public virtual DbSet<Quotation> Quotations { get; set; }

		public virtual DbSet<DocumentType> DocumentTypes { get; set; }

		public virtual DbSet<Document> Documents { get; set; }

		public virtual DbSet<InvoicePaymentMethod> InvoicePaymentMethods { get; set; }

		public virtual DbSet<InvoicePayment> InvoicePayments { get; set; }

		public virtual DbSet<STCZoneRating> STCZoneRatings { get; set; }

		public virtual DbSet<STCPostalCode> STCPostalCodes { get; set; }

		public virtual DbSet<STCYearWiseRate> STCYearWiseRates { get; set; }

		public virtual DbSet<JobProductItem> JobProductItems { get; set; }

		public virtual DbSet<PromotionMaster> PromotionMasters { get; set; }

		public virtual DbSet<FinanceOption> FinanceOptions { get; set; }

		public virtual DbSet<HouseType> HouseTypes { get; set; }

		public virtual DbSet<CancelReason> CancelReasons { get; set; }

		public virtual DbSet<RejectReason> RejectReasons { get; set; }

		public virtual DbSet<MeterPhase> MeterPhases { get; set; }

		public virtual DbSet<MeterUpgrade> MeterUpgrades { get; set; }

		public virtual DbSet<ElecRetailer> ElecRetailers { get; set; }

		public virtual DbSet<DepositOption> DepositOptions { get; set; }

		public virtual DbSet<PaymentOption> PaymentOptions { get; set; }

		public virtual DbSet<PromotionOffer> PromotionOffers { get; set; }

		public virtual DbSet<JobVariation> JobVariations { get; set; }

		public virtual DbSet<Variation> Variations { get; set; }

		public virtual DbSet<JobPromotion> JobPromotions { get; set; }

		public virtual DbSet<Job> Jobs { get; set; }

		public virtual DbSet<JobStatus> JobStatuses { get; set; }

		public virtual DbSet<ProductItem> ProductItems { get; set; }

		public virtual DbSet<ElecDistributor> ElecDistributors { get; set; }

		public virtual DbSet<RoofAngle> RoofAngles { get; set; }

		public virtual DbSet<RoofType> RoofTypes { get; set; }

		public virtual DbSet<ProductType> ProductTypes { get; set; }

		public virtual DbSet<JobType> JobTypes { get; set; }

		public virtual DbSet<PromotionUser> PromotionUsers { get; set; }

		public virtual DbSet<Promotion> Promotions { get; set; }

		public virtual DbSet<PromotionResponseStatus> PromotionResponseStatuses { get; set; }

		public virtual DbSet<PromotionType> PromotionTypes { get; set; }

		public virtual DbSet<Department> Departments { get; set; }

		public virtual DbSet<LeadExpense> LeadExpenses { get; set; }

		public virtual DbSet<UserTeam> UserTeams { get; set; }
		public virtual DbSet<PreviousJobStatus> PreviousJobStatuses { get; set; }
		public virtual DbSet<Category> Categories { get; set; }

		public virtual DbSet<Team> Teams { get; set; }

		public virtual DbSet<LeadAction> LeadActions { get; set; }
		public virtual DbSet<Section> Sections { get; set; }

		public virtual DbSet<LeadActivityLog> LeadActivityLogs { get; set; }

		public virtual DbSet<LeadStatus> LeadStatuses { get; set; }

		public virtual DbSet<Lead> Leads { get; set; }

		public virtual DbSet<LeadSource> LeadSources { get; set; }

		public virtual DbSet<PostalType> PostalTypes { get; set; }

		public virtual DbSet<PostCode> PostCodes { get; set; }
		public virtual DbSet<PriceItemList> PriceItemLists { get; set; }

		public virtual DbSet<State> States { get; set; }

		public virtual DbSet<StreetName> StreetNames { get; set; }

		public virtual DbSet<StreetType> StreetTypes { get; set; }

		public virtual DbSet<UnitType> UnitTypes { get; set; }

		public virtual DbSet<ApplicationSetting> ApplicationSettings { get; set; }

		public virtual DbSet<QuotationDetail> QuotationDetails { get; set; }

		public virtual DbSet<QuotationQunityAndModelDetail> QuotationQunityAndModelDetails { get; set; }

		public virtual DbSet<Quotations.JobAcknowledgement> JobAcknowledgements { get; set; }

		public virtual DbSet<Quotations.JobAcknowledgementLinkHistory> JobAcknowledgementLinkHistories { get; set; }

        public virtual DbSet<Quotations.QuotationTemplate> QuotationTemplates { get; set; }

        public virtual DbSet<LeadSourceOrganizationUnit> LeadSourceOrganizationUnits { get; set; }

        public virtual DbSet<TemplateType> TemplateTypes { get; set; }

        /* Define an IDbSet for each entity of the application */

        public virtual DbSet<BinaryObject> BinaryObjects { get; set; }

		public virtual DbSet<Friendship> Friendships { get; set; }

		public virtual DbSet<ChatMessage> ChatMessages { get; set; }

		public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }

		public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }

		public virtual DbSet<Invoice> Invoices { get; set; }

		public virtual DbSet<PersistedGrantEntity> PersistedGrants { get; set; }

		public virtual DbSet<SubscriptionPaymentExtensionData> SubscriptionPaymentExtensionDatas { get; set; }

		public virtual DbSet<UserDelegation> UserDelegations { get; set; }
		public virtual DbSet<InstallerAvailability> InstallerAvailabilities { get; set; }

		public virtual DbSet<InstallerInvoice> InstallerInvoices { get; set; }
		public virtual DbSet<MyInstallerPriceItemList> MyInstallerPriceItemLists { get; set; }
		public virtual DbSet<DocumentLibrary> DocumentLibrarys { get; set; }
		public virtual DbSet<ServiceDoc> ServiceDocs { get; set; }
		public virtual DbSet<ServiceCategoryDoc> ServiceCategoryDocs { get; set; }

		public virtual DbSet<LeadtrackerHistory> LeadtrackerHistorys { get; set; }
		public virtual DbSet<JobTrackerHistory> JobTrackerHistorys { get; set; }

		public virtual DbSet<InvoiceFile> InvoiceFiles { get; set; }
		public virtual DbSet<InvoiceImportData> InvoiceImportDatas { get; set; }
		public virtual DbSet<InstInvoiceHistory> InstInvoiceHistory { get; set; }
		public virtual DbSet<JobApprovalFileHistory> JobApprovalFileHistory { get; set; }

		public virtual DbSet<Jobwarranty> Jobwarranty { get; set; }

		public virtual DbSet<JobOldSystemDetail> JobOldSystemDetails { get; set; }
		public virtual DbSet<EmailTempData> EmailTempData { get; set; }
		public virtual DbSet<LeadExpenseInvestment> LeadExpenseInvestment { get; set; }
		public virtual DbSet<UserWiseEmailOrg> UserWiseEmailOrgs { get; set; }
		public virtual DbSet<GreenBoatData> GreenBoatDatas { get; set; }
		public virtual DbSet<GreenBoatSerialNumber> GreenBoatSerialNumbers { get; set; }
		public virtual DbSet<JobReview> JobReview { get; set; }
		public virtual DbSet<PayWayData> PayWayData { get; set; }
		public virtual DbSet<WestPackData> WestPackData { get; set; }
		public virtual DbSet<WestPackPrincipalAmount> WestPackPrincipalAmount { get; set; }
		public virtual DbSet<WestPackSurchargeAmount> WestPackSurchargeAmount { get; set; }
		public virtual DbSet<WestPackTotalAmount> WestPackTotalAmount { get; set; }
		public virtual DbSet<MyInstallerActivityLog> MyInstallerActivityLog { get; set; }
		public virtual DbSet<VICRebate> VICRebate { get; set; }
		public virtual DbSet<SolarRebateStatus> SolarRebateStatus { get; set; }
		public virtual DbSet<MyInstallerDocument> MyInstallerDocuments { get; set; }
		public virtual DbSet<MyInstallerActivityHistory> MyInstallerActivityHistories { get; set; }

		public virtual DbSet<OrganizationUnitMap> OrganizationUnitMaps { get; set; }

		public virtual DbSet<PostCodeRange.PostCodeRange> PostCodeRanges { get; set; }

		public virtual DbSet<ProductPackage> ProductPackages { get; set; }

		public virtual DbSet<ProductPackageItem> ProductPackageItems { get; set; }

		public virtual DbSet<CallHistory.CallHistory> CallHistory { get; set; }

		public virtual DbSet<CallHistory.CallType> CallTypes { get; set; }

		//Custom Email Setting
		public virtual DbSet<EmailSettings> EmailSettings { get; set; }
		
		public virtual DbSet<EmailProviders> EmailProviders { get; set; }


        #region Installation Cost
        // Other Charge
        public virtual DbSet<OtherCharge> OtherCharges { get; set; }

		//StateWiseInstallationCosts
        public virtual DbSet<StateWiseInstallationCost> StateWiseInstallationCosts { get; set; }

        //Installation Item List
        public virtual DbSet<InstallationItemPeriod> InstallationItemPeriods { get; set; }

        public virtual DbSet<InstallationItemList> InstallationItemLists { get; set; }

        //Fixed Cost Price
        public virtual DbSet<FixedCostPrice> FixedCostPrices { get; set; }

        public virtual DbSet<ExtraInstallationCharge> ExtraInstallationCharges { get; set; }

        public virtual DbSet<STCCost> STCCosts { get; set; }

        public virtual DbSet<BatteryInstallationCost> BatteryInstallationCosts { get; set; }

        public virtual DbSet<TravelCost> TravelCosts { get; set; }

        public virtual DbSet<DeclarationForm> DeclarationForms { get; set; }
        
		public virtual DbSet<DeclarationFormLinkHistory> DeclarationFormLinkHistorys { get; set; }
        #endregion

        public virtual DbSet<DocumentRequestLinkHistory> DocumentRequestLinkHistorys { get; set; }

        public virtual DbSet<DocumentRequest> DocumentRequests { get; set; }
        public virtual DbSet<RECData> RECDatas { get; set; }
        public virtual DbSet<LeadGenAppointment> LeadGenAppointments { get; set; }

        public virtual DbSet<EnfornicaCallDetails> EnfornicaCallDetails { get; set; }
        public virtual DbSet<CallFlowQueue> CallFlowQueues { get; set; }
        public virtual DbSet<CallFlowQueueOrganizationUnit> CallFlowQueueOrganizationUnits { get; set; }

        public virtual DbSet<ServiceDocumentType> ServiceDocumentTypes { get; set; }
        public virtual DbSet<ServiceDocumentRequest> ServiceDocumentRequests { get; set; }
        public virtual DbSet<ServiceDocumentRequestLinkHistory> ServiceDocumentRequestLinkHistorys { get; set; }

        public virtual DbSet<PylonDocument> PylonDocuments { get; set; }

        public virtual DbSet<ServiceInvo> ServiceInvos { get; set; }

        public virtual DbSet<WholeSaleStatus> WholeSaleStatuses { get; set; }

        public virtual DbSet<WholeSaleLead> WholeSaleLeads { get; set; }

        public virtual DbSet<InstallerInvoicePriceList> InstallerInvoicePriceLists { get; set; }

        public virtual DbSet<WholeSalePromotionUser> WholeSalePromotionUsers { get; set; }

        public virtual DbSet<WholeSalePromotion> WholeSalePromotions { get; set; }

        public virtual DbSet<WholeSalePromotionResponseStatus> WholeSalePromotionResponseStatuses { get; set; }

        public virtual DbSet<WholeSalePromotionType> WholeSalePromotionTypes { get; set; }

        public virtual DbSet<WholeSaleLeadActivityLog> WholeSaleLeadActivityLogs { get; set; }

        public virtual DbSet<WholeSaleLeadtrackerHistory> WholeSaleLeadtrackerHistorys { get; set; }

        public virtual DbSet<WholeSaleLeadDocumentType> WholeSaleLeadDocumentTypes { get; set; }

        public virtual DbSet<WholeSaleLeadDoc> WholeSaleLeadDocs { get; set; }

        public virtual DbSet<WholeSaleSmsTemplate> WholeSaleSmsTemplates { get; set; }

        public virtual DbSet<WholeSaleEmailTemplate> WholeSaleEmailTemplates { get; set; }

        public virtual DbSet<PromotionHistory> PromotionHistorys { get; set; }

        public virtual DbSet<VppConection> VppConections { get; set; }

        public virtual DbSet<CommissionRange> CommissionRanges { get; set; }

        public virtual DbSet<UserIPAddress> UserIPAddresses { get; set; }

        public virtual DbSet<DashboardMessage> DashboardMessages { get; set; }

        public virtual DbSet<CategoryInstallationItemPeriod> CategoryInstallationItemPeriods { get; set; }

        public virtual DbSet<CategoryInstallationItemList> CategoryInstallationItemLists { get; set; }

        public virtual DbSet<SmsAmount> SmsAmounts { get; set; }

        public virtual DbSet<ECommerceSlider> ECommerceSliders { get; set; }

        public virtual DbSet<EcomerceSpecialStatus> EcomerceSpecialStatuss { get; set; }

        public virtual DbSet<ECommerceDocumentType> ECommerceDocumentTypes { get; set; }

        public virtual DbSet<ECommercePriceCategory> ECommercePriceCategorys { get; set; }

        public virtual DbSet<EcommerceProductItem> EcommerceProductItems { get; set; }

        public virtual DbSet<EcommerceProductItemDocument> EcommerceProductItemDocuments { get; set; }

        public virtual DbSet<EcommerceProductItemPriceCategory> EcommerceProductItemPriceCategorys { get; set; }
        public virtual DbSet<Business> Businesses { get; set; }
        public virtual DbSet<BuisnessStructure> BuisnessStructures { get; set; }
        public virtual DbSet<BuisnessDocumentType> BuisnessDocumentTypes { get; set; }

        public virtual DbSet<PostoCodePrice> PostoCodePrices { get; set; }

        public virtual DbSet<BrandingPartner> BrandingPartners { get; set; }

        public virtual DbSet<Wholesales.Ecommerces.Series> Serieses { get; set; }

        public virtual DbSet<SpecialOffer> SpecialOffers { get; set; }

        public virtual DbSet<SpecialOfferProduct> SpecialOfferProducts { get; set; }

        public virtual DbSet<EcommerceUserRegisterRequest> EcommerceUserRegisterRequests { get; set; }

        public virtual DbSet<EcommerceSolarPackage> EcommerceSolarPackages { get; set; }

        public virtual DbSet<EcommerceSolarPackageItem> EcommerceSolarPackageItems { get; set; }

        public virtual DbSet<WholeSaleLeadContact> WholeSaleLeadContacts { get; set; }

        public virtual DbSet<TradingSTCWhom> TradingSTCWhoms { get; set; }

        public virtual DbSet<CustomerType> CustomerTypes { get; set; }

        public virtual DbSet<EcommerceSubscriber> EcommerceSubscribers { get; set; }

        public virtual DbSet<EcommerceContactUs> EcommerceContactUses { get; set; }

        public virtual DbSet<InstallerInvoiceType> InstallerInvoiceTypes { get; set; }

        public virtual DbSet<DataVaultSection> DataVaultSections { get; set; }

        public virtual DbSet<DataVaultActivityLog> DataVaultActivityLogs { get; set; }

        public virtual DbSet<DataVaultActivityLogHistory> DataVaultActivityLogHistory { get; set; }

        public virtual DbSet<Checkdeposite> CheckDepositReceived { get; set; }

        public virtual DbSet<CheckApplication> CheckApplication { get; set; }
       
		public virtual DbSet<CheckActive> CheckActive { get; set; }

        public virtual DbSet<TransportCompany> TransportCompanies { get; set; }

        public virtual DbSet<TransportationCost> TransportationCosts { get; set; }

        public virtual DbSet<TransportationCostJob> TransportationCostJobs { get; set; }

        public virtual DbSet<PurchaseCompany> PurchaseCompanies { get; set; }

        public virtual DbSet<StockOrderStatuses.StockOrderStatus> StockOrderStatuses { get; set; }

        public virtual DbSet<DeliveryType> DeliveryTypes { get; set; }

        public virtual DbSet<PaymentStatus> PaymentStatuses { get; set; }

        public virtual DbSet<PaymentMethod> PaymentMethods { get; set; }

        public virtual DbSet<PaymentType> PaymentTypes { get; set; }

        public virtual DbSet<Currency> Currencies { get; set; }

        public virtual DbSet<FreightCompany> FreightCompanies { get; set; }

        public virtual DbSet<StockFrom> StockFroms { get; set; }

        public virtual DbSet<StockOrderFor> StockOrderFors { get; set; }

        public virtual DbSet<PurchaseDocumentList> PurchaseDocumentLists { get; set; }

        public virtual DbSet<Vendor> Vendors { get; set; }

        public virtual DbSet<VendorContact> VendorContacts { get; set; }

        public virtual DbSet<EcommerceProductItemImage> EcommerceProductItemImages { get; set; }

        public virtual DbSet<STCRegister> STCRegisters { get; set; }

		public virtual DbSet<PostCodePricePeriod> PostCodePricePeriods { get; set; }
        
		public virtual DbSet<IncoTerm> IncoTerms { get; set; }
        
		public virtual DbSet<PurchaseOrder> PurchaseOrders { get; set; }

		public virtual DbSet<STCProvider> STCProviders { get; set; }

		public virtual DbSet<PurchaseOrderItem> PurchaseOrderItems { get; set; }

		public virtual DbSet<QLDBatteryRebate> QLDBatteryRebates { get; set; }

        public virtual DbSet<QuickStockSection> QuickStockSections { get; set; }

        public virtual DbSet<QuickStockActivityLog> QuickStockActivityLogs { get; set; }

        public virtual DbSet<QuickStockActivityLogHistory> QuickStockActivityLogHistory { get; set; }

        public virtual DbSet<UserWarehouseLocation> UserWarehouseLocations { get; set; }

        public virtual DbSet<RequestWholesaleLeadTransfer> RequestWholesaleLeadTransfers { get; set; }

        public virtual DbSet<WholeSaleDataVaultSection> WholeSaleDataVaultSections { get; set; }
        
        public virtual DbSet<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityLogs { get; set; }

        public virtual DbSet<WholeSaleDataVaultActivityLogHistory> WholeSaleDataVaultActivityLogHistorys { get; set; }

        public virtual DbSet<WholesaleInvoicetype> Wholesaleinvoicetypes { get; set; }

        public virtual DbSet<Wholesales.Ecommerces.WholesaleDeliveryOption> WholesaleDeliveryOptions { get; set; }
        
		public virtual DbSet<WholesaleTransportType> WholesaleTransportTypes { get; set; }

        public virtual DbSet<WholesaleJobType> WholesaleJobTypes { get; set; }

        public virtual DbSet<WholesalePropertyType> WholesalePropertyTypes { get; set; }

        public virtual DbSet<WholesalePVDStatus> WholesalePVDStatuses { get; set; }
        
		public virtual DbSet<WholesaleJobStatus> WholesaleJobStatuses { get; set; }

        public virtual DbSet<EcommerceSpecification> EcommerceSpecifications { get; set; }

        public virtual DbSet<Bonus> Bonuses { get; set; }

        public virtual DbSet<BonusCommissionItem> BonusCommissionItems { get; set; }

        public virtual DbSet<EcommerceProductType> EcommerceProductTypes { get; set; }

        public virtual DbSet<EcommerceCartProductItem> EcommerceCartProductItems { get; set; }

        public virtual DbSet<WholesaleInvoice> WholesaleInvoices { get; set; }

        public virtual DbSet<StockSerialNo> StockSerialNos { get; set; }

        public virtual DbSet<EcommerceProductItemSpecification> EcommerceProductItemSpecifications { get; set; }

        public virtual DbSet<StockTransfer> StockTransfers { get; set; }

        public virtual DbSet<StockTransferProductItem> StockTransferProductItems { get; set; }

        public virtual DbSet<CommonSection> CommonSections { get; set; }

        public virtual DbSet<UserActivityLog> UserActivityLogs { get; set; }

        public virtual DbSet<GreenDealJob> GreenDealJobs { get; set; }

        public virtual DbSet<GreenDealJobSerialNo> GreenDealJobSerialNos { get; set; }

        public virtual DbSet<PurchaseOrderDocument> PurchaseOrderDocuments { get; set; }

        public virtual DbSet<JobCostFixExpenseType> JobCostFixExpenseTypes { get; set; }

        public virtual DbSet<JobCostFixExpensePeriod> JobCostFixExpensePeriods { get; set; }

        public virtual DbSet<JobCostFixExpenseList> JobCostFixExpenseLists { get; set; }

		public virtual DbSet<RECDetail> RECDetails { get; set; }

		public virtual DbSet<StockPayment> StockPayments { get; set; }
        public virtual DbSet<StockPaymentOrderList> StockPaymentOrderLists { get; set; }

        public virtual DbSet<JobSuggestedProduct> JobSuggestedProducts { get; set; }
        public virtual DbSet<JobSuggestedProductItemList> JobSuggestedProductItemLists { get; set; }
        public virtual DbSet<StockVariation> StockVariations { get; set; }
        public virtual DbSet<StockOrderVariation> StockOrderVariations { get; set; }
        public virtual DbSet<StcDealPrice> StcDealPrices { get; set; }

        public virtual DbSet<CimetDetail> CimetDetails { get; set; }

        public virtual DbSet<OtherStockItemQty> OtherStockItemQty { get; set; }

        public virtual DbSet<AppDocument> AppDocuments { get; set; }

        public virtual DbSet<SerialNoStatus> SerialNoStatuses { get; set; }

        public virtual DbSet<StockSerialNoLogHistory> StockSerialNoLogHistorys { get; set; }
        public virtual DbSet<VoucherMaster> VoucherMasters { get; set; }
        public virtual DbSet<MultipleOrganization> MultipleOrganizations { get; set; }
        
        public TheSolarProductDbContext(DbContextOptions<TheSolarProductDbContext> options)
			: base(options)
		{

		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<SmsTemplate>(s =>
			{
				s.HasIndex(e => new { e.TenantId });
			});
			modelBuilder.Entity<FreebieTransport>(f =>
						{
							f.HasIndex(e => new { e.TenantId });
						});
			modelBuilder.Entity<InvoiceStatus>(i =>
					   {
						   i.HasIndex(e => new { e.TenantId });
					   });
			modelBuilder.Entity<RefundReason>(r =>
					   {
						   r.HasIndex(e => new { e.TenantId });
					   });
			modelBuilder.Entity<HoldReason>(h =>
					   {
						   h.HasIndex(e => new { e.TenantId });
					   });
			modelBuilder.Entity<RefundReason>(r =>
					{
						r.HasIndex(e => new { e.TenantId });
					});
			modelBuilder.Entity<JobRefund>(j =>
					   {
						   j.HasIndex(e => new { e.TenantId });
						   modelBuilder.Entity<ProductItem>(p =>
				{
					p.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<InstallerDetail>(i =>
						   {
							   i.HasIndex(e => new { e.TenantId });
						   });

						   modelBuilder.Entity<InstallerAddress>(i =>
						   {
							   i.HasIndex(e => new { e.TenantId });
						   });

						   modelBuilder.Entity<InvoicePaymentMethod>(i =>
						   {
							   i.HasIndex(e => new { e.TenantId });
						   });
						   modelBuilder.Entity<InvoicePayment>(i =>
						   {
							   i.HasIndex(e => new { e.TenantId });
						   });
						   modelBuilder.Entity<JobProductItem>(j =>
				{
					j.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<PromotionMaster>(p =>
				{
					p.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<FinanceOption>(f =>
				{
					f.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<CancelReason>(c =>
				{
					c.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<RejectReason>(r =>
				{
					r.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<DepositOption>(d =>
				{
					d.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<PaymentOption>(p =>
				{
					p.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<JobVariation>(j =>
				{
					j.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<Variation>(v =>
				{
					v.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<JobPromotion>(j =>
				{
					j.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<Job>(j =>
				{
					j.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<ProductItem>(p =>
				{
					p.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<PromotionUser>(p =>
				{
					p.HasIndex(e => new { e.TenantId });
				});
						   modelBuilder.Entity<Promotion>(p =>
				{
					p.HasIndex(e => new { e.TenantId });
				});
						   //modelBuilder.Entity<PromotionResponseStatus>(p =>
						   //           {
						   //               p.HasIndex(e => new { e.TenantId });
						   //           });
						   //modelBuilder.Entity<PromotionType>(p =>
						   //           {
						   //               p.HasIndex(e => new { e.TenantId });
						   //           });
						   modelBuilder.Entity<Department>(d =>
												 {
													 d.HasIndex(e => new { e.TenantId });
												 });
						   modelBuilder.Entity<Lead>(l =>
			 {
				 l.HasIndex(e => new { e.TenantId });
			 });

						   //modelBuilder.Entity<LeadSource>(l =>
						   //		   {
						   //			   l.HasIndex(e => new { e.TenantId });
						   //		   });
						   modelBuilder.Entity<BinaryObject>(b =>
									  {
										  b.HasIndex(e => new { e.TenantId });
									  });

						   modelBuilder.Entity<ChatMessage>(b =>
						   {
							   b.HasIndex(e => new { e.TenantId, e.UserId, e.ReadState });
							   b.HasIndex(e => new { e.TenantId, e.TargetUserId, e.ReadState });
							   b.HasIndex(e => new { e.TargetTenantId, e.TargetUserId, e.ReadState });
							   b.HasIndex(e => new { e.TargetTenantId, e.UserId, e.ReadState });
						   });

						   modelBuilder.Entity<Friendship>(b =>
						   {
							   b.HasIndex(e => new { e.TenantId, e.UserId });
							   b.HasIndex(e => new { e.TenantId, e.FriendUserId });
							   b.HasIndex(e => new { e.FriendTenantId, e.UserId });
							   b.HasIndex(e => new { e.FriendTenantId, e.FriendUserId });
						   });

						   modelBuilder.Entity<Tenant>(b =>
						   {
							   b.HasIndex(e => new { e.SubscriptionEndDateUtc });
							   b.HasIndex(e => new { e.CreationTime });
						   });

						   modelBuilder.Entity<SubscriptionPayment>(b =>
						   {
							   b.HasIndex(e => new { e.Status, e.CreationTime });
							   b.HasIndex(e => new { PaymentId = e.ExternalPaymentId, e.Gateway });
						   });

						   modelBuilder.Entity<SubscriptionPaymentExtensionData>(b =>
						   {
							   b.HasQueryFilter(m => !m.IsDeleted)
						.HasIndex(e => new { e.SubscriptionPaymentId, e.Key, e.IsDeleted })
						.IsUnique();
						   });

						   modelBuilder.Entity<UserDelegation>(b =>
						   {
							   b.HasIndex(e => new { e.TenantId, e.SourceUserId });
							   b.HasIndex(e => new { e.TenantId, e.TargetUserId });
						   });

						   modelBuilder.Entity<Section>(s =>
						   {
							   s.HasIndex(e => new { e.SectionId }).IsUnique();
						   });

						   modelBuilder.ConfigurePersistedGrantEntity();
					   });
		}

	}
}