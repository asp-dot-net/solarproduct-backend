using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace TheSolarProduct.EntityFrameworkCore
{
    public static class TheSolarProductDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<TheSolarProductDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<TheSolarProductDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}