﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using TheSolarProduct.Configuration;
using TheSolarProduct.Web;

namespace TheSolarProduct.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class TheSolarProductDbContextFactory : IDesignTimeDbContextFactory<TheSolarProductDbContext>
    {
        public TheSolarProductDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<TheSolarProductDbContext>();
            var configuration = AppConfigurations.Get(
                WebContentDirectoryFinder.CalculateContentRootFolder(),
                addUserSecrets: true
            );

            TheSolarProductDbContextConfigurer.Configure(builder, configuration.GetConnectionString(TheSolarProductConsts.ConnectionStringName));

            return new TheSolarProductDbContext(builder.Options);
        }
    }
}