﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_InvoicePayment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InvoicePayments_InvoicePaymentMethods_InvoicePaymentStatusId",
                table: "InvoicePayments");

            migrationBuilder.AddForeignKey(
                name: "FK_InvoicePayments_InvoiceStatuses_InvoicePaymentStatusId",
                table: "InvoicePayments",
                column: "InvoicePaymentStatusId",
                principalTable: "InvoiceStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InvoicePayments_InvoiceStatuses_InvoicePaymentStatusId",
                table: "InvoicePayments");

            migrationBuilder.AddForeignKey(
                name: "FK_InvoicePayments_InvoicePaymentMethods_InvoicePaymentStatusId",
                table: "InvoicePayments",
                column: "InvoicePaymentStatusId",
                principalTable: "InvoicePaymentMethods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
