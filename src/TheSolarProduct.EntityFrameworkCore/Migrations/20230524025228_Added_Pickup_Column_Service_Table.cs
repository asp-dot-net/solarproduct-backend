﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_Pickup_Column_Service_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "PickUpDate",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Pickby",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PickupMethod",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PickupNotes",
                table: "Services",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PickUpDate",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "Pickby",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "PickupMethod",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "PickupNotes",
                table: "Services");
        }
    }
}
