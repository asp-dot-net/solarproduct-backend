﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class housetypeandfinoptionstable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FinPaymentTypeId",
                table: "Jobs");

            migrationBuilder.AddColumn<int>(
                name: "FinanceOptionId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "FinanceOptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    DisplayOrder = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FinanceOptions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HouseTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    DisplayOrder = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HouseTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_FinanceOptionId",
                table: "Jobs",
                column: "FinanceOptionId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_HouseTypeId",
                table: "Jobs",
                column: "HouseTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_FinanceOptions_TenantId",
                table: "FinanceOptions",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_FinanceOptions_FinanceOptionId",
                table: "Jobs",
                column: "FinanceOptionId",
                principalTable: "FinanceOptions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_HouseTypes_HouseTypeId",
                table: "Jobs",
                column: "HouseTypeId",
                principalTable: "HouseTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_FinanceOptions_FinanceOptionId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_HouseTypes_HouseTypeId",
                table: "Jobs");

            migrationBuilder.DropTable(
                name: "FinanceOptions");

            migrationBuilder.DropTable(
                name: "HouseTypes");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_FinanceOptionId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_HouseTypeId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FinanceOptionId",
                table: "Jobs");

            migrationBuilder.AddColumn<int>(
                name: "FinPaymentTypeId",
                table: "Jobs",
                type: "int",
                nullable: true);
        }
    }
}
