﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Warrantysmsemail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "WarrantyEmailSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "WarrantyEmailSendDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "WarrantySmsSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "WarrantySmsSendDate",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WarrantyEmailSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "WarrantyEmailSendDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "WarrantySmsSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "WarrantySmsSendDate",
                table: "Jobs");
        }
    }
}
