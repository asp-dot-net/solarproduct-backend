﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_Leads_NewFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FormName",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GCLId",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserIP",
                table: "Leads",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FormName",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "GCLId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "UserIP",
                table: "Leads");
        }
    }
}
