﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Updated_JobCostFixExpensePeriod_Table_Added_MonthYear : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndDate",
                table: "JobCostFixExpensePeriods");

            migrationBuilder.DropColumn(
                name: "StartDate",
                table: "JobCostFixExpensePeriods");

            migrationBuilder.AddColumn<int>(
                name: "Month",
                table: "JobCostFixExpensePeriods",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Year",
                table: "JobCostFixExpensePeriods",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Month",
                table: "JobCostFixExpensePeriods");

            migrationBuilder.DropColumn(
                name: "Year",
                table: "JobCostFixExpensePeriods");

            migrationBuilder.AddColumn<DateTime>(
                name: "EndDate",
                table: "JobCostFixExpensePeriods",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "StartDate",
                table: "JobCostFixExpensePeriods",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
