﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Add_Color_Filed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ColorClass",
                table: "LeadStatus",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ColorClass",
                table: "JobStatuses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ColorClass",
                table: "CallTypes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ColorClass",
                table: "LeadStatus");

            migrationBuilder.DropColumn(
                name: "ColorClass",
                table: "JobStatuses");

            migrationBuilder.DropColumn(
                name: "ColorClass",
                table: "CallTypes");
        }
    }
}
