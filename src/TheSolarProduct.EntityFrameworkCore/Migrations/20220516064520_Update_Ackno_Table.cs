﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_Ackno_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CustSignFileName",
                table: "JobAcknowledgements",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustSignIP",
                table: "JobAcknowledgements",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustSignLatitude",
                table: "JobAcknowledgements",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustSignLongitude",
                table: "JobAcknowledgements",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsSigned",
                table: "JobAcknowledgements",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "SignDate",
                table: "JobAcknowledgements",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SignFilePath",
                table: "JobAcknowledgements",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustSignFileName",
                table: "JobAcknowledgements");

            migrationBuilder.DropColumn(
                name: "CustSignIP",
                table: "JobAcknowledgements");

            migrationBuilder.DropColumn(
                name: "CustSignLatitude",
                table: "JobAcknowledgements");

            migrationBuilder.DropColumn(
                name: "CustSignLongitude",
                table: "JobAcknowledgements");

            migrationBuilder.DropColumn(
                name: "IsSigned",
                table: "JobAcknowledgements");

            migrationBuilder.DropColumn(
                name: "SignDate",
                table: "JobAcknowledgements");

            migrationBuilder.DropColumn(
                name: "SignFilePath",
                table: "JobAcknowledgements");
        }
    }
}
