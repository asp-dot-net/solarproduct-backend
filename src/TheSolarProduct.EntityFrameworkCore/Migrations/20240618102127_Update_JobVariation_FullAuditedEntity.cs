﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_JobVariation_FullAuditedEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "JobVariations",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "JobVariations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "JobVariations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "JobVariations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "JobVariations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "JobVariations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "JobVariations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "JobVariations");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "JobVariations");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "JobVariations");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "JobVariations");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "JobVariations");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "JobVariations");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "JobVariations");
        }
    }
}
