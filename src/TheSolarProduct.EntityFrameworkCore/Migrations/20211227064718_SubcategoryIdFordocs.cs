﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class SubcategoryIdFordocs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceCategoryDocs_ServiceCategorys_ServiceSubCategorybId",
                table: "ServiceCategoryDocs");

            migrationBuilder.DropIndex(
                name: "IX_ServiceCategoryDocs_ServiceSubCategorybId",
                table: "ServiceCategoryDocs");

            migrationBuilder.DropColumn(
                name: "ServiceCategoryId",
                table: "ServiceCategoryDocs");

            migrationBuilder.DropColumn(
                name: "ServiceSubCategorybId",
                table: "ServiceCategoryDocs");

            migrationBuilder.AddColumn<int>(
                name: "SubCategoryId",
                table: "ServiceCategoryDocs",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceCategoryDocs_SubCategoryId",
                table: "ServiceCategoryDocs",
                column: "SubCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceCategoryDocs_ServiceCategorys_SubCategoryId",
                table: "ServiceCategoryDocs",
                column: "SubCategoryId",
                principalTable: "ServiceCategorys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceCategoryDocs_ServiceCategorys_SubCategoryId",
                table: "ServiceCategoryDocs");

            migrationBuilder.DropIndex(
                name: "IX_ServiceCategoryDocs_SubCategoryId",
                table: "ServiceCategoryDocs");

            migrationBuilder.DropColumn(
                name: "SubCategoryId",
                table: "ServiceCategoryDocs");

            migrationBuilder.AddColumn<string>(
                name: "ServiceCategoryId",
                table: "ServiceCategoryDocs",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ServiceSubCategorybId",
                table: "ServiceCategoryDocs",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceCategoryDocs_ServiceSubCategorybId",
                table: "ServiceCategoryDocs",
                column: "ServiceSubCategorybId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceCategoryDocs_ServiceCategorys_ServiceSubCategorybId",
                table: "ServiceCategoryDocs",
                column: "ServiceSubCategorybId",
                principalTable: "ServiceCategorys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
