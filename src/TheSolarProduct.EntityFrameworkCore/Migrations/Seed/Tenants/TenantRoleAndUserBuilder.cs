﻿using System.Collections.Generic;
using System.Linq;
using Abp;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.MultiTenancy;
using Abp.Notifications;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using TheSolarProduct.Authorization;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.EmailSetting;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Jobs;
using TheSolarProduct.Notifications;
using TheSolarProduct.PVDStatuses;

namespace TheSolarProduct.Migrations.Seed.Tenants
{
    public class TenantRoleAndUserBuilder
    {
        private readonly TheSolarProductDbContext _context;
        private readonly int _tenantId;
        public static List<PVDStatus> InitialPVDStatus => GetInitialPVDStatus();

        public TenantRoleAndUserBuilder(TheSolarProductDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            CreateRolesAndUsers();
            CreatePVDStatus();
            //CreateSection();
            //CreateProductTypes();
            //CreateLeadSources();
            //CreateLeadStatus();
            //CreateLeadAction();
            //CreateSTCZoneRatings();
            //CreateSTCYearWiseRates();
            CreateEmailTemplate();
            CreateEmailProvider();
        }

        private void CreateRolesAndUsers()
        {
            //Admin role

            var adminRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Admin);
            if (adminRole == null)
            {
                adminRole = _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Admin, StaticRoleNames.Tenants.Admin) { IsStatic = true }).Entity;
                _context.SaveChanges();
            }

            //User role

            var userRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.User);
            if (userRole == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.User, StaticRoleNames.Tenants.User) { IsStatic = true, IsDefault = true });
                _context.SaveChanges();
            }

            //SalesRep role

            var SalesRep = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.SalesRep);
            if (SalesRep == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.SalesRep, StaticRoleNames.Tenants.SalesRep) { IsStatic = true });
                _context.SaveChanges();
            }

            //SalesManager role

            var SalesManager = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.SalesManager);
            if (SalesManager == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.SalesManager, StaticRoleNames.Tenants.SalesManager) { IsStatic = true });
                _context.SaveChanges();
            }

            //Installer Role
            var Installer = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Installer);
            if (Installer == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Installer, StaticRoleNames.Tenants.Installer) { IsStatic = true });
                _context.SaveChanges();
            }

            //Installer Manager Role
            var InstallerManager = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.InstallerManager);
            if (InstallerManager == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.InstallerManager, StaticRoleNames.Tenants.InstallerManager) { IsStatic = true });
                _context.SaveChanges();
            }

            //Installer Manager Role
            var PostInstallation = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.PostInstallation);
            if (PostInstallation == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.PostInstallation, StaticRoleNames.Tenants.PostInstallation) { IsStatic = true });
                _context.SaveChanges();
            }

            //Installer Manager Role
            var PreInstallation = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.PreInstallation);
            if (PreInstallation == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.PreInstallation, StaticRoleNames.Tenants.PreInstallation) { IsStatic = true });
                _context.SaveChanges();
            }

            //LeadOperator role

            var LeadOperator = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.LeadOperator);
            if (LeadOperator == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.LeadOperator, StaticRoleNames.Tenants.LeadOperator) { IsStatic = true });
                _context.SaveChanges();
            }

            //Wholesale Client
            //var WholesaleClient = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.WholesaleClient);
            //if (WholesaleClient == null)
            //{
            //    _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.WholesaleClient, StaticRoleNames.Tenants.WholesaleClient) { IsStatic = true });
            //    _context.SaveChanges();
            //}

            //admin user

            var adminUser = _context.Users.IgnoreQueryFilters().FirstOrDefault(u => u.TenantId == _tenantId && u.UserName == AbpUserBase.AdminUserName);
            if (adminUser == null)
            {
                adminUser = User.CreateTenantAdminUser(_tenantId, "admin@defaulttenant.com");
                adminUser.Password = new PasswordHasher<User>(new OptionsWrapper<PasswordHasherOptions>(new PasswordHasherOptions())).HashPassword(adminUser, "123qwe");
                adminUser.IsEmailConfirmed = true;
                adminUser.ShouldChangePasswordOnNextLogin = false;
                adminUser.IsActive = true;

                _context.Users.Add(adminUser);
                _context.SaveChanges();

                //Assign Admin role to admin user
                _context.UserRoles.Add(new UserRole(_tenantId, adminUser.Id, adminRole.Id));
                _context.SaveChanges();

                //User account of admin user
                if (_tenantId == 1)
                {
                    _context.UserAccounts.Add(new UserAccount
                    {
                        TenantId = _tenantId,
                        UserId = adminUser.Id,
                        UserName = AbpUserBase.AdminUserName,
                        EmailAddress = adminUser.EmailAddress
                    });
                    _context.SaveChanges();
                }

                //Notification subscription
                _context.NotificationSubscriptions.Add(new NotificationSubscriptionInfo(SequentialGuidGenerator.Instance.Create(), _tenantId, adminUser.Id, AppNotificationNames.NewUserRegistered));
                _context.SaveChanges();
            }
        }

        private void CreateProductTypes()
        {
            //1
            var Panels = _context.ProductTypes.FirstOrDefault(t => t.Name == "Panels");
            if (Panels == null)
            {
                _context.ProductTypes.Add(
              new ProductType
              {
                  Name = "Panels"
              });
            }
            //2
            var Inverters = _context.ProductTypes.FirstOrDefault(t => t.Name == "Inverters");
            if (Inverters == null)
            {
                _context.ProductTypes.Add(
              new ProductType
              {
                  Name = "Inverters"
              });
            }
        }

        private void CreateLeadSources()
        {
            //1
            var SocialMedia = _context.LeadSources.FirstOrDefault(t => t.Name == "Social Media");
            if (SocialMedia == null)
            {
                _context.LeadSources.Add(
              new LeadSources.LeadSource
              {
                  Name = "Social Media"
              });
            }

            //2
            var Email = _context.LeadSources.FirstOrDefault(t => t.Name == "Email");
            if (Email == null)
            {
                _context.LeadSources.Add(
              new LeadSources.LeadSource
              {
                  Name = "Email"
              });
            }

            //3
            var Facebook = _context.LeadSources.FirstOrDefault(t => t.Name == "Facebook");
            if (Facebook == null)
            {
                _context.LeadSources.Add(
              new LeadSources.LeadSource
              {
                  Name = "Facebook"
              });
            }
        }
        private static List<PVDStatus> GetInitialPVDStatus()
        {
            return new List<PVDStatus> {
                    new PVDStatus{ Name="Approved"},
                    new PVDStatus{ Name="Compliance"},
                    new PVDStatus{ Name="Failed"},
                    new PVDStatus{ Name="New"},
                    new PVDStatus{ Name="Pending"},
                    new PVDStatus{ Name="Traded"}
      };


        }
        private void CreatePVDStatus()
        {


            foreach (var itemval in InitialPVDStatus)
            {
                var items = _context.PVDStatus.FirstOrDefault(t => t.Name == itemval.Name);
                if (items == null)
                {
                    _context.PVDStatus.Add(itemval);
                }
            }
            _context.SaveChanges();
        }

        private void CreateEmailTemplate()
        {
            var Quote = _context.EmailTemplates.FirstOrDefault(t => t.TemplateName == "Quote");
            if (Quote == null)
            {
                _context.EmailTemplates.Add(
              new EmailTemplates.EmailTemplate
              {
                  TemplateName = "Quote",
                  Body = "",
                  Subject = ""
              });
            }
        }

        private void CreateEmailProvider()
        {
            var Gmail = "Gmail";
            var GmailEmailProvider = _context.EmailProviders.FirstOrDefault(t => t.Name == Gmail);
            if (GmailEmailProvider == null)
            {
                _context.EmailProviders.Add(new EmailProviders
                {
                    Name = Gmail
                });
            }

            var Outlook = "Outlook";
            var OutlookEmailProvider = _context.EmailProviders.FirstOrDefault(t => t.Name == Outlook);
            if (OutlookEmailProvider == null)
            {
                _context.EmailProviders.Add(new EmailProviders
                {
                    Name = Outlook
                });
            }

        }
    }
}
