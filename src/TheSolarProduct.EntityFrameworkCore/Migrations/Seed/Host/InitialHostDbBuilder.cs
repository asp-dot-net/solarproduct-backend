﻿using System.Linq;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.Migrations.Seed.Host
{
    public class InitialHostDbBuilder
    {
        private readonly TheSolarProductDbContext _context;

        public InitialHostDbBuilder(TheSolarProductDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();

            _context.SaveChanges();
            //CreateState();
        }

        private void CreateState()
        {
            var ACT = _context.States.FirstOrDefault(t => t.Name == "ACT");
            if (ACT == null)
            {
                _context.States.Add(
              new States.State
              {
                  Name = "ACT"
              });
            }

            var NSW = _context.States.FirstOrDefault(t => t.Name == "NSW");
            if (NSW == null)
            {
                _context.States.Add(
              new States.State
              {
                  Name = "NSW"
              });
            }

            var NT = _context.States.FirstOrDefault(t => t.Name == "NT");
            if (NT == null)
            {
                _context.States.Add(
              new States.State
              {
                  Name = "NT"
              });
            }

            var QLD = _context.States.FirstOrDefault(t => t.Name == "QLD");
            if (QLD == null)
            {
                _context.States.Add(
              new States.State
              {
                  Name = "QLD"
              });
            }

            var SA = _context.States.FirstOrDefault(t => t.Name == "SA");
            if (SA == null)
            {
                _context.States.Add(
              new States.State
              {
                  Name = "SA"
              });
            }

            var TAS = _context.States.FirstOrDefault(t => t.Name == "TAS");
            if (TAS == null)
            {
                _context.States.Add(
              new States.State
              {
                  Name = "TAS"
              });
            }

            var VIC = _context.States.FirstOrDefault(t => t.Name == "VIC");
            if (VIC == null)
            {
                _context.States.Add(
              new States.State
              {
                  Name = "VIC"
              });
            }

            var WA = _context.States.FirstOrDefault(t => t.Name == "WA");
            if (WA == null)
            {
                _context.States.Add(
              new States.State
              {
                  Name = "WA"
              });
            }

        }

    }
}
