﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection.Metadata;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Security.Cryptography.Xml;
using Abp;
using Abp.Authorization.Roles;
using Abp.MultiTenancy;
using Castle.Core;
using Castle.MicroKernel.Registration;
using IdentityServer4.Models;
using Microsoft.Azure.KeyVault.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyModel;
using NUglify.JavaScript.Syntax;
using Org.BouncyCastle.Cms;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Ocsp;
using Org.BouncyCastle.Utilities;
using Org.BouncyCastle.Utilities.IO;
using PayPalCheckoutSdk.Orders;
using RestSharp;
using Stripe;
using Stripe.Terminal;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.BonusList;
using TheSolarProduct.CallHistory;
using TheSolarProduct.CheckActives;
using TheSolarProduct.CheckApplications;
using TheSolarProduct.CheckDepositReceived;
using TheSolarProduct.CommmonActivityLogs;
using TheSolarProduct.DashboardCustomization;
using TheSolarProduct.DataVaults;
using TheSolarProduct.Editions;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.InstallationCost.BatteryInstallationCost;
using TheSolarProduct.InstallationCost.STCCost;
using TheSolarProduct.InstallationCost.TravelCost;
using TheSolarProduct.Jobs;
using TheSolarProduct.Leads;
using TheSolarProduct.LeadSources;
using TheSolarProduct.Promotions;
using TheSolarProduct.QuickStocks;
using TheSolarProduct.Quotations;
using TheSolarProduct.TenantDashboard.Dto;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.VppConections;
using TheSolarProduct.WholeSalePromotions;
using TheSolarProduct.Wholesales.DataVault;
using TheSolarProduct.WholeSaleStatuses;
using Twilio.TwiML.Voice;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using static TheSolarProduct.TheSolarProductDashboardCustomizationConsts;

namespace TheSolarProduct.Migrations.Seed.DefaultCommonValues
{
    public class DefaultCommonValuesBuilder
    {
        private readonly TheSolarProductDbContext _context;

        public DefaultCommonValuesBuilder(TheSolarProductDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            //CreateLeadSources();
            //CreatePromotionResponseStatus();
            //CreatePromotionTypes();
            CreateTemplateType();
            CreateCallType();
            CreateSTCCost();
            CreateBatteryInstallationCost();
            //CreateTravelCost();
            CreateVppConnection();
            CreateDataVaultSection();
            CreateQuickStockSection();
            CreateWholeSaleDataVaultSection();
            CreateCommonSection();
            CreateCheckActive();
            CreateCheckApplication();
            CreateCheckDepositReceived();
        }

        private void CreateTemplateType()
        {
            var Quotation = "Quotation";
            var QuotationTemplateType = _context.TemplateTypes.FirstOrDefault(e => e.Name == Quotation);
            if (QuotationTemplateType == null)
            {
                _context.TemplateTypes.Add(new TemplateType
                {
                    Name = Quotation
                });
            }

            var Email = "Email";
            var EmailTemplateType = _context.TemplateTypes.FirstOrDefault(e => e.Name == Email);
            if (EmailTemplateType == null)
            {
                _context.TemplateTypes.Add(new TemplateType
                {
                    Name = Email
                });
            }

            var TaxInvoice = "Tax Invoice";
            var TaxInvoiceTemplateType = _context.TemplateTypes.FirstOrDefault(e => e.Name == TaxInvoice);
            if (TaxInvoiceTemplateType == null)
            {
                _context.TemplateTypes.Add(new TemplateType
                {
                    Name = TaxInvoice
                });
            }

            var PaymentReceipt = "Payment Receipt";
            var PaymentReceiptTemplateType = _context.TemplateTypes.FirstOrDefault(e => e.Name == PaymentReceipt);
            if (PaymentReceiptTemplateType == null)
            {
                _context.TemplateTypes.Add(new TemplateType
                {
                    Name = PaymentReceipt
                });
            }

            var Picklist = "Picklist";
            var PicklistTemplateType = _context.TemplateTypes.FirstOrDefault(e => e.Name == Picklist);
            if (PicklistTemplateType == null)
            {
                _context.TemplateTypes.Add(new TemplateType
                {
                    Name = Picklist
                });
            }

            var ExportControlForm = "Export Control Form";
            var ExportControlFormTemplateType = _context.TemplateTypes.FirstOrDefault(e => e.Name == ExportControlForm);
            if (ExportControlFormTemplateType == null)
            {
                _context.TemplateTypes.Add(new TemplateType
                {
                    Name = ExportControlForm
                });
            }

            var FeedInTariffForm = "Feed In Tariff Form";
            var FeedInTariffFormTemplateType = _context.TemplateTypes.FirstOrDefault(e => e.Name == FeedInTariffForm);
            if (ExportControlFormTemplateType == null)
            {
                _context.TemplateTypes.Add(new TemplateType
                {
                    Name = FeedInTariffForm
                });
            }

            var RefundForm = "Refund Form";
            var RefundFormTemplateType = _context.TemplateTypes.FirstOrDefault(e => e.Name == RefundForm);
            if (RefundFormTemplateType == null)
            {
                _context.TemplateTypes.Add(new TemplateType
                {
                    Name = RefundForm
                });
            }

            var ReferralForm = "Referral Form";
            var ReferralFormTemplateType = _context.TemplateTypes.FirstOrDefault(e => e.Name == ReferralForm);
            if (ReferralFormTemplateType == null)
            {
                _context.TemplateTypes.Add(new TemplateType
                {
                    Name = ReferralForm
                });
            }

            var EmailWithRebate = "Email With Rebate";
            var EmailWithRebateTemplateType = _context.TemplateTypes.FirstOrDefault(e => e.Name == EmailWithRebate);
            if (EmailWithRebateTemplateType == null)
            {
                _context.TemplateTypes.Add(new TemplateType
                {
                    Name = EmailWithRebate
                });
            }

            var ShadingDeclarationForm = "Shading Declaration Form";
            var ShadingDeclarationFormTemplateType = _context.TemplateTypes.FirstOrDefault(e => e.Name == ShadingDeclarationForm);
            if (ShadingDeclarationFormTemplateType == null)
            {
                _context.TemplateTypes.Add(new TemplateType
                {
                    Name = ShadingDeclarationForm
                });
            }

            var EfficiencyDeclarationForm = "Efficiency Declaration Form";
            var EfficiencyDeclarationFormTemplateType = _context.TemplateTypes.FirstOrDefault(e => e.Name == EfficiencyDeclarationForm);
            if (EfficiencyDeclarationFormTemplateType == null)
            {
                _context.TemplateTypes.Add(new TemplateType
                {
                    Name = EfficiencyDeclarationForm
                });
            }

            var POForm = "PO Form";
            var POFormTemplateType = _context.TemplateTypes.FirstOrDefault(e => e.Name == POForm);
            if (POFormTemplateType == null)
            {
                _context.TemplateTypes.Add(new TemplateType
                {
                    Name = POForm
                });
            }
        }

        private void CreateCallType()
        {
            var Inbound = "Inbound";
            var InboundCallType = _context.CallTypes.FirstOrDefault(e => e.Name == Inbound);
            if (InboundCallType == null)
            {
                _context.CallTypes.Add(new CallType
                {
                    Name = Inbound,
                    IconClass = "las la-headset",
                    ColorClass = "activity_transfer"
                });
            }

            var Outbound = "Outbound";
            var OutboundCallType = _context.CallTypes.FirstOrDefault(e => e.Name == Outbound);
            if (OutboundCallType == null)
            {
                _context.CallTypes.Add(new CallType
                {
                    Name = Outbound,
                    IconClass = "las la-phone-volume",
                    ColorClass = "activity_update"
                });
            }

            var Notanswered = "Notanswered";
            var NotansweredCallType = _context.CallTypes.FirstOrDefault(e => e.Name == Notanswered);
            if (NotansweredCallType == null)
            {
                _context.CallTypes.Add(new CallType
                {
                    Name = Notanswered,
                    IconClass = "las la-phone-slash",
                    ColorClass = "activity_exreminder"
                });
            }

            var Missed = "Missed";
            var MissedCallType = _context.CallTypes.FirstOrDefault(e => e.Name == Missed);
            if (MissedCallType == null)
            {
                _context.CallTypes.Add(new CallType
                {
                    Name = Missed,
                    IconClass = "las la-phone-slash",
                    ColorClass = "activity_sms"
                });
            }
        }

        private void CreateSTCCost()
        {
            decimal cost = 43.5m;
            var StcCost = _context.STCCosts.FirstOrDefault(e => e.Cost == cost);
            if (StcCost == null)
            {
                _context.STCCosts.Add(new STCCost
                {
                    Cost = cost
                });
            }
        }

        private void CreateBatteryInstallationCost()
        {
            var batteryInstallationCost = _context.BatteryInstallationCosts.FirstOrDefault(e => e.FixCost == 1500);
            if (batteryInstallationCost == null)
            {
                _context.BatteryInstallationCosts.Add(new BatteryInstallationCost
                {
                    FixCost = 1500,
                    Kw = 6,
                    ExtraCost = 70
                });
            }
        }

        private void CreateVppConnection()
        {
            var originVPP = _context.VppConections.FirstOrDefault(e => e.Name == "Origin VPP");
            if (originVPP == null)
            {
                _context.VppConections.Add(new VppConection
                {
                    Name = "Origin VPP"
                });
            }
        }

        private void CreateDataVaultSection()
        {
            var LeadExpense = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Lead Expense");
            if (LeadExpense == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Lead Expense"
                });
            }

            var LeadSource = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Lead Source");
            if (LeadSource == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Lead Source"
                });
            }


            var Teams = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Teams");
            if (Teams == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Teams"
                });
            }

            var EmployeeCategory = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Employee Category");
            if (EmployeeCategory == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Employee Category"
                });
            }

            var CancelReasons = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Cancel Reasons");
            if (CancelReasons == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Cancel Reasons"
                });
            }

            var RejectReasons = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Reject Reasons");
            if (RejectReasons == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Reject Reasons"
                });
            }

            var HoldReasons = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Hold Reasons");
            if (HoldReasons == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Hold Reasons"
                });
            }

            var RefundReasons = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Refund Reasons");
            if (RefundReasons == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Refund Reasons"
                });
            }

            var JobCancellationReasons = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Job Cancellation Reasons");
            if (JobCancellationReasons == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Job Cancellation Reasons"
                });
            }

            var Electricitydistributors = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Electricity Distributors");
            if (Electricitydistributors == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Electricity Distributors"
                });
            }

            var ElectricityRetailers = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Electricity Retailers");
            if (ElectricityRetailers == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Electricity Retailers"
                });
            }

            var Jobtypes = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Job Types");
            if (Jobtypes == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Job Types"
                });
            }

            var JobStatus = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Job Status");
            if (JobStatus == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Job Status"
                });
            }

            var ProductTypes = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Product Types");
            if (ProductTypes == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Product Types"
                });
            }

            var ProductItems = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Product Items");
            if (ProductItems == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Product Items"
                });
            }

            var HouseTypes = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "House Types");
            if (HouseTypes == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "House Types"
                });
            }

            var RoofTypes = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Roof Types");
            if (RoofTypes == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Roof Types"
                });
            }

            var RoofAngles = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Roof Angles");
            if (RoofAngles == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "JobRoof Angles"
                });
            }

            var PaymentMethods = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Payment Methods");
            if (PaymentMethods == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Payment Methods"
                });
            }

            var PaymentOptions = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Payment Options");
            if (PaymentOptions == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Payment Options"
                });
            }

            var PaymentTerms = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Payment Terms");
            if (PaymentTerms == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Payment Terms"
                });
            }

            var PromotionType = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Promotion Type");
            if (PromotionType == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Promotion Type"
                });
            }

            var PriceVariations = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Price Variations");
            if (PriceVariations == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Price Variations"
                });
            }

            var InvoicePaymentMethods = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Invoice Payment Methods");
            if (InvoicePaymentMethods == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Invoice Payment Methods"
                });
            }

            var InvoiceStatuses = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Invoice Statuses");
            if (InvoiceStatuses == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Invoice Statuses"
                });
            }

            var EmailTemplate = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Email Templates");
            if (EmailTemplate == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Email Templates"
                });
            }

            var SMSTemplates = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "SMS Templates");
            if (SMSTemplates == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "SMS Templates"
                });
            }

            var FreebieTransports = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Freebie Transports");
            if (FreebieTransports == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Freebie Transports"
                });
            }

            var DocumentType = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Document Type");
            if (DocumentType == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Document Type"
                });
            }

            var DocumentLibrary = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Document Library");
            if (DocumentLibrary == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Document Library"
                });
            }

            var StcStatus = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Stc Status");
            if (StcStatus == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Stc Status"
                });
            }

            var ServiceCategory = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Service Category");
            if (ServiceCategory == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Service Category"
                });
            }

            var ServiceSources = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Service Sources");
            if (ServiceSources == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Service Sources"
                });
            }

            var ServiceStatus = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Service Status");
            if (ServiceStatus == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Service Status"
                });
            }

            var ServiceType = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Service Type");
            if (ServiceType == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Service Type"
                });
            }

            var ServiceSubCategory = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Service SubCategory");
            if (ServiceSubCategory == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Service SubCategory"
                });
            }

            var ReviewType = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Review Type");
            if (ReviewType == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Review Type"
                });
            }

            var PriceItem = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Price Item");
            if (PriceItem == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Price Item"
                });
            }

            var HtmlTemplate = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Html Template");
            if (HtmlTemplate == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Html Template"
                });
            }

            var PostCodeRange = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Post Code Range");
            if (PostCodeRange == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Post Code Range"
                });
            }

            var WarehouseLocation = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Warehouse Location");
            if (WarehouseLocation == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Warehouse Location"
                });
            }

            var ProductPackages = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Product Packages");
            if (ProductPackages == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Product Packages"
                });
            }

            var CallFlowQueue = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Call Flow Queue");
            if (CallFlowQueue == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Call Flow Queue"
                });
            }

            var CommissionRange = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Commission Range");
            if (CommissionRange == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Commission Range"
                });
            }

            var DashboardMessage = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Dashboard Message");
            if (DashboardMessage == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Dashboard Message"
                });
            }

            var OtherCharges = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Other Charges");
            if (OtherCharges == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Other Charges"
                });
            }

            var StateWiseCost = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "State Wise Cost");
            if (StateWiseCost == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "State Wise Cost"
                });
            }

            var InstallationItemList = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Installation Item List");
            if (InstallationItemList == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Installation Item List"
                });
            }

            var FixedCostPrice = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Fixed Cost Price");
            if (FixedCostPrice == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Fixed Cost Price"
                });
            }

            var ExtraInsCharges = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Extra Ins Charges");
            if (ExtraInsCharges == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Extra Ins Charges"
                });
            }

            var STCCost = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "STC Cost");
            if (STCCost == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "STC Cost"
                });
            }

            var BatteryInstallationCost = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Battery Installation Cost");
            if (BatteryInstallationCost == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Battery Installation Cost"
                });
            }

            var CategoryInstallationItem = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Category Installation Item");
            if (CategoryInstallationItem == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Category Installation Item"
                });
            }

            var PostCodePrice = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Post Code Price");
            if (PostCodePrice == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Post Code Price"
                });
            }

            var ServiceDocumentType = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Service Document Type");
            if (ServiceDocumentType == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Service Document Type"
                });
            }

            var ChcekApplication = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Check Application");
            if (ChcekApplication == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Check Application"
                });
            }

            var checkDepositeReceived = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Check Deposite Received");
            if (checkDepositeReceived == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Check Deposite Received"
                });
            }

            var checkActive = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Check Active");
            if (checkActive == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Check Active"
                });
            }

            var PurchaseCompany = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Purchase Company");
            if (PurchaseCompany == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Purchase Company"
                });
            }

            var TransportCompany = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Transport Company");
            if (TransportCompany == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Transport Company"
                });
            }

            var StockOrderStatus = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Stock Order Status");
            if (StockOrderStatus == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Stock Order Status"
                });
            }

            var DeliveryType = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Delivery Type");
            if (DeliveryType == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Delivery Type"
                });
            }

            var PaymentStatus = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Payment Status");
            if (PaymentStatus == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Payment Status"
                });
            }

            var PaymentMethod = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Payment Method");
            if (PaymentMethod == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Payment Method"
                });
            }

            var PaymentType = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Payment Type");
            if (PaymentType == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Payment Type"
                });
            }

            var Currency = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Currency");
            if (Currency == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Currency"
                });
            }

            var FreightCompany = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Freight Company");
            if (FreightCompany == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Freight Company"
                });
            }

            var StockFrom = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Stock From");
            if (StockFrom == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Stock From"
                });
            }

            var OrderFor = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Stock Order For");
            if (OrderFor == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Stock Order For"
                });
            }

            var PurchaseDocumentList = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Purchase Document List");
            if (PurchaseDocumentList == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Purchase Document List"
                });
            }

            var Vendor = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Vendor");
            if (Vendor == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Vendor"
                });
            }
            var IncoTerm = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "IncoTerm");
            if (IncoTerm == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "IncoTerm"
                });
            }
            var StreetType = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Street Type");
            if (StreetType == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Street Type"
                });
            }
            var StreetName = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Street Name");
            if (StreetType == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Street Name"
                });
            }
            var UnitType = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Unit Type");
            if (UnitType == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Unit Type"
                });
            }

            var JobCostFixExpence = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Job Cost Fix Expence");
            if (JobCostFixExpence == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Job Cost Fix Expence"
                });
            }

            var Variation = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "Variation");
            if (Variation == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "Variation"
                });
            }

            var State = _context.DataVaultSections.FirstOrDefault(e => e.SectionName == "State");
            if (State == null)
            {
                _context.DataVaultSections.Add(new DataVaultSection
                {
                    SectionName = "State"
                });
            }

        }

        private void CreateQuickStockSection()
        {
            var PurchaserOrder = _context.QuickStockSections.FirstOrDefault(e => e.SectionName == "Purchase Order");
            if (PurchaserOrder == null)
            {
                _context.QuickStockSections.Add(new QuickStockSection
                {
                    SectionName = "Purchase Order"
                });
            }

            var StockTransfer = _context.QuickStockSections.FirstOrDefault(e => e.SectionName == "Stock Transfer");
            if (StockTransfer == null)
            {
                _context.QuickStockSections.Add(new QuickStockSection
                {
                    SectionName = "Stock Transfer"
                });
            }
        }

        private void CreateWholeSaleDataVaultSection()
        {
            var WholeSaleStatus = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "WholeSale Status");
            if (WholeSaleStatus == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "WholeSale Status"
                });
            }
            var DocumentType = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Document Type");
            if (DocumentType == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Document Type"
                });
            }
            var SmsTemplate = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "SMS Template");
            if (SmsTemplate == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "SMS Template"
                });
            }
            var EmailTemplate = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Email Template");
            if (EmailTemplate == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Email Template"
                });
            }
            var InvoiceType = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Invoice Type");
            if (InvoiceType == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Invoice Type"
                });
            }
            var DeliveryOption = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Delivery Option");
            if (DeliveryOption == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Delivery Option"
                });
            }
            var TransportType = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Transport Type");
            if (TransportType == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Transport Type"
                });
            }
            var JobType = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Job Type");
            if (JobType == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Job Type"
                });
            }
            var PropertyType = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Property Type");
            if (PropertyType == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Property Type"
                });
            }
            var PvdStatus = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Pvd Status");
            if (PvdStatus == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Pvd Status"
                });
            }
            var Series = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Series");
            if (Series == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Series"
                });
            }
            var Slider = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Slider");
            if (Slider == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Slider"
                });
            }
            var SpecialOffer = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Special Offer");
            if (SpecialOffer == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Special Offer"
                });
            }
            var BrandingPartner = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Branding Partner");
            if (BrandingPartner == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Branding Partner"
                });
            }
            var UserRequest = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "User Request");
            if (UserRequest == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "User Request"
                });
            }
            var SolarPackage = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Solar Package");
            if (SolarPackage == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Solar Package"
                });
            }
            var Subcribers = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Subcribers");
            if (Subcribers == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Subcribers"
                });
            }
            var Products = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Products");
            if (Products == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Products"
                });
            }

            var ProductTypes = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Product Type");
            if (ProductTypes == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Product Type"
                });
            }

            var jobStatus = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Job Status");
            if (jobStatus == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Job Status"
                });
            }

            var specification = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "Specification");
            if (specification == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "Specification"
                });
            }

            var STCRegister = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "STC Register");
            if (STCRegister == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "STC Register"
                });
            }

            var STCDealPrice = _context.WholeSaleDataVaultSections.FirstOrDefault(e => e.SectionName == "STC Deal Price");
            if (STCDealPrice == null)
            {
                _context.WholeSaleDataVaultSections.Add(new WholeSaleDataVaultSection
                {
                    SectionName = "STC Deal Price"
                });
            }
        }

        private void CreateCommonSection()
        {
            var Login = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Login");
            if (Login == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Login"
                });
            }

            var Dashboard = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Dashboard");
            if (Dashboard == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Dashboard"
                });
            }

            var DashboardSales = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Dashboard Sales");
            if (DashboardSales == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Dashboard Sales"
                });
            }

            var DashboardInvoice = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Dashboard Invoice");
            if (DashboardInvoice == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Dashboard Invoice"
                });
            }

            var DashboardService = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Dashboard Service");
            if (DashboardService == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Dashboard Service"
                });
            }

            var DataVaultActivity = _context.CommonSections.FirstOrDefault(e => e.SectionName == "DataVault Activity");
            if (DataVaultActivity == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "DataVault Activity"
                });
            }

            var CallFlowQueue = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Call Flow Queue");
            if (CallFlowQueue == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Call Flow Queue"
                });
            }

            var CancelReason = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Cancel Reason");
            if (CancelReason == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Cancel Reason"
                });
            }

            var CheckActive = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Check Active");
            if (CheckActive == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Check Active"
                });
            }

            var CheckApplication = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Check Application");
            if (CheckApplication == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Check Application"
                });
            }

            var CheckDepositReceived = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Check Deposit Received");
            if (CheckDepositReceived == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Check Deposit Received"
                });
            }

            var CommissionRange = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Commission Range");
            if (CommissionRange == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Commission Range"
                });
            }

            var DashboardMessage = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Dashboard Message");
            if (DashboardMessage == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Dashboard Message"
                });
            }

            var DocumentLibrary = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Document Library");
            if (DocumentLibrary == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Document Library"
                });
            }

            var DocumentType = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Document Type");
            if (DocumentType == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Document Type"
                });
            }

            var ElectricityDistributor = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Electricity Distributor");
            if (ElectricityDistributor == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Electricity Distributor"
                });
            }

            var ElectricityRetailer = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Electricity Retailer");
            if (ElectricityRetailer == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Electricity Retailer"
                });
            }

            var EmailTemplates = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Email Templates");
            if (EmailTemplates == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Email Templates"
                });
            }

            var EmployeeCategories = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Employee Categories");
            if (EmployeeCategories == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Employee Categories"
                });
            }

            var FreebieTransports = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Freebie Transports");
            if (FreebieTransports == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Freebie Transports"
                });
            }

            var HoldReasons = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Hold Reasons");
            if (HoldReasons == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Hold Reasons"
                });
            }

            var HouseTypes = _context.CommonSections.FirstOrDefault(e => e.SectionName == "House Types");
            if (HouseTypes == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "House Types"
                });
            }

            var HtmlTemplates = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Html Templates");
            if (HtmlTemplates == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Html Templates"
                });
            }

            var BatteryInstallationCost = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Battery Installation Cost");
            if (BatteryInstallationCost == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Battery Installation Cost"
                });
            }

            var CategoryInstallationItem = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Category Installation Item");
            if (CategoryInstallationItem == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Category Installation Item"
                });
            }

            var ExtraInstallationCharges = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Extra Installation Charges");
            if (ExtraInstallationCharges == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Extra Installation Charges"
                });
            }

            var FixedCostPrice = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Fixed Cost Price");
            if (FixedCostPrice == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Fixed Cost Price"
                });
            }

            var InstallationProductItemList = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Installation Product Item List");
            if (InstallationProductItemList == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Installation Product Item List"
                });
            }

            var OtherCharges = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Other Charges");
            if (OtherCharges == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Other Charges"
                });
            }

            var PostCodePrice = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Post Code Price");
            if (PostCodePrice == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Post Code Price"
                });
            }

            var StateWiseCost = _context.CommonSections.FirstOrDefault(e => e.SectionName == "State Wise Cost");
            if (StateWiseCost == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "State Wise Cost"
                });
            }

            var STCCost = _context.CommonSections.FirstOrDefault(e => e.SectionName == "STC Cost");
            if (STCCost == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "STC Cost"
                });
            }

            var InvoicePayments = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Invoice Payments");
            if (InvoicePayments == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Invoice Payments"
                });
            }

            var InvoiceStatus = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Invoice Status");
            if (InvoiceStatus == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Invoice Status"
                });
            }

            var JobCancellation = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Job Cancellation");
            if (JobCancellation == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Job Cancellation"
                });
            }

            var JobStatus = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Job Status");
            if (JobStatus == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Job Status"
                });
            }

            var JobType = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Job Type");
            if (JobType == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Job Type"
                });
            }

            var LeadExpenses = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Lead Expenses");
            if (LeadExpenses == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Lead Expenses"
                });
            }

            var LeadSources = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Lead Sources");
            if (LeadSources == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Lead Sources"
                });
            }

            var PaymentMethod = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Payment Method");
            if (PaymentMethod == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Payment Method"
                });
            }

            var PaymentOptions = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Payment Options");
            if (PaymentOptions == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Payment Options"
                });
            }

            var PaymentTerm = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Payment Term");
            if (PaymentTerm == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Payment Term"
                });
            }

            var PostCodes = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Post Codes");
            if (PostCodes == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Post Codes"
                });
            }

            var PostCodeRange = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Post Code Range");
            if (PostCodeRange == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Post Code Range"
                });
            }

            var PostalTypes = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Postal Types");
            if (PostalTypes == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Postal Types"
                });
            }

            var PriceItem = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Price Item");
            if (PriceItem == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Price Item"
                });
            }

            var PriceVariations = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Price Variations");
            if (PriceVariations == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Price Variations"
                });
            }

            var ProductItems = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Product Items");
            if (ProductItems == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Product Items"
                });
            }

            var ProductPackages = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Product Packages");
            if (ProductPackages == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Product Packages"
                });
            }

            var ProductType = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Product Type");
            if (ProductType == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Product Type"
                });
            }

            var PromotionType = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Promotion Type");
            if (PromotionType == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Promotion Type"
                });
            }

            var RefundReason = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Refund Reason");
            if (RefundReason == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Refund Reason"
                });
            }

            var RejectReason = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Reject Reason");
            if (RejectReason == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Reject Reason"
                });
            }

            var ReviewType = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Review Type");
            if (ReviewType == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Review Type"
                });
            }

            var RoofAngle = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Roof Angle");
            if (RoofAngle == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Roof Angle"
                });
            }

            var RoofTypes = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Roof Types");
            if (RoofTypes == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Roof Types"
                });
            }

            var SalesTeams = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Sales Teams");
            if (SalesTeams == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Sales Teams"
                });
            }

            var ServiceCategory = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Service Category");
            if (ServiceCategory == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Service Category"
                });
            }

            var ServiceDocumentType = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Service Document Type");
            if (ServiceDocumentType == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Service Document Type"
                });
            }

            var ServiceSources = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Service Sources");
            if (ServiceSources == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Service Sources"
                });
            }

            var ServiceStatus = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Service Status");
            if (ServiceStatus == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Service Status"
                });
            }

            var ServiceSubCategory = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Service SubCategory");
            if (ServiceSubCategory == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Service SubCategory"
                });
            }

            var ServiceType = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Service Type");
            if (ServiceType == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Service Type"
                });
            }

            var SMSTemplates = _context.CommonSections.FirstOrDefault(e => e.SectionName == "SMS Templates");
            if (SMSTemplates == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "SMS Templates"
                });
            }

            var State = _context.CommonSections.FirstOrDefault(e => e.SectionName == "State");
            if (State == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "State"
                });
            }

            var StcStatus = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Stc Status");
            if (StcStatus == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Stc Status"
                });
            }

            var StreetNames = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Street Names");
            if (StreetNames == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Street Names"
                });
            }

            var StreetTypes = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Street Types");
            if (StreetTypes == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Street Types"
                });
            }

            var UnitTypes = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Unit Types");
            if (UnitTypes == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Unit Types"
                });
            }

            var WarehouseLocation = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Warehouse Location");
            if (WarehouseLocation == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Warehouse Location"
                });
            }

            var ManageLeads = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Manage Leads");
            if (ManageLeads == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Manage Leads"
                });
            }

            var DuplicateLead = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Duplicate Lead");
            if (DuplicateLead == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Duplicate Lead"
                });
            }

            var MyLead = _context.CommonSections.FirstOrDefault(e => e.SectionName == "My Lead");
            if (MyLead == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "My Lead"
                });
            }

            var LeadTracker = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Lead Tracker");
            if (LeadTracker == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Lead Tracker"
                });
            }

            var CancelLead = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Cancel Lead");
            if (CancelLead == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Cancel Lead"
                });
            }

            var RejectLead = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Reject Lead");
            if (RejectLead == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Reject Lead"
                });
            }

            var ClosedLead = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Closed Lead");
            if (ClosedLead == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Closed Lead"
                });
            }

            var _3rdPartyLeads = _context.CommonSections.FirstOrDefault(e => e.SectionName == "3rd Party Leads");
            if (_3rdPartyLeads == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "3rd Party Leads"
                });
            }

            var Promotions = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Promotions");
            if (Promotions == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Promotions"
                });
            }

            var PromotionsTracker = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Promotion Tracker");
            if (PromotionsTracker == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Promotion Tracker"
                });
            }

            var Essential = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Essential Tracker");
            if (Essential == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Essential Tracker"
                });
            }

            var Application = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Application Tracker");
            if (Application == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Application Tracker"
                });
            }

            var Freebies = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Freebies Tracker");
            if (Freebies == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Freebies Tracker"
                });
            }

            var Finance = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Finance Tracker");
            if (Finance == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Finance Tracker"
                });
            }

            var Refund = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Refund Tracker");
            if (Refund == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Refund Tracker"
                });
            }

            var Referral = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Referral Tracker");
            if (Referral == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Referral Tracker"
                });
            }

            var JobActive = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Job Active Tracker");
            if (JobActive == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Job Active Tracker"
                });
            }

            var GridConnect = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Grid Connect Tracker");
            if (GridConnect == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Grid Connect Tracker"
                });
            }

            var STC = _context.CommonSections.FirstOrDefault(e => e.SectionName == "STC Tracker");
            if (STC == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "STC Tracker"
                });
            }

            var Warranty = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Warranty Tracker");
            if (Warranty == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Warranty Tracker"
                });
            }

            var HoldJobs = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Hold Jobs Tracker");
            if (HoldJobs == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Hold Jobs Tracker"
                });
            }

            var TransportCost = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Transport Cost Tracker");
            if (TransportCost == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Transport Cost Tracker"
                });
            }

            var Jobs = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Jobs");
            if (Jobs == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Jobs"
                });
            }

            var Service = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Service");
            if (Service == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Service"
                });
            }

            var InvoiceIssued = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Invoice Issued");
            if (InvoiceIssued == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Invoice Issued"
                });
            }

            var InvoicePaid = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Invoice Paid");
            if (InvoicePaid == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Invoice Paid"
                });
            }

            var PayWay = _context.CommonSections.FirstOrDefault(e => e.SectionName == "PayWay");
            if (PayWay == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "PayWay"
                });
            }

            var InvoiceFileList = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Invoice File List");
            if (InvoiceFileList == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Invoice File List"
                });
            }

            var MyInstaller = _context.CommonSections.FirstOrDefault(e => e.SectionName == "My Installer");
            if (MyInstaller == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "My Installer"
                });
            }

            var JobAssign = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Job Assign");
            if (JobAssign == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Job Assign"
                });
            }

            var InstallationMap = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Installation Map");
            if (InstallationMap == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Installation Map"
                });
            }

            var JobBooking = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Job Booking");
            if (JobBooking == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Job Booking"
                });
            }

            var InstallationCalender = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Installation Calender");
            if (InstallationCalender == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Installation Calender"
                });
            }

            var Installation = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Installation");
            if (Installation == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Installation"
                });
            }

            var NewInstallerInvoice = _context.CommonSections.FirstOrDefault(e => e.SectionName == "New Installer Invoice");
            if (NewInstallerInvoice == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "New Installer Invoice"
                });
            }

            var PendingInstallerInvoice = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Pending Installer Invoice");
            if (PendingInstallerInvoice == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Pending Installer Invoice"
                });
            }

            var ReadyToPay = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Ready To Pay");
            if (ReadyToPay == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Ready To Pay"
                });
            }

            var MyLeadsGeneration = _context.CommonSections.FirstOrDefault(e => e.SectionName == "My Leads Generation");
            if (MyLeadsGeneration == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "My Leads Generation"
                });
            }

            var LeadsGenerationInstallation = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Leads Generation Installation");
            if (LeadsGenerationInstallation == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Leads Generation Installation"
                });
            }

            var LeadGenerationMap = _context.CommonSections.FirstOrDefault(e => e.SectionName == "LeadGeneration Map");
            if (LeadGenerationMap == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "LeadGeneration Map"
                });
            }

            var Commission = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Commission");
            if (Commission == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Commission"
                });
            }

            var ManageService = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Manage Service");
            if (ManageService == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Manage Service"
                });
            }

            var MyService = _context.CommonSections.FirstOrDefault(e => e.SectionName == "My Service");
            if (MyService == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "My Service"
                });
            }

            var ServiceMap = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Service Map");
            if (ServiceMap == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Service Map"
                });
            }

            var ServiceInstallation = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Service Installation");
            if (ServiceInstallation == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Service Installation"
                });
            }

            var WarrentyClaim = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Warrenty Claim");
            if (WarrentyClaim == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Warrenty Claim"
                });
            }

            var ServiceInvoice = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Service Invoice");
            if (ServiceInvoice == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Service Invoice"
                });
            }

            var Review = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Review");
            if (Review == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Review"
                });
            }

            var SMS = _context.CommonSections.FirstOrDefault(e => e.SectionName == "SMS");
            if (SMS == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "SMS"
                });
            }

            var Email = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Email");
            if (Email == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Email"
                });
            }

            var ActivityReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Activity Report");
            if (ActivityReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Activity Report"
                });
            }

            var ToDoActivityReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "ToDo Activity Report");
            if (ToDoActivityReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "ToDo Activity Report"
                });
            }

            var LeadExpenseReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Lead Expense Report");
            if (LeadExpenseReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Lead Expense Report"
                });
            }

            var OutStandingReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "OutStanding Report");
            if (OutStandingReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "OutStanding Report"
                });
            }

            var JobCostReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Job Cost Report");
            if (JobCostReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Job Cost Report"
                });
            }

            var LeadAssignReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Lead Assign Report");
            if (LeadAssignReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Lead Assign Report"
                });
            }

            var LeadSoldReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Lead Sold Report");
            if (LeadSoldReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Lead Sold Report"
                });
            }

            var EmployeeJobCostReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Employee Job Cost Report");
            if (EmployeeJobCostReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Employee Job Cost Report"
                });
            }

            var JobCommissionReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Job Commission Report");
            if (JobCommissionReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Job Commission Report"
                });
            }

            var JobCommissionPaidReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Job Commission Paid Report");
            if (JobCommissionPaidReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Job Commission Paid Report"
                });
            }

            var ProductSoldReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Product Sold Report");
            if (ProductSoldReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Product Sold Report"
                });
            }

            var SMSCountReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "SMS Count Report");
            if (SMSCountReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "SMS Count Report"
                });
            }

            var UserDetailReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "User Detail Report");
            if (UserDetailReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "User Detail Report"
                });
            }

            var ComparisonReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Comparison Report");
            if (ComparisonReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Comparison Report"
                });
            }

            var LeadComparisonReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Lead Comparison Report");
            if (LeadComparisonReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Lead Comparison Report"
                });
            }

            var MonthlyComparisonReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Monthly Comparison Report");
            if (MonthlyComparisonReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Monthly Comparison Report"
                });
            }

            var InstallerPaymentReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Installer Payment Report");
            if (InstallerPaymentReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Installer Payment Report"
                });
            }

            var InvoicePaymentReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Invoice Payment Report");
            if (InvoicePaymentReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Invoice Payment Report"
                });
            }

            var JobCostMonthWiseReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Job Cost Month Wise Report");
            if (JobCostMonthWiseReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Job Cost Month Wise Report"
                });
            }

            var JobVariationReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Job Variation Report");
            if (JobVariationReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Job Variation Report"
                });
            }

            var RescheduleInstallationReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Reschedule Installation Report");
            if (RescheduleInstallationReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Reschedule Installation Report"
                });
            }

            var UserCallHistory = _context.CommonSections.FirstOrDefault(e => e.SectionName == "User Call History");
            if (UserCallHistory == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "User Call History"
                });
            }

            var StateWiseCallHistory = _context.CommonSections.FirstOrDefault(e => e.SectionName == "State Wise Call History");
            if (StateWiseCallHistory == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "State Wise Call History"
                });
            }

            var _3CXCallQueue = _context.CommonSections.FirstOrDefault(e => e.SectionName == "3CX Call Queue");
            if (_3CXCallQueue == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "3CX Call Queue"
                });
            }

            var _3CXCallQueueWeekly = _context.CommonSections.FirstOrDefault(e => e.SectionName == "3CX Call Queue Weekly");
            if (_3CXCallQueueWeekly == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "3CX Call Queue Weekly"
                });
            }

            var _3CXCallQueueUserWise = _context.CommonSections.FirstOrDefault(e => e.SectionName == "3CX Call Queue User Wise");
            if (_3CXCallQueueUserWise == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "3CX Call Queue User Wise"
                });
            }

            var WholesaleDashboard = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Dashboard");
            if (WholesaleDashboard == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Dashboard"
                });
            }

            var WholesaleDataVaultsActivity = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Data Vaults Activity");
            if (WholesaleDataVaultsActivity == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Data Vaults Activity"
                });
            }

            var WholesaleStatus = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Status");
            if (WholesaleStatus == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Status"
                });
            }

            var WholesaleInvoiceType = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Invoice Type");
            if (WholesaleInvoiceType == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Invoice Type"
                });
            }

            var WholesaleDeliveryOptions = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Delivery Options");
            if (WholesaleDeliveryOptions == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Delivery Options"
                });
            }

            var WholesaleJobType = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Job Type");
            if (WholesaleJobType == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Job Type"
                });
            }

            var WholesaleTransportType = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Transport Type");
            if (WholesaleTransportType == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Transport Type"
                });
            }

            var WholesalePropertyType = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Property Type");
            if (WholesalePropertyType == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Property Type"
                });
            }

            var WholesalePVDStatus = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale PVD Status");
            if (WholesalePVDStatus == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale PVD Status"
                });
            }

            var WholesaleDocumentType = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Document Type");
            if (WholesaleDocumentType == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Document Type"
                });
            }

            var WholesaleSmsTemplate = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Sms Template");
            if (WholesaleSmsTemplate == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Sms Template"
                });
            }

            var WholesaleEmailTemplate = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Email Template");
            if (WholesaleEmailTemplate == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Email Template"
                });
            }

            var WholesaleSeries = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Series");
            if (WholesaleSeries == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Series"
                });
            }

            var WholesaleProductType = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Product Type");
            if (WholesaleProductType == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Product Type"
                });
            }

            var WholesaleJobStatus = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Job Status");
            if (WholesaleJobStatus == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Job Status"
                });
            }

            var ECommerceActivity = _context.CommonSections.FirstOrDefault(e => e.SectionName == "ECommerce Activity");
            if (ECommerceActivity == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "ECommerce Activity"
                });
            }

            var ECommerceProducts = _context.CommonSections.FirstOrDefault(e => e.SectionName == "ECommerce Products");
            if (ECommerceProducts == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "ECommerce Products"
                });
            }

            var SpecialOffers = _context.CommonSections.FirstOrDefault(e => e.SectionName == "ECommerce Special Offers");
            if (SpecialOffers == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "ECommerce Special Offers"
                });
            }

            var BrandingPartner = _context.CommonSections.FirstOrDefault(e => e.SectionName == "ECommerce Branding Partner");
            if (BrandingPartner == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "ECommerce Branding Partner"
                });
            }

            var ECommerceUserRequest = _context.CommonSections.FirstOrDefault(e => e.SectionName == "ECommerce User Request");
            if (ECommerceUserRequest == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "ECommerce User Request"
                });
            }

            var SolarPackage = _context.CommonSections.FirstOrDefault(e => e.SectionName == "ECommerce Solar Package");
            if (SolarPackage == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "ECommerce Solar Package"
                });
            }

            var Subcriber = _context.CommonSections.FirstOrDefault(e => e.SectionName == "ECommerce Subcriber");
            if (Subcriber == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "ECommerce Subcriber"
                });
            }

            var Specification = _context.CommonSections.FirstOrDefault(e => e.SectionName == "ECommerce Specification");
            if (Specification == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "ECommerce Specification"
                });
            }

            var WholesalePromotions = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Promotions");
            if (WholesalePromotions == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Promotions"
                });
            }

            var WholesalePromotionTracker = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Promotion Tracker");
            if (WholesalePromotionTracker == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Promotion Tracker"
                });
            }

            var WholesaleLeads = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Leads");
            if (WholesaleLeads == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Leads"
                });
            }

            var RequestToTransfer = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Leads Request To Transfer");
            if (RequestToTransfer == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Leads Request To Transfer"
                });
            }

            var WholesaleInvoice = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale Invoice");
            if (WholesaleInvoice == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale Invoice"
                });
            }

            var CreditApplications = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Credit Applications");
            if (CreditApplications == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Credit Applications"
                });
            }

            var ContactUs = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Contact Us");
            if (ContactUs == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Contact Us"
                });
            }

            var WholesaleSMS = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Wholesale SMS");
            if (WholesaleSMS == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Wholesale SMS"
                });
            }

            var InventoryCurrency = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Inventory Currency");
            if (InventoryCurrency == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Inventory Currency"
                });
            }

            var InventoryDeliveryType = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Inventory Delivery Type");
            if (InventoryDeliveryType == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Inventory Delivery Type"
                });
            }

            var InventoryFreightCompany = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Inventory Freight Company");
            if (InventoryFreightCompany == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Inventory Freight Company"
                });
            }

            var InventoryIncoTerm = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Inventory Inco Term");
            if (InventoryIncoTerm == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Inventory Inco Term"
                });
            }

            var InventoryPaymentMethod = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Inventory Payment Method");
            if (InventoryPaymentMethod == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Inventory Payment Method"
                });
            }

            var InventoryPaymentStatus = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Inventory Payment Status");
            if (InventoryPaymentStatus == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Inventory Payment Status"
                });
            }

            var InventoryPaymentType = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Inventory Payment Type");
            if (InventoryPaymentType == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Inventory Payment Type"
                });
            }

            var InventoryPurchaseCompany = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Inventory Purchase Company");
            if (InventoryPurchaseCompany == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Inventory Purchase Company"
                });
            }

            var InventoryPurchaseDocumentList = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Inventory Purchase Document List");
            if (InventoryPurchaseDocumentList == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Inventory Purchase Document List"
                });
            }

            var InventoryStockFrom = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Inventory Stock From");
            if (InventoryStockFrom == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Inventory Stock From"
                });
            }

            var InventoryStockOrderFor = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Inventory Stock Order For");
            if (InventoryStockOrderFor == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Inventory Stock Order For"
                });
            }

            var InventoryStockOrderStatus = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Inventory Stock Order Status");
            if (InventoryStockOrderStatus == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Inventory Stock Order Status"
                });
            }

            var InventoryTransportCompany = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Inventory Transport Company");
            if (InventoryTransportCompany == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Inventory Transport Company"
                });
            }

            var InventoryVendors = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Inventory Vendors");
            if (InventoryVendors == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Inventory Vendors"
                });
            }

            var StockOrder = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Stock Order");
            if (StockOrder == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Stock Order"
                });
            }

            var StockTransfer = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Stock Transfer");
            if (StockTransfer == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Stock Transfer"
                });
            }

            var Organization = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Organization");
            if (Organization == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Organization"
                });
            }

            var Roles = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Roles");
            if (Roles == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Roles"
                });
            }

            var Users = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Users");
            if (Users == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Users"
                });
            }

            var Installer = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Installer");
            if (Installer == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Installer"
                });
            }

            var SalesRep = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Sales Rep");
            if (SalesRep == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Sales Rep"
                });
            }

            var SalesManager = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Sales Manager");
            if (SalesManager == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Sales Manager"
                });
            }

            var AuditLogs = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Audit Logs");
            if (AuditLogs == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Audit Logs"
                });
            }

            var Maintenance = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Maintenance");
            if (Maintenance == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Maintenance"
                });
            }

            var HostSettings = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Host Settings");
            if (HostSettings == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Host Settings"
                });
            }

            var TenantSettings = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Tenant Settings");
            if (TenantSettings == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Tenant Settings"
                });
            }

            var ECommerceSlider = _context.CommonSections.FirstOrDefault(e => e.SectionName == "ECommerce Slider");
            if (ECommerceSlider == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "ECommerce Slider"
                });
            }

            var JobCostFixExpence = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Job Cost Fix Expence");
            if (JobCostFixExpence == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Job Cost Fix Expence"
                });
            }

            var StockPayment = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Stock Payment");
            if (StockPayment == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Stock Payment"
                });
            }

            var CIMATTracker = _context.CommonSections.FirstOrDefault(e => e.SectionName == "CIMAT Tracker");
            if (CIMATTracker == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "CIMAT Tracker"
                });
            }
            var StockPaymentTracker = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Stock Payment Tracker");
            if (StockPaymentTracker == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Stock Payment Tracker"
                });
            }
            var Variation = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Variation");
            if (Variation == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Variation"
                });
            }

            var STCRegister = _context.CommonSections.FirstOrDefault(e => e.SectionName == "STC Register");
            if (STCRegister == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "STC Register"
                });
            }

            var STCDealPrice = _context.CommonSections.FirstOrDefault(e => e.SectionName == "STC Deal Price");
            if (STCDealPrice == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "STC Deal Price"
                });
            }
            var StockReceivedReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Stock Received Report");
            if (StockReceivedReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Stock Received Report"
                });
            }
            var ReviewReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Review Report");
            if (ReviewReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Review Report"
                });
            }
            var LeadMonthlyComparison = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Lead Monthly Comparison");
            if (LeadMonthlyComparison == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Lead Monthly Comparison"
                });
            }
            var EmployeeProfitReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Employee Profit Report");
            if (EmployeeProfitReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Employee Profit Report"
                });
            }

            var SerialNoStatus = _context.CommonSections.FirstOrDefault(e => e.SectionName == "SerialNo Status");
            if (SerialNoStatus == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "SerialNo Status"
                });
            }
            var SerialNoHistoryReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "SerialNo History Report");
            if (SerialNoHistoryReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "SerialNo History Report"
                });
            }
            var ApplicationFeeTracker = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Application Fee Tracker");
            if (ApplicationFeeTracker == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Application Fee Tracker"
                });
            }
            var ProgressReport = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Progress Report");
            if (ProgressReport == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Progress Report"
                });
            }

            var Voucher = _context.CommonSections.FirstOrDefault(e => e.SectionName == "Voucher");
            if (Voucher == null)
            {
                _context.CommonSections.Add(new CommonSection
                {
                    SectionName = "Voucher"
                });
            }
        }

        private void CreateCheckActive()
        {
            var NMINumber = _context.CheckActive.FirstOrDefault(e => e.Name == "NMI Number");
            if (NMINumber == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "NMI Number",
                    IsActive = true
                });
            }

            var ApprovedReferenceNumber = _context.CheckActive.FirstOrDefault(e => e.Name == "Approved Reference Number");
            if (ApprovedReferenceNumber == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "Approved Reference Number",
                    IsActive = true
                });
            }

            var PeakMeterNo = _context.CheckActive.FirstOrDefault(e => e.Name == "Peak Meter No");
            if (PeakMeterNo == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "Peak Meter No",
                    IsActive = true
                });
            }

            var ElecRetailer = _context.CheckActive.FirstOrDefault(e => e.Name == "Elec Retailer");
            if (ElecRetailer == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "Elec Retailer",
                    IsActive = true
                });
            }

            var Deposit = _context.CheckActive.FirstOrDefault(e => e.Name == "Deposit");
            if (Deposit == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "Deposit",
                    IsActive = true
                });
            }

            var SignedQuote = _context.CheckActive.FirstOrDefault(e => e.Name == "Signed Quote");
            if (SignedQuote == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "Signed Quote",
                    IsActive = true
                });
            }

            var MeterBoxPhoto = _context.CheckActive.FirstOrDefault(e => e.Name == "Meter Box Photo");
            if (MeterBoxPhoto == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "Meter Box Photo",
                    IsActive = true
                });
            }

            var MeterUpgrade = _context.CheckActive.FirstOrDefault(e => e.Name == "Meter Upgrade");
            if (MeterUpgrade == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "Meter Upgrade",
                    IsActive = true
                });
            }

            var DistApproveDate = _context.CheckActive.FirstOrDefault(e => e.Name == "Dist Approve Date");
            if (DistApproveDate == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "Dist Approve Date",
                    IsActive = true
                });
            }

            var Finance = _context.CheckActive.FirstOrDefault(e => e.Name == "Finance");
            if (Finance == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "Finance",
                    IsActive = true
                });
            }

            var ExpiryDate = _context.CheckActive.FirstOrDefault(e => e.Name == "Expiry Date");
            if (ExpiryDate == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "Expiry Date",
                    IsActive = true
                });
            }

            var SolarRebateStatus = _context.CheckActive.FirstOrDefault(e => e.Name == "Solar Rebate Status");
            if (SolarRebateStatus == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "Solar Rebate Status",
                    IsActive = true
                });
            }

            var PaymentVerified = _context.CheckActive.FirstOrDefault(e => e.Name == "Payment Verified");
            if (PaymentVerified == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "Payment Verified",
                    IsActive = true
                });
            }

            var LastQuoteSigned = _context.CheckActive.FirstOrDefault(e => e.Name == "Last Quote Signed");
            if (LastQuoteSigned == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "Last Quote Signed",
                    IsActive = true
                });
            }

            var ExportControlFormSigned = _context.CheckActive.FirstOrDefault(e => e.Name == "Export Control Form Signed");
            if (ExportControlFormSigned == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "Export Control Form Signed",
                    IsActive = true
                });
            }

            var FeedInTariffSigned = _context.CheckActive.FirstOrDefault(e => e.Name == "Feed In Tariff Signed");
            if (FeedInTariffSigned == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "Feed In Tariff Signed",
                    IsActive = true
                });
            }

            var ShadingDeclarationSigned = _context.CheckActive.FirstOrDefault(e => e.Name == "Shading Declaration Signed");
            if (ShadingDeclarationSigned == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "Shading Declaration Signed",
                    IsActive = true
                });
            }

            var EfficiencyDeclarationSigned = _context.CheckActive.FirstOrDefault(e => e.Name == "Efficiency Declaration Signed");
            if (EfficiencyDeclarationSigned == null)
            {
                _context.CheckActive.Add(new CheckActive
                {
                    Name = "Efficiency Declaration Signed",
                    IsActive = true
                });
            }

        }

        private void CreateCheckApplication()
        {
            var MeterPhase = _context.CheckApplication.FirstOrDefault(e => e.Name == "Meter Phase");
            if (MeterPhase == null)
            {
                _context.CheckApplication.Add(new CheckApplication
                {
                    Name = "Meter Phase",
                    IsActive = true
                });
            }

            var NMINumber = _context.CheckApplication.FirstOrDefault(e => e.Name == "NMI Number");
            if (NMINumber == null)
            {
                _context.CheckApplication.Add(new CheckApplication
                {
                    Name = "NMI Number",
                    IsActive = true
                });
            }

            var PeakMeterNo = _context.CheckApplication.FirstOrDefault(e => e.Name == "Peak Meter No");
            if (PeakMeterNo == null)
            {
                _context.CheckApplication.Add(new CheckApplication
                {
                    Name = "Peak Meter No",
                    IsActive = true
                });
            }

            var DocList = _context.CheckApplication.FirstOrDefault(e => e.Name == "Doc List");
            if (DocList == null)
            {
                _context.CheckApplication.Add(new CheckApplication
                {
                    Name = "Doc List",
                    IsActive = true
                });
            }
        }

        private void CreateCheckDepositReceived()
        {
            var HouseType = _context.CheckDepositReceived.FirstOrDefault(e => e.Name == "House Type");
            if (HouseType == null)
            {
                _context.CheckDepositReceived.Add(new Checkdeposite
                {
                    Name = "House Type",
                    IsActive = true
                });
            }

            var ElecDistributor = _context.CheckDepositReceived.FirstOrDefault(e => e.Name == "Elec Distributor");
            if (ElecDistributor == null)
            {
                _context.CheckDepositReceived.Add(new Checkdeposite
                {
                    Name = "Elec Distributor",
                    IsActive = true
                });
            }

            var RoofAngle = _context.CheckDepositReceived.FirstOrDefault(e => e.Name == "Roof Angle");
            if (RoofAngle == null)
            {
                _context.CheckDepositReceived.Add(new Checkdeposite
                {
                    Name = "Roof Angle",
                    IsActive = true
                });
            }

            var RoofType = _context.CheckDepositReceived.FirstOrDefault(e => e.Name == "Roof Type");
            if (RoofType == null)
            {
                _context.CheckDepositReceived.Add(new Checkdeposite
                {
                    Name = "Roof Type",
                    IsActive = true
                });
            }

            var Quote = _context.CheckDepositReceived.FirstOrDefault(e => e.Name == "Quote");
            if (Quote == null)
            {
                _context.CheckDepositReceived.Add(new Checkdeposite
                {
                    Name = "Quote",
                    IsActive = true
                });
            }

            var Invoice = _context.CheckDepositReceived.FirstOrDefault(e => e.Name == "Invoice");
            if (Invoice == null)
            {
                _context.CheckDepositReceived.Add(new Checkdeposite
                {
                    Name = "Invoice",
                    IsActive = true
                });
            }

            var ProductDetail = _context.CheckDepositReceived.FirstOrDefault(e => e.Name == "Product Detail");
            if (ProductDetail == null)
            {
                _context.CheckDepositReceived.Add(new Checkdeposite
                {
                    Name = "Product Detail",
                    IsActive = true
                });
            }

            var BonusAdded = _context.CheckDepositReceived.FirstOrDefault(e => e.Name == "Bonus Added");
            if (BonusAdded == null)
            {
                _context.CheckDepositReceived.Add(new Checkdeposite
                {
                    Name = "Bonus Added",
                    IsActive = true
                });
            }
        }
    }
}