﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Updated_RECDatails_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RECDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    ActionType = table.Column<string>(nullable: true),
                    CompletedTime = table.Column<DateTime>(nullable: false),
                    CertificateType = table.Column<string>(nullable: true),
                    RegisteredPersonNumber = table.Column<long>(nullable: false),
                    AccreditationCode = table.Column<string>(nullable: true),
                    GenerationYear = table.Column<long>(nullable: false),
                    GenerationState = table.Column<string>(nullable: true),
                    StartSerialNumber = table.Column<long>(nullable: false),
                    EndSerialNumber = table.Column<long>(nullable: false),
                    FuelSource = table.Column<string>(nullable: true),
                    OwnerAccount = table.Column<string>(nullable: true),
                    OwnerAccountId = table.Column<long>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    GM_DisplayName = table.Column<string>(nullable: true),
                    GM_MonthCode = table.Column<long>(nullable: false),
                    GM_MonthName = table.Column<string>(nullable: true),
                    GM_Name = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RECDetails", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RECDetails");
        }
    }
}
