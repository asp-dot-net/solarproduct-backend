﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_Column_LeadGenAppointment_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LeadId",
                table: "LeadGenAppointments",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_LeadGenAppointments_LeadId",
                table: "LeadGenAppointments",
                column: "LeadId");

            migrationBuilder.AddForeignKey(
                name: "FK_LeadGenAppointments_Leads_LeadId",
                table: "LeadGenAppointments",
                column: "LeadId",
                principalTable: "Leads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeadGenAppointments_Leads_LeadId",
                table: "LeadGenAppointments");

            migrationBuilder.DropIndex(
                name: "IX_LeadGenAppointments_LeadId",
                table: "LeadGenAppointments");

            migrationBuilder.DropColumn(
                name: "LeadId",
                table: "LeadGenAppointments");
        }
    }
}
