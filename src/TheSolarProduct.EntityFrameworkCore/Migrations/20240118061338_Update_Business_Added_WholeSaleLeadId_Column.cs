﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_Business_Added_WholeSaleLeadId_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "WholeSaleLeadId",
                table: "Businesses",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Businesses_WholeSaleLeadId",
                table: "Businesses",
                column: "WholeSaleLeadId");

            migrationBuilder.AddForeignKey(
                name: "FK_Businesses_WholeSaleLeads_WholeSaleLeadId",
                table: "Businesses",
                column: "WholeSaleLeadId",
                principalTable: "WholeSaleLeads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Businesses_WholeSaleLeads_WholeSaleLeadId",
                table: "Businesses");

            migrationBuilder.DropIndex(
                name: "IX_Businesses_WholeSaleLeadId",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "WholeSaleLeadId",
                table: "Businesses");
        }
    }
}
