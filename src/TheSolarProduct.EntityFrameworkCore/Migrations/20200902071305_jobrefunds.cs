﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class jobrefunds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "JobRefunds",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Amount = table.Column<int>(nullable: true),
                    BankName = table.Column<string>(nullable: false),
                    AccountName = table.Column<string>(nullable: true),
                    BSBNo = table.Column<string>(nullable: true),
                    AccountNo = table.Column<string>(nullable: false),
                    Notes = table.Column<string>(nullable: true),
                    PaidDate = table.Column<DateTime>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    PaymentOptionId = table.Column<int>(nullable: true),
                    JobId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobRefunds", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobRefunds_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JobRefunds_PaymentOptions_PaymentOptionId",
                        column: x => x.PaymentOptionId,
                        principalTable: "PaymentOptions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_JobRefunds_JobId",
                table: "JobRefunds",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_JobRefunds_PaymentOptionId",
                table: "JobRefunds",
                column: "PaymentOptionId");

            migrationBuilder.CreateIndex(
                name: "IX_JobRefunds_TenantId",
                table: "JobRefunds",
                column: "TenantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JobRefunds");
        }
    }
}
