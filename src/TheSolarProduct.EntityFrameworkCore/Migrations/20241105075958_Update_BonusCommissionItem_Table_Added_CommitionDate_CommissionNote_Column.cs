﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_BonusCommissionItem_Table_Added_CommitionDate_CommissionNote_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CommissionNote",
                table: "BonusCommissionItems",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CommitionDate",
                table: "BonusCommissionItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CommissionNote",
                table: "BonusCommissionItems");

            migrationBuilder.DropColumn(
                name: "CommitionDate",
                table: "BonusCommissionItems");
        }
    }
}
