﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Updated_EcommerceProductItemSpecifications_Table_Added_Type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcommerceProductItemSpecifications_ProductItems_ProductItemID",
                table: "EcommerceProductItemSpecifications");

            migrationBuilder.DropIndex(
                name: "IX_EcommerceProductItemSpecifications_ProductItemID",
                table: "EcommerceProductItemSpecifications");

            migrationBuilder.DropColumn(
                name: "ProductItemID",
                table: "EcommerceProductItemSpecifications");

            migrationBuilder.AddColumn<int>(
                name: "EcommerceProductItemId",
                table: "EcommerceProductItemSpecifications",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceProductItemSpecifications_EcommerceProductItemId",
                table: "EcommerceProductItemSpecifications",
                column: "EcommerceProductItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_EcommerceProductItemSpecifications_EcommerceProductItems_EcommerceProductItemId",
                table: "EcommerceProductItemSpecifications",
                column: "EcommerceProductItemId",
                principalTable: "EcommerceProductItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcommerceProductItemSpecifications_EcommerceProductItems_EcommerceProductItemId",
                table: "EcommerceProductItemSpecifications");

            migrationBuilder.DropIndex(
                name: "IX_EcommerceProductItemSpecifications_EcommerceProductItemId",
                table: "EcommerceProductItemSpecifications");

            migrationBuilder.DropColumn(
                name: "EcommerceProductItemId",
                table: "EcommerceProductItemSpecifications");

            migrationBuilder.AddColumn<int>(
                name: "ProductItemID",
                table: "EcommerceProductItemSpecifications",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceProductItemSpecifications_ProductItemID",
                table: "EcommerceProductItemSpecifications",
                column: "ProductItemID");

            migrationBuilder.AddForeignKey(
                name: "FK_EcommerceProductItemSpecifications_ProductItems_ProductItemID",
                table: "EcommerceProductItemSpecifications",
                column: "ProductItemID",
                principalTable: "ProductItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
