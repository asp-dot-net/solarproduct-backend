﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Lead_Remove_SubSource : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Leads_LeadSubSources_LeadSubSourceId",
                table: "Leads");

            migrationBuilder.DropTable(
                name: "LeadSubSources");

            migrationBuilder.DropIndex(
                name: "IX_Leads_LeadSubSourceId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "LeadSubSourceId",
                table: "Leads");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LeadSubSourceId",
                table: "Leads",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "LeadSubSources",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    LeadSourceId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeadSubSources", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LeadSubSources_LeadSources_LeadSourceId",
                        column: x => x.LeadSourceId,
                        principalTable: "LeadSources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Leads_LeadSubSourceId",
                table: "Leads",
                column: "LeadSubSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_LeadSubSources_LeadSourceId",
                table: "LeadSubSources",
                column: "LeadSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_LeadSubSources_TenantId",
                table: "LeadSubSources",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_LeadSubSources_LeadSubSourceId",
                table: "Leads",
                column: "LeadSubSourceId",
                principalTable: "LeadSubSources",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
