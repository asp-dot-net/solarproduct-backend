﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_QuickStockActivityLog_Added_StockTransferId_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StockTransferId",
                table: "QuickStockActivityLogs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "QuickStockActivityLogs",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_QuickStockActivityLogs_StockTransferId",
                table: "QuickStockActivityLogs",
                column: "StockTransferId");

            migrationBuilder.AddForeignKey(
                name: "FK_QuickStockActivityLogs_StockTransfers_StockTransferId",
                table: "QuickStockActivityLogs",
                column: "StockTransferId",
                principalTable: "StockTransfers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_QuickStockActivityLogs_StockTransfers_StockTransferId",
                table: "QuickStockActivityLogs");

            migrationBuilder.DropIndex(
                name: "IX_QuickStockActivityLogs_StockTransferId",
                table: "QuickStockActivityLogs");

            migrationBuilder.DropColumn(
                name: "StockTransferId",
                table: "QuickStockActivityLogs");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "QuickStockActivityLogs");
        }
    }
}
