﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class update_WholesaleLead_Added_Suburb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DelivarySuburb",
                table: "WholeSaleLeads",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DelivarySuburbId",
                table: "WholeSaleLeads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalSuburb",
                table: "WholeSaleLeads",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PostalSuburbId",
                table: "WholeSaleLeads",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DelivarySuburb",
                table: "WholeSaleLeads");

            migrationBuilder.DropColumn(
                name: "DelivarySuburbId",
                table: "WholeSaleLeads");

            migrationBuilder.DropColumn(
                name: "PostalSuburb",
                table: "WholeSaleLeads");

            migrationBuilder.DropColumn(
                name: "PostalSuburbId",
                table: "WholeSaleLeads");
        }
    }
}
