﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_State_Table_Added_BatteryRebate_column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "BatteryRebate",
                table: "State",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActiveBatteryRebate",
                table: "State",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BatteryRebate",
                table: "State");

            migrationBuilder.DropColumn(
                name: "IsActiveBatteryRebate",
                table: "State");
        }
    }
}
