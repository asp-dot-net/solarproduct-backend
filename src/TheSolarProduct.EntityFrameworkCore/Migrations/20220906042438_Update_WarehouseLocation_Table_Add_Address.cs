﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_WarehouseLocation_Table_Add_Address : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "WareHouseLocation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Latitude",
                table: "WareHouseLocation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Longitude",
                table: "WareHouseLocation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostCode",
                table: "WareHouseLocation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StreetName",
                table: "WareHouseLocation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StreetNo",
                table: "WareHouseLocation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StreetType",
                table: "WareHouseLocation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UnitNo",
                table: "WareHouseLocation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UnitType",
                table: "WareHouseLocation",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "WareHouseLocation");

            migrationBuilder.DropColumn(
                name: "Latitude",
                table: "WareHouseLocation");

            migrationBuilder.DropColumn(
                name: "Longitude",
                table: "WareHouseLocation");

            migrationBuilder.DropColumn(
                name: "PostCode",
                table: "WareHouseLocation");

            migrationBuilder.DropColumn(
                name: "StreetName",
                table: "WareHouseLocation");

            migrationBuilder.DropColumn(
                name: "StreetNo",
                table: "WareHouseLocation");

            migrationBuilder.DropColumn(
                name: "StreetType",
                table: "WareHouseLocation");

            migrationBuilder.DropColumn(
                name: "UnitNo",
                table: "WareHouseLocation");

            migrationBuilder.DropColumn(
                name: "UnitType",
                table: "WareHouseLocation");
        }
    }
}
