﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class AddedColumnJob : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RECPvdNumber",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RECPvdStatus",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RECPvdNumber",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "RECPvdStatus",
                table: "Jobs");
        }
    }
}
