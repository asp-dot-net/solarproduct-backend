﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Updated_StockTransfer_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CornetNo",
                table: "StockTransfers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Received",
                table: "StockTransfers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReceivedByDate",
                table: "StockTransfers",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "TransferByDate",
                table: "StockTransfers",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "Transferd",
                table: "StockTransfers",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CornetNo",
                table: "StockTransfers");

            migrationBuilder.DropColumn(
                name: "Received",
                table: "StockTransfers");

            migrationBuilder.DropColumn(
                name: "ReceivedByDate",
                table: "StockTransfers");

            migrationBuilder.DropColumn(
                name: "TransferByDate",
                table: "StockTransfers");

            migrationBuilder.DropColumn(
                name: "Transferd",
                table: "StockTransfers");
        }
    }
}
