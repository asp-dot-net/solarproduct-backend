﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_Job_Added_HouseAge_SiteVisit_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "HouseAge",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SiteVisit",
                table: "Jobs",
                nullable: false,
                defaultValue: false);

            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HouseAge",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "SiteVisit",
                table: "Jobs");
        }
    }
}
