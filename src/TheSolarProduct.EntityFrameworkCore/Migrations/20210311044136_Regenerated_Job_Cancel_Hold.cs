﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_Job_Cancel_Hold : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "JobCancelReason",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "JobCancelReasonId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "JobHoldReason",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "NextFollowUpDate",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "JobCancelReason",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "JobCancelReasonId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "JobHoldReason",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "NextFollowUpDate",
                table: "Jobs");
        }
    }
}
