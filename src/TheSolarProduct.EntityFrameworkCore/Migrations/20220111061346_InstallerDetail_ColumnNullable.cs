﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class InstallerDetail_ColumnNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AreaName",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SourceTypeId",
                table: "InstallerDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AreaName",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "SourceTypeId",
                table: "InstallerDetails");
        }
    }
}
