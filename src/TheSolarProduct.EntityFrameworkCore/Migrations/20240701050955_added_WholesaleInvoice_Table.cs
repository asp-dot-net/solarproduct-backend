﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class added_WholesaleInvoice_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "WholesaleInvoiceId",
                table: "EcommerceCartProductItems",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "WholesaleInvoices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    InvoiceTypeId = table.Column<int>(nullable: true),
                    InvoiceNo = table.Column<string>(nullable: true),
                    JobTypeId = table.Column<int>(nullable: true),
                    WholesaleLeadId = table.Column<int>(nullable: true),
                    CreditAmount = table.Column<decimal>(nullable: true),
                    Reference = table.Column<string>(nullable: true),
                    DeliveryOptions = table.Column<string>(nullable: true),
                    JobStatusId = table.Column<int>(nullable: true),
                    StoreLoactionId = table.Column<int>(nullable: true),
                    InvoiceAmount = table.Column<decimal>(nullable: true),
                    InvoiceAmountWithGST = table.Column<decimal>(nullable: true),
                    DeliveryDate = table.Column<DateTime>(nullable: true),
                    DeliveryTime = table.Column<string>(nullable: true),
                    TansportTypeId = table.Column<int>(nullable: true),
                    Consign = table.Column<string>(nullable: true),
                    ActualFreigthCharges = table.Column<decimal>(nullable: true),
                    CustFreigthCharges = table.Column<decimal>(nullable: true),
                    PropertyTypeId = table.Column<int>(nullable: true),
                    PVDStatusId = table.Column<int>(nullable: true),
                    STCID = table.Column<string>(nullable: true),
                    NoOfSTC = table.Column<int>(nullable: true),
                    STCRate = table.Column<decimal>(nullable: true),
                    PVDNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WholesaleInvoices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WholesaleInvoices_Wholesaleinvoicetypes_InvoiceTypeId",
                        column: x => x.InvoiceTypeId,
                        principalTable: "Wholesaleinvoicetypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WholesaleInvoices_WholesaleJobStatuses_JobStatusId",
                        column: x => x.JobStatusId,
                        principalTable: "WholesaleJobStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WholesaleInvoices_WholesaleJobTypes_JobTypeId",
                        column: x => x.JobTypeId,
                        principalTable: "WholesaleJobTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WholesaleInvoices_WholesalePVDStatuses_PVDStatusId",
                        column: x => x.PVDStatusId,
                        principalTable: "WholesalePVDStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WholesaleInvoices_WholesalePropertyTypes_PropertyTypeId",
                        column: x => x.PropertyTypeId,
                        principalTable: "WholesalePropertyTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WholesaleInvoices_WareHouseLocation_StoreLoactionId",
                        column: x => x.StoreLoactionId,
                        principalTable: "WareHouseLocation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WholesaleInvoices_WholesaleTransportTypes_TansportTypeId",
                        column: x => x.TansportTypeId,
                        principalTable: "WholesaleTransportTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WholesaleInvoices_WholeSaleLeads_WholesaleLeadId",
                        column: x => x.WholesaleLeadId,
                        principalTable: "WholeSaleLeads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceCartProductItems_WholesaleInvoiceId",
                table: "EcommerceCartProductItems",
                column: "WholesaleInvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_WholesaleInvoices_InvoiceTypeId",
                table: "WholesaleInvoices",
                column: "InvoiceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_WholesaleInvoices_JobStatusId",
                table: "WholesaleInvoices",
                column: "JobStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_WholesaleInvoices_JobTypeId",
                table: "WholesaleInvoices",
                column: "JobTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_WholesaleInvoices_PVDStatusId",
                table: "WholesaleInvoices",
                column: "PVDStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_WholesaleInvoices_PropertyTypeId",
                table: "WholesaleInvoices",
                column: "PropertyTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_WholesaleInvoices_StoreLoactionId",
                table: "WholesaleInvoices",
                column: "StoreLoactionId");

            migrationBuilder.CreateIndex(
                name: "IX_WholesaleInvoices_TansportTypeId",
                table: "WholesaleInvoices",
                column: "TansportTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_WholesaleInvoices_WholesaleLeadId",
                table: "WholesaleInvoices",
                column: "WholesaleLeadId");

            migrationBuilder.AddForeignKey(
                name: "FK_EcommerceCartProductItems_WholesaleInvoices_WholesaleInvoiceId",
                table: "EcommerceCartProductItems",
                column: "WholesaleInvoiceId",
                principalTable: "WholesaleInvoices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcommerceCartProductItems_WholesaleInvoices_WholesaleInvoiceId",
                table: "EcommerceCartProductItems");

            migrationBuilder.DropTable(
                name: "WholesaleInvoices");

            migrationBuilder.DropIndex(
                name: "IX_EcommerceCartProductItems_WholesaleInvoiceId",
                table: "EcommerceCartProductItems");

            migrationBuilder.DropColumn(
                name: "WholesaleInvoiceId",
                table: "EcommerceCartProductItems");
        }
    }
}
