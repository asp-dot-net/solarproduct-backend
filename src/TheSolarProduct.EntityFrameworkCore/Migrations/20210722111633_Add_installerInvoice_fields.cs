﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Add_installerInvoice_fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ActualPanels_Installed",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AllGood_NotGood",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CES",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Cx_Sign",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmailSent",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Front_of_property",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Inst_Des_Ele_sign",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "InstalaltionPic",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Installation_Maintenance_Inspection",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Installer_selfie",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "InverterSerialNo",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "InvoiceAmount",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InvoiceNo",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Noof_Panels_Portal",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Noof_Panels_invoice",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NotesOrReasonforPending",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PanelsSerialNo",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Remark_if_owing",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Splits",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Traded",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TravelKMfromWarehouse",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Wi_FiDongle",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "otherExtraInvoiceNumber",
                table: "InstallerInvoices",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActualPanels_Installed",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "AllGood_NotGood",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "CES",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "Cx_Sign",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "EmailSent",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "Front_of_property",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "Inst_Des_Ele_sign",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "InstalaltionPic",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "Installation_Maintenance_Inspection",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "Installer_selfie",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "InverterSerialNo",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "InvoiceAmount",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "InvoiceNo",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "Noof_Panels_Portal",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "Noof_Panels_invoice",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "NotesOrReasonforPending",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "PanelsSerialNo",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "Remark_if_owing",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "Splits",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "Traded",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "TravelKMfromWarehouse",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "Wi_FiDongle",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "otherExtraInvoiceNumber",
                table: "InstallerInvoices");
        }
    }
}
