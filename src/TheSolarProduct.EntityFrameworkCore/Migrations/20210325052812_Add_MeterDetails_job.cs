﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Add_MeterDetails_job : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ComplianceCertificate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IncompleteReason",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "InspectionDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InspectorName",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "InstalledcompleteDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MeterApplyRef",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "iscomplete",
                table: "Jobs",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ComplianceCertificate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "IncompleteReason",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "InspectionDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "InspectorName",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "InstalledcompleteDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "MeterApplyRef",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "iscomplete",
                table: "Jobs");
        }
    }
}
