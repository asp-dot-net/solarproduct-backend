﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Remove_PostalAddress_Lead : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Country",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "IsSameAddress",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalAddress",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalIsGoogle",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalPostCode",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalState",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalStateId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalStreetName",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalStreetNo",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalStreetType",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalSuburb",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalSuburbId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalUnitNo",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalUnitType",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "Postallatitude",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "Postallongitude",
                table: "Leads");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Leads",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsSameAddress",
                table: "Leads",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalAddress",
                table: "Leads",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalIsGoogle",
                table: "Leads",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalPostCode",
                table: "Leads",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalState",
                table: "Leads",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PostalStateId",
                table: "Leads",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalStreetName",
                table: "Leads",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalStreetNo",
                table: "Leads",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalStreetType",
                table: "Leads",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalSuburb",
                table: "Leads",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PostalSuburbId",
                table: "Leads",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalUnitNo",
                table: "Leads",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalUnitType",
                table: "Leads",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Postallatitude",
                table: "Leads",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Postallongitude",
                table: "Leads",
                type: "nvarchar(max)",
                nullable: true);

        }
    }
}
