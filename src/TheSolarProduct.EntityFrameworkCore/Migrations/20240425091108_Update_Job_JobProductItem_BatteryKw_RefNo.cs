﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_Job_JobProductItem_BatteryKw_RefNo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "BatteryKw",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RefNo",
                table: "JobProductItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BatteryKw",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "RefNo",
                table: "JobProductItems");
        }
    }
}
