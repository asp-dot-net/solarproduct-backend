﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class PayWayJbFk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_PayWayDatas_jobId",
                table: "PayWayDatas",
                column: "jobId");

            migrationBuilder.AddForeignKey(
                name: "FK_PayWayDatas_Jobs_jobId",
                table: "PayWayDatas",
                column: "jobId",
                principalTable: "Jobs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PayWayDatas_Jobs_jobId",
                table: "PayWayDatas");

            migrationBuilder.DropIndex(
                name: "IX_PayWayDatas_jobId",
                table: "PayWayDatas");
        }
    }
}
