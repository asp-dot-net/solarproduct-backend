﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class ForeignKey_invoiceinstaller : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_InstallerInvoices_JobId",
                table: "InstallerInvoices",
                column: "JobId");

            migrationBuilder.AddForeignKey(
                name: "FK_InstallerInvoices_Jobs_JobId",
                table: "InstallerInvoices",
                column: "JobId",
                principalTable: "Jobs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InstallerInvoices_Jobs_JobId",
                table: "InstallerInvoices");

            migrationBuilder.DropIndex(
                name: "IX_InstallerInvoices_JobId",
                table: "InstallerInvoices");
        }
    }
}
