﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_purchasecompanyedit_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "PurchaseCompanies",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "PurchaseCompanies",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Logo",
                table: "PurchaseCompanies",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "PurchaseCompanies",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "PurchaseCompanies");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "PurchaseCompanies");

            migrationBuilder.DropColumn(
                name: "Logo",
                table: "PurchaseCompanies");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "PurchaseCompanies");
        }
    }
}
