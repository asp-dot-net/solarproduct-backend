﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_EcommerceProductItems_Added_EcommerceProductTypeId_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EcommerceProductTypeId",
                table: "EcommerceProductItems",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceProductItems_EcommerceProductTypeId",
                table: "EcommerceProductItems",
                column: "EcommerceProductTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_EcommerceProductItems_EcommerceProductTypes_EcommerceProductTypeId",
                table: "EcommerceProductItems",
                column: "EcommerceProductTypeId",
                principalTable: "EcommerceProductTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcommerceProductItems_EcommerceProductTypes_EcommerceProductTypeId",
                table: "EcommerceProductItems");

            migrationBuilder.DropIndex(
                name: "IX_EcommerceProductItems_EcommerceProductTypeId",
                table: "EcommerceProductItems");

            migrationBuilder.DropColumn(
                name: "EcommerceProductTypeId",
                table: "EcommerceProductItems");
        }
    }
}
