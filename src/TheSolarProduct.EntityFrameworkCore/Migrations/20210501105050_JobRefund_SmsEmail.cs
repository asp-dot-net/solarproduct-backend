﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class JobRefund_SmsEmail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "JobRefundEmailSend",
                table: "JobRefunds",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "JobRefundEmailSendDate",
                table: "JobRefunds",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "JobRefundSmsSend",
                table: "JobRefunds",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "JobRefundSmsSendDate",
                table: "JobRefunds",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "JobRefundEmailSend",
                table: "JobRefunds");

            migrationBuilder.DropColumn(
                name: "JobRefundEmailSendDate",
                table: "JobRefunds");

            migrationBuilder.DropColumn(
                name: "JobRefundSmsSend",
                table: "JobRefunds");

            migrationBuilder.DropColumn(
                name: "JobRefundSmsSendDate",
                table: "JobRefunds");
        }
    }
}
