﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class InstallerInvoices : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InstallerInvoices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: true),
                    InvTypeId = table.Column<int>(nullable: true),
                    Amount = table.Column<int>(nullable: true),
                    InvDate = table.Column<DateTime>(nullable: true),
                    AdvanceAmount = table.Column<int>(nullable: true),
                    LessDeductAmount = table.Column<int>(nullable: true),
                    TotalAmount = table.Column<int>(nullable: true),
                    AdvancePayDate = table.Column<DateTime>(nullable: true),
                    PayDate = table.Column<DateTime>(nullable: true),
                    PaymentTypeId = table.Column<DateTime>(nullable: true),
                    InstallerId = table.Column<int>(nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    IsPaid = table.Column<bool>(nullable: false),
                    IsVerify = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstallerInvoices", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InstallerInvoices");
        }
    }
}
