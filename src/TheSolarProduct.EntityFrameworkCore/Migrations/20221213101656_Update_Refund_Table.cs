﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_Refund_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobRefunds_PaymentOptions_PaymentOptionId",
                table: "JobRefunds");

            migrationBuilder.AddForeignKey(
                name: "FK_JobRefunds_InvoicePaymentMethods_PaymentOptionId",
                table: "JobRefunds",
                column: "PaymentOptionId",
                principalTable: "InvoicePaymentMethods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobRefunds_InvoicePaymentMethods_PaymentOptionId",
                table: "JobRefunds");

            migrationBuilder.AddForeignKey(
                name: "FK_JobRefunds_PaymentOptions_PaymentOptionId",
                table: "JobRefunds",
                column: "PaymentOptionId",
                principalTable: "PaymentOptions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
