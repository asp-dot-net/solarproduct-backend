﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Drop_RECDatails_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RECDetails");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RECDetails",
                columns: table => new
                {
                    PvdNumber = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    AccreditationCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ActionType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CertificateType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CompletedTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    EndSerialNumber = table.Column<long>(type: "bigint", nullable: false),
                    FuelSource = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GM_DisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GM_MonthCode = table.Column<long>(type: "bigint", nullable: false),
                    GM_MonthName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GM_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GenerationState = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GenerationYear = table.Column<long>(type: "bigint", nullable: false),
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    OwnerAccount = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OwnerAccountId = table.Column<long>(type: "bigint", nullable: false),
                    RegisteredPersonNumber = table.Column<long>(type: "bigint", nullable: false),
                    StartSerialNumber = table.Column<long>(type: "bigint", nullable: false),
                    Status = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RECDetails", x => x.PvdNumber);
                });
        }
    }
}
