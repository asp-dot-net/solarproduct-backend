﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_Buisness_BuisnessStructure_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BuisnessStructures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Structure = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuisnessStructures", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Businesses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    ABNNumber = table.Column<string>(nullable: true),
                    BuisnessStructureId = table.Column<int>(nullable: true),
                    UserId = table.Column<int>(nullable: true),
                    YearOfInspaction = table.Column<DateTime>(nullable: true),
                    LastFinancialYearTurnover = table.Column<decimal>(nullable: true),
                    AverageMonthlyTurnover = table.Column<decimal>(nullable: true),
                    NetProfit = table.Column<decimal>(nullable: true),
                    CECRegistrationNumber = table.Column<string>(nullable: true),
                    ElectricalLicence = table.Column<string>(nullable: true),
                    NumberOfEmployees = table.Column<int>(nullable: true),
                    NumberOfOffices = table.Column<int>(nullable: true),
                    LegalEntityName = table.Column<string>(nullable: true),
                    MainTradingName = table.Column<string>(nullable: true),
                    OtherTradingNames = table.Column<string>(nullable: true),
                    CreditLimitRequested = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Businesses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Businesses_BuisnessStructures_BuisnessStructureId",
                        column: x => x.BuisnessStructureId,
                        principalTable: "BuisnessStructures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Businesses_BuisnessStructureId",
                table: "Businesses",
                column: "BuisnessStructureId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Businesses");

            migrationBuilder.DropTable(
                name: "BuisnessStructures");
        }
    }
}
