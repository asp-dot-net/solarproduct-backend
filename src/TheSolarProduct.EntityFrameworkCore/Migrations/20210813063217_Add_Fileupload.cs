﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Add_Fileupload : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ApprovalLetter_FilePath",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ApprovalLetter_Filename",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApprovalLetter_FilePath",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ApprovalLetter_Filename",
                table: "Jobs");
        }
    }
}
