﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Job_ReviewSmsEmail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "ReviewEmailSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReviewEmailSendDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ReviewSmsSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReviewSmsSendDate",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReviewEmailSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ReviewEmailSendDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ReviewSmsSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ReviewSmsSendDate",
                table: "Jobs");
        }
    }
}
