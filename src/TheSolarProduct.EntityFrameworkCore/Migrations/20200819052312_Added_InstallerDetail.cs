﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_InstallerDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InstallerDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenantId = table.Column<int>(nullable: false),
                    InstallerAccreditationNumber = table.Column<string>(nullable: true),
                    InstallerAccreditationExpiryDate = table.Column<DateTime>(nullable: false),
                    DesignerLicenseNumber = table.Column<string>(nullable: true),
                    DesignerLicenseExpiryDate = table.Column<DateTime>(nullable: false),
                    ElectricianLicenseNumber = table.Column<string>(nullable: true),
                    ElectricianLicenseExpiryDate = table.Column<DateTime>(nullable: false),
                    DocInstaller = table.Column<string>(nullable: true),
                    DocDesigner = table.Column<string>(nullable: true),
                    DocElectrician = table.Column<string>(nullable: true),
                    OtherDocsCSV = table.Column<string>(nullable: true),
                    UserId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstallerDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InstallerDetails_AbpUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AbpUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InstallerDetails_TenantId",
                table: "InstallerDetails",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_InstallerDetails_UserId",
                table: "InstallerDetails",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InstallerDetails");
        }
    }
}
