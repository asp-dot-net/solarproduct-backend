﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_Commission_Appointment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CommissionNote",
                table: "LeadGenAppointments",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "CommitionAmount",
                table: "LeadGenAppointments",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<DateTime>(
                name: "CommitionDate",
                table: "LeadGenAppointments",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<decimal>(
                name: "CommitionAmount",
                table: "AbpUsers",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CommissionNote",
                table: "LeadGenAppointments");

            migrationBuilder.DropColumn(
                name: "CommitionAmount",
                table: "LeadGenAppointments");

            migrationBuilder.DropColumn(
                name: "CommitionDate",
                table: "LeadGenAppointments");

            migrationBuilder.DropColumn(
                name: "CommitionAmount",
                table: "AbpUsers");
        }
    }
}
