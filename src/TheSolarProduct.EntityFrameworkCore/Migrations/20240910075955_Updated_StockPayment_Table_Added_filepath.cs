﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Updated_StockPayment_Table_Added_filepath : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "StockPayments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FilePath",
                table: "StockPayments",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FileName",
                table: "StockPayments");

            migrationBuilder.DropColumn(
                name: "FilePath",
                table: "StockPayments");
        }
    }
}
