﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_Job_ApprovedCapacity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "ApprovedCapacityonExport",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ApprovedCapacityonNonExport",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApprovedCapacityonExport",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ApprovedCapacityonNonExport",
                table: "Jobs");
        }
    }
}
