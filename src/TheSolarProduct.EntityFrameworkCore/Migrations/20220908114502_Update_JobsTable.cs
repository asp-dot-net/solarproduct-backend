﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_JobsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BlackOutProtection",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "FinanceAmount",
                table: "Jobs",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "FinanceDepositeAmount",
                table: "Jobs",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "FinanceNetAmount",
                table: "Jobs",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "ProductPackageId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_ProductPackageId",
                table: "Jobs",
                column: "ProductPackageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ProductPackages_ProductPackageId",
                table: "Jobs",
                column: "ProductPackageId",
                principalTable: "ProductPackages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ProductPackages_ProductPackageId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_ProductPackageId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "BlackOutProtection",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FinanceAmount",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FinanceDepositeAmount",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FinanceNetAmount",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ProductPackageId",
                table: "Jobs");
        }
    }
}
