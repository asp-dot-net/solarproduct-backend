﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Updated_PurchaseOrderDocument_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PurchaseOrderDocuments_PurchaseDocumentListes_PurchaseDocumentId",
                table: "PurchaseOrderDocuments");

            migrationBuilder.DropColumn(
                name: "PurchaseDocument",
                table: "PurchaseOrderDocuments");

            migrationBuilder.AlterColumn<int>(
                name: "PurchaseDocumentId",
                table: "PurchaseOrderDocuments",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PurchaseOrderDocuments_PurchaseDocumentListes_PurchaseDocumentId",
                table: "PurchaseOrderDocuments",
                column: "PurchaseDocumentId",
                principalTable: "PurchaseDocumentListes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PurchaseOrderDocuments_PurchaseDocumentListes_PurchaseDocumentId",
                table: "PurchaseOrderDocuments");

            migrationBuilder.AlterColumn<int>(
                name: "PurchaseDocumentId",
                table: "PurchaseOrderDocuments",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "PurchaseDocument",
                table: "PurchaseOrderDocuments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_PurchaseOrderDocuments_PurchaseDocumentListes_PurchaseDocumentId",
                table: "PurchaseOrderDocuments",
                column: "PurchaseDocumentId",
                principalTable: "PurchaseDocumentListes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
