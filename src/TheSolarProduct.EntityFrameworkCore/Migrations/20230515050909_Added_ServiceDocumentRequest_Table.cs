﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_ServiceDocumentRequest_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ServiceDocumentRequestLinkHistorys",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Token = table.Column<string>(nullable: true),
                    ServiceDocumentRequestId = table.Column<int>(nullable: true),
                    Expired = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceDocumentRequestLinkHistorys", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ServiceDocumentRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    JobId = table.Column<int>(nullable: false),
                    ServiceDocTypeId = table.Column<int>(nullable: false),
                    IsSubmitted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceDocumentRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServiceDocumentRequests_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceDocumentRequests_ServiceDocumentTypes_ServiceDocTypeId",
                        column: x => x.ServiceDocTypeId,
                        principalTable: "ServiceDocumentTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ServiceDocumentRequests_JobId",
                table: "ServiceDocumentRequests",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceDocumentRequests_ServiceDocTypeId",
                table: "ServiceDocumentRequests",
                column: "ServiceDocTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ServiceDocumentRequestLinkHistorys");

            migrationBuilder.DropTable(
                name: "ServiceDocumentRequests");
        }
    }
}
