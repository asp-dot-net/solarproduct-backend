﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Add_bankdata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
             

            migrationBuilder.AddColumn<string>(
                name: "BankReferenceNo",
                table: "Jobs",
               nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReferralRemark",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BankReferenceNo",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ReferralRemark",
                table: "Jobs");
        }
    }
}
