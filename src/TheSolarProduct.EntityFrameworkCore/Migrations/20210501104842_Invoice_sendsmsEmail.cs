﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Invoice_sendsmsEmail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "InvoiceEmailSend",
                table: "InvoicePayments",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "InvoiceEmailSendDate",
                table: "InvoicePayments",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "InvoiceSmsSend",
                table: "InvoicePayments",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "InvoiceSmsSendDate",
                table: "InvoicePayments",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InvoiceEmailSend",
                table: "InvoicePayments");

            migrationBuilder.DropColumn(
                name: "InvoiceEmailSendDate",
                table: "InvoicePayments");

            migrationBuilder.DropColumn(
                name: "InvoiceSmsSend",
                table: "InvoicePayments");

            migrationBuilder.DropColumn(
                name: "InvoiceSmsSendDate",
                table: "InvoicePayments");

        }
    }
}
