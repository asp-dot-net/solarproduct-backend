﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_PromotionUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PromotionUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    ResponseDate = table.Column<DateTime>(nullable: false),
                    ResponseMessage = table.Column<string>(nullable: true),
                    PromotionId = table.Column<int>(nullable: true),
                    LeadId = table.Column<int>(nullable: true),
                    PromotionResponseStatusId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PromotionUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PromotionUsers_Leads_LeadId",
                        column: x => x.LeadId,
                        principalTable: "Leads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PromotionUsers_Promotions_PromotionId",
                        column: x => x.PromotionId,
                        principalTable: "Promotions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PromotionUsers_PromotionResponseStatuses_PromotionResponseStatusId",
                        column: x => x.PromotionResponseStatusId,
                        principalTable: "PromotionResponseStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PromotionUsers_LeadId",
                table: "PromotionUsers",
                column: "LeadId");

            migrationBuilder.CreateIndex(
                name: "IX_PromotionUsers_PromotionId",
                table: "PromotionUsers",
                column: "PromotionId");

            migrationBuilder.CreateIndex(
                name: "IX_PromotionUsers_PromotionResponseStatusId",
                table: "PromotionUsers",
                column: "PromotionResponseStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_PromotionUsers_TenantId",
                table: "PromotionUsers",
                column: "TenantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PromotionUsers");
        }
    }
}
