﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Job_DepActiveNotes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ActiveStatusNotes",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DepositeStatusNotes",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActiveStatusNotes",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DepositeStatusNotes",
                table: "Jobs");
        }
    }
}
