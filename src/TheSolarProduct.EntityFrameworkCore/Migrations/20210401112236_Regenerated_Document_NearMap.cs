﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_Document_NearMap : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "NearMapImage1",
                table: "Documents",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NearMapImage2",
                table: "Documents",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NearMapImage3",
                table: "Documents",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NearMapImage1",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "NearMapImage2",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "NearMapImage3",
                table: "Documents");
        }
    }
}
