﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_LeadFK_WholeSaleLeadFK_WholeSalePromotionUser_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WholeSalePromotionUsers_Leads_LeadId",
                table: "WholeSalePromotionUsers");

            migrationBuilder.DropIndex(
                name: "IX_WholeSalePromotionUsers_LeadId",
                table: "WholeSalePromotionUsers");

            migrationBuilder.DropColumn(
                name: "LeadId",
                table: "WholeSalePromotionUsers");

            migrationBuilder.AddColumn<int>(
                name: "WholeSaleLeadId",
                table: "WholeSalePromotionUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WholeSalePromotionUsers_WholeSaleLeadId",
                table: "WholeSalePromotionUsers",
                column: "WholeSaleLeadId");

            migrationBuilder.AddForeignKey(
                name: "FK_WholeSalePromotionUsers_WholeSaleLeads_WholeSaleLeadId",
                table: "WholeSalePromotionUsers",
                column: "WholeSaleLeadId",
                principalTable: "WholeSaleLeads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WholeSalePromotionUsers_WholeSaleLeads_WholeSaleLeadId",
                table: "WholeSalePromotionUsers");

            migrationBuilder.DropIndex(
                name: "IX_WholeSalePromotionUsers_WholeSaleLeadId",
                table: "WholeSalePromotionUsers");

            migrationBuilder.DropColumn(
                name: "WholeSaleLeadId",
                table: "WholeSalePromotionUsers");

            migrationBuilder.AddColumn<int>(
                name: "LeadId",
                table: "WholeSalePromotionUsers",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WholeSalePromotionUsers_LeadId",
                table: "WholeSalePromotionUsers",
                column: "LeadId");

            migrationBuilder.AddForeignKey(
                name: "FK_WholeSalePromotionUsers_Leads_LeadId",
                table: "WholeSalePromotionUsers",
                column: "LeadId",
                principalTable: "Leads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
