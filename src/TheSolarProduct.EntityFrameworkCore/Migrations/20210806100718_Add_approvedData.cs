﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Add_approvedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "ApproedAmount",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ApproedDate",
                table: "InstallerInvoices",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApproedAmount",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "ApproedDate",
                table: "InstallerInvoices");
        }
    }
}
