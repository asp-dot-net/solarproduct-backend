﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Updated_Jobs_Table_Added_BonusAmt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "BonusAmt",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BonusAmt",
                table: "Jobs");
        }
    }
}
