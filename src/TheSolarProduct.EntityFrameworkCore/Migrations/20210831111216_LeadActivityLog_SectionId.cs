﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class LeadActivityLog_SectionId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SectionId",
                table: "LeadActivityLog",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Section_SectionId",
                table: "Section",
                column: "SectionId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_LeadActivityLog_SectionId",
                table: "LeadActivityLog",
                column: "SectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_LeadActivityLog_Section_SectionId",
                table: "LeadActivityLog",
                column: "SectionId",
                principalTable: "Section",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeadActivityLog_Section_SectionId",
                table: "LeadActivityLog");

            migrationBuilder.DropIndex(
                name: "IX_Section_SectionId",
                table: "Section");

            migrationBuilder.DropIndex(
                name: "IX_LeadActivityLog_SectionId",
                table: "LeadActivityLog");

            migrationBuilder.DropColumn(
                name: "SectionId",
                table: "LeadActivityLog");
        }
    }
}
