﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_Tables_For_WholeSale_Ecommerce_ProductItems : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EcomerceSpecialStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EcomerceSpecialStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ECommerceDocumentTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    DocumentType = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ECommerceDocumentTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ECommercePriceCategorys",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    PriceCategory = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ECommercePriceCategorys", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EcommerceProductItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    ProductCategoryId = table.Column<int>(nullable: true),
                    ProductItemID = table.Column<int>(nullable: true),
                    ProductDescription = table.Column<string>(nullable: true),
                    IsDiscounted = table.Column<bool>(nullable: false),
                    IsNewArrival = table.Column<bool>(nullable: false),
                    IsFeatured = table.Column<bool>(nullable: false),
                    Rating = table.Column<int>(nullable: true),
                    SpecialStatusId = table.Column<int>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EcommerceProductItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EcommerceProductItems_ProductTypes_ProductCategoryId",
                        column: x => x.ProductCategoryId,
                        principalTable: "ProductTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EcommerceProductItems_ProductItems_ProductItemID",
                        column: x => x.ProductItemID,
                        principalTable: "ProductItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EcommerceProductItems_EcomerceSpecialStatuses_SpecialStatusId",
                        column: x => x.SpecialStatusId,
                        principalTable: "EcomerceSpecialStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EcommerceProductItemDocuments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    EcommerceProductItemId = table.Column<int>(nullable: true),
                    DocumentTypeId = table.Column<int>(nullable: true),
                    DocumentName = table.Column<string>(nullable: true),
                    Documemt = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EcommerceProductItemDocuments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EcommerceProductItemDocuments_ECommerceDocumentTypes_DocumentTypeId",
                        column: x => x.DocumentTypeId,
                        principalTable: "ECommerceDocumentTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EcommerceProductItemDocuments_EcommerceProductItems_EcommerceProductItemId",
                        column: x => x.EcommerceProductItemId,
                        principalTable: "EcommerceProductItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EcommerceProductItemPriceCategorys",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    EcommerceProductItemId = table.Column<int>(nullable: true),
                    ECommercePriceCategoryId = table.Column<int>(nullable: true),
                    Price = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EcommerceProductItemPriceCategorys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EcommerceProductItemPriceCategorys_ECommercePriceCategorys_ECommercePriceCategoryId",
                        column: x => x.ECommercePriceCategoryId,
                        principalTable: "ECommercePriceCategorys",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EcommerceProductItemPriceCategorys_EcommerceProductItems_EcommerceProductItemId",
                        column: x => x.EcommerceProductItemId,
                        principalTable: "EcommerceProductItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceProductItemDocuments_DocumentTypeId",
                table: "EcommerceProductItemDocuments",
                column: "DocumentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceProductItemDocuments_EcommerceProductItemId",
                table: "EcommerceProductItemDocuments",
                column: "EcommerceProductItemId");

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceProductItemPriceCategorys_ECommercePriceCategoryId",
                table: "EcommerceProductItemPriceCategorys",
                column: "ECommercePriceCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceProductItemPriceCategorys_EcommerceProductItemId",
                table: "EcommerceProductItemPriceCategorys",
                column: "EcommerceProductItemId");

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceProductItems_ProductCategoryId",
                table: "EcommerceProductItems",
                column: "ProductCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceProductItems_ProductItemID",
                table: "EcommerceProductItems",
                column: "ProductItemID");

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceProductItems_SpecialStatusId",
                table: "EcommerceProductItems",
                column: "SpecialStatusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EcommerceProductItemDocuments");

            migrationBuilder.DropTable(
                name: "EcommerceProductItemPriceCategorys");

            migrationBuilder.DropTable(
                name: "ECommerceDocumentTypes");

            migrationBuilder.DropTable(
                name: "ECommercePriceCategorys");

            migrationBuilder.DropTable(
                name: "EcommerceProductItems");

            migrationBuilder.DropTable(
                name: "EcomerceSpecialStatuses");
        }
    }
}
