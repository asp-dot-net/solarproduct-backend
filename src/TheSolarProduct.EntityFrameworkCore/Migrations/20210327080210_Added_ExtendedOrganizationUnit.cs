﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_ExtendedOrganizationUnit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AbpOrganizationUnits",
                nullable: false,
                defaultValue: "ExtendOrganizationUnit");

            migrationBuilder.AddColumn<string>(
                name: "ABN",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactNo",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Logo",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OrganizationCode",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProjectId",
                table: "AbpOrganizationUnits",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "ABN",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "ContactNo",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "Logo",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "OrganizationCode",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "AbpOrganizationUnits");
        }
    }
}
