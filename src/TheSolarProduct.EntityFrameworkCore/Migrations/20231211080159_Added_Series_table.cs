﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_Series_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SeriesId",
                table: "EcommerceProductItems",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Serieses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    SeriesName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Serieses", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceProductItems_SeriesId",
                table: "EcommerceProductItems",
                column: "SeriesId");

            migrationBuilder.AddForeignKey(
                name: "FK_EcommerceProductItems_Serieses_SeriesId",
                table: "EcommerceProductItems",
                column: "SeriesId",
                principalTable: "Serieses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcommerceProductItems_Serieses_SeriesId",
                table: "EcommerceProductItems");

            migrationBuilder.DropTable(
                name: "Serieses");

            migrationBuilder.DropIndex(
                name: "IX_EcommerceProductItems_SeriesId",
                table: "EcommerceProductItems");

            migrationBuilder.DropColumn(
                name: "SeriesId",
                table: "EcommerceProductItems");
        }
    }
}
