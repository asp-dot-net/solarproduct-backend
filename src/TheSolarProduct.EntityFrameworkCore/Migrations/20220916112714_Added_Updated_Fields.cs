﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_Updated_Fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "InverterLocation",
                table: "QuotationDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IconClass",
                table: "PromotionTypes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InverterLocation",
                table: "QuotationDetails");

            migrationBuilder.DropColumn(
                name: "IconClass",
                table: "PromotionTypes");
        }
    }
}
