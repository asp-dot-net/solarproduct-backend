﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_ServiceId_LeadActivityLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ServiceId",
                table: "LeadActivityLog",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LeadActivityLog_ServiceId",
                table: "LeadActivityLog",
                column: "ServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_LeadActivityLog_Services_ServiceId",
                table: "LeadActivityLog",
                column: "ServiceId",
                principalTable: "Services",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeadActivityLog_Services_ServiceId",
                table: "LeadActivityLog");

            migrationBuilder.DropIndex(
                name: "IX_LeadActivityLog_ServiceId",
                table: "LeadActivityLog");

            migrationBuilder.DropColumn(
                name: "ServiceId",
                table: "LeadActivityLog");
        }
    }
}
