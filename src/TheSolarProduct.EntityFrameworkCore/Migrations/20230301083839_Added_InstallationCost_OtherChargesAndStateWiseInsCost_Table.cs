﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_InstallationCost_OtherChargesAndStateWiseInsCost_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "OtherCharges",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OtherCharges", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StateWiseInstallationCosts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StateId = table.Column<int>(nullable: false),
                    Metro = table.Column<decimal>(nullable: true),
                    Regional = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StateWiseInstallationCosts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StateWiseInstallationCosts_State_StateId",
                        column: x => x.StateId,
                        principalTable: "State",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StateWiseInstallationCosts_StateId",
                table: "StateWiseInstallationCosts",
                column: "StateId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OtherCharges");

            migrationBuilder.DropTable(
                name: "StateWiseInstallationCosts");
        }
    }
}
