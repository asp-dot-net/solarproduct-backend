﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class AddedEnfornicaCallDetailsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EnfornicaCallDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    From = table.Column<string>(nullable: true),
                    CreateMethod = table.Column<string>(nullable: true),
                    StartTime = table.Column<string>(nullable: true),
                    EndTime = table.Column<string>(nullable: true),
                    AnswerTime = table.Column<string>(nullable: true),
                    AnswerDuration = table.Column<string>(nullable: true),
                    FromLocationRegionCode = table.Column<string>(nullable: true),
                    FromLocationAdministrativeArea = table.Column<string>(nullable: true),
                    FromLocationLocality = table.Column<string>(nullable: true),
                    FromLocationCoordinatesLatitude = table.Column<string>(nullable: true),
                    FromLocationCoordinatesLongitude = table.Column<string>(nullable: true),
                    LabelsMoli = table.Column<string>(nullable: true),
                    PriceCurrencyCode = table.Column<string>(nullable: true),
                    ToDisplayName = table.Column<string>(nullable: true),
                    RingTime = table.Column<string>(nullable: true),
                    Cost = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnfornicaCallDetails", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EnfornicaCallDetails");
        }
    }
}
