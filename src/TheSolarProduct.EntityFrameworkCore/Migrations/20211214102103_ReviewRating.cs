﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class ReviewRating : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ReviewNotes",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ReviewRating",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReviewNotes",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ReviewRating",
                table: "Jobs");
        }
    }
}
