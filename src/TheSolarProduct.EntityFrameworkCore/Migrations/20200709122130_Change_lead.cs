﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Change_lead : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSameAddress",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LeadSourceId",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PostalStateId",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalStreetName",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PostalSuburbId",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StateId",
                table: "Leads",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SuburbId",
                table: "Leads",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSameAddress",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "LeadSourceId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalStateId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalStreetName",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalSuburbId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "StateId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "SuburbId",
                table: "Leads");
        }
    }
}
