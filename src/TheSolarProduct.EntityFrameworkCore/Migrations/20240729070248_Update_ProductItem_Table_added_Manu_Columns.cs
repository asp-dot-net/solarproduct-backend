﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_ProductItem_Table_added_Manu_Columns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ManuEmail",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ManuMobile",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ManuPhone",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ManuWebsite",
                table: "ProductItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ManuEmail",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "ManuMobile",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "ManuPhone",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "ManuWebsite",
                table: "ProductItems");
        }
    }
}
