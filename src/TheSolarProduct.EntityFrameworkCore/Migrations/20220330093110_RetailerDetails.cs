﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class RetailerDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ApproxFeedTariff",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ExpectedPaybackPeriod",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ExportDetail",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Exportkw",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsGridConnected",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "NonExportDetail",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApproxFeedTariff",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ExpectedPaybackPeriod",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ExportDetail",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "Exportkw",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "IsGridConnected",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "NonExportDetail",
                table: "Jobs");
        }
    }
}
