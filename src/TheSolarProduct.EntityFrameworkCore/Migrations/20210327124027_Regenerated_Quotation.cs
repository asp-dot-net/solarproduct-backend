﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_Quotation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CustSignFileName",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustSignLatitude",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustSignLongitude",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "QuoteFileName",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "Quotations",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustSignFileName",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "CustSignLatitude",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "CustSignLongitude",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "QuoteFileName",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "Quotations");
        }
    }
}
