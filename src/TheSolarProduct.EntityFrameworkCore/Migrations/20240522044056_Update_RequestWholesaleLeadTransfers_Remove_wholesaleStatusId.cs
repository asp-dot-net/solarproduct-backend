﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_RequestWholesaleLeadTransfers_Remove_wholesaleStatusId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RequestWholesaleLeadTransfers_Leads_WholeSaleStatusId",
                table: "RequestWholesaleLeadTransfers");

            migrationBuilder.DropIndex(
                name: "IX_RequestWholesaleLeadTransfers_WholeSaleStatusId",
                table: "RequestWholesaleLeadTransfers");

            migrationBuilder.DropColumn(
                name: "WholeSaleStatusId",
                table: "RequestWholesaleLeadTransfers");

            migrationBuilder.CreateIndex(
                name: "IX_RequestWholesaleLeadTransfers_LeadId",
                table: "RequestWholesaleLeadTransfers",
                column: "LeadId");

            migrationBuilder.AddForeignKey(
                name: "FK_RequestWholesaleLeadTransfers_Leads_LeadId",
                table: "RequestWholesaleLeadTransfers",
                column: "LeadId",
                principalTable: "Leads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RequestWholesaleLeadTransfers_Leads_LeadId",
                table: "RequestWholesaleLeadTransfers");

            migrationBuilder.DropIndex(
                name: "IX_RequestWholesaleLeadTransfers_LeadId",
                table: "RequestWholesaleLeadTransfers");

            migrationBuilder.AddColumn<int>(
                name: "WholeSaleStatusId",
                table: "RequestWholesaleLeadTransfers",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RequestWholesaleLeadTransfers_WholeSaleStatusId",
                table: "RequestWholesaleLeadTransfers",
                column: "WholeSaleStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_RequestWholesaleLeadTransfers_Leads_WholeSaleStatusId",
                table: "RequestWholesaleLeadTransfers",
                column: "WholeSaleStatusId",
                principalTable: "Leads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
