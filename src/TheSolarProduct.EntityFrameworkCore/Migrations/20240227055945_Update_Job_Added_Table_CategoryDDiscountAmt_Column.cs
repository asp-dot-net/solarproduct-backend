﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_Job_Added_Table_CategoryDDiscountAmt_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "CategoryDDiscountAmt",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CategoryDNotes",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CategoryDDiscountAmt",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "CategoryDNotes",
                table: "Jobs");
        }
    }
}
