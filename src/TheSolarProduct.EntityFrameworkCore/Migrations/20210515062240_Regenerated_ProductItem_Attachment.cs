﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_ProductItem_Attachment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FilePath",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FileType",
                table: "ProductItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FileName",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "FilePath",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "FileType",
                table: "ProductItems");
        }
    }
}
