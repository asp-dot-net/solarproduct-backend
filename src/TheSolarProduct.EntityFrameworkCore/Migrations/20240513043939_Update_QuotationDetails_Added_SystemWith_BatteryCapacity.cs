﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_QuotationDetails_Added_SystemWith_BatteryCapacity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BatteryCapecity",
                table: "QuotationDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SystemWith",
                table: "QuotationDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BatteryCapecity",
                table: "QuotationDetails");

            migrationBuilder.DropColumn(
                name: "SystemWith",
                table: "QuotationDetails");
        }
    }
}
