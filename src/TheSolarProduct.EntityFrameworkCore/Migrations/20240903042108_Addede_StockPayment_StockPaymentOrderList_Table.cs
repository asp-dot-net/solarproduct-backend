﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Addede_StockPayment_StockPaymentOrderList_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StockPayments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    OrganizationId = table.Column<int>(nullable: false),
                    StockNo = table.Column<long>(nullable: false),
                    PaymentMethodId = table.Column<int>(nullable: true),
                    PaymentDate = table.Column<DateTime>(nullable: true),
                    CurrencyId = table.Column<int>(nullable: true),
                    GSTType = table.Column<int>(nullable: true),
                    Rate = table.Column<decimal>(nullable: true),
                    PaymentAmount = table.Column<decimal>(nullable: true),
                    AUD = table.Column<decimal>(nullable: true),
                    USD = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StockPayments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StockPayments_Currencies_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "Currencies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StockPayments_PaymentMethods_PaymentMethodId",
                        column: x => x.PaymentMethodId,
                        principalTable: "PaymentMethods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StockPaymentOrderLists",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    StockPaymentId = table.Column<int>(nullable: true),
                    PurchaseOrderId = table.Column<int>(nullable: true),
                    Amount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StockPaymentOrderLists", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StockPaymentOrderLists_PurchaseOrders_PurchaseOrderId",
                        column: x => x.PurchaseOrderId,
                        principalTable: "PurchaseOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StockPaymentOrderLists_StockPayments_StockPaymentId",
                        column: x => x.StockPaymentId,
                        principalTable: "StockPayments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StockPaymentOrderLists_PurchaseOrderId",
                table: "StockPaymentOrderLists",
                column: "PurchaseOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_StockPaymentOrderLists_StockPaymentId",
                table: "StockPaymentOrderLists",
                column: "StockPaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_StockPayments_CurrencyId",
                table: "StockPayments",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_StockPayments_PaymentMethodId",
                table: "StockPayments",
                column: "PaymentMethodId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StockPaymentOrderLists");

            migrationBuilder.DropTable(
                name: "StockPayments");
        }
    }
}
