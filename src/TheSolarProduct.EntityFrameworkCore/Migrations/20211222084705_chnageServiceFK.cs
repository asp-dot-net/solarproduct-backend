﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class chnageServiceFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Services_ServiceCategorys_ServiceSubCategorybId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Services_ServiceSubCategorybId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "ServiceSubCategorybId",
                table: "Services");

            migrationBuilder.CreateIndex(
                name: "IX_Services_ServiceSubCategoryId",
                table: "Services",
                column: "ServiceSubCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Services_ServiceCategorys_ServiceSubCategoryId",
                table: "Services",
                column: "ServiceSubCategoryId",
                principalTable: "ServiceCategorys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Services_ServiceCategorys_ServiceSubCategoryId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Services_ServiceSubCategoryId",
                table: "Services");

            migrationBuilder.AddColumn<int>(
                name: "ServiceSubCategorybId",
                table: "Services",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Services_ServiceSubCategorybId",
                table: "Services",
                column: "ServiceSubCategorybId");

            migrationBuilder.AddForeignKey(
                name: "FK_Services_ServiceCategorys_ServiceSubCategorybId",
                table: "Services",
                column: "ServiceSubCategorybId",
                principalTable: "ServiceCategorys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
