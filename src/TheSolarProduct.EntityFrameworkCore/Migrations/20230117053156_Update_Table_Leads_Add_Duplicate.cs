﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_Table_Leads_Add_Duplicate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DublicateLeadId",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "HideDublicate",
                table: "Leads",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DublicateLeadId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "HideDublicate",
                table: "Leads");
        }
    }
}
