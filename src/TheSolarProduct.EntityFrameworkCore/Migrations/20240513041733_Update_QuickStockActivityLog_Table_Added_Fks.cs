﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_QuickStockActivityLog_Table_Added_Fks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ActionId",
                table: "QuickStockActivityLogs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ActivityDate",
                table: "QuickStockActivityLogs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PurchaseOrderId",
                table: "QuickStockActivityLogs",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_QuickStockActivityLogs_ActionId",
                table: "QuickStockActivityLogs",
                column: "ActionId");

            migrationBuilder.CreateIndex(
                name: "IX_QuickStockActivityLogs_PurchaseOrderId",
                table: "QuickStockActivityLogs",
                column: "PurchaseOrderId");

            migrationBuilder.AddForeignKey(
                name: "FK_QuickStockActivityLogs_LeadAction_ActionId",
                table: "QuickStockActivityLogs",
                column: "ActionId",
                principalTable: "LeadAction",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_QuickStockActivityLogs_PurchaseOrders_PurchaseOrderId",
                table: "QuickStockActivityLogs",
                column: "PurchaseOrderId",
                principalTable: "PurchaseOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_QuickStockActivityLogs_LeadAction_ActionId",
                table: "QuickStockActivityLogs");

            migrationBuilder.DropForeignKey(
                name: "FK_QuickStockActivityLogs_PurchaseOrders_PurchaseOrderId",
                table: "QuickStockActivityLogs");

            migrationBuilder.DropIndex(
                name: "IX_QuickStockActivityLogs_ActionId",
                table: "QuickStockActivityLogs");

            migrationBuilder.DropIndex(
                name: "IX_QuickStockActivityLogs_PurchaseOrderId",
                table: "QuickStockActivityLogs");

            migrationBuilder.DropColumn(
                name: "ActionId",
                table: "QuickStockActivityLogs");

            migrationBuilder.DropColumn(
                name: "ActivityDate",
                table: "QuickStockActivityLogs");

            migrationBuilder.DropColumn(
                name: "PurchaseOrderId",
                table: "QuickStockActivityLogs");
        }
    }
}
