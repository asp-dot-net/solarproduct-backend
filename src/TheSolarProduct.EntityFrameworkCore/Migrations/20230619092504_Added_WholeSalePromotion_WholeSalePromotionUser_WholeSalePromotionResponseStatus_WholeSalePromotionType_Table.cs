﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_WholeSalePromotion_WholeSalePromotionUser_WholeSalePromotionResponseStatus_WholeSalePromotionType_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WholeSalePromotionResponseStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WholeSalePromotionResponseStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WholeSalePromotionTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    IconClass = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WholeSalePromotionTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WholeSalePromotions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    PromoCharge = table.Column<decimal>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    WholeSalePromotionTypeId = table.Column<int>(nullable: true),
                    LeadCount = table.Column<int>(nullable: false),
                    OrganizationID = table.Column<int>(nullable: false),
                    StartDateFilter = table.Column<DateTime>(nullable: true),
                    EndDateFilter = table.Column<DateTime>(nullable: true),
                    LeadStatusIdsFilter = table.Column<string>(nullable: true),
                    LeadSourceIdsFilter = table.Column<string>(nullable: true),
                    StateIdsFilter = table.Column<string>(nullable: true),
                    TeamIdsFilter = table.Column<string>(nullable: true),
                    JobStatusIdsFilter = table.Column<string>(nullable: true),
                    AreaNameFilter = table.Column<string>(nullable: true),
                    TypeNameFilter = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WholeSalePromotions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WholeSalePromotions_WholeSalePromotionTypes_WholeSalePromotionTypeId",
                        column: x => x.WholeSalePromotionTypeId,
                        principalTable: "WholeSalePromotionTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WholeSalePromotionUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    ResponseDate = table.Column<DateTime>(nullable: false),
                    ResponseMessage = table.Column<string>(nullable: true),
                    WholeSalePromotionId = table.Column<int>(nullable: true),
                    LeadId = table.Column<int>(nullable: true),
                    WholeSalePromotionResponseStatusId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WholeSalePromotionUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WholeSalePromotionUsers_Leads_LeadId",
                        column: x => x.LeadId,
                        principalTable: "Leads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WholeSalePromotionUsers_WholeSalePromotions_WholeSalePromotionId",
                        column: x => x.WholeSalePromotionId,
                        principalTable: "WholeSalePromotions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WholeSalePromotionUsers_WholeSalePromotionResponseStatuses_WholeSalePromotionResponseStatusId",
                        column: x => x.WholeSalePromotionResponseStatusId,
                        principalTable: "WholeSalePromotionResponseStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WholeSalePromotions_WholeSalePromotionTypeId",
                table: "WholeSalePromotions",
                column: "WholeSalePromotionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_WholeSalePromotionUsers_LeadId",
                table: "WholeSalePromotionUsers",
                column: "LeadId");

            migrationBuilder.CreateIndex(
                name: "IX_WholeSalePromotionUsers_WholeSalePromotionId",
                table: "WholeSalePromotionUsers",
                column: "WholeSalePromotionId");

            migrationBuilder.CreateIndex(
                name: "IX_WholeSalePromotionUsers_WholeSalePromotionResponseStatusId",
                table: "WholeSalePromotionUsers",
                column: "WholeSalePromotionResponseStatusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WholeSalePromotionUsers");

            migrationBuilder.DropTable(
                name: "WholeSalePromotions");

            migrationBuilder.DropTable(
                name: "WholeSalePromotionResponseStatuses");

            migrationBuilder.DropTable(
                name: "WholeSalePromotionTypes");
        }
    }
}
