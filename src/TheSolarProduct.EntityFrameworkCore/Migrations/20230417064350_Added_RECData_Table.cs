﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_RECData_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RECDatas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    PVDNumber = table.Column<string>(nullable: true),
                    JobNumber = table.Column<string>(nullable: true),
                    NoOfPanels = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    RECCreated = table.Column<int>(nullable: false),
                    RECPassed = table.Column<int>(nullable: false),
                    RECPanding = table.Column<int>(nullable: false),
                    RECFailed = table.Column<int>(nullable: false),
                    RECRegistered = table.Column<int>(nullable: false),
                    LastAuditedDate = table.Column<DateTime>(nullable: false),
                    BlukUploadId = table.Column<long>(nullable: false),
                    Exception = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RECDatas", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RECDatas");
        }
    }
}
