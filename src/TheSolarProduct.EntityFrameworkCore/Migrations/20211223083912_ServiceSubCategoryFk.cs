﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class ServiceSubCategoryFk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Services_ServiceCategorys_ServiceSubCategoryId",
                table: "Services");

            migrationBuilder.AddForeignKey(
                name: "FK_Services_ServiceSubCategorys_ServiceSubCategoryId",
                table: "Services",
                column: "ServiceSubCategoryId",
                principalTable: "ServiceSubCategorys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Services_ServiceSubCategorys_ServiceSubCategoryId",
                table: "Services");

            migrationBuilder.AddForeignKey(
                name: "FK_Services_ServiceCategorys_ServiceSubCategoryId",
                table: "Services",
                column: "ServiceSubCategoryId",
                principalTable: "ServiceCategorys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
