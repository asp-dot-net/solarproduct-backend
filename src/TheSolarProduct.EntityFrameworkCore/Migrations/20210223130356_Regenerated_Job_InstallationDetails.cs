﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_Job_InstallationDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DesignerId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ElectricianId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "InstallationDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InstallationNotes",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InstallationTime",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "InstallerId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WarehouseLocation",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DesignerId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ElectricianId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "InstallationDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "InstallationNotes",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "InstallationTime",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "InstallerId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "WarehouseLocation",
                table: "Jobs");
        }
    }
}
