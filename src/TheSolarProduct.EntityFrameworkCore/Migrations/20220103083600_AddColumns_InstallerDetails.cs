﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class AddColumns_InstallerDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AreaName",
                table: "AbpUsers");

            migrationBuilder.DropColumn(
                name: "SourceTypeId",
                table: "AbpUsers");

            migrationBuilder.AddColumn<string>(
                name: "AreaName",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SourceTypeId",
                table: "InstallerDetails",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AreaName",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "SourceTypeId",
                table: "InstallerDetails");

            migrationBuilder.AddColumn<string>(
                name: "AreaName",
                table: "AbpUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SourceTypeId",
                table: "AbpUsers",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
