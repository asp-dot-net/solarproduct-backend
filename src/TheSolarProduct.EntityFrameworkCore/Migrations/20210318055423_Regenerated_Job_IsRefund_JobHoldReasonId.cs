﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_Job_IsRefund_JobHoldReasonId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsRefund",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "JobHoldReasonId",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsRefund",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "JobHoldReasonId",
                table: "Jobs");
        }
    }
}
