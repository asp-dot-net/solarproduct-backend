﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_Jobs_Table_Added_BatteryRebate_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "BatteryBess",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "BatteryBessPerPrice",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "BatteryRebate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsBatteryRebate",
                table: "Jobs",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BatteryBess",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "BatteryBessPerPrice",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "BatteryRebate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "IsBatteryRebate",
                table: "Jobs");
        }
    }
}
