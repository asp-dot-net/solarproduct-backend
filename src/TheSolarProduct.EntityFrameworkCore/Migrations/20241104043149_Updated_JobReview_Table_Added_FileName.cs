﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Updated_JobReview_Table_Added_FileName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "JobReview",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FilePath",
                table: "JobReview",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FileName",
                table: "JobReview");

            migrationBuilder.DropColumn(
                name: "FilePath",
                table: "JobReview");
        }
    }
}
