﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Job_sendEmaile_SmsandEmailDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "EmailSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EmailSendDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SmsSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SmsSendDate",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmailSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "EmailSendDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "SmsSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "SmsSendDate",
                table: "Jobs");
        }
    }
}
