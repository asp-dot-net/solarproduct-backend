﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_PostoCodePrice_Added_ForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PostCodePricePeriodId",
                table: "PostoCodePrices",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PostoCodePrices_PostCodePricePeriodId",
                table: "PostoCodePrices",
                column: "PostCodePricePeriodId");

            migrationBuilder.AddForeignKey(
                name: "FK_PostoCodePrices_PostCodePricePeriods_PostCodePricePeriodId",
                table: "PostoCodePrices",
                column: "PostCodePricePeriodId",
                principalTable: "PostCodePricePeriods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PostoCodePrices_PostCodePricePeriods_PostCodePricePeriodId",
                table: "PostoCodePrices");

            migrationBuilder.DropIndex(
                name: "IX_PostoCodePrices_PostCodePricePeriodId",
                table: "PostoCodePrices");

            migrationBuilder.DropColumn(
                name: "PostCodePricePeriodId",
                table: "PostoCodePrices");
        }
    }
}
