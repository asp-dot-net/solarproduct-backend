﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class CreateTable_MyInstallerActivityLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MyInstallerActivityLog",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    MyInstallerId = table.Column<int>(nullable: false),
                    ActionId = table.Column<int>(nullable: false),
                    ActionNote = table.Column<string>(nullable: true),
                    ActivityDate = table.Column<DateTime>(nullable: true),
                    Body = table.Column<string>(nullable: true),
                    ActivityNote = table.Column<string>(nullable: true),
                    MessageId = table.Column<string>(nullable: true),
                    ReferanceId = table.Column<int>(nullable: true),
                    IsMark = table.Column<bool>(nullable: true),
                    TemplateId = table.Column<int>(nullable: true),
                    Subject = table.Column<string>(nullable: true),
                    PromotionId = table.Column<int>(nullable: true),
                    PromotionUserId = table.Column<int>(nullable: true),
                    SectionId = table.Column<int>(nullable: true),
                    TodopriorityId = table.Column<int>(nullable: true),
                    Todopriority = table.Column<string>(nullable: true),
                    IsTodoComplete = table.Column<bool>(nullable: true),
                    TodoResponse = table.Column<string>(nullable: true),
                    TodoTag = table.Column<string>(nullable: true),
                    TodoDueDate = table.Column<DateTime>(nullable: true),
                    TodoresponseTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MyInstallerActivityLog", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MyInstallerActivityLog");
        }
    }
}
