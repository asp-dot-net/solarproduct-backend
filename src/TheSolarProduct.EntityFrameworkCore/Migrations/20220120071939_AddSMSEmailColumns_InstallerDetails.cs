﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class AddSMSEmailColumns_InstallerDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "EmailSend",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EmailSendDate",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SmsSend",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SmsSendDate",
                table: "InstallerDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmailSend",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "EmailSendDate",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "SmsSend",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "SmsSendDate",
                table: "InstallerDetails");
        }
    }
}
