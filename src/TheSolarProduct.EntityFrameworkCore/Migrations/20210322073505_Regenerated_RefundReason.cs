﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_RefundReason : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Active",
                table: "RefundReasons");

            migrationBuilder.AlterColumn<int>(
                name: "TenantId",
                table: "RefundReasons",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "RefundReasons",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "RefundReasons",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "RefundReasons",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "RefundReasons",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "RefundReasons",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "RefundReasons",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "RefundReasons",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "RefundReasons");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "RefundReasons");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "RefundReasons");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "RefundReasons");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "RefundReasons");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "RefundReasons");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "RefundReasons");

            migrationBuilder.AlterColumn<int>(
                name: "TenantId",
                table: "RefundReasons",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "RefundReasons",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
