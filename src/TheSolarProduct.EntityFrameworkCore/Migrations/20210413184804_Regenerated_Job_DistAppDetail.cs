﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_Job_DistAppDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ApplicationNotes",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DistExpiryDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProjectNotes",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApplicationNotes",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DistExpiryDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ProjectNotes",
                table: "Jobs");
        }
    }
}
