﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_Job_Finance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "FinanceApplicationDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FinanceAppliedBy",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FinanceDocReceivedBy",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FinanceDocReceivedDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FinanceDocSentBy",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FinanceDocSentDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "FinanceDocumentVerified",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FinancePurchaseNo",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FinanceApplicationDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FinanceAppliedBy",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FinanceDocReceivedBy",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FinanceDocReceivedDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FinanceDocSentBy",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FinanceDocSentDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FinanceDocumentVerified",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FinancePurchaseNo",
                table: "Jobs");
        }
    }
}
