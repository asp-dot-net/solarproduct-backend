﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_ContactUS_Column_First_LastName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "EcommerceContactUses");

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "EcommerceContactUses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "EcommerceContactUses",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "EcommerceContactUses");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "EcommerceContactUses");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "EcommerceContactUses",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
