﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class update_WholeSale_Added_WholesaleClientId_And_TradinSTCWhomId_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TradinSTCWhom",
                table: "WholeSaleLeads");

            migrationBuilder.AddColumn<int>(
                name: "TradinSTCWhomId",
                table: "WholeSaleLeads",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WholesaleClientId",
                table: "WholeSaleLeads",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WholeSaleLeads_TradinSTCWhomId",
                table: "WholeSaleLeads",
                column: "TradinSTCWhomId");

            migrationBuilder.CreateIndex(
                name: "IX_WholeSaleLeads_WholesaleClientId",
                table: "WholeSaleLeads",
                column: "WholesaleClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_WholeSaleLeads_TradingSTCWhoms_TradinSTCWhomId",
                table: "WholeSaleLeads",
                column: "TradinSTCWhomId",
                principalTable: "TradingSTCWhoms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WholeSaleLeads_EcommerceUserRegisterRequests_WholesaleClientId",
                table: "WholeSaleLeads",
                column: "WholesaleClientId",
                principalTable: "EcommerceUserRegisterRequests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WholeSaleLeads_TradingSTCWhoms_TradinSTCWhomId",
                table: "WholeSaleLeads");

            migrationBuilder.DropForeignKey(
                name: "FK_WholeSaleLeads_EcommerceUserRegisterRequests_WholesaleClientId",
                table: "WholeSaleLeads");

            migrationBuilder.DropIndex(
                name: "IX_WholeSaleLeads_TradinSTCWhomId",
                table: "WholeSaleLeads");

            migrationBuilder.DropIndex(
                name: "IX_WholeSaleLeads_WholesaleClientId",
                table: "WholeSaleLeads");

            migrationBuilder.DropColumn(
                name: "TradinSTCWhomId",
                table: "WholeSaleLeads");

            migrationBuilder.DropColumn(
                name: "WholesaleClientId",
                table: "WholeSaleLeads");

            migrationBuilder.AddColumn<string>(
                name: "TradinSTCWhom",
                table: "WholeSaleLeads",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
