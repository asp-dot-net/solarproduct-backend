﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_ActivityLog_Promotion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PromotionId",
                table: "LeadActivityLog",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PromotionUserId",
                table: "LeadActivityLog",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PromotionId",
                table: "LeadActivityLog");

            migrationBuilder.DropColumn(
                name: "PromotionUserId",
                table: "LeadActivityLog");
        }
    }
}
