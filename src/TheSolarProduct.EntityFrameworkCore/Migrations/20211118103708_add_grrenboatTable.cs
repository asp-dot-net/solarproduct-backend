﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class add_grrenboatTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GreenBoatDatas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: true),
                    ProjectNo = table.Column<int>(nullable: false),
                    VendorJobId = table.Column<string>(nullable: true),
                    JobID = table.Column<int>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    RefNumber = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    JobType = table.Column<int>(nullable: true),
                    JobStage = table.Column<int>(nullable: true),
                    Priority = table.Column<int>(nullable: true),
                    InstallationDate = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<string>(nullable: true),
                    IsGst = table.Column<bool>(nullable: false),
                    OwnerType_JOD = table.Column<string>(nullable: true),
                    CompanyName_JOD = table.Column<string>(nullable: true),
                    FirstName_JOD = table.Column<string>(nullable: true),
                    LastName_JOD = table.Column<string>(nullable: true),
                    Phone_JOD = table.Column<string>(nullable: true),
                    Mobile_JOD = table.Column<string>(nullable: true),
                    Email_JOD = table.Column<string>(nullable: true),
                    IsPostalAddress_JOD = table.Column<bool>(nullable: false),
                    UnitTypeID_JOD = table.Column<string>(nullable: true),
                    UnitNumber_JOD = table.Column<string>(nullable: true),
                    StreetNumber_JOD = table.Column<string>(nullable: true),
                    StreetName_JOD = table.Column<string>(nullable: true),
                    StreetTypeID_JOD = table.Column<int>(nullable: true),
                    Town_JOD = table.Column<string>(nullable: true),
                    State_JOD = table.Column<string>(nullable: true),
                    PostCode_JOD = table.Column<string>(nullable: true),
                    DistributorID_JID = table.Column<string>(nullable: true),
                    MeterNumber_JID = table.Column<string>(nullable: true),
                    PhaseProperty_JID = table.Column<string>(nullable: true),
                    ElectricityProviderID_JID = table.Column<string>(nullable: true),
                    IsSameAsOwnerAddress_JID = table.Column<bool>(nullable: false),
                    IsPostalAddress_JID = table.Column<bool>(nullable: false),
                    UnitTypeID_JID = table.Column<string>(nullable: true),
                    UnitNumber_JID = table.Column<string>(nullable: true),
                    StreetNumber_JID = table.Column<string>(nullable: true),
                    StreetName_JID = table.Column<string>(nullable: true),
                    StreetTypeID_JID = table.Column<int>(nullable: true),
                    Town_JID = table.Column<string>(nullable: true),
                    State_JID = table.Column<string>(nullable: true),
                    PostCode_JID = table.Column<string>(nullable: true),
                    NMI_JID = table.Column<string>(nullable: true),
                    SystemSize_JID = table.Column<string>(nullable: true),
                    PropertyType_JID = table.Column<string>(nullable: true),
                    SingleMultipleStory_JID = table.Column<string>(nullable: true),
                    InstallingNewPanel_JID = table.Column<string>(nullable: true),
                    Location_JID = table.Column<string>(nullable: true),
                    ExistingSystem_JID = table.Column<bool>(nullable: false),
                    ExistingSystemSize_JID = table.Column<string>(nullable: true),
                    SystemLocation_JID = table.Column<string>(nullable: true),
                    NoOfPanels_JID = table.Column<string>(nullable: true),
                    AdditionalInstallationInformation_JID = table.Column<string>(nullable: true),
                    SystemSize_JSD = table.Column<string>(nullable: true),
                    SerialNumbers_JSD = table.Column<string>(nullable: true),
                    NoOfPanel_JSD = table.Column<string>(nullable: true),
                    AdditionalCapacityNotes_JSTC = table.Column<string>(nullable: true),
                    TypeOfConnection_JSTC = table.Column<string>(nullable: true),
                    SystemMountingType_JSTC = table.Column<string>(nullable: true),
                    DeemingPeriod_JSTC = table.Column<string>(nullable: true),
                    CertificateCreated_JSTC = table.Column<string>(nullable: true),
                    FailedAccreditationCode_JSTC = table.Column<string>(nullable: true),
                    FailedReason_JSTC = table.Column<string>(nullable: true),
                    MultipleSGUAddress_JSTC = table.Column<string>(nullable: true),
                    Location_JSTC = table.Column<string>(nullable: true),
                    AdditionalLocationInformation_JSTC = table.Column<string>(nullable: true),
                    AdditionalSystemInformation_JSTC = table.Column<string>(nullable: true),
                    FirstName_IV = table.Column<string>(nullable: true),
                    LastName_IV = table.Column<string>(nullable: true),
                    Email_IV = table.Column<string>(nullable: true),
                    Phone_IV = table.Column<string>(nullable: true),
                    Mobile_IV = table.Column<string>(nullable: true),
                    CECAccreditationNumber_IV = table.Column<string>(nullable: true),
                    ElectricalContractorsLicenseNumber_IV = table.Column<string>(nullable: true),
                    IsPostalAddress_IV = table.Column<bool>(nullable: false),
                    UnitTypeID_IV = table.Column<int>(nullable: true),
                    UnitNumber_IV = table.Column<string>(nullable: true),
                    StreetNumber_IV = table.Column<string>(nullable: true),
                    StreetName_IV = table.Column<string>(nullable: true),
                    StreetTypeID_IV = table.Column<int>(nullable: true),
                    Town_IV = table.Column<string>(nullable: true),
                    State_IV = table.Column<string>(nullable: true),
                    PostCode_IV = table.Column<string>(nullable: true),
                    FirstName_DV = table.Column<string>(nullable: true),
                    LastName_DV = table.Column<string>(nullable: true),
                    Email_DV = table.Column<string>(nullable: true),
                    Phone_DV = table.Column<string>(nullable: true),
                    Mobile_DV = table.Column<string>(nullable: true),
                    CECAccreditationNumber_DV = table.Column<string>(nullable: true),
                    ElectricalContractorsLicenseNumber_DV = table.Column<string>(nullable: true),
                    IsPostalAddress_DV = table.Column<bool>(nullable: false),
                    UnitTypeID_DV = table.Column<int>(nullable: true),
                    UnitNumber_DV = table.Column<string>(nullable: true),
                    StreetNumber_DV = table.Column<string>(nullable: true),
                    StreetName_DV = table.Column<string>(nullable: true),
                    StreetTypeID_DV = table.Column<int>(nullable: true),
                    Town_DV = table.Column<string>(nullable: true),
                    State_DV = table.Column<string>(nullable: true),
                    PostCode_DV = table.Column<string>(nullable: true),
                    CompanyName_JE = table.Column<string>(nullable: true),
                    FirstName_JE = table.Column<string>(nullable: true),
                    LastName_JE = table.Column<string>(nullable: true),
                    Email_JE = table.Column<string>(nullable: true),
                    Phone_JE = table.Column<string>(nullable: true),
                    Mobile_JE = table.Column<string>(nullable: true),
                    LicenseNumber_JE = table.Column<string>(nullable: true),
                    IsPostalAddress_JE = table.Column<bool>(nullable: false),
                    UnitTypeID_JE = table.Column<string>(nullable: true),
                    UnitNumber_JE = table.Column<string>(nullable: true),
                    StreetNumber_JE = table.Column<string>(nullable: true),
                    StreetName_JE = table.Column<string>(nullable: true),
                    StreetTypeID_JE = table.Column<string>(nullable: true),
                    Town_JE = table.Column<string>(nullable: true),
                    State_JE = table.Column<string>(nullable: true),
                    PostCode_JE = table.Column<string>(nullable: true),
                    Brand_LstJID = table.Column<string>(nullable: true),
                    Model_LstJID = table.Column<string>(nullable: true),
                    Series_LstJID = table.Column<string>(nullable: true),
                    NoOfInverter_LstJID = table.Column<string>(nullable: true),
                    Brand_LstJPD = table.Column<string>(nullable: true),
                    Model_LstJPD = table.Column<string>(nullable: true),
                    NoOfPanel_LstJPD = table.Column<int>(nullable: true),
                    STCStatus_JSTCD = table.Column<string>(nullable: true),
                    CalculatedSTC_JSTCD = table.Column<string>(nullable: true),
                    STCPrice_JSTCD = table.Column<string>(nullable: true),
                    FailureNotice_JSTCD = table.Column<string>(nullable: true),
                    ComplianceNotes_JSTCD = table.Column<string>(nullable: true),
                    STCSubmissionDate_JSTCD = table.Column<string>(nullable: true),
                    STCInvoiceStatus_JSTCD = table.Column<string>(nullable: true),
                    IsInvoiced_JSTCD = table.Column<bool>(nullable: false),
                    FieldName_LstCD = table.Column<string>(nullable: true),
                    FieldValue_LstCD = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    OrganizationId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GreenBoatDatas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GreenBoatSerialNumbers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    GreenBoatId = table.Column<int>(nullable: true),
                    SerialNumbers = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GreenBoatSerialNumbers", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GreenBoatDatas");

            migrationBuilder.DropTable(
                name: "GreenBoatSerialNumbers");
        }
    }
}
