﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class jobproductitems : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "JobId",
                table: "JobVariations",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "JobProducts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Quantity = table.Column<int>(nullable: true),
                    ProductItemId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobProducts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobProducts_ProductItems_ProductItemId",
                        column: x => x.ProductItemId,
                        principalTable: "ProductItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_JobVariations_JobId",
                table: "JobVariations",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_JobProducts_ProductItemId",
                table: "JobProducts",
                column: "ProductItemId");

            migrationBuilder.CreateIndex(
                name: "IX_JobProducts_TenantId",
                table: "JobProducts",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_JobVariations_Jobs_JobId",
                table: "JobVariations",
                column: "JobId",
                principalTable: "Jobs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobVariations_Jobs_JobId",
                table: "JobVariations");

            migrationBuilder.DropTable(
                name: "JobProducts");

            migrationBuilder.DropIndex(
                name: "IX_JobVariations_JobId",
                table: "JobVariations");

            migrationBuilder.DropColumn(
                name: "JobId",
                table: "JobVariations");
        }
    }
}
