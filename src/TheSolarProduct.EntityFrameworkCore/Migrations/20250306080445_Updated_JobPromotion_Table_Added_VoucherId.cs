﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Updated_JobPromotion_Table_Added_VoucherId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "VoucherId",
                table: "JobPromotions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_JobPromotions_VoucherId",
                table: "JobPromotions",
                column: "VoucherId");

            migrationBuilder.AddForeignKey(
                name: "FK_JobPromotions_VoucherMasters_VoucherId",
                table: "JobPromotions",
                column: "VoucherId",
                principalTable: "VoucherMasters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobPromotions_VoucherMasters_VoucherId",
                table: "JobPromotions");

            migrationBuilder.DropIndex(
                name: "IX_JobPromotions_VoucherId",
                table: "JobPromotions");

            migrationBuilder.DropColumn(
                name: "VoucherId",
                table: "JobPromotions");
        }
    }
}
