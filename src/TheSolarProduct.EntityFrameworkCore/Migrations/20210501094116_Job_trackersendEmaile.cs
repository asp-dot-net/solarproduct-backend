﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Job_trackersendEmaile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "GridConnectionEmailSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "GridConnectionEmailSendDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "GridConnectionSmsSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "GridConnectionSmsSendDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "JobActiveEmailSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "JobActiveEmailSendDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "JobActiveSmsSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "JobActiveSmsSendDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StcEmailSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "StcEmailSendDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "StcSmsSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "StcSmsSendDate",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GridConnectionEmailSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "GridConnectionEmailSendDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "GridConnectionSmsSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "GridConnectionSmsSendDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "JobActiveEmailSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "JobActiveEmailSendDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "JobActiveSmsSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "JobActiveSmsSendDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "StcEmailSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "StcEmailSendDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "StcSmsSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "StcSmsSendDate",
                table: "Jobs");
        }
    }
}
