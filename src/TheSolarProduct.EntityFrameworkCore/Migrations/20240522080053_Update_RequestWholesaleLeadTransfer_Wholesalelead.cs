﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_RequestWholesaleLeadTransfer_Wholesalelead : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RequestWholesaleLeadTransfers_Leads_LeadId",
                table: "RequestWholesaleLeadTransfers");

            migrationBuilder.AddForeignKey(
                name: "FK_RequestWholesaleLeadTransfers_WholeSaleLeads_LeadId",
                table: "RequestWholesaleLeadTransfers",
                column: "LeadId",
                principalTable: "WholeSaleLeads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RequestWholesaleLeadTransfers_WholeSaleLeads_LeadId",
                table: "RequestWholesaleLeadTransfers");

            migrationBuilder.AddForeignKey(
                name: "FK_RequestWholesaleLeadTransfers_Leads_LeadId",
                table: "RequestWholesaleLeadTransfers",
                column: "LeadId",
                principalTable: "Leads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
