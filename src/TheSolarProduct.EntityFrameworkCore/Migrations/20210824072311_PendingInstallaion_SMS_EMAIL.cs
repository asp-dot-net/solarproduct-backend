﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class PendingInstallaion_SMS_EMAIL : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "PendingInstallerEmailSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PendingInstallerEmailSendDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "PendingInstallerSmsSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PendingInstallerSmsSendDate",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PendingInstallerEmailSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "PendingInstallerEmailSendDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "PendingInstallerSmsSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "PendingInstallerSmsSendDate",
                table: "Jobs");
        }
    }
}
