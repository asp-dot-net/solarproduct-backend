﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class promotionmastertable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobPromotions_PromotionOffers_PromotionOfferId",
                table: "JobPromotions");

            migrationBuilder.DropIndex(
                name: "IX_JobPromotions_PromotionOfferId",
                table: "JobPromotions");

            migrationBuilder.DropColumn(
                name: "PromotionOfferId",
                table: "JobPromotions");

            migrationBuilder.AddColumn<int>(
                name: "PromotionMasterId",
                table: "JobPromotions",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PromotionMasters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    DisplayOrder = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PromotionMasters", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_JobPromotions_PromotionMasterId",
                table: "JobPromotions",
                column: "PromotionMasterId");

            migrationBuilder.CreateIndex(
                name: "IX_PromotionMasters_TenantId",
                table: "PromotionMasters",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_JobPromotions_PromotionMasters_PromotionMasterId",
                table: "JobPromotions",
                column: "PromotionMasterId",
                principalTable: "PromotionMasters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobPromotions_PromotionMasters_PromotionMasterId",
                table: "JobPromotions");

            migrationBuilder.DropTable(
                name: "PromotionMasters");

            migrationBuilder.DropIndex(
                name: "IX_JobPromotions_PromotionMasterId",
                table: "JobPromotions");

            migrationBuilder.DropColumn(
                name: "PromotionMasterId",
                table: "JobPromotions");

            migrationBuilder.AddColumn<int>(
                name: "PromotionOfferId",
                table: "JobPromotions",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_JobPromotions_PromotionOfferId",
                table: "JobPromotions",
                column: "PromotionOfferId");

            migrationBuilder.AddForeignKey(
                name: "FK_JobPromotions_PromotionOffers_PromotionOfferId",
                table: "JobPromotions",
                column: "PromotionOfferId",
                principalTable: "PromotionOffers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
