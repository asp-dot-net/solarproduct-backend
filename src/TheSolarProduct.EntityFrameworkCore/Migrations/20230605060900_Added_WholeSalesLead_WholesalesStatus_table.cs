﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_WholeSalesLead_WholesalesStatus_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WholeSaleStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WholeSaleStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WholeSaleLeads",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    ABNNumber = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    Mobile = table.Column<string>(nullable: true),
                    Talephone = table.Column<string>(nullable: true),
                    CustomerType = table.Column<string>(nullable: true),
                    WholeSaleStatusId = table.Column<int>(nullable: true),
                    PostalUnitNo = table.Column<string>(nullable: true),
                    PostalUnitType = table.Column<string>(nullable: true),
                    PostalStreetNo = table.Column<string>(nullable: true),
                    PostalStreetName = table.Column<string>(nullable: true),
                    PostalStreetType = table.Column<string>(nullable: true),
                    PostalStateId = table.Column<int>(nullable: true),
                    PostalPostCode = table.Column<string>(nullable: true),
                    PostalLatitude = table.Column<string>(nullable: true),
                    PostalLongitude = table.Column<string>(nullable: true),
                    DelivaryUnitNo = table.Column<string>(nullable: true),
                    DelivaryUnitType = table.Column<string>(nullable: true),
                    DelivaryStreetNo = table.Column<string>(nullable: true),
                    DelivaryStreetName = table.Column<string>(nullable: true),
                    DelivaryStreetType = table.Column<string>(nullable: true),
                    DelivaryStateId = table.Column<int>(nullable: true),
                    DelivaryPostCode = table.Column<string>(nullable: true),
                    DelivaryLatitude = table.Column<string>(nullable: true),
                    DelivaryLongitude = table.Column<string>(nullable: true),
                    BankName = table.Column<string>(nullable: true),
                    AccountName = table.Column<string>(nullable: true),
                    BSBNo = table.Column<string>(nullable: true),
                    AccoutNo = table.Column<string>(nullable: true),
                    CreditAmount = table.Column<decimal>(nullable: true),
                    CreditDays = table.Column<int>(nullable: true),
                    AssignUserId = table.Column<int>(nullable: true),
                    AssignDate = table.Column<DateTime>(nullable: true),
                    FirstAssignUserId = table.Column<int>(nullable: true),
                    FirstAssignDate = table.Column<DateTime>(nullable: true),
                    BDMId = table.Column<int>(nullable: true),
                    AditionalNotes = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WholeSaleLeads", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WholeSaleLeads_State_DelivaryStateId",
                        column: x => x.DelivaryStateId,
                        principalTable: "State",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WholeSaleLeads_State_PostalStateId",
                        column: x => x.PostalStateId,
                        principalTable: "State",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WholeSaleLeads_WholeSaleStatuses_WholeSaleStatusId",
                        column: x => x.WholeSaleStatusId,
                        principalTable: "WholeSaleStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WholeSaleLeads_DelivaryStateId",
                table: "WholeSaleLeads",
                column: "DelivaryStateId");

            migrationBuilder.CreateIndex(
                name: "IX_WholeSaleLeads_PostalStateId",
                table: "WholeSaleLeads",
                column: "PostalStateId");

            migrationBuilder.CreateIndex(
                name: "IX_WholeSaleLeads_WholeSaleStatusId",
                table: "WholeSaleLeads",
                column: "WholeSaleStatusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WholeSaleLeads");

            migrationBuilder.DropTable(
                name: "WholeSaleStatuses");
        }
    }
}
