﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Add_STCDetail_jobTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PVDNumber",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PVDStatus",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "STCAppliedDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "STCNotes",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "STCUploaddate",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PVDNumber",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "PVDStatus",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "STCAppliedDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "STCNotes",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "STCUploaddate",
                table: "Jobs");
        }
    }
}
