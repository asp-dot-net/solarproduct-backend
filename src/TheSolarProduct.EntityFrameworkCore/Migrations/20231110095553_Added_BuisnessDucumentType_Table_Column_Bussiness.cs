﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_BuisnessDucumentType_Table_Column_Bussiness : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "APCContactNo",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "APCEmail",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "APCFAX",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "APCFullName",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AddressDoc",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ApprovedAmount",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_Latitude2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_Longitude2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_PostCode2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "B_StateId2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_StreetName2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_StreetNo2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_StreetType2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_UnitNo2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_UnitType2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DP_BuisnessDocumentTypeId",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DP_BuisnessDocumentTypeId2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DP_DOB",
                table: "Businesses",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DP_DOB2",
                table: "Businesses",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DP_Email",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_Email2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_Latitude",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_Latitude2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_Longitude",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_Longitude2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_Mobile",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_Mobile2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_Name",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_Name2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_PassportCountry",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_PassportCountry2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_PassportNo",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_PassportNo2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_Phone",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_Phone2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_PostCode",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_PostCode2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DP_StateId",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DP_StateId2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_StreetName",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_StreetName2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_StreetNo",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_StreetNo2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_StreetType",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_StreetType2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_UnitNo",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_UnitNo2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_UnitType",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DP_UnitType2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GuarantorCompanyName",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GuarantorCompanyName2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GuarantorEmail",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GuarantorEmail2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GuarantorName",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GuarantorName2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GuarantorPhone",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GuarantorPhone2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LicencePassportDoc",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Notes",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PrimaryContactNo",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PrimaryEmail",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PrimaryFAX",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PrimaryFullName",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PrimaryPosition",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_Latitude2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_Longitude2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_PostCode2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "R_StateId2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_StreetName2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_StreetNo2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_StreetType2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_UnitNo2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_UnitType2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TradeCompanyName",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TradeCompanyName2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TradeCompanyName3",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TradeEmail",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TradeEmail2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TradeEmail3",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TradeFullName",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TradeFullName2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TradeFullName3",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TradePhone",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TradePhone2",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TradePhone3",
                table: "Businesses",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BuisnessDocumentTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    DocType = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuisnessDocumentTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Businesses_B_StateId2",
                table: "Businesses",
                column: "B_StateId2");

            migrationBuilder.CreateIndex(
                name: "IX_Businesses_DP_BuisnessDocumentTypeId",
                table: "Businesses",
                column: "DP_BuisnessDocumentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Businesses_DP_BuisnessDocumentTypeId2",
                table: "Businesses",
                column: "DP_BuisnessDocumentTypeId2");

            migrationBuilder.CreateIndex(
                name: "IX_Businesses_DP_StateId",
                table: "Businesses",
                column: "DP_StateId");

            migrationBuilder.CreateIndex(
                name: "IX_Businesses_DP_StateId2",
                table: "Businesses",
                column: "DP_StateId2");

            migrationBuilder.CreateIndex(
                name: "IX_Businesses_R_StateId2",
                table: "Businesses",
                column: "R_StateId2");

            migrationBuilder.AddForeignKey(
                name: "FK_Businesses_State_B_StateId2",
                table: "Businesses",
                column: "B_StateId2",
                principalTable: "State",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Businesses_BuisnessDocumentTypes_DP_BuisnessDocumentTypeId",
                table: "Businesses",
                column: "DP_BuisnessDocumentTypeId",
                principalTable: "BuisnessDocumentTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Businesses_BuisnessDocumentTypes_DP_BuisnessDocumentTypeId2",
                table: "Businesses",
                column: "DP_BuisnessDocumentTypeId2",
                principalTable: "BuisnessDocumentTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Businesses_State_DP_StateId",
                table: "Businesses",
                column: "DP_StateId",
                principalTable: "State",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Businesses_State_DP_StateId2",
                table: "Businesses",
                column: "DP_StateId2",
                principalTable: "State",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Businesses_State_R_StateId2",
                table: "Businesses",
                column: "R_StateId2",
                principalTable: "State",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Businesses_State_B_StateId2",
                table: "Businesses");

            migrationBuilder.DropForeignKey(
                name: "FK_Businesses_BuisnessDocumentTypes_DP_BuisnessDocumentTypeId",
                table: "Businesses");

            migrationBuilder.DropForeignKey(
                name: "FK_Businesses_BuisnessDocumentTypes_DP_BuisnessDocumentTypeId2",
                table: "Businesses");

            migrationBuilder.DropForeignKey(
                name: "FK_Businesses_State_DP_StateId",
                table: "Businesses");

            migrationBuilder.DropForeignKey(
                name: "FK_Businesses_State_DP_StateId2",
                table: "Businesses");

            migrationBuilder.DropForeignKey(
                name: "FK_Businesses_State_R_StateId2",
                table: "Businesses");

            migrationBuilder.DropTable(
                name: "BuisnessDocumentTypes");

            migrationBuilder.DropIndex(
                name: "IX_Businesses_B_StateId2",
                table: "Businesses");

            migrationBuilder.DropIndex(
                name: "IX_Businesses_DP_BuisnessDocumentTypeId",
                table: "Businesses");

            migrationBuilder.DropIndex(
                name: "IX_Businesses_DP_BuisnessDocumentTypeId2",
                table: "Businesses");

            migrationBuilder.DropIndex(
                name: "IX_Businesses_DP_StateId",
                table: "Businesses");

            migrationBuilder.DropIndex(
                name: "IX_Businesses_DP_StateId2",
                table: "Businesses");

            migrationBuilder.DropIndex(
                name: "IX_Businesses_R_StateId2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "APCContactNo",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "APCEmail",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "APCFAX",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "APCFullName",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "AddressDoc",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "ApprovedAmount",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_Latitude2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_Longitude2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_PostCode2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_StateId2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_StreetName2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_StreetNo2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_StreetType2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_UnitNo2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_UnitType2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_BuisnessDocumentTypeId",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_BuisnessDocumentTypeId2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_DOB",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_DOB2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_Email",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_Email2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_Latitude",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_Latitude2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_Longitude",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_Longitude2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_Mobile",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_Mobile2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_Name",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_Name2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_PassportCountry",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_PassportCountry2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_PassportNo",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_PassportNo2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_Phone",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_Phone2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_PostCode",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_PostCode2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_StateId",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_StateId2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_StreetName",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_StreetName2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_StreetNo",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_StreetNo2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_StreetType",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_StreetType2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_UnitNo",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_UnitNo2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_UnitType",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "DP_UnitType2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "GuarantorCompanyName",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "GuarantorCompanyName2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "GuarantorEmail",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "GuarantorEmail2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "GuarantorName",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "GuarantorName2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "GuarantorPhone",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "GuarantorPhone2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "LicencePassportDoc",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "Notes",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "PrimaryContactNo",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "PrimaryEmail",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "PrimaryFAX",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "PrimaryFullName",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "PrimaryPosition",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_Latitude2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_Longitude2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_PostCode2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_StateId2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_StreetName2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_StreetNo2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_StreetType2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_UnitNo2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_UnitType2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "TradeCompanyName",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "TradeCompanyName2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "TradeCompanyName3",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "TradeEmail",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "TradeEmail2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "TradeEmail3",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "TradeFullName",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "TradeFullName2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "TradeFullName3",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "TradePhone",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "TradePhone2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "TradePhone3",
                table: "Businesses");
        }
    }
}
