﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class service_LeadId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LeadId",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ServiceCategoryName",
                table: "Services",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Services_LeadId",
                table: "Services",
                column: "LeadId");

            migrationBuilder.AddForeignKey(
                name: "FK_Services_Leads_LeadId",
                table: "Services",
                column: "LeadId",
                principalTable: "Leads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Services_Leads_LeadId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Services_LeadId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "LeadId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "ServiceCategoryName",
                table: "Services");
        }
    }
}
