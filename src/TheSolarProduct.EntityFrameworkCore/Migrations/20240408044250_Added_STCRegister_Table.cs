﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_STCRegister_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            

            migrationBuilder.CreateTable(
                name: "STCRegisters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TradingName = table.Column<string>(nullable: true),
                    ABNNumber = table.Column<string>(nullable: true),
                    UnitNo = table.Column<string>(nullable: true),
                    UnitType = table.Column<string>(nullable: true),
                    StreetNo = table.Column<string>(nullable: true),
                    StreetName = table.Column<string>(nullable: true),
                    StreetType = table.Column<string>(nullable: true),
                    StateId = table.Column<int>(nullable: true),
                    PostCode = table.Column<string>(nullable: true),
                    Latitude = table.Column<string>(nullable: true),
                    Longitude = table.Column<string>(nullable: true),
                    PhoneNo = table.Column<string>(nullable: true),
                    EmailForRemittances = table.Column<string>(nullable: true),
                    PrimaryEmail = table.Column<string>(nullable: true),
                    InstallerFullName = table.Column<string>(nullable: true),
                    Installer_DesignerEmail = table.Column<string>(nullable: true),
                    Installer_LicenceNumber = table.Column<string>(nullable: true),
                    InstallerUnitNo = table.Column<string>(nullable: true),
                    InstallerUnitType = table.Column<string>(nullable: true),
                    InstallerStreetNo = table.Column<string>(nullable: true),
                    InstallerStreetName = table.Column<string>(nullable: true),
                    InstallerStreetType = table.Column<string>(nullable: true),
                    InstallerStateId = table.Column<int>(nullable: true),
                    InstallerPostCode = table.Column<string>(nullable: true),
                    InstallerLatitude = table.Column<string>(nullable: true),
                    InstallerLongitude = table.Column<string>(nullable: true),
                    InstallerPhone = table.Column<string>(nullable: true),
                    InstallerFullName2 = table.Column<string>(nullable: true),
                    Installer_DesignerEmail2 = table.Column<string>(nullable: true),
                    Installer_LicenceNumber2 = table.Column<string>(nullable: true),
                    InstallerUnitNo2 = table.Column<string>(nullable: true),
                    InstallerUnitType2 = table.Column<string>(nullable: true),
                    InstallerStreetNo2 = table.Column<string>(nullable: true),
                    InstallerStreetName2 = table.Column<string>(nullable: true),
                    InstallerStreetType2 = table.Column<string>(nullable: true),
                    InstallerStateId2 = table.Column<int>(nullable: true),
                    InstallerPostCode2 = table.Column<string>(nullable: true),
                    InstallerLatitude2 = table.Column<string>(nullable: true),
                    InstallerLongitude2 = table.Column<string>(nullable: true),
                    InstallerPhone2 = table.Column<string>(nullable: true),
                    Claim = table.Column<string>(nullable: true),
                    PublicLiabilityCertificate = table.Column<string>(nullable: true),
                    ProductLiabilityCertificate = table.Column<string>(nullable: true),
                    PhotoId = table.Column<string>(nullable: true),
                    ASICCertificate = table.Column<string>(nullable: true),
                    AccountName = table.Column<string>(nullable: true),
                    BSB = table.Column<string>(nullable: true),
                    AccountNumber = table.Column<string>(nullable: true),
                    FindUs = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STCRegisters", x => x.Id);
                    table.ForeignKey(
                        name: "FK_STCRegisters_State_InstallerStateId",
                        column: x => x.InstallerStateId,
                        principalTable: "State",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_STCRegisters_State_InstallerStateId2",
                        column: x => x.InstallerStateId2,
                        principalTable: "State",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_STCRegisters_State_StateId",
                        column: x => x.StateId,
                        principalTable: "State",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

          

            migrationBuilder.CreateIndex(
                name: "IX_STCRegisters_InstallerStateId",
                table: "STCRegisters",
                column: "InstallerStateId");

            migrationBuilder.CreateIndex(
                name: "IX_STCRegisters_InstallerStateId2",
                table: "STCRegisters",
                column: "InstallerStateId2");

            migrationBuilder.CreateIndex(
                name: "IX_STCRegisters_StateId",
                table: "STCRegisters",
                column: "StateId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "STCRegisters");
        }
    }
}
