﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Organization_WestPackScreteAndPubKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "WestPacPublishKey",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WestPacSecreteKey",
                table: "AbpOrganizationUnits",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WestPacPublishKey",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "WestPacSecreteKey",
                table: "AbpOrganizationUnits");
        }
    }
}
