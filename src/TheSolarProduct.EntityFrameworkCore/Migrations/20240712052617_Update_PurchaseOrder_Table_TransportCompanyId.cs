﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_PurchaseOrder_Table_TransportCompanyId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PurchaseOrders_TransportCompanies_IncoTermId",
                table: "PurchaseOrders");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrders_TransportCompanyId",
                table: "PurchaseOrders",
                column: "TransportCompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_PurchaseOrders_TransportCompanies_TransportCompanyId",
                table: "PurchaseOrders",
                column: "TransportCompanyId",
                principalTable: "TransportCompanies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PurchaseOrders_TransportCompanies_TransportCompanyId",
                table: "PurchaseOrders");

            migrationBuilder.DropIndex(
                name: "IX_PurchaseOrders_TransportCompanyId",
                table: "PurchaseOrders");

            migrationBuilder.AddForeignKey(
                name: "FK_PurchaseOrders_TransportCompanies_IncoTermId",
                table: "PurchaseOrders",
                column: "IncoTermId",
                principalTable: "TransportCompanies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
