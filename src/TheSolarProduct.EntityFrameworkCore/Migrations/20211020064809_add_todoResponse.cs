﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class add_todoResponse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsTodoComplete",
                table: "LeadActivityLog",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "TodoDueDate",
                table: "LeadActivityLog",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TodoResponse",
                table: "LeadActivityLog",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TodoTag",
                table: "LeadActivityLog",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "TodoresponseTime",
                table: "LeadActivityLog",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsTodoComplete",
                table: "LeadActivityLog");

            migrationBuilder.DropColumn(
                name: "TodoDueDate",
                table: "LeadActivityLog");

            migrationBuilder.DropColumn(
                name: "TodoResponse",
                table: "LeadActivityLog");

            migrationBuilder.DropColumn(
                name: "TodoTag",
                table: "LeadActivityLog");

            migrationBuilder.DropColumn(
                name: "TodoresponseTime",
                table: "LeadActivityLog");
        }
    }
}
