﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class JobPromotion_sendEmaile_SmsandEmailDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "EmailSend",
                table: "JobPromotions",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EmailSendDate",
                table: "JobPromotions",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SmsSendDate",
                table: "JobPromotions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmailSend",
                table: "JobPromotions");

            migrationBuilder.DropColumn(
                name: "EmailSendDate",
                table: "JobPromotions");

            migrationBuilder.DropColumn(
                name: "SmsSendDate",
                table: "JobPromotions");
        }
    }
}
