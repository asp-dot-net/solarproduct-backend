﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_QuotationDetails_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QuotationDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    JobId = table.Column<int>(nullable: true),
                    QuotationId = table.Column<int>(nullable: true),
                    QuoteNumber = table.Column<string>(nullable: true),
                    Date = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Mobile = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Address1 = table.Column<string>(nullable: true),
                    Address2 = table.Column<string>(nullable: true),
                    Capecity = table.Column<string>(nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    InstName = table.Column<string>(nullable: true),
                    InstMobile = table.Column<string>(nullable: true),
                    InstEmail = table.Column<string>(nullable: true),
                    SubTotal = table.Column<string>(nullable: true),
                    STC = table.Column<string>(nullable: true),
                    STCDesc = table.Column<string>(nullable: true),
                    GrandTotal = table.Column<string>(nullable: true),
                    DepositeText = table.Column<string>(nullable: true),
                    Deposite = table.Column<string>(nullable: true),
                    Total = table.Column<string>(nullable: true),
                    Balance = table.Column<string>(nullable: true),
                    MeaterPhase = table.Column<string>(nullable: true),
                    PaymentMethod = table.Column<string>(nullable: true),
                    SpecialDiscount = table.Column<string>(nullable: true),
                    AdditionalCharges = table.Column<string>(nullable: true),
                    MeaterUpgrade = table.Column<string>(nullable: true),
                    NoofStory = table.Column<string>(nullable: true),
                    RoofTypes = table.Column<string>(nullable: true),
                    RoofPinch = table.Column<string>(nullable: true),
                    EnergyDist = table.Column<string>(nullable: true),
                    EnergyRetailer = table.Column<string>(nullable: true),
                    NMINumber = table.Column<string>(nullable: true),
                    NearMap1 = table.Column<string>(nullable: true),
                    NearMap2 = table.Column<string>(nullable: true),
                    NearMap3 = table.Column<string>(nullable: true),
                    DepositRequired = table.Column<string>(nullable: true),
                    STCIncetive = table.Column<string>(nullable: true),
                    SolarVICRebate = table.Column<string>(nullable: true),
                    SolarVICLoanDiscount = table.Column<string>(nullable: true),
                    FinalPrice = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuotationDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuotationDetails_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_QuotationDetails_Quotations_QuotationId",
                        column: x => x.QuotationId,
                        principalTable: "Quotations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "QuotationQunityAndModelDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    QuotationDetailId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuotationQunityAndModelDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuotationQunityAndModelDetails_QuotationDetails_QuotationDetailId",
                        column: x => x.QuotationDetailId,
                        principalTable: "QuotationDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_QuotationDetails_JobId",
                table: "QuotationDetails",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_QuotationDetails_QuotationId",
                table: "QuotationDetails",
                column: "QuotationId");

            migrationBuilder.CreateIndex(
                name: "IX_QuotationQunityAndModelDetails_QuotationDetailId",
                table: "QuotationQunityAndModelDetails",
                column: "QuotationDetailId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QuotationQunityAndModelDetails");

            migrationBuilder.DropTable(
                name: "QuotationDetails");
        }
    }
}
