﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Greenboatfetchuserpw : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "GreenBoatPasswordForFetch",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GreenBoatUsernameForFetch",
                table: "AbpOrganizationUnits",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GreenBoatPasswordForFetch",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "GreenBoatUsernameForFetch",
                table: "AbpOrganizationUnits");
        }
    }
}
