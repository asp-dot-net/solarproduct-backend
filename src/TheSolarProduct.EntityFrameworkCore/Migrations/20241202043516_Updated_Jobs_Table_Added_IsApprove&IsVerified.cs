﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Updated_Jobs_Table_Added_IsApproveIsVerified : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "CommitionIsApprove",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "CommitionIsVerified",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CommitionIsApprove",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "CommitionIsVerified",
                table: "Jobs");
        }
    }
}
