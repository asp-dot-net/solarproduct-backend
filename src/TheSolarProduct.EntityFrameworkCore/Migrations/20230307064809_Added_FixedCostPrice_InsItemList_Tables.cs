﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_FixedCostPrice_InsItemList_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FixedCostPrice",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    Cost = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FixedCostPrice", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InstallationItemPeriods",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrganizationUnit = table.Column<long>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstallationItemPeriods", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InstallationItemPeriods_AbpOrganizationUnits_OrganizationUnit",
                        column: x => x.OrganizationUnit,
                        principalTable: "AbpOrganizationUnits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InstallationItemLists",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InstallationItemPeriodId = table.Column<int>(nullable: false),
                    ProductItemId = table.Column<int>(nullable: false),
                    PricePerWatt = table.Column<decimal>(nullable: true),
                    UnitPrice = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstallationItemLists", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InstallationItemLists_InstallationItemPeriods_InstallationItemPeriodId",
                        column: x => x.InstallationItemPeriodId,
                        principalTable: "InstallationItemPeriods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InstallationItemLists_ProductItems_ProductItemId",
                        column: x => x.ProductItemId,
                        principalTable: "ProductItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InstallationItemLists_InstallationItemPeriodId",
                table: "InstallationItemLists",
                column: "InstallationItemPeriodId");

            migrationBuilder.CreateIndex(
                name: "IX_InstallationItemLists_ProductItemId",
                table: "InstallationItemLists",
                column: "ProductItemId");

            migrationBuilder.CreateIndex(
                name: "IX_InstallationItemPeriods_OrganizationUnit",
                table: "InstallationItemPeriods",
                column: "OrganizationUnit");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FixedCostPrice");

            migrationBuilder.DropTable(
                name: "InstallationItemLists");

            migrationBuilder.DropTable(
                name: "InstallationItemPeriods");
        }
    }
}
