﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_isactive_column_DocumentType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InvoiceImportDatas_Jobs_JobId",
                table: "InvoiceImportDatas");

            migrationBuilder.AlterColumn<int>(
                name: "JobId",
                table: "InvoiceImportDatas",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "DocumentTypes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddForeignKey(
                name: "FK_InvoiceImportDatas_Jobs_JobId",
                table: "InvoiceImportDatas",
                column: "JobId",
                principalTable: "Jobs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InvoiceImportDatas_Jobs_JobId",
                table: "InvoiceImportDatas");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "DocumentTypes");

            migrationBuilder.AlterColumn<int>(
                name: "JobId",
                table: "InvoiceImportDatas",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_InvoiceImportDatas_Jobs_JobId",
                table: "InvoiceImportDatas",
                column: "JobId",
                principalTable: "Jobs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
