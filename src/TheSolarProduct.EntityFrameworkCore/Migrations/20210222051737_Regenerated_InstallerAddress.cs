﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_InstallerAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IsGoogle",
                table: "InstallerAddresses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "latitude",
                table: "InstallerAddresses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "longitude",
                table: "InstallerAddresses",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsGoogle",
                table: "InstallerAddresses");

            migrationBuilder.DropColumn(
                name: "latitude",
                table: "InstallerAddresses");

            migrationBuilder.DropColumn(
                name: "longitude",
                table: "InstallerAddresses");
        }
    }
}
