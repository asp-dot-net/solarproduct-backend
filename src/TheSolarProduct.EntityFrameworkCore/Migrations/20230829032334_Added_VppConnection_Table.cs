﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_VppConnection_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "VppConnectionId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "VppConections",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VppConections", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_VppConnectionId",
                table: "Jobs",
                column: "VppConnectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_VppConections_VppConnectionId",
                table: "Jobs",
                column: "VppConnectionId",
                principalTable: "VppConections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_VppConections_VppConnectionId",
                table: "Jobs");

            migrationBuilder.DropTable(
                name: "VppConections");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_VppConnectionId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "VppConnectionId",
                table: "Jobs");
        }
    }
}
