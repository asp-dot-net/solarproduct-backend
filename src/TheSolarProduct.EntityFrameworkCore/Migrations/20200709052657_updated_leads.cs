﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class updated_leads : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Leads_LeadSources_LeadSourceId",
                table: "Leads");

            migrationBuilder.DropForeignKey(
                name: "FK_Leads_State_StateId",
                table: "Leads");

            migrationBuilder.DropForeignKey(
                name: "FK_Leads_PostCodes_SuburbId",
                table: "Leads");

            migrationBuilder.DropIndex(
                name: "IX_Leads_LeadSourceId",
                table: "Leads");

            migrationBuilder.DropIndex(
                name: "IX_Leads_StateId",
                table: "Leads");

            migrationBuilder.DropIndex(
                name: "IX_Leads_SuburbId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "CopanyName",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "LeadSourceId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "StateId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "SuburbId",
                table: "Leads");

            migrationBuilder.AlterColumn<string>(
                name: "UnitType",
                table: "Leads",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UnitNo",
                table: "Leads",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "StreetType",
                table: "Leads",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "StreetNo",
                table: "Leads",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "StreetName",
                table: "Leads",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Requirements",
                table: "Leads",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(500)",
                oldMaxLength: 500,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Phone",
                table: "Leads",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(10)",
                oldMaxLength: 10,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Mobile",
                table: "Leads",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(10)",
                oldMaxLength: 10,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Leads",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Address",
                table: "Leads",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(300)",
                oldMaxLength: 300,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ABN",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AltPhone",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AngleType",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Area",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyName",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Fax",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HouseAgeType",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeadSource",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalAddress",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalPostCode",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalState",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalStreetNo",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalStreetType",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalSuburb",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalUnitNo",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalUnitType",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RoofType",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SolarType",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StoryType",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Suburb",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SystemType",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "Leads",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ABN",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "AltPhone",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "AngleType",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "Area",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "CompanyName",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "Fax",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "HouseAgeType",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "LeadSource",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalAddress",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalPostCode",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalState",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalStreetNo",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalStreetType",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalSuburb",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalUnitNo",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalUnitType",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "RoofType",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "SolarType",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "State",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "StoryType",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "Suburb",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "SystemType",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "Leads");

            migrationBuilder.AlterColumn<string>(
                name: "UnitType",
                table: "Leads",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UnitNo",
                table: "Leads",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "StreetType",
                table: "Leads",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "StreetNo",
                table: "Leads",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "StreetName",
                table: "Leads",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Requirements",
                table: "Leads",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Phone",
                table: "Leads",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Mobile",
                table: "Leads",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Leads",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Address",
                table: "Leads",
                type: "nvarchar(300)",
                maxLength: 300,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CopanyName",
                table: "Leads",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LeadSourceId",
                table: "Leads",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StateId",
                table: "Leads",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SuburbId",
                table: "Leads",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Leads_LeadSourceId",
                table: "Leads",
                column: "LeadSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_StateId",
                table: "Leads",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_SuburbId",
                table: "Leads",
                column: "SuburbId");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_LeadSources_LeadSourceId",
                table: "Leads",
                column: "LeadSourceId",
                principalTable: "LeadSources",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_State_StateId",
                table: "Leads",
                column: "StateId",
                principalTable: "State",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_PostCodes_SuburbId",
                table: "Leads",
                column: "SuburbId",
                principalTable: "PostCodes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
