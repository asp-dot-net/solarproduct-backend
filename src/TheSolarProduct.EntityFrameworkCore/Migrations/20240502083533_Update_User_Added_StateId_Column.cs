﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_User_Added_StateId_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StateId",
                table: "AbpUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AbpUsers_StateId",
                table: "AbpUsers",
                column: "StateId");

            migrationBuilder.AddForeignKey(
                name: "FK_AbpUsers_State_StateId",
                table: "AbpUsers",
                column: "StateId",
                principalTable: "State",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AbpUsers_State_StateId",
                table: "AbpUsers");

            migrationBuilder.DropIndex(
                name: "IX_AbpUsers_StateId",
                table: "AbpUsers");

            migrationBuilder.DropColumn(
                name: "StateId",
                table: "AbpUsers");
        }
    }
}
