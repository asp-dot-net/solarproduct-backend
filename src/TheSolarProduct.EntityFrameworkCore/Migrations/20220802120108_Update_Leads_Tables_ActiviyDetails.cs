﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_Leads_Tables_ActiviyDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ActivityDate",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ActivityDescription",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ActivityNote",
                table: "Leads",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActivityDate",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "ActivityDescription",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "ActivityNote",
                table: "Leads");
        }
    }
}
