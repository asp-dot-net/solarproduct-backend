﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class RemovePromoIdSecIdColumns_MyInstallerActivityLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PromotionId",
                table: "MyInstallerActivityLog");

            migrationBuilder.DropColumn(
                name: "PromotionUserId",
                table: "MyInstallerActivityLog");

            migrationBuilder.DropColumn(
                name: "SectionId",
                table: "MyInstallerActivityLog");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PromotionId",
                table: "MyInstallerActivityLog",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PromotionUserId",
                table: "MyInstallerActivityLog",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SectionId",
                table: "MyInstallerActivityLog",
                type: "int",
                nullable: true);
        }
    }
}
