﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class ChangeColumns_InstallerDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AreaName",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "SourceTypeId",
                table: "InstallerDetails");

            migrationBuilder.AddColumn<string>(
                name: "AreaName",
                table: "AbpUsers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SourceTypeId",
                table: "AbpUsers",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AreaName",
                table: "AbpUsers");

            migrationBuilder.DropColumn(
                name: "SourceTypeId",
                table: "AbpUsers");

            migrationBuilder.AddColumn<string>(
                name: "AreaName",
                table: "InstallerDetails",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SourceTypeId",
                table: "InstallerDetails",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
