﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class jobtablenewcolumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AdditionalComments",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ApplicationRefNo",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DistAppliedDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MeterNumber",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdditionalComments",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ApplicationRefNo",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DistAppliedDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "MeterNumber",
                table: "Jobs");
        }
    }
}
