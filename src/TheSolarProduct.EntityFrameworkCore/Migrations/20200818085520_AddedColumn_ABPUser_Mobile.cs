﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class AddedColumn_ABPUser_Mobile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "State",
                table: "InstallerAddresses");

            migrationBuilder.AddColumn<int>(
                name: "StateId",
                table: "InstallerAddresses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Mobile",
                table: "AbpUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StateId",
                table: "InstallerAddresses");

            migrationBuilder.DropColumn(
                name: "Mobile",
                table: "AbpUsers");

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "InstallerAddresses",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
