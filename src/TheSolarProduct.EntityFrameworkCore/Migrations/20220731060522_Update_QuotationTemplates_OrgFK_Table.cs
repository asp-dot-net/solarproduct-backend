﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_QuotationTemplates_OrgFK_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "OrganizationUnitId",
                table: "QuotationTemplates",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_QuotationTemplates_OrganizationUnitId",
                table: "QuotationTemplates",
                column: "OrganizationUnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_QuotationTemplates_AbpOrganizationUnits_OrganizationUnitId",
                table: "QuotationTemplates",
                column: "OrganizationUnitId",
                principalTable: "AbpOrganizationUnits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_QuotationTemplates_AbpOrganizationUnits_OrganizationUnitId",
                table: "QuotationTemplates");

            migrationBuilder.DropIndex(
                name: "IX_QuotationTemplates_OrganizationUnitId",
                table: "QuotationTemplates");

            migrationBuilder.AlterColumn<int>(
                name: "OrganizationUnitId",
                table: "QuotationTemplates",
                type: "int",
                nullable: false,
                oldClrType: typeof(long));
        }
    }
}
