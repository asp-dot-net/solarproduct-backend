﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_InstallationCost_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "StateWiseInstallationCosts",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "StateWiseInstallationCosts",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "StateWiseInstallationCosts",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "StateWiseInstallationCosts",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "StateWiseInstallationCosts",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "StateWiseInstallationCosts",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "StateWiseInstallationCosts",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "OtherCharges",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "OtherCharges",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "OtherCharges",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "OtherCharges",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "OtherCharges",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "OtherCharges",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "OtherCharges",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "InstallationItemPeriods",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "InstallationItemPeriods",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "InstallationItemPeriods",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "InstallationItemPeriods",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "InstallationItemPeriods",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "InstallationItemPeriods",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "InstallationItemPeriods",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "InstallationItemLists",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "InstallationItemLists",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "InstallationItemLists",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "InstallationItemLists",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "InstallationItemLists",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "InstallationItemLists",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "InstallationItemLists",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "FixedCostPrice",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "FixedCostPrice",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "FixedCostPrice",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "FixedCostPrice",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "FixedCostPrice",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "FixedCostPrice",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "FixedCostPrice",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "StateWiseInstallationCosts");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "StateWiseInstallationCosts");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "StateWiseInstallationCosts");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "StateWiseInstallationCosts");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "StateWiseInstallationCosts");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "StateWiseInstallationCosts");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "StateWiseInstallationCosts");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "OtherCharges");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "OtherCharges");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "OtherCharges");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "OtherCharges");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "OtherCharges");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "OtherCharges");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "OtherCharges");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "InstallationItemPeriods");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "InstallationItemPeriods");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "InstallationItemPeriods");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "InstallationItemPeriods");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "InstallationItemPeriods");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "InstallationItemPeriods");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "InstallationItemPeriods");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "InstallationItemLists");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "InstallationItemLists");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "InstallationItemLists");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "InstallationItemLists");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "InstallationItemLists");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "InstallationItemLists");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "InstallationItemLists");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "FixedCostPrice");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "FixedCostPrice");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "FixedCostPrice");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "FixedCostPrice");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "FixedCostPrice");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "FixedCostPrice");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "FixedCostPrice");
        }
    }
}
