﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_iscompletecolumn_Job : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "iscomplete",
                table: "Jobs");

            migrationBuilder.AddColumn<int>(
                name: "PostInstallationStatus",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PostInstallationStatus",
                table: "Jobs");

            migrationBuilder.AddColumn<bool>(
                name: "iscomplete",
                table: "Jobs",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
