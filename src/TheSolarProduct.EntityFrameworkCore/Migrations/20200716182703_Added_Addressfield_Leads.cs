﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_Addressfield_Leads : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IsGoogle",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalIsGoogle",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Postallatitude",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Postallongitude",
                table: "Leads",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsGoogle",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostalIsGoogle",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "Postallatitude",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "Postallongitude",
                table: "Leads");
        }
    }
}
