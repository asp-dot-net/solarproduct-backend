﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class addwestpackdetailcoloumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MerchantId",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PublishKey",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SecrateKey",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "businesscode",
                table: "AbpOrganizationUnits",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MerchantId",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "PublishKey",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "SecrateKey",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "businesscode",
                table: "AbpOrganizationUnits");
        }
    }
}
