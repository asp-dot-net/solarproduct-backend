﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_Job_Remove_FK_MeterPhase_MeterUpgrade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_MeterPhases_MeterPhaseId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_MeterUpgrades_MeterUpgradeId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_MeterPhaseId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_MeterUpgradeId",
                table: "Jobs");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Jobs_MeterPhaseId",
                table: "Jobs",
                column: "MeterPhaseId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_MeterUpgradeId",
                table: "Jobs",
                column: "MeterUpgradeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_MeterPhases_MeterPhaseId",
                table: "Jobs",
                column: "MeterPhaseId",
                principalTable: "MeterPhases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_MeterUpgrades_MeterUpgradeId",
                table: "Jobs",
                column: "MeterUpgradeId",
                principalTable: "MeterUpgrades",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
