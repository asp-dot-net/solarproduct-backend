﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_Job_Added_PaymentTerm_RepaymentAmt_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PaymentTerm",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "RepaymentAmt",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PaymentTerm",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "RepaymentAmt",
                table: "Jobs");
        }
    }
}
