﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class jobrefund : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RefundReasonId",
                table: "JobRefunds",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "RefundReasons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenantId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefundReasons", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_JobRefunds_RefundReasonId",
                table: "JobRefunds",
                column: "RefundReasonId");

            migrationBuilder.CreateIndex(
                name: "IX_RefundReasons_TenantId",
                table: "RefundReasons",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_JobRefunds_RefundReasons_RefundReasonId",
                table: "JobRefunds",
                column: "RefundReasonId",
                principalTable: "RefundReasons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobRefunds_RefundReasons_RefundReasonId",
                table: "JobRefunds");

            migrationBuilder.DropTable(
                name: "RefundReasons");

            migrationBuilder.DropIndex(
                name: "IX_JobRefunds_RefundReasonId",
                table: "JobRefunds");

            migrationBuilder.DropColumn(
                name: "RefundReasonId",
                table: "JobRefunds");
        }
    }
}
