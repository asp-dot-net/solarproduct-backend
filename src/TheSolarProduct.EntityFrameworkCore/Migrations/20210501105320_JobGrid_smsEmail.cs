﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class JobGrid_smsEmail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "JobGridEmailSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "JobGridEmailSendDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "JobGridSmsSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "JobGridSmsSendDate",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "JobGridEmailSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "JobGridEmailSendDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "JobGridSmsSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "JobGridSmsSendDate",
                table: "Jobs");
        }
    }
}
