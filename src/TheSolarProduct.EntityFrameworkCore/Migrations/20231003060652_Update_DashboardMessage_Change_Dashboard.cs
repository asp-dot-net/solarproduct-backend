﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_DashboardMessage_Change_Dashboard : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BashboardType",
                table: "DashboardMessages");

            migrationBuilder.AddColumn<string>(
                name: "DashboardType",
                table: "DashboardMessages",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DashboardType",
                table: "DashboardMessages");

            migrationBuilder.AddColumn<string>(
                name: "BashboardType",
                table: "DashboardMessages",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
