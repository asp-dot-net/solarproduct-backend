﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class add_stateforexpense : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LeadinvestmentId",
                table: "LeadExpenses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StateId",
                table: "LeadExpenses",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LeadExpenses_LeadinvestmentId",
                table: "LeadExpenses",
                column: "LeadinvestmentId");

            migrationBuilder.CreateIndex(
                name: "IX_LeadExpenses_StateId",
                table: "LeadExpenses",
                column: "StateId");

            migrationBuilder.AddForeignKey(
                name: "FK_LeadExpenses_LeadExpenseInvestments_LeadinvestmentId",
                table: "LeadExpenses",
                column: "LeadinvestmentId",
                principalTable: "LeadExpenseInvestments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_LeadExpenses_State_StateId",
                table: "LeadExpenses",
                column: "StateId",
                principalTable: "State",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeadExpenses_LeadExpenseInvestments_LeadinvestmentId",
                table: "LeadExpenses");

            migrationBuilder.DropForeignKey(
                name: "FK_LeadExpenses_State_StateId",
                table: "LeadExpenses");

            migrationBuilder.DropIndex(
                name: "IX_LeadExpenses_LeadinvestmentId",
                table: "LeadExpenses");

            migrationBuilder.DropIndex(
                name: "IX_LeadExpenses_StateId",
                table: "LeadExpenses");

            migrationBuilder.DropColumn(
                name: "LeadinvestmentId",
                table: "LeadExpenses");

            migrationBuilder.DropColumn(
                name: "StateId",
                table: "LeadExpenses");
        }
    }
}
