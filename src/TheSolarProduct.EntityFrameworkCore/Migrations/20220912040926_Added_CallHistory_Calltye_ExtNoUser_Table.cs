﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_CallHistory_Calltye_ExtNoUser_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ExtensionNumber",
                table: "AbpUsers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CallHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CallType = table.Column<string>(nullable: true),
                    Number = table.Column<string>(nullable: true),
                    CallDirection = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    EntityId = table.Column<string>(nullable: true),
                    EntityType = table.Column<string>(nullable: true),
                    Agent = table.Column<string>(nullable: true),
                    AgentFirstName = table.Column<string>(nullable: true),
                    AgentLastName = table.Column<string>(nullable: true),
                    AgentEmail = table.Column<string>(nullable: true),
                    Duration = table.Column<string>(nullable: true),
                    DurationTimeSpan = table.Column<string>(nullable: true),
                    DateTime = table.Column<DateTime>(nullable: false),
                    CallStartTimeLocal = table.Column<DateTime>(nullable: false),
                    CallStartTimeUTC = table.Column<DateTime>(nullable: false),
                    CallEstablishedTimeLocal = table.Column<DateTime>(nullable: false),
                    CallEstablishedTimeUTC = table.Column<DateTime>(nullable: false),
                    CallEndTimeLocal = table.Column<DateTime>(nullable: false),
                    CallEndTimeUTC = table.Column<DateTime>(nullable: false),
                    CallStartTimeLocalMillis = table.Column<string>(nullable: true),
                    CallStartTimeUTCMillis = table.Column<string>(nullable: true),
                    CallEstablishedTimeLocalMillis = table.Column<string>(nullable: true),
                    CallEstablishedTimeUTCMillis = table.Column<string>(nullable: true),
                    CallEndTimeLocalMillis = table.Column<string>(nullable: true),
                    CallEndTimeUTCMillis = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CallHistory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CallTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    IconClass = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CallTypes", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CallHistory");

            migrationBuilder.DropTable(
                name: "CallTypes");

            migrationBuilder.DropColumn(
                name: "ExtensionNumber",
                table: "AbpUsers");
        }
    }
}
