﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Installer_inVoice_smssendandemailsend : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "EmaiSendDate",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SmsSend",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SmsSendDate",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "installerEmailSend",
                table: "InstallerInvoices",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmaiSendDate",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "SmsSend",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "SmsSendDate",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "installerEmailSend",
                table: "InstallerInvoices");
        }
    }
}
