﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class add_priorty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Todopriority",
                table: "LeadActivityLog",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TodopriorityId",
                table: "LeadActivityLog",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Todopriority",
                table: "LeadActivityLog");

            migrationBuilder.DropColumn(
                name: "TodopriorityId",
                table: "LeadActivityLog");
        }
    }
}
