﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_WholeSaleLead_Added_ActivityDescription_ActivityComment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ActivityComment",
                table: "WholeSaleLeads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ActivityDescription",
                table: "WholeSaleLeads",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActivityComment",
                table: "WholeSaleLeads");

            migrationBuilder.DropColumn(
                name: "ActivityDescription",
                table: "WholeSaleLeads");
        }
    }
}
