﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_BrandPartner_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BrandName",
                table: "EcommerceProductItems");

            migrationBuilder.AddColumn<int>(
                name: "BrandPartnerId",
                table: "EcommerceProductItems",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BrandingPartners",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    ProductCategoryId = table.Column<int>(nullable: true),
                    BrandName = table.Column<string>(nullable: true),
                    Logo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BrandingPartners", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BrandingPartners_ProductTypes_ProductCategoryId",
                        column: x => x.ProductCategoryId,
                        principalTable: "ProductTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceProductItems_BrandPartnerId",
                table: "EcommerceProductItems",
                column: "BrandPartnerId");

            migrationBuilder.CreateIndex(
                name: "IX_BrandingPartners_ProductCategoryId",
                table: "BrandingPartners",
                column: "ProductCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_EcommerceProductItems_BrandingPartners_BrandPartnerId",
                table: "EcommerceProductItems",
                column: "BrandPartnerId",
                principalTable: "BrandingPartners",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcommerceProductItems_BrandingPartners_BrandPartnerId",
                table: "EcommerceProductItems");

            migrationBuilder.DropTable(
                name: "BrandingPartners");

            migrationBuilder.DropIndex(
                name: "IX_EcommerceProductItems_BrandPartnerId",
                table: "EcommerceProductItems");

            migrationBuilder.DropColumn(
                name: "BrandPartnerId",
                table: "EcommerceProductItems");

            migrationBuilder.AddColumn<string>(
                name: "BrandName",
                table: "EcommerceProductItems",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
