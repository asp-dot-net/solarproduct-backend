﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Updated_LeadGenAppoinment_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeadGenAppointments_AbpUsers_UserFKId",
                table: "LeadGenAppointments");

            migrationBuilder.DropIndex(
                name: "IX_LeadGenAppointments_UserFKId",
                table: "LeadGenAppointments");

            migrationBuilder.DropColumn(
                name: "UserFKId",
                table: "LeadGenAppointments");

            migrationBuilder.AlterColumn<long>(
                name: "AppointmentFor",
                table: "LeadGenAppointments",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_LeadGenAppointments_AppointmentFor",
                table: "LeadGenAppointments",
                column: "AppointmentFor");

            migrationBuilder.AddForeignKey(
                name: "FK_LeadGenAppointments_AbpUsers_AppointmentFor",
                table: "LeadGenAppointments",
                column: "AppointmentFor",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeadGenAppointments_AbpUsers_AppointmentFor",
                table: "LeadGenAppointments");

            migrationBuilder.DropIndex(
                name: "IX_LeadGenAppointments_AppointmentFor",
                table: "LeadGenAppointments");

            migrationBuilder.AlterColumn<int>(
                name: "AppointmentFor",
                table: "LeadGenAppointments",
                type: "int",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<long>(
                name: "UserFKId",
                table: "LeadGenAppointments",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LeadGenAppointments_UserFKId",
                table: "LeadGenAppointments",
                column: "UserFKId");

            migrationBuilder.AddForeignKey(
                name: "FK_LeadGenAppointments_AbpUsers_UserFKId",
                table: "LeadGenAppointments",
                column: "UserFKId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
