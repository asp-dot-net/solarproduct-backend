﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class FkRemoveCtgryDoc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceCategoryDocs_ServiceCategorys_SubCategoryId",
                table: "ServiceCategoryDocs");

            migrationBuilder.DropIndex(
                name: "IX_ServiceCategoryDocs_SubCategoryId",
                table: "ServiceCategoryDocs");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_ServiceCategoryDocs_SubCategoryId",
                table: "ServiceCategoryDocs",
                column: "SubCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceCategoryDocs_ServiceCategorys_SubCategoryId",
                table: "ServiceCategoryDocs",
                column: "SubCategoryId",
                principalTable: "ServiceCategorys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
