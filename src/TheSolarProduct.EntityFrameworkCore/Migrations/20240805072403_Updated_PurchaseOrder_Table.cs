﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Updated_PurchaseOrder_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Comment",
                table: "PurchaseOrders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReminderDes",
                table: "PurchaseOrders",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReminderTime",
                table: "PurchaseOrders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Comment",
                table: "PurchaseOrders");

            migrationBuilder.DropColumn(
                name: "ReminderDes",
                table: "PurchaseOrders");

            migrationBuilder.DropColumn(
                name: "ReminderTime",
                table: "PurchaseOrders");
        }
    }
}
