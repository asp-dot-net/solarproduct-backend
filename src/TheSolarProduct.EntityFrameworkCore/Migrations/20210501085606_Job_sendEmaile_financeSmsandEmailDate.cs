﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Job_sendEmaile_financeSmsandEmailDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "FinanceEmailSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FinanceEmailSendDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "FinanceSmsSend",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FinanceSmsSendDate",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FinanceEmailSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FinanceEmailSendDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FinanceSmsSend",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FinanceSmsSendDate",
                table: "Jobs");
        }
    }
}
