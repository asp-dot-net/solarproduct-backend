﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_Column_ServiceDocumentTypeID_To_ServiceDoc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ServiceDocumentTypeId",
                table: "ServiceDocs",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceDocs_ServiceDocumentTypeId",
                table: "ServiceDocs",
                column: "ServiceDocumentTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceDocs_ServiceDocumentTypes_ServiceDocumentTypeId",
                table: "ServiceDocs",
                column: "ServiceDocumentTypeId",
                principalTable: "ServiceDocumentTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceDocs_ServiceDocumentTypes_ServiceDocumentTypeId",
                table: "ServiceDocs");

            migrationBuilder.DropIndex(
                name: "IX_ServiceDocs_ServiceDocumentTypeId",
                table: "ServiceDocs");

            migrationBuilder.DropColumn(
                name: "ServiceDocumentTypeId",
                table: "ServiceDocs");
        }
    }
}
