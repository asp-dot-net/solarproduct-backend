﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_JobPromotion_Transport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FreebieTransportId",
                table: "JobPromotions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_JobPromotions_FreebieTransportId",
                table: "JobPromotions",
                column: "FreebieTransportId");

            migrationBuilder.AddForeignKey(
                name: "FK_JobPromotions_FreebieTransports_FreebieTransportId",
                table: "JobPromotions",
                column: "FreebieTransportId",
                principalTable: "FreebieTransports",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobPromotions_FreebieTransports_FreebieTransportId",
                table: "JobPromotions");

            migrationBuilder.DropIndex(
                name: "IX_JobPromotions_FreebieTransportId",
                table: "JobPromotions");

            migrationBuilder.DropColumn(
                name: "FreebieTransportId",
                table: "JobPromotions");
        }
    }
}
