﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_StockTransfers_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StockTransfers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TransportCompanyId = table.Column<int>(nullable: true),
                    WarehouseLocationFromId = table.Column<int>(nullable: true),
                    WarehouseLocationToId = table.Column<int>(nullable: true),
                    OrganizationId = table.Column<int>(nullable: false),
                    VehicalNo = table.Column<string>(nullable: true),
                    TrackingNumber = table.Column<string>(nullable: true),
                    Amount = table.Column<decimal>(nullable: true),
                    AdditionalNotes = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StockTransfers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StockTransfers_TransportCompanies_TransportCompanyId",
                        column: x => x.TransportCompanyId,
                        principalTable: "TransportCompanies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StockTransfers_WareHouseLocation_WarehouseLocationFromId",
                        column: x => x.WarehouseLocationFromId,
                        principalTable: "WareHouseLocation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StockTransfers_WareHouseLocation_WarehouseLocationToId",
                        column: x => x.WarehouseLocationToId,
                        principalTable: "WareHouseLocation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StockTransfers_TransportCompanyId",
                table: "StockTransfers",
                column: "TransportCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_StockTransfers_WarehouseLocationFromId",
                table: "StockTransfers",
                column: "WarehouseLocationFromId");

            migrationBuilder.CreateIndex(
                name: "IX_StockTransfers_WarehouseLocationToId",
                table: "StockTransfers",
                column: "WarehouseLocationToId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StockTransfers");
        }
    }
}
