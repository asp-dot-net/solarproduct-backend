﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_VIC_Rebate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ApprovalDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ExpiryDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RebateAppRef",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SolarRebateStatus",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "SolarVICLoanDiscont",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "SolarVICRebate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VicRebate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VicRebateNotes",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApprovalDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ExpiryDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "RebateAppRef",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "SolarRebateStatus",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "SolarVICLoanDiscont",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "SolarVICRebate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "VicRebate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "VicRebateNotes",
                table: "Jobs");
        }
    }
}
