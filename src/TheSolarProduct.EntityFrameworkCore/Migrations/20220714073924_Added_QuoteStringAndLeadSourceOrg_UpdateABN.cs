﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_QuoteStringAndLeadSourceOrg_UpdateABN : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActiveSalesRep",
                table: "LeadSources",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ABNNumber",
                table: "AbpOrganizationUnits",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "LeadSourceOrganizationUnits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    LeadSourceId = table.Column<int>(nullable: true),
                    OrganizationUnitId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeadSourceOrganizationUnits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LeadSourceOrganizationUnits_LeadSources_LeadSourceId",
                        column: x => x.LeadSourceId,
                        principalTable: "LeadSources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LeadSourceOrganizationUnits_AbpOrganizationUnits_OrganizationUnitId",
                        column: x => x.OrganizationUnitId,
                        principalTable: "AbpOrganizationUnits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "QuotationTemplates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    TemplateName = table.Column<string>(nullable: true),
                    ViewHtml = table.Column<string>(nullable: true),
                    ApiHtml = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuotationTemplates", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LeadSourceOrganizationUnits_LeadSourceId",
                table: "LeadSourceOrganizationUnits",
                column: "LeadSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_LeadSourceOrganizationUnits_OrganizationUnitId",
                table: "LeadSourceOrganizationUnits",
                column: "OrganizationUnitId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LeadSourceOrganizationUnits");

            migrationBuilder.DropTable(
                name: "QuotationTemplates");

            migrationBuilder.DropColumn(
                name: "IsActiveSalesRep",
                table: "LeadSources");

            migrationBuilder.AlterColumn<int>(
                name: "ABNNumber",
                table: "AbpOrganizationUnits",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
