﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_Fields_LeadActivityLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ActivityDate",
                table: "LeadActivityLog",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ActivityNote",
                table: "LeadActivityLog",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActivityDate",
                table: "LeadActivityLog");

            migrationBuilder.DropColumn(
                name: "ActivityNote",
                table: "LeadActivityLog");
        }
    }
}
