﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_PaymentOption_Added_EstablishmentFee_MonthlyAccFee_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "EstablishmentFee",
                table: "PaymentOptions",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "MonthlyAccFee",
                table: "PaymentOptions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EstablishmentFee",
                table: "PaymentOptions");

            migrationBuilder.DropColumn(
                name: "MonthlyAccFee",
                table: "PaymentOptions");
        }
    }
}
