﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_InstallerDetails_Table_Added_NameSurname : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Surname",
                table: "InstallerDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "Surname",
                table: "InstallerDetails");
        }
    }
}
