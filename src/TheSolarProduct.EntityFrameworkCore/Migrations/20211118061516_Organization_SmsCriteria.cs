﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Organization_SmsCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FoneDynamicsAccountSid",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FoneDynamicsPhoneNumber",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FoneDynamicsPropertySid",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FoneDynamicsToken",
                table: "AbpOrganizationUnits",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FoneDynamicsAccountSid",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "FoneDynamicsPhoneNumber",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "FoneDynamicsPropertySid",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "FoneDynamicsToken",
                table: "AbpOrganizationUnits");
        }
    }
}
