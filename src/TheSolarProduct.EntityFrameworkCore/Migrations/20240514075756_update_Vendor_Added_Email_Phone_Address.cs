﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class update_Vendor_Added_Email_Phone_Address : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostCode",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StateId",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StreetName",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StreetNo",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StreetType",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Suburb",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SuburbId",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UnitNo",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UnitType",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "latitude",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "longitude",
                table: "Vendors",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "PostCode",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "State",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "StateId",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "StreetName",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "StreetNo",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "StreetType",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "Suburb",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "SuburbId",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "UnitNo",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "UnitType",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "latitude",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "longitude",
                table: "Vendors");
        }
    }
}
