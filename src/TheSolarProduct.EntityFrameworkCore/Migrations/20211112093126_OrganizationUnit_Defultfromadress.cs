﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class OrganizationUnit_Defultfromadress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "defaultFromAddress",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "defaultFromDisplayName",
                table: "AbpOrganizationUnits",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "defaultFromAddress",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "defaultFromDisplayName",
                table: "AbpOrganizationUnits");
        }
    }
}
