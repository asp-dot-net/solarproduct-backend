﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_AssignDateAndInverterLocation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "AssignDate",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InverterLocation",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssignDate",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "InverterLocation",
                table: "Jobs");
        }
    }
}
