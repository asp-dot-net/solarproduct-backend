﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_Quetation_CustSignPath_Field : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CustSignFilePath",
                table: "Quotations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustSignFilePath",
                table: "Quotations");
        }
    }
}
