﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Add_StockSerialNo_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StockSerialNo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    PurchaseOrderId = table.Column<int>(nullable: false),
                    ProductItemId = table.Column<int>(nullable: false),
                    WarehouseLocationId = table.Column<int>(nullable: false),
                    PalletNo = table.Column<string>(nullable: true),
                    SerialNo = table.Column<string>(nullable: true),
                    IsApp = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StockSerialNo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StockSerialNo_ProductItems_ProductItemId",
                        column: x => x.ProductItemId,
                        principalTable: "ProductItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StockSerialNo_PurchaseOrders_PurchaseOrderId",
                        column: x => x.PurchaseOrderId,
                        principalTable: "PurchaseOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StockSerialNo_WareHouseLocation_WarehouseLocationId",
                        column: x => x.WarehouseLocationId,
                        principalTable: "WareHouseLocation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StockSerialNo_ProductItemId",
                table: "StockSerialNo",
                column: "ProductItemId");

            migrationBuilder.CreateIndex(
                name: "IX_StockSerialNo_PurchaseOrderId",
                table: "StockSerialNo",
                column: "PurchaseOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_StockSerialNo_WarehouseLocationId",
                table: "StockSerialNo",
                column: "WarehouseLocationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StockSerialNo");
        }
    }
}
