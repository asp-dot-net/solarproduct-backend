﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class InstallerInvoice_DueDateAndPriorityId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DueDate",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PriorityId",
                table: "InstallerInvoices",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DueDate",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "PriorityId",
                table: "InstallerInvoices");
        }
    }
}
