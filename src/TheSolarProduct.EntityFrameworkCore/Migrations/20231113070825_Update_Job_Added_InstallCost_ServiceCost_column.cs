﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_Job_Added_InstallCost_ServiceCost_column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "InstallCost",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ServiceCost",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InstallCost",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ServiceCost",
                table: "Jobs");
        }
    }
}
