﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class doctypeadd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "InstallerInvoices",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FilePath",
                table: "InstallerInvoices",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FileName",
                table: "InstallerInvoices");

            migrationBuilder.DropColumn(
                name: "FilePath",
                table: "InstallerInvoices");
        }
    }
}
