﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_Leads_Add_FistAssignUser_FistAssignDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "FirstAssignDate",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "FirstAssignUserId",
                table: "Leads",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstAssignDate",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "FirstAssignUserId",
                table: "Leads");
        }
    }
}
