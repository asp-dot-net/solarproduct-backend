﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_CategoryInstallationItemPeriods_CategoryInstallationItemLists_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CategoryInstallationItemPeriods",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    OrganizationUnit = table.Column<long>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryInstallationItemPeriods", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CategoryInstallationItemPeriods_AbpOrganizationUnits_OrganizationUnit",
                        column: x => x.OrganizationUnit,
                        principalTable: "AbpOrganizationUnits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CategoryInstallationItemLists",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CategoryInstallationItemPeriodId = table.Column<int>(nullable: false),
                    CategoryName = table.Column<string>(nullable: true),
                    StartValue = table.Column<int>(nullable: true),
                    EndValue = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryInstallationItemLists", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CategoryInstallationItemLists_CategoryInstallationItemPeriods_CategoryInstallationItemPeriodId",
                        column: x => x.CategoryInstallationItemPeriodId,
                        principalTable: "CategoryInstallationItemPeriods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CategoryInstallationItemLists_CategoryInstallationItemPeriodId",
                table: "CategoryInstallationItemLists",
                column: "CategoryInstallationItemPeriodId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryInstallationItemPeriods_OrganizationUnit",
                table: "CategoryInstallationItemPeriods",
                column: "OrganizationUnit");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CategoryInstallationItemLists");

            migrationBuilder.DropTable(
                name: "CategoryInstallationItemPeriods");
        }
    }
}
