﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_ProductPackage_Added_Organizations_States_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Organizations",
                table: "ProductPackages",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "States",
                table: "ProductPackages",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Organizations",
                table: "ProductPackages");

            migrationBuilder.DropColumn(
                name: "States",
                table: "ProductPackages");
        }
    }
}
