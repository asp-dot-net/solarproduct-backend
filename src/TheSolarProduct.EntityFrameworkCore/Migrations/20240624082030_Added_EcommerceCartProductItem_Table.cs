﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_EcommerceCartProductItem_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EcommerceCartProductItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    UserId = table.Column<int>(nullable: true),
                    EcommerceProductItemId = table.Column<int>(nullable: true),
                    QTY = table.Column<int>(nullable: true),
                    Purcahse = table.Column<bool>(nullable: false),
                    Price = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EcommerceCartProductItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EcommerceCartProductItems_EcommerceProductItems_EcommerceProductItemId",
                        column: x => x.EcommerceProductItemId,
                        principalTable: "EcommerceProductItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceCartProductItems_EcommerceProductItemId",
                table: "EcommerceCartProductItems",
                column: "EcommerceProductItemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EcommerceCartProductItems");
        }
    }
}
