﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_EcommerceProductItemSpecifications_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           
            migrationBuilder.CreateTable(
                name: "EcommerceProductItemSpecifications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    SpecificationId = table.Column<int>(nullable: true),
                    ProductItemID = table.Column<int>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EcommerceProductItemSpecifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EcommerceProductItemSpecifications_ProductItems_ProductItemID",
                        column: x => x.ProductItemID,
                        principalTable: "ProductItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EcommerceProductItemSpecifications_EcommerceSpecifications_SpecificationId",
                        column: x => x.SpecificationId,
                        principalTable: "EcommerceSpecifications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceProductItemSpecifications_ProductItemID",
                table: "EcommerceProductItemSpecifications",
                column: "ProductItemID");

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceProductItemSpecifications_SpecificationId",
                table: "EcommerceProductItemSpecifications",
                column: "SpecificationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EcommerceProductItemSpecifications");

            migrationBuilder.DropTable(
                name: "EcommerceSpecifications");
        }
    }
}
