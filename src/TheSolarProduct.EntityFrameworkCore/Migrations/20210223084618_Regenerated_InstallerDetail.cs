﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_InstallerDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDesi",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsElec",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IsGoogle",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsInst",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostCode",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StateId",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StreetName",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StreetNo",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StreetType",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Suburb",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Unit",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UnitType",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "latitude",
                table: "InstallerDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "longitude",
                table: "InstallerDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "IsDesi",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "IsElec",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "IsGoogle",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "IsInst",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "PostCode",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "StateId",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "StreetName",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "StreetNo",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "StreetType",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "Suburb",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "Unit",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "UnitType",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "latitude",
                table: "InstallerDetails");

            migrationBuilder.DropColumn(
                name: "longitude",
                table: "InstallerDetails");
        }
    }
}
