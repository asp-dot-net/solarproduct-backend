﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_PurchaseOrderItemChanges_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ExpiryDate",
                table: "PurchaseOrderItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModelNo",
                table: "PurchaseOrderItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExpiryDate",
                table: "PurchaseOrderItems");

            migrationBuilder.DropColumn(
                name: "ModelNo",
                table: "PurchaseOrderItems");
        }
    }
}
