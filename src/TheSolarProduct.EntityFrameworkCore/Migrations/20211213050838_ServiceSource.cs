﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class ServiceSource : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "serviceid",
                table: "Services");

            migrationBuilder.AddColumn<string>(
                name: "ServiceSource",
                table: "Services",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ServiceSource",
                table: "Services");

            migrationBuilder.AddColumn<int>(
                name: "serviceid",
                table: "Services",
                type: "int",
                nullable: true);
        }
    }
}
