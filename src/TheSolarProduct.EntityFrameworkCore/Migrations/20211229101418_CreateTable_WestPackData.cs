﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class CreateTable_WestPackData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WestPackData",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    ReceiptNumber = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    ResponseCode = table.Column<string>(nullable: true),
                    ResponseDescription = table.Column<string>(nullable: true),
                    SummaryCode = table.Column<string>(nullable: true),
                    TransactionType = table.Column<string>(nullable: true),
                    FraudGuardResult = table.Column<string>(nullable: true),
                    TransactionTime = table.Column<DateTime>(nullable: false),
                    SettlementDate = table.Column<DateTime>(nullable: false),
                    CustomerReferenceNumber = table.Column<string>(nullable: true),
                    PaymentReferenceNumber = table.Column<string>(nullable: true),
                    User = table.Column<string>(nullable: true),
                    Voidable = table.Column<bool>(nullable: false),
                    Refundable = table.Column<bool>(nullable: false),
                    Comment = table.Column<string>(nullable: true),
                    IpAddress = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WestPackData", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WestPackData");
        }
    }
}
