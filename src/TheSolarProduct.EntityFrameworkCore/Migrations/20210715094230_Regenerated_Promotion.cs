﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_Promotion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AreaNameFilter",
                table: "Promotions",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EndDateFilter",
                table: "Promotions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "JobStatusIdsFilter",
                table: "Promotions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeadSourceIdsFilter",
                table: "Promotions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeadStatusIdsFilter",
                table: "Promotions",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartDateFilter",
                table: "Promotions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StateIdsFilter",
                table: "Promotions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TeamIdsFilter",
                table: "Promotions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TypeNameFilter",
                table: "Promotions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AreaNameFilter",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "EndDateFilter",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "JobStatusIdsFilter",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "LeadSourceIdsFilter",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "LeadStatusIdsFilter",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "StartDateFilter",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "StateIdsFilter",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "TeamIdsFilter",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "TypeNameFilter",
                table: "Promotions");
        }
    }
}
