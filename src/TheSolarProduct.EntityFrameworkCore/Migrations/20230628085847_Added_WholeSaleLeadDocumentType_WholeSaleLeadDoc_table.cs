﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_WholeSaleLeadDocumentType_WholeSaleLeadDoc_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WholeSaleLeadDocumentTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    IsDocumentRequest = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WholeSaleLeadDocumentTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WholeSaleLeadDocs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    WholeSaleLeadId = table.Column<int>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    FileType = table.Column<string>(nullable: true),
                    FilePath = table.Column<string>(nullable: true),
                    WholeSaleLeadDocumentTypeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WholeSaleLeadDocs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WholeSaleLeadDocs_WholeSaleLeadDocumentTypes_WholeSaleLeadDocumentTypeId",
                        column: x => x.WholeSaleLeadDocumentTypeId,
                        principalTable: "WholeSaleLeadDocumentTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WholeSaleLeadDocs_WholeSaleLeads_WholeSaleLeadId",
                        column: x => x.WholeSaleLeadId,
                        principalTable: "WholeSaleLeads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WholeSaleLeadDocs_WholeSaleLeadDocumentTypeId",
                table: "WholeSaleLeadDocs",
                column: "WholeSaleLeadDocumentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_WholeSaleLeadDocs_WholeSaleLeadId",
                table: "WholeSaleLeadDocs",
                column: "WholeSaleLeadId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WholeSaleLeadDocs");

            migrationBuilder.DropTable(
                name: "WholeSaleLeadDocumentTypes");
        }
    }
}
