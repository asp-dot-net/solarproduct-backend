﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_SpecialOffer_And_SpecialOfferProduct_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SpecialOffers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    OfferTitle = table.Column<string>(nullable: true),
                    DealStarts = table.Column<DateTime>(nullable: true),
                    DealEnds = table.Column<DateTime>(nullable: true),
                    IsOfferActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpecialOffers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SpecialOfferProducts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    SpecialOfferId = table.Column<int>(nullable: true),
                    ProductItemId = table.Column<int>(nullable: true),
                    OldPrice = table.Column<decimal>(nullable: true),
                    NewPrice = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpecialOfferProducts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SpecialOfferProducts_EcommerceProductItems_ProductItemId",
                        column: x => x.ProductItemId,
                        principalTable: "EcommerceProductItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SpecialOfferProducts_SpecialOffers_SpecialOfferId",
                        column: x => x.SpecialOfferId,
                        principalTable: "SpecialOffers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SpecialOfferProducts_ProductItemId",
                table: "SpecialOfferProducts",
                column: "ProductItemId");

            migrationBuilder.CreateIndex(
                name: "IX_SpecialOfferProducts_SpecialOfferId",
                table: "SpecialOfferProducts",
                column: "SpecialOfferId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SpecialOfferProducts");

            migrationBuilder.DropTable(
                name: "SpecialOffers");
        }
    }
}
