﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_Series_Added_ProductCategoryId_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProductCategoryId",
                table: "Serieses",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Serieses_ProductCategoryId",
                table: "Serieses",
                column: "ProductCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Serieses_ProductTypes_ProductCategoryId",
                table: "Serieses",
                column: "ProductCategoryId",
                principalTable: "ProductTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Serieses_ProductTypes_ProductCategoryId",
                table: "Serieses");

            migrationBuilder.DropIndex(
                name: "IX_Serieses_ProductCategoryId",
                table: "Serieses");

            migrationBuilder.DropColumn(
                name: "ProductCategoryId",
                table: "Serieses");
        }
    }
}
