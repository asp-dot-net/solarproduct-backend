﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class invoicetables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InvoicePaymentMethods",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    PaymentMethod = table.Column<string>(nullable: true),
                    ShortCode = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvoicePaymentMethods", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InvoicePayments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    InvoicePayExGST = table.Column<decimal>(nullable: true),
                    InvoicePayGST = table.Column<decimal>(nullable: true),
                    InvoicePayTotal = table.Column<decimal>(nullable: true),
                    InvoicePayDate = table.Column<DateTime>(nullable: true),
                    CCSurcharge = table.Column<decimal>(nullable: true),
                    VerifiedOn = table.Column<DateTime>(nullable: true),
                    PaymentNote = table.Column<string>(maxLength: 255, nullable: true),
                    Refund = table.Column<bool>(nullable: false),
                    RefundType = table.Column<int>(nullable: true),
                    RefundDate = table.Column<DateTime>(nullable: true),
                    PaymentNumber = table.Column<int>(nullable: true),
                    IsVerified = table.Column<bool>(nullable: false),
                    ReceiptNumber = table.Column<string>(nullable: true),
                    ActualPayDate = table.Column<DateTime>(nullable: true),
                    PaidComment = table.Column<string>(nullable: true),
                    JobId = table.Column<int>(nullable: true),
                    UserId = table.Column<long>(nullable: true),
                    VerifiedBy = table.Column<long>(nullable: true),
                    RefundBy = table.Column<long>(nullable: true),
                    InvoicePaymentMethodId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvoicePayments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InvoicePayments_InvoicePaymentMethods_InvoicePaymentMethodId",
                        column: x => x.InvoicePaymentMethodId,
                        principalTable: "InvoicePaymentMethods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InvoicePayments_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InvoicePayments_AbpUsers_RefundBy",
                        column: x => x.RefundBy,
                        principalTable: "AbpUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InvoicePayments_AbpUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AbpUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InvoicePayments_AbpUsers_VerifiedBy",
                        column: x => x.VerifiedBy,
                        principalTable: "AbpUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InvoicePaymentMethods_TenantId",
                table: "InvoicePaymentMethods",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_InvoicePayments_InvoicePaymentMethodId",
                table: "InvoicePayments",
                column: "InvoicePaymentMethodId");

            migrationBuilder.CreateIndex(
                name: "IX_InvoicePayments_JobId",
                table: "InvoicePayments",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_InvoicePayments_RefundBy",
                table: "InvoicePayments",
                column: "RefundBy");

            migrationBuilder.CreateIndex(
                name: "IX_InvoicePayments_TenantId",
                table: "InvoicePayments",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_InvoicePayments_UserId",
                table: "InvoicePayments",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_InvoicePayments_VerifiedBy",
                table: "InvoicePayments",
                column: "VerifiedBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InvoicePayments");

            migrationBuilder.DropTable(
                name: "InvoicePaymentMethods");
        }
    }
}
