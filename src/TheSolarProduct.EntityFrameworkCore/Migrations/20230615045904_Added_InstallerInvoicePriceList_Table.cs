﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_InstallerInvoicePriceList_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InstallerInvoicePriceLists",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    PriceItemId = table.Column<int>(nullable: true),
                    Amount = table.Column<decimal>(nullable: true),
                    InstallerInvoId = table.Column<int>(nullable: true),
                    Notes = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstallerInvoicePriceLists", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InstallerInvoicePriceLists_InstallerInvoices_InstallerInvoId",
                        column: x => x.InstallerInvoId,
                        principalTable: "InstallerInvoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InstallerInvoicePriceLists_PriceItemLists_PriceItemId",
                        column: x => x.PriceItemId,
                        principalTable: "PriceItemLists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InstallerInvoicePriceLists_InstallerInvoId",
                table: "InstallerInvoicePriceLists",
                column: "InstallerInvoId");

            migrationBuilder.CreateIndex(
                name: "IX_InstallerInvoicePriceLists_PriceItemId",
                table: "InstallerInvoicePriceLists",
                column: "PriceItemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InstallerInvoicePriceLists");
        }
    }
}
