﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class ServiceSubCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ServiceSubCategoryId",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ServiceSubCategorybId",
                table: "Services",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Services_ServiceSubCategorybId",
                table: "Services",
                column: "ServiceSubCategorybId");

            migrationBuilder.AddForeignKey(
                name: "FK_Services_ServiceCategorys_ServiceSubCategorybId",
                table: "Services",
                column: "ServiceSubCategorybId",
                principalTable: "ServiceCategorys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Services_ServiceCategorys_ServiceSubCategorybId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Services_ServiceSubCategorybId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "ServiceSubCategoryId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "ServiceSubCategorybId",
                table: "Services");
        }
    }
}
