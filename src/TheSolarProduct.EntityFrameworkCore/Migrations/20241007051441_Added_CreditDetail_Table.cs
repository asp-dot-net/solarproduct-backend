﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_CreditDetail_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CreditDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    ProviderName = table.Column<string>(nullable: true),
                    PlanName = table.Column<string>(nullable: true),
                    AffiliateName = table.Column<string>(nullable: true),
                    SubaffiliateName = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    StatusErrorDescription = table.Column<string>(nullable: true),
                    StatusCreatedAt = table.Column<DateTime>(nullable: true),
                    StatusUpdatedAt = table.Column<DateTime>(nullable: true),
                    SaleSubStatus = table.Column<string>(nullable: true),
                    SaleProductsEnergyServiceId = table.Column<long>(nullable: true),
                    CommissionStatus = table.Column<string>(nullable: true),
                    ReferenceNo = table.Column<string>(nullable: true),
                    ProductType = table.Column<long>(nullable: true),
                    SaleStatus = table.Column<long>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    ProductId = table.Column<long>(nullable: true),
                    LeadId = table.Column<long>(nullable: true),
                    VisitorId = table.Column<long>(nullable: true),
                    Status = table.Column<long>(nullable: true),
                    VisitorSource = table.Column<long>(nullable: true),
                    AgentName = table.Column<string>(nullable: true),
                    AgentId = table.Column<string>(nullable: true),
                    AgentEmail = table.Column<string>(nullable: true),
                    SaleCreated = table.Column<DateTime>(nullable: true),
                    Rc = table.Column<long>(nullable: true),
                    SaleCreatedAt = table.Column<DateTime>(nullable: true),
                    ProductUpdatedAt = table.Column<DateTime>(nullable: true),
                    ProductStatus = table.Column<long>(nullable: true),
                    BundleId = table.Column<long>(nullable: true),
                    Cui = table.Column<long>(nullable: true),
                    UtmSource = table.Column<string>(nullable: true),
                    UtmMedium = table.Column<string>(nullable: true),
                    UtmCampaign = table.Column<string>(nullable: true),
                    UtmTerm = table.Column<string>(nullable: true),
                    UtmContent = table.Column<string>(nullable: true),
                    Gclid = table.Column<long>(nullable: true),
                    Fbclid = table.Column<long>(nullable: true),
                    Msclkid = table.Column<long>(nullable: true),
                    ServiceId = table.Column<long>(nullable: true),
                    SaleStatusDescription = table.Column<string>(nullable: true),
                    SubStatusDescription = table.Column<string>(nullable: true),
                    ServiceName = table.Column<string>(nullable: true),
                    StatusTitle = table.Column<string>(nullable: true),
                    AffiliateCommissionStatus = table.Column<string>(nullable: true),
                    SaleStatusUpdatedAt = table.Column<DateTime>(nullable: true),
                    SaleStatusCreatedAt = table.Column<DateTime>(nullable: true),
                    PostalAddress = table.Column<string>(nullable: true),
                    SaleType = table.Column<string>(nullable: true),
                    PropertyType = table.Column<string>(nullable: true),
                    MoveinType = table.Column<string>(nullable: true),
                    MovinDate = table.Column<string>(nullable: true),
                    SolarType = table.Column<string>(nullable: true),
                    MedicalSupport = table.Column<string>(nullable: true),
                    EnergyType = table.Column<string>(nullable: true),
                    NmiMirnNumber = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    TransfersStatus = table.Column<string>(nullable: true),
                    ConnectionName = table.Column<string>(nullable: true),
                    ConnectionType = table.Column<long>(nullable: true),
                    SaleProductsMobileServiceId = table.Column<long>(nullable: true),
                    Provider = table.Column<string>(nullable: true),
                    PlanMobile = table.Column<string>(nullable: true),
                    ReferalCode = table.Column<string>(nullable: true),
                    SaleProductsBroadbandServiceId = table.Column<long>(nullable: true),
                    PlanBroadband = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CreditDetails", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CreditDetails");
        }
    }
}
