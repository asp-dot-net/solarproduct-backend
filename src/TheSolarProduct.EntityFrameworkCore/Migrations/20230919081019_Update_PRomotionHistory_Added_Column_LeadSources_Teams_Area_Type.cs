﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_PRomotionHistory_Added_Column_LeadSources_Teams_Area_Type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Area",
                table: "PromotionHistorys",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeadSources",
                table: "PromotionHistorys",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Teams",
                table: "PromotionHistorys",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "PromotionHistorys",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Area",
                table: "PromotionHistorys");

            migrationBuilder.DropColumn(
                name: "LeadSources",
                table: "PromotionHistorys");

            migrationBuilder.DropColumn(
                name: "Teams",
                table: "PromotionHistorys");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "PromotionHistorys");
        }
    }
}
