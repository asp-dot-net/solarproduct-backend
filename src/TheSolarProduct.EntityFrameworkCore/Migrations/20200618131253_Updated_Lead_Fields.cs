﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Updated_Lead_Fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Leads_PostCodes_PostCodeId",
                table: "Leads");

            migrationBuilder.DropIndex(
                name: "IX_Leads_PostCodeId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostCodeId",
                table: "Leads");

            migrationBuilder.AddColumn<int>(
                name: "AssignToUserID",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LeadStatusId",
                table: "Leads",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "PostCode",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StreetName",
                table: "Leads",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "StreetNo",
                table: "Leads",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "StreetType",
                table: "Leads",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "UnitNo",
                table: "Leads",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UnitType",
                table: "Leads",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "latitude",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "longitude",
                table: "Leads",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Leads_LeadStatusId",
                table: "Leads",
                column: "LeadStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_LeadStatus_LeadStatusId",
                table: "Leads",
                column: "LeadStatusId",
                principalTable: "LeadStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Leads_LeadStatus_LeadStatusId",
                table: "Leads");

            migrationBuilder.DropIndex(
                name: "IX_Leads_LeadStatusId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "AssignToUserID",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "LeadStatusId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "PostCode",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "StreetName",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "StreetNo",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "StreetType",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "UnitNo",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "UnitType",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "latitude",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "longitude",
                table: "Leads");

            migrationBuilder.AddColumn<int>(
                name: "PostCodeId",
                table: "Leads",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Leads_PostCodeId",
                table: "Leads",
                column: "PostCodeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_PostCodes_PostCodeId",
                table: "Leads",
                column: "PostCodeId",
                principalTable: "PostCodes",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }
    }
}
