﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_Job_DecomentVerify : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CoolingoffPeriodEnd",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DocumentsVeridiedBy",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DocumentsVeridiedDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FinanceNotes",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CoolingoffPeriodEnd",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DocumentsVeridiedBy",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DocumentsVeridiedDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FinanceNotes",
                table: "Jobs");
        }
    }
}
