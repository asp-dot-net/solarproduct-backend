﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_PurchaseOrder_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PurchaseOrders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    OrderNo = table.Column<long>(nullable: false),
                    VendorId = table.Column<int>(nullable: true),
                    PurchaseCompanyId = table.Column<int>(nullable: true),
                    ManualOrderNo = table.Column<string>(nullable: true),
                    ProductNotes = table.Column<string>(nullable: true),
                    Vendor = table.Column<string>(nullable: true),
                    ContainerNo = table.Column<string>(nullable: true),
                    StockOrderStatusId = table.Column<int>(nullable: true),
                    StockFromId = table.Column<int>(nullable: true),
                    DeliveryTypeId = table.Column<int>(nullable: true),
                    PaymentStatusId = table.Column<int>(nullable: true),
                    PaymentTypeId = table.Column<int>(nullable: true),
                    WarehouseLocationId = table.Column<int>(nullable: true),
                    IncoTermId = table.Column<int>(nullable: true),
                    TransportCompanyId = table.Column<int>(nullable: true),
                    DocSentDate = table.Column<DateTime>(nullable: true),
                    TargetArrivalDate = table.Column<DateTime>(nullable: true),
                    ETD = table.Column<DateTime>(nullable: true),
                    ETA = table.Column<DateTime>(nullable: true),
                    PaymentDueDate = table.Column<DateTime>(nullable: true),
                    PhysicalDeliveryDate = table.Column<DateTime>(nullable: true),
                    DiscountPrice = table.Column<decimal>(nullable: true),
                    CreditNotePrice = table.Column<decimal>(nullable: true),
                    Amount = table.Column<decimal>(nullable: true),
                    CurrencyId = table.Column<int>(nullable: true),
                    GSTType = table.Column<int>(nullable: true),
                    StockOrderForId = table.Column<int>(nullable: true),
                    AdditionalNotes = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PurchaseOrders_Currencies_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "Currencies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseOrders_DeliveryTypes_DeliveryTypeId",
                        column: x => x.DeliveryTypeId,
                        principalTable: "DeliveryTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseOrders_IncoTerms_IncoTermId",
                        column: x => x.IncoTermId,
                        principalTable: "IncoTerms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseOrders_TransportCompanies_IncoTermId",
                        column: x => x.IncoTermId,
                        principalTable: "TransportCompanies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseOrders_PaymentStatuses_PaymentStatusId",
                        column: x => x.PaymentStatusId,
                        principalTable: "PaymentStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseOrders_PaymentTypes_PaymentTypeId",
                        column: x => x.PaymentTypeId,
                        principalTable: "PaymentTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseOrders_PurchaseCompanies_PurchaseCompanyId",
                        column: x => x.PurchaseCompanyId,
                        principalTable: "PurchaseCompanies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseOrders_StockFroms_StockFromId",
                        column: x => x.StockFromId,
                        principalTable: "StockFroms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseOrders_StockOrderFors_StockOrderForId",
                        column: x => x.StockOrderForId,
                        principalTable: "StockOrderFors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseOrders_StockOrderStatuses_StockOrderStatusId",
                        column: x => x.StockOrderStatusId,
                        principalTable: "StockOrderStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseOrders_Vendors_VendorId",
                        column: x => x.VendorId,
                        principalTable: "Vendors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseOrders_WareHouseLocation_WarehouseLocationId",
                        column: x => x.WarehouseLocationId,
                        principalTable: "WareHouseLocation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrders_CurrencyId",
                table: "PurchaseOrders",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrders_DeliveryTypeId",
                table: "PurchaseOrders",
                column: "DeliveryTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrders_IncoTermId",
                table: "PurchaseOrders",
                column: "IncoTermId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrders_PaymentStatusId",
                table: "PurchaseOrders",
                column: "PaymentStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrders_PaymentTypeId",
                table: "PurchaseOrders",
                column: "PaymentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrders_PurchaseCompanyId",
                table: "PurchaseOrders",
                column: "PurchaseCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrders_StockFromId",
                table: "PurchaseOrders",
                column: "StockFromId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrders_StockOrderForId",
                table: "PurchaseOrders",
                column: "StockOrderForId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrders_StockOrderStatusId",
                table: "PurchaseOrders",
                column: "StockOrderStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrders_VendorId",
                table: "PurchaseOrders",
                column: "VendorId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrders_WarehouseLocationId",
                table: "PurchaseOrders",
                column: "WarehouseLocationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PurchaseOrders");
        }
    }
}
