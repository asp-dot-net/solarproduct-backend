﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_Service_Column_ServiceDocumentRequest_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ServiceId",
                table: "ServiceDocumentRequests",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceDocumentRequests_ServiceId",
                table: "ServiceDocumentRequests",
                column: "ServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceDocumentRequests_Services_ServiceId",
                table: "ServiceDocumentRequests",
                column: "ServiceId",
                principalTable: "Services",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceDocumentRequests_Services_ServiceId",
                table: "ServiceDocumentRequests");

            migrationBuilder.DropIndex(
                name: "IX_ServiceDocumentRequests_ServiceId",
                table: "ServiceDocumentRequests");

            migrationBuilder.DropColumn(
                name: "ServiceId",
                table: "ServiceDocumentRequests");
        }
    }
}
