﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Service_JobId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "JobId",
                table: "Services",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Services_JobId",
                table: "Services",
                column: "JobId");

            migrationBuilder.AddForeignKey(
                name: "FK_Services_Jobs_JobId",
                table: "Services",
                column: "JobId",
                principalTable: "Jobs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Services_Jobs_JobId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Services_JobId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "JobId",
                table: "Services");
        }
    }
}
