﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class OrganizationUsit_Logo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LogoFileName",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LogoFilePath",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Mobile",
                table: "AbpOrganizationUnits",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "LogoFileName",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "LogoFilePath",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "Mobile",
                table: "AbpOrganizationUnits");
        }
    }
}
