﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class InvoiceimportData_PurchasenumndRcpnum : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PurchaseNumber",
                table: "InvoiceImportDatas",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReceiptNumber",
                table: "InvoiceImportDatas",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PurchaseNumber",
                table: "InvoiceImportDatas");

            migrationBuilder.DropColumn(
                name: "ReceiptNumber",
                table: "InvoiceImportDatas");
        }
    }
}
