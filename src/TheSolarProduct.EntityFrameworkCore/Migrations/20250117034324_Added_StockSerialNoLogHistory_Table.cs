﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_StockSerialNoLogHistory_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StockSerialNoLogHistorys",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    StockSerialNoId = table.Column<int>(nullable: true),
                    SerialNoStatusId = table.Column<int>(nullable: true),
                    ActionNote = table.Column<string>(nullable: true),
                    IsApp = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StockSerialNoLogHistorys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StockSerialNoLogHistorys_SerialNoStatuses_SerialNoStatusId",
                        column: x => x.SerialNoStatusId,
                        principalTable: "SerialNoStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StockSerialNoLogHistorys_StockSerialNo_StockSerialNoId",
                        column: x => x.StockSerialNoId,
                        principalTable: "StockSerialNo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StockSerialNoLogHistorys_SerialNoStatusId",
                table: "StockSerialNoLogHistorys",
                column: "SerialNoStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_StockSerialNoLogHistorys_StockSerialNoId",
                table: "StockSerialNoLogHistorys",
                column: "StockSerialNoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StockSerialNoLogHistorys");
        }
    }
}
