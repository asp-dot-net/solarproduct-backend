﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class ServiceInstaller : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ServiceInstaller",
                table: "Services");

            migrationBuilder.AddColumn<DateTime>(
                name: "InstallerDate",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "InstallerId",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InstallerNotes",
                table: "Services",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InstallerDate",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "InstallerId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "InstallerNotes",
                table: "Services");

            migrationBuilder.AddColumn<string>(
                name: "ServiceInstaller",
                table: "Services",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
