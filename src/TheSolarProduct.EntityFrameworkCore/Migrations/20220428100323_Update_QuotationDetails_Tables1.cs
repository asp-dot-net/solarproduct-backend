﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_QuotationDetails_Tables1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_QuotationQunityAndModelDetails_QuotationDetails_QuotationDetailId",
                table: "QuotationQunityAndModelDetails");

            migrationBuilder.DropIndex(
                name: "IX_QuotationQunityAndModelDetails_QuotationDetailId",
                table: "QuotationQunityAndModelDetails");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_QuotationQunityAndModelDetails_QuotationDetailId",
                table: "QuotationQunityAndModelDetails",
                column: "QuotationDetailId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_QuotationQunityAndModelDetails_QuotationDetails_QuotationDetailId",
                table: "QuotationQunityAndModelDetails",
                column: "QuotationDetailId",
                principalTable: "QuotationDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
