﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_ProductItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AUSMinumumQty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "AUSSalesTag",
                table: "ProductItems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "AUSStockQty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "ProductItems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "NSWMinimumQty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "NSWSalesTag",
                table: "ProductItems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "NSWStockQty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NTMinimumQty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "NTSalesTag",
                table: "ProductItems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "NTStockQty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "QLDMinimumQty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "QLDSalesTag",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "QLDStockQty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SAMinimumQty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SASalesTag",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SAStockQty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StockId",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StockItem",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TASMinimumQty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TASSalesTag",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TASStockQty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VICMinimumQty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VICSalesTag",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VICStockQty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WAMinimumQty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WASalesTag",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WAStockQty",
                table: "ProductItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AUSMinumumQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "AUSSalesTag",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "AUSStockQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "NSWMinimumQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "NSWSalesTag",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "NSWStockQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "NTMinimumQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "NTSalesTag",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "NTStockQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "QLDMinimumQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "QLDSalesTag",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "QLDStockQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "SAMinimumQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "SASalesTag",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "SAStockQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "StockId",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "StockItem",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "TASMinimumQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "TASSalesTag",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "TASStockQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "VICMinimumQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "VICSalesTag",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "VICStockQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "WAMinimumQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "WASalesTag",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "WAStockQty",
                table: "ProductItems");
        }
    }
}
