﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_Lead : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Leads",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    CopanyName = table.Column<string>(maxLength: 100, nullable: false),
                    Email = table.Column<string>(maxLength: 255, nullable: false),
                    Phone = table.Column<string>(maxLength: 10, nullable: false),
                    Mobile = table.Column<string>(maxLength: 10, nullable: false),
                    Address = table.Column<string>(maxLength: 300, nullable: false),
                    Requirements = table.Column<string>(maxLength: 500, nullable: false),
                    SuburbId = table.Column<int>(nullable: false),
                    StateId = table.Column<int>(nullable: false),
                    PostCodeId = table.Column<int>(nullable: false),
                    LeadSourceId = table.Column<int>(nullable: false),
                    LeadSubSourceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Leads", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Leads_LeadSources_LeadSourceId",
                        column: x => x.LeadSourceId,
                        principalTable: "LeadSources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Leads_LeadSubSources_LeadSubSourceId",
                        column: x => x.LeadSubSourceId,
                        principalTable: "LeadSubSources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Leads_PostCodes_PostCodeId",
                        column: x => x.PostCodeId,
                        principalTable: "PostCodes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Leads_State_StateId",
                        column: x => x.StateId,
                        principalTable: "State",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Leads_PostCodes_SuburbId",
                        column: x => x.SuburbId,
                        principalTable: "PostCodes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Leads_LeadSourceId",
                table: "Leads",
                column: "LeadSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_LeadSubSourceId",
                table: "Leads",
                column: "LeadSubSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_PostCodeId",
                table: "Leads",
                column: "PostCodeId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_StateId",
                table: "Leads",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_SuburbId",
                table: "Leads",
                column: "SuburbId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_TenantId",
                table: "Leads",
                column: "TenantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Leads");
        }
    }
}
