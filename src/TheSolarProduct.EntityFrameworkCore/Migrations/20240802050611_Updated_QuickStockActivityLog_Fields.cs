﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Updated_QuickStockActivityLog_Fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Body",
                table: "QuickStockActivityLogs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MessageId",
                table: "QuickStockActivityLogs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ReferanceId",
                table: "QuickStockActivityLogs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Subject",
                table: "QuickStockActivityLogs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TemplateId",
                table: "QuickStockActivityLogs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "QuickStockActivityLogs",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Body",
                table: "QuickStockActivityLogs");

            migrationBuilder.DropColumn(
                name: "MessageId",
                table: "QuickStockActivityLogs");

            migrationBuilder.DropColumn(
                name: "ReferanceId",
                table: "QuickStockActivityLogs");

            migrationBuilder.DropColumn(
                name: "Subject",
                table: "QuickStockActivityLogs");

            migrationBuilder.DropColumn(
                name: "TemplateId",
                table: "QuickStockActivityLogs");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "QuickStockActivityLogs");
        }
    }
}
