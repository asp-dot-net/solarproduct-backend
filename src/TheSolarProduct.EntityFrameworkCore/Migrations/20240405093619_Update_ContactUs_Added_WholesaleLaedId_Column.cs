﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_ContactUs_Added_WholesaleLaedId_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "WholesaleLeadId",
                table: "EcommerceContactUses",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceContactUses_WholesaleLeadId",
                table: "EcommerceContactUses",
                column: "WholesaleLeadId");

            migrationBuilder.AddForeignKey(
                name: "FK_EcommerceContactUses_WholeSaleLeads_WholesaleLeadId",
                table: "EcommerceContactUses",
                column: "WholesaleLeadId",
                principalTable: "WholeSaleLeads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcommerceContactUses_WholeSaleLeads_WholesaleLeadId",
                table: "EcommerceContactUses");

            migrationBuilder.DropIndex(
                name: "IX_EcommerceContactUses_WholesaleLeadId",
                table: "EcommerceContactUses");

            migrationBuilder.DropColumn(
                name: "WholesaleLeadId",
                table: "EcommerceContactUses");
        }
    }
}
