﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_Column_CaseAndTracking_Table_Service : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CaseNo",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CaseNotes",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CaseStatus",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TrackingNo",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TrackingNotes",
                table: "Services",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CaseNo",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "CaseNotes",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "CaseStatus",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "TrackingNo",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "TrackingNotes",
                table: "Services");
        }
    }
}
