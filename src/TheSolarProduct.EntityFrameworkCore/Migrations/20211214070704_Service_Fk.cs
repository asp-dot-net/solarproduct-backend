﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Service_Fk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Services_ServicePrioritys_PriorityId",
                table: "Services");

            migrationBuilder.DropForeignKey(
                name: "FK_Services_ServiceStatuses_StatusId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Services_PriorityId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Services_StatusId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "PriorityId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "Services");

            migrationBuilder.CreateIndex(
                name: "IX_Services_ServicePriorityId",
                table: "Services",
                column: "ServicePriorityId");

            migrationBuilder.CreateIndex(
                name: "IX_Services_ServiceStatusId",
                table: "Services",
                column: "ServiceStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Services_ServicePrioritys_ServicePriorityId",
                table: "Services",
                column: "ServicePriorityId",
                principalTable: "ServicePrioritys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Services_ServiceStatuses_ServiceStatusId",
                table: "Services",
                column: "ServiceStatusId",
                principalTable: "ServiceStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Services_ServicePrioritys_ServicePriorityId",
                table: "Services");

            migrationBuilder.DropForeignKey(
                name: "FK_Services_ServiceStatuses_ServiceStatusId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Services_ServicePriorityId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Services_ServiceStatusId",
                table: "Services");

            migrationBuilder.AddColumn<int>(
                name: "PriorityId",
                table: "Services",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                table: "Services",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Services_PriorityId",
                table: "Services",
                column: "PriorityId");

            migrationBuilder.CreateIndex(
                name: "IX_Services_StatusId",
                table: "Services",
                column: "StatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Services_ServicePrioritys_PriorityId",
                table: "Services",
                column: "PriorityId",
                principalTable: "ServicePrioritys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Services_ServiceStatuses_StatusId",
                table: "Services",
                column: "StatusId",
                principalTable: "ServiceStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
