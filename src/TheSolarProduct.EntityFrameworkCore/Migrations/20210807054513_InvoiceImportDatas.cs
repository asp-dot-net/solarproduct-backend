﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class InvoiceImportDatas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InvoiceImportDatas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: true),
                    JobNumber = table.Column<string>(nullable: true),
                    JobId = table.Column<int>(nullable: true),
                    InvoicePaymentmothodName = table.Column<string>(nullable: true),
                    InvoicePaymentMethodId = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    PaidAmmount = table.Column<decimal>(nullable: true),
                    AllotedBy = table.Column<string>(nullable: true),
                    InvoiceNotesDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvoiceImportDatas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InvoiceImportDatas_InvoicePaymentMethods_InvoicePaymentMethodId",
                        column: x => x.InvoicePaymentMethodId,
                        principalTable: "InvoicePaymentMethods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InvoiceImportDatas_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceImportDatas_InvoicePaymentMethodId",
                table: "InvoiceImportDatas",
                column: "InvoicePaymentMethodId");

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceImportDatas_JobId",
                table: "InvoiceImportDatas",
                column: "JobId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InvoiceImportDatas");
        }
    }
}
