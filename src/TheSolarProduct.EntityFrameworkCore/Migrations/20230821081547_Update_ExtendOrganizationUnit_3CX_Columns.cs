﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_ExtendOrganizationUnit_3CX_Columns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AccountId",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GateWayAccountEmail",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GateWayAccountPassword",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "smsFrom",
                table: "AbpOrganizationUnits",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountId",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "GateWayAccountEmail",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "GateWayAccountPassword",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "smsFrom",
                table: "AbpOrganizationUnits");
        }
    }
}
