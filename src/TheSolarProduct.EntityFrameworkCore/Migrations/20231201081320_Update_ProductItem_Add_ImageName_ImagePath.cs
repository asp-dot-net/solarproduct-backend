﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_ProductItem_Add_ImageName_ImagePath : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ImageName",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ImagePath",
                table: "ProductItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImageName",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "ImagePath",
                table: "ProductItems");
        }
    }
}
