﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_Bussiness_Update_Columns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Businesses_State_B_StateId2",
                table: "Businesses");

            migrationBuilder.DropForeignKey(
                name: "FK_Businesses_State_R_StateId2",
                table: "Businesses");

            migrationBuilder.DropIndex(
                name: "IX_Businesses_B_StateId2",
                table: "Businesses");

            migrationBuilder.DropIndex(
                name: "IX_Businesses_R_StateId2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_Latitude2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_Longitude2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_PostCode2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_StateId2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_StreetName2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_StreetNo2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_StreetType2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_UnitNo2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_UnitType2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_Latitude2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_Longitude2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_PostCode2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_StateId2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_StreetName2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_StreetNo2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_StreetType2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_UnitNo2",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_UnitType2",
                table: "Businesses");

            migrationBuilder.AddColumn<string>(
                name: "B_Latitude",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_Longitude",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_PostCode",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "B_StateId",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_StreetName",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_StreetNo",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_StreetType",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_UnitNo",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_UnitType",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_Latitude",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_Longitude",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_PostCode",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "R_StateId",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_StreetName",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_StreetNo",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_StreetType",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_UnitNo",
                table: "Businesses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_UnitType",
                table: "Businesses",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Businesses_B_StateId",
                table: "Businesses",
                column: "B_StateId");

            migrationBuilder.CreateIndex(
                name: "IX_Businesses_R_StateId",
                table: "Businesses",
                column: "R_StateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Businesses_State_B_StateId",
                table: "Businesses",
                column: "B_StateId",
                principalTable: "State",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Businesses_State_R_StateId",
                table: "Businesses",
                column: "R_StateId",
                principalTable: "State",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Businesses_State_B_StateId",
                table: "Businesses");

            migrationBuilder.DropForeignKey(
                name: "FK_Businesses_State_R_StateId",
                table: "Businesses");

            migrationBuilder.DropIndex(
                name: "IX_Businesses_B_StateId",
                table: "Businesses");

            migrationBuilder.DropIndex(
                name: "IX_Businesses_R_StateId",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_Latitude",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_Longitude",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_PostCode",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_StateId",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_StreetName",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_StreetNo",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_StreetType",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_UnitNo",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "B_UnitType",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_Latitude",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_Longitude",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_PostCode",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_StateId",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_StreetName",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_StreetNo",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_StreetType",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_UnitNo",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "R_UnitType",
                table: "Businesses");

            migrationBuilder.AddColumn<string>(
                name: "B_Latitude2",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_Longitude2",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_PostCode2",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "B_StateId2",
                table: "Businesses",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_StreetName2",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_StreetNo2",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_StreetType2",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_UnitNo2",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "B_UnitType2",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_Latitude2",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_Longitude2",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_PostCode2",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "R_StateId2",
                table: "Businesses",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_StreetName2",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_StreetNo2",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_StreetType2",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_UnitNo2",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "R_UnitType2",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Businesses_B_StateId2",
                table: "Businesses",
                column: "B_StateId2");

            migrationBuilder.CreateIndex(
                name: "IX_Businesses_R_StateId2",
                table: "Businesses",
                column: "R_StateId2");

            migrationBuilder.AddForeignKey(
                name: "FK_Businesses_State_B_StateId2",
                table: "Businesses",
                column: "B_StateId2",
                principalTable: "State",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Businesses_State_R_StateId2",
                table: "Businesses",
                column: "R_StateId2",
                principalTable: "State",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
