﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_PromotionType9069 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            
            migrationBuilder.CreateTable(
                name: "ElecDistributors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    Seq = table.Column<int>(nullable: true),
                    NSW = table.Column<bool>(nullable: false),
                    SA = table.Column<bool>(nullable: false),
                    QLD = table.Column<bool>(nullable: false),
                    VIC = table.Column<bool>(nullable: false),
                    WA = table.Column<bool>(nullable: false),
                    ACT = table.Column<bool>(nullable: false),
                    TAS = table.Column<string>(nullable: true),
                    NT = table.Column<bool>(nullable: false),
                    ElectDistABB = table.Column<string>(nullable: true),
                    ElecDistAppReq = table.Column<bool>(nullable: false),
                    GreenBoatDistributor = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ElecDistributors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "JobStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "JobTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    DisplayOrder = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    DisplayOrder = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PromotionOffers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PromotionOffers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RoofAngles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    DisplayOrder = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoofAngles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RoofTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    DisplayOrder = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoofTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Variations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Variations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 500, nullable: false),
                    Manufacturer = table.Column<string>(maxLength: 200, nullable: true),
                    Model = table.Column<string>(maxLength: 50, nullable: true),
                    Series = table.Column<string>(maxLength: 50, nullable: true),
                    Size = table.Column<decimal>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(maxLength: 250, nullable: true),
                    Code = table.Column<int>(nullable: true),
                    InverterCert = table.Column<string>(nullable: true),
                    StockQuantity = table.Column<int>(nullable: true),
                    StockLocation = table.Column<int>(nullable: true),
                    MinLevel = table.Column<int>(nullable: true),
                    Reserved = table.Column<int>(nullable: true),
                    MinPrice = table.Column<decimal>(nullable: true),
                    CostPrice = table.Column<decimal>(nullable: true),
                    CustomerPrice = table.Column<decimal>(nullable: true),
                    ApprovedDate = table.Column<DateTime>(nullable: true),
                    ExpiryDate = table.Column<DateTime>(nullable: true),
                    FireTested = table.Column<string>(maxLength: 100, nullable: true),
                    ACPower = table.Column<string>(maxLength: 100, nullable: true),
                    ProductTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductItems_ProductTypes_ProductTypeId",
                        column: x => x.ProductTypeId,
                        principalTable: "ProductTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Jobs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenantId = table.Column<int>(nullable: false),
                    Note = table.Column<string>(maxLength: 1000, nullable: true),
                    HouseTypeId = table.Column<int>(nullable: true),
                    PanelOnFlat = table.Column<int>(nullable: true),
                    PanelOnPitched = table.Column<int>(nullable: true),
                    MeterUpgrade = table.Column<string>(nullable: true),
                    NMINumber = table.Column<string>(nullable: true),
                    RegPlanNo = table.Column<string>(nullable: true),
                    LotNumber = table.Column<string>(nullable: true),
                    PeakMeterNo = table.Column<string>(nullable: true),
                    OffPeakMeter = table.Column<string>(nullable: true),
                    MeterPhase = table.Column<string>(nullable: true),
                    EnoughMeterSpace = table.Column<bool>(nullable: false),
                    IsSystemOffPeak = table.Column<bool>(nullable: false),
                    IsFinanceWithUs = table.Column<bool>(nullable: false),
                    PaymentOptionId = table.Column<int>(nullable: true),
                    DepositOptionId = table.Column<int>(nullable: true),
                    FinPaymentTypeId = table.Column<int>(nullable: true),
                    BasicCost = table.Column<decimal>(nullable: true),
                    SolarVLCRebate = table.Column<decimal>(nullable: true),
                    SolarVLCLoan = table.Column<decimal>(nullable: true),
                    TotalCost = table.Column<decimal>(nullable: true),
                    Suburb = table.Column<string>(nullable: true),
                    SuburbId = table.Column<int>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    StateId = table.Column<int>(nullable: true),
                    PostalCode = table.Column<string>(nullable: true),
                    UnitNo = table.Column<string>(nullable: true),
                    UnitType = table.Column<string>(nullable: true),
                    StreetNo = table.Column<string>(nullable: true),
                    StreetName = table.Column<string>(nullable: true),
                    StreetType = table.Column<string>(nullable: true),
                    Latitude = table.Column<string>(nullable: true),
                    Longitude = table.Column<string>(nullable: true),
                    SystemCapacity = table.Column<decimal>(nullable: true),
                    STC = table.Column<decimal>(nullable: true),
                    STCPrice = table.Column<decimal>(nullable: true),
                    Rebate = table.Column<decimal>(nullable: true),
                    JobTypeId = table.Column<int>(nullable: true),
                    JobStatusId = table.Column<int>(nullable: true),
                    RoofTypeId = table.Column<int>(nullable: true),
                    RoofAngleId = table.Column<int>(nullable: true),
                    ElecDistributorId = table.Column<int>(nullable: true),
                    LeadId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Jobs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Jobs_ElecDistributors_ElecDistributorId",
                        column: x => x.ElecDistributorId,
                        principalTable: "ElecDistributors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Jobs_JobStatuses_JobStatusId",
                        column: x => x.JobStatusId,
                        principalTable: "JobStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Jobs_JobTypes_JobTypeId",
                        column: x => x.JobTypeId,
                        principalTable: "JobTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Jobs_Leads_LeadId",
                        column: x => x.LeadId,
                        principalTable: "Leads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Jobs_RoofAngles_RoofAngleId",
                        column: x => x.RoofAngleId,
                        principalTable: "RoofAngles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Jobs_RoofTypes_RoofTypeId",
                        column: x => x.RoofTypeId,
                        principalTable: "RoofTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "JobVariations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenantId = table.Column<int>(nullable: false),
                    Cost = table.Column<decimal>(nullable: true),
                    VariationId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobVariations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobVariations_Variations_VariationId",
                        column: x => x.VariationId,
                        principalTable: "Variations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "JobPromotions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: false),
                    PromotionOfferId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobPromotions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobPromotions_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobPromotions_PromotionOffers_PromotionOfferId",
                        column: x => x.PromotionOfferId,
                        principalTable: "PromotionOffers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_JobPromotions_JobId",
                table: "JobPromotions",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_JobPromotions_PromotionOfferId",
                table: "JobPromotions",
                column: "PromotionOfferId");

            migrationBuilder.CreateIndex(
                name: "IX_JobPromotions_TenantId",
                table: "JobPromotions",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_ElecDistributorId",
                table: "Jobs",
                column: "ElecDistributorId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_JobStatusId",
                table: "Jobs",
                column: "JobStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_JobTypeId",
                table: "Jobs",
                column: "JobTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_LeadId",
                table: "Jobs",
                column: "LeadId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_RoofAngleId",
                table: "Jobs",
                column: "RoofAngleId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_RoofTypeId",
                table: "Jobs",
                column: "RoofTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_TenantId",
                table: "Jobs",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_JobVariations_TenantId",
                table: "JobVariations",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_JobVariations_VariationId",
                table: "JobVariations",
                column: "VariationId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductItems_ProductTypeId",
                table: "ProductItems",
                column: "ProductTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductItems_TenantId",
                table: "ProductItems",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTypes_TenantId",
                table: "ProductTypes",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Variations_TenantId",
                table: "Variations",
                column: "TenantId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Promotions_PromotionTypes_PromotionTypeId",
            //    table: "Promotions",
            //    column: "PromotionTypeId",
            //    principalTable: "PromotionTypes",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropTable(
                name: "JobPromotions");

            migrationBuilder.DropTable(
                name: "JobVariations");

            migrationBuilder.DropTable(
                name: "ProductItems");

            migrationBuilder.DropTable(
                name: "Jobs");

            migrationBuilder.DropTable(
                name: "PromotionOffers");

            migrationBuilder.DropTable(
                name: "Variations");

            migrationBuilder.DropTable(
                name: "ProductTypes");

            migrationBuilder.DropTable(
                name: "ElecDistributors");

            migrationBuilder.DropTable(
                name: "JobStatuses");

            migrationBuilder.DropTable(
                name: "JobTypes");

            migrationBuilder.DropTable(
                name: "RoofAngles");

            migrationBuilder.DropTable(
                name: "RoofTypes");


        }
    }
}
