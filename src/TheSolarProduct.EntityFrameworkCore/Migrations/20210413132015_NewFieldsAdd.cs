﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class NewFieldsAdd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AppliedBy",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ApprovalRef",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DistApplied",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DistApproveBy",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DistApproveDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EmpId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "InvPaidDate",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InvRefNo",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "PaidAmmount",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PaidStatus",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Paidby",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AppliedBy",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ApprovalRef",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DistApplied",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DistApproveBy",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DistApproveDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "EmpId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "InvPaidDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "InvRefNo",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "PaidAmmount",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "PaidStatus",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "Paidby",
                table: "Jobs");
        }
    }
}
