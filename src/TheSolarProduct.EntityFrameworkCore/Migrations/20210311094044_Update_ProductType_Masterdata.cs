﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_ProductType_Masterdata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DisplayOrder",
                table: "ProductTypes");

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "ProductTypes",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "ProductTypes");

            migrationBuilder.AddColumn<int>(
                name: "DisplayOrder",
                table: "ProductTypes",
                type: "int",
                nullable: true);
        }
    }
}
