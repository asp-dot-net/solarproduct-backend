﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class added_ReasonsId_Leads : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CancelReasonId",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RejectReasonId",
                table: "Leads",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CancelReasonId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "RejectReasonId",
                table: "Leads");
        }
    }
}
