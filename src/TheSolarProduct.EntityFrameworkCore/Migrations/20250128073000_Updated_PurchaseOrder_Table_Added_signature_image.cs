﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Updated_PurchaseOrder_Table_Added_signature_image : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Latitude",
                table: "PurchaseOrders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Longitude",
                table: "PurchaseOrders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "signature_image",
                table: "PurchaseOrders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "user_image",
                table: "PurchaseOrders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Latitude",
                table: "PurchaseOrders");

            migrationBuilder.DropColumn(
                name: "Longitude",
                table: "PurchaseOrders");

            migrationBuilder.DropColumn(
                name: "signature_image",
                table: "PurchaseOrders");

            migrationBuilder.DropColumn(
                name: "user_image",
                table: "PurchaseOrders");
        }
    }
}
