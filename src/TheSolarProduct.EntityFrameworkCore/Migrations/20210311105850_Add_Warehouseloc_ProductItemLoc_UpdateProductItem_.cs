﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Add_Warehouseloc_ProductItemLoc_UpdateProductItem_ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ACPower",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "AUSMinumumQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "AUSSalesTag",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "AUSStockQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "CostPrice",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "CustomerPrice",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "FireTested",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "InverterCert",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "MinLevel",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "MinPrice",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "NSWMinimumQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "NSWSalesTag",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "NSWStockQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "NTMinimumQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "NTSalesTag",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "NTStockQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "QLDMinimumQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "QLDSalesTag",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "QLDStockQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "Reserved",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "SAMinimumQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "SASalesTag",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "SAStockQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "ShortName",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "StockId",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "StockItem",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "StockLocation",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "StockQuantity",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "TASMinimumQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "TASSalesTag",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "TASStockQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "VICMinimumQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "VICSalesTag",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "VICStockQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "WAMinimumQty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "WASalesTag",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "WAStockQty",
                table: "ProductItems");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "ProductItems",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "ProductItems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "ProductItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "ProductItems");

            migrationBuilder.AddColumn<string>(
                name: "ACPower",
                table: "ProductItems",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AUSMinumumQty",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "AUSSalesTag",
                table: "ProductItems",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "AUSStockQty",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "CostPrice",
                table: "ProductItems",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "CustomerPrice",
                table: "ProductItems",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "ProductItems",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FireTested",
                table: "ProductItems",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InverterCert",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "ProductItems",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "MinLevel",
                table: "ProductItems",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "MinPrice",
                table: "ProductItems",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NSWMinimumQty",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "NSWSalesTag",
                table: "ProductItems",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "NSWStockQty",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NTMinimumQty",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "NTSalesTag",
                table: "ProductItems",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "NTStockQty",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "QLDMinimumQty",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "QLDSalesTag",
                table: "ProductItems",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "QLDStockQty",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Reserved",
                table: "ProductItems",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SAMinimumQty",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SASalesTag",
                table: "ProductItems",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "SAStockQty",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ShortName",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StockId",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StockItem",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StockLocation",
                table: "ProductItems",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StockQuantity",
                table: "ProductItems",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TASMinimumQty",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "TASSalesTag",
                table: "ProductItems",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "TASStockQty",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VICMinimumQty",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "VICSalesTag",
                table: "ProductItems",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "VICStockQty",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WAMinimumQty",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "WASalesTag",
                table: "ProductItems",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "WAStockQty",
                table: "ProductItems",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
