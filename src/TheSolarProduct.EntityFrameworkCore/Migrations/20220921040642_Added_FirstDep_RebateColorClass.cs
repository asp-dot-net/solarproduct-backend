﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_FirstDep_RebateColorClass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ColorClass",
                table: "SolarRebateStatus",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FirstDepositDate",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ColorClass",
                table: "SolarRebateStatus");

            migrationBuilder.DropColumn(
                name: "FirstDepositDate",
                table: "Jobs");
        }
    }
}
