﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_PromotionTypeFK_WholeSalePromotion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WholeSalePromotions_WholeSalePromotionTypes_WholeSalePromotionTypeId",
                table: "WholeSalePromotions");

            migrationBuilder.DropIndex(
                name: "IX_WholeSalePromotions_WholeSalePromotionTypeId",
                table: "WholeSalePromotions");

            migrationBuilder.DropColumn(
                name: "WholeSalePromotionTypeId",
                table: "WholeSalePromotions");

            migrationBuilder.AddColumn<int>(
                name: "PromotionTypeId",
                table: "WholeSalePromotions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WholeSalePromotions_PromotionTypeId",
                table: "WholeSalePromotions",
                column: "PromotionTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_WholeSalePromotions_PromotionTypes_PromotionTypeId",
                table: "WholeSalePromotions",
                column: "PromotionTypeId",
                principalTable: "PromotionTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WholeSalePromotions_PromotionTypes_PromotionTypeId",
                table: "WholeSalePromotions");

            migrationBuilder.DropIndex(
                name: "IX_WholeSalePromotions_PromotionTypeId",
                table: "WholeSalePromotions");

            migrationBuilder.DropColumn(
                name: "PromotionTypeId",
                table: "WholeSalePromotions");

            migrationBuilder.AddColumn<int>(
                name: "WholeSalePromotionTypeId",
                table: "WholeSalePromotions",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WholeSalePromotions_WholeSalePromotionTypeId",
                table: "WholeSalePromotions",
                column: "WholeSalePromotionTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_WholeSalePromotions_WholeSalePromotionTypes_WholeSalePromotionTypeId",
                table: "WholeSalePromotions",
                column: "WholeSalePromotionTypeId",
                principalTable: "WholeSalePromotionTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
