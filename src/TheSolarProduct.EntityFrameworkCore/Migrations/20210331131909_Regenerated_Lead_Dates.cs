﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Regenerated_Lead_Dates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ChangeStatusDate",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LeadAssignDate",
                table: "Leads",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChangeStatusDate",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "LeadAssignDate",
                table: "Leads");
        }
    }
}
