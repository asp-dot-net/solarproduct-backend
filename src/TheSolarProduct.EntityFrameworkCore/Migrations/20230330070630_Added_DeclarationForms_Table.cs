﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_DeclarationForms_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DeclarationFormLinkHistorys",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Token = table.Column<string>(nullable: true),
                    DeclarationFormId = table.Column<int>(nullable: true),
                    Expired = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeclarationFormLinkHistorys", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DeclarationForms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    DocType = table.Column<string>(nullable: true),
                    JobId = table.Column<int>(nullable: true),
                    Kw = table.Column<decimal>(nullable: true),
                    IsSigned = table.Column<bool>(nullable: false),
                    SignFilePath = table.Column<string>(nullable: true),
                    CustSignFileName = table.Column<string>(nullable: true),
                    CustSignLongitude = table.Column<string>(nullable: true),
                    CustSignLatitude = table.Column<string>(nullable: true),
                    CustSignIP = table.Column<string>(nullable: true),
                    SignDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeclarationForms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DeclarationForms_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DeclarationForms_JobId",
                table: "DeclarationForms",
                column: "JobId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DeclarationFormLinkHistorys");

            migrationBuilder.DropTable(
                name: "DeclarationForms");
        }
    }
}
