﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_LeadActivityLog_ForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeadActivityLog_LeadAction_LeadActionId",
                table: "LeadActivityLog");

            migrationBuilder.DropIndex(
                name: "IX_LeadActivityLog_LeadActionId",
                table: "LeadActivityLog");

            migrationBuilder.DropColumn(
                name: "LeadActionId",
                table: "LeadActivityLog");

            migrationBuilder.CreateIndex(
                name: "IX_LeadActivityLog_ActionId",
                table: "LeadActivityLog",
                column: "ActionId");

            migrationBuilder.AddForeignKey(
                name: "FK_LeadActivityLog_LeadAction_ActionId",
                table: "LeadActivityLog",
                column: "ActionId",
                principalTable: "LeadAction",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeadActivityLog_LeadAction_ActionId",
                table: "LeadActivityLog");

            migrationBuilder.DropIndex(
                name: "IX_LeadActivityLog_ActionId",
                table: "LeadActivityLog");

            migrationBuilder.AddColumn<int>(
                name: "LeadActionId",
                table: "LeadActivityLog",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LeadActivityLog_LeadActionId",
                table: "LeadActivityLog",
                column: "LeadActionId");

            migrationBuilder.AddForeignKey(
                name: "FK_LeadActivityLog_LeadAction_LeadActionId",
                table: "LeadActivityLog",
                column: "LeadActionId",
                principalTable: "LeadAction",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
