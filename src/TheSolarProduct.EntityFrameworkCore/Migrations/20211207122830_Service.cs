﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Service : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Services",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    OrganizationId = table.Column<int>(nullable: false),
                    serviceid = table.Column<int>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    ServicePriorityId = table.Column<int>(nullable: true),
                    ServiceCategoryId = table.Column<int>(nullable: true),
                    ServiceStatusId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Mobile = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    JobNumber = table.Column<string>(nullable: true),
                    QuoteNo = table.Column<string>(nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    ServiceAssignDate = table.Column<DateTime>(nullable: true),
                    ServiceInstaller = table.Column<string>(nullable: true),
                    ServiceLine = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Services", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Services");
        }
    }
}
