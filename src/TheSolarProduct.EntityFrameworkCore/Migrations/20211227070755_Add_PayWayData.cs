﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Add_PayWayData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PayWayDatas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    jobId = table.Column<int>(nullable: false),
                    transactionId = table.Column<string>(nullable: true),
                    receiptNumber = table.Column<string>(nullable: true),
                    status = table.Column<string>(nullable: true),
                    responseCode = table.Column<string>(nullable: true),
                    responseText = table.Column<string>(nullable: true),
                    transactionType = table.Column<string>(nullable: true),
                    customerNumber = table.Column<string>(nullable: true),
                    customerName = table.Column<string>(nullable: true),
                    currency = table.Column<string>(nullable: true),
                    principalAmount = table.Column<decimal>(nullable: true),
                    surchargeAmount = table.Column<decimal>(nullable: true),
                    paymentAmount = table.Column<decimal>(nullable: true),
                    paymentMethod = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PayWayDatas", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PayWayDatas");
        }
    }
}
