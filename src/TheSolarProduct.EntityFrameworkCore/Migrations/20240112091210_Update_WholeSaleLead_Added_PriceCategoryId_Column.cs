﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Update_WholeSaleLead_Added_PriceCategoryId_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PriceCategoryId",
                table: "WholeSaleLeads",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WholeSaleLeads_PriceCategoryId",
                table: "WholeSaleLeads",
                column: "PriceCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_WholeSaleLeads_ECommercePriceCategorys_PriceCategoryId",
                table: "WholeSaleLeads",
                column: "PriceCategoryId",
                principalTable: "ECommercePriceCategorys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WholeSaleLeads_ECommercePriceCategorys_PriceCategoryId",
                table: "WholeSaleLeads");

            migrationBuilder.DropIndex(
                name: "IX_WholeSaleLeads_PriceCategoryId",
                table: "WholeSaleLeads");

            migrationBuilder.DropColumn(
                name: "PriceCategoryId",
                table: "WholeSaleLeads");
        }
    }
}
