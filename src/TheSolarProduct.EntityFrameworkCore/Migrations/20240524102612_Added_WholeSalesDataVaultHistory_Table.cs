﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_WholeSalesDataVaultHistory_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WholeSaleDataVaultActivityLogHistorys",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    FieldName = table.Column<string>(nullable: true),
                    PrevValue = table.Column<string>(nullable: true),
                    CurValue = table.Column<string>(nullable: true),
                    Action = table.Column<string>(nullable: true),
                    ActivityLogId = table.Column<int>(nullable: true),
                    SectionId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WholeSaleDataVaultActivityLogHistorys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WholeSaleDataVaultActivityLogHistorys_WholeSaleDataVaultActivityLogs_SectionId",
                        column: x => x.SectionId,
                        principalTable: "WholeSaleDataVaultActivityLogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WholeSaleDataVaultActivityLogHistorys_SectionId",
                table: "WholeSaleDataVaultActivityLogHistorys",
                column: "SectionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WholeSaleDataVaultActivityLogHistorys");
        }
    }
}
