﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class newjobreleatedmasters : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MeterPhase",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "MeterUpgrade",
                table: "Jobs");

            migrationBuilder.AlterColumn<decimal>(
                name: "PromoCharge",
                table: "Promotions",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "LeadCount",
            //    table: "Promotions",
            //    nullable: false,
            //    defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "Jobs",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ElecRetailerId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Jobs",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MeterPhaseId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MeterUpgradeId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PromotionOfferId",
                table: "Jobs",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DepositOptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    DisplayOrder = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepositOptions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ElecRetailers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    NSW = table.Column<bool>(nullable: false),
                    SA = table.Column<bool>(nullable: false),
                    QLD = table.Column<bool>(nullable: false),
                    VIC = table.Column<bool>(nullable: false),
                    WA = table.Column<bool>(nullable: false),
                    ACT = table.Column<bool>(nullable: false),
                    TAS = table.Column<bool>(nullable: false),
                    NT = table.Column<bool>(nullable: false),
                    ElectricityProviderId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ElecRetailers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MeterPhases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    DisplayOrder = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeterPhases", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MeterUpgrades",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    DisplayOrder = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeterUpgrades", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PaymentOptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    DisplayOrder = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentOptions", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_DepositOptionId",
                table: "Jobs",
                column: "DepositOptionId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_ElecRetailerId",
                table: "Jobs",
                column: "ElecRetailerId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_MeterPhaseId",
                table: "Jobs",
                column: "MeterPhaseId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_MeterUpgradeId",
                table: "Jobs",
                column: "MeterUpgradeId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_PaymentOptionId",
                table: "Jobs",
                column: "PaymentOptionId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_PromotionOfferId",
                table: "Jobs",
                column: "PromotionOfferId");

            migrationBuilder.CreateIndex(
                name: "IX_DepositOptions_TenantId",
                table: "DepositOptions",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentOptions_TenantId",
                table: "PaymentOptions",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_DepositOptions_DepositOptionId",
                table: "Jobs",
                column: "DepositOptionId",
                principalTable: "DepositOptions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ElecRetailers_ElecRetailerId",
                table: "Jobs",
                column: "ElecRetailerId",
                principalTable: "ElecRetailers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_MeterPhases_MeterPhaseId",
                table: "Jobs",
                column: "MeterPhaseId",
                principalTable: "MeterPhases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_MeterUpgrades_MeterUpgradeId",
                table: "Jobs",
                column: "MeterUpgradeId",
                principalTable: "MeterUpgrades",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_PaymentOptions_PaymentOptionId",
                table: "Jobs",
                column: "PaymentOptionId",
                principalTable: "PaymentOptions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_PromotionOffers_PromotionOfferId",
                table: "Jobs",
                column: "PromotionOfferId",
                principalTable: "PromotionOffers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_DepositOptions_DepositOptionId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ElecRetailers_ElecRetailerId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_MeterPhases_MeterPhaseId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_MeterUpgrades_MeterUpgradeId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_PaymentOptions_PaymentOptionId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_PromotionOffers_PromotionOfferId",
                table: "Jobs");

            migrationBuilder.DropTable(
                name: "DepositOptions");

            migrationBuilder.DropTable(
                name: "ElecRetailers");

            migrationBuilder.DropTable(
                name: "MeterPhases");

            migrationBuilder.DropTable(
                name: "MeterUpgrades");

            migrationBuilder.DropTable(
                name: "PaymentOptions");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_DepositOptionId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_ElecRetailerId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_MeterPhaseId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_MeterUpgradeId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_PaymentOptionId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_PromotionOfferId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "LeadCount",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "ElecRetailerId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "MeterPhaseId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "MeterUpgradeId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "PromotionOfferId",
                table: "Jobs");

            migrationBuilder.AlterColumn<string>(
                name: "PromoCharge",
                table: "Promotions",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AddColumn<string>(
                name: "MeterPhase",
                table: "Jobs",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MeterUpgrade",
                table: "Jobs",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
