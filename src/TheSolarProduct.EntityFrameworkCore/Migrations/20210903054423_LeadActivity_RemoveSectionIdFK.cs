﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class LeadActivity_RemoveSectionIdFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeadActivityLog_Section_SectionId",
                table: "LeadActivityLog");

            migrationBuilder.DropIndex(
                name: "IX_LeadActivityLog_SectionId",
                table: "LeadActivityLog");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_LeadActivityLog_SectionId",
                table: "LeadActivityLog",
                column: "SectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_LeadActivityLog_Section_SectionId",
                table: "LeadActivityLog",
                column: "SectionId",
                principalTable: "Section",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
