﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class update_EcommerceSolarPackages_Table_Banner_Added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProductPackageId",
                table: "EcommerceSolarPackageItems");

            migrationBuilder.AddColumn<string>(
                name: "Banner",
                table: "EcommerceSolarPackages",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EcommerceSolarPackageId",
                table: "EcommerceSolarPackageItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_EcommerceSolarPackageItems_EcommerceSolarPackageId",
                table: "EcommerceSolarPackageItems",
                column: "EcommerceSolarPackageId");

            migrationBuilder.AddForeignKey(
                name: "FK_EcommerceSolarPackageItems_EcommerceSolarPackages_EcommerceSolarPackageId",
                table: "EcommerceSolarPackageItems",
                column: "EcommerceSolarPackageId",
                principalTable: "EcommerceSolarPackages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcommerceSolarPackageItems_EcommerceSolarPackages_EcommerceSolarPackageId",
                table: "EcommerceSolarPackageItems");

            migrationBuilder.DropIndex(
                name: "IX_EcommerceSolarPackageItems_EcommerceSolarPackageId",
                table: "EcommerceSolarPackageItems");

            migrationBuilder.DropColumn(
                name: "Banner",
                table: "EcommerceSolarPackages");

            migrationBuilder.DropColumn(
                name: "EcommerceSolarPackageId",
                table: "EcommerceSolarPackageItems");

            migrationBuilder.AddColumn<int>(
                name: "ProductPackageId",
                table: "EcommerceSolarPackageItems",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
