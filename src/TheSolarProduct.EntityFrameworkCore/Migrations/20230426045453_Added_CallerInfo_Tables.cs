﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_CallerInfo_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LeadId",
                table: "CallHistory");

            migrationBuilder.DropColumn(
                name: "State",
                table: "CallHistory");

            migrationBuilder.CreateTable(
                name: "CallerInfo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    callerid = table.Column<string>(nullable: true),
                    did = table.Column<string>(nullable: true),
                    callflow = table.Column<string>(nullable: true),
                    tennantid = table.Column<string>(nullable: true),
                    orgid = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CallerInfo", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CallerInfo");

            migrationBuilder.AddColumn<int>(
                name: "LeadId",
                table: "CallHistory",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "CallHistory",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
