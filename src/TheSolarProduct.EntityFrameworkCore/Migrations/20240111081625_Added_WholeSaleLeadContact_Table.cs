﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_WholeSaleLeadContact_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "TradinSTC",
                table: "WholeSaleLeads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TradinSTCNote",
                table: "WholeSaleLeads",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TradinSTCPrice",
                table: "WholeSaleLeads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TradinSTCWhom",
                table: "WholeSaleLeads",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "WholeSaleLeadContacts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    WholesaleLeadId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Mobile = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WholeSaleLeadContacts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WholeSaleLeadContacts_WholeSaleLeads_WholesaleLeadId",
                        column: x => x.WholesaleLeadId,
                        principalTable: "WholeSaleLeads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WholeSaleLeadContacts_WholesaleLeadId",
                table: "WholeSaleLeadContacts",
                column: "WholesaleLeadId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WholeSaleLeadContacts");

            migrationBuilder.DropColumn(
                name: "TradinSTC",
                table: "WholeSaleLeads");

            migrationBuilder.DropColumn(
                name: "TradinSTCNote",
                table: "WholeSaleLeads");

            migrationBuilder.DropColumn(
                name: "TradinSTCPrice",
                table: "WholeSaleLeads");

            migrationBuilder.DropColumn(
                name: "TradinSTCWhom",
                table: "WholeSaleLeads");
        }
    }
}
