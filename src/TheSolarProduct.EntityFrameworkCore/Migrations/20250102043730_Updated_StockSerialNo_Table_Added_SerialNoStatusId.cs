﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Updated_StockSerialNo_Table_Added_SerialNoStatusId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SerialNoStatusId",
                table: "StockSerialNo",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_StockSerialNo_SerialNoStatusId",
                table: "StockSerialNo",
                column: "SerialNoStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_StockSerialNo_SerialNoStatuses_SerialNoStatusId",
                table: "StockSerialNo",
                column: "SerialNoStatusId",
                principalTable: "SerialNoStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StockSerialNo_SerialNoStatuses_SerialNoStatusId",
                table: "StockSerialNo");

            migrationBuilder.DropIndex(
                name: "IX_StockSerialNo_SerialNoStatusId",
                table: "StockSerialNo");

            migrationBuilder.DropColumn(
                name: "SerialNoStatusId",
                table: "StockSerialNo");
        }
    }
}
