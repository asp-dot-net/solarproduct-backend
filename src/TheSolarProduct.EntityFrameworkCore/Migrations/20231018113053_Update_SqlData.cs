﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.IO;

namespace TheSolarProduct.Migrations
{
    public partial class Update_SqlData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            string SqlPath = Environment.CurrentDirectory + "/SqlData/";
            string[] fileEntries = Directory.GetFiles(SqlPath);
            foreach (string fileName in fileEntries)
                migrationBuilder.Sql(File.ReadAllText(fileName));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
