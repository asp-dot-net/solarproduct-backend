﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_JobCostFixExpenseTypes_JobCostFixExpensePeriods_JobCostFixExpenseLists_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "JobCostFixExpensePeriods",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    OrganizationUnit = table.Column<long>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobCostFixExpensePeriods", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobCostFixExpensePeriods_AbpOrganizationUnits_OrganizationUnit",
                        column: x => x.OrganizationUnit,
                        principalTable: "AbpOrganizationUnits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JobCostFixExpenseTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobCostFixExpenseTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "JobCostFixExpenseLists",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    JobCostFixExpensePeriodId = table.Column<int>(nullable: false),
                    JobCostFixExpenseTypeId = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobCostFixExpenseLists", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobCostFixExpenseLists_JobCostFixExpensePeriods_JobCostFixExpensePeriodId",
                        column: x => x.JobCostFixExpensePeriodId,
                        principalTable: "JobCostFixExpensePeriods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobCostFixExpenseLists_JobCostFixExpenseTypes_JobCostFixExpenseTypeId",
                        column: x => x.JobCostFixExpenseTypeId,
                        principalTable: "JobCostFixExpenseTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_JobCostFixExpenseLists_JobCostFixExpensePeriodId",
                table: "JobCostFixExpenseLists",
                column: "JobCostFixExpensePeriodId");

            migrationBuilder.CreateIndex(
                name: "IX_JobCostFixExpenseLists_JobCostFixExpenseTypeId",
                table: "JobCostFixExpenseLists",
                column: "JobCostFixExpenseTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_JobCostFixExpensePeriods_OrganizationUnit",
                table: "JobCostFixExpensePeriods",
                column: "OrganizationUnit");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JobCostFixExpenseLists");

            migrationBuilder.DropTable(
                name: "JobCostFixExpensePeriods");

            migrationBuilder.DropTable(
                name: "JobCostFixExpenseTypes");
        }
    }
}
