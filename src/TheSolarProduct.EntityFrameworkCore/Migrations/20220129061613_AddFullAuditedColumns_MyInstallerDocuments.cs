﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class AddFullAuditedColumns_MyInstallerDocuments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "MyInstallerDocuments",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "MyInstallerDocuments",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "MyInstallerDocuments",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "MyInstallerDocuments",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "MyInstallerDocuments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "MyInstallerDocuments",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "MyInstallerDocuments",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "MyInstallerDocuments");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "MyInstallerDocuments");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "MyInstallerDocuments");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "MyInstallerDocuments");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "MyInstallerDocuments");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "MyInstallerDocuments");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "MyInstallerDocuments");
        }
    }
}
