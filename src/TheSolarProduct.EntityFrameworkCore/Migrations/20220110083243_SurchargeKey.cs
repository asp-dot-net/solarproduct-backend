﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class SurchargeKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SurchargeAuthorizationKey",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SurchargePublishKey",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SurchargeSecrateKey",
                table: "AbpOrganizationUnits",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SurchargeAuthorizationKey",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "SurchargePublishKey",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "SurchargeSecrateKey",
                table: "AbpOrganizationUnits");
        }
    }
}
