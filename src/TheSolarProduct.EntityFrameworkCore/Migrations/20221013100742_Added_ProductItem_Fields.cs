﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class Added_ProductItem_Fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Amount",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ExtendedWarranty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PerformanceWarranty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "PricePerWatt",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProductWarranty",
                table: "ProductItems",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WorkmanshipWarranty",
                table: "ProductItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Amount",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "ExtendedWarranty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "PerformanceWarranty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "PricePerWatt",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "ProductWarranty",
                table: "ProductItems");

            migrationBuilder.DropColumn(
                name: "WorkmanshipWarranty",
                table: "ProductItems");
        }
    }
}
