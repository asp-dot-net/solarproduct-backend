﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheSolarProduct.Migrations
{
    public partial class add_payway_field : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "cardNumber",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "cardholderName",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "cvn",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "expiryDateMonth",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "expiryDateYear",
                table: "AbpOrganizationUnits",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "paymentMethod",
                table: "AbpOrganizationUnits",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "cardNumber",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "cardholderName",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "cvn",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "expiryDateMonth",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "expiryDateYear",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "paymentMethod",
                table: "AbpOrganizationUnits");
        }
    }
}
