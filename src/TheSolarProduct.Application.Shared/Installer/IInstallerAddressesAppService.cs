﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Installer.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Installer
{
    public interface IInstallerAddressesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetInstallerAddressForViewDto>> GetAll(GetAllInstallerAddressesInput input);

		Task<GetInstallerAddressForEditOutput> GetInstallerAddressForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditInstallerAddressDto input);

		Task Delete(EntityDto input);

		
		Task<PagedResultDto<InstallerAddressUserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input);
		
    }
}