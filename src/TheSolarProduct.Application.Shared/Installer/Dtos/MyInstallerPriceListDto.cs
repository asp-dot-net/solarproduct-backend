﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Installer.Dtos
{
    public class MyInstallerPriceListDto
    {
        public int? MyInstallerId { get; set; }
        public int? PriceItemListId { get; set; }
        public decimal? Cost { get; set; }
        public decimal? GST { get; set; }
        public decimal? PriceINCGST { get; set; }
        public int? Id { get; set; }
    }
}
