﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Installer.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}