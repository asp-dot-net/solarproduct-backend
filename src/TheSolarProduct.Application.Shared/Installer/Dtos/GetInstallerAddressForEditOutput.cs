﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Installer.Dtos
{
    public class GetInstallerAddressForEditOutput
    {
		public CreateOrEditInstallerAddressDto InstallerAddress { get; set; }

		public string UserName { get; set;}


    }
}