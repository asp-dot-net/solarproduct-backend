﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Installer.Dtos
{
    public class CreateOrEditMyInstallerPriceListDto
    {
        public List<MyInstallerPriceListDto> pricelist { get; set; }
    }
}
