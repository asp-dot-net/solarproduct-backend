﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Installer.Dtos
{
    public class GetInstallerAvailabilityDto : EntityDto
    {
        public virtual long? UserId { get; set; }
        public DateTime AvailabilityDate { get; set; }
        public int InstallationCount { get; set; }

}
}
