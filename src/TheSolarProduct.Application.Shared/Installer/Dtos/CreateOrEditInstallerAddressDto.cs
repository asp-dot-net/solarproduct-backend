﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Installer.Dtos
{
    public class CreateOrEditInstallerAddressDto : EntityDto<int?>
    {



		public virtual string Unit { get; set; }

		public virtual string UnitType { get; set; }

		public virtual string StreetNo { get; set; }

		public virtual string StreetName { get; set; }

		public virtual string Suburb { get; set; }

		public virtual int? StateId { get; set; }

		public virtual string PostCode { get; set; }


		public long? UserId { get; set; }
		 
		 
    }
}