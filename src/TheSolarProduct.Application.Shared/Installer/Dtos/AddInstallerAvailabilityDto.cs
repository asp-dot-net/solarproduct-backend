﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Installer.Dtos
{
    public class AddInstallerAvailabilityDto : EntityDto
    {
        public long? UserId { get; set; }

        public DateTime AvailabilityDate { get; set; }
    }
}
