﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Installer.Dtos
{
   public class InstallerAvailableListDto
	{
		public int Id { get; set; }
		public string DisplayName { get; set; }
		public int AvailabilityCount { get; set; }
		public int BookCount { get; set; }
		public bool availbale { get; set; }
	}
}
