﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Installer.Dtos
{
    public class InstallerAddressUserLookupTableDto
    {
		public long Id { get; set; }

		public string DisplayName { get; set; }
    }
}