﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Installer.Dtos
{
    public class InstallerContractDto : EntityDto<int?>
	{
		public long UserId { get; set; }

		public int OrganizationId { get; set; }

		public string FileToken { get; set; }
		public string FileName { get; set; }

		public string FilePath { get; set; }

		public string UserName { get; set; }
		public string OrganizationName { get; set; }

	}
}
