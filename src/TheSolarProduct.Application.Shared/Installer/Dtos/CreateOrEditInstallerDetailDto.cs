﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Installer.Dtos
{
	public class CreateOrEditInstallerDetailDto : EntityDto<int?>
	{
		public string InstallerAccreditationNumber { get; set; }

		public DateTime InstallerAccreditationExpiryDate { get; set; }

		public string DesignerLicenseNumber { get; set; }

		public DateTime DesignerLicenseExpiryDate { get; set; }

		public string ElectricianLicenseNumber { get; set; }

		public DateTime ElectricianLicenseExpiryDate { get; set; }

		public string DocInstaller { get; set; }

		public string DocDesigner { get; set; }

		public string DocElectrician { get; set; }

		public string OtherDocsCSV { get; set; }

		public long UserId { get; set; }

		public virtual bool? IsInst { get; set; }

		public virtual bool? IsDesi { get; set; }

		public virtual bool? IsElec { get; set; }

		//Address Detail
		public virtual string Address { get; set; }

		public virtual string IsGoogle { get; set; }

		public virtual string latitude { get; set; }

		public virtual string longitude { get; set; }

		public virtual string Unit { get; set; }

		public virtual string UnitType { get; set; }

		public virtual string StreetNo { get; set; }

		public virtual string StreetType { get; set; }

		public virtual string StreetName { get; set; }

		public virtual string Suburb { get; set; }

		public virtual int? StateId { get; set; }

		public virtual string PostCode { get; set; }
	}
}