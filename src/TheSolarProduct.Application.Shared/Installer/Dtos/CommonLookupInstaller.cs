﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Installer.Dtos
{
    public class CommonLookupInstaller : EntityDto
    {
        public string InstallerName { get; set; }

        public string CompanyName { get; set; }
    }
}
