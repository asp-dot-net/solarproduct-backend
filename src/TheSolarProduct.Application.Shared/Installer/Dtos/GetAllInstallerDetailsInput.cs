﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.Installer.Dtos
{
    public class GetAllInstallerDetailsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }


		 public string UserNameFilter { get; set; }

		 
    }
}