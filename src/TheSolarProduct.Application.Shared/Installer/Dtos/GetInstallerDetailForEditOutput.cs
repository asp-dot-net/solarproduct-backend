﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Installer.Dtos
{
    public class GetInstallerDetailForEditOutput
    {
		public CreateOrEditInstallerDetailDto InstallerDetail { get; set; }

		public string UserName { get; set;}


    }
}