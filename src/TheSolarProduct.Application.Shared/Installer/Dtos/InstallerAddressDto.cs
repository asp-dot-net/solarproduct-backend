﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.Installer.Dtos
{
    public class InstallerAddressDto : EntityDto
    {

	    public long? UserId { get; set; }

        public string Address { get; set; }

        public string IsGoogle { get; set; }

        public string latitude { get; set; }

        public string longitude { get; set; }

        public string Unit { get; set; }
        
        public string UnitType { get; set; }
        
        public string StreetNo { get; set; }

        public string StreetType { get; set; }

        public string StreetName { get; set; }
        
        public string Suburb { get; set; }
        
        public int? StateId { get; set; }

        public string StateName { get; set; }
        
        public string PostCode { get; set; }
    }
}
