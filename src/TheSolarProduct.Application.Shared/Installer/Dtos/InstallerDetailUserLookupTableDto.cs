﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Installer.Dtos
{
    public class InstallerDetailUserLookupTableDto
    {
		public long Id { get; set; }

		public string DisplayName { get; set; }
    }
}