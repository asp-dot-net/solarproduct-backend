﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Installer.Dtos;
using TheSolarProduct.Jobs.Dtos;

namespace TheSolarProduct.Installer
{
    public interface IInstallerAppService : IApplicationService
    {
        Task<int> AddInstallerAvailability(AddInstallerAvailabilityDto input);
        
        Task<List<GetInstallerAvailabilityDto>> GetAll(long? userid);
        
        Task RemoveInstallerAvailability(EntityDto input);
        
        Task<List<CommonLookupDto>> GetAllInstallers(int? orgid);

        Task<List<GetInstallerAvailabilityDto>> GetAllJobBokingCount(long? userid, int? installerId, int? orgID);

        Task<bool> CheckIntallerAccreditation(long? userid, string AccreditationNumber);

        Task<List<CommonLookupInstaller>> GetAllInstallersWithCompnayName(int? orgid);
    }
}
