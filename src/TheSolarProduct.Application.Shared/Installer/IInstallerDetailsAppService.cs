﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Installer.Dtos;
using TheSolarProduct.Dto;
using System.Collections.Generic;

namespace TheSolarProduct.Installer
{
    public interface IInstallerDetailsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetInstallerDetailForViewDto>> GetAll(GetAllInstallerDetailsInput input);

		Task<GetInstallerDetailForEditOutput> GetInstallerDetailForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditInstallerDetailDto input);

		Task Delete(EntityDto input);

		
		Task<PagedResultDto<InstallerDetailUserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input);
		Task CreateOrEditOrganizationDoc(InstallerContractDto input);
		Task<List<InstallerContractDto>> getUserWiseDocList(int id);
		bool checkExistList(int userid, int orgid);
		Task<List<MyInstallerPriceListDto>> GetPriceItemListEdit(int Id);
		Task CreateMyInstallerPriceList(CreateOrEditMyInstallerPriceListDto input);
	}
}