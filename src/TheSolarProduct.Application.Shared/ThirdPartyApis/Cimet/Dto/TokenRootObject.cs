﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ThirdPartyApis.Cimet.Dto
{
    public class TokenRootObject
    {
        [JsonProperty("status")]
        public bool Status { get; set; }

        [JsonProperty("code")]
        public long Code { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
        
        [JsonProperty("data")]
        public TokenData Data { get; set; }
    }

    public class TokenData
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("token_expire_time")]
        public DateTime Code { get; set; }
    }
}
