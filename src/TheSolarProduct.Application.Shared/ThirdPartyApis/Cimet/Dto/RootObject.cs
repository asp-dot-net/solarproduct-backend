﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq.Dynamic.Core.Exceptions;
using System.Text;

namespace TheSolarProduct.ThirdPartyApis.Cimet.Dto
{
    public class RootObject
    {
        [JsonProperty("status")]
        public bool Status { get; set; }

        [JsonProperty("code")]
        public long Code { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("data")]
        public Data Data { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("total_sales")]
        public TotalSales TotalSales { get; set; }

        [JsonProperty("saleData")]
        public List<SaleData> SaleData { get; set; }
    }

    public partial class TotalSales
    {
        [JsonProperty("energy")]
        public long Energy { get; set; }

        [JsonProperty("mobile")]
        public long Mobile { get; set; }

        [JsonProperty("internet")]
        public long Internet { get; set; }
    }

    public partial class SaleData
    {
        [JsonProperty("provider_name")]
        public string ProviderName { get; set; }

        [JsonProperty("plan_name")]
        public string PlanName { get; set; }

        [JsonProperty("affiliate_name")]
        public string AffiliateName { get; set; }

        [JsonProperty("subaffiliate_name")]
        public string SubaffiliateName { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("status_error_description")]
        public string StatusErrorDescription { get; set; }

        [JsonProperty("status_created_at")]
        [JsonConverter(typeof(ConditionalDateTimeConverter))]
        public DateTime? StatusCreatedAt { get; set; }

        [JsonProperty("status_updated_at")]
        [JsonConverter(typeof(ConditionalDateTimeConverter))]
        public DateTime? StatusUpdatedAt { get; set; }

        [JsonProperty("sale_sub_status")]
        public string SaleSubStatus { get; set; }

        [JsonProperty("sale_products_energy.service_id")]
        [JsonConverter(typeof(ConditionalLongConverter))]
        public long? SaleProductsEnergyServiceId { get; set; }

        [JsonProperty("commission_status")]
        public string CommissionStatus { get; set; }

        [JsonProperty("reference_no")]
        public string ReferenceNo { get; set; }

        [JsonProperty("product_type")]
        [JsonConverter(typeof(ConditionalLongConverter))]
        public long? ProductType { get; set; }

        [JsonProperty("sale_status")]
        [JsonConverter(typeof(ConditionalLongConverter))]
        public long? SaleStatus { get; set; }

        [JsonProperty("created_at")]
        [JsonConverter(typeof(ConditionalDateTimeConverter))]
        public DateTime? CreatedAt { get; set; }

        [JsonProperty("product_id")]
        [JsonConverter(typeof(ConditionalLongConverter))]
        public long? ProductId { get; set; }

        [JsonProperty("lead_id")]
        [JsonConverter(typeof(ConditionalLongConverter))]
        public long? LeadId { get; set; }

        [JsonProperty("visitor_id")]
        [JsonConverter(typeof(ConditionalLongConverter))]
        public long? VisitorId { get; set; }

        [JsonProperty("status")]
        [JsonConverter(typeof(ConditionalLongConverter))]
        public long? Status { get; set; }

        [JsonProperty("visitor_source")]
        [JsonConverter(typeof(ConditionalLongConverter))]
        public long? VisitorSource { get; set; }

        [JsonProperty("agent_name")]
        public string AgentName { get; set; }

        [JsonProperty("agent_id")]
        public string AgentId { get; set; }

        [JsonProperty("agent_email")]
        public string AgentEmail { get; set; }

        [JsonProperty("sale_created")]
        [JsonConverter(typeof(ConditionalDateTimeConverter))]
        public DateTime? SaleCreated { get; set; }

        [JsonProperty("rc")]
        [JsonConverter(typeof(ConditionalLongConverter))]
        public long? Rc { get; set; }

        [JsonProperty("sale_created_at")]
        [JsonConverter(typeof(ConditionalDateTimeConverter))]
        public DateTime? SaleCreatedAt { get; set; }

        [JsonProperty("productUpdatedAt")]
        [JsonConverter(typeof(ConditionalDateTimeConverter))]
        public DateTime? ProductUpdatedAt { get; set; }

        [JsonProperty("product_status")]
        [JsonConverter(typeof(ConditionalLongConverter))]
        public long? ProductStatus { get; set; }

        [JsonProperty("bundle_id")]
        [JsonConverter(typeof(ConditionalLongConverter))]
        public long? BundleId { get; set; }

        [JsonProperty("cui")]
        [JsonConverter(typeof(ConditionalLongConverter))]
        public long? Cui { get; set; }

        [JsonProperty("utm_source")]
        public string UtmSource { get; set; }

        [JsonProperty("utm_medium")]
        public string UtmMedium { get; set; }

        [JsonProperty("utm_campaign")]
        public string UtmCampaign { get; set; }

        [JsonProperty("utm_term")]
        public string UtmTerm { get; set; }

        [JsonProperty("utm_content")]
        public string UtmContent { get; set; }

        [JsonProperty("gclid")]
        //[JsonConverter(typeof(ConditionalLongConverter))]
        public string Gclid { get; set; }

        [JsonProperty("fbclid")]
        //[JsonConverter(typeof(ConditionalLongConverter))]
        public string Fbclid { get; set; }

        [JsonProperty("msclkid")]
        //[JsonConverter(typeof(ConditionalLongConverter))]
        public string Msclkid { get; set; }

        [JsonProperty("service_id")]
        //[JsonConverter(typeof(ConditionalLongConverter))]
        public string ServiceId { get; set; }

        [JsonProperty("sale_status_description")]
        public string SaleStatusDescription { get; set; }

        [JsonProperty("sub_status_description")]
        public string SubStatusDescription { get; set; }

        [JsonProperty("service_name")]
        public string ServiceName { get; set; }

        [JsonProperty("status_title")]
        public string StatusTitle { get; set; }

        [JsonProperty("affiliate_commission_status")]
        public string AffiliateCommissionStatus { get; set; }

        [JsonProperty("sale_status_updated_at")]
        [JsonConverter(typeof(ConditionalDateTimeConverter))]
        public DateTime? SaleStatusUpdatedAt { get; set; }

        [JsonProperty("sale_status_created_at")]
        [JsonConverter(typeof(ConditionalDateTimeConverter))]
        public DateTime? SaleStatusCreatedAt { get; set; }
        
        [JsonProperty("postal_address")]
        public string PostalAddress { get; set; }

        [JsonProperty("sale_type")]
        public string SaleType { get; set; }

        [JsonProperty("property_type")]
        public string PropertyType { get; set; }

        [JsonProperty("movein_type")]
        public string MoveinType { get; set; }

        [JsonProperty("movin_date")]
        public string MovinDate { get; set; }

        [JsonProperty("solar_type")]
        public string SolarType { get; set; }

        [JsonProperty("medical_support")]
        public string MedicalSupport { get; set; }

        [JsonProperty("energy_type")]
        public string EnergyType { get; set; }

        [JsonProperty("nmi_mirn_number")]
        public string NmiMirnNumber { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("updated_at")]
        [JsonConverter(typeof(ConditionalDateTimeConverter))]
        public DateTime? UpdatedAt { get; set; }

        [JsonProperty("transfers_status")]
        public string TransfersStatus { get; set; }

        [JsonProperty("connection_name")]
        public string ConnectionName { get; set; }

        [JsonProperty("connection_type")]
        public string ConnectionType { get; set; }

        [JsonProperty("sale_products_mobile.service_id")]
        //[JsonConverter(typeof(ConditionalLongConverter))]
        public string SaleProductsMobileServiceId { get; set; }

        [JsonProperty("provider")]
        public string Provider { get; set; }

        [JsonProperty("plan_mobile")]
        public string PlanMobile { get; set; }

        [JsonProperty("referal_code")]
        public string ReferalCode { get; set; }

        [JsonProperty("sale_products_broadband.service_id")]
        //[JsonConverter(typeof(ConditionalLongConverter))]
        public string SaleProductsBroadbandServiceId { get; set; }

        [JsonProperty("plan_broadband")]
        public string PlanBroadband { get; set; }
    }

    public class ConditionalDateTimeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            // This converter is specifically for nullable DateTime
            return objectType == typeof(DateTime?) || objectType == typeof(DateTime);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            // Handle null values
            if (reader.TokenType == JsonToken.Null)
            {
                return null;
            }

            var value = reader.Value?.ToString();

            // Check conditions for setting null
            if (value == "N/A" || string.IsNullOrEmpty(value))
            {
                return null;
            }

            // Parse valid date strings
            if (DateTime.TryParse(value, out var dateTime))
            {
                return dateTime;
            }

            // If parsing fails, throw an exception
            throw new JsonException($"Invalid date format: {value}");
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(value);
        }
    }

    public class ConditionalLongConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            // This converter is specifically for nullable DateTime
            return objectType == typeof(long?) || objectType == typeof(long);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            // Handle null values
            if (reader.TokenType == JsonToken.Null)
            {
                return null;
            }

            var value = reader.Value?.ToString();

            // Check conditions for setting null
            if (value == "N/A" || string.IsNullOrEmpty(value))
            {
                return null;
            }

            // Parse valid date strings
            if (long.TryParse(value, out var val))
            {
                return val;
            }

            // If parsing fails, throw an exception
            throw new JsonException($"Invalid Long format: {value}");
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(value);
        }
    }
}
