﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ThirdPartyApis.GreenDeals.Dtos;

namespace TheSolarProduct.ThirdPartyApis.GreenDeals
{
    public interface IGreenDealAppService : IApplicationService
    {
        //Task<HttpResponseMessage> GetJobSerialNo();

        //Task<HttpResponseMessage> GetCECPanels();

        //Task<HttpResponseMessage> GetCECInverters();

        //Task<HttpResponseMessage> GetCECBattteris();

        Task<CreateOrUpdateGreenDealJobOutputDto> CreateOrUpdateGreenDealJob(int jobId, int providerId);
    }
}
