﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ThirdPartyApis.GreenDeals.Dtos
{
    public class InstallerRootObject
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("response")]
        public InstallerResponse Response { get; set; }
    }

    public class InstallerResponse
    {
        [JsonProperty("body")]
        public List<InstallersObj> Installers { get; set; }
    }

    public class InstallersObj
    {
        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("saa_number")]
        public string SAANumber { get; set; }
        
        [JsonProperty("plumber_licenses")]
        public string PlumberLicenses { get; set; }
        
        [JsonProperty("grid_type")]
        public string GridType { get; set; }
        
        [JsonProperty("sps")]
        public string SPS { get; set; }
        
        [JsonProperty("accreditation_number")]
        public string AccreditationNumber { get; set; }

        [JsonProperty("electrician_license")]
        public string ElectricianLicense { get; set; }

        [JsonProperty("stauts")]
        public string Stauts { get; set; }
    }
}
