﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ThirdPartyApis.GreenDeals.Dtos
{
    public class StcJobRootObject
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("response")]
        public StcJobResponse Response { get; set; }
    }

    public class StcJobResponse
    {
        [JsonProperty("body")]
        public StcJobDetails Job { get; set; }
    }

    public class StcJobDetails
    {
        [JsonProperty("gwtid")]
        public string GWTId { get; set; }

        [JsonProperty("pvd_num")]
        public string PVDNumber { get; set; }

        //[JsonProperty("trade_mode")]
        //public string TradeMode { get; set; }
        
        //public object claimed_quantity { get; set; }
        
        //public object price { get; set; }
        
        //public bool gst { get; set; }
        
        //public string status { get; set; }
        
        //public string payment_status { get; set; }
        
        //public object submit_date { get; set; }
        
        //public object credit_memo { get; set; }
        
        //public object unqualified_reasons { get; set; }
        
        //public object failed_reasons { get; set; }
        
        //public string created_via { get; set; }
        
        //public object note { get; set; }
        
        //public object received_date { get; set; }
        
        //public string reference { get; set; }
        
        //public int amount { get; set; }
        
        //public DateTime created_at { get; set; }
        
        //public object finished_at { get; set; }
        
        //public object finished_location { get; set; }
        
        //public InstallDetail install_detail { get; set; }
        
        //public OwnerDetail owner_detail { get; set; }
        
        //public SystemDetail system_detail { get; set; }
        
        //public List<string> serial_numbers { get; set; }
        
        //public List<string> serial_numbers_photo { get; set; }
        
        //public Installer installer { get; set; }
        
        //public Designer designer { get; set; }
        
        //public Electrician electrician { get; set; }
        
        //public List<object> stc_documents { get; set; }
        //public List<object> coc_documents { get; set; }
        //public List<object> noc_documents { get; set; }
        //public List<object> other_documents { get; set; }
    }
}
