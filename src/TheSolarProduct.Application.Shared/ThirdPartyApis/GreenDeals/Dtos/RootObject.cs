﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace TheSolarProduct.ThirdPartyApis.GreenDeals.Dtos
{
    public class RootObject
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("response")]
        public Response Response { get; set; }

        public int ProductTypeId { get; set; }
    }

    public class Response
    {
        [JsonProperty("body")]
        public List<Body> CECProductItems { get; set; }
    }

    public class Body
    {
        [JsonProperty("id")]
        public string Model { get; set; }

        [JsonProperty("text")]
        public string Name { get; set; }

        [JsonProperty("brand_id")]
        public int BrandId { get; set; }

        [JsonProperty("name")]
        public string Series { get; set; }

        [JsonProperty("expiry_date")]
        public string ExpiryDate { get; set; }
    }

    public class ErrorRootObject
    {
        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("message")]
        public ErrorMessage Message { get; set; }
        
        [JsonProperty("code")]
        public int Code { get; set; }
    }

    public class ErrorMessage
    {
        [JsonProperty("param")]
        public string Parameter { get; set; }
    }
}
