﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace TheSolarProduct.ThirdPartyApis.GreenDeals.Dtos
{
    public class JobInfoRootObject
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("response")]
        public JobResponse Response { get; set; }
    }

    public class JobResponse
    {
        [JsonProperty("body")]
        public JobBody Body { get; set; }
        
        //public List<Body> CECProductItems { get; set; }
    }

    public class JobBody
    {
        [JsonProperty("gwtid")]
        public string GwtId { get; set; }

        [JsonProperty("pvd_num")]
        public string PvdNumber { get; set; }
        
        [JsonProperty("trade_mode")]
        public string TradeMode { get; set; }

        [JsonProperty("claimed_quantity")]
        public int? ClaimedQuantity { get; set; }

        [JsonProperty("price")]
        public Decimal? Price { get; set; }

        [JsonProperty("gst")]
        public bool Gst { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("payment_status")]
        public string PaymentStatus { get; set; }

        [JsonProperty("submit_date")]
        public DateTime? SubmitDate { get; set; }

        [JsonProperty("paid_date")]
        public DateTime? PaidDate { get; set; }

        [JsonProperty("credit_memo")]
        public string CreditMemo { get; set; }

        [JsonProperty("unqualified_reasons")]
        public string UnqualifiedReasons { get; set; }

        [JsonProperty("failed_reasons")]
        public string FailedReasons { get; set; }

        [JsonProperty("created_via")]
        public string CreatedVia { get; set; }

        [JsonProperty("note")]
        public string Note { get; set; }

        [JsonProperty("received_date")]
        public DateTime? ReceivedDate { get; set; }

        [JsonProperty("reference")]
        public string Reference { get; set; }

        [JsonProperty("amount")]
        public decimal? Amount { get; set; }

        [JsonProperty("created_at")]
        public DateTime? CreatedAt { get; set; }

        [JsonProperty("finished_at")]
        public DateTime? FinishedAt { get; set; }

        [JsonProperty("finished_location")]
        public string FinishedLocation { get; set; }

        [JsonProperty("rec_quantity")]
        public int? RecQuantity { get; set; }

        [JsonProperty("po_reference")]
        public string PoReference { get; set; }

        [JsonProperty("order_reference")]
        public string OrderReference { get; set; }

        [JsonProperty("install_detail")]
        public InstallDetail InstallDetail { get; set; }

        [JsonProperty("owner_detail")]
        public OwnerDetail OwnerDetail { get; set; }

        [JsonProperty("system_detail")]
        public SystemDetail SystemDetail { get; set; }

        [JsonProperty("serial_numbers")]
        public List<string> SerialNumbers { get; set; }

        [JsonProperty("serial_numbers_photo")]
        public List<string> SerialNumbersPhoto { get; set; }

        [JsonProperty("installer")]
        public InstallerDetails Installer { get; set; }

        [JsonProperty("designer")]
        public InstallerDetails Designer { get; set; }

        [JsonProperty("electrician")]
        public InstallerDetails Electrician { get; set; }

        [JsonProperty("stc_documents")]
        public List<Documents> StcDocuments { get; set; }

        [JsonProperty("coc_documents")]
        public List<Documents> CocDocuments { get; set; }

        [JsonProperty("other_documents")]
        public List<Documents> OtherDocuments { get; set; }

        [JsonProperty("noc_documents")]
        public List<Documents> NocDocuments { get; set; }

        [JsonProperty("installer_signed_at")]
        public string InstallerSignedAt { get; set; }

        [JsonProperty("installer_signed_signature")]
        public string InstallerSignedSignature { get; set; }

        [JsonProperty("designer_signed_at")]
        public string DesignerSignedAt { get; set; }

        [JsonProperty("designer_signed_signature")]
        public string DesignerSignedSignature { get; set; }

        [JsonProperty("owner_signed_at")]
        public string OwnerSignedAt { get; set; }

        [JsonProperty("owner_signed_signature")]
        public string OwnerSignedSignature { get; set; }

        [JsonProperty("witness_signed_at")]
        public string WitnessSignedAt { get; set; }

        [JsonProperty("witness_signed_signature")]
        public string WitnessSignedSignature { get; set; }

        [JsonProperty("retailer_signed_at")]
        public string RetailerSignedAt { get; set; }

        [JsonProperty("retailer_signed_signature")]
        public string RetailerSignedSignature { get; set; }

        [JsonProperty("retailer_witness_signed_at")]
        public string RetailerWitnessSignedAt { get; set; }

        [JsonProperty("retailer_witness_signed_signature")]
        public string RetailerWitnessSignedSignature { get; set; }

        [JsonProperty("installer_witness_signed_at")]
        public string InstallerWitnessSignedAt { get; set; }

        [JsonProperty("installer_witness_signed_signature")]
        public string InstallerWitnessSignedSignature { get; set; }

        [JsonProperty("designer_witness_signed_at")]
        public string DesignerWitnessSignedAt { get; set; }

        [JsonProperty("designer_witness_signed_signature")]
        public string DesignerWitnessSignedSignature { get; set; }
    }

    public class InstallDetail
    {
        [JsonProperty("unit_type")]
        public string UnitType { get; set; }

        [JsonProperty("unit_number")]
        public string UnitNumber { get; set; }

        [JsonProperty("post_code")]
        public string PostCode { get; set; }

        [JsonProperty("street_number")]
        public string StreetNumber { get; set; }

        [JsonProperty("street_name")]
        public string StreetName { get; set; }

        [JsonProperty("suburb")]
        public string Suburb { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("property_type")]
        public string PropertyType { get; set; }

        [JsonProperty("latitude")]
        public string Latitude { get; set; }

        [JsonProperty("longitude")]
        public string Longitude { get; set; }

        [JsonProperty("special_address")]
        public string SpecialAddress { get; set; }

        [JsonProperty("story")]
        public string Story { get; set; }

        [JsonProperty("nmi")]
        public string NMI { get; set; }

        [JsonProperty("job_number")]
        public string JobNumber { get; set; }

        [JsonProperty("installation_address")]
        public string InstallationAddress { get; set; }

        [JsonProperty("single_or_multi_story")]
        public string SingleOrMultiStory { get; set; }

        [JsonProperty("same_as_postal")]
        public bool SameAsPostal { get; set; }

        [JsonProperty("street_type")]
        public string StreetType { get; set; }
    }

    public class OwnerDetail
    {
        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("delivery_type")]
        public string DeliveryType { get; set; }

        [JsonProperty("delivery_number")]
        public string DeliveryNumber { get; set; }

        [JsonProperty("unit_type")]
        public string UnitType { get; set; }

        [JsonProperty("unit_number")]
        public string UnitNumber { get; set; }

        [JsonProperty("post_code")]
        public string PostCode { get; set; }

        [JsonProperty("street_number")]
        public string StreetNumber { get; set; }

        [JsonProperty("street_name")]
        public string StreetName { get; set; }

        [JsonProperty("suburb")]
        public string Suburb { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("price_to_the_home_owner")]
        public string PriceToTheHomeOwner { get; set; }

        [JsonProperty("home_owner_abn")]
        public string HomeOwnerAbn { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("type_name")]
        public string TypeName { get; set; }

        [JsonProperty("address_type")]
        public string AddressType { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("street_type")]
        public string StreetType { get; set; }

        [JsonProperty("owner_representative_name")]
        public string OwnerRepresentativeName { get; set; }

        [JsonProperty("owner_representative_position")]
        public string OwnerRepresentativePosition { get; set; }
    }

    public class SystemDetail
    {
        [JsonProperty("deemed_years")]
        public int DeemedYears { get; set; }

        [JsonProperty("installation_date")]
        public DateTime? InstallationDate { get; set; }

        [JsonProperty("type_of_connection")]
        public string TypeOfConnection { get; set; }

        [JsonProperty("connected_type")]
        public string ConnectedType { get; set; }

        [JsonProperty("is_battery")]
        public bool IsBattery { get; set; }

        [JsonProperty("system_mounting_type")]
        public string SystemMountingType { get; set; }

        [JsonProperty("rated_output")]
        public double? RatedOutput { get; set; }

        [JsonProperty("complete_unit")]
        public string CompleteUnit { get; set; }

        [JsonProperty("additional_upgrade_system_details")]
        public string AdditionalUpgradeSystemDetails { get; set; }

        [JsonProperty("install_additional_information")]
        public string InstallAdditionalInformation { get; set; }

        [JsonProperty("panel_brand")]
        public string PanelBrand { get; set; }

        [JsonProperty("panel_model")]
        public string PanelModel { get; set; }

        [JsonProperty("number_panels")]
        public int? NumberPanels { get; set; }

        [JsonProperty("number_inverters")]
        public int? NumberInverters { get; set; }

        [JsonProperty("watts_per_panel")]
        public int? WattsPerPanel { get; set; }

        [JsonProperty("inverter_manufacture")]
        public string InverterManufacture { get; set; }

        [JsonProperty("inverter_series")]
        public string InverterSeries { get; set; }

        [JsonProperty("inverter_model")]
        public string InverterModel { get; set; }

        [JsonProperty("battery_manufacture")]
        public string BatteryManufacture { get; set; }

        [JsonProperty("battery_series")]
        public string BatterySeries { get; set; }

        [JsonProperty("battery_model")]
        public string BatteryModel { get; set; }
    }

    public class InstallerDetails
    {
        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("accreditation", NullValueHandling = NullValueHandling.Ignore)]
        public string Accreditation { get; set; }

        [JsonProperty("unit_type")]
        public string UnitType { get; set; }

        [JsonProperty("unit_number")]
        public string UnitNumber { get; set; }

        [JsonProperty("street_number")]
        public string StreetNumber { get; set; }

        [JsonProperty("street_name")]
        public string StreetName { get; set; }

        [JsonProperty("street_type")]
        public string StreetType { get; set; }

        [JsonProperty("suburb")]
        public string Suburb { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("address_type")]
        public string AddressType { get; set; }

        [JsonProperty("delivery_type")]
        public string DeliveryType { get; set; }

        [JsonProperty("delivery_number")]
        public string DeliveryNumber { get; set; }

        [JsonProperty("postcode")]
        public string PostCode { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("license", NullValueHandling = NullValueHandling.Ignore)]
        public string License { get; set; }

        [JsonProperty("plumber_licenses")]
        public string PlumberLicenses { get; set; }
    }

    public class Documents
    {
        [JsonProperty("filename")]
        public string FileName { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }

    public class JobSerialNoRootObject
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("response")]
        public JobSerialNoResponse Response { get; set; }
    }

    public class JobSerialNoResponse
    {
        [JsonProperty("body")]
        public List<JobSerialNoBody> Body { get; set; }
    }

    public class JobSerialNoBody
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("code")]
        public string SerialNo { get; set; }
    }
}
