﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ThirdPartyApis.GreenDeals.Dtos
{
    public class ValidateJobOutputDto
    {
        public string FieldName { get; set; }

        public string ErrorMessage { get; set; }

        public string Description { get; set; }
    }
}
