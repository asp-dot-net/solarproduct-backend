﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace TheSolarProduct.ThirdPartyApis.GreenDeals.Dtos
{
    public class CreateOrUpdateGreenDealJobOutputDto
    {
        public HttpStatusCode Code { get; set; }

        public List<ValidateJobOutputDto> ErrorList { get; set; }

        public string Message { get; set; }
    }
}
