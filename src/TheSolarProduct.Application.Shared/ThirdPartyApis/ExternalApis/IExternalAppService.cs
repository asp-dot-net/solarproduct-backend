﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.ThirdPartyApis.ExternalApis.Dtos;

namespace TheSolarProduct.ThirdPartyApis.ExternalApis
{
    public interface IExternalAppService : IApplicationService
    {
        Task ImportExternalLead(CreateOrEditForExternalLeadDto input);
    }
}
