﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using TheSolarProduct.Leads;

namespace TheSolarProduct.ThirdPartyApis.ExternalApis.Dtos
{
    public class CreateOrEditForExternalLeadDto : EntityDto<int?>
    {
        public int TenantId { get; set; }

        [StringLength(LeadConsts.MaxCopanyNameLength, MinimumLength = LeadConsts.MinCopanyNameLength)]
        public string CompanyName { get; set; }
        
        public string Email { get; set; }

        public string Phone { get; set; }

        public string Mobile { get; set; }

        public string Address { get; set; }

        public string ExcelAddress { get; set; }

        public string Requirements { get; set; }

        public string Suburb { get; set; }

        public string State { get; set; }

        public string PostCode { get; set; }

        public string LeadSource { get; set; }

        public string UnitNo { get; set; }

        public string UnitType { get; set; }

        public string StreetNo { get; set; }

        public string StreetName { get; set; }

        public string StreetType { get; set; }

        public int? LeadStatusID { get; set; }

        public string LeadStatusName { get; set; }

        public int? AssignToUserID { get; set; }

        public string latitude { get; set; }

        public string longitude { get; set; }

        public bool? IsDuplicate { get; set; }

        [RegularExpression(LeadConsts.PhoneRegex)]
        public string AltPhone { get; set; }

        public string Type { get; set; }

        public string SolarType { get; set; }

        public string Area { get; set; }

        public string Country { get; set; }

        public string ABN { get; set; }

        public string Fax { get; set; }

        public string SystemType { get; set; }

        public string RoofType { get; set; }

        public string AngleType { get; set; }

        public string StoryType { get; set; }

        public string HouseAgeType { get; set; }

        public int? LeadSourceId { get; set; }

        public int? SuburbId { get; set; }

        public int StateId { get; set; }

        public string from { get; set; }

        public string IsGoogle { get; set; }

        public virtual int IsExternalLead { get; set; }

        public virtual int OrganizationId { get; set; }

        public string GCLId { get; set; }

        public string FormName { get; set; }

        public string UserIP { get; set; }
    }
}
