﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace TheSolarProduct.ThirdPartyApis.REC.Dtos
{
    public class RecRootObject
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("result")]
        public List<Result> Result { get; set; }
    }

    public class Result
    {
        [JsonProperty("actionType")]
        public string ActionType { get; set; }

        [JsonProperty("completedTime")]
        public DateTime CompletedTime { get; set; }

        [JsonProperty("certificateRanges")]
        public List<CertificateRange> CertificateRanges { get; set; }
    }

    public class CertificateRange
    {
        [JsonProperty("certificateType")]
        public string CertificateType { get; set; }

        [JsonProperty("registeredPersonNumber")]
        public long RegisteredPersonNumber { get; set; }

        [JsonProperty("accreditationCode")]
        public string AccreditationCode { get; set; }

        [JsonProperty("generationYear")]
        public long GenerationYear { get; set; }

        [JsonProperty("generationState")]
        public string GenerationState { get; set; }

        [JsonProperty("startSerialNumber")]
        public long StartSerialNumber { get; set; }

        [JsonProperty("endSerialNumber")]
        public long EndSerialNumber { get; set; }

        [JsonProperty("fuelSource")]
        public string FuelSource { get; set; }

        [JsonProperty("ownerAccount")]
        public string OwnerAccount { get; set; }

        [JsonProperty("ownerAccountId")]
        public long OwnerAccountId { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("generationMonth")]
        public GenerationMonth GenerationMonth { get; set; }

        [JsonProperty("creationDate")]
        public DateTime CreationDate { get; set; }
    }

    public class GenerationMonth
    {
        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("monthCode")]
        public long MonthCode { get; set; }

        [JsonProperty("monthName")]
        public string MonthName { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
