﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.WholesaleJobTypes.Dtos;
using TheSolarProduct.WholesalePropertyTypes.Dtos;

namespace TheSolarProduct.WholesalePropertyTypes
{
    public interface IWholesalePropertyTypeAppService : IApplicationService
    {
        Task<PagedResultDto<GetWholesalePropertyTypeForViewDto>> GetAll(GetAllWholesalePropertyTypeInput input);

        Task CreateOrEdit(CreateOrEditWholesalePropertyTypeDto input);

        Task<GetWholesalePropertyTypeForEditOutput> GetWholesalePropertyTypeForEdit(EntityDto input);

        Task Delete(EntityDto input);
    }
}
