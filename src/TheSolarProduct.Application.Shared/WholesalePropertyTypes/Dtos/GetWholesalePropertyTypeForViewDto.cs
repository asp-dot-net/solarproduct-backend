﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.WholesaleJobTypes.Dtos;

namespace TheSolarProduct.WholesalePropertyTypes.Dtos
{
    public class GetWholesalePropertyTypeForViewDto
    {
        public WholesalePropertTypeDto PropertyType { get; set; }
    }
}
