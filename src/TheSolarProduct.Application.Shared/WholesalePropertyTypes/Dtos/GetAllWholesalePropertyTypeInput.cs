﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholesalePropertyTypes.Dtos
{
    public class GetAllWholesalePropertyTypeInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
