﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.StockOrderStatus.Dtos;

namespace TheSolarProduct.StockOrderStatus
{
    public interface IStockOrderStatusAppService : IApplicationService
    {

        Task<PagedResultDto<GetStockOrderStatusForViewDto>> GetAll(GetAllStockOrderStatusInput input);
        Task<GetStockOrderStatusForEditOutput> GetStockOrderStatusForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditStockOrderStatusDto input);
        Task Delete(EntityDto input);
        Task<FileDto> GetStockOrderStatusToExcel(GetAllStockOrderStatusForExcelInput input);
    }
}
