﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockOrderStatus.Dtos
{
    public class GetAllStockOrderStatusInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
