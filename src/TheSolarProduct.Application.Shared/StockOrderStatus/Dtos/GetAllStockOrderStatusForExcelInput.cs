﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockOrderStatus.Dtos
{
    public class GetAllStockOrderStatusForExcelInput
    {
        public string Filter { get; set; }
    }
}
