﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.PurchaseCompanys.Dtos;

namespace TheSolarProduct.StockOrderStatus.Dtos
{
    public class GetStockOrderStatusForViewDto
    {
        public StockOrderStatusDto StockOrderStatus { get; set; }
    }
}
