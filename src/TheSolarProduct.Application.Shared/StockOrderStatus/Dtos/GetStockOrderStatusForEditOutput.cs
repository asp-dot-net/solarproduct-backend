﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.PurchaseCompanys.Dtos;

namespace TheSolarProduct.StockOrderStatus.Dtos
{
    public class GetStockOrderStatusForEditOutput
    {
        public CreateOrEditStockOrderStatusDto StockOrderStatus { get; set; }
    }
}
