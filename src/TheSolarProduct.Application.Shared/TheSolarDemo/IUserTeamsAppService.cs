﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using TheSolarProduct.Dto;
using TheSolarProduct.TheSolarDemo.Dtos;

namespace TheSolarDemo.TheSolarDemo
{
    public interface IUserTeamsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetUserTeamForViewDto>> GetAll(GetAllUserTeamsInput input);

        Task<GetUserTeamForViewDto> GetUserTeamForView(int id);

		Task<GetUserTeamForEditOutput> GetUserTeamForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditUserTeamDto input);

		Task Delete(EntityDto input);

		
		Task<List<UserTeamUserLookupTableDto>> GetAllUserForTableDropdown();
		
		Task<List<UserTeamTeamLookupTableDto>> GetAllTeamForTableDropdown();
		
    }
}