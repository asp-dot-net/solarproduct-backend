﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Dto;
using TheSolarProduct.TheSolarDemo.Dtos;

namespace TheSolarProduct.TheSolarDemo
{
    public interface ITeamsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetTeamForViewDto>> GetAll(GetAllTeamsInput input);
        
        Task<GetTeamForViewDto> GetTeamForView(int id);

		Task<GetTeamForEditOutput> GetTeamForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditTeamDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetTeamsToExcel(GetAllTeamsForExcelInput input);

        List<NameValue<string>> GetAllTeamsAssignedTo(GetAllUserTeamsInput input);
        List<NameValue<string>> GetAllTeams(string searchTerm);
    }
}