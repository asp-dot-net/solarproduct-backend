﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.TheSolarDemo.Dtos
{
    public class CategoryDto : EntityDto
    {
		public string Name { get; set; }

        public Boolean IsActive {  get; set; }



    }
}