﻿using TheSolarProduct.TheSolarProduct.Dtos;

namespace TheSolarProduct.TheSolarDemo.Dtos
{
    public class GetUserTeamForViewDto
    {
		public UserTeamDto UserTeam { get; set; }

		public string UserName { get; set;}

		public string TeamName { get; set;}


    }
}