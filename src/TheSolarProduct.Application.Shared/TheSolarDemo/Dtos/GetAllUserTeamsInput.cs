﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.TheSolarDemo.Dtos
{
    public class GetAllUserTeamsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

        public long? UserId { get; set; }
        public string UserNameFilter { get; set; }

        public long? TeamId { get; set; }
        public string TeamNameFilter { get; set; }

		 
    }
}