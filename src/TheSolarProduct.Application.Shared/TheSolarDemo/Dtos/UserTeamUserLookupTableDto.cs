﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.TheSolarDemo.Dtos
{
    public class UserTeamUserLookupTableDto
    {
		public long Id { get; set; }

		public string DisplayName { get; set; }
    }
}