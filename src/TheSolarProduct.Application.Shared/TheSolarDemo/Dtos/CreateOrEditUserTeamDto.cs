﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.TheSolarDemo.Dtos
{
    public class CreateOrEditUserTeamDto : EntityDto<int?>
    {

		 public long? UserId { get; set; }
		 
		 		 public int? TeamId { get; set; }
		 
		 
    }
}