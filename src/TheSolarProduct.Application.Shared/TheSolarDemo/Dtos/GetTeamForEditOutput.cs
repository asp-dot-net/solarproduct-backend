﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.TheSolarDemo.Dtos
{
    public class GetTeamForEditOutput
    {
		public CreateOrEditTeamDto Team { get; set; }


    }
}