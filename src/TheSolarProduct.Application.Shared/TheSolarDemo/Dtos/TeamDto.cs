﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.TheSolarProduct.Dtos
{
    public class TeamDto : EntityDto
    {
		public string Name { get; set; }

        public  Boolean IsActive { get; set; }

    }
}