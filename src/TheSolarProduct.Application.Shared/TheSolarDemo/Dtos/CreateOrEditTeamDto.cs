﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.TheSolarDemo.Dtos
{
    public class CreateOrEditTeamDto : EntityDto<int?>
    {

		[Required]
		[StringLength(TeamConsts.MaxNameLength, MinimumLength = TeamConsts.MinNameLength)]
		public string Name { get; set; }
        public Boolean IsActive { get; set; }


    }
}