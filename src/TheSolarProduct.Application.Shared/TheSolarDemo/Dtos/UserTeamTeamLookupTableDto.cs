﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.TheSolarDemo.Dtos
{
    public class UserTeamTeamLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}