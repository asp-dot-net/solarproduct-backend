﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.TheSolarDemo.Dtos
{
    public class GetAllCategoriesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }



    }
}