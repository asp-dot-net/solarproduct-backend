﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.TheSolarDemo.Dtos
{
    public class CreateOrEditCategoryDto : EntityDto<int?>
    {

		[StringLength(CategoryConsts.MaxNameLength, MinimumLength = CategoryConsts.MinNameLength)]
		public string Name { get; set; }

		public Boolean IsActive {  get; set; }	
		
		

    }
}