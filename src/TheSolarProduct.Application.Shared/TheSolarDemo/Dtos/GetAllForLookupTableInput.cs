﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.TheSolarDemo.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}