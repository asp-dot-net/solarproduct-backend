﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.TheSolarDemo.Dtos
{
    public class GetUserTeamForEditOutput
    {
		public CreateOrEditUserTeamDto UserTeam { get; set; }

		public string UserName { get; set;}

		public string TeamName { get; set;}


    }
}