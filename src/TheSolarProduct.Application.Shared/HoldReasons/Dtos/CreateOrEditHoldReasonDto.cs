﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.HoldReasons.Dtos
{
    public class CreateOrEditHoldReasonDto : EntityDto<int?>
    {

        [Required]
        public string JobHoldReason { get; set; }
        public Boolean IsActive { get; set; }
    }
}