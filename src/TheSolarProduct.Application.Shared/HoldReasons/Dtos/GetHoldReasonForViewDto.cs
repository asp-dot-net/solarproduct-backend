﻿namespace TheSolarProduct.HoldReasons.Dtos
{
    public class GetHoldReasonForViewDto
    {
        public HoldReasonDto JobHoldReason { get; set; }

    }
}