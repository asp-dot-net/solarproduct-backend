﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.HoldReasons.Dtos
{
    public class GetAllHoldReasonsForExcelInput
    {
        public string Filter { get; set; }

        public string HoldReasonFilter { get; set; }

    }
}