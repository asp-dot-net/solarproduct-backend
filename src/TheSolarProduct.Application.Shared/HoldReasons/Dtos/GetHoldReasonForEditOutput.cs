﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.HoldReasons.Dtos
{
    public class GetHoldReasonForEditOutput
    {
        public CreateOrEditHoldReasonDto JobHoldReason { get; set; }

    }
}