﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.HoldReasons.Dtos
{
    public class GetAllHoldReasonsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string HoldReasonFilter { get; set; }

    }
}