﻿using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.HoldReasons.Dtos
{
    public class HoldReasonDto : EntityDto
    {
        public string JobHoldReason { get; set; }
        public Boolean IsActive { get; set; }
    }
}