﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.HoldReasons.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}