﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.HoldReasons.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.HoldReasons
{
    public interface IHoldReasonsAppService : IApplicationService
    {
        Task<PagedResultDto<GetHoldReasonForViewDto>> GetAll(GetAllHoldReasonsInput input);

        Task<GetHoldReasonForViewDto> GetHoldReasonForView(int id);

        Task<GetHoldReasonForEditOutput> GetHoldReasonForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditHoldReasonDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetHoldReasonsToExcel(GetAllHoldReasonsForExcelInput input);

    }
}