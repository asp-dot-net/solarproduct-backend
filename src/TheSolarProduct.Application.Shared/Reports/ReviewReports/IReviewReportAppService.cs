﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Reports.ReviewReports.Dtos;

namespace TheSolarProduct.Reports.ReviewReports
{
    public interface  IReviewReportAppService : IApplicationService
    {
        Task<PagedResultDto<ReviewReportDto>> GetAllReviewList(ReviewReportInputDto input);
    }
}
