﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.ReviewReports.Dtos
{
    public class ReviewReportInputDto : PagedAndSortedResultRequestDto
    {
        public string FilterName { get; set; }
        public string Filter { get; set; }
        public int? ReviewType { get; set; }
        public int OrganizationId { get; set; }
        public string Datefilter { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
