﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.ReviewReports.Dtos
{
    public class ReviewReportDto :EntityDto
    {
       public string User {  get; set; }   
        public string ACTcount {  get; set; }
        public string NSWcount { get; set; }
        public string NTcount { get; set; }
        public string QLDcount { get; set; }
        public string SAcount { get; set; }
        public string TAScount { get; set; }
        public string VICcount { get; set; }
        public string WAcount { get; set; }
        
    }
}
