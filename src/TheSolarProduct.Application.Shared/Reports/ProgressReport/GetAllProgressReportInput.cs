﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using System.Text;

namespace TheSolarProduct.Reports.ProgressReport
{
    public class GetAllProgressReportInput : PagedAndSortedResultRequestDto
    {
        public int OrganizationId { get; set; }
        
        public string Filter {  get; set; }
        
        public string StateFilter { get; set; }
        
        public string Datefilter { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
