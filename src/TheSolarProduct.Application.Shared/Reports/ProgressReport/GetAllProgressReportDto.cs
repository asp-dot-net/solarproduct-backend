﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.ProgressReport
{
    public class GetAllProgressReportDto : EntityDto
    {
        public string JobNumber { get; set; }
      
        public string State { get; set; }
        
        public decimal? SystemkW { get; set; }
      
        public int? FirstDepositDays {  get; set; }
      
        public int? ActiveDays { get; set; }
      
        public int? InstallationBookDays { get; set; }
      
        public int? InstallationCompleteDays { get; set; }
    }
}
