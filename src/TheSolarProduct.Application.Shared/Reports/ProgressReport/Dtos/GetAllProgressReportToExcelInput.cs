﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.ProgressReport.Dtos
{
    public class GetAllProgressReportToExcelInput
    {
        public int OrganizationId { get; set; }
        
        public string Filter { get; set; }
        
        public string StateFilter { get; set; }
        
        public string Datefilter { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
