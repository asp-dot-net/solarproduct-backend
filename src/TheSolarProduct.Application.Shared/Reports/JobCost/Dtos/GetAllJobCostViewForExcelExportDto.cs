﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.JobCost.Dtos
{
    public class GetAllJobCostViewForExcelExportDto : EntityDto
    {
        public string JobNumber { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public string AreaName { get; set; }

        public string Team { get; set; }

        public string SalesRep { get; set; }

        public string JobType { get; set; }

        public string JobStatus { get; set; }

        public List<string> SystemDetails { get; set; }

        public decimal? PanelSize { get; set; }

        public int? NoOfPanel { get; set; }

        public decimal? PricePerKw { get; set; }

        public decimal? SystemCapacity { get; set; }

        public decimal? PanelCost { get; set; }

        public decimal? InverterCost { get; set; }

        public decimal? BatteryCost { get; set; }

        public decimal? InstallationCost { get; set; }

        public string InstallationCostType { get; set; }

        public decimal? BatteryInstallation { get; set; }

        public decimal? Racking { get; set; }

        public decimal? Accessories { get; set; }

        public decimal? AdCost { get; set; }

        public decimal? FixedExpense { get; set; }

        public decimal? ExtraInsCost { get; set; }

        public decimal? TotalCost { get; set; }

        public decimal? STCRebate { get; set; }

        public decimal? CostAfteRebate { get; set; }

        public decimal? SellPrice { get; set; }

        public decimal? NP { get; set; }

        public decimal? NPPercentage { get; set; }

        public decimal? GP { get; set; }

        public List<decimal> VariationCost { get; set; }

        public decimal? NP_SysCap { get; set; }

        public string Category { get; set; }

        public DateTime? CategoryDate { get; set; }

        public decimal? FinanceCost { get; set; }
        public decimal? BatteryKw { get; set; }

    }
}
