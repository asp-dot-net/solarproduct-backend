﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.JobCost.Dtos
{
    public class GetAllJobCostMonthWiseViewDto : EntityDto
    {
        public string Month { get; set; }

        public decimal MegaWatt { get; set; }

        public decimal SystemCapacity { get; set; }

        public decimal BatteryCapacity { get; set; }

        public int NoOfPanel { get; set; }

        public decimal PricePerKw { get; set; }

        public decimal PanelCost { get; set; }

        public decimal InverterCost { get; set; }

        public decimal BatteryCost { get; set; }

        public decimal InstallationCost { get; set; }

        public decimal BatteryInstallationCost { get; set; }

        public decimal Racking { get; set; }

        public decimal Accessories { get; set; }

        public decimal AdCost { get; set; }

        public decimal FixedExpense { get; set; }

        public decimal ExtraInstallationCost { get; set; }

        public decimal FinanceCost { get; set; }

        public decimal TotalCost { get; set; }

        public decimal STCRebate { get; set; }

        public decimal CostAfteRebate { get; set; }

        public decimal SellPrice { get; set; }

        public decimal NP { get; set; }

        public decimal NPPercentage { get; set; }

        public decimal GP { get; set; }

        public decimal NP_SysCap { get; set; }

    }
}
