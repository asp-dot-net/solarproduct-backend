﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.JobCost.Dtos
{
    public class SalesRepProfitLossDto : EntityDto
    {
        public string SalesRep { get; set; }
        public decimal Profit { get; set; }
        public decimal Loss { get; set; }
        public string Category { get; set; }
        public decimal? SystemKw { get; set; }
        public decimal? BatteryKw { get; set; }

    }
}
