﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.JobCost.Dtos
{
    public class GetAllJobCostMonthWiseInput : PagedAndSortedResultRequestDto
    {
        public int OrganizationUnit { get; set; }

        public string DateType { get; set; }

        public int Year { get; set; }

        public List<string> State { get; set; }

        public string AreaFilter { get; set; }
    }
}
