﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.JobCost.Dtos
{
    public class GetCheckInstallationCostOutput
    {
        public bool IsExists { get; set; }

        public int? PostCodeId { get; set; }

        public DateTime? Date { get; set; }
    }

    public class GetCheckSystemCostOutput
    {
        public bool IsExists { get; set; }

        public string ProductType { get; set; }

        public int? InstallationItemPeriodId { get; set; }

        public int OrganizationUnit { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string ProductItemName { get; set; }
    }
}
