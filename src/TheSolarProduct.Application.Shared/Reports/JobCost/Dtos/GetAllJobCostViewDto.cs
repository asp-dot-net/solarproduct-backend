﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.JobCost.Dtos
{
    public class GetAllJobCostViewDto : EntityDto
    {
        public int? LeadId { get; set; }

        public string JobNumber { get; set; }

        public string State { get; set; }

        public string PostCode { get; set; }
        
        public string AreaName { get; set; }

        public string Team { get; set; }
        
        public string SalesRep { get; set; }
        
        public decimal? PanelSize { get; set; }

        public int? NoOfPanel { get; set; }

        public decimal? PricePerKw { get; set; }

        public decimal? SystemCapacity { get; set; }

        public decimal? PanelCost { get; set; }

        public decimal? InverterCost { get; set; }

        public decimal? BatteryCost { get; set; }

        public decimal? InstallationCost { get; set; }

        public string InstallationCostType { get; set; }

        public decimal? BatteryInstallation { get; set; }

        public decimal? Racking { get; set; }

        public decimal? Accessories { get; set; }

        public decimal? AdCost { get; set; }

        public decimal? FixedExpense { get; set; }

        public decimal? ExtraInsCost { get; set; }

        public decimal? TotalCost { get; set; }

        public decimal? STCRebate { get; set; }

        public decimal? CostAfteRebate { get; set; }
        
        public decimal? SellPrice { get; set; }

        public decimal? NP { get; set; }

        public decimal? NPPercentage { get; set; }

        public decimal? GP { get; set; }

        public int? JobStatusId { get; set; }

        public int? AssignToUserID { get; set; }

        public decimal? FinOptionPercentage { get; set; }

        public decimal? NP_SysCap { get; set; }

        public string Category { get; set; }

        public DateTime? CategoryDate { get; set; }

        public decimal? FinanceCost { get; set; }

        public JobCostSummary JobCostSummary { get; set; }

        public decimal? BatteryKw { get; set; }

    }

    public class JobCostSummary
    {
        public int NoOfPanels { get; set; }

        public decimal PricePerKw { get; set; }

        public decimal MegaWatt { get; set; }

        public decimal SystemCapacity { get; set; }

        public decimal PanelCost { get; set; }

        public decimal InverterCost { get; set; }

        public decimal BatteryCost { get; set; }

        public decimal InstallationCost { get; set; }

        public decimal BatteryInstallation { get; set; }

        public decimal Racking { get; set; }

        public decimal Accessories { get; set; }

        public decimal AdCost { get; set; }

        public decimal FixedExpense { get; set; }

        public decimal ExtraInsCost { get; set; }

        public decimal FinanceCost { get; set; }

        public decimal TotalCost { get; set; }

        public decimal STCRebate { get; set; }

        public decimal CostAfteRebate { get; set; }

        public decimal SellPrice { get; set; }

        public decimal NP { get; set; }

        public decimal NPPercentage { get; set; }

        public decimal GP { get; set; }

        public decimal NP_SysCap { get; set; }

        public decimal? BatteryKw { get; set; }

    }
}
