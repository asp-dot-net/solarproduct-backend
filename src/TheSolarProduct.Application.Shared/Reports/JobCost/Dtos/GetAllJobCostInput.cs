﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.JobCost.Dtos
{
    public class GetAllJobCostInput : PagedAndSortedResultRequestDto
    {
        public int OrganizationUnit { get; set; }

        public string Filter { get; set; }

        public List<int?> JobStatus { get; set; }

        public string DateType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public bool isEmpJobCost { get; set; }

        public string SalesRap { get; set; }

        public int? Cost { get; set; }

        public string AreaName { get; set; }

        public int? SystemFilter { get; set; }

        public string Category { get; set; }

        public string CurruntDate { get; set; }

        public string State { get; set; }

        public int? Installation { get; set; }
        
        public int? StartSysCapRange { get; set; }
        
        public int? EndSysCapRange { get; set; }

        public int? ProductItemId { get; set; }
    }
}
