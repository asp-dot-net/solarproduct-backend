﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class SummaryJobCount
    {
        public int TotalProfit { get; set; }
        public int TotalLoss { get; set; }
    }
}
