﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.JobCost
{
    public class SummaryJobCostCount
    {
        public decimal? TotalProfit { get; set; }

        public decimal? TotalLoss { get; set; }
        
    }
}
