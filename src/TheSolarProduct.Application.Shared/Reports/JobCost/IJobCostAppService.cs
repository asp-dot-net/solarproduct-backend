﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Reports.JobCost.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.ProgressReport;

namespace TheSolarProduct.Reports.JobCost
{
    public interface IJobCostAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllJobCostViewDto>> GetAll(GetAllJobCostInput input);

        Task<FileDto> GetJobCostToExcel(GetAllJobCostForExcelInput input);

        Task<SummaryJobCostCount> TotalCount(GetAllJobCostForExcelInput input);

        Task<GetCheckInstallationCostOutput> GetCheckInstallationCost(int typeId, int jobId, string postCode, string dateType);

        Task<GetCheckSystemCostOutput> GetCheckSystemCost(int typeId, int jobId, string dateType);

        Task<PagedResultDto<GetAllJobCostMonthWiseViewDto>> GetAllJobCostMonthWise(GetAllJobCostMonthWiseInput input);

        Task<FileDto> GetAllJobCostMonthWiseExcel(GetAllJobCostMonthWiseExcelInput input);
        Task<PagedResultDto<GetAllProgressReportDto>> GetAllProgressReport(GetAllProgressReportInput input);
    }
}
