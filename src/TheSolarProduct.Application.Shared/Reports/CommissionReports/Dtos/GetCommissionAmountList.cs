﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CommissionReports.Dtos
{
    public class GetCommissionAmountList
    {
        public string Range { get; set; }

        public double? value { get; set; }
    }
}
