﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CommissionReports.Dtos
{
    public class GetAllCommissionPaidReportInputDto : PagedAndSortedResultRequestDto
    { 
        public string filter { get; set; }  

        public int Orgid { get; set; }

        public int? TeamId { get; set; }

        public int? UserId { get; set;}

        public DateTime? StartDate { get; set;}

        public DateTime? EndDate { get; set;}
    }
}
