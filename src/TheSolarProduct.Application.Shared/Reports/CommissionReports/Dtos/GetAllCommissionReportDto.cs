﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CommissionReports.Dtos
{
    public class GetAllCommissionReportDto : EntityDto
    {
        //public string ProjectNo { get; set; }

        //public string CustomerName { get; set; }

        //public DateTime? InstallDate { get; set; }

        //public string State { get; set; }

        //public decimal? TotalCost { get; set; }

        //public DateTime? CommissionDate { get; set; }   

        public string SalesRap { get; set; }

        public int? JobCount { get; set; }   

        public decimal? SystemCapacity { get; set; }

        public int? NoOfPanel { get; set; }

        public double? Commission { get; set; }
        
        public bool IsSelect { get; set; }

        public List<int > jobs { get; set; }

        public List<GetCommissionAmountList> CommissionAmountList { get; set; }

        public SummaryCommissionReport summary { get; set; }

        public decimal? BatteryKw { get; set; }

        public string CommissionDate { get; set; }   

        public decimal? TotalCommission { get; set; }
        
        public decimal? TotalBonusCommission { get; set;}
        public DateTime? CommitionDate { get; set; }

        public bool IsSalesRap {get; set;}

        

    }

    public class SummaryCommissionReport
    {
        public int? TotalJobCount { get; set; }

        public decimal? TotalSystemCapacity { get; set; }

        public int? TotalNoOfPanel { get; set; }

        public double? TotalCommission { get; set; }

        public decimal? TotalBatteryKw { get; set; }

    }
}
