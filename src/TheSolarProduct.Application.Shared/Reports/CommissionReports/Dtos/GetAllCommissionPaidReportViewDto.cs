﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CommissionReports.Dtos
{
    public class GetAllCommissionPaidReportViewDto : EntityDto
    {
        public string JobNumber { get; set; }

        public string SalesREp { get; set; }

        public int? NoOfPanel { get; set; }

        public decimal? SystemCapacity { get; set; }

        public DateTime? CommissionDate { get; set; }
        public decimal? CommissionAmount { get; set; }
        public decimal? ActualAmount { get; set; }
    }
}
