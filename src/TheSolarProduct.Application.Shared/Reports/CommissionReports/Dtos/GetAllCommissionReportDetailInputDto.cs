﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CommissionReports.Dtos
{
    public class GetAllCommissionReportDetailInputDto
    {
        public string filter { get; set; }

        public int Orgid { get; set; }

        public int UserId { get; set; }

        public string dateFilter { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string CommissionDate { get; set; }
    }
}
