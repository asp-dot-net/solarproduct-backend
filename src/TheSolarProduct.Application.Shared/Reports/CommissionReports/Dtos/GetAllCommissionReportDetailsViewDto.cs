﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CommissionReports.Dtos
{
    public class GetAllCommissionReportDetailsViewDto : EntityDto
    {
        public string ProjectNo { get; set; }

        public DateTime? InstallDate { get; set;}

        public string State { get; set; }

        public decimal? SystemCapacity { get; set; }

        public decimal? BatteryKW { get; set; }
    }
}
