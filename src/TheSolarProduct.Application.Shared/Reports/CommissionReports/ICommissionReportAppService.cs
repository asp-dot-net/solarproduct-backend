﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheSolarProduct.LeadGeneration.Dtos;
using TheSolarProduct.Reports.CommissionReports.Dtos;

namespace TheSolarProduct.Reports.CommissionReports
{
    public interface ICommissionReportAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllCommissionReportDto>> GetAll(GetAllCommissionReportInputDto input);
        Task CreateCommission(CreateCommissionDto input);

        //Task<double> getCommissionAmountByUserId(List<int> jobIds);

        Task<List<GetAllCommissionReportDetailsViewDto>> GetAllCommissionReportDetail(GetAllCommissionReportDetailInputDto input);

        Task<PagedResultDto<GetAllCommissionPaidReportViewDto>> GetAllCommissionPaidReport(GetAllCommissionPaidReportInputDto input);



    }
}
