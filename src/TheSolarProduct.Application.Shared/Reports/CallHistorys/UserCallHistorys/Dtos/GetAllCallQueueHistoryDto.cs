﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos
{
    public class GetAllCallQueueHistoryDto : EntityDto
    {
        public string Name { get; set; }

        public int Count { get; set; }

         public int ExtentionNumber { get; set; }

         public int TotalCalls { get; set; }
    }
}
