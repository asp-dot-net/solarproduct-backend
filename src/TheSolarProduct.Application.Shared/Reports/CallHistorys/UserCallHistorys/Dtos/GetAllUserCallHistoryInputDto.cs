﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos
{
    public class GetAllUserCallHistoryInputDto : PagedAndSortedResultRequestDto
    {
        public int OrganizationUnitId { get; set; }

        public int UserId { get; set; }

        public string State { get; set; }

        public string Mobile { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
