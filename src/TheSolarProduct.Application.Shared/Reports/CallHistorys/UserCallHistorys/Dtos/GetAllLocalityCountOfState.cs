﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos
{

    public class GetAllLocalityCountOfState
    {
        public string FromLocationLocality { get; set; }
        public int Count { get; set; }

    }
}
