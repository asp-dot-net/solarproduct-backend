﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos
{
    public class GetCallQueueWeeklyHistoryInput : PagedAndSortedResultRequestDto
    {
        public int OrganizationUnitId { get; set; }

        public DateTime? StartDate { get; set; }

        public string Mobile { get; set; }

    }
}
