﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos
{
    public class GetCallQueueDetailsCountInput
    {
        public int OrganizationUnitId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string ExtentionNumber { get; set; }

        public string QueueExtentionnumber { get; set; }

        public string Mobile { get; set; }

    }
}
