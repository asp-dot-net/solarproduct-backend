﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Promotions.Dtos;

namespace TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos
{
    public class GetAllCallFlowQueueCallHistoryDetailsDto
    {
        public string Mobile { get;set; }

        public DateTime StartTime { get;set; }

        public DateTime EndTime { get;set; }

        public string Duration { get; set; }

        public GetcallFlowDetailSummary Summary { get; set; }
    }

    public class GetcallFlowDetailSummary
    {
        public int TotalDuration { get; set; }

        public double AvgDuration { get; set; }
    }
}
