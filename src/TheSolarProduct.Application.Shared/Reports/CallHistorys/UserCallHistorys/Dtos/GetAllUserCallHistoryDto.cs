﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos
{
    public class GetAllUserCallHistoryDto : EntityDto
    {

        public string UserName { get; set; }

        public int Inbound { get; set; }

        public int Outbound { get; set; }

        public int Notanswered { get; set; }

        public int Missed { get; set; }

        public string Agent { get; set; }

        public UserCallHistoryCount Summary { get; set; }

    }

    public class UserCallHistoryCount
    {
        public int TotalMissed { get; set; }

        public int TotalInbound { get; set; }

        public int TotalNotanswered { get; set; }

        public int TotalOutbound { get; set; }



    }
}
