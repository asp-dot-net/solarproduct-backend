﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos
{
    public class GetAllCallFlowQueueCountDetailsDto
    {
        public string User { get; set; }

        public int Count { get;  set; }

        public string ExtentionNumber { get; set; }

    }
}
