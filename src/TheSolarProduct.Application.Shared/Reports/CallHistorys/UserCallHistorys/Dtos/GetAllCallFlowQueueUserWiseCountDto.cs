﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos
{
    public class GetAllCallFlowQueueUserWiseCountDto : EntityDto
    {
        public string User { get; set; }

        public int Count { get; set; }

        public string ExtentionNumber { get; set; }

        public int TotalCalls { get; set; }
    }
}
