﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos
{
    public class GetAllStateCallHistoryDto : EntityDto
    {
        public DateTime CreationDate { get; set; }

        public int CreateUser { get; set; }

        public string From { get; set; }

        public string CreateTime { get; set; }

        public string CreateMethod { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public DateTime AnswerTime { get; set; }

        public string AnswerDuration { get; set; }

        public string FromLocationRegionCode { get; set; }

        public string FromLocationAdministrativeArea { get; set; }

        public string FromLocationLocality { get; set; }

        public string FromLocationCoordinatesLatitude { get; set; }

        public string FromLocationCoordinatesLongitude { get; set; }

        public string LabelsMoli { get; set; }

        public string PriceCurrencyCode { get; set; }

        public string ToDisplayName { get; set; }

        public string RingTime { get; set; }

        public string Cost { get; set; }
        public StateCallHistoryCount Summary { get; set; }

    }

    public class StateCallHistoryCount
    {
        public int TotalQLD { get; set; }

        public int TotalSA { get; set; }

        public int TotalNSW { get; set; }

        public int TotalVIC { get; set; }

        public int TotalOther { get; set; }



    }
}
