﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos
{
    public class GetUserCallHistoryDetailsInputDto
    {
        public string State { get; set; }

        public string Mobile { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Agent { get; set; }

        public string CallType { get; set; }
        public string DurationFilter { get; set; }
    }
}
