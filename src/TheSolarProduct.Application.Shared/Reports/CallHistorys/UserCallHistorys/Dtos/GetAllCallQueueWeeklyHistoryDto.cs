﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos
{
    public class GetAllCallQueueWeeklyHistoryDto : EntityDto
    {
        public string Name { get; set; }

        public int Count { get; set; }

        public int ExtentionNumber { get; set; }

        public int TotalCalls { get; set; }

        public int MondayCount { get; set; }

        public int TuesdayCount { get; set; }

        public int WednesdayCount { get; set; }

        public int ThrusdayCount { get; set; }

        public int FridayCount { get; set; }

        public int SatdayCount { get; set; }

        public int SundayCount { get; set; }
    }
}
