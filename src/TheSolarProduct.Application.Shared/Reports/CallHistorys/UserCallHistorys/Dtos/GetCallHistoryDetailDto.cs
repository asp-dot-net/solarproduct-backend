﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos
{
    public class GetCallHistoryDetailDto
    {
        public string Mobile { get; set; }

        public string Duration { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
