﻿using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Reports.CallHistorys.UserCallHistorys
{
    public interface IUserCallHistoryAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllUserCallHistoryDto>> GetAll(GetAllUserCallHistoryInputDto input);

        Task<FileDto> GetUserCallHistoryToExcel(GetAllUserCallHistoryExcelInputDto input);

        Task<FileDto> GetUserCallHistoryDetailsToExcel(GetUserCallHistoryDetailsInputDto input);

        Task<List<GetCallHistoryDetailDto>> GetUSerCallHistoryDetails(GetUserCallHistoryDetailsInputDto input);

        Task<PagedResultDto<GetAllStateCallHistoryDto>> GetAllStateWiseCallHistory(GetAllStateCallHistoryInputDto input);

        Task<FileDto> GetStateWiseCallHistoryToExcel(GetAllStateCallHistoryExcelInputDto input);

        Task<PagedResultDto<GetAllCallQueueHistoryDto>> GetAllCallQueueCallHistory(GetCallQueueHistoryInput input);

        Task<List<GetAllCallFlowQueueCountDetailsDto>> GetCallQueueDetailsCount(GetCallQueueDetailsCountInput input);

        Task<List<GetAllCallFlowQueueCallHistoryDetailsDto>> GetCallQueueCallHistoryDetails(GetCallQueueDetailsCountInput input);

        Task<FileDto> GetAllCallQueueCallHistoryExcel(GetCallQueueHistoryInput input);

        Task<FileDto> GetCallQueueDetailsCountExcel(GetCallQueueDetailsCountInput input);

        Task<FileDto> GetCallQueueCallHistoryDetailsExcel(GetCallQueueDetailsCountInput input);

        Task<List<GetAllLocalityCountOfState>> GetAllLocalityCountOfStateHistory(GetAllStateCallHistoryInputDto input);

        Task<FileDto> GetAllLocalityCountOfStateHistoryExport(GetAllStateCallHistoryInputDto input);

        Task<PagedResultDto<GetAllCallQueueWeeklyHistoryDto>> GetAllCallQueueweeklyHistory(GetCallQueueWeeklyHistoryInput input);

    }

}
