﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.LeadSolds.Dtos
{
    public class GetAllLeadSoldInputDto : PagedAndSortedResultRequestDto
    {
        public int? OrganizationUnitId { get; set; }

        public int? UserId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

		public List<int> LeadSourceIdFilter { get; set; }

        public int ReportType { get; set; }

        public int? TeamId { get; set; }

        public List<string> states { get; set; }
    }
}
