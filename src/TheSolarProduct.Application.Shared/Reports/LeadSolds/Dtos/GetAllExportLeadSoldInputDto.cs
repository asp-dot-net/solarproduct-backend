﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.LeadSolds.Dtos
{
    public class GetAllExportLeadSoldInputDto
    {
        public int? OrganizationUnitId { get; set; }

        public int? UserId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public List<int> LeadSourceIdFilter { get; set; }

        public int ReportType { get; set; }

        public int excelorcsv { get; set; }

        public List<int?> states { get; set; }
    }
}
