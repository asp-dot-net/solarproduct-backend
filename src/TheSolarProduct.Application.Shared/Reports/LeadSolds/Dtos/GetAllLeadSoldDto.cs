﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.LeadSolds.Dtos
{
    public class GetAllLeadSoldDto : EntityDto
    {
        public string UserName { get; set; }

        public int? LeadAssign { get; set; }

        public int? ActualLeadAssign { get; set; }

        public int? SoldLead { get; set; }

        public int? ActualSoldLead { get; set; }

        public int? Ratio { get; set; }

        public int? ActualRatio { get; set; }

        public double Missing { get; set; }

        public double ActualMissing { get; set; }

        public string LeadSorce { get; set; }

        public LeadSoldSummaryCount SummaryCount { get; set; }

    }

    public class LeadSoldSummaryCount
    {
        public int? LeadAssignCount { get; set; }

        public int? ActualLeadAssignCount { get; set; }

        public int? SoldLeadCount { get; set; }

        public int? ActualSoldLeadCount { get; set; }

        public int? RatioCount { get; set; }

        public int? ActualRatioCount { get; set; }

        public double? MissingCount { get; set; }

        public double? ActualMissingCount { get; set; }

    }
}
