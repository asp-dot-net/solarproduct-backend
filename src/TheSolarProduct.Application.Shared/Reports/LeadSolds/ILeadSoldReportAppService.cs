﻿using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.LeadAssigns.Dtos;
using TheSolarProduct.Reports.LeadSolds.Dtos;

namespace TheSolarProduct.Reports.LeadSolds
{
    public interface ILeadSoldReportAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllLeadSoldDto>> GetAll(GetAllLeadSoldInputDto input);

        Task<FileDto> GetLeadSoldToExcel(GetAllExportLeadSoldInputDto input);


    }
}
