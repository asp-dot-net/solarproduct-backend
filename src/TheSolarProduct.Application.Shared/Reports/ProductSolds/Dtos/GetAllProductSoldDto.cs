﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.ProductSolds.Dtos
{
    public class GetAllProductSoldDto : EntityDto
    {
        public string ProductItem { get; set; }

        public int? NSW { get; set; }

        public int? SA { get; set; }

        public int? VIC { get; set; }

        public int? QLD { get; set; }

        public int? Total { get; set; }

        public SummaryProductSold summary { get; set; }

    }

    public class SummaryProductSold
    {
        public int? SumNSW { get; set;}

        public int? SumSA { get; set;}

        public int? SumVIC { get; set;}

        public int? SumQLD { get; set;}

        public int? TotalSum { get; set; }

    }

    public class ProductSoldPriceDto
    {
        public decimal? TotalPrice { get; set; }

        public decimal? NSWMetroPrice { get; set; }

        public decimal? NSWRegionalPrice { get; set; }

        public decimal? SAMetroPrice { get; set; }

        public decimal? SARegionalPrice { get; set; }

        public decimal? VICMetroPrice { get; set; }

        public decimal? VICRegionalPrice { get; set; }

        public decimal? QLDMetroPrice { get; set; }

        public decimal? QLDRegionalPrice { get; set; }
    }
}
