﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.ProductSolds.Dtos
{
    public class GetAllProductSoldInputDto : PagedAndSortedResultRequestDto
    {
        public int? OrganizationUnitId { get; set; }

        public int? UserId { get; set; }

        public string DateType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public List<int?> JobStatus { get; set; }

        public int? ProductTypeId { get; set;}

        public int? ProductItemId { get; set;}

        public string ProductItem { get; set; }

        public int? TeamId { get; set; }

        public int? SystemFilter { get; set; }

        public string AreaName { get; set; }
    }
}
