﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.ProductSolds.Dtos;

namespace TheSolarProduct.Reports.ProductSolds
{
    public interface IProductSoldReportAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllProductSoldDto>> GetAll(GetAllProductSoldInputDto input);

        Task<FileDto> GetProductSoldToExcel(GetAllExcelProductSoldInputDto input);

        Task<ProductSoldPriceDto> GetProductSoldPriceDto(GetAllExcelProductSoldInputDto input);

    }
}
