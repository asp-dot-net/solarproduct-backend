﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.RescheduleInstallations.Dtos
{
    public class GetRescheduleInstallationHistoryDto : EntityDto
    {
        public string InstallationDate { get; set; }

        public string Installer { get; set; }

        public DateTime? CreateDate { get; set; }

    }
}
