﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace TheSolarProduct.Reports.RescheduleInstallations.Dtos
{
    public class GetAllRescheduleInstallationDto : EntityDto
    {
        public string Jobnumber { get; set; }

        public string State { get; set; }
        public string Area { get; set; }

        public string PostCode { get; set; }

        public int? NoOfPanel { get; set; }

        public decimal? SystemCapacity { get; set; }

        public int? RescheduleCount { get; set;}

        public List<SummaryRescheduleInstallationDto> summary { get; set; }
    }

    public class SummaryRescheduleInstallationDto
    {
        public string state { get; set; }

        public int? count { get; set; } 

    }
}
