﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.RescheduleInstallations.Dtos
{
    public class GetRescheduleInstallationInputDto : PagedAndSortedResultRequestDto
    {
        public string FilterName { get; set; }

        public string Filter { get; set; }

        public string JobDateFilter { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? OrganizationUnit { get; set; }
        public String AreaName { get; set; }
        public String StateFilter { get; set; }
    }
}
