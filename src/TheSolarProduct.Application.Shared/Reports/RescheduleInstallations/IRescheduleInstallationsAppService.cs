﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Reports.RescheduleInstallations.Dtos;

namespace TheSolarProduct.Reports.RescheduleInstallations
{
    public interface IRescheduleInstallationsAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllRescheduleInstallationDto>> GetAll(GetRescheduleInstallationInputDto input);

        Task<List<GetRescheduleInstallationHistoryDto>> GetRescheduleInstallationHistory(int jobId);

    }
}
