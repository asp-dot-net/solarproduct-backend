﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.InstallerInvoicePaymentDetail.Dtos;

namespace TheSolarProduct.Reports.InstallerInvoicePaymentDetail
{
    public interface IInstallerInvoicePaymentDetailAppService : IApplicationService
    {
        Task<PagedResultDto<InstallerInvoicePaymentDetailDto>> GetAll(InstallerInvoicePaymentDetailInputDto input);

        Task<FileDto> GetInstallerInvoicePaymentExport(InstallerInvoicePaymentDetailInputDto input);

        Task<PagedResultDto<GetAllInstallerPaymentInstallerWiseDto>> GetAllInstallerPaymentInstallerWise(GetInstallerPaymentInstallerWiseInputDto input);

        Task<FileDto> GetInstallerPaymentInstallerWiseExport(GetInstallerPaymentInstallerWiseInputDto input);

    }
}
