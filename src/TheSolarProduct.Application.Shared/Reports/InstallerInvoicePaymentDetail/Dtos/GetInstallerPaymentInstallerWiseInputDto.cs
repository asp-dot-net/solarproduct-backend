﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.InstallerInvoicePaymentDetail.Dtos
{
    public class GetInstallerPaymentInstallerWiseInputDto : PagedAndSortedResultRequestDto
    {

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string InvoiceType { get; set; }

        public int InstallerId { get; set;}
        public string State { get; set; }
    }
}
