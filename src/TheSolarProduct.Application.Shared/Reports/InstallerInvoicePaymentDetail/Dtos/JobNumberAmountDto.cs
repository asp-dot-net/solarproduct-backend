﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.InstallerInvoicePaymentDetail.Dtos
{
    public class JobNumberAmountDto
    {
        public string JobNumber { get; set; }
        public decimal? Amount { get; set; }
    }
}
