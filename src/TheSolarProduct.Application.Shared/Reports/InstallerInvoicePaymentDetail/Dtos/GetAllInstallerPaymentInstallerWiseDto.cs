﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.InstallerInvoicePaymentDetail.Dtos
{
    public class GetAllInstallerPaymentInstallerWiseDto
    {
        public string Installer { get; set; }
        public int? installerId { get; set; }    

        public decimal? MetroPrice { get; set; }

        public decimal? MetroKW { get; set; }

        public decimal? MetroPricePerKW { get; set; }

        public decimal? RegionalPrice { get; set; }

        public decimal? RegionalKW { get; set; }

        public decimal? RegionalPricePerKW { get; set; }

        public InstallerPaymentInstallerWiseSummary Summary { get; set; }
    }
    public class GetAllInstallerPaymentInstallerJobWiseDto
    {
        public string Installer { get; set; }
        public int? installerId { get; set; }
        public string JobNumber { get; set; }
        public decimal? JobPrice { get; set; }
        public decimal? priceKw { get; set; }
        public decimal? priceperkw { get; set; }


    }

    public class InstallerPaymentInstallerWiseSummary
    {
        public decimal? MetroPrice { get; set; }

        public decimal? MetroKW { get; set; }

        public decimal? MetroPricePerKW { get; set; }

        public decimal? RegionalPrice { get; set; }

        public decimal? RegionalKW { get; set; }

        public decimal? RegionalPricePerKW { get; set; }
    }
    public class InstallerinvoiceInstallerWiseSummary
    {
        public int? installerId { get; set; }
        public string JobNumber { get; set; }

        public string Installer { get; set; }

        public decimal? KW { get; set; }

        public decimal? InvoiceAmt { get; set; }
    }
}
