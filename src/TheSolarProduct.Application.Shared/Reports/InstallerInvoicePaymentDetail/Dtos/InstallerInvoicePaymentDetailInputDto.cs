﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.InstallerInvoicePaymentDetail.Dtos
{
    public class InstallerInvoicePaymentDetailInputDto : PagedAndSortedResultRequestDto
    {
        public string filter { get; set; }
        
        public int? orgId { get; set; }

       public int? InstallerId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Areafilter { get; set; }

        public string InvoiceType { get; set; }
       
    }
}
