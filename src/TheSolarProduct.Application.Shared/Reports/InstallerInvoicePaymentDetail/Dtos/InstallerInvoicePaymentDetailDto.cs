﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.InstallerInvoicePaymentDetail.Dtos
{
    public class InstallerInvoicePaymentDetailDto
    {
        public string Price { get; set; }

        public decimal? NSW { get; set; }

        public decimal? QLD { get; set; }

        public decimal? VIC { get; set; }

        public decimal? SA { get; set; }

        public InstallerInvoicePaymentDetailSummary detailSummary { get; set; }

    }

    public class InstallerInvoicePaymentDetailSummary
    {
        public decimal? NSW { get; set; }

        public decimal? QLD { get; set; }

        public decimal? VIC { get; set; }

        public decimal? SA { get; set; }

        public decimal? total { get; set; }
    }
}
