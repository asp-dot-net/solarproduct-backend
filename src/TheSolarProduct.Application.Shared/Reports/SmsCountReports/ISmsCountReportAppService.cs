﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.SmsCountReports.Dtos;

namespace TheSolarProduct.Reports.SmsCountReports
{
    public interface ISmsCountReportAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllSmsCountDto>> GetAll(GetAllSmsCountInputDto input);

        Task<FileDto> GetSmsCountToExcel(GetAllExcelSmsCountInputDto input);

        Task<List<GetAllSmsCountSectionWiseDto>> GetAllSectionWise(GetAllExcelSmsCountInputDto input);

    }
}
