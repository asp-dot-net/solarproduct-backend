﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.SmsCountReports.Dtos
{
    public class GetAllExcelSmsCountInputDto
    {
        public int? OrganizationUnitId { get; set; }

        public int? UserId { get; set; }

        public string DateType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string ExcelOrCSV { get; set; }

    }
}
