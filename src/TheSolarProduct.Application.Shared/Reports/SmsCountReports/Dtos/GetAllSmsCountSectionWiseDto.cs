﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.SmsCountReports.Dtos
{
    public class GetAllSmsCountSectionWiseDto : EntityDto
    {
        public string Section { get; set; }

        public int? SmsCount { get; set; }

        public int? CreditCount { get; set; }

        public decimal? Amount { get; set; }

    }
}
