﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.SmsCountReports.Dtos
{
    public class GetAllSmsCountInputDto : PagedAndSortedResultRequestDto
    {
        public int? OrganizationUnitId { get; set; }

        public int? UserId { get; set; }

        public string DateType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }

}
