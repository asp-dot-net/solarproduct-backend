﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.SmsCountReports.Dtos
{
    public class GetAllSmsCountDto : EntityDto
    {
        public string userName { get; set; }

        public int? SmsCount { get; set; }

        public int? CreditCount { get; set; }

        public decimal? Amount { get; set; }

        public SummarySmsCount SummarySmsCount { get; set; }
    }

    public class SummarySmsCount
    {
        public int? TotalSmsCount { get; set; }

        public int? TotalCreditCount { get; set; }

        public decimal? TotalAmount { get; set; }
    }
}
