﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.LeadSolds.Dtos;
using TheSolarProduct.Reports.UserDetail.Dto;

namespace TheSolarProduct.Reports.UserDetail
{
    public interface IUserDetailExcelExporter
    {
        FileDto UserDetailExport(List<GetAllUserDetailDto> detail, string fileName);


    }
}
