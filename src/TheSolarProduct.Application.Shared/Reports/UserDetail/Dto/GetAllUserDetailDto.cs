﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.UserDetail.Dto
{
    public class GetAllUserDetailDto : EntityDto
    {
        public string UserName { get; set; }

        public string Roles { get; set; }

        public string Organization { get; set; }

        public string Email { get; set; }   

        public string Mobile { get; set; }

        public string Extention { get; set; }

        public string Teams { get; set; }

        public bool IsActive { get; set; }
    }
}
