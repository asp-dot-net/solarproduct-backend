﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.UserDetail.Dto
{
    public class GetUserDetailExportInput
    {

        public int OrganizationUnit { get; set; }

        public string FilterName { get; set; }

        public string Filter { get; set; }

        public string fileName { get; set; }
    }
}
