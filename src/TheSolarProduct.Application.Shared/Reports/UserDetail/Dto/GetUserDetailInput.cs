﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.UserDetail.Dto
{
    public class GetUserDetailInput : PagedAndSortedResultRequestDto
    {
        public int OrganizationUnit { get; set; }

        public string FilterName { get; set;}

        public string Filter { get; set;}

    }
}
