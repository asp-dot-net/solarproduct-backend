﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.LeadSolds.Dtos;
using TheSolarProduct.Reports.UserDetail.Dto;

namespace TheSolarProduct.Reports.UserDetail
{
    public interface IUserDetailReportAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllUserDetailDto>> GetAll(GetUserDetailInput input);

        Task<FileDto> GetUserDetailToExcel(GetUserDetailExportInput input);
    }
}
