﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.Variations.Dtos;

namespace TheSolarProduct.Reports.Variations
{
    public interface IJobVariationReportExcelExport
    {
        FileDto ExportToFile(List<GetAllVariationDto> Listdata);
    }
}
