﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.Variations.Dtos
{
    public class GetAllVariationDto : EntityDto
    {
        public string JobNumber { get; set; }

        public string Variation { get; set; }

        public decimal? Amount { get; set; }

        public string Note { get; set; }

        public string CreatedBy { get; set; }

        public string Created { get; set; }
        public DateTime? InstallationDate { get; set; }

        public List<summaryVariationDto> summaryVariations { get; set; }

        public string State { get; set; }
        public string Staus { get; set; }

    }

    public class summaryVariationDto
    {
        public string State { get; set; }

        public decimal? Amount { get; set; }
    }
}
