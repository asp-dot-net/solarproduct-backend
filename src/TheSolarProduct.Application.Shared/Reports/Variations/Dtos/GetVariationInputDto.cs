﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.Variations.Dtos
{
    public class GetVariationInputDto : PagedAndSortedResultRequestDto
    {
        public string JobNumber { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set;}

        public List<int?> VariationIds { get; set; }

        public int? OId { get; set; }

        public List<string> State { get; set; }

    }
}
