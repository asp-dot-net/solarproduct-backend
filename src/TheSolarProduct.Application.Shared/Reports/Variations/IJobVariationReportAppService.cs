﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.Variations.Dtos;

namespace TheSolarProduct.Reports.Variations
{
    public interface IJobVariationReportAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllVariationDto>> GetAll(GetVariationInputDto input);

        Task<FileDto> GetAllForExport(GetVariationExportInputDto input);

        Task<List<GetAllVariationDto>> GetVariationTypeAmountByState(GetVariationInputDto input);
    }
}
