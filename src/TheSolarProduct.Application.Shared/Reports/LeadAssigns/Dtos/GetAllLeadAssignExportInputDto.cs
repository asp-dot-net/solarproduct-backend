﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.LeadAssigns.Dtos
{
    public class GetAllLeadAssignExportInputDto
    {
        public int OrganizationUnitId { get; set; }

        public int UserId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int excelorcsv { get; set; }

    }
}
