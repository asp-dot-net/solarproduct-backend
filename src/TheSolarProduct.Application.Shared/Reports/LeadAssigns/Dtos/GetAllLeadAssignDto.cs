﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos;

namespace TheSolarProduct.Reports.LeadAssigns.Dtos
{
    public class GetAllLeadAssignDto : EntityDto
    {
        public string UserName { get; set; }

        public int TV { get; set; }

        public int Google { get; set; }

        public int Facebook { get; set; }
        
        public int Refferal { get; set; }

        public int Others { get; set; }

        public int TVSold { get; set; }

        public int GoogleSold { get; set; }

        public int FacebookSold { get; set; }

        public int RefferalSold { get; set; }

        public int OthersSold { get; set; }

        public double TVRatio { get; set; }

        public double GoogleRatio { get; set; }

        public double FacebookRatio { get; set; }

        public double RefferalRatio { get; set; }

        public double OthersRatio { get; set; }
        public double Total {  get; set; }

        public LeadAssignSummaryCount Summary { get; set; }

    }

    public class LeadAssignSummaryCount
    {
        public int TotalTV { get; set; }

        public int TotalGoogle { get; set; }

        public int TotalFacebook { get; set; }

        public int TotalOthers { get; set; }

        public int TotalRefferal { get; set; }

    }
}
