﻿using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Reports.LeadAssigns.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Reports.LeadAssigns
{
    public interface ILeadAssignReportAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllLeadAssignDto>> GetAll(GetAllLeadAssignInputDto input);

        Task<FileDto> GetLeadAssignToExcel(GetAllLeadAssignExportInputDto input);


    }
}
