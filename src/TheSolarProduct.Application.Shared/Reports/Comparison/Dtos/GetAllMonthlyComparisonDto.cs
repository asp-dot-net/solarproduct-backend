﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.Comparison.Dtos
{
    public class GetAllMonthlyComparisonDto
    {
        public int Year { get; set; }

        public int Jan_Lead { get; set; }

        public decimal? Jan_PanelSold_KW { get; set; }

        public decimal? Jan_Installed_KW { get; set; }

        public int Fab_Lead { get; set; }

        public decimal? Fab_PanelSold_KW { get; set; }

        public decimal? Fab_Installed_KW { get; set; }

        public int Mar_Lead { get; set; }

        public decimal? Mar_PanelSold_KW { get; set; }

        public decimal? Mar_Installed_KW { get; set; }

        public int Apr_Lead { get; set; }

        public decimal? Apr_PanelSold_KW { get; set; }

        public decimal? Apr_Installed_KW { get; set; }

        public int May_Lead { get; set; }

        public decimal? May_PanelSold_KW { get; set; }

        public decimal? May_Installed_KW { get; set; }

        public int Jun_Lead { get; set; }

        public decimal? Jun_PanelSold_KW { get; set; }

        public decimal? Jun_Installed_KW { get; set; }

        public int Jul_Lead { get; set; }

        public decimal? Jul_PanelSold_KW { get; set; }

        public decimal? Jul_Installed_KW { get; set; }

        public int Aug_Lead { get; set; }

        public decimal? Aug_PanelSold_KW { get; set; }

        public decimal? Aug_Installed_KW { get; set; }

        public int Sep_Lead { get; set; }

        public decimal? Sep_PanelSold_KW { get; set; }

        public decimal? Sep_Installed_KW { get; set; }

        public int Oct_Lead { get; set; }

        public decimal? Oct_PanelSold_KW { get; set; }

        public decimal? Oct_Installed_KW { get; set; }

        public int Nov_Lead { get; set; }

        public decimal? Nov_PanelSold_KW { get; set; }

        public decimal? Nov_Installed_KW { get; set; }

        public int Dec_Lead { get; set; }

        public decimal? Dec_PanelSold_KW { get; set; }

        public decimal? Dec_Installed_KW { get; set; }

        public decimal? Jan_Expence { get; set; }

        public decimal? Fab_Expence { get; set; }

        public decimal? Mar_Expence { get; set; }

        public decimal? Apr_Expence { get; set; }

        public decimal? May_Expence { get; set; }

        public decimal? Jun_Expence { get; set; }

        public decimal? Jul_Expence { get; set; }

        public decimal? Aug_Expence { get; set; }

        public decimal? Sep_Expence { get; set; }

        public decimal? Oct_Expence { get; set; }

        public decimal? Nov_Expence { get; set; }

        public decimal? Dec_Expence { get; set; }

        public decimal? Jan_BatterySold_KW { get; set; }

        public decimal? Fab_BatterySold_KW { get; set; }

        public decimal? Mar_BatterySold_KW { get; set; }

        public decimal? Apr_BatterySold_KW { get; set; }

        public decimal? May_BatterySold_KW { get; set; }

        public decimal? Jun_BatterySold_KW { get; set; }

        public decimal? Jul_BatterySold_KW { get; set; }

        public decimal? Aug_BatterySold_KW { get; set; }

        public decimal? Sep_BatterySold_KW { get; set; }

        public decimal? Oct_BatterySold_KW { get; set; }

        public decimal? Nov_BatterySold_KW { get; set; }

        public decimal? Dec_BatterySold_KW { get; set; }

    }
}
