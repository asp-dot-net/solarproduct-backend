﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.Comparison.Dtos
{
    public class GetAllComparisonDto : EntityDto
    {
        public int TotalLead { get; set; }

        public int TotalLeadInstalled { get; set; }

        public decimal? Spend { get; set; }

        public decimal? PanelSold_KW { get; set; }

        public decimal? Installed_KW { get; set; }

        public int LastTotalLead { get; set; }

        public int LastTotalLeadInstalled { get; set; }

        public decimal? LastSpend { get; set; }

        public decimal? LastPanelSold_KW { get; set; }

        public decimal? LastInstalled_KW { get; set; }

        public int LastTotalLead2 { get; set; }

        public int LastTotalLeadInstalled2 { get; set; }

        public decimal? LastSpend2 { get; set; }

        public decimal? LastPanelSold2_KW { get; set; }

        public decimal? LastInstalled2_KW { get; set; }

        public decimal? PanelSold_NoOfPanel { get; set; }

        public decimal? Installed_NoOfPanel { get; set; }

        public decimal? LastPanelSold_NoOfPanel { get; set; }

        public decimal? LastInstalled_NoOfPanel { get; set; }

        public decimal? LastPanelSold2_NoOfPanel { get; set; }

        public decimal? LastInstalled2_NoOfPanel { get; set; }

        public string Date { get; set; }

        public string LastDate { get; set; }

        public string LastDate2 { get; set; }
         public int? LeadSourceId {get; set;}
        public string LeadSource { get; set; }

        public decimal? BatterySold_KW { get; set; }

        public decimal? LastBatterySold_KW { get; set; }

        public decimal? LastBatterySold_KW2 { get; set; }


    }

}
