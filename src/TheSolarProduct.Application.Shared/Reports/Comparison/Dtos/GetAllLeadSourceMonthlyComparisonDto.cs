﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.Comparison.Dtos
{
    public class GetAllLeadSourceMonthlyComparisonDto : EntityDto
    {
        public string Leadsource { get; set; }

        public List<GetMonthsCompareValue> compareValues { get; set; }
    }

    public class GetMonthsCompareValue
    {
        public int Month { get; set; }
        public decimal? Spend { get; set; }

        public int TotalLead { get; set; }

        public decimal? PanelSold_KW { get; set; }

        public decimal? Installed_KW { get; set; }

        public decimal? BatterySold_KW { get; set; }
    }


    public class GetLeadSourceMonthlyComparisonInputDto : PagedAndSortedResultRequestDto
    {
        public int Year { get; set; }

        public List<int> Months { get; set; }

        public int? OrgId { get; set; }

        public List<int> Leadsources { get; set; }

        public List<string> States { get; set; }

        public List<string> AreaFilter { get; set; }
    }
}
