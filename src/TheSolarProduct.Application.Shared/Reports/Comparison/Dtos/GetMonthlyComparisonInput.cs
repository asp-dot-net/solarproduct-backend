﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.Comparison.Dtos
{
    public class GetMonthlyComparisonInput : PagedAndSortedResultRequestDto
    {
        public int? OrgId { get; set; }

        public List<int> Leadsources { get; set; }

        public List<string> States { get; set; }

        public List<string> AreaFilter { get; set; }
    }
}
