﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Reports.Comparison.Dtos;

namespace TheSolarProduct.Reports.Comparison
{
    public interface IComparisonAppService : IApplicationService
    {
        Task<GetAllComparisonDto> GetAll(GetComparisonInput input);

        Task<PagedResultDto<GetAllMonthlyComparisonDto>> GetAllMonthly(GetMonthlyComparisonInput input);

        Task<PagedResultDto<GetAllComparisonDto>> GetAllLeadSource(GetLeadComparisonInput input);

        Task<PagedResultDto<GetAllLeadSourceMonthlyComparisonDto>> GetAllLeadSourceMonthly(GetLeadSourceMonthlyComparisonInputDto input);
    }
}
