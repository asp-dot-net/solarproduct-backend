﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Currencies.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Wholesaleinvoicetype.Dtos;

namespace TheSolarProduct.Wholesaleinvoicetype
{
    public interface IWholesaleinvoicetypeAppService : IApplicationService
    {
        Task<PagedResultDto<GetWholesaleinvoicetypeForViewDto>> GetAll(GetAllWholesaleinvoicetypeInput input);

        Task CreateOrEdit(CreateOrEditWholesaleinvoicetypesDto input);

        Task<GetWholesaleinvoicetypeForEditOutput> GetWholesaleinvoicetypeForEdit(EntityDto input);

        Task Delete(EntityDto input);
        Task<FileDto> GetInvoiceToExcel(GetAllWholesaleinvoicetypeForExcelInput input);
    }
}
