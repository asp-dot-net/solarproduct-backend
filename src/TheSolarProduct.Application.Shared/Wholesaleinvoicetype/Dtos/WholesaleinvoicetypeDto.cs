﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.WholesaleDeliveryOption.Dto;

namespace TheSolarProduct.Wholesaleinvoicetype.Dtos
{
    public class WholesaleinvoicetypeDto : EntityDto
    {
        public string Name { get; set; }

        public bool? IsActive { get; set; }

       
    }
}
