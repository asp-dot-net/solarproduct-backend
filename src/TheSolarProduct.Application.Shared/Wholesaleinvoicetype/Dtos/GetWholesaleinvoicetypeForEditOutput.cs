﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Currencies.Dtos;

namespace TheSolarProduct.Wholesaleinvoicetype.Dtos
{
    public class GetWholesaleinvoicetypeForEditOutput
    {
        public CreateOrEditWholesaleinvoicetypesDto Invoice { get; set; }
    }
}
