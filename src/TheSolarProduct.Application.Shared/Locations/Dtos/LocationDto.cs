﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Locations.Dtos
{
    public class LocationDto : EntityDto
    {
        public string location { get; set; }

        public string Address { get; set; }

        public string state { get; set; }
        public virtual string UnitNo { get; set; }

        public virtual string UnitType { get; set; }

        public virtual string StreetNo { get; set; }

        public virtual string StreetName { get; set; }

        public virtual string StreetType { get; set; }

        public virtual string PostCode { get; set; }

        public virtual string Latitude { get; set; }

        public virtual string Longitude { get; set; }
        public virtual string Suburb { get; set; }
    }
}
