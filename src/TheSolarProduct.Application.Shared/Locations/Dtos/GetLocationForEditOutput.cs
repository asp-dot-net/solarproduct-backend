﻿namespace TheSolarProduct.Locations.Dtos
{
    public class GetLocationForEditOutput
    {
        public CreateOrEditLocationDto Location { get; set; }
    }
}
