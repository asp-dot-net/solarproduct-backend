﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Locations.Dtos
{
    public class GetLocationForViewDto
    {
        public LocationDto Location { get; set; }
    }
}
