﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Locations.Dtos
{
    public class GetAllLocationForInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
