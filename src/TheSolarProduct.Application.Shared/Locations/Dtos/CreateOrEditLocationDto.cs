﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Locations.Dtos
{
    public class CreateOrEditLocationDto : EntityDto<int?>
    {
        public string location { get; set; }

        public string state { get; set; }

        public string Address { get; set; }

        public string UnitNo { get; set; }

        public string UnitType { get; set; }

        public string StreetNo { get; set; }

        public string StreetName { get; set; }

        public string StreetType { get; set; }

        public string PostCode { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }
        public virtual string Suburb { get; set; }
    }
}
