﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using TheSolarProduct.Locations.Dtos;

namespace TheSolarProduct.Locations
{
    public interface ILocationAppService : IApplicationService
    {
        Task<PagedResultDto<GetLocationForViewDto>> GetAll(GetAllLocationForInput input);

        //Task<GetPostalTypeForViewDto> GetPostalTypeForView(int id);

        Task<GetLocationForEditOutput> GetLocationForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditLocationDto input);

        //Task Delete(EntityDto input);
    }
}
