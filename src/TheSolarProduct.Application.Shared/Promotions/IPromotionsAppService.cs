﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Promotions.Dtos;
using TheSolarProduct.Dto;
using System.Collections.Generic;
using TheSolarProduct.WholeSalePromotions.Dtos;

namespace TheSolarProduct.Promotions
{
    public interface IPromotionsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetPromotionForViewDto>> GetAll(GetAllPromotionsInput input);

        Task<GetPromotionForViewDto> GetPromotionForView(int id);

		Task<GetPromotionForEditOutput> GetPromotionForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditPromotionDto input);

		Task Delete(int promoId);

		Task<FileDto> GetPromotionsToExcel(GetAllPromotionsForExcelInput input);
				
		Task<List<PromotionPromotionTypeLookupTableDto>> GetAllPromotionTypeForTableDropdown();

		Task SubscribeUnsubscribePromo(string STR, int PromotionResponseStatusId);

		Task<FileDto> CreateWithExcel(CreateOrEditPromotionDto input);

		Task<GetPromotionHistorybyPromotionIdOutPut> GetPromotionHistorybyPromotionId(int promotionId);

		Task<string> GetOrganizationLogo(string STR);

    }
}