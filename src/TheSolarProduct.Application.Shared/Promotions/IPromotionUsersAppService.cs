﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Promotions.Dtos;
using TheSolarProduct.Dto;
using System.Collections.Generic;

namespace TheSolarProduct.Promotions
{
    public interface IPromotionUsersAppService : IApplicationService 
    {
        Task<PagedResultDto<GetPromotionUserForViewDto>> GetAll(GetAllPromotionUsersInput input);
		Task<promotioncount> GetAllCount(GetAllPromotionUsersInput input);

		Task<GetPromotionUserForViewDto> GetPromotionUserForView(int id);

		Task<GetPromotionUserForEditOutput> GetPromotionUserForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditPromotionUserDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetPromotionUsersToExcel(GetAllPromotionUsersForExcelInput input);

		
		Task<List<PromotionUserPromotionLookupTableDto>> GetAllPromotionForTableDropdown();
		
		Task<List<PromotionUserLeadLookupTableDto>> GetAllLeadForTableDropdown();
		
		Task<List<PromotionUserPromotionResponseStatusLookupTableDto>> GetAllPromotionResponseStatusForTableDropdown();

		Task UpdatePromotionResponseStatus(int PromotionUserId, int value);
		Task MarkAsReadPromotionSms(int id);
		Task UpdateResponce(int? LeaId, string msg);
		Task MarkAsReadPromotionSmsInBulk(List<int> ids, int Readorunreadtag);

	}
}