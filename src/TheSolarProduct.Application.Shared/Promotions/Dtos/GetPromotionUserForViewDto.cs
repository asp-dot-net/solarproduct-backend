﻿using System;

namespace TheSolarProduct.Promotions.Dtos
{
    public class GetPromotionUserForViewDto
    {
        public PromotionUserDto PromotionUser { get; set; }
        public GetPromotionForViewDto getPromotionForViewDto { get; set; }
        public string PromotionTitle { get; set; }

        public string LeadCopanyName { get; set; }

        public string PromotionResponseStatusName { get; set; }
        public string Mobile { get; set; }
        public string EMail { get; set; }
        public long? AssignedTo { get; set; }
        public string AssignedToName { get; set; }
        public string LeadStatus { get; set; }

        public string ProjectStatus { get; set; }
        public string ProjectNumber { get; set; }
        public bool? Mark { get; set; }
        public string LastComment { get; set; }

        public DateTime? LastCommentDate { get; set; }

        public string CurrentLeadOwner { get; set; }

        public DateTime? CreationTime { get; set; }

        public DateTime? CustEntered { get; set; }

        public string SalesTeam { get; set; }

        public string Interested { get; set; }

        public int? LeadId { get; set; }
        public DateTime? ActivityReminderTime { get; set; }
        public string ActivityDescription { get; set; }
        public string ActivityComment { get; set; }
        public int? TotalPromotion { get; set; }

        public int? NoReplay { get; set; }
        public int? InterestedCount { get; set; }
        public int? NotInterestedCount { get; set; }
        public int? OtherCount { get; set; }
        public int? Unhandle { get; set; }

        public int? PromotionProject { get; set; }
        public int? PromotionSales { get; set; }
        public int? PromotionSold { get; set; }
        public bool? isSelected { get; set; }

        public string PromotionTypeName { get; set; }

        public int? JobId { get; set; }


    }
    public class promotioncount
    {
        public int? TotalPromotion { get; set; }

        public int? NoReplay { get; set; }
        public int? InterestedCount { get; set; }
        public int? NotInterestedCount { get; set; }
        public int? OtherCount { get; set; }
        public int? Unhandle { get; set; }

        public int? PromotionProject { get; set; }
        public int? PromotionSales { get; set; }
        public int? PromotionSold { get; set; }
    }
}