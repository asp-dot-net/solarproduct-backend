﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Promotions.Dtos
{
    public class PromotionPromotionTypeLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}