﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Promotions.Dtos
{
    public class PromotionUserPromotionResponseStatusLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}