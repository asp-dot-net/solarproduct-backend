﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Promotions.Dtos
{
    public class GetPromotionTypeForEditOutput
    {
		public CreateOrEditPromotionTypeDto PromotionType { get; set; }


    }
}