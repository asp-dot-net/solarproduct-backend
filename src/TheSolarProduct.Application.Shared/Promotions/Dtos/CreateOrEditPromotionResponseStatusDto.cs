﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Promotions.Dtos
{
    public class CreateOrEditPromotionResponseStatusDto : EntityDto<int?>
    {

		[Required]
		[StringLength(PromotionResponseStatusConsts.MaxNameLength, MinimumLength = PromotionResponseStatusConsts.MinNameLength)]
		public string Name { get; set; }
		
		

    }
}