﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Promotions.Dtos
{
    public class CreateOrEditPromotionUserDto : EntityDto<int?>
    {

		public DateTime ResponseDate { get; set; }
		
		public string ResponseMessage { get; set; }
		
		public int? PromotionId { get; set; }
		 
		public int? LeadId { get; set; }
		 
		public int? PromotionResponseStatusId { get; set; }
		 
		 
    }
}