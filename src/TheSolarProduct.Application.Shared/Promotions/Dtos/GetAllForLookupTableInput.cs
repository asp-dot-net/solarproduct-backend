﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Promotions.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}