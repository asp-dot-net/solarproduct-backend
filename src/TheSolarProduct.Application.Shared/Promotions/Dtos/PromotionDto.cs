﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.Promotions.Dtos
{
    public class PromotionDto : EntityDto
    {
		public string Title { get; set; }
        public string OrganizationName { get; set; }
        public decimal PromoCharge { get; set; }

		public string Description { get; set; }
        public DateTime CreationTime { get; set; }

        public int? PromotionTypeId { get; set; }

        public int? TotalLeads { get; set; }

        public  int OrganizationID { get; set; }
        public string ResponseMessage { get; set; }
    }
}