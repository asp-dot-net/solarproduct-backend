﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.Promotions.Dtos
{
    public class GetAllPromotionResponseStatusesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }



    }
}