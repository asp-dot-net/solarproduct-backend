﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Promotions.Dtos
{
    public class PromotionUserPromotionLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}