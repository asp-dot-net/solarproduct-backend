﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Promotions.Dtos
{
    public class GetPromotionUserForEditOutput
    {
		public CreateOrEditPromotionUserDto PromotionUser { get; set; }

		public string PromotionTitle { get; set;}

		public string LeadCopanyName { get; set;}

		public string PromotionResponseStatusName { get; set;}

		public string PromotionResponseStatusName2 { get; set;}


    }
}