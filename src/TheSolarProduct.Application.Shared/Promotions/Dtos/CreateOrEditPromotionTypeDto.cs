﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Promotions.Dtos
{
    public class CreateOrEditPromotionTypeDto : EntityDto<int?>
    {

		[Required]
		[StringLength(PromotionTypeConsts.MaxNameLength, MinimumLength = PromotionTypeConsts.MinNameLength)]
		public string Name { get; set; }
		
		

    }
}