﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Promotions.Dtos
{
	public class PromotionSMSEmailList
	{
		public string SMS { get; set; }

		public string Email { get; set; }
	}
}
