﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Promotions.Dtos
{
    public class GetPromotionForEditOutput
    {
		public CreateOrEditPromotionDto Promotion { get; set; }

		public string PromotionTypeName { get; set;}


    }
}