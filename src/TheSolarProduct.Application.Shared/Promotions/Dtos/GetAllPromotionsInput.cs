﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.Promotions.Dtos
{
    public class GetAllPromotionsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string TitleFilter { get; set; }

		public string PromoChargeFilter { get; set; }

		public string DescriptionFilter { get; set; }

        public decimal? MaxAmountFilter { get; set; }
        public decimal? MinAmountFilter { get; set; }
        public DateTime? FromDateFilter { get; set; }
        public DateTime? ToDateFilter { get; set; }

        public string PromotionTypeNameFilter { get; set; }

        public int? OrganizationUnit { get; set; }
        public int? PromotionTitleFilter { get; set; }
        public int? PromoType { get; set; }
    }
}