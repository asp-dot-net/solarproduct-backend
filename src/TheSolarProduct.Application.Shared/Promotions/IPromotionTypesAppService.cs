﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Promotions.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Promotions
{
    public interface IPromotionTypesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetPromotionTypeForViewDto>> GetAll(GetAllPromotionTypesInput input);

		Task<GetPromotionTypeForEditOutput> GetPromotionTypeForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditPromotionTypeDto input);

		Task Delete(EntityDto input);

		
    }
}