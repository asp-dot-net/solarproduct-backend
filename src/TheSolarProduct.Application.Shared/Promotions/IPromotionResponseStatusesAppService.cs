﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Promotions.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Promotions
{
    public interface IPromotionResponseStatusesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetPromotionResponseStatusForViewDto>> GetAll(GetAllPromotionResponseStatusesInput input);

        Task<GetPromotionResponseStatusForViewDto> GetPromotionResponseStatusForView(int id);

		Task<GetPromotionResponseStatusForEditOutput> GetPromotionResponseStatusForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditPromotionResponseStatusDto input);

		Task Delete(EntityDto input);

		
    }
}