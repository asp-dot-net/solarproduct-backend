﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockOrderFors.Dtos
{
    public class GetAllStockOerderForsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
