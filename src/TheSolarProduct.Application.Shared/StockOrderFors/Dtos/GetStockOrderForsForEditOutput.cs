﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.StockFroms.Dtos;

namespace TheSolarProduct.StockOrderFors.Dtos
{
    public class GetStockOrderForsForEditOutput
    {
        public CreateOrEditStockOrderForsDto stockorderfors { get; set; }
    }
}
