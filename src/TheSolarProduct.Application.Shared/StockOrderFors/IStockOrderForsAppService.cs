﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.StockFroms.Dtos;
using TheSolarProduct.StockOrderFors.Dtos;

namespace TheSolarProduct.StockOrderFors
{
    public interface IStockOrderForsAppService : IApplicationService
    {
        Task<PagedResultDto<GetStockOrderForsForViewDto>> GetAll(GetAllStockOerderForsInput nput);

        Task CreateOrEdit(CreateOrEditStockOrderForsDto input);

        Task<GetStockOrderForsForEditOutput> GetStockOrderForsForEdit(EntityDto input);

        Task<FileDto> GetStockOrderForsToExcel(GetAllStockOrderForsForExcelInput nput);

        Task Delete(EntityDto input);
    }
}
