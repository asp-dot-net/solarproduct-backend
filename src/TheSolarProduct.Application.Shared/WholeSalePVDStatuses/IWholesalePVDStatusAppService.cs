﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.WholeSalePVDStatuses.Dtos;

namespace TheSolarProduct.WholeSalePVDStatuses
{
    public interface IWholesalePVDStatusAppService : IApplicationService
    {
        Task<PagedResultDto<GetWholesalePVDStatusForViewDto>> GetAll(GetAllWholesalePVDStatusInput input);

        Task CreateOrEdit(CreateOrEditWholesalePVDStatusDto input);

        Task<GetWholesalePVDStatusForEditOutput> GetWholesalePVDStatusForEdit(EntityDto input);

        Task Delete(EntityDto input);
    }
}
