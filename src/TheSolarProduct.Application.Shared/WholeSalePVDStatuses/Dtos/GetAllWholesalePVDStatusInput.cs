﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSalePVDStatuses.Dtos
{
    public class GetAllWholesalePVDStatusInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
