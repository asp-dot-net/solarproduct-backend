﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.WholeSalePVDStatuses.Dtos;

namespace TheSolarProduct.WholeSalePVDStatuses.Dtos
{
    public class GetWholesalePVDStatusForEditOutput
    {
        public CreateOrEditWholesalePVDStatusDto PVDStatus { get; set; }
    }
}
