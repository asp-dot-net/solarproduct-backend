﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.WholeSalePVDStatuses.Dtos;

namespace TheSolarProduct.WholeSalePVDStatuses.Dtos
{
    public class GetWholesalePVDStatusForViewDto
    {
        public WholesalePVDStatusDto PVDStatus { get; set; }
    }
}
