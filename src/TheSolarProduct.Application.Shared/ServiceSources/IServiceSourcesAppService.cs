﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.ServiceSources.Dtos;

namespace TheSolarProduct.ServiceSources
{
    public interface IServiceSourcesAppService : IApplicationService
    {
        Task<PagedResultDto<GetServiceSourcesForViewDto>> GetAll(GetAllServiceSourcesInput input);
        Task<GetServiceSourcesForViewDto> GetServiceSourcesForView(int id);
        Task<GetServiceSourcesForEditOutput> GetServiceSourcesForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditServiceSourcesDto input);
        Task Delete(EntityDto input);
        Task<FileDto> GetServiceSourcesToExcel(GetAllServiceSourcesForExcelInput input);
    }
}
