﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ServiceSources.Dtos
{
    public class GetAllServiceSourcesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string ServiceSourcesFilter { get; set; }
    }
}
