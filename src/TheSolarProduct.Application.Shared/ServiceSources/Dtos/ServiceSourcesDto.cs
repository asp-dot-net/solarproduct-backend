﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ServiceSources.Dtos
{
    public class ServiceSourcesDto : EntityDto
    {
        public string JobServiceSources { get; set; }
        public Boolean IsActive { get; set; }
    }
}
