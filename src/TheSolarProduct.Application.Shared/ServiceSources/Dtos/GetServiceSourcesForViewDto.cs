﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ServiceSources.Dtos
{
    public class GetServiceSourcesForViewDto
    {
        public ServiceSourcesDto JobServiceSources { get; set; }
    }
}
