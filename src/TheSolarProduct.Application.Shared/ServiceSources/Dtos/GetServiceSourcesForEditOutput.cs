﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ServiceSources.Dtos
{
    public class GetServiceSourcesForEditOutput
    {
        public CreateOrEditServiceSourcesDto JobServiceSources { get; set; }
    }
}
