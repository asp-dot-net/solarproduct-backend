﻿using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.WholeSalePromotions.Dtos;

namespace TheSolarProduct.WholeSalePromotions
{
    public interface IWholeSalePromotionTypesAppService : IApplicationService
    {
        Task<PagedResultDto<GetWholeSalePromotionTypeForViewDto>> GetAll(GetAllWholeSalePromotionTypesInput input);

        Task<GetWholeSalePromotionTypeForEditOutput> GetWholeSalePromotionTypeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditWholeSalePromotionTypeDto input);

        Task Delete(EntityDto input);


    }
}
