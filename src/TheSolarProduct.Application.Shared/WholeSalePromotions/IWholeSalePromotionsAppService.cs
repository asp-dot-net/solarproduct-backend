﻿using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.WholeSalePromotions.Dtos;

namespace TheSolarProduct.WholeSalePromotions
{
    public interface IWholeSalePromotionsAppService : IApplicationService
    {
        Task<PagedResultDto<GetWholeSalePromotionForViewDto>> GetAll(GetAllWholeSalePromotionsInput input);

        Task<GetWholeSalePromotionForViewDto> GetWholeSalePromotionForView(int id);

        Task<GetWholeSalePromotionForEditOutput> GetWholeSalePromotionForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditWholeSalePromotionDto input);

        Task Delete(int promoId);

        Task<FileDto> GetWholeSalePromotionsToExcel(GetAllWholeSalePromotionsForExcelInput input);

        Task<List<WholeSalePromotionTypeLookupTableDto>> GetAllWholeSalePromotionTypeForTableDropdown();

        Task SubScribeUnsubscribepromo(string STR, int WholeSalePromotionResponseStatusId);

        Task<FileDto> CreateWithExcel(CreateOrEditWholeSalePromotionDto input);

        Task<GetPromotionHistorybyPromotionIdOutPut> GetPromotionHistorybyPromotionId(int  promotionId);
    }
}
