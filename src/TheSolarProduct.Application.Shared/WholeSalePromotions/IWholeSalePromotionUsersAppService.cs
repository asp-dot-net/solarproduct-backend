﻿using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.WholeSalePromotions.Dtos;

namespace TheSolarProduct.WholeSalePromotions
{
    public interface IWholeSalePromotionUsersAppService : IApplicationService
    {
        Task<PagedResultDto<GetWholeSalePromotionUserForViewDto>> GetAll(GetAllWholeSalePromotionUsersInput input);
        Task<WholeSalePromotioncount> GetAllCount(GetAllWholeSalePromotionUsersInput input);

        Task<GetWholeSalePromotionUserForViewDto> GetWholeSalePromotionUserForView(int id);

        Task<GetWholeSalePromotionUserForEditOutput> GetWholeSalePromotionUserForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditWholeSalePromotionUserDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetWholeSalePromotionUsersToExcel(GetAllWholeSalePromotionUsersForExcelInput input);


        Task<List<WholeSalePromotionUserWholeSalePromotionLookupTableDto>> GetAllWholeSalePromotionForTableDropdown();

        Task<List<WholeSalePromotionUserLeadLookupTableDto>> GetAllWholeSaleLeadForTableDropdown();

        Task<List<WholeSalePromotionUserWholeSalePromotionResponseStatusLookupTableDto>> GetAllWholeSalePromotionResponseStatusForTableDropdown();

        Task UpdateWholeSalePromotionResponseStatus(int WholeSalePromotionUserId, int value);
        Task MarkAsReadWholeSalePromotionSms(int id);
        Task UpdateResponce(int? LeaId, string msg);
        Task MarkAsReadWholeSalePromotionSmsInBulk(List<int> ids, int Readorunreadtag);

    }
}
