﻿using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.WholeSalePromotions.Dtos;

namespace TheSolarProduct.WholeSalePromotions
{
    public interface IWholeSalePromotionResponseStatusesAppService : IApplicationService
    {
        Task<PagedResultDto<GetWholeSalePromotionResponseStatusForViewDto>> GetAll(GetAllWholeSalePromotionResponseStatusesInput input);

        Task<GetWholeSalePromotionResponseStatusForViewDto> GetWholeSalePromotionResponseStatusForView(int id);

        Task<GetWholeSalePromotionResponseStatusForEditOutput> GetWholeSalePromotionResponseStatusForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditWholeSalePromotionResponseStatusDto input);

        Task Delete(EntityDto input);


    }
}
