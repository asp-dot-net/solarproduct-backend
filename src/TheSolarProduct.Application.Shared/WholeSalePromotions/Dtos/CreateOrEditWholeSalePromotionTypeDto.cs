﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TheSolarProduct.Promotions;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class CreateOrEditWholeSalePromotionTypeDto : EntityDto<int?>
    {

        [Required]
        [StringLength(WholeSalePromotionTypeConsts.MaxNameLength, MinimumLength = WholeSalePromotionTypeConsts.MinNameLength)]
        public string Name { get; set; }



    }
}
