﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class GetAllWholeSalePromotionsForExcelInput
    {
        public string Filter { get; set; }

        public string TitleFilter { get; set; }

        public string PromoChargeFilter { get; set; }

        public string DescriptionFilter { get; set; }

        public decimal? MaxAmountFilter { get; set; }
        public decimal? MinAmountFilter { get; set; }
        public DateTime? FromDateFilter { get; set; }
        public DateTime? ToDateFilter { get; set; }

        public string WholeSalePromotionTypeNameFilter { get; set; }

        public int? OrganizationUnit { get; set; }

        public int? WholeSalePromotionTitleFilter { get; set; }

        public int? PromoType { get; set; }
    }
}
