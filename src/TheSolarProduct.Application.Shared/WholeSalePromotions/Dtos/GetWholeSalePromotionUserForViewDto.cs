﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Promotions.Dtos;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class GetWholeSalePromotionUserForViewDto
    {
        public WholeSalePromotionUserDto WholeSalePromotionUser { get; set; }
        public GetWholeSalePromotionForViewDto getWholeSalePromotionForViewDto { get; set; }
        public string WholeSalePromotionTitle { get; set; }

        public string LeadCopanyName { get; set; }

        public string WholeSalePromotionResponseStatusName { get; set; }
        public string Mobile { get; set; }
        public string EMail { get; set; }
        public long? AssignedTo { get; set; }
        public string AssignedToName { get; set; }
        public string LeadStatus { get; set; }

        public string ProjectStatus { get; set; }
        public string ProjectNumber { get; set; }
        public bool? Mark { get; set; }
        public string LastComment { get; set; }

        public DateTime? LastCommentDate { get; set; }

        public string CurrentLeadOwner { get; set; }

        public DateTime? CreationTime { get; set; }

        public DateTime? CustEntered { get; set; }

        public string SalesTeam { get; set; }

        public string Interested { get; set; }

        public int? LeadId { get; set; }
        public DateTime? ActivityReminderTime { get; set; }
        public string ActivityDescription { get; set; }
        public string ActivityComment { get; set; }
        public int? TotalWholeSalePromotion { get; set; }

        public int? NoReplay { get; set; }
        public int? InterestedCount { get; set; }
        public int? NotInterestedCount { get; set; }
        public int? OtherCount { get; set; }
        public int? Unhandle { get; set; }

        public int? WholeSalePromotionProject { get; set; }
        public int? WholeSalePromotionSales { get; set; }
        public int? WholeSalePromotionSold { get; set; }
        public bool? isSelected { get; set; }

        public string WholeSalePromotionTypeName { get; set; }
    }

    public class WholeSalePromotioncount
    {
        public int? TotalWholeSalePromotion { get; set; }

        public int? NoReplay { get; set; }

        public int? InterestedCount { get; set; }

        public int? NotInterestedCount { get; set; }

        public int? OtherCount { get; set; }

        public int? Unhandle { get; set; }

        public int? WholeSalePromotionProject { get; set; }

        public int? WholeSalePromotionSales { get; set; }

        public int? WholeSalePromotionSold { get; set; }
    }
}
