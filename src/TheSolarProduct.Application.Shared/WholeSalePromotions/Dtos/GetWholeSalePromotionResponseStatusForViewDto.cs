﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Promotions.Dtos;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class GetWholeSalePromotionResponseStatusForViewDto
    {
        public WholeSalePromotionResponseStatusDto WholeSalePromotionResponseStatus { get; set; }

    }
}
