﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class CreateOrEditWholeSalePromotionResponseStatusDto : EntityDto<int?>
    {

        [Required]
        [StringLength(WholeSalePromotionResponseStatusConsts.MaxNameLength, MinimumLength = WholeSalePromotionResponseStatusConsts.MinNameLength)]
        public string Name { get; set; }


    }
}
