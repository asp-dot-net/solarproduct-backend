﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class GetWholeSalePromotionTypeForEditOutput
    {
		public CreateOrEditWholeSalePromotionTypeDto PromotionType { get; set; }
    }
}
