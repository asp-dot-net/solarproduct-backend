﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class WholeSalePromotionSMSEmailList
    {
        public string SMS { get; set; }

        public string Email { get; set; }
    }
}
