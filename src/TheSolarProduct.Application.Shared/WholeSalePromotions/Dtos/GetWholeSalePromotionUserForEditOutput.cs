﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Promotions.Dtos;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class GetWholeSalePromotionUserForEditOutput
    {
        public CreateOrEditWholeSalePromotionUserDto WholeSalePromotionUser { get; set; }

        public string WholeSalePromotionTitle { get; set; }

        public string LeadCopanyName { get; set; }

        public string WholeSalePromotionResponseStatusName { get; set; }

        public string WholeSalePromotionResponseStatusName2 { get; set; }


    }
}
