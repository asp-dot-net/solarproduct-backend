﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Promotions.Dtos;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class GetWholeSalePromotionForEditOutput
    {
        public CreateOrEditWholeSalePromotionDto WholeSalePromotion { get; set; }

        public string WholeSalePromotionTypeName { get; set; }

    }
}
