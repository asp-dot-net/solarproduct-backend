﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class GetWholeSalePromotionTypeForViewDto
    {
		public WholeSalePromotionTypeDto WholeSalePromotionType { get; set; }
    }
}
