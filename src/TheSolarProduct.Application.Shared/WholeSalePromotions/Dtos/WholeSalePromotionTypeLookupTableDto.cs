﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class WholeSalePromotionTypeLookupTableDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }
    }
}
