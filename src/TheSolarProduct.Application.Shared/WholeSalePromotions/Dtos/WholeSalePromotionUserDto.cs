﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class WholeSalePromotionUserDto : EntityDto
    {
        public DateTime ResponseDate { get; set; }

        public string ResponseMessage { get; set; }


        public int? PromotionId { get; set; }

        public int? LeadId { get; set; }

        public int? WholeSalePromotionResponseStatusId { get; set; }

    }
}
