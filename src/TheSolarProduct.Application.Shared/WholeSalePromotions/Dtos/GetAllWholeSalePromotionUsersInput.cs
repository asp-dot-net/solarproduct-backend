﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class GetAllWholeSalePromotionUsersInput : PagedAndSortedResultRequestDto
    {
        public string FilterName { get; set; }
        public string Filter { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? OrganizationUnit { get; set; }
        public string ResponseMessageFilter { get; set; }

        public int? WholeSalePromotionTitleFilter { get; set; }

        public string LeadCopanyNameFilter { get; set; }

        public string WholeSalePromotionResponseStatusNameFilter { get; set; }

        public int? WholeSalePromotionResponseSatusIdFilter { get; set; }

        public List<int> JobStatusIDFilter { get; set; }

        public int? TeamId { get; set; }
        public int? SalesRepId { get; set; }
        public string ResponseDateFilter { get; set; }
        public int? PromoType { get; set; }

        public int? ReadUnreadsms { get; set; }

    }
}
