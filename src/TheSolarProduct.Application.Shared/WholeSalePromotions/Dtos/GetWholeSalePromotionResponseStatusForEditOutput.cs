﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Promotions.Dtos;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class GetWholeSalePromotionResponseStatusForEditOutput
    {
        public CreateOrEditWholeSalePromotionResponseStatusDto WholeSalePromotionResponseStatus { get; set; }

    }
}
