﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Promotions.Dtos;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class GetWholeSalePromotionForViewDto
    {
        public WholeSalePromotionDto WholeSalePromotion { get; set; }

        public string WholeSalePromotionTypeName { get; set; }
        public string CreatorUserName { get; set; }
        public int InterestedCount { get; set; }
        public int NotInterestedCount { get; set; }
        public int OtherCount { get; set; }
        public int NoReply { get; set; }
        public int TotalLeads { get; set; }
        public int? TotalWholeSalePromotion { get; set; }
        public int TotalSendWholeSalePromotion { get; set; }
        public int? NoReplay { get; set; }
        public int? summaryInterestedCount { get; set; }
        public int? summaryNotInterestedCount { get; set; }
        public int? summaryOtherCount { get; set; }
        public int? Unhandle { get; set; }

        public int? WholeSalePromotionProject { get; set; }
        public int? WholeSalePromotionSales { get; set; }
        public int? WholeSalePromotionSold { get; set; }
    }
}
