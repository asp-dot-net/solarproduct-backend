﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class WholeSalePromotionDto : EntityDto
    {
        public string Title { get; set; }
        public string OrganizationName { get; set; }
        public decimal PromoCharge { get; set; }

        public string Description { get; set; }
        public DateTime CreationTime { get; set; }

        public int? PromotionTypeId { get; set; }

        public int? TotalLeads { get; set; }

        public int OrganizationID { get; set; }
        public string ResponseMessage { get; set; }
    }
}
