﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class WholeSalePromotionUserWholeSalePromotionResponseStatusLookupTableDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }
    }
}
