﻿using Abp.Application.Services.Dto;
using Abp;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.WholeSalePromotions.Dtos
{
    public class CreateOrEditWholeSalePromotionDto : EntityDto<int?>
    {
        [Required]
        public string Title { get; set; }

        public decimal PromoCharge { get; set; }

        public string Description { get; set; }

        public int? WholeSalePromotionTypeId { get; set; }

        public string SelectedLeadIdsForWholeSalePromotion { get; set; }

        public int LeadCount { get; set; }

        public string MobileNos { get; set; }

        public string Emails { get; set; }

        public int SaveOrTest { get; set; }

        public int OrganizationID { get; set; }


        //Filter Value
        public DateTime? StartDateFilter { get; set; }

        public DateTime? EndDateFilter { get; set; }

        public List<NameValue<int>> LeadStatusIdsFilter { get; set; }

        public List<NameValue<int>> LeadSourceIdsFilter { get; set; }

        public List<NameValue<int>> StateIdsFilter { get; set; }

        public List<NameValue<int>> TeamIdsFilter { get; set; }

        public List<NameValue<int>> JobStatusIdsFilter { get; set; }

        public string AreaNameFilter { get; set; }

        public string TypeNameFilter { get; set; }

        public string EmailFrom { get; set; }

        public int? totalCharCount { get; set; }

        public int? totalCredit { get; set; }

        public string LeadStatuses { get; set; }

        public string States { get; set; }

        public bool IsFooterdAttached { get; set; }
    }
}
