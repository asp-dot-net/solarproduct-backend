﻿namespace TheSolarProduct.EntityDynamicParameters
{
    public class EntityDynamicParameterGetAllInput
    {
        public string EntityFullName { get; set; }
    }
}
