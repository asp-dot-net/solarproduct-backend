﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheSolarProduct.PickList.Dtos;

namespace TheSolarProduct.PickList
{
    public interface IPickListAppService : IApplicationService
    {
        Task SavePickList(CreateOrEditPickListDto input);
        
        Task<List<GetPicklistItemInput>> GetPickList(int JobId);
        
        Task<List<GetPicklistItemForReport>> GetPickListForReport(int Id);

        Task<PicklistDto> GetPickListById(int Id);

        IQueryable<PicklistItemDto> GetPickListItemDetailsByPickId(int PickId);

        Task<bool> CheckInstallationPicklistExists(int JobId);

        Task<bool> CheckSecondPicklistItems(int JobId);
    }
}
