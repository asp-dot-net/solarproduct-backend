﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PickList.Dtos
{
    public class PicklistItemDto
    {
        public int PicklistItemId { get; set; }

        public int PicklistId { get; set; }

        public int StockItemId { get; set; }

        public string StockItem { get; set; }

        public int OrderQuantity { get; set; }

        public int CategoryId { get; set; }

        public int StockOnHand { get; set; }
    }
}
