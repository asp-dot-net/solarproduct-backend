﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PickList.Dtos
{
    public class GetPicklistDataDto
    {
        public string JobNumber { get; set; }

        public string Date { get; set; }

        public string Name { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string ManualQuoteNo { get; set; }

        public string StoreLoc { get; set; }

        public string MeterPhase { get; set; }

        public string MeterUpgrad { get; set; }

        public string RoofType { get; set; }

        public string PropertyType { get; set; }

        public string RoofPitch { get; set; }

        public string ElecDist { get; set; }

        public string ElecRetailer { get; set; }

        public string NMINumber { get; set; }

        public string MeterNo { get; set; }

        public string InstallerName { get; set; }

        public string InstallerMobile { get; set; }

        public string InstallationDate { get; set; }

        public string SiteAddressLine1 { get; set; }

        public string SiteAddressLine2 { get; set; }

        public string Notes { get; set; }

        public List<PicklistItemDetails> PicklistItemDetails { get; set; }

        public string InstallerNotes { get; set; }

        public string PickedBy { get; set; }

        public string PickedDate { get; set; }

        public string PickSign { get; set; }

        public string ViewHtml { get; set; }
    }

    public class PicklistItemDetails
    {
        public string Category { get; set; }

        public string Item { get; set; }

        public string Model { get; set; }

        public string Qty { get; set; }

        public bool Done { get; set; }
    }
}
