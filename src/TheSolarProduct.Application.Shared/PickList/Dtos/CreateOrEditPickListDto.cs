﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Jobs.Dtos;

namespace TheSolarProduct.PickList.Dtos
{
    public class CreateOrEditPickListDto
    {
        public int JobId { get; set; }

        public string Reason { get; set; }

        public int? SectionId { get; set; }

        public int? picklistwarehouseLocation { get; set; }

        public int? picklistinstallerId { get; set; }

        public int? stockWithId { get; set; }

        public int PicklistType { get; set; }
        
        public List<CreateOrEditJobProductItemDto> JobProductItems { get; set; }
    }
}
