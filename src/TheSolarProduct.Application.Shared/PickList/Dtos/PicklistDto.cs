﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PickList.Dtos
{
    public class PicklistDto
    {
        public int PicklistId { get; set; }

        public int JobId { get; set; }

        public string JobNumber { get; set; }

        public DateTime InstallBookedDate { get; set; }

        public int InstallerId { get; set; }

        public string InstallerName { get; set; }

        public DateTime CreationTime { get; set; }

        public int CreatedBy { get; set; }

        public string DeductBy { get; set; }

        public DateTime? DeductOn { get; set; }

        public int LocationId { get; set; }

        public string Reason { get; set; }

        public int? StockWith { get; set; }
    }
}
