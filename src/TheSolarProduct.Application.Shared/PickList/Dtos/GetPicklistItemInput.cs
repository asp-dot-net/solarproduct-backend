﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PickList.Dtos
{
    public class GetPicklistItemInput
    {
        public int ID { get; set; }
        public DateTime PickListDateTime { get; set; }
        public DateTime? InstallBookedDate { get; set; }
        public string InstallerName { get; set; }
        public string SystemDetail { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DeductOn { get; set; }
        public int ScanCount { get; set; }

        public string Reason { get; set; }
    }
}
