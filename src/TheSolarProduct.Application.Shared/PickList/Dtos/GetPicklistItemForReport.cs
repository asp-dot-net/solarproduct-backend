﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PickList.Dtos
{
	public class GetPicklistItemForReport
	{
		public int Qty { get; set; }
		public string Model { get; set; }
		public string Item { get; set; }
		public string Description { get; set; }
		public int ProductTypeId { get; set; }
		public int PickListId { get; set; }
		public int ScanIteamCount { get; set; }
		public int? ProductItemId { get; set; }
		public int? picklistinstallerId { get; set; }
		public int? picklistwarehouseLocation { get; set; }
		public int? stockWith { get; set; }
		public int PickID { get; set; }

		public int? Stock { get; set; }
	}
}
