﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PickList.Dtos
{
	public class EditPickListDto
	{
		public List<GetPicklistItemForReport> EditedPickList { get; set; }

		public List<GetPicklistItemForReport> PickListResultList { get; set; }

		public int? JobId { get; set; }

		public int? SectionId { get; set; }
		public int? picklistwarehouseLocation { get; set; }
		public int? picklistinstallerId { get; set; }

		public int? stockWithId { get; set; }

		public int pickID { get; set; }
	}
}
