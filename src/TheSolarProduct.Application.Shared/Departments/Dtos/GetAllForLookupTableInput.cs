﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Departments.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}