﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.Departments.Dtos
{
    public class GetAllDepartmentsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }



    }
}