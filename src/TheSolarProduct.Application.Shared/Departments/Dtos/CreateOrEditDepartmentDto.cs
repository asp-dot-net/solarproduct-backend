﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Departments.Dtos
{
    public class CreateOrEditDepartmentDto : EntityDto<int?>
    {
		[Required]
		[StringLength(DepartmentConsts.MaxNameLength, MinimumLength = DepartmentConsts.MinNameLength)]
		public string Name { get; set; }
    }
}