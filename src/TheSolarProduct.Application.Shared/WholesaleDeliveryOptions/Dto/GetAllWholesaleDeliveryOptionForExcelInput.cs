﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholesaleDeliveryOption.Dto
{
    public class GetAllWholesaleDeliveryOptionForExcelInput
    {
        public string Filter { get; set; }
    }
}
