﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholesaleDeliveryOption.Dto
{
    public class GetAllWholesaleDeliveryoptionInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
