﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.WholesaleDeliveryOptions.Dto;

namespace TheSolarProduct.WholesaleDeliveryOption.Dto
{
    public class GetWholesaleDeliveryOptionForEditOutput
    {
        public CreateOrEditWholesaleDeliveryOptionDto Deliveryoption { get; set; }
    }
}
