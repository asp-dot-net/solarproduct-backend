﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholesaleDeliveryOptions.Dto
{
    public class DeliveryOptionDto : EntityDto
    {
        public string Name { get; set; }

        public bool? IsActive { get; set; }
    }
}
