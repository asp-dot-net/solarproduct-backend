﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.WholesaleDeliveryOption.Dto;


namespace TheSolarProduct.WholesaleDeliveryOption
{
    public interface IWholesaleDeliveryOptionAppService : IApplicationService
    {
        Task<PagedResultDto<GetWholesaleDeliveryOptiontForViewDto>> GetAll(GetAllWholesaleDeliveryoptionInput input);

        Task CreateOrEdit(CreateOrEditWholesaleDeliveryOptionDto input);

        Task<GetWholesaleDeliveryOptionForEditOutput> GetWholesaleDeliveryoptionForEdit(EntityDto input);

        Task Delete(EntityDto input);
        Task<FileDto> GetDeliveryoptionToExcel(GetAllWholesaleDeliveryOptionForExcelInput input);
    }
}
