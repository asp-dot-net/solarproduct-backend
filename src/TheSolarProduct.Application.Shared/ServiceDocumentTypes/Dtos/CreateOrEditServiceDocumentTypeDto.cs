﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.ServiceDocumentTypes.Dtos
{
    public class CreateOrEditServiceDocumentTypeDto : EntityDto<int?>
    {
        public string Title { get; set; }
        public bool IsDocumentRequest { get; set; }

    }
}
