﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ServiceDocumentTypes.Dtos
{
    public class ServiceDocumentTypeDto : EntityDto
    {
        public string Title { get; set; }
        public bool IsDocumentRequest { get; set; }


    }
}
