﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ServiceDocumentTypes.Dtos
{
    public class GetAllServiceDocumentTypeInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
