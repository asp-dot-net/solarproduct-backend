﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.DocumentTypes.Dtos;
using TheSolarProduct.ServiceDocumentTypes.Dtos;

namespace TheSolarProduct.ServiceDocumentTypes
{
    public interface IServiceDocumentTypeAppService : IApplicationService
    {
        Task<PagedResultDto<ServiceDocumentTypeDto>> GetAll(GetAllServiceDocumentTypeInput input);

        Task<ServiceDocumentTypeDto> GetDepartmentForView(int id);

        Task<GetServiceDocumentTypeforEditOutput> GetDepartmentForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditServiceDocumentTypeDto input);

        Task Delete(EntityDto input);
    }
}
