﻿using System;

namespace TheSolarProduct.DemoUiComponents.Dto
{
    public class UploadFileOutput
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }

        public string FilePath { get; set; }
    }
}
