﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization.Permissions.Dto;

namespace TheSolarProduct.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
