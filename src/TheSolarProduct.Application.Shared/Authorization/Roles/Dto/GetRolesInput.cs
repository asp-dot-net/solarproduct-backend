﻿using System.Collections.Generic;

namespace TheSolarProduct.Authorization.Roles.Dto
{
    public class GetRolesInput
    {
        public List<string> Permissions { get; set; }
    }
}
