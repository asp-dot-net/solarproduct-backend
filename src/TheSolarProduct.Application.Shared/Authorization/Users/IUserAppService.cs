﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization.Users.Dto;
using TheSolarProduct.Dto;
using TheSolarProduct.Installer.Dtos;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Leads;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.Organizations.Dto;

namespace TheSolarProduct.Authorization.Users
{
    public interface IUserAppService : IApplicationService
    {
        Task<PagedResultDto<UserListDto>> GetUsers(GetUsersInput input);
        Task<PagedResultDto<UserListDto>> GetSalesRep(GetUsersInput input);
        Task<PagedResultDto<UserListDto>> GetSalesManager(GetUsersInput input);
        Task<PagedResultDto<GetInstallerDto>> GetInstaller(GetInstallerInput input);

        Task<List<OrganizationUnitDto>> GetOrganizationUnit();

        Task<FileDto> GetUsersToExcel(GetUsersToExcelInput input);
        Task<FileDto> GetSalesRepToExcel(GetUsersToExcelInput input);
        Task<FileDto> GetSalesManagerToExcel(GetUsersToExcelInput input);
        Task<FileDto> GetInstallerToExcel(GetInstallerToExcelInput input);

        Task<GetUserForEditOutput> GetUserForEdit(NullableIdDto<long> input);
        Task<List<GetUserWiseOrgEditOutput>> UserWiseFromEmail(int UserId);
        Task<GetUserForEditOutput> GetSalesRepForEdit(NullableIdDto<long> input);
        Task<GetUserForEditOutput> GetSalesManagerForEdit(NullableIdDto<long> input);
        Task<GetInstallerForEditOutput> GetInstallerForEdit(NullableIdDto<long> input);

        Task<GetUserPermissionsForEditOutput> GetUserPermissionsForEdit(EntityDto<long> input);
        Task ResetUserSpecificPermissions(EntityDto<long> input);
        Task UpdateUserPermissions(UpdateUserPermissionsInput input);

        Task<GetUserPermissionsForEditOutput> GetInstallerPermissionsForEdit(EntityDto<long> input);
        Task ResetInstallerSpecificPermissions(EntityDto<long> input);
        Task UpdateInstallerPermissions(UpdateUserPermissionsInput input);


        Task ResetSalesRepSpecificPermissions(EntityDto<long> input);
        Task UpdateSalesRepPermissions(UpdateUserPermissionsInput input);

        Task ResetSalesManagerSpecificPermissions(EntityDto<long> input);
        Task UpdateSalesManagerPermissions(UpdateUserPermissionsInput input);

        Task CreateOrUpdateUser(CreateOrUpdateUserInput input);
        Task CreateOrUpdateSalesRep(CreateOrUpdateUserInput input);
        Task CreateOrUpdateSalesManager(CreateOrUpdateUserInput input);
        Task CreateOrUpdateInstaller(CreateOrUpdateInstallerInput input);
        //Task UploadFiles();

        Task DeleteUser(EntityDto<long> input);
        Task DeleteSalesRep(EntityDto<long> input);
        Task DeleteSalesManager(EntityDto<long> input);
        Task DeleteInstaller(EntityDto<long> input);

        Task UnlockUser(EntityDto<long> input);
        Task UnlockSalesRep(EntityDto<long> input);
        Task UnlockSalesManager(EntityDto<long> input);
        Task UnlockInstaller(EntityDto<long> input);
        Task<List<OrganizationUnitDto>> GetUserWiseOrganizationUnit(int userid);
        bool checkorgwisephonedynamicavaibleornot(int userId);
        Task<List<CommonLookupDto>> getOrgWiseDefultandownemailadd(int userId);
        Task AddActivityLog(ActivityLogInput input);
        Task SendSms(SmsEmailDto input);
        Task SendEmail(SmsEmailDto input);
        string LoginuserwiseOrganization();
        Task<List<GetMyInstallerActivityLogDto>> GetMyInstallerActivityLog(GetActivityLogInput input);
        bool checkExistList(int userid, int orgid);
		Task CreateOrEditOrganizationDoc(InstallerContractDto input);
		Task<List<InstallerContractDto>> getUserWiseDocList(int id);
        Task DeleteMyInstallerDoc(int? id);
        Task<List<MyInstallerActivityHistoryDto>> GetMyInstallerActivityLogHistory(GetActivityLogInput input);
       Task ApproveInstaller(int? id);
       Task NotApproveInstaller(int? id);

        Task<FileDto> GetUsersPermissionReportToExcel(GetUsersToExcelInput input);

    }
}