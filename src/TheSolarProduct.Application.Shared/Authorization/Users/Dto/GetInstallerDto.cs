﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Authorization.Users.Dto
{
    public class GetInstallerDto : EntityDto<long>, IPassivable, IHasCreationTime
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }

        public string Mobile { get; set; }
		public string CompanyName { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public Guid? ProfilePictureId { get; set; }

        public bool IsEmailConfirmed { get; set; }

        public List<UserListRoleDto> Roles { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreationTime { get; set; }

		public int TenantId { get; set; }

		public virtual long UserId { get; set; }

		public virtual string InstallerAccreditationNumber { get; set; }

		public virtual DateTime InstallerAccreditationExpiryDate { get; set; }

		public virtual string DesignerLicenseNumber { get; set; }

		public virtual DateTime DesignerLicenseExpiryDate { get; set; }

		public virtual string ElectricianLicenseNumber { get; set; }

		public virtual DateTime ElectricianLicenseExpiryDate { get; set; }

		public virtual string DocInstaller { get; set; }

		public virtual string DocDesigner { get; set; }

		public virtual string DocElectrician { get; set; }

		public virtual string OtherDocsCSV { get; set; }

		public virtual bool? IsInst { get; set; }

		public virtual bool? IsDesi { get; set; }

		public virtual bool? IsElec { get; set; }

		//Address Detail
		public virtual string Address { get; set; }

		public virtual string IsGoogle { get; set; }

		public virtual string latitude { get; set; }

		public virtual string longitude { get; set; }

		public virtual string Unit { get; set; }

		public virtual string UnitType { get; set; }

		public virtual string StreetNo { get; set; }

		public virtual string StreetType { get; set; }

		public virtual string StreetName { get; set; }

		public virtual string Suburb { get; set; }

		public virtual int? StateId { get; set; }
		public virtual string State { get; set; }

		public virtual string PostCode { get; set; }
		public int? SourceTypeId { get; set; }
		public string SourceType { get; set; }
		public string AreaName { get; set; }
		public string Notes { get; set; }
		public string Comment { get; set; }
		public DateTime? Followup { get; set; }
		public string FollowupDesc { get; set; }
		public string CreatorBy { get; set; }
		//public UserManager Users { get; set; }
		public bool? SmsSend { get; set; }
		public DateTime? SmsSendDate { get; set; }
		public bool? EmailSend { get; set; }
		public DateTime? EmailSendDate { get; set; }
		public bool ismyinstalleruser { get; set; }

		public bool IsApproveInstaller { get; set; }
	}
}
