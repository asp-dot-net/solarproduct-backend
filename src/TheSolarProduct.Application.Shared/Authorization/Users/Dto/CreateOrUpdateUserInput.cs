﻿using Abp;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TheSolarProduct.Installer.Dtos;
using TheSolarProduct.Jobs.Dtos;

namespace TheSolarProduct.Authorization.Users.Dto
{
    public class CreateOrUpdateUserInput
    {
        [Required]
        public UserEditDto User { get; set; }

        [Required]
        public string[] AssignedRoleNames { get; set; }

        public List<NameValue<string>> AssignedTeamNames { get; set; }

        public int TeamId { get; set; }

        public bool SendActivationEmail { get; set; }

        public bool SetRandomPassword { get; set; }


        public List<long> OrganizationUnits { get; set; }

		public InstallerAddressDto InstallerAddress { get; set; }

		public CreateOrEditInstallerDetailDto installerDetail { get; set; }

		public CreateOrUpdateUserInput()
        {
            OrganizationUnits = new List<long>();
        }

        public List<CreateOrEditUserOrgDto> UserEmailDetail { get; set; }

        public List<UserIPAddressForEdit> UserIPAddress { get; set; }

        public List<CommonLookupDto> WarehoseLocations { get; set; }

        
    }
}