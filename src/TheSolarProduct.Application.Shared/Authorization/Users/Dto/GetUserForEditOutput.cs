﻿using System;
using System.Collections.Generic;
using TheSolarProduct.Organizations.Dto;
using TheSolarProduct.Installer.Dtos;
using TheSolarProduct.Jobs.Dtos;

namespace TheSolarProduct.Authorization.Users.Dto
{
    public class GetUserForEditOutput
    {
        public Guid? ProfilePictureId { get; set; }

        public UserEditDto User { get; set; }
        
        public UserRoleDto[] Roles { get; set; }

        public List<OrganizationUnitDto> AllOrganizationUnits { get; set; }

        public List<string> MemberedOrganizationUnits { get; set; }

        public List<UserIPAddressForEdit> UserIPAddress { get; set; }

        public List<CommonLookupDto> WarehoseLocations { get; set; }
    }
}