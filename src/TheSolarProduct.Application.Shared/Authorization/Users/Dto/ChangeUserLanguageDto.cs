﻿using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Authorization.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}
