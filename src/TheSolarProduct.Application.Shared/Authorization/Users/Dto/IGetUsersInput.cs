﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;

namespace TheSolarProduct.Authorization.Users.Dto
{
    public interface IGetUsersInput : ISortedResultRequest
    {
        string Filter { get; set; }

        List<string> Permissions { get; set; }

        int? Role { get; set; }
        string RoleName { get; set; }
        int? OrganizationUnit { get; set; }
        bool OnlyLockedUsers { get; set; }
        bool ismyinstalleruser { get; set; }
        bool IsApproveInstaller { get; set; }
    }
}