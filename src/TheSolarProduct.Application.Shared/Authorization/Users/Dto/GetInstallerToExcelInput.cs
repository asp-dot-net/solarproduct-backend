﻿using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Authorization.Users.Dto
{
    public class GetInstallerToExcelInput : IShouldNormalize, IGetInstallerInput
    {
        public string FilterName { get; set; }

        public string Filter { get; set; }

        public List<string> Permissions { get; set; }

        public int? Role { get; set; }

        public string RoleName { get; set; }

        public int? OrganizationUnit { get; set; }

        public bool OnlyLockedUsers { get; set; }
        public bool ismyinstalleruser { get; set; }
        public bool IsApproveInstaller { get; set; }
        public string Sorting { get; set; }

        public string Suburb { get; set; }

        public int? State { get; set; }

        public long? FromPostCode { get; set; }

        public long? ToPostCode { get; set; }

        public int? JobType { get; set; }

        public string AreaType { get; set; }

        public string DateType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name,Surname";
            }
        }
    }
}
