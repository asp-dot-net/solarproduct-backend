﻿using Abp;

namespace TheSolarProduct.Authorization.Users.Dto
{
    public class UpdateInstallerToGreenDealJobArgs
    {
        public int? TenantId { get; set; }

        public UserIdentifier User { get; set; }
    }
}
