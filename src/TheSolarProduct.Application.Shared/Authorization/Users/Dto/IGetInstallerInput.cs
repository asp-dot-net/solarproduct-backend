﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Authorization.Users.Dto
{
    public interface IGetInstallerInput : ISortedResultRequest
    {
        string FilterName { get; set; }
        string Filter { get; set; }

        List<string> Permissions { get; set; }

        int? Role { get; set; }
        string RoleName { get; set; }
        int? OrganizationUnit { get; set; }
        bool OnlyLockedUsers { get; set; }
        bool ismyinstalleruser { get; set; }
        bool IsApproveInstaller { get; set; }

        string Suburb { get; set; }

        int? State { get; set; }

        long? FromPostCode { get; set; }

        long? ToPostCode { get; set; }

        int? JobType { get; set; }

        string AreaType { get; set; }

        string DateType { get; set; }

        DateTime? StartDate { get; set; }

        DateTime? EndDate { get; set; }
    }
}
