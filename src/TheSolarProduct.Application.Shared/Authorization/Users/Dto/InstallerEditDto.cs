﻿using Abp.Auditing;
using Abp.Authorization.Users;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.Authorization.Users.Dto
{
	public class InstallerEditDto : IPassivable
	{
        /// <summary>
        /// Set null to create a new user. Set user's Id to update a user
        /// </summary>
        public long? Id { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxSurnameLength)]
        public string Surname { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        [StringLength(UserConsts.MaxPhoneNumberLength)]
        public string PhoneNumber { get; set; }

        //[Required]
        public string Target { get; set; }

        //[Required]
        public int CategoryId { get; set; }

        public int? MonthlySMSQuote { get; set; }

        public int? MonthlyEMailQuote { get; set; }

        public string CompanyName { get; set; }

        public string Mobile { get; set; }

        // Not used "Required" attribute since empty value is used to 'not change password'
        [StringLength(AbpUserBase.MaxPlainPasswordLength)]
        [DisableAuditing]
        public string Password { get; set; }

        public bool IsActive { get; set; }

        public bool ShouldChangePasswordOnNextLogin { get; set; }

        public virtual bool IsTwoFactorEnabled { get; set; }

        public virtual bool IsLockoutEnabled { get; set; }

        //Installer Details
        public string InstallerAccreditationNumber { get; set; }

        public DateTime InstallerAccreditationExpiryDate { get; set; }

        public string DesignerLicenseNumber { get; set; }

        public DateTime DesignerLicenseExpiryDate { get; set; }

        public string ElectricianLicenseNumber { get; set; }

        public DateTime ElectricianLicenseExpiryDate { get; set; }

        public string DocInstaller { get; set; }

        public string DocDesigner { get; set; }

        public string DocElectrician { get; set; }

        public string DocInstallerFileName { get; set; }

        public string DocDesignerFileName { get; set; }

        public string DocElectricianFileName { get; set; }

        public string OtherDocsCSV { get; set; }

        public long UserId { get; set; }

        public virtual bool? IsInst { get; set; }

        public virtual bool? IsDesi { get; set; }

        public virtual bool? IsElec { get; set; }

        //Address Detail
        public virtual string Address { get; set; }

        public virtual string IsGoogle { get; set; }

        public virtual string latitude { get; set; }

        public virtual string longitude { get; set; }

        public virtual string Unit { get; set; }

        public virtual string UnitType { get; set; }

        public virtual string StreetNo { get; set; }

        public virtual string StreetType { get; set; }

        public virtual string StreetName { get; set; }

        public virtual string Suburb { get; set; }

        public virtual int? StateId { get; set; }

        public virtual string PostCode { get; set; }

        public string FilePath { get; set; }
        public bool ismyinstalleruser { get; set; }
        public int? SourceTypeId { get; set; }
        public string AreaName { get; set; }
        public string Notes { get; set; }
        public string SourceType { get; set; }
        public string State { get; set; }
        public string CreatedUser { get; set; }
        public bool? SmsSend { get; set; }
        public DateTime? SmsSendDate { get; set; }
        public bool? EmailSend { get; set; }
        public DateTime? EmailSendDate { get; set; }
    }
}
