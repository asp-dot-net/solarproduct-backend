﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Authorization.Users.Dto
{
    public class CreateOrEditUserOrgDto : EntityDto<int?>
    {
        public long UserId { get; set; }

        public long OrganizationUnitId { get; set; }
        public string EmailFromAdress { get; set; }
        public string OrganizationUnit { get; set; }
    }
}
