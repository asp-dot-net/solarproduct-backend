﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Authorization.Users.Dto
{
    public class UserPermissiomExcelReport
    {
        public string User { get; set; }

        public bool SalesDashboard { get; set; }

        public bool InvoiceDashboard { get; set; }

        public bool ServiceDashboard { get; set; }

        public bool UnitTypes { get; set; }

        public bool StreetTypes { get; set; }

        public bool StreetNames { get; set; }

        public bool PostCodes { get; set; }

        public bool PostalTypes { get; set; }

        public bool LeadExpenses { get; set; }

        public bool LeadSources { get; set; }

        public bool SalesTeams { get; set; }

        public bool EmpCategories { get; set; }

        public bool CancelLeads { get; set; }

        public bool RejectLeads { get; set; }

        public bool HoldJobs { get; set; }

        public bool RefundReasons { get; set; }

        public bool JobCancellations { get; set; }

        public bool ElecDistributors { get; set; }

        public bool ElecRetailers { get; set; }

        public bool JobTypes { get; set; }

        public bool JobStatuses { get; set; }

        public bool ProductTypes { get; set; }

        public bool ProductItems { get; set; }

        public bool HouseTypes { get; set; }

        public bool RoofTypes { get; set; }

        public bool RoofAngles { get; set; }

        public bool PaymentMethods { get; set; }

        public bool PaymentOptions { get; set; }

        public bool PaymentTerms { get; set; }

        public bool PromotionTypes { get; set; }

        public bool PriceVariations { get; set; }

        public bool InvoicePayments { get; set; }

        public bool InvoiceStatuses { get; set; }

        public bool EmailTemplates { get; set; }

        public bool SMSTemplates { get; set; }

        public bool FreebieTransports { get; set; }

        public bool DocumentTypes { get; set; }

        public bool DocumentLibraries { get; set; }

        public bool StcStatuses { get; set; }

        public bool ServiceCategories { get; set; }

        public bool ServiceSources { get; set; }

        public bool ServiceStatus { get; set; }

        public bool ServiceTypes { get; set; }

        public bool ServiceSubCategories { get; set; }

        public bool ReviewTypes { get; set; }

        public bool PriceItems { get; set; }

        public bool HtmlTemplates { get; set; }

        public bool PostCodeRanges { get; set; }

        public bool WarehouseLocations { get; set; }

        public bool ProductPackages { get; set; }

        public bool CallFlowQueues { get; set; }

        public bool CommissionRanges { get; set; }

        public bool DashboardMessages { get; set; }

        public bool InstallationCost_OtherCharges { get; set; }

        public bool InstallationCost_StateWiseCost { get; set; }

        public bool InstallationCost_InstallationItemList { get; set; }

        public bool InstallationCost_FixedCostPrice { get; set; }

        public bool InstallationCost_ExtraInstallationCharges { get; set; }

        public bool InstallationCost_STCCost { get; set; }

        public bool InstallationCost_BatteryInstallationCost { get; set; }

        public bool InstallationCost_CategoryInstallationItemList { get; set; }

        public bool ServiceDocumentTypes { get; set; }

        public bool ManageLeads { get; set; }

        public bool DuplicateLead { get; set; }

        public bool MyLeads { get; set; }

        public bool LeadTracker { get; set; }

        public bool CancelLead { get; set; }

        public bool RejectLead { get; set; }

        public bool ClosedLead { get; set; }

        public bool Promotions { get; set; }

        public bool PromotionTracker { get; set; }

        public bool ApplicationTracker { get; set; }

        public bool FreebiesTracker { get; set; }

        public bool FinanceTracker { get; set; }

        public bool RefundTracker { get; set; }

        public bool ReferralTracker { get; set; }

        public bool JobActiveTracker { get; set; }

        public bool GridConnectTracker { get; set; }

        public bool STCTracker { get; set; }

        public bool WarrantyTracker { get; set; }

        public bool HoldJobsTracker { get; set; }

        public bool Jobs { get; set; }

        public bool ServicesTracker { get; set; }

        public bool InvoiceIssued { get; set; }

        public bool InvoicePaid { get; set; }

        public bool PayWay { get; set; }

        public bool InvoiceFileList { get; set; }

        public bool MyInstaller { get; set; }

        public bool JobAssign { get; set; }

        public bool Installation_Map { get; set; }

        public bool JobBooking { get; set; }

        public bool Installation_Calenders { get; set; }

        public bool Installation { get; set; }

        public bool InstallerInvoice_New { get; set; }

        public bool PendingInvoice { get; set; }

        public bool ReadyToPay { get; set; }

        public bool MyLeadsGeneration { get; set; }

        public bool LeadsGeneration_Installation { get; set; }

        public bool LeadsGeneration_Map { get; set; }

        public bool Commission { get; set; }

        public bool ManageService { get; set; }

        public bool MyService { get; set; }

        public bool ServiceMap { get; set; }

        public bool ServiceInstallation { get; set; }

        public bool WarrantyClaim { get; set; }

        public bool ServiceInvoice { get; set; }

        public bool Review { get; set; }

        public bool Notification_SMS { get; set; }

        public bool Notification_Email { get; set; }

        public bool ActivityReport { get; set; }

        public bool ToDoActivityReport { get; set; }

        public bool LeadexpenseReport { get; set; }

        public bool OutStandingReport { get; set; }

        public bool JobCostReport { get; set; }

        public bool LeadAssignReport { get; set; }

        public bool LeadSoldReport { get; set; }

        public bool EmpJobCostReport { get; set; }

        public bool JobCommissionReport { get; set; }

        public bool JobCommissionPaidReport { get; set; }

        public bool ProductSoldReport { get; set; }

        public bool UserCallHistoryReport { get; set; }

        public bool StateWiseCallHistoryReport { get; set; }

        public bool CallFlowQueueCallHistoryReport { get; set; }

        public bool WholeSale_Dashboard { get; set; }

        public bool WholeSaleStatuses { get; set; }

        public bool WholeSale_DocumentTypes { get; set; }

        public bool WholeSale_SmsTemplates { get; set; }

        public bool WholeSale_EmailTemplates { get; set; }

        public bool WholeSale_Leads { get; set; }

        public bool WholeSale_Promotions { get; set; }

        public bool WholeSale_PromotionTracker { get; set; }

        public bool WholeSale_Invoice { get; set; }

        public bool Organization { get; set; }

        public bool Roles { get; set; }

        public bool Users { get; set; }

        public bool Installer { get; set; }

        public bool SalesRep { get; set; }

        public bool SalesManager { get; set; }

        public bool AuditLogs { get; set; }

        public bool Maintenance { get; set; }

        public bool HostSettings { get; set; }

        public bool TenantSettings { get; set; }
    }
}
