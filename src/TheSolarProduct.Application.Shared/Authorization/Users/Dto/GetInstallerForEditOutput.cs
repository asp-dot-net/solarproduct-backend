﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Organizations.Dto;

namespace TheSolarProduct.Authorization.Users.Dto
{
	public class GetInstallerForEditOutput
	{
        public Guid? ProfilePictureId { get; set; }

        public InstallerEditDto User { get; set; }

        public UserRoleDto[] Roles { get; set; }

        public List<OrganizationUnitDto> AllOrganizationUnits { get; set; }

        public List<string> MemberedOrganizationUnits { get; set; }

        public List<UserIPAddressForEdit> UserIPAddress { get; set; }
    }
}
