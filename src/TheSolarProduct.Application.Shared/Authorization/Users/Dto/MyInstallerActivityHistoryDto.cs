﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Authorization.Users.Dto
{
    public class MyInstallerActivityHistoryDto : FullAuditedEntity
    {
        public virtual string FieldName { get; set; }
        public virtual string PrevValue { get; set; }
        public virtual string CurValue { get; set; }
        public virtual string Action { get; set; }
        // public virtual string DisplayField { get; set; }
        public virtual DateTime LastmodifiedDateTime { get; set; }
        public int MyInstallerId { get; set; }
        public int MyInstallerActionId { get; set; }
        public virtual string Lastmodifiedbyuser { get; set; }
    }
}
