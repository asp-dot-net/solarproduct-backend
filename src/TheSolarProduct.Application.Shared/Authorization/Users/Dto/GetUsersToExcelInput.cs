﻿using System;
using System.Collections.Generic;
using Abp.Runtime.Validation;

namespace TheSolarProduct.Authorization.Users.Dto
{
    public class GetUsersToExcelInput: IShouldNormalize, IGetUsersInput
    {
        public string Filter { get; set; }

        public List<string> Permissions { get; set; }

        public int? Role { get; set; }

        public string RoleName { get; set; }

        public int? OrganizationUnit { get; set; }

        public bool OnlyLockedUsers { get; set; }
        public bool ismyinstalleruser { get; set; }
        public bool IsApproveInstaller { get; set; }
        public string Sorting { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name,Surname";
            }
        }
       
    }
}
