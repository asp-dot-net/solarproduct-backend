﻿using System.Collections.Generic;
using Abp.Runtime.Validation;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Authorization.Users.Dto
{
    public class GetUsersInput : PagedAndSortedInputDto, IShouldNormalize, IGetUsersInput
    {
        public string Filter { get; set; }

        public List<string> Permissions { get; set; }

        public int? Role { get; set; }
        
        public string RoleName { get; set; }

        public int? OrganizationUnit { get; set; }

        public bool OnlyLockedUsers { get; set; }
        public bool ismyinstalleruser { get; set; }
        public bool IsApproveInstaller { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name,Surname";
            }

            Filter = Filter?.Trim();
        }
    }
}