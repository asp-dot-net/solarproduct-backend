﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Auditing;
using Abp.Authorization.Users;
using Abp.Domain.Entities;

namespace TheSolarProduct.Authorization.Users.Dto
{
    //Mapped to/from User in CustomDtoMapper
    public class UserEditDto : IPassivable
    {
        /// <summary>
        /// Set null to create a new user. Set user's Id to update a user
        /// </summary>
        public long? Id { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxSurnameLength)]
        public string Surname { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        [StringLength(UserConsts.MaxPhoneNumberLength)]
        public string PhoneNumber { get; set; }

        //[Required]
        public string Target { get; set; }

        //[Required]
        public int CategoryId { get; set; }

        public int? MonthlySMSQuote { get; set; }

        public int? MonthlyEMailQuote { get; set; }

        public string CompanyName { get; set; }

        public string Mobile { get; set; }

        // Not used "Required" attribute since empty value is used to 'not change password'
        [StringLength(AbpUserBase.MaxPlainPasswordLength)]
        [DisableAuditing]
        public string Password { get; set; }

        public bool IsActive { get; set; }

        public bool ShouldChangePasswordOnNextLogin { get; set; }

        public virtual bool IsTwoFactorEnabled { get; set; }

        public virtual bool IsLockoutEnabled { get; set; }

        public string ExtensionNumber { get; set; }

        public bool IsOutdoor { get; set; }

        public decimal CommitionAmount { get; set; }

        public int? LeadCount { get; set; }

        public string CimetLink { get; set; }
        public bool ShowAnotherTeamLead { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }
    }
}