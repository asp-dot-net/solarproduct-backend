﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Authorization.Users.Dto
{
    public class UserIPAddressForEdit
    {
        public string IPAdress { get; set; }

        public bool IsActive { get; set; }
    }
}
