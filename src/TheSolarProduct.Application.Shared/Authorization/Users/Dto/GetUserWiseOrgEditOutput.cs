﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Authorization.Users.Dto
{
   public class GetUserWiseOrgEditOutput
    {
        public CreateOrEditUserOrgDto UserEmailDetail { get; set; }
    }
}
