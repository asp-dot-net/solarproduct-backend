﻿using System.Collections.Generic;
using TheSolarProduct.Authorization.Permissions.Dto;

namespace TheSolarProduct.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}