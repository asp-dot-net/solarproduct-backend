﻿using Abp;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.Authorization.Users.Dto
{
	public class CreateOrUpdateInstallerInput
	{
        [Required]
        public InstallerEditDto User { get; set; }

        [Required]
        public string[] AssignedRoleNames { get; set; }

        public List<NameValue<string>> AssignedTeamNames { get; set; }

        public int TeamId { get; set; }

        public bool SendActivationEmail { get; set; }

        public bool SetRandomPassword { get; set; }

        public List<long> OrganizationUnits { get; set; }

        public CreateOrUpdateInstallerInput()
        {
            OrganizationUnits = new List<long>();
        }
        public List<CreateOrEditUserOrgDto> UserEmailDetail { get; set; }

        public List<UserIPAddressForEdit> UserIPAddress { get; set; }
    }
}
