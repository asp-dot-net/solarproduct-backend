﻿using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Authorization.Accounts.Dto
{
    public class SendEmailActivationLinkInput
    {
        [Required]
        public string EmailAddress { get; set; }
    }
}