﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.MultiTenancy.HostDashboard.Dto;
using TheSolarProduct.TenantDashboard.Dto;

namespace TheSolarProduct.TenantDashboard
{    
    public interface ITenantDashboardAppService : IApplicationService
    {        
        List<LeadSummary> GetLeadSummary(DateTime startDate, DateTime endDate);
        
        List<LeadSummaryDetails> GetLeadSummaryDetails(DateTime startDate, DateTime endDate);

        Task<List<MyRemindersDto>> MyReminder();
        
        DataTable OrganizationWiseTrackerCount();

        Task<TrackerCountList> TrackerList();
        Task<List<TrackerCountList>> PivotTrackerList();
        Task<GetEditionTenantStatisticsOutput> GetareaStatisticsData(int OrganizationId, DateTime startDate, DateTime endDate);

        Task<GetEditionTenantStatisticsOutput> GettypeStatisticsData(int OrganizationId, DateTime startDate, DateTime endDate);
        Task<GetEditionTenantStatisticsOutput> GetstateStatisticsData(int OrganizationId, DateTime startDate, DateTime endDate);
        Task<GetEditionTenantStatisticsOutput> GetLeadStatusStatisticsData(int OrganizationId, DateTime startDate, DateTime endDate);
        Task<GetEditionTenantStatisticsOutput> GetLeadSourceStatisticsData(int OrganizationId, DateTime startDate, DateTime endDate);
        LeadStatusWiseCount LeadSourceWiseCount(int OrganizationId, DateTime startDate, DateTime endDate);

        List<LeadSummary> GetOrgList(DateTime startDate, DateTime endDate);

        Task<List<SalesDetailDto>> SalesDetail();
        Task<SalesDetailDto> GetLeadCountsForSalesRep(int? Id);

        Task<List<InvoiceStatusDetailDto>> GetInvoiceStatusDetail(DateTime startDate, DateTime endDate);
        Task<List<ToDoDetailDto>> GetToDoDetail(int? Id);
        LeadStatusWiseCount LeadSourceCountwiserank( DateTime startDate, DateTime endDate);
        List<LeadStatusWiseCountDetailDto> LeadSourceDetailMonthly(DateTime startDate, DateTime endDate);
        List<LeadStatusWiseCountDetailDto> LeadSourceDetailDaily(DateTime startDate, DateTime endDate);
    }
}
