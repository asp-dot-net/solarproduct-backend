﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.TenantDashboard.Dto
{
	public class TrackerCountList
	{
		public int Application { get; set; }

		public int GridConnection { get; set; }

		public int STC { get; set; }

		public int ProjectInvoice { get; set; }

		public int InstallerInvoice { get; set; }

		public int Finance { get; set; }

		public int Refund { get; set; }

		public int Freebies { get; set; }

		public int UnAssigned { get; set; }

		public int New { get; set; }

		public int PendingInstallation { get; set; }

		public string TodaysInstallation { get; set; }

		public int PromoReply { get; set; }
		public string Organization { get; set; }


	}
}
