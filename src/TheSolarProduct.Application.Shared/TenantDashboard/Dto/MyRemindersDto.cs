﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.TenantDashboard.Dto
{
    public class MyRemindersDto
    {
        public string CustomerName { get; set; }
        public DateTime? ReminderDate { get; set; }
        public string Comment { get; set; }

        public string Source { get; set; }

        public string Mobileno { get; set; }

        public string email { get; set; }
    }
}
