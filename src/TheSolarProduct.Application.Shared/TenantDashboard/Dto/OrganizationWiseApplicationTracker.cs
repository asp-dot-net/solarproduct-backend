﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.TenantDashboard.Dto
{
    public class OrganizationWiseApplicationTracker
    {
        public List<OrganizationName> Organizations { get; set; }
        public List<TrackerInfo> TrackerInformation { get; set; }
    }
    public class OrganizationName
    {
        public string OrgName { get; set; }
        public long Id { get; set; }
    }
    public class TrackerInfo
    {
        public string OrgName { get; set; }
        public long OrgId { get; set; }
        public int PendingApplication { get; set; }
        public int PendingGridConnectionApplication { get; set; }
        public int PendingSTC { get; set; }
        public int PendingProjectInvoice { get; set; }
        public int PendingInstallerInvoice { get; set; }
        public int FinanceRequest { get; set; }
        public int RefundRequest { get; set; }
        public int Freebies { get; set; }
        public int UnassignedLeads { get; set; }
        public int NewLeads { get; set; }
        public List<NewLeadUserNames> AssignedNewLeads { get; set; }
        public int PendingInstallation { get; set; }
        public string TodaysInstallation { get; set; }
        public int PromoReply { get; set; }
    }
    public class NewLeadUserNames
    {
        public string name { get; set; }
        public int NewLeadCount { get; set; }
    }
}
