﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.TenantDashboard.Dto
{
    public class LeadSummaryDetails
    {
        public string OrgName { get; set; }
        public int Lead { get; set; }
        public int Jobs { get; set; }
        public int DepositeReceived { get; set; }
        public int Active { get; set; }
        public int InstallationBook { get; set; }
        public int InstallationCompleted { get; set; }
        public int Completed { get; set; }
    }
}
