﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.TenantDashboard.Dto
{
    public class SalesDetailDto
    {
        public string Name { get; set; }
        public int New { get; set; }
        public int UnHandled { get; set; }
        public int Cold { get; set; }
        public int Warm { get; set; }
        public int Hot { get; set; }
        public int Upgrade { get; set; }
        public int Rejected { get; set; }
        public int Canceled { get; set; }
        public int Closed { get; set; }
        public int Assigned { get; set; }
        public int ReAssigned { get; set; }
        public int LeadId { get; set; }
        public int Planned { get; set; }
        public int OnHold { get; set; }
        public int Cancel { get; set; }
        public int DepositeReceived { get; set; }
        public int Active { get; set; }
        public int JobBook { get; set; }
        public int JobIncomplete { get; set; }
        public int JobInstalled { get; set; }
        public int TotalLead { get; set; }
        public decimal? PlannedTotalCost { get; set; }
        public decimal? OnHoldTotalCost { get; set; }
        public decimal? CancelTotalCost { get; set; }
        public decimal? DepositeReceivedTotalCost { get; set; }
        public decimal? ActiveTotalCost { get; set; }
        public decimal? JobBookTotalCost { get; set; }
        public decimal? JobIncompleteTotalCost { get; set; }
        public decimal? JobInstalledTotalCost { get; set; }


        public int? TotalJobs { get; set; }

        public int? TotalLeads { get; set; }
        public decimal? TotalCost { get; set; }
        public decimal? TotalJobsRefundAmount { get; set; }
        public decimal? TotalJobsCost { get; set; }
        
    }

    public class LeadStatuswisecountDto
    {
        public string Name { get; set; }
        public decimal Tv { get; set; }
        public decimal Google { get; set; }
        public decimal FaceBook { get; set; }
        public decimal Others { get; set; }
        public decimal Refferal { get; set; }

        public decimal Tvupgrade { get; set; }
        public decimal Googleupgrade { get; set; }
        public decimal FaceBookupgrade { get; set; }
        public decimal Othersupgrade { get; set; }
        public decimal Refferalupgrade { get; set; }

        public decimal Tvsales { get; set; }
        public decimal Googlesales { get; set; }
        public decimal FaceBooksales { get; set; }
        public decimal Otherssales { get; set; }
        public decimal Refferalsales { get; set; }

        public decimal TvRatio { get; set; }
        public decimal GoogleRatio { get; set; }
        public decimal FaceBookRatio { get; set; }
        public decimal OthersRatio { get; set; }
        public decimal RefferalRatio { get; set; }
        
    }
}
