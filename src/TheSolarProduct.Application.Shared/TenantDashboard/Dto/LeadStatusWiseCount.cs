﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.TenantDashboard.Dto
{
	public class LeadStatusWiseCount
	{
		public int Total { get; set; }

		public int New { get; set; }
		public int UnHandled { get; set; }
		public int Cold { get; set; }
		public int Warm { get; set; }
		public int Hot { get; set; }
		public int Upgrade { get; set; }
		public int Rejected { get; set; }
		public int Canceled { get; set; }
		public int Closed { get; set; }
		public int Assigned { get; set; }
		public int ReAssigned { get; set; } 
		public int ACT { get; set; }
		public int NSW { get; set; }
		public int NT { get; set; }
		public int QLD { get; set; }
		public int SA { get; set; }
		public int TAS { get; set; }
		public int VIC { get; set; }
		public int WA { get; set; } 
		public int Metro { get; set; }
		public int Regional { get; set; } 
		public int Res { get; set; }
		public int Com { get; set; }
		public int Google { get; set; }
		public int Referral { get; set; }
		public int TV { get; set; }
		public int Facebook { get; set; }
		public int Others { get; set; }
		public int Lead1 { get; set; }
		public int Lead2 { get; set; }
		public int Lead3 { get; set; }
		public int Lead4 { get; set; }
		public int Lead5 { get; set; }
		public int Lead6 { get; set; }
		public int Lead7 { get; set; }
		public int Lead8 { get; set; }
		public int Lead9 { get; set; }
		public int Lead10 { get; set; }
		public int SBFacebook { get; set; }
		public int SBGoogle { get; set; }
		public int AriseProspects { get; set; }

		public string username { get; set; }

	}

	public class Person 
	{
		public int Rank { get; set; }
		public int assignedid { get;   set; }
		public int Score { get; set; }
}
	public class LeadStatusWiseCountDetailDto
	{
		public string jobnumber { get; set; }
		public int?  panels { get; set; }
		 
	}

}
