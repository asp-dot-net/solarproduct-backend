﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.TenantDashboard.Dto
{
    public class LeadSummary
    {
        public int OrgId { get; set; }
        public string OrgName { get; set; }
        public int Lead { get; set; }
        public int DuplicateLead { get; set; }
        public int TotalLead { get; set; }
    }
}
