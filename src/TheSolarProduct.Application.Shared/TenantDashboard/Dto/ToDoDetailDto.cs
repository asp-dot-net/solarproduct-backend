﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.TenantDashboard.Dto
{
    public class ToDoDetailDto
    {
        public string CustomerName {get;set;}
        public string ActivityNote {get;set;}
        public DateTime? CreationDate {get;set;}
        public string CreatedUser {get;set;}

        public string jobnumber { get; set; }
        public int? Id { get; set; }
    }
}
