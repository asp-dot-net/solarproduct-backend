﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.TenantDashboard.Dto
{
    public class InvoiceStatusDetailDto
    {
        public string OrgName {get; set;}
        public string Name {get; set;}
        public decimal? Planned { get; set; }
        public decimal? OnHold { get; set; }
        public decimal? Cancel { get; set; }
        public decimal? DepositeReceived { get; set; }
        public decimal? Active { get; set; }
        public decimal? JobBook { get; set; }
        public decimal? JobIncomplete { get; set; }
        public decimal? JobInstalled { get; set; }
        public decimal? Total { get; set; }
    }
}
