﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.StreetNames.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.StreetNames
{
    public interface IStreetNamesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetStreetNameForViewDto>> GetAll(GetAllStreetNamesInput input);

        Task<GetStreetNameForViewDto> GetStreetNameForView(int id);

		Task<GetStreetNameForEditOutput> GetStreetNameForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditStreetNameDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetStreetNamesToExcel(GetAllStreetNamesForExcelInput input);

		
    }
}