﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.StreetNames.Dtos
{
    public class GetAllStreetNamesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }



    }
}