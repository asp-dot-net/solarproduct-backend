﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.StreetNames.Dtos
{
    public class CreateOrEditStreetNameDto : EntityDto<int?>
    {

		[Required]
		[StringLength(StreetNameConsts.MaxNameLength, MinimumLength = StreetNameConsts.MinNameLength)]
		public string Name { get; set; }
        public Boolean IsActive { get; set; }


    }
}