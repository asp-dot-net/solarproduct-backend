﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.StreetNames.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}