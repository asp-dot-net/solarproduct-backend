﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.StreetNames.Dtos
{
    public class GetStreetNameForEditOutput
    {
		public CreateOrEditStreetNameDto StreetName { get; set; }


    }
}