﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.StreetNames.Dtos
{
    public class StreetNameDto : EntityDto
    {
		public string Name { get; set; }

        public Boolean IsActive { get; set; }

    }
}