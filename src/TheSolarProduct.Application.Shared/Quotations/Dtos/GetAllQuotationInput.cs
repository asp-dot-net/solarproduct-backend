﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Quotations.Dtos
{
    public class GetAllQuotationInput : PagedAndSortedResultRequestDto
    {
        public int JobId { get; set; }
    }
}
