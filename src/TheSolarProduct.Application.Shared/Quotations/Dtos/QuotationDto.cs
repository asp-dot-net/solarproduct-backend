﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Quotations.Dtos
{
    public class QuotationDto : EntityDto
    {
        public DateTime QuoteDate { get; set; }
        public bool IsSigned { get; set; }
        public Guid? DocumentId { get; set; }
        public int? JobId { get; set; }
        public string CreatedBy { get; set; }
        public string QuoteFileName { get; set; }
        public string SignedQuoteFileName { get; set; }
        public string QuoteFilePath { get; set; }
        public string QuoteNumber { get; set; }
    }
}
