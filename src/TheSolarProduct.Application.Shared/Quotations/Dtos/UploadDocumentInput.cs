﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using Abp.Extensions;
using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.Quotations.Dtos
{
    public class UploadDocumentInput : EntityDto<int>,ICustomValidate
    {
        [MaxLength(400)]
        public string FileToken { get; set; }
        public virtual int? DocumentTypeId { get; set; }
        public Guid? MediaId { get; set; }
        public virtual int? JobId { get; set; }
        public virtual string FileName { get; set; }
        public virtual string FileType { get; set; }
        public virtual string FilePath { get; set; }
        public virtual string SectionName { get; set; }
        public virtual int? SectionId { get; set; }
        public void AddValidationErrors(CustomValidationContext context)
        {
            if (FileToken.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(FileToken));
            }
        }
    }
}
