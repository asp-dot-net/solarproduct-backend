﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Quotations.Dtos
{
    public class GetQuotationForViewDto
    {
        public QuotationDto Quotation { get; set; }

        public bool NewQuote { get; set; }
    }
}
