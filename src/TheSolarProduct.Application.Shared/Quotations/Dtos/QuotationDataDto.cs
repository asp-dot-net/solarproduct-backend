﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Quotations.Dtos
{
    public class QuotationDataDto
    {
        public int JobId { get; set; }

        public int QuotationId { get; set; }

        public string QuoteNumber { get; set; }

        public string Date { get; set; }

        public string Name { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string SystemWith { get; set; }

        public string Capecity { get; set; }

        public string BatteryCapecity { get; set; }

        public string Notes { get; set; }

        public string InstName { get; set; }

        public string InstMobile { get; set; }

        public string InstEmail { get; set; }

        public string SubTotal { get; set; }

        public string STC { get; set; }

        public string STCDesc { get; set; }

        public string GrandTotal { get; set; }

        public string DepositeText { get; set; }

        public string Deposite { get; set; }

        public string Total { get; set; }

        public string Balance { get; set; }

        public string MeaterPhase { get; set; }

        public string PaymentMethod { get; set; }

        public string SpecialDiscount { get; set; }

        public string AdditionalCharges { get; set; }

        public string MeaterUpgrade { get; set; }

        public string NoofStory { get; set; }

        public string RoofTypes { get; set; }
        
        public string RoofPinch { get; set; }
        
        public string EnergyDist { get; set; }
        
        public string EnergyRetailer { get; set; }
        
        public string NMINumber { get; set; }

        public string MeterNo { get; set; }

        public string NearMap1 { get; set; }

        public string NearMap2 { get; set; }
        
        public string NearMap3 { get; set; }

        public string DepositRequired { get; set; }

        public string STCIncetive { get; set; }

        public string SolarVICRebate { get; set; }

        public string SolarVICLoanDiscount { get; set; }

        public string FinalPrice { get; set; }

        public string InverterLocation { get; set; }

        public List<QunityAndModelList> qunityAndModelLists { get; set; }  

        public string ViewHtml { get; set; }  

        public string SignSrc { get; set; }  

        public string SignName { get; set; }  
        
        public string SignDate { get; set; }  

        public string RepresentativeSignSrc { get; set; }  

        public string VicRebate { get; set; }  

        public string TotalCost { get; set; }

        public string PaymentOption { get; set; }

        public string FinPaymentType { get; set; }

        public string DepositOption { get; set; }

        public string FinancePurchaseNo { get; set; }

        public decimal? FinanceAmount { get; set; }

        public decimal? FinanceDepositeAmount { get; set; }

        public decimal? FinanceNetAmount { get; set; }

        public string PaymentTerm { get; set; }

        public decimal? RepaymentAmount { get; set; }

        public int? PerformanceWarranty { get; set; }

        public int? ProductWarranty { get; set; }

        public int? ExtendedWarranty { get; set; }

        public int? WorkmanshipWarranty { get; set; }

        public string RebateRefNo { get; set; }

        public string ManuPhone { get; set; }

        public string ManuMobile { get; set; }

        public string ManuEmail { get; set; }

        public string ManuWebsite { get; set; }

        public decimal? BatteryRebate { get; set; }



    }

    public class QunityAndModelList
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
    
}
