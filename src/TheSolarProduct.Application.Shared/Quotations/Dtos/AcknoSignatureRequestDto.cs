﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Quotations.Dtos
{
    public class AcknoSignatureRequestDto
    {
        public string JobNumber { get; set; }

        public string CustName { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }
        
        public string Mobile { get; set; }

        public string Kw { get; set; }

        public bool Expiry { get; set; }

        public bool Signed { get; set; }
        
        public string DocNo { get; set; }

        public string InstName { get; set; }

        public string Organization { get; set; }

        public string Date { get; set; }

        public string OrgLogo { get; set; }

        public int OrganizationId { get; set; }
    }
}
