﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Quotations.Dtos
{
	public class SignatureRequestDto
	{
		public bool Expiry { get; set; }

		public bool Signed { get; set; }
		
		public string QuoteNo { get; set; }
		
		public string CustName { get; set; }

		public string CustMobile { get; set; }

		public string CustEmail { get; set; }

		public string Address1 { get; set; }

		public string Address2 { get; set; }

		public string MeterPhase { get; set; }
		
		public string Upgrades { get; set; }

		public string RoofType { get; set; }

		public string RoofPinch { get; set; }

		public string EnergyDist { get; set; }

		public string EnergyRet { get; set; }

		public string NonOfStory { get; set; }

		public string NMINumber { get; set; }

		public string JobNumber { get; set; }

		public List<QunityAndModelList> QunityAndModelLists { get; set; }

		public string SubTotal { get; set; }

		public string STC { get; set; }

		public string STCDesc { get; set; }

		public string GrandTotal { get; set; }

		public string SolarVICRebate { get; set; }

		public string SolarVICLoanDiscount { get; set; }

		public string AdditionalCharges { get; set; }

		public string Total { get; set; }

		public string DepositRequired { get; set; }

		public string DepositeText { get; set; }

		public string Balance { get; set; }

		public int OrganizationId { get; set; }

		public string Organization { get; set; }

		public string OrgLogo { get; set; }

		public string State { get; set; }

		public string Capecity { get; set; } 

		//public string BattryCapecity { get; set; } 

		public string Discount { get; set; }

		public string VicRebate { get; set; }
		public string JobNotes { get; set; }
    }
}
