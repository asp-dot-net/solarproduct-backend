﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Quotations.Dtos
{
    public class CreateOrEditQuotationDto : EntityDto<int?>
    {
        public virtual DateTime QuoteDate { get; set; }
        public virtual bool IsSigned { get; set; }
        public virtual Guid? DocumentId { get; set; }
        public virtual string QuoteMode { get; set; }
        public virtual int? JobId { get; set; }
        public string EmailBody { get; set; }

        public string EmailTo { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public string Subject { get; set; }

        public object quotationString { get; set; }

        public string EmailFrom { get; set; }

        public int? SectionId { get; set; }
    }
}
