﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Quotations.Dtos
{
    public class GetDocumentTypeForView : EntityDto
    {
        public string DocumentTitle { get; set; }
    }
}
