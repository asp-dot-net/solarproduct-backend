﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Quotations.Dtos
{
    public class GetJobDocumentForView : EntityDto
    {
        public virtual int? DocumentTypeId { get; set; }
        public string DocumentTitle { get; set; }
        public string DocumentPath { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public Guid? MediaId { get; set; }
        public virtual int? JobId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreationTime { get; set; }
    }
}
