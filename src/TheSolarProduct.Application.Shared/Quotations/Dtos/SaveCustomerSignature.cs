﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Quotations.Dtos
{
	public class SaveCustomerSignature
	{
		public string CustSignIP { get; set; }

		public string CustSignLongitude { get; set; }

		public string CustSignLatitude { get; set; }

		public string EncString { get; set; }

		public string ImageData { get; set; }
	}
}
