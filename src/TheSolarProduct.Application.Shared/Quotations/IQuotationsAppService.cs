﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheSolarProduct.Quotations.Dtos;

namespace TheSolarProduct.Quotations
{
    public interface IQuotationsAppService : IApplicationService
    {
        Task<PagedResultDto<GetQuotationForViewDto>> GetAll(GetAllQuotationInput input);
        Task<GetQuotationForViewDto> GetQuotationForView(int id);
        Task<GetQuotationForEditOutput> GetQuotationForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditQuotationDto input);
        Task SendQuotation(CreateOrEditQuotationDto input);
        Task Delete(EntityDto input);
        Task SaveDocument(UploadDocumentInput input);
        Task<SignatureRequestDto> QuotationData(string STR);
        Task SaveSignature(SaveCustomerSignature input);
        Task<List<GetDocumentTypeForView>> GetAllDocumentType();
        Task<List<GetJobDocumentForView>> GetJobDocumentsByJobId(int jobId);

        Task<QuotationDataDto> GetQuotationDataByQuoateIdJobId(int JobId, int? QuoteId);

        Task<List<GetDocumentTypeForView>> GetActiveDocumentType();

        Task DeleteJobDocument(EntityDto input, int sectionId);
    }
}
