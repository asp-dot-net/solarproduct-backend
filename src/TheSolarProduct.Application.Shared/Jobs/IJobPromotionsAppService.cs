﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using System.Collections.Generic;


namespace TheSolarProduct.Jobs
{
    public interface IJobPromotionsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetJobPromotionForViewDto>> GetAll(GetAllJobPromotionsInput input);

		Task<List<GetJobPromotionForEditOutput>> GetJobPromotionByJobId(int jobid);
		
		Task<GetJobPromotionForEditOutput> GetJobPromotionForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditJobPromotionDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetJobPromotionsToExcel(GetAllJobPromotionsForExcelInput input);

		//Task<List<JobPromotionJobLookupTableDto>> GetAllJobForTableDropdown();
		
		Task<List<JobPromotionPromotionMasterLookupTableDto>> GetAllPromotionMasterForTableDropdown();

		Task<List<JobPromotionPromotionMasterLookupTableDto>> GetAllFreebieTransportRepositoryForTableDropdown();
		Task FreebiesSendSms(int id ,int smstempid);

		Task FreebiesSendEmail(int id, int emailtempid);
		
	}
}