﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Jobs
{
    public interface IPromotionMastersAppService : IApplicationService 
    {
        Task<PagedResultDto<GetPromotionMasterForViewDto>> GetAll(GetAllPromotionMastersInput input);

        Task<GetPromotionMasterForViewDto> GetPromotionMasterForView(int id);

		Task<GetPromotionMasterForEditOutput> GetPromotionMasterForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditPromotionMasterDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetPromotionMastersToExcel(GetAllPromotionMastersForExcelInput input);

		
    }
}