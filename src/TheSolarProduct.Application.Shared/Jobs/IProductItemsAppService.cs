﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using System.Collections.Generic;


namespace TheSolarProduct.Jobs
{
	public interface IProductItemsAppService : IApplicationService
	{
		Task<PagedResultDto<GetProductItemForViewDto>> GetAll(GetAllProductItemsInput input);

		Task<GetProductItemForViewDto> GetProductItemForView(int id);

		Task<GetProductItemForEditOutput> GetProductItemForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditProductItemDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetProductItemsToExcel(GetAllProductItemsForExcelInput input);

		Task<List<ProductItemProductTypeLookupTableDto>> GetAllProductTypeForTableDropdown();

		Task FetchAndInsert();

		Task SaveProductIteamDocument(string FileToken, string FileNames, int id);

        Task<List<GetProductItemsCountWithProdoctTypeDto>> GetAllProductItemsCountWithProdoctType();

		Task SaveProductIteamImage(string FileToken, int id);

		//Task FetchProductItemFormGreenDeal();
    }
}