﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Jobs
{
    public interface IPaymentOptionsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetPaymentOptionForViewDto>> GetAll(GetAllPaymentOptionsInput input);

        Task<GetPaymentOptionForViewDto> GetPaymentOptionForView(int id);

		Task<GetPaymentOptionForEditOutput> GetPaymentOptionForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditPaymentOptionDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetPaymentOptionsToExcel(GetAllPaymentOptionsForExcelInput input);

		
    }
}