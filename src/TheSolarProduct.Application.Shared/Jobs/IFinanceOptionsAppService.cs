﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Jobs
{
    public interface IFinanceOptionsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetFinanceOptionForViewDto>> GetAll(GetAllFinanceOptionsInput input);

        Task<GetFinanceOptionForViewDto> GetFinanceOptionForView(int id);

		Task<GetFinanceOptionForEditOutput> GetFinanceOptionForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditFinanceOptionDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetFinanceOptionsToExcel(GetAllFinanceOptionsForExcelInput input);

		
    }
}