﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Jobs
{
    public interface IJobTypesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetJobTypeForViewDto>> GetAll(GetAllJobTypesInput input);

        Task<GetJobTypeForViewDto> GetJobTypeForView(int id);

		Task<GetJobTypeForEditOutput> GetJobTypeForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditJobTypeDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetJobTypesToExcel(GetAllJobTypesForExcelInput input);

		
    }
}