﻿using Abp;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Common.Dto;
using TheSolarProduct.Dto;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Quotations.Dtos;
using TheSolarProduct.Reports.JobCost.Dtos;

namespace TheSolarProduct.Jobs
{
    public  interface IJobInstallerInvoicesAppService : IApplicationService
    {
        Task CreateOrEdit(CreateOrEditJobInstallerInvoiceDto input);

        bool CheckInstallerDetail(CreateOrEditJobInstallerInvoiceDto input);
        
        Task Delete(EntityDto input);

        Task<PagedResultDto<InstallerNewDto>> GetAll(InstallerNewInputDto input);

        Task<List<GetSearchResultDto>> GetSearchFilter(string str, string filterName);

        Task<List<GetSearchResultDto>> GetSearchFilterByOrg(string str, string InvoiceType, int OrganizationId);

        Task<CreateOrEditJobInstallerInvoiceDto> GetDataForEdit(EntityDto input);

        Task<PagedResultDto<GetInstallerInvoiceForViewDto>> GetPendingInstallerInvoice(GetAllInstallerInvoiceInput input);

        Task<CreateOrEditJobInstallerInvoiceDto> GetInstallerInvoiceForEdit(EntityDto input);

        Task UpdateAddNotes(CreateOrEditJobInstallerInvoiceDto input);

        Task UpdateApprovedInvoice(CreateOrEditJobInstallerInvoiceDto input);

        Task<PagedResultDto<InstallerPaidDto>> GetInstallerApprovedList(InstallerPaidInputDto input);

        Task RevertApprovedInvoice(int installerinvoiceId);

        Task UpdateInvoicePayment(CreateOrEditJobInstallerInvoiceDto input);





        //Task SaveDocument(UploadDocumentInput input);
        //Task<List<CreateOrEditJobInstallerInvoiceDto>> GetJobInstllationInvoicesByJobId(EntityDto input);

        //Task<PagedResultDto<InstallerPaidDto>> GetInstallerPaidList(InstallerPaidInputDto input);
        //Task<CreateOrEditJobInstallerInvoiceDto> GetEditInstallerInvoice(EntityDto input);

        //Task UpdatechangeDate(updateremarkinvoiceDto input);
        Task<List<JobJobTypeLookupTableDto>> GetInstallerForTableDropdown();
        //Task<List<string>> GetOnlyprojectno(string leadSourceName);
        

        //Task<TotalSammaryCountDto> getAllTrackerCount(int organizationid);
        Task<List<CommonLookupDto>> GetAllJobNumberList();
        
        Task RevertVerifiedInvoice(int installerinvoiceId);
        
        
        Task<PagedResultDto<InstallerInvoiceFilesViewDto>> GetAllInvoiceFiles(InstallerInvoiceFilesInput input);
        Task<FileDto> getinvoiceimportToExcel(InstallerInvoiceImportDataInput input);
        Task<PagedResultDto<InstallerInvoiceImportDataViewDto>> GetAllInvoiceImportData(InstallerInvoiceImportDataInput input);
        
        List<NameValue<string>> promotionGetAllLeadSource(string leadSourceName);
        Task<List<string>> GetInstallerForDropdown(string leadSourceName);

        Task<List<string>> GetAlljobNumberforAdd(string leadSourceName);
        Task<List<string>> GetAlljobNumberforAddToDo(string leadSourceName);
        //Task<List<string>> GetAlljobNumberforCommonFilter(string leadSourceName);
        
        CreateOrEditImportDto getImportDataForEdit(int? Id);

        Task DeleteImportData(EntityDto input, int SectionId);

        Task Update(CreateOrEditImportDto input);
        Task<PagedResultDto<PayWayViewDataDto>> GetPayWayListingData(PayWayInputDataDto input);

        Task<List<GetSearchResultDto>> GetAllJobNumberForMyService(string input);

        bool CheckDuplicateJobnumber(string inspection, int jobId, int id);

        Task CreateManualInvoicePaid(CreatePaidInvoiceDto input);

        Task<FileDto> GetAllToExcel(InstallerNewInputForExcelDto input);

        Task VerifyOrReadyToPay(List<int> ids, int verifyOrReadyToPay);

        Task<FileDto> GetInstallerApprovedListToExcel(InstallerPaidInputForExcelDto input);

        Task<FileDto> GetAllToExcelPerc(InstallerNewInputForExcelDto input);

        Task RevertInvoicePayment(int Id, int? sectionid);
    }
}
