﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Jobs
{
    public interface IJobStatusesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetJobStatusForViewDto>> GetAll(GetAllJobStatusesInput input);

        Task<GetJobStatusForViewDto> GetJobStatusForView(int id);

		Task<GetJobStatusForEditOutput> GetJobStatusForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditJobStatusDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetJobStatusesToExcel(GetAllJobStatusesForExcelInput input);

		
    }
}