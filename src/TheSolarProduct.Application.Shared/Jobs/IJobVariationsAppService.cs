﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using System.Collections.Generic;


namespace TheSolarProduct.Jobs
{
    public interface IJobVariationsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetJobVariationForViewDto>> GetAll(GetAllJobVariationsInput input);

		Task<GetJobVariationForEditOutput> GetJobVariationForEdit(EntityDto input);

		Task<List<GetJobVariationForEditOutput>> GetJobVariationByJobId(int jobid);

		Task CreateOrEdit(CreateOrEditJobVariationDto input);

		Task Delete(EntityDto input);

		
		Task<List<JobVariationVariationLookupTableDto>> GetAllVariationForTableDropdown();
		
		//Task<List<JobVariationJobLookupTableDto>> GetAllJobForTableDropdown();
		
    }
}