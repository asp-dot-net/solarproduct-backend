﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Jobs
{
    public interface IMeterPhasesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetMeterPhaseForViewDto>> GetAll(GetAllMeterPhasesInput input);

        Task<GetMeterPhaseForViewDto> GetMeterPhaseForView(int id);

		Task<GetMeterPhaseForEditOutput> GetMeterPhaseForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditMeterPhaseDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetMeterPhasesToExcel(GetAllMeterPhasesForExcelInput input);

		
    }
}