﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using System.Collections.Generic;


namespace TheSolarProduct.Jobs
{
    public interface IJobRefundsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetJobRefundForViewDto>> GetAll(GetAllJobRefundsInput input);

        Task<GetJobRefundForViewDto> GetJobRefundForView(int id);

		Task<GetJobRefundForEditOutput> GetJobRefundForEdit(EntityDto input);

		Task<List<CreateOrEditJobRefundDto>> GetJobRefundByJobId(EntityDto input);

		Task CreateOrEdit(CreateOrEditJobRefundDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetJobRefundsToExcel(GetAllJobRefundsForExcelInput input);

		
		Task<List<JobRefundPaymentOptionLookupTableDto>> GetAllPaymentOptionForTableDropdown();
		
		//Task<List<JobRefundJobLookupTableDto>> GetAllJobForTableDropdown();
		
		Task<List<JobRefundRefundReasonLookupTableDto>> GetAllRefundReasonForTableDropdown();

		Task JobRefundSendSms(int id, int smstempid);

		Task JobRefundSendEmail(int id, int emailtempid);
		Task verifyorNotVerifyJobRefund(EntityDto input, Boolean VerifyOrNot, int SectionId);

	}
}