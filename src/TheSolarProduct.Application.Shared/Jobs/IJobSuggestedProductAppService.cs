﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Jobs.Dtos;

namespace TheSolarProduct.Jobs
{
    public interface IJobSuggestedProductAppService : IApplicationService
    {
        Task CreateOrEdit(CreateOrEditJobSuggestedProductDto input);
        Task<List<JobSuggestedProductDto>> GetAllByJobId(int jobId);
    }
}
