﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Jobs
{
    public interface IElecDistributorsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetElecDistributorForViewDto>> GetAll(GetAllElecDistributorsInput input);

        Task<GetElecDistributorForViewDto> GetElecDistributorForView(int id);

		Task<GetElecDistributorForEditOutput> GetElecDistributorForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditElecDistributorDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetElecDistributorsToExcel(GetAllElecDistributorsForExcelInput input);

		
    }
}