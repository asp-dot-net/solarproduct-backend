﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Jobs
{
    public interface IVariationsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetVariationForViewDto>> GetAll(GetAllVariationsInput input);

        Task<GetVariationForViewDto> GetVariationForView(int id);

		Task<GetVariationForEditOutput> GetVariationForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditVariationDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetVariationsToExcel(GetAllVariationsForExcelInput input);

		
    }
}