﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Jobs
{
    public interface IRoofTypesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRoofTypeForViewDto>> GetAll(GetAllRoofTypesInput input);

        Task<GetRoofTypeForViewDto> GetRoofTypeForView(int id);

		Task<GetRoofTypeForEditOutput> GetRoofTypeForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditRoofTypeDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetRoofTypesToExcel(GetAllRoofTypesForExcelInput input);

		
    }
}