﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using System.Collections.Generic;


namespace TheSolarProduct.Jobs
{
    public interface IJobProductsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetJobProductForViewDto>> GetAll(GetAllJobProductsInput input);

        Task<GetJobProductForViewDto> GetJobProductForView(int id);

		Task<GetJobProductForEditOutput> GetJobProductForEdit(EntityDto input);
		Task<List<GetJobProductForEditOutput>> GetJobProductByJobId(int jobid);
		Task CreateOrEdit(CreateOrEditJobProductDto input);

		Task Delete(EntityDto input);

		
		Task<List<JobProductProductItemLookupTableDto>> GetAllProductItemForTableDropdown();
		Task<List<string>> GetAllProductItemname(string name);
		//Task<List<JobProductJobLookupTableDto>> GetAllJobForTableDropdown();
		
    }
}