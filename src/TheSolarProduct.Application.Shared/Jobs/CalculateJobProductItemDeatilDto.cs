﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs
{
    public class CalculateJobProductItemDeatilDto
    {
        public int ProductItemId { get; set; }

        public string ProductType { get; set; }

        public string ProductItem { get; set; }

        public string Model { get; set; }

        public int? Quantity { get; set; }

        public decimal Amount { get; set; }
    }
}
