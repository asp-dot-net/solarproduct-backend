﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs
{
    public class RefundFormDto
    {
        public string JobNumber { get; set; }

        public string JobStatus { get; set; }

        public string Date { get; set; }

        public string Name { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string MeterPhase { get; set; }

        public string MeterUpgrad { get; set; }

        public string RoofType { get; set; }

        public string PropertyType { get; set; }

        public string RoofPitch { get; set; }

        public string ElecDist { get; set; }

        public string ElecRetailer { get; set; }

        public string NoOfPanels { get; set; }

        public string RefundReason { get; set; }

        public string DepositDate { get; set; }

        public string CusBankName { get; set; }

        public string CusAccNumber { get; set; }

        public string CusAccName { get; set; }

        public decimal RefundAmount { get; set; }

        public string RefundDate { get; set; }

        public string CusBSB { get; set; }

        public string SalesRepName { get; set; }

        public string Manager { get; set; }

        public string ViewHtml { get; set; }
    }
}
