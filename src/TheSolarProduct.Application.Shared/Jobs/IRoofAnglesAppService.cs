﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Jobs
{
    public interface IRoofAnglesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRoofAngleForViewDto>> GetAll(GetAllRoofAnglesInput input);

        Task<GetRoofAngleForViewDto> GetRoofAngleForView(int id);

		Task<GetRoofAngleForEditOutput> GetRoofAngleForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditRoofAngleDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetRoofAnglesToExcel(GetAllRoofAnglesForExcelInput input);

		
    }
}