﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Jobs
{
    public interface IHouseTypesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetHouseTypeForViewDto>> GetAll(GetAllHouseTypesInput input);

        Task<GetHouseTypeForViewDto> GetHouseTypeForView(int id);

		Task<GetHouseTypeForEditOutput> GetHouseTypeForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditHouseTypeDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetHouseTypesToExcel(GetAllHouseTypesForExcelInput input);

		
    }
}