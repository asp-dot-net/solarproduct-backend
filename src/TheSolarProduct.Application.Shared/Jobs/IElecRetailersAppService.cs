﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Jobs
{
    public interface IElecRetailersAppService : IApplicationService 
    {
        Task<PagedResultDto<GetElecRetailerForViewDto>> GetAll(GetAllElecRetailersInput input);

        Task<GetElecRetailerForViewDto> GetElecRetailerForView(int id);

		Task<GetElecRetailerForEditOutput> GetElecRetailerForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditElecRetailerDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetElecRetailersToExcel(GetAllElecRetailersForExcelInput input);

		
    }
}