﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Jobs
{
    public interface IMeterUpgradesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetMeterUpgradeForViewDto>> GetAll(GetAllMeterUpgradesInput input);

        Task<GetMeterUpgradeForViewDto> GetMeterUpgradeForView(int id);

		Task<GetMeterUpgradeForEditOutput> GetMeterUpgradeForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditMeterUpgradeDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetMeterUpgradesToExcel(GetAllMeterUpgradesForExcelInput input);

		
    }
}