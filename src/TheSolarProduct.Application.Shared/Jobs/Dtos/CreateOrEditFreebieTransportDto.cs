﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditFreebieTransportDto : EntityDto<int?>
    {

        [Required]
        public string Name { get; set; }

        public string TransportLink { get; set; }

        public Boolean IsActive { get; set; }
        
    }
}