﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class DisplayNameStatusDto
    {
        public string DisplayName { get; set; }
        public bool IsActive { get; set; }
    }
}
