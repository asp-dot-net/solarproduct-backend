﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using TheSolarProduct.Invoices.Dtos;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetJobForEditOutput
    {
		public CreateOrEditJobDto Job { get; set; }

		public string JobTypeName { get; set;}

		public string JobStatusName { get; set;}

		public string RoofTypeName { get; set;}

		public string RoofAngleName { get; set;}

		public string ElecDistributorName { get; set;}

		public string LeadCompanyName { get; set;}

		public string ElecRetailerName { get; set;}

		public string PaymentOptionName { get; set;}

		public string DepositOptionName { get; set;}

		public string MeterUpgradeName { get; set;}

		public string MeterPhaseName { get; set;}

		public string PromotionOfferName { get; set;}

		public string HouseTypeName { get; set;}

		public string FinanceOptionName { get; set;}

		public decimal? DepPaid { get; set; }

		public decimal? BalanceDue { get; set; }
		public decimal? TotalCost { get; set; }

		public int? AssignToUserID { get; set; }


		public string LeadEmail { get; set; }
		public string state { get; set; }

		public DateTime? FinanceApplicationDates { get; set; }

		public string InstallerEmail { get; set; }

		public string InstallerMobile { get; set; }
		public int? InstallerID { get; set;}

		public virtual int? RefferedJobStatusId { get; set; }

		public virtual int? RefferedJobId { get; set; }

		public string AccountName { get; set; }
		public string BsbNo { get; set; }
		public string AccountNo { get; set; }
		public decimal? ReferralAmount { get; set; }
		public string refferedJobName { get; set; }
		public virtual DateTime? ReferralPayDate { get; set; }
		public virtual bool ReferralPayment { get; set; }
		public string BankReferenceNo { get; set; }
		public string ReferralRemark { get; set; }

		public Boolean displayifdp { get; set; }

		public double WarehouseDistance { get; set; }

		public bool IsStateBatteryRebate { get; set; }

		public bool IsOnHoldEdit { get; set; }
        public  bool IsBroadbandWifiConnection { get; set; }
        public string GWTId { get; set; }
		public decimal ExtraCostValue { get; set; }
        
    }
}