﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class updateremarkinvoiceDto : EntityDto<int?>
    {
        public int TenantId { get; set; }

        public  int? JobId { get; set; }

        public  int? InvTypeId { get; set; }

        public  int? Amount { get; set; }

        public  DateTime? InvDate { get; set; }

        public  int? AdvanceAmount { get; set; }

        public  int? LessDeductAmount { get; set; }

        public  int? TotalAmount { get; set; }

        public  DateTime? AdvancePayDate { get; set; }

        public  DateTime? PayDate { get; set; }

        public  DateTime? PaymentTypeId { get; set; }

        public  int? InstallerId { get; set; }

        public  string Notes { get; set; }

        public  DateTime? Date { get; set; }

        public  string Remarks { get; set; }

        public  bool IsPaid { get; set; }

        public  bool IsVerify { get; set; }

        public  int? PaymentsTypeId { get; set; }
    }
}
