﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditElecRetailerDto : EntityDto<int?>
    {

		public string Name { get; set; }
		
		
		public bool NSW { get; set; }
		
		
		public bool SA { get; set; }
		
		
		public bool QLD { get; set; }
		
		
		public bool VIC { get; set; }
		
		
		public bool WA { get; set; }
		
		
		public bool ACT { get; set; }
		
		
		public bool TAS { get; set; }
		
		
		public bool NT { get; set; }
		
		
		public int? ElectricityProviderId { get; set; }

        public  Boolean IsActive { get; set; }

    }
}