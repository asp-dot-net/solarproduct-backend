﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditJobStatusDto : EntityDto<int?>
    {

		[Required]
		[StringLength(JobStatusConsts.MaxNameLength, MinimumLength = JobStatusConsts.MinNameLength)]
		public string Name { get; set; }
		public Boolean IsActive {  get; set; }
		
		

    }
}