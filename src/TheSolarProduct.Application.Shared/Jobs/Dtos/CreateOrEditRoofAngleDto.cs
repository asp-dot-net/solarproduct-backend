﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditRoofAngleDto : EntityDto<int?>
    {

		[Required]
		[StringLength(RoofAngleConsts.MaxNameLength, MinimumLength = RoofAngleConsts.MinNameLength)]
		public string Name { get; set; }
		
		
		public int? DisplayOrder { get; set; }

        public  Boolean IsActive { get; set; }

    }
}