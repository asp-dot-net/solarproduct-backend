﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Jobs.Dtos
{
    public class JobProductItemProductItemLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
        public int ProductTypeId { get; set; }
        public decimal? Size { get; set; }
        public string Model { get; set; }
    }
}