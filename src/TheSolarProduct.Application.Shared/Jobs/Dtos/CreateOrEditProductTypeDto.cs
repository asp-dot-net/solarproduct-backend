﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditProductTypeDto : EntityDto<int?>
    {

		[Required]
		[StringLength(ProductTypeConsts.MaxNameLength, MinimumLength = ProductTypeConsts.MinNameLength)]
		public string Name { get; set; }
		
		
		public int? DisplayOrder { get; set; }

        public string Img { get; set; }

        public string ImgPath { get; set; }

    }
}