﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetProductItemForEditOutput
    {
		public CreateOrEditProductItemDto ProductItem { get; set; }

		public string ProductTypeName { get; set;}

	}
}