﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetJobProductDetailsForExcel : EntityDto
    {
        public string JobNumber { get; set; }

        public string JobStatus { get; set; }

        public string Team { get; set; }

        public string State { get; set; }

        public decimal? SystemCapacity { get; set; }

        public decimal? Amount { get; set; }

        public string FinanceType { get; set; }

        public string Region { get; set; }

        public string JobType { get; set; }

        public string ProductType { get; set; }

        public string ProductItem { get; set; }

        public string ProductModel { get; set; }

        public int? Quantity { get; set; }

        public List<GetJobProductItemDetailsForExcel> ProductItems { get; set; }

        public string SalesRapName { get; set; }
    }

    public class GetJobProductItemDetailsForExcel : EntityDto
    {
        public string ProductType { get; set; }

        public string ProductItem { get; set; }

        public string ProductModel { get; set; }

        public decimal? ProductSize { get; set; }

        public int? Quantity { get; set; }

        public decimal? betteryKW { get; set; }
    }

    public class GetJobProductItemsForExcel : EntityDto
    {
        public string JobNumber { get; set; }

        public string ProductType { get; set; }

        public string ProductItem { get; set; }

        public string ProductModel { get; set; }

        public decimal? ProductSize { get; set; }

        public int? Quantity { get; set; }

        public string State { get; set; }

        public string Teams { get; set; }
    }
}
