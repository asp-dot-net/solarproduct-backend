﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetDetailForJobEditOutPut
    {
        public DateTime? DistApplied { get; set; }

        public string ApprovalRef { get; set; }

        public int? DistApproveBy { get; set; }

        public DateTime? DistExpiryDate { get; set; }

        public int? AppliedBy { get; set; }

        public DateTime? DistApproveDate { get; set; }

        public int? EmpId { get; set; }

        public decimal? PaidAmmount { get; set; }

        public int? Paidby { get; set; }

        public int? PaidStatus { get; set; }

        public string InvRefNo { get; set; }

        public DateTime? InvPaidDate { get; set; }

        public string ProjectNotes { get; set; }

        public string ApplicationNotes { get; set; }

        public DateTime? DocumentsVeridiedDate { get; set; }

        public int? DocumentsVeridiedBy { get; set; }
               
        public DateTime? CoolingoffPeriodEnd { get; set; }
        
        public DateTime?  ExpiryDate { get; set; }

        public DateTime? ApprovalDate { get; set; }

        public DateTime?  FinanceApplicationDate { get; set; }
        
        public  DateTime? StcUploaddate { get; set; }

        public  string StcUploadNumber { get; set; }

        public  int? PvdStatus { get; set; }

        public  DateTime? StcAppliedDate { get; set; }

        public  string PvdNumber { get; set; }

        public  int? Quickformid { get; set; }

        public  string StcNotes { get; set; }

        public  TimeSpan? MeterTime { get; set; }

        public  string MeterApplyRef { get; set; }

        public  string InspectorName { get; set; }

        public  DateTime? InspectionDate { get; set; }
        
        public  string FinancePurchaseNo { get; set; }

        /// <summary>
        /// 1 = Verify
        /// 2 = Not Verify
        /// 3 = Query
        /// </summary>
        public int? FinanceDocumentVerified { get; set; }
        
        public string FinanceNotes { get; set; }
        
        public string  ApplicationRefNo { get; set; }
        
        public DateTime?  ActiveDate { get; set; }
        
        public string DocumentsVeridiedByName { get; set; }
        
        public string PvdStatusName { get; set; }
        
        public string DistApproveByName { get; set; }
        
        public string EployeeByName { get; set; }
        
        public string ApplyByName { get; set; }
        
        public CreateOrEditJobDto Job { get; set; }

		public List<DetailJobPromotionDto> JobPromotion { get; set; }
		
        public List<CreateOrEditJobRefundDto> Refund { get; set; }
		
        public List<DetailDto> InvoicePayment { get; set; }
	}
}
