﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Jobs.Dtos
{
    public class JobVariationVariationLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }

        public string ActionName { get; set; }
    }
}