﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class DisplayNameStatusResultDto
    {
        public List<DisplayNameStatusDto> DepositStatuses { get; set; }
        public List<DisplayNameStatusDto> ActiveStatuses { get; set; }
        public List<DisplayNameStatusDto> ApplicationStatuses { get; set; }
    }
}
