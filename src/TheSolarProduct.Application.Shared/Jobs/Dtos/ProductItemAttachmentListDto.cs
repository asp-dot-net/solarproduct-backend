﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
	public class ProductItemAttachmentListDto
	{
		public string FileName { get; set; }

		public string FilePath { get; set; }
	}
}
