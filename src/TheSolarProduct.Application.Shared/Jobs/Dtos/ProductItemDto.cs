﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.Jobs.Dtos
{
	public class ProductItemDto : EntityDto
	{
		public string Name { get; set; }

		public string Manufacturer { get; set; }

		public string Model { get; set; }

		public string Series { get; set; }

		public decimal? Size { get; set; }

		public string ShortName { get; set; }

		public string Description { get; set; }

		public int? Code { get; set; }

		public int? StockQuantity { get; set; }

		public int? MinLevel { get; set; }

		public decimal? CostPrice { get; set; }

		public string ACPower { get; set; }

		public string StockItem { get; set; }

		public string StockId { get; set; }

		public int ProductTypeId { get; set; }

		public string FileName { get; set; }

		public string FilePath { get; set; }

		public DateTime? ExpiryDate { get ; set;}
		public string  Status { get; set; }

		public string Image { get; set; }

	}
}