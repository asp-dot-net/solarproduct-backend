﻿using System;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetAllJobPromotionsForExcelInput
    {
		public string Filter { get; set; }

		public string ProjectNumber { get; set; }

		public string CompanyName { get; set; }

		public string MobileNo { get; set; }

		public string Email { get; set; }

		public string Address { get; set; }

		public int? PromoType { get; set; }

		public int? OrganizationUnit { get; set; }

		public int? applicationstatus { get; set; }

		public int? FreebieTransportId { get; set; }

		public int? SalesManagerId { get; set; }

		public int? SalesRepId { get; set; }

		public DateTime? StartDate { get; set; }

		public DateTime? EndDate { get; set; }
		public int? excelorcsv { get; set; }
		public string stateNameFilter { get; set; }

	}
}