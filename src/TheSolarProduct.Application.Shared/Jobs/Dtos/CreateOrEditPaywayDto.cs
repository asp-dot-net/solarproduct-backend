﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
   public class CreateOrEditPaywayDto
    {
        public int jobId { get; set; }
        public string transactionId { get; set; }
        public string receiptNumber { get; set; }
        public string status { get; set; }
        public string responseCode { get; set; }
        public string responseText { get; set; }
        public string transactionType { get; set; }
        public string customerNumber { get; set; }
        public string customerName { get; set; }
        public string currency { get; set; }
        public decimal principalAmount { get; set; }
        public decimal surchargeAmount { get; set; }
        public decimal paymentAmount { get; set; }
        public string paymentMethod { get; set; }


    }
    public class westpack
    { 
            public string receiptNumber { get; set; }
        public PrincipalAmount principalAmount { get; set; }
        public surchargeAmount surchargeAmount { get; set; }
        public totalAmount totalAmount { get; set; }

        public string status { get; set; }
             public string responseCode { get; set; }
             public string responseDescription { get; set; }
             public string summaryCode { get; set; }
             public string transactionType { get; set; }
             public string fraudGuardResult { get; set; }
             public string transactionTime { get; set; }
             public string settlementDate { get; set; }
             public string source { get; set; }
             public string customerReferenceNumber { get; set; }
             public string paymentReferenceNumber { get; set; }
             public string user { get; set; }
             public string voidable { get; set; }
             public string refundable { get; set; }
             public string comment { get; set; }
             public string ipAddress { get; set; }
             
   
}
    public class PrincipalAmount
    {
        public string currency { get; set; }
        public string amount { get; set; }
        public string displayAmount{ get; set; }
}
    public class surchargeAmount
    {
        public string currency { get; set; }
        public string amount { get; set; }
        public string displayAmount { get; set; }
    }
    public class totalAmount {
        public string currency { get; set; }
    public string amount { get; set; }
    public string displayAmount { get; set; }
}
}
