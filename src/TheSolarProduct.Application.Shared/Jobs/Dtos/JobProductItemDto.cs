﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.Jobs.Dtos
{
    public class JobProductItemDto : EntityDto
    {

		 public int? JobId { get; set; }

		 public int? ProductItemId { get; set; }
    }
}