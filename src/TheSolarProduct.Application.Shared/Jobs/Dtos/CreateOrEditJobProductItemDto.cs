﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
	public class CreateOrEditJobProductItemDto : EntityDto<int?>
	{
		public int? Quantity { get; set; }

		public int? JobId { get; set; }

		public string ProductItemName { get; set; }

		public int? ProductItemId { get; set; }

		public int? ProductTypeId { get; set; }

		public decimal? Size { get; set; }

		public string RefNo { get; set; }

		public int? Stock { get; set; }


    }
}