﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Jobs.Dtos
{
    public class JobPromotionJobLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}