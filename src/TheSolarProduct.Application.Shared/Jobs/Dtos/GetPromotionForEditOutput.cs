﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetPromotionForEditOutput
    {
		public CreateOrEditPromotionOfferDto Promotion { get; set; }


    }
}