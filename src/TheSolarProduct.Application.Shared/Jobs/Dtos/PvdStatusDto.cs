﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
  public  class PvdStatusDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }
    }

}
