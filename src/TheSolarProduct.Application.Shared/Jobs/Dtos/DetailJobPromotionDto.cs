﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class DetailJobPromotionDto
    {
		public int JobId { get; set; }

		public int? PromotionMasterId { get; set; }

		public string TrackingNumber { get; set; }

		public string Description { get; set; }

		public DateTime? DispatchedDate { get; set; }

		public int? FreebieTransportId { get; set; }

		public bool? SmsSend { get; set; }

		public string Name { get; set; }

	}
}
