﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class EssentialTrackerActiveStatusDto
    {
        public Boolean EssentialGreenBox { get; set; }
        public Boolean ElectricPoleImg { get; set; }
        public Boolean MeterBox { get; set; }
        public Boolean ElectricityBill { get; set; }
    }
}
