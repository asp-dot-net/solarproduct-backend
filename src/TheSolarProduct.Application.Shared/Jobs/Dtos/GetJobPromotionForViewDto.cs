﻿using System;

namespace TheSolarProduct.Jobs.Dtos
{
	public class GetJobPromotionForViewDto
	{
		public JobPromotionDto JobPromotion { get; set; }

		public string CompanyName { get; set; }

		public string Email { get; set; }

		public string Phone { get; set; }

		public string Mobile { get; set; }

		public string Address { get; set; }
		
		public string Suburb { get; set; }
		
		public string State { get; set; }
		
		public string PostCode { get; set; }
		
		public string JobNumber { get; set; }
		
		public string ProjectStatus { get; set; }
		
		public decimal? BalQwing { get; set; }

		public string PromotionMasterName { get; set; }

		public string FreebieTransportName { get; set; }

		public bool? SmsSend { get; set; }

		public string CurrentLeadOwaner { get; set; }
		public int? LeadId { get; set; }
		public DateTime? ActivityReminderTime { get; set; }
		public string ActivityDescription { get; set; }
		public string ActivityComment { get; set; }
	}
}