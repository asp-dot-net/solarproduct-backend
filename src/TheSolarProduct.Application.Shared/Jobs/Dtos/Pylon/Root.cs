﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace TheSolarProduct.Jobs.Dtos.Pylon
{
    public class Acceptance
    {
        public bool is_accepted { get; set; }
        public bool manually_sold { get; set; }
        public bool latest_esignature { get; set; }
    }

    public class Attributes
    {
        public string reference_number { get; set; }
        public List<double> site_location { get; set; }
        public SiteAddress site_address { get; set; }
        public CustomerDetails customer_details { get; set; }
        public Acceptance acceptance { get; set; }
        public bool is_archived { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }

    public class CustomerDetails
    {
        public string name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
    }

    public class Data
    {
        public string type { get; set; }
        public string id { get; set; }
        public Attributes attributes { get; set; }
        public Relationships relationships { get; set; }
        public Links links { get; set; }
    }

    public class Designs
    {
        public List<Data> data { get; set; }
        public Links links { get; set; }
    }

    public class Links
    {
        public string related { get; set; }
        public string self { get; set; }
        public string first { get; set; }
        public object prev { get; set; }
        public object next { get; set; }
    }

    public class Meta
    {
        public Page page { get; set; }
    }

    public class Opportunities
    {
        public List<object> data { get; set; }
        public Links links { get; set; }
    }

    public class Owner
    {
        public Data data { get; set; }
        public Links links { get; set; }
    }

    public class Page
    {
        public int current { get; set; }
        public int size { get; set; }
        public int count { get; set; }
    }

    public class PrimaryDesign
    {
        public Data data { get; set; }
        public Links links { get; set; }
    }

    public class Relationships
    {
        public Owner owner { get; set; }
        public Designs designs { get; set; }
        public PrimaryDesign primary_design { get; set; }
        public Opportunities opportunities { get; set; }
    }

    public class Root
    {
        public List<Data> data { get; set; }
        public Links links { get; set; }
        public Meta meta { get; set; }
    }

    public class SiteAddress
    {
        public string line1 { get; set; }
        public object line2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
    }

    public class SAttributes
    {
        public SSummary summary { get; set; }
        public DateTime created_at { get; set; }
    }

    public class SData
    {
        public string type { get; set; }
        public string id { get; set; }
        public SAttributes attributes { get; set; }
        public SRelationships relationships { get; set; }
        public SLinks links { get; set; }
    }

    public class SLinks
    {
        public string self { get; set; }
    }

    public class SRelationships
    {
    }

    public class SRoot
    {
        public SData data { get; set; }
    }

    public class SSummary
    {
        public double dc_output_kw { get; set; }
        public int storage_kwh { get; set; }
        public string description { get; set; }
        public string web_proposal_url { get; set; }
        public string pdf_proposal_url { get; set; }
        public string latest_snapshot_url { get; set; }
    }
}
