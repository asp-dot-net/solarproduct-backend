﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
	public class CreateOrEditJobOdSysDetails : EntityDto<int?>
	{
		public int? Quantity { get; set; }

		public int? JobId { get; set; }

		public string Name { get; set; }
	}
}
