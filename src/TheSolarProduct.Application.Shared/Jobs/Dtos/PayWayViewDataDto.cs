﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class PayWayViewDataDto : EntityDto
    {
        
        public AllPayWayDatasDto payway { get; set; }
        public decimal? TotalInvoicePayment { get; set; }
        public decimal? AmmountRcv { get; set; }
    }
}
