﻿namespace TheSolarProduct.Jobs.Dtos
{
    public class GetJobTypeForViewDto
    {
		public JobTypeDto JobType { get; set; }
    }
}