﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetVariationForEditOutput
    {
		public CreateOrEditVariationDto Variation { get; set; }


    }
}