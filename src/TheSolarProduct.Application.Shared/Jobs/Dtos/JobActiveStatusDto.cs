﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class JobActiveStatusDto
    {
        public Boolean NMINumber { get; set; }
        public Boolean ApprovedReferenceNumber { get; set; }
        public Boolean MeterUpdgrade { get; set; }
        public Boolean MeterBoxPhoto { get; set; }
        public Boolean SignedQuote { get; set; }
        public Boolean QuoteAcceptDate { get; set; }
        public Boolean Deposite { get; set; }
        public Boolean Finance { get; set; }
        public int? payementoptionid { get; set; }


        public Boolean HouseTypeId { get; set; }
        public Boolean ElecDistributorId { get; set; }

        public Boolean RoofAngleId { get; set; }
        public Boolean RoofTypeId { get; set; }
        public Boolean InstallerNotes { get; set; }
        public Boolean Quote { get; set; }
        public Boolean Invoice { get; set; }
        public Boolean ProductDetail { get; set; }

        public int? JobStatusId { get; set; }

        public Boolean MeterPhadeIDs { get; set; }
        public Boolean PeakMeterNos { get; set; }
        public Boolean DocList { get; set; }
        public Boolean elecRetailerId { get; set; }
        public Boolean ExpiryDate { get; set; }
        public Boolean SolarRebateStatus { get; set; }

        public Boolean DistApproved { get; set; }

        public string VicRebate { get; set; }
        public string State { get; set; }

        public Boolean paymentType { get; set; }

        public Boolean PaymentVerified { get; set;}

        public Boolean QuoteSigned { get; set; }

        public Boolean ExportControlFormSigned { get; set; }

        public Boolean FeedInTariffSigned { get; set; }

        public Boolean ShadingDeclarationSigned { get; set; }

        public Boolean EfficiencyDeclarationSigned { get; set; }

        public Boolean ExportControlForm { get; set; }

        public Boolean FeedInTariff { get; set; }

        public Boolean ShadingDeclaration { get; set; }

        public Boolean EfficiencyDeclaration { get; set; }

    }
}
