﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetAllProductTypesForExcelInput
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }



    }
}