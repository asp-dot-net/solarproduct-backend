﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Jobs.Dtos
{
    public class JobPromotionPromotionLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}