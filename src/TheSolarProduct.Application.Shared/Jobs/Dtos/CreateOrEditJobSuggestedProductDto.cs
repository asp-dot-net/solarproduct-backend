﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditJobSuggestedProductDto : EntityDto<int?>
    {
        public virtual int? JobId { get; set; }
        public virtual string Name { get; set; }
        public virtual decimal? SysCapacity { get; set; }
        public virtual decimal? Amount { get; set; }
        public List<JobSuggestedProductItemDto> JobSuggestedProductItemDto { get; set; }
    }
}
