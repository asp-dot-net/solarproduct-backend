﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetActualCostInput
    {
        public int LeadId { get; set; }

        public int JobId { get; set; }

        public string AreaName { get; set; }

        public string StateName { get; set; }

        public decimal? SystemKw { get; set; }

        public List<GetActualCostItemListInput> ProductItemList { get; set; }

        public int? HouseType { get; set; }

        public int? RoofType { get; set; }

        public int? RoofAngle { get; set; }

        public List<int?> VariationList { get; set; }

        public int? STC { get; set; }

        public decimal? Distance { get; set; }

        public int? FinPaymentType { get; set; }

        public int? TotalCost { get; set; }

        public int? CategoryDDiscountAmt { get; set; }
    }

    public class GetActualCostItemListInput
    {
        public int? ProductTypeId { get; set; }

        public int? ProductItemId { get; set; }

        public decimal? Size { get; set; }

        public int? Qty { get; set; }
    }

    public class GetActualCostOutput
    {
        public decimal ActualCost { get; set; }

        public string Category { get; set; }

        public decimal PanelCost { get; set; }

        public decimal PricePerWatt { get; set; }

        public decimal InverterCost { get; set; }

        public decimal BatteryCost { get; set; }

        public decimal InstallationCost { get; set; }

        public decimal BetteryInstallationCost { get; set; }

        public string Datetype { get; set; }
    }
}
