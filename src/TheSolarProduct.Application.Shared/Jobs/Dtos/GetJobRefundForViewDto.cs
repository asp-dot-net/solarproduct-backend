﻿using System;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetJobRefundForViewDto
    {
		public JobRefundDto JobRefund { get; set; }

		public string PaymentOptionName { get; set;}

		public string PaidBy { get; set; }

		public string JobNote { get; set;}

		public string ProjectName { get; set; }
		public string CompanyName { get; set; }
		public string ProjectStatus { get; set; }

		public string PreviousProjectStatus { get; set; }

		public string RefundReasonName { get; set;}

		public string CurrentLeadOwaner { get; set; }

		public string RequestedBy { get; set; }
		public Boolean? verifyornot { get; set; }
	    public int? LeadId { get; set; }
		public DateTime? ActivityReminderTime { get; set; }
		public string ActivityDescription { get; set; }
		public string ActivityComment { get; set; }

		public Boolean? IsVerify { get; set; }

	}
}