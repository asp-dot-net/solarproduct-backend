﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditVariationDto : EntityDto<int?>
    {

		public string Name { get; set; }
		
		
		[Required]
		[StringLength(VariationConsts.MaxActionLength, MinimumLength = VariationConsts.MinActionLength)]
		public string Action { get; set; }

        public  Boolean IsActive { get; set; }

    }
}