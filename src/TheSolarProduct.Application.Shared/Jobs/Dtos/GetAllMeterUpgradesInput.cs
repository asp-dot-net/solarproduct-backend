﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetAllMeterUpgradesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }



    }
}