﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditImportDto : EntityDto<int?>
    {
        public int TenantId { get; set; }
        public DateTime? Date { get; set; }
        public string JobNumber { get; set; }
        public int? JobId { get; set; }
        public string InvoicePaymentmothodName { get; set; }
        public int? InvoicePaymentMethodId { get; set; }
        public string Description { get; set; }
        public Decimal? PaidAmmount { get; set; }
        public Decimal? SSCharge { get; set; }
        public string AllotedBy { get; set; }

        public string ReceiptNumber { get; set; }
        public string PurchaseNumber { get; set; }
        public string InvoiceNotesDescription { get; set; }
        public int? SectionId { get; set; }

    }
}
