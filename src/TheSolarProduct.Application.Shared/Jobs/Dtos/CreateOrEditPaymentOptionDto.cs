﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditPaymentOptionDto : EntityDto<int?>
    {

		public string Name { get; set; }
				
		public int? DisplayOrder { get; set; }

        public decimal? EstablishmentFee { get; set; }

        public decimal? MonthlyAccFee { get; set; }
        public Boolean IsActive {  get; set; }
    }
}