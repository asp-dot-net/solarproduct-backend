﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditElecDistributorDto : EntityDto<int?>
    {

		[Required]
		[StringLength(ElecDistributorConsts.MaxNameLength, MinimumLength = ElecDistributorConsts.MinNameLength)]
		public string Name { get; set; }
		
		
		public int? Seq { get; set; }
		
		
		public bool NSW { get; set; }
		
		
		public bool SA { get; set; }
		
		
		public bool QLD { get; set; }
		
		
		public bool VIC { get; set; }
		
		
		public bool WA { get; set; }
		
		
		public bool ACT { get; set; }
		
		
		public string TAS { get; set; }
		
		
		public bool NT { get; set; }
		
		
		public string ElectDistABB { get; set; }
		
		
		public bool ElecDistAppReq { get; set; }
		
		
		public int? GreenBoatDistributor { get; set; }

        public  Boolean IsActive { get; set; }

    }
}