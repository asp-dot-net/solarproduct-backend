﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;

namespace TheSolarProduct.Jobs.Dtos
{
	public class GetAllJobRefundsInput : PagedAndSortedResultRequestDto
	{
		public string Filter { get; set; }

		public int? MaxAmountFilter { get; set; }

		public int? MinAmountFilter { get; set; }

		public string BankNameFilter { get; set; }

		public string AccountNameFilter { get; set; }

		public string BSBNoFilter { get; set; }

		public string AccountNoFilter { get; set; }

		public DateTime? MaxPaidDateFilter { get; set; }

		public DateTime? MinPaidDateFilter { get; set; }

		public string PaymentOptionNameFilter { get; set; }

		public string JobNoteFilter { get; set; }

		public string RefundReasonNameFilter { get; set; }

		public int? OrganizationUnit { get; set; }

		public int? applicationstatus { get; set; }

		public int? SalesManagerId { get; set; }

		public int? SalesRepId { get; set; }
		public List<int> JobStatusID { get; set; }
		public int? JobRefundID { get; set; }
	}
}