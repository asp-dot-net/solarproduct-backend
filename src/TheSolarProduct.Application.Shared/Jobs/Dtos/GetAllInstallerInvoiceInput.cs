﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetAllInstallerInvoiceInput : PagedAndSortedResultRequestDto
    {
        public string FilterName { get; set; }

        public int? OrganizationUnit { get; set; }

        public int? InvoiceStatus { get; set; }

        public string InvoiceType { get; set; }

        public string Filter { get; set; }

        public int InstallerId { get; set; }

        public string Suburb { get; set; }

        public string State { get; set; }

        public string DateFilter { get; set; }

        public DateTime? StartDate { get; set; }
        
        public DateTime? EndDate { get; set; }
    }

    public class GetAllInstallerInvoiceExcelInput
    {
        public string FilterName { get; set; }

        public int? OrganizationUnit { get; set; }

        public int? InvoiceStatus { get; set; }

        public string InvoiceType { get; set; }

        public string Filter { get; set; }

        public int InstallerId { get; set; }

        public string Suburb { get; set; }

        public string State { get; set; }

        public string DateFilter { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? ExcelOrCsv { get; set; }

    }
}
