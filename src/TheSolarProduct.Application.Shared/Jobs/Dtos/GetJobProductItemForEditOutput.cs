﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetJobProductItemForEditOutput
    {
		public CreateOrEditJobProductItemDto JobProductItem { get; set; }

		public string JobNote { get; set;}

		public string ProductItemName { get; set;}

        public decimal? Size { get; set; }

        public string Model { get; set; }
        public int? Quantity { get; set; }


        public int? ID { get; set; }

        public int Stock { get; set; }

        public string DatasheetUrl { get; set; }
    }
}