﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditRefundReasonDto : EntityDto<int?>
    {

        public string Name { get; set; }
        public  Boolean IsActive { get; set; }
    }
}