﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class InstallerPaymentJobNumberWiseInputDto
    {
        public string filter { get; set; }

        public int? orgId { get; set; }

       

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Areafilter { get; set; }

        public string InvoiceType { get; set; }
        public string state { get; set; }

    }
}
