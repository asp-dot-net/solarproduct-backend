﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
  public  class ReferralInputDto : PagedAndSortedResultRequestDto
	{

        public string FilterName { get; set; }

        public string Filter { get; set; }

        public int OrganizationUnit { get; set; }

        public int PaymentStatus { get; set; }

        public string State { get; set; }

        public int SalesRep { get; set; }

        public string DateType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? excelorcsvfile { get; set; }
    }
}
