﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.ServiceDocumentRequests.Dtos;

namespace TheSolarProduct.Jobs.Dtos
{
	public class SmsEmailDto : EntityDto
	{

		public int? LeadId { get; set; }
		public int? ID { get; set; }

		public int? EmailTemplateId { get; set; }

		public int? SMSTemplateId { get; set; }

		public string Body { get; set; }

		public int? TrackerId { get; set; }

		public int? CustomeTagsId { get; set; }

		public string activityName { get; set; }

		public string Subject { get; set; }

		public string EmailTo { get; set; }
		public string EmailFrom { get; set; }
		public string cc { get; set; }
		public string Bcc { get; set; }

		public string SignedQuotedDoc { get; set; }
		public string SignedQuotedDocPath { get; set; }
		public string UserManualDoc { get; set; }
		public string UserManualDocPath { get; set; }
		public string PanelBrocherDoc { get; set; }
		public string PanelBrocherDocPath { get; set; }
		public string InverterBrocherDoc { get; set; }
		public string InverterBrocherDocPath { get; set; }
		public string InverterWarrantyDoc { get; set; }
		public string InverterWarrantyDocPath { get; set; }
		public string PanelWarrantyDoc { get; set; }
		public string PanelWarrantyDocPath { get; set; }
		public string TaxInvoiceFileName { get; set; }
		public string TaxInvoiceDocPath { get; set; }
		public int MyInstallerId { get; set; }

       public List<ServiceDocumnetLookupDto> Documents { get; set; }

        public int? ServiceId { get; set; }


    }
}
