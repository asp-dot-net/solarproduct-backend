﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.Jobs.Dtos
{
    public class PromotionMasterDto : EntityDto
    {
		public string Name { get; set; }

        public int? DisplayOrder { get; set; }
        public  Boolean IsActive { get; set; }
    }
}