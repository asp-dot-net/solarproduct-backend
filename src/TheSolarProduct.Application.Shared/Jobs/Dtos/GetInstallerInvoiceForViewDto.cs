﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetInstallerInvoiceForViewDto
    {
        public int Id { get; set; }

        public int JobId { get; set; }

        public int LeadId { get; set; }

        public bool IsVerify { get; set; }

        public bool PaidStatus { get; set; }

        public string JobNumber { get; set; }

        public string State { get; set; }

        public string InstallerName { get; set; }

        public string InstallerCompanyName { get; set; }

        public string Installation_Maintenance_Inspection { get; set; }

        public int? InvNo { get; set; }

        public decimal? Amount { get; set; }

        public DateTime? InvDate { get; set; }

        public DateTime? InvoiceIssuedDate { get; set; }

        public DateTime? InstalledCompleteDate { get; set; }

        public decimal? TotalCost { get; set; }

        public decimal? OwningAmt { get; set; }

        public string RemarkIfOwing { get; set; }

        public string FinanceName { get; set; }

        public bool? GridConnection { get; set; }

        public string NotesOrReasonforPending { get; set; }

        public string ActivityComment { get; set; }

        public string ActivityReminderTime { get; set; }
        
        public string ActivityDescription { get; set; }
        
        public InstallerSummaryCountDto SummaryCount { get; set; }
        
        public string InvoiceNo { get; set; }
    }

    public class InstallerSummaryCountDto
    {
        public int Total { get; set; }

        public int Pending { get; set; }

        public int Verified { get; set; }
    }

    public class GetInstallerInvoiceForExportDto : EntityDto
    {

        public int JobId { get; set; }

        public int LeadId { get; set; }

        public bool IsVerify { get; set; }

        public bool PaidStatus { get; set; }

        public string JobNumber { get; set; }

        public string State { get; set; }

        public string InstallerName { get; set; }

        public string InstallerCompanyName { get; set; }

        public string Installation_Maintenance_Inspection { get; set; }

        public int? InvNo { get; set; }

        public decimal? Amount { get; set; }

        public DateTime? InvDate { get; set; }

        public DateTime? InvoiceIssuedDate { get; set; }

        public DateTime? InstalledCompleteDate { get; set; }

        public decimal? TotalCost { get; set; }

        public decimal? OwningAmt { get; set; }

        public string RemarkIfOwing { get; set; }

        public string FinanceName { get; set; }

        public bool? GridConnection { get; set; }

        public string NotesOrReasonforPending { get; set; }

        public string ActivityComment { get; set; }

        public string ActivityReminderTime { get; set; }

        public string ActivityDescription { get; set; }

        public string InvoiceNo { get; set; }
    }
}
