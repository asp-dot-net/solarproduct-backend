﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.Jobs.Dtos
{
    public class JobStatusDto : EntityDto
    {
		public string Name { get; set; }

        public Boolean IsActive { get; set; }

    }
}