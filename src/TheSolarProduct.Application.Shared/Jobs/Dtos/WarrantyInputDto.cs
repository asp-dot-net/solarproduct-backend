﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
  public  class WarrantyInputDto : PagedAndSortedResultRequestDto
    {
      

        public string FilterName { get; set; }
        public string DateFilterType { get; set; }
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? OrganizationUnit { get; set; }
        public string InvoiceType { get; set; }
        public string InstallerId { get; set; }
        public string Filter { get; set; }
        public int? PaymentStatus { get; set; }
        public int? documentStatus { get; set; }
        public string StateId { get; set; }
        public int? SalesRepId { get; set; }
    }
}
