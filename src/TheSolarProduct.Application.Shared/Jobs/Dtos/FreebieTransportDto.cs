﻿using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.Jobs.Dtos
{
    public class FreebieTransportDto : EntityDto
    {
        public string Name { get; set; }
        public string TransportLink { get; set; }

        public Boolean IsActive {  get; set; }
        

    }
}