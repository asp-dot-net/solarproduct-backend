﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
	public class InstallerPaidDto : EntityDto
	{
		public int JobId { get; set; }

		public int LeadId { get; set; }

		public string JobNumber { get; set; }

		public string Customer { get; set; }

		public string Address { get; set; }

		public string PCode { get; set; }

		public DateTime? InstallDate { get; set; }

		public int? InvNo { get; set; }

        public DateTime? InvDate { get; set; }

        public decimal? Ammount { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public decimal? ApprovedAmount { get; set; }

        public string ApprovedNotes { get; set; }

        public string BankRefNo { get; set; }

        public bool IsPayment { get; set; }

        public DateTime? DueDate { get; set; }

        public string FileName { get; set; }

        public InstallerPaidSummaryCountDto SummaryCount { get; set; }
        
        public string InvoiceNo { get; set; }
    }

    public class InstallerPaidSummaryCountDto
    {
        public int Total { get; set; }

        public int Paid { get; set; }

        public int UnPaid { get; set; }

        public decimal? TotalMetroKw { get; set; }

        public decimal? TotalMetroPrice { get; set;}

        public decimal? MetroPricePerKw { get; set; }

        public decimal? TotalRegionalKw { get; set; }

        public decimal? TotalRegionalPrice { get; set;}

        public decimal? RegionalPricePerKw { get; set; }




        public int? TotalInstallation { get; set; }

        public int? PaidInstallation { get; set; }

        public int? UnPaidInstallation { get; set; }

        public decimal? TotalMetroKwInstallation { get; set; }

        public decimal? TotalMetroPriceInstallation { get; set; }

        public decimal? MetroPricePerKwInstallation { get; set; }

        public decimal? TotalRegionalKwInstallation { get; set; }

        public decimal? TotalRegionalPriceInstallation { get; set; }

        public decimal? RegionalPricePerKwInstallation { get; set;}



        public int? TotalMaintenance { get; set; }

        public int? PaidMaintenance { get; set; }

        public int? UnPaidMaintenance { get; set; }

        public decimal? TotalMetroKwMaintenance { get; set; }

        public decimal? TotalMetroPriceMaintenance { get; set; }

        public decimal? MetroPricePerKwMaintenance { get; set; }

        public decimal? TotalRegionalKwMaintenance { get; set; }

        public decimal? TotalRegionalPriceMaintenance { get; set; }

        public decimal? RegionalPricePerKwMaintenance { get; set; }




        public int? TotalInspection { get; set; }

        public int? PaidInspection { get; set; }

        public int? UnPaidInspection { get; set; }

        public decimal? TotalMetroKwInspection { get; set; }

        public decimal? TotalMetroPriceInspection { get; set; }

        public decimal? MetroPricePerKwInspection { get; set; }

        public decimal? TotalRegionalKwInspection { get; set; }

        public decimal? TotalRegionalPriceInspection { get; set; }

        public decimal? RegionalPricePerKwInspection { get; set; }
    }

    public class InstallerPaidExcelDto : EntityDto
    {
        public int JobId { get; set; }

        public int LeadId { get; set; }

        public string JobNumber { get; set; }

        public string Customer { get; set; }

        public string Address { get; set; }

        public string PCode { get; set; }

        public DateTime? InstallDate { get; set; }

        public string InvoiceNo { get; set; }
        public string InvoiceType{ get; set; }

        public DateTime? InvDate { get; set; }

        public DateTime? DueDate { get; set; }

        public decimal? Amount { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public decimal? ApprovedAmount { get; set; }

        public string ApprovedNotes { get; set; }

        public string BankRefNo { get; set; }

        public string Area { get; set; }

        public string State { get; set; }

        public string InstallerName { get; set; }

        public decimal? SysPrice { get; set; }

        public decimal? SysCapacity { get; set; }

    }
}
