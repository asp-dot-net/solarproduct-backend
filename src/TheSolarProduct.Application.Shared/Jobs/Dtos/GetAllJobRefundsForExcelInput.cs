﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetAllJobRefundsForExcelInput
    {
        public string FilterName { get; set; }

        public string Filter { get; set; }

        public int OrganizationUnit { get; set; }

        public int ApplicationStatus { get; set; }

        public int SalesRep { get; set; }

        public List<int> JobStatus { get; set; }

        public int RefundReason { get; set; }

        public string DateFilter { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public bool? IsVerify { get; set; }

        public string RefundPaymentType { get; set; }

        public int? excelorcsv { get; set; }

        public string StateFilter { get; set; }

        public string AreaNameFilter { get; set; }
    }
}