﻿namespace TheSolarProduct.Jobs.Dtos
{
    public class GetRefundReasonForViewDto
    {
        public RefundReasonDto RefundReason { get; set; }

    }
}