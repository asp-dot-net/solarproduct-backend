﻿using System;

namespace TheSolarProduct.Jobs.Dtos
{
	public class GetJobForViewDto
	{
		public JobDto Job { get; set; }

		public string JobTypeName { get; set; }

		public string JobStatusName { get; set; }

		public string LeadCompanyName { get; set; }
	    public string ElecDistributorName { get; set; }

		public string Email { get; set; }

		public string ElecRetailerName { get; set; }

		public string Mobile { get; set; }

		public string LastComment { get; set; }

		public DateTime? LastCommentDate { get; set; }

		public string LeadSourceName { get; set; }


		public bool MeterPhadeIDs { get; set; }

		public bool DocLists { get;set;}
		public bool Nminumbers { get; set; }
		public bool  PeakMeterNos { get; set; }
		public bool ExpiryDate { get; set; }
		public bool SolarRebateStatus { get; set; }

		public string State { get; set; }
		public bool NMIs { get; set; }
		public bool ApprovedReferenceNumbers { get; set; }
		public bool MeterUpdgrades { get; set; }
		public bool MeterBoxPhotos { get; set; }
		public bool SignedQuotes { get; set; }
		public bool Deposites { get; set; }
		public bool QuoteAcceptDates { get; set; }
		public bool Finances { get; set; }
		public bool payementoptionids { get; set; }

		public decimal? TotalSystemCapacity { get; set; }
		public decimal? TotalCost { get; set; }
		public decimal? Price { get; set; }
		public int? DepositeReceived { get; set; }
		public int? Active { get; set; }
		public int? InstallJob { get; set; }
		public int? TotalNoOfPanel { get; set; }
		public int? TotalNoOfInvert { get; set; }
		public string Team { get; set; }
		public int? DateDiff { get; set; }
		public DateTime? LeadCreationDate { get; set; }

		public string Panels { get; set; }
		public string Inverters { get; set; }
		public string RoofType { get; set; }
		public string HouseType { get; set; }
		public string FinanceWith { get; set; }

		public string LeadStatus { get; set; }

		public DateTime? FirstDepDate { get; set; }
	public decimal? FirstDepAmmount { get; set; }
		public string JobAssignemployee { get; set; }
		public DateTime? JobAssignDate { get; set; }
		public int? TotalData { get; set; }
		public int? TotalDepRcvData { get; set; }
		public int? TotalActiveData { get; set; }
		public int? TotalJobBookedData { get; set; }
        public decimal? SystemCapacity { get; set; }

		public DateTime? ActivityReminderTime { get; set; }
		public string ReminderTime { get; set; }
		public string ActivityDescription { get; set; }
		public string ActivityComment { get; set; }
		public string JobSolarRebateStatus { get; set; }

		public jobcountdata SummerCount { get; set; }

		public DateTime? CancelDate { get; set; }

		public string JobStatusColorClass { get; set; }

		public string LeadSourceColorClass { get; set; }

		public double WarehouseDistance { get; set; }

		public string JobCancelReason { get; set; }

		public string JobCancelReasonNotes { get; set; }

		public string PreviousJobStatus { get; set; }

        public string JobHoldReason { get; set; }

        public string JobHoldReasonNotes { get; set; }

		public int? NotAppliedDays { get; set; }

		public int? NotApprovedDays { get; set; }

		public int? ExpiredDays { get; set; }

		public bool IsBattery { get; set; }

    }
    public class jobcountdata {
		public decimal? TotalSystemCapacity { get; set; }
		public decimal? TotalCost { get; set; }
		public decimal? Price { get; set; }
		public int? DepositeReceived { get; set; }
		public int? Active { get; set; }
		public int? InstallJob { get; set; }
		public int? TotalNoOfPanel { get; set; }
		public int? TotalNoOfInvert { get; set; }

		public decimal? TotalBatteryKw { get; set; }
        public decimal? TotalMetroSystemCapacity { get; set; }
        public decimal? TotalRegionalSystemCapacity { get; set; }

        public decimal? TotalRegionalCost { get; set; }

        public decimal? TotalMetroCost { get; set; }

        public decimal? TotalRegionalBatteryKw { get; set; }

        public decimal? TotalMetroBatteryKw { get; set; }
	}
}