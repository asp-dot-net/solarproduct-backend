﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Leads.Dtos;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetJobForExcelViewDto : EntityDto
    {
        public JobDto Job { get; set; }

        public LeadDto Lead { get; set; }                                                                                                                                                                    

        public string JobStatusName { get; set; }

        public string Team { get; set; }

        public string HouseType { get; set; }

        public string RoofType { get; set; }

        public string PaymentOption { get; set; }

        public decimal? FirstDepositAmount { get; set; }

        public string ReminderTime { get; set; }

        public string ActivityDescription { get; set; }
        
        public string ActivityComment { get; set; }

        public int? noOfPanel { get;set; }

        public string JobCancelReason { get; set; }
        
        public string JobCancelReasonNotes { get; set; }

        public string PreviousJobStatus { get; set; }
        public string SalesRapName { get; set; }
        public string RoofAngle { get; set; }

        public string MeterUpgrade { get; set; }

        public int? MeterPhase { get; set; }  

        public string FinPaymentType { get; set; }  
        public string Deposit { get; set; }  
        public string OldSys { get; set; }

        public int? DiffDays { get; set; }


        public List<GetJobProductItemDetailsForExcel> ProductItems { get; set; }

        public int? First2Depodit { get; set; }

        public int? Deposit2Active { get; set; }

        public int? Active2Install { get; set; }
    }
}
