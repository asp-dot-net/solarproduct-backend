﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class WarrantyGridDto  
    {

        public string ProjectName { get; set; }
        public string Customer { get; set; }

        public string Address { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string panelName { get; set; }
        public string PanelModel { get; set; }
        public string PanelDocu { get; set; }
        public string PanelDocuPath { get; set; }
        public string PanelFiletype { get; set; }
        public string InverterFiletype { get; set; }

        public string InverterNname { get; set; }
        public string InverterModel { get; set; }
        public string InverterDoc { get; set; }
        public decimal? BalanceOwing { get; set; }
        public string SalesRep { get; set; }
        public string Sms { get; set; }
       public string Emailsend { get; set; }
        public string InverterDocPath { get; set; }
        
        public DateTime? ActivityReminderTime { get; set; }
        public string ReminderTime { get; set; }
        public string ActivityDescription { get; set; }
        public string ActivityComment { get; set; }
        public bool? WarrantySmsSend { get; set; }
        public DateTime? WarrantySmsSendDate { get; set; }
        public bool? WarrantyEmailSend { get; set; }
        public DateTime? WarrantyEmailSendDate { get; set; }
        public int? jobid { get; set; }
        public int? Leadid { get; set; }
        public int? Total { get; set; }
        public int? Send { get; set; }
        public int? Pending { get; set; }
    }
}

