﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetAllJobStatusesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }



    }
}