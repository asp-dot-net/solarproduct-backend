﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetAllProductItemsForExcelInput
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }

		public string ManufacturerFilter { get; set; }

		public string ModelFilter { get; set; }

		public string SeriesFilter { get; set; }

		public decimal? MaxSizeFilter { get; set; }
		public decimal? MinSizeFilter { get; set; }

		public string ShortNameFilter { get; set; }

		public int? MaxCodeFilter { get; set; }
		public int? MinCodeFilter { get; set; }

		public string InverterCertFilter { get; set; }

		public int? MaxStockLocationFilter { get; set; }
		public int? MinStockLocationFilter { get; set; }

		public int? MaxMinLevelFilter { get; set; }
		public int? MinMinLevelFilter { get; set; }

		public decimal? MaxCostPriceFilter { get; set; }
		public decimal? MinCostPriceFilter { get; set; }

		public DateTime? MaxExpiryDateFilter { get; set; }
		public DateTime? MinExpiryDateFilter { get; set; }

		public string ACPowerFilter { get; set; }

		public string StockItemFilter { get; set; }

		public string StockIdFilter { get; set; }


		 public string ProductTypeNameFilter { get; set; }

        public string ActiveOrDeactive { get; set; }

        public int? ProductTypeId { get; set; }

        public string Attechment { get; set; }

        public List<int?> location { get; set; }
    }
}