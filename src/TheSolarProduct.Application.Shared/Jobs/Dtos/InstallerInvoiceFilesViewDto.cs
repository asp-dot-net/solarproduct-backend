﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class InstallerInvoiceFilesViewDto : EntityDto
    {
        public string FileName {get;set;}
        public string FilePath {get;set;}
        public string CreatedUser {get;set;}
        public DateTime? CreatedDate {get;set;}
    }
}
