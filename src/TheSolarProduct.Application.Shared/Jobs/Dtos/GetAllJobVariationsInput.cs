﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetAllJobVariationsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }


		 public string VariationNameFilter { get; set; }

		 		 public string JobNoteFilter { get; set; }

        public int JobId { get; set; }
    }
}