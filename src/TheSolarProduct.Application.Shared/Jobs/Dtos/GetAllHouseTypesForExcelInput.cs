﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetAllHouseTypesForExcelInput
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }

		public int? MaxDisplayOrderFilter { get; set; }
		public int? MinDisplayOrderFilter { get; set; }



    }
}