﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class DetailDto : FullAuditedEntity
    {
		public decimal? InvoicePayExGST { get; set; }

		public decimal? InvoicePayGST { get; set; }

		public decimal? InvoicePayTotal { get; set; }

		public DateTime? InvoicePayDate { get; set; }

		public decimal? CCSurcharge { get; set; }

		public DateTime? VerifiedOn { get; set; }

		public int? PaymentNumber { get; set; }

		public bool IsVerified { get; set; }

		public string ReceiptNumber { get; set; }

		public DateTime? ActualPayDate { get; set; }

		public string PaidComment { get; set; }

		public string PaymentNote { get; set; }

		public string CreatedBy { get; set; }

		public int? InvoiceNo { get; set; }

		public int? JobId { get; set; }

		public long? UserId { get; set; }

		public long? VerifiedBy { get; set; }

		public long? RefundBy { get; set; }

		public int? InvoicePaymentMethodId { get; set; }

		public string InvoicePaymentMethodType { get; set; }

		public int? InvoicePaymentStatusId { get; set; }

		public string InvoicePaymentStatusName { get; set; }
	}
}
