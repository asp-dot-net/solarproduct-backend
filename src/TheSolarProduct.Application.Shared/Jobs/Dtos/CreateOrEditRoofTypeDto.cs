﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditRoofTypeDto : EntityDto<int?>
    {

		[Required]
		[StringLength(RoofTypeConsts.MaxNameLength, MinimumLength = RoofTypeConsts.MinNameLength)]
		public string Name { get; set; }
		
		
		public int? DisplayOrder { get; set; }

        public Boolean IsActive { get; set; }

    }
}