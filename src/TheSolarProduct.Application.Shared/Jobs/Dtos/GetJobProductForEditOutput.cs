﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetJobProductForEditOutput
    {
		public CreateOrEditJobProductDto JobProduct { get; set; }

		public string ProductItemName { get; set;}

		public string JobNote { get; set;}


    }
}