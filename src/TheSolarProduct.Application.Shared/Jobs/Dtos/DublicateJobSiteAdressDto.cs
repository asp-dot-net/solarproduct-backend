﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class DublicateJobSiteAdressDto : FullAuditedEntity
	{
			public string UnitNo { get; set; }
			public string UnitType { get; set; }
			public string StreetNo { get; set; }
			public string StreetName { get; set; }
			public string StreetType { get; set; }
			public string Suburb { get; set; }
			public string State { get; set; }
			public string Postcode { get; set; }
			public string Address { get; set; }
			public string CurrentAssignUserName { get; set; }
			public string CreatedByName { get; set; }
		    public int jobTypeId { get; set; }
	}
}
