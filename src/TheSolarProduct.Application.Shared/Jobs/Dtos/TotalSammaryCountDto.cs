﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
	public class TotalSammaryCountDto
	{

		public int? TotalAppTractorData { get; set; }
		public int? TotalAppTractorVerifyData { get; set; }

		public int? TotalAppTractornotVerifyData { get; set; }
		public int? TotalActiveData { get; set; }
		public int? TotalActiveVerifyData { get; set; }
		public int? TotalActiveDatanotVerifyData { get; set; }

		public int? TotalActiveDatanotAwaitingData { get; set; }
		public int? TotalExpiryData { get; set; }
		public int? TotalAppTractorAwaitingData { get; set; }


		public int? TotalfinanceData { get; set; }
		public int? TotalfinanceVerifyData { get; set; }
		public int? TotalfinanceDatanotVerifyData { get; set; }
		public int? TotalfinanceQueryData { get; set; }

		public int? TotaljobrefundData { get; set; }
		public int? TotaljobrefundVerifyData { get; set; }
		public int? TotaljobrefundnotVerifyData { get; set; }

		public int? TotalpaymentData { get; set; }
		public int? TotalpaymentVerifyData { get; set; }
		public int? TotalpaymentnotVerifyData { get; set; }

		public int? TotalfreebieData { get; set; }
		public int? TotalAwaitingData { get; set; }
		public int? TotalfreebieVerifyData { get; set; }
		public int? TotalfreebienotVerifyData { get; set; }

		public decimal? totalrefundamount { get; set; }
		public decimal? TotalAmountOfInvoice { get; set; }
		public decimal? TotalAmountReceived { get; set; }
		public decimal? BalanceOwning { get; set; }
		public decimal? TotalCancelAmount { get; set; }
		public decimal? TotalVICAmountOfInvoice { get; set; }
		public decimal? TotalVICAmountReceived { get; set; }
		public decimal? TotalVICBAlOwning { get; set; }
		public decimal? TotalCustAmountOfInvoice { get; set; }
		public decimal? TotalCustAmountReceived { get; set; }

		public decimal? TotalCustBAlOwning { get; set; }
		public int? TotalPromotion { get; set; }

		public int? NoReplay { get; set; }
		public int? InterestedCount { get; set; }
		public int? NotInterestedCount { get; set; }
		public int? OtherCount { get; set; }
		public int? Unhandle { get; set; }

		public int? PromotionProject { get; set; }
		public int? PromotionSales { get; set; }
		public int? PromotionSold { get; set; }


		public int? TotalNoOfPanel { get; set; }
		public int? TotalNoOfInvert { get; set; }
		public decimal? TotalSystemCapacity { get; set; }

		public decimal? TotalCost { get; set; }
		public decimal? Price { get; set; }
		public int? InstallJob { get; set; }
		public int? Active { get; set; }
		public int? DepositeReceived { get; set; }

		public decimal? PendingStc { get; set; }

		public decimal? AprrovedStc { get; set; }
		public decimal? TotalStc { get; set; }
		public int? TotalinsInvoice { get; set; }
		public int? PendinginsInvoice { get; set; }
		public int? VerifiedinsInvoice { get; set; }


		public int? PendingGrid { get; set; }

		public int? AprrovedGrid { get; set; }
		public int? AwaitingGrid { get; set; }
		public int? TotalGrid { get; set; }

		public int? TotalMapData { get; set; }
		public int? TotalDepRcvMapData { get; set; }
		public int? TotalActiveMapData { get; set; }
		public int? TotalJobBookedMapData { get; set; }
		public decimal? Compliancestc { get; set; }
		public decimal? FailedStc { get; set; }

		public decimal? TradedStc { get; set; }
		public int? TotalReferral { get; set; }
		public int? AwaitingReferral { get; set; }

		public int? readytopayReferral { get; set; }
		public int? paidReferral { get; set; }
		public int? WarrantyTotalData { get; set; }
		public int? WarrantyTotalEmailSend { get; set; }
		public int? WarrantyTotalEmailPending { get; set; }
	}
}
