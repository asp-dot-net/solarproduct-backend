﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditJobVariationDto : EntityDto<int?>
    {

        public decimal? Cost { get; set; }


        public int? VariationId { get; set; }

        public int? JobId { get; set; }

        public string Notes { get; set; }


    }
}