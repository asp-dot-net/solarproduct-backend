﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.Jobs.Dtos
{
	public class JobDto : EntityDto
	{
		public string JobNumber { get; set; }
		
		public int? JobTypeId { get; set; }
		
		public int? JobStatusId { get; set; }

		public string Address { get; set; }

		public string UnitNo { get; set; }

		public string UnitType { get; set; }

		public string StreetNo { get; set; }

		public string StreetName { get; set; }

		public string StreetType { get; set; }

		public string Suburb { get; set; }

		public string State { get; set; }

		public string PostalCode { get; set; }

		public int? ElecDistributorId { get; set; }

		public DateTime? DistAppliedDate { get; set; }

		public DateTime? ExpiryDate { get; set; }

		public string ApplicationRefNo { get; set; }

		public string InstallerNotes { get; set; }

		public string Notes { get; set; }

		public string OldSystemDetails { get; set; }

		public int? ElecRetailerId { get; set; }

		public string NMINumber { get; set; }

		public int? LeadId { get; set; }

		public string MeterNumber { get; set; }
        public string Mobile { get; set; }

        public string Email { get; set; }
        public string CompanyName { get; set; }
        public string RegPlanNo { get; set; }

		public string LotNumber { get; set; }
		
		public DateTime? STCUploaddate { get; set; }
		
		public int? PVDStatus { get; set; }
		
		public DateTime? STCAppliedDate { get; set; }
		
		public string PVDNumber { get; set; }
		
		public string STCNotes { get; set; }

		public decimal? TotalCost { get; set; }
		public decimal? SystemCapacity { get; set; }

		public string MeterApplyRef { get; set; }
		public DateTime? InspectionDate { get; set; }
		public string STCUploadNumber { get; set; }
		
		public string InstallerName { get; set; }
		public string InstallerCompanyName { get; set; }
		public string PVDStatusName { get; set; }

		public string FinancePurchaseNo { get; set; }
		public int? FinanceDocumentVerified { get; set; }
		public DateTime? DistApplied { get; set; }
		public DateTime? CreationTime { get; set; }

		public DateTime? DistApproveDate { get; set; }

		public DateTime? DistExpiryDate { get; set; }
		public DateTime?  DepositeRecceivedDate { get; set; }
		public string CurrentLeadOwaner { get; set; }
		public string JobHoldReasonName { get; set; }
		public string HoldNotes { get; set; }
		public DateTime? InstallationDate { get; set; }
		public DateTime? InstalledcompleteDate { get; set; }
		public bool? SmsSend { get; set; }
		public DateTime? SmsSendDate { get; set; }
		public bool? EmailSend { get; set; }
		public DateTime? EmailSendDate { get; set; }

		public bool? FinanceSmsSend { get; set; }
		public DateTime? FinanceSmsSendDate { get; set; }
		public bool? FinanceEmailSend { get; set; }
		public DateTime? FinanceEmailSendDate { get; set; }

		public virtual bool? JobActiveSmsSend { get; set; }
		public virtual DateTime? JobActiveSmsSendDate { get; set; }
		public virtual bool? JobActiveEmailSend { get; set; }
		public virtual DateTime? JobActiveEmailSendDate { get; set; }


		public virtual bool? GridConnectionSmsSend { get; set; }
		public virtual DateTime? GridConnectionSmsSendDate { get; set; }
		public virtual bool? GridConnectionEmailSend { get; set; }
		public virtual DateTime? GridConnectionEmailSendDate { get; set; }

		public virtual bool? StcSmsSend { get; set; }
		public virtual DateTime? StcSmsSendDate { get; set; }
		public virtual bool? StcEmailSend { get; set; }
		public virtual DateTime? StcEmailSendDate { get; set; }

		public virtual bool? JobGridSmsSend { get; set; }
		public virtual DateTime? JobGridSmsSendDate { get; set; }
		public virtual bool? JobGridEmailSend { get; set; }
		public virtual DateTime? JobGridEmailSendDate { get; set; }
        public decimal? StcTotalPrice { get; set; }
        public decimal? Stc { get; set; }

		public string ManualQuote { get; set; }
		public string Note { get; set; }

		public DateTime? JobAssignDate { get; set; }

		public decimal owningAmt { get; set; }
		public string Remark_if_owing { get; set; }
		public string FinanceName { get; set; }
		public decimal? paidAmmount { get; set; }
		public decimal? JoID { get; set; }
		public string SolarRebateStatus { get; set; }
		public string LastUpdatedDate { get; set; }
		public int? ReviewRating { get; set; }
		public string ReviewNotes { get; set; }
		public string ReviewType { get; set; }
		public string ProductReviewType { get; set; }
		public bool? ReviewSmsSend { get; set; }
		public DateTime? ReviewSmsSendDate { get; set; }
		public bool? ReviewEmailSend { get; set; }
		public DateTime? ReviewEmailSendDate { get; set; }

		public DateTime? ActiveDate { get; set; }

		public string InvoiceNotes { get; set; }

        public DateTime? FirstDepositDate { get; set; }

        public decimal? SolarVICRebate { get; set; }
        
		public decimal? SolarVICLoanDiscont { get; set; }

        public string Jobtype { get; set; }

		public string PriceType { get; set; }

        public string NextFollowup { get; set; }

        public string Discription { get; set; }

        public string Comment { get; set; }

		public int? NotVerifiedDays { get; set; }
        public decimal? BatteryKw { get; set; }
    }
}