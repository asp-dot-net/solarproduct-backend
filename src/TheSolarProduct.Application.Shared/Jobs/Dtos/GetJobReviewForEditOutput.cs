﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetJobReviewForEditOutput
    {
        public CreateOrEditJobReviewDto JobReview { get; set; }

        public string ReviewType { get; set; }
        public int Id { get; set; }
        public string ReviewNote { get; set; }
        public string ReviewRating { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }

    }
}
