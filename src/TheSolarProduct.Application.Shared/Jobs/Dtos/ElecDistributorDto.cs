﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.Jobs.Dtos
{
    public class ElecDistributorDto : EntityDto
    {
		public string Name { get; set; }

		public int? Seq { get; set; }

		public bool NSW { get; set; }

		public bool SA { get; set; }

		public bool QLD { get; set; }

		public bool VIC { get; set; }

		public bool WA { get; set; }

		public bool ACT { get; set; }

		public string TAS { get; set; }

		public bool NT { get; set; }

		public string ElectDistABB { get; set; }

		public bool ElecDistAppReq { get; set; }

		public int? GreenBoatDistributor { get; set; }

        public  Boolean IsActive { get; set; }

    }
}