﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class ImportDataViewDto : EntityDto
    {
        public DateTime? Date { get; set; }
        public string JobNumber { get; set; }
        public string PaymentMethodName { get; set; }
        public string Description { get; set; }
        public decimal? PaidAmmount { get; set; }
        public decimal? SSCharge { get; set; }
        public string AllotedBy { get; set; }
        public string ReceiptNumber { get; set; }
        public string PurchaseNumber { get; set; }
        public int? InvoicePaymentMethodId { get; set; }
        public string InvoiceNotDescription { get; set; }
        public string CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? LeadId { get; set; }
        public int ID { get; set; }
    }
}
