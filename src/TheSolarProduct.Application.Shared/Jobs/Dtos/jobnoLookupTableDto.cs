﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class jobnoLookupTableDto
    {
        public int? Value { get; set; }

        public string Name { get; set; }

        public string jobNumber { get; set; }
    }
}
