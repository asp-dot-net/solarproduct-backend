﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
   public  class InstallerInvoiceImportDataViewDto : EntityDto
    {
        public ImportDataViewDto Importdata { get; set; }
        
        public decimal? TotalInvoicePayment { get; set; }

        public decimal? AmmountRcv { get; set; }

        public decimal? AmmountRcvWO { get; set; }
    }
}
