﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
	public class JobInstallationDto : EntityDto
	{
		//Installation Details
		public virtual DateTime? InstallationDate { get; set; }

		public virtual string InstallationTime { get; set; }

		public virtual int? InstallerId { get; set; }

		public virtual string InstallerName { get; set; }

		public virtual int? ElectricianId { get; set; }

		public virtual string ElectricianName { get; set; }

		public virtual int? DesignerId { get; set; }

		public virtual string DesignerName { get; set; }

		public virtual int? WarehouseLocation { get; set; }

		public virtual string InstallationNotes { get; set; }

        public virtual decimal? STC { get; set; }

        public virtual decimal? Rebate { get; set; }

        public int? sectionId { get; set; }

		public decimal? EstimateCost { get; set; }

        public string PostalCode { get; set; }

        public decimal? SystemCapacity { get; set; }



    }
}
