﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditJobRefundDto : EntityDto<int?>
    {
		public decimal? Amount { get; set; }

		public decimal? PaidAmount { get; set; }
		
		//[Required]
		public string BankName { get; set; }
		
		public string AccountName { get; set; }
		
		public string BSBNo { get; set; }
		
		//[Required]
		public string AccountNo { get; set; }
		
		public string Notes { get; set; }
		
		public DateTime? PaidDate { get; set; }
		
		public string Remarks { get; set; }
		
		public int? PaymentOptionId { get; set; }
		 
		public int? JobId { get; set; }
		 
		public int? RefundReasonId { get; set; }

		public string RefundReason { get; set; }

		public string PaymentOptionName { get; set; }

		public string ReceiptNo { get; set; }
		
		public  Boolean? JobRefundSmsSend { get; set; }

		public  Boolean? JobRefundEmailSend { get; set; }

		public  DateTime? JobRefundSmsSendDate { get; set; }

		public  DateTime? JobRefundEmailSendDate { get; set; }

        public string RefundPaymentType { get; set; }

        public virtual int? SectionId { get; set; }

        public virtual int? JobCancelReasonId { get; set; }
        public  Boolean? XeroInvCreated { get; set; }

    }
}