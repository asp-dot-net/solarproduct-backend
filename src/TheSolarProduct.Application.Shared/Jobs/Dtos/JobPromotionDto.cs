﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.Jobs.Dtos
{
    public class JobPromotionDto : EntityDto
    {

		 public int JobId { get; set; }

		 		 public int? PromotionMasterId { get; set; }

		public string TrackingNumber { get; set; }
		public string Description { get; set; }

		public bool? SmsSend { get; set; }
		public bool? EmailSend { get; set; }

		public DateTime? SmsSendDate { get; set; }

		public DateTime? EmailSendDate { get; set; }

		public string FreebieTransportName { get; set; }

	}
}