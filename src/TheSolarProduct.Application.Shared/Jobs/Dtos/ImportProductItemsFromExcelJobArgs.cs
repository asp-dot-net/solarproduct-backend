﻿using Abp;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
	public class ImportProductItemsFromExcelJobArgs
	{
		public int? TenantId { get; set; }

		public Guid BinaryObjectId { get; set; }

		public UserIdentifier User { get; set; }

		public string ProductTypeId { get; set; }
	}
}
