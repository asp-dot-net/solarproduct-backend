﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetAllPvdStatusForGridExcelInput
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }

        public string Filename { get; set; }
    }
}
