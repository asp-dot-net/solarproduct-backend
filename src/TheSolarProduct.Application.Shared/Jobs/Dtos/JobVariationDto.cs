﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.Jobs.Dtos
{
    public class JobVariationDto : EntityDto
    {

		 public int? VariationId { get; set; }

		 		 public int? JobId { get; set; }

        public decimal? Cost { get; set; }
    }
}