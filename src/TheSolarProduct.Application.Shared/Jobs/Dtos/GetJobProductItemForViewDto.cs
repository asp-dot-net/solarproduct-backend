﻿namespace TheSolarProduct.Jobs.Dtos
{
    public class GetJobProductItemForViewDto
    {
		public JobProductItemDto JobProductItem { get; set; }

		public string JobNote { get; set;}

		public string ProductItemName { get; set;}


    }
}