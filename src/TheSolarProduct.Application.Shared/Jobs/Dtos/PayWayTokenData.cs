﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
   public class PayWayTokenData
    {
        public string JobNumber { get; set; }
        public int jobid { get; set; }
        public string paymentMethod { get; set; }
        public string cardNumber { get; set; } 
        public int expiryDateMonth { get; set; }
        public int expiryDateYear { get; set; }
        public string cvn { get; set; }
        public string cardholderName { get; set; }
        public decimal principalAmount { get; set; }
        public int? WithOrWithoutSurcharge { get; set; }
        public int? PaymentType { get; set; }

        public int? sectionId { get; set; }
    }
}
