﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class AllPayWayDatasDto : EntityDto
    {
        public int jobId { get; set; }
        public string transactionId { get; set; }
        public string status { get; set; }
        public string responseCode { get; set; }
        public string responseText { get; set; }
        public string transactionType { get; set; }
        public string customerNumber { get; set; }
        public string customerName { get; set; }
        public string currency { get; set; }
        public decimal? principalAmount { get; set; }
        public decimal? surchargeAmount { get; set; }
        public decimal? paymentAmount { get; set; }
        public string paymentMethod { get; set; }
        public int? PaymentType { get; set; }
        public int? OrganizationId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string JobNumber { get; set; }
        public string PaymentMethodName { get; set; }
        public string Description { get; set; }
        public decimal? PaidAmmount { get; set; }
        public decimal? SSCharge { get; set; }
        public string AllotedBy { get; set; }
        public string ReceiptNumber { get; set; }
        public string PurchaseNumber { get; set; }
        public int? LeadId { get; set; }
        public string CreatedUser { get; set; }
        public int? ID { get; set; }
    }
}
