﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class ExistJobSiteAddDto : EntityDto<int?>
	{
		public string UnitNo { get; set; }

		public string UnitType { get; set; }

		public string StreetNo { get; set; }

		public string StreetName { get; set; }

		public string StreetType { get; set; }

		public string Suburb { get; set; }

		public string State { get; set; }

		public string PostCode { get; set; }

		public int OrganizationId { get; set; }

		public int? jobTypeId { get; set; }

		public int? jobid { get; set; }

		public int leadid { get; set; }

		public string Suburbs { get; set; }
	}
}
