﻿namespace TheSolarProduct.Jobs.Dtos
{
    public class GetProductItemForViewDto
    {
		public ProductItemDto ProductItem { get; set; }

		public string ProductTypeName { get; set;}

        public string QLD { get; set;}

        public string NSW { get; set;}

        public string VIC { get; set;}

        public string NT { get; set;}

        public string SA { get; set;}

        public string AUS { get; set;}

        public string TAS { get; set;}

        public string WA { get; set;}

    }
}