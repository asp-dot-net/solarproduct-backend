﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.Jobs.Dtos
{
	public class GetAllJobProductsInput : PagedAndSortedResultRequestDto
	{
		public string Filter { get; set; }

		public string ProductItemNameFilter { get; set; }

		public string JobNoteFilter { get; set; }

		public string Model { get; set; }

		public int JobId { get; set; }
	}
}