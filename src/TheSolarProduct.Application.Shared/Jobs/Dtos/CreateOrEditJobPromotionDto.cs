﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
	public class CreateOrEditJobPromotionDto : EntityDto<int?>
	{
		public int JobId { get; set; }

		public int? PromotionMasterId { get; set; }

		public string TrackingNumber { get; set; }

		public string Description { get; set; }

		public DateTime? DispatchedDate { get; set; }

		public int? FreebieTransportId { get; set; }

		public bool? SmsSend { get; set; }
		public bool? EmailSend { get; set; }

		public  DateTime? SmsSendDate { get; set; }

		public  DateTime? EmailSendDate { get; set; }

        public virtual int? SectionId { get; set; }

		public string SerialNo { get; set; }
        public int? VoucherId { get; set; }
    }
}