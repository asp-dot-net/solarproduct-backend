﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.Jobs.Dtos
{
    public class ProductTypeDto : EntityDto
    {
		public string Name { get; set; }

		public int? DisplayOrder { get; set; }



    }
}