﻿using Abp;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditJobInstallerInvoiceDto : EntityDto<int?>
    {
        public int? totalNoOfInvert;

        public int TenantId { get; set; }

        public int? JobId { get; set; }
        
        public int? InvTypeId { get; set; }

        public decimal? Amount { get; set; }

        public DateTime? InvDate { get; set; }

        public decimal? AdvanceAmount { get; set; }

        public decimal? LessDeductAmount { get; set; }

        public decimal? TotalAmount { get; set; }

        public DateTime? AdvancePayDate { get; set; }

        public DateTime? PayDate { get; set; }

        public DateTime? PaymentTypeId { get; set; }

        public int? InstallerId { get; set; }

        public string Notes { get; set; }

        public DateTime? Date { get; set; }

        public string Remarks { get; set; }

        public bool IsPaid { get; set; }

        public bool IsVerify { get; set; }

        public int? PaymentsTypeId { get; set; }

        public  int? InvNo { get; set; }

        public virtual string FileName { get; set; }
        public virtual string FilePath { get; set; }

        public string FileToken { get; set; }
        public int? Installer_selfie { get; set; }
        public int? Front_of_property { get; set; }
        public int? Inst_Des_Ele_sign { get; set; }
        public int? Cx_Sign { get; set; }
        public int? CES { get; set; }
        public int? PanelsSerialNo { get; set; }
        public int? InstalaltionPic { get; set; }
        public int? InverterSerialNo { get; set; }
        public int? Wi_FiDongle { get; set; }
        public int? Traded { get; set; }
        public string Remark_if_owing { get; set; }
        public int? Noof_Panels_Invoice { get; set; }
        public int? ActualPanels_Installed { get; set; }
        public int? Splits { get; set; }
        public string TravelKMfromWarehouse { get; set; }
        public string otherExtraInvoiceNumber { get; set; }
        public string Installation_Maintenance_Inspection { get; set; }
        public string AllGood_NotGood { get; set; }
        public string NotesOrReasonforPending { get; set; }
        public string EmailSent { get; set; }
        public int? Finance { get; set; }
        public string JobNumber { get; set; }
        public string State { get; set; }
        public decimal? TotalCost { get; set; }
        public string InstalledcompleteDate { get; set; }
        public string InstallerName { get; set; }
        public string InstallerCompanyName { get; set; }
        public int? TotalNoOfPanel { get; set; }
        public decimal? owningAmt { get; set; }
        public string Verifieddoublestory { get; set; }
        public string GridConnection { get; set; }
        public string InvoiceNo { get; set; }
        public decimal? InvoiceAmount { get; set; }

        public int? Noof_Panels_invoice { get; set; }

        public virtual DateTime? ApproedDate { get; set; }
        
        public virtual decimal? ApproedAmount { get; set; }
        
        public string ApprovedNotes { get; set; } 
        
        public string Jobdetails { get; set; } 
        
        public  string   installers { get; set; }
        
        public string BankRefNo { get; set; }
        
        public virtual DateTime? invoiceIssuedDate { get; set; }
        
        public string FinanceName { get; set; }
        
        public string threephase { get; set; }
        
        public  DateTime? SmsSendDate { get; set; }
        
        public  DateTime? EmaiSendDate { get; set; }
        
        public  Boolean? SmsSend { get; set; }
        
        public  Boolean? installerEmailSend { get; set; }
        
        public  DateTime? DueDate { get; set; }
        
        public  int? PriorityId { get; set; }

        public int? SectionId { get; set; }

        public List<InstallerInvoicePriceListDto> installerInvoicePriceListDtos { get; set; }

        public bool? IsGSTInclude { get; set; }


    }
}
