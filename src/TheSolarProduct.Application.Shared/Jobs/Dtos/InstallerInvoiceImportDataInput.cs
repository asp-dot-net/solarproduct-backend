﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
   public  class InstallerInvoiceImportDataInput : PagedAndSortedResultRequestDto
    {
        public string FilterName { get; set; }

        public string FilterText { get; set; }
        public int? OrganizationID { get; set; }
        public int? UserID { get; set; }
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        public string State { get; set; }
        public int? Paymenttypeid { get; set; }
        public int? Payby { get; set; }
        public string DateFilterType { get; set; }
        public int? excelorcsv { get; set; }
    }
}
