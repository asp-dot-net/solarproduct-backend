﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class STCZoneRatingDto : EntityDto
    {
        public decimal Rating { get; set; }
        public string UpSizeTs { get; set; }
    }

    public class STCPostalCodeDto : EntityDto
    {
        public string PostCodeFrom { get; set; }
        public string PostCodeTo { get; set; }
        public int ZoneId { get; set; }
    }

    public class STCYearWiseRateDto : EntityDto
    {
        public int Year { get; set; }
        public int Rate { get; set; }
    }
}
