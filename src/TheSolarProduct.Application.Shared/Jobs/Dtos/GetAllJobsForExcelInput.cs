﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetAllJobsForExcelInput
    {
        public string FilterName { get; set; }

        public string Filter { get; set; }

        public string SuburbFilter { get; set; }

        public string StateFilter { get; set; }

        public string JobTypeNameFilter { get; set; }

        public string JobStatusNameFilter { get; set; }

        public string RoofTypeNameFilter { get; set; }

        public string RoofAngleNameFilter { get; set; }

        public string ElecDistributorNameFilter { get; set; }

        public string LeadCompanyNameFilter { get; set; }

        public string ElecRetailerNameFilter { get; set; }

        public string PaymentOptionNameFilter { get; set; }

        public string DepositOptionNameFilter { get; set; }

        public string MeterUpgradeNameFilter { get; set; }

        public string MeterPhaseNameFilter { get; set; }

        public string PromotionOfferNameFilter { get; set; }

        public string HouseTypeNameFilter { get; set; }

        public string FinanceOptionNameFilter { get; set; }

        public int? OrganizationUnit { get; set; }

        public string projectFilter { get; set; }

        public string stateNameFilter { get; set; }

        public int? elecDistributorId { get; set; }

        public List<int> jobStatusIDFilter { get; set; }

        public int? applicationstatus { get; set; }

        public int? jobTypeId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public int? IsVerified { get; set; }

        public int? SalesManagerId { get; set; }

        public int? SalesRepId { get; set; }

        public string JobDateFilter { get; set; }

        public string PanelModel { get; set; }

        public string InvertModel { get; set; }

        public int? Readytoactive { get; set; }

        public decimal? SystemStrtRange { get; set; }

        public decimal? SystemEndRange { get; set; }

        public List<int> LeadSourceIdFilter { get; set; }
        public string DateNameFilter { get; set; }
        public int? InstallerID { get; set; }

        public int? pvdStatus { get; set; }
        //public List<int> testfilter { get; set; }

        public string pvdNo { get; set; }

        public int? TeamId { get; set; }

        public string AddressFilter
        {
            get; set;



        }

        public List<int> pvdStat { get; set; }

        public int excelorcsv { get; set; }
        public int? paymentid { get; set; }
        public string AreaNameFilter { get; set; }
        public int? pvdnoStatus { get; set; }
        public string PostalCodeFrom { get; set; }
        public string PostalCodeTo { get; set; }
        public int? Solarvicstatus { get; set; }

        public int? BatteryFilter { get; set; }

        public List<int> PreviousJobStatusId { get; set; }

        public string BatteryModel { get; set; }

        public List<string> JobTypeNameListFilter { get; set; }
    }
}