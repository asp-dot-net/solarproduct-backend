﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetDepositOptionForEditOutput
    {
		public CreateOrEditDepositOptionDto DepositOption { get; set; }


    }
}