﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetAllFreebieTransportsForExcelInput
    {
        public string Filter { get; set; }

    }
}