﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
	public class UpdateJobStatusDto : EntityDto
	{
		public int? JobStatusId { get; set; }

		public DateTime? NextFollowUpDate { get; set; }

		public string JobHoldReason { get; set; }
		public int? JobHoldReasonId { get; set; }

		public string JobCancelReason { get; set; }
		public string DepositeStatusNotes { get; set; }
		public string  ActiveStatusNotes { get; set; }
		public int? JobCancelReasonId { get; set; }

		public bool? IsRefund { get; set; }

		public string JobCancelRequestReason { get; set; }

		public bool? IsJobCancelRequest { get; set; }

		public int? SectionId { get; set; }

		public string SectionName { get; set; }

    }
}
