﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetJobRefundForEditOutput
    {
		public CreateOrEditJobRefundDto JobRefund { get; set; }

		public string PaymentOptionName { get; set;}

		public string JobNote { get; set;}

		public string RefundReasonName { get; set;}
		public  string ReceiptNo { get; set; }
		public decimal? Amount { get; set; }

		public int?  PaymentOptionId { get; set; }
		public string JobNumber { get; set; }
		public string LeadCompanyName { get; set; }
        public  Boolean? XeroInvCreated { get; set; }

    }
}