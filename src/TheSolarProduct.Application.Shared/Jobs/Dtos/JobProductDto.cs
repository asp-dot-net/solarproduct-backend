﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.Jobs.Dtos
{
    public class JobProductDto : EntityDto
    {

		 public int? ProductItemId { get; set; }

		 		 public int? JobId { get; set; }

        public int? Quantity { get; set; }
		 
    }
}