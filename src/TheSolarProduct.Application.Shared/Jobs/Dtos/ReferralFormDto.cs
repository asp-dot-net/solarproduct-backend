﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class ReferralFormDto
    {
        public string JobNumber { get; set; }

        public string JobStatus { get; set; }

        public string EntryDate { get; set; }

        public string Name { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string NoOfPanels { get; set; }

        public string DepositDate { get; set; }

        public string CusAccNumber { get; set; }

        public string CusAccName { get; set; }

        public decimal RefAmount { get; set; }

        public string CusBSB { get; set; }

        public string SalesRepName { get; set; }

        public string Manager { get; set; }

        public string ViewHtml { get; set; }

        public string RefName { get; set; } 

        public string RefProj { get; set; }

        public string RefAddress { get; set; }
    }
}
