﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetAllElecDistributorsForExcelInput
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }

		public int? MaxSeqFilter { get; set; }
		public int? MinSeqFilter { get; set; }

		public int NSWFilter { get; set; }

		public int SAFilter { get; set; }

		public int QLDFilter { get; set; }

		public int VICFilter { get; set; }

		public int WAFilter { get; set; }

		public int ACTFilter { get; set; }

		public string TASFilter { get; set; }

		public int NTFilter { get; set; }

		public string ElectDistABBFilter { get; set; }

		public int ElecDistAppReqFilter { get; set; }

		public int? MaxGreenBoatDistributorFilter { get; set; }
		public int? MinGreenBoatDistributorFilter { get; set; }



    }
}