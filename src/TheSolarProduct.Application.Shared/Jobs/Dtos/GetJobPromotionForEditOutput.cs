﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetJobPromotionForEditOutput
    {
		public CreateOrEditJobPromotionDto JobPromotion { get; set; }

		public string JobStreetName { get; set;}

		public string PromotionMasterName { get; set;}

        public string Name { get; set; }

        public string JobNumber { get; set; }

        public string LeadCompanyName { get; set; }
        public Dictionary<int, int> VoucherUsageCounts { get; set; }
    }
}