﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetProductItemsCountWithProdoctTypeDto : EntityDto
    {
        public string ProductType { get; set; }

        public string Image { get; set; }

        public int ProductItemCount { get; set; }
    }
}
