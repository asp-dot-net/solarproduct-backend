﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
   public class TotalStcCountDto
    {
        public string Stc { get; set; }
        public decimal? totalstcamount { get; set; }
        public int? totaljob { get; set; }
    }
}
