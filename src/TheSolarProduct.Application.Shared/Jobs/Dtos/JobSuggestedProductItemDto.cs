﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class JobSuggestedProductItemDto : EntityDto<int?>
    {
        public virtual int? SuggestedProductId { get; set; }

        public virtual int? ProductItemId { get; set; } 

        public virtual int? Quantity { get; set; }
        public int? ProductTypeId { get; set; }

        public string ProductItem { get; set; }
        public string ModelName { get; set; }
        public decimal? Size { get; set; }

    }
}
