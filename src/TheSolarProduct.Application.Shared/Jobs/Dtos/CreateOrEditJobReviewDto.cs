﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditJobReviewDto : EntityDto<int?>
    {
        public virtual int? ReviewTypeId { get; set; }
        public virtual int? Rating { get; set; }
        public virtual string ReviewNotes { get; set; }
        public int? JobId { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
    }

}
