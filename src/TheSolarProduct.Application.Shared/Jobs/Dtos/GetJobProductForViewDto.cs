﻿namespace TheSolarProduct.Jobs.Dtos
{
    public class GetJobProductForViewDto
    {
		public JobProductDto JobProduct { get; set; }

		public string ProductItemName { get; set;}

		public string JobNote { get; set;}


    }
}