﻿using Abp.Application.Services.Dto;
using System;
using TheSolarProduct.JobTrackers.Dtos;

namespace TheSolarProduct.Jobs.Dtos
{
    public class InstallerNewDto : EntityDto
    {
        public string ProjectName { get; set; }
        
        public string Customer { get; set; }

        public string Address { get; set; }

        public string PCode { get; set; }

        public DateTime? InstallDate { get; set; }
        
        public DateTime? InstallComplate { get; set; }
        
        public decimal? Ammount { get; set; }
        
        public DateTime? InvDate { get; set; }
        
        public DateTime? Issued { get; set; }
        
        public DateTime? PaymentDate { get; set; }
        
        public DateTime? PaymentStaus { get; set; }
        
        public string Suburb { get; set; }

        public string State { get; set; }

        public int? InvNo { get; set; }

        public int? id {get;set;}

        public int? JobId {get;set;}

        public string installername { get; set; }
        
        public string invoiceType { get; set; }
        
        public string invoiceNotes { get; set; }
        
        public string InvoiceNO { get; set; }
        
        public int? invoiceinstallerId { get; set; }

        public string Filenameinv { get; set; }
        
        public string FilePathinv { get; set; }

        public string Area { get; set; }
        public string CompanyName { get; set; }

        public decimal? SysAmount { get; set; }

        public decimal? Per {get; set;}

        public decimal? TotalKW { get; set; }

        public decimal? AVGInvAmtPerKw { get; set; }
    }
}
