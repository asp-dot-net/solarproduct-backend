﻿namespace TheSolarProduct.Jobs.Dtos
{
    public class GetJobVariationForViewDto
    {
		public JobVariationDto JobVariation { get; set; }

		public string VariationName { get; set;}

		public string JobNote { get; set;}


    }
}