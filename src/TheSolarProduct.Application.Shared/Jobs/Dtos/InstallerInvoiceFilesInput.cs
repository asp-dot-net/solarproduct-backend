﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
   public class InstallerInvoiceFilesInput : PagedAndSortedResultRequestDto
    {
        public int? OrganizationID { get; set; }
        public int? UserID { get; set; }
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

    }
}
