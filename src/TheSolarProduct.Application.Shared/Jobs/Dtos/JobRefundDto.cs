﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.Jobs.Dtos
{
	public class JobRefundDto : FullAuditedEntityDto
	{
		public decimal? Amount { get; set; }

		public string BankName { get; set; }

		public string AccountName { get; set; }

		public string BSBNo { get; set; }

		public string AccountNo { get; set; }

		public DateTime? PaidDate { get; set; }

		public string Remarks { get; set; }

		public string Notes { get; set; }

		public int? PaymentOptionId { get; set; }

		public int? JobId { get; set; }

		public int? RefundReasonId { get; set; }
		public  string ReceiptNo { get; set; }

		public string JobRefundtype { get; set; }
		public Boolean? JobRefundSmsSend { get; set; }

		public Boolean? JobRefundEmailSend { get; set; }

		public DateTime? JobRefundSmsSendDate { get; set; }

		public DateTime? JobRefundEmailSendDate { get; set; }
		public Boolean? IsVerify { get; set; }

	}
}