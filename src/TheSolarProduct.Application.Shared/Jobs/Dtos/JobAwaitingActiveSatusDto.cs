﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class JobAwaitingActiveSatusDto
    {
        ///// deposite received
        public Boolean Housetype { get; set; }
        public Boolean Elecdistributor { get; set; }

        public Boolean Roofangle { get; set; }
        public Boolean Rooftype { get; set; }
        public Boolean Quote { get; set; }
        public Boolean Invoice { get; set; }
        public Boolean Productdetail { get; set; }
        public Boolean paymenttype { get; set; }
        public Boolean Finance { get; set; }
        public int? payementoptionid { get; set; }
        ///// Application
        public string Vicrebate { get; set; }
        public string State { get; set; }
        public Boolean Meterphase { get; set; }
        public Boolean Nminumber { get; set; }
        public Boolean Peakmeterno { get; set; }
        public Boolean Doclist { get; set; }




        ///// Active
        public Boolean Approvedreferencenumber { get; set; }

        public Boolean Distapprovedate { get; set; }

        public Boolean Meterupgrade { get; set; }
        public Boolean Meterboxphoto { get; set; }
        public Boolean Signedquote { get; set; }

        public Boolean Deposit { get; set; }
        public Boolean Quoteacceptdate { get; set; }


        public Boolean Elecretailer { get; set; }

        public Boolean Expirydate { get; set; }
        public Boolean Solarrebatestatus { get; set; }
        public Boolean Paymentverified { get; set; }
        public Boolean Lastquotesigned { get; set; }
        public Boolean Exportcontrolformsigned { get; set; }

        public Boolean Feedintariffsigned { get; set; }

        public Boolean Shadingdeclarationsigned { get; set; }

        public Boolean Efficiencydeclarationsigned { get; set; }
        public Boolean Exportcontrolform { get; set; }

        public Boolean Feedintariff { get; set; }

        public Boolean Shadingdeclaration { get; set; }

        public Boolean Efficiencydeclaration { get; set; }

        public Boolean Bonusadded { get; set; }
    } 

    }

