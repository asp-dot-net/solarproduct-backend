﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetJobVariationForEditOutput
    {
		public CreateOrEditJobVariationDto JobVariation { get; set; }

		public string VariationName { get; set;}

		public string JobNote { get; set;}


    }
}