﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetProductItemSearchResult : EntityDto
    {
        public string ProductItem { get; set; }

        public string ProductModel { get; set; }

        public decimal? Size { get; set; }

        public string DatasheetUrl { get; set; }

        public bool IsExpired { get; set; }
        
        public  DateTime? ExpiryDate { get; set; }

        public decimal? Price { get; set; }

        public int? PerformanceWarranty { get; set; }

        public int? ProductWarranty { get; set; }

        public int? ExtendedWarranty { get; set; }
    }
}
