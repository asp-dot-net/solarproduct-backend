﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Common.Dto;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Quotations.Dtos;

namespace TheSolarProduct.Jobs.Dtos
{
    public class JobApplicationViewDto
    {
        public CreateOrEditJobDto Job { get; set; }

        public string JobTypeName { get; set; }

        public string JobStatusName { get; set; }

        public string RoofTypeName { get; set; }

        public string RoofAngleName { get; set; }

        public string ElecDistributorName { get; set; }

        public string LeadCompanyName { get; set; }

        public string ElecRetailerName { get; set; }

        public string PaymentOptionName { get; set; }

        public string DepositOptionName { get; set; }

        public string MeterUpgradeName { get; set; }

        public string MeterPhaseName { get; set; }

        public string PromotionOfferName { get; set; }

        public string HouseTypeName { get; set; }

        public string FinanceOptionName { get; set; }

        public string JobNumber { get; set; }

        public int? JobTypeId { get; set; }

        public int? JobStatusId { get; set; }

        public string UnitNo { get; set; }

        public string UnitType { get; set; }

        public string StreetNo { get; set; }

        public string StreetName { get; set; }

        public string StreetType { get; set; }

        public string Suburb { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public int? ElecDistributorId { get; set; }

        public DateTime? DistApplied { get; set; }

        public DateTime? DistAppliedDate { get; set; }

        public string ApplicationRefNo { get; set; }

        public string InstallerNotes { get; set; }

        public string Notes { get; set; }

        public string OldSystemDetails { get; set; }

        public int? ElecRetailerId { get; set; }

        public string NMINumber { get; set; }

        public int? LeadId { get; set; }

        public string MeterNumber { get; set; }

        public string RegPlanNo { get; set; }

        public string LotNumber { get; set; }

        public virtual DateTime? STCUploaddate { get; set; }

        public virtual int? PVDStatus { get; set; }

        public virtual DateTime? STCAppliedDate { get; set; }

        public string PVDNumber { get; set; }

        public string STCNotes { get; set; }

        public string STCUploadNumber { get; set; }

        public string InstallerName { get; set; }
        public string InstallerMobile { get; set; }
        public string InstallerEmail { get; set; }
        public string InstallerAdress { get; set; }
        public DateTime? AccExpDate { get; set; }
        public string AccCodeExpDate { get; set; }
        public DateTime? ElectExpDate { get; set; }
        public string ElectCodeExpDate { get; set; }
        public DateTime? DesignExpDate { get; set; }
        public string DesignCodeExpDate { get; set; }
        public string PVDStatusName { get; set; }

        public string adress { get; set; }

        public decimal? SystemCapacity { get; set; }

        public string ModelName { get; set; }

        public string Note { get; set; }

        public int JobID { get; set; }

        //public string Panel { get; set; }

        //public string PanelModelNo { get; set; }

        //public string Inverter { get; set; }

        //public string InverterModelNo { get; set; }

        //public int? PanelQTY { get; set; }

        //public int? InverterQTY { get; set; }

        //public string PanelNAme { get; set; }

        //public string InverterNAme { get; set; }

        public string Mobileno { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string filepath { get; set; }

        public string filename { get; set; }

        public int? DocumentTypeId { get; set; }

        public string ApplicationNotes { get; set; }

        public DateTime? DistExpiryDate { get; set; }

        //public decimal? InverterOutPut { get; set; }

        public string InstallerDepartmentNotes { get; set; }

        //public decimal? Price { get; set; }

        public List<GetJobDocumentForView> Doc { get; set; }

        public List<DetailDto> InvPaymentList { get; set; }

        public string FinanceNotes { get; set; }
        public string GridConnectionNotes { get; set; }

        public string Finance { get; set; }
        public string RebateAppRef { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string VicRebate { get; set; }
        public decimal? SolarVICRebate { get; set; }
        public decimal? SolarVICLoanDiscont { get; set; }
        public int? SolarRebateStatus { get; set; }
        public string vicRebateNotes { get; set; }
        public DateTime? FinanceApplicationDate { get; set; }
        public DateTime? ApplicationDate { get; set; }

        public DateTime? ApproveDate { get; set; }
        public string FinancePurchaseNo { get; set; }
        public DateTime? DocumentsVeridiedDate { get; set; }
        public DateTime? CoolingoffPeriodEnd { get; set; }
        public string DocumentsVeridiedByName { get; set; }
        public string PeakMeterNo { get; set; }

        public string OffPeakMeter { get; set; }
        public int? MeterPhaseId { get; set; }
        public string EnoughMeterSpace { get; set; }
        public string IsSystemOffPeak { get; set; }
        public int? FinanceDocumentVerified { get; set; }
        public int? ActualPanelsInstalled { get; set; }
        public string AllGoodNotGood { get; set; }
        public int? CES { get; set; }
        public int? CxSign { get; set; }
        public string EmailSent { get; set; }
        public int? Frontofproperty { get; set; }
        public int? InstDesElesign { get; set; }
        public int? InstalaltionPic { get; set; }
        public string InstMaintInspPic { get; set; }
        public int? Installerselfie { get; set; }
        public int? InverterSerialNo { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public string InvoiceNo { get; set; }
        public string NotesOrReasonforPending { get; set; }
        public string Remarkifowing { get; set; }
        public string otherExtraInvoiceNumber { get; set; }
        public string TravelKMfromWarehouse { get; set; }
        public int? NoofPanelsinvoice { get; set; }
        public int? NoofPanelsPortal { get; set; }
        public int? PanelsSerialNo { get; set; }
        public int? Splits { get; set; }
        public int? Traded { get; set; }
        public int? WiFiDongle { get; set; }
        public string Filenameinv { get; set; }
        public string FilePathinv { get; set; }

        public string gridFilename { get; set; }
        public string gridFilePath { get; set; }
        public DateTime? creationTime { get; set; }
        public DateTime? DepositeRecceivedDate { get; set; }
        public DateTime? ActiveDate { get; set; }
        public DateTime? FinanceDocReceivedDate { get; set; }
        public DateTime? QuoteCreated { get; set; }
        public string createdBy { get; set; }
        public List<installerinvoicefileDto> filelist { get; set; }
        public List<JobApprovalFileHistoryDto> ApprovalFilelist { get; set; }
        public List<JobApprovalFileHistoryDto> SubCatServieDocFilelist { get; set; }
        public string AccountName { get; set; }
        public string BsbNo { get; set; }
        public string AccountNo { get; set; }
        public decimal? ReferralAmount { get; set; }
        public string refferedJobName { get; set; }
        public virtual DateTime? ReferralPayDate { get; set; }
        public virtual bool ReferralPayment { get; set; }
        public string BankReferenceNo { get; set; }
        public string ReferralRemark { get; set; }

        public List<NamingValueDto> Variations { get; set; }

        public List<JobProductItems> JobProducts { get; set; }
    }
}

public class JobProductItems
{
    public string Category { get; set; }

    public string Name { get; set; }

    public int? Quantity  { get; set; }
}

public class installerinvoicefileDto
{
    public string documentPath { get; set; }

    public string fileName { get; set; }
    
    public DateTime? creationTime { get; set; }
    
    public string createdBy { get; set; }

}
public class JobApprovalFileHistoryDto
{
    public string documentPath { get; set; }

    public string fileName { get; set; }
    
    public DateTime? creationTime { get; set; }
    
    public string createdBy { get; set; }
}