﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditJobDto : EntityDto<int?>
    {

        public string Note { get; set; }
        public string OldSystemDetails { get; set; }

        public string InstallerNotes { get; set; }

        public int? PanelOnFlat { get; set; }

        public int? PanelOnPitched { get; set; }

        public string NMINumber { get; set; }

        public string RegPlanNo { get; set; }

        public string LotNumber { get; set; }

        public string PeakMeterNo { get; set; }

        public string OffPeakMeter { get; set; }

        public bool EnoughMeterSpace { get; set; }

        public bool IsSystemOffPeak { get; set; }

        public bool IsFinanceWithUs { get; set; }

        public decimal? BasicCost { get; set; }

        public decimal? SolarVLCRebate { get; set; }

        public decimal? SolarVLCLoan { get; set; }

        public decimal? TotalCost { get; set; }

        public string Suburb { get; set; }

        public int? SuburbId { get; set; }

        public string State { get; set; }

        public int? StateId { get; set; }

        public string PostalCode { get; set; }

        public string UnitNo { get; set; }

        public string UnitType { get; set; }

        public string StreetNo { get; set; }

        public string StreetName { get; set; }

        public string StreetType { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public decimal? SystemCapacity { get; set; }

        public decimal? STC { get; set; }

        public decimal? STCPrice { get; set; }

        public decimal? Rebate { get; set; }

        public string Address { get; set; }

        public string Country { get; set; }

        public int? JobTypeId { get; set; }

        public int? JobStatusId { get; set; }

        public int? RoofTypeId { get; set; }

        public int? RoofAngleId { get; set; }

        public int? ElecDistributorId { get; set; }

        public int? LeadId { get; set; }

        public int? ElecRetailerId { get; set; }

        public int? PaymentOptionId { get; set; }

        public int? DepositOptionId { get; set; }

        public int? MeterUpgradeId { get; set; }

        public int? MeterPhaseId { get; set; }

        public int? PromotionOfferId { get; set; }

        public int? HouseTypeId { get; set; }

        public int? FinanceOptionId { get; set; }

        public DateTime? DistAppliedDate { get; set; }

        public string MeterNumber { get; set; }

        public string ApplicationRefNo { get; set; }
        public string ApprovalRef { get; set; }

        public string AdditionalComments { get; set; }

        public string JobNumber { get; set; }

        public bool? IsRefund { get; set; }

        public bool? RefundProcess { get; set; }

        public DateTime? InstalledcompleteDate { get; set; }
        
        public string IncompleteReason { get; set; }
        
        public string MeterApplyRef { get; set; }
        
        public string InspectorName { get; set; }
        
        public DateTime? InspectionDate { get; set; }
        
        public string ComplianceCertificate { get; set; }
        
        public int? PostInstallationStatus { get; set; }

        public DateTime? STCUploaddate { get; set; }
        
        public int? PVDStatus { get; set; }
        
        public DateTime? STCAppliedDate { get; set; }
        
        public string PVDNumber { get; set; }
        
        public string STCNotes { get; set; }
        
        public string STCUploadNumber { get; set; }
        
        public string Quickformid { get; set; }
        
        public TimeSpan? MeterTime { get; set; }

        public DateTime? FinanceApplicationDate { get; set; }
        
        public string FinancePurchaseNo { get; set; }
        
        public DateTime? FinanceDocSentDate { get; set; }
        
        public int? FinanceAppliedBy { get; set; }
        
        public int? FinanceDocSentBy { get; set; }
        
        public DateTime? FinanceDocReceivedDate { get; set; }
        
        public int? FinanceDocReceivedBy { get; set; }


        /// <summary>
        /// 1 = Verify
        /// 2 = Not Verify
        /// 3 = Query
        /// </summary>
        public int? FinanceDocumentVerified { get; set; }

        public bool? IsActive { get; set; }

        public DateTime? ActiveDate { get; set; }

        public string RebateAppRef { get; set; }

        public DateTime? ApprovalDate { get; set; }

        public DateTime? ExpiryDate { get; set; }

        public string VicRebate { get; set; }

        public decimal? SolarVICRebate { get; set; }

        public decimal? SolarVICLoanDiscont { get; set; }

        public int? SolarRebateStatus { get; set; }

        public string VicRebateNotes { get; set; }

        public  DateTime? DistApplied { get; set; }

        public  int? DistApproveBy { get; set; }

        public DateTime? DistExpiryDate { get; set; }

        public  int? AppliedBy { get; set; }

        public  DateTime? DistApproveDate { get; set; }

        public  int? EmpId { get; set; }

        public  decimal? PaidAmmount { get; set; }


        public  int? Paidby { get; set; }

        public  int? PaidStatus { get; set; }

        public  string InvRefNo { get; set; }

        public  DateTime? InvPaidDate { get; set; }

        public string ProjectNotes { get; set; }

        public string ApplicationNotes { get; set; }

        public DateTime? DocumentsVeridiedDate { get; set; }

        public int? DocumentsVeridiedBy { get; set; }

        public DateTime? CoolingoffPeriodEnd { get; set; }

        public string FinanceNotes { get; set; }

        public List<CreateOrEditJobProductItemDto> JobProductItems { get; set; }

        public List<CreateOrEditJobVariationDto> JobVariation { get; set; }

        public List<CreateOrEditJobPromotionDto> JobPromotion { get; set; }
        public List<CreateOrEditJobOdSysDetails> JobOldSystemDetail { get; set; }
        public string ManualQuote { get; set; }
        public decimal? ApprovedCapacityonExport { get; set; }

        public decimal? ApprovedCapacityonNonExport { get; set; }

        public  decimal? DepositRequired { get; set; }

        public  bool? SmsSend { get; set; }
        public  DateTime? SmsSendDate { get; set; }
        public  bool? EmailSend { get; set; }
        public  DateTime? EmailSendDate { get; set; }

        public  bool? FinanceSmsSend { get; set; }
        public  DateTime? FinanceSmsSendDate { get; set; }
        public  bool? FinanceEmailSend { get; set; }
        public  DateTime? FinanceEmailSendDate { get; set; }

        public  bool? JobActiveSmsSend { get; set; }
        public  DateTime? JobActiveSmsSendDate { get; set; }
        public  bool? JobActiveEmailSend { get; set; }
        public  DateTime? JobActiveEmailSendDate { get; set; }


        public  bool? GridConnectionSmsSend { get; set; }
        public  DateTime? GridConnectionSmsSendDate { get; set; }
        public  bool? GridConnectionEmailSend { get; set; }
        public  DateTime? GridConnectionEmailSendDate { get; set; }

        public  bool? StcSmsSend { get; set; }
        public  DateTime? StcSmsSendDate { get; set; }
        public  bool? StcEmailSend { get; set; }
        public  DateTime? StcEmailSendDate { get; set; }

        public  bool? JobGridSmsSend { get; set; }
        public  DateTime? JobGridSmsSendDate { get; set; }
        public  bool? JobGridEmailSend { get; set; }
        public  DateTime? JobGridEmailSendDate { get; set; }

        public string JobCancelRequestReason { get; set; }

        public bool? IsJobCancelRequest { get; set; }

        public string JobCancelReason { get; set; }

        public int? JobCancelReasonId { get; set; }
        public string JobCancelRejectReason { get; set; }
        public string InstallationNotes { get; set; }

        public string GridConnectionNotes { get; set; }
         
        public DateTime? DepositeRecceivedDate { get; set; }
        public virtual string FileName { get; set; }
        public virtual string FilePath { get; set; } 
        public string FileToken { get; set; }

        
             public virtual string ApprovalLetter_Filename { get; set; }
        public virtual string ApprovalLetter_FilePath { get; set; }

        public int? Applicationfeespaid { get; set; }
        public virtual string Applicationfeespaidstatus { get; set; }

        public bool? PendingInstallerSmsSend { get; set; }
        public DateTime? PendingInstallerSmsSendDate { get; set; }
        public bool? PendingInstallerEmailSend { get; set; }
        public DateTime? PendingInstallerEmailSendDate { get; set; }
        public virtual int? RefferedJobStatusId { get; set; }

        public virtual int? RefferedJobId { get; set; }

        public string AccountName { get; set; }
        public string BsbNo { get; set; }
        public string AccountNo { get; set; }
        public decimal? ReferralAmount { get; set; }
        public string refferedJobName { get; set; }
        public virtual DateTime? ReferralPayDate { get; set; }
        public virtual bool ReferralPayment { get; set; }
        public string BankReferenceNo { get; set; }
        public string ReferralRemark { get; set; }
        public int? RemoveOldSys { get; set; }
        public bool?  IsRefferalVerify { get; set; }
        public string DepositeStatusNotes { get; set; }
        public string ActiveStatusNotes { get; set; }
        public int? ReviewRating { get; set; }
        public string ReviewNotes { get; set; }
        public int? ReviewType { get; set; }
        public bool? ReviewSmsSend { get; set; }
        public DateTime? ReviewSmsSendDate { get; set; }
        public bool? ReviewEmailSend { get; set; }
        public DateTime? ReviewEmailSendDate { get; set; }
        public int? JobHoldReasonId { get; set; }
        public string JobHoldReason { get; set; }
        public int? ExportDetail { get; set; }
        public decimal? Exportkw { get; set; }
        public decimal? NonExportDetail { get; set; }
        public bool? IsGridConnected { get; set; }
        public string ApproxFeedTariff { get; set; }
        public string ExpectedPaybackPeriod { get; set; }

        public int? ProductPackageId { get; set; }

        public string BlackOutProtection { get; set; }

        public decimal FinanceAmount { get; set; }

        public decimal FinanceDepositeAmount { get; set; }

        public decimal FinanceNetAmount { get; set; }

        public string InverterLocation { get; set; }

        public virtual DateTime? FirstDepositDate { get; set; }

        public  int? SectionId { get; set; }

        public string PriceType { get; set; }

		public int? VppConnectionId { get; set; }

        public int orgid { get; set; }

        public DateTime? VICQRcodeScanDate { get; set; }

        public decimal? InstallCost { get; set; }

        public decimal? ServiceCost { get; set; }

        public string PaymentTerm { get; set; }

        public decimal? RepaymentAmt { get; set; }

        public decimal? CategoryDDiscountAmt { get; set; }

        public string CategoryDNotes { get; set; }

        public bool IsLeadAddresschange { get; set; }

        public decimal? BatteryKw { get; set; }

        public string RebateRefNo{ get; set; }

        public string HouseAge { get; set; }

        public bool SiteVisit { get; set; }
        public  DateTime? EssentialDistApplied { get; set; }

        public  string EssentialApprovalRef { get; set; }

        public string Section { get; set; }
		public string LocationOfSwitchBoard { get; set; }
        public Boolean Bonus { get; set; }
        public Boolean? IsBroadbandWifiConnection { get; set; }
        
        public decimal? BonusAmt { get; set; }

        public decimal? BatteryBess { get; set; }

        public decimal? BatteryBessPerPrice { get; set; }

        public decimal? BatteryRebate { get; set; }

        public bool? IsBatteryRebate { get; set; }

        public decimal? BatteryBessState { get; set; }

        public decimal? BatteryBessPerPriceState { get; set; }
        public string GWTId { get; set; }
        public string FlexibleRange {  get; set; }
    }

    public class TokenData
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
    public class RootObject
    {
        public TokenData TokenData { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string access_token { get; set; }
    }
    public class PayWayRootObject
    {
        //public paywayTokenObject TokenData { get; set; }
        public string singleUseTokenId { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string access_token { get; set; }
        public string customerNumber { get; set; }
    }
    public class PayWayTransactionObject
    {
        //public paywayTokenObject TokenData { get; set; }
        public string transactionId { get; set; }
        public string receiptNumber { get; set; }
        public string status { get; set; }
        public string responseCode { get; set; }
        public string responseText { get; set; }
        public string transactionType { get; set; }
        public string customerNumber { get; set; }
        public string customerName { get; set; }
        public string currency { get; set; }
        public decimal principalAmount { get; set; }
        public decimal surchargeAmount { get; set; }
        public decimal paymentAmount { get; set; }
        public string paymentMethod { get; set; }
    }
    public class paywayTokenObject
    {
        public string singleUseTokenId { get; set; }
        public string paymentMethod { get; set; }
    }
    public class TokenObject
    {
        public string access_token { get; set; }
        public string CustomerUserID { get; set; }
    }
    public class RootObj
    {
        public string Message { get; set; }
        public string guidvalue { get; set; }
        public string code { get; set; }
    }
    public partial class Panel
    {
        //[JsonProperty("Brand")]
        public string Brand { get; set; }

        //[JsonProperty("Model")]
        public string Model { get; set; }

        //[JsonProperty("NoOfPanel")]
        public string NoOfPanel { get; set; }

        //[JsonProperty("size")]
        public string size { get; set; }
        //[JsonProperty("STC")]
        public string STC { get; set; }
    }
    public partial class Inverter
    {
        //[JsonProperty("Brand")]
        public string Brand { get; set; }

        //[JsonProperty("Series")]
        public string Series { get; set; }

        //[JsonProperty("Model")]
        public string Model { get; set; }
        //[JsonProperty("size")]
        public string size { get; set; }
        //[JsonProperty("noofinverter")]
        public int noofinverter { get; set; }
    }

    public partial class BatteryManufacturer
    {
        public string Brand { get; set; }

        public string Model { get; set; }

        public string BatterySystemPartOfAnAggregatedControl { get; set; }
        
        public string ChangedSettingOfBatteryStorageSystem { get; set; }
    }

    public partial class CreateJob
    {
        //[JsonProperty("VendorJobId")]
        public int VendorJobId { get; set; }

        //[JsonProperty("Arisesolarid")]
        public int Arisesolarid { get; set; }

        //[JsonProperty("BasicDetails")]
        public BasicDetails BasicDetails { get; set; }

        //[JsonProperty("InstallerView")]
        public InstallerView InstallerView { get; set; }

        //[JsonProperty("ElectricianView")]
        public JobElectricians JobElectricians { get; set; }

        //[JsonProperty("DesignerView")]
        public DesignerView DesignerView { get; set; }

        //[JsonProperty("JobInstallationDetails")]
        public JobInstallationDetails JobInstallationDetails { get; set; }

        //[JsonProperty("JobOwnerDetails")]
        public JobOwnerDetails JobOwnerDetails { get; set; }

        //[JsonProperty("JobSystemDetails")]
        public JobSystemDetails JobSystemDetails { get; set; }

        //[JsonProperty("JobSTCDetails")]
        public JobStcDetails JobStcDetails { get; set; }

        //[JsonProperty("panel")]
        public Panel[] Panel { get; set; }

        //[JsonProperty("inverter")]
        public Inverter[] Inverter { get; set; }

        public BatteryManufacturer[] BatteryManufacturer { get; set; }

        //[JsonProperty("lstJobNotes")]
        //public LstJobNote[] LstJobNotes { get; set; }

        //[JsonProperty("lstCustomDetails")]
        //public LstCustomDetail[] LstCustomDetails { get; set; }

        public string quickformGuid { get; set; }

    }

    public partial class BasicDetails
    {
   
        public int JobType { get; set; }
     
        public int Jobstatus { get; set; }

      
        public string RefNumber { get; set; }

   
        public int JobStage { get; set; }

      
        public string Description { get; set; }

     
        public string Title { get; set; }

    
        
        public DateTime StrInstallationDate { get; set; }
        public DateTime InstallationDate { get; set; }


        public int Priority { get; set; }
    }

    public partial class InstallerView
    {
        public string FirstName { get; set; }
     
        public string LastName { get; set; }

        public string PostCode { get; set; }

        //[JsonProperty("State")]
        public string State { get; set; }

        public string StreetName { get; set; }

        public string StreetNumber { get; set; }
   
        public int StreetTypeId { get; set; }
     
        public int AddressID { get; set; }
      
        public string Town { get; set; }
   
        public string CECAccreditationNumber { get; set; }
    
        public int SeDesignRoleId { get; set; }
     
        public string Phone { get; set; }

        public string Mobile { get; set; }
     
        public bool IsPostalAddress { get; set; }

        public bool IsAutoAddVisit { get; set; }
     
    }

    public partial class JobElectricians
    {
      
        public string FirstName { get; set; }
  //[JsonProperty("LastName")]
        public string LastName { get; set; }

        public string Email { get; set; }

     
        public string PostCode { get; set; }

     
        public string State { get; set; }


        public string StreetName { get; set; }

      
        public string StreetNumber { get; set; }

        public int StreetTypeId { get; set; }

 
        public int AddressID { get; set; }

    
        public string Town { get; set; }

       
        public string CECAccreditationNumber { get; set; }

      
        public string LicenseNumber { get; set; }
        public int SeDesignRoleId { get; set; }

    
        public string Phone { get; set; }

   
        public string Mobile { get; set; }

     
     
        public bool IsPostalAddress { get; set; }
       
   
    }

    public partial class DesignerView
    {
   
        public string FirstName { get; set; }

     
        public string LastName { get; set; }

        public string PostCode { get; set; }

       
        public string State { get; set; }

     
        public string StreetName { get; set; }

       
        public string StreetNumber { get; set; }

       
        public int StreetTypeId { get; set; }

   
        public int AddressID { get; set; }

       
        public string Town { get; set; }

       
        public string CECAccreditationNumber { get; set; }

        public bool IsUpdate { get; set; }
        public int SeDesignRoleId { get; set; }

        
        public string Phone { get; set; }

     
        public string Mobile { get; set; }

        public bool IsPostalAddress { get; set; }
      
        public string IsMoreSGU { get; set; }
     
    }
    public partial class JobInstallationDetails
    {
   
        public int? UnitTypeID { get; set; }

       
        public string UnitNumber { get; set; }

      
        public string PostCode { get; set; }

        public int DistributorID { get; set; }
        public string State { get; set; }

      
        public string StreetName { get; set; }

      
        public string StreetNumber { get; set; }

      
        public int StreetTypeId { get; set; }

    
        public string Town { get; set; }

      
        public int AddressId { get; set; }

        public int PostalAddressId { get; set; }
        public string PropertyType { get; set; }

     
        public string SingleMultipleStory { get; set; }

    
        public string InstallingNewPanel { get; set; }

       
        public bool ExistingSystem { get; set; }

   
        public int NoOfPanels { get; set; }

       
        public int ElectricityProviderID { get; set; }

       
        public string NMI { get; set; }

      
        public string MeterNumber { get; set; }

      
        public string PhaseProperty { get; set; }

    }

    public partial class JobOwnerDetails
    {
       
        public string OwnerType { get; set; }

      
        public string CompanyName { get; set; }

   
        public string FirstName { get; set; }

    
        public string LastName { get; set; }

       
        public string Email { get; set; }

      
        public string Phone { get; set; }

        public string Mobile { get; set; }


        public string UnitNumber { get; set; }

    
        public int? UnitTypeID { get; set; }

    
        public string StreetNumber { get; set; }

      
        public string StreetName { get; set; }


        public int StreetTypeId { get; set; }

        
        public string StreetAddress { get; set; }


   
        public string Town { get; set; }

   
        public string State { get; set; }

      
        public string PostCode { get; set; }


       
        public int AddressID { get; set; }

    
        public bool IsPostalAddress { get; set; }
        
    }

    public partial class JobStcDetails
    {
     
        public string TypeOfConnection { get; set; }

   
        public string SystemMountingType { get; set; }

      
        public string DeemingPeriod { get; set; }

   
        public string MultipleSguAddress { get; set; }

        public DateTime InstallationDate { get; set; }
        //public string Systembrand { get; set; }
        //public string SystemModel { get; set; }
        //public string Installationpostcode { get; set; }


    }
    public partial class JobStcDetailsPanelBrand
    {
       
        public string Brand { get; set; }

        
        public string Model { get; set; }

       
        public string NoOfPanel { get; set; }
    }

    public partial class JobSystemDetails
    {
       
        public string SystemModel { get; set; }

   
        public decimal SystemSize { get; set; }

     
        public int Systemtype { get; set; }

         
        public int ConnectionType { get; set; }

       
        public int MountingType { get; set; }

       
        public string SerialNumbers { get; set; }

      
        public int NoOfPanel { get; set; }
            
        public string InstallationType { get; set; }

    }

    public partial class LstCustomDetail
    {
         
        public long VendorJobCustomFieldId { get; set; }

       
        public string FieldValue { get; set; }

       
        public string FieldName { get; set; }
    }

    public partial class LstJobNote
    {
  
        public string VendorJobNoteId { get; set; }
 
        public string Notes { get; set; }
    }

    public partial class BridgeSelectJobDto
    {
        public string crmid { get; set; }

        public string at { get; set; }

        public string e { get; set; }

        public string fn { get; set; }

        public string id { get; set; }

        public string ie { get; set; }

        public string ifn { get; set; }

        public double ilat { get; set; }

        public string iln { get; set; }

        public double ilng { get; set; }

        public string im { get; set; }

        public string ipa { get; set; }

        public int ipc { get; set; }

        public string iph { get; set; }

        public string ipra { get; set; }

        public string isb { get; set; }

        public string ist { get; set; }

        public string istn { get; set; }

        public string istp { get; set; }


    }
}                     
                       
                       
                      
                      