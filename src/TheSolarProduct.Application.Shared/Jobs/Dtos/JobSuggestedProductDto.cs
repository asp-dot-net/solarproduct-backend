﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class JobSuggestedProductDto : EntityDto<int?>
    {
        public virtual int? JobId { get; set; }
        public virtual string Name { get; set; }
        public virtual decimal? SysCapacity { get; set; }
        public virtual decimal? Amount { get; set; }
        public List<JobSuggestedProductItemDto> JobSuggestedProductItemDto { get; set; }
    }
}
