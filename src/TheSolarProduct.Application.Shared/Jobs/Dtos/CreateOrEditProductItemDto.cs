﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Abp.Runtime.Validation;
using Abp.Extensions;

namespace TheSolarProduct.Jobs.Dtos
{
	public class CreateOrEditProductItemDto : EntityDto<int?>
	{

		[Required]
		[StringLength(ProductItemConsts.MaxNameLength, MinimumLength = ProductItemConsts.MinNameLength)]
		public string Name { get; set; }

		[StringLength(ProductItemConsts.MaxManufacturerLength, MinimumLength = ProductItemConsts.MinManufacturerLength)]
		public string Manufacturer { get; set; }

		[StringLength(ProductItemConsts.MaxModelLength, MinimumLength = ProductItemConsts.MinModelLength)]
		public string Model { get; set; }

		//[StringLength(ProductItemConsts.MaxSeriesLength, MinimumLength = ProductItemConsts.MinSeriesLength)]
		public string Series { get; set; }

		public decimal? Size { get; set; }

		public string ShortName { get; set; }

		[StringLength(ProductItemConsts.MaxDescriptionLength, MinimumLength = ProductItemConsts.MinDescriptionLength)]
		public string Description { get; set; }

		public int? Code { get; set; }

		public string InverterCert { get; set; }

		public int? StockQuantity { get; set; }

		public int? StockLocation { get; set; }

		public int? MinLevel { get; set; }

		public int? Reserved { get; set; }

		public decimal? MinPrice { get; set; }

		public decimal? CostPrice { get; set; }

		public decimal? CustomerPrice { get; set; }

		public DateTime? ApprovedDate { get; set; }

		public DateTime? ExpiryDate { get; set; }

		[StringLength(ProductItemConsts.MaxFireTestedLength, MinimumLength = ProductItemConsts.MinFireTestedLength)]
		public string FireTested { get; set; }

		[StringLength(ProductItemConsts.MaxACPowerLength, MinimumLength = ProductItemConsts.MinACPowerLength)]
		public string ACPower { get; set; }

		public string StockItem { get; set; }

		public string StockId { get; set; }

		public bool IsActive { get; set; }

		public string AUSStockQty { get; set; }

		public string AUSMinumumQty { get; set; }

		public bool AUSSalesTag { get; set; }

		public string NSWStockQty { get; set; }

		public string NSWMinimumQty { get; set; }

		public bool NSWSalesTag { get; set; }

		public string NTStockQty { get; set; }

		public string NTMinimumQty { get; set; }

		public bool NTSalesTag { get; set; }

		public string QLDStockQty { get; set; }

		public string QLDMinimumQty { get; set; }

		public bool QLDSalesTag { get; set; }

		public string SAStockQty { get; set; }

		public string SAMinimumQty { get; set; }

		public bool SASalesTag { get; set; }

		public string TASStockQty { get; set; }

		public string TASMinimumQty { get; set; }

		public bool TASSalesTag { get; set; }

		public string VICStockQty { get; set; }

		public string VICMinimumQty { get; set; }

		public bool VICSalesTag { get; set; }

		public string WAStockQty { get; set; }

		public string WAMinimumQty { get; set; }

		public bool WASalesTag { get; set; }

		public int ProductTypeId { get; set; }

		[MaxLength(400)]
		public string FileToken { get; set; }

		public virtual string FileName { get; set; }

		public virtual string FileType { get; set; }

		public virtual string FilePath { get; set; }

		public void AddValidationErrors(CustomValidationContext context)
		{
			if (FileToken.IsNullOrEmpty())
			{
				throw new ArgumentNullException(nameof(FileToken));
			}
		}

		public int? PerformanceWarranty { get; set; }

		public int? ProductWarranty { get; set; }
		
		public int? ExtendedWarranty { get; set; }
		
		public int? WorkmanshipWarranty { get; set; }
		
		public decimal? PricePerWatt { get; set; }
		
		public decimal? Amount { get; set; }

        public virtual bool ECommerce { get; set; }

		public string ImagePath { get; set; }

		public string ImageName { get; set; }

        public string ManuPhone { get; set; }

        public string ManuMobile { get; set; }

        public string ManuEmail { get; set; }

        public string ManuWebsite { get; set; }

        public bool IsScanned { get; set; }
    }
}