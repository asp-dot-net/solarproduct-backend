﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditFinanceOptionDto : EntityDto<int?>
    {
		public string Name { get; set; }
		
		public int? DisplayOrder { get; set; }

        public decimal? Percentage { get; set; }
        public Boolean? IsActive { get; set; }
    }
}