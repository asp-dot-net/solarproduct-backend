﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class GetPaymentOptionForEditOutput
    {
		public CreateOrEditPaymentOptionDto PaymentOption { get; set; }


    }
}