﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Dtos
{
    public class InstallerInvoicePriceListDto : EntityDto<int?>
    {
        public int? PriceItemId { get; set; }

        public decimal? Amount { get; set; }

        public int? InstallerInvoId { get; set; }

        public string Notes { get; set; }

        public string PriceItem { get; set; }

    }
}
