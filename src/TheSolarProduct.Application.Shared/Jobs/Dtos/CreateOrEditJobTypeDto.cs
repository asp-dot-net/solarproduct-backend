﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Jobs.Dtos
{
    public class CreateOrEditJobTypeDto : EntityDto<int?>
    {

		[Required]
		[StringLength(JobTypeConsts.MaxNameLength, MinimumLength = JobTypeConsts.MinNameLength)]
		public string Name { get; set; }
		
		public Boolean IsActive {  get; set; }
		public int? DisplayOrder { get; set; }
		
		

    }
}