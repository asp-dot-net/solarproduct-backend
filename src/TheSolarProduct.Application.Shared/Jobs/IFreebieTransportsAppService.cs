﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Jobs
{
    public interface IFreebieTransportsAppService : IApplicationService
    {
        Task<PagedResultDto<GetFreebieTransportForViewDto>> GetAll(GetAllFreebieTransportsInput input);

        Task<GetFreebieTransportForViewDto> GetFreebieTransportForView(int id);

        Task<GetFreebieTransportForEditOutput> GetFreebieTransportForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditFreebieTransportDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetFreebieTransportsToExcel(GetAllFreebieTransportsForExcelInput input);

    }
}