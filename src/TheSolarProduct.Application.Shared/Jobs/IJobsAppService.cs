﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.EmailTemplates.Dtos;
using TheSolarProduct.Installer.Dtos;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.JobTrackers.Dtos;
using TheSolarProduct.Quotations.Dtos;
using TheSolarProduct.SmsTemplates.Dtos;
using GetAllForLookupTableInput = TheSolarProduct.Jobs.Dtos.GetAllForLookupTableInput;

namespace TheSolarProduct.Jobs
{
	public interface IJobsAppService : IApplicationService
	{
		//Task<PagedResultDto<GetJobForViewDto>> GetAll(GetAllJobsInput input);

		Task<PagedResultDto<GetJobForViewDto>> GetAllForJobFinanceTracker(GetAllJobsInput input);

		//Task<PagedResultDto<GetJobForViewDto>> GetAllForJobActiveTracker(GetAllJobsInput input);

		Task<PagedResultDto<GetJobForViewDto>> GetAllJobGrid(GetAllJobsInput input);

		Task<FileDto> GetJobGirdTrackerProductToExcel(GetAllJobsForExcelInput input);

		Task<FileDto> GetJobGirdTrackerProductItemToExcel(GetAllJobsForExcelInput input);

        //Task<jobcountdata> getAlljobCount(GetAllJobsInput input);
        Task<GetJobForEditOutput> GetJobForEdit(EntityDto input);

		Task<GetDetailForJobEditOutPut> GetJobDetailEditByJobID(EntityDto input);

		Task<JobApplicationViewDto> GetDataForQuickViewByJobID(int Id, int? sectionid);

		Task CreateOrEdit(CreateOrEditJobDto input);

		Task UpdateDataFromTracker(CreateOrEditJobDto input);

		Task ActiveJob(CreateOrEditJobDto input);

		Task<JobInstallationDto> GetJobInstallationDetailsForEdit(EntityDto input);

		Task<JobActiveStatusDto> GetJobActiveStatus(EntityDto input);

		string GetJobStatus(EntityDto input);

		Task UpdateJobStatus(UpdateJobStatusDto input);
		Task UpdateCanacelationRequest(CreateOrEditJobDto input);

		Task UpdateJobInstallationDetails(JobInstallationDto input);

		Task Delete(EntityDto input);

		//Task<FileDto> GetJobsToExcel(GetAllJobsForExcelInput input);

		Task<List<JobJobTypeLookupTableDto>> GetAllJobTypeForTableDropdown();

		Task<List<JobJobStatusLookupTableDto>> GetAllJobStatusForTableDropdown();

		Task<List<JobRoofTypeLookupTableDto>> GetAllRoofTypeForTableDropdown();

		Task<List<JobRoofAngleLookupTableDto>> GetAllRoofAngleForTableDropdown();

		Task<List<JobElecDistributorLookupTableDto>> GetAllElecDistributorForTableDropdown();

		Task<List<JobLeadLookupTableDto>> GetAllLeadForTableDropdown();

		Task<List<JobElecRetailerLookupTableDto>> GetAllElecRetailerForTableDropdown();

		Task<List<JobPaymentOptionLookupTableDto>> GetAllPaymentOptionForTableDropdown();

		Task<List<JobDepositOptionLookupTableDto>> GetAllDepositOptionForTableDropdown();

		Task<List<JobMeterUpgradeLookupTableDto>> GetAllMeterUpgradeForTableDropdown();

		Task<List<JobMeterPhaseLookupTableDto>> GetAllMeterPhaseForTableDropdown();

		Task<List<InstallerAvailableListDto>> GetAllInstallerAvailabilityList(DateTime AvailableDate, int jobid);

		Task<List<InstallerAvailableListDto>> GetAllElectricianAvailabilityList(DateTime AvailableDate, int jobid);

		Task<List<InstallerAvailableListDto>> GetAllDesignerAvailabilityList(DateTime AvailableDate, int jobid);

		Task<List<CommonLookupDto>> GetWareHouseDropdown(string stateid);

		Task<PagedResultDto<JobPromotionOfferLookupTableDto>> GetAllPromotionOfferForLookupTable(GetAllForLookupTableInput input);

		Task<List<JobHouseTypeLookupTableDto>> GetAllHouseTypeForTableDropdown();

		Task<List<JobFinanceOptionLookupTableDto>> GetAllFinanceOptionForTableDropdown();

		Task<List<STCZoneRatingDto>> GetSTCZoneRating();

		Task<List<STCPostalCodeDto>> GetSTCPostalCode();

		Task<List<STCYearWiseRateDto>> GetSTCYearWiseRate();

		Task<List<CommonLookupDto>> GetAllCancelReasonForTableDropdown();

		Task<List<CommonLookupDto>> GetAllHoldReasonForTableDropdown();

		string GetVariationsAction(int? Id);

		Task JobActive(int Id);

		Task JobActiveLoop();

		//Task JobDeposite(int Id);

		Task Update_PostInstallationDetails(CreateOrEditJobDto input);

		Task Update_JOb_STCDetails(CreateOrEditJobDto input);

		//Task<PagedResultDto<GetJobForViewDto>> GetAllForSTCTrackerTracker(GetAllJobsInput input);

		Task<List<JobLeadLookupTableDto>> GetPVDStatusList();

		//Task<PagedResultDto<GetJobForViewDto>> GetGridConnectionTracker(GetAllJobsInput input);

		Task Update_MeterDetails(CreateOrEditJobDto input);

		Task<List<CommonLookupDto>> GetAllUsersTableDropdown();

		Task<Boolean> CheckExistJobsiteAddList(ExistJobSiteAddDto input);

		int checkjobcreatepermission(int leadid);

		Task<List<CommonLookupDto>> GetAllUsers();

		Task<TotalSammaryCountDto> getAllApplicationTrackerCount(int organizationid);

		Task<FileDto> getApplicationTrackerToExcel(GetAllJobApplicationTrackerInput input);

		Task<FileDto> getFinanaceTrackerToExcel(GetAllFinanceTrackerInput input);

		//Task<FileDto> getJobActiveTrackerToExcel(GetAllJobsForExcelInput input);

		Task<FileDto> getJobGirdTrackerToExcel(GetAllJobsForExcelInput input);

		Task<FileDto> getSTCTrackerToExcel(GetAllJobsForExcelInput input);

		//Task<FileDto> getGridConnectionTrackerToExcel(GetAllJobsForExcelInput input);

		Task<bool> CheckValidation(CreateOrEditJobDto input);

		Task SendSms(SmsEmailDto input);

		Task SendEmail(SmsEmailDto input);

		Task<List<GetProductItemSearchResult>> GetProductItemList(int productTypeId, string productItem, string state, bool salesTag);

		Task<List<string>> getinvertmodel(string invertmodel);

		Task<List<string>> getpanelmodel(string panelmodel);

		Task<GetSmsTemplateForEditOutput> GetSmsTemplateForEditForSms(EntityDto input);

		Task<GetEmailTemplateForEdit> GetEmailTemplateForEditForEmail(EntityDto input);

		//Task SendDocumentRequestLinkSMS(int input, int DocumentTypeId);

		Task<List<ProductItemAttachmentListDto>> ProductItemAttachmentList(int JobId);
		Task<List<PvdStatusDto>> GetAllPVDStatusDropdown();
		Task UpdateBookingManagerID(List<int> Jobids, int? userId);

		Task<List<CommonLookupDto>> GetUsers();
		Task<List<CommonLookupDto>> GetUsersListForToDo();
		Task<PagedResultDto<PvdStatusDto>> GetAllPvdStatusForGrid(GetAllRefundReasonsInput input);
		Task CreateOrEditStcStatus(CreateOrEditStcStatusDto input);
		Task<CreateOrEditStcStatusDto> GetstatusForEdit(EntityDto input);
		Task DeleteStcPvdStatus(EntityDto input);
		//List<TotalStcCountDto> getStcTrackerCount(GetAllJobsInput input);
		GetJobForEditOutput getJobdetailbylead(int? leadid);
		Task<PagedResultDto<ReferralGridDto>> GetReferralTrackerList(ReferralInputDto input);
		Task<FileDto> getRefferalTrackerToExcel(ReferralInputDto input);
		Task<List<jobnoLookupTableDto>> GetAlljobNumberforAdd(string leadSourceName);
		Task<PagedResultDto<WarrantyGridDto>> GetWarrantyTrackerList(WarrantyInputDto input);

		Task SaveDocument(UploadDocumentInput input);
		Task<List<GetDocumentTypeForView>> GetAllDocumentType();
		Task<List<GetDocumentTypeForView>> getRemainingDocumentType(int id);
		Task SaveDocumentforwarrantymail(UploadDocumentInput input);
		Task SendWarrantymail(SmsEmailDto input);
		Task deleteWarrantyattachment(int id);
		Task RevertApprovedInvoice(int id);
		Task<ListResultDto<SendEmailAttachmentDto>> getWarrantyattachment(int id);
		Task<TotalSammaryCountDto> getWarrantyTrackerCount(int organizationid);

		Task<List<GetJobOldSysDetaiilForEditOutpu>> getJobOldSystemDetailByJobId(int jobid);
		Task CreateGreenBoatData(int jobid, int ProviderId);
		Task CreatePaywayData(PayWayTokenData jobdata);

		Task verifyorNotVerifyJobRefund(int id, Boolean VerifyOrNot);
		Task<PagedResultDto<GetJobForViewDto>> GetAllJobHoldTrackerData(GetAllHoldJobInputDto input);
		Task<jobcountdata> getAllJobHoldTrackerCount(GetAllHoldJobInputDto input);
		Task<FileDto> getJobHoldTrackerToExcel(GetAllHoldJobInputDto input);

		Task<List<CalculateJobProductItemDeatilDto>> GetCalculateJobProductItemDeatils(List<CalculateJobProductItemDeatilDto> ItemsList);

		Task UpdateAddress(CreateOrEditJobDto input);

		Task<List<GetProductItemSearchResult>> GetPicklistProductItemList(int productTypeId, string productItem);

		Task<GetActualCostOutput> GetActualCost(GetActualCostInput input);

		Task GetPylon(EntityDto input, int SectionId);

		Task<List<string>> GetBatteryModel(string betterymodel);

        Task<FileDto> GetAllPvdStatusForGridExcel(GetAllPvdStatusForGridExcelInput input);

		Task<string> GetRefNoForItme(int itemId, int qty);

		Task<List<GetActualCostOutput>> GetActualCostList(GetActualCostInput input);
		Task UpdateGWTID(int jobid, string GWTID);

    }
}