﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Jobs
{
    public interface IDepositOptionsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetDepositOptionForViewDto>> GetAll(GetAllDepositOptionsInput input);

        Task<GetDepositOptionForViewDto> GetDepositOptionForView(int id);

		Task<GetDepositOptionForEditOutput> GetDepositOptionForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditDepositOptionDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetDepositOptionsToExcel(GetAllDepositOptionsForExcelInput input);

		
    }
}