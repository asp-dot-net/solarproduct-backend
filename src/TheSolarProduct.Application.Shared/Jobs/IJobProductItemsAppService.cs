﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using System.Collections.Generic;


namespace TheSolarProduct.Jobs
{
    public interface IJobProductItemsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetJobProductItemForViewDto>> GetAll(GetAllJobProductItemsInput input);

		Task<GetJobProductItemForEditOutput> GetJobProductItemForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditJobProductItemDto input);

		Task Delete(EntityDto input);

		
		//Task<List<JobProductItemJobLookupTableDto>> GetAllJobForTableDropdown();
		
		Task<List<JobProductItemProductItemLookupTableDto>> GetAllProductItemForTableDropdown();
		Task<List<JobProductItemProductItemLookupTableDto>> GetAllProductItemForTableDropdownForEdit();
		Task<List<GetJobProductItemForEditOutput>> GetJobProductItemByJobId(int jobid);


	}
}