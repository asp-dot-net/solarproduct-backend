﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.MyLeads.Dtos
{
	public class GetMyLeadsActivityLogViewDto : FullAuditedEntity
	{
		public virtual int LeadId { get; set; }

		public virtual string CreatorUserName { get; set; }		

		public virtual string LeadCompanyName { get; set; }

		public virtual int ActionId { get; set; }

		public virtual string ActionName { get; set; }

		public virtual string ActionNote { get; set; }
        public string Section { get; set; }
    }
}
