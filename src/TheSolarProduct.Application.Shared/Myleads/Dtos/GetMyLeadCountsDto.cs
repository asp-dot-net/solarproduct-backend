﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.MyLeads.Dtos
{
	public class GetMyLeadCountsDto
	{
		public string UnHandled { get; set; }

		public string New { get; set; }

		public string Hot { get; set; }

		public string Cold { get; set; }

		public string Warm { get; set; }

		public string Upgrade { get; set; }

		public string Rejected { get; set; }

		public string Cancelled { get; set; }

		public string Closed { get; set; }
	}
}
