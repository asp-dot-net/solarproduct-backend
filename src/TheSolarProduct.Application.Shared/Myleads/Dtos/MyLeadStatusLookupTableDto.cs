﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.MyLeads.Dtos
{
	public class MyLeadStatusLookupTableDto
	{
		public int Value { get; set; }

		public string Name { get; set; }
	}
}
