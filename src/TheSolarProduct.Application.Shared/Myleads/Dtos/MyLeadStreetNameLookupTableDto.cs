﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.MyLeads.Dtos
{
	public class MyLeadStreetNameLookupTableDto
	{
		public int Id { get; set; }

		public string DisplayName { get; set; }
	}
}
