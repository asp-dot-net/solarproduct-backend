﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.MyLeads.Dtos
{
    public class GetAllForLookupTableInputMyLead : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}