﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.MyLeads.Dtos
{
	public class MyLeadUsersLookupTableDto
	{
		public long Id { get; set; }

		public string DisplayName { get; set; }
	}
}
