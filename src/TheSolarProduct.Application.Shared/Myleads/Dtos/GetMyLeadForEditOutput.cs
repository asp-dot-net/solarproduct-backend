﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.MyLeads.Dtos
{
    public class GetMyLeadForEditOutput
	{
		public CreateOrEditMyLeadDto Lead { get; set; }

		public string PostCodeSuburb { get; set;}

		public string StateName { get; set;}

		public string PostCodePostalCode2 { get; set;}

		public string LeadSourceName { get; set;}

		public string LeadStatusName { get; set; }
	}
}