﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.MyLeads.Dtos
{
    public class GetMyLeadForChangeStatusOutput : EntityDto
	{
		public string CompanyName { get; set; }

		public int? LeadStatusID { get; set; }

		public string StatusName { get; set; }
	}
}
