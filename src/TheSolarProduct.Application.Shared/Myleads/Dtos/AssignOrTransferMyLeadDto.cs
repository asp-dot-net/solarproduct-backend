﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.MyLeads.Dtos
{
	public class AssignOrTransferMyLeadDto : EntityDto
	{
		public virtual int? LeadStatusID { get; set; }

		public virtual int? AssignToUserID { get; set; }
	}
}
