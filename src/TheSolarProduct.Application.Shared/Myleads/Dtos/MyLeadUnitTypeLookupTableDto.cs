﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.MyLeads.Dtos
{
	public class MyLeadUnitTypeLookupTableDto
	{
		public int Id { get; set; }

		public string DisplayName { get; set; }
	}
}
