﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.MyLeads.Dtos
{
	public class GetAllMyLeadsInput : PagedAndSortedResultRequestDto
	{
		public string Filter { get; set; }

		public string CopanyNameFilter { get; set; }

		public string EmailFilter { get; set; }

		public string PhoneFilter { get; set; }

		public string MobileFilter { get; set; }

		public string AddressFilter { get; set; }

		public string RequirementsFilter { get; set; }

		public string PostCodeSuburbFilter { get; set; }

		public string StateNameFilter { get; set; }

		public string PostCodeFilter { get; set; }

		public string LeadSourceNameFilter { get; set; }

		public string leadStatusName { get; set; }

		public int? leadStatusId { get; set; }

		public DateTime? StartDate { get; set; }

		public DateTime? EndDate { get; set; }
	}
}