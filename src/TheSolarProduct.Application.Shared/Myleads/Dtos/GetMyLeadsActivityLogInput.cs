﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.MyLeads.Dtos
{
	public class GetMyLeadsActivityLogInput
	{
		public int LeadId { get; set; }
	}
}
