﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.MyLeads.Dtos
{
    public class MyLeadSubSourceLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}