﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.MyLeads.Dtos
{
	public class MyLeadDto : EntityDto
	{
		public string CompanyName { get; set; }

		public string Email { get; set; }

		public string Phone { get; set; }

		public string Mobile { get; set; }

		public string Address { get; set; }

		public string Requirements { get; set; }


		public string Suburb { get; set; }

		public string State { get; set; }

		public string PostCode { get; set; }

		public string LeadSource { get; set; }


		public string UnitNo { get; set; }

		public string UnitType { get; set; }

		public string StreetNo { get; set; }

		public string StreetName { get; set; }

		public string StreetType { get; set; }

		public int? LeadStatusID { get; set; }

		public int? AssignToUserID { get; set; }

	}
}