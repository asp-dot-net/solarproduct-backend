﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.MyLeads.Dtos
{
    public class MyLeadStateLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}