﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.MyLeads.Dtos
{
    public class MyLeadSourceLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}