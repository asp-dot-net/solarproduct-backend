﻿namespace TheSolarProduct.MyLeads.Dtos
{
    public class GetMyLeadForViewDto
	{
		public MyLeadDto Lead { get; set; }

		public string PostCodeSuburb { get; set;}

		public string StateName { get; set;}

		public string LeadSourceName { get; set;}

		public string LeadStatusName { get; set; }

		public bool WebDuplicate { get; set; }

		public bool Duplicate { get; set; }
	}
}