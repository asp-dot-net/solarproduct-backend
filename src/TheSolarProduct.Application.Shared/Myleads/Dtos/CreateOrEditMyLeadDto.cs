﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using TheSolarProduct.Leads;

namespace TheSolarProduct.MyLeads.Dtos
{
	public class CreateOrEditMyLeadDto : EntityDto<int?>
	{

		[Required]
		[StringLength(LeadConsts.MaxCopanyNameLength, MinimumLength = LeadConsts.MinCopanyNameLength)]
		public string CompanyName { get; set; }


		[Required]
		[RegularExpression(LeadConsts.EmailRegex)]
		[StringLength(LeadConsts.MaxEmailLength, MinimumLength = LeadConsts.MinEmailLength)]
		public string Email { get; set; }


		[Required]
		[RegularExpression(LeadConsts.PhoneRegex)]
		[StringLength(LeadConsts.MaxPhoneLength, MinimumLength = LeadConsts.MinPhoneLength)]
		public string Phone { get; set; }


		[Required]
		[RegularExpression(LeadConsts.MobileRegex)]
		[StringLength(LeadConsts.MaxMobileLength, MinimumLength = LeadConsts.MinMobileLength)]
		public string Mobile { get; set; }


		[Required]
		[StringLength(LeadConsts.MaxAddressLength, MinimumLength = LeadConsts.MinAddressLength)]
		public string Address { get; set; }


		[Required]
		[StringLength(LeadConsts.MaxRequirementsLength, MinimumLength = LeadConsts.MinRequirementsLength)]
		public string Requirements { get; set; }


		public string Suburb { get; set; }

		public string State { get; set; }

		[Required]
		public string PostCode { get; set; }

		public string LeadSource { get; set; }


		[StringLength(LeadConsts.MaxUnitNoLength, MinimumLength = LeadConsts.MinUnitNoLength)]
		public virtual string UnitNo { get; set; }

		[StringLength(LeadConsts.MaxUnitTypeLength, MinimumLength = LeadConsts.MinUnitTypeLength)]
		public virtual string UnitType { get; set; }

		[Required]
		[StringLength(LeadConsts.MaxStreetNoLength, MinimumLength = LeadConsts.MinStreetNoLength)]
		public virtual string StreetNo { get; set; }

		[Required]
		[StringLength(LeadConsts.MaxStreetNameLength, MinimumLength = LeadConsts.MinStreetNameLength)]
		public virtual string StreetName { get; set; }

		[Required]
		[StringLength(LeadConsts.MaxStreetTypeLength, MinimumLength = LeadConsts.MinStreetTypeLength)]
		public virtual string StreetType { get; set; }

		public virtual int? LeadStatusID { get; set; }

		public virtual string LeadStatusName { get; set; }

		public virtual int? AssignToUserID { get; set; }

		public virtual string latitude { get; set; }

		public virtual string longitude { get; set; }
	}
}