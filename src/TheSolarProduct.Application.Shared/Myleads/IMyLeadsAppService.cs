﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.MyLeads.Dtos;
using TheSolarProduct.Dto;
using System.Collections.Generic;
using Abp;

namespace TheSolarProduct.MyLeads
{
	public interface IMyLeadsAppService : IApplicationService
	{
		Task<PagedResultDto<GetMyLeadForViewDto>> GetAll(GetAllMyLeadsInput input);

		Task<GetMyLeadForViewDto> GetLeadForView(int id);

		Task<GetMyLeadForEditOutput> GetLeadForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditMyLeadDto input);

		Task Delete(EntityDto input);

		Task ChangeStatus(GetMyLeadForChangeStatusOutput input);

		Task<GetMyLeadForChangeStatusOutput> GetLeadForChangeStatus(EntityDto input);

		Task<FileDto> GetLeadsToExcel(GetAllMyLeadsForExcelInput input);

		Task<List<MyLeadUnitTypeLookupTableDto>> GetAllUnitTypeForUnitTypeTableDropdown();

		Task<List<MyLeadStreetNameLookupTableDto>> GetAllStreetNameForStreetNameTableDropdown();

		Task<List<MyLeadStreetTypeLookupTableDto>> GetAllStreetTypeForStreetTypeTableDropdown();		

		Task<List<MyLeadSuburbLookupTableDto>> GetAllSuburbForPostCodeTableDropdown();

		Task<List<MyLeadSuburbLookupTableDto>> GetAllPostCodeForTableDropdown();

		Task<List<MyLeadStateLookupTableDto>> GetAllStateForTableDropdown();

		Task<List<MyLeadSourceLookupTableDto>> GetAllLeadSourceForTableDropdown();

		Task<List<MyLeadStatusLookupTableDto>> GetAllLeadStatusForTableDropdown(string lead);

		NameValue<string> SendAndGetSelectedValue(NameValue<string> selectedCountries);

		int GetSuburbId(string PostCode);

		int StateId(string StateName);

		Task<GetMyLeadForAssignOrTransferOutput> GetLeadForAssignOrTransfer(EntityDto input);

		Task<List<MyLeadUsersLookupTableDto>> GetAllUserLeadAssignDropdown();

		Task AssignOrTransferLead(GetMyLeadForAssignOrTransferOutput input);

		Task<List<GetMyLeadsActivityLogViewDto>> GetLeadActivityLog(GetMyLeadsActivityLogInput input);

		GetMyLeadCountsDto GetLeadCounts();
	}
}