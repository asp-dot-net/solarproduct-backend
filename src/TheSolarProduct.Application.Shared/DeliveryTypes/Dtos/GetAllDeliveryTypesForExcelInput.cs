﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DeliveryTypes.Dtos
{
   public class GetAllDeliveryTypesForExcelInput
    {
        public string Filter { get; set; }
    }
}
