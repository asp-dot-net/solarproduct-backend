﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckApplications.Dtos;

namespace TheSolarProduct.DeliveryTypes.Dtos
{
    public class GetDeliveryTypesForEditOutput
    {
        public CreateOrEditDeliveryTypesDto DeliveryTypes { get; set; }
    }
}
