﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DeliveryTypes.Dtos
{
    public class DeliveryTypesDto : EntityDto
    {

        public string Name { get; set; }

        public bool? IsActive { get; set; }
    }
}
