﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DeliveryTypes.Dtos;

namespace TheSolarProduct.DeliveryTypes.Dtos
{
    public class GetDeliveryTypesForViewDto
    {
        public DeliveryTypesDto DeliveryTypes { get; set; }
    }
}
