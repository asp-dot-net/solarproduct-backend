﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DeliveryTypes.Dtos
{
    public class GetAllDeliveryTypesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
