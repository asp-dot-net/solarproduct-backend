﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.DeliveryTypes;
using TheSolarProduct.DeliveryTypes.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.DeliveryTypes
{
    public interface IDeliveryTypesAppService : IApplicationService
    {
        Task<PagedResultDto<GetDeliveryTypesForViewDto>> GetAll(GetAllDeliveryTypesInput input);

        Task<GetDeliveryTypesForEditOutput> GetDeliveryTypesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDeliveryTypesDto input);

        Task Delete(EntityDto input);
        Task<FileDto> GetDeliveryTypesToExcel(GetAllDeliveryTypesForExcelInput input);



    }
}
