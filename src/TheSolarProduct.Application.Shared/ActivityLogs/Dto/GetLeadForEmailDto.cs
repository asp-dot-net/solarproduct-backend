﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs.Dtos;

namespace TheSolarProduct.ActivityLogs.Dto
{
    public class GetLeadForSMSEmailDto 
    {
        public string Email { get; set; }

        public string Mobile { get; set; }

        public string CompanyName { get; set; }

        public List<SendEmailAttachmentDto> filelist { get; set; }

        public string Jobnumber { get; set; }

    }
}
