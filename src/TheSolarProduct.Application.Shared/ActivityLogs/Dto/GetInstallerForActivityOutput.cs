﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ActivityLogs.Dto
{
    public class GetInstallerForActivityOutput
    {

        public string CompanyName { get; set; }
        public string CreatedUser { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
    }
}
