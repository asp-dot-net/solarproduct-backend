﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.ActivityLogs.Dto
{
    public class GetLeadForSMSEmailTemplateDto : EntityDto
    {
        public string CompanyName { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }
        
        public string Phone { get; set; }

        public string Mobile { get; set; }

        public string CurrentAssignUserName { get; set; } 
        
        public string CurrentAssignUserMobile { get; set;}

        public string CurrentAssignUserEmail { get; set; }

        public string CurrentAssignUserCimetLink { get; set; }

        public string JobNumber { get; set; }

        public string SystemCapacity { get; set; }

        public string QunityAndModelList { get; set; }

        public string TotalQuotaion { get; set; }

        public DateTime? InstallationDate { get; set; }

        public string InstallerName { get; set;}

        public DateTime DispatchedDate { get; set; }

        public string TrackingNo { get; set; }

        public string TransportCompanyName { get; set; }

        public string TransportLink { get; set; }

        public string PromoType { get; set; }

        public string UserName { get; set;}

        public string UserEmail { get; set;}

        public string UserMobile { get; set;}

        public string Owing { get; set;}

        public string OrgName { get; set; }

        public string OrgEmail { get; set;}

        public string OrgMobile { get; set;}

        public string Orglogo { get; set;}

        public string Link { get; set;}

        public int? jobId { get; set; }

        public string PaymentOption { get; set; }

        public string FinPaymentType { get; set; }

        public string DepositOption { get; set; }

        public string FinancePurchaseNo { get; set; }

        public decimal? FinanceAmount { get; set; }

        public decimal? FinanceDepositeAmount { get; set; }

        public decimal? FinanceNetAmount { get; set; }

        public string PaymentTerm { get; set; }

        public decimal? RepaymentAmount { get; set; }

        public decimal? InvoiceAmount { get; set; }

        public decimal? ApproedAmount { get; set; }


    }
}
