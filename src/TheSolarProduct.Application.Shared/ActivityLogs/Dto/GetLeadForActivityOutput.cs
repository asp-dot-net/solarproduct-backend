﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ActivityLogs.Dto
{
    public class GetLeadForActivityOutput
    {
        public string JobNumber { get; set; }
        public string CurrentAssignUserName { get; set; }
        public string CompanyName { get; set; }

    }
}
