﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ActivityLogs.Dto
{
    public class ActivitylogInput : EntityDto
    {
        public int TenantId { get; set; }

        public int? leadId { get; set; }

        public int? SectionId { get; set; }

        public string ActivityNote { get; set; }

        public DateTime? ActivityDate { get; set; }

        public int? UserId { get; set; }

        public int? serviceId { get; set; }
    }
}
