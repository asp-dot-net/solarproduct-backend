﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ActivityLogs.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.WholeSaleActivitys.Dtos;

namespace TheSolarProduct.ActivityLogs
{
    public interface IActivityLogAppService : IApplicationService
    {
        Task<GetLeadForSMSEmailTemplateDto> GetLeadForSMSEmailTemplate(int id, int? JobPromoId,int? InstallInoiceId);

        Task SendSms(SmsEmailDto input);
               
        Task SendEmail(SmsEmailDto input);

        Task AddNotifyActivityLog(ActivitylogInput input);

        Task AddReminderActivityLog(ActivitylogInput input);

        Task AddToDoActivityLog(ActivitylogInput input);

        Task AddCommentActivityLog(ActivitylogInput input);

        Task<GetLeadForActivityOutput> GetLeadForActivity(int id);

        Task<GetLeadForSMSEmailDto> GetLeadForSMSEmailActivity(int id);
        //Task<GetLeadForSMSEmailTemplateDto> GetLeadForSMSEmailTemplateForInvoiceIssued(int id, int? JobPromoId);
        Task<GetLeadForSMSEmailDto> GetLeadForSMSEmailActivityInvoiceIssued(int id);

        Task<GetInstallerForActivityOutput> GetInstallerForActivity(int id);


    }
}
