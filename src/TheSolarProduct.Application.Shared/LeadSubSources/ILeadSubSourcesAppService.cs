﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.LeadSubSources.Dtos;
using TheSolarProduct.Dto;
using System.Collections.Generic;


namespace TheSolarProduct.LeadSubSources
{
    public interface ILeadSubSourcesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetLeadSubSourceForViewDto>> GetAll(GetAllLeadSubSourcesInput input);

        Task<GetLeadSubSourceForViewDto> GetLeadSubSourceForView(int id);

		Task<GetLeadSubSourceForEditOutput> GetLeadSubSourceForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditLeadSubSourceDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetLeadSubSourcesToExcel(GetAllLeadSubSourcesForExcelInput input);

		
		Task<List<LeadSubSourceLeadSourceLookupTableDto>> GetAllLeadSourceForTableDropdown();
		
    }
}