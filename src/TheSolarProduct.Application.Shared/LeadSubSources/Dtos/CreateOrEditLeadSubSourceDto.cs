﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.LeadSubSources.Dtos
{
    public class CreateOrEditLeadSubSourceDto : EntityDto<int?>
    {

		[Required]
		[StringLength(LeadSubSourceConsts.MaxNameLength, MinimumLength = LeadSubSourceConsts.MinNameLength)]
		public string Name { get; set; }
		
		
		 public int LeadSourceId { get; set; }
		 
		 
    }
}