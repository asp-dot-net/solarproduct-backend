﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.LeadSubSources.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}