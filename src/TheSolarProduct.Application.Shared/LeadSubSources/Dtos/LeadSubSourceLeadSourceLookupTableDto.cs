﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.LeadSubSources.Dtos
{
    public class LeadSubSourceLeadSourceLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}