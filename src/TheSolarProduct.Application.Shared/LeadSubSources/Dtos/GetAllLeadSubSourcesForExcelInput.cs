﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.LeadSubSources.Dtos
{
    public class GetAllLeadSubSourcesForExcelInput
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }


		 public string LeadSourceNameFilter { get; set; }

		 
    }
}