﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.LeadSubSources.Dtos
{
    public class GetLeadSubSourceForEditOutput
    {
		public CreateOrEditLeadSubSourceDto LeadSubSource { get; set; }

		public string LeadSourceName { get; set;}


    }
}