﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.JobTrackers.Dtos;
using TheSolarProduct.SystemReports.Dto;

namespace TheSolarProduct.SystemReports
{
    public interface ISystemReportsAppService : IApplicationService
    {
        //Task<PagedResultDto<GetActivityForViewDto>> GetAll(GetAllActivityInput input);
        PagedResultDto<GetActivityForViewDto> GetAll(GetAllActivityInput input);
        Task<PagedResultDto<GetActivityForViewDto>> GetAllTodoActivityList(GetAllActivityInputDto input);
        Task<LeadActivityLogDto> GetactivitydetailfromId(int id);
        Task markascompleteActivityLog(int id);
        Task updateActivityLog(LeadActivityLogDto input);
        Task SaveActivityLogTask(LeadActivityLogDto input);
        Task<PagedResultDto<GetActivityForViewDto>> GetAllTodoActivityListFilterwise(GetAllActivityInputDto input);
        Task<GetActivitytaskcountDto> getAllApplicationTrackerCount(GetAllActivityInputDto input);
        Task deleteActivityLog(int id);
        Task deleteAllActivityLog(List<int> ids);
        Task<FileDto> getSalesReportToExcel(SalesReportDto input);
        //DataTable getOutstandingReport(SalesReportDto input);
        Task<List<OutstandingReportDto>> getoutstandingsForReport(SalesReportDto input);

        //Task<PagedResultDto<GetAllJobCostViewDto>> GetJobCostReport(GetAllJobCostInput input);
    }
}
