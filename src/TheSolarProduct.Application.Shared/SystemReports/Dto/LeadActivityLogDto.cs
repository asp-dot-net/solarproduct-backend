﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
 
namespace TheSolarProduct.SystemReports.Dto
{
 
	public class LeadActivityLogDto : EntityDto<int?>
	{
		public int TenantId { get; set; }

		public virtual int LeadId { get; set; }

		 

		public virtual int ActionId { get; set; }

	 

		public virtual string ActionNote { get; set; }

		public virtual DateTime? ActivityDate { get; set; }

		public virtual string Body { get; set; }

		public virtual string ActivityNote { get; set; }

		public virtual string MessageId { get; set; }

		public virtual int? ReferanceId { get; set; }

		public virtual bool? IsMark { get; set; }

		public virtual int? TemplateId { get; set; }

		public virtual string Subject { get; set; }

		public virtual int? PromotionId { get; set; }

		public virtual int? PromotionUserId { get; set; }

		public virtual int? SectionId { get; set; }

		 

		public virtual int? TodopriorityId { get; set; }
		public virtual string Todopriority { get; set; }
		public virtual bool? IsTodoComplete { get; set; }

		public virtual string TodoResponse { get; set; }
		public virtual string TodoTag { get; set; }
		public virtual DateTime? TodoDueDate { get; set; }
		public virtual DateTime? TodoresponseTime { get; set; }

		public string Jobdetails { get; set; }
	}
}
