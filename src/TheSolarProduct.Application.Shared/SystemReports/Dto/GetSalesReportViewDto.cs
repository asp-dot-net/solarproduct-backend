﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.SystemReports.Dto
{
    public class GetSalesReportViewDto
    {
        public int? JobStatusId { get; set; }
        public string JobStatusName { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public DateTime? InstallationDate { get; set; }
        public decimal? STC { get; set; }
        public decimal? TotalCost { get; set; }
        public decimal? WaveOff { get; set; }
    }
}
