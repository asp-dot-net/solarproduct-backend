﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.SystemReports.Dto
{
  public  class GetAllActivityInputDto : PagedAndSortedResultRequestDto
	{ 
		public string Filter { get; set; }
		public int? Organizationid { get; set; }
		public string CopanyNameFilter { get; set; }
		public int? TeamId { get; set; }
		public int? UserId { get; set; }
		public string DateNameFilter { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }

		public int? taskid { get; set; }

	}

}
 