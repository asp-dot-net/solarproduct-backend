﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.SystemReports.Dto
{
   public class SalesReportDto
    {
        public int? OrganizationId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<int?> Paymenttypeid { get; set; }
    }
    public class OutstandingReportDto
    {
        public int? OrganizationId { get; set; }
        public string JobNumber { get; set; }
        public string CompanyName { get; set; }
        public decimal? TotalCost { get; set; }

        public decimal? PriviousOutStanding { get; set; }

        public DateTime? InstallationDate { get; set; }
        public List<monthdetail> months { get; set; }

    }

    public class monthdetail
    {
        public decimal? months { get; set; }
        public string HeaderName { get; set; }
    }

}
