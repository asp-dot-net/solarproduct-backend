﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.SystemReports.Dto
{
    public class OutStandingReportDto
    {
        public string JobNumber { get; set; }
        public string CompanyName { get; set; }
        public decimal? TotalCost { get; set; }
        public decimal? PriviousOutStanding { get; set; }
        public List<string> LsitOfMonths { get; set; }
    }
}
