﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.SystemReports.Dto
{
    public class GetActivityForViewDto : PagedAndSortedResultRequestDto
	{
		public int Id { get; set; }
		public int LeadId { get; set; }

		public string CreatorUserName { get; set; }
		public string AssignedTo { get; set; }
		public int? assignedtoid { get; set; }
		public string LeadCompanyName { get; set; }

		public int ActionId { get; set; }

		public string ActionName { get; set; }

		public string ActionNote { get; set; }

		public string LogDate { get; set; }

		public string ActivityNote { get; set; }

		public string Body { get; set; }

		public string jobnumber { get; set; }
		public DateTime CreationTime { get; set; }
		public string Prioritytype { get; set; }
		public bool isSelected { get; set; }


	}


	public class GetActivitylistViewDto
	{
		public int LeadId { get; set; }

		public string CreatorUserName { get; set; }

		public string LeadCompanyName { get; set; }

		public int ActionId { get; set; }

		public string ActionName { get; set; }

		public string ActionNote { get; set; }

		public string LogDate { get; set; }

		public string ActivityNote { get; set; }

		public string Body { get; set; }
		public DateTime actvitydate { get; set; }

		public string activitynname { get; set; }
		public int CreatorUserId { get; set; }
		
	}


	public class GetActivitytaskcountDto
	{
		public int taskcount { get; set; }

		 

	}
}
