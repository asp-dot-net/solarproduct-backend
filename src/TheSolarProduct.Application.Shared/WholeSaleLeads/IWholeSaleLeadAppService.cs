﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Common.Dto;
using TheSolarProduct.Dto;
using TheSolarProduct.EmailTemplates.Dtos;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Leads;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.SmsTemplates.Dtos;
using TheSolarProduct.WholeSaleEmailTemplates.Dto;
using TheSolarProduct.WholeSaleLeads.Dtos;
using TheSolarProduct.WholeSaleSmsTemplates.Dtos;

namespace TheSolarProduct.WholeSaleLeads
{
    public interface IWholeSaleLeadAppService : IApplicationService
    {
        Task CreateOrEdit(CreateOrEditWholeSaleLeadDto input);

        Task<PagedResultDto<GetWholeSaleLeadForViewDto>> GetAll(GetAllWholeSaleLeadInput input);
        Task<FileDto> GetLaedsForExport(GetAllWholeSaleLeadExportInput input);

        Task<CreateOrEditWholeSaleLeadDto> GetWholeSaleLeadForEdit(EntityDto input);

        Task<List<GetWholeSaleLeadForViewDto>> CheckExistWholeSaleLeadsList(CreateOrEditWholeSaleLeadDto input);

        Task Delete(EntityDto input);

        Task<List<int>> GetTotalWholeSaleLeadsCountForWholeSalePromotion(GetAllWholeSaleLeadsInputForWholeSalePromotion input);

        Task<List<CommonLookupDto>> GetallSMSTemplates(int wholeSaleLeadId);

        Task<List<CommonLookupDto>> getOrgWiseDefultandownemailadd(int wholeSaleLeadId);

        Task<List<CommonLookupDto>> GetallEmailTemplates(int wholeSaleLeadId);

        Task<GetWholeSaleEmailTemplateForEditOutput> GetEmailTemplateForEditForEmail(EntityDto input);

        Task<GetWholeSaleSmsTemplateForEditOutput> GetSmsTemplateForEditForSms(EntityDto input);

        Task SaveWholeSaleDocument(string FileToken, string FileName, int id, int? docType, int sectionId);

        Task<List<GetWholeSaleLeadDoc>> GetallWholeSaledoc(int? id);

        Task DeleteWholeSaleDoc(int? id, int wholeSaleLeadId, int sectionId);

        Task<List<GetActivityLogViewDto>> GetWholeSaleLeadActivityLog(GetActivityLogInput input);

        Task<GetWholeSaleLeadForViewDto> GetWholeSaleLeadForView(int id);

        Task ChangeStatus(GetLeadForChangeStatusOutput input);

        Task<List<LeadtrackerHistoryDto>> GetWholeSaleLeadActivityLogHistory(GetActivityLogInput input);

        Task AsssignOrTransferLeads(AssignOrDeleteWholeSaleLeadInput input);

        Task<List<CommonLookupDto>> GetallWholeSaleDocument();

        Task<List<GetAllWholeSaleLeadByLeadStatusOutput>> GetallWholeSaleDashboard();

        Task<string> AddtoUserRequest(int leadId);

        Task<List<GetWholeSaleLeadHeaderSerachDto>> GetWholeSaleLeadSearchFilter(string str, string filterName);

        Task<PagedResultDto<GetWholeSaleLeadForViewDto>> GetAllWholeSaleLeadSearch(int Id);

        Task<int?> GetLeadCountByUserId();

        Task RequestToTransfer(int leadId);

        Task<PagedResultDto<GetAllRequestToWholeSaleLeadTransferDto>> GetAllRequestToWholeSaleLeadTransfer(GetAllRequestToWholeSaleLeadInput input);

        Task RequestToTransferApproveNotApprove(List<int> Ids, string approve);

        Task<List<string>> getAllEmailList(int leadId);

        Task<List<CommonLookupDto>> GetCreateUsers();

    }
}
