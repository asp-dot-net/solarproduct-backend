﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleLeads.Dtos
{
    public class GetAllWholeSaleLeadExportInput
    {
        public string FilterName { get; set; }
        public string Filter { get; set; }

        public int? StateId { get; set; }

        public string PostCode { get; set; }

        public string Address { get; set; }

        public string Datefilter { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int OrganizationId { get; set; }

        public int? SalesMangerId { get; set; }

        public int? SalesRapId { get; set; }

        public int? StatusId { get; set; }

        public bool MyLead { get; set; }

        public int? CreatedBy { get; set; }
    }
}
