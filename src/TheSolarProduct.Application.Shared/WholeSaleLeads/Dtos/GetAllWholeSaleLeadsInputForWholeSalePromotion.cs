﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleLeads.Dtos
{
    public class GetAllWholeSaleLeadsInputForWholeSalePromotion
    {
        public int? LeadStatusIdFilter { get; set; }

        public DateTime? StartDateFilter { get; set; }

        public DateTime? EndDateFilter { get; set; }

        public int? StateIdFilter { get; set; }

        public List<int?> LeadStatusIdsFilter { get; set; }

        public List<int?> StateIdsFilter { get; set; }

        public  int OrganizationId { get; set; }



    }
}
