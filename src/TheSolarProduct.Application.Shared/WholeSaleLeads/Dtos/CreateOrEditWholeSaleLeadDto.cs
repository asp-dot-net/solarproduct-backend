﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.WholeSaleLeads.Dtos
{
    public class CreateOrEditWholeSaleLeadDto : EntityDto<int?>
    {
        public string ABNNumber { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CompanyName { get; set; }

        public string Mobile { get; set; }

        public string Talephone { get; set; }

        public string CustomerType { get; set; }

        public int? WholeSaleStatusId { get; set; }

        public virtual string PostalUnitNo { get; set; }

        public virtual string PostalUnitType { get; set; }

        public virtual string PostalStreetNo { get; set; }

        public virtual string PostalStreetName { get; set; }

        public virtual string PostalStreetType { get; set; }

        public int? PostalStateId { get; set; }

        public virtual string PostalPostCode { get; set; }

        public virtual string PostalLatitude { get; set; }

        public virtual string PostalLongitude { get; set; }

        public virtual string DelivaryUnitNo { get; set; }

        public virtual string DelivaryUnitType { get; set; }

        public virtual string DelivaryStreetNo { get; set; }

        public virtual string DelivaryStreetName { get; set; }

        public virtual string DelivaryStreetType { get; set; }

        public int? DelivaryStateId { get; set; }

        public virtual string DelivaryPostCode { get; set; }

        public virtual string DelivaryLatitude { get; set; }

        public virtual string DelivaryLongitude { get; set; }

        public virtual string BankName { get; set; }

        public virtual string AccountName { get; set; }

        public virtual string BSBNo { get; set; }

        public virtual string AccoutNo { get; set; }

        public virtual decimal? CreditAmount { get; set; }

        public virtual int? CreditDays { get; set; }

        public virtual int? AssignUserId { get; set; }

        public virtual DateTime? AssignDate { get; set; }

        public virtual int? FirstAssignUserId { get; set; }

        public virtual DateTime? FirstAssignDate { get; set; }

        public virtual int? BDMId { get; set; }

        public string AditionalNotes { get; set; }

        public bool IsDelivery { get; set; }

        public string PostalAddress { get; set; }

        public string DelivaryAddress { get; set; }

        public string PostalState { get; set; }

        public string DelivaryState { get; set; }

        public string WholeSaleStatus { get; set; }

        public string Email { get; set; }

        public virtual int OrganizationId { get; set; }

        public string OrgName { get; set; }

        public string OrgMobile { get; set; }

        public string OrgEmail { get; set; }

        public string OrgLogo { get; set; }

        public string AssignUser { get; set; }

        public string FirstAssignUser { get; set; }

        public virtual int? SalesRapId { get; set; }

        public string SalesRapName { get; set; }
        public string SalesRapMobile { get; set; }
        public string SalesRapEmail { get; set; }

        public string UserName { get; set; }
        public string UserMobile { get; set; }
        public string UserEmail { get; set; }
        public string Owning { get; set; }

        public string CreateUserName { get; set; }

        public string BDM { get; set; }

        public List<WholeSaleLeadContactDto> WholeSaleLeadContacts { get; set; }

        public bool? TradinSTC { get; set; }

        public decimal? TradinSTCPrice { get; set; }

        public int? TradinSTCWhomId { get; set; }

        public string TradinSTCNote { get; set; }

        public int? PriceCategoryId { get; set; }

        public int? WholesaleClientId { get; set; }

        public string PriceCategory { get; set; }

        public string TradinSTCWhom { get; set; }

        public string createTime { get; set; }

        public string PostalSuburb { get; set; }

        public int? PostalSuburbId { get; set; }

        public string DelivarySuburb { get; set; }

        public int? DelivarySuburbId { get; set; }

    }
}
