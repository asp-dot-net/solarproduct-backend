﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleLeads.Dtos
{
    public class GetAllRequestToWholeSaleLeadTransferDto : EntityDto
    {
        public int? CreatedById { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int? LeadId { get; set; }

        public string Name { get; set;}

        public string CompanyName { get; set;}

        public string ABNNumber { get; set;}

        public bool? IsSelected { get; set; }


    }
}
