﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleLeads.Dtos
{
    public class GetWholeSaleLeadHeaderSerachDto : EntityDto
    {
        public string ABNNumber { get; set; }

        public string CustomerName { get; set; }

        public string CompanyName { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public string Address { get; set; }
    }
}
