﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleLeads.Dtos
{
    public class GetWholeSaleLeadDoc : EntityDto
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public DateTime? CreationTime { get; set; }
        public string CreationUser { get; set; }

        public List<InsertMultipleWholeSaleDocs> ListofDocs { get; set; }
        public int? WholeSaleLeadId { get; set; }

        public string DocTitle { get; set; }
    }

    public class InsertMultipleWholeSaleDocs
    {
        public string FileName { get; set; }
        public string FileToken { get; set; }
    }
}
