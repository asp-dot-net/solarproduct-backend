﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleLeads.Dtos
{
    public class GetWholeSaleLeadForViewDto
    {
        public CreateOrEditWholeSaleLeadDto createOrEditWholeSaleLeadDto { get; set; }

        public string ReminderTime { get; set; }

        public string ActivityDescription { get; set; }

        public string ActivityComment { get; set; }

        public string LeadStatusName { get; set; }

        public string CreatedByName { get; set;}

        public string CurrentAssignUserName { get; set;}

        public string CurrentAssignUserEmail { get; set;}

        public string CurrentAssignUserMobile { get; set;}

        public DateTime? CreatedOn { get; set; }

        public string latitude { get; set; }

        public string longitude { get; set; }

        public string OrgName { get; set; }

        public string OrgMobile { get; set; }

        public string OrgEmail { get; set; }

        public string OrgLogo { get; set; }

        public string UserName { get; set; }

        public string UserEmail { get; set; }

        public string UserPhone { get; set; }

        public string PostalState { get; set; }

        public List<WholeSaleLeadsSummaryCount> SummaryCount { get; set; }

        public  bool IsCredit { get; set; }

        public string CommentBy { get; set; }

        public string Followupby { get; set;}

        public string Transfer { get; set; }

    }

    public class WholeSaleLeadsSummaryCount
    {
        //public string DNC { get; set; }

        //public string Existing { get; set; }

        //public string Cold { get; set; }

        //public string Warm { get; set; }

        //public string Hot { get; set; }

        //public string Total { get; set; }

        public string Status { get; set; }

        public int totalLead { get; set; }
    }
}
