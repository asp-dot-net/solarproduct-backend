﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleLeads.Dtos
{
    public class GetAllRequestToWholeSaleLeadInput : PagedAndSortedResultRequestDto
    {
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int OrganizationId { get; set; }

        public string Type { get; set; }
    }
}
