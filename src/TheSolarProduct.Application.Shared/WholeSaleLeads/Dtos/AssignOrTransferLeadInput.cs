﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleLeads.Dtos
{
    public class AssignOrDeleteWholeSaleLeadInput
    {
        public int? OrganizationID { get; set; }

        public int AssignToUserID { get; set; }

        public int LeadActionId { get; set; }

        public int SectionId { get; set; }

        public List<int> WholeSaleLeadIds { get; set; }
    }
}
