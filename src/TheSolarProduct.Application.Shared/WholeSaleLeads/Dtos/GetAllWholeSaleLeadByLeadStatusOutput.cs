﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleLeads.Dtos
{
    public class GetAllWholeSaleLeadByLeadStatusOutput : EntityDto
    {
        public string Status { get; set; }

        public List<CreateOrEditWholeSaleLeadDto> WholeSaleLeads { get; set; }
    }
}
