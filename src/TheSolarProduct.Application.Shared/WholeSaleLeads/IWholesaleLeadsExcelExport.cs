﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.WholeSaleLeads.Dtos;

namespace TheSolarProduct.WholeSaleLeads
{
    public interface IWholesaleLeadsExcelExport
    {
        FileDto LeadTrackerExportToFile(List<GetWholeSaleLeadForViewDto> leadListDtos);
    }
}
