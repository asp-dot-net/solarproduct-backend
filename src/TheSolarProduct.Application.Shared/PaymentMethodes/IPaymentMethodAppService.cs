﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.PaymentMethodes.Dtos;

namespace TheSolarProduct.PaymentMethodes
{
    public interface IPaymentMethodAppService : IApplicationService
    {
        Task<PagedResultDto<GetPaymentMethodForViewDto>> GetAll(GetAllPaymentMethodInput input);
        Task<GetPaymentMethodForEditOutput> GetPaymentMethodForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditPaymentMethod input);
        Task Delete(EntityDto input);
        Task<FileDto> GetPaymentMethodToExcel(GetAllPaymentMethodForExcelInput input);

    }
}
