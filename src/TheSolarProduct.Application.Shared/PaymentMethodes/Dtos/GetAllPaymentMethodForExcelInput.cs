﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PaymentMethodes.Dtos
{
    public class GetAllPaymentMethodForExcelInput
    {
        public string Filter { get; set; }
    }
}
