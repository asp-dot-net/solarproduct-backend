﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Quickstock.Dto
{
    public class StockDetails
    {
        public int StockOnHand { get; set; }
    }
}
