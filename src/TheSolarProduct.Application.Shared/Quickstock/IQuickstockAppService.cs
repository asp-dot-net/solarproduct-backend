﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Quickstock.Dto;

namespace TheSolarProduct.Quickstock
{
    public interface IQuickstockAppService : IApplicationService
    {
        StockDetails GetStockDetails(int StockItemId, int Location);

        StockDetails GetStockDetailByState(int StockItemId, string State);

        Task<bool> GetCheckActualAudit(int InstallerId);
    }
}
