﻿using Abp;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class ImportPurchaseOrderItemFromExcelArgs
    {
        public int? TenantId { get; set; }

        public Guid BinaryObjectId { get; set; }

        public UserIdentifier User { get; set; }

        public int OrganizationId { get; set; }

        public byte[] FileBytes { get; set; }

        public virtual int PurchaseOrderId { get; set; }

        public virtual int ProductItemId { get; set; }

        public virtual int WarehouseLocationId { get; set; }

    
        public virtual bool IsApp { get; set; }
    }
}
