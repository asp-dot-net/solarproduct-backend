﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class GetStockVariationForViewDto
    {
        public StockVariationDto StockVariationDto { get; set; }
    }
}
