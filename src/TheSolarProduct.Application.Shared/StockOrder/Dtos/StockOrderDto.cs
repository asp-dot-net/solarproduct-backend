﻿using Abp.Application.Services.Dto;
using Castle.MicroKernel.Registration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Numerics;
using System.Text;
using TheSolarProduct.Jobs.Dtos.Pylon;
using TheSolarProduct.ProductPackages.Dtos;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class StockOrderDto : EntityDto
    {
        public virtual long OrderNo { get; set; }
       
        public virtual int OrganizationId { get; set; }
        public virtual string Vendor { get; set; }
        public virtual string VendorAddress { get; set; }
        public int? PaymentTerm { get; set; }
        public virtual string VendorInoviceNo { get; set; }
        public virtual string PurchaseCompany { get; set; }

        public virtual string ManualOrderNo { get; set; }

        public virtual string ProductNotes { get; set; }

        public virtual string ContainerNo { get; set; }

        public virtual string StockOrderStatus { get; set; }

        public virtual string StockFrom { get; set; }

        public virtual string DeliveryType { get; set; }

        public virtual string PaymentStatus { get; set; }

        public virtual string PaymentType { get; set; }
        public virtual string WarehouseLocation { get; set; }

        public virtual int? WarehouseLocationId { get; set; }

        public virtual string IncoTerm { get; set; }
        public virtual string TransportCompany { get; set; }

        public virtual DateTime? DocSentDate { get; set; }

        public virtual DateTime? TargetArrivalDate { get; set; }

        public virtual DateTime? ETD { get; set; }

        public virtual DateTime? ETA { get; set; }
        public virtual DateTime? OrderDate { get; set; }
        

        public virtual DateTime? PaymentDueDate { get; set; }

        public virtual DateTime? PhysicalDeliveryDate { get; set; }

        public virtual decimal? DiscountPrice { get; set; }

        public virtual decimal? CreditNotePrice { get; set; }

        public virtual decimal? Amount { get; set; }


        public virtual string Currency { get; set; }

        public virtual string GSTType { get; set; }

        public virtual string StockOrderFor { get; set; }
        public virtual string AdditionalNotes { get; set; }
        public int Qty {  get; set; }
        public string ViewHtml { get; set; }
        public string OrderDatee { get; set; }
        public bool isSubmit { get; set; }
    }
}
