﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class GetAllStockVariationsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string ActionFilter { get; set; }

    }
}
