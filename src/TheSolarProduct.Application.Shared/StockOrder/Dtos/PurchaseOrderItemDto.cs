﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class PurchaseOrderItemDto : EntityDto<int?>
    {
        public int? PurchaseOrderId { get; set; }
        public int ProductTypeId { get; set; }

        public int? ProductItemId { get; set; }

        public string ProductItem { get; set; }
       
        public  int? Quantity { get; set; }

        public decimal? PricePerWatt { get; set; }

        public  decimal? UnitRate { get; set; }

        public  decimal? EstRate { get; set; }

        public  decimal? Amount { get; set; }

        public  string ModelNo { get; set; }
        public  DateTime? ExpiryDate { get; set; }
    }
}
