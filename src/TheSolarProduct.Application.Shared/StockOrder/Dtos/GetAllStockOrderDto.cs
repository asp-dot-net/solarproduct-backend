﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Promotions.Dtos;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class GetAllStockOrderDto : EntityDto
    {
        public List<GetAllStockOrderOutputDto> StockOrderList { get; set; }
        public int? ModuleSummary {  get; set; }
        public int? InvertersSummary { get; set; }
        public int? MountingSummary { get; set; }
        public int? ElectricalSummary { get; set; }
        public int? BatterySummary { get; set; }
        public int? OthersSummary { get; set; }

    }
}
