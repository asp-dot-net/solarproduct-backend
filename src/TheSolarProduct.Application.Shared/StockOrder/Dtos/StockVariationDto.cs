﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class StockVariationDto : EntityDto<int?>
    {
        public string Name { get; set; }

        public string Action { get; set; }

    }
}
