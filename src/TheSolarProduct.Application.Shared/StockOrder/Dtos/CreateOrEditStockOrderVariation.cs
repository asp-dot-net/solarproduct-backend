﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class CreateOrEditStockOrderVariation : EntityDto<int?>
    {
        public virtual decimal? Cost { get; set; }

        public virtual int? StockVariationId { get; set; }

        public virtual int? PurchaseOrderId { get; set; }

        public string Notes { get; set; }
    }
}
