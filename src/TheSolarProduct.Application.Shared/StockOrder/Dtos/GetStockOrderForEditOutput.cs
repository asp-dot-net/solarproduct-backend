﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.StockOrderStatus.Dtos;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class GetStockOrderForEditOutput
    {
        public CreateOrEditStockOrderDto StockOrder { get; set; }

        public string Vendor { get; set; }

    }
}
