﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class CreateOrEditStockOrderDto : EntityDto<int?>
    {
        public virtual long OrderNo { get; set; }
        public virtual int OrganizationId { get; set; }
        public int? VendorId { get; set; }
        public int? PurchaseCompanyId { get; set; }
        public string ManualOrderNo { get; set; }
        public string VendorInoviceNo { get; set; }
        public string ContainerNo { get; set; }
        public virtual string ProductNotes { get; set; }
        public int? StockOrderStatusId { get; set; }
        public int? StockFromId { get; set; }
        public int? DeliveryTypeId { get; set; }
        public int? PaymentStatusId { get; set; }
        public int? PaymentTypeId { get; set; }
        public int? WarehouseLocationId { get; set; }
        public int? IncoTermId { get; set; }
        public int? TransportCompanyId { get; set; }
        public virtual DateTime? PaymentDueDate { get; set; }
        public DateTime? TargetArrivalDate { get; set; }
        public DateTime? ETD { get; set; }
        public DateTime? ETA { get; set; }
        public DateTime? PhysicalDeliveryDate { get; set; }
        public decimal DiscountPrice { get; set; }
        public decimal CreditNotePrice { get; set; }
        public int? CurrencyId { get; set; }
        public int? GSTType { get; set; }
        public int? StockOrderForId { get; set; }
        public virtual string AdditionalNotes { get; set; }

        public List<PurchaseOrderItemDto> PurchaseOrderInfo { get; set; } 

        public List<CreateOrEditStockOrderVariation> StockOrderVariationDto { get; set; }

    }
}
