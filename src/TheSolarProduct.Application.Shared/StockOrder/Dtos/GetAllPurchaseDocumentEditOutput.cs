﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class GetAllPurchaseDocumentEditOutput
    {
        public  int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public string CreatedBy { get; set; }
        public  string PurchaseDocumentTitle { get; set; }
        public  string FileName { get; set; }
        public  string FilePath { get; set; }

        public string DocName { get; set; }

    }
}
