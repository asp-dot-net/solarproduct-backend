﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class GetAllSerialNoHistoryReportInput : PagedAndSortedResultRequestDto
    {
        public string FilterName { get; set; }
        public string Filter { get; set; }
        public List<int?> SerialNoStatusIds { get; set; }

        public string Datefilter { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
