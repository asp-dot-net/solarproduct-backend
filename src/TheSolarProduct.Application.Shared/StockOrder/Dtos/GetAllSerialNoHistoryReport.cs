﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class GetAllSerialNoHistoryReport
    {

        public int? Id { get; set; }
        public long OrderNo { get; set; }
        public string CatogoryName { get; set; }
        public string ItemName { get; set; }

        public string SerialNo { get; set; }
        public string PalletNo { get; set; }
        public string InstallerName { get; set; }
        public string NameOrConsignmentNo { get; set; }
        public string ModuleName { get; set; }
        public string Location { get; set; }
        public string DeductBy { get; set; }
        public DateTime DeductDate { get; set; }
        public string Message { get; set; }
    }
}
