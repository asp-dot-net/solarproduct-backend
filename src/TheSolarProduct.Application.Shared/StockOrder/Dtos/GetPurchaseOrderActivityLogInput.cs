﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class GetPurchaseOrderActivityLogInput
    {
        public int PurchaseOrderId { get; set; }
        public int? ActionId { get; set; }
        public int? SectionId { get; set; }
        public bool? CurrentPage { get; set; }
        public bool AllActivity { get; set; }
    }
}
