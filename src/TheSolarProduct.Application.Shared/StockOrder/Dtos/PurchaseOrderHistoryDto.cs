﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class PurchaseOrderHistoryDto : FullAuditedEntity
    {
        public virtual string FieldName { get; set; }
        public virtual string PrevValue { get; set; }
        public virtual string CurValue { get; set; }
        public virtual string Action { get; set; }
        public virtual DateTime LastmodifiedDateTime { get; set; }
        public int PurchaseOrderId { get; set; }
        public int PurchaseOrderActionId { get; set; }
        public virtual string Lastmodifiedbyuser { get; set; }
    }
}
