﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.QuickStockApp.StockOrder.Dto;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class GetAllStockOrderOutputDto : EntityDto
    {
        public virtual long OrderNo { get; set; }
        public virtual int OrganizationId { get; set; }
        public virtual string Vendor { get; set; }
        public virtual string VendorInoviceNo { get; set; }
        public virtual string PurchaseCompany { get; set; }

        public virtual string ContainerNo { get; set; }
        public virtual string DeliveryType { get; set; }
        public virtual string WarehouseLocation { get; set; }

        public int? Qty { get; set; }
        public int? PendingQty { get; set; }
        public virtual string ManualOrderNo { get; set; }

        public virtual string StockOrderStatus { get; set; }
      
        public virtual string Currency { get; set; }

        public virtual string PaymentStatus { get; set; }

        public virtual string PaymentType { get; set; }
        public virtual string StockFrom { get; set; }
        public virtual DateTime? OrderDate { get; set; }
        public virtual DateTime? ExpDelivaryDate { get; set; }

        public int AmountWithGst { get; set; }
        public int AmountWithOutGst { get; set; }
        public bool isSubmit {  get; set; }
        public List<PurchaseOrderItemDto> PurchaseOrderItemList { get; set; }

       
    }
}
