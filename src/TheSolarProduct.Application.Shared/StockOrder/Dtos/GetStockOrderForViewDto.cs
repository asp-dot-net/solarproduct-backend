﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.StockFroms.Dtos;
using TheSolarProduct.StockOrderStatus.Dtos;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class GetStockOrderForViewDto : EntityDto
    {
        public StockOrderDto PurchaseOrder { get; set; }
        public List<GetPurchaseOrderItemListForViewDto> PurchaseOrderItemList { get; set; }
        
    }

    public class GetPurchaseOrderItemListForViewDto : EntityDto
    {
        public int ProductTypeId { get; set; }

        public int? ProductItemId { get; set; }
        public string ProductType { get; set; }

        public string ProductItem { get; set; }
        public int? Quantity { get; set; }

        public int? PendingQuantity { get; set; }
        public int? UploadedQuantity { get; set; }

        public decimal? PricePerWatt { get; set; }

        public decimal? UnitRate { get; set; }

        public decimal? EstRate { get; set; }

        public decimal? Amount { get; set; }
        public virtual string ModelNo { get; set; }
        public virtual DateTime? ExpiryDate { get; set; }
        public bool IsApp { get; set; } 
        public string ExpiryDatee {  get; set; }
    }
}


