﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockOrder.Dtos
{
    public class GetAllStockOrderReceivedReportInput : PagedAndSortedResultRequestDto
    {
        public string FilterName { get; set; }
        public string Filter { get; set; }
        public int? PurchaseCompanyIdFiletr { get; set; }

        public int? ProductTypeIdFiletr { get; set; }
        public List<int?> StockOrderStatusIds { get; set; }
        public int? OrderTypeIdFiletr { get; set; }
        public List<int?> StateFitlerIds { get; set; }

        public int OrganizationId { get; set; }
        public string Datefilter { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}