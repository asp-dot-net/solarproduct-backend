﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.StockOrder.Dtos;

namespace TheSolarProduct.StockOrder
{
    public interface IStockVariationsAppService : IApplicationService
    {
        Task<PagedResultDto<GetStockVariationForViewDto>> GetAll(GetAllVariationsInput input);

        Task<GetStockVariationForViewDto> GetVariationForView(int id);

        Task<StockVariationDto> GetVariationForEdit(EntityDto input);

        Task CreateOrEdit(StockVariationDto input);

        Task Delete(EntityDto input);
    }
}
