﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.PurchaseDocumentLists.Dtos;
using TheSolarProduct.QuickStockApp.StockOrder.Dto;
using TheSolarProduct.Reports.JobCost.Dtos;
using TheSolarProduct.StockOrder.Dtos;

namespace TheSolarProduct.StockOrder
{

    public interface IStockOrderAppService : IApplicationService
    {

        Task<PagedResultDto<GetAllStockOrderDto>> GetAll(GetAllStockOerderInput input);
        Task<List<GetPurchaseOrderActivityLogViewDto>> GetPurchaseOrderActivityLog(GetPurchaseOrderActivityLogInput input);
        Task<List<PurchaseOrderHistoryDto>> GetPurchaseOrderActivityLogHistory(GetPurchaseOrderActivityLogInput input);
        Task<long> GetLastOrderNumber(int organizationId);
        Task<int?> GetCreditDays(int vendorId);
        Task<GetStockOrderForEditOutput> GetStockOrderForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditStockOrderDto input);
        Task<GetStockOrderForViewDto> GetStockOrderById(int id);
        Task<FileDto> GetStockOrderToExcel(GetStockorderForExcelInput input);
        Task Delete(EntityDto input);
        Task<GetStockOrderForEditOutput> GetStockOrderByOrderNo(long orderNo);
        Task<PurchaseDocumentListDto> GetPurchaseDocumentData(int id);
        Task<ListResultDto<GetAllPurchaseDocumentEditOutput>> GetAllPurchaseDocument(EntityDto input);
        Task DeletePurchaseDoucment(EntityDto input);
        Task<GetStockOrderForViewDto> GetPoDataByPurchaseOrderId(int PurchaseOrderId);
} }
