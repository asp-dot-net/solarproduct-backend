﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.RejectReasons.Dtos
{
    public class GetAllRejectReasonsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string RejectReasonNameFilter { get; set; }



    }
}