﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.RejectReasons.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}