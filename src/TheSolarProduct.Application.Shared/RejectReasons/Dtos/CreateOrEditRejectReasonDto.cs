﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.RejectReasons.Dtos
{
    public class CreateOrEditRejectReasonDto : EntityDto<int?>
    {

		[Required]
		[StringLength(RejectReasonConsts.MaxRejectReasonNameLength, MinimumLength = RejectReasonConsts.MinRejectReasonNameLength)]
		public string RejectReasonName { get; set; }

        public Boolean IsActive { get; set; }

    }
}