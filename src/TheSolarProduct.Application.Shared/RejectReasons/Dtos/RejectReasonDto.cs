﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.RejectReasons.Dtos
{
    public class RejectReasonDto : EntityDto
    {
		public string RejectReasonName { get; set; }

        public Boolean IsActive {  get; set; }  

    }
}