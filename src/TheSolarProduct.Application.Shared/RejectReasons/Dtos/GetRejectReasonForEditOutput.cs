﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.RejectReasons.Dtos
{
    public class GetRejectReasonForEditOutput
    {
		public CreateOrEditRejectReasonDto RejectReason { get; set; }


    }
}