﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.RejectReasons.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.RejectReasons
{
    public interface IRejectReasonsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRejectReasonForViewDto>> GetAll(GetAllRejectReasonsInput input);

        Task<GetRejectReasonForViewDto> GetRejectReasonForView(int id);

		Task<GetRejectReasonForEditOutput> GetRejectReasonForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditRejectReasonDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetRejectReasonsToExcel(GetAllRejectReasonsForExcelInput input);

		
    }
}