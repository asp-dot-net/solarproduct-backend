﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckActives.Dtos;

namespace TheSolarProduct.CheckActives.Dtos
{
    public class GetCheckActiveForEditOutput
    {
        public CreateOrEditCheckActiveDto CheckActive { get; set; }
    }
}
