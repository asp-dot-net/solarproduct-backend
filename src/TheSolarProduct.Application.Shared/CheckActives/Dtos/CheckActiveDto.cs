﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CheckActives.Dtos
{
    public class CheckActiveDto : EntityDto
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }



    }
}
