﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CheckActives.Dtos
{
    public class GetAllCheckActiveForExcelInput
    {
        public string Filter { get; set; }
    }
}
