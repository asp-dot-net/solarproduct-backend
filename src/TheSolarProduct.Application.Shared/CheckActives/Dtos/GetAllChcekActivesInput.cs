﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CheckActives.Dtos
{
    public class GetAllChcekActivesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
