﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.CheckActives.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.CheckActives
{
    public interface ICheckActiveAppService : IApplicationService
    {
        Task<PagedResultDto<GetCheckActiveForViewDto>> GetAll(GetAllChcekActivesInput input);
        Task<GetCheckActiveForEditOutput> GetCheckActiveForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditCheckActiveDto input);
        Task Delete(EntityDto input);
        Task<FileDto> GetChcekActiveToExcel(GetAllCheckActiveForExcelInput input);
    }
}
