﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.HoldReasons.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.ServiceCategorys.Dtos;

namespace TheSolarProduct.ServiceCategorys
{
    public interface IServiceCategorysAppService : IApplicationService
    {
        Task<PagedResultDto<GetServiceCategoryForViewDto>> GetAll(GetAllServiceCategoryInput input);

        Task<GetServiceCategoryForViewDto> GetServiceCategoryForView(int id);

        Task<GetServiceCategoryForEditOutput> GetServiceCategoryForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditServiceCategoryDto input);

        Task Delete(EntityDto input);

       
    }
}
