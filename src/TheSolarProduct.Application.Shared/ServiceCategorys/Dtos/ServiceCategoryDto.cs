﻿using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.ServiceCategorys.Dtos
{
    public class ServiceCategoryDto : EntityDto
    {
        public string Name { get; set; }
        public  Boolean IsActive { get; set; }
    }
}