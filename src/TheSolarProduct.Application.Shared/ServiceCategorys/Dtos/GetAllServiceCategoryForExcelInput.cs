﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.ServiceCategorys.Dtos
{
    public class GetAllServiceCategoryForExcelInput
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }

    }
}