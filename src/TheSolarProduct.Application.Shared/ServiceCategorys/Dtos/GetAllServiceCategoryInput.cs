﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.ServiceCategorys.Dtos
{
    public class GetAllServiceCategoryInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }

    }
}