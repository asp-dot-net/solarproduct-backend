﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.ServiceCategorys.Dtos
{
    public class GetServiceCategoryForEditOutput
    {
        public CreateOrEditServiceCategoryDto serviceCategorys { get; set; }

    }
}