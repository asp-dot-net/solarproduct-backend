﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.ServiceCategorys.Dtos
{
    public class CreateOrEditServiceCategoryDto : EntityDto<int?>
    {

        [Required]
        public string Name { get; set; }
        public  Boolean IsActive { get; set; }
    }
}