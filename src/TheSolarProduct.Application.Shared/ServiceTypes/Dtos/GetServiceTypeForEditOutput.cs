﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using TheSolarProduct.ServiceCategorys.Dtos;

namespace TheSolarProduct.ServiceTypes.Dtos
{
    public class GetServiceTypeForEditOutput
    {
        public CreateOrEditServiceTypeDto serviceTypes { get; set; }

    }
}