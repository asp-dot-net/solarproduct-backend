﻿namespace TheSolarProduct.ServiceTypes.Dtos
{
    public class GetServiceTypeForViewDto
    {
        public ServiceTypeDto serviceTypes { get; set; }

    }
}