﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.ServiceTypes.Dtos
{
    public class GetAllServiceTypeInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }

    }
}