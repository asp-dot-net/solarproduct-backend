﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.ServiceTypes.Dtos
{
    public class CreateOrEditServiceTypeDto : EntityDto<int?>
    {

        [Required]
        public string Name { get; set; }
        public Boolean IsActive { get; set; }

    }
}