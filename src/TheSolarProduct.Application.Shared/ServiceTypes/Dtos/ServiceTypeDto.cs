﻿using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.ServiceTypes.Dtos
{
    public class ServiceTypeDto : EntityDto
    {
        public string Name { get; set; }
        public Boolean IsActive { get; set; }
    }
}