﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.ServiceTypes.Dtos
{
    public class GetAllServiceTypeForExcelInput
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }

    }
}