﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.ServiceStatuses.Dtos;
using TheSolarProduct.ServiceTypes.Dtos;

namespace TheSolarProduct.ServiceTypes
{
    public interface IServiceTypesAppService : IApplicationService
    {
        Task<PagedResultDto<GetServiceTypeForViewDto>> GetAll(GetAllServiceTypeInput input);

        Task<GetServiceTypeForViewDto> GetServiceTypeForView(int id);

        Task<GetServiceTypeForEditOutput> GetServiceTypeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditServiceTypeDto input);

        Task Delete(EntityDto input);

       
    }
}
