﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ServiceInvoice.Dtos
{
    public class GetAllServiceInvoiceInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
