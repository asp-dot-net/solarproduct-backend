﻿using Abp.Application.Services.Dto;
using Castle.Core;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ServiceInvoice.Dtos
{
    public class ServiceInvoiceDto : EntityDto
    {
        public int? ServiceId {  get; set; }

        public int? JobId { get; set; }

        public int? LeadId { get; set; }
        
        public string ProjectNumber { get; set; }
        
        public string CustomerName { get; set; }
        
        public string ManufacturerName { get; set; }
       
        public string ManufacturerTicketNumber { get; set; }
        
        public string ReplacementAddress { get; set; }
        
        public string TicketNumber { get; set; }
        
        public string InstallerName { get; set; }
        
        public DateTime? TicketResolvedDate { get; set; }
        
        public string InvoiceNo { get; set; }
        
        public DateTime? InvoiceDate { get; set; }
        
        public decimal InvoiceAmount { get; set; }
        
        public DateTime? InvoicePaidDate { get; set; }
        
        public string InvoicePaidReferenceNo { get; set; }
    }
}
