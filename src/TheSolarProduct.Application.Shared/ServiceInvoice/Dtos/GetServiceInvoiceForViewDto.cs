﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ServiceInvoice.Dtos
{
    public class GetServiceInvoiceForViewDto
    {
        public ServiceInvoiceDto serviceInvoice { get; set; }
    }
}
