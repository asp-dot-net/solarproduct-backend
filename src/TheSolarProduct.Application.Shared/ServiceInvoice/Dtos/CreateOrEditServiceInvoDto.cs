﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.ServiceInvoice.Dtos
{
    public class CreateOrEditServiceInvoDto : EntityDto
    {
        public int TenantId { get; set; }

        public int ServiceID { get; set; }

        public int JobID { get; set; }

        public string InvoiceNo { get; set; }

        public DateTime InvoiceDate { get; set; }

        public decimal Amount { get; set; }

        public DateTime PaidDate { get; set; }

        public string PaidReferenceNo { get; set; }
    }
}
