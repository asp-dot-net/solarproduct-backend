﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ServiceInvoice.Dtos;
using TheSolarProduct.Services.Dtos;

namespace TheSolarProduct.ServiceInvoice
{
    public interface IServiceIncoiceAppServices : IApplicationService
    {
        Task<PagedResultDto<GetServiceInvoiceForViewDto>> GetAll(GetAllServiceInvoiceInput input);
        Task CreateOrEditService(CreateOrEditServiceInvoDto input, int sectionId, string Section);

        Task<CreateOrEditServiceInvoDto> GetserviceForEdit(EntityDto input);


    }
}
