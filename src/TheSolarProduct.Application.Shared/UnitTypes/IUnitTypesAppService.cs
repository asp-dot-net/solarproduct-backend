﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.UnitTypes.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.UnitTypes
{
    public interface IUnitTypesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetUnitTypeForViewDto>> GetAll(GetAllUnitTypesInput input);

        Task<GetUnitTypeForViewDto> GetUnitTypeForView(int id);

		Task<GetUnitTypeForEditOutput> GetUnitTypeForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditUnitTypeDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetUnitTypesToExcel(GetAllUnitTypesForExcelInput input);

		
    }
}