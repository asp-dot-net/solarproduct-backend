﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.UnitTypes.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}