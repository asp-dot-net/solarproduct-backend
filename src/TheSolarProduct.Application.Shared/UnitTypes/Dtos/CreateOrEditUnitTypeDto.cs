﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.UnitTypes.Dtos
{
    public class CreateOrEditUnitTypeDto : EntityDto<int?>
    {

		[Required]
		[StringLength(UnitTypeConsts.MaxNameLength, MinimumLength = UnitTypeConsts.MinNameLength)]
		public string Name { get; set; }
		public Boolean IsActive {  get; set; }	
		

    }
}