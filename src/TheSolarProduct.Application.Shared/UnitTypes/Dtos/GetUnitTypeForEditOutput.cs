﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.UnitTypes.Dtos
{
    public class GetUnitTypeForEditOutput
    {
		public CreateOrEditUnitTypeDto UnitType { get; set; }


    }
}