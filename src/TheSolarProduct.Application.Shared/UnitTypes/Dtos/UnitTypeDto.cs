﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.UnitTypes.Dtos
{
    public class UnitTypeDto : EntityDto
    {
		public string Name { get; set; }

        public Boolean IsActive {  get; set; }  

    }
}