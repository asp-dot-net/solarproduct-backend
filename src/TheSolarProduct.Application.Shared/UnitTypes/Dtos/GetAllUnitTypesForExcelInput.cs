﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.UnitTypes.Dtos
{
    public class GetAllUnitTypesForExcelInput
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }



    }
}