﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.PriceItemLists.Dtos;

namespace TheSolarProduct.PriceItemLists
{
   public interface IPriceItemListsAppService : IApplicationService
    {
        Task<PagedResultDto<GetPriceItemListsForViewDto>> GetAll(GetAllPriceItemListsInput input);
        Task<GetPriceItemListsForViewDto> GetPriceItemListsForView(int id);
        Task<GetPriceItemListsForEditOutput> GetPriceItemListsForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditPriceItemListsDto input);
        Task Delete(EntityDto input);
    }
}
