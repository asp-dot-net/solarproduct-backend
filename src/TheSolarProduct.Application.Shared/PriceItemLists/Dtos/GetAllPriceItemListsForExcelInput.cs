﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PriceItemLists.Dtos
{
    public class GetAllPriceItemListsForExcelInput
    {
        public string Filter { get; set; }

        public string Name { get; set; }
    }
}
