﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PriceItemLists.Dtos
{
    public class GetPriceItemListsForViewDto
    {
        public PriceItemListsDto PriceItemLists { get; set; }
    }
}
