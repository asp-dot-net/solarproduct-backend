﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PriceItemLists.Dtos
{
    public class GetAllPriceItemListsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string Name { get; set; }
    }
}
