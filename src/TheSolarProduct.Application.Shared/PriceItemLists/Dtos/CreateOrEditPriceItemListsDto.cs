﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.PriceItemLists.Dtos
{
    public class CreateOrEditPriceItemListsDto : EntityDto<int?>
    {
        [Required]
        public string Name { get; set; }
        public  Boolean IsActive { get; set; }
    }
}
