﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PriceItemLists.Dtos
{
    public class GetPriceItemListsForEditOutput
    {
        public CreateOrEditPriceItemListsDto PriceItemLists { get; set; }
    }
}
