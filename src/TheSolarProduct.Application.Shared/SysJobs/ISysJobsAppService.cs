﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TheSolarProduct.SysJobs
{
    public interface ISysJobsAppService : IApplicationService
    {
        Task GetGreenbotDetails();

        Task UpdateGreenbotDetails();

        Task UpdateFbLeads();

        Task AutoAssignLeads();

        Task GetGreenDealJobInformation();

        Task GetAllRECDetails(TimeZoneInfo timeZoneInfo);

        Task UpdateRECDetails();

        Task GetCimetDetails();
    }
}
