﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.RejectReasons.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Review.Dtos;
using System.Collections.Generic;

namespace TheSolarProduct.Review
{
    public interface IReviewAppServicecs : IApplicationService 
    {
        Task<PagedResultDto<GetReviewViewDto>> GetReviewData(GetAllReviewInput input);
        Task SendSms(SmsEmailDto input);
        Task SendEmail(SmsEmailDto input);
        Task UpdateAssignUserID(List<int> Ids, int? userId);
        //Task UpdateReview(List<CreateOrEditJobReviewDto> jobReviewList);
        Task<List<GetJobReviewForEditOutput>> GetJobReviewByJobId(int jobid);
    }
}