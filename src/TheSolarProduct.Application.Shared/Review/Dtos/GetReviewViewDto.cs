﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Jobs.Dtos;

namespace TheSolarProduct.Review.Dtos
{
   public class GetReviewViewDto
    {
        public JobDto Job { get; set; }
        public string ReminderTime { get; set; }
        public string ActivityDescription { get; set; }
        public string ActivityComment { get; set; }
        public string LeadCompanyName { get; set; }
        public int? Yes { get; set; }
        public int? No { get; set; }
        public string AssignUserName { get; set; }
        public DateTime? AssignDate { get; set; }
        public int? TotalEmailSend { get; set; }
        public int? EmailSend { get; set; }
        public int? EmailPending { get; set; }

        public decimal? TotalRating { get; set; }
        public int? totalraterecords { get; set; }
    }
}
