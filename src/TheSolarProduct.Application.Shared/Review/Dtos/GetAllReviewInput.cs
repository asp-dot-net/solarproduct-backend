﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Review.Dtos
{
    public class GetAllReviewInput : PagedAndSortedResultRequestDto
    {

        public string FilterName { get; set; }

        public string Filter { get; set; }

        public int? OrganizationUnit { get; set; }


        public string stateNameFilter { get; set; }

        public List<int> jobStatusIDFilter { get; set; }

        public int? jobTypeId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? Reviewrate { get; set; }

        public int? IsAssign { get; set; }

        public  int? ReviewType { get; set; }
        public  int? emailsend { get; set; }
        public  string Reviewdatefilter { get; set; }
        
    }
}
