﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.FreightCompanyes.Dtos;

namespace TheSolarProduct.IncoTerms.Dtos
{
    public class GetIncoTermForVIewDto
    {
        public IncoTermDto IncoTerm { get; set; }
    }
}
