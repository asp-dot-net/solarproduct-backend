﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.IncoTerms.Dtos
{
    public class GetAllIncoTermInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
