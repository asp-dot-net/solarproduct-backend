﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DashboardSales.Dtos
{
    public class GetLeadOverViewDto
    {
        public int? Assigned { get; set; }

        public int? Cold { get; set;}

        public int? Hot { get; set;}

        public int? Warm { get; set;}

        public int? UnHandled { get; set; }

        public int? Canceled { get; set;}

    }
}
