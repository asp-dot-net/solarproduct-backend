﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DashboardSales.Dtos
{
    public class GetHeaderValue
    {
        public string Name { get; set; }

        public string SystemCapacity { get; set; }

        public int Rank { get; set; }
    }
}
