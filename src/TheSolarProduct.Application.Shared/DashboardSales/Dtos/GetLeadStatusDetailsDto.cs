﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DashboardSales.Dtos
{
    public class GetLeadStatusDetailsDto
    {
        public string LeadSource { get; set; }

        public int? Lead { get; set;}
               
        public int? Upgrade { get; set;}
               
        public int? Sales { get; set; }
               
        public decimal? Ratio { get; set; }
    }
}
