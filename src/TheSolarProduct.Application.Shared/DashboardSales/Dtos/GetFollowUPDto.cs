﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DashboardSales.Dtos
{
    public class GetFollowUPDto
    {
        public string UserName { get; set; }

        public string Mobile { get; set; }

        public string LeadStatus { get; set; }

        public string LeadSource { get; set; }

        public string Comment { get; set;}
    }
}
