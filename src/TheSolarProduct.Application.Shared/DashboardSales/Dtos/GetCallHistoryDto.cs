﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DashboardSales.Dtos
{
    public class GetCallHistoryDto
    {
        public int? Inbound { get; set; }

        public int? Outbound { get; set; }

        public int? Missed { get; set; }

        public int? NotAnswerd { get; set; }

    }
}
