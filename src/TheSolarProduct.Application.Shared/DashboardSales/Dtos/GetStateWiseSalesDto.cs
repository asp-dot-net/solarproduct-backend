﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DashboardSales.Dtos
{
    public class GetStateWiseSalesDto
    {
        public int? NSW { get; set; }

        public int? QLD { get; set; }

        public int? VIC { get; set; }

        public int? SA { get; set; }
    }
}
