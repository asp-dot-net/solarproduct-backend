﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DashboardSales.Dtos
{
    public class GetLeadSalesListDto
    {
        public string UserName { get; set; }
        public decimal SystemCapacity { get; set; }

        public decimal? NoOfPanel { get; set; }
    }
}
