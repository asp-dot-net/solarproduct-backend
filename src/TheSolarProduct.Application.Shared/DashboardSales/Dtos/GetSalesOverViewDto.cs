﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DashboardSales.Dtos
{
    public class GetSalesOverViewDto
    {
        public int? TotalJobs { get; set; }

        public int? Planned { get; set; }

        public int? active { get; set; }

        public int? JobBook { get; set; }

        public int? Installed { get; set; }

        public int? Can_Hold { get; set; }
    }
}
