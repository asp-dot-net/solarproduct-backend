﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.DashboardSales.Dtos;
using TheSolarProduct.Jobs.Dtos;

namespace TheSolarProduct.DashboardSales
{
    public interface IDashboardSalesAppService : IApplicationService
    {
        Task<List<CommonLookupDto>> GetUsers(int org);

        Task<GetLeadOverViewDto> GetLeadOverView(GetLeadOverViewInput input);

        Task<GetSalesOverViewDto> GetSalesOverView(GetLeadOverViewInput input);

        Task<GetCallHistoryDto> GetCallHistory(GetLeadOverViewInput input);

        Task<GetStateWiseSalesDto> getStateWiseSales(GetLeadOverViewInput input);

        Task<List<GetFollowUPDto>> getFollowUP(GetLeadOverViewInput input);

        Task<List<GetLeadStatusDetailsDto>> getLeadStatusDetail(GetLeadOverViewInput input);

        Task<GetHeaderValue> getHeaderValue(GetLeadOverViewInput input);

        Task<List<GetLeadSalesListDto>> getLeadSalesList(GetLeadOverViewInput input);

        Task<List<string>> getMessageBoardDataList(int OrgId);



    }
}
