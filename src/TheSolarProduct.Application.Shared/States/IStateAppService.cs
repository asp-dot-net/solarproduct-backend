﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.States.Dtos;
using TheSolarProduct.StreetTypes.Dtos;

namespace TheSolarProduct.States
{
    public interface IStateAppService : IApplicationService
    {
        Task<PagedResultDto<GetStateForViewDto>> GetAll(GetAllStateInput input);

        Task UpdateRebate(StatesDto state);

        Task<StatesDto> GetStateForEdit(EntityDto input);

    }
}
