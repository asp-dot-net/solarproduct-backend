﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;

namespace TheSolarProduct.States.Dtos
{
    public class StatesDto : EntityDto
    {
        public string Name { get; set; }

        public bool IsReport { get; set; }

        public decimal? BatteryRebate { get; set; }

        public bool IsActiveBatteryRebate { get; set; }

        public decimal? BatteryBess { get; set; }
    }
}
