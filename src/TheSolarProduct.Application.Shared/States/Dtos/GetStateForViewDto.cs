﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.States.Dtos
{
    public class GetStateForViewDto
    {
        public StatesDto States { get; set; }
    }
}
