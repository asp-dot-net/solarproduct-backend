﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.States.Dtos
{
    public class GetAllStateInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
