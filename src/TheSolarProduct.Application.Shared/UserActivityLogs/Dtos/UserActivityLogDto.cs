﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.UserActivityLogs.Dtos
{
    public class UserActivityLogDto : EntityDto<int?>
    {
        public int? ActionId { get; set; }

        public string ActionNote { get; set; }

        public int? SectionId { get; set; }

        public string Section { get; set; }
    }
}
