﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.UserActivityLogs.Dtos
{
    public class GetAllUserActivityLogDto
    {
        public UserActivityLogDto UserActivityLogs { get; set; }

        public DateTime? CreationTime { get; set; }

        public string CreatedBy { get; set; }

        public string Action { get; set; }

    }
}
