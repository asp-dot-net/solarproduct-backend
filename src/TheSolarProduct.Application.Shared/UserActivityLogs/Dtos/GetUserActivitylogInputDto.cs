﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.UserActivityLogs.Dtos
{
    public class GetUserActivitylogInputDto : PagedAndSortedResultRequestDto
    {

        public DateTime? Sdate { get; set; }

        public DateTime? Edate { get; set; }

        public int? UserId { get; set; }
    }
}
