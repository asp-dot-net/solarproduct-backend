﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ECommerces.EcommerceProductItems.Dtos;
using TheSolarProduct.UserActivityLogs.Dtos;

namespace TheSolarProduct.UserActivityLogs
{
    public interface IUserActivityLogAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllUserActivityLogDto>> GetAll(GetUserActivitylogInputDto input);

        Task Create(UserActivityLogDto input);
    }
}
