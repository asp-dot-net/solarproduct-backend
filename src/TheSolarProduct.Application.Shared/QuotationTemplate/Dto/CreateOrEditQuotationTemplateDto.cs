﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.QuotationTemplate.Dto
{
    public class CreateOrEditQuotationTemplateDto : EntityDto<int?>
    {
        public int TenantId { get; set; }

        public int OrganizationUnitId { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1)]
        public string TemplateName { get; set; }

        [Required]
        public string ViewHtml { get; set; }

        public string ApiHtml { get; set; }

        public bool IsActive { get; set; }

        public int TemplateTypeId { get; set; }

        public string Subject { get; set; }
    }
}
