﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuotationTemplate.Dto
{
    public class GetQuotationTemplateForViewDto
    {
        public QuotationTemplateDto QuotationTemplates { get; set; }
    }
}
