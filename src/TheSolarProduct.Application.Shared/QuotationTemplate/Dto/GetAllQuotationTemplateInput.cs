﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuotationTemplate.Dto
{
    public class GetAllQuotationTemplateInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int OrganizationId { get; set; }

        public int TemplateTypeId { get; set; }
    }
}
