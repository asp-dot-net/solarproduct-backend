﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuotationTemplate.Dto
{
    public class GetQuotationTemplateForEditOutput
    {
        public CreateOrEditQuotationTemplateDto QuotationTemplate { get; set; }
    }
}
