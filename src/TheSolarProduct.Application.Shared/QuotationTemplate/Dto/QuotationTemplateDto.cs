﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuotationTemplate.Dto
{
    public class QuotationTemplateDto : EntityDto
    {
        public string TemplateName { get; set; }

        public string OrganazationUnitsName { get; set; }

        public string ViewHtml { get; set; }

        public string ApiHtml { get; set; }

        public bool IsActive { get; set; }

        public string TemplateType { get; set; }
    }
}
