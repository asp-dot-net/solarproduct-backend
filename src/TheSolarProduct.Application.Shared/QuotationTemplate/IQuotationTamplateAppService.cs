﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.QuotationTemplate.Dto;

namespace TheSolarProduct.QuotationTemplate
{
    public interface IQuotationTamplateAppService
    {
        Task<PagedResultDto<GetQuotationTemplateForViewDto>> GetAll(GetAllQuotationTemplateInput input);

        Task<GetQuotationTemplateForEditOutput> GetQuotationTemplateForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditQuotationTemplateDto input);

        Task Delete(EntityDto input);

        Task<List<CommonLookupDto>> GetCustomTemplateForTableDropdown(int leadId);
    }
}
