﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.TransportationCosts.Dtos;

namespace TheSolarProduct.TransportationCosts
{
    public interface ITransportationCostAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllTransportCostForViewDto>> GetAll(GetTransportCostInputDto input);

        Task<CreateOrEditTransportCost> GetTransportCostForEdit(EntityDto input);

        Task Delete(EntityDto input);

        Task CreateOrEdit(CreateOrEditTransportCost input);


    }
}
