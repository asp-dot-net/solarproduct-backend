﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.TransportationCosts.Dtos
{
    public class GetAllTransportCostForViewDto : EntityDto
    {
        public string Jobs { get; set; }

        public string TransportCompany { get; set; }

        public string Mobile { get; set; }

        public string TrackingNo { get; set; }

        public decimal? Amount { get; set; }

        public DateTime? Created {  get; set; }

    }
}
