﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.TransportationCosts.Dtos
{
    public class GetTransportCostInputDto : PagedAndSortedResultRequestDto
    {
        public string FilterName { get; set; }

        public string FilterText { get; set; }

        public DateTime? StartDate { get; set; } 

        public DateTime? EndDate { get; set;}
    }
}
