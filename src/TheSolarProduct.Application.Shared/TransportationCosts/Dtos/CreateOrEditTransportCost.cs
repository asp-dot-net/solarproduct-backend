﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Jobs.Dtos;

namespace TheSolarProduct.TransportationCosts.Dtos
{
    public class CreateOrEditTransportCost : EntityDto<int?>
    {
        public int? TransportCompanyId { get; set; }

        //public List<int?> Jobids { get; set; }

        public string Mobile { get; set; }

        public string TrackingNo { get; set; }

        public decimal? Amount { get; set; }

        public string Notes { get; set; }

        public List<CommonLookupDto> jobnumber { get; set; }
    }
}
