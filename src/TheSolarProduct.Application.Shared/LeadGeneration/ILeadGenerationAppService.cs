﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.Installation.Dtos;
using TheSolarProduct.LeadGeneration.Dtos;
using TheSolarProduct.Leads.Dtos;

namespace TheSolarProduct.LeadGeneration
{
    public interface ILeadGenerationAppService : IApplicationService
    {
        Task<PagedResultDto<GetLeadGenerationForViewDto>> GetAllMyLeadGeneration(GellAllMyLeadGenerationInput input);

        Task TransferToSalesRep(List<int> leadIds, int userId, int sectionId);
        
        Task CreateLeadGenAppointment(CreateOrEditLeadGenAppointmentDto input);

        bool CheckAppointmentExist(string appointmentDate, int? appointmentFor);

        Task<List<LeadsAppointmentCalendarDto>> GetAppointmentCalendar(int appointentFor, DateTime calendarStartDate, int? orgID, string areaNameFilter, string state, int createdBy);

        Task Delete(EntityDto input);

        Task<PagedResultDto<GetAllCommisionViewDto>> GetCommissionJobs(GellAllCommissionInput input);

        Task<FileDto> GetCommissionJobsExcel(GellAllCommissionExcelInput input);

        Task<double> getCommissionAmountByUserId(int UserId);

        Task CreateCommission(CreateCommissionDto input);

        Task<List<GetInstallerMapDto>> GetLeadGenMap(GetAllInputMapDto input);

    }
}
