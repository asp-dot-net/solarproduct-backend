﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Installation.Dtos;

namespace TheSolarProduct.LeadGeneration.Dtos
{
    public class LeadsAppointmentCalendarDto
    {
        public DateTime AppointmentDate { get; set; }
        public int AppointmentsCount { get; set; }
        public List<InstallationLeadsAppointmentDto> Appointments { get; set; }

    }
}
