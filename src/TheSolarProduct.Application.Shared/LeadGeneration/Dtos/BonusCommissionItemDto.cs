﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.LeadGeneration.Dtos
{
    public class BonusCommissionItemDto : EntityDto<int?>
    {
        public int? BonusId { get; set; }
        public decimal? Amount { get; set; }

        public int? JobId { get; set; }

        public DateTime? CommitionDate { get; set; }

        public string CommissionNote { get; set; }

        public int? UserId { get; set; }

        public string Notes { get; set; }
    }
}
