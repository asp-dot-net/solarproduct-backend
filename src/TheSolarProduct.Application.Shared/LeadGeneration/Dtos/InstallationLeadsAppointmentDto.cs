﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.LeadGeneration.Dtos
{
    public class InstallationLeadsAppointmentDto : EntityDto
    {
        public int LeadId { get; set; }

        public string CustomerName { get; set; }

        public long AppointmentFor { get; set; }

        public string AppointmentUserName { get; set; }

        public DateTime AppointmentDateTime { get; set; }

        public string Suburb { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public string Address { get; set;}
    }
}
