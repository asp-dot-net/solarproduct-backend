﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.LeadGeneration.Dtos
{
    public class CreateOrEditLeadGenAppointmentDto : EntityDto<int?>
    {
        public int? TenantId { get; set; }

        public string AppointmentDate { get; set; }

        public int AppointmentFor { get; set; }

        public string Notes { get; set; }

        public int LeadId { get; set; }

        public int SectionId { get; set;}

    }
}
