﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.LeadGeneration.Dtos
{
    public class GellAllMyLeadGenerationInput : PagedAndSortedResultRequestDto
    {
        public string FilterName { get; set; }

        public int OrganizationUnit { get; set; }

        public string Filter { get; set; }

        public int UserId { get; set; }

        public string DateType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
