﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.LeadGeneration.Dtos
{
    public class GellAllCommissionExcelInput
    {
        public string FilterName { get; set; }

        public int OrganizationUnit { get; set; }

        public string Filter { get; set; }

        public int UserId { get; set; }

        public string DateType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? ApointmentFor { get; set; }

        public int excelorcsv { get; set; }
    }
}
