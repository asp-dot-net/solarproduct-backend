﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.LeadGeneration.Dtos
{
    public class GetLeadGenerationForViewDto : EntityDto
    {
        public string CompanyName { get; set; }

        public string IsGoogle { get; set; }

        public DateTime? ReAssignDate { get; set; }

        public int LeadStatusId { get; set; }

        public string LeadStatus { get; set; }

        public string LeadStatusColorClass { get; set; }

        public string JobStatus { get; set; }

        public string Mobile { get; set; }

        public string Address { get; set; }

        public string Suburb { get; set; }

        public string State { get; set; }

        public string PostCode { get; set; }

        public string LeadSource { get; set; }

        public string LeadSourceColorClass { get; set; }

        public string NextFollowup { get; set; }

        public string Description { get; set; }

        public string Comment { get; set; }

        public DateTime? AssignDate { get; set; }

        public string CurrentLeadOwner { get; set; }

        public DateTime? CreationDate { get; set; }

        public bool? IsSelected { get; set; }

        public string LastDescription { get; set; }

        public string LastComment { get; set; }

    }
}
