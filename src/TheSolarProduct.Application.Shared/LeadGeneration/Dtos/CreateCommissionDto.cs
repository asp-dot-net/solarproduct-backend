﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Vendors.Dtos;

namespace TheSolarProduct.LeadGeneration.Dtos
{
    public class CreateCommissionDto
    {
        public DateTime CommitionDate { get; set; } 

        public string Note { get; set; }

        public decimal? Amount { get; set; }

        public List<int> AppointmentIds { get; set; }

        public int SectionId { get; set; }

        public bool? IsBonusCommission {  get; set; }

        public decimal? CommitionActualAmount { get; set; }

        public decimal? TotalBonusAmount { get; set; }


        public List<BonusCommissionItemDto> BonusCommissionItemDto { get; set; }

        public int? UserId { get; set; }

        public List<int?> BonusIds { get; set; }
        public bool? CommitionIsVerified { get; set; }

        public bool? CommitionIsApprove { get; set; }
    }
}
