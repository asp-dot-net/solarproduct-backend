﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.WholeSaleStatuses.Dtos;

namespace TheSolarProduct.WholeSaleStatuses
{
    public interface IWholeSaleStatuseAppService : IApplicationService
    {
        Task<PagedResultDto<GetWholeSaleStatusForViewDto>> GetAll(GetAllWholeSaleLeadStatusInput input);

        Task<WholeSaleStatusDto> GetLeadSourceForEdit(EntityDto input);

        Task CreateOrEdit(WholeSaleStatusDto input);

        Task Delete(EntityDto input);
    }
}
