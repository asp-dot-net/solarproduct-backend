﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleStatuses.Dtos
{
    public class GetAllWholeSaleLeadStatusInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
