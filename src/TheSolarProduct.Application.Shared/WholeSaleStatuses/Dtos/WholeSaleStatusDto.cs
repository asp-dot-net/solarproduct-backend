﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleStatuses.Dtos
{
    public class WholeSaleStatusDto : EntityDto
    {
        public string Name { get; set; }
    }
}
