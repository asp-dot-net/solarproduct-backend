﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleStatuses.Dtos
{
    public class GetWholeSaleStatusForViewDto 
    {
        public WholeSaleStatusDto WholeSaleStatus { get; set; }
    }
}
