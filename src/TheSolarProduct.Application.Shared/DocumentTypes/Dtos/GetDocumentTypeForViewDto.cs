﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DocumentTypes.Dtos
{
    public class GetDocumentTypeForViewDto
    {
        public DocumentTypeDto Documenttype { get; set; }
        public bool IsActive { get; set; }
    }
}
