﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DocumentTypes.Dtos
{
    public class DocumentTypeDto : EntityDto
    {
        public string Title { get; set; }
        public bool IsActive { get; set; }


    }
}
