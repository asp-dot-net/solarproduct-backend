﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DocumentTypes.Dtos
{
   public class GetDocumentTypeforEditOutput
    {
        public CreateorEditDocumentTypeDto DocumenttypeDto { get; set; }
    }
}
