﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.DocumentTypes.Dtos
{
    public class CreateorEditDocumentTypeDto : EntityDto<int?>
    {
        [Required]
        public string Title { get; set; }
        public bool IsActive { get; set; }

    }
}
