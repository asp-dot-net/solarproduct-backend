﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DocumentTypes.Dtos
{
    public class GetAllDocumentTypeInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
