﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.DocumentTypes.Dtos;

namespace TheSolarProduct.DocumentTypes
{
    public interface IDocumentTypeAppService : IApplicationService
    {
        Task<PagedResultDto<GetDocumentTypeForViewDto>> GetAll(GetAllDocumentTypeInput input);

        Task<GetDocumentTypeForViewDto> GetDepartmentForView(int id);

        Task<GetDocumentTypeforEditOutput> GetDepartmentForEdit(EntityDto input);

        Task CreateOrEdit(CreateorEditDocumentTypeDto input);

        Task Delete(EntityDto input);

    }


}
