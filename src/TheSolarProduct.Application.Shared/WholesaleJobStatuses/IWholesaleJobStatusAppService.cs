﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.WholesaleJobStatuses.Dtos;


namespace TheSolarProduct.WholesaleJobStatuses
{
    public interface IWholesaleJobStatusAppService : IApplicationService
    {
        Task<PagedResultDto<GetWholesaleJobStatusForViewDto>> GetAll(GetAllWholesaleJobStatusInput input);

        Task CreateOrEdit(CreateOrEditWholesaleJobStatusDto input);

        Task<GetWholesaleJobStatusForEditOutput> GetWholesaleJobStatusForEdit(EntityDto input);

        Task Delete(EntityDto input);
    }
}
