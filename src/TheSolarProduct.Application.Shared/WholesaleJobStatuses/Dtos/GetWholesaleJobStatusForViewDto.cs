﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.WholesalePropertyTypes.Dtos;

namespace TheSolarProduct.WholesaleJobStatuses.Dtos
{
    public class GetWholesaleJobStatusForViewDto
    {
        public WholesaleJobStatusDto JobStatus { get; set; }
    }
}
