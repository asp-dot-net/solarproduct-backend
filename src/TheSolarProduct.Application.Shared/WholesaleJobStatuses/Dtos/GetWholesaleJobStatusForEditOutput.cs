﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.WholesalePropertyTypes.Dtos;

namespace TheSolarProduct.WholesaleJobStatuses.Dtos
{
    public class GetWholesaleJobStatusForEditOutput
    {
        public CreateOrEditWholesaleJobStatusDto JobStatus { get; set; }
    }
}
