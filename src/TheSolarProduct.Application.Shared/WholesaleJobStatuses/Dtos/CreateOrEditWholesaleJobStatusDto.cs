﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholesaleJobStatuses.Dtos
{
    public class CreateOrEditWholesaleJobStatusDto : EntityDto<int?>
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
