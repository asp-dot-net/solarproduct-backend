﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholesaleJobStatuses.Dtos
{
    public class GetAllWholesaleJobStatusInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
