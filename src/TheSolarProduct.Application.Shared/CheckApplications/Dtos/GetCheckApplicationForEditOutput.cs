﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DeliveryTypes.Dtos;

namespace TheSolarProduct.CheckApplications.Dtos
{
    public class GetCheckApplicationForEditOutput
    {
        public CreateOrEditCheckApplicationDto ChcekApplications { get; set; }
    }
}
