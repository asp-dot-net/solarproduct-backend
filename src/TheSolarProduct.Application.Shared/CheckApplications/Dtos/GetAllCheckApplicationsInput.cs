﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CheckApplications.Dtos
{
    public class GetAllCheckApplicationsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        //public string NameFilter { get; set; }

    }
   
}
