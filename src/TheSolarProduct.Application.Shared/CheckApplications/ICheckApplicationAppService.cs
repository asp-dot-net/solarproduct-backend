﻿using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.States.Dtos;
using Abp;
using TheSolarProduct.Dto;
using TheSolarProduct.TheSolarDemo.Dtos;
using TheSolarProduct.CheckApplications.Dtos;

namespace TheSolarProduct.CheckApplications
{
    
    public interface ICheckApplicationAppService : IApplicationService
    {
       

        Task<PagedResultDto<GetCheckApplicationForViewDto>> GetAll(GetAllCheckApplicationsInput input);
        //Task<GetCheckApplicationForViewDto> GetCheckApplicationForView(int id);

        Task<GetCheckApplicationForEditOutput> GetCheckApplicationForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditCheckApplicationDto input);

        Task Delete(EntityDto input);
        Task<FileDto> GetChcekApplicationToExcel(GetAllChcekApplicationForExcelInput input);
        

    }
}
