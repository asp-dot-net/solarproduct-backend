﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.StreetTypes.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.StreetTypes
{
    public interface IStreetTypesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetStreetTypeForViewDto>> GetAll(GetAllStreetTypesInput input);

        Task<GetStreetTypeForViewDto> GetStreetTypeForView(int id);

		Task<GetStreetTypeForEditOutput> GetStreetTypeForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditStreetTypeDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetStreetTypesToExcel(GetAllStreetTypesForExcelInput input);

		
    }
}