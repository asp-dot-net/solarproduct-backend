﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.StreetTypes.Dtos
{
    public class CreateOrEditStreetTypeDto : EntityDto<int?>
    {

		[Required]
		[StringLength(StreetTypeConsts.MaxNameLength, MinimumLength = StreetTypeConsts.MinNameLength)]
		public string Name { get; set; }
		
		
		[Required]
		[StringLength(StreetTypeConsts.MaxCodeLength, MinimumLength = StreetTypeConsts.MinCodeLength)]
		public string Code { get; set; }

        public Boolean IsActive { get; set; }

    }
}