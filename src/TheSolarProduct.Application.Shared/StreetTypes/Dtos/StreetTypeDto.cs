﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.StreetTypes.Dtos
{
    public class StreetTypeDto : EntityDto
    {
		public string Name { get; set; }

		public string Code { get; set; }
        public Boolean IsActive { get; set; }


    }
}