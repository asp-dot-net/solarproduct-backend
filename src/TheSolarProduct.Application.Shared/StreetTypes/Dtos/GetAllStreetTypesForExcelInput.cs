﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.StreetTypes.Dtos
{
    public class GetAllStreetTypesForExcelInput
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }

		public string CodeFilter { get; set; }



    }
}