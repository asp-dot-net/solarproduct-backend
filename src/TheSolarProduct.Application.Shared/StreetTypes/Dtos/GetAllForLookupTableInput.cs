﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.StreetTypes.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}