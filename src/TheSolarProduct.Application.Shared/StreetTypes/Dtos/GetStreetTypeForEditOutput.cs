﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.StreetTypes.Dtos
{
    public class GetStreetTypeForEditOutput
    {
		public CreateOrEditStreetTypeDto StreetType { get; set; }


    }
}