﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.DocumentLibrarys.Dtos;

namespace TheSolarProduct.DocumentLibrarys
{
    public interface IDocumentLibraryAppsnService : IApplicationService
    {
        Task<PagedResultDto<GetDocumentLibraryForViewDto>> GetAll(GetAllDocumentLibraryInput input);

        Task<GetDocumentLibraryForViewDto> GetDocumentLibraryForView(int id);

        Task<GetDocumentLibraryforEditOutput> GetDocumentLibraryForEdit(EntityDto input);

        Task CreateOrEdit(CreateorEditDocumentLibraryDto input);

        Task Delete(EntityDto input);
   
    }
}
