﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.DocumentLibrarys.Dtos
{
    public class CreateorEditDocumentLibraryDto : EntityDto<int?>
    {
        
        public int TenantId { get; set; }
        public string Title { get; set; }
        public string FileToken { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileType { get; set; }
       

    }
}
