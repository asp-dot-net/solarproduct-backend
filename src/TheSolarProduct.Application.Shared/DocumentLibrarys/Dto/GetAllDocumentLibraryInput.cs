﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DocumentLibrarys.Dtos
{
    public class GetAllDocumentLibraryInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
