﻿using System.Threading.Tasks;
using Abp.Webhooks;

namespace TheSolarProduct.WebHooks
{
    public interface IWebhookEventAppService
    {
        Task<WebhookEvent> Get(string id);
    }
}
