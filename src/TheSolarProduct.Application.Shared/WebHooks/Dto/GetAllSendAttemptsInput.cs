﻿using TheSolarProduct.Dto;

namespace TheSolarProduct.WebHooks.Dto
{
    public class GetAllSendAttemptsInput : PagedInputDto
    {
        public string SubscriptionId { get; set; }
    }
}
