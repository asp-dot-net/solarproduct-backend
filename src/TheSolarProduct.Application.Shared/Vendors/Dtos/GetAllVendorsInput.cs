﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Vendors.Dtos
{
    public class GetAllVendorsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
