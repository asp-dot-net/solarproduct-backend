﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Vendors.Dtos
{
    public class VendorsDto : EntityDto
    {
        public string CompanyName { get; set; }

        public string Address { get; set; }
        public string Notes { get; set; }
       
        public List<VendorContactDto> VendorContacts { get; set; }
    }
}
