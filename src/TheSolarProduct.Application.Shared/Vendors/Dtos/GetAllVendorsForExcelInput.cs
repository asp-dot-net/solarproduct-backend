﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Vendors.Dtos
{
    public class GetAllVendorsForExcelInput
    {
        public string Filter { get; set; }
    }
}
