﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.PaymentMethodes.Dtos;

namespace TheSolarProduct.Vendors.Dtos
{
    public class GetVendorForViewDto
    {
        public VendorsDto Vendors { get; set; }
    }
}
