﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.WholeSaleLeads.Dtos;

namespace TheSolarProduct.Vendors.Dtos
{
    public class CreateOrEditVendorsDto : EntityDto<int?>
    {
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string Notes { get; set; }
        public int? CreditDays { get; set; }

        public List<VendorContactDto> VendorContacts { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Suburb { get; set; }

        public int? SuburbId { get; set; }

        public string State { get; set; }

        public int? StateId { get; set; }

        public string PostCode { get; set; }

        public string UnitNo { get; set; }

        public string UnitType { get; set; }

        public string StreetNo { get; set; }

        public string StreetName { get; set; }

        public string StreetType { get; set; }

        public string latitude { get; set; }

        public string longitude { get; set; }

        public string IsGoogle { get; set; }

    }
}
