﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Vendors.Dtos;

namespace TheSolarProduct.Vendors
{
    public interface IVendorsAppService : IApplicationService
    
    {
        Task<PagedResultDto<GetVendorForViewDto>> GetAll(GetAllVendorsInput input);
        Task<CreateOrEditVendorsDto> GetVendorsForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditVendorsDto input);
        Task Delete(EntityDto input);


    }
}
