﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Currencies.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Currencies
{
    public interface ICurrenciesAppService : IApplicationService
    {
        Task<PagedResultDto<GetCurrenciesForViewDto>> GetAll(GetAllCurrenciesInput input);

        Task CreateOrEdit(CreateOrEditCurrenciesDto input);

        Task<GetCurrenciesForEditOutput> GetCurrenciesForEdit(EntityDto input);
       
        Task Delete(EntityDto input);
        Task<FileDto> GetCurrenciesToExcel(GetAllCurrenciesForExcelInput input);
    }
}
