﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Departments.Dtos;

namespace TheSolarProduct.Currencies.Dtos
{
    public class GetCurrenciesForEditOutput
    {
        public CreateOrEditCurrenciesDto Currency { get; set; }
    }
}
