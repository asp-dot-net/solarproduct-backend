﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Currencies.Dtos
{
    public class GetAllCurrenciesForExcelInput
    {
        public string Filter { get; set; }
    }
}
