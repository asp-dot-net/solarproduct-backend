﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Currencies.Dtos
{
    public class GetAllCurrenciesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
