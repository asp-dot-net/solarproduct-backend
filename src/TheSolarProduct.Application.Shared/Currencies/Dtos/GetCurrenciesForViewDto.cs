﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Departments.Dtos;

namespace TheSolarProduct.Currencies.Dtos
{
    public class GetCurrenciesForViewDto
    {
        public CurrenciesDto Currency { get; set; }
    }
}
