﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Expense.Dtos
{
    public class LeadExpenseLeadSourceLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}