﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.Expense.Dtos
{
    public class LeadExpenseDto : EntityDto
    {
		public DateTime FromDate { get; set; }

		public DateTime ToDate { get; set; }

		public decimal Amount { get; set; }


		 public int? LeadSourceId { get; set; }

		public string Organization { get; set; }
		public string ForPeriod { get; set; }
	}
}