﻿using System;

namespace TheSolarProduct.Expense.Dtos
{
    public class GetLeadExpenseForViewDto
    {
		public int Id { get; set; }
		public LeadExpenseDto LeadExpense { get; set; }

		public string LeadSourceSourceName { get; set;}

		public DateTime FromDate { get; set; }

		public DateTime ToDate { get; set; }

		public decimal Amount { get; set; }


		public int? LeadSourceId { get; set; }

		public string Organization { get; set; }
		public string ForPeriod { get; set; }
        public bool IsEdit { get; set; }

		public decimal? TotalSummaryAmount { get; set; }
		public decimal? TotalNSWAmount { get; set; }
		public decimal? TotalQLDAmount { get; set; }
		public decimal? TotalSAAmount { get; set; }
		public decimal? TotalWAAmount { get; set; }
		public decimal? TotalVICAmount { get; set; }

    }
}