﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Expense.Dtos
{
    public class CreateOrEditLeadExpenseDto : EntityDto<int?>
    {

		public DateTime FromDate { get; set; }


		public DateTime ToDate { get; set; }


		public decimal TotalAmount { get; set; }


		public int? LeadSourceId { get; set; }
		public virtual int? organizationId { get; set; }

		public decimal NswAmount { get; set; }
		public decimal QldAmount { get; set; }
		public decimal SaAmount { get; set; }
		public decimal WaAmount { get; set; }
		public decimal VICAmount { get; set; }

        public int? LeadExpenseId { get; set; }

        public int? NswId { get; set; }
        public int? QldId { get; set; }
        public int? SaId{ get; set; }
        public int? WaId{ get; set; }
        public int? VICId{ get; set; }


    }
}