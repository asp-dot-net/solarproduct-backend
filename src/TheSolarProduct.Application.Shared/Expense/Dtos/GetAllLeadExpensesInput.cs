﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.Expense.Dtos
{
    public class GetAllLeadExpensesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		//public DateTime? MaxFromDateFilter { get; set; }
		//public DateTime? MinFromDateFilter { get; set; }

		//public DateTime? MaxToDateFilter { get; set; }
		//public DateTime? MinToDateFilter { get; set; }

		public decimal? MaxAmountFilter { get; set; }
		public decimal? MinAmountFilter { get; set; }

		public DateTime? FromDateFilter { get; set; }
		public DateTime? ToDateFilter { get; set; }
		public int? LeadSourceSourceIdFilter { get; set; }
		public int? OrganizationId { get; set; }


    }


	public class GetAllLeadExpensesInputReport : PagedAndSortedResultRequestDto
	{
		//public string Filter { get; set; }

		////public DateTime? MaxFromDateFilter { get; set; }
		////public DateTime? MinFromDateFilter { get; set; }

		////public DateTime? MaxToDateFilter { get; set; }
		////public DateTime? MinToDateFilter { get; set; }

		//public decimal? MaxAmountFilter { get; set; }
		//public decimal? MinAmountFilter { get; set; }

		public DateTime? FromDateFilter { get; set; }
		public DateTime? ToDateFilter { get; set; }
		//public int? LeadSourceSourceIdFilter { get; set; }
		//public string LeadSourceSourceNameFilter { get; set; }


	}
}