﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Expense.Dtos
{
    public class GetLeadExpenseForEditOutput
    {
		public CreateOrEditLeadExpenseDto LeadExpense { get; set; }

		public string LeadSourceSourceName { get; set;}


    }
}