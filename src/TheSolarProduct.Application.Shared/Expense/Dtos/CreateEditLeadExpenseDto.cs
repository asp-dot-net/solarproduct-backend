﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Expense.Dtos
{
  public  class CreateEditLeadExpenseDto
    {
		public DateTime FromDate { get; set; }


		public DateTime ToDate { get; set; }


		public decimal TotalAmount { get; set; }


		public int? LeadSourceId { get; set; }
		public virtual int? StateId { get; set; }

		public decimal NswAmount { get; set; }
		public decimal QldAmount { get; set; }
		public decimal SaAmount { get; set; }
		public decimal WaAmount { get; set; }


		public int? LeadExpenseId { get; set; }
	}
}
