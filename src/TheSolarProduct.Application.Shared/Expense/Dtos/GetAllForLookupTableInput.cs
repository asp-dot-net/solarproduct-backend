﻿using Abp.Application.Services.Dto;

namespace TheSolarDemo.Expense.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}