﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Expense.Dtos
{
    public class GetLeadExpenseInvestmentsDto : EntityDto
    {
		public int? TenantId { get; set; }
		public virtual int OrganizationId { get; set; }
		public virtual DateTime FromDate { get; set; }
		public virtual DateTime ToDate { get; set; }
		public virtual string MonthYear { get; set; }
		public virtual float Investment { get; set; }
		public virtual bool Isexsistdata { get; set; }
	}
}
