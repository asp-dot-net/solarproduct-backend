﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Expense.Dtos;
using TheSolarProduct.Dto;
using System.Collections.Generic;
using TheSolarProduct.Leads.Dtos;
using System.Data;

namespace TheSolarProduct.Expense
{
    public interface ILeadExpensesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetLeadExpenseForViewDto>> GetAll(GetAllLeadExpensesInput input);

		Task<GetLeadExpenseForEditOutput> GetLeadExpenseForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditLeadExpenseDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetLeadExpensesToExcel(GetAllLeadExpensesForExcelInput input);

		
		Task<List<LeadExpenseLeadSourceLookupTableDto>> GetAllLeadSourceForTableDropdown();
		Task CreateExpense(List<CreateOrEditLeadExpenseDto> input);

		Task<List<LeadExpenseAddDto>> getAllexpenseTableForEdit(int investmentid);

		Task<TotalCountDto> getTotalCount(int investmentid);
		Task EditExpense(List<LeadExpenseAddDto> input);
		Task<List<LeadExpenseReportDto>> getAllexpenseTableForReport(GetAllexpenseInput input);
		Task<List<LeadStateLookupTableDto>> GetAllStateForTableDropdownForReport(GetAllexpenseInput input);
		Task<List<CreateOrEditLeadExpenseDto>> getAllexpenseTableFor_Edit(DateTime frmdate, int investmentid);
		Task editExpenselist(List<CreateOrEditLeadExpenseDto> input);
		Task<List<LeadStateLookupTableDto>> GetAllStateForTableDropdownReportHeader(GetAllexpenseInput input);
		//Task<List<LeadExpenseLeadSourceLookupTableDto>> GetAllStateForTableDropdownReportHeader();
		GetLeadExpenseInvestmentsDto LeadExpenseExsistenceById(string monthYear);

        Task<FileDto> getAllexpenseTableForReportExport(GetAllexpenseInput input);

    }
}