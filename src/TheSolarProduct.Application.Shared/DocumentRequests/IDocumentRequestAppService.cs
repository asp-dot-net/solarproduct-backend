﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.DocumentRequests.Dtos;

namespace TheSolarProduct.DocumentRequests
{
    public interface IDocumentRequestAppService : IApplicationService
    {
        Task SendDocumentRequestForm(SendDocumentRequestInputDto input);

        Task<DocumentRequestDto> DocumentRequestData(string STR);
    }
}
