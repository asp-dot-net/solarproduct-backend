﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DocumentRequests.Dtos
{
    public class SendDocumentRequestInputDto
    {
        public int JobId { get; set; }

        public int SectionId { get; set; }

        public int DocTypeId { get; set; }

        public string SendMode { get; set; }
    }
}
