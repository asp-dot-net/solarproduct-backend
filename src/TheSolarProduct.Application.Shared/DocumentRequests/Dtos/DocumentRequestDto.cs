﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Quotations.Dtos;

namespace TheSolarProduct.DocumentRequests.Dtos
{
    public class DocumentRequestDto
    {
        public bool Expiry { get; set; }

        public bool IsSubmitted { get; set; }

        public string DocumnetType { get; set; }

        public string RequestedBy { get; set; }
    }
}
