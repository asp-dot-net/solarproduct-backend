﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.EmailTemplates.Dtos;

namespace TheSolarProduct.EmailTemplates
{
	public interface IEmailTemplateAppService : IApplicationService
	{
		Task<PagedResultDto<GetEmailTemplateDto>> GetAll(GetAllEmailTemplateInput input);

		Task<GetEmailTemplateForEdit> GetEmailTemplateForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditEmailTemplateDto input);

		Task Delete(EntityDto input);
	}
}
