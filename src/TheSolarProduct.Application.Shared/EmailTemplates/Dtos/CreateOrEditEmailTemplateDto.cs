﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.EmailTemplates.Dtos
{
	public class CreateOrEditEmailTemplateDto : EntityDto<int?>
	{
		[Required]
		public virtual string TemplateName { get; set; }

		[Required]
		public virtual string Body { get; set; }

		public virtual string Subject { get; set; }

		public virtual int? OrganizationUnitId { get; set; }


	}
}
