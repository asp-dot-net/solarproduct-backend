﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.EmailTemplates.Dtos
{
    public class GetAllEmailTemplateInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        public string NameFilter { get; set; }

        public int OrganizationUnitId { get; set; }
    }
}
