﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.EmailTemplates.Dtos
{
	public class GetEmailTemplateDto
	{
		public EmailTemplateDto EmailTemplate { get; set; }

		public string Organization { get; set; }
	}
}
