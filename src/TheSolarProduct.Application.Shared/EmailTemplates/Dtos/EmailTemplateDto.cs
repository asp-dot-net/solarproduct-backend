﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.EmailTemplates.Dtos
{
    public class EmailTemplateDto : EntityDto
    {
        public string TemplateName { get; set; }

        public string Body { get; set; }

        public string Subject { get; set; }


    }
}
