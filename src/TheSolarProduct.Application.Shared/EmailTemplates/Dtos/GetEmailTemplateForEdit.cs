﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.EmailTemplates.Dtos
{
	public class GetEmailTemplateForEdit
	{
		public CreateOrEditEmailTemplateDto EmailTemplate { get; set; }
	}
}
