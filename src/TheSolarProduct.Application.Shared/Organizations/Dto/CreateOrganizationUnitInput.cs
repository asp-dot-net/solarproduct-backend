﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Organizations;

namespace TheSolarProduct.Organizations.Dto
{
    public class CreateOrganizationUnitInput
    {
        public long? ParentId { get; set; }

        [Required]
        [StringLength(OrganizationUnit.MaxDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        public string OrganizationCode { get; set; }

        public string ProjectId { get; set; }

		public string Logo { get; set; }

		public string ABN { get; set; }

		public string Address { get; set; }

		public string ContactNo { get; set; }
        public string GreenBoatUsername { get; set; }
        public string GreenBoatPassword { get; set; }
        public string defaultFromAddress { get; set; }
        public string defaultFromDisplayName { get; set; }
        public string FoneDynamicsPhoneNumber { get; set; }
        public string FoneDynamicsPropertySid { get; set; }
        public string FoneDynamicsAccountSid { get; set; }
        public string FoneDynamicsToken { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string FileToken { get; set; }
        public string LogoFileName { get; set; }
        public string LogoFilePath { get; set; }
        public string paymentMethod { get; set; }
        public string cardNumber { get; set; }
        public string expiryDateMonth { get; set; }
        public string expiryDateYear { get; set; }
        public string cvn { get; set; }
        public string cardholderName { get; set; }
        public string MerchantId { get; set; }
        public string PublishKey { get; set; }
        public string SecrateKey { get; set; }
        public string businesscode { get; set; }
        public string AuthorizationKey { get; set; }
        public string WestPacSecreteKey { get; set; }
        public string WestPacPublishKey { get; set; }
        public string SurchargeAuthorizationKey { get; set; }
        public string SurchargeSecrateKey { get; set; }
        public string SurchargePublishKey { get; set; }
        public string GreenBoatPasswordForFetch { get; set; }
        public string GreenBoatUsernameForFetch { get; set; }
        public string ABNNumber { get; set; }

        public List<OrganizationUnitMapDto> OrganizationUnitMaps { get; set; }

        public bool IsDefault { get; set; }

        public string AccountId { get; set; }

        public string GateWayAccountEmail { get; set; }

        public string GateWayAccountPassword { get; set; }

        public string smsFrom { get; set; }

        public int? AutoAssignLeadUserId { get; set; }

        public List<STCProviderDto> STCProviderList { get; set; }

        public long? ProjectOrderNo { get; set; }


    }
}