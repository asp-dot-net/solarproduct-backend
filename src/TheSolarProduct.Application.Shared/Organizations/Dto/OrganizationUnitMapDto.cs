﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Organizations.Dto
{
    public class OrganizationUnitMapDto : EntityDto<int?>
    {
        public string MapProvider { get; set; }

        public string MapApiKey { get; set; }
    }
}