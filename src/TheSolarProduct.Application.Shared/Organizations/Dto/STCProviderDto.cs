﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Organizations.Dto
{
    public class STCProviderDto : EntityDto<int?>
    {
        public long? OrganizationUnitId { get; set; }

        public string Provider { get; set; }

        public string Name { get; set; }

        public string UserId { get; set; }

        public string Password { get; set; }
    }
}
