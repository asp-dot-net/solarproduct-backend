﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.EmailSetup.Dto;

namespace TheSolarProduct.EmailSetup
{
    public interface IEmailAppService : IApplicationService
    {
        Task<List<object>> ReceiveEmail(int maxCount = 10);

        Task<string> ReceiveEmailBody(uint index);

        Task<string> SendComposeEmail(EmailMessage emailMessage);
    }
}
