﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.StockOrder.Dtos;
using TheSolarProduct.StockPaymentTrackers.Dtos;

namespace TheSolarProduct.StockPaymentTrackers
{
    public interface IStockPaymentTrackerAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllStockPaymentTrackerWithSummary>> GetAll(GetAllStockPaymentTrackerInput input);

        Task<FileDto> GetStockOrderToExcel(GetStockorderForExcelInput input);

    }
}
