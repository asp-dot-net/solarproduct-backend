﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockPaymentTrackers.Dtos
{
    public class GetAllStockPaymentTrackerOutputDto : EntityDto
    {
        public virtual long OrderNo { get; set; }

        public virtual string VendorInoviceNo { get; set; }

        public virtual string WarehouseLocation { get; set; }

        public virtual DateTime? OrderDate { get; set; }

        public virtual string ManualOrderNo { get; set; }

        public virtual string PaymentMethod { get; set; }

        public virtual DateTime? DeliveryDate { get; set; }

        public virtual string Currency { get; set; }

        public virtual string GSTType { get; set; }

        public int? QTY { get; set; }

        public decimal? Amount { get; set; }

        public decimal? PaidAmount { get; set; }

        public decimal? RemAmount { get; set;}
    }
}
