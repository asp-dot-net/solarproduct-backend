﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockPaymentTrackers.Dtos
{
    public class GetStockPaymentTrackerExportInput
    {
        public string FilterName { get; set; }
        public string Filter { get; set; }
        public int? GstTypeIdFilter { get; set; }
        public int? TransportCompanyFilter { get; set; }
        public List<int?> StockOrderStatusIds { get; set; }
        public List<int?> WarehouseLocationIdFiletr { get; set; }
        public int? PaymentStatusIdFiletr { get; set; }
        public List<int?> PaymentTypeIdFiletr { get; set; }
        public int? VendorIdFiletr { get; set; }
        public int? DeliveryTypeIdFiletr { get; set; }
        public int? StockOrderIdFiletr { get; set; }
        public int OrganizationId { get; set; }
        public string Datefilter { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
