﻿using Abp.Application.Services.Dto;
using Castle.Core;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.StockOrder.Dtos;

namespace TheSolarProduct.StockPaymentTrackers.Dtos
{
    public class GetAllStockPaymentTrackerWithSummary : EntityDto
    {
        public List<GetAllStockPaymentTrackerOutputDto> StockPaymentTrackerList { get; set; }
        public int? Total { get; set; }
        public int? Amount { get; set; }
        public int? PaidAmount { get; set; }
        public int? RemainingAmount { get; set; }
        
    }
}
