﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholesaleTransportTypes.Dtos
{
    public class GetAllWholesaleTransportTypeInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
