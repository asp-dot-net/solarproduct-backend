﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Wholesaleinvoicetype.Dtos;

namespace TheSolarProduct.WholesaleTransportTypes.Dtos
{
    public class GetWholesaleTransportTypeForViewDto
    {
        public TransportTypeDto TransportType { get; set; }
    }
}
