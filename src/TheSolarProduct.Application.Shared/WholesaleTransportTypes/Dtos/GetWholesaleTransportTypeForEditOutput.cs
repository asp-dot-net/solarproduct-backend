﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Wholesaleinvoicetype.Dtos;

namespace TheSolarProduct.WholesaleTransportTypes.Dtos
{
    public class GetWholesaleTransportTypeForEditOutput
    {
        public CreateOrEditWholesaleTransportTypeDto TransportType { get; set; }
    }
}
