﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Series.Dto;
using TheSolarProduct.Wholesaleinvoicetype.Dtos;
using TheSolarProduct.WholesaleTransportTypes.Dtos;

namespace TheSolarProduct.WholesaleTransportTypes
{
    public interface IWholesaleTransportTypeAppService : IApplicationService
    {
        Task<PagedResultDto<GetWholesaleTransportTypeForViewDto>> GetAll(GetAllWholesaleTransportTypeInput input);

        Task CreateOrEdit(CreateOrEditWholesaleTransportTypeDto input);

        Task<GetWholesaleTransportTypeForEditOutput> GetWholesaleTransportTypeForEdit(EntityDto input);

        Task Delete(EntityDto input);
    }
}
