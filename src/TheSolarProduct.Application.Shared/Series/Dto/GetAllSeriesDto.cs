﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.ECommerces.BrandingPartners.Dto;

namespace TheSolarProduct.Series.Dto
{
    public class GetAllSeriesDto
    {
        public SeriesDto Series { get; set; }
        public string Filter { get; set; }
    }
}
