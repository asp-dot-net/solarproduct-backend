﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Series.Dto
{
    public class SeriesDto : EntityDto<int?>
    {
        public int? ProductCategoryId { get; set; }
        public string SeriesName { get; set; }

        public string ProductCategory { get; set; }
    }
}
