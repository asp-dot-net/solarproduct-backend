﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Series.Dto
{
    public class GetAllSeriesinputDto : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
