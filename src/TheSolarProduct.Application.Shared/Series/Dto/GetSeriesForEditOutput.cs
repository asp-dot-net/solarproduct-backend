﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Currencies.Dtos;

namespace TheSolarProduct.Series.Dto
{
    public class GetSeriesForEditOutput
    {
        public CreateOrEditSeriesDto Series { get; set; }
    }
}
