﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ECommerces.BrandingPartners.Dto;
using TheSolarProduct.Series.Dto;


namespace TheSolarProduct.Series
{
    public interface ISeriesAppService : IApplicationService
    {
        Task<PagedResultDto<GetSeriesForViewDto>> GetAll(GetAllSeriesinputDto input);

       // Task<SeriesDto> GetBusinessForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditSeriesDto input);

        Task Delete(EntityDto input);

    }
}
