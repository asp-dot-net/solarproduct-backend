﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.StockTransfers.Dtos;

namespace TheSolarProduct.StockTransfers
{
    public interface IStockTransferAppService : IApplicationService
    {
        Task<PagedResultDto<GetStockTransferForViewDto>> GetAll(GetAllStockTransferInput input);
        Task CreateOrEdit(CreateOrEditStockTransferDto input);
        Task<GetStockTransferForEditOutput> GetStockTransferForEdit(EntityDto input);
        Task Delete(EntityDto input);
        Task<GetStockTransferForViewDto> GetStockTransferById(int id);
        Task<FileDto> GetStockTransferToExcel(GetAllStockTransferForExcelInput input);
        Task<List<GetStockTransferActivityLogViewDto>> GetStockTransferActivityLog(GetStockTransferActivityLogInput input);
        Task<List<StockTransferHistoryDto>> GetStockTransferActivityLogHistory(GetStockTransferActivityLogInput input);
    }
}
