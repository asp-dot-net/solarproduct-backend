﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockTransfers.Dtos
{
    public class GetStockTransferActivityLogViewDto : FullAuditedEntity
    {
        public int? StockTransferId { get; set; }
        public int? ActionId { get; set; }
        public string CreatorUserName { get; set; }
        public string ActionName { get; set; }
        public string ActionNote { get; set; }
        public string LogDate { get; set; }
        public string Body { get; set; }
        public string ActivityNote { get; set; }
        public DateTime? ActivityDate { get; set; }
        public string ActivitysDate { get; set; }
        public string Section { get; set; }
    }
}
