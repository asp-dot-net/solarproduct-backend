﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockTransfers.Dtos
{
    public class ProductItemDto : EntityDto<int?>
    {
        public string ProductCategory { get; set; }
        public string ProductItem { get; set; }
        public int ModelNo { get; set; }

        public decimal Size { get; set; }
        public int Quantity { get; set; }

      
    }
}

