﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockTransfers.Dtos
{
    public class ProductsItemDto : EntityDto<int?>
    {
        public int? StockTransferId { get; set; }
        public int ProductTypeId { get; set; }

        public int? ProductItemId { get; set; }

        public string ProductItem { get; set; }
        public string ProductType{ get; set; }


        public string ModelNo { get; set; }

        public decimal? Size { get; set; }
        public int? Quantity { get; set; }
    }
}
