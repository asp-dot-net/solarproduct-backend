﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockTransfers.Dtos
{
    public class StockTransferDto : EntityDto
    {
        public int TransportCompany { get; set; }

        public int FromLocation { get; set; }
        public int ToLocation { get; set; }

        public int TransportLocation { get; set; }

        public int VehicalNo { get; set; }

        public int TrackingNumber { get; set; }

        public decimal Amount { get; set; }

        public string Notes { get; set; }

        public List<ProductsItemDto> ProductItemInfo { get; set; }
    }
}
