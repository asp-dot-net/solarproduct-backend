﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.StockTransfers.Dtos
{
    public class GetStockTransferForViewDto : EntityDto
    {   public long? TransferNo { get; set; }
        public string TransportCompany { get; set; }

        public string WarehouseLocationFrom { get; set; }

        public string WarehouseLocationTo { get; set; }

        public  string Organization { get; set; }

        public  string VehicalNo { get; set; }

        public  string TrackingNumber { get; set; }

        public  decimal? Amount { get; set; }

        public  string AdditionalNotes { get; set; }

        public int? Qty { get; set; }
        public string CornetNo { get; set; }
        public DateTime TransferDate { get; set; }

        public virtual int Transferd { get; set; }
        public virtual DateTime TransferByDate { get; set; }
        public virtual int Received { get; set; }
        public virtual DateTime ReceivedByDate { get; set; }

        public List<ProductsItemDto> ProductItemInfo { get; set; }

    }
}
