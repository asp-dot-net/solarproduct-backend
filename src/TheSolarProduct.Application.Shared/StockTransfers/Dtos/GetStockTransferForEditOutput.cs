﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.StockOrder.Dtos;

namespace TheSolarProduct.StockTransfers.Dtos
{
    public class GetStockTransferForEditOutput 
    {
        public CreateOrEditStockTransferDto StockTransfer { get; set; }
    }
}
