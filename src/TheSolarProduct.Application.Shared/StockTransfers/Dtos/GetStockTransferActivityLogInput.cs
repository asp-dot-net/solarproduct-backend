﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockTransfers.Dtos
{
    public class GetStockTransferActivityLogInput
    {
        public int StockTransferId { get; set; }
        public int? ActionId { get; set; }
        public int? SectionId { get; set; }
        public bool? CurrentPage { get; set; }
        public bool AllActivity { get; set; }
    }
}
