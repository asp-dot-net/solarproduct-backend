﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockTransfers.Dtos
{
    public class GetAllStockTransferForExcelInput
    {
        public string FilterName { get; set; }
        public string Filter { get; set; }
        public int? WarehouseLocationFromIdFilter { get; set; }
        public int? WarehouseLocationToIdFilter { get; set; }
        public int? TransportCompanyFilter { get; set; }
        public int OrganizationId { get; set; }
        public string Datefilter { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
