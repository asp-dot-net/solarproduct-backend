﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.StockOrder.Dtos;

namespace TheSolarProduct.StockTransfers.Dtos
{
    public class CreateOrEditStockTransferDto : EntityDto<int?>
    {
        public  int? TransportCompanyId { get; set; }

        public  int? WarehouseLocationFromId { get; set; }

        public int? WarehouseLocationToId { get; set; }

        public virtual int OrganizationId { get; set; }

        public virtual string VehicalNo { get; set; }

        public virtual string TrackingNumber { get; set; }

        public virtual decimal? Amount { get; set; }

        public virtual string AdditionalNotes { get; set; }
        public virtual string CornetNo { get; set; }

        public virtual long? TransferNo { get; set; }
        public virtual int Transferd { get; set; }
        public virtual DateTime TransferByDate { get; set; }
        public virtual int Received { get; set; }
        public virtual DateTime ReceivedByDate { get; set; }

        
        public List<ProductsItemDto> ProductItemInfo { get; set; }
    }
}
