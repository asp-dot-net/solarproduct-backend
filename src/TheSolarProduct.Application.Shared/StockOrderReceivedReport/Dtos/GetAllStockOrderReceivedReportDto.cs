﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.StockOrder.Dtos;

namespace TheSolarProduct.StockOrderReceivedReport.Dtos
{
    public class GetAllStockOrderReceivedReportDto
    {
        public List<GetAllStockOrderReceivedReport> StockOrderReceivedReport { get; set; }
        public int AustraliaSummary { get; set; }
        public int BrisbaneSummary { get; set; }
        public int MelbourneSummary { get; set; }

        public int SydenySummary { get; set; }
        public int PerthSummary { get; set; }
        public int DarwinSummary { get; set; }

        public int HobartSummary { get; set; }
        public int AdelaideSummary { get; set; }
        public int CanberraSummary { get; set; }
    }
}
