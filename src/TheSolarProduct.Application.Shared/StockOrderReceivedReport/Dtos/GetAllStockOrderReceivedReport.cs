﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockOrderReceivedReport.Dtos
{
    public class GetAllStockOrderReceivedReport
    {
        public int? PurchaseOrderId { get; set; }
        public int ProductTypeId { get; set; }

        public int? ProductItemId { get; set; }

        public string ProductItem { get; set; }

        public int? Quantity { get; set; }
        public decimal? Size { get; set; }
        public string ModelNo { get; set; }
        public int Australia { get; set; }
        public int? Brisbane { get; set; }
        public int? Melbourne { get; set; }

        public int? Sydeny { get; set; }
        public int? Perth { get; set; }
        public int? Darwin { get; set; }

        public int? Hobart { get; set; }
        public int? Adelaide { get; set; }
        public int? Canberra { get; set; }
        public int? Total { get; set; }
    }
}
