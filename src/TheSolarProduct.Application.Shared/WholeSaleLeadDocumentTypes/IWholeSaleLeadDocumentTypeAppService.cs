﻿using System.Threading.Tasks;
using TheSolarProduct.WholeSaleLeadDocumentTypes.Dtos;
using Abp.Application.Services;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.WholeSaleLeadDocumentTypes
{
    public interface IWholeSaleLeadDocumentTypeAppService : IApplicationService
    {
        Task<PagedResultDto<WholeSaleLeadDocumentTypeDto>> GetAll(GetAllWholeSaleLeadDocumentTypeInput input);

        Task<WholeSaleLeadDocumentTypeDto> GetWholeSaleLeadDocumentTypeForView(int id);

        Task<GetWholeSaleLeadDocumentTypeforEditOutput> GetWholeSaleLeadDocumentTypeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditWholeSaleLeadDocumentTypeDto input);

        Task Delete(EntityDto input);
    }
}
