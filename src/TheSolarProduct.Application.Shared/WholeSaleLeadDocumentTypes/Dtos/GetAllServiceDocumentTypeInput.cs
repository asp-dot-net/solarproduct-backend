﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleLeadDocumentTypes.Dtos
{
    public class GetAllWholeSaleLeadDocumentTypeInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
