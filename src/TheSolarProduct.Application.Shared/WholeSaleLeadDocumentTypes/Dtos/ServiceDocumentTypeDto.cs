﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.WholeSaleLeadDocumentTypes.Dtos
{
    public class WholeSaleLeadDocumentTypeDto : EntityDto
    {
        public string Title { get; set; }

        public bool IsDocumentRequest { get; set; }

    }
}
