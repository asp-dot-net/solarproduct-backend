﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.ServiceStatuses.Dtos;

namespace TheSolarProduct.ServiceStatuses
{
    public interface IServiceStatusesAppService : IApplicationService
    {
        Task<PagedResultDto<GetServiceStatusForViewDto>> GetAll(GetAllServiceStatusInput input);

        Task<GetServiceStatusForViewDto> GetServiceStatusForView(int id);

        Task<GetServiceStatusForEditOutput> GetServiceStatusForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditServiceStatusDto input);

        Task Delete(EntityDto input);

       
    }
}
