﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using TheSolarProduct.ServiceCategorys.Dtos;

namespace TheSolarProduct.ServiceStatuses.Dtos
{
    public class GetServiceStatusForEditOutput
    {
        public CreateOrEditServiceStatusDto serviceStatuses { get; set; }

    }
}