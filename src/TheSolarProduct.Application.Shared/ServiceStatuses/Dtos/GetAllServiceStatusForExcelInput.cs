﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.ServiceStatuses.Dtos
{
    public class GetAllServiceStatusForExcelInput
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }

    }
}