﻿using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.ServiceStatuses.Dtos
{
    public class ServiceStatusDto : EntityDto
    {
        public string Name { get; set; }
        public Boolean IsActive { get; set; }
    }
}