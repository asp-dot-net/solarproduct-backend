﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.ServiceStatuses.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}