﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.ServiceStatuses.Dtos
{
    public class CreateOrEditServiceStatusDto : EntityDto<int?>
    {

        [Required]
        public string Name { get; set; }
        public Boolean IsActive { get; set; }

    }
}