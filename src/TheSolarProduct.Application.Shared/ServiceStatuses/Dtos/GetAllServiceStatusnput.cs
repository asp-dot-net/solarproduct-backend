﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.ServiceStatuses.Dtos
{
    public class GetAllServiceStatusInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }

    }
}