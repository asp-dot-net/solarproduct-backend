﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DeliveryTypes.Dtos;

namespace TheSolarProduct.StockFroms.Dtos
{
    public class GetStockFromsForEditOutput
    {
        public CreateOrEditStockFromsDto stockfroms { get; set; }
    }
}
