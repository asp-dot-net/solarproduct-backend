﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockFroms.Dtos
{
    public class GetAllStockFromsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
