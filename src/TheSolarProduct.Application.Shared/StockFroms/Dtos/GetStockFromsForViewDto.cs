﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Currencies.Dtos;

namespace TheSolarProduct.StockFroms.Dtos
{
    public class GetStockFromsForViewDto
    {
        public StockFromsDto stockfroms { get; set; }
    }
}
