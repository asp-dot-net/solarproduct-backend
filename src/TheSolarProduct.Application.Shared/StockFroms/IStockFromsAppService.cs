﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.StockFroms.Dtos;
using TheSolarProduct.TransportCompanies.Dto;

namespace TheSolarProduct.StockFroms
{
    public interface IStockFromsAppService : IApplicationService
    {
        Task<PagedResultDto<GetStockFromsForViewDto>> GetAll(GetAllStockFromsInput nput);

        Task CreateOrEdit(CreateOrEditStockFromsDto input);

        Task<GetStockFromsForEditOutput> GetStockFromsForEdit(EntityDto input);

        Task<FileDto> GetStockFromsToExcel(GetAllStockFromsForExcelInput nput);

        Task Delete(EntityDto input);

    }
}
