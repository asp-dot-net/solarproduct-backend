﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DashboardMessages.Dtos
{
    public class GetAllDashboardMessageViewDto
    {
        public CreateOrEditDashboardMessageDto DashboardMessageDto { get; set; }
    }
}
