﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DashboardMessages.Dtos
{
    public class GetAllDashboardMessageInputDto : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string DashboardType { get; set; }

        public int? OrgId { get; set; }

    }
}
