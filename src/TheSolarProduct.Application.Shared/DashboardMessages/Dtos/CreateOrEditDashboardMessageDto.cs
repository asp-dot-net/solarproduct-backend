﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DashboardMessages.Dtos
{
    public class CreateOrEditDashboardMessageDto : EntityDto<int?>
    {
        public int? OrgId { get; set; }

        public string Message { get; set; }

        public bool IsActive { get; set; }

        public string DashboardType { get; set; }
    }
}
