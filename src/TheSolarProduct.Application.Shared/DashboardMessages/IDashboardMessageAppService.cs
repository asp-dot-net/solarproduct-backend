﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.CommissionRanges.Dtos;
using TheSolarProduct.DashboardMessages.Dtos;

namespace TheSolarProduct.DashboardMessages
{
    public interface IDashboardMessageAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllDashboardMessageViewDto>> GetAll(GetAllDashboardMessageInputDto input);

        Task<GetAllDashboardMessageViewDto> GetDashboardMessageForView(int id);

        Task CreateOrEdit(CreateOrEditDashboardMessageDto input);

        Task Delete(EntityDto input);
    }
}
