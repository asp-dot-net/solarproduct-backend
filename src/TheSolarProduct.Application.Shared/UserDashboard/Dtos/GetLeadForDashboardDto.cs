﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.UserDashboard.Dtos
{
	public class GetLeadForDashboardDto : FullAuditedEntityDto
	{
		public string CompanyName { get; set; }

		public string Email { get; set; }

		public string Phone { get; set; }

		public string Mobile { get; set; }

		public string Address { get; set; }

		public string PostCode { get; set; }

		public string UnitNo { get; set; }

		public string UnitType { get; set; }

		public string StreetNo { get; set; }

		public string StreetName { get; set; }

		public string StreetType { get; set; }

		public string StateName { get; set; }

		public string LeadSourceName { get; set; }

		public DateTime? LeadAssignDate { get; set; }

		public string LastReminder { get; set; }

		public DateTime? LastReminderDate { get; set; }
		public int inactive { get; set; }
		public DateTime? followupdate { get; set; }

		public string LeadSourceImage { get; set; }

	}
}
