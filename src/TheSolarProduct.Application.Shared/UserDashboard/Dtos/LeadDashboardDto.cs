﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.UserDashboard.Dtos
{
	public class LeadDashboardDto
	{
		public string CompanyName { get; set; }

		public string Email { get; set; }

		public string Phone { get; set; }

		public string Mobile { get; set; }

		public string Address { get; set; }

		public string Requirements { get; set; }


		public int SuburbId { get; set; }

		public int StateId { get; set; }

		public string PostCode { get; set; }

		public int LeadSourceId { get; set; }


		public string UnitNo { get; set; }

		public string UnitType { get; set; }

		public string StreetNo { get; set; }

		public string StreetName { get; set; }

		public string StreetType { get; set; }

		public int? LeadStatusID { get; set; }

		public int? AssignToUserID { get; set; }
	}
}
