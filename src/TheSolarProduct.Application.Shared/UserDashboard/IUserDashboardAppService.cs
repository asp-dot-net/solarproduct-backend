﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.UserDashboard.Dtos;

namespace TheSolarProduct.UserDashboard
{
	public interface IUserDashboardAppService : IApplicationService
	{
		Task<List<GetLeadForDashboardDto>> GetUserLeadForDashboard();

		Task<List<GetLeadForDashboardDto>> GetInactiveLeadForDashboard(int Id);

		int GetAllUnAssignedCount();
	}
}