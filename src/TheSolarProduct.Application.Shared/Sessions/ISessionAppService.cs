﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TheSolarProduct.Sessions.Dto;

namespace TheSolarProduct.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();

        Task<UpdateUserSignInTokenOutput> UpdateUserSignInToken();
    }
}
