﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Vendors.Dtos;

namespace TheSolarProduct.Vouchers.Dtos
{
    public class VoucherDto : EntityDto
    {
        public virtual string VoucherName { get; set; }

        public virtual string FilePath { get; set; }

        public virtual string FileName { get; set; }

        public virtual bool? IsActive { get; set; }

        public List<MultipleOrganizationDto> MultipleOrganization { get; set; }
    } 

}
