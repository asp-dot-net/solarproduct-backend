﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Vouchers.Dtos
{
    public class GetAllVouchersInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
