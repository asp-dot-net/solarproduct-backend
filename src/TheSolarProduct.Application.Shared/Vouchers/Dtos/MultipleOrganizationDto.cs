﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.Vouchers.Dtos
{
    public class MultipleOrganizationDto
    {
        public int? Id { get; set; }
        public int? VoucherId { get; set; }
        public int OrgId { get; set; }
    }
}
