﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Vouchers.Dtos
{
    public class CreateOrEditVouchersDto : EntityDto<int?>
    {
        public virtual string VoucherName { get; set; }
        
        public virtual string FilePath { get; set; }

        public virtual string FileName { get; set; }

        public virtual bool? IsActive { get; set; }
        public string FileToken { get; set; }

        public List<MultipleOrganizationDto> MultipleOrganization { get; set; }
    }
}
