﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Vendors.Dtos;

namespace TheSolarProduct.Vouchers.Dtos
{
    public class GetVoucherForViewDto
    {
        public VoucherDto Vouchers { get; set; }
    }
}
