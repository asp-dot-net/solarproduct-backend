﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Common.Dto
{
    public class StockPaymentReciptDocumentReturnDto
    {
        public string FinalFilePath { get; set; }
        public string filename { get; set; }
        public string TenantName { get; set; }
        public int StockPaymentId { get; set; }
    }
}
