﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Common.Dto
{
    public class NamingValueDto
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}
