﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Common.Dto
{
    public class PurchaseOrderDoucmentReturnDto
    {
        public string FinalFilePath { get; set; }
        public string filename { get; set; }
        public string TenantName { get; set; }
        public int PurchaseOrderId{ get; set; }
    }
}
