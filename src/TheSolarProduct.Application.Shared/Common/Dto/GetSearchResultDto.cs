﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Common.Dto
{
    public class GetSearchResultDto
    {
        public int JobId { get; set; }

        public int LeadId { get; set; }

        public string JobNumber { get; set; }

        public string CustomerName { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public string Address { get; set; }

        public string SalesRep { get; set; }

        public string InstallerName { get; set; }

        public int? InstallerId { get; set; }

        public string State { get; set; }


    }
}
