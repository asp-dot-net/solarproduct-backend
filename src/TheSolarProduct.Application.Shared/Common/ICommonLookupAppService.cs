﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.CallHistory.Dtos;
using TheSolarProduct.Common.Dto;
using TheSolarProduct.Editions.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.Organizations.Dto;

namespace TheSolarProduct.Common
{
    public interface ICommonLookupAppService : IApplicationService
    {
        Task<ListResultDto<SubscribableEditionComboboxItemDto>> GetEditionsForCombobox(bool onlyFreeItems = false);

        Task<PagedResultDto<NameValueDto>> FindUsers(FindUsersInput input);

        GetDefaultEditionNameOutput GetDefaultEditionName();
        Task<List<OrganizationUnitDto>> GetOrganizationUnit();
        Task<List<JobJobTypeLookupTableDto>> GetAllJobTypeForTableDropdown();
        Task<List<LeadSourceLookupTableDto>> GetAllLeadSourceDropdown();
        Task<List<LeadStateLookupTableDto>> GetAllStateForTableDropdown();
        Task<List<JobStatusTableDto>> GetAllJobStatusTableDropdown();
        Task<List<JobPaymentOptionLookupTableDto>> GetAllPaymentOptionForTableDropdown();
        Task<List<JobFinanceOptionLookupTableDto>> GetAllFinanceOptionForTableDropdown();
        Task<List<LeadUsersLookupTableDto>> GetTeamForFilter();
        Task<List<LeadUsersLookupTableDto>> GetSalesRepForFilter(int OId, int? TeamId);
        Task<List<LeadSourceLookupTableDto>> GetAllLeadSourceForTableDropdown();
        Task<List<LeadStatusLookupTableDto>> GetAllLeadStatusForTableDropdown(string lead);
        Task<List<string>> GetOnlySuburb(string suburb);
        Task<List<LeadUsersLookupTableDto>> GetSalesManagerForFilter(int OId, int? TeamId);
        Task<List<CommonLookupDto>> GetServieCategoryDropdown();
        Task<List<CommonLookupDto>> GetServieSourceDropdown();
        Task<List<CommonLookupDto>> GetServieStatusDropdown();
        Task<List<CommonLookupDto>> GetPriceItemListDropdown();
        Task<List<CommonLookupDto>> GetServiePriorityDropdown();
        Task<List<LeadUsersLookupTableDto>> GetServiceUserForFilter(int OId);
        Task<List<LeadUsersLookupTableDto>> GetServiceReviewUserForFilter(int OId);
        Task<List<LeadUsersLookupTableDto>> GetServiceManagerUserForFilter(int OId);
        Task<List<JobVariationVariationLookupTableDto>> GetAllVariationForTableDropdown();
        Task<List<JobProductItemProductItemLookupTableDto>> GetAllProductItemForTableDropdown();
        Task<List<JobProductItemProductItemLookupTableDto>> GetAllProductItemForTableDropdownForEdit();
        Task<List<ProductItemProductTypeLookupTableDto>> GetAllProductTypeForTableDropdown();
        Task<List<CommonLookupDto>> GetReviewTypeDropdown();
        Task<List<CommonLookupDto>> getServieSubCategoryDropdown(string Name);
        DocumentReturnValuesDto SaveCommonDocument(byte[] ByteArray, string filename, string SectionName, int? JobId, int? DocumentTypeId, int? TenantId, int wholeSaleleadId = 0);
       

        Task<List<LeadSourceLookupTableDto>> GetAllLeadSourceForTableDropdownByOrg(int Id);

        Task<List<LeadSourceLookupTableDto>> GetAllLeadSourceForTableDropdownOnlyOrg(int Id);

        Task<List<CommonLookupDto>> GetTemplateTypeForTableDropdown();

        string DownloadQuote(string JobNumber, object QuoteString, string FileName);

        string DownloadPdf(string JobNumber, object String, string FileName);

        Task<List<CommonLookupDto>> GetAllEmailProviderDropdown();

        Task<List<OrganizationUnitMapDto>> GetOrganizationMapById(int organizationId);

        Task<List<CommonLookupDto>> GetAllUsersByRoleNameTableDropdown(string RoleName, int OrgId);

        Task<List<CommonLookupDto>> GetAllUsersByRoleNameContainsTableDropdown(string[] RoleName, int OrgId);

        Task<List<CommonLookupDto>> GetAllVariationForDropdown();

        Task<List<CommonLookupDto>> GetAllHouseTypeForDropdown();
        
        Task<List<CommonLookupDto>> GetAllRoofAngleForDropdown();
        
        Task<List<CommonLookupDto>> GetAllOtherChargeForDropdown();

        Task<List<CommonLookupDto>> GetAllRoofTypeForDropdown();

        Task<List<CommonLookupDto>> GetAllUsersForAppointment(int OrgId);

        Task<List<LocalityLookupDto>> GetAlLocalityForTableDropdown();

        Task<List<CommonLookupDto>> GetAllUsersTableDropdownForUserCallHistory(int OrgId);

        Task<List<CommonLookupDto>> GetWholesaleStatusDropdown();

        Task<List<string>> GetCurrentUserRoles();

        Task<List<CommonLookupDto>> GetVppConnection();

        Task<List<string>> GetAllUnitType(string unitType);

        Task<List<string>> GetAllStreetName(string streetName);

        Task<List<string>> GetAllStreetType(string streetType);

        Task<List<string>> GetAllSuburb(string suburb);

        DocumentReturnValuesDto SaveEcommerceDocument(byte[] ByteArray, string filename, string SectionName, int? TenantId);

       // DocumentReturnValuesDto SaveEcommerceImage(byte[] ByteArray, string filename, string SectionName, int? TenantId);

        Task<List<CommonLookupDto>> GetSpecialStatusDropdown();

        Task<List<CommonLookupDto>> GetEcommerceDocumentTypeDropdown();

        Task<List<CommonLookupDto>> GetEcommercePriceCategoryDropdown();

        Task<List<CommonLookupDto>> GetBrandingPartnersDropdown(int? categoryId);

        Task<List<CommonLookupDto>> GetSeriesDropdown(int? categoryId);

        Task<List<string>> GetAllInstallerInvoiceType();

        Task<List<CommonLookupDto>> GetTransportCompanyDropdown();

        Task<List<CommonLookupDto>> GetJobsDropdown(string searchTerm);

        Task<List<CommonLookupDto>> GetSTCProviderDropdown(int OrgId);

        Task<List<CommonLookupDto>> GetWarehouseLocationForFilter(string str);

        Task<List<CommonLookupDto>> GetEcommerceProductTypesDropdown();

        Task<List<CommonLookupDto>> GetSpecification(int? type);

        Task<List<CommonLookupDto>> GetWholesaleInvoicetypeDropdown();

        Task<List<CommonLookupDto>> GetWholesaleJobTypeDropdown();

        Task<List<CommonLookupDto>> GetWholesaleDelivaryOptionDropdown();

        Task<List<CommonLookupDto>> GetWholesaleJobStatusDropdown();

        Task<List<CommonLookupDto>> GetWholesaleTransportTypeDropdown();

        Task<List<CommonLookupDto>> GetWholesalePropertyTypeDropdown();

        Task<List<CommonLookupDto>> GetWholesalePVDStatusDropdown();

        Task<List<CommonLookupDto>> GetAllUsersByRoleNameNotContainsTableDropdown(string[] RoleName, int OrgId);

        Task<List<GetProductItemSearchResult>> GetEcommerceProductItemList(int productTypeId, string productItem, int? wholesaleId);

        Task<List<CommonLookupDto>> GetJobCostFixExpenseTypeDropdown();

        Task<List<CommonLookupDto>> GetAllProductItemForSearchable(string query);
        Task<List<JobVariationVariationLookupTableDto>> GetAllStockVariation();
        Task<List<CommonLookupDto>> GetAllVoucher();
    }
}