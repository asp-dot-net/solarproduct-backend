﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.PaymentStatuses.Dtos;

namespace TheSolarProduct.PaymentStatuses
{
    public interface IPaymentStatusAppService : IApplicationService
    {
        Task<PagedResultDto<GePaymentStatusForViewDto>> GetAll(GetAllPyamentStatusInput input);
        Task<GetPaymentStatusForEditOutput> GetPaymentStatusForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditPaymentStatusDto input);
        Task Delete(EntityDto input);
        Task<FileDto> GetPaymentStatusToExcel(GetAllPaymentStatusForExcelInput input);

    }
}
