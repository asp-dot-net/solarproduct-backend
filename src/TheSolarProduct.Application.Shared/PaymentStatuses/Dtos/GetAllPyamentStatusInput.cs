﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PaymentStatuses.Dtos
{
    public class GetAllPyamentStatusInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
