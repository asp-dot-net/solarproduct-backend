﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.PurchaseCompanys.Dtos;

namespace TheSolarProduct.PaymentStatuses.Dtos
{
    public class GePaymentStatusForViewDto
    {
        public PyamentStatusDto PaymentStatus { get; set; }
    }
}
