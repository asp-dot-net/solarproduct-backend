﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PaymentStatuses.Dtos
{
    public class GetAllPaymentStatusForExcelInput
    {
        public string Filter { get; set; }
    }
}
