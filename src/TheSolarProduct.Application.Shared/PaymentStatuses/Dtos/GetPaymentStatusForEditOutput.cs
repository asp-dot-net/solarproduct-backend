﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.PurchaseCompanys.Dtos;

namespace TheSolarProduct.PaymentStatuses.Dtos
{
    public class GetPaymentStatusForEditOutput
    {
        public CreateOrEditPaymentStatusDto PaymentStatus { get; set; }
    }
}
