﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PaymentStatuses.Dtos
{
    public class PyamentStatusDto : EntityDto
    {
        public string Name { get; set; }

        public bool? IsActive { get; set; }
    }
}
