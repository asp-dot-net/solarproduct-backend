﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.StockPayments.Dtos;

namespace TheSolarProduct.StockPayments
{
    public interface IStockPaymentAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllStockPaymentOutputDto>> GetAll(GetAllStockPaymentInput input);
        Task CreateOrEdit(CreateOrEditStockPaymentDto input);
        Task Delete(EntityDto input);
        Task<GetStockPaymentForEditOutput> GetStockPaymentForEdit(EntityDto input);
        Task<long> GetLastStockNo(int organizationId);
        Task<List<GetOrderNoSuggestionDto>> GetOrderNoSuggestions(string search, int orgid, int currencyId, int gstType);
        Task<GetStockPaymentForEditOutput> GetStockPaymentByStockNo(long stockNo);
        Task<FileDto> GetStockPaymentToExcel(GetStockPaymentForExcelInput input);
    }
}
