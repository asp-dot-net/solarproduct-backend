﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockPayments.Dtos
{
    public class GetOrderNoSuggestionDto
    {
        public string OrderNo { get; set; }
        public int PurchaseOrderId { get; set; }
    }
}
