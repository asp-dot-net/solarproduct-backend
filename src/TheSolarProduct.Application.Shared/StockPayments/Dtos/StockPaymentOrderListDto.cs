﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockPayments.Dtos
{
    public class StockPaymentOrderListDto : EntityDto<int?>
    {
        public int? StockPaymentId { get; set; }

        public int? PurchaseOrderId { get; set; }
        public long OrderNo { get; set; }   
        public decimal Amount { get; set; }
    }
}
