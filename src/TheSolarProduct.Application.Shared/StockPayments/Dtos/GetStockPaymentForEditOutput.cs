﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.StockOrder.Dtos;

namespace TheSolarProduct.StockPayments.Dtos
{
    public class GetStockPaymentForEditOutput
    {
        public CreateOrEditStockPaymentDto StockPayment { get; set; }
    }
}
