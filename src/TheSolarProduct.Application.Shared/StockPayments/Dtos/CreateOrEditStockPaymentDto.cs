﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.StockOrder.Dtos;

namespace TheSolarProduct.StockPayments.Dtos
{
    public class CreateOrEditStockPaymentDto : EntityDto<int?>
    {
        public int OrganizationId { get; set; }

        public long StockNo { get; set; }

        public int? PaymentMethodId { get; set; }

        public DateTime? PaymentDate { get; set; }

        public int? CurrencyId { get; set; }

        public int? GSTType { get; set; }

        public decimal? Rate { get; set; }

        public decimal? PaymentAmount { get; set; }

        public decimal? AUD { get; set; }

        public decimal? USD { get; set; }
        public string FileName { get; set; }

        public string FilePath { get; set; }

        public List<StockPaymentOrderListDto> StockPaymentOrderlist { get; set; }
    }
}
