﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockPayments.Dtos
{
    public class GetStockPaymentForExcelInput
    {
        public string FilterName { get; set; }
        public string Filter { get; set; }
        public int OrganizationId { get; set; }
        public int? GstTypeIdFilter { get; set; }
        public int PaymentMethodFilter { get; set; }

        public string Datefilter { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
