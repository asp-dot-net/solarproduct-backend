﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.StockPayments.Dtos
{
    public class GetAllStockPaymentOutputDto : EntityDto
    {
        public int OrganizationId { get; set; }

        public long StockNo { get; set; }

        public int? PaymentMethodId { get; set; }

        public String PaymentMethod { get; set; }

        public DateTime? PaymentDate { get; set; }

        public int? CurrencyId { get; set; }

        public String Currency { get; set; }

        public String GSTType { get; set; }
       

        public decimal? Rate { get; set; }

        public decimal? PaymentAmount { get; set; }


        public decimal? AUD { get; set; }

        public decimal? USD { get; set; }
    }
}
