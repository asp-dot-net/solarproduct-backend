﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.FreightCompanyes.Dtos;

namespace TheSolarProduct.FreightCompanyes
{
    public interface IFreightCompanyAppService : IApplicationService
    {
        Task<PagedResultDto<GetFreightCompanyForViewDto>> GetAll(GetAllFreightCompanyInput input);
        Task<GetFreightCompanyForEditOutput> GetFreightCompanyForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditFreightCompanyDto input);
        Task Delete(EntityDto input);
        Task<FileDto> GetFreightCompanyToExcel(GetAllFreightCompanyForExcelInput input);
    }
}
