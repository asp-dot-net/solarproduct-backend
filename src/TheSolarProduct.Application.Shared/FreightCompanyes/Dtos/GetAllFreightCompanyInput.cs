﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.FreightCompanyes.Dtos
{
    public class GetAllFreightCompanyInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
