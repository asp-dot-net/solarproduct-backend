﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.PaymentTypes.Dtos;

namespace TheSolarProduct.FreightCompanyes.Dtos
{
    public class GetFreightCompanyForEditOutput
    {
        public CreateOrEditFreightCompanyDto FreightCompany { get; set; }
    }
}
