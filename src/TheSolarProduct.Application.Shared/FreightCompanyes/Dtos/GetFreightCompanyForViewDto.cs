﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.PaymentTypes.Dtos;

namespace TheSolarProduct.FreightCompanyes.Dtos
{
    public class GetFreightCompanyForViewDto
    {
        public FreightCompanyDto FreightCompany { get; set; }
    }
}
