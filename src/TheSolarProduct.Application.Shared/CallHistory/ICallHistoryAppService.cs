﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.CallHistory.Dtos;

namespace TheSolarProduct.CallHistory
{
    public interface ICallHistoryAppService : IApplicationService
    {
        Task<List<GetCallHistoryViewDto>> GetCallHistory(GetCallHistoryInput input);

        Task<List<GetCallHistoryViewDto>> GetWholeSaleCallHistory(GetCallHistoryInput input);
    }
}
