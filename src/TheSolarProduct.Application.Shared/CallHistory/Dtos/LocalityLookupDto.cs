﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CallHistory.Dtos
{
    public class LocalityLookupDto
    {
        public string DisplayName { get; set; }
    }
}
