﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CallHistory.Dtos
{
    public class GetCallHistoryViewDto
    {
        public string CallType { get; set; }

        public string ColorClass { get; set; }

        public string CallTypeIcon { get; set; }

        public DateTime CallStartTimeUTC { get; set; }

        public string Duration { get; set; }

        public string Agent { get; set; }

        public string AgentName { get; set; }

        public string RecordingSource { get; set; }
    }
}
