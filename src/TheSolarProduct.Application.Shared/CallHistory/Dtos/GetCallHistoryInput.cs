﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CallHistory.Dtos
{
    public class GetCallHistoryInput
    {
        public int LeadId { get; set; }

        public string CallType { get; set; }

        public bool MyHistorty { get; set; }
    }
}
