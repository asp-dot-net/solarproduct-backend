﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Invoices.Dtos
{
    public class GetInvoicePaymentForEditOutput
    {
		public CreateOrEditInvoicePaymentDto InvoicePayment { get; set; }

		public string JobNote { get; set;}

		public string UserName { get; set;}

		public string UserName2 { get; set;}

		public string UserName3 { get; set;}
		public string JobNumber { get; set;}
		public string LeadCompanyName { get; set;}

		public string InvoicePaymentMethodPaymentMethod { get; set;}


    }
}