﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.Invoices.Dtos
{
    public class GetAllInvoicePaymentMethodsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string PaymentMethodFilter { get; set; }

		public string ShortCodeFilter { get; set; }

		public int ActiveFilter { get; set; }



    }
}