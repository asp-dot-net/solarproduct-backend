﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Invoices.Dtos
{
	public class CreateOrEditInvoicePaymentDto : EntityDto<int?>
	{
		public decimal? InvoicePayExGST { get; set; }

		public decimal? InvoicePayGST { get; set; }

		public decimal? InvoicePayTotal { get; set; }

		public DateTime? InvoicePayDate { get; set; }

		public decimal? CCSurcharge { get; set; }

		public DateTime? VerifiedOn { get; set; }

		[StringLength(InvoicePaymentConsts.MaxPaymentNoteLength, MinimumLength = InvoicePaymentConsts.MinPaymentNoteLength)]
		public string PaymentNote { get; set; }

		public bool Refund { get; set; }

		public int? RefundType { get; set; }

		public DateTime? RefundDate { get; set; }

		public int? PaymentNumber { get; set; }

		public bool IsVerified { get; set; }

		public string ReceiptNumber { get; set; }

		public string TransactionCode { get; set; }

		public DateTime? ActualPayDate { get; set; }

		public string PaidComment { get; set; }

		public int? JobId { get; set; }

		public long? UserId { get; set; }

		public long? VerifiedBy { get; set; }

		public long? RefundBy { get; set; }

		public int? InvoicePaymentMethodId { get; set; }

		public int? InvoicePaymentStatusId { get; set; }

		public  Boolean? InvoiceSmsSend { get; set; }

		public  Boolean? InvoiceEmailSend { get; set; }

		public  DateTime? InvoiceSmsSendDate { get; set; }

		public  DateTime? InvoiceEmailSendDate { get; set; }

        public int? SectionId { get; set; }

    }
}