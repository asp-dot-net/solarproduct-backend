﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.Invoices.Dtos
{
    public class GetAllInvoiceStatusesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }

    }
}