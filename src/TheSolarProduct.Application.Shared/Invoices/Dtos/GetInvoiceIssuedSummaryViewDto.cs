﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Invoices.Dtos
{
   public class GetInvoiceIssuedSummaryViewDto
    {
        public List<GetInvoiceIssuedForViewDto> IncoiceSummary { get; set; }
    }
}
