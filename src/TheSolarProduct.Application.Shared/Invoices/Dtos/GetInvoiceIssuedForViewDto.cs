﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Jobs.Dtos;

namespace TheSolarProduct.Invoices.Dtos
{
    public class GetInvoiceIssuedForViewDto
    {
		public JobDto Job { get; set; }
		
		public string PaymentMethod{ get; set; }

		public string CompanyName { get; set; }

		public string Email { get; set; }

		public string Phone { get; set; }

		public string Mobile { get; set; }

		public string Address { get; set; }

		public string JobNumber { get; set; }


		public string ProjectStatus { get; set; }
		public string JobStatusColorClass { get; set; }
		public string CurrentLeadOwaner { get; set; }
		public int? LeadId { get; set; }
		public string FinanceWith { get; set; }
		public int? InvoiceNumber { get; set; }
		public string Installer { get; set; }
		public string StateName { get; set; }
		public string ActivityComment { get; set; }
		public DateTime? ActivityCommentDate { get; set; }
		public string ActivityDescription { get; set; }
		public DateTime? ActivityReminderTime { get; set; }
		public decimal? TotalInvoicePay { get; set; }
		public decimal? TotalCost { get; set; }
		public int? TotalCost1 { get; set; }

		public decimal? Owning { get; set; }
		public decimal? NewOwning { get; set; }
		public DateTime? CreationDate { get; set; }
		public int? InvoiceID { get; set; }
		public string JobTypeName { get; set; }
		public int? TotalpaymentData { get; set; }
		public int? TotalpaymentVerifyData { get; set; }
		public int? TotalpaymentnotVerifyData { get; set; }
		public decimal? TotalAmountOfInvoice { get; set; }
		public decimal? TotalRefundamount { get; set; }
		public decimal? TotalAmountReceived { get; set; }
		public decimal? NewTotalAmountReceived { get; set; }
		public decimal? TotalCancelAmount { get; set; }
		public decimal? TotalVICAmountOfInvoice { get; set; }

		public decimal? TotalVICLoanOfInvoice { get; set; }
		public decimal? TotalCustAmountOfInvoice { get; set; }
		public decimal? TotalCustAmountReceived { get; set; }

		public decimal? TotalVICAmmointReceived { get; set; }

		public decimal? DepositRequired { get; set; }

		public string SalesRep { get; set; }

        public decimal? TotalSystemCapacity { get; set; }

    }
}
