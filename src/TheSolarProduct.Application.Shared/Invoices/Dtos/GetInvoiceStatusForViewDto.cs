﻿namespace TheSolarProduct.Invoices.Dtos
{
    public class GetInvoiceStatusForViewDto
    {
        public InvoiceStatusDto InvoiceStatus { get; set; }

    }
}