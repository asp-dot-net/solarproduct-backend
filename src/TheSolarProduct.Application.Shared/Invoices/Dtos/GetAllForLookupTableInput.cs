﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Invoices.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}