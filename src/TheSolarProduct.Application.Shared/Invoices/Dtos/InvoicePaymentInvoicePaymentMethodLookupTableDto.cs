﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Invoices.Dtos
{
    public class InvoicePaymentInvoicePaymentMethodLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}