﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;

namespace TheSolarProduct.Invoices.Dtos
{
	public class GetAllInvoicePaymentsInput : PagedAndSortedResultRequestDto
	{
		public string Filter { get; set; }

		public DateTime? MaxRefundDateFilter { get; set; }

		public DateTime? MinRefundDateFilter { get; set; }

		public int? MaxPaymentNumberFilter { get; set; }

		public int? MinPaymentNumberFilter { get; set; }

		public int IsVerifiedFilter { get; set; }

		public string ReceiptNumberFilter { get; set; }

		public DateTime? MaxActualPayDateFilter { get; set; }

		public DateTime? MinActualPayDateFilter { get; set; }

		public string PaidCommentFilter { get; set; }

		public string JobNoteFilter { get; set; }

		public string UserNameFilter { get; set; }

		public string UserName2Filter { get; set; }

		public string UserName3Filter { get; set; }

		public int? JobPaymentOption { get; set; }

		public int? OrganizationUnit { get; set; }

		public int? SalesManagerId { get; set; }

		public int? SalesRepId { get; set; }

		public string PostCodeSuburbFilter { get; set; }
		public string StateNameFilter { get; set; }
		public List<int> JobStatusID { get; set; }
		public int? InvoicePaymentMethodId { get; set; }
		public int? InvoicePaymentStatusId { get; set; }
		public int? FinanceOptionId { get; set; }
		public string InvoiceDateNameFilter { get; set; }

		public DateTime? StartDate { get; set; }

		public DateTime? EndDate { get; set; }
	}
}