﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.Invoices.Dtos
{
    public class InvoicePaymentMethodDto : EntityDto
    {
		public string PaymentMethod { get; set; }

		public string ShortCode { get; set; }

		public bool Active { get; set; }



    }
}