﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Invoices.Dtos
{
    public class CreateOrEditInvoiceStatusDto : EntityDto<int?>
    {

        [Required]
        public string Name { get; set; }
        public Boolean IsActive { get; set; }
        

    }
}