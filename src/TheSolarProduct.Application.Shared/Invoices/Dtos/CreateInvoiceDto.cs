﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Invoices.Dtos
{
    public class CreatePaidInvoiceDto
    {
        public int JobId { get; set; }

        public string JobNumber { get; set; }

        public DateTime? PaymentDate { get; set; }

        public int InvoicePaymentMethodId { get; set; }

        public string Description { get; set; }

        public decimal? AmountPaid { get; set; }

        public string PurchaseNumber { get; set; }

        public string ReceiptNumber { get; set; }
        
        public decimal? SSCharge { get; set; }

        public string InvoiceNotesDescription { get; set; }

        public int SectionId { get; set; }
    }
}
