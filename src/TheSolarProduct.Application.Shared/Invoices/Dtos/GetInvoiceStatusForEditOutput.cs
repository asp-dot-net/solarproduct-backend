﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Invoices.Dtos
{
    public class GetInvoiceStatusForEditOutput
    {
        public CreateOrEditInvoiceStatusDto InvoiceStatus { get; set; }

    }
}