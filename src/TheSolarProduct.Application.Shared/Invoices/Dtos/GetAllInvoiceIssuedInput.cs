﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Invoices.Dtos
{
   public class GetAllInvoiceIssuedInput : PagedAndSortedResultRequestDto
	{
		public string FilterName { get; set; }
		public string Filter { get; set; }
		public string JobNoteFilter { get; set; }
		public int? JobPaymentOption { get; set; }

		public int? OrganizationUnit { get; set; }

		public int? SalesManagerId { get; set; }

		public int? SalesRepId { get; set; }

		public string PostCodeSuburbFilter { get; set; }
		public string StateNameFilter { get; set; }
		public List<int> JobStatusID { get; set; }
		public int? PaymentMethodId { get; set; }
		public int? InvoicePaymentStatusId { get; set; }
		public int? FinanceOptionId { get; set; }
		public string InvoiceDateNameFilter { get; set; }

		public DateTime? StartDate { get; set; }

		public DateTime? EndDate { get; set; }
		public string PostalCodeFrom { get; set; }
		public string PostalCodeTo { get; set; }
		public string AreaNameFilter { get; set; }

		public int? Installerid { get; set; }
		public string Invoicenamefilter { get; set; }
		public int? excelorcsv { get; set; }
		public string vicrebate { get; set; }
		public int? solarRebateStatus { get; set; }
	}
}
