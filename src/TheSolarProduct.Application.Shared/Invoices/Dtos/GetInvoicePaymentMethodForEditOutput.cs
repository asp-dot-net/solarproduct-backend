﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Invoices.Dtos
{
    public class GetInvoicePaymentMethodForEditOutput
    {
		public CreateOrEditInvoicePaymentMethodDto InvoicePaymentMethod { get; set; }


    }
}