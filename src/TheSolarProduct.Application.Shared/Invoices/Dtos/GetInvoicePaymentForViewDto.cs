﻿namespace TheSolarProduct.Invoices.Dtos
{
	public class GetInvoicePaymentForViewDto
	{
		public InvoicePaymentDto InvoicePayment { get; set; }

		public string JobNote { get; set; }

		public string UserName { get; set; }

		public string UserName2 { get; set; }

		public string UserName3 { get; set; }

		public string InvoicePaymentMethodPaymentMethod { get; set; }

		public string CompanyName { get; set; }

		public string Email { get; set; }

		public string Phone { get; set; }

		public string Mobile { get; set; }

		public string Address { get; set; }

		public string JobNumber { get; set; }

		public string CurrentLeadOwaner { get; set; }
		public int? LeadId { get; set; }
		public int? TotalpaymentData { get; set; }
		public int? TotalpaymentVerifyData { get; set; }
		public int? TotalpaymentnotVerifyData { get; set; }
		public decimal? TotalAmountOfInvoice { get; set; }
		public decimal? TotalRefundamount { get; set; }
		public decimal? TotalAmountReceived { get; set; }
		public decimal? TotalCancelAmount { get; set; }
		public decimal? TotalVICAmountOfInvoice { get; set; }
		public decimal? TotalCustAmountOfInvoice { get; set; }
		public decimal? TotalCustAmountReceived { get; set; }
		

	}
}