﻿using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.Invoices.Dtos
{
    public class InvoiceStatusDto : EntityDto
    {
        public string Name { get; set; }
        public Boolean IsActive { get; set; }
    }
}