﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Quotations.Dtos;

namespace TheSolarProduct.Invoices.Dtos
{
    public class TexInvoiceDto
    {
        public string JobNumber { get; set; }

        public string Date { get; set; }

        public string Name { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string MeterPhase { get; set; }

        public string MeterUpgrad { get; set; }

        public string RoofType { get; set; }

        public string PropertyType { get; set; }

        public string RoofPitch { get; set; }

        public string ElecDist { get; set; }

        public string ElecRetailer { get; set; }

        public string NMINumber { get; set; }

        public string MeterNo { get; set; }

        public string BalanceDue { get; set; }

        public string TotalCost { get; set; }

        public string Stc { get; set; }

        public string SolarRebate { get; set; }

        public string SolarLoan { get; set; }

        public string NetCost { get; set; }

        public string Deposit { get; set; }

        public string ACharge { get; set; }

        public string Discount { get; set; }

        public List<QunityAndModelList> qunityAndModelLists { get; set; }

        public string ViewHtml { get; set; }

        public string InvoicePaymentPaid { get; set; }

        public string InvoicePaymentBalanceDue { get; set; }

        public string TCost { get; set; }
    }
}
