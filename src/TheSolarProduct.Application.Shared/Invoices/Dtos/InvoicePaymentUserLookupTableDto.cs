﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Invoices.Dtos
{
    public class InvoicePaymentUserLookupTableDto
    {
		public long Id { get; set; }

		public string DisplayName { get; set; }
    }
}