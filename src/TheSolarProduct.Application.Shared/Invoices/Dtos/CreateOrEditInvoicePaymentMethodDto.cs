﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Invoices.Dtos
{
    public class CreateOrEditInvoicePaymentMethodDto : EntityDto<int?>
    {

		public string PaymentMethod { get; set; }
		
		
		public string ShortCode { get; set; }
		
		
		public bool Active { get; set; }
		
		

    }
}