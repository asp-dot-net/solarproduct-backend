﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Invoices.Dtos
{
    public class PaymentReceiptDto
    {
        public string JobNumber { get; set; }

        public string Date { get; set; }

        public string Name { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string SiteAddressLine1 { get; set; }

        public string SiteAddressLine2 { get; set; }

        public string TotalCost { get; set; }

        public string LessStcRebate { get; set; }

        public string SolarVICRebate { get; set; }

        public string SolarVICLoan { get; set; }

        public string ACharge { get; set; }

        public string Discount { get; set; }

        public string NetCost { get; set; }

        public string BalanceDue { get; set; }

        public List<PaymentDetails> PaymentDetails { get; set; }

        public string ViewHtml { get; set; }

        public string InvoicePaymentPaid { get; set; }

        public string InvoicePaymentBalanceDue { get; set; }

        public List<PaymentDetails> InvoicePaymentPaymentDetails { get; set; }
    }

    public class PaymentDetails
    {
        public string Date { get; set; }

        public string Amount { get; set; }

        public string SSCharge { get; set; }

        public string Method { get; set; }
    }
}
