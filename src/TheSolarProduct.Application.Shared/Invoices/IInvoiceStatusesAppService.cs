﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Invoices
{
    public interface IInvoiceStatusesAppService : IApplicationService
    {
        Task<PagedResultDto<GetInvoiceStatusForViewDto>> GetAll(GetAllInvoiceStatusesInput input);

        Task<GetInvoiceStatusForViewDto> GetInvoiceStatusForView(int id);

        Task<GetInvoiceStatusForEditOutput> GetInvoiceStatusForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditInvoiceStatusDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetInvoiceStatusesToExcel(GetAllInvoiceStatusesForExcelInput input);

    }
}