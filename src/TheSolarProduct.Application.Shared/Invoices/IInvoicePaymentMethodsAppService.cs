﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.Invoices
{
    public interface IInvoicePaymentMethodsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetInvoicePaymentMethodForViewDto>> GetAll(GetAllInvoicePaymentMethodsInput input);

		Task<GetInvoicePaymentMethodForEditOutput> GetInvoicePaymentMethodForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditInvoicePaymentMethodDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetInvoicePaymentMethodsToExcel(GetAllInvoicePaymentMethodsForExcelInput input);

		
    }
}