﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Dto;
using System.Collections.Generic;
using TheSolarProduct.Jobs.Dtos;

namespace TheSolarProduct.Invoices
{
    public interface IInvoicePaymentsAppService : IApplicationService 
    {
		Task<List<GetInvoicePaymentForViewDto>> GetInvoicesByJobId(int jobid);

		Task<PagedResultDto<GetInvoicePaymentForViewDto>> GetAll(GetAllInvoicePaymentsInput input);

		Task<GetInvoicePaymentForEditOutput> GetInvoicePaymentForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditInvoicePaymentDto input);

		Task Delete(EntityDto input, int? sectionId);

		Task<FileDto> GetInvoicePaymentsToExcel(GetAllInvoicePaymentsForExcelInput input);

		Task VerifyInvoicePayment(CreateOrEditInvoicePaymentDto input);
		//Task<List<InvoicePaymentJobLookupTableDto>> GetAllJobForTableDropdown();
		
		Task<List<InvoicePaymentUserLookupTableDto>> GetAllUserForTableDropdown();
		
		Task<List<InvoicePaymentInvoicePaymentMethodLookupTableDto>> GetAllInvoicePaymentMethodForTableDropdown();

		Task<List<CommonLookupDto>> GetAllInvoiceStatusForTableDropdown();

		Task JobInvoiceSendSms(int id, int smstempid);

		Task JobInvoiceSendEmail(int id, int emailtempid);

		Task<PagedResultDto<GetInvoiceIssuedForViewDto>> GetAllInvoiceIssuedData(GetAllInvoiceIssuedInput input);
		Task<FileDto> GetAllInvoiceIssuedDataExcel(GetAllInvoiceIssuedInput input);
		Task<FileDto> GetAllInvoiceIssuedOwningDataExcel(GetAllInvoiceIssuedInput input);
		Task<FileDto> getAllInvoiceIssueddepositeDataExcel(GetAllInvoiceIssuedInput input);
		Task<List<ImportDataViewDto>> GetAllinvoiceiisuedpaymentView(int jobid);

		Task<Decimal> CustomerPaidForActualAmount(int jobid);

    }
}