﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.Client.Dto
{
    public class CreditApplicationInfo
    {
        public bool IsConfirm { get; set; }

        public bool IsApproved { get; set; }
    }
}
