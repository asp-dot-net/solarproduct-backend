﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.EcommerceProducctSpecification.Dtos;

namespace TheSolarProduct.ECommerces.Client.Dto
{
    public class GetProductDetailDto : EntityDto<int?>
    {
        public int? InvoiceTypeId { get; set; }
        public int? EcommerceProductItemId { get; set; }
        public string ProductName { get; set; }

        public string Model { get; set; }


        public decimal? Price { get; set; }


        public int? categoryId { get; set; }

        public int? QTY { get; set; }


        /* public int ProductItemId { get; set; }
 */

        public decimal Rating { get; set; }

        public string ProductShortDescription { get; set; }

        public string ProductDescription { get; set; }

        

        public string ImageUrl { get; set; }

       

        public string FileUrl { get; set; }

        public string Specification { get; set; }

        public List<GetProductDocumentDto> Documnets { get; set; }

        public List<string> ImageUrlList { get; set; }

        public string SpecialStatus { get; set; }

        public int EcomProdId { get; set; }

        public int? CartId { get; set; }   
        
        

        public DateTime? DeliveryDate { get; set; }

        public List<EcommerceProductItemSpecificationDto> EcommerceProductItemSpecification { get; set; }

        public int? WholesaleInvoiceId { get; set; }

       
    }
}
