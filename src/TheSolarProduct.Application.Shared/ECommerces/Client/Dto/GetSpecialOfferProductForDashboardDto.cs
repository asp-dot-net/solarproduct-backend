﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.Client.Dto
{
    public class GetSpecialOfferProductForDashboardDto : EntityDto
    {
        public string ProductName { get; set; }

        public decimal Rating { get; set; }

        public string ImageUrl { get; set; }

        public string SpecialStatus { get; set; }

        public decimal? NewPrice { get; set; }

        public decimal? OldPrice { get; set;}

        public DateTime? DealEnd { get; set; }
    }
}
