﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.Client.Dto
{
    public class UserInfoClientOutputDto : Entity<int>
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public int? PriceCategoryId { get; set; }

        public string SuccessMsg { get; set; }

        public int? CartItemCount { get; set; }
    }
}
