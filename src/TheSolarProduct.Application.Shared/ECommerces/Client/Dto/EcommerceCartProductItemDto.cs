﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.ECommerces.Client.Dto
{
    public class EcommerceCartProductItemDto : EntityDto<int?>
    {
        public int TenantId { get; set; }

        public int? UserId { get; set; }

        public int? EcommerceProductItemId { get; set; }

        public int? QTY { get; set; }

        public bool Purcahse { get; set; }

        public decimal? Price { get; set; }

        public string Title { get; set; }

        public string ImageUrl { get; set; }

        public int? ProductItemId { get; set; }

        public int? categoryId { get; set; }

        public string ProductName { get; set; }

        public string Model { get; set; }

       /* public int? CartId { get; set; }*/

    }
}
