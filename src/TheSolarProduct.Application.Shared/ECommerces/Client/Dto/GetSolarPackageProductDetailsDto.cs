﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.Client.Dto
{
    public class GetSolarPackageProductDetailsDto : EntityDto
    {
        public int? ProductItemId { get; set; }

        public string ProductType { get; set; }

        public string ProductName { get; set; }

        public string Model { get; set; }

        public int Quantity { get; set; }

        public string Image { get; set; }

    }
}
