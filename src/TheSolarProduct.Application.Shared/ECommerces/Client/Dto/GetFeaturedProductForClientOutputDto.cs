﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.Client.Dto
{
    public class GetFeaturedProductForClientOutputDto : EntityDto
    {
        public string ProductName { get; set; }

        public decimal Price { get; set; }

        public decimal Rating { get; set; }

        public string ImageUrl { get; set; }

        public string SpecialStatus { get; set; }

        public int EcomProdId { get; set; }
    }
}
