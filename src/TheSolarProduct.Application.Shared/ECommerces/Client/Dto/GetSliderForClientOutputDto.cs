﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.Client.Dto
{
    public class GetSliderForClientOutputDto
    {
        public string Name { get; set; }

        public string Image { get; set; }

        public string Header { get; set; }

        public string SubHeader { get; set; }

        public string Button { get; set; }

        public string ImageUrl { get; set; }
    }
}
