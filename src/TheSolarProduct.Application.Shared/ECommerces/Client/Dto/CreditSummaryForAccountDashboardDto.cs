﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.Client.Dto
{
    public class CreditSummaryForAccountDashboardDto : EntityDto
    {
        public decimal? CreditLimit { get; set; }

        public decimal? PaymentTerrm { get; set; }

        public decimal? CreditUsed { get; set; }

        public decimal? AvailableCredit { get; set; }

        public bool IsApproved { get; set; }

    }
}
