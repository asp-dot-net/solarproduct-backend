﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.Client.Dto
{
    public class GetProductTypeForDashboardClientOutputDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int ProductItemCount { get; set; }

        public string ImageUrl { get; set; }
    }
}
