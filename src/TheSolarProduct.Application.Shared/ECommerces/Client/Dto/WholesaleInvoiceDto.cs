﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.EcommerceProducctSpecification.Dtos;
using TheSolarProduct.InstallationCost.InstallationItemList.Dtos;

namespace TheSolarProduct.ECommerces.Client.Dto
{
    public class WholesaleInvoiceDto : EntityDto<int?>
    {
        public int TenantId { get; set; }

        public int? InvoiceTypeId { get; set; }

        public int? WholesaleInvoiceId { get; set; }


        public int ProductTypeId { get; set; }

        public string InvoiceType { get; set; }

        public int? categoryId { get; set; }

        public string CustomerName { get; set; }

        public int? EcommerceProductItemId { get; set; }

        public string Model { get; set; }

       
        public string InvoiceNo { get; set; }

        public int? JobTypeId { get; set; }

        public int? WholesaleLeadId { get; set; }

        public decimal? CreditAmount { get; set; }

        public string Reference { get; set; }

        public string DeliveryOptions { get; set; }

        public string Name { get; set; }

        public int? JobStatusId { get; set; }

        public int? StoreLoactionId { get; set; }

        public decimal? InvoiceAmount { get; set; }

        public decimal? InvoiceAmountWithGST { get; set; }

        public DateTime? DeliveryDate { get; set; }

       public DateTime? InvoiceDate { get; set; }

        public string ETPDeliveryDate { get; set; }

        public string DeliveryTime { get; set; }

        public int? TansportTypeId { get; set; }

        public string Consign { get; set; }

        public decimal? ActualFreigthCharges { get; set; }

        public decimal? CustFreigthCharges { get; set; }

        public int? PropertyTypeId { get; set; }

        public int? PVDStatusId { get; set; }

        public string STCID { get; set; }

        public int? NoOfSTC { get; set; }

        public decimal? STCRate { get; set; }

        public int? QTY { get; set; }

        public decimal? Price { get; set; }
        public string PVDNumber { get; set; }
        public List<EcommerceCartProductItemDto> ProductitemDetailDto { get; set; }

        public List<GetProductDetailDto> ProductDetailDto { get; set; }

        public long OrganizationUnit { get; set; }
       

    }
}
