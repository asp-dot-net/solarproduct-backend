﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.Client.Dto
{
    public class GetBrandingPartnersForDashboardClientOutputDto : EntityDto
    {
        public string Logo { get; set; }

        public string Name { get; set; }
    }
}
