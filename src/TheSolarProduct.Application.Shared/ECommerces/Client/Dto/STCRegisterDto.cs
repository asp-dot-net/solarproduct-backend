﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.ECommerces.Client.Dto
{
    public class STCRegisterDto : EntityDto<int?>
    {
        public int TenantId { get; set; }

        public string TradingName { get; set; }

        public string ABNNumber { get; set; }

        public string UnitNo { get; set; }

        public string UnitType { get; set; }

        public string StreetNo { get; set; }

        public string StreetName { get; set; }

        public string StreetType { get; set; }

        public int? StateId { get; set; }
        
        public string State { get; set; }

        public string PostCode { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string PhoneNo { get; set; }

        public string EmailForRemittances { get; set; }

        public string PrimaryEmail { get; set; }

        public string InstallerFullName { get; set; }

        public string Installer_DesignerEmail { get; set; }

        public string Installer_LicenceNumber { get; set; }

        public string InstallerUnitNo { get; set; }

        public string InstallerUnitType { get; set; }

        public string InstallerStreetNo { get; set; }

        public string InstallerStreetName { get; set; }

        public string InstallerStreetType { get; set; }

        public int? InstallerStateId { get; set; }

        public string InstallerState { get; set; }

        public string InstallerPostCode { get; set; }

        public string InstallerLatitude { get; set; }

        public string InstallerLongitude { get; set; }

        public string InstallerPhone { get; set; }

        public string InstallerFullName2 { get; set; }

        public string Installer_DesignerEmail2 { get; set; }

        public string Installer_LicenceNumber2 { get; set; }

        public string InstallerUnitNo2 { get; set; }

        public string InstallerUnitType2 { get; set; }

        public string InstallerStreetNo2 { get; set; }

        public string InstallerStreetName2 { get; set; }

        public string InstallerStreetType2 { get; set; }

        public int? InstallerStateId2 { get; set; }

        public string InstallerState2 { get; set; }

        public string InstallerPostCode2 { get; set; }

        public string InstallerLatitude2 { get; set; }

        public string InstallerLongitude2 { get; set; }

        public string InstallerPhone2 { get; set; }

        public string Claim { get; set; }

        public string PublicLiabilityCertificate { get; set; }

        public string ProductLiabilityCertificate { get; set; }

        public string PhotoId { get; set; }

        public string ASICCertificate { get; set; }

        public string AccountName { get; set; }

        public string BSB { get; set; }

        public string AccountNumber { get; set; }

        public string FindUs { get; set; }

        public string FindUsSpecify { get; set; }
    }
}
