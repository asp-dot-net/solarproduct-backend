﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.Client.Dto
{
    public class EcommerceSubscriberDto : EntityDto<int?>
    {
        public int TenantId { get; set; }

        public string SubscriberEmail { get; set; }

    }
}
