﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.Client.Dto
{
    public class AauthenticationModelDto
    {
        public int TenantId { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }
    }
}
