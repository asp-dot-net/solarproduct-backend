﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.Client.Dto
{
    public class GetProductDocumentDto : EntityDto
    {
        public int? DocTypeId { get; set; }

        public string DocType { get; set; }

        public string DocName { get; set; }

        public string DocUrl { get; set; }
    }
}
