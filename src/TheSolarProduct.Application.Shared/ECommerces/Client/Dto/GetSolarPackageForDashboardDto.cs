﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.Client.Dto
{
    public class GetSolarPackageForDashboardDto : EntityDto
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public string Banner { get; set; }

    }
}
