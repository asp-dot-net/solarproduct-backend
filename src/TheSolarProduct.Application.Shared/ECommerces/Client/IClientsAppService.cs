﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.EcommerceProducctSpecification.Dtos;
using TheSolarProduct.ECommerces.Businesses.Dtos;
using TheSolarProduct.ECommerces.Client.Dto;
using TheSolarProduct.ECommerces.EcommerceUserRegisterRequests.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.Locations.Dtos;

namespace TheSolarProduct.ECommerces.Client
{
    public interface IClientsAppService : IApplicationService
    {
        Task<List<GetSliderForClientOutputDto>> GetSlider(int TenantId);

        Task<List<GetProductTypeForDashboardClientOutputDto>> GetProductTypeForDashboard(int TenantId);

        Task<List<GetBrandingPartnersForDashboardClientOutputDto>> GetBrandingPartnersForDashboard(int TenantId, int? productCategoryId);

        Task<List<GetFeaturedProductForClientOutputDto>> GetFeaturedProduct(int TenantId, int? PriceCategoryId);

        Task<List<CommonLookupDto>> GetAllSeries(int TenantId, int categoryId, List<int?> Brand);

        Task<List<decimal?>> GetAllSizes(int TenantId, int categoryId, List<int?> Brand);

        Task<GetProductDetailDto> GetProductDetail(int TenantId, int ProductItemId, int? PriceCategoryId);

        Task<List<GetSpecialOfferProductForDashboardDto>> GetSpecialOfferProductForDashboard(int TenantId);

        Task<string> CreateUserRequest(EcommerceUserRegisterRequestCreateDto input);

        Task<List<int?>> CheckUserExist(int TenantId, string userName, string Email);

        Task<List<GetFeaturedProductForClientOutputDto>> GetAllProduct(int TenantId, int? productCategoryId, List<int?> Brand, List<int?> Series, List<decimal?> Size, string filter, int? PriceCategoryId, List<int?> SpecialStatus, List<int?> Types);

        Task<List<GetSolarPackageForDashboardDto>> GetSolarPackageForDashboard(int TenantId);

        Task<List<GetSolarPackageProductDetailsDto>> GetSolarPackageProductDetails(int TenantId, int PackageId);

        Task<int> AddToCreditApplication(BusinessDto input);

        Task<UserInfoClientOutputDto> Authentication(AauthenticationModelDto input);

        Task<BusinessDto> GetCreditApplicationForEdit(int TenantId, int LoginId);

        Task<List<LeadStateLookupTableDto>> GetAllBuisnessStructureDropdown(int TenantId);

        Task<List<LeadStateLookupTableDto>> GetAllBuisnessDocumentTypeDropdown(int TenantId);

        Task<string> AddSubscriber(EcommerceSubscriberDto input);

        Task<string> ContactUs(CuntactUsDto input);

        Task<CreditApplicationInfo> GetUserCreditApplicationInfo(int tenantId, int loginId);

        Task<List<GetBrandingPartnersForDashboardClientOutputDto>> GetAllCategories(int TenantId, int categoryId);

        Task<CreditSummaryForAccountDashboardDto> GetCreditSummaryForAccountDashboard(int TenantId, int LoginId);

        Task<List<CommonLookupDto>> GetAllSpecialStatus(int TenantId);

        Task<string> AddSTCRegister(STCRegisterDto input);

        Task<List<CommonLookupDto>> GetAllTypes(int TenantId, int categoryId, List<int?> Brand);

        Task<int> AddedToEcommerceCart(EcommerceCartProductItemDto input);

        Task<List<GetProductDetailDto>> GetAllEcommerceCartProductItem(int TenantId,int UserId, int PriceCategoryId);

        Task<string> DeleteToEcommerceCart(int TenantId, int Id);

        Task<string> AddWholesaleInvoice(WholesaleInvoiceDto input);

        Task<List<LocationDto>> GetAllWareHouseLocation(int TenantId);

        Task<decimal?> GetSTCDealPrice(int TenantId);


    }
}