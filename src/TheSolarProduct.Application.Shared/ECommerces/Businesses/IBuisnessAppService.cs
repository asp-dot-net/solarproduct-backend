﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ECommerces.Businesses.Dtos;

namespace TheSolarProduct.ECommerces.Businesses
{
    public interface IBuisnessAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllBusinessDto>> GetAll(GetAllBusinessInput input);

        Task<BusinessDto> GetBusinessForView(int id,string type);

        Task<BusinessDto> GetBusinessForEdit(EntityDto input);

        Task CreateOrEdit(BusinessDto input);

        Task Delete(EntityDto input);
    }
}
