﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.Businesses.Dtos
{
    public class GetAllBusinessInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        public int? BuisnessStructureId { get; set; }
        public string IsApproved { get; set; }

    }
}
