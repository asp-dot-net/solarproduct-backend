﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.Businesses.Dtos
{
    public class GetAllBusinessDto
    {
        public BusinessDto Business { get; set; }
    }
}
