﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceUserRegisterRequests.Dto
{
    public class GetEcommerceUserRegisterRequestInputDto : PagedAndSortedResultRequestDto
    {

        public string Filter { get; set; }

        public string IsApproved { get; set; }
    }
}
