﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceUserRegisterRequests.Dto
{
    public class GetECommerceUserRegisterRequestViewDto
    {
        public EcommerceUserRegisterRequestDto ecommerceUserRegisterRequest { get; set; }
    }
}
