﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ECommerces.ECommerceSliders.Dtos;
using TheSolarProduct.ECommerces.EcommerceUserRegisterRequests.Dto;

namespace TheSolarProduct.ECommerces.EcommerceUserRegisterRequests
{
    public interface IECommerceUserRegisterRequestAppService : IApplicationService
    {
        Task<PagedResultDto<GetECommerceUserRegisterRequestViewDto>> GetAll(GetEcommerceUserRegisterRequestInputDto input);

        Task<GetECommerceUserRegisterRequestViewDto> GetEcommerceUserRegisterRequestForView(int id);

        Task Delete(EntityDto input);

        Task CreateOrEdit(EcommerceUserRegisterRequestDto input);

        Task<string> ApproveUserRequest(int requestId, int OrganizationUnit);
    }
}
