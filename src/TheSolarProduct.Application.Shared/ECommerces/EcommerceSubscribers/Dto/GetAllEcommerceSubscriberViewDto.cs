﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceSubscribers.Dto
{
    public class GetAllEcommerceSubscriberViewDto : EntityDto
    {
        public string SubscriberEmail { get; set; }

        public bool IsSelected { get; set; }
    }
}
