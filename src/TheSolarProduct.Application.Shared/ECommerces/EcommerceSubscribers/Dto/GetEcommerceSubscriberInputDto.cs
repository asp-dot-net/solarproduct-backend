﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceSubscribers.Dto
{
    public class GetEcommerceSubscriberInputDto : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
