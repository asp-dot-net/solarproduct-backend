﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceSubscribers.Dto
{
    public class GetEcommerceSubscribeEmailDto
    {
        public string Body { get; set; }

        public string Subject { get; set; }

        public string EmailFrom { get; set; }

        public List<string> EmailList { get; set; }
    }
}
