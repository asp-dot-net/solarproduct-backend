﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ECommerces.EcommerceSolarPackages.Dto;
using TheSolarProduct.ECommerces.EcommerceSubscribers.Dto;

namespace TheSolarProduct.ECommerces.EcommerceSubscribers
{
    public interface IEcommerceSubscriberAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllEcommerceSubscriberViewDto>> GetAll(GetEcommerceSubscriberInputDto input);

        Task SendEmailBluk(GetEcommerceSubscribeEmailDto input);

    }
}
