﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceDataVaultActivity.Dtos
{
    public class GetEcommerceActivityLogInputDto : PagedAndSortedResultRequestDto
    {
        public int? SectionId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

    }
}
