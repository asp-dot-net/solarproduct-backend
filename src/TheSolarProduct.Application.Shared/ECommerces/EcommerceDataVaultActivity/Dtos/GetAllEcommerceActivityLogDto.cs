﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceDataVaultActivity.Dtos
{
    public class GetAllEcommerceActivityLogDto : EntityDto
    {
        public string SectionName { get; set; }

        public string UserName { get; set; }

        public DateTime? LastModificationTime { get; set; }

        public string ActionName { get; set; }

        public string ActionNote { get; set; }

        public int SectionId { get; set; }

    }
}
