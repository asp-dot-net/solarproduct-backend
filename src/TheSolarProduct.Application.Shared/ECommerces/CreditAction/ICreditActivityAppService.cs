﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ECommerces.CreditAction.Dto;
using TheSolarProduct.WholeSaleActivitys.Dtos;

namespace TheSolarProduct.ECommerces.CreditAction
{
    public interface ICreditActivityAppService : IApplicationService
    {
        Task SendSms(CreditSmsEmailDto input);

        Task SendEmail(CreditSmsEmailDto input);
    }
}
