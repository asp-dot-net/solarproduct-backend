﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.STCRegisters.Dto
{
    public class GetStcRegisterInputDto : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
