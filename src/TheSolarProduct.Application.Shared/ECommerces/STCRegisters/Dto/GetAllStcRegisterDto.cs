﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.ECommerces.Client.Dto;

namespace TheSolarProduct.ECommerces.STCRegisters.Dto
{
    public class GetAllStcRegisterDto
    {
        public STCRegisterDto RegisterDto { get; set; }
    }
}
