﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ECommerces.STCRegisters.Dto;

namespace TheSolarProduct.ECommerces.STCRegisters
{
    public interface IStcRegisterAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllStcRegisterDto>> GetAll(GetStcRegisterInputDto input);
    }
}
