﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceProductTypes.Dto
{
    public class EcommerceProductTypeDto : EntityDto
    {
        public string Type { get; set; }
    }
}
