﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceProductTypes.Dto
{
    public class GetAllEcommerceProductTypeDto
    {
        public EcommerceProductTypeDto EcommerceProductType { get; set; }

    }
}
