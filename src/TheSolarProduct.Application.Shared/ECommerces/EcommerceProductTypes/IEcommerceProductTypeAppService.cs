﻿using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System.Threading.Tasks;
using TheSolarProduct.ECommerces.EcommerceProductTypes.Dto;

namespace TheSolarProduct.ECommerces.EcommerceProductTypes
{
    public interface IEcommerceProductTypeAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllEcommerceProductTypeDto>> GetAll(GetEcommerceProductTypeInputDto input);

        Task<EcommerceProductTypeDto> GetLeadSourceForEdit(EntityDto input);

        Task CreateOrEdit(EcommerceProductTypeDto input);

        Task Delete(EntityDto input);
    }
}
