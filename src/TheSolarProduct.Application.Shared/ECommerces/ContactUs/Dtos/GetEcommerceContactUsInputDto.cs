﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.ContactUs.Dtos
{
    public class GetEcommerceContactUsInputDto : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
