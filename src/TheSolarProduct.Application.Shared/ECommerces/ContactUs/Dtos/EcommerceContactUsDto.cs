﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.ContactUs.Dtos
{
    public  class EcommerceContactUsDto : EntityDto<int?>
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Subject { get; set; }

        public string Message { get; set; }

        public DateTime CreateDate { get; set; }

        public int? WholesaleLeadId { get; set; }
    }
}
