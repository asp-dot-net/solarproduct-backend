﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.ContactUs.Dtos
{
    public class GetAllEcommerceContactUsDto
    {
        public EcommerceContactUsDto ecommerceContactUs { get; set; }
    }
}
