﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ECommerces.BrandingPartners.Dto;
using TheSolarProduct.ECommerces.ContactUs.Dtos;
using TheSolarProduct.WholeSaleActivitys.Dtos;
using TheSolarProduct.WholeSaleLeads.Dtos;

namespace TheSolarProduct.ECommerces.ContactUs
{
    public interface IEcommerceContactUsAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllEcommerceContactUsDto>> GetAll(GetEcommerceContactUsInputDto input);

        Task Edit(EcommerceContactUsDto input);

        Task Delete(EntityDto input);

        Task<EcommerceContactUsDto> GetEcommerceContactUsForEdit(EntityDto input);

        Task SendEmail(WholeSaleSmsEmailDto input);

        Task SendSMS(WholeSaleSmsEmailDto input);

        Task<string> AddtoLead(int contactId, int OrganizationUnit);

    }
}
