﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using TheSolarProduct.ECommerces.BrandingPartners.Dto;

namespace TheSolarProduct.ECommerces.BrandingPartners
{
    public interface IBrandingPartnerAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllBrandingPartnerDto>> GetAll(GetBrandingPartnerinputDto input);

        Task<BrandingPartnerDto> GetBusinessForEdit(EntityDto input);

        Task CreateOrEdit(BrandingPartnerDto input);

        Task Delete(EntityDto input);
    }
}
