﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.BrandingPartners.Dto
{
    public  class GetAllBrandingPartnerDto
    {
        public BrandingPartnerDto BrandingPartner { get; set; }
    }
}
