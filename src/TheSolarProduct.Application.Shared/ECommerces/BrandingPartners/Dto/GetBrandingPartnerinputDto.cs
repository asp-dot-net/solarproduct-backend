﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.BrandingPartners.Dto
{
    public class GetBrandingPartnerinputDto : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
