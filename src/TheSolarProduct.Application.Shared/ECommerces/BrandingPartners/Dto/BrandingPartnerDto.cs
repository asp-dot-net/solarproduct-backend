﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.BrandingPartners.Dto
{
    public class BrandingPartnerDto : EntityDto<int?>
    {
        public int? ProductCategoryId { get; set; }

        public string BrandName { get; set; }

        public string Logo { get; set; }

        public string ProductCategory { get; set; }

        public string FileToken { get; set; }
    }
}
