﻿using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ECommerces.EcommerceSolarPackages.Dto;
using TheSolarProduct.Jobs.Dtos;

namespace TheSolarEcommerceSolar.ECommerces.EcommerceSolarPackages
{
    public interface IEcommerceSolarPackagesAppService : IApplicationService
    {
        Task<PagedResultDto<GetEcommerceSolarPackageForViewDto>> GetAll(GetAllEcommerceSolarPackageForInput input);

        Task<GetEcommerceSolarPackageForEditOutput> GetEcommerceSolarPackageForView(int id);

        Task<GetEcommerceSolarPackageForEditOutput> GetEcommerceSolarPackageForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditEcommerceSolarPackageDto input);

        Task Delete(EntityDto input);

        Task<List<NameValueDto>> GetEcommerceSolarPackageForDropdown();

        Task<List<CreateOrEditEcommerceSolarPackagesItemDto>> GetEcommerceSolarPackageItemByPackageId(int EcommerceSolarPackageId);

        //Task<List<GetAllPackageDetailsDto>> GetAllPackageDetails();

        Task<List<GetProductItemSearchResult>> GetPicklistProductItemList(int productTypeId, string productItem);
    }
}
