﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceSolarPackages.Dto
{
    public class EcommerceSolarPackageDto : EntityDto
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public bool IsActive { get; set; }
    }
}
