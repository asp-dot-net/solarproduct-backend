﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.ProductPackages.Dtos;

namespace TheSolarProduct.ECommerces.EcommerceSolarPackages.Dto
{
    public class CreateOrEditEcommerceSolarPackageDto : EntityDto<int?>
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public bool IsActive { get; set; }

        public string Banner { get; set; }

        public string FileToken { get; set; }

        public List<CreateOrEditEcommerceSolarPackagesItemDto> EcommerceSolarPackageItem { get; set; }

    }
}
