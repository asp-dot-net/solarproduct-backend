﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceSolarPackages.Dto
{
    public class CreateOrEditEcommerceSolarPackagesItemDto : EntityDto<int?>
    {
        public int EcommerceSolarPackageId { get; set; }

        public int ProductItemId { get; set; }

        public int Quantity { get; set; }

        public int ProductTypeId { get; set; }

        public string Model { get; set; }

        public decimal? Size { get; set; }

        public string ProductItem { get; set; }

        public string DatasheetUrl { get; set; }

        public string ProductType { get; set; }

    }
}
