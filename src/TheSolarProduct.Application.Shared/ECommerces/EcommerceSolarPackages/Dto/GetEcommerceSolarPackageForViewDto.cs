﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.ProductPackages.Dtos;

namespace TheSolarProduct.ECommerces.EcommerceSolarPackages.Dto
{
    public class GetEcommerceSolarPackageForViewDto
    {
        public EcommerceSolarPackageDto EcommerceSolarPackage { get; set; }
    }
}
