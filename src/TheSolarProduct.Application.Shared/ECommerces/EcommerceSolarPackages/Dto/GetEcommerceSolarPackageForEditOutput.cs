﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.ProductPackages.Dtos;

namespace TheSolarProduct.ECommerces.EcommerceSolarPackages.Dto
{
    public class GetEcommerceSolarPackageForEditOutput
    {
        public CreateOrEditEcommerceSolarPackageDto EcommerceSolarPackage { get; set; }
    }
}
