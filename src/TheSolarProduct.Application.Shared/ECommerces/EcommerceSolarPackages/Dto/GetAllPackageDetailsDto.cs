﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceSolarPackages.Dto
{
    public class GetAllPackageDetailsDto : EntityDto
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public List<EcommerceSolarPackageItems> PackageItems { get; set; }
    }

    public class EcommerceSolarPackageItems
    {
        public string ProductType { get; set; }

        public string ProductItem { get; set; }

        public string Model { get; set; }

        public int Quantity { get; set; }
    }
}
