﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceSolarPackages.Dto
{
    public class GetAllEcommerceSolarPackageForInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
