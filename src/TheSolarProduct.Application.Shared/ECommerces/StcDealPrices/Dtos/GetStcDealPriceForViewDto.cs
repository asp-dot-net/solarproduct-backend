﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Currencies.Dtos;

namespace TheSolarProduct.ECommerces.StcDealPrices.Dtos
{
    public class GetStcDealPriceForViewDto
    {
        public StcDealPriceDto StcDealPrice { get; set; }
    }
}
