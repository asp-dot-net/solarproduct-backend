﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Currencies.Dtos;

namespace TheSolarProduct.ECommerces.StcDealPrices.Dtos
{
    public class GetStcDealPriceForEditOutput
    {
        public CreateOrEditStcDealPriceDto StcDealPrice { get; set; }
    }
}
