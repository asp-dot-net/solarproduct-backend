﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.ECommerces.StcDealPrices.Dtos
{
    public class CreateOrEditStcDealPriceDto : EntityDto<int?>
    {
        public string Value { get; set; }
    }
}
