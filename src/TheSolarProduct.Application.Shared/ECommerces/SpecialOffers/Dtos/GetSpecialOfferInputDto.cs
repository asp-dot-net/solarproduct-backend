﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.SpecialOffers.Dtos
{
    public class GetSpecialOfferInputDto : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
