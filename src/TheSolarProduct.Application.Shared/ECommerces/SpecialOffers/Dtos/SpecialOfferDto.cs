﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.SpecialOffers.Dtos
{
    public class SpecialOfferDto : EntityDto<int?>
    {
        public string OfferTitle { get; set; }

        public DateTime? DealStarts { get; set; }

        public DateTime? DealEnds { get; set; }

        public bool IsOfferActive { get; set; }

        public DateTime? OfferCreatedDate { get; set; }

        public string OfferCreatedBy { get; set; }

        public int? ProductCount { get; set; }  

        public List<SpecialOfferProductDto> specialOfferProducts { get; set; }
    }
}
