﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.SpecialOffers.Dtos
{
    public class GetAllSpecialOfferDto
    {
        public SpecialOfferDto SpecialOffer { get; set; }
    }
}
