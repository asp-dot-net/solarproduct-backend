﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.SpecialOffers.Dtos
{
    public class SpecialOfferProductDto : EntityDto<int?>
    {
        public int? SpecialOfferId { get; set; }

        public int? ProductItemId { get; set; }

        public string ProductName { get; set; }

        public decimal? OldPrice { get; set; }

        public decimal? NewPrice { get; set; }

        public string Brand { get; set; }

        public string ProductCategory { get; set; }
    }
}
