﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.SpecialOffers.Dtos
{
    public class GetEcommerceProductsForDropdown :Entity
    {
        public string ProductName { get; set; }

        public decimal? Price { get; set; } 

        public string Brand { get; set; }

        public string ProductCategory { get; set; } 
    }
}
