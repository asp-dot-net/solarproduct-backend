﻿using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ECommerces.SpecialOffers.Dtos;

namespace TheSolarProduct.ECommerces.SpecialOffers
{
    public interface ISpecialOfferAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllSpecialOfferDto>> GetAll(GetSpecialOfferInputDto input);

        Task<SpecialOfferDto> GetSpecialOfferForEdit(EntityDto input);

        Task CreateOrEdit(SpecialOfferDto input);

        Task Delete(EntityDto input);

        Task<List<GetEcommerceProductsForDropdown>> GetEcommerceProductsForDropdown(string Query);
    }
}
