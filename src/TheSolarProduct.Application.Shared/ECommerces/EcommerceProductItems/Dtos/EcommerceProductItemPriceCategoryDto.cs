﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceProductItems.Dtos
{
    public class EcommerceProductItemPriceCategoryDto : EntityDto<int?>
    {
        public int? EcommerceProductItemId { get; set; }

        public int? ECommercePriceCategoryId { get; set; }

        public decimal? Price { get; set; }

        public string ECommercePriceCategory { get; set; }

    }
}
