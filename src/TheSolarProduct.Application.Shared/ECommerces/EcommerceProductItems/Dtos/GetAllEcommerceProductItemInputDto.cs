﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceProductItems.Dtos
{
    public class GetAllEcommerceProductItemInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int? ProductCategoryId { get; set; }

        public string IsActive { get; set; }

    }
}
