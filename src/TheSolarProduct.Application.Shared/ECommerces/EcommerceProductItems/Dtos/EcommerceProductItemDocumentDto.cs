﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceProductItems.Dtos
{
    public class EcommerceProductItemDocumentDto : EntityDto<int?>
    {
        public int? EcommerceProductItemId { get; set; }

        public int? DocumentTypeId { get; set; }

        public string DocumentName { get; set; }

        public string Documemt { get; set; }

        public string FileToken { get; set; }

        public string FileName { get; set; }

        public string DocumentType { get; set; }

    }
}
