﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceProductItems.Dtos
{
    public class GetAllEcommerceProductItemDto
    {
        public EcommerceProductItemDto EcommerceProductItem { get; set; }
    }
}
