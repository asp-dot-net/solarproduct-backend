﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceProductItems.Dtos
{
    public class EcommerceProductItemDto : EntityDto<int?>
    {
        public int? ProductCategoryId { get; set; }

        public string ProductCategory { get; set; }

        public int? ProductItemID { get; set; }

        public string ProductName { get; set; }

        public string Model { get; set; }

        public string Brand { get; set; }

        public string Code { get; set; }

        public decimal? Size { get; set; }

        public string StockSeries { get; set; }

        public string ProductImage { get; set; }

        public string ProductDescription { get; set; }

        public bool IsDiscounted { get; set; }

        public bool IsNewArrival { get; set; }

        public bool IsFeatured { get; set; }

        public int? Rating { get; set; }

        public int? SpecialStatusId { get; set; }

        public string SpecialStatus { get; set; }

        public bool IsActive { get; set; }

        public List<EcommerceProductItemDocumentDto> EcommerceProductItemDocuments { get; set; }

        public List<EcommerceProductItemPriceCategoryDto> EcommerceProductItemPriceCategories { get; set; }

        public string BrandName { get; set; }

        public int? BrandPartnerId { get; set; }

        public int? SeriesId { get; set; }
        
        public string ProductShortDescription { get; set; }

        public string Specification { get; set; }

        public int? EcommerceProductTypeId { get; set; }


    }
}
