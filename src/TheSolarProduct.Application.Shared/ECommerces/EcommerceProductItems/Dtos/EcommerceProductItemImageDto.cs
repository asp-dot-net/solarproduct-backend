﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.EcommerceProductItems.Dtos
{
    public class EcommerceProductItemImageDto : EntityDto<int?>
    {
        public int? EcommerceProductItemId { get; set; }

        public string ImageName { get; set; }

        public string Image { get; set; }

        public string FileToken { get; set; }

        public string FileName { get; set; }

        public string Extention { get; set; }

        public string CreationTime { get; set; }

        public string CreateBy { get; set; }

       
    }
}
