﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ECommerces.EcommerceProductItems.Dtos;
using TheSolarProduct.ECommerces.ECommerceSliders.Dtos;
using TheSolarProduct.Jobs.Dtos;

namespace TheSolarProduct.ECommerces.EcommerceProductItems
{
    public interface IECommerceProductItemsAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllEcommerceProductItemDto>> GetAll(GetAllEcommerceProductItemInput input);

        Task<EcommerceProductItemDto> GetEcommerceProductItemForView(int id);

        Task<EcommerceProductItemDto> GetEcommerceProductItemForEdit(EntityDto input);

        Task CreateOrEdit(EcommerceProductItemDto input);

        Task Delete(EntityDto input);

        Task<List<GetProductItemForViewDto>> GetAllProductItems(string Model, int productType);

        Task SaveDocument(EcommerceProductItemDocumentDto input);

        Task SaveImage(EcommerceProductItemImageDto input);



        Task<List<EcommerceProductItemDocumentDto>> GetAllEcommerceProductItemDocuments(int productItemId);
        Task<List<EcommerceProductItemImageDto>> GetAllEcommerceProductItemImages(int productItemId);


        //Task<List<string>> GetAllCategories();



    }
}
