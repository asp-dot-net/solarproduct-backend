﻿using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ECommerces.ECommerceSliders.Dtos;

namespace TheSolarProduct.ECommerces.ECommerceSliders
{
    public interface IECommerceSliderAppService : IApplicationService
    {
        Task<PagedResultDto<GetECommerceSliderViewDto>> GetAll(GetAllECommerceSliderInput input);

        Task<GetECommerceSliderViewDto> GetECommerceSliderForView(int id);

        Task<GetECommerceSliderForEditOutputDto> GetECommerceSliderForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditECommerceSliderDto input);

        Task Delete(EntityDto input);
    }
}
