﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.ECommerceSliders.Dtos
{
    public class GetECommerceSliderViewDto
    {
        public ECommerceSliderDto ECommerceSliders { get; set; }
    }
}
