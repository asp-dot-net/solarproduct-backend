﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.ECommerceSliders.Dtos
{
    public class GetAllECommerceSliderInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string IsActive { get; set; }
    }
}
