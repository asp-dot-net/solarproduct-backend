﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.ECommerceSliders.Dtos
{
    public class CreateOrEditECommerceSliderDto : EntityDto<int?>
    {
        public string Name { get; set; }

        public string Img { get; set; }

        public string ImgPath { get; set; }

        public string Header { get; set; }

        public string SubHeader { get; set; }

        public string button { get; set; }

        public bool active { get; set; }

        public string ImageUrl { get; set; }
    }
}
