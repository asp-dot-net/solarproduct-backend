﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ECommerces.ECommerceSliders.Dtos
{
    public class GetECommerceSliderForEditOutputDto
    {
        public CreateOrEditECommerceSliderDto CreateOrEditECommerceSlider { get; set; }
    }
}
