﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleSmsTemplates.Dtos
{
    public class GetWholeSaleSmsTemplateForViewDto
    {
        public WholeSaleSmsTemplateDto WholeSaleSmsTemplate { get; set; }
        public string Organization { get; set; }

    }
}
