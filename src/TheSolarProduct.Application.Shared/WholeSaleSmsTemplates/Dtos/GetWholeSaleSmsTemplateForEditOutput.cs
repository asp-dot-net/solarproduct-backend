﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleSmsTemplates.Dtos
{
    public class GetWholeSaleSmsTemplateForEditOutput
    {
        public CreateOrEditWholeSaleSmsTemplateDto wholeSaleSmsTemplate { get; set; }
    }
}
