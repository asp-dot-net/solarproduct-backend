﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleSmsTemplates.Dtos
{
    public class GetAllWholeSaleSmsTemplatesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int OrganizationUnitId { get; set; }
    }
}
