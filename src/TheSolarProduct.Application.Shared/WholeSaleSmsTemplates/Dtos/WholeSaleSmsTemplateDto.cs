﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleSmsTemplates.Dtos
{
    public class WholeSaleSmsTemplateDto : EntityDto
    {
        public string Name { get; set; }

        public string Text { get; set; }

    }
}
