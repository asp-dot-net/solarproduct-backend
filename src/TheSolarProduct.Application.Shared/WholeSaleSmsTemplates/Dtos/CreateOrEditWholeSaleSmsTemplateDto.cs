﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.WholeSaleSmsTemplates.Dtos
{
    public class CreateOrEditWholeSaleSmsTemplateDto : EntityDto<int?>
    {

        [Required]
        public string Name { get; set; }

        [Required]
        public string Text { get; set; }
        public virtual int? OrganizationUnitId { get; set; }
    }
}
