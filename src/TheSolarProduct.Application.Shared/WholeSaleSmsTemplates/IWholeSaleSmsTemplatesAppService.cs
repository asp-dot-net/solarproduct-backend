﻿using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.WholeSaleSmsTemplates.Dtos;

namespace TheSolarProduct.WholeSaleSmsTemplates
{
    public interface IWholeSaleSmsTemplatesAppService : IApplicationService
    {
        Task<PagedResultDto<GetWholeSaleSmsTemplateForViewDto>> GetAll(GetAllWholeSaleSmsTemplatesInput input);

        Task<GetWholeSaleSmsTemplateForViewDto> GetWholeSaleSmsTemplateForView(int id);

        Task<GetWholeSaleSmsTemplateForEditOutput> GetWholeSaleSmsTemplateForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditWholeSaleSmsTemplateDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetWholeSaleSmsTemplatesToExcel(GetAllWholeSaleSmsTemplatesForExcelInput input);



    }
}
