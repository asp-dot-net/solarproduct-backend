﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.HoldReasons.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.ServiceCategorys.Dtos;
using TheSolarProduct.ServiceSubCategorys.Dtos;
using System.Collections.Generic;

namespace TheSolarProduct.ServiceSubCategorys
{
    public interface IServiceSubCategorysAppService : IApplicationService
    {
        Task<PagedResultDto<GetServiceSubCategoryForViewDto>> GetAll(GetAllServiceSubCategoryInput input);

        Task<GetServiceSubCategoryForViewDto> GetServiceSubCategoryForView(int id);

        Task<GetServiceSubCategoryForEditOutput> GetServiceSubCategoryForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditServiceSubCategoryDto input);

        Task Delete(EntityDto input);
        Task<List<GetSubCategoryDocs>> GetallSubCategorydoc(int? subcategoryid);
        Task SaveSubCategoryDocument(string FileToken, string FileName, int id);
        Task DeleteSubCategoryDoc(int? id);

    }
}
