﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.ServiceSubCategorys.Dtos
{
    public class CreateOrEditServiceSubCategoryDto : EntityDto<int?>
    {
        [Required]
        public int ServiceCategoryId { get; set; }

        [Required]
        public string Name { get; set; }
        public Boolean IsActive { get; set; }
    }
}