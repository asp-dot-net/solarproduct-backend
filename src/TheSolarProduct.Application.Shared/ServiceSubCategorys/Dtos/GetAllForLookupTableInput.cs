﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.ServiceSubCategorys.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}