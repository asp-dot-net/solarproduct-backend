﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.ServiceSubCategorys.Dtos
{
    public class GetAllServiceSubCategoryInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int? ServiceCategoryFilter { get; set; }
        public string NameFilter { get; set; }

    }
}