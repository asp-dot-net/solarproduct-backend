﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.ServiceSubCategorys.Dtos
{
    public class GetAllServiceSubCategoryForExcelInput
    {
        public string Filter { get; set; }

        public string ServiceCategoryFilter { get; set; }
        public string NameFilter { get; set; }

    }
}