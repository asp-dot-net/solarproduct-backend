﻿using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.ServiceSubCategorys.Dtos
{
    public class ServiceSubCategoryDto : EntityDto
    {
        public int ServiceCategoryId { get; set; }
        public string Name { get; set; }
        public string ServiceCategoryName { get; set; }
        public Boolean IsActive { get; set; } 
    }
}