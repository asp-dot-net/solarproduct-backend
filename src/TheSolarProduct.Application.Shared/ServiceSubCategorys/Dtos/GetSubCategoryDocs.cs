﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ServiceSubCategorys.Dtos
{
    public class GetSubCategoryDocs
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public DateTime? CreationTime { get; set; }
        public string CreationUser { get; set; }
        public int? Id { get; set; }
    }
}
