﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.ServiceSubCategorys.Dtos
{
    public class GetServiceSubCategoryForEditOutput
    {
        public CreateOrEditServiceSubCategoryDto serviceSubCategorys { get; set; }

    }
}