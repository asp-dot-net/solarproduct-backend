﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.InstallationCost.InstallationItemList.Dtos;

namespace TheSolarProduct.CategoryInstallationItemLists.Dtos
{
    public class CreateOrEditCategoryInstallationItemPeriodDto : EntityDto<int?>
    {
        public long OrganizationUnit { get; set; }

        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public List<CreateOrEditCategoryInstallationItemListDto> CategoryInstallationItemList { get; set; }

        public string Organization { get; set; }
    }
}
