﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.InstallationCost.InstallationItemList.Dtos;

namespace TheSolarProduct.CategoryInstallationItemLists.Dtos
{
    public class CategoryInstallationItemPeriodForViewDto
    {
        public CategoryInstallationItemPeriodDto CategoryInstallationItemPeriod { get; set; }

        public bool IsEdit { get; set; }
    }
}
