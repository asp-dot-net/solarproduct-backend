﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CategoryInstallationItemLists.Dtos
{
    public class GetCategoryInstallationItemPeriodForEditOutput
    {
        public CreateOrEditCategoryInstallationItemPeriodDto CategoryInstallationItemPeriod { get; set; }
    }
}
