﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CategoryInstallationItemLists.Dtos
{
    public class CreateOrEditCategoryInstallationItemListDto : EntityDto<int?>
    {
        public virtual int CategoryInstallationItemPeriodId { get; set; }

        public virtual string CategoryName { get; set; }

        public virtual int? StartValue { get; set; }

        public virtual int? EndValue { get; set; }
    }
}
