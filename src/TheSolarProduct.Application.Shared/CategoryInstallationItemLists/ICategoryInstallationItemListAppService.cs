﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.CategoryInstallationItemLists.Dtos;
using TheSolarProduct.InstallationCost.InstallationItemList.Dtos;

namespace TheSolarProduct.CategoryInstallationItemLists
{
    public interface ICategoryInstallationItemListAppService : IApplicationService
    {
        Task<PagedResultDto<CategoryInstallationItemPeriodForViewDto>> GetAll(GetAllCategoryInstallationItemListInput input);

        Task<GetCategoryInstallationItemPeriodForEditOutput> GetInstallationItemPeriodForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditCategoryInstallationItemPeriodDto input);

        Task Delete(EntityDto input);

        Task<GetCategoryInstallationItemPeriodForEditOutput> GetInstallationItemPeriodForView(EntityDto input);

        //Task<List<CreateOrEditCategoryInstallationItemListDto>> GetInstallationItemListForCreateView(DateTime StartDate, DateTime EndDate, int? org);

        Task<List<CategoryInstallationItemPeriodForViewDto>> CheckExistInstallationItemList(CreateOrEditCategoryInstallationItemPeriodDto input);
    }
}
