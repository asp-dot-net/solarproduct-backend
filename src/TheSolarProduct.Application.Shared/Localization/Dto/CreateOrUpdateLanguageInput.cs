﻿using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Localization.Dto
{
    public class CreateOrUpdateLanguageInput
    {
        [Required]
        public ApplicationLanguageEditDto Language { get; set; }
    }
}