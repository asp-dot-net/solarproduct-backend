﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.SmsTemplates.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.SmsTemplates
{
    public interface ISmsTemplatesAppService : IApplicationService
    {
        Task<PagedResultDto<GetSmsTemplateForViewDto>> GetAll(GetAllSmsTemplatesInput input);

        Task<GetSmsTemplateForViewDto> GetSmsTemplateForView(int id);

        Task<GetSmsTemplateForEditOutput> GetSmsTemplateForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditSmsTemplateDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetSmsTemplatesToExcel(GetAllSmsTemplatesForExcelInput input);

        

    }
}