﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.SmsTemplates.Dtos
{
    public class GetAllSmsTemplatesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int OrganizationUnitId { get; set; }
    }
}