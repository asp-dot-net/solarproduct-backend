﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.SmsTemplates.Dtos
{
    public class CreateOrEditSmsTemplateDto : EntityDto<int?>
    {

        [Required]
        public string Name { get; set; }

        [Required]
        public string Text { get; set; }
         public virtual int? OrganizationUnitId { get; set; }
    }
}