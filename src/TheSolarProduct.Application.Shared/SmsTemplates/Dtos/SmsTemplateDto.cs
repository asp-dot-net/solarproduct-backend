﻿using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.SmsTemplates.Dtos
{
    public class SmsTemplateDto : EntityDto
    {
        public string Name { get; set; }

        public string Text { get; set; }

    }
}