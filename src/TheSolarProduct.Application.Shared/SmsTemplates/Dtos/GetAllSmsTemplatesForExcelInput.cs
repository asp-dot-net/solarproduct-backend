﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.SmsTemplates.Dtos
{
    public class GetAllSmsTemplatesForExcelInput
    {
        public string Filter { get; set; }

    }
}