﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.SmsTemplates.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}