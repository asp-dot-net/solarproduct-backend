﻿namespace TheSolarProduct.SmsTemplates.Dtos
{
    public class GetSmsTemplateForViewDto
    {
        public SmsTemplateDto SmsTemplate { get; set; }
        public string Organization { get; set; }
    }
}