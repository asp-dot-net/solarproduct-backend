﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.SmsTemplates.Dtos
{
    public class GetSmsTemplateForEditOutput
    {
        public CreateOrEditSmsTemplateDto SmsTemplate { get; set; }

    }
}