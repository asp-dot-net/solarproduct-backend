﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CallFlowQueues.Dtos
{
    public class GetAllCallFlowQueuesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
