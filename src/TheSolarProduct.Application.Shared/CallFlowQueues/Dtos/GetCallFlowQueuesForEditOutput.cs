﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CallFlowQueues.Dtos
{
    public class GetCallFlowQueuesForEditOutput
    {
        public CreateOrEditCallFlowQueueDto CallFlowQueueDto { get; set; }
    }
}
