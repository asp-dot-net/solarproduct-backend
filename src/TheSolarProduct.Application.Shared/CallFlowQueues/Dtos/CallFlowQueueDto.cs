﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CallFlowQueues.Dtos
{
    public class CallFlowQueueDto : EntityDto
    {
        public string Name { get; set; }

        public List<string> OrganizationUnitsName { get; set; }

        public int ExtentionNumber { get; set; }
        public Boolean IsActive {  get; set; }
    }
}
