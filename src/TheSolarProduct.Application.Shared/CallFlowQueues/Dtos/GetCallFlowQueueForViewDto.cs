﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.LeadSources.Dtos;

namespace TheSolarProduct.CallFlowQueues.Dtos
{
    public class GetCallFlowQueueForViewDto
    {
        public CallFlowQueueDto CallFlowQueue { get; set; }

    }
}
