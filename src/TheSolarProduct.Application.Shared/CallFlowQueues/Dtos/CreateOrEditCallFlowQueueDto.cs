﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CallFlowQueues.Dtos
{
    public class CreateOrEditCallFlowQueueDto : EntityDto<int?>
    {
        public string Name { get; set; }

        public List<long> OrganizationUnits { get; set; }

        public int ExtentionNumber { get; set; }
        public Boolean IsActive {  get; set; }
    }
}
