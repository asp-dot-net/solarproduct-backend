﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using TheSolarProduct.CallFlowQueues.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.CallFlowQueues
{
    public interface ICallFlowQueueAppService : IApplicationService
    {
        Task<PagedResultDto<GetCallFlowQueueForViewDto>> GetAll(GetAllCallFlowQueuesInput input);

        Task<GetCallFlowQueueForViewDto> GetCallFlowQueueForView(int id);

        Task<GetCallFlowQueuesForEditOutput> GetCallFlowQueueForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditCallFlowQueueDto input);

        Task Delete(EntityDto input);

        //Task<FileDto> GetLeadSourcesToExcel(GetAllCallFlowQueuesInput input);

    }
}
