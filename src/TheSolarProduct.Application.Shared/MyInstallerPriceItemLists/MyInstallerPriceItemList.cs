﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.MyInstallerPriceItemLists
{
    [Table("MyInstallerPriceItemLists")]
    public class MyInstallerPriceItemList : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public int MyInstallerId { get; set; }

        public int PrictListItemId { get; set; }
        public decimal? Cost { get; set; }
        public decimal? GST { get; set; }
        public decimal? PriceINCGST { get; set; }
    }
}
