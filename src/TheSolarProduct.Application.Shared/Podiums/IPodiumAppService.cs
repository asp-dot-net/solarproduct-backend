﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TheSolarProduct.Podiums
{
    public interface IPodiumAppService : IApplicationService
    {
        Task<string> GetAuthorizationUrl();
    }
}
