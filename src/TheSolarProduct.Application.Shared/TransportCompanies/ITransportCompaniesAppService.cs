﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.CheckDepositReceived.Dto;
using TheSolarProduct.Departments.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.TransportCompanies.Dto;

namespace TheSolarProduct.TransportCompanies
{
    public interface ITransportCompaniesAppService : IApplicationService
    {
        Task<PagedResultDto<GetTransportCompaniesForViewDto>> GetAll(GetAllTransportCompaniesInput nput);

        Task CreateOrEdit(CreateOrEditTransportCompaniesDto input);

        Task<GetTransportCompaniesForEditOutput> GetTransportCompaniesForEdit(EntityDto input);
        Task<FileDto> GetTransportCompanyToExcel(GetAllTransportCompaniesForExcelInput input);

        Task Delete(EntityDto input);


    }
}
