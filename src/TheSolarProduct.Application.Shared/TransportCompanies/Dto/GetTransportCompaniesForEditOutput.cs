﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckDepositReceived.Dto;

namespace TheSolarProduct.TransportCompanies.Dto
{
    public class GetTransportCompaniesForEditOutput
    {
        public CreateOrEditTransportCompaniesDto Transportcompanies { get; set; }
    }
}
