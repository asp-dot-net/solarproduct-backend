﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.TransportCompanies.Dto
{
    public class GetAllTransportCompaniesForExcelInput
    {
        public string Filter { get; set; }
    }
}
