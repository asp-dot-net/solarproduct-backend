﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.TransportCompanies.Dto
{
    public class CreateOrEditTransportCompaniesDto : EntityDto<int?>
    {
        public string CompanyName { get; set; }
        //public object Id { get; set; }
    }
}

