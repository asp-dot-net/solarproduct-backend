﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.TransportCompanies.Dto
{
    public class TransportCompaniesDto : EntityDto
    {
        public string CompanyName { get; set; }
    }
}
