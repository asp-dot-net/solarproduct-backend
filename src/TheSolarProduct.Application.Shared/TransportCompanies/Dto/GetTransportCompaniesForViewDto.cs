﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Departments.Dtos;

namespace TheSolarProduct.TransportCompanies.Dto
{
    public class GetTransportCompaniesForViewDto
    {
        public TransportCompaniesDto transportCompanies { get; set; }
    }
}
