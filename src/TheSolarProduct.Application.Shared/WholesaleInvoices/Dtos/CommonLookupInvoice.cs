﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholesaleInvoices.Dtos
{
    public class CommonLookupInvoice : EntityDto
    {
        
        public string CustomerName { get; set; }

        public string CompanyName { get; set; }
    }
}
