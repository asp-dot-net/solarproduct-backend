﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.ECommerces.Client.Dto;

namespace TheSolarProduct.WholesaleInvoices.Dtos
{
    public class GetWholeSaleInvoiceForViewDto 
    {
        public WholesaleInvoiceDto WholesaleInvoiceDto { get; set; }

        public string CustomerName { get; set; }

        public string Location { get; set; }

        public string JobType { get; set; }

        public decimal? Kw { get; set; }

        public string DeductBy { get; set; }

        public string EmpName { get; set; }


        public int? ProductItemId { get; set; }

        public int? categoryId { get; set; }

        public string ProductName { get; set; }

        public string Model { get; set; }

    }
}
