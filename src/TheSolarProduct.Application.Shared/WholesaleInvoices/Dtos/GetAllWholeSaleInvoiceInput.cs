﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholesaleInvoices.Dtos
{
    public class GetAllWholeSaleInvoiceInput : PagedAndSortedResultRequestDto
    {
        public string FilterName { get; set; }

        public string Filter { get; set; }
        public int? StateId { get; set; }

        public string PostCode { get; set; }

        public string Address { get; set; }

        public string Datefilter { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int OrganizationId { get; set; }
    }
}