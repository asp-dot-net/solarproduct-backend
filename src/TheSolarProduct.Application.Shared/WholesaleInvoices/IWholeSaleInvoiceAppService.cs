﻿using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ECommerces.Client.Dto;
using TheSolarProduct.WholesaleInvoices.Dtos;

namespace TheSolarProduct.WholesaleInvoices
{
    public interface IWholeSaleInvoiceAppService : IApplicationService
    {
        Task CreateOrEdit(WholesaleInvoiceDto input);

        Task<PagedResultDto<GetWholeSaleInvoiceForViewDto>> GetAll(GetAllWholeSaleInvoiceInput input);

        Task Delete(EntityDto input);

        Task<WholesaleInvoiceDto> GetWholeSaleLeadForEdit(EntityDto input);

        Task<List<CommonLookupInvoice>> GetAllInvoiceWithFullName();


    }
}
