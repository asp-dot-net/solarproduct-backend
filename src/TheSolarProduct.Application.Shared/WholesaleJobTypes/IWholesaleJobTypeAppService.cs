﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.WholesaleJobTypes.Dtos;

namespace TheSolarProduct.WholesaleJobTypes
{
    public interface IWholesaleJobTypeAppService : IApplicationService
    {
        Task<PagedResultDto<GetWholesaleJobTypeForViewDto>> GetAll(GetAllWholesaleJobTypeInput input);

        Task CreateOrEdit(CreateOrEditWholesaleJobTypeDto input);

        Task<GetWholesaleJobTypeForEditOutput> GetWholesaleJobTypeForEdit(EntityDto input);

        Task Delete(EntityDto input);
    }
}
