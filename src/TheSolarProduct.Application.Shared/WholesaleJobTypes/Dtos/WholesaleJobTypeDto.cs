﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholesaleJobTypes.Dtos
{
    public class WholesaleJobTypeDto : EntityDto
    {
        public string Name { get; set; }

        public bool? IsActive { get; set; }
    }
}
