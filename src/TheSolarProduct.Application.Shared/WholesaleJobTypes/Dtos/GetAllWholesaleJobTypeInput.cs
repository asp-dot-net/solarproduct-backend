﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholesaleJobTypes.Dtos
{
    public class GetAllWholesaleJobTypeInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
