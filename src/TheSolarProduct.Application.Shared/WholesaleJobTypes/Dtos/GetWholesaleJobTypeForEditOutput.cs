﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.WholesaleTransportTypes.Dtos;

namespace TheSolarProduct.WholesaleJobTypes.Dtos
{
    public class GetWholesaleJobTypeForEditOutput
    {
        public CreateOrEditWholesaleJobTypeDto JobType { get; set; }
    }
}
