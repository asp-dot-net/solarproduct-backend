﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholesaleJobTypes.Dtos
{
    public class CreateOrEditWholesaleJobTypeDto : EntityDto<int?>
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
