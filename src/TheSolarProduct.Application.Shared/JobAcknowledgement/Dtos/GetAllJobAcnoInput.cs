﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobAcknowledgement.Dtos
{
    public class GetAllJobAcnoInput : PagedAndSortedResultRequestDto
    {
        public int JobId { get; set; }

        public string DocType { get; set; }
    }
}
