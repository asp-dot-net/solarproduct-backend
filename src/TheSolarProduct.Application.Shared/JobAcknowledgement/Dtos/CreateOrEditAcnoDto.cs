﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobAcknowledgement.Dtos
{
    public class CreateOrEditAcnoDto : EntityDto<int?>
	{
		public int? TenantId { get; set; }

		public string DocType { get; set; }

		public int? JobId { get; set; }

		public string Name { get; set; }

		public string Address { get; set; }

		public string Mobile { get; set; }

		public string Email { get; set; }

		public string Kw { get; set; }

		public bool YN { get; set; }

		public int? SectionId { get; set;}
	}
}
