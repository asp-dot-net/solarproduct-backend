﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobAcknowledgement.Dtos
{
    public class SendAcknowledgementInputDto
    {
        public int JobId { get; set; }

        public string DocType { get; set; }

        public string SendMode { get; set; }

        public int? SectionId { get; set; }
    }
}
