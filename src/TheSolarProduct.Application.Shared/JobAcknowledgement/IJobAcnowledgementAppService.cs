﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.JobAcknowledgement.Dtos;
using TheSolarProduct.Quotations.Dtos;

namespace TheSolarProduct.JobAcknowledgement
{
    public interface IJobAcnowledgementAppService : IApplicationService
    {
        Task CreateOrEdit(CreateOrEditAcnoDto input);

        Task<PagedResultDto<GetJobAcnoForViewDto>> GetAll(GetAllJobAcnoInput input);

        Task SendAcknowledgement(SendAcknowledgementInputDto input);

        Task<AcknoSignatureRequestDto> AcknoData(string STR);

        Task SaveSignature(SaveSignature input);
    }
}
