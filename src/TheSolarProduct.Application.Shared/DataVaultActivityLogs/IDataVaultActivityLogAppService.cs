﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.DataVaultActivityLogs.Dtos;

namespace TheSolarProduct.DataVaultActivityLogs
{
    public interface IDataVaultActivityLogAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllActivityLogDto>> GetAllDataVaultActivityLog(GetActivityLogInputDto input);

        Task<List<GetAllActivityLogDto>> GetDataVaultActivityLogDetail(int sectionId);

        Task<List<GetActivityLogHistoryDto>> GetDataVaultActivityLogHistory(int activityLogId);
    }
}
