﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DataVaultActivityLogs.Dtos
{
    public class GetActivityLogHistoryDto : FullAuditedEntity
    {
        public string FieldName { get; set; }

        public string prevValue { get; set; }

        public string curValue { get; set; }

        public string ActionName { get; set; }

        public string Lastmodifiedbyuser { get; set; }
    }
}
