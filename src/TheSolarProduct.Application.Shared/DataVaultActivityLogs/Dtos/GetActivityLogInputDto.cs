﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DataVaultActivityLogs.Dtos
{
    public class GetActivityLogInputDto : PagedAndSortedResultRequestDto
    {
        public int? SectionId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set;}

        public bool IsInventory { get; set; }

    }
}
