﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleDataVaultActivityLog.Dtos
{
    public class GetWholeSaleActivityLogInputDto : PagedAndSortedResultRequestDto
    {
        public int? SectionId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

    }
}
