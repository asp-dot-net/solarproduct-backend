﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.WholeSaleDataVaultActivityLog.Dtos;

namespace TheSolarProduct.WholeSaleDataVaultActivityLog
{
    public interface IWholeSaleDataVaultActivityLogAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllWholeSaleActivityLogDto>> GetAllWholeSaleDataVaultActivityLog(GetWholeSaleActivityLogInputDto input);
        Task<List<GetAllWholeSaleActivityLogDto>> GetWholeSaleDataVaultActivityLogDetail(int sectionId);
        Task<List<GetWholeSaleActivityLogHistoryDto>> GetWholeSaleDataVaultActivityLogHistory(int activityLogId);
    }
}
