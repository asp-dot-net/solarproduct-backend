﻿using Abp.Application.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.StockOrdersActivity.Dtos;

namespace TheSolarProduct.StockOrdersActivity
{
    public interface IStockOrderActivityAppService : IApplicationService
    {
        Task<GetVendorForSMSEmailDto> GetVendorForSMSEmailActivity(int id);
        Task SendSms(SMSEmailDto input);
        Task SendEmail(SMSEmailDto input);
        Task AddReminderActivityLog(StockOrderActivityLogInput input);
        Task<List<CommonLookupDto>> GetallSMSTemplates(int PurchaseOrderId);
    }
}
