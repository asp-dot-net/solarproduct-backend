﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockOrdersActivity.Dtos
{
    public class StockOrderActivityLogInput : EntityDto
    {
        public int TenantId { get; set; }

        public int? PurchaseOrderId { get; set; }

        public int? SectionId { get; set; }

        public string ActivityNote { get; set; }

        public DateTime? ActivityDate { get; set; }

        public int? UserId { get; set; }

    }
}
