﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.StockOrdersActivity.Dtos
{
    public class PurchaseOrderDocumentDto
    {
        public  int PurchaseOrderId { get; set; }

        public int PurchaseDocumentId { get; set; }
        public  string PurchaseDocument { get; set; }

        public  string FileName { get; set; }

        public  string FilePath { get; set; }
    }
}
