﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.StockOrdersActivity.Dtos
{
    public class GetVendorForSMSEmailDto
    {
        public string Email { get; set; }

        public string Mobile { get; set; }

        public string CompanyName { get; set; }

        //public List<SendEmailAttachmentDto> filelist { get; set; }

        public string OrderNo { get; set; }
    }
}
