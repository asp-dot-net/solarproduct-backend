﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockOrdersActivity.Dtos
{
    public class SMSEmailDto : EntityDto
    {
        public int? PurchaseOrderId { get; set; }
        

        public int? EmailTemplateId { get; set; }

        public int? SMSTemplateId { get; set; }

        public string Body { get; set; }

        public int? TrackerId { get; set; }

        public int? CustomeTagsId { get; set; }

        public string activityName { get; set; }

        public string Subject { get; set; }

        public string EmailTo { get; set; }
        public string EmailFrom { get; set; }
        public string cc { get; set; }
        public string Bcc { get; set; }
    }
}
