﻿using Abp.Application.Services;
using TheSolarProduct.Dto;
using TheSolarProduct.Logging.Dto;

namespace TheSolarProduct.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();
    }
}
