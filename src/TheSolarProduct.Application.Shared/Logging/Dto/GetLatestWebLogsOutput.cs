﻿using System.Collections.Generic;

namespace TheSolarProduct.Logging.Dto
{
    public class GetLatestWebLogsOutput
    {
        public List<string> LatestWebLogLines { get; set; }
    }
}
