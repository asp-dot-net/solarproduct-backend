﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.Installer.Dto
{
    public class InstalllerInstallationInput : IMustHaveTenant
    {
        public int TenantId { get; set; }

        public int UserId { get; set; }

        public DateTime InstallationDate { get; set; }
        
    }
}
