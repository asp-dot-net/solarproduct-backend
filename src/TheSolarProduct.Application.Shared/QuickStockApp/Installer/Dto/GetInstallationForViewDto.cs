﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.Installer.Dto
{
    public class GetInstallationForViewDto : EntityDto
    {
        public string JobNumber { get; set; }

        public string CustomerName { get; set; }

        public string InstallationTime { get; set; }

        public string Address { get; set; }

        public string Address2 { get; set; }
    }
}
