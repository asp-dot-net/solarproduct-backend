﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.QuickStockApp.Installer.Dto;

namespace TheSolarProduct.QuickStockApp.Installer
{
    public interface IInstallerAppService : IApplicationService
    {
        Task<List<GetInstallationForViewDto>> GetInstallation(InstalllerInstallationInput input);
        Task<List<GetInstallationForViewDto>> SaveInstallation(InstalllerInstallationInput input1);
    }
}
