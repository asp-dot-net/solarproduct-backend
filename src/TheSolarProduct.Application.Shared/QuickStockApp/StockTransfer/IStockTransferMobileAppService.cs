﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.QuickStockApp.StockOrder.Dto;
using TheSolarProduct.QuickStockApp.StockTransfer.Dtos;

namespace TheSolarProduct.QuickStockApp.StockTransfer
{
    public interface IStockTransferMobileAppService : IApplicationService
    {
        Task<List<GetAlllStockTransferInOutDto>> GetAllStockTransferOut(GetAllStockOrderInputDto input);
        Task<List<GetAlllStockTransferInOutDto>> GetAllStockTransferIn(GetAllStockOrderInputDto input);
        Task<GetAllStockTransferDto> GetStockTransferById(int id, string type);
        Task<bool> CheckVerifySerialNo( int productItemId, string serialNo, int WarehouseLocationFromId, string type, int WarehouseLocationToId);
        Task<List<GetAllSerialNo>> GetAllSerialNo( int productItemId, string palletNo, int WarehouseLocationFromId, string type, int WarehouseLocationToId);
    }
}
