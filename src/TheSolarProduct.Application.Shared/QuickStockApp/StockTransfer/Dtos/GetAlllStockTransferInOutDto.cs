﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockTransfer.Dtos
{
    public class GetAlllStockTransferInOutDto : EntityDto
    {

        public virtual long? TransferNo { get; set; }
        public virtual string TrackingNumber { get; set; }
        public virtual string CreationDate { get; set; }
        public virtual DateTime TransferByDate { get; set; }
       
        public virtual int TransferdBy { get; set; }
        
        public virtual DateTime RecivedByDate { get; set; }
        public virtual int RecivedBy { get; set; }
        

        public virtual int? Quantity { get; set; }
        public virtual string WarehouseLocation { get; set; }


        public List<StockTransferItemNameDto> StockTransferItem { get; set; }
    }
        public class StockTransferItemNameDto

        {
            public int Id { get; set; }
            public string StockItem { get; set; }
            
        }
    
}
