﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TheSolarProduct.StockOrder.Dtos;

namespace TheSolarProduct.QuickStockApp.StockTransfer.Dtos
{
    public class GetAllStockTransferDto : EntityDto
    {
        public virtual long? TransferNo { get; set; }
       
        public virtual int? WarehouseLocationToId { get; set; }
        public virtual int? WarehouseLocationFromId { get; set; }
       
        public List<StockTransferItemDto> StockTransferItem { get; set; }
    }
    public class StockTransferItemDto

    {
        public int Id { get; set; }
        public string StockItem { get; set; }
        public int? StockItemId { get; set; }
        public string StockCategory { get; set; }
        public virtual int? Quantity { get; set; }
        public string ModalNo { get; set; }
        public int ? AvailableQty { get; set; }
        public virtual int? PendingQty { get; set; }

        public virtual int? ScanQty { get; set; }
    }
}
