﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockTransfer.Dtos
{
    public class GetAllStockTransferInput : IMustHaveTenant
    {
        public int TenantId { get; set; }

        public int UserId { get; set; }

        public string Filter { get; set; }
    }
}
