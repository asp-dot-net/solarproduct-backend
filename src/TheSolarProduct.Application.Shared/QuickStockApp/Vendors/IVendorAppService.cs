﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.QuickStockApp.Installer.Dto;
using TheSolarProduct.QuickStockApp.Vendors.Dto;

namespace TheSolarProduct.QuickStockApp.Vendors
{
    public interface IVendorAppService : IApplicationService
    {
        Task<List<GetAllVendorDtoForOutput>> GetAllVendors(GetAllVendorInputDto input);

    }
}
