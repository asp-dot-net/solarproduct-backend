﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.Vendors.Dto
{
    public class GetAllVendorInputDto : IMustHaveTenant
    {
        public int TenantId { get; set; }

        public int UserId { get; set; }

        public string Filter { get; set; }
    }
}
