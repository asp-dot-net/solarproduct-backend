﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Vendors.Dtos;

namespace TheSolarProduct.QuickStockApp.Vendors.Dto
{
    public class GetAllVendorDtoForOutput : EntityDto
    {
        public string CompanyName { get; set; }

        public string Notes { get; set; }

        public List<GetVendorContactDto> VendorContacts { get; set; }

    }
}
