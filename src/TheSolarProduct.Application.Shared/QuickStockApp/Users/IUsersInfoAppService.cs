﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.QuickStockApp.Installer.Dto;
using TheSolarProduct.QuickStockApp.Users.Dto;

namespace TheSolarProduct.QuickStockApp.Users
{
    public interface IUsersInfoAppService : IApplicationService
    {
        Task ChangeUserPassword(ChangeUserPasswordInput input);

        Task<UserInfo> GetUserInfo(UserInfoInput input);
        
        //Task SaveInfo(UserInfoInput input);

        Task<List<UserInfo>> SaveUsersList(UserInfoInput input);

        
     }
}