﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.Users.Dto
{
    public class UserWarehouseLocationDto : EntityDto<int?>
    {
        public string State { get; set; }

        public string WarehouseLocation { get; set; }
    }
}
