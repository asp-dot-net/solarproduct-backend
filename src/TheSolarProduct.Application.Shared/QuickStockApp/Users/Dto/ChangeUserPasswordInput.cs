﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.QuickStockApp.Users.Dto
{
    public class ChangeUserPasswordInput
    {
        [Required]
        public string CurrentPassword { get; set; }

        [Required]
        public string NewPassword { get; set; }

        public int TenantId { get; set; }
       // public int? TenantId { get; set; }
    }
}
