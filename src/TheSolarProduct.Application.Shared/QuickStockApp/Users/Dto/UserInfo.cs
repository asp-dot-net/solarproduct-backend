﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.Users.Dto
{
    public class UserInfo : EntityDto<long>
    {
        public string Name { get; set; }
        public string FullName { get; set; }

        public string ProfilePicture { get; set; }

        public bool IsInstaller { get; set; }

        public List<string> GrantedPermissions { get; set; }

        public List<UserWarehouseLocationDto> UserWarehouseLocations { get; set; }
    }
}
