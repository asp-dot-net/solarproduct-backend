﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.Users.Dto
{
    public class UserInfoInput
    {
        public long UserId { get; set; }

        public int TenantId { get; set; }
    }
}
