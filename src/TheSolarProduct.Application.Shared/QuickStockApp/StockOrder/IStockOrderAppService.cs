﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.MultiTenancy.Payments.Stripe.Dto;
using TheSolarProduct.QuickStockApp.StockOrder.Dto;
using TheSolarProduct.QuickStockApp.Vendors.Dto;

namespace TheSolarProduct.QuickStockApp.StockOrder
{
    public interface IStockOrderAppService : IApplicationService
    {
        Task<List<GetAllStockOrderOutput>> GetAllStockOrder(GetAllStockOrderInputDto input);
        
        Task<List<GetStockOrderItemDtoForOutput>> GetStockOrderItemById(int stockOrderId);

        //Task SaveScanSerialNo(SaveSerialNoListInputDto input);

        Task<int> CheckDuplicateSerialNo( List<string> SerialNo);

        Task ImportSerialNo(ImportSerialNoInputDto input);

        Task<List<GetPalletNoOutputDto>> GetPalletNo(GetPalletNoDto input);

        Task<bool> CheckVerifySerialNo( int orderId, int productItemId, string verifyType, string palletNo, string serialNo);

        Task VerifySerialNo( int orderId, int productItemId, string verifyType, List<string> palletNos);

        //Task<List<GetSerialNoOutputDto>> GetAllStockSerialNo(GetPalletNoDto input);
        //    Task SubmitOrder(int orderId, byte[] fileBytesForuser_image, byte[] fileBytesForsignature_image, string longitude, string latitude);
        Task RevertAll( int orderId, int productItemId);

        Task RevertBySerialNo( int orderId, int productItemId, List<string> SerialNo);

        Task AddOtherStockItemQty(OtherStockItemQtyDto input);

        Task RevertOtherQuntity( int orderId, int productItemId);

        Task RevertOtherQuntityByQuntity( int orderId, int productItemId, int QTY);

        Task ImageUpload(List<byte[]> fileBytes, int typeId, int id, int purchaseOrderId);

        Task<bool> CheckSerialNoIsExist(string serialNo);

        Task UpdateStatusFaultyOrDefectSerialNo(List<GetFaultyOrDefectSerialNo> input);

        Task SubmitOrder(GetSubmitOrderDto GetSubmitOrderDto);
        Task<GetAllSerialNoHistoryAppDto> GetSerialNoHistory(string serialNo);
        Task<List<GetLiveStockItemDto>> GetLiveStockByProductName(string ProductName);


    }
}
