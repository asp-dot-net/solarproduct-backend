﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockOrder.Dto
{

    public class GetAllPurchaseOrderItemListForViewDto : EntityDto
    {
        public int ProductTypeId { get; set; }

        public int? ProductItemId { get; set; }
        public string ProductType { get; set; }

        public string ProductItem { get; set; }
        public int? Quantity { get; set; }

        public decimal? PricePerWatt { get; set; }

        public decimal? UnitRate { get; set; }

        public decimal? EstRate { get; set; }

        public decimal? Amount { get; set; }

        public virtual string ModelNo { get; set; }

        public virtual DateTime? ExpiryDate { get; set; }
    }
}
