﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockOrder.Dto
{
    public class GetStockOrderItemDto
    { 
        public string ProductItem { get; set; }

        public string ProductType { get; set; }

        public int PandingQty { get; set; }
    }
}
