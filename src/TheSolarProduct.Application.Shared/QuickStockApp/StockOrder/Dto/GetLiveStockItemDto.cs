﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockOrder.Dto
{
    public class GetLiveStockItemDto
    {
      
        public int Id { get; set; }
        public string ProductItem { get; set; }
        public string Category { get; set; }
        public string ModelNo { get; set; }
        public int? Quantity { get; set; }

        

    }
}
