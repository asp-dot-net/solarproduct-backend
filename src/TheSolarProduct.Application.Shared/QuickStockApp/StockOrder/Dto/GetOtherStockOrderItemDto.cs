﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockOrder.Dto
{
    public class GetOtherStockOrderItemDto
    {
        public int Id { get; set; }
        public int? ProductItemId { get; set; }
        public string ProductItem { get; set; }
       
        public string Category { get; set; }
        public string ModelNo { get; set; }
        public bool IsScanned { get; set; }
    }
}
