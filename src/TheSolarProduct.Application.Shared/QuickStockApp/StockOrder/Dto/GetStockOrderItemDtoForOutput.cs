﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockOrder.Dto
{
    public class GetStockOrderItemDtoForOutput : EntityDto
    {
        public int ProductItemId { get; set; }

        public string ProductType { get; set; }

        public string ProductItem { get; set; }

        public int PendingQuantity { get; set; }

        public int ScanQuantity { get; set; }

        public int? Quantity { get; set; }

        public string ModelNo { get; set; }

        public bool IsExcel { get; set; }

        public bool IsScanned { get; set; }
    }
}
