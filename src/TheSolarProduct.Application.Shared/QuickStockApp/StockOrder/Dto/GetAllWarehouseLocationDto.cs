﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockOrder.Dto
{
    public class GetAllWarehouseLocationDto
    {
        public string LocationName {  get; set; }
        public int LocationId { get; set; }
    }
}
