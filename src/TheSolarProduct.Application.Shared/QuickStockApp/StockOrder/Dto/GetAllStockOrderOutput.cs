﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockOrder.Dto
{
    public class GetAllStockOrderOutput : EntityDto
    {
        public virtual long OrderNo { get; set; }
        public virtual int OrganizationId { get; set; }
        public virtual string Vendor { get; set; }
        public virtual string VendorInoviceNo { get; set; }
        public virtual string PurchaseCompany { get; set; }
        public virtual string ContainerNo { get; set; }

        public virtual string WarehouseLocation { get; set; }
        public virtual string TargetArrivalDate { get; set; }

        public List<string> Items { get; set;}

        public List<GetStockOrderItemDto> StockOrderItems { get; set; }

        public bool IsSubmit { get; set; }



        //  public int Qty { get; set; }
        //    public int PendingQty { get; set; }

        //   public List<GetAllPurchaseOrderItemListDto> PurchaseOrderItemList { get; set; }

    }
}
