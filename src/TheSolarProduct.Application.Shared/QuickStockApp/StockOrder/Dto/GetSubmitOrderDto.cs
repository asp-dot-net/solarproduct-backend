﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockOrder.Dto
{
    public class GetSubmitOrderDto
    {
        public int orderId { get; set; }
        public byte[] fileBytesForuser_image { get; set; }

        public byte[] fileBytesForsignature_image { get; set; }
        public string longitude { get; set; }
        public string latitude { get; set; }

    }
}
