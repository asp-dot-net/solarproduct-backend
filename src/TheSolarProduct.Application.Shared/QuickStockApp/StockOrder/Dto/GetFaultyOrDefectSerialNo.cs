﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockOrder.Dto
{
    public class GetFaultyOrDefectSerialNo
    {
        public List<byte[]> fileBytes { get; set; }

        public int StatusId { get; set; }

        public string SerialNo { get; set; }
    }
}
