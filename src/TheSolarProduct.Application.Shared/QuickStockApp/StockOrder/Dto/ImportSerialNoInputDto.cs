﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockOrder.Dto
{
    public class ImportSerialNoInputDto
    {
        
        
        public int PurchaseOrderId { get; set; }

        public int PurchaseOrderItemId { get; set; }

        public string PalletNo { get; set; }

        public List<string> SerialNo { get; set; }
    }
}
