﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockOrder.Dto
{
    public class GetPalletNoOutputDto
    {
        public string PalletNo { get; set; }

        public bool IsScanned { get; set; }
    }
}
