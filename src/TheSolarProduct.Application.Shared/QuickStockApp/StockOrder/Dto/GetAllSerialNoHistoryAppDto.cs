﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockOrder.Dto
{
    public class GetAllSerialNoHistoryAppDto
    {
        
        public string ProductName { get; set; }
        public string Catogory { get; set; }
        public string SerialNo { get; set; }
        public string ModelNo { get; set; }
        public List<GetSerialNoHistoryAppDto> SerialNoHistory { get; set; }
    }
}
