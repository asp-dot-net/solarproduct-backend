﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockOrder.Dto
{
    public class GetSerialNoHistoryAppDto
    {
        public int? Id { get; set; }
        public string Section { get; set; }
        public string DeductDate { get; set; }
        public string ModuleName { get; set; }
        public string Location { get; set; }
        public string Message { get; set; }
    }
}
