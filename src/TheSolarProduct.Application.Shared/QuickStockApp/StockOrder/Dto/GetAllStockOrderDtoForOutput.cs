﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockOrder.Dto
{
    public class GetAllStockOrderDtoForOutput : EntityDto
    {
        public virtual long OrderNo { get; set; }
        public virtual int OrganizationId { get; set; }
        public virtual string Vendor { get; set; }
        public virtual string VendorInoviceNo { get; set; }
        public virtual string PurchaseCompany { get; set; }

        public virtual string ContainerNo { get; set; }

        public virtual string WarehouseLocation { get; set; }

        public virtual String TargetArrivalDate { get; set; }

        public int Qty { get; set; }
        public int PendingQty { get; set; }


          public virtual string ManualOrderNo { get; set; }

           public virtual string ProductNotes { get; set; }


           public virtual string StockOrderStatus { get; set; }
           public virtual string StockFrom { get; set; }

          public virtual string DeliveryType { get; set; }

          public virtual string PaymentStatus { get; set; }

           public virtual string PaymentType { get; set; }

           public virtual string IncoTerm { get; set; }
            public virtual string TransportCompany { get; set; }

            public virtual String DocSentDate { get; set; }


               public virtual String ETD { get; set; }

               public virtual String ETA { get; set; }

              public virtual String PaymentDueDate { get; set; }
              public virtual String PhysicalDeliveryDate { get; set; }

              public virtual decimal? DiscountPrice { get; set; }

              public virtual decimal? CreditNotePrice { get; set; }

              public virtual decimal? Amount { get; set; }


              public virtual string Currency { get; set; }

              public virtual string GSTType { get; set; }

              public virtual string StockOrderFor { get; set; }
              public virtual string AdditionalNotes { get; set; }
              public virtual DateTime? OrderDate { get; set; }
             public virtual DateTime? ExpDelivaryDate { get; set; }

          public int AmountWithGst { get; set; }
          public int AmountWithOutGst { get; set; }
        public List<GetAllPurchaseOrderItemListForViewDto> PurchaseOrderItemList { get; set; }
       
    }

}
