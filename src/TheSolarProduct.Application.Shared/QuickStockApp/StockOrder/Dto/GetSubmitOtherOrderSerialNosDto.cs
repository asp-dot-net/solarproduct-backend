﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockOrder.Dto
{
    public class GetSubmitOtherOrderSerialNosDto
    {
        public virtual int PurchaseOrderId { get; set; }

        public virtual int ProductItemId { get; set; }

        public virtual int WarehouseLocationId { get; set; }

        public virtual string PalletNo { get; set; }

        public virtual List<string> SerialNo { get; set; }

        public virtual bool IsApp { get; set; }

        public virtual bool IsVerify { get; set; }

        public virtual int? SerialNoStatusId { get; set; }

       
    }
}
