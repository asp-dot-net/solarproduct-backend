﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.QuickStockApp.StockOrder.Dto
{
    public class OtherStockItemQtyDto
    {
        

        public int PurchaseOrderId { get; set; }

        public int ProductItemId { get; set; }

        public int Quantity { get; set; }

        
    }
}
