﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TheSolarProduct.MultiTenancy.Payments.PayPal.Dto;

namespace TheSolarProduct.MultiTenancy.Payments.PayPal
{
    public interface IPayPalPaymentAppService : IApplicationService
    {
        Task ConfirmPayment(long paymentId, string paypalOrderId);

        PayPalConfigurationDto GetConfiguration();
    }
}
