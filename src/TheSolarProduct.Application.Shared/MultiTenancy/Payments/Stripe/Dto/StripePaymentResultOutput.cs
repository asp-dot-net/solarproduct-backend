﻿namespace TheSolarProduct.MultiTenancy.Payments.Stripe.Dto
{
    public class StripePaymentResultOutput
    {
        public bool PaymentDone { get; set; }
    }
}
