﻿namespace TheSolarProduct.MultiTenancy.Payments.Stripe.Dto
{
    public class StripeConfirmPaymentInput
    {
        public string StripeSessionId { get; set; }
    }
}