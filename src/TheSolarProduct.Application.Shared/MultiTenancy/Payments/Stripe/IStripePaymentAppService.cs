﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TheSolarProduct.MultiTenancy.Payments.Dto;
using TheSolarProduct.MultiTenancy.Payments.Stripe.Dto;

namespace TheSolarProduct.MultiTenancy.Payments.Stripe
{
    public interface IStripePaymentAppService : IApplicationService
    {
        Task ConfirmPayment(StripeConfirmPaymentInput input);

        StripeConfigurationDto GetConfiguration();

        Task<SubscriptionPaymentDto> GetPaymentAsync(StripeGetPaymentInput input);

        Task<string> CreatePaymentSession(StripeCreatePaymentSessionInput input);
    }
}