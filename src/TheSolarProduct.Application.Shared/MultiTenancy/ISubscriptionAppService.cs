﻿using System.Threading.Tasks;
using Abp.Application.Services;

namespace TheSolarProduct.MultiTenancy
{
    public interface ISubscriptionAppService : IApplicationService
    {
        Task DisableRecurringPayments();

        Task EnableRecurringPayments();
    }
}
