using System.Threading.Tasks;
using Abp.Application.Services;
using TheSolarProduct.Editions.Dto;
using TheSolarProduct.MultiTenancy.Dto;

namespace TheSolarProduct.MultiTenancy
{
    public interface ITenantRegistrationAppService: IApplicationService
    {
        Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input);

        Task<EditionsSelectOutput> GetEditionsForSelect();

        Task<EditionSelectDto> GetEdition(int editionId);
    }
}