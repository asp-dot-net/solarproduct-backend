﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.PostalTypes.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.PostalTypes
{
    public interface IPostalTypesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetPostalTypeForViewDto>> GetAll(GetAllPostalTypesInput input);

        Task<GetPostalTypeForViewDto> GetPostalTypeForView(int id);

		Task<GetPostalTypeForEditOutput> GetPostalTypeForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditPostalTypeDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetPostalTypesToExcel(GetAllPostalTypesForExcelInput input);
    }
}