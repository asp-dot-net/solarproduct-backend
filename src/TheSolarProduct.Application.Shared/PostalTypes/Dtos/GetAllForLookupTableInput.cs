﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.PostalTypes.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}