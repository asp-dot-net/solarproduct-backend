﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.PostalTypes.Dtos
{
    public class PostalTypeDto : EntityDto
    {
		public string Name { get; set; }

		public string Code { get; set; }
        public  Boolean IsActive { get; set; }


    }
}