﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.PostalTypes.Dtos
{
    public class GetAllPostalTypesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }

		public string CodeFilter { get; set; }



    }
}