﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.PostalTypes.Dtos
{
    public class CreateOrEditPostalTypeDto : EntityDto<int?>
    {

		[Required]
		[StringLength(PostalTypeConsts.MaxNameLength, MinimumLength = PostalTypeConsts.MinNameLength)]
		public string Name { get; set; }
		
		
		[Required]
		[StringLength(PostalTypeConsts.MaxCodeLength, MinimumLength = PostalTypeConsts.MinCodeLength)]
		public string Code { get; set; }

        public virtual Boolean IsActive { get; set; }

    }
}