﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.PostalTypes.Dtos
{
    public class GetPostalTypeForEditOutput
    {
		public CreateOrEditPostalTypeDto PostalType { get; set; }


    }
}