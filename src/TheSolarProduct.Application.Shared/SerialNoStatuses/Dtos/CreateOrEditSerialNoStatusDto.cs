﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.SerialNoStatuses.Dtos
{
    public class CreateOrEditSerialNoStatusDto : EntityDto<int?>
    {
        public string Status { get; set; }

        public bool IsActive { get; set; }
    }
}
