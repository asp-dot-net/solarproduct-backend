﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckActives.Dtos;

namespace TheSolarProduct.SerialNoStatuses.Dtos
{
    public class GetSerialNoStatusForEditOutput
    {
        public CreateOrEditSerialNoStatusDto SerialNoStatus { get; set; }
    }
}
