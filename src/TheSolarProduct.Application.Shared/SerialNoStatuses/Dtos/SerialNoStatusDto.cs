﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.SerialNoStatuses.Dtos
{
    public class SerialNoStatusDto : EntityDto
    {
        public string status { get; set; }

        public bool IsActive { get; set; }



    }
}
