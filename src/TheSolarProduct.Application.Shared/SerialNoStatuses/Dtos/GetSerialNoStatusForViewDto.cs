﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckActives.Dtos;

namespace TheSolarProduct.SerialNoStatuses.Dtos
{
    public class GetSerialNoStatusForViewDto
    {
        public SerialNoStatusDto SerialNoStatus { get; set; }
    }
}
