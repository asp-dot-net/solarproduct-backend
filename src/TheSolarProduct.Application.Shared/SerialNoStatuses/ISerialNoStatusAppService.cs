﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.SerialNoStatuses.Dtos;

namespace TheSolarProduct.SerialNoStatuses
{
  public interface ISerialNoStatusAppService : IApplicationService
    {
        Task<PagedResultDto<GetSerialNoStatusForViewDto>> GetAll(GetAllSerialNoStatusInput input);
        Task<GetSerialNoStatusForEditOutput> GetSerialNoStatusForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditSerialNoStatusDto input);
        Task Delete(EntityDto input);
    }
}
