﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DashboardInvoice.Dtos
{
    public class GetCashInvoiceFinanceInvoiceStateWiseOwingDto
    {
        public decimal? C_Receive { get; set; }

        public decimal? C_Owing { get; set; }

        public decimal? C_Total { get; set; }

        public decimal? F_Receive { get; set; }

        public decimal? F_Owing { get; set; }

        public decimal? F_Total { get; set; }

        public decimal? NSW { get; set; }

        public decimal? QLD { get; set; }

        public decimal? VIC { get; set; }

        public decimal? SA { get; set; }
    }
}
