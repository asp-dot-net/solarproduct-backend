﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DashboardInvoice.Dtos
{
    public class GetInvoiceOverviewDto
    {
        public decimal? Total { get; set; }

        public decimal? Deposite { get; set; }

        public decimal? Active { get; set; }

        public decimal? JobBook { get; set; }

        public decimal? Installed { get; set; }

        public decimal? InComplete { get; set; }
    }
}
