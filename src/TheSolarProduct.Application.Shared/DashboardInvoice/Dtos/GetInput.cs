﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DashboardInvoice.Dtos
{
    public class GetInput
    {
        public int? UserId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? OrgId { get; set; }
    }
}
