﻿using Abp.Application.Services;
using System.Threading.Tasks;
using TheSolarProduct.DashboardInvoice.Dtos;

namespace TheSolarProduct.DashboardInvoice
{
    public interface IDashboardInvoiceAppService : IApplicationService
    {

        Task<GetInvoiceOverviewDto> GetInvoiceOverview(GetInput input);

        Task<GetCashInvoiceFinanceInvoiceStateWiseOwingDto> GetCashInvoice_financeInvoice_StateWiseOwing(GetInput input);

    }
}
