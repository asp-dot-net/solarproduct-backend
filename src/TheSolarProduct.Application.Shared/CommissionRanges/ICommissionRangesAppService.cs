﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using TheSolarProduct.CommissionRanges.Dtos;

namespace TheSolarProduct.CommissionRanges
{
    public interface ICommissionRangesAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllCommissionRangesViewDto>> GetAll(GetAllCommissionRangesInputDto input);

        Task<GetAllCommissionRangesViewDto> GetCommissionRangeForView(int id);

        Task CreateOrEdit(CreateOrEditCommissionRangesDto input);

        Task Delete(EntityDto input);
    }
}
