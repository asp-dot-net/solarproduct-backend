﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CommissionRanges.Dtos
{
    public class GetAllCommissionRangesViewDto
    {
        public CreateOrEditCommissionRangesDto commissionRangesDto { get; set; }
    }
}
