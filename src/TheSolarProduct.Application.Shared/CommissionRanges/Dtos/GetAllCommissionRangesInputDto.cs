﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CommissionRanges.Dtos
{
    public class GetAllCommissionRangesInputDto : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int orgId { get; set; }

    }
}
