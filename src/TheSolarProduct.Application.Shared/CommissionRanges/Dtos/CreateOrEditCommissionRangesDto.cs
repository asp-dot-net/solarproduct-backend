﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CommissionRanges.Dtos
{
    public class CreateOrEditCommissionRangesDto : EntityDto<int?>
    {
        public string Range { get; set; }

        public decimal? Value { get; set; }
        public int? OrgId { get; set; }

        public string OrgName { get; set; }

        public  Boolean IsActive {  get; set; }

    }
}
