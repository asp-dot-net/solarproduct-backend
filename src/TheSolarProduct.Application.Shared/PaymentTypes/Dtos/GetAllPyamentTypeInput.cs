﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PaymentTypes.Dtos
{
    public class GetAllPyamentTypeInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
