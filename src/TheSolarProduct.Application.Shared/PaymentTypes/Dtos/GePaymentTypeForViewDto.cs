﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PaymentTypes.Dtos
{
    public class GePaymentTypeForViewDto
    {
       public PaymentTypeDto PaymentType { get; set; }
    }
}
