﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.PaymentStatuses.Dtos;

namespace TheSolarProduct.PaymentTypes.Dtos
{
    public class GetPaymentTypeForEditOutput
    {
        public CreateOrEditPaymentTypeDto PaymentType { get; set; }
    }
}
