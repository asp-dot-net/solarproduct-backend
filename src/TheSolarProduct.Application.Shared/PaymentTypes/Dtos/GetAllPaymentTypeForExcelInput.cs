﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PaymentTypes.Dtos
{
    public class GetAllPaymentTypeForExcelInput
    {
        public string Filter { get; set; }
    }
}
