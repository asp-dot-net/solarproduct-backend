﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.ReviewTypes.Dtos;
using TheSolarProduct.ServiceStatuses.Dtos;
using TheSolarProduct.ServiceTypes.Dtos;

namespace TheSolarProduct.Reviews
{
    public interface IReviewTypesAppService : IApplicationService
    {
        Task<PagedResultDto<GetReviewTypeForViewDto>> GetAll(GetAllReviewTypeInput input);

        Task<GetReviewTypeForViewDto> GetReviewTypeForView(int id);

        Task<GetReviewTypeForEditOutput> GetReviewTypeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditReviewTypeDto input);

        Task Delete(EntityDto input);

       
    }
}
