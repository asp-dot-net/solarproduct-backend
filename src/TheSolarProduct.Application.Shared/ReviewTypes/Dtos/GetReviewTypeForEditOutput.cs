﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using TheSolarProduct.ServiceCategorys.Dtos;

namespace TheSolarProduct.ReviewTypes.Dtos
{
    public class GetReviewTypeForEditOutput
    {
        public CreateOrEditReviewTypeDto reviewTypes { get; set; }

    }
}