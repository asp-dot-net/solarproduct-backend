﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.ReviewTypes.Dtos
{
    public class CreateOrEditReviewTypeDto : EntityDto<int?>
    {

        [Required]
        public string Name { get; set; }
        public string ReviewLink { get; set; }
        public Boolean IsActive { get; set; }
    }
}