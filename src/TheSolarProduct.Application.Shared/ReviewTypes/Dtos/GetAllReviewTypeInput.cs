﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.ReviewTypes.Dtos
{
    public class GetAllReviewTypeInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }
        public string ReviewLinkFilter { get; set; }

    }
}