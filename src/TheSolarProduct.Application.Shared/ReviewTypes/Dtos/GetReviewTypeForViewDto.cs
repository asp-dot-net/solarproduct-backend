﻿namespace TheSolarProduct.ReviewTypes.Dtos
{
    public class GetReviewTypeForViewDto
    {
        public ReviewTypeDto reviewTypes { get; set; }

    }
}