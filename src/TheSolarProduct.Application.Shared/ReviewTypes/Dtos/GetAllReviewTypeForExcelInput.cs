﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.ReviewTypes.Dtos
{
    public class GetAllReviewTypeForExcelInput
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }

    }
}