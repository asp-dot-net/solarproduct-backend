﻿using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.ReviewTypes.Dtos
{
    public class ReviewTypeDto : EntityDto
    {
        public string Name { get; set; }
        public string ReviewLink { get; set; }
        public Boolean IsActive { get; set; }
    }
}