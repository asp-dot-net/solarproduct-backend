﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.ServiceTypes.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}