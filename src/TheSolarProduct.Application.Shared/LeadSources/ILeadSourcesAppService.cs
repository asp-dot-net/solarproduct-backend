﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.LeadSources.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.LeadSources
{
    public interface ILeadSourcesAppService : IApplicationService
    {
        Task<PagedResultDto<GetLeadSourceForViewDto>> GetAll(GetAllLeadSourcesInput input);

        Task<GetLeadSourceForViewDto> GetLeadSourceForView(int id);

        Task<GetLeadSourceForEditOutput> GetLeadSourceForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditLeadSourceDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetLeadSourcesToExcel(GetAllLeadSourcesForExcelInput input);

    }
}