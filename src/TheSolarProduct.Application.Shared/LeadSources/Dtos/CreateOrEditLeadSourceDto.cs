﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace TheSolarProduct.LeadSources.Dtos
{
    public class CreateOrEditLeadSourceDto : EntityDto<int?>
    {
        [Required]
        [StringLength(LeadSourceConsts.MaxNameLength, MinimumLength = LeadSourceConsts.MinNameLength)]
        public string Name { get; set; }

        public List<long> OrganizationUnits { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsActiveSalesRep { get; set; }

        public decimal MissingPer { get; set; }


    }
}