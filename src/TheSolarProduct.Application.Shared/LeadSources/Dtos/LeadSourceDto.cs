﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.LeadSources.Dtos
{
    public class LeadSourceDto : EntityDto
    {
        public string Name { get; set; }

        public List<string> OrganizationUnitsName { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsActiveSalesRep { get; set; }

    }
}