﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.LeadSources.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}