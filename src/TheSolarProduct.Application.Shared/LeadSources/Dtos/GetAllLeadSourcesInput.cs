﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.LeadSources.Dtos
{
    public class GetAllLeadSourcesInput : PagedAndSortedResultRequestDto
    {
		public int? OrganizationUnit { get; set; }

        public string Filter { get; set; }

        public string NameFilter { get; set; }

    }
}