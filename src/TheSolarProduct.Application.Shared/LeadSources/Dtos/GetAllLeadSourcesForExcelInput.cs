﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.LeadSources.Dtos
{
    public class GetAllLeadSourcesForExcelInput
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }

    }
}