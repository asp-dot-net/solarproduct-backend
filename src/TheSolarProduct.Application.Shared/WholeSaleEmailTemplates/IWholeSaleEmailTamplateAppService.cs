﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.WholeSaleEmailTemplates.Dto;

namespace TheSolarProduct.WholeSaleEmailTemplates
{
    public interface IWholeSaleEmailTamplateAppService
    {
        Task<PagedResultDto<GetWholeSaleEmailTemplateForViewDto>> GetAll(GetAllWholeSaleEmailTemplateInput input);

        Task<GetWholeSaleEmailTemplateForEditOutput> GetWholeSaleEmailTemplateForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditWholeSaleEmailTemplateDto input);

        Task Delete(EntityDto input);

        Task<List<CommonLookupDto>> GetCustomTemplateForTableDropdown(int leadId);
    }
}
