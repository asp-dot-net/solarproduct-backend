﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleEmailTemplates.Dto
{
    public class GetAllWholeSaleEmailTemplateInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int OrganizationId { get; set; }
    }
}
