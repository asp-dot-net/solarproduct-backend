﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.WholeSaleEmailTemplates.Dto
{
    public class CreateOrEditWholeSaleEmailTemplateDto : EntityDto<int?>
    {
        public int TenantId { get; set; }

        public int OrganizationUnitId { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1)]
        public string TemplateName { get; set; }

        [Required]
        public string ViewHtml { get; set; }

        public bool IsActive { get; set; }

    }
}
