﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleEmailTemplates.Dto
{
    public class WholeSaleEmailTamplateDto : EntityDto
    {
        public string TemplateName { get; set; }

        public string OrganazationUnitsName { get; set; }

        public string ViewHtml { get; set; }

        public string ApiHtml { get; set; }

        public bool IsActive { get; set; }

        public string TemplateType { get; set; }
    }
}
