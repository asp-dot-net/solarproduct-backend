﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleEmailTemplates.Dto
{
    public class GetWholeSaleEmailTemplateForEditOutput
    {
        public CreateOrEditWholeSaleEmailTemplateDto EmailTemplateDto { get; set; }
    }
}
