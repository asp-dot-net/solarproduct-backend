﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.CancelReasons.Dtos;
using TheSolarProduct.Dto;


namespace TheSolarProduct.CancelReasons
{
    public interface ICancelReasonsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetCancelReasonForViewDto>> GetAll(GetAllCancelReasonsInput input);

        Task<GetCancelReasonForViewDto> GetCancelReasonForView(int id);

		Task<GetCancelReasonForEditOutput> GetCancelReasonForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditCancelReasonDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetCancelReasonsToExcel(GetAllCancelReasonsForExcelInput input);

		
    }
}