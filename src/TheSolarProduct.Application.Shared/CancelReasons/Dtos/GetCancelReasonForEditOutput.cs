﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.CancelReasons.Dtos
{
    public class GetCancelReasonForEditOutput
    {
		public CreateOrEditCancelReasonDto CancelReason { get; set; }


    }
}