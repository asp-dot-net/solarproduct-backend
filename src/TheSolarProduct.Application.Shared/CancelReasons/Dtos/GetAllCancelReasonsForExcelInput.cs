﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.CancelReasons.Dtos
{
    public class GetAllCancelReasonsForExcelInput
    {
		public string Filter { get; set; }

		public string CancelReasonNameFilter { get; set; }



    }
}