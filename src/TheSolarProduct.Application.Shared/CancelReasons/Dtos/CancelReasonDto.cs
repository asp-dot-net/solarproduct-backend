﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.CancelReasons.Dtos
{
    public class CancelReasonDto : EntityDto
    {
		public string CancelReasonName { get; set; }

        public Boolean IsActive { get; set; }


    }
}