﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.CancelReasons.Dtos
{
    public class CreateOrEditCancelReasonDto : EntityDto<int?>
    {

		[Required]
		[StringLength(CancelReasonConsts.MaxCancelReasonNameLength, MinimumLength = CancelReasonConsts.MinCancelReasonNameLength)]
		public string CancelReasonName { get; set; }
		public Boolean IsActive {  get; set; }
		

    }
}