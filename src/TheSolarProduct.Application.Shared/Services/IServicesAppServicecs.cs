﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.Dto;
using System.Collections.Generic;
using Abp;
using TheSolarProduct.Services.Dtos;
using TheSolarProduct.Jobs.Dtos;
using System;
using TheSolarProduct.ServiceDocumentRequests.Dtos;

namespace TheSolarProduct.Services
{
    public interface IServicesAppServicecs : IApplicationService
    {
        Task CreateOrEditService(CreateOrEditServiceDto input, int sectionID, string Section);
        Task<PagedResultDto<GetServiceForViewDto>> GetAll(GetAllServicessInput input);
        Task<List<GetDuplicateServicePopupDto>> GetDuplicateServiceForView(int id, int orgId);
        Task ChangeDuplicateStatus(int id, bool? IsDuplicate);
        Task DeleteDuplicateServices(EntityDto input);
        Task mergeServices(int id);
        Task deletemergeRecords(int id);
        Task UpdateAssignUserID(List<int> serviceId, int? userId);
        Task<PagedResultDto<GetServiceForViewDto>> GetAllForMyService(GetAllMyServicessInput input);
        Task<GetServiceForCount> GetAllForMyServiceCount(GetAllMyServicessInput input);
        Task<GetMyServiceForEditOutput> GetserviceForEdit(EntityDto input);
        Task<List<GetDuplicateServicePopupDto>> CheckExistServiceList(CreateOrEditServiceDto input);
        Task SendSms(SmsEmailDto input);
        Task SendEmail(SmsEmailDto input);
        Task DeleteManageServices(List<int> serviceId);
        Task<List<GetServiceMapDto>> GetServiceMap(GetAllInputServiceMapDto input);
        Task<List<GetMyServiceDocs>> Getallservicedoc(int? id);
        Task SaveServiceDocument(string FileToken, string FileName, int id);

        Task DeleteServiceDoc(int? id);
        List<ServiceInstallationCalendarDto> GetServiceInstallation(int installerId, DateTime CalendarStartDate, int? orgID, string areaNameFilter, string state);
        Task DeleteMyServices(int serviceId);

        Task<PagedResultDto<GetServiceForViewDto>> GetAllForWarrentyClaimMyService(GetAllMyServicessInput input);

        Task<GetServiceForCount> GetAllForWarrentyClaimMyServiceCount(GetAllMyServicessInput input);

        Task DeleteServiceTempFolder(List<ServiceDocumnetLookupDto> Documents);
        //Task EditServiceCaseNotes(CreateOrEditServiceDto input);
    }
}
