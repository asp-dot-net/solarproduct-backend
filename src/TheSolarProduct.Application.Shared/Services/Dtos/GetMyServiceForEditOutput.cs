﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Services.Dtos
{
    public class GetMyServiceForEditOutput
    {
        public CreateOrEditServiceDto service { get; set; }
    }
}
