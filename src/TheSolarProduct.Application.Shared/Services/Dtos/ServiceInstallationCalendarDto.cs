﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Services.Dtos
{
    public class ServiceInstallationCalendarDto
    {
        public DateTime InstallationDate { get; set; }
        public int InstallationCount { get; set; }
        public List<ServiceInstallationJobsDto> Jobs { get; set; }
    }
}
