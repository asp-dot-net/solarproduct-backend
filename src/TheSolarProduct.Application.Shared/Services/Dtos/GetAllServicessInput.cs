﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Services.Dtos
{
    public class GetAllServicessInput : PagedAndSortedResultRequestDto
    {
        public string FilterName { get; set; }
        public string Filter { get; set; }
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        public int? OrganizationUnit { get; set; }
        public string ServiceCategory { get; set; }
        public string stateNameFilter { get; set; }
        public string AdressFilter { get; set; }
       
    }
}
