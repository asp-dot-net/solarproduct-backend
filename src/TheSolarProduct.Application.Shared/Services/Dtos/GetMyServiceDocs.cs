﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Services.Dtos
{
    public class GetMyServiceDocs
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public DateTime? CreationTime { get; set; }
        public string CreationUser { get; set; }

        public List<InsertMultipleDocs>  listofDocs {get;set;}
        public int? serviceId {get;set;}
    }

    public class InsertMultipleDocs
    {
        public string FileName { get; set; }
        public string FileToken { get; set; }
    }
}
