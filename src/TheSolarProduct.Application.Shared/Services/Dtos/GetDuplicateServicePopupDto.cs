﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Services.Dtos
{
    public  class GetDuplicateServicePopupDto : FullAuditedEntity
    {
		public string CompanyName { get; set; }

		public string CurrentLeadOwaner { get; set; }

		public string Email { get; set; }

		public string Mobile { get; set; }


		public string UnitType { get; set; }


		public string StreetType { get; set; }


		public string State { get; set; }


		public string Address { get; set; }


		public string CurrentAssignUserName { get; set; }

		public string CreatedByName { get; set; }

		public string ProjectNo { get; set; }

		public string ProjectStatus { get; set; }

		public DateTime? ProjectOpenDate { get; set; }

		public DateTime? LastFollowupDate { get; set; }

		public string Description { get; set; }

		public string RoleName { get; set; }
		public string ServiceSource { get; set; }
		public string ServiceCategory { get; set; }
		public string ServiceStatus { get; set; }
		public string ServicePriority { get; set; }
		public bool? EmailExist { get; set; }
		public bool? MobileExist { get; set; }

		public string ServiceSubCategory { get; set; }
		public string OtherEmail { get; set; }
		public string Notes { get; set; }
		public string ServiceLine { get; set; }
	}
}
