﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Services.Dtos
{
    public class GetServiceForViewDto
    {
        public ServiceDto service { get; set; }

        public string OrganizationName { get; set; }

        public bool Duplicate { get; set; }
        public bool IsMerge { get; set; }
        public bool? isSelected { get; set; }

        public string ReminderTime { get; set; }
        public string ActivityDescription { get; set; }
        public string ActivityComment { get; set; }
        public string AssignUser { get; set; }
        public string ServiceStatus { get; set; }
        public string ServicePriority { get; set; }
    }

    public class GetServiceForCount
    {
        public string Open { get; set; }
        public string Pending { get; set; }
        public string Assign { get; set; }
        public string Resolved { get; set; }
        public string Closed { get; set; }
        
    }
}
