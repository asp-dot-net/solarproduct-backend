﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Services.Dtos
{
    public class ServiceDto : EntityDto
    {
        public int OrganizationId { get; set; }
        public string ServiceSource { get; set; }
        public string State { get; set; }
        public int? ServicePriorityId { get; set; }
        public int? ServiceCategoryId { get; set; }
        public int? ServiceStatusId { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string JobNumber { get; set; }
        public string QuoteNo { get; set; }
        public string Notes { get; set; }
        public DateTime? ServiceAssignDate { get; set; }
        public string ServiceInstaller { get; set; }
        public string ServiceLine { get; set; }
        public int? LeadId { get; set; }
        public string ServiceCategoryName { get; set; }

        public int? JobId { get; set; }
        public string TicketNo { get; set; }

        public int TenentId { get; set; }
    }
}
