﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Services.Dtos
{
    public class CreateOrEditServiceDto : EntityDto<int?>
    {
        public int OrganizationId { get; set; }
        public string ServiceSource { get; set; }
        public string State { get; set; }
        public int? ServicePriorityId { get; set; }
        public int? ServiceCategoryId { get; set; }
        public int? ServiceStatusId { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string JobNumber { get; set; }
        public string QuoteNo { get; set; }
        public string Notes { get; set; }
        public DateTime? ServiceAssignDate { get; set; }
        public string ServiceInstaller { get; set; }
        public string ServiceLine { get; set; }
        public  int? LeadId { get; set; }
        public  int? JobId { get; set; }
        public string ServiceCategoryName { get; set; }
        public int? ServiceAssignId { get; set; }
        public int? IsExternalLead { get; set; }
        public  bool? IsDuplicate { get; set; }
        public  bool? IsMergeRecord { get; set; }
        public int? IsMergeCount { get; set; }
        public string  OtherEmail { get; set; }
        public int? ServiceSubCategoryId { get; set; }
        public int? InstallerId { get; set; }
        public DateTime? InstallerDate { get; set; }
        public string InstallerNotes { get; set; }
        public  bool IsWarrantyClaim { get; set; }

        public string CaseNo { get; set; }

        public string CaseNotes { get; set; }

        public string CaseStatus { get; set; }

        public string TrackingNo { get; set; }

        public string TrackingNotes { get; set; }

        public string TicketNo { get; set; }

        public string Pickby { get; set; }

        public DateTime? PickUpDate { get; set; }

        public string PickupMethod { get; set; }

        public string PickupNotes { get; set; }
        public DateTime? StockArrivalDate { get; set; }

        public string ServiceAssign { get; set; }



    }
}
