﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Services.Dtos
{
    public class ServiceInstallationJobsDto
    {
		public string InstallerName { get; set; }
		public string CustomerName { get; set; }
		public string InstallationTime { get; set; }
		public int JobId { get; set; }
		public int? LeadId { get; set; }
		public string JobNumber { get; set; }
		public string Suburb { get; set; }
		public string State { get; set; }
		public string PostalCode { get; set; }
		public string Address { get; set; }
		public string ServiceSource { get; set; }
		public string ServiceStatus { get; set; }
		public string ServicePriority { get; set; }

		public string ServiceCategoryName { get; set; }
	}
}
