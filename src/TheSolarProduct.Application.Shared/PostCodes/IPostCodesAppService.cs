﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.PostCodes.Dtos;
using TheSolarProduct.Dto;
using System.Collections.Generic;


namespace TheSolarProduct.PostCodes
{
    public interface IPostCodesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetPostCodeForViewDto>> GetAll(GetAllPostCodesInput input);

        Task<GetPostCodeForViewDto> GetPostCodeForView(int id);

		Task<GetPostCodeForEditOutput> GetPostCodeForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditPostCodeDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetPostCodesToExcel(GetAllPostCodesForExcelInput input);

		Task<List<PostCodeStateLookupTableDto>> GetAllStateForTableDropdown();
		
    }
}