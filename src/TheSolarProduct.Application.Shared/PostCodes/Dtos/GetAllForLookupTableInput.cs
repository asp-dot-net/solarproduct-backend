﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.PostCodes.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}