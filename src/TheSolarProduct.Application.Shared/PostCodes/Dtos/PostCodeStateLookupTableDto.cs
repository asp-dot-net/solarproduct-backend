﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.PostCodes.Dtos
{
    public class PostCodeStateLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}