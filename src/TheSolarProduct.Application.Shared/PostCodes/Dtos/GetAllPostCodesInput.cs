﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.PostCodes.Dtos
{
    public class GetAllPostCodesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string PostalCodeFilter { get; set; }

		public string SuburbFilter { get; set; }

		public string POBoxesFilter { get; set; }

		public string AreaFilter { get; set; }


		 public string StateNameFilter { get; set; }

		 
    }
}