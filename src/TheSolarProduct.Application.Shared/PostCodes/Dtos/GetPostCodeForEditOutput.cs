﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.PostCodes.Dtos
{
    public class GetPostCodeForEditOutput
    {
		public CreateOrEditPostCodeDto PostCode { get; set; }

		public string StateName { get; set;}


    }
}