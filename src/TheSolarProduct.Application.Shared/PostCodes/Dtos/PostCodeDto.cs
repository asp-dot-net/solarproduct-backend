﻿
using System;
using Abp.Application.Services.Dto;

namespace TheSolarProduct.PostCodes.Dtos
{
    public class PostCodeDto : EntityDto
    {
		public string PostalCode { get; set; }

		public string Suburb { get; set; }

		public string POBoxes { get; set; }

		public string Area { get; set; }


		 public int StateId { get; set; }

		public  string Areas { get; set; }
		public Boolean IsActive {  get; set; }
	}
}