﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.PostCodes.Dtos
{
    public class CreateOrEditPostCodeDto : EntityDto<int?>
    {

		[Required]
		[RegularExpression(PostCodeConsts.PostalCodeRegex)]
		[StringLength(PostCodeConsts.MaxPostalCodeLength, MinimumLength = PostCodeConsts.MinPostalCodeLength)]
		public string PostalCode { get; set; }
		
		
		[Required]
		[StringLength(PostCodeConsts.MaxSuburbLength, MinimumLength = PostCodeConsts.MinSuburbLength)]
		public string Suburb { get; set; }
		
		
		//[Required]
		[StringLength(PostCodeConsts.MaxPOBoxesLength, MinimumLength = PostCodeConsts.MinPOBoxesLength)]
		public string POBoxes { get; set; }
		
		
		[StringLength(PostCodeConsts.MaxAreaLength, MinimumLength = PostCodeConsts.MinAreaLength)]
		public string Area { get; set; }

		public string Areas { get; set; }
		public int StateId { get; set; }
        public Boolean IsActive { get; set; }

    }
}