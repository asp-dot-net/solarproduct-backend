﻿using Abp.Configuration;

namespace TheSolarProduct.Timing.Dto
{
    public class GetTimezonesInput
    {
        public SettingScopes DefaultTimezoneScope { get; set; }
    }
}
