﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.DeclarationForms.Dtos;

namespace TheSolarProduct.DeclarationForms
{
    public interface IDeclarationFormAppService : IApplicationService
    {
        Task CreateOrEdit(CreateOrEditDeclarationFormDto input);

        Task<PagedResultDto<GetDeclarationFormForViewDto>> GetAll(GetAllDeclarationFormInput input);

        Task SendDeclarationForm(SendDeclarationFormInputDto input);
        Task<DeclarationFormSignatureRequestDto> DeclarationFormData(string STR);

        Task SaveSignature(SaveDeclareSignature input);
    }
}
