﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DeclarationForms.Dtos
{
    public class SendDeclarationFormInputDto
    {
        public int JobId { get; set; }

        public string DocType { get; set; }

        public string SendMode { get; set; }

        public int? SectionId { get; set; }
    }
}
