﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DeclarationForms.Dtos
{
    public class GetDeclarationFormForViewDto
    {

        public DeclarationFormDto DeclarationForm { get; set; }
    }
}
