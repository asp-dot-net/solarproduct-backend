﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DeclarationForms.Dtos
{
    public class CreateOrEditDeclarationFormDto : EntityDto<int?>
    {
        public int? TenantId { get; set; }

        public string DocType { get; set; }

        public int? JobId { get; set; }

        public string Kw { get; set; }

        public bool YN { get; set; }

        public int? SectionId { get; set; }

    }
}
