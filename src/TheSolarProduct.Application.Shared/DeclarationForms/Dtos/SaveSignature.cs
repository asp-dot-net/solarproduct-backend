﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DeclarationForms.Dtos
{
    public class SaveDeclareSignature
    {
        public string Page { get; set; }

        public string CustSignIP { get; set; }

        public string CustSignLongitude { get; set; }

        public string CustSignLatitude { get; set; }

        public string EncString { get; set; }

        public string ImageData { get; set; }

        public bool YesNo { get; set; }
    }
}
