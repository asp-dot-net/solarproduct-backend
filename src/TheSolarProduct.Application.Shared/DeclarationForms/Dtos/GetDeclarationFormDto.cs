﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.DeclarationForms.Dtos
{
    public class GetDeclarationFormDto
    {
        public string DocType { get; set; }

        public string Name { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public string JobNumber { get; set; }

        public string Kw { get; set; }

        public bool? YN { get; set; }

        public string SignSrc { get; set; }

        public string SignDate { get; set; }

        public string ViewHtml { get; set; }

        public string OrgLogo { get; set; }

        public string State { get; set; }

    }
}
