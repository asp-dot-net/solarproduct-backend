﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.WholesalePropertyTypes.Dtos;

namespace TheSolarProduct.EcommerceSpecifications.Dtos
{
    public class GetEcommerceSpecificationForViewDto
    {
        public EcommerceSpecificationDto Specification { get; set; }
    }
}
