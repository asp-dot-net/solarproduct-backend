﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.EcommerceSpecifications.Dtos
{
    public class CreateOrEditSpecificationDto : EntityDto<int?>
    {
        public string Name { get; set; }

        public List<string> ProductType { get; set; }
        public bool IsActive { get; set; }

       
    }
}
