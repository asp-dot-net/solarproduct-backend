﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.WholesalePropertyTypes.Dtos;

namespace TheSolarProduct.EcommerceSpecifications.Dtos
{
    public class GetEcommerceSpecificationForEditOutput
    {
        public CreateOrEditSpecificationDto Specification { get; set; }
    }
}
