﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.EcommerceSpecifications.Dtos;
using TheSolarProduct.WholesalePropertyTypes.Dtos;

namespace TheSolarProduct.EcommerceSpecifications
{
    public interface IEcommerceSpecificationAppService : IApplicationService
    {
        Task<PagedResultDto<GetEcommerceSpecificationForViewDto>> GetAll(GetAllEcommerceSpecificationInput input);

        Task CreateOrEdit(CreateOrEditSpecificationDto input);

        Task<GetEcommerceSpecificationForEditOutput> GetEcommerceSpecificationForEdit(EntityDto input);

        Task Delete(EntityDto input);
    }
}
