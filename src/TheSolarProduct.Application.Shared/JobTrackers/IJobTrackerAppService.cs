﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.JobTrackers.Dtos;

namespace TheSolarProduct.JobTrackers
{
    public interface IJobTrackerAppService : IApplicationService
    {
        Task<PagedResultDto<GetViewTrackerDto>> GetAllJobActiveTracker(GetAllJobActiveTrackerInput input);

        Task<PagedResultDto<GetViewApplicationTrackerDto>> GetAllJobApplicationTracker(GetAllJobApplicationTrackerInput input);

        Task<PagedResultDto<GetViewFreebiesTrackerDto>> GetAllFreebiesTracker(GetAllFreebiesTrackerInput input);

        Task<PagedResultDto<GetViewFinanceTrackerDto>> GetAllFinanceTracker(GetAllFinanceTrackerInput input);

        Task<PagedResultDto<GetViewRefundTrackerDto>> GetAllRefundTracker(GetAllRefundTrackerInput input);

        Task<PagedResultDto<GetViewReferralTrackerDto>> GetAllReferralTracker(GetAllReferralTrackerInput input);

        Task<PagedResultDto<GetViewGridConnectionTrackerDto>> GetAllGridConnectionTracker(GetAllGridConnectionTrackerInput input);

        Task<PagedResultDto<GetViewSTCTrackerDto>> GetAllSTCTracker(GetAllSTCTrackerInput input);

        #region Excel Export
        Task<FileDto> GetAllJobActiveTrackerExport(GetAllJobActiveTrackerExcelInput input);

        Task<FileDto> GetAllJobActiveCheckActiveTrackerExport(GetAllJobActiveTrackerExcelInput input);

        Task<FileDto> GetAllSTCTrackerExport(GetAllSTCTrackerExcelInput input);

        Task<FileDto> GetAllGridConnectionTrackerExcel(GetAllGridConnectionTrackerExcelInput input);

        Task<FileDto> GetAllGridConnectExcel(GetAllGridConnectionTrackerExcelInput input);

        Task<FileDto> GetAllFreebiesTrackerExcel(GetAllFreebiesTrackerExcelInput input);

        #endregion
    }
}
