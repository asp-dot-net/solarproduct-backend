﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetAllJobActiveTrackerExcelInput
    {
        
        public string FilterName { get; set; }

        public string Filter { get; set; }

        public int OrganizationUnit { get; set; }

        public int ApplicationStatus { get; set; }

        public int? PaymentId { get; set; }

        public List<int> JobStatusIDFilter { get; set; }

        public string DateType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        
        public int ExcelOrCsv { get; set; }

        public int? SalesRepId { get; set; }

        public string State { get; set; }

    }
}
