﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class SummaryCountDto
    {
        public int Total { get; set; }

        public int Active { get; set; }

        public int ActiveReady { get; set; }

        public int Awaiting { get; set; }
    }
}
