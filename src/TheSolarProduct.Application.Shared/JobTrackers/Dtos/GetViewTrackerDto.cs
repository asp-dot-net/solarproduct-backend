﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetViewTrackerDto
    {
        public int Id { get; set; }

        public int LeadId { get; set; }

        public string JobNumber { get; set; }

        public string JobType { get; set; }

        public string JobStatus { get; set; }

        public string JobStatusColorClass { get; set; }

        public string CompanyName { get; set; }

        public bool JobActiveSmsSend { get; set; }

        public DateTime? JobActiveSmsSendDate { get; set; }

        public bool JobActiveEmailSend { get; set; }

        public DateTime? JobActiveEmailSendDate { get; set; }

        public string CurruntLeadOwner { get; set; }

        public string Comment { get; set; }

        public string NextFollowup { get; set; }

        public string Discription { get; set; }

        public SummaryCountDto Summary { get; set; }
    }
}
