﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetAllFreebiesTrackerExcelInput
    {
        public string FilterName { get; set; }

        public string Filter { get; set; }

        public int OrganizationUnit { get; set; }

        public int ApplicationStatus { get; set; }

        public int PromoType { get; set; }

        public string State { get; set; }

        public int TransportType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Filename { get; set; }
    }
}
