﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetViewRefundTrackerDto
    {
        public int Id { get; set; }

        public int JobId { get; set; }

        public int LeadId { get; set; }

        public string JobNumber { get; set; }

        public string CompanyName { get; set; }

        public DateTime CreationTime { get; set; }

        public decimal? Amount { get; set; }

        public decimal? PaidAmount { get; set; }

        public string BSBNo { get; set; }

        public string AccountNo { get; set; }

        public string CurrentLeadOwaner { get; set; }

        public string RefundReasonName { get; set; }

        public string PaymentOptionName { get; set; }

        public DateTime? PaidDate { get; set; }

        public string PreviousProjectStatus { get; set; }

        public string ProjectStatus { get; set; }

        public string JobStatusColorClass { get; set; }

        public bool? JobRefundSmsSend { get; set; }

        public DateTime? JobRefundSmsSendDate { get; set; }
        
        public bool? JobRefundEmailSend { get; set; }

        public DateTime? JobRefundEmailSendDate { get; set; }

        public string RequestedBy { get; set; }

        public string ActivityReminderTime { get; set; }

        public string ActivityDescription { get; set; }

        public string ActivityComment { get; set; }

        public bool Verifyornot { get; set; }

        public RefundSummaryCountDto Summary { get; set; }

        public string RefundNotes { get ; set; }

        public string PaymentType { get ; set; }

        public string VerifyBy { get; set; }

        public int? NotVerifiedDays { get; set; }  

        public int? NotPaidDays { get; set; }

        public decimal? TotalAmountRequested { get; set; }

        public bool IsSelect { get; set; }

    }

    public class RefundSummaryCountDto
    {
        public int Total { get; set; }

        public decimal TotalAmt { get; set; }

        public int Pending { get; set; }

        public decimal PendingAmt { get; set; }

        public int Refunded { get; set; }

        public decimal RefundedAmt { get; set; }
    }
}
