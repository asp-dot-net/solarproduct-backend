﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetViewReferralTrackerDto
    {
        public int Id { get; set; }

        public int LeadId { get; set; }

        public string JobNumber { get; set; }

        public string CompanyName { get; set; }

        public string JobStatus { get; set; }

        public string JobStatusColorClass { get; set; }

        public DateTime? InstallComplate { get; set; }

        public decimal? TotalCost { get; set; }

        public decimal OwningAmt { get; set; }

        public string RefferedProjectName { get; set; }

        public string RefferedProjectStatus { get; set; }

        public string RefferedProjectStatusColorClass { get; set; }

        public decimal? RefferedTotalCost { get; set; }

        public decimal RefferedowningAmt { get; set; }

        public string AccountName { get; set; }

        public string BSBNo { get; set; }

        public string AccountNo { get; set; }

        public decimal? ReferralAmount { get; set; }

        public string ActivityReminderTime { get; set; }

        public string ActivityDescription { get; set; }

        public string ActivityComment { get; set; }

        public bool ReferralPayment { get; set; }

        public bool? IsRefferalVerify { get; set; }

        public bool Isverifyornot { get; set; }

        public ReferralSummaryCountDto Summary { get; set; }

        public DateTime? PaymentDate { get; set; }

        public string BankRefNo { get; set; }

        public string PaymentRemark { get; set;}
    }

    public class ReferralSummaryCountDto
    {
        public int Total { get; set; }

        public int Paid { get; set; }

        public int ReadyToPay { get; set; }

        public int Awaiting { get; set; }
    }
}
