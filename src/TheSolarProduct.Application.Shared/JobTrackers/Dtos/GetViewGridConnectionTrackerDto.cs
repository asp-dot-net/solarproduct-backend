﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetViewGridConnectionTrackerDto
    {
        public int Id { get; set; }

        public int LeadId { get; set; }

        public string JobNumber { get; set; }

        public string JobTypeName { get; set; }

        public string JobType { get; set; }

        public string JobStatus { get; set; }

        public string JobStatusColorClass { get; set; }

        public string CompanyName { get; set; }

        public DateTime? InstallationDate { get; set; }

        public string Address { get; set; }

        public string Suburb { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public string InstallerName { get; set; }

        public DateTime? InstalledCompleteDate { get; set; }

        public string MeterApplyRef { get; set; }

        public DateTime? InspectionDate { get; set; }

        public string ElecDistributorName { get; set; }

        public string ElecRetailerName { get; set; }

        public DateTime? DistApproveDate { get; set; }

        public DateTime? DistExpiryDate { get; set; }

        public string ApplicationRefNo { get; set; }

        public decimal? SystemCapacity { get; set; }

        public string ActivityReminderTime { get; set; }

        public string ActivityDescription { get; set; }

        public string ActivityComment { get; set; }

        public bool? GridConnectionSmsSend { get; set; }

        public DateTime? GridConnectionSmsSendDate { get; set; }

        public bool? GridConnectionEmailSend { get; set; }

        public DateTime? GridConnectionEmailSendDate { get; set; }

        public GridConnectionSummaryCountDto Summary { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }
        public string NMINumber { get; set; }

        public int? NotAppliedDays { get; set; }

    }

    public class GridConnectionSummaryCountDto
    {
        public int Total { get; set; }

        public int Awaiting { get; set; }

        public int Pending { get; set; }

        public int Complete { get; set; }
    }
}
