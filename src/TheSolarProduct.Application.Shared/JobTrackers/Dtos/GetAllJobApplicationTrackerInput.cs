﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetAllJobApplicationTrackerInput : PagedAndSortedResultRequestDto
    {
        public string FilterName { get; set; }

        public string Filter { get; set; }

        public int OrganizationUnit { get; set; }

        public int ApplicationStatus { get; set; }

        public string State { get; set; }

        public int? ElecDistributorId { get; set; }

        public List<int> JobStatusIDFilter { get; set; }

        public int? JobTypeId { get; set; }

        public string DateType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int excelorcsv { get; set; }

    }
}
