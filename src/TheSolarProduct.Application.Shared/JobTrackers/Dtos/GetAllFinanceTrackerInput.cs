﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetAllFinanceTrackerInput : PagedAndSortedResultRequestDto
    {
        public string FilterName { get; set; }

        public string Filter { get; set; }

        public int OrganizationUnit { get; set; }

        public int PaymentOption { get; set; }

        public int IsVerified { get; set; }

        public string State { get; set; }

        public int? excelorcsv { get; set; }
    }
}
