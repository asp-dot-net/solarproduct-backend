﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetAllSTCTrackerInput : PagedAndSortedResultRequestDto
    {
        public string FilterName { get; set; }

        public string Filter { get; set; }

        public int OrganizationUnit { get; set; }

        public List<int> PvdStatus { get; set; }

        public int? PvdNoStatus { get; set; }

        public int? SalesRepId { get; set; }

        public List<int> JobStatusIDFilter { get; set; }

        public int JobTypeId { get; set; }

        public string PvdNo { get; set; }

        public int? InstallerId { get; set; }

        public string State { get; set; }

        public string RECStatus { get; set; }

        public string DateType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? BatteryFilter { get; set; }
    }
}
