﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetAllGridConnectExcelDto
    {
        public string CompanyName { get; set; } 

        public string Mobile { get; set; } 

        public string Email { get; set; } 

        public string Address { get; set; } 

        public string PostCode { get; set; } 

        public string State { get; set; } 

        public string JobNumber { get; set; } 

        public string JobType { get; set; } 

        public DateTime? InstallationDate { get; set; }

        public decimal? SystemCapacity { get; set;}

        public string NMINumber { get; set; }

        public string ApplicationRefNo { get; set; }

        public string ElecRetailers { get; set; }

    }
}
