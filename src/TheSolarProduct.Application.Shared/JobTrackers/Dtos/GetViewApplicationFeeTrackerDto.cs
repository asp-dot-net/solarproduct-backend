﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Net.Mime.MediaTypeNames;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetViewApplicationFeeTrackerDto
    {
        public int Id { get; set; }

        public int LeadId { get; set; }

        public string JobNumber { get; set; }

        public string JobType { get; set; }

        public string JobStatus { get; set; }

        public string JobStatusColorClass { get; set; }

        public string CompanyName { get; set; }

        public string Address { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }
        public string Suburb { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public string ElecDistributor { get; set; }
        public string ApplicationFeesPaid { get; set; }

        public decimal? PaidAmount { get; set; }
        public string PaidBy { get; set; }
        public string PaidStatus { get; set; }
        public string InvRefNo { get; set; }
        public DateTime? InvVerifyDate { get; set; }
        public bool? SmsSend { get; set; }

        public DateTime? SmsSendDate { get; set; }

        public bool? EmailSend { get; set; }

        public DateTime? EmailSendDate { get; set; }

        public string CurruntLeadOwner { get; set; }

        public string NextFollowup { get; set; }

        public string Discription { get; set; }

        public string Comment { get; set; }

        public DateTime? EssentialDistApplied { get; set; }

        public string EssentialApprovalRef { get; set; }

        public ApplicationFeeSummaryCountDto Summary { get; set; }

        public bool IsExpired { get; set; }
    }

    public class ApplicationFeeSummaryCountDto
    {
        public int Total { get; set; }

        public int Paid { get; set; }

        public int Pending { get; set; }

      
    }
}

