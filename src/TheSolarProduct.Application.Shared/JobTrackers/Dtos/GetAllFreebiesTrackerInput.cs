﻿using Abp.Application.Services.Dto;
using System;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetAllFreebiesTrackerInput : PagedAndSortedResultRequestDto
    {
        public string FilterName { get; set; }

        public string Filter { get; set; }

        public int OrganizationUnit { get; set; }

        public int ApplicationStatus { get; set; }

        public int PromoType { get; set; }
        
        public string State { get; set; }

        public int TransportType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
