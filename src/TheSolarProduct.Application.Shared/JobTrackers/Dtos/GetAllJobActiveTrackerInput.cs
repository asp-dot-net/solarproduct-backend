﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetAllJobActiveTrackerInput : PagedAndSortedResultRequestDto
    {
        public string FilterName { get; set; }

        public string Filter { get; set; }

        public int OrganizationUnit { get; set; }

        public int ApplicationStatus { get; set; }

        public int? PaymentId { get; set; }

        public List<int> JobStatusIDFilter { get; set; }

        public string DateType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string State { get; set; }
        public int? SalesRepId { get; set; }

    }
}
