﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetViewFreebiesTrackerDto
    {
        public int Id { get; set; }

        public int JobId { get; set; }

        public int LeadId { get; set; }

        public string JobNumber { get; set; }

        public string PromotionName { get; set; }

        public string CompanyName { get; set; }

        public string Mobile { get; set; }

        public string Address { get; set; }

        public string Suburb { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public string Email { get; set; }

        public string JobStatus { get; set; }

        public string JobStatusColorClass { get; set; }

        public decimal? BalOwing { get; set; }

        public decimal? TotalCost { get; set; }

        public string TransportName { get; set; }

        public string TrackingNumber { get; set; }

        public string CurruntLeadOwner { get; set; }

        public string NextFollowup { get; set; }

        public string Discription { get; set; }

        public string Comment { get; set; }

        public bool? SmsSend { get; set; }

        public DateTime? SmsSendDate { get; set; }

        public bool? EmailSend { get; set; }

        public DateTime? EmailSendDate { get; set; }

        public FreebiesSummaryCountDto Summary { get; set; }

        public int? PandingDays { get; set; }
    }

    public class FreebiesSummaryCountDto
    {
        public int Total { get; set; }

        public int Awaiting { get; set; }

        public int Pending { get; set; }

        public int Sent { get; set; }
    }
}
