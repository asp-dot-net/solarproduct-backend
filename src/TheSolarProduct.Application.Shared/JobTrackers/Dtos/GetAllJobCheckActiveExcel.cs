﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetAllJobCheckActiveExcel
    {
        public string JobNumber { get; set; }

        public string DepositeReceived { get; set; }

        public string NMINumber { get; set; }

        public string ApprovedReferenceNumber { get; set; }

        public string DistApproved { get; set; }

        public string MeterUpgrade { get; set; }

        public string MeterBoxPhoto { get; set; }

        public string SignedQuote { get; set; }

        public string Deposit { get; set; }

        public string ElecRetailer { get; set; }

        public string PeakMeterNo { get; set; }

        public string CurruntLeadOwner { get; set; }

        public string State { get; set; }

        public string PaymentOption { get; set; }

        public string SystemCapacity { get; set; }

        public string ActivityComment { get; set; }

        public string Notify { get; set; }

        public DateTime? DistApplied { get; set; }

        public DateTime? DistApprovedDate { get; set; }

        public DateTime? DistExpire { get; set; }

        public int? NotDepRecYet { get; set; }

        public int? NotActiveYet { get; set; }

        public int? NotBookedYet { get; set; }
    }
}
