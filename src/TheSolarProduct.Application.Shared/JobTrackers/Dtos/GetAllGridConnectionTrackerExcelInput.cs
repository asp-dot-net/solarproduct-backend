﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetAllGridConnectionTrackerExcelInput
    {
        public string FilterName { get; set; }

        public string Filter { get; set; }

        public int OrganizationUnit { get; set; }

        public int ApplicationStatus { get; set; }

        public string State { get; set; }

        public int ElecDistributorId { get; set; }

        public List<int> JobStatusIDFilter { get; set; }

        public int JobTypeId { get; set; }

        public int InstallerId { get; set; }

        public string DateType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string NMINumber { get; set; }

        public int ExcelOrCsv { get; set; }
    }
}
