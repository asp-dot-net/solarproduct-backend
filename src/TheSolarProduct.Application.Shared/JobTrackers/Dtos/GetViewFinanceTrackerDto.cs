﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetViewFinanceTrackerDto
    {
        public int Id { get; set; }

        public int LeadId { get; set; }

        public string JobNumber { get; set; }

        public string FinancePurchaseNo { get; set; }

        public int FinanceDocumentVerified { get; set; }

        public string JobStatus { get; set; }

        public string JobStatusColorClass { get; set; }

        public string CompanyName { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string Suburb { get; set; }
        
        public string State { get; set; }

        public string PostalCode { get; set; }

        public bool? FinanceSmsSend { get; set; }

        public DateTime? FinanceSmsSendDate { get; set; }
        
        public bool? FinanceEmailSend { get; set; }

        public DateTime? FinanceEmailSendDate { get; set; }
        
        public string CurrentLeadOwaner { get; set; }

        public string ActivityReminderTime { get; set; }

        public string ActivityDescription { get; set; }

        public string ActivityComment { get; set; }

        public FinanceSummaryCountDto Summary { get; set; }

        public int? NotVerifiedDays { get; set; }
    }

    public class FinanceSummaryCountDto
    {
        public int Total { get; set; }

        public int Verified { get; set; }

        public int NotVerified { get; set; }

        public int Query { get; set; }

        public int Awaiting { get; set; }
    }
}
