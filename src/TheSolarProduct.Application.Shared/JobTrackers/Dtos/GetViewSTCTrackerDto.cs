﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobTrackers.Dtos
{
    public class GetViewSTCTrackerDto
    {
        public int Id { get; set; }

        public int LeadId { get; set; }

        public string JobNumber { get; set; }

        public string JobTypeName { get; set; }

        public string StreetNo { get; set; }

        public string StreetName { get; set; }

        public string StreetType { get; set; }

        public string Address { get; set; }

        public string Suburb { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public DateTime? InstallationDate { get; set; }

        public DateTime? STCAppliedDate { get; set; }

        public string PvdNumber { get; set; }

        public string PvdStatusName { get; set; }

        public string JobStatusName { get; set; }

        public string JobStatusColorClass { get; set; }

        public decimal? SystemCapacity { get; set; }

        public decimal? STC { get; set; }

        public decimal? STCTotalPrice { get; set; }

        public decimal? GreenbotSTC { get; set; }

        public string InstallerName { get; set; }

        public string STCNotes { get; set; }

        public bool? STCSmsSend { get; set; }

        public DateTime? STCSmsSendDate { get; set; }

        public bool? STCEmailSend { get; set; }

        public DateTime? STCEmailSendDate { get; set; }

        public string CurrentLeadOwaner { get; set; }

        public string ActivityReminderTime { get; set; }

        public string ActivityDescription { get; set; }

        public string ActivityComment { get; set; }

        public string LastUpdatedDate { get; set; }

        public string RECPvdNumber { get; set; }

        public string RECPvdStatus { get; set; }

        public DateTime? LastRECUpdated { get; set; }
        
        public List<STCSummaryCountDto> Summary { get; set; }

        public int? NotAppliedDays { get; set; }

        public int? NotApprovedDays { get; set; }

        public string ActivityCommentDate { get; set; }

    }

    public class STCSummaryCountDto
    {
        public string STC { get; set; }

        public decimal? TotalSTCAmount { get; set; }

        public int? TotalJob { get; set; }
    }
}