﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.EcommerceProducctSpecification.Dtos
{
    public class EcommerceProductItemSpecificationDto : EntityDto<int?>
    {

        public int? SpecificationId { get; set; }

        public int? EcommerceProductItemId { get; set; }

        public string Value { get; set; }

        public string Name { get; set; }

    }
}
