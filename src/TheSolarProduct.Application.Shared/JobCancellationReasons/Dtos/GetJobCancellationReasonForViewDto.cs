﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobCancellationReasons.Dtos
{
   public class GetJobCancellationReasonForViewDto
    {
        public virtual JobCancellationReasonDto JobCancellationReason { get; set; }
    }
}
