﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobCancellationReasons.Dtos
{
    public class JobCancellationReasonDto : EntityDto
    {
        public string Name { get; set; }
        public Boolean IsActive { get; set; }
    }
}
