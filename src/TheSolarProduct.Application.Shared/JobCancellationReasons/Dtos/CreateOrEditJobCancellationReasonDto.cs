﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.JobCancellationReasons.Dtos
{
    public class CreateOrEditJobCancellationReasonDto : EntityDto<int?>
    {
        [Required]
        public string Name { get; set; }
        public Boolean IsActive { get; set; }

    }
}
