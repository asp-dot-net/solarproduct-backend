﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobCancellationReasons.Dtos
{
    public class GetJobCancellationReasonforEditOutput
    {
        public CreateOrEditJobCancellationReasonDto JobCancellationReasonDto { get; set; }
    }
}
