﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.JobCancellationReasons.Dtos
{
    public class GetAllJobCancellationReasonInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
