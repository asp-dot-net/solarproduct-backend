﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.JobCancellationReasons.Dtos;

namespace TheSolarProduct.JobCancellationReasons
{
    public interface IJobCancellationReasonAppService : IApplicationService
    {
        Task<PagedResultDto<GetJobCancellationReasonForViewDto>> GetAll(GetAllJobCancellationReasonInput input);

        Task<GetJobCancellationReasonForViewDto> GetJobCancellationReasonForView(int id);

        Task<GetJobCancellationReasonforEditOutput> GetJobCancellationReasonForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditJobCancellationReasonDto input);

        Task Delete(EntityDto input);
    }
}
