﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
	public class LeadStatusChangeDto : EntityDto
	{
		public string CompanyName { get; set; }

		public int? LeadStatusID { get; set; }
	}
}
