﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
    public class SmsReplyInputDto : PagedAndSortedResultRequestDto
    {

        public string FilterName { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? OrganizationUnit { get; set; }
        public int? Markstatusid { get; set; }
        public string Filter { get; set; }

        public string DateNameFilter { get; set; }
    }
}
