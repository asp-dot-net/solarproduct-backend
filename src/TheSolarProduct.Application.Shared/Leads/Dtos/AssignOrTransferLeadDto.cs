﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
	public class AssignOrTransferLeadDto : EntityDto
	{
		public virtual int? LeadStatusID { get; set; }

		public virtual int? AssignToUserID { get; set; }
	}
}
