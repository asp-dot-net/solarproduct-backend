﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Abp;

namespace TheSolarProduct.Leads.Dtos
{
	public class CreateOrEditLeadDto : EntityDto<int?>
	{

		//[Required]
		[StringLength(LeadConsts.MaxCopanyNameLength, MinimumLength = LeadConsts.MinCopanyNameLength)]
		public string CompanyName { get; set; }

		//[Required]
		[RegularExpression(LeadConsts.EmailRegex)]
		[StringLength(LeadConsts.MaxEmailLength, MinimumLength = LeadConsts.MinEmailLength)]
		public string Email { get; set; }

		//[RegularExpression(LeadConsts.PhoneRegex)]
		public string Phone { get; set; }

		[RegularExpression(LeadConsts.MobileRegex)]
		[StringLength(LeadConsts.MaxMobileLength, MinimumLength = LeadConsts.MinMobileLength)]
		public string Mobile { get; set; }


		public string Address { get; set; }

		public string ExcelAddress { get; set; }

		public string Requirements { get; set; }

		public string Suburb { get; set; }

		public string State { get; set; }

		public string PostCode { get; set; }

		public string LeadSource { get; set; }

		public string UnitNo { get; set; }

		public string UnitType { get; set; }

		public string StreetNo { get; set; }

		public string StreetName { get; set; }

		public string StreetType { get; set; }

		public int? LeadStatusID { get; set; }

		public string LeadStatusName { get; set; }

		public int? AssignToUserID { get; set; }

		public string latitude { get; set; }

		public string longitude { get; set; }

		public bool? IsDuplicate { get; set; }


		//[RegularExpression(LeadConsts.PhoneRegex)]
		public string AltPhone { get; set; }

		public string Type { get; set; }

		public string SolarType { get; set; }

		//public bool? IsSameAddress { get; set; }

		//public string PostalAddress { get; set; }

		//public string PostalUnitNo { get; set; }

		////[StringLength(LeadConsts.MaxUnitTypeLength, MinimumLength = LeadConsts.MinUnitTypeLength)]

		//public string PostalUnitType { get; set; }

		////[Required]
		//public string PostalStreetNo { get; set; }

		////[Required]
		////[StringLength(LeadConsts.MaxStreetNameLength, MinimumLength = LeadConsts.MinStreetNameLength)]
		//public string PostalStreetName { get; set; }

		////[Required]
		////[StringLength(LeadConsts.MaxStreetTypeLength, MinimumLength = LeadConsts.MinStreetTypeLength)]
		//public string PostalStreetType { get; set; }

		////[Required]
		//public string PostalSuburb { get; set; }

		////[Required]
		//public string PostalState { get; set; }

		////[Required]
		//public string PostalPostCode { get; set; }

		public string Area { get; set; }

		public string Country { get; set; }

		public string ABN { get; set; }

		public string Fax { get; set; }

		public string SystemType { get; set; }

		public string RoofType { get; set; }

		public string AngleType { get; set; }

		public string StoryType { get; set; }

		public string HouseAgeType { get; set; }

		public int? LeadSourceId { get; set; }

		public int? SuburbId { get; set; }

		public int StateId { get; set; }

		//public int? PostalStateId { get; set; }

		//public int? PostalSuburbId { get; set; }

		public string from { get; set; }

		//public string Postallatitude { get; set; }

		//public string Postallongitude { get; set; }

		public string IsGoogle { get; set; }

		//public string PostalIsGoogle { get; set; }

		public virtual int IsExternalLead { get; set; }

		public virtual int OrganizationId { get; set; }

		public virtual int CreatorUserId { get; set; }

		public string ReferralName { get; set; }
		
		public virtual int? ReferralLeadId { get; set; }

		public int? SectionId { get; set; }

        public bool IsJobAddresschange { get; set; }


    }
}