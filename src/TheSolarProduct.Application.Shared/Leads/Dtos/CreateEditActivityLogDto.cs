﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
    public class CreateEditActivityLogDto : EntityDto<int?>
    {
		public int TenantId { get; set; }

		public  int LeadId { get; set; }

		public  int ActionId { get; set; }
	
		public  string ActionNote { get; set; }

		public  DateTime? ActivityDate { get; set; }

		public  string Body { get; set; }

		public  string ActivityNote { get; set; }

		public  string MessageId { get; set; }

		public  int? ReferanceId { get; set; }

		public  bool? IsMark { get; set; }

		public  int? TemplateId { get; set; }

		public int? UserId { get; set; }
	}
  
}
