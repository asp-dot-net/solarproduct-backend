﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
	public class CheckExistLeadDto : EntityDto<int?>
	{
		public string Email { get; set; }

		public string Mobile { get; set; }

		public string UnitNo { get; set; }
		
		public string UnitType { get; set; }
		
		public string StreetNo { get; set; }
		
		public string StreetName { get; set; }
		
		public string StreetType { get; set; }		
		
		public string Suburb { get; set; }
		
		public string State { get; set; }
		
		public string PostCode { get; set; }

		public int OrganizationId { get; set; }
	}
}
