﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
    public class GetLeadForChangeStatusOutput : EntityDto
	{
		public string CompanyName { get; set; }

		public int? LeadStatusID { get; set; }

		public string StatusName { get; set; }

		public string RejectReason { get; set; }

		public int? RejectReasonId { get; set; }

		public int? CancelReasonId { get; set; }

		public string Section { get; set; }
	}
}
