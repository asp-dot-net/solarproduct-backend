﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads
{
	public class GetActivityLogInput
	{
		public int LeadId { get; set; }

		public int? ActionId { get; set; }
		public int? SectionId { get; set; }
		public bool? CurrentPage { get; set; }
		public bool? OnlyMy { get; set; }
		public int MyInstallerId { get; set; }

        public bool AllActivity { get; set; }

		public int? ServiceId { get; set; }
    }
}
