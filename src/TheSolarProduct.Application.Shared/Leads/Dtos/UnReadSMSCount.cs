﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
	public class UnReadSMSCount
	{
		public int Total { get; set; }
		public int Promotion { get; set; }
		public int Reply { get; set; }
	}
}
