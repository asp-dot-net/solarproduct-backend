﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Leads.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}