﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using TheSolarProduct.DemoUiComponents.Dto;

namespace TheSolarProduct.Leads.Dtos
{
	public class ActivityLogInput : EntityDto
	{
		public int TenantId { get; set; }

		public int LeadId { get; set; }

		public int ActionId { get; set; }

		public int? EmailTemplateId { get; set; }

		public int? SMSTemplateId { get; set; }
		
	    public int? CustomeTagsId { get; set; }

		public string ActionNote { get; set; }

		public DateTime? ActivityDate { get; set; }

		public string Body { get; set; }

		public string ActivityNote { get; set; }

		public int? TrackerId { get; set; }
		public string Subject { get; set; }
		public int? UserId { get; set; }
		public List<UploadFileOutput> AttachmentList { get; set; }

		public int? ReferanceId { get; set; }
		public int? SectionId { get; set; }
		public   int? TodopriorityId { get; set; }
		public   string Todopriority { get; set; }
		public   string EmailFrom { get; set; }
		public int MyInstallerId { get; set; }
		public string CreatedUser { get; set; }

		public int? ServiceId { get; set; }	
	}
}
