﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Leads.Dtos
{
    public class LeadStateLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}