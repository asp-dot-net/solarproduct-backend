﻿using System;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
    public class LeadExpenseReportExportDtos
    {
        public string Leadsource { get; set; }

        public decimal Totaltotallead{ get; set; }
        
        public decimal TotaltotalCost{ get; set; }
        
        public decimal TotalCostLead{ get; set; }
        
        public decimal TotalKwSold{ get; set; }
        
        public decimal TotalProjectOpen{ get; set; }
        
        public decimal TotalperKwSold{ get; set; }
        
        public decimal TotalProjectOpenpercent{ get; set; }
        
        public decimal TotalDeprecCount{ get; set; }
        
        public decimal TotalDeprecKWSold{ get; set; }
        
        public decimal TotalPannelsold{ get; set; }
        
        public decimal TotalCostpannel{ get; set; }
        
        public decimal NSWtotallead{ get; set; }
        
        public decimal NSWtotalCost{ get; set; }
        
        public decimal NSWCostLead{ get; set; }
        
        public decimal NSWKwSold{ get; set; }
        
        public decimal NSWProjectOpen{ get; set; }
        
        public decimal NSWperKwSold{ get; set; }
        
        public decimal NSWProjectOpenpercent{ get; set; }
        
        public decimal NSWDeprecCount{ get; set; }
        
        public decimal NSWDeprecKWSold{ get; set; }
        
        public decimal NSWPannelsold{ get; set; }
        
        public decimal NSWCostpannel{ get; set; }
        
        public decimal QLDtotallead{ get; set; }
        
        public decimal QLDtotalCost{ get; set; }
        
        public decimal QLDCostLead{ get; set; }
        
        public decimal QLDKwSold{ get; set; }

        public decimal QLDProjectOpen{ get; set; }
        
        public decimal QLDperKwSold{ get; set; }
        
        public decimal QLDProjectOpenpercent{ get; set; }
        
        public decimal QLDDeprecCount{ get; set; }
        
        public decimal QLDDeprecKWSold{ get; set; }
        
        public decimal QLDPannelsold{ get; set; }
        
        public decimal QLDCostpannel{ get; set; }
        
        public decimal SAtotallead{ get; set; }
        
        public decimal SAtotalCost{ get; set; }
        
        public decimal SACostLead{ get; set; }
        
        public decimal SAKwSold{ get; set; }

        public decimal SAProjectOpen{ get; set; }
        
        public decimal SAperKwSold{ get; set; }
        
        public decimal SAProjectOpenpercent{ get; set; }
        
        public decimal SADeprecCount{ get; set; }
        
        public decimal SADeprecKWSold{ get; set; }
        
        public decimal SAPannelsold{ get; set; }
        
        public decimal SACostpannel{ get; set; }
        
        public decimal WAtotallead{ get; set; }
        
        public decimal WAtotalCost{ get; set; }
        
        public decimal WACostLead{ get; set; }
        
        public decimal WAKwSold{ get; set; }

        public decimal WAProjectOpen{ get; set; }
        
        public decimal WAperKwSold{ get; set; }
        
        public decimal WAProjectOpenpercent{ get; set; }
        
        public decimal WADeprecCount{ get; set; }
        
        public decimal WADeprecKWSold{ get; set; }
        
        public decimal WAPannelsold{ get; set; }
        
        public decimal WACostpannel{ get; set; }
        
        public decimal VICtotallead{ get; set; }
        
        public decimal VICtotalCost{ get; set; }
        
        public decimal VICCostLead{ get; set; }
        
        public decimal VICKwSold{ get; set; }

        public decimal VICProjectOpen{ get; set; }
        
        public decimal VICperKwSold{ get; set; }
        
        public decimal VICProjectOpenpercent{ get; set; }
        
        public decimal VICDeprecCount{ get; set; }
        
        public decimal VICDeprecKWSold{ get; set; }
        
        public decimal VICPannelsold{ get; set; }
        
        public decimal VICCostpannel{ get; set; }

    }
}
