﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
   public class StateForLeadExpenseDto
    { 
        public int stateid { get; set; }
        public string statename { get; set; }

        public decimal Amount { get; set; }

        public int leadexpenseid { get; set; }
    }
}
