﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
	public class LeadStatusLookupTableDto
	{
		public int Value { get; set; }

		public string Name { get; set; }
	}
}
