﻿using Abp;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
namespace TheSolarProduct.Leads.Dtos
{
    public class GetAllLeadsInputForPromotion : PagedAndSortedResultRequestDto
	{
		public int? LeadStatusIdFilter { get; set; }

		public int? LeadSourceIdFilter { get; set; }

		public DateTime? StartDateFilter { get; set; }

		public DateTime? EndDateFilter { get; set; }

		public int? StateIdFilter { get; set; }

		public int? TeamIdFilter { get; set; }



        //public List<NameValue<int>> LeadStatusIdsFilter { get; set; }

        //public List<NameValue<int>> LeadSourceIdsFilter { get; set; }

        //public List<NameValue<int>> StateIdsFilter { get; set; }

        //public List<NameValue<int>> TeamIdsFilter { get; set; }
        //public List<NameValue<int>> JobStatusIdsFilter { get; set; }

        public List<int?> LeadStatusIdsFilter { get; set; }

        public List<int?> LeadSourceIdsFilter { get; set; }

        public List<int?> StateIdsFilter { get; set; }

        public List<int?> TeamIdsFilter { get; set; }
        public List<int?> JobStatusIdsFilter { get; set; }
        public string AreaNameFilter { get; set; }
		public string TypeNameFilter { get; set; }
		public int? Orgaid { get; set; }

        public string DateType { get; set; }

        public List<string> AreasFilter { get; set; }

	}
}

