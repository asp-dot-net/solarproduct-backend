﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
    public class LeadExcelExportTto
    {
		public string FilterName { get; set; }

		public string Filter { get; set; }

		public string CopanyNameFilter { get; set; }

		public string EmailFilter { get; set; }

		public string PhoneFilter { get; set; }

		public string MobileFilter { get; set; }

		public string AddressFilter { get; set; }

		public string RequirementsFilter { get; set; }

		public string PostCodeSuburbFilter { get; set; }

		public string StateNameFilter { get; set; }

		public string StreetNameFilter { get; set; }

		public string PostCodeFilter { get; set; }

		public string LeadSourceNameFilter { get; set; }

		public List<int> LeadSourceIdFilter { get; set; }

		public string LeadStatusName { get; set; }

		public string TypeNameFilter { get; set; }

		public string AreaNameFilter { get; set; }

		public int? LeadStatusId { get; set; }

		public int? AssignToUserId { get; set; }

		public DateTime? StartDate { get; set; }

		public DateTime? EndDate { get; set; }

		public int? OrganizationUnit { get; set; }

		public int? TeamId { get; set; }

		public int? SalesManagerId { get; set; }

		public int? SalesRepId { get; set; }

		public string DateFilterType { get; set; }

		public string DuplicateFilter { get; set; }

		public bool OnlyAssignLead { get; set; }

		public int? jobStatusIDFilter { get; set; }

		public int? CancelReasonId { get; set; }

		public int? RejectReasonId { get; set; }
		public int? excelorcsv { get; set; }
		public List<int> JobStatusID { get; set; }
		public List<int> LeadStatusIDS { get; set; }
		public int? Cancelrequestfilter { get; set; }
	}
}
