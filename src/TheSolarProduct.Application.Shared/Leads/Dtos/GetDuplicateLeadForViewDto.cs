﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
	public class GetDuplicateLeadForViewDto
	{
		public DuplicateLeadDto DuplicateLead { get; set; }

		public List<DuplicateDetailDto> DuplicateDetail { get; set; }

		public string PostCodeSuburb { get; set; }

		public string StateName { get; set; }

		public string LeadSourceName { get; set; }

		public string LeadStatusName { get; set; }

		public bool WebDuplicate { get; set; }

		public bool Duplicate { get; set; }

		public string CurrentAssignUserName { get; set; }

		public string CreatedByName { get; set; }

		public DateTime? CreatedOn { get; set; }

		public int? WebDuplicateCount { get; set; }

		public int? DuplicateCount { get; set; }

		public string Facebook { get; set; }

		public string Online { get; set; }

		public string Others { get; set; }

		public string Total { get; set; }
		public string CurrentLeadOwner { get; set; }

		public bool IsFakeLead { get; set; }

		public bool IsAssigned { get; set;}

		public bool IsDeleted { get; set;}

        public string Process { get; set; }
    }
}
