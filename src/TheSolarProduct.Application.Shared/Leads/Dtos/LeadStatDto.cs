﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
    public class LeadStatDto
    {
        public int TotalLeads { get; set; }
        public int New { get; set; }
        public int UnHandledLeads { get; set; }
        public int Upgrade { get; set; }
        public int Canceled { get; set; }
        public int Cold { get; set; }
        public int Warm { get; set; }
        public int Hot { get; set; }
        public int Closed { get; set; }
        public int Rejected { get; set; }

    }
}
