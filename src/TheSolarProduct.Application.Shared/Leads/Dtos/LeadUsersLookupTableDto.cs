﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
	public class LeadUsersLookupTableDto
	{
		public long Id { get; set; }

		public string DisplayName { get; set; }
	}
}
