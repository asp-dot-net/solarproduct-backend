﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
	public class GetLeadCountsDto
	{
		public string UnHandled { get; set; }

		public string Assigned { get; set; }

		public string New { get; set; }

		public string Hot { get; set; }

		public string Cold { get; set; }

		public string Warm { get; set; }

		public string Upgrade { get; set; }

		public string Rejected { get; set; }

		public string Cancelled { get; set; }

		public string Closed { get; set; }

		public string Total { get; set; }
	}
}
