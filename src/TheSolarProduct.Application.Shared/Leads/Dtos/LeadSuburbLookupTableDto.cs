﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
	public class LeadSuburbLookupTableDto
	{
		public int Id { get; set; }

		public string DisplayName { get; set; }
	}
}
