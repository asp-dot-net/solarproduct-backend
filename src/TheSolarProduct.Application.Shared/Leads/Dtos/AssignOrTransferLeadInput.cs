﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
    public class AssignOrTransferLeadInput
    {
        public int? OrganizationID { get; set; }

        public int AssignToUserID { get; set; }

        public int LeadActionId { get; set; }

        public int SectionId { get; set; }

        public List<int> LeadIds { get; set; }

        public string Section { get; set; }
    }
}
