﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
	public class GetLeadForAssignOrTransferOutput:EntityDto<int?>
	{
		public string CompanyName { get; set; }

		public int? LeadStatusID { get; set; }

		public int? OrganizationID { get; set; }

		public List<int> LeadIds { get; set; }

		public int? AssignToUserID { get; set; }

		public string UserName { get; set; }

		public int? SectionId { get; set; }

        public string Section { get; set; }

    }
}
