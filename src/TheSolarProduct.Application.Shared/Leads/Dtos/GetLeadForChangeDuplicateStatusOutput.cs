﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
	public class GetLeadForChangeDuplicateStatusOutput : EntityDto<int?>
	{
		public int?[] LeadId { get; set; }

		public bool? IsDuplicate { get; set; }

		public bool? IsWebDuplicate { get; set; }

		public bool? HideDublicate { get; set; }
		public int? SectionId { get; set; }
    }
}
