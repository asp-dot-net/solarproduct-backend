﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
	public class LeadStatePostcodeLookupTable
	{
		public int State { get; set; }

		public string Postcode { get; set; }
	}
}
