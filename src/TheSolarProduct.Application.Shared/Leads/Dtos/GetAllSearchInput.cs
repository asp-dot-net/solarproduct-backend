﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
    public class GetAllSearchInput : PagedAndSortedResultRequestDto
    {
        public int LeadId { get; set; }

        public string Filter { get; set; }
    }
}
