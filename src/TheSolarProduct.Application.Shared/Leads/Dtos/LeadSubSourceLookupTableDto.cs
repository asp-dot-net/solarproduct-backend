﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.Leads.Dtos
{
    public class LeadSubSourceLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}