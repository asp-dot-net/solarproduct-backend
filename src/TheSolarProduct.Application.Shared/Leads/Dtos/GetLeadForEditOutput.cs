﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.Leads.Dtos
{
    public class GetLeadForEditOutput
	{
		public CreateOrEditLeadDto Lead { get; set; }

		public string PostCodeSuburb { get; set;}

		public string StateName { get; set;}

		public string PostCodePostalCode2 { get; set;}

		public string LeadSourceName { get; set;}

		public string LeadStatusName { get; set; }

		public int CurrentUserId { get; set; }
		public bool? OwnTeamUser { get; set; }

		public int? JobId { get; set; }
	}
}