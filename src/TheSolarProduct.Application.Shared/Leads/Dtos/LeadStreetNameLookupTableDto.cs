﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
	public class LeadStreetNameLookupTableDto
	{
		public int Id { get; set; }

		public string DisplayName { get; set; }
	}
}
