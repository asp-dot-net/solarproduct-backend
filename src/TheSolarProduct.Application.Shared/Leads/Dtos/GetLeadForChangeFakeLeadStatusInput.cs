﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
    public class GetLeadForChangeFakeLeadStatusInput : EntityDto<int?>
    {
        public int?[] LeadId { get; set; }

        public bool? IsFakeLead { get; set; }

        public int? SectionId { get; set; }
    }
}
