﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
   public class LeadExpenseReportDto
    {
		public int? TenantId { get; set; }

		public int? OrganizationId { get; set; }
		
		public virtual decimal TotalAmount { get; set; }
		public virtual string Leadsource { get; set; }

		public virtual int? LeadSourceId { get; set; }
		public List<StateForLeadExpenseReportDto> leadstates { get; set; }
		public decimal coltotallead { get; set; }
		public decimal coltotalCost { get; set; }
		public decimal colCostLead { get; set; }
		public decimal colKwSold { get; set; }

		public decimal colperKwCost { get; set; }
		public decimal colProjectOpen { get; set; }

		public decimal colProjectOpenpercent { get; set; }
		public decimal colDeprecCount { get; set; }
		public decimal colDeprecKWSold { get; set; }
		public decimal colPannelsold { get; set; }
		public decimal colCostpannel { get; set; }


        public decimal? nswtotallead { get; set; }
        public decimal nswtotalCost { get; set; }
        public decimal nswwCostLead { get; set; }
        public decimal rownswKwSold { get; set; }
        public decimal nswcolCostLead { get; set; }
        public decimal nswcolKwSold { get; set; }
        public decimal nswcolProjectOpen { get; set; }

        public decimal nswcolProjectOpenpercent { get; set; }
        public decimal nswcolDeprecCount { get; set; }
        public decimal nswcolDeprecKWSold { get; set; }
        public decimal nswcolPannelsold { get; set; }
        public decimal nswcolCostpannel { get; set; }

        public decimal? qldtotallead { get; set; }
        public decimal qldtotalCost { get; set; }
        public decimal qldcolCostLead { get; set; }
        public decimal qldcolKwSold { get; set; }

        public decimal qldcolperKwCost { get; set; }
        public decimal qldcolProjectOpen { get; set; }

        public decimal qldcolProjectOpenpercent { get; set; }
        public decimal qldcolDeprecCount { get; set; }
        public decimal qldcolDeprecKWSold { get; set; }
        public decimal qldcolPannelsold { get; set; }
        public decimal qldcolCostpannel { get; set; }

        public decimal? satotallead { get; set; }
        public decimal satotalCost { get; set; }
        public decimal sacolCostLead { get; set; }
        public decimal sacolKwSold { get; set; }

        public decimal sacolperKwCost { get; set; }
        public decimal sacolProjectOpen { get; set; }

        public decimal sacolProjectOpenpercent { get; set; }
        public decimal sacolDeprecCount { get; set; }
        public decimal sacolDeprecKWSold { get; set; }
        public decimal sacolPannelsold { get; set; }
        public decimal sacolCostpannel { get; set; }

        public decimal? watotallead { get; set; }
        public decimal watotalCost { get; set; }
        public decimal wacolCostLead { get; set; }
        public decimal wacolKwSold { get; set; }

        public decimal wacolperKwCost { get; set; }
        public decimal wacolProjectOpen { get; set; }

        public decimal wacolProjectOpenpercent { get; set; }
        public decimal wacolDeprecCount { get; set; }
        public decimal wacolDeprecKWSold { get; set; }
        public decimal wacolPannelsold { get; set; }
        public decimal wacolCostpannel { get; set; }
    }

	public class StateForLeadExpenseReportDto 
	{
		public decimal totallead { get; set; }
		public string statename { get; set; }

		public decimal totalCost { get; set; }
		public decimal CostLead { get; set; }
		public int leadexpenseid { get; set; }
		public decimal  KwSold { get; set; }
		public decimal  perKwCost { get; set; }
		public decimal  ProjectOpen { get; set; }
		public decimal ProjectOpenpercent { get; set; }
		public decimal DeprecCount { get; set; }

		public int DeprecPercent { get; set; }
		public decimal DeprecKWSold { get; set; }

		public int Pannelsold { get; set; }

		public decimal Costpannel { get; set; }
		public decimal PanelSoldperpanbelCost { get; set; }

        public decimal InstallKwSold { get; set; }
        public decimal perInstallKwCost { get; set; }

    }

    public class GetAllexpenseInput 
    {

        public string DateType { get; set; }
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        public List<int> states { get; set; }
        public List<int> leadSources { get; set; }

		public int? organizationId { get; set; }

        public string areaFilter { get; set; }
        

        public bool WithBattery { get; set;}
    }
}
