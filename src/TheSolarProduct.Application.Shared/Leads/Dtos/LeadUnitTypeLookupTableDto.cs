﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
	public class LeadUnitTypeLookupTableDto
	{
		public int Id { get; set; }

		public string DisplayName { get; set; }
	}
}
