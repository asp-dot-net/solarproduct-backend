﻿
using System;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;

namespace TheSolarProduct.Leads.Dtos
{
	public class LeadDto : FullAuditedEntity
	{
		public string CompanyName { get; set; }

		public string Email { get; set; }

		public string Phone { get; set; }

		public string Mobile { get; set; }

		public string Address { get; set; }

		public string Requirements { get; set; }

		public string Suburb { get; set; }

		public string State { get; set; }

		public string PostCode { get; set; }

		public string LeadSource { get; set; }

		public string UnitNo { get; set; }

		public string UnitType { get; set; }

		public string StreetNo { get; set; }

		public string StreetName { get; set; }

		public string StreetType { get; set; }

		public int? LeadStatusID { get; set; }

		public int? AssignToUserID { get; set; }

		public bool? IsDuplicate { get; set; }

		public string AltPhone { get; set; }

		public string Type { get; set; }

		public string SolarType { get; set; }

		//public bool? IsSameAddress { get; set; }

		//public string PostalAddress { get; set; }

		//public string PostalUnitNo { get; set; }

		//public string PostalUnitType { get; set; }

		//public string PostalStreetNo { get; set; }

		//public string PostalStreetName { get; set; }

		//public string PostalStreetType { get; set; }

		//public string PostalSuburb { get; set; }

		//public string PostalState { get; set; }

		//public string PostalPostCode { get; set; }

		public string Area { get; set; }

		public string Country { get; set; }

		public string ABN { get; set; }

		public string Fax { get; set; }

		public string SystemType { get; set; }

		public string RoofType { get; set; }

		public string AngleType { get; set; }

		public string StoryType { get; set; }

		public string HouseAgeType { get; set; }

		public int? LeadSourceId { get; set; }

		public int? SuburbId { get; set; }

		public int StateId { get; set; }

		public int? PostalStateId { get; set; }

		public int? PostalSuburbId { get; set; }

		public string RejectReason { get; set; }

		public int? RejectReasonId { get; set; }

		public int? CancelReasonId { get; set; }

		public int IsExternalLead { get; set; }

		public int OrganizationId { get; set; }

		public string OrganizationName { get; set; }

		public DateTime? FollowupDate { get; set; }

		public int? ActionId { get; set; }

		public string latitude { get; set; }
		public string IsGoogle { get; set; }
		public string longitude { get; set; }

		public string CurrentLeadOwner { get; set; }

		public DateTime? ChangeStatusDate { get; set; }

		public DateTime? LeadAssignDate { get; set; }

		public string JobNumber { get; set; }

		public string JobStatus { get; set; }

		public DateTime? LeadreationDate { get; set; }

		public string FName { get; set; }

        public string GCLId { get; set; }
        public DateTime? ReAssignDate { get; set; }
    }
}