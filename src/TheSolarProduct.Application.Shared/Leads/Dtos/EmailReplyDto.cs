﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
    public class EmailReplyDto
    {
	

		public  int? LeadId { get; set; }
		public  int? ActionId { get; set; }
		public string Email { get; set; }
		public  string ActionNote { get; set; }

		public  DateTime? ActivityDate { get; set; }

		public  string Body { get; set; }

		public  string ActivityNote { get; set; }

		public DateTime? CreationTime { get; set; }

		public string LeadCompanyName { get; set; }
		public string JobNumber { get; set; }
		public int Id { get; set; }
		public string TemplateName { get; set; }
		public string UserName { get; set; }
		public string Subject { get; set; }
		public int? TemplateId { get; set; }

	}
}
