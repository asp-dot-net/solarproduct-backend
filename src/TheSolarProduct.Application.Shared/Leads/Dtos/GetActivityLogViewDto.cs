﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads
{
	public class GetActivityLogViewDto : FullAuditedEntity
	{
		public int LeadId { get; set; }

		public string CreatorUserName { get; set; }		

		public string LeadCompanyName { get; set; }

		public int ActionId { get; set; }
        
        public string ActionName { get; set; }
	
		public string ActionNote { get; set; }

		public string LogDate { get; set; }

		public string ActivityNote { get; set; }

		public string Body { get; set; }
		public int ShowLeadJobDetail { get; set; }
		public int? ShowJobLink { get; set; }
		public DateTime? ActivityDate { get; set; }
		public string ActivitysDate { get; set; }

		public string Section { get; set; }
		public string SmsResponce { get; set; }
		public string Sendsms { get; set; }

		public int? promotionId { get; set; }
}
}
