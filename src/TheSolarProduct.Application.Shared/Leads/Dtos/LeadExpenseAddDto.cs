﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
	public class LeadExpenseAddDto
	{
		public int? TenantId { get; set; }

		public int? OrganizationId { get; set; }
		public virtual DateTime FromDate { get; set; }

		public virtual DateTime ToDate { get; set; }

		public virtual decimal TotalAmount { get; set; }

		public virtual int? LeadSourceId { get; set; }

		public virtual string Leadsource { get; set; }
		public int? InvestmentId { get; set; }
		public List<StateForLeadExpenseDto> leadstates { get; set; }
	}
	public class TotalCountDto
	{ 
		public virtual decimal TotalAmount { get; set; }
		public virtual DateTime FromDate { get; set; }

		public virtual DateTime ToDate { get; set; }


        public decimal TotalNswAmount { get; set; }

        public decimal TotalQLDAmount { get; set; }

        public decimal TotalSAAmount { get; set; }

        public decimal TotalWAAmount { get; set; }

        public decimal TotalVICAmount { get; set; }
    }
		
}
