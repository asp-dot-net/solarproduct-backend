﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
    public class SmsReplyDto
    {
		public int? LeadId { get; set; }
		public int? ActionId { get; set; }
		public string Email { get; set; }
		public string ActionNote { get; set; }

		public DateTime? ActivityDate { get; set; }

		public string Body { get; set; }

		public string ActivityNote { get; set; }

		public DateTime? CreationTime { get; set; }

		public string LeadCompanyName { get; set; }
		public string JobNumber { get; set; }

		public int? ResponceActionId { get; set; }

		public string ResponceActionNote { get; set; }

		public DateTime? ResponceActivityDate { get; set; }

		public string ResponceBody { get; set; }

		public string ResponceActivityNote { get; set; }

		public DateTime? ResponceCreationTime { get; set; }
		public Boolean? IsMark { get; set; }

		public  int id { get; set; }

		public string UserName { get; set; }

		public string SectionName { get; set; }
		public int? TemplateId { get; set; }
		public bool? isSelected { get; set; }

	}
}
