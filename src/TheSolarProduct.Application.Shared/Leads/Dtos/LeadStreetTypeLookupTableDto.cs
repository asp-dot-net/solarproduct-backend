﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Dtos
{
	public class LeadStreetTypeLookupTableDto
	{
		public int Id { get; set; }

		public string DisplayName { get; set; }
	}
}
