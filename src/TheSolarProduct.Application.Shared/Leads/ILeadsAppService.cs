﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.Dto;
using System.Collections.Generic;
using Abp;
using TheSolarProduct.Jobs.Dtos;
using System.Data;

namespace TheSolarProduct.Leads
{
	public interface ILeadsAppService : IApplicationService
	{
		Task<PagedResultDto<GetLeadForViewDto>> GetAll(GetAllLeadsInput input);

		Task<PagedResultDto<GetLeadForViewDto>> GetLeadTrackerData(GetAllLeadsInput input);

        Task<FileDto> getLeadTrackerToExcel(LeadExcelExportTto input);
        //Task<LeadsSummaryCount> getLeadSummaryCount(GetAllLeadsInput input);

        Task<PagedResultDto<GetDuplicateLeadForViewDto>> GetAllDuplicateLead(GetAllLeadsInput input);

		Task<PagedResultDto<GetLeadForViewDto>> GetAllForMyLead(GetAllLeadsInput input);

		//Task<LeadsSummaryCount> getmyLeadSummaryCount(GetAllLeadsInput input);
		
		Task<PagedResultDto<GetLeadForViewDto>> GetAllForMyClosedLead(GetAllLeadsInput input);

		Task<PagedResultDto<GetLeadForViewDto>> GetAllForCancelLead(GetAllLeadsInput input);
		  Task<CancleLeadsSummaryCount> GetAllForCancelLeadCount(GetAllLeadsInput input);
		Task<PagedResultDto<GetLeadForViewDto>> GetAllForRejectedLead(GetAllLeadsInput input);
		Task<CancleLeadsSummaryCount> GetAllRejectedLeadCount(GetAllLeadsInput input);
		Task<GetLeadForViewDto> GetLeadForView(int id, int? JobPromoId);

		Task<List<GetDuplicateLeadPopupDto>> CheckExistLeadList(CheckExistLeadDto input);

		Task<List<GetDuplicateLeadPopupDto>> GetDuplicateLeadForView(int id, int orgId);

		Task<GetLeadForEditOutput> GetLeadForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditLeadDto input);
		Task CreateOrEditLead(CreateOrEditForAppLeadDto input);

		Task Delete(int[] input);

		Task DeleteLeads(EntityDto input);

		Task DeleteDuplicateLeads(EntityDto input);

		Task ChangeStatus(GetLeadForChangeStatusOutput input);

		Task<GetLeadForChangeStatusOutput> GetLeadForChangeStatus(EntityDto input);

		Task<FileDto> GetLeadsToExcel(GetAllLeadsForExcelInput input);
		Task<FileDto> getDublicateLeadsToExcel(DuplicateLeadExceltoExport input);
		Task<FileDto> getClosedLeadsToExcel(LeadExcelExportTto input);
		Task<FileDto> getCancelLeadsToExcel(LeadExcelExportTto input);

		Task<FileDto> getRejectLeadsToExcel(LeadExcelExportTto input);

		
		Task<PagedResultDto<GetLeadForViewDto>> GetSearchData(GetAllSearchInput input);
		Task<FileDto> getMyLeadToExcel(LeadExcelExportTto input);


		//Task<List<LeadUnitTypeLookupTableDto>> GetAllUnitTypeForUnitTypeTableDropdown();

		//Task<List<LeadStreetNameLookupTableDto>> GetAllStreetNameForStreetNameTableDropdown();

		//Task<List<LeadStreetTypeLookupTableDto>> GetAllStreetTypeForStreetTypeTableDropdown();		

		//Task<List<LeadSuburbLookupTableDto>> GetAllSuburbForPostCodeTableDropdown();

		//Task<List<LeadSuburbLookupTableDto>> GetAllPostCodeForTableDropdown();
		Task<List<LeadSourceLookupTableDto>> GetLeadSourceDrpdwnForLeadEditInsert();
		Task<List<LeadSourceLookupTableDto>> GetAllLeadSourceForTableDropdown();
		Task<List<LeadSourceLookupTableDto>> GetAllLeadSourceDropdown();
		
		Task<List<JobStatusTableDto>> GetAllJobStatusTableDropdown();

		Task<List<LeadStatusLookupTableDto>> GetAllLeadStatusForTableDropdown(string lead);

		NameValue<string> SendAndGetSelectedValue(NameValue<string> selectedCountries);

		int GetSuburbId(string PostCode);

		int StateId(string StateName);

		Task<GetLeadForAssignOrTransferOutput> GetLeadForAssignOrTransfer(EntityDto input);

		Task<List<LeadUsersLookupTableDto>> GetAllUserLeadAssignDropdown(int OId);

		Task<List<LeadUsersLookupTableDto>> GetSalesManagerUser(int OId);

		Task AssignOrTransferLead(GetLeadForAssignOrTransferOutput input);

		Task TransferLeadToOtherOrganization(GetLeadForAssignOrTransferOutput input);

		Task AddActivityLog(ActivityLogInput input);

		Task AddActivityLogWithNotification(ActivityLogInput input);

		Task<List<GetActivityLogViewDto>> GetLeadActivityLog(GetActivityLogInput input);

		GetLeadCountsDto GetLeadCounts();

		Task<List<GetLeadForViewDto>> GetUserLeadForDashboard();

		Task ChangeDuplicateStatus(GetLeadForChangeDuplicateStatusOutput input);

		Task ChangeDuplicateStatusForMultipleLeads(GetLeadForChangeDuplicateStatusOutput input);

		Task ChangeDuplicateStatusForMultipleLeadsHide(GetLeadForChangeDuplicateStatusOutput input);

        Task<List<string>> GetAllUnitType(string unitType);

		Task<List<string>> GetAllStreetName(string streetName);

		Task<List<string>> GetAllStreetType(string streetType);

		Task<List<string>> GetAllSuburb(string suburb);

		Task<List<string>> GetOnlySuburb(string suburb);

		Task<List<LeadStateLookupTableDto>> GetAllStateForTableDropdown();

		List<NameValue<string>> PromotionGetAllLeadStatus(string lead);

		List<NameValue<string>> promotionGetAllJobStatus(string jobStatusName);
		List<NameValue<string>> promotionGetAllLeadSource(string leadSourceName);
		
		List<NameValue<string>> promotionGetAllState(string stateName);

		//Task<PagedResultDto<GetLeadForViewDto>> GetTotalLeadsCountForPromotion(GetAllLeadsInputForPromotion input);
		Task<List<int>> GetTotalLeadsCountForPromotion(GetAllLeadsInputForPromotion input);

		Task<List<LeadUsersLookupTableDto>> GetAllTeamsForFilter();

		Task<List<LeadUsersLookupTableDto>> GetSalesManagerForFilter(int OId, int? TeamId);

		Task<List<LeadUsersLookupTableDto>> GetSalesRepForFilter(int OId, int? TeamId);
	
		Task CopyLead(int OId, int LeadId, int? SectionId);

		bool CheckCopyExist(int OId, int LeadId);

		Task<List<CommonLookupDto>> GetallEmailTemplates(int leadId);
		Task<List<CommonLookupDto>> getOrgWiseDefultandownemailadd(int leadId);
		bool checkorgwisephonedynamicavaibleornot(int leadId);
		Task<List<CommonLookupDto>> GetallSMSTemplates(int leadId);
		
		Task<List<CommonLookupDto>> GetAllLeadAction();

		Task<string> GetCurrentUserRole();

		Task<int> GetCurrentUserId();
		Task<string> GetCurrentUserIdName();

		Task<bool> CheckValidation(CreateOrEditLeadDto input);

		Task<List<LeadUsersLookupTableDto>> GetSalesRepBySalesManagerid(int? id, int? OId);

		Task<List<LeadUsersLookupTableDto>> GetTeamForFilter();
		Task<PagedResultDto<SmsReplyDto>> GetSmsReplyList(SmsReplyInputDto input);
		Task<List<SmsReplyDto>> GetOldSmsReplyList(int? Id, int? LeadId, int orgId);
		Task<PagedResultDto<EmailReplyDto>> GetEmailReplyList(EmailReplyInputDto input);
		Task<List<EmailReplyDto>> GetOldEmailReplyList(int? LeadId, int orgId);
		Task MarkReadSms(int id);
		Task MarkReadSmsInBulk(int Readorunreadtag, List<int> ids);
		Task<SmsReplyDto> GetLeadActivitybyID(int id);

		Task<int> GetUnReadSMSCount();

		Task<int> GetUnReadPromotionCount();

		Task<PagedResultDto<GetLeadForViewDto>> GetMyReminderData(GetAllLeadsInput input);

		string GetareaBysuburbPostandstate(string suburb, string state, string postCode);

		Task ReAssignCancelOrRejectLead(int leadid);
		Task<List<LeadtrackerHistoryDto>> GetLeadActivityLogHistory(GetActivityLogInput input);

		Task<List<LeadtrackerHistoryDto>> GetJobActivityLogHistory(GetActivityLogInput input);
		Task<List<LeadUsersLookupTableDto>> GetUserByTeamId(int? teamid);
		Task UpdateIsMark();

		Task UpdateLeadActivityDetailForTodo(CreateEditActivityLogDto input);
		Task<List<LeadExpenseAddDto>> getAllexpenseTable();
		Task<List<LeadExpenseAddDto>> getAllexpenseTableById();

		Task UpdateWebDupLeads(GetLeadForChangeDuplicateStatusOutput input);

		Task AsssignOrTransferLeads(AssignOrTransferLeadInput input);

		Task UpdateFacebookLead();

		Task UpdateFakeLeadFlag(GetLeadForChangeFakeLeadStatusInput input);

        Task<PagedResultDto<GetLeadForViewDto>> Get3rdPartyLeadTrackerData(GetAll3rdPartyLeadsInput input);

		Task UpdateEnableAutoAssignLead(int oid, bool enable);

        Task<PagedResultDto<GetLeadForViewDto>> GetCIMETTrackerData(GetAll3rdPartyLeadsInput input);


    }
}