﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TheSolarProduct.Configuration.Tenants.Dto;

namespace TheSolarProduct.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);

        Task ClearLogo();

        Task ClearCustomCss();
    }
}
