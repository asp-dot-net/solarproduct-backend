﻿using Abp.Auditing;
using TheSolarProduct.Configuration.Dto;

namespace TheSolarProduct.Configuration.Tenants.Dto
{
    public class TenantEmailSettingsEditDto : EmailSettingsEditDto
    {
        public bool UseHostDefaultEmailSettings { get; set; }
    }
}