﻿namespace TheSolarProduct.Configuration
{
    public interface IExternalLoginOptionsCacheManager
    {
        void ClearCache();
    }
}
