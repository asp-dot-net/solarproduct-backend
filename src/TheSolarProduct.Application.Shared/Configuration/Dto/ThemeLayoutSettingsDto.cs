namespace TheSolarProduct.Configuration.Dto
{
    public class ThemeLayoutSettingsDto
    {
        public string LayoutType { get; set; }
    }
}