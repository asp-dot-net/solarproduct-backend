﻿using Abp.Application.Services.Dto;
using Abp.Auditing;

namespace TheSolarProduct.Configuration.Dto
{
    public class EmailSettingsDto : EntityDto<int?>
    {
        public string Provider { get; set; }

        public string DefaultFromAddress { get; set; }

        public string DefaultFromDisplayName { get; set; }

        public string SmtpHost { get; set; }

        public int SmtpPort { get; set; }

        public string ImapHost { get; set; }

        public int ImapPort { get; set; }

        public string UserName { get; set; }

        [DisableAuditing]
        public string Password { get; set; }

        public string Domain { get; set; }

        public bool EnableSsl { get; set; }

        public bool UseDefaultCredentials { get; set; }

        public bool IsActive { get; set; }
    }
}
