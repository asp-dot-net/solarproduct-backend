﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Quotations.Dtos;
using TheSolarProduct.ServiceDocumentRequests.Dtos;

namespace TheSolarProduct.ServiceDocumentRequests
{
    public interface IServiceDocumentRequestAppService : IApplicationService
    {
        Task SendServiceDocumentRequestForm(SendServiceDocumentRequestInputDto input);

        Task<ServiceDocumentRequestDto> ServiceDocumentRequestData(string STR);

        Task<List<CommonLookupDto>> GetActiveDocumentType();

        Task<List<GetServiceDocumentForView>> GetServiceDocumentsByServiceId(int serviceId);

        Task DeleteServiceDocument(EntityDto input, int sectionId);

        Task<List<ServiceDocumnetLookupDto>> GetServiceDocumentsByServiceIdForMail(int serviceId);

    }
}
