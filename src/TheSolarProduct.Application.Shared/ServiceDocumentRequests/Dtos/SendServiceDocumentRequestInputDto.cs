﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ServiceDocumentRequests.Dtos
{
    public class SendServiceDocumentRequestInputDto
    {
        public int ServiceId { get; set; }

        public int JobId { get; set; }

        public int SectionId { get; set; }

        public int ServiceDocTypeId { get; set; }

        public string SendMode { get; set; }

    }
}
