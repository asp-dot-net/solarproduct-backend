﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ServiceDocumentRequests.Dtos
{
    public class ServiceDocumentRequestDto
    {
        public bool Expiry { get; set; }

        public bool IsSubmitted { get; set; }

        public string ServiceDocumnetType { get; set; }

        public string RequestedBy { get; set; }
    }
}
