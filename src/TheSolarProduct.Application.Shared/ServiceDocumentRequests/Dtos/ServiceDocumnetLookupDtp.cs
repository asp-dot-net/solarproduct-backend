﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ServiceDocumentRequests.Dtos
{
    public class ServiceDocumnetLookupDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }

        public string FilePath { get; set; }

        public string Filetype { get; set; }
    }
}
