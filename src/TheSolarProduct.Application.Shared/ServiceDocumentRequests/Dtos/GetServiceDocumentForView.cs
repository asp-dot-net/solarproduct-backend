﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ServiceDocumentRequests.Dtos
{
    public class GetServiceDocumentForView : EntityDto
    {
        public virtual int? ServiceDocumentTypeId { get; set; }
        public string ServiceDocumentTitle { get; set; }
        public string ServiceDocumentPath { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public Guid? MediaId { get; set; }
        public virtual int? JobId { get; set; }
        public virtual int? ServiceId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreationTime { get; set; }
    }
}
