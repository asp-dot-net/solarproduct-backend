﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using TheSolarProduct.PurchaseCompanys.Dtos;

namespace TheSolarProduct.PurchaseCompanys
{
    public interface IPurchaseCompanyAppService : IApplicationService
    {
        Task<GetPurchaseCompanyForEditOutput> GetPurchaseCompanyForEdit(EntityDto input);
        Task<PagedResultDto<GetPurchaseCompanyForViewDto>> GetAll(GetAllPurchaseCompanyInput input);
        Task CreateOrEdit(CreateOrEditPurchaseCompanyDto input);
        Task Delete(EntityDto input);
        Task<FileDto> GetPurchaseCompanyToExcel(GetAllPurchaseCompanyForExcelInput input);
    }
}
