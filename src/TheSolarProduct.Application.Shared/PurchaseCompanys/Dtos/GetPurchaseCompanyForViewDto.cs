﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckActives.Dtos;

namespace TheSolarProduct.PurchaseCompanys.Dtos
{
    public class GetPurchaseCompanyForViewDto
    {
        public PurchaseCompanyDto PurchaseCompany { get; set; }
    }
}
