﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PurchaseCompanys.Dtos
{
    public class GetAllPurchaseCompanyInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
