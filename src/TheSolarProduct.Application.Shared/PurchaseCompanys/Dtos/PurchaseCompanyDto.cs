﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PurchaseCompanys.Dtos
{
    public class PurchaseCompanyDto : EntityDto
    {
        public string Name { get; set; }

        public bool? IsActive { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public string Logo { get; set; }
        public string FileToken { get; set; }
    }
}
