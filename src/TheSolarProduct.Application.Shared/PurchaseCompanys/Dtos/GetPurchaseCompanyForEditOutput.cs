﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckActives.Dtos;

namespace TheSolarProduct.PurchaseCompanys.Dtos
{
    public class GetPurchaseCompanyForEditOutput
    {
        public CreateOrEditPurchaseCompanyDto PurchaseCompany { get; set; }
    }
}
