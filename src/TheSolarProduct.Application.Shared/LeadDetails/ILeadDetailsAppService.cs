﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.LeadDetails.Dtos;

namespace TheSolarProduct.LeadDetails
{
    public interface ILeadDetailsAppService : IApplicationService
    {
        Task<GetLeadDetailsForOutput> GetLeadDetailsByLeadId(EntityDto input);

        Task<string> CreateNewJob(CreateJobDto createJobDto);

        Task<GetJobStatusDto> GetJobStatus(EntityDto input);

        Task<string> UpdateJobAddress(EditJobAddressDto editJobDto);

        Task<string> UpdateSalesTab(EditSalesTabDto input);
    }
}
