﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.LeadDetails.Dtos
{
    public class EditJobAddressDto
    {
        public int JobId { get; set; }

        public int SectionId { get; set; }

        public CreateOrEditAddressDto Address { get; set; }
    }
}
