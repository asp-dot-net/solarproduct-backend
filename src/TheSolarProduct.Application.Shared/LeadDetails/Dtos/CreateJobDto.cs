﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.LeadDetails.Dtos
{
    public class CreateJobDto
    {
        public int SectionId { get; set; }

        public int LeadId { get; set; }

        public int JobTypeId { get; set; }

        public CreateOrEditAddressDto Address { get; set; }
    }
}
