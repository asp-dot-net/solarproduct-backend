﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.LeadDetails.Dtos
{
    public class GetJobStatusDto
    {
        public int? JobStatusId { get; set; }

        public string JobStatus { get; set; }

        public string JobStatusColorClass { get; set; }
    }
}
