﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.LeadDetails.Dtos
{
    public class EditSalesTabDto
    {
        public int JobId { get; set; }

        public int SectionId { get; set; }

        public int? JobTypeId { get; set; }

        public string ManualQuote { get; set; }

        public string PriceType { get; set; }

        public string Note { get; set; }

        public string OldSystemDetails { get; set; }

        public string InstallerNotes { get; set; }

        public decimal? SystemCapacity { get; set; }

        public decimal? STC { get; set; }

        public decimal? Rebate { get; set; }
    }
}
