﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.LeadDetails.Dtos
{
    public class GetLeadDetailsForOutput
    {
        public string CurrentUserRole { get; set; }

        public LeadDetails LeadDetails { get; set; }

        public JobDetails JobDetails { get; set; }
    }

    public class LeadDetails
    {
        public int LeadId { get; set; }

        public int OrganizationId { get; set; }

        public int? LeadStatusId { get; set; }

        public string CompanyName { get; set; }

        public string Mobile { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string CurrentAssignUserName { get; set; }

        public string CreatedByName { get; set; }

        public DateTime CreatedOn { get; set; }

        public string Address { get; set; }

        public string Suburb { get; set; }

        public string State { get; set; }

        public string PostCode { get; set; }

        public string Area { get; set; }

        public string Type { get; set; }

        public string LeadSource { get; set; }

        public string Requirements { get; set; }

        public string FormName { get; set; }

        public string Longitude { get; set; }

        public string Latitude { get; set; }

        public string IsGoogle { get; set; }

        public string StreetNo { get; set; }
        
        public string StreetName { get; set; }

        public string StreetType { get; set; }

        public string UnitNo { get; set; }

        public string UnitType { get; set; }
    }

    public class JobDetails
    {
        public int? JobId { get; set; }

        public string JobNumber { get; set; }

        public int? JobStatusId { get; set; }

        public string JobStatus { get; set; }

        public string JobStatusColorClass { get; set; }

        public string PylonUrl { get; set; }

        public bool? IsJobCancelRequest { get; set; }

        public DateTime? DepositeRecceivedDate { get; set; }

        public DateTime? ActiveDate { get; set; }

        public int? JobTypeId { get; set; }

        public string ManualQuote { get; set; }

        public string PriceType { get; set; }

        public string Note { get; set; }
        
        public string OldSystemDetails { get; set; }
        
        public string InstallerNotes { get; set; }

        public string UnitNo { get; set; }

        public string UnitType { get; set; }

        public string StreetNo { get; set; }

        public string StreetName { get; set; }

        public string StreetType { get; set; }

        public string Suburb { get; set; }

        public string State { get; set; }

        public string PostCode { get; set; }

        public string Longitude { get; set; }

        public string Latitude { get; set; }

        public string Country { get; set; }

        public double WarehouseDistance { get; set; }

        public decimal? SystemCapacity { get; set; }

        public decimal? STC { get; set; }

        public decimal? STCPrice { get; set; }

        public decimal? Rebate { get; set; }

        public List<GetJobProductItemDto> JobProducts { get; set; }




        //Installation Details
        public DateTime? InstallationDate { get; set; }
    }
}
