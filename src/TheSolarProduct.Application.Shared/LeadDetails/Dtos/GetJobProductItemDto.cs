﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.LeadDetails.Dtos
{
    public class GetJobProductItemDto
    {
        public int Id { get; set; }

        public int ProductTypeId { get; set; }

        public int? ProductItemId { get; set; }

        public string Name { get; set; }

        public string Model { get; set; }

        public decimal? Size { get; set; }

        public int? Quantity { get; set; }

        public int LiveStock { get; set; }
    }
}