﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using TheSolarProduct.ApplicationSettings.Dto;

namespace TheSolarProduct.ApplicationSettings
{
    public interface IApplicationSettingsAppService : IApplicationService
    {
        ApplicationSettingListDto GetApplicationSetting(int? tenantId);
        Task EditApplicationSetting(EditApplicationSettingDto input);
        Task SendSMS(SendSMSInput input);

        Task SendBulkSMS(SendBulkSMSInput input);

        Task SendWholeSaleSMS(SendSMSInput input);

        Task SendWholeSaleBulkSMS(SendBulkSMSInput input);
        Task SendStockOrderSMS(SendSMSInput input);
    }
}
