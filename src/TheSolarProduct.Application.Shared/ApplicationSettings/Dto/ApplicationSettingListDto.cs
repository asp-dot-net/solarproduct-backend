﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.ApplicationSettings.Dto
{
    public class ApplicationSettingListDto : FullAuditedEntityDto
    {
        public bool FoneDynamicsEnabled { get; set; }
        public string FoneDynamicsPhoneNumber { get; set; }
        public string FoneDynamicsPropertySid { get; set; }
        public string FoneDynamicsAccountSid { get; set; }
        public string FoneDynamicsToken { get; set; }
    }
}
