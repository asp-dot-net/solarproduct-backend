﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ApplicationSettings.Dto
{
    public class SendSMSRequestInput
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Text { get; set; }
        //public string ExternalId { get; set; }
        public string WebhookUri { get; set; }
        public string WebhookMethod { get; set; }
    }
}
