﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.ApplicationSettings.Dto
{
    public class SendSMSInput
    {
        [Required]
        [MaxLength(ApplicationSettingConsts.MaxPhoneNumberLength)]
        public string PhoneNumber { get; set; }

        public string Text { get; set; }

        public int ActivityId { get; set; }

        public int promoresponseID { get; set; }

        public bool IsMyInstallerUser { get; set; }

        public int SectionID { get; set; }

        public int? oId { get; set; }
    }
}
