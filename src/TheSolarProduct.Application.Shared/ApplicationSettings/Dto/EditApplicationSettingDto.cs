﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ApplicationSettings.Dto
{
    public class EditApplicationSettingDto : EntityDto<int?>
    {
        public bool FoneDynamicsEnabled { get; set; }
        public string FoneDynamicsPhoneNumber { get; set; }
        public string FoneDynamicsPropertySid { get; set; }
        public string FoneDynamicsAccountSid { get; set; }
        public string FoneDynamicsToken { get; set; }
    }
}
