﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ApplicationSettings.Dto
{
    public class SendBulkSMSInput
    {
        public List<SendSMSInput> Messages { get; set; }
    }
}
