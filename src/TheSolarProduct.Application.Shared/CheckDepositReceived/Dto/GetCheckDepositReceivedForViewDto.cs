﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckDepositReceived.Dto;

namespace TheSolarProduct.CheckDepositReceived.Dto
{
    public class GetCheckDepositReceivedForViewDto
    {
        public CheckDepositReceivedDto CheckDepositReceived { get; set; }

    }
}
