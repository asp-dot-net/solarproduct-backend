﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CheckDepositReceived.Dto
{
    public class GetCheckDepositeReceiveForEditOutput
    {
        public CreateOrEditCheckDepositReceivedDto CheckDepositReceived { get; set; }
    }
}
