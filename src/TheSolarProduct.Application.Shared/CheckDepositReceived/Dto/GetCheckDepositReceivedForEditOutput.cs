﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Departments.Dtos;

namespace TheSolarProduct.CheckDepositReceived.Dto
{
    public class GetCheckDepositReceivedForEditOutput
    {
        public CreateOrEditCheckDepositReceivedDto CheckDepositReceived { get; set; }
    }
}
