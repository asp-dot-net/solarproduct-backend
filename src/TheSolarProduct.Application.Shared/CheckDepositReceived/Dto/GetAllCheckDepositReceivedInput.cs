﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CheckDepositReceived.Dto
{
    public class GetAllCheckDepositReceivedInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        //public string Sorting { get; set; }
    }
}
