﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CheckDepositReceived.Dto
{
    public class GetAllCheckDepositeReceivedForExcelInput
    {
        public string Filter { get; set; }
    }
}
