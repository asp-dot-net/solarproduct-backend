﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.CheckDepositReceived.Dto
{
    public class CreateOrEditCheckDepositReceivedDto : EntityDto<int?>
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
