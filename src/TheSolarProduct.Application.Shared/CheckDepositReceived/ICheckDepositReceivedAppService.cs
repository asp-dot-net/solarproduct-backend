﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.CheckDepositReceived.Dto;
using TheSolarProduct.Dto;

namespace TheSolarProduct.CheckDepositReceived
{
    public interface ICheckDepositReceivedAppService : IApplicationService
    {
        Task<PagedResultDto<GetCheckDepositReceivedForViewDto>> GetAll(GetAllCheckDepositReceivedInput input);

        Task CreateOrEdit(CreateOrEditCheckDepositReceivedDto input);

        Task<GetCheckDepositReceivedForEditOutput> GetCheckDepositReceivedForEdit(EntityDto input);

        Task<FileDto> GetCheckDepositeReceiveToExcel(GetAllCheckDepositeReceivedForExcelInput input);

        Task Delete(EntityDto input);
    }

}
