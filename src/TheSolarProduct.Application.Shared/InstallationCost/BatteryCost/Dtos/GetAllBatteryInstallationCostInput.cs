﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.BatteryCost.Dtos
{
    public class GetAllBatteryInstallationCostInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
