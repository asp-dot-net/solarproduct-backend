﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.BatteryCost.Dtos
{
    public class BatteryInstallationCostDto : EntityDto
    {
        public decimal? FixCost { get; set; }

        public decimal? Kw { get; set; }

        public decimal? ExtraCost { get; set; }
    }
}
