﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.BatteryCost.Dtos
{
    public class GetBatteryInstallationCostForEditOutput
    {
        public CreateOrEditBatteryInstallationCostDto BatteryInstallationCosts { get; set; }
    }
}
