﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.BatteryCost.Dtos
{
    public class GetBatteryInstallationCostForViewDto
    {
        public BatteryInstallationCostDto BatteryInstallationCost { get; set; }
    }
}
