﻿using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.InstallationCost.BatteryCost.Dtos;

namespace TheSolarProduct.InstallationCost.BatteryCost
{
    public interface IBatteryInstallationCostAppService : IApplicationService
    {
        Task<PagedResultDto<GetBatteryInstallationCostForViewDto>> GetAll(GetAllBatteryInstallationCostInput input);

        Task<GetBatteryInstallationCostForEditOutput> GetBatteryInstallationCostForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditBatteryInstallationCostDto input);

        Task Delete(EntityDto input);
    }
}
