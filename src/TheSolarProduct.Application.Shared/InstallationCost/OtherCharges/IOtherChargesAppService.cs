﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using TheSolarProduct.InstallationCost.OtherCharges.Dtos;

namespace TheSolarProduct.InstallationCost.OtherCharges
{
    public interface IOtherChargesAppService : IApplicationService
    {
        Task<PagedResultDto<GetOtherChargesForViewDto>> GetAll(GetAllOtherChargesInput input);

        Task<GetOtherChargesForEditOutput> GetOtherChargesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditOtherChargesDto input);

        Task Delete(EntityDto input);
    }
}
