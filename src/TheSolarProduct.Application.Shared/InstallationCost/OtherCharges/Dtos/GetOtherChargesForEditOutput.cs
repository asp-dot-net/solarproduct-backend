﻿namespace TheSolarProduct.InstallationCost.OtherCharges.Dtos
{
    public class GetOtherChargesForEditOutput
    {
        public CreateOrEditOtherChargesDto OtherCharges { get; set; }
    }
}
