﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.OtherCharges.Dtos
{
    public class OtherChargesDto : EntityDto
    {
        public string Name { get; set; }
    }
}
