﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.InstallationCost.OtherCharges.Dtos
{
    public class CreateOrEditOtherChargesDto : EntityDto<int?>
    {
        [Required]
        public string Name { get; set; }
    }
}
