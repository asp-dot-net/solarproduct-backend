﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.InstallationCost.OtherCharges.Dtos
{
    public class GetAllOtherChargesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
