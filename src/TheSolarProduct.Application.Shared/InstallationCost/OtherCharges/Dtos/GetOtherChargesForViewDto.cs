﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.OtherCharges.Dtos
{
    public class GetOtherChargesForViewDto
    {
        public OtherChargesDto OtherCharges { get; set; }
    }
}
