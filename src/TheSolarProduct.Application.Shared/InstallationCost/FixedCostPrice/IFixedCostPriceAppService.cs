﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using TheSolarProduct.InstallationCost.FixedCostPrice.Dtos;

namespace TheSolarProduct.InstallationCost.FixedCostPrice
{
    public interface IFixedCostPriceAppService : IApplicationService
    {
        Task<PagedResultDto<GetFixedCostPriceForViewDto>> GetAll(GetAllFixedCostPriceInput input);

        Task<GetFixedCostPriceForEditOutput> GetFixedCostPriceForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditFixedCostPriceDto input);

        Task Delete(EntityDto input);
    }
}
