﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.FixedCostPrice.Dtos
{
    public class GetFixedCostPriceForEditOutput
    {
        public CreateOrEditFixedCostPriceDto FixedCostPrice {  get; set; }
    }
}
