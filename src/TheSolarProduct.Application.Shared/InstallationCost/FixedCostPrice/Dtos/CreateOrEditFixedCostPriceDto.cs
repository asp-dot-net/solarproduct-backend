﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.FixedCostPrice.Dtos
{
    public class CreateOrEditFixedCostPriceDto : EntityDto<int?>
    {
        public string Name { get; set; }

        public string Type { get; set; }

        public decimal? Cost { get; set; }
    }
}
