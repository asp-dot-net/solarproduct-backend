﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.InstallationCost.OtherCharges.Dtos;

namespace TheSolarProduct.InstallationCost.FixedCostPrice.Dtos
{
    public class GetFixedCostPriceForViewDto
    {
        public FixedCostPriceDto FixedCostPrice { get; set; }
    }
}
