﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.InstallationCost.FixedCostPrice.Dtos
{
    public class GetAllFixedCostPriceInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
