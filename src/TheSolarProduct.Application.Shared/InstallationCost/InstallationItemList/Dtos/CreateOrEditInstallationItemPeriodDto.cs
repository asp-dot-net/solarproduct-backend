﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.InstallationItemList.Dtos
{
    public class CreateOrEditInstallationItemPeriodDto : EntityDto<int?>
    {
        public long OrganizationUnit { get; set; }

        public string Name { get; set; }

        //public DateTime StartDate { get; set; }

        //public DateTime EndDate { get; set; }

        public List<CreateOrEditInstallationItemListDto> InstallationItemList { get; set; }

        public string Organization { get; set; }

        public int Month { get; set; }

    }
}
