﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.InstallationCost.InstallationItemList.Dtos
{
    public class CreateOrEditInstallationItemListDto : EntityDto<int?>
    {
        public int? InstallationItemPeriodId { get; set; }

        public int ProductTypeId { get; set; }

        public int ProductItemId { get; set; }

        public string ProductItem { get; set; }

        public decimal? Size { get; set; }

        public decimal? PricePerWatt { get; set; }

        public decimal? UnitPrice { get; set; }
    }
}
