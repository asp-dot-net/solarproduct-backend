﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.InstallationItemList.Dtos
{
    public class GetInstallationPeriodForViewDto
    {
        public InstallationItemPeriodDto InstallationItemPeriod { get; set; }

        public List<GetInstallationListForViewDto> InstallationItemList { get; set; }
    }

    public class GetInstallationListForViewDto : EntityDto
    {
        public string ProductType { get; set; }

        public string ProductItem { get; set; }

        public decimal? PricePerWatt { get; set; }

        public decimal? UnitPrice { get; set; }
    }
}
