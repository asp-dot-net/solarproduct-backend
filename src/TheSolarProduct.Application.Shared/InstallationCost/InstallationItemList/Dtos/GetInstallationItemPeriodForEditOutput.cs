﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.InstallationItemList.Dtos
{
    public class GetInstallationItemPeriodForEditOutput
    {
        public CreateOrEditInstallationItemPeriodDto InstallationItemPeriod { get; set; }
    }
}
