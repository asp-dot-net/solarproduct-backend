﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.InstallationItemList.Dtos
{
    public class InstallationItemPeriodForViewDto
    {
        public InstallationItemPeriodDto InstallationItemPeriod { get; set; }
        public bool IsEdit { get; set; }

        //public List<GetInstallationItemListForViewDto> InstallationItemList { get; set; }
    }
}
