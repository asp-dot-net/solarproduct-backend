﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.InstallationItemList.Dtos
{
    public class GetAllInstallationItemListInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int OrganizationUnit { get; set; }

        public DateTime? FilterDate { get; set; }
    }
}
