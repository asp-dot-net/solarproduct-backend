﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.InstallationCost.InstallationItemList.Dtos;

namespace TheSolarProduct.InstallationCost.InstallationItemList
{
    public interface IInstallationItemListAppService : IApplicationService
    {
        Task<PagedResultDto<InstallationItemPeriodForViewDto>> GetAll(GetAllInstallationItemListInput input);

        Task<GetInstallationItemPeriodForEditOutput> GetInstallationItemPeriodForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditInstallationItemPeriodDto input);

        Task Delete(EntityDto input);

        Task<GetInstallationPeriodForViewDto> GetInstallationItemPeriodForView(EntityDto input);

        Task<List<CreateOrEditInstallationItemListDto>> GetInstallationItemListForCreateView(int month, int? org);

        Task<List<InstallationItemPeriodForViewDto>> CheckExistInstallationItemList(CreateOrEditInstallationItemPeriodDto input);
    }
}
