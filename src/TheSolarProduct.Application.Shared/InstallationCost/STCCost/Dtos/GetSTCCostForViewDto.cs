﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.InstallationCost.OtherCharges.Dtos;

namespace TheSolarProduct.InstallationCost.STCCost.Dtos
{
    public class GetSTCCostForViewDto
    {
        public STCCostDto STCCost {get; set; }
    }
}
