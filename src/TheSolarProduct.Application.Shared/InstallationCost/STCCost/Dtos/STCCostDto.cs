﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.STCCost.Dtos
{
    public class STCCostDto : EntityDto
    {
        public decimal? Cost { get; set; }
    }
}
