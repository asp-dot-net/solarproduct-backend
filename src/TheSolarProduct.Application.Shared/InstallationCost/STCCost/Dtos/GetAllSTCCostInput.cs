﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.STCCost.Dtos
{
    public class GetAllSTCCostInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
