﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.InstallationCost.STCCost.Dtos
{
    public class CreateOrEditSTCCostDto : EntityDto<int?>
    {
        [Required]
        public decimal? Cost { get; set; }
    }
}
