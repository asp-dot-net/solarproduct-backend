﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.STCCost.Dtos
{
    public class GetSTCCostForEditOutput
    {
        public CreateOrEditSTCCostDto STCCosts { get; set; }
    }
}
