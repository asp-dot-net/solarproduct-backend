﻿using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.InstallationCost.STCCost.Dtos;

namespace TheSolarProduct.InstallationCost.STCCost
{
    public interface ISTCCostAppService : IApplicationService
    {
        Task<PagedResultDto<GetSTCCostForViewDto>> GetAll(GetAllSTCCostInput input);

        Task<GetSTCCostForEditOutput> GetSTCCostForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditSTCCostDto input);

        Task Delete(EntityDto input);
    }
}
