﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheSolarProduct.InstallationCost.JobCostFixExpenses.Dto
{
    public class CreateOrEditJobCostFixExpenseListDto : EntityDto<int?>
    {
        public int JobCostFixExpensePeriodId { get; set; }

        public int JobCostFixExpenseTypeId { get; set; }

        public decimal? Amount { get; set; }

        public string JobCostFixExpenseType { get; set; }
    }
}
