﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.JobCostFixExpenses.Dto
{
    public class CreateOrEditJobCostFixExpensePeriodDto : EntityDto<int?>
    {
        public long OrganizationUnit { get; set; }

        public string Name { get; set; }

        public List<CreateOrEditJobCostFixExpenseListDto> JobCostFixExpenseList { get; set; }

        public string Organization { get; set; }

        public DateTime? Date { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }

    }
}
