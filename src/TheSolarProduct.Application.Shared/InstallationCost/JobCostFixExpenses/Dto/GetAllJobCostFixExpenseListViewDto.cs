﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.JobCostFixExpenses.Dto
{
    public class GetAllJobCostFixExpenseListViewDto : EntityDto
    {
        public string OrganizationUnit { get; set; }

        public string Name { get; set; }

        public string Period { get; set; }
    }
}
