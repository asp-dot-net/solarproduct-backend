﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheSolarProduct.InstallationCost.JobCostFixExpenses.Dto;

namespace TheSolarProduct.InstallationCost.JobCostFixExpenses
{
    public interface IJobCostFixExpenseListAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllJobCostFixExpenseListViewDto>> GetAll(GetJobCostFixExpenseListInput input);

        Task<CreateOrEditJobCostFixExpensePeriodDto> GetJobCostFixExpensePeriodForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditJobCostFixExpensePeriodDto input);

        Task Delete(EntityDto input);

        //Task<List<GetAllJobCostFixExpenseListViewDto>> CheckExistJobCostFixExpenseList(CreateOrEditJobCostFixExpensePeriodDto input);

        //Task<List<CreateOrEditJobCostFixExpenseListDto>> GetJobCostFixExpenseForCreateView(int month, int? org);
    }
}
