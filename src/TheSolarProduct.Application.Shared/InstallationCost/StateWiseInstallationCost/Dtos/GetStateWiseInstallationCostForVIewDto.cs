﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.StateWiseInstallationCost.Dtos
{
    public class GetStateWiseInstallationCostForVIewDto
    {
       public StateWiseInstallationCostDto StateWiseInstallationCost { get; set; }
    }
}
