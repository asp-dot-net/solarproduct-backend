﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TheSolarProduct.InstallationCost.StateWiseInstallationCost.Dtos
{
    public class CreateOrEditStateWiseInstallationCostDto : EntityDto<int?>
    {
        [Required]
        public int StateId { get; set; }

        public decimal? Metro { get; set; }

        public decimal? Regional { get; set; }
    }
}
