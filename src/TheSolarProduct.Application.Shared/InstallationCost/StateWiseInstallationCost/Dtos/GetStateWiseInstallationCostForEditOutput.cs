﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.StateWiseInstallationCost.Dtos
{
    public class GetStateWiseInstallationCostForEditOutput
    {
        public CreateOrEditStateWiseInstallationCostDto StateWiseInstallationCost { get; set; }
    }
}
