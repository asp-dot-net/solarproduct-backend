﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.StateWiseInstallationCost.Dtos
{
    public class GetAllStateWiseInstallationCostInput : PagedAndSortedResultRequestDto
    {
        public int? StateId { get; set; }
    }
}
