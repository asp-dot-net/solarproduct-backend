﻿using Abp.Application.Services.Dto;

namespace TheSolarProduct.InstallationCost.StateWiseInstallationCost.Dtos
{
    public class StateWiseInstallationCostDto : EntityDto
    {
        public string State { get; set; }

        public decimal? Metro { get; set; }

        public decimal? Regional { get; set; }
    }
}
