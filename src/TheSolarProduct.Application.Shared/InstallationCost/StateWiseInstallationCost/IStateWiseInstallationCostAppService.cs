﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using TheSolarProduct.InstallationCost.StateWiseInstallationCost.Dtos;

namespace TheSolarProduct.InstallationCost.StateWiseInstallationCost
{
    public interface IStateWiseInstallationCostAppService : IApplicationService
    {
        Task<PagedResultDto<GetStateWiseInstallationCostForVIewDto>> GetAll(GetAllStateWiseInstallationCostInput input);

        Task<GetStateWiseInstallationCostForEditOutput> GetStateWiseInstallationCostForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditStateWiseInstallationCostDto input);

        Task Delete(EntityDto input);
    }
}
