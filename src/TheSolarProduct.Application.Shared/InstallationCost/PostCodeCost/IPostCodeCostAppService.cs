﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using TheSolarProduct.InstallationCost.PostCodeCost.Dtos;

namespace TheSolarProduct.InstallationCost.PostCodeCost
{
    public interface IPostCodeCostAppService : IApplicationService
    {
        Task<PagedResultDto<GetPostCodeCostForVIewDto>> GetAll(GetAllPostCodeCostInput input);

        Task<CreateOrEditPostCodePricePeriodDto> GetPostCodeCostForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditPostCodePricePeriodDto input);

        Task Delete(EntityDto input);

        Task<bool> CheckExistingPostCode(CreateOrEditPostCodePricePeriodDto input);

        Task<List<CreateOrEditPostCodePriceListDto>> GetPrevPostCodePriceList(DateTime? date);
    }
}
