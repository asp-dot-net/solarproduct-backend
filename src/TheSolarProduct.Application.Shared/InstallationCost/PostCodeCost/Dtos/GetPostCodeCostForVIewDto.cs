﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.PostCodeCost.Dtos
{
    public class GetPostCodeCostForVIewDto
    {
        public PostCodePricePeriodDto PostoCodePrice { get; set; }

        public bool IsEdit { get; set; }
    }
}
