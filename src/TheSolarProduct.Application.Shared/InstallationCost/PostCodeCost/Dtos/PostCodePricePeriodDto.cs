﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.PostCodeCost.Dtos
{
    public class PostCodePricePeriodDto : EntityDto
    {
        public string Name { get; set; }

        public string Period { get; set; }
    }
}
