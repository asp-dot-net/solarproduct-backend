﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.PostCodeCost.Dtos
{
    public class CreateOrEditPostCodePriceListDto : EntityDto<int?>
    {
        public string PostCode { get; set; }

        public decimal? Price { get; set; }
    }
}
