﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.PostCodeCost.Dtos
{
    public class CreateOrEditPostCodePricePeriodDto : EntityDto<int?>
    {
        public string Name { get; set; }

        public DateTime? Date { get; set; }

        public List<CreateOrEditPostCodePriceListDto> PostCodePriceList { get; set; }
    }
}
