﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.PostCodeCost.Dtos
{
    public class GetAllPostCodeCostInput : PagedAndSortedResultRequestDto
    {
        public string filter { get; set; }
    }
}
