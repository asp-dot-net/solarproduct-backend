﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.ExtraInstallationCharge.Dtos
{
    public class GetExtraInstallationChargesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string Category { get; set; }

        public int Name { get; set; }
    }
}
