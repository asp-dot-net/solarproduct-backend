﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.ExtraInstallationCharge.Dtos
{
    public class GetExtraInstallationChargesForViewDto
    {
        public ExtraInstallationChargesDto ExtraInstallationCharges { get; set; }
    }
}