﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.ExtraInstallationCharge.Dtos
{
    public class ExtraInstallationChargesDto : EntityDto
    {
        public string Category { get; set; }

        public string Name { get; set; }

        public string State { get; set; }

        public string Type { get; set; }

        public decimal? MetroAmount { get; set; }

        public decimal? RegionalAmount { get; set; }
    }
}
