﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace TheSolarProduct.InstallationCost.ExtraInstallationCharge.Dtos
{
    public class CreateOrEditExtraInstallationChargeDto : EntityDto<int?>
    {
        public virtual string Category { get; set; }

        public virtual int NameId { get; set; }

        public int StateId { get; set; }

        public string Type { get; set; }

        public decimal? MetroAmount { get; set; }

        public decimal? RegionalAmount { get; set; }

        public List<int> StateIds { get; set; }
    }
}
