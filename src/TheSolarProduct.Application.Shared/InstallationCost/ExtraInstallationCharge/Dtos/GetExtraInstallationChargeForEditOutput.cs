﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.InstallationCost.ExtraInstallationCharge.Dtos
{
    public class GetExtraInstallationChargeForEditOutput
    {
        public CreateOrEditExtraInstallationChargeDto ExtraInstallationCharge { get; set; }
    }
}
