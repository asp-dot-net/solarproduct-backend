﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.InstallationCost.ExtraInstallationCharge.Dtos;

namespace TheSolarProduct.InstallationCost.ExtraInstallationCharge
{
    public interface IExtraInstallationChargesAppService : IApplicationService
    {
        Task<PagedResultDto<GetExtraInstallationChargesForViewDto>> GetAll(GetExtraInstallationChargesInput input);

        Task<GetExtraInstallationChargeForEditOutput> GetExtraInstallationChargesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditExtraInstallationChargeDto input);

        Task Delete(EntityDto input);
    }
}
