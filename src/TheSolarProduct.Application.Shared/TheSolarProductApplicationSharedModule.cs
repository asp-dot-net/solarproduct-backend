﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace TheSolarProduct
{
    [DependsOn(typeof(TheSolarProductCoreSharedModule))]
    public class TheSolarProductApplicationSharedModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TheSolarProductApplicationSharedModule).GetAssembly());
        }
    }
}