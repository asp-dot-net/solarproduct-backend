﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PurchaseDocumentLists.Dtos
{
    public class CreateOrEditPurchaseDocumentListDto : EntityDto<int?>
    {
        public string Name { get; set; }
        public int Size { get; set; }
        public List<string> Formats { get; set; }
        public bool IsActive { get; set; }
    }
}
