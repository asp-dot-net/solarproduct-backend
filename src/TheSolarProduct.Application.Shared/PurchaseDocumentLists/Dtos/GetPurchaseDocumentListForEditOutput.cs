﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.PurchaseCompanys.Dtos;

namespace TheSolarProduct.PurchaseDocumentLists.Dtos
{
    public class GetPurchaseDocumentListForEditOutput
    {
        public CreateOrEditPurchaseDocumentListDto PurchaseDocumentList { get; set; }
    }
}
