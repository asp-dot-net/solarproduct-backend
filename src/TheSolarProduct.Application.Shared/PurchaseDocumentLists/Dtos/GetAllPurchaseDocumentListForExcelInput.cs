﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PurchaseDocumentLists.Dtos
{
    public class GetAllPurchaseDocumentListForExcelInput
    {
        public string Filter { get; set; }
    }
}
