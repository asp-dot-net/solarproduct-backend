﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.PurchaseCompanys.Dtos;

namespace TheSolarProduct.PurchaseDocumentLists.Dtos
{
    public class GetPurchaseDocumentListForViewDto
    {
        public PurchaseDocumentListDto PurchaseDocumentList { get; set; }
    }
}
