﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PurchaseDocumentLists.Dtos
{
    public class GetAllPurchaseDocumentListInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
