﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ProductPackages.Dtos;

namespace TheSolarProduct.ProductPackages
{
    public interface IProductPackageAppService : IApplicationService
    {
        Task<PagedResultDto<GetProductPackageForViewDto>> GetAll(GetAllProductPackageForInput input);

        Task<GetProductPackageForViewDto> GetProductPackageForView(int id);

        Task<GetProductPackageForEditOutput> GetProductPackageForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditProductPackageDto input);

        Task Delete(EntityDto input);

        Task<List<NameValueDto>> GetProductPackageForDropdown();

        Task<List<CreateOrEditProductPackageItemDto>> GetProductPackageItemByPackageId(int productPackageId);

        Task<List<GetAllPackageDetailsDto>> GetAllPackageDetails();
    }
}
