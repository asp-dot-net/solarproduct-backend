﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ProductPackages.Dtos
{
    public class GetAllPackageDetailsDto : EntityDto
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public List<PackageItems> PackageItems { get; set; }
    }

    public class PackageItems
    {
        public string ProductType { get; set; }

        public string ProductItem { get; set; }

        public string Model { get; set; }

        public int Quantity { get; set; }
    }
}
