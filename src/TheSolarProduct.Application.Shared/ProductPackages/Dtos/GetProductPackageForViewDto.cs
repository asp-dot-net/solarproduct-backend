﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ProductPackages.Dtos
{
    public class GetProductPackageForViewDto
    {
        public ProductPackageDto ProductPackage { get; set; }
    }
}
