﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ProductPackages.Dtos
{
    public class GetAllProductPackageForInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
