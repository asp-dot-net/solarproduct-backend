﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ProductPackages.Dtos
{
    public class CreateOrEditProductPackageDto : EntityDto<int?>
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public bool IsActive { get; set; }

        public List<CreateOrEditProductPackageItemDto> ProductPackageItem { get; set; }

        public List<int> OrganizationIds { get; set; }

        public List<int> StateIds { get; set; }

    }
}
