﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ProductPackages.Dtos
{
    public class GetProductPackageForEditOutput
    {
        public CreateOrEditProductPackageDto ProductPackage { get; set; }
    }
}
