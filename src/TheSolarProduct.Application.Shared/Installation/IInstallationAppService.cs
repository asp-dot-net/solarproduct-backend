﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Installation.Dtos;
using TheSolarProduct.Jobs.Dtos;

namespace TheSolarProduct.Installation
{
    public interface IInstallationAppService : IApplicationService
    {
        Task<PagedResultDto<GetJobForViewDto>> GetPendingInstallationList(GetAllInstallationInput input);
        Task<GetJobForViewDto> GetPendingInstallationcount(GetAllInstallationInput input);
        List<InstallationCalendarDto> GetInstallationCalendar(int installerId, DateTime CalendarStartDate, int? orgID, string areaNameFilter, string state);
        Task<List<GetInstallerMapDto>> GetInstallerMap(GetAllInputMapDto input);
        Task<PagedResultDto<GetJobForViewDto>> GetJobBookingInstallationList(GetAllInstallationInput input);
        //Task<TotalSammaryCountDto> getSummaryCountForMap();

        Task<PagedResultDto<GetMissingJobDto>> GetMissingJobs(GetMissingJobInput input);
    }
}
