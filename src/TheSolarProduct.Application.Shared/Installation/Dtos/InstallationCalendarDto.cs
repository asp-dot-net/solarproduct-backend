﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Installation.Dtos
{
    public class InstallationCalendarDto
    {
        public DateTime InstallationDate { get; set; }
        public int InstallationCount { get; set; }
        public List<InstallationJobsDto> Jobs { get; set; }
    }
}
