﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Jobs.Dtos;

namespace TheSolarProduct.Installation.Dtos
{
    public class GetInstallerMapDto
    {
        public string label { get; set; }
        public string Company { get; set; }

        public string Status { get; set; }
        public string Adress { get; set; }
        public string State { get; set; }
        public string PhoneEmail { get; set; }
        public DateTime? DepRecDate { get; set; }
        public DateTime? ActiveDate { get; set; }
        public string HouseType { get; set; }
        public string RoofType { get; set; }

        public string AreaType { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Icon { get; set; }
        public int LeadId { get; set; }
        public double? Distance { get; set; }
        public int? TotalMapData { get; set; }
        public int? TotalDepRcvMapData { get; set; }
        public int? TotalActiveMapData { get; set; }
        public int? TotalJobBookedMapData { get; set; }
        public int? TotalJobInstallMapData { get; set; }
        public int? Other { get; set; }

        public List<int> ListOfmissingLatlong { get; set; }
        public List<GetJobProductItemForEditOutput> ProductIteams { get; set; }
        public List<string> postalcodesss { get; set; }
        public string fistpostalcode { get; set; }
        public string secondpostalcode { get; set; }
        public string FirstNearestInstallerName { get; set; }
        //public string FirstNearestInstallerName { get; set; }
        public string SecondNearestInstallerName { get; set; }

        public int Id { get; set; }
        public int? JobStatusId { get; set; }

    }
}
