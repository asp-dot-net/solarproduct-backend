﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Installation.Dtos
{
    public class GetMissingJobDto : EntityDto
    {
        public int LeadId { get; set; }

        public string JobNumber { get; set; }

        public string CompanyName { get; set; }

        public string JobStatus { get; set; }

        public string JobStatusColorClass { get; set; }
    }

    public class GetMissingJobInput : PagedAndSortedResultRequestDto
    {
        public List<int> JobIds { get; set; }
    }
}
