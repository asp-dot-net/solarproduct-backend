﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Installation.Dtos
{
    public class InstallationJobsDto
    {
        public string InstallerName { get; set; }
		public string CustomerName { get; set; }
		public string InstallationTime { get; set; }
		public int JobId { get; set; }
		public int? LeadId { get; set; }
		public string JobNumber { get; set; }
		public string Suburb { get; set; }
		public string State { get; set; }
		public string PostalCode { get; set; }
		public string Address { get; set; }

		public string ColorClass { get; set; }

	}
}
