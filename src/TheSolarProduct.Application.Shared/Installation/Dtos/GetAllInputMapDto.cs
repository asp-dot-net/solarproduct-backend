﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Installation.Dtos
{
    public class GetAllInputMapDto
    {
        public string FilterName { get; set; }
        public int OrganizationUnit { get; set; }
        public string FilterText { get;set; }
        public int? HousetypeId { get;set; }
        public int? Rooftypeid { get;set; }
        public int? Jobstatusid { get;set; }
        public string Panelmodel { get;set; }
        public string Invertmodel { get;set; }
        public int? Paymenttypeid { get;set; }
        public string AreaNameFilter { get;set; }
        public string DateFilterType { get;set; }
        public string State { get;set; }
        public string SteetAddressFilter { get;set; }
        public string PostalCodeFrom { get;set; }
        public string PostalCodeTo { get;set; }
        public DateTime? StartDate { get;set; }
        public DateTime? EndDate { get;set; }
        public int? Priority { get;set; }
    }
}
