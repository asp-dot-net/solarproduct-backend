﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.PostCodeRange.Dtos;

namespace TheSolarProduct.PostCodeRange
{
    public interface IPostCodeRangeAppService : IApplicationService
    {
        Task<PagedResultDto<GetPostCodeRangeForViewDto>> GetAll(GetAllPostCodeRangeForInput input);

        //Task<GetPostalTypeForViewDto> GetPostalTypeForView(int id);

        Task<GetPostCodeRangeForEditOutput> GetPostCodeRangeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditPostCodeRangeDto input);

        Task Delete(EntityDto input);

        Task<string> GetAreaByPostCodeRange(string postCode);
    }
}
