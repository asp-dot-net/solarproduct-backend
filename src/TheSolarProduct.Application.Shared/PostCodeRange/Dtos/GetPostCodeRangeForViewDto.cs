﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PostCodeRange.Dtos
{
    public class GetPostCodeRangeForViewDto
    {
        public PostCodeRangeDto PostCodeRange { get; set; }
    }
}
