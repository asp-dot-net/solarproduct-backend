﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PostCodeRange.Dtos
{
    public class GetPostCodeRangeForEditOutput
    {
        public CreateOrEditPostCodeRangeDto PostCodeRange { get; set; }
    }
}
