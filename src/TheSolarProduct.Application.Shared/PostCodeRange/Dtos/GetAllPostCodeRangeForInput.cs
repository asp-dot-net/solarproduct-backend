﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PostCodeRange.Dtos
{
    public class GetAllPostCodeRangeForInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
