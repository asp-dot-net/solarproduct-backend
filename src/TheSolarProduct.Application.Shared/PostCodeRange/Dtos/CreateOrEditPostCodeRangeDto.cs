﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.PostCodeRange.Dtos
{
    public class CreateOrEditPostCodeRangeDto : EntityDto<int?>
    {
        public long StartPostCode { get; set; }

        public long EndPostCode { get; set; }

        public string Area { get; set; }
        public Boolean IsActive { get; set; }
    }
}
