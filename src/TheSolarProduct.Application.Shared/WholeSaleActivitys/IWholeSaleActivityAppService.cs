﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.WholeSaleActivitys.Dtos;

namespace TheSolarProduct.WholeSaleActivitys
{
    public interface IWholeSaleActivityAppService : IApplicationService
    {
        Task SendSms(WholeSaleSmsEmailDto input);

        Task SendEmail(WholeSaleSmsEmailDto input);

        Task AddNotifyActivityLog(WholeSaleActivitylogInput input);

        Task AddReminderActivityLog(WholeSaleActivitylogInput input);

        Task AddToDoActivityLog(WholeSaleActivitylogInput input);

        Task AddCommentActivityLog(WholeSaleActivitylogInput input);

        Task<PagedResultDto<SmsReplyDto>> GetSmsReplyList(SmsReplyInputDto input);

        Task MarkReadSmsInBulk(int Readorunreadtag, List<int> ids);

        Task MarkReadSms(int id);

        Task<List<SmsReplyDto>> GetOldSmsReplyList(int? Id, int? LeadId, int orgId);

    }
}
