﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSaleActivitys.Dtos
{
    public class WholeSaleActivitylogInput : EntityDto
    {
        public int TenantId { get; set; }

        public int? WholeSaleLeadId { get; set; }

        public int? SectionId { get; set; }

        public string ActivityNote { get; set; }

        public DateTime? ActivityDate { get; set; }

        public int? UserId { get; set; }
    }
}
