﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace TheSolarProduct.Web.Public.Views
{
    public abstract class TheSolarProductViewComponent : AbpViewComponent
    {
        protected TheSolarProductViewComponent()
        {
            LocalizationSourceName = TheSolarProductConsts.LocalizationSourceName;
        }
    }
}