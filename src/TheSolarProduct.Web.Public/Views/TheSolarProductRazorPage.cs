﻿using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace TheSolarProduct.Web.Public.Views
{
    public abstract class TheSolarProductRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected TheSolarProductRazorPage()
        {
            LocalizationSourceName = TheSolarProductConsts.LocalizationSourceName;
        }
    }
}
