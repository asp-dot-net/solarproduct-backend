using Microsoft.AspNetCore.Mvc;
using TheSolarProduct.Web.Controllers;

namespace TheSolarProduct.Web.Public.Controllers
{
    public class HomeController : TheSolarProductControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}