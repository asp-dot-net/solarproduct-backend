﻿namespace TheSolarProduct.LeadSources
{
    public class LeadSourceConsts
    {

        public const int MinNameLength = 2;
        public const int MaxNameLength = 50;

    }
}