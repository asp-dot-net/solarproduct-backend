﻿namespace TheSolarProduct.PostCodes
{
    public class PostCodeConsts
    {

		public const int MinPostalCodeLength = 2;
		public const int MaxPostalCodeLength = 50;
		public const string PostalCodeRegex = @"^[0-9]+$";
						
		public const int MinSuburbLength = 2;
		public const int MaxSuburbLength = 50;
						
		public const int MinPOBoxesLength = 2;
		public const int MaxPOBoxesLength = 50;
						
		public const int MinAreaLength = 2;
		public const int MaxAreaLength = 50;
						
    }
}