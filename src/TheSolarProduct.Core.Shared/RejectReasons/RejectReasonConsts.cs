﻿namespace TheSolarProduct.RejectReasons
{
    public class RejectReasonConsts
    {

		public const int MinRejectReasonNameLength = 2;
		public const int MaxRejectReasonNameLength = 100;
						
    }
}