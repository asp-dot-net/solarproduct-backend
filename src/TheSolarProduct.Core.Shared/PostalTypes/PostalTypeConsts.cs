﻿namespace TheSolarProduct.PostalTypes
{
    public class PostalTypeConsts
    {

		public const int MinNameLength = 2;
		public const int MaxNameLength = 50;
						
		public const int MinCodeLength = 2;
		public const int MaxCodeLength = 50;
						
    }
}