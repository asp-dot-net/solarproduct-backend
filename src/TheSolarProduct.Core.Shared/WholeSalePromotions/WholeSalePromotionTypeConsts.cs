﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.WholeSalePromotions
{
    public class WholeSalePromotionTypeConsts
    {
        public const int MinNameLength = 1;
        public const int MaxNameLength = 200;
    }
}
