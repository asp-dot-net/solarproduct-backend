﻿namespace TheSolarProduct.Leads
{
    public class LeadConsts
    {

		public const int MinCopanyNameLength = 1;
		public const int MaxCopanyNameLength = 100;
						
		public const int MinEmailLength = 1;
		public const int MaxEmailLength = 255;
		public const string EmailRegex = @"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$";
						
		public const int MinPhoneLength = 10;
		public const int MaxPhoneLength = 10;
		public const string PhoneRegex = @"^(07|02|03|08|04|13|18)[\d]{8}";
						
		public const int MinMobileLength = 10;
		public const int MaxMobileLength = 10;
		public const string MobileRegex = @"^(04)[\d]{8}";
						
		public const int MinAddressLength = 1;
		public const int MaxAddressLength = 300;
						
		public const int MinRequirementsLength = 1;
		public const int MaxRequirementsLength = 500;

		public const int MinUnitNoLength = 1;
		public const int MaxUnitNoLength = 50;

		public const int MinUnitTypeLength = 1;
		public const int MaxUnitTypeLength = 50;

		public const int MinStreetNoLength = 1;
		public const int MaxStreetNoLength = 50;

		public const int MinStreetNameLength = 1;
		public const int MaxStreetNameLength = 50;

		public const int MinStreetTypeLength = 1;
		public const int MaxStreetTypeLength = 50;
	}
}