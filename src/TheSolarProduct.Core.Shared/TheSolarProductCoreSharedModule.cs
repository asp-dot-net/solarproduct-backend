﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace TheSolarProduct
{
    public class TheSolarProductCoreSharedModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TheSolarProductCoreSharedModule).GetAssembly());
        }
    }
}