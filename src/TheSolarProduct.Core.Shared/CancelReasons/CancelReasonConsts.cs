﻿namespace TheSolarProduct.CancelReasons
{
    public class CancelReasonConsts
    {

		public const int MinCancelReasonNameLength = 2;
		public const int MaxCancelReasonNameLength = 100;
						
    }
}