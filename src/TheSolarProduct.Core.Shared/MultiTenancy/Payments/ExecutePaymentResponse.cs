namespace TheSolarProduct.MultiTenancy.Payments
{
    public abstract class ExecutePaymentResponse
    {
        public abstract string GetId();
    }
}