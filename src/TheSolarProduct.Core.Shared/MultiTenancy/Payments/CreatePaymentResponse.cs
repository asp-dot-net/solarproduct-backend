﻿namespace TheSolarProduct.MultiTenancy.Payments
{
    public abstract class CreatePaymentResponse
    {
        public abstract string GetId();
    }
}