﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.ApplicationSettings
{
    public class ApplicationSettingConsts
    {
        public const int MaxPhoneNumberLength = 15;
        public const int MaxPropertySidLength = 50;
        public const int MaxAccountSidLength = 50;

        #if DEBUG
            public const string DocumentPath = @"d:\thesolarproductdocs"; // For Local
        #else
            public const string DocumentPath = @"s:\documents.thesolarproduct.com"; //For Live
        #endif

        public const string ViewDocumentPath = "https://documents.thesolarproduct.com";

        public const string Api2PdfKey = "7e764038-dd32-4543-8249-22b867fc6964";

        //public const string QuickStockSqlconString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=ariseadmin;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
        public const string QuickStockSqlconString = "Data Source=20.244.18.204,1434;Initial Catalog=quickstockdb;Persist Security Info=True;User ID=sa;Password=Webp0rt4l@1412;";

    }
}
