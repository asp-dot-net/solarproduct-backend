﻿namespace TheSolarProduct.Jobs
{
    public class ProductItemConsts
    {

		public const int MinNameLength = 3;
		public const int MaxNameLength = 500;
						
		public const int MinManufacturerLength = 3;
		public const int MaxManufacturerLength = 200;
						
		public const int MinModelLength = 1;
		public const int MaxModelLength = 500;
						
		public const int MinSeriesLength = 2;
		public const int MaxSeriesLength = 50;
						
		public const int MinDescriptionLength = 3;
		public const int MaxDescriptionLength = 250;
						
		public const int MinFireTestedLength = 1;
		public const int MaxFireTestedLength = 100;
						
		public const int MinACPowerLength = 1;
		public const int MaxACPowerLength = 100;
							
    }
}