﻿using MailKit;
using MailKit.Net.Imap;
using MailKit.Net.Pop3;
using MailKit.Net.Smtp;
using MailKit.Search;
using MailKit.Security;
using MimeKit;
using MimeKit.Cryptography;
using MimeKit.Text;
using Newtonsoft.Json;
using TheSolarProduct.EmailSetup.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;

namespace TheSolarProduct.EmailSetup
{
    public class EmailAppService : TheSolarProductAppServiceBase, IEmailAppService
    {
        //private readonly IEmailConfiguration _emailConfiguration;

        public EmailAppService(/*IEmailConfiguration emailConfiguration*/)
        {
            //_emailConfiguration = emailConfiguration;
        }

        public async Task<List<object>> ReceiveEmail(int maxCount = 10)
        {
            try
            {
                using (var emailClient = new ImapClient())
                {
                    emailClient.SslProtocols = SslProtocols.Ssl3 | SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12 | SslProtocols.Tls13;

                    emailClient.ServerCertificateValidationCallback = (s, c, ch, e) => true;
                    await emailClient.ConnectAsync("outlook.office365.com", 993, SecureSocketOptions.SslOnConnect);
                    //await emailClient.ConnectAsync("imap.gmail.com", 993, SecureSocketOptions.SslOnConnect);

                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");
                    //emailClient.Authenticate("parmarsuresh204@gmail.com", "rkezkmlfsufrwsum");
                    await emailClient.AuthenticateAsync("4455cdaf-6a80-47db-ac25-4b1c1bd3eb26", "Ob8Q~IiiWJ_0weKt-xDlx2BWTP6zqpCxVIRsa-y");

                    // The Inbox folder is always available on all IMAP servers...

                    var inbox = emailClient.Inbox;
                    await inbox.OpenAsync(FolderAccess.ReadOnly);
                    var summaries = await inbox.FetchAsync(0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.InternalDate | MessageSummaryItems.Envelope | MessageSummaryItems.References | MessageSummaryItems.Flags);

                    summaries = summaries.OrderByDescending(r => r.InternalDate).ToList();

                    List<object> emails = new List<object>();
                    foreach (var email in summaries)
                    {
                        EmailClass additem = new EmailClass();
                        additem.Emailsubject = email.NormalizedSubject;
                        additem.Flag = email.Flags;
                        additem.InternalDate = email.InternalDate;
                        additem.uniqueid = (UniqueId)email.UniqueId;

                        emails.Add(additem);
                    }

                    await emailClient.DisconnectAsync(true);
                    return emails;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public async Task<string> ReceiveEmailBody(uint index)
        {
            try
            {
                UniqueId duniqueid = new UniqueId(index);
                
                using (var emailClient = new ImapClient())
                {
                    emailClient.SslProtocols = SslProtocols.Ssl3 | SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12 | SslProtocols.Tls13;

                    emailClient.CheckCertificateRevocation = false;
                    await emailClient.ConnectAsync("imap.gmail.com", 993, SecureSocketOptions.SslOnConnect);

                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                    await emailClient.AuthenticateAsync("parmarsuresh204@gmail.com", "rkezkmlfsufrwsum");
                    // The Inbox folder is always available on all IMAP servers...
                    var inbox = emailClient.Inbox;
                    await inbox.OpenAsync(FolderAccess.ReadOnly);

                    var messageBody = await inbox.GetMessageAsync(duniqueid);
                    var body = !string.IsNullOrEmpty(messageBody.HtmlBody) ? messageBody.HtmlBody : messageBody.TextBody;
                    return body;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<string> SendComposeEmail(EmailMessage emailMessage)
        {
            try
            {
                var message = new MimeMessage();
                message.To.AddRange(emailMessage.ToAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));
                message.From.AddRange(emailMessage.FromAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));

                message.Subject = emailMessage.Subject;
                //We will say we are sending HTML. But there are options for plaintext etc. 
                message.Body = new TextPart(TextFormat.Html)
                {
                    Text = emailMessage.Content
                };

                SendEmailCommonFun(message);
                // create our message text, just like before (except don't set it as the message.Body)
                var body = new TextPart("plain")
                {
                    Text = @"Hey Alice,
                    What are you up to this weekend? Monica is throwing one of her parties on
                    Saturday and I was hoping you could make it.
                    Will you be my +1?
                    -- Joey
                    "
                };
                var path = "";
                // create an image attachment for the file located at path
                var attachment1 = new MimePart("image", "gif")
                {
                    Content = new MimeContent(File.OpenRead(path), ContentEncoding.Default),
                    ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                    ContentTransferEncoding = ContentEncoding.Base64,
                    FileName = Path.GetFileName(path)
                };
                //Save Msg and Attachment
                // clone the default formatting options
                var format = FormatOptions.Default.Clone();


                // override the line-endings to be DOS no matter what platform we are on
                format.NewLineFormat = NewLineFormat.Dos;
                message.WriteTo(format, "message.eml");
                foreach (var attachment in message.Attachments)
                {
                    if (attachment is MessagePart)
                    {
                        var fileName = attachment.ContentDisposition?.FileName;
                        var rfc822 = (MessagePart)attachment;

                        if (string.IsNullOrEmpty(fileName))
                            fileName = "attached-message.eml";

                        using (var stream = File.Create(fileName))
                            rfc822.Message.WriteTo(stream);
                    }
                    else
                    {
                        var part = (MimePart)attachment;
                        var fileName = part.FileName;

                        using (var stream = File.Create(fileName))
                            part.Content.DecodeTo(stream);
                    }
                    format.ParameterEncodingMethod = ParameterEncodingMethod.Rfc2047;

                    //message.WriteTo(format, part);
                }
                // now create the multipart/mixed container to hold the message text and the
                // image attachment
                var multipart = new Multipart("mixed");
                multipart.Add(body);
                multipart.Add(attachment1);

                // now set the multipart/mixed as the message body
                message.Body = multipart;
                return message.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<string> SendEmailCommonFun(MimeMessage message)
        {
            using (var emailClient = new SmtpClient())
            {
                await emailClient.ConnectAsync("smtp.office365.com", 587, SecureSocketOptions.SslOnConnect);
                // await emailClient.ConnectAsync("smtp.gmail.com", 587, SecureSocketOptions.SslOnConnect);

                emailClient.AuthenticationMechanisms.Remove("XOAUTH2");
                emailClient.Authenticate("pravin1.arisesolar@outlook.com", "Arise@123");
                //The last parameter here is to use SSL (Which you should!)

                //Remove any OAuth functionality as we won't be using it. 
                emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                // emailClient.Authenticate(_emailConfiguration.SmtpUsername, _emailConfiguration.SmtpPassword);

                emailClient.Send(message);

                emailClient.Disconnect(true);
            }
            return "";
        }
    }

    public class EmailClass
    {
        public object Emailsubject { get; set; }

        public UniqueId uniqueid { get; set; }

        public MessageFlags? Flag { get; set; }

        public DateTimeOffset? InternalDate { get; set; }
    }
}
