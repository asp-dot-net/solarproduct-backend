﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.IdentityFramework;
using Abp.Runtime.Session;
using Abp.Threading;
using Api2Pdf;
using Microsoft.AspNetCore.Identity;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Common;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.Quotations;

namespace TheSolarProduct
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class TheSolarProductAppServiceBase : ApplicationService
    {
        public TenantManager TenantManager { get; set; }

        public UserManager UserManager { get; set; }

        public CommonManager CommonManager { get; set; }

        private string Api2PdfKey = ApplicationSettingConsts.Api2PdfKey;

        protected TheSolarProductAppServiceBase()
        {
            LocalizationSourceName = TheSolarProductConsts.LocalizationSourceName;
        }

        protected virtual async Task<User> GetCurrentUserAsync()
        {
            var user = await UserManager.FindByIdAsync(AbpSession.GetUserId().ToString());
            if (user == null)
            {
                throw new Exception("There is no current user!");
            }

            return user;
        }

        protected virtual User GetCurrentUser()
        {
            return AsyncHelper.RunSync(GetCurrentUserAsync);
        }

        protected virtual Task<Tenant> GetCurrentTenantAsync()
        {
            using (CurrentUnitOfWork.SetTenantId(null))
            {
                return TenantManager.GetByIdAsync(AbpSession.GetTenantId());
            }
        }

        protected virtual Tenant GetCurrentTenant()
        {
            using (CurrentUnitOfWork.SetTenantId(null))
            {
                return TenantManager.GetById(AbpSession.GetTenantId());
            }
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        protected virtual Quotations.QuotationTemplate GetTemplate(int OrganazationId, int TemplateTypeId)
        {
            return CommonManager.GetTemplateAsync(OrganazationId, TemplateTypeId);
        }

        protected virtual double GetDistance(double WarehouseLatitude, double WarehouseLongitude, double Latitude, double Longitude)
        {
            // The math module contains
            // a function named toRadians
            // which converts from degrees
            // to radians.
            WarehouseLongitude = toRadians(WarehouseLongitude);
            Longitude = toRadians(Longitude);

            WarehouseLatitude = toRadians(WarehouseLatitude);
            Latitude = toRadians(Latitude);

            // Haversine formula
            double dlon = Longitude - WarehouseLongitude;
            double dlat = Latitude - WarehouseLatitude;
            double a = Math.Pow(Math.Sin(dlat / 2), 2) +
                       Math.Cos(WarehouseLatitude) * Math.Cos(Latitude) *
                       Math.Pow(Math.Sin(dlon / 2), 2);

            double c = 2 * Math.Asin(Math.Sqrt(a));

            // Radius of earth in
            // kilometers. Use 3956
            // for miles
            double r = 6371;

            // calculate the result
            return Math.Round((c * r), 2);
        }

        private double toRadians(double angleIn10thofaDegree)
        {
            // Angle in 10th
            // of a degree
            return (angleIn10thofaDegree * Math.PI) / 180;
        }

        protected virtual string GeneratePdf(string JobNumber, object QuoteString, string Name)
        {
            //var a2pClient = new Api2Pdf.Api2Pdf("7e764038-dd32-4543-8249-22b867fc6964");
            var a2pClient = new Api2Pdf.Api2Pdf(Api2PdfKey);

            var options = new ChromeHtmlToPdfOptions
            {
                Delay = 0,
                PuppeteerWaitForMethod = "WaitForNavigation",
                PuppeteerWaitForValue = "Load",
                UsePrintCss = true,
                Landscape = false,
                PrintBackground = true,
                DisplayHeaderFooter = false,
                Width = "8.27in",
                Height = "11.69in",
                MarginBottom = "0in",
                MarginTop = "0in",
                MarginLeft = "0in",
                MarginRight = "0in",
                PageRanges = "1-10000",
                Scale = 1,
                OmitBackground = false
            };

            var request = new ChromeHtmlToPdfRequest
            {
                Html = QuoteString.ToString().Replace("\"", "'"),
                FileName = Name + "_" + JobNumber + ".pdf",
                Inline = true,
                Options = options,
                UseCustomStorage = false,
                //Storage =
                //            {
                //	Method = "PUT",
                //	Url = "https://presignedurl",
                //	ExtraHTTPHeaders =
                //  {

                //  }
                //}
            };

            var apiResponse = a2pClient.Chrome.HtmlToPdf(request);

            return apiResponse.FileUrl;
        }

        protected virtual async Task<string> GenerateAnyToPdf(string Url, string Name)
        {
            //var a2pClient = new Api2Pdf.Api2Pdf("7e764038-dd32-4543-8249-22b867fc6964");
            var a2pClient = new Api2Pdf.Api2Pdf(Api2PdfKey);

            var request = new LibreFileConversionRequest
            {
                Url = Url,
                FileName = Name + ".pdf",
                Inline = true,
                UseCustomStorage = false,
                //Storage =
                //            {
                //	Method = "PUT",
                //	Url = "https://presignedurl",
                //	ExtraHTTPHeaders =
                //  {

                //  }
                //}
            };

            var apiResponse = await a2pClient.LibreOffice.AnyToPdfAsync(request);

            return apiResponse.FileUrl;
        }

        protected virtual string GenerateMergedPdf(List<string> Urls, string Name)
        {
            //var a2pClient = new Api2Pdf.Api2Pdf("7e764038-dd32-4543-8249-22b867fc6964");
            var a2pClient = new Api2Pdf.Api2Pdf(Api2PdfKey);

            var request = new PdfMergeRequest
            {
                Urls = Urls,
                FileName = Name + ".pdf",
                Inline = true,
                UseCustomStorage = false,
            };

            var apiResponse = a2pClient.PdfSharp.MergePdfs(request);

            return apiResponse.FileUrl;
        }

        //protected virtual DateTime? ChangeTime(DateTime? dateTime, DateTime UtcNow)
        //{
        //    if(dateTime == null)
        //    {
        //        return dateTime;
        //    }
        //    else
        //    {
        //        return new DateTime(
        //        dateTime.Value.Year,
        //        dateTime.Value.Month,
        //        dateTime.Value.Day,
        //        UtcNow.Hour,
        //        UtcNow.Minute,
        //        UtcNow.Second,
        //        UtcNow.Millisecond,
        //        dateTime.Value.Kind);
        //    }
        //}

    }
}