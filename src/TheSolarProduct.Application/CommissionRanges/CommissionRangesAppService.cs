﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Dtos;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.CommissionRanges.Dtos;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.Jobs;
using Abp.Organizations;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using System.Collections.Generic;
using TheSolarProduct.CallFlowQueues;

namespace TheSolarProduct.CommissionRanges
{
    public class CommissionRangesAppService : TheSolarProductAppServiceBase, ICommissionRangesAppService
    {
        private readonly IRepository<CommissionRange> _commissionRangesRepository;
        private readonly IRepository<OrganizationUnit,long> _organizationRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;

        public CommissionRangesAppService(
            IRepository<CommissionRange> commissionRangesRepository
            , IRepository<OrganizationUnit, long> organizationRepository
            , IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider, IRepository<OrganizationUnit, long> organizationUnitRepository
            ) 
        {
            _commissionRangesRepository = commissionRangesRepository;
            _organizationRepository = organizationRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _organizationRepository = organizationRepository;
        }

        public async Task CreateOrEdit(CreateOrEditCommissionRangesDto input)
        {
            if(input.Id == null)

            {
                await Create(input);
            }

            else
            {
                await Update(input);
            }
        }
        protected virtual async Task Create(CreateOrEditCommissionRangesDto input)
        {
            var commissionRanges = ObjectMapper.Map<CommissionRange>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 44;
            dataVaultLog.ActionNote = "Commission Range Created : " + input.Range;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _commissionRangesRepository.InsertAsync(commissionRanges);
        }

        protected virtual async Task Update(CreateOrEditCommissionRangesDto input)
        {
            var commissionRanges = await _commissionRangesRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 44;
            dataVaultLog.ActionNote = "Commission Range Updated : " + commissionRanges.Range;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Range != commissionRanges.Range)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Range";
                history.PrevValue = commissionRanges.Range;
                history.CurValue = input.Range;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Value != commissionRanges.Value)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Value";
                history.PrevValue = commissionRanges.Value.ToString();
                history.CurValue = input.Value.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.OrgId != commissionRanges.OrgId)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Value";
                history.PrevValue = commissionRanges.OrgId > 0 ? _organizationRepository.Get((long)commissionRanges.OrgId).DisplayName : "";
                history.CurValue = input.OrgId > 0 ? _organizationRepository.Get((long)input.OrgId).DisplayName : "";
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != commissionRanges.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = commissionRanges.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            ObjectMapper.Map(input, commissionRanges);

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
        }
        public async Task Delete(EntityDto input)
        {
            var Range = _commissionRangesRepository.Get(input.Id).Range;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 44;
            dataVaultLog.ActionNote = "Commission Range Deleted : " + Range;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _commissionRangesRepository.DeleteAsync(input.Id);

        }

        public async Task<PagedResultDto<GetAllCommissionRangesViewDto>> GetAll(GetAllCommissionRangesInputDto input)
        {
            var filteredCommissionRanges = await _commissionRangesRepository.GetAll()
                        .Where(e =>e.OrgId == input.orgId)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Range.Contains(input.Filter)).AsNoTracking().OrderBy(e=>e.Id).ToListAsync();

            //var pagedAndFilteredCommissionRanges = filteredCommissionRanges
            //    .OrderBy(input.Sorting ?? "id desc")
            //    .PageBy(input);

            var commissionRanges = from o in filteredCommissionRanges
                                   select new GetAllCommissionRangesViewDto()
                                   {
                                       commissionRangesDto = new CreateOrEditCommissionRangesDto
                                       {
                                           Range = o.Range,
                                           Id = o.Id,
                                           Value = o.Value,
                                           OrgName = _organizationRepository.Get((long)o.OrgId).DisplayName,
                                           IsActive = o.IsActive,
                                   }
                               };

            var totalCount = filteredCommissionRanges.Count();

            return new PagedResultDto<GetAllCommissionRangesViewDto>(
                totalCount,
                commissionRanges.ToList()
            );
        }

        public async Task<GetAllCommissionRangesViewDto> GetCommissionRangeForView(int id)
        {
            var commissionRanges = await _commissionRangesRepository.FirstOrDefaultAsync(id);

            var output = new GetAllCommissionRangesViewDto { commissionRangesDto = ObjectMapper.Map<CreateOrEditCommissionRangesDto>(commissionRanges) };

            return output;
        }
    }
}
