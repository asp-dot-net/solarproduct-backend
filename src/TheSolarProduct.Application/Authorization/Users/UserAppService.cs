﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Net.Mail;
using Abp.Notifications;
using Abp.Organizations;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Abp.UI;
using Abp.Zero.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.Authorization.Permissions;
using TheSolarProduct.Authorization.Permissions.Dto;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users.Dto;
using TheSolarProduct.Authorization.Users.Exporting;
using TheSolarProduct.Common;
using TheSolarProduct.Configuration;
using TheSolarProduct.Dto;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Installer;
using TheSolarProduct.Installer.Dtos;
using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.LeadActions;
using TheSolarProduct.Leads;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.MyInstallerActivityLogs;
using TheSolarProduct.Notifications;
using TheSolarProduct.Organizations;
using TheSolarProduct.Organizations.Dto;
using TheSolarProduct.Sections;
using TheSolarProduct.States;
using TheSolarProduct.Storage;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Timing;
using TheSolarProduct.Url;
using TheSolarProduct.UserWiseEmailOrgs;
using Z.EntityFramework.Plus;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace TheSolarProduct.Authorization.Users
{
    //[AbpAuthorize(AppPermissions.Pages_Administration_Users, AppPermissions.Pages_Administration_SalesRep,
    //	AppPermissions.Pages_Administration_SalesManager, AppPermissions.Pages_Administration_Installer,
    //	AppPermissions.Pages_LeadTracker, AppPermissions.Pages_ManageLeads, AppPermissions.Pages_MyLeads)]

    public class UserAppService : TheSolarProductAppServiceBase, IUserAppService
    {
        private const int MaxFileBytes = 5242880; //5MB
        public IAppUrlService AppUrlService { get; set; }
        private readonly RoleManager _roleManager;
        private readonly IUserEmailer _userEmailer;
        private readonly IUserListExcelExporter _userListExcelExporter;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<RolePermissionSetting, long> _rolePermissionRepository;
        private readonly IRepository<UserPermissionSetting, long> _userPermissionRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IUserPolicy _userPolicy;
        private readonly IEnumerable<IPasswordValidator<User>> _passwordValidators;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRoleManagementConfig _roleManagementConfig;
        private readonly UserManager _userManager;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<OrganizationUnitRole, long> _organizationUnitRoleRepository;
        private readonly IRepository<UserTeam, int> _userTeamRepository;
        private readonly IRepository<InstallerAddress, int> _installerAddress;
        private readonly IRepository<InstallerDetail, int> _installerDetail;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly TempFileCacheManager _tempFileCacheManager;
        private readonly IRepository<User, long> _userRepository;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<UserWiseEmailOrg> _UserWiseEmailOrgsRepository;
        private readonly IRepository<JobType, int> _lookup_jobTypeRepository;
        private readonly IRepository<State, int> _lookup_stateRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendedOrganizationUnitRepository;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IEmailSender _emailSender;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<Section> _SectionAppService;
        private readonly IRepository<MyInstallerActivityLog> _myinstalleractivityRepository;
        private readonly IRepository<LeadAction, int> _lookup_leadActionRepository;
        private readonly IRepository<MyInstallerDocument> _myinstallerdocRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<MyInstallerActivityHistory> _myinstalleractivityHistoryRepository;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly IRepository<UserIPAddress> _userIPAddressRepository;
        private readonly IRepository<UserWarehouseLocation> _userWarehouseLocationRepository;

        public UserAppService(
            RoleManager roleManager,
            IUserEmailer userEmailer,
            IUserListExcelExporter userListExcelExporter,
            INotificationSubscriptionManager notificationSubscriptionManager,
            IAppNotifier appNotifier,
            IRepository<RolePermissionSetting, long> rolePermissionRepository,
            IRepository<UserPermissionSetting, long> userPermissionRepository,
            IRepository<UserRole, long> userRoleRepository,
            IRepository<Role> roleRepository,
            IUserPolicy userPolicy,
            IEnumerable<IPasswordValidator<User>> passwordValidators,
            IPasswordHasher<User> passwordHasher,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRoleManagementConfig roleManagementConfig,
            UserManager userManager,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IRepository<OrganizationUnitRole, long> organizationUnitRoleRepository,
            IRepository<UserTeam, int> userTeamRepository,
            IRepository<InstallerAddress> installerAddress,
            IRepository<InstallerDetail> installerDetail,
            TempFileCacheManager tempFileCacheManager,
            IBinaryObjectManager binaryObjectManager,
            IRepository<User, long> userRepository
            , IWebHostEnvironment env
            , IRepository<Tenant> tenantRepository,
             IRepository<UserWiseEmailOrg> UserWiseEmailOrgsRepository
            , IRepository<JobType, int> lookup_jobTypeRepository
            , IRepository<State, int> lookup_stateRepository
            , IRepository<ExtendOrganizationUnit, long> extendedOrganizationUnitRepository
            , IApplicationSettingsAppService applicationSettings
            , IEmailSender emailSender
            , ITimeZoneConverter timeZoneConverter
            , IRepository<Section> SectionAppService
            , IRepository<MyInstallerActivityLog> myinstalleractivityRepository
            , IRepository<LeadAction, int> lookup_leadActionRepository
            , IRepository<MyInstallerDocument> myinstallerdocRepository
            , IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            , IRepository<MyInstallerActivityHistory> myinstalleractivityHistoryRepository,
             ICommonLookupAppService CommonDocumentSaveRepository,
             ITimeZoneService timeZoneService,
             IRepository<UserIPAddress> userIPAddressRepository,
             IRepository<UserWarehouseLocation> userWarehouseLocationRepository
            )
        {
            _roleManager = roleManager;
            _userEmailer = userEmailer;
            _userListExcelExporter = userListExcelExporter;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _appNotifier = appNotifier;
            _rolePermissionRepository = rolePermissionRepository;
            _userPermissionRepository = userPermissionRepository;
            _userRoleRepository = userRoleRepository;
            _userPolicy = userPolicy;
            _passwordValidators = passwordValidators;
            _passwordHasher = passwordHasher;
            _organizationUnitRepository = organizationUnitRepository;
            _roleManagementConfig = roleManagementConfig;
            _userManager = userManager;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _organizationUnitRoleRepository = organizationUnitRoleRepository;
            _roleRepository = roleRepository;
            _userTeamRepository = userTeamRepository;
            _installerAddress = installerAddress;
            _installerDetail = installerDetail;
            _tempFileCacheManager = tempFileCacheManager;
            _binaryObjectManager = binaryObjectManager;
            AppUrlService = NullAppUrlService.Instance;
            _userRepository = userRepository;
            _env = env;
            _tenantRepository = tenantRepository;
            _UserWiseEmailOrgsRepository = UserWiseEmailOrgsRepository;
            _lookup_jobTypeRepository = lookup_jobTypeRepository;
            _lookup_stateRepository = lookup_stateRepository;
            _extendedOrganizationUnitRepository = extendedOrganizationUnitRepository;
            _applicationSettings = applicationSettings;
            _emailSender = emailSender;
            _timeZoneConverter = timeZoneConverter;
            _SectionAppService = SectionAppService;
            _myinstalleractivityRepository = myinstalleractivityRepository;
            _lookup_leadActionRepository = lookup_leadActionRepository;
            _myinstallerdocRepository = myinstallerdocRepository;
            _dbcontextprovider = dbcontextprovider;
            _myinstalleractivityHistoryRepository = myinstalleractivityHistoryRepository;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _timeZoneService = timeZoneService;
            _userIPAddressRepository = userIPAddressRepository;
            _userWarehouseLocationRepository = userWarehouseLocationRepository;
        }

        public async Task<PagedResultDto<UserListDto>> GetUsers(GetUsersInput input)
        {
            var query = GetUsersFilteredQuery(input);

            var userCount = await query.CountAsync();

            var users = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var userListDtos = ObjectMapper.Map<List<UserListDto>>(users);
            await FillRoleNames(userListDtos);

            return new PagedResultDto<UserListDto>(
                userCount,
                userListDtos
                );
        }
        public async Task<PagedResultDto<UserListDto>> GetSalesRep(GetUsersInput input)
        {
            var query = GetStaticUsersFilteredQuery(input);

            var userCount = await query.CountAsync();

            var users = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var userListDtos = ObjectMapper.Map<List<UserListDto>>(users);
            await FillRoleNames(userListDtos);

            return new PagedResultDto<UserListDto>(
                userCount,
                userListDtos
                );
        }
        public async Task<PagedResultDto<UserListDto>> GetSalesManager(GetUsersInput input)
        {
            var query = GetStaticUsersFilteredQuery(input);

            var userCount = await query.CountAsync();

            var users = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var userListDtos = ObjectMapper.Map<List<UserListDto>>(users);
            await FillRoleNames(userListDtos);

            return new PagedResultDto<UserListDto>(
                userCount,
                userListDtos
                );
        }
        public async Task<PagedResultDto<GetInstallerDto>> GetInstaller(GetInstallerInput input)
        {
            var query = GetInstallersFilteredQuery(input);

            var userCount = await query.CountAsync();

            var users = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var userListDtos = ObjectMapper.Map<List<GetInstallerDto>>(users);
            await FillRoleNamesforInstaller(userListDtos);

            return new PagedResultDto<GetInstallerDto>(
                userCount,
                userListDtos
                );
        }

        public async Task<List<OrganizationUnitDto>> GetOrganizationUnit()
        {
            int UserId = (int)AbpSession.UserId;


            // var User_List = _userRepository
            //.GetAll();

            // var RoleName = (from user in User_List
            //                 join ur in _userRoleRepository.GetAll() on user.Id equals ur.UserId into urJoined
            //                 from ur in urJoined.DefaultIfEmpty()
            //                 join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
            //                 from us in usJoined.DefaultIfEmpty()
            //                 where (us != null && user.Id == UserId)
            //                 select (us.DisplayName));

            var OrganizationList = new List<OrganizationUnitDto>();
            //if (!RoleName.Contains("Admin"))
            //{
            //    var dt = _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == UserId).Select(e=>e.OrganizationUnitId).ToList();
            OrganizationList = (from or in _UserWiseEmailOrgsRepository.GetAll()
                                join uo in _organizationUnitRepository.GetAll() on or.OrganizationUnitId equals uo.Id into uoJoined
                                from uo in uoJoined.DefaultIfEmpty()
                                where (or.UserId == UserId)
                                select new OrganizationUnitDto
                                {
                                    Id = uo.Id,
                                    Code = uo.Code,
                                    DisplayName = uo.DisplayName,
                                    ParentId = uo.ParentId
                                }).OrderBy(e => e.Id).ToList();
            //}
            //else
            //{
            //    OrganizationList = _organizationUnitRepository.GetAll()
            //                        .Select(state => new OrganizationUnitDto
            //                        {
            //                            Id = state.Id,
            //                            Code = state.Code,
            //                            DisplayName = state.DisplayName,
            //                            ParentId = state.ParentId
            //                        }).OrderBy(e => e.Id).ToList();
            //}

            return OrganizationList;
        }
        #region Export to Excel
        public async Task<FileDto> GetUsersToExcel(GetUsersToExcelInput input)
        {
            var query = GetUsersFilteredQuery(input);

            var users = await query
                .OrderBy(input.Sorting)
                .ToListAsync();

            var userListDtos = ObjectMapper.Map<List<UserListDto>>(users);
            await FillRoleNames(userListDtos);

            return _userListExcelExporter.ExportToFile(userListDtos);
        }
        public async Task<FileDto> GetSalesRepToExcel(GetUsersToExcelInput input)
        {
            var query = GetStaticUsersFilteredQuery(input);

            var users = await query
                .OrderBy(input.Sorting)
                .ToListAsync();

            var userListDtos = ObjectMapper.Map<List<UserListDto>>(users);
            await FillRoleNames(userListDtos);

            return _userListExcelExporter.ExportToFile(userListDtos);
        }
        public async Task<FileDto> GetSalesManagerToExcel(GetUsersToExcelInput input)
        {
            var query = GetStaticUsersFilteredQuery(input);

            var users = await query
                .OrderBy(input.Sorting)
                .ToListAsync();

            var userListDtos = ObjectMapper.Map<List<UserListDto>>(users);
            await FillRoleNames(userListDtos);

            return _userListExcelExporter.ExportToFile(userListDtos);
        }
        public async Task<FileDto> GetInstallerToExcel(GetInstallerToExcelInput input)
        {
            //var query = GetStaticUsersFilteredQuery(input);
            var query = GetInstallersFilteredQuery(input);

            var users = await query
                .OrderBy(input.Sorting)
                .ToListAsync();

            //var userListDtos = ObjectMapper.Map<List<UserListDto>>(users);
            var userListDtos = ObjectMapper.Map<List<GetInstallerDto>>(users);
            //await FillRoleNames(userListDtos);
            await FillRoleNamesforInstaller(userListDtos);

            return _userListExcelExporter.MyInstallerExportToFile(userListDtos);
        }
        #endregion

        [AbpAuthorize(AppPermissions.Pages_Administration_SalesRep_Create, AppPermissions.Pages_Administration_SalesRep_Edit)]
        public async Task<GetUserForEditOutput> GetSalesRepForEdit(NullableIdDto<long> input)
        {
            //Getting all available roles
            var userRoleDtos = await _roleManager.Roles
                .OrderBy(r => r.DisplayName)
                .Select(r => new UserRoleDto
                {
                    RoleId = r.Id,
                    RoleName = r.Name,
                    RoleDisplayName = r.DisplayName
                })
                .ToArrayAsync();

            var allOrganizationUnits = await _organizationUnitRepository.GetAllListAsync();

            var output = new GetUserForEditOutput
            {
                Roles = userRoleDtos,
                AllOrganizationUnits = ObjectMapper.Map<List<OrganizationUnitDto>>(allOrganizationUnits),
                MemberedOrganizationUnits = new List<string>()
            };

            if (!input.Id.HasValue)
            {
                //Creating a new user
                output.User = new UserEditDto
                {
                    IsActive = true,
                    ShouldChangePasswordOnNextLogin = true,
                    IsTwoFactorEnabled = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.TwoFactorLogin.IsEnabled),
                    IsLockoutEnabled = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.UserLockOut.IsEnabled)
                };

                foreach (var defaultRole in await _roleManager.Roles.Where(r => r.IsDefault).ToListAsync())
                {
                    var defaultUserRole = userRoleDtos.FirstOrDefault(ur => ur.RoleName == defaultRole.Name);
                    if (defaultUserRole != null)
                    {
                        defaultUserRole.IsAssigned = true;
                    }
                }

                output.UserIPAddress = new List<UserIPAddressForEdit>();
            }
            else
            {
                //Editing an existing user
                var user = await UserManager.GetUserByIdAsync(input.Id.Value);

                output.User = ObjectMapper.Map<UserEditDto>(user);
                output.ProfilePictureId = user.ProfilePictureId;

                var organizationUnits = await UserManager.GetOrganizationUnitsAsync(user);
                output.MemberedOrganizationUnits = organizationUnits.Select(ou => ou.Code).ToList();

                var allRolesOfUsersOrganizationUnits = GetAllRoleNamesOfUsersOrganizationUnits(input.Id.Value);

                foreach (var userRoleDto in userRoleDtos)
                {
                    userRoleDto.IsAssigned = await UserManager.IsInRoleAsync(user, userRoleDto.RoleName);
                    userRoleDto.InheritedFromOrganizationUnit = allRolesOfUsersOrganizationUnits.Contains(userRoleDto.RoleName);
                }

                //IpAddress
                var userIp = _userIPAddressRepository.GetAll().Where(e => e.UserId == input.Id.Value);
                output.UserIPAddress = (from o in userIp
                                        select new UserIPAddressForEdit
                                        {
                                            IPAdress = o.IPAdress,
                                            IsActive = o.IsActive
                                        }).ToList();
            }

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_SalesManager_Create, AppPermissions.Pages_Administration_SalesManager_Edit)]
        public async Task<GetUserForEditOutput> GetSalesManagerForEdit(NullableIdDto<long> input)
        {
            //Getting all available roles
            var userRoleDtos = await _roleManager.Roles
                .OrderBy(r => r.DisplayName)
                .Select(r => new UserRoleDto
                {
                    RoleId = r.Id,
                    RoleName = r.Name,
                    RoleDisplayName = r.DisplayName
                })
                .ToArrayAsync();

            var allOrganizationUnits = await _organizationUnitRepository.GetAllListAsync();

            var output = new GetUserForEditOutput
            {
                Roles = userRoleDtos,
                AllOrganizationUnits = ObjectMapper.Map<List<OrganizationUnitDto>>(allOrganizationUnits),
                MemberedOrganizationUnits = new List<string>()
            };

            if (!input.Id.HasValue)
            {
                //Creating a new user
                output.User = new UserEditDto
                {
                    IsActive = true,
                    ShouldChangePasswordOnNextLogin = true,
                    IsTwoFactorEnabled = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.TwoFactorLogin.IsEnabled),
                    IsLockoutEnabled = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.UserLockOut.IsEnabled)
                };

                foreach (var defaultRole in await _roleManager.Roles.Where(r => r.IsDefault).ToListAsync())
                {
                    var defaultUserRole = userRoleDtos.FirstOrDefault(ur => ur.RoleName == defaultRole.Name);
                    if (defaultUserRole != null)
                    {
                        defaultUserRole.IsAssigned = true;
                    }
                }

                output.UserIPAddress = new List<UserIPAddressForEdit>();
            }
            else
            {
                //Editing an existing user
                var user = await UserManager.GetUserByIdAsync(input.Id.Value);

                output.User = ObjectMapper.Map<UserEditDto>(user);
                output.ProfilePictureId = user.ProfilePictureId;

                var organizationUnits = await UserManager.GetOrganizationUnitsAsync(user);
                output.MemberedOrganizationUnits = organizationUnits.Select(ou => ou.Code).ToList();

                var allRolesOfUsersOrganizationUnits = GetAllRoleNamesOfUsersOrganizationUnits(input.Id.Value);

                foreach (var userRoleDto in userRoleDtos)
                {
                    userRoleDto.IsAssigned = await UserManager.IsInRoleAsync(user, userRoleDto.RoleName);
                    userRoleDto.InheritedFromOrganizationUnit = allRolesOfUsersOrganizationUnits.Contains(userRoleDto.RoleName);
                }

                //IpAddress
                var userIp = _userIPAddressRepository.GetAll().Where(e => e.UserId == input.Id.Value);
                output.UserIPAddress = (from o in userIp
                                        select new UserIPAddressForEdit
                                        {
                                            IPAdress = o.IPAdress,
                                            IsActive = o.IsActive
                                        }).ToList();
            }

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Installer_Create, AppPermissions.Pages_Administration_Installer_Edit, AppPermissions.Pages_MyInstaller_Sms)]
        public async Task<GetInstallerForEditOutput> GetInstallerForEdit(NullableIdDto<long> input)
        {
            //Getting all available roles
            var userRoleDtos = await _roleManager.Roles
                .OrderBy(r => r.DisplayName)
                .Select(r => new UserRoleDto
                {
                    RoleId = r.Id,
                    RoleName = r.Name,
                    RoleDisplayName = r.DisplayName
                })
                .ToArrayAsync();

            var allOrganizationUnits = await _organizationUnitRepository.GetAllListAsync();

            var output = new GetInstallerForEditOutput
            {
                Roles = userRoleDtos,
                AllOrganizationUnits = ObjectMapper.Map<List<OrganizationUnitDto>>(allOrganizationUnits),
                MemberedOrganizationUnits = new List<string>()
            };

            if (!input.Id.HasValue)
            {
                //Creating a new user
                output.User = new InstallerEditDto
                {
                    IsActive = true,
                    ShouldChangePasswordOnNextLogin = true,
                    IsTwoFactorEnabled = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.TwoFactorLogin.IsEnabled),
                    IsLockoutEnabled = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.UserLockOut.IsEnabled)
                };

                foreach (var defaultRole in await _roleManager.Roles.Where(r => r.IsDefault).ToListAsync())
                {
                    var defaultUserRole = userRoleDtos.FirstOrDefault(ur => ur.RoleName == defaultRole.Name);
                    if (defaultUserRole != null)
                    {
                        defaultUserRole.IsAssigned = true;
                    }
                }

                output.UserIPAddress = new List<UserIPAddressForEdit>();
            }
            else
            {
                //Editing an existing user
                var user = await UserManager.GetUserByIdAsync(input.Id.Value);
                output.User = ObjectMapper.Map<InstallerEditDto>(user);

                var installerDetail = _installerDetail.GetAll().Where(e => e.UserId == user.Id).FirstOrDefault();
                if (installerDetail != null)
                {

                    output.User.UserId = installerDetail.UserId;
                    output.User.InstallerAccreditationNumber = installerDetail.InstallerAccreditationNumber;
                    output.User.InstallerAccreditationExpiryDate = installerDetail.InstallerAccreditationExpiryDate;
                    output.User.DesignerLicenseNumber = installerDetail.DesignerLicenseNumber;
                    output.User.DesignerLicenseExpiryDate = installerDetail.DesignerLicenseExpiryDate;
                    output.User.ElectricianLicenseNumber = installerDetail.ElectricianLicenseNumber;
                    output.User.ElectricianLicenseExpiryDate = installerDetail.ElectricianLicenseExpiryDate;
                    output.User.DocDesigner = installerDetail.DocDesigner;
                    output.User.DocElectrician = installerDetail.DocElectrician;
                    output.User.DocInstaller = installerDetail.DocInstaller;
                    output.User.IsDesi = installerDetail.IsDesi;
                    output.User.IsElec = installerDetail.IsElec;
                    output.User.IsInst = installerDetail.IsInst;
                    output.User.Unit = installerDetail.Unit;
                    output.User.UnitType = installerDetail.UnitType;
                    output.User.PostCode = installerDetail.PostCode;
                    output.User.StateId = installerDetail.StateId;
                    output.User.StateId = installerDetail.StateId;
                    output.User.State = _lookup_stateRepository.GetAll().Where(x => x.Id == installerDetail.StateId).Select(x => x.Name).FirstOrDefault();
                    output.User.StreetName = installerDetail.StreetName;
                    output.User.StreetNo = installerDetail.StreetNo;
                    output.User.StreetType = installerDetail.StreetType;
                    output.User.Suburb = installerDetail.Suburb;
                    output.User.Address = installerDetail.Address;
                    output.User.IsGoogle = installerDetail.IsGoogle;
                    output.User.latitude = installerDetail.latitude;
                    output.User.longitude = installerDetail.longitude;
                    output.User.OtherDocsCSV = installerDetail.OtherDocsCSV;
                    output.User.SourceTypeId = installerDetail.SourceTypeId;
                    output.User.SourceType = _lookup_jobTypeRepository.GetAll().Where(x => x.Id == installerDetail.SourceTypeId).Select(x => x.Name).FirstOrDefault();
                    output.User.AreaName = installerDetail.AreaName;
                    output.User.Notes = installerDetail.Notes;


                    var TenantName = _tenantRepository.GetAll().Where(e => e.Id == installerDetail.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                    output.User.FilePath = "\\Documents" + "\\" + TenantName + "\\Installer\\";
                    output.ProfilePictureId = user.ProfilePictureId;
                }
                long? createduserId = _userRepository.GetAll().Where(u => u.Id == input.Id).Select(u => u.CreatorUserId).FirstOrDefault();
                output.User.CreatedUser = createduserId != null ? _userRepository.GetAll().Where(u => u.Id == createduserId).Select(u => u.UserName).FirstOrDefault() : null;


                var organizationUnits = await UserManager.GetOrganizationUnitsAsync(user);
                output.MemberedOrganizationUnits = organizationUnits.Select(ou => ou.Code).ToList();

                var allRolesOfUsersOrganizationUnits = GetAllRoleNamesOfUsersOrganizationUnits(input.Id.Value);

                foreach (var userRoleDto in userRoleDtos)
                {
                    userRoleDto.IsAssigned = await UserManager.IsInRoleAsync(user, userRoleDto.RoleName);
                    userRoleDto.InheritedFromOrganizationUnit = allRolesOfUsersOrganizationUnits.Contains(userRoleDto.RoleName);
                }

                //IpAddress
                var userIp = _userIPAddressRepository.GetAll().Where(e => e.UserId == input.Id.Value);
                output.UserIPAddress = (from o in userIp
                                        select new UserIPAddressForEdit
                                        {
                                            IPAdress = o.IPAdress,
                                            IsActive = o.IsActive
                                        }).ToList();
            }

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Users_Create, AppPermissions.Pages_Administration_Users_Edit)]
        public async Task<GetUserForEditOutput> GetUserForEdit(NullableIdDto<long> input)
        {
            //Getting all available roles
            var userRoleDtos = await _roleManager.Roles
                //.Where(r => r.Name != "Sales Rep" && r.Name != "Sales Manager" && r.Name != "Installer")
                .OrderBy(r => r.DisplayName)
                .Select(r => new UserRoleDto
                {
                    RoleId = r.Id,
                    RoleName = r.Name,
                    RoleDisplayName = r.DisplayName
                })
                .ToArrayAsync();

            var allOrganizationUnits = await _organizationUnitRepository.GetAllListAsync();

            var output = new GetUserForEditOutput
            {
                Roles = userRoleDtos,
                AllOrganizationUnits = ObjectMapper.Map<List<OrganizationUnitDto>>(allOrganizationUnits),
                MemberedOrganizationUnits = new List<string>()
            };

            if (!input.Id.HasValue)
            {
                //Creating a new user
                output.User = new UserEditDto
                {
                    IsActive = true,
                    ShouldChangePasswordOnNextLogin = true,
                    IsTwoFactorEnabled = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.TwoFactorLogin.IsEnabled),
                    IsLockoutEnabled = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.UserLockOut.IsEnabled)
                };

                foreach (var defaultRole in await _roleManager.Roles.Where(r => r.IsDefault).ToListAsync())
                {
                    var defaultUserRole = userRoleDtos.FirstOrDefault(ur => ur.RoleName == defaultRole.Name);
                    if (defaultUserRole != null)
                    {
                        defaultUserRole.IsAssigned = true;
                    }
                }

                output.UserIPAddress = new List<UserIPAddressForEdit>();
                output.WarehoseLocations = new List<CommonLookupDto>();
            }
            else
            {
                //Editing an existing user
                var user = await UserManager.GetUserByIdAsync(input.Id.Value);

                output.User = ObjectMapper.Map<UserEditDto>(user);
                output.ProfilePictureId = user.ProfilePictureId;

                var organizationUnits = await UserManager.GetOrganizationUnitsAsync(user);
                output.MemberedOrganizationUnits = organizationUnits.Select(ou => ou.Code).ToList();

                var allRolesOfUsersOrganizationUnits = GetAllRoleNamesOfUsersOrganizationUnits(input.Id.Value);

                foreach (var userRoleDto in userRoleDtos)
                {
                    userRoleDto.IsAssigned = await UserManager.IsInRoleAsync(user, userRoleDto.RoleName);
                    userRoleDto.InheritedFromOrganizationUnit = allRolesOfUsersOrganizationUnits.Contains(userRoleDto.RoleName);
                }

                var userIp = _userIPAddressRepository.GetAll().Where(e => e.UserId == input.Id.Value);
                output.UserIPAddress = (from o in userIp
                                        select new UserIPAddressForEdit
                                        {
                                            IPAdress = o.IPAdress,
                                            IsActive = o.IsActive
                                        }).ToList();

                var locations = _userWarehouseLocationRepository.GetAll().Where(e => e.UserId == input.Id.Value);
                output.WarehoseLocations = (from o in locations
                                            select new CommonLookupDto
                                            {
                                                Id = (int)o.WarehouseLocationId,
                                                DisplayName = o.WarehouseLocationFk.state
                                            }).ToList();
            }

            return output;
        }

        public async Task<List<GetUserWiseOrgEditOutput>> UserWiseFromEmail(int UserId)
        {
            long organizationUnitId = 0;
            string organizationUnitsEmail = "";
            var joboldsysdetail = _UserWiseEmailOrgsRepository.GetAll().Where(x => x.UserId == UserId).ToList();
            if (UserId != null && UserId != 0)
            {
                organizationUnitId = _userOrganizationUnitRepository.GetAll().Where(x => x.UserId == UserId).Select(x => x.OrganizationUnitId).FirstOrDefault();
            }
            else
            {
                organizationUnitId = _userOrganizationUnitRepository.GetAll().Where(x => x.UserId == AbpSession.UserId).Select(x => x.OrganizationUnitId).FirstOrDefault();
            }
            if (organizationUnitId != null && organizationUnitId != 0)
            {
                organizationUnitsEmail = _extendedOrganizationUnitRepository.GetAll().Where(x => x.Id == organizationUnitId).Select(x => x.Email).FirstOrDefault();
            }

            var output = new List<GetUserWiseOrgEditOutput>();

            foreach (var item in joboldsysdetail)
            {
                var outobj = new GetUserWiseOrgEditOutput();
                outobj.UserEmailDetail = ObjectMapper.Map<CreateOrEditUserOrgDto>(item);
                outobj.UserEmailDetail.EmailFromAdress = (item.EmailFromAdress != null && item.EmailFromAdress != "" ? item.EmailFromAdress : organizationUnitsEmail);
                output.Add(outobj);
            }


            return output;
        }

        private List<string> GetAllRoleNamesOfUsersOrganizationUnits(long userId)
        {
            return (from userOu in _userOrganizationUnitRepository.GetAll()
                    join roleOu in _organizationUnitRoleRepository.GetAll() on userOu.OrganizationUnitId equals roleOu
                        .OrganizationUnitId
                    join userOuRoles in _roleRepository.GetAll() on roleOu.RoleId equals userOuRoles.Id
                    where userOu.UserId == userId
                    select userOuRoles.Name).ToList();
        }

        #region Permission - User -  Get, Update and Reset
        [AbpAuthorize(AppPermissions.Pages_Administration_Users_ChangePermissions)]
        public async Task<GetUserPermissionsForEditOutput> GetUserPermissionsForEdit(EntityDto<long> input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            var permissions = PermissionManager.GetAllPermissions();
            var grantedPermissions = await UserManager.GetGrantedPermissionsAsync(user);

            return new GetUserPermissionsForEditOutput
            {
                Permissions = ObjectMapper.Map<List<FlatPermissionDto>>(permissions).ToList(),
                GrantedPermissionNames = grantedPermissions.Select(p => p.Name).ToList()
            };
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Users_ChangePermissions)]
        public async Task ResetUserSpecificPermissions(EntityDto<long> input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            await UserManager.ResetAllPermissionsAsync(user);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Users_ChangePermissions)]
        public async Task UpdateUserPermissions(UpdateUserPermissionsInput input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            var grantedPermissions = PermissionManager.GetPermissionsFromNamesByValidating(input.GrantedPermissionNames);
            await UserManager.SetGrantedPermissionsAsync(user, grantedPermissions);
        }
        #endregion

        #region Permission - SalesRep -  Get, Update and Reset
        [AbpAuthorize(AppPermissions.Pages_Administration_SalesRep_ChangePermissions)]
        public async Task<GetUserPermissionsForEditOutput> GetSalesRepPermissionsForEdit(EntityDto<long> input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            var permissions = PermissionManager.GetAllPermissions();
            var grantedPermissions = await UserManager.GetGrantedPermissionsAsync(user);

            return new GetUserPermissionsForEditOutput
            {
                Permissions = ObjectMapper.Map<List<FlatPermissionDto>>(permissions).OrderBy(p => p.DisplayName).ToList(),
                GrantedPermissionNames = grantedPermissions.Select(p => p.Name).ToList()
            };
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_SalesRep_ChangePermissions)]
        public async Task ResetSalesRepSpecificPermissions(EntityDto<long> input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            await UserManager.ResetAllPermissionsAsync(user);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_SalesRep_ChangePermissions)]
        public async Task UpdateSalesRepPermissions(UpdateUserPermissionsInput input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            var grantedPermissions = PermissionManager.GetPermissionsFromNamesByValidating(input.GrantedPermissionNames);
            await UserManager.SetGrantedPermissionsAsync(user, grantedPermissions);
        }
        #endregion

        #region Permission - SalesManager -  Get, Update and Reset
        [AbpAuthorize(AppPermissions.Pages_Administration_SalesManager_ChangePermissions)]
        public async Task<GetUserPermissionsForEditOutput> GetSalesManagerPermissionsForEdit(EntityDto<long> input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            var permissions = PermissionManager.GetAllPermissions();
            var grantedPermissions = await UserManager.GetGrantedPermissionsAsync(user);

            return new GetUserPermissionsForEditOutput
            {
                Permissions = ObjectMapper.Map<List<FlatPermissionDto>>(permissions).OrderBy(p => p.DisplayName).ToList(),
                GrantedPermissionNames = grantedPermissions.Select(p => p.Name).ToList()
            };
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_SalesManager_ChangePermissions)]
        public async Task ResetSalesManagerSpecificPermissions(EntityDto<long> input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            await UserManager.ResetAllPermissionsAsync(user);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_SalesManager_ChangePermissions)]
        public async Task UpdateSalesManagerPermissions(UpdateUserPermissionsInput input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            var grantedPermissions = PermissionManager.GetPermissionsFromNamesByValidating(input.GrantedPermissionNames);
            await UserManager.SetGrantedPermissionsAsync(user, grantedPermissions);
        }
        #endregion

        #region Permission - Installer -  Get, Update and Reset
        [AbpAuthorize(AppPermissions.Pages_Administration_Installer_ChangePermissions)]
        public async Task<GetUserPermissionsForEditOutput> GetInstallerPermissionsForEdit(EntityDto<long> input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            var permissions = PermissionManager.GetAllPermissions();
            var grantedPermissions = await UserManager.GetGrantedPermissionsAsync(user);

            return new GetUserPermissionsForEditOutput
            {
                Permissions = ObjectMapper.Map<List<FlatPermissionDto>>(permissions).OrderBy(p => p.DisplayName).ToList(),
                GrantedPermissionNames = grantedPermissions.Select(p => p.Name).ToList()
            };
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Installer_ChangePermissions)]
        public async Task ResetInstallerSpecificPermissions(EntityDto<long> input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            await UserManager.ResetAllPermissionsAsync(user);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Installer_ChangePermissions)]
        public async Task UpdateInstallerPermissions(UpdateUserPermissionsInput input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            var grantedPermissions = PermissionManager.GetPermissionsFromNamesByValidating(input.GrantedPermissionNames);
            await UserManager.SetGrantedPermissionsAsync(user, grantedPermissions);
        }
        #endregion

        public async Task CreateOrUpdateUser(CreateOrUpdateUserInput input)
        {
            if (input.User.Id.HasValue)
            {
                await UpdateUserAsync(input);
            }
            else
            {
                await CreateUserAsync(input);
            }
        }

        public async Task CreateOrUpdateSalesRep(CreateOrUpdateUserInput input)
        {
            if (input.User.Id.HasValue)
            {
                await UpdateSalesRepAsync(input);
            }
            else
            {
                await CreateSalesRepAsync(input);
            }
        }

        public async Task CreateOrUpdateSalesManager(CreateOrUpdateUserInput input)
        {
            if (input.User.Id.HasValue)
            {
                await UpdateSalesManagerAsync(input);
            }
            else
            {
                await CreateSalesManagerAsync(input);
            }
        }

        public async Task CreateOrUpdateInstaller(CreateOrUpdateInstallerInput input)
        {
            if (input.User.Id.HasValue)
            {
                await UpdateInstallerAsync(input);
            }
            else
            {
                await CreateInstallerAsync(input);
            }
        }


        #region Delete with Permission : User, SalesRep, SalesManager and Installer
        [AbpAuthorize(AppPermissions.Pages_Administration_Users_Delete)]
        public async Task DeleteUser(EntityDto<long> input)
        {
            if (input.Id == AbpSession.GetUserId())
            {
                throw new UserFriendlyException(L("YouCanNotDeleteOwnAccount"));
            }

            var user = await UserManager.FindByIdAsync(input.Id.ToString());

            //Update user properties
            user.IsActive = false;
            CheckErrors(await UserManager.UpdateAsync(user));
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_SalesRep_Delete)]
        public async Task DeleteSalesRep(EntityDto<long> input)
        {
            if (input.Id == AbpSession.GetUserId())
            {
                throw new UserFriendlyException(L("YouCanNotDeleteOwnAccount"));
            }

            //var user = await UserManager.GetUserByIdAsync(input.Id);
            //CheckErrors(await UserManager.DeleteAsync(user));
            var user = await UserManager.FindByIdAsync(input.Id.ToString());

            //Update user properties
            user.IsActive = false;
            CheckErrors(await UserManager.UpdateAsync(user));
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_SalesManager_Delete)]
        public async Task DeleteSalesManager(EntityDto<long> input)
        {
            if (input.Id == AbpSession.GetUserId())
            {
                throw new UserFriendlyException(L("YouCanNotDeleteOwnAccount"));
            }

            //var user = await UserManager.GetUserByIdAsync(input.Id);
            //CheckErrors(await UserManager.DeleteAsync(user));
            var user = await UserManager.FindByIdAsync(input.Id.ToString());

            //Update user properties
            user.IsActive = false;
            CheckErrors(await UserManager.UpdateAsync(user));
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Installer_Delete)]
        public async Task DeleteInstaller(EntityDto<long> input)
        {
            if (input.Id == AbpSession.GetUserId())
            {
                throw new UserFriendlyException(L("YouCanNotDeleteOwnAccount"));
            }

            //var user = await UserManager.GetUserByIdAsync(input.Id);
            //CheckErrors(await UserManager.DeleteAsync(user));
            var user = await UserManager.FindByIdAsync(input.Id.ToString());

            //Update user properties
            user.IsActive = false;
            CheckErrors(await UserManager.UpdateAsync(user));
        }
        #endregion

        #region UnBlock With Permissioin : User, SalesRep, SalesManager and Installer
        [AbpAuthorize(AppPermissions.Pages_Administration_Users_Unlock)]
        public async Task UnlockUser(EntityDto<long> input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            user.Unlock();
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_SalesRep_Unlock)]
        public async Task UnlockSalesRep(EntityDto<long> input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            user.Unlock();
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_SalesManager_Unlock)]
        public async Task UnlockSalesManager(EntityDto<long> input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            user.Unlock();
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Installer_Unlock)]
        public async Task UnlockInstaller(EntityDto<long> input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            user.Unlock();
        }
        #endregion

        #region Update With Permission: User, SalesRep, SalesManager and Installer
        [AbpAuthorize(AppPermissions.Pages_Administration_Users_Edit)]
        protected virtual async Task UpdateUserAsync(CreateOrUpdateUserInput input)
        {
            Debug.Assert(input.User.Id != null, "input.User.Id should be set.");

            if (input.AssignedRoleNames.Length == 0)
            {
                throw new UserFriendlyException(L("RoleNameNotSelected"));
            }

            var user = await UserManager.FindByIdAsync(input.User.Id.Value.ToString());

            //Update user properties
            ObjectMapper.Map(input.User, user); //Passwords is not mapped (see mapping configuration)

            CheckErrors(await UserManager.UpdateAsync(user));

            if (input.SetRandomPassword)
            {
                var randomPassword = await _userManager.CreateRandomPassword();
                user.Password = _passwordHasher.HashPassword(user, randomPassword);
                input.User.Password = randomPassword;
            }
            else if (!input.User.Password.IsNullOrEmpty())
            {
                await UserManager.InitializeOptionsAsync(AbpSession.TenantId);
                CheckErrors(await UserManager.ChangePasswordAsync(user, input.User.Password));
            }

            //Update roles
            CheckErrors(await UserManager.SetRolesAsync(user, input.AssignedRoleNames));

            if (input.UserEmailDetail != null)
            {
                if (input.UserEmailDetail[0].OrganizationUnitId != null)
                {
                    _UserWiseEmailOrgsRepository.Delete(x => x.UserId == (long)input.User.Id);
                }
                foreach (var useremailorgdetail in input.UserEmailDetail)
                {
                    if (useremailorgdetail.OrganizationUnitId != null)
                    {
                        input.OrganizationUnits.Add(useremailorgdetail.OrganizationUnitId);
                        var detail = new UserWiseEmailOrg();
                        detail.UserId = (long)input.User.Id;
                        if (AbpSession.TenantId != null)
                        {
                            detail.TenantId = (int)AbpSession.TenantId;
                        }
                        detail.EmailFromAdress = useremailorgdetail.EmailFromAdress;
                        detail.OrganizationUnitId = useremailorgdetail.OrganizationUnitId;
                        _UserWiseEmailOrgsRepository.Insert(detail);
                    }
                }
            }

            if (!input.AssignedRoleNames.Contains("Admin"))
            {
                if (input.OrganizationUnits.Count == 0)
                {
                    throw new UserFriendlyException(L("OrganizationUnitsNotSelected"));
                }
            }

            user.CommitionAmount = input.User.CommitionAmount;

            //Ip Address
            await _userIPAddressRepository.HardDeleteAsync(e => e.UserId == (long)input.User.Id);
            if (input.UserIPAddress != null)
            {
                foreach (var ip in input.UserIPAddress)
                {
                    var userIpAddress = new UserIPAddress();
                    userIpAddress.UserId = (long)input.User.Id;
                    userIpAddress.IPAdress = ip.IPAdress;
                    userIpAddress.IsActive = ip.IsActive;

                    await _userIPAddressRepository.InsertAsync(userIpAddress);
                }
            }

            // Warehouse Location
            await _userWarehouseLocationRepository.HardDeleteAsync(e => e.UserId == (long)input.User.Id);
            if (input.WarehoseLocations != null)
            {
                foreach (var item in input.WarehoseLocations)
                {
                    var userWarehouseLocation = new UserWarehouseLocation();
                    userWarehouseLocation.UserId = (long)input.User.Id;
                    userWarehouseLocation.WarehouseLocationId = item.Id;

                    await _userWarehouseLocationRepository.InsertAsync(userWarehouseLocation);
                }
            }

            //Update organization units 
            await UserManager.SetOrganizationUnitsAsync(user, input.OrganizationUnits.ToArray());

            if (input.SendActivationEmail)
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(
                    user,
                    AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId),
                    input.User.Password
                );
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_SalesRep_Edit)]
        protected virtual async Task UpdateSalesRepAsync(CreateOrUpdateUserInput input)
        {
            Debug.Assert(input.User.Id != null, "input.User.Id should be set.");
            //if (input.OrganizationUnits.Count == 0)
            //{
            //    throw new UserFriendlyException(L("OrganizationUnitsNotSelected"));
            //}

            if (input.AssignedRoleNames.Length == 0)
            {
                throw new UserFriendlyException(L("RoleNameNotSelected"));
            }

            var user = await UserManager.FindByIdAsync(input.User.Id.Value.ToString());

            //Update user properties
            ObjectMapper.Map(input.User, user); //Passwords is not mapped (see mapping configuration)

            CheckErrors(await UserManager.UpdateAsync(user));

            if (input.SetRandomPassword)
            {
                var randomPassword = await _userManager.CreateRandomPassword();
                user.Password = _passwordHasher.HashPassword(user, randomPassword);
                input.User.Password = randomPassword;
            }
            else if (!input.User.Password.IsNullOrEmpty())
            {
                await UserManager.InitializeOptionsAsync(AbpSession.TenantId);
                CheckErrors(await UserManager.ChangePasswordAsync(user, input.User.Password));
            }

            //Update roles
            CheckErrors(await UserManager.SetRolesAsync(user, input.AssignedRoleNames));

            //Update Teams
            UserTeam userTeam;
            if (input.AssignedTeamNames != null)
            {
                _userTeamRepository.Delete(x => x.UserId == input.User.Id);

                foreach (var item in input.AssignedTeamNames)
                {
                    userTeam = new UserTeam();
                    userTeam.TenantId = AbpSession.TenantId;
                    userTeam.UserId = user.Id;
                    userTeam.TeamId = Convert.ToInt32(item.Value);
                    _userTeamRepository.InsertOrUpdate(userTeam);
                }
            }
            else
            {
                userTeam = new UserTeam();
                userTeam.TenantId = AbpSession.TenantId;
                userTeam.UserId = user.Id;
                userTeam.TeamId = Convert.ToInt32(input.TeamId);
                _userTeamRepository.InsertOrUpdate(userTeam);
            }

            if (input.UserEmailDetail != null)
            {
                if (input.UserEmailDetail[0].OrganizationUnitId != null)
                {
                    _UserWiseEmailOrgsRepository.Delete(x => x.UserId == (long)input.User.Id);
                }
                foreach (var useremailorgdetail in input.UserEmailDetail)
                {
                    if (useremailorgdetail.OrganizationUnitId != null)
                    {
                        input.OrganizationUnits.Add(useremailorgdetail.OrganizationUnitId);
                        var detail = new UserWiseEmailOrg();
                        detail.UserId = (long)input.User.Id;
                        if (AbpSession.TenantId != null)
                        {
                            detail.TenantId = (int)AbpSession.TenantId;
                        }
                        detail.EmailFromAdress = useremailorgdetail.EmailFromAdress;
                        detail.OrganizationUnitId = useremailorgdetail.OrganizationUnitId;
                        _UserWiseEmailOrgsRepository.Insert(detail);
                    }
                }
            }

            //Ip Address
            await _userIPAddressRepository.HardDeleteAsync(e => e.UserId == (long)input.User.Id);
            if (input.UserIPAddress != null)
            {
                foreach (var ip in input.UserIPAddress)
                {
                    var userIpAddress = new UserIPAddress();
                    userIpAddress.UserId = (long)input.User.Id;
                    userIpAddress.IPAdress = ip.IPAdress;
                    userIpAddress.IsActive = ip.IsActive;

                    await _userIPAddressRepository.InsertAsync(userIpAddress);
                }
            }

            if (input.OrganizationUnits.Count == 0)
            {
                throw new UserFriendlyException(L("OrganizationUnitsNotSelected"));
            }

            //update organization units 
            await UserManager.SetOrganizationUnitsAsync(user, input.OrganizationUnits.ToArray());

            if (input.SendActivationEmail)
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(
                    user,
                    AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId),
                    input.User.Password
                );
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_SalesManager_Edit)]
        protected virtual async Task UpdateSalesManagerAsync(CreateOrUpdateUserInput input)
        {
            Debug.Assert(input.User.Id != null, "input.User.Id should be set.");
            //if (input.OrganizationUnits.Count == 0)
            //{
            //    throw new UserFriendlyException(L("OrganizationUnitsNotSelected"));
            //}

            if (input.AssignedRoleNames.Length == 0)
            {
                throw new UserFriendlyException(L("RoleNameNotSelected"));
            }

            var user = await UserManager.FindByIdAsync(input.User.Id.Value.ToString());

            //Update user properties
            ObjectMapper.Map(input.User, user); //Passwords is not mapped (see mapping configuration)

            CheckErrors(await UserManager.UpdateAsync(user));

            if (input.SetRandomPassword)
            {
                var randomPassword = await _userManager.CreateRandomPassword();
                user.Password = _passwordHasher.HashPassword(user, randomPassword);
                input.User.Password = randomPassword;
            }
            else if (!input.User.Password.IsNullOrEmpty())
            {
                await UserManager.InitializeOptionsAsync(AbpSession.TenantId);
                CheckErrors(await UserManager.ChangePasswordAsync(user, input.User.Password));
            }

            //Update roles
            CheckErrors(await UserManager.SetRolesAsync(user, input.AssignedRoleNames));

            //Update Teams
            UserTeam userTeam;
            if (input.AssignedTeamNames != null)
            {
                _userTeamRepository.Delete(x => x.UserId == input.User.Id);

                foreach (var item in input.AssignedTeamNames)
                {
                    //int val = Convert.ToInt32(item.Value);
                    //var Exist = _userTeamRepository.GetAll().Where(e => e.UserId == input.User.Id && e.TeamId == val).Any();
                    //if (!Exist)
                    //{
                    userTeam = new UserTeam();
                    userTeam.TenantId = AbpSession.TenantId;
                    userTeam.UserId = user.Id;
                    userTeam.TeamId = Convert.ToInt32(item.Value);
                    _userTeamRepository.InsertOrUpdate(userTeam);
                    //}
                }
            }
            else
            {
                userTeam = new UserTeam();
                userTeam.TenantId = AbpSession.TenantId;
                userTeam.UserId = user.Id;
                userTeam.TeamId = Convert.ToInt32(input.TeamId);
                _userTeamRepository.InsertOrUpdate(userTeam);
            }

            if (input.UserEmailDetail != null)
            {
                if (input.UserEmailDetail[0].OrganizationUnitId != null)
                {
                    _UserWiseEmailOrgsRepository.Delete(x => x.UserId == (long)input.User.Id);
                }
                foreach (var useremailorgdetail in input.UserEmailDetail)
                {
                    if (useremailorgdetail.OrganizationUnitId != null)
                    {
                        input.OrganizationUnits.Add(useremailorgdetail.OrganizationUnitId);
                        var detail = new UserWiseEmailOrg();
                        detail.UserId = (long)input.User.Id;
                        if (AbpSession.TenantId != null)
                        {
                            detail.TenantId = (int)AbpSession.TenantId;
                        }
                        detail.EmailFromAdress = useremailorgdetail.EmailFromAdress;
                        detail.OrganizationUnitId = useremailorgdetail.OrganizationUnitId;
                        _UserWiseEmailOrgsRepository.Insert(detail);
                    }
                }
            }

            //Ip Address
            await _userIPAddressRepository.HardDeleteAsync(e => e.UserId == (long)input.User.Id);
            if (input.UserIPAddress != null)
            {
                foreach (var ip in input.UserIPAddress)
                {
                    var userIpAddress = new UserIPAddress();
                    userIpAddress.UserId = (long)input.User.Id;
                    userIpAddress.IPAdress = ip.IPAdress;
                    userIpAddress.IsActive = ip.IsActive;

                    await _userIPAddressRepository.InsertAsync(userIpAddress);
                }
            }

            if (input.OrganizationUnits.Count == 0)
            {
                throw new UserFriendlyException(L("OrganizationUnitsNotSelected"));
            }

            //update organization units 
            await UserManager.SetOrganizationUnitsAsync(user, input.OrganizationUnits.ToArray());

            if (input.SendActivationEmail)
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(
                    user,
                    AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId),
                    input.User.Password
                );
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Installer_Edit)]
        protected virtual async Task UpdateInstallerAsync(CreateOrUpdateInstallerInput input)
        {
            Debug.Assert(input.User.Id != null, "input.User.Id should be set.");

            input.User.InstallerAccreditationExpiryDate = (DateTime)_timeZoneConverter.Convert(input.User.InstallerAccreditationExpiryDate, (int)AbpSession.TenantId);
            input.User.DesignerLicenseExpiryDate = (DateTime)_timeZoneConverter.Convert(input.User.DesignerLicenseExpiryDate, (int)AbpSession.TenantId);
            input.User.ElectricianLicenseExpiryDate = (DateTime)_timeZoneConverter.Convert(input.User.ElectricianLicenseExpiryDate, (int)AbpSession.TenantId);

            var user = await UserManager.FindByIdAsync(input.User.Id.Value.ToString());
            if (input.AssignedRoleNames.Length == 0)
            {
                throw new UserFriendlyException(L("RoleNameNotSelected"));

            }

            //Ip Address
            await _userIPAddressRepository.HardDeleteAsync(e => e.UserId == (long)input.User.Id);
            if (input.UserIPAddress != null)
            {
                foreach (var ip in input.UserIPAddress)
                {
                    var userIpAddress = new UserIPAddress();
                    userIpAddress.UserId = (long)input.User.Id;
                    userIpAddress.IPAdress = ip.IPAdress;
                    userIpAddress.IsActive = ip.IsActive;

                    await _userIPAddressRepository.InsertAsync(userIpAddress);
                }
            }

            var InstallerByteArray = _tempFileCacheManager.GetFile(input.User.DocInstaller);
            var DesignerByteArray = _tempFileCacheManager.GetFile(input.User.DocDesigner);
            var ElectricianByteArray = _tempFileCacheManager.GetFile(input.User.DocElectrician);

            if (input.User.DocInstaller != null)
            {
                if (InstallerByteArray == null)
                {
                    throw new UserFriendlyException("There is no such file with the token: " + input.User.DocInstaller);
                }
                else if (InstallerByteArray.Length > MaxFileBytes)
                {
                    throw new UserFriendlyException("Document size exceeded");
                }
                var storedFile = new BinaryObject(AbpSession.TenantId, InstallerByteArray);
                await _binaryObjectManager.SaveAsync(storedFile);
            }

            if (input.User.DocDesigner != null)
            {
                if (DesignerByteArray == null)
                {
                    throw new UserFriendlyException("There is no such file with the token: " + input.User.DocDesigner);
                }
                else if (DesignerByteArray.Length > MaxFileBytes)
                {
                    throw new UserFriendlyException("Document size exceeded");
                }
                var storedFile = new BinaryObject(AbpSession.TenantId, DesignerByteArray);
                await _binaryObjectManager.SaveAsync(storedFile);
            }

            if (input.User.DocElectrician != null)
            {
                if (ElectricianByteArray == null)
                {
                    throw new UserFriendlyException("There is no such file with the token: " + input.User.DocElectrician);
                }
                else if (ElectricianByteArray.Length > MaxFileBytes)
                {
                    throw new UserFriendlyException("Document size exceeded");
                }
                var storedFile = new BinaryObject(AbpSession.TenantId, ElectricianByteArray);
                await _binaryObjectManager.SaveAsync(storedFile);
            }

            if (InstallerByteArray != null || DesignerByteArray != null || ElectricianByteArray != null)
            {
                var Path1 = @"s:\thesolarproductdocs";
                var MainFolder = Path1;
                MainFolder = MainFolder + "\\Documents";
                var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                string TenantPath = MainFolder + "\\" + TenantName;
                string InstallerPath = TenantPath + "\\Installer";
                string filenameinst = "";
                string filenameDesi = "";
                string filenameElect = "";
                string FinalinstFilePath = "";
                string FinalDesiFilePath = "";
                string FinalElectFilePath = "";
                if (input.User.DocInstallerFileName != null)
                {
                    var extInst = Path.GetExtension(input.User.DocInstallerFileName);
                    filenameinst = DateTime.Now.Ticks + "_" + "Installer" + extInst;
                    input.User.DocInstallerFileName = filenameinst;
                    FinalinstFilePath = InstallerPath + "\\" + filenameinst;
                }
                if (input.User.DocDesignerFileName != null)
                {
                    var extcDesi = Path.GetExtension(input.User.DocDesignerFileName);
                    filenameDesi = DateTime.Now.Ticks + "_" + "Installer" + extcDesi;
                    input.User.DocDesignerFileName = filenameDesi;
                    FinalDesiFilePath = InstallerPath + "\\" + filenameDesi;
                }
                if (input.User.DocElectricianFileName != null)
                {
                    var extElect = Path.GetExtension(input.User.DocElectricianFileName);
                    filenameElect = DateTime.Now.Ticks + "_" + "Installer" + extElect;
                    input.User.DocElectricianFileName = filenameElect;
                    FinalElectFilePath = InstallerPath + "\\" + filenameElect;
                }

                if (System.IO.Directory.Exists(MainFolder))
                {
                    if (System.IO.Directory.Exists(TenantPath))
                    {
                        if (System.IO.Directory.Exists(InstallerPath))
                        {
                            if (FinalinstFilePath != "")
                            {
                                using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                }
                            }
                            if (FinalDesiFilePath != "")
                            {
                                using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                }
                            }
                            if (FinalElectFilePath != "")
                            {
                                using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                }
                            }

                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(InstallerPath);
                            if (FinalinstFilePath != "")
                            {
                                using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                }
                            }
                            if (FinalDesiFilePath != "")
                            {
                                using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                }
                            }
                            if (FinalElectFilePath != "")
                            {
                                using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(TenantPath);
                        if (System.IO.Directory.Exists(InstallerPath))
                        {
                            if (System.IO.Directory.Exists(InstallerPath))
                            {
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(InstallerPath);
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(InstallerPath);
                            if (System.IO.Directory.Exists(InstallerPath))
                            {
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                            else
                            {
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(MainFolder);
                    if (System.IO.Directory.Exists(TenantPath))
                    {
                        if (System.IO.Directory.Exists(InstallerPath))
                        {
                            if (System.IO.Directory.Exists(InstallerPath))
                            {
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(InstallerPath);
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(InstallerPath);
                            if (System.IO.Directory.Exists(InstallerPath))
                            {
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(InstallerPath);
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(TenantPath);
                        if (System.IO.Directory.Exists(InstallerPath))
                        {
                            if (System.IO.Directory.Exists(InstallerPath))
                            {
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(InstallerPath);
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(InstallerPath);
                            if (System.IO.Directory.Exists(InstallerPath))
                            {
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(InstallerPath);
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // add activity log of user's fields

            MyInstallerActivityLog activity = new MyInstallerActivityLog();

            activity.ActionId = _lookup_leadActionRepository.GetAll().Where(x => x.ActionName == "My Installer Modified").Select(x => x.Id).FirstOrDefault();
            activity.ActionNote = "My Installer Activity Updated";
            activity.MyInstallerId = Convert.ToInt32(input.User.Id);
            if (AbpSession.TenantId != null)
            {
                activity.TenantId = (int)AbpSession.TenantId;
            }
            var myinstalleractionid = _myinstalleractivityRepository.InsertAndGetId(activity);

            var list = new List<MyInstallerActivityHistory>();

            if (user.Name != input.User.Name)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Name Updated";
                myinstallerhistory.PrevValue = user.Name;
                myinstallerhistory.CurValue = input.User.Name;
                myinstallerhistory.Action = "My Installer Name";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (user.Surname != input.User.Surname)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Surname Updated";
                myinstallerhistory.PrevValue = user.Surname;
                myinstallerhistory.CurValue = input.User.Surname;
                myinstallerhistory.Action = "My Installer Surname";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (user.CompanyName != input.User.CompanyName)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Company Name Updated";
                myinstallerhistory.PrevValue = user.CompanyName;
                myinstallerhistory.CurValue = input.User.CompanyName;
                myinstallerhistory.Action = "My Installer Company Name";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (user.EmailAddress != input.User.EmailAddress)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Email Address Updated";
                myinstallerhistory.PrevValue = user.EmailAddress;
                myinstallerhistory.CurValue = input.User.EmailAddress;
                myinstallerhistory.Action = "My Installer Email Address";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (user.PhoneNumber != input.User.PhoneNumber)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Phone Number Updated";
                myinstallerhistory.PrevValue = user.PhoneNumber;
                myinstallerhistory.CurValue = input.User.PhoneNumber;
                myinstallerhistory.Action = "My Installer Phone Number";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (user.Mobile != input.User.Mobile)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Mobile Updated";
                myinstallerhistory.PrevValue = user.Mobile;
                myinstallerhistory.CurValue = input.User.Mobile;
                myinstallerhistory.Action = "My Installer Mobile";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }
            if (input.User.ismyinstalleruser == true)
            {
                string[] items;
                if (user.UserName == "NULL")
                {
                    user.UserName = user.UserName + "-";
                    items = user.UserName.Split('-');
                    string a = items[0];
                    int b = (items[1] != "" ? Convert.ToInt32(items[1]) : 0) + 1;
                    input.User.UserName = "NULL-" + b;
                }
                //else
                //{
                //    //items = user.UserName.Split('-');
                //    //string a = items[0];
                //    //int b = (items[1] != "" ? Convert.ToInt32(items[1]) : 0) + 1;
                //    //input.User.UserName = "NULL-" + b;
                //}
            }
            else
            {
                user.UserName = input.User.UserName;
            }
            //Update user properties
            ObjectMapper.Map(input.User, user); //Passwords is not mapped (see mapping configuration)

            CheckErrors(await UserManager.UpdateAsync(user));

            if (input.SetRandomPassword)
            {
                var randomPassword = await _userManager.CreateRandomPassword();
                user.Password = _passwordHasher.HashPassword(user, randomPassword);
                input.User.Password = randomPassword;
            }
            else if (!input.User.Password.IsNullOrEmpty())
            {
                await UserManager.InitializeOptionsAsync(AbpSession.TenantId);
                CheckErrors(await UserManager.ChangePasswordAsync(user, input.User.Password));
            }

            //Update roles
            CheckErrors(await UserManager.SetRolesAsync(user, input.AssignedRoleNames));

            //var userObj = _userRepository.GetAll().Where(e => e.Id == input.User.Id).FirstOrDefault();
            var installerdetailObj = _installerDetail.GetAll().Where(e => e.UserId == input.User.Id).FirstOrDefault();

            if (installerdetailObj.SourceTypeId != input.User.SourceTypeId)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Source Type Updated";
                myinstallerhistory.PrevValue = _lookup_jobTypeRepository.GetAll().Where(e => e.Id == installerdetailObj.SourceTypeId).Select(e => e.Name).FirstOrDefault();
                myinstallerhistory.CurValue = _lookup_jobTypeRepository.GetAll().Where(e => e.Id == input.User.SourceTypeId).Select(e => e.Name).FirstOrDefault();
                myinstallerhistory.Action = "My Installer Source Type";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.AreaName != input.User.AreaName)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Area Name Updated";
                myinstallerhistory.PrevValue = installerdetailObj.AreaName;
                myinstallerhistory.CurValue = input.User.AreaName;
                myinstallerhistory.Action = "My Installer Area Name";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.Notes != input.User.Notes)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Notes Updated";
                myinstallerhistory.PrevValue = installerdetailObj.Notes;
                myinstallerhistory.CurValue = input.User.Notes;
                myinstallerhistory.Action = "My Installer Notes";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.IsGoogle != input.User.IsGoogle)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "IsGoogle Field Updated";
                myinstallerhistory.PrevValue = installerdetailObj.IsGoogle;
                myinstallerhistory.CurValue = input.User.IsGoogle;
                myinstallerhistory.Action = "My Installer IsGoogle Field";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.Unit != input.User.Unit)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Unit Updated";
                myinstallerhistory.PrevValue = installerdetailObj.Unit;
                myinstallerhistory.CurValue = input.User.Unit;
                myinstallerhistory.Action = "My Installer Unit";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.UnitType != input.User.UnitType)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Unit Type Updated";
                myinstallerhistory.PrevValue = installerdetailObj.UnitType;
                myinstallerhistory.CurValue = input.User.UnitType;
                myinstallerhistory.Action = "My Installer Unit Type";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.StreetNo != input.User.StreetNo)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Street No Updated";
                myinstallerhistory.PrevValue = installerdetailObj.StreetNo;
                myinstallerhistory.CurValue = input.User.StreetNo;
                myinstallerhistory.Action = "My Installer Street No";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.StreetName != input.User.StreetName)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Street Name Updated";
                myinstallerhistory.PrevValue = installerdetailObj.StreetName;
                myinstallerhistory.CurValue = input.User.StreetName;
                myinstallerhistory.Action = "My Installer Street Name";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.StreetType != input.User.StreetType)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Street Type Updated";
                myinstallerhistory.PrevValue = installerdetailObj.StreetType;
                myinstallerhistory.CurValue = input.User.StreetType;
                myinstallerhistory.Action = "My Installer Street Type";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.Suburb != input.User.Suburb)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Suburb Updated";
                myinstallerhistory.PrevValue = installerdetailObj.Suburb;
                myinstallerhistory.CurValue = input.User.Suburb;
                myinstallerhistory.Action = "My Installer Suburb";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.StateId != input.User.StateId)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "State Updated";
                myinstallerhistory.PrevValue = installerdetailObj.StateId.ToString();
                myinstallerhistory.CurValue = input.User.StateId.ToString();
                myinstallerhistory.Action = "My Installer State";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.PostCode != input.User.PostCode)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "PostCode Updated";
                myinstallerhistory.PrevValue = installerdetailObj.PostCode;
                myinstallerhistory.CurValue = input.User.PostCode;
                myinstallerhistory.Action = "My Installer PostCode";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.IsInst != input.User.IsInst)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "IsInst Updated";
                myinstallerhistory.PrevValue = installerdetailObj.IsInst.ToString();
                myinstallerhistory.CurValue = input.User.IsInst.ToString();
                myinstallerhistory.Action = "My Installer IsInst";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.IsDesi != input.User.IsDesi)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "IsDesi Updated";
                myinstallerhistory.PrevValue = installerdetailObj.IsDesi.ToString();
                myinstallerhistory.CurValue = input.User.IsDesi.ToString();
                myinstallerhistory.Action = "My Installer IsDesi";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.IsElec != input.User.IsElec)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "IsElec Updated";
                myinstallerhistory.PrevValue = installerdetailObj.IsElec.ToString();
                myinstallerhistory.CurValue = input.User.IsElec.ToString();
                myinstallerhistory.Action = "My Installer IsElec";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.InstallerAccreditationNumber != input.User.InstallerAccreditationNumber)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Installer Accreditation Number Updated";
                myinstallerhistory.PrevValue = installerdetailObj.InstallerAccreditationNumber;
                myinstallerhistory.CurValue = input.User.InstallerAccreditationNumber;
                myinstallerhistory.Action = "My Installer Installer Accreditation Number";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.InstallerAccreditationExpiryDate != input.User.InstallerAccreditationExpiryDate)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Installer Accreditation Expiry Date Updated";
                myinstallerhistory.PrevValue = installerdetailObj.InstallerAccreditationExpiryDate.ToString();
                myinstallerhistory.CurValue = input.User.InstallerAccreditationExpiryDate.ToString();
                myinstallerhistory.Action = "My Installer Installer Accreditation Expiry Date";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.DesignerLicenseNumber != input.User.DesignerLicenseNumber)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Designer License Number Updated";
                myinstallerhistory.PrevValue = installerdetailObj.DesignerLicenseNumber;
                myinstallerhistory.CurValue = input.User.DesignerLicenseNumber;
                myinstallerhistory.Action = "My Installer Designer License Number";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.DesignerLicenseExpiryDate != input.User.DesignerLicenseExpiryDate)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Designer License Expiry Date Updated";
                myinstallerhistory.PrevValue = installerdetailObj.DesignerLicenseExpiryDate.ToString();
                myinstallerhistory.CurValue = input.User.DesignerLicenseExpiryDate.ToString();
                myinstallerhistory.Action = "My Installer Designer License Expiry Date";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.ElectricianLicenseNumber != input.User.ElectricianLicenseNumber)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Electrician License Number Updated";
                myinstallerhistory.PrevValue = installerdetailObj.ElectricianLicenseNumber;
                myinstallerhistory.CurValue = input.User.ElectricianLicenseNumber;
                myinstallerhistory.Action = "My Installer Electrician License Number";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (installerdetailObj.ElectricianLicenseExpiryDate != input.User.ElectricianLicenseExpiryDate)
            {
                MyInstallerActivityHistory myinstallerhistory = new MyInstallerActivityHistory();
                if (AbpSession.TenantId != null)
                {
                    myinstallerhistory.TenantId = (int)AbpSession.TenantId;
                }
                myinstallerhistory.FieldName = "Electrician License Expiry Date Updated";
                myinstallerhistory.PrevValue = installerdetailObj.ElectricianLicenseExpiryDate.ToString();
                myinstallerhistory.CurValue = input.User.ElectricianLicenseExpiryDate.ToString();
                myinstallerhistory.Action = "My Installer Electrician License Expiry Date";
                myinstallerhistory.LastmodifiedDateTime = DateTime.Now;
                myinstallerhistory.MyInstallerId = Convert.ToInt32(input.User.Id);
                myinstallerhistory.MyInstallerActionId = myinstalleractionid;
                list.Add(myinstallerhistory);
            }

            if (list.Count > 0)
            {
                await _dbcontextprovider.GetDbContext().MyInstallerActivityHistories.AddRangeAsync(list);
                await _dbcontextprovider.GetDbContext().SaveChangesAsync();
            }

            if (input.User != null)
            {
                var installerDetail = _installerDetail.GetAll().Where(e => e.UserId == input.User.Id).FirstOrDefault();

                installerDetail.UserId = user.Id;
                installerDetail.TenantId = (int)user.TenantId;
                installerDetail.IsInst = input.User.IsInst;
                installerDetail.IsElec = input.User.IsElec;
                installerDetail.IsDesi = input.User.IsDesi;
                installerDetail.InstallerAccreditationNumber = input.User.InstallerAccreditationNumber;
                installerDetail.InstallerAccreditationExpiryDate = input.User.InstallerAccreditationExpiryDate;
                installerDetail.DesignerLicenseNumber = input.User.DesignerLicenseNumber;
                installerDetail.DesignerLicenseExpiryDate = input.User.DesignerLicenseExpiryDate;
                installerDetail.ElectricianLicenseNumber = input.User.ElectricianLicenseNumber;
                installerDetail.ElectricianLicenseExpiryDate = input.User.ElectricianLicenseExpiryDate;
                if (input.User.DocInstallerFileName != null)
                {
                    installerDetail.DocInstaller = input.User.DocInstallerFileName;
                }
                if (input.User.DocDesignerFileName != null)
                {
                    installerDetail.DocDesigner = input.User.DocDesignerFileName;
                }
                if (input.User.DocElectricianFileName != null)
                {

                    installerDetail.DocElectrician = input.User.DocElectricianFileName;
                }

                installerDetail.OtherDocsCSV = input.User.OtherDocsCSV;
                installerDetail.Unit = input.User.Unit;
                installerDetail.IsGoogle = input.User.IsGoogle;
                installerDetail.latitude = input.User.latitude;
                installerDetail.longitude = input.User.longitude;
                installerDetail.UnitType = input.User.UnitType;
                installerDetail.StreetNo = input.User.StreetNo;
                installerDetail.StreetName = input.User.StreetName;
                installerDetail.Suburb = input.User.Suburb;
                installerDetail.StateId = input.User.StateId;
                installerDetail.PostCode = input.User.PostCode;
                installerDetail.Address = input.User.Address;
                installerDetail.StreetType = input.User.StreetType;
                installerDetail.SourceTypeId = input.User.SourceTypeId;
                installerDetail.AreaName = input.User.AreaName;
                installerDetail.Notes = input.User.Notes;

                await _installerDetail.UpdateAsync(installerDetail);
            }


            if (input.UserEmailDetail != null)
            {
                if (input.UserEmailDetail[0].OrganizationUnitId != null)
                {
                    _UserWiseEmailOrgsRepository.Delete(x => x.UserId == (long)input.User.Id);
                }
                foreach (var useremailorgdetail in input.UserEmailDetail)
                {
                    if (useremailorgdetail.OrganizationUnitId != null && useremailorgdetail.OrganizationUnitId != 0)
                    {
                        input.OrganizationUnits.Add(useremailorgdetail.OrganizationUnitId);
                        var detail = new UserWiseEmailOrg();
                        detail.UserId = (long)input.User.Id;
                        if (AbpSession.TenantId != null)
                        {
                            detail.TenantId = (int)AbpSession.TenantId;
                        }
                        detail.EmailFromAdress = useremailorgdetail.EmailFromAdress;
                        detail.OrganizationUnitId = useremailorgdetail.OrganizationUnitId;
                        _UserWiseEmailOrgsRepository.Insert(detail);
                    }
                }
            }

            //if (input.User.ismyinstalleruser == false)
            //{
            if (input.OrganizationUnits.Count == 0)
            {
                throw new UserFriendlyException(L("OrganizationUnitsNotSelected"));
            }

            //update organization units 
            await UserManager.SetOrganizationUnitsAsync(user, input.OrganizationUnits.ToArray());
            //}

            if (input.SendActivationEmail)
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(
                    user,
                    AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId),
                    input.User.Password
                );
            }
        }


        #endregion

        #region Create With Permission : User, SalesRep, SalesManager and Installer
        [AbpAuthorize(AppPermissions.Pages_Administration_Users_Create)]
        protected virtual async Task CreateUserAsync(CreateOrUpdateUserInput input)
        {
            input.User.Target = "-";
            input.User.CategoryId = 0;
            input.User.MonthlySMSQuote = 0;
            input.User.MonthlyEMailQuote = 0;

            if (AbpSession.TenantId.HasValue)
            {
                await _userPolicy.CheckMaxUserCountAsync(AbpSession.GetTenantId());
            }


            if (input.AssignedRoleNames.Length == 0)
            {
                throw new UserFriendlyException(L("RoleNameNotSelected"));
            }

            var user = ObjectMapper.Map<User>(input.User); //Passwords is not mapped (see mapping configuration)
            user.TenantId = AbpSession.TenantId;

            //Set password
            if (input.SetRandomPassword)
            {
                var randomPassword = await _userManager.CreateRandomPassword();
                user.Password = _passwordHasher.HashPassword(user, randomPassword);
                input.User.Password = randomPassword;
            }
            else if (!input.User.Password.IsNullOrEmpty())
            {
                await UserManager.InitializeOptionsAsync(AbpSession.TenantId);
                foreach (var validator in _passwordValidators)
                {
                    CheckErrors(await validator.ValidateAsync(UserManager, user, input.User.Password));
                }

                user.Password = _passwordHasher.HashPassword(user, input.User.Password);
            }

            user.ShouldChangePasswordOnNextLogin = input.User.ShouldChangePasswordOnNextLogin;

            //Assign roles
            user.Roles = new Collection<UserRole>();
            foreach (var roleName in input.AssignedRoleNames)
            {
                var role = await _roleManager.GetRoleByNameAsync(roleName);
                user.Roles.Add(new UserRole(AbpSession.TenantId, user.Id, role.Id));
            }

            CheckErrors(await UserManager.CreateAsync(user));
            await CurrentUnitOfWork.SaveChangesAsync(); //To get new user's Id.

            ////Assign Teams
            //UserTeam userTeam;
            //if (input.AssignedTeamNames != null)
            //{
            //    foreach (var item in input.AssignedTeamNames)
            //    {
            //        userTeam = new UserTeam();
            //        userTeam.TenantId = AbpSession.TenantId;
            //        userTeam.UserId = user.Id;
            //        userTeam.TeamId = Convert.ToInt32(item.Value);
            //        _userTeamRepository.InsertOrUpdate(userTeam);
            //    }
            //}
            //else
            //{
            //    userTeam = new UserTeam();
            //    userTeam.TenantId = AbpSession.TenantId;
            //    userTeam.UserId = user.Id;
            //    userTeam.TeamId = Convert.ToInt32(input.TeamId);
            //    _userTeamRepository.InsertOrUpdate(userTeam);
            //}

            //Notifications
            await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(user.ToUserIdentifier());
            await _appNotifier.WelcomeToTheApplicationAsync(user);

            if (input.UserEmailDetail != null)
            {
                foreach (var useremailorgdetail in input.UserEmailDetail)
                {
                    input.OrganizationUnits.Add(useremailorgdetail.OrganizationUnitId);
                    if (useremailorgdetail.OrganizationUnitId != null)
                    {
                        var detail = new UserWiseEmailOrg();
                        detail.UserId = (long)user.Id;
                        if (AbpSession.TenantId != null)
                        {
                            detail.TenantId = (int)AbpSession.TenantId;
                        }
                        detail.EmailFromAdress = useremailorgdetail.EmailFromAdress;
                        detail.OrganizationUnitId = useremailorgdetail.OrganizationUnitId;
                        _UserWiseEmailOrgsRepository.Insert(detail);
                    }
                }
            }

            //IP Address
            if (input.UserIPAddress != null)
            {
                foreach (var ip in input.UserIPAddress)
                {
                    var userIpAddress = new UserIPAddress();
                    userIpAddress.UserId = (long)user.Id;
                    userIpAddress.IPAdress = ip.IPAdress;
                    userIpAddress.IsActive = ip.IsActive;

                    await _userIPAddressRepository.InsertAsync(userIpAddress);
                }
            }

            // Warehouse Location
            if (input.WarehoseLocations != null)
            {
                foreach (var item in input.WarehoseLocations)
                {
                    var userWarehouseLocation = new UserWarehouseLocation();
                    userWarehouseLocation.UserId = (long)user.Id;
                    userWarehouseLocation.WarehouseLocationId = item.Id;

                    await _userWarehouseLocationRepository.InsertAsync(userWarehouseLocation);
                }
            }

            if (!input.AssignedRoleNames.Contains("Admin"))
            {
                if (input.OrganizationUnits.Count == 0)
                {
                    throw new UserFriendlyException(L("OrganizationUnitsNotSelected"));
                }
            }

            //Organization Units
            await UserManager.SetOrganizationUnitsAsync(user, input.OrganizationUnits.ToArray());

            //Send activation email
            if (input.SendActivationEmail)
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(
                    user,
                    AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId),
                    input.User.Password
                );
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_SalesRep_Create)]
        protected virtual async Task CreateSalesRepAsync(CreateOrUpdateUserInput input)
        {
            if (AbpSession.TenantId.HasValue)
            {
                await _userPolicy.CheckMaxUserCountAsync(AbpSession.GetTenantId());
            }


            if (input.OrganizationUnits.Count > 1)
            {
                throw new UserFriendlyException(L("OnlyOneOrganizationUnitPerSalesRep"));
            }

            if (input.AssignedRoleNames.Length == 0)
            {
                throw new UserFriendlyException(L("RoleNameNotSelected"));
            }

            var user = ObjectMapper.Map<User>(input.User); //Passwords is not mapped (see mapping configuration)
            user.TenantId = AbpSession.TenantId;

            //Set password
            if (input.SetRandomPassword)
            {
                var randomPassword = await _userManager.CreateRandomPassword();
                user.Password = _passwordHasher.HashPassword(user, randomPassword);
                input.User.Password = randomPassword;
            }
            else if (!input.User.Password.IsNullOrEmpty())
            {
                await UserManager.InitializeOptionsAsync(AbpSession.TenantId);
                foreach (var validator in _passwordValidators)
                {
                    CheckErrors(await validator.ValidateAsync(UserManager, user, input.User.Password));
                }

                user.Password = _passwordHasher.HashPassword(user, input.User.Password);
            }

            user.ShouldChangePasswordOnNextLogin = input.User.ShouldChangePasswordOnNextLogin;

            //Assign roles
            user.Roles = new Collection<UserRole>();
            foreach (var roleName in input.AssignedRoleNames)
            {
                var role = await _roleManager.GetRoleByNameAsync(roleName);
                user.Roles.Add(new UserRole(AbpSession.TenantId, user.Id, role.Id));
            }

            CheckErrors(await UserManager.CreateAsync(user));
            await CurrentUnitOfWork.SaveChangesAsync(); //To get new user's Id.

            //Assign Teams
            UserTeam userTeam;
            if (input.AssignedTeamNames != null)
            {
                foreach (var item in input.AssignedTeamNames)
                {
                    userTeam = new UserTeam();
                    userTeam.TenantId = AbpSession.TenantId;
                    userTeam.UserId = user.Id;
                    userTeam.TeamId = Convert.ToInt32(item.Value);
                    _userTeamRepository.InsertOrUpdate(userTeam);
                }
            }
            else
            {
                userTeam = new UserTeam();
                userTeam.TenantId = AbpSession.TenantId;
                userTeam.UserId = user.Id;
                userTeam.TeamId = Convert.ToInt32(input.TeamId);
                _userTeamRepository.InsertOrUpdate(userTeam);
            }

            //Notifications
            await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(user.ToUserIdentifier());
            await _appNotifier.WelcomeToTheApplicationAsync(user);

            if (input.UserEmailDetail != null)
            {
                foreach (var useremailorgdetail in input.UserEmailDetail)
                {
                    input.OrganizationUnits.Add(useremailorgdetail.OrganizationUnitId);
                    if (useremailorgdetail.OrganizationUnitId != null)
                    {
                        var detail = new UserWiseEmailOrg();
                        detail.UserId = (long)user.Id;
                        if (AbpSession.TenantId != null)
                        {
                            detail.TenantId = (int)AbpSession.TenantId;
                        }
                        detail.EmailFromAdress = useremailorgdetail.EmailFromAdress;
                        detail.OrganizationUnitId = useremailorgdetail.OrganizationUnitId;
                        _UserWiseEmailOrgsRepository.Insert(detail);
                    }
                }
            }

            //IP Address
            if (input.UserIPAddress != null)
            {
                foreach (var ip in input.UserIPAddress)
                {
                    var userIpAddress = new UserIPAddress();
                    userIpAddress.UserId = (long)input.User.Id;
                    userIpAddress.IPAdress = ip.IPAdress;
                    userIpAddress.IsActive = ip.IsActive;

                    await _userIPAddressRepository.InsertAsync(userIpAddress);
                }
            }

            if (input.OrganizationUnits.Count == 0)
            {
                throw new UserFriendlyException(L("OrganizationUnitsNotSelected"));
            }


            //Organization Units
            await UserManager.SetOrganizationUnitsAsync(user, input.OrganizationUnits.ToArray());

            //Send activation email
            if (input.SendActivationEmail)
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(
                    user,
                    AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId),
                    input.User.Password
                );
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_SalesManager_Create)]
        protected virtual async Task CreateSalesManagerAsync(CreateOrUpdateUserInput input)
        {
            if (AbpSession.TenantId.HasValue)
            {
                await _userPolicy.CheckMaxUserCountAsync(AbpSession.GetTenantId());
            }
            //if (input.OrganizationUnits.Count == 0)
            //{
            //    throw new UserFriendlyException(L("OrganizationUnitsNotSelected"));
            //}
            if (input.OrganizationUnits.Count > 1)
            {
                throw new UserFriendlyException(L("OnlyOneOrganizationUnitPerSalesManager"));
            }

            if (input.AssignedRoleNames.Length == 0)
            {
                throw new UserFriendlyException(L("RoleNameNotSelected"));
            }

            var user = ObjectMapper.Map<User>(input.User); //Passwords is not mapped (see mapping configuration)
            user.TenantId = AbpSession.TenantId;

            //Set password
            if (input.SetRandomPassword)
            {
                var randomPassword = await _userManager.CreateRandomPassword();
                user.Password = _passwordHasher.HashPassword(user, randomPassword);
                input.User.Password = randomPassword;
            }
            else if (!input.User.Password.IsNullOrEmpty())
            {
                await UserManager.InitializeOptionsAsync(AbpSession.TenantId);
                foreach (var validator in _passwordValidators)
                {
                    CheckErrors(await validator.ValidateAsync(UserManager, user, input.User.Password));
                }

                user.Password = _passwordHasher.HashPassword(user, input.User.Password);
            }

            user.ShouldChangePasswordOnNextLogin = input.User.ShouldChangePasswordOnNextLogin;

            //Assign roles
            user.Roles = new Collection<UserRole>();
            foreach (var roleName in input.AssignedRoleNames)
            {
                var role = await _roleManager.GetRoleByNameAsync(roleName);
                user.Roles.Add(new UserRole(AbpSession.TenantId, user.Id, role.Id));
            }

            CheckErrors(await UserManager.CreateAsync(user));
            await CurrentUnitOfWork.SaveChangesAsync(); //To get new user's Id.

            //Assign Teams
            UserTeam userTeam;
            if (input.AssignedTeamNames != null)
            {
                foreach (var item in input.AssignedTeamNames)
                {
                    userTeam = new UserTeam();
                    userTeam.TenantId = AbpSession.TenantId;
                    userTeam.UserId = user.Id;
                    userTeam.TeamId = Convert.ToInt32(item.Value);
                    _userTeamRepository.InsertOrUpdate(userTeam);
                }
            }
            else
            {
                userTeam = new UserTeam();
                userTeam.TenantId = AbpSession.TenantId;
                userTeam.UserId = user.Id;
                userTeam.TeamId = Convert.ToInt32(input.TeamId);
                _userTeamRepository.InsertOrUpdate(userTeam);
            }

            //Notifications
            await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(user.ToUserIdentifier());
            await _appNotifier.WelcomeToTheApplicationAsync(user);


            if (input.UserEmailDetail != null)
            {
                foreach (var useremailorgdetail in input.UserEmailDetail)
                {
                    input.OrganizationUnits.Add(useremailorgdetail.OrganizationUnitId);
                    if (useremailorgdetail.OrganizationUnitId != null)
                    {
                        var detail = new UserWiseEmailOrg();
                        detail.UserId = (long)user.Id;
                        if (AbpSession.TenantId != null)
                        {
                            detail.TenantId = (int)AbpSession.TenantId;
                        }
                        detail.EmailFromAdress = useremailorgdetail.EmailFromAdress;
                        detail.OrganizationUnitId = useremailorgdetail.OrganizationUnitId;
                        _UserWiseEmailOrgsRepository.Insert(detail);
                    }
                }
            }

            //IP Address
            if (input.UserIPAddress != null)
            {
                foreach (var ip in input.UserIPAddress)
                {
                    var userIpAddress = new UserIPAddress();
                    userIpAddress.UserId = (long)input.User.Id;
                    userIpAddress.IPAdress = ip.IPAdress;
                    userIpAddress.IsActive = ip.IsActive;

                    await _userIPAddressRepository.InsertAsync(userIpAddress);
                }
            }

            if (input.OrganizationUnits.Count == 0)
            {
                throw new UserFriendlyException(L("OrganizationUnitsNotSelected"));
            }


            //Organization Units
            await UserManager.SetOrganizationUnitsAsync(user, input.OrganizationUnits.ToArray());

            //Send activation email
            if (input.SendActivationEmail)
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(
                    user,
                    AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId),
                    input.User.Password
                );
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Installer_Create)]
        protected virtual async Task CreateInstallerAsync(CreateOrUpdateInstallerInput input)
        {

            input.User.InstallerAccreditationExpiryDate = (DateTime)_timeZoneConverter.Convert(input.User.InstallerAccreditationExpiryDate, (int)AbpSession.TenantId);
            input.User.DesignerLicenseExpiryDate = (DateTime)_timeZoneConverter.Convert(input.User.DesignerLicenseExpiryDate, (int)AbpSession.TenantId);
            input.User.ElectricianLicenseExpiryDate = (DateTime)_timeZoneConverter.Convert(input.User.ElectricianLicenseExpiryDate, (int)AbpSession.TenantId);

            if (AbpSession.TenantId.HasValue)
            {
                await _userPolicy.CheckMaxUserCountAsync(AbpSession.GetTenantId());
            }

            if (input.AssignedRoleNames.Length == 0)
            {
                throw new UserFriendlyException(L("RoleNameNotSelected"));
            }

            var InstallerByteArray = _tempFileCacheManager.GetFile(input.User.DocInstaller);
            var DesignerByteArray = _tempFileCacheManager.GetFile(input.User.DocDesigner);
            var ElectricianByteArray = _tempFileCacheManager.GetFile(input.User.DocElectrician);

            if (input.User.DocInstaller != null)
            {
                if (InstallerByteArray == null)
                {
                    throw new UserFriendlyException("There is no such file with the token: " + input.User.DocInstaller);
                }
                else if (InstallerByteArray.Length > MaxFileBytes)
                {
                    throw new UserFriendlyException("Document size exceeded");
                }
                var storedFile = new BinaryObject(AbpSession.TenantId, InstallerByteArray);
                await _binaryObjectManager.SaveAsync(storedFile);
            }

            if (input.User.DocDesigner != null)
            {
                if (DesignerByteArray == null)
                {
                    throw new UserFriendlyException("There is no such file with the token: " + input.User.DocDesigner);
                }
                else if (DesignerByteArray.Length > MaxFileBytes)
                {
                    throw new UserFriendlyException("Document size exceeded");
                }
                var storedFile = new BinaryObject(AbpSession.TenantId, DesignerByteArray);
                await _binaryObjectManager.SaveAsync(storedFile);
            }

            if (input.User.DocElectrician != null)
            {
                if (ElectricianByteArray == null)
                {
                    throw new UserFriendlyException("There is no such file with the token: " + input.User.DocElectrician);
                }
                else if (ElectricianByteArray.Length > MaxFileBytes)
                {
                    throw new UserFriendlyException("Document size exceeded");
                }
                var storedFile = new BinaryObject(AbpSession.TenantId, ElectricianByteArray);
                await _binaryObjectManager.SaveAsync(storedFile);
            }

            if (InstallerByteArray != null || DesignerByteArray != null || ElectricianByteArray != null)
            {
                var Path1 = @"s:\thesolarproductdocs";
                var MainFolder = Path1;
                MainFolder = MainFolder + "\\Documents";
                var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                string TenantPath = MainFolder + "\\" + TenantName;
                string InstallerPath = TenantPath + "\\Installer";
                string filenameinst = "";
                string filenameDesi = "";
                string filenameElect = "";
                string FinalinstFilePath = "";
                string FinalDesiFilePath = "";
                string FinalElectFilePath = "";
                if (input.User.DocInstallerFileName != null)
                {
                    var extInst = Path.GetExtension(input.User.DocInstallerFileName);
                    filenameinst = DateTime.Now.Ticks + "_" + "Installer" + extInst;
                    input.User.DocInstallerFileName = filenameinst;
                    FinalinstFilePath = InstallerPath + "\\" + filenameinst;
                }
                if (input.User.DocDesignerFileName != null)
                {
                    var extcDesi = Path.GetExtension(input.User.DocDesignerFileName);
                    filenameDesi = DateTime.Now.Ticks + "_" + "Installer" + extcDesi;
                    input.User.DocDesignerFileName = filenameDesi;
                    FinalDesiFilePath = InstallerPath + "\\" + filenameDesi;
                }
                if (input.User.DocElectricianFileName != null)
                {
                    var extElect = Path.GetExtension(input.User.DocElectricianFileName);
                    filenameElect = DateTime.Now.Ticks + "_" + "Installer" + extElect;
                    input.User.DocElectricianFileName = filenameElect;
                    FinalElectFilePath = InstallerPath + "\\" + filenameElect;
                }

                if (System.IO.Directory.Exists(MainFolder))
                {
                    if (System.IO.Directory.Exists(TenantPath))
                    {
                        if (System.IO.Directory.Exists(InstallerPath))
                        {
                            if (FinalinstFilePath != "")
                            {
                                using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                }
                            }
                            if (FinalDesiFilePath != "")
                            {
                                using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                }
                            }
                            if (FinalElectFilePath != "")
                            {
                                using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                }
                            }

                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(InstallerPath);
                            if (FinalinstFilePath != "")
                            {
                                using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                }
                            }
                            if (FinalDesiFilePath != "")
                            {
                                using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                }
                            }
                            if (FinalElectFilePath != "")
                            {
                                using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(TenantPath);
                        if (System.IO.Directory.Exists(InstallerPath))
                        {
                            if (System.IO.Directory.Exists(InstallerPath))
                            {
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(InstallerPath);
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(InstallerPath);
                            if (System.IO.Directory.Exists(InstallerPath))
                            {
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                            else
                            {
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(MainFolder);
                    if (System.IO.Directory.Exists(TenantPath))
                    {
                        if (System.IO.Directory.Exists(InstallerPath))
                        {
                            if (System.IO.Directory.Exists(InstallerPath))
                            {
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(InstallerPath);
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(InstallerPath);
                            if (System.IO.Directory.Exists(InstallerPath))
                            {
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(InstallerPath);
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(TenantPath);
                        if (System.IO.Directory.Exists(InstallerPath))
                        {
                            if (System.IO.Directory.Exists(InstallerPath))
                            {
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(InstallerPath);
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(InstallerPath);
                            if (System.IO.Directory.Exists(InstallerPath))
                            {
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(InstallerPath);
                                if (FinalinstFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(InstallerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalinstFilePath), InstallerByteArray);
                                    }
                                }
                                if (FinalDesiFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(DesignerByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalDesiFilePath), DesignerByteArray);
                                    }
                                }
                                if (FinalElectFilePath != "")
                                {
                                    using (MemoryStream mStream = new MemoryStream(ElectricianByteArray))
                                    {
                                        System.IO.File.WriteAllBytes(string.Format("{0}", FinalElectFilePath), ElectricianByteArray);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            var user = ObjectMapper.Map<User>(input.User); //Passwords is not mapped (see mapping configuration)
            user.TenantId = AbpSession.TenantId;

            //Set password
            if (input.SetRandomPassword)
            {
                var randomPassword = await _userManager.CreateRandomPassword();
                user.Password = _passwordHasher.HashPassword(user, randomPassword);
                input.User.Password = randomPassword;
            }
            else if (!input.User.Password.IsNullOrEmpty())
            {
                await UserManager.InitializeOptionsAsync(AbpSession.TenantId);
                foreach (var validator in _passwordValidators)
                {
                    CheckErrors(await validator.ValidateAsync(UserManager, user, input.User.Password));
                }

                user.Password = _passwordHasher.HashPassword(user, input.User.Password);
            }

            user.ShouldChangePasswordOnNextLogin = input.User.ShouldChangePasswordOnNextLogin;

            //Assign roles
            user.Roles = new Collection<UserRole>();
            foreach (var roleName in input.AssignedRoleNames)
            {
                var role = await _roleManager.GetRoleByNameAsync(roleName);
                user.Roles.Add(new UserRole(AbpSession.TenantId, user.Id, role.Id));
            }

            if (input.User.ismyinstalleruser == true)
            {
                string[] items;
                if (user.UserName == "NULL")
                {
                    user.UserName = user.UserName + "-";
                    items = user.UserName.Split('-');
                    string a = items[0];
                    int b = (items[1] != "" ? Convert.ToInt32(items[1]) : 0) + 1;
                    input.User.UserName = "NULL-" + b;
                }
                else
                {
                    //items = user.UserName.Split('-');
                    //string a = items[0];
                    //int b = (items[1] != "" ? Convert.ToInt32(items[1]) : 0) + 1;
                    //input.User.UserName = "NULL-" + b;
                    user.UserName = input.User.UserName;
                }
            }
            else
            {
                user.UserName = input.User.UserName;
                user.ismyinstalleruser = true;
                user.IsApproveInstaller = true;
            }
            CheckErrors(await UserManager.CreateAsync(user));
            await CurrentUnitOfWork.SaveChangesAsync(); //To get new user's Id.

            //InstallerAddress
            //if (input.User.Address != null)
            //{
            //	InstallerAddress installeraddress = new InstallerAddress
            //	{
            //		UserId = user.Id,
            //		TenantId = (int)user.TenantId,
            //		IsGoogle = input.User.IsGoogle,
            //		Address = input.User.Address,
            //		latitude = input.User.latitude,
            //		longitude = input.User.longitude,
            //		Unit = input.User.Unit,
            //		UnitType = input.User.UnitType,
            //		StreetNo = input.User.StreetNo,
            //		StreetName = input.User.StreetName,
            //		StreetType = input.User.StreetType,
            //		Suburb = input.User.Suburb,
            //		StateId = input.User.StateId,
            //		PostCode = input.User.PostCode
            //	};
            //	_installerAddress.Insert(installeraddress);
            //}

            if (input.User != null)
            {
                InstallerDetail installerDetail = new InstallerDetail
                {

                    UserId = user.Id,
                    TenantId = (int)user.TenantId,
                    InstallerAccreditationNumber = input.User.InstallerAccreditationNumber,
                    InstallerAccreditationExpiryDate = input.User.InstallerAccreditationExpiryDate,
                    DesignerLicenseNumber = input.User.DesignerLicenseNumber,
                    DesignerLicenseExpiryDate = input.User.DesignerLicenseExpiryDate,
                    ElectricianLicenseNumber = input.User.ElectricianLicenseNumber,
                    ElectricianLicenseExpiryDate = input.User.ElectricianLicenseExpiryDate,
                    DocInstaller = input.User.DocInstallerFileName,
                    DocDesigner = input.User.DocDesignerFileName,
                    DocElectrician = input.User.DocElectricianFileName,
                    OtherDocsCSV = input.User.OtherDocsCSV,
                    IsDesi = input.User.IsDesi,
                    IsElec = input.User.IsElec,
                    IsInst = input.User.IsInst,
                    IsGoogle = input.User.IsGoogle,
                    Address = input.User.Address,
                    latitude = input.User.latitude,
                    longitude = input.User.longitude,
                    Unit = input.User.Unit,
                    UnitType = input.User.UnitType,
                    StreetNo = input.User.StreetNo,
                    StreetName = input.User.StreetName,
                    StreetType = input.User.StreetType,
                    Suburb = input.User.Suburb,
                    StateId = input.User.StateId,
                    PostCode = input.User.PostCode,
                    SourceTypeId = input.User.SourceTypeId,
                    AreaName = input.User.AreaName,
                    Notes = input.User.Notes
                };
                _installerDetail.Insert(installerDetail);
            }


            //Notifications
            await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(user.ToUserIdentifier());
            await _appNotifier.WelcomeToTheApplicationAsync(user);


            if (input.UserEmailDetail != null)
            {
                foreach (var useremailorgdetail in input.UserEmailDetail)
                {
                    input.OrganizationUnits.Add(useremailorgdetail.OrganizationUnitId);
                    if (useremailorgdetail.OrganizationUnitId != null && useremailorgdetail.OrganizationUnitId != 0)
                    {
                        var detail = new UserWiseEmailOrg();
                        detail.UserId = (long)user.Id;
                        if (AbpSession.TenantId != null)
                        {
                            detail.TenantId = (int)AbpSession.TenantId;
                        }
                        detail.EmailFromAdress = useremailorgdetail.EmailFromAdress;
                        detail.OrganizationUnitId = useremailorgdetail.OrganizationUnitId;
                        _UserWiseEmailOrgsRepository.Insert(detail);
                    }
                }
            }

            //IP Address
            if (input.UserIPAddress != null)
            {
                foreach (var ip in input.UserIPAddress)
                {
                    var userIpAddress = new UserIPAddress();
                    userIpAddress.UserId = (long)input.User.Id;
                    userIpAddress.IPAdress = ip.IPAdress;
                    userIpAddress.IsActive = ip.IsActive;

                    await _userIPAddressRepository.InsertAsync(userIpAddress);
                }
            }


            //if (input.User.ismyinstalleruser == false)
            //{
            if (input.OrganizationUnits.Count == 0)
            {
                throw new UserFriendlyException(L("OrganizationUnitsNotSelected"));
            }


            //Organization Units
            await UserManager.SetOrganizationUnitsAsync(user, input.OrganizationUnits.ToArray());
            //}

            //Send activation email
            if (input.SendActivationEmail)
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(
                    user,
                    AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId),
                    input.User.Password
                );
            }

            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> roles = await _userManager.GetRolesAsync(User);
            //if (!roles.Contains("Admin"))
            //{
            //    var roleids = _roleRepository.GetAll().Where(e => e.Name == "Admin").Select(e => e.Id).ToList();
            //    var userids = _userRoleRepository.GetAll().Where(e => roleids.Contains(e.RoleId)).Select(e => e.UserId).ToList();

            //    foreach (var id in userids)
            //    {
            //        var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == id).FirstOrDefault();
            //        string msg = string.Format("New Installer Created {0}.", input.User.Name);
            //        await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
            //    }
            //}
        }
        #endregion

        private async Task FillRoleNames(IReadOnlyCollection<UserListDto> userListDtos)
        {
            /* This method is optimized to fill role names to given list. */
            var userIds = userListDtos.Select(u => u.Id);

            var userRoles = await _userRoleRepository.GetAll()
                .Where(userRole => userIds.Contains(userRole.UserId))
                .Select(userRole => userRole).ToListAsync();

            var distinctRoleIds = userRoles.Select(userRole => userRole.RoleId).Distinct();

            foreach (var user in userListDtos)
            {
                var rolesOfUser = userRoles.Where(userRole => userRole.UserId == user.Id).ToList();
                user.Roles = ObjectMapper.Map<List<UserListRoleDto>>(rolesOfUser);
            }

            var roleNames = new Dictionary<int, string>();
            foreach (var roleId in distinctRoleIds)
            {
                var role = await _roleManager.FindByIdAsync(roleId.ToString());
                if (role != null)
                {
                    roleNames[roleId] = role.DisplayName;
                }
            }

            foreach (var userListDto in userListDtos)
            {
                foreach (var userListRoleDto in userListDto.Roles)
                {
                    if (roleNames.ContainsKey(userListRoleDto.RoleId))
                    {
                        userListRoleDto.RoleName = roleNames[userListRoleDto.RoleId];
                    }
                }

                userListDto.Roles = userListDto.Roles.Where(r => r.RoleName != null).OrderBy(r => r.RoleName).ToList();
            }
        }
        private IQueryable<User> GetStaticUsersFilteredQuery(IGetUsersInput input)
        {
            if (!input.RoleName.IsNullOrEmpty()) input.Role = _roleManager.GetRoleByName(input.RoleName).Id;

            var query = UserManager.Users
                .WhereIf(input.OrganizationUnit.HasValue, u => u.OrganizationUnits.Any(r => r.OrganizationUnitId == input.OrganizationUnit.Value))
                .WhereIf(input.Role.HasValue, u => u.Roles.Any(r => r.RoleId == input.Role.Value))
                .WhereIf(input.OnlyLockedUsers, u => u.LockoutEndDateUtc.HasValue && u.LockoutEndDateUtc.Value > DateTime.UtcNow)
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u =>
                        u.Name.Contains(input.Filter) ||
                        u.Surname.Contains(input.Filter) ||
                        u.UserName.Contains(input.Filter) ||
                        u.EmailAddress.Contains(input.Filter)
                );

            if (input.Permissions != null && input.Permissions.Any(p => !p.IsNullOrWhiteSpace()))
            {
                var staticRoleNames = _roleManagementConfig.StaticRoles.Where(
                    r => r.GrantAllPermissionsByDefault &&
                         r.Side == AbpSession.MultiTenancySide
                ).Select(r => r.RoleName).ToList();

                input.Permissions = input.Permissions.Where(p => !string.IsNullOrEmpty(p)).ToList();

                query = from user in query
                        join ur in _userRoleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                        from ur in urJoined.DefaultIfEmpty()
                        join urr in _roleRepository.GetAll() on ur.RoleId equals urr.Id into urrJoined
                        from urr in urrJoined.DefaultIfEmpty()
                        join up in _userPermissionRepository.GetAll()
                            .Where(userPermission => input.Permissions.Contains(userPermission.Name)) on user.Id equals up.UserId into upJoined
                        from up in upJoined.DefaultIfEmpty()
                        join rp in _rolePermissionRepository.GetAll()
                            .Where(rolePermission => input.Permissions.Contains(rolePermission.Name)) on
                            new { RoleId = ur == null ? 0 : ur.RoleId } equals new { rp.RoleId } into rpJoined
                        from rp in rpJoined.DefaultIfEmpty()
                        where (up != null && up.IsGranted) ||
                              (up == null && rp != null && rp.IsGranted) ||
                              (up == null && rp == null && staticRoleNames.Contains(urr.Name))
                        select user;
            }

            return query;
        }
        private IQueryable<User> GetUsersFilteredQuery(IGetUsersInput input)
        {
            if (!input.RoleName.IsNullOrEmpty()) input.Role = _roleManager.GetRoleByName(input.RoleName).Id;
            var SalesRepId = 0;
            var SalesManager = 0;
            var Installer = 0;
            var adminId = 0;
            if (AbpSession.TenantId != null)
            {
                adminId = Convert.ToInt32(_roleManager.GetRoleByName("Admin").Id);
                SalesRepId = Convert.ToInt32(_roleManager.GetRoleByName("Sales Rep").Id);
                SalesManager = Convert.ToInt32(_roleManager.GetRoleByName("Sales Manager").Id);
                Installer = Convert.ToInt32(_roleManager.GetRoleByName("Installer").Id);
            }
            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            // var role = UserManager.Users.Where(e => e.Id == AbpSession.UserId).Select(e => e.Roles).FirstOrDefault();

            var orgIds = _UserWiseEmailOrgsRepository.GetAll().Where(e => e.UserId == AbpSession.UserId).Select(e => e.OrganizationUnitId).Distinct().ToList();

            var query = UserManager.Users
                //.WhereIf(true, u => u.Roles.Any(r => r.RoleId!= _roleManager.GetRoleByName("Sales Rep").Id
                //&& r.RoleId != _roleManager.GetRoleByName("Sales Manager").Id
                //&& r.RoleId != _roleManager.GetRoleByName("Installer").Id)
                //)
                .Where(u => u.Roles.Any(r => (r.RoleId != SalesRepId && r.RoleId != SalesManager && r.RoleId != Installer)) && u.OrganizationUnits.Any(r => orgIds.Contains(r.OrganizationUnitId)))
                //.Where(u => u.Roles.Any(r => u.OrganizationUnits.Any(r => orgIds.Contains(r.OrganizationUnitId))))
                //.WhereIf(role.Any(r => r.RoleId != adminId) ,u => u.OrganizationUnits.Any(r => orgIds.Contains(r.OrganizationUnitId)))
                .WhereIf(input.Role.HasValue, u => u.Roles.Any(r => r.RoleId == input.Role.Value))
                .WhereIf(input.OrganizationUnit.HasValue, u => u.OrganizationUnits.Any(r => r.OrganizationUnitId == input.OrganizationUnit.Value))
                .WhereIf(input.OnlyLockedUsers, u => u.LockoutEndDateUtc.HasValue && u.LockoutEndDateUtc.Value > DateTime.UtcNow)
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u =>
                        u.Name.Contains(input.Filter) ||
                        u.Surname.Contains(input.Filter) ||
                        u.UserName.Contains(input.Filter) ||
                        u.EmailAddress.Contains(input.Filter)
                );

            if (input.Permissions != null && input.Permissions.Any(p => !p.IsNullOrWhiteSpace()))
            {
                var staticRoleNames = _roleManagementConfig.StaticRoles.Where(
                    r => r.GrantAllPermissionsByDefault &&
                         r.Side == AbpSession.MultiTenancySide
                ).Select(r => r.RoleName).ToList();

                input.Permissions = input.Permissions.Where(p => !string.IsNullOrEmpty(p)).ToList();

                query = from user in query
                        join ur in _userRoleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                        from ur in urJoined.DefaultIfEmpty()
                        join urr in _roleRepository.GetAll() on ur.RoleId equals urr.Id into urrJoined
                        from urr in urrJoined.DefaultIfEmpty()
                        join up in _userPermissionRepository.GetAll()
                            .Where(userPermission => input.Permissions.Contains(userPermission.Name)) on user.Id equals up.UserId into upJoined
                        from up in upJoined.DefaultIfEmpty()
                            //join rp in _rolePermissionRepository.GetAll()
                            //    .Where(rolePermission => input.Permissions.Contains(rolePermission.Name)) on
                            //    new { RoleId = ur == null ? 0 : ur.RoleId } equals new { rp.RoleId } into rpJoined
                            //from rp in rpJoined.DefaultIfEmpty()
                        where (up != null && up.IsGranted) ||
                              // (up == null && rp != null && rp.IsGranted) ||
                              //(up == null && rp == null && staticRoleNames.Contains(urr.Name))
                              (up == null && staticRoleNames.Contains(urr.Name))
                        select user;
            }

            return query;
        }


        public async Task<List<OrganizationUnitDto>> GetUserWiseOrganizationUnit(int userid)
        {

            var OrganizationList = (from or in _userOrganizationUnitRepository.GetAll()
                                    where (or.UserId == userid)
                                    join uo in _organizationUnitRepository.GetAll() on or.OrganizationUnitId equals uo.Id into uoJoined
                                    from uo in uoJoined.DefaultIfEmpty()
                                    select new OrganizationUnitDto
                                    {
                                        Id = uo.Id,
                                        Code = uo.Code,
                                        DisplayName = uo.DisplayName,
                                        ParentId = uo.ParentId
                                    }).ToList();

            return OrganizationList;
        }

        private IQueryable<GetInstallerDto> GetInstallersFilteredQuery(IGetInstallerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            if (!input.RoleName.IsNullOrEmpty()) input.Role = _roleManager.GetRoleByName(input.RoleName).Id;

            if (input.ismyinstalleruser == true)
            {
                var queryfilter = UserManager.Users
                      .WhereIf(input.OrganizationUnit.HasValue, u => u.OrganizationUnits.Any(r => r.OrganizationUnitId == input.OrganizationUnit.Value))
                      .WhereIf(input.Role.HasValue, u => u.Roles.Any(r => r.RoleId == input.Role.Value))
                      .WhereIf(input.OnlyLockedUsers, u => u.LockoutEndDateUtc.HasValue && u.LockoutEndDateUtc.Value > DateTime.UtcNow)
                      .WhereIf(input.IsApproveInstaller == false, u => u.IsApproveInstaller == false)
                      .WhereIf(input.IsApproveInstaller == true, u => u.IsApproveInstaller == true)
                      .Where(u => u.ismyinstalleruser == input.ismyinstalleruser)
                      //.WhereIf(!input.Filter.IsNullOrWhiteSpace(), u => u.Name.Contains(input.Filter) || u.Surname.Contains(input.Filter)
                      //|| u.UserName.Contains(input.Filter) || u.EmailAddress.Contains(input.Filter))
                      .WhereIf(input.FilterName == "Name" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Name == input.Filter)
                      .WhereIf(input.FilterName == "Surname" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Surname == input.Filter)
                      .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                      .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                      .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.EmailAddress == input.Filter)
                      .WhereIf(input.DateType == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                      .WhereIf(input.DateType == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                        ;

                var query = (from user in queryfilter
                             join inst in _installerDetail.GetAll() on user.Id equals inst.UserId into instJoined
                             from inst in instJoined.DefaultIfEmpty()
                             join jobtype in _lookup_jobTypeRepository.GetAll() on inst.SourceTypeId equals jobtype.Id into typeJoined
                             from jobtype in typeJoined.DefaultIfEmpty()
                             join state in _lookup_stateRepository.GetAll() on inst.StateId equals state.Id into stateJoined
                             from state in stateJoined.DefaultIfEmpty()
                                 ///  where user.ismyinstalleruser == input.ismyinstalleruser
                             select new GetInstallerDto
                             {
                                 Name = user.Name,
                                 Surname = user.Surname,
                                 CompanyName = user.CompanyName,
                                 //Verify = user.Verify,
                                 Id = user.Id,
                                 UserId = inst.Id,
                                 UserName = user.UserName,
                                 EmailAddress = user.EmailAddress,
                                 IsActive = user.IsActive,
                                 Mobile = user.Mobile,
                                 Address = inst.Address == null ? "" : inst.Address.Replace("null", "").Trim(),
                                 Suburb = inst.Suburb,
                                 StateId = inst.StateId,
                                 State = state.Name,
                                 PostCode = inst.PostCode,
                                 SourceTypeId = inst.SourceTypeId,
                                 SourceType = jobtype.Name,
                                 Notes = inst.Notes,
                                 CreationTime = user.CreationTime,
                                 Comment = _myinstalleractivityRepository.GetAll().OrderByDescending(x => x.Id).Where(x => x.ActionId == 24).Select(x => x.ActivityNote).FirstOrDefault(),
                                 Followup = _myinstalleractivityRepository.GetAll().OrderByDescending(x => x.Id).Where(x => x.ActionId == 8).Select(x => x.ActivityDate).FirstOrDefault(),
                                 FollowupDesc = _myinstalleractivityRepository.GetAll().OrderByDescending(x => x.Id).Where(x => x.ActionId == 8).Select(x => x.ActivityNote).FirstOrDefault(),
                                 //Users = user
                                 //CreatorBy = createduser.UserName,
                                 CreatorBy = _userManager.Users.Where(x => x.Id == user.CreatorUserId).Select(x => x.UserName).FirstOrDefault(),
                                 SmsSend = inst.SmsSend,
                                 SmsSendDate = inst.SmsSendDate,
                                 EmailSend = inst.EmailSend,
                                 EmailSendDate = inst.EmailSendDate,
                                 ismyinstalleruser = user.ismyinstalleruser,
                                 IsApproveInstaller = user.IsApproveInstaller,
                                 AreaName = inst.AreaName
                             });

                query = query.WhereIf(!input.Suburb.IsNullOrWhiteSpace(), e => e.Suburb.Contains(input.Suburb))
                    .WhereIf(input.State != 0, e => e.StateId == input.State)
                    .WhereIf(input.FromPostCode != 0 && input.FromPostCode != null, e => (e.PostCode == null ? 0 : Convert.ToInt64(e.PostCode)) >= Convert.ToInt64(input.FromPostCode))
                    .WhereIf(input.ToPostCode != 0 && input.ToPostCode != null, e => (e.PostCode == null ? 0 : Convert.ToInt64(e.PostCode)) <= Convert.ToInt64(input.ToPostCode))
                    .WhereIf(input.JobType != 0, e => e.SourceTypeId == input.JobType)
                    .WhereIf(!input.AreaType.IsNullOrWhiteSpace(), e => e.AreaName == input.AreaType);

                if (input.Permissions != null && input.Permissions.Any(p => !p.IsNullOrWhiteSpace()))
                {
                    var staticRoleNames = _roleManagementConfig.StaticRoles.Where(
                        r => r.GrantAllPermissionsByDefault &&
                             r.Side == AbpSession.MultiTenancySide
                    ).Select(r => r.RoleName).ToList();

                    input.Permissions = input.Permissions.Where(p => !string.IsNullOrEmpty(p)).ToList();

                    query = from user in query
                            join ur in _userRoleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join urr in _roleRepository.GetAll() on ur.RoleId equals urr.Id into urrJoined
                            from urr in urrJoined.DefaultIfEmpty()
                            join up in _userPermissionRepository.GetAll()
                                .Where(userPermission => input.Permissions.Contains(userPermission.Name)) on user.Id equals up.UserId into upJoined
                            from up in upJoined.DefaultIfEmpty()
                            join rp in _rolePermissionRepository.GetAll()
                                .Where(rolePermission => input.Permissions.Contains(rolePermission.Name)) on
                                new { RoleId = ur == null ? 0 : ur.RoleId } equals new { rp.RoleId } into rpJoined
                            from rp in rpJoined.DefaultIfEmpty()
                            where (up != null && up.IsGranted) ||
                                  (up == null && rp != null && rp.IsGranted) ||
                                  (up == null && rp == null && staticRoleNames.Contains(urr.Name))
                            select user;
                }

                return query;
            }
            else
            {
                var queryfilter = UserManager.Users
                     .WhereIf(input.OrganizationUnit.HasValue, u => u.OrganizationUnits.Any(r => r.OrganizationUnitId == input.OrganizationUnit.Value))
                     .WhereIf(input.Role.HasValue, u => u.Roles.Any(r => r.RoleId == input.Role.Value))
                     .WhereIf(input.OnlyLockedUsers, u => u.LockoutEndDateUtc.HasValue && u.LockoutEndDateUtc.Value > DateTime.UtcNow)
                     .Where(u => u.IsApproveInstaller == true)
                     .WhereIf(
                         !input.Filter.IsNullOrWhiteSpace(),
                         u =>
                             u.Name.Contains(input.Filter) ||
                             u.Surname.Contains(input.Filter) ||
                             u.UserName.Contains(input.Filter) ||
                             u.EmailAddress.Contains(input.Filter)
                     );

                var query = (from user in queryfilter
                             join inst in _installerDetail.GetAll() on user.Id equals inst.UserId into instJoined
                             from inst in instJoined.DefaultIfEmpty()
                             join jobtype in _lookup_jobTypeRepository.GetAll() on inst.SourceTypeId equals jobtype.Id into typeJoined
                             from jobtype in typeJoined.DefaultIfEmpty()
                             join state in _lookup_stateRepository.GetAll() on inst.StateId equals state.Id into stateJoined
                             from state in stateJoined.DefaultIfEmpty()
                                 ///  where user.ismyinstalleruser == input.ismyinstalleruser
                             select new GetInstallerDto
                             {
                                 Name = user.Name,
                                 Surname = user.Surname,
                                 CompanyName = user.CompanyName,
                                 //Verify = user.Verify,
                                 Id = user.Id,
                                 UserId = inst.Id,
                                 UserName = user.UserName,
                                 EmailAddress = user.EmailAddress,
                                 IsActive = user.IsActive,
                                 Mobile = user.Mobile,
                                 Address = inst.Address,
                                 Suburb = inst.Suburb,
                                 StateId = inst.StateId,
                                 State = state.Name,
                                 PostCode = inst.PostCode,
                                 SourceTypeId = inst.SourceTypeId,
                                 SourceType = jobtype.Name,
                                 Notes = inst.Notes,
                                 CreationTime = user.CreationTime,
                                 Comment = _myinstalleractivityRepository.GetAll().OrderByDescending(x => x.Id).Where(x => x.ActionId == 24).Select(x => x.ActivityNote).FirstOrDefault(),
                                 Followup = _myinstalleractivityRepository.GetAll().OrderByDescending(x => x.Id).Where(x => x.ActionId == 8).Select(x => x.ActivityDate).FirstOrDefault(),
                                 FollowupDesc = _myinstalleractivityRepository.GetAll().OrderByDescending(x => x.Id).Where(x => x.ActionId == 8).Select(x => x.ActivityNote).FirstOrDefault(),
                                 //Users = user
                                 //CreatorBy = createduser.UserName,
                                 CreatorBy = _userManager.Users.Where(x => x.Id == user.CreatorUserId).Select(x => x.UserName).FirstOrDefault(),
                                 SmsSend = inst.SmsSend,
                                 SmsSendDate = inst.SmsSendDate,
                                 EmailSend = inst.EmailSend,
                                 EmailSendDate = inst.EmailSendDate,
                                 ismyinstalleruser = user.ismyinstalleruser
                             });

                if (input.Permissions != null && input.Permissions.Any(p => !p.IsNullOrWhiteSpace()))
                {
                    var staticRoleNames = _roleManagementConfig.StaticRoles.Where(
                        r => r.GrantAllPermissionsByDefault &&
                             r.Side == AbpSession.MultiTenancySide
                    ).Select(r => r.RoleName).ToList();

                    input.Permissions = input.Permissions.Where(p => !string.IsNullOrEmpty(p)).ToList();

                    query = from user in query
                            join ur in _userRoleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join urr in _roleRepository.GetAll() on ur.RoleId equals urr.Id into urrJoined
                            from urr in urrJoined.DefaultIfEmpty()
                            join up in _userPermissionRepository.GetAll()
                                .Where(userPermission => input.Permissions.Contains(userPermission.Name)) on user.Id equals up.UserId into upJoined
                            from up in upJoined.DefaultIfEmpty()
                            join rp in _rolePermissionRepository.GetAll()
                                .Where(rolePermission => input.Permissions.Contains(rolePermission.Name)) on
                                new { RoleId = ur == null ? 0 : ur.RoleId } equals new { rp.RoleId } into rpJoined
                            from rp in rpJoined.DefaultIfEmpty()
                            where (up != null && up.IsGranted) ||
                                  (up == null && rp != null && rp.IsGranted) ||
                                  (up == null && rp == null && staticRoleNames.Contains(urr.Name))
                            select user;
                }

                return query;
            }


        }

        private async Task FillRoleNamesforInstaller(List<GetInstallerDto> userListDtos)
        {
            /* This method is optimized to fill role names to given list. */
            var userIds = userListDtos.Select(u => u.Id);

            var userRoles = await _userRoleRepository.GetAll()
                .Where(userRole => userIds.Contains(userRole.UserId))
                .Select(userRole => userRole).ToListAsync();

            var distinctRoleIds = userRoles.Select(userRole => userRole.RoleId).Distinct();

            foreach (var user in userListDtos)
            {
                var rolesOfUser = userRoles.Where(userRole => userRole.UserId == user.Id).ToList();
                user.Roles = ObjectMapper.Map<List<UserListRoleDto>>(rolesOfUser);
            }

            var roleNames = new Dictionary<int, string>();
            foreach (var roleId in distinctRoleIds)
            {
                var role = await _roleManager.FindByIdAsync(roleId.ToString());
                if (role != null)
                {
                    roleNames[roleId] = role.DisplayName;
                }
            }

            foreach (var userListDto in userListDtos)
            {
                foreach (var userListRoleDto in userListDto.Roles)
                {
                    if (roleNames.ContainsKey(userListRoleDto.RoleId))
                    {
                        userListRoleDto.RoleName = roleNames[userListRoleDto.RoleId];
                    }
                }

                userListDto.Roles = userListDto.Roles.Where(r => r.RoleName != null).OrderBy(r => r.RoleName).ToList();
            }
        }
        public bool checkorgwisephonedynamicavaibleornot(int userId)
        {
            var organizationUnitId = _UserWiseEmailOrgsRepository.GetAll().Where(e => e.UserId == userId).Select(e => e.OrganizationUnitId).FirstOrDefault();
            var org = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == organizationUnitId).FirstOrDefault();
            if (org.FoneDynamicsPropertySid != null && org.FoneDynamicsAccountSid != null && org.FoneDynamicsToken != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<List<CommonLookupDto>> getOrgWiseDefultandownemailadd(int userId)
        {
            var organizationUnitId = _UserWiseEmailOrgsRepository.GetAll().Where(e => e.UserId == userId).Select(e => e.OrganizationUnitId).FirstOrDefault();
            //var emailadress = new List<string?>();
            var list = new List<CommonLookupDto>();

            var ownemailadd = _UserWiseEmailOrgsRepository.GetAll().Where(e => e.UserId == AbpSession.UserId).Select(e => e.EmailFromAdress).FirstOrDefault();
            if (ownemailadd != null)
            {
                CommonLookupDto comon = new CommonLookupDto();
                comon.Id = 0;
                comon.DisplayName = ownemailadd;
                list.Add(comon);
            }
            var defultemail = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == organizationUnitId).Select(e => e.defaultFromAddress).FirstOrDefault();

            if (defultemail != null)
            {
                CommonLookupDto comon = new CommonLookupDto();
                comon.Id = 0;
                comon.DisplayName = defultemail;
                list.Add(comon);
            }
            //EmailAddress.add(_organizationUnitRepository.GetAll(e => e.id))
            return list;
        }
        public async Task AddActivityLog(ActivityLogInput input)
        {
            var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            //var lead = await _leadRepository.FirstOrDefaultAsync(input.LeadId);
            var myinstallerUser = await _userRepository.FirstOrDefaultAsync(input.MyInstallerId);
            //var CreatedUser = _userRepository.GetAll().Where(u => u.Id == myinstallerUser.CreatorUserId).FirstOrDefault();
            var CreatedUser = _userRepository.GetAll().Where(u => u.UserName == input.CreatedUser).FirstOrDefault();

            //var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();

            var activity = ObjectMapper.Map<MyInstallerActivityLog>(input);
            activity.ActionNote = input.ActionNote;
            activity.Subject = input.Subject;
            //activity.SectionId = input.SectionId;
            var SectionName = _SectionAppService.GetAll().Where(e => e.SectionId == input.SectionId).Select(e => e.SectionName).FirstOrDefault();
            if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
            {
                activity.TemplateId = input.EmailTemplateId;
            }
            else if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
            {
                activity.TemplateId = input.SMSTemplateId;
            }
            else
            {
                activity.TemplateId = 0;
            }

            if (AbpSession.TenantId != null)
            {
                input.TenantId = (int)AbpSession.TenantId;
            }
            if (input.ActionId == 9)
            {
                string msg = input.ActionNote + " " + input.ActivityNote + " For " + myinstallerUser.CompanyName + " By " + CurrentUser.Name + " From " + SectionName;
                await _appNotifier.LeadComment(CreatedUser, msg, NotificationSeverity.Warn);
            }
            if (input.ActionId == 25)
            {
                var assignedToUserForTodo = _userRepository.GetAll().Where(u => u.Id == input.UserId).FirstOrDefault();
                string msg = input.ActionNote + " " + input.ActivityNote + " For " + myinstallerUser.CompanyName + " By " + CurrentUser.Name + " From " + SectionName;
                await _appNotifier.LeadComment(CreatedUser, msg, NotificationSeverity.Warn);
            }
            if (input.ActionId == 8)
            {
                var CurrentTime = (_timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId));
                if (Convert.ToDateTime(input.Body) < CurrentTime)
                {
                    throw new UserFriendlyException(L("PleaseSelectGreaterDatethanCurrentDate"));
                }
                activity.ActivityDate = Convert.ToDateTime(input.Body);
                input.Body = "";
                string msg = string.Format("Reminder " + input.ActionNote + " " + input.ActivityNote + " For " + myinstallerUser.CompanyName + " By " + CurrentUser.Name + " From " + SectionName);
                await _appNotifier.LeadAssiged(CreatedUser, msg, NotificationSeverity.Info);
            }
            if (input.ActionId == 7)
            {
                await _emailSender.SendAsync(new MailMessage
                {
                    From = new MailAddress(input.EmailFrom),
                    To = { myinstallerUser.EmailAddress }, ////{ "hiral.prajapati@meghtechnologies.com" },
                    Subject = input.Subject,//EmailBody.TemplateName + "The Solar Product Lead Notification",
                    Body = input.Body,
                    IsBodyHtml = true
                });
            }
            activity.Body = Regex.Replace(input.Body, "<.*?>", String.Empty);
            activity.CreatorUserId = AbpSession.UserId;
            await _myinstalleractivityRepository.InsertAndGetIdAsync(activity);
            if (input.ActionId == 6)
            {
                //Send SMS
                SendSMSInput sendSMSInput = new SendSMSInput();
                sendSMSInput.PhoneNumber = myinstallerUser.Mobile;
                sendSMSInput.Text = input.Body;
                sendSMSInput.ActivityId = activity.Id;
                sendSMSInput.IsMyInstallerUser = true;
                await _applicationSettings.SendSMS(sendSMSInput);
            }
        }
        public async Task SendSms(SmsEmailDto input)
        {
            var user = await _userRepository.FirstOrDefaultAsync((int)input.MyInstallerId);
            if (!string.IsNullOrEmpty(user.Mobile))
            {
                var myinstallerdetails = _installerDetail.GetAll().Where(e => e.UserId == user.Id).FirstOrDefault();
                var output = ObjectMapper.Map<InstallerEditDto>(myinstallerdetails);
                myinstallerdetails.SmsSend = true;
                myinstallerdetails.SmsSendDate = DateTime.UtcNow;

                //ObjectMapper.Map(output, myinstallerdetails);

                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                MyInstallerActivityLog myinstalleractivity = new MyInstallerActivityLog();
                myinstalleractivity.ActionId = 6;
                myinstalleractivity.ActionNote = "Sms Send From My Installer";
                myinstalleractivity.MyInstallerId = (int)input.MyInstallerId;
                myinstalleractivity.Subject = input.Body;
                myinstalleractivity.ActivityDate = DateTime.UtcNow;
                if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                {
                    myinstalleractivity.TemplateId = input.SMSTemplateId;
                }
                else
                {
                    myinstalleractivity.TemplateId = 0;
                }
                if (AbpSession.TenantId != null)
                {
                    myinstalleractivity.TenantId = (int)AbpSession.TenantId;
                }
                myinstalleractivity.Body = input.Body;
                await _myinstalleractivityRepository.InsertAndGetIdAsync(myinstalleractivity);

                SendSMSInput sendSMSInput = new SendSMSInput();
                sendSMSInput.PhoneNumber = user.Mobile;
                sendSMSInput.Text = input.Body;
                sendSMSInput.ActivityId = myinstalleractivity.Id;
                sendSMSInput.IsMyInstallerUser = true;
                await _applicationSettings.SendSMS(sendSMSInput);
            }
        }
        public async Task SendEmail(SmsEmailDto input)
        {

            var user = await _userRepository.FirstOrDefaultAsync((int)input.MyInstallerId);
            //var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
            int? TemplateId;
            if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
            {
                TemplateId = input.EmailTemplateId;
            }
            else
            {
                TemplateId = 0;
            }

            if (!string.IsNullOrEmpty(user.EmailAddress))
            {
                if (input.cc != null && input.Bcc != null)
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { user.EmailAddress }, //{ "viral.jain@meghtechnologies.com" }, //
                        CC = { input.cc },
                        Bcc = { input.Bcc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                }

                else if (input.cc != null && input.cc != "" && input.Bcc == null)
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { user.EmailAddress }, //{ "viral.jain@meghtechnologies.com" }, //
                        CC = { input.cc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                }

                else if (input.cc == null && input.Bcc != null && input.Bcc != "")
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { user.EmailAddress }, //{ "viral.jain@meghtechnologies.com" }, //
                        Bcc = { input.Bcc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                }

                else
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { user.EmailAddress }, //{ "hiral.prajapati@meghtechnologies.com" }, //

                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                }
            }

            var myinstallerdetails = _installerDetail.GetAll().Where(e => e.UserId == user.Id).FirstOrDefault();
            var output = ObjectMapper.Map<InstallerEditDto>(myinstallerdetails);
            output.EmailSend = true;
            output.EmailSendDate = DateTime.UtcNow;
            ObjectMapper.Map(output, myinstallerdetails);

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            MyInstallerActivityLog myinstalleractivity = new MyInstallerActivityLog();
            myinstalleractivity.ActionId = 7;
            myinstalleractivity.ActionNote = "Email Send From My Installer";
            myinstalleractivity.MyInstallerId = (int)input.MyInstallerId;
            myinstalleractivity.TemplateId = TemplateId;
            myinstalleractivity.Subject = input.Subject;
            myinstalleractivity.Body = input.Body;
            myinstalleractivity.ActivityDate = DateTime.UtcNow;
            if (AbpSession.TenantId != null)
            {
                myinstalleractivity.TenantId = (int)AbpSession.TenantId;
            }
            await _myinstalleractivityRepository.InsertAsync(myinstalleractivity);
        }
        public string LoginuserwiseOrganization()
        {
            long organizationUnitId = 0;
            string organizationUnitsEmail = "";
            organizationUnitId = _userOrganizationUnitRepository.GetAll().Where(x => x.UserId == AbpSession.UserId).Select(x => x.OrganizationUnitId).FirstOrDefault();
            if (organizationUnitId != null && organizationUnitId != 0)
            {
                organizationUnitsEmail = _extendedOrganizationUnitRepository.GetAll().Where(x => x.Id == organizationUnitId).Select(x => x.Email).FirstOrDefault();
            }

            return organizationUnitsEmail;
        }

        public async Task<List<GetMyInstallerActivityLogDto>> GetMyInstallerActivityLog(GetActivityLogInput input)
        {
            List<int> smsActionids = new List<int>() { 6, 20 };
            var allactionids = _lookup_leadActionRepository.GetAll().Select(e => e.Id).ToList();
            var Result = _myinstalleractivityRepository
                .GetAll()
                .Where(L => L.MyInstallerId == input.MyInstallerId)
                .WhereIf(input.ActionId == 0, L => allactionids.Contains(L.ActionId))
                .WhereIf(input.ActionId == 6, L => smsActionids.Contains(L.ActionId))
                .WhereIf(input.ActionId == 7, L => L.ActionId == 7)
                .WhereIf(input.ActionId == 8, L => L.ActionId == 8)
                .WhereIf(input.ActionId == 9, L => L.ActionId == 9)
                .WhereIf(input.ActionId == 24, L => L.ActionId == 24)
                .WhereIf(input.ActionId == 25, L => L.ActionId == 25)
                .WhereIf(input.ActionId == 32, L => L.ActionId == 32)
                .WhereIf(input.OnlyMy != false, L => L.CreatorUserId == AbpSession.UserId)
                .OrderByDescending(e => e.Id);

            var myinstaller = from o in Result
                              join o1 in _lookup_leadActionRepository.GetAll() on o.ActionId equals o1.Id into j1
                              from s1 in j1.DefaultIfEmpty()

                              join o2 in _userRepository.GetAll() on o.MyInstallerId equals o2.Id into j2
                              from s2 in j2.DefaultIfEmpty()

                              let link = _myinstalleractivityHistoryRepository.GetAll().Where(e => e.MyInstallerActionId == o.Id).Any()

                              select new GetMyInstallerActivityLogDto()
                              {
                                  Id = o.Id,
                                  ActionName = s1 == null || s1.ActionName == null ? "" : s1.ActionName.ToString(),
                                  ActionId = o.ActionId,
                                  ActionNote = o.ActionNote,
                                  ActivityNote = o.ActionId == 6 ? o.Body : o.ActivityNote,
                                  MyInstallerId = o.MyInstallerId,
                                  CreationTime = o.CreationTime,
                                  ShowJobLink = link == true ? 1 : 2,
                                  ActivityDate = o.ActivityDate,
                                  ActivitysDate = o.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss"),
                                  CreatorUserName = s2 == null || s2.FullName == null ? "" : s2.FullName.ToString(),
                                  CompanyName = s2 == null || s2.CompanyName == null ? "" : s2.CompanyName.ToString()
                              };

            return new List<GetMyInstallerActivityLogDto>(
                   await myinstaller.ToListAsync()
               );
        }
        public bool checkExistList(int userid, int orgid)
        {
            var Result = false;
            Result = _myinstallerdocRepository.GetAll().Where(e => e.UserId == userid && e.OrganizationId == orgid).Any();

            return Result;
        }





        public async Task CreateOrEditOrganizationDoc(InstallerContractDto input)
        {

            var extc = Path.GetExtension(input.FileName);
            var OrgName = _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationId).Select(e => e.DisplayName).FirstOrDefault();
            var file = DateTime.Now.Ticks + "_" + OrgName + extc;
            byte[] ByteArray = _tempFileCacheManager.GetFile(input.FileToken);
            var filepath =

            _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, file, "MyInstallerOrganizationDoc", 0, 0, AbpSession.TenantId);

            MyInstallerDocument myInstallerdoc = new MyInstallerDocument();
            if (AbpSession.TenantId != null)
            {
                myInstallerdoc.TenantId = (int)AbpSession.TenantId;
            }
            myInstallerdoc.UserId = input.UserId;
            myInstallerdoc.OrganizationId = input.OrganizationId;
            myInstallerdoc.FileName = filepath.filename;
            myInstallerdoc.FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\MyInstallerOrganizationDoc\\" + file;


            MyInstallerActivityLog activity = new MyInstallerActivityLog();

            activity.ActionId = 39;
            activity.ActionNote = "Organization Doc Upload";
            activity.MyInstallerId = Convert.ToInt32(input.UserId);
            if (AbpSession.TenantId != null)
            {
                activity.TenantId = (int)AbpSession.TenantId;
            }
            _myinstalleractivityRepository.InsertAndGetId(activity);



            await _myinstallerdocRepository.InsertAsync(myInstallerdoc);

        }
        public async Task<List<InstallerContractDto>> getUserWiseDocList(int id)
        {
            var result = (from or in _myinstallerdocRepository.GetAll()
                          where (or.UserId == id)
                          join uo in _organizationUnitRepository.GetAll() on or.OrganizationId equals uo.Id into uoJoined
                          from uo in uoJoined.DefaultIfEmpty()

                          join user in _userRepository.GetAll() on or.UserId equals user.Id into userJoined
                          from user in userJoined.DefaultIfEmpty()

                          select new InstallerContractDto
                          {
                              Id = or.Id,
                              UserId = user.Id,
                              UserName = user.Name,
                              OrganizationName = uo.DisplayName,
                              FileName = or.FileName,
                              FilePath = or.FilePath,
                          }).ToList();

            return result;
        }

        public async Task DeleteMyInstallerDoc(int? id)
        {
            var docs = _myinstallerdocRepository.GetAll().Where(e => e.Id == id).FirstOrDefault();

            var Path = ApplicationSettingConsts.DocumentPath;
            var MainFolder = Path;
            var fileSavePath = Path + docs.FilePath;
            if (System.IO.File.Exists(fileSavePath))
            {
                try
                {
                    await _myinstallerdocRepository.DeleteAsync((int)id);
                    System.IO.File.Delete(fileSavePath);
                }
                catch (Exception ex)
                {
                    Exception e;
                }
            }
            MyInstallerActivityLog activity = new MyInstallerActivityLog();

            activity.ActionId = 40;
            activity.ActionNote = "Organization Doc Deleted";
            activity.MyInstallerId = Convert.ToInt32(docs.UserId);
            if (AbpSession.TenantId != null)
            {
                activity.TenantId = (int)AbpSession.TenantId;
            }
            _myinstalleractivityRepository.InsertAndGetId(activity);
        }

        public async Task<List<MyInstallerActivityHistoryDto>> GetMyInstallerActivityLogHistory(GetActivityLogInput input)
        {
            try
            {
                var Result = (from item in _myinstalleractivityHistoryRepository.GetAll()
                              join ur in _userRepository.GetAll() on item.CreatorUserId equals ur.Id into urjoined
                              from ur in urjoined.DefaultIfEmpty()
                              where (item.MyInstallerActionId == input.LeadId)
                              select new MyInstallerActivityHistoryDto()
                              {
                                  Id = item.Id,
                                  FieldName = item.FieldName,
                                  PrevValue = item.PrevValue,
                                  CurValue = item.CurValue,
                                  Lastmodifiedbyuser = ur.Name + " " + ur.Surname
                              })
                    .OrderByDescending(e => e.Id).ToList();
                return Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }



        public async Task ApproveInstaller(int? id)
        {
            var users = _userRepository.GetAll().Where(e => e.Id == id).FirstOrDefault();

            MyInstallerActivityLog activity = new MyInstallerActivityLog();

            activity.ActionId = _lookup_leadActionRepository.GetAll().Where(x => x.ActionName == "My Installer Modified").Select(x => x.Id).FirstOrDefault(); ;
            activity.ActionNote = "My Installer Approved";
            activity.MyInstallerId = Convert.ToInt32(id);
            if (AbpSession.TenantId != null)
            {
                activity.TenantId = (int)AbpSession.TenantId;
            }
            _myinstalleractivityRepository.InsertAndGetId(activity);

            users.IsApproveInstaller = true;
            _userRepository.Update(users);
        }
        public async Task NotApproveInstaller(int? id)
        {
            var users = _userRepository.GetAll().Where(e => e.Id == id).FirstOrDefault();
            MyInstallerActivityLog activity = new MyInstallerActivityLog();
            activity.ActionId = _lookup_leadActionRepository.GetAll().Where(x => x.ActionName == "My Installer Modified").Select(x => x.Id).FirstOrDefault(); ;
            activity.ActionNote = "My Installer UnApproved";
            activity.MyInstallerId = Convert.ToInt32(id);
            if (AbpSession.TenantId != null)
            {
                activity.TenantId = (int)AbpSession.TenantId;
            }
            _myinstalleractivityRepository.InsertAndGetId(activity);

            users.IsApproveInstaller = false;
            _userRepository.Update(users);
        }

        public async Task<FileDto> GetUsersPermissionReportToExcel(GetUsersToExcelInput input)
        {
            var query = await GetUsersFilteredQuery(input).ToListAsync();

            var output = from u in query
                         select new UserPermissiomExcelReport()
                         {
                             User = u.Name,
                             SalesDashboard = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_Dashboard_Sales),
                             InvoiceDashboard = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_Dashboard_Invoice),
                             ServiceDashboard = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_Dashboard_Service),
                             UnitTypes = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_UnitTypes),
                             StreetTypes = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_StreetTypes),
                             StreetNames = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_StreetNames),
                             PostCodes = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_PostCodes),
                             PostalTypes = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_PostalTypes),
                             LeadExpenses = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_LeadExpenses),
                             LeadSources = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_LeadSources),
                             SalesTeams = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Teams),
                             EmpCategories = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Categories),
                             CancelLeads = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_CancelReasons),
                             RejectLeads = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_RejectReasons),
                             HoldJobs = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_HoldReasons),
                             RefundReasons = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_RefundReasons),
                             JobCancellations = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_datavault_JobCancellationReason),
                             ElecDistributors = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ElecDistributors),
                             ElecRetailers = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ElecRetailers),
                             JobTypes = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_JobTypes),
                             JobStatuses = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_JobStatuses),
                             ProductTypes = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ProductTypes),
                             ProductItems = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ProductItems),
                             HouseTypes = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_HouseTypes),
                             RoofTypes = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_RoofTypes),
                             RoofAngles = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_RoofAngles),
                             PaymentMethods = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_PaymentOptions),
                             PaymentOptions = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_DepositOptions),
                             PaymentTerms = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_FinanceOptions),
                             PromotionTypes = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_PromotionMasters),
                             PriceVariations = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Variations),
                             InvoicePayments = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_InvoicePaymentMethods),
                             InvoiceStatuses = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_InvoiceStatuses),
                             EmailTemplates = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_DataVaults_EmailTemplates),
                             SMSTemplates = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_SmsTemplates),
                             FreebieTransports = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_FreebieTransports),
                             DocumentTypes = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_datavault_DocumentType),
                             DocumentLibraries = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_datavault_DocumentLibrary),
                             StcStatuses = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_StcPvdStatus),
                             ServiceCategories = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ServiceCategory),
                             ServiceSources = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ServiceSources),
                             ServiceStatus = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ServiceStatus),
                             ServiceTypes = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ServiceType),
                             ServiceSubCategories = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ServiceSubCategory),
                             ReviewTypes = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ReviewType),
                             PriceItems = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_PriceItemLists),
                             HtmlTemplates = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_QuotationTemplate),
                             PostCodeRanges = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_PostCodeRange),
                             WarehouseLocations = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_WarehouseLocation),
                             ProductPackages = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ProductPackages),
                             CallFlowQueues = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_CallFlowQueues),
                             CommissionRanges = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_CommissionRanges),
                             DashboardMessages = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_DashboardMessage),
                             InstallationCost_OtherCharges = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_DataVaults_InstallationCost_OtherCharges),
                             InstallationCost_StateWiseCost = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_DataVaults_InstallationCost_StateWiseInstallationCost),
                             InstallationCost_InstallationItemList = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_DataVaults_InstallationCost_InstallationItemList),
                             InstallationCost_FixedCostPrice = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_DataVaults_InstallationCost_FixedCostPrice),
                             InstallationCost_ExtraInstallationCharges = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_DataVaults_InstallationCost_ExtraInstallationCharges),
                             InstallationCost_STCCost = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_DataVaults_InstallationCost_STCCost),
                             InstallationCost_BatteryInstallationCost = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_DataVaults_InstallationCost_BatteryInstallationCost),
                             InstallationCost_CategoryInstallationItemList = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_DataVaults_InstallationCost_CategoryInstallationItemList),
                             ServiceDocumentTypes = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_datavault_ServiceDocumentType),
                             ManageLeads = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ManageLeads),
                             DuplicateLead = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Lead_Duplicate),
                             MyLeads = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_MyLeads),
                             LeadTracker = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_LeadTracker),
                             CancelLead = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Lead_Calcel),
                             RejectLead = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Lead_Rejects),
                             ClosedLead = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Lead_Closed),
                             Promotions = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Promotions),
                             PromotionTracker = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_PromotionUsers),
                             ApplicationTracker = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ApplicationTracker),
                             FreebiesTracker = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_FreebiesTracker),
                             FinanceTracker = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_FinanceTracker),
                             RefundTracker = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_RefundTracker),
                             ReferralTracker = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tracker_Referral),
                             JobActiveTracker = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_JobActiveTracker),
                             GridConnectTracker = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Gridconnectiontracker),
                             STCTracker = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_STCTracker),
                             WarrantyTracker = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tracker_Warranty),
                             HoldJobsTracker = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_HoldJobTracker),
                             Jobs = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_JobGrid),
                             //ServicesTracker = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Service_ServicesTracker),
                             InvoiceIssued = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_InvoiceIssuedTracker),
                             InvoicePaid = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_InvoiceTracker),
                             PayWay = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_InvoicePayWay),
                             //InvoiceFileList = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_HoldJobTracker),
                             MyInstaller = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_MyInstaller),
                             JobAssign = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_PendingInstallation),
                             Installation_Map = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Map),
                             JobBooking = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_JobBooking),
                             Installation_Calenders = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Calenders),
                             Installation = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Installation),
                             InstallerInvoice_New = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Installer_New),
                             PendingInvoice = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Installer_Invoice),
                             ReadyToPay = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Installer_ReadyToPay),
                             MyLeadsGeneration = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_LeadGeneration_MyLeadsGeneration),
                             LeadsGeneration_Installation = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_LeadGeneration_Installation),
                             LeadsGeneration_Map = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_LeadGeneration_Map),
                             Commission = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_LeadGeneration_Commission),
                             ManageService = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Lead_ManageService),
                             MyService = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_MyService),
                             ServiceMap = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ServiceMap),
                             ServiceInstallation = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ServiceInstallation),
                             WarrantyClaim = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_WarrantyClaim),
                             ServiceInvoice = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ServiceInvoice),
                             Review = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Review),
                             Notification_SMS = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Sms),
                             Notification_Email = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Email),
                             ActivityReport = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ActivityReport),
                             ToDoActivityReport = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_ToDoActivityReport),
                             LeadexpenseReport = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_HoldJobTracker),
                             OutStandingReport = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_LeadexpenseReport),
                             JobCostReport = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Report_JobCost),
                             LeadAssignReport = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Report_LeadAssign),
                             LeadSoldReport = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Report_LeadSold),
                             EmpJobCostReport = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Report_EmpJobCost),
                             JobCommissionReport = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Report_JobCommission),
                             JobCommissionPaidReport = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Report_JobCommissionPaid),
                             ProductSoldReport = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Report_ProductSold),
                             UserCallHistoryReport = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_CallHistory_UserCallHistory),
                             StateWiseCallHistoryReport = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_CallHistory_StateWiseCallHistory),
                             CallFlowQueueCallHistoryReport = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_CallHistory_CallFlowQueueCallHistory),
                             WholeSale_Dashboard = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_WholeSale_Dashboard),
                             WholeSaleStatuses = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_WholeSale_DataVaults_WholeSaleStatus),
                             WholeSale_DocumentTypes = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_WholeSale_DataVaults_DocumentType),
                             WholeSale_SmsTemplates = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_WholeSale_DataVaults_SmsTemplates),
                             WholeSale_EmailTemplates = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Tenant_WholeSale_DataVaults_EmailTemplates),
                             WholeSale_Leads = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_WholeSale_Leads),
                             WholeSale_Promotions = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_WholeSale_Promotions),
                             WholeSale_PromotionTracker = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_WholeSale_PromotionUsers),
                             WholeSale_Invoice = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_WholeSale_Invoice),
                             Organization = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Administration_OrganizationUnits),
                             Roles = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Administration_Roles),
                             Users = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Administration_Users),
                             Installer = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Administration_Installer),
                             SalesRep = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Administration_SalesRep),
                             SalesManager = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Administration_SalesManager),
                             AuditLogs = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Administration_AuditLogs),
                             Maintenance = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Administration_Host_Maintenance),
                             HostSettings = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Administration_Host_Settings),
                             TenantSettings = PermissionChecker.IsGranted(new Abp.UserIdentifier(u.TenantId, u.Id), AppPermissions.Pages_Administration_Tenant_Settings),
                         };



            return _userListExcelExporter.UserPermissiomExportToFile(output.ToList());
        }
    }
}
