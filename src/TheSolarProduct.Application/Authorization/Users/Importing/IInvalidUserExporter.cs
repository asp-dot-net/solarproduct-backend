﻿using System.Collections.Generic;
using TheSolarProduct.Authorization.Users.Importing.Dto;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Authorization.Users.Importing
{
    public interface IInvalidUserExporter
    {
        FileDto ExportToFile(List<ImportUserDto> userListDtos);
    }
}
