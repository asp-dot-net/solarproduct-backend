﻿using System.Collections.Generic;
using TheSolarProduct.Authorization.Users.Importing.Dto;
using Abp.Dependency;

namespace TheSolarProduct.Authorization.Users.Importing
{
    public interface IUserListExcelDataReader: ITransientDependency
    {
        List<ImportUserDto> GetUsersFromExcel(byte[] fileBytes);
    }
}
