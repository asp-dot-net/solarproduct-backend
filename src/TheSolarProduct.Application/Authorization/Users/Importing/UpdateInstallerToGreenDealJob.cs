﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.Threading;
using Abp.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Jobs.Importing.Dto;
using TheSolarProduct.Notifications;
using TheSolarProduct.Organizations;
using TheSolarProduct.Storage;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Authorization.Users.Dto;
using TheSolarProduct.Jobs;
using TheSolarProduct.ThirdPartyApis.GreenDeals.Dtos;
using TheSolarProduct.Installer;

namespace TheSolarProduct.Authorization.Users.Importing
{
    public class UpdateInstallerToGreenDealJob : BackgroundJob<UpdateInstallerToGreenDealJobArgs>, ITransientDependency
    {
        private readonly IAppNotifier _appNotifier;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<STCProvider> _stcProviderRepository;
        private readonly IRepository<InstallerDetail, int> _installerDetailRepository;
        private readonly IRepository<User, long> _userRepository;

        public UserManager UserManager { get; set; }

        private static string endPoint = "https://www.greendeal.com.au/rapi/v1"; // Production Env

        public UpdateInstallerToGreenDealJob(IAppNotifier appNotifier
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<STCProvider> stcProviderRepository
            , IRepository<InstallerDetail, int> installerDetailRepository
            , IRepository<User, long> userRepository
            )
        {
            _appNotifier = appNotifier;
            _unitOfWorkManager = unitOfWorkManager;
            _stcProviderRepository = stcProviderRepository;
            _installerDetailRepository = installerDetailRepository;
            _userRepository = userRepository;
        }

        [UnitOfWork]
        public override void Execute(UpdateInstallerToGreenDealJobArgs args)
        {
            var installers = GetInstallerListFromGreenDeal(args);
            if (installers.Result == null || !installers.Result.Any())
            {
                SendErrorNotification(args);
                return;
            }
            else
            {
                bool hasError = false;
                try
                {
                    AsyncHelper.RunSync(() => UpdateInstallerAsync(installers.Result, args));
                }
                catch (Exception e)
                {
                    hasError = true;
                }

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                    {
                        AsyncHelper.RunSync(() => ProcessUpdateInstallerResultAsync(args, hasError));
                    }
                    uow.Complete();
                }
            }
        }

        private async Task<List<InstallersObj>> GetInstallerListFromGreenDeal(UpdateInstallerToGreenDealJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    try
                    {
                        var stcProvider = await _stcProviderRepository.GetAll().Where(e => e.Provider == "Green Deal").FirstOrDefaultAsync();

                        var rootObject = await GetInstallerAsync(stcProvider.UserId, stcProvider.Password);

                        return rootObject.Response.Installers;
                    }
                    catch (Exception)
                    {
                        return new List<InstallersObj>();
                    }
                    finally
                    {
                        uow.Complete();
                    }
                }
            }
        }

        private async Task UpdateInstallerAsync(List<InstallersObj> input, UpdateInstallerToGreenDealJobArgs args)
        {
            using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
            {
                foreach (var installer in input)
                {
                    var existData = _installerDetailRepository.GetAll().Include(e => e.UserFk).Where(e => e.UserFk.IsDeleted == false && e.InstallerAccreditationNumber.ToLower() == installer.AccreditationNumber.ToLower()).FirstOrDefault();
                    if (existData != null)
                    {
                        //var user = await UserManager.FindByIdAsync(existData.ToString());
                        //await UserManager.UpdateAsync(user);

                        //var user = await _userRepository.FirstOrDefaultAsync(existData);
                        //await _userRepository.UpdateAsync(user);

                        existData.Name = installer.FirstName;
                        existData.Surname = installer.LastName;
                        existData.ElectricianLicenseNumber = installer.ElectricianLicense;
                        existData.SAANumber = installer.SAANumber;

                        if(installer.GridType == "Install Only")
                        {
                            existData.IsDesi = false;
                        }
                        else if(installer.GridType == "Design & Install")
                        {
                            existData.IsDesi = true;
                        }

                        await _installerDetailRepository.UpdateAsync(existData);
                    }
                }
            }
        }

        private void SendErrorNotification(UpdateInstallerToGreenDealJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(args.User, "Something is wrong from greendeal.", Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }

        private async Task ProcessUpdateInstallerResultAsync(UpdateInstallerToGreenDealJobArgs args, bool hasError)
        {
            if (hasError)
            {
                await _appNotifier.SendMessageAsync(args.User, "Something is wrong.", Abp.Notifications.NotificationSeverity.Error);
            }
            else
            {
                await _appNotifier.SendMessageAsync(args.User, "Installer Updated Successfully.", Abp.Notifications.NotificationSeverity.Success);
            }
        }

        public static string GetEncryptedSignature(string timestamp, string clientId, string clientSecret)
        {
            string signature_string = "GD:" + clientId + timestamp + clientSecret;

            string hash = BitConverter.ToString(MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(signature_string))).Replace("-", "");

            return hash.ToLower();
        }

        public static string GetTimeStamp(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-ddTHH:mm:ss:z");
        }

        private async Task<InstallerRootObject> GetInstallerAsync(string clientId, string clientSecret)
        {
            var url = endPoint + "/installers";

            var timestamp = GetTimeStamp(DateTime.UtcNow);
            var encryptedSignature = GetEncryptedSignature(timestamp, clientId, clientSecret);

            var temp = new
            {
                client_id = clientId,
                signature = encryptedSignature,
                timestamp = timestamp
            };

            var content = JsonConvert.SerializeObject(temp, Formatting.Indented);
            HttpContent httpContent = new StringContent(content.ToString(), Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("Accept", "*/*");
            request.Content = httpContent;
            var response = await client.SendAsync(request);

            //return response;

            bool IsSuccess = response.IsSuccessStatusCode;

            if (IsSuccess)
            {
                var jsonResponse = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<InstallerRootObject>(jsonResponse);
            }
            else
            {
                return null;
            }
        }
    }
}
