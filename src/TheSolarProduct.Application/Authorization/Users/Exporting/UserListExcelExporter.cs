﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abp.Collections.Extensions;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.Authorization.Users.Dto;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using NPOI.SS.UserModel;

namespace TheSolarProduct.Authorization.Users.Exporting
{
    public class UserListExcelExporter : NpoiExcelExporterBase, IUserListExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly PermissionChecker _permissionChecker;

        public UserListExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager
            , PermissionChecker permissionChecker)
            : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _permissionChecker = permissionChecker;
        }

        public FileDto ExportToFile(List<UserListDto> userListDtos)
        {
            return CreateExcelPackage(
                "UserList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Users"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Surname"),
                        L("UserName"),
                        L("PhoneNumber"),
                        L("EmailAddress"),
                        L("EmailConfirm"),
                        L("Roles"),
                        L("Active"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, userListDtos,
                        _ => _.Name,
                        _ => _.Surname,
                        _ => _.UserName,
                        _ => _.PhoneNumber,
                        _ => _.EmailAddress,
                        _ => _.IsEmailConfirmed,
                        _ => _.Roles.Select(r => r.RoleName).JoinAsString(", "),
                        _ => _.IsActive,
                        _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())
                        );
                    
                    for (var i = 1; i <= userListDtos.Count; i++)
                    {
                        //Formatting cells
                        SetCellDataFormat(sheet.GetRow(i).Cells[8], "yyyy-mm-dd");
                    }
                    
                    for (var i = 0; i < 9; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto UserPermissiomExportToFile(List<UserPermissiomExcelReport> userListDtos)
        {
            return CreateExcelPackage(
                "UserPermissionList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Users"));

                    AddHeader(
                        sheet,
                        "Name",
                        "Sales Dashboard",
                        "Invoice Dashboard",
                        "Service Dashboard",
                        "Unit Types",
                        "Street Types",
                        "Street Names",
                        "Post Codes",
                        "Postal Types",
                        "Lead Expenses",
                        "Lead Sources",
                        "Sales Teams",
                        "Emp Categories",
                        "Cancel Leads",
                        "Reject Leads",
                        "Hold Jobs",
                        "Refund Reason",
                        "Job Cancellation",
                        "Elec Distributor",
                        "Elec Retailer",
                        "Job Types",
                        "Job Statuses",
                        "Product Type",
                        "Product Items",
                        "House Type",
                        "Roof Type",
                        "Roof Angle",
                        "Payment Method",
                        "Payment Options",
                        "Payment Term",
                        "Promotion Type",
                        "Price Variations",
                        "Invoice Payments",
                        "Invoice Status",
                        "Email Templates",
                        "SMS Templates",
                        "Freebie Transports",
                        "Document Type",
                        "Document Library",
                        "Stc Status",
                        "Service Category",
                        "Service Sources",
                        "Service Status",
                        "Service Type",
                        "Service SubCategory",
                        "Review Type",
                        "Price Item",
                        "Html Template",
                        "Post Code Range",
                        "Warehouse Location",
                        "Product Packages",
                        "Call Flow Queue",
                        "Commission Range",
                        "Dashboard Message",
                        "Installation Cost Other Charges",
                        "Installation Cost State Wise Cost",
                        "Installation Cost Installation Item List",
                        "Installation Cost Fixed Cost Price",
                        "Installation Cost Extra Ins Charges",
                        "Installation STC Cost",
                        "Battery Installation Cost",
                        "Category Installation Item",
                        "Service Document Type",
                        "Manage Leads",
                        "Duplicate Lead",
                        "My Lead ",
                        "Lead Tracker",
                        "Cancel Lead",
                        "Reject Lead",
                        "Closed Lead",
                        "Promotions",
                        "Promotion Tracker",
                        "Application Tracker",
                        "Freebies Tracker",
                        "Finance Tracker",
                        "Refund Tracker",
                        "Referral Tracker",
                        "Job Active Tracker",
                        "Grid Connect Tracker",
                        "STC Tracker",
                        "Warranty Tracker",
                        "Hold Jobs Tracker",
                        "Jobs",
                       // "Service Tracker",
                        "Invoice Issued",
                        "Invoice Paid",
                        "PayWay",
                        "Invoice File List",
                        "My Installer",
                        "Job Assign",
                        "Installation Map",
                        "Job Booking",
                        "Installation Calender",
                        "Installation",
                        "Installer Invoice New",
                        "Installer Pending Invoice",
                        "Installer Invoice Ready To Pay",
                        "My Leads Generation",
                        "Leads Generation Installation",
                        "Leads Generation Map",
                        "Leads Generation Commission",
                        "Manage Service",
                        "My Service",
                        "Service Map",
                        "Service Installation",
                        "Warrenty Claim",
                        "Service Invoice",
                        "Review",
                        "Notification SMS",
                        "Notification Email",
                        "Activity Report",
                        "ToDo Activity Report",
                        "Lead Expense Report",
                        "OutStanding Report",
                        "Job Cost Report",
                        "Lead Assign Report",
                        "Lead Sold Report",
                        "Employee Job Cost Report",
                        "Job Commission Report",
                        "Job Commission Paid Report",
                        "Product Sold Report",
                        "User Call History Report",
                        "State Wise Report",
                        "3CX Call Queue Report",
                        "Wholesale Dashboard",
                        "Wholesale Status",
                        "Wholesale Document Type",
                        "Wholesale Sms Tamplate",
                        "Wholesale Email Tamplate",
                        "Wholesale Leads",
                        "Wholesale Promotions",
                        "Wholesale Promotion Tracker",
                        "Wholesale Invoice",
                        "Organization",
                        "Roles",
                        "Users",
                        "Installer",
                        "Sales Rep",
                        "Sales Manager",
                        "Audit Logs",
                        "Maintenance",
                        "Host Settings",
                        "Tenant Settings"
                        );

                    AddObjects(
                        sheet, 2, userListDtos,
                        _ => _.User,
                        _ => _.SalesDashboard != true ? "No" : "YES",
                        _ => _.InvoiceDashboard != true ? "No" : "YES",
                        _ => _.ServiceDashboard != true ? "No" : "YES",
                        _ => _.UnitTypes != true ? "No" : "YES",
                        _ => _.StreetTypes != true ? "No" : "YES",
                        _ => _.StreetNames != true ? "No" : "YES",
                        _ => _.PostCodes != true ? "No" : "YES",
                        _ => _.PostalTypes != true ? "No" : "YES",
                        _ => _.LeadExpenses != true ? "No" : "YES",
                        _ => _.LeadSources != true ? "No" : "YES",
                        _ => _.SalesTeams != true ? "No" : "YES",
                        _ => _.EmpCategories != true ? "No" : "YES",
                        _ => _.CancelLeads != true ? "No" : "YES",
                        _ => _.RejectLeads != true ? "No" : "YES",
                        _ => _.HoldJobs != true ? "No" : "YES",
                        _ => _.RefundReasons != true ? "No" : "YES",
                        _ => _.JobCancellations != true ? "No" : "YES",
                        _ => _.ElecDistributors != true ? "No" : "YES",
                        _ => _.ElecRetailers != true ? "No" : "YES",
                        _ => _.JobTypes != true ? "No" : "YES",
                        _ => _.JobStatuses != true ? "No" : "YES",
                        _ => _.ProductTypes != true ? "No" : "YES",
                        _ => _.ProductItems != true ? "No" : "YES",
                        _ => _.HouseTypes != true ? "No" : "YES",
                        _ => _.RoofTypes != true ? "No" : "YES",
                        _ => _.RoofAngles != true ? "No" : "YES",
                        _ => _.PaymentMethods != true ? "No" : "YES",
                        _ => _.PaymentOptions != true ? "No" : "YES",
                        _ => _.PaymentTerms != true ? "No" : "YES",
                        _ => _.PromotionTypes != true ? "No" : "YES",
                        _ => _.PriceVariations != true ? "No" : "YES",
                        _ => _.InvoicePayments != true ? "No" : "YES",
                        _ => _.InvoiceStatuses != true ? "No" : "YES",
                        _ => _.EmailTemplates != true ? "No" : "YES",
                        _ => _.SMSTemplates != true ? "No" : "YES",
                        _ => _.FreebieTransports != true ? "No" : "YES",
                        _ => _.DocumentTypes != true ? "No" : "YES",
                        _ => _.DocumentLibraries != true ? "No" : "YES",
                        _ => _.StcStatuses != true ? "No" : "YES",
                        _ => _.ServiceCategories != true ? "No" : "YES",
                        _ => _.ServiceSources != true ? "No" : "YES",
                        _ => _.ServiceStatus != true ? "No" : "YES",
                        _ => _.ServiceTypes != true ? "No" : "YES",
                        _ => _.ServiceSubCategories != true ? "No" : "YES",
                        _ => _.ReviewTypes != true ? "No" : "YES",
                        _ => _.PriceItems != true ? "No" : "YES",
                        _ => _.HtmlTemplates != true ? "No" : "YES",
                        _ => _.PostCodeRanges != true ? "No" : "YES",
                        _ => _.WarehouseLocations != true ? "No" : "YES",
                        _ => _.ProductPackages != true ? "No" : "YES",
                        _ => _.CallFlowQueues != true ? "No" : "YES",
                        _ => _.CommissionRanges != true ? "No" : "YES",
                        _ => _.DashboardMessages != true ? "No" : "YES",
                        _ => _.InstallationCost_OtherCharges != true ? "No" : "YES",
                        _ => _.InstallationCost_StateWiseCost != true ? "No" : "YES",
                        _ => _.InstallationCost_InstallationItemList != true ? "No" : "YES",
                        _ => _.InstallationCost_FixedCostPrice != true ? "No" : "YES",
                        _ => _.InstallationCost_ExtraInstallationCharges != true ? "No" : "YES",
                        _ => _.InstallationCost_STCCost != true ? "No" : "YES",
                        _ => _.InstallationCost_BatteryInstallationCost != true ? "No" : "YES",
                        _ => _.InstallationCost_CategoryInstallationItemList != true ? "No" : "YES",
                        _ => _.ServiceDocumentTypes != true ? "No" : "YES",
                        _ => _.ManageLeads != true ? "No" : "YES",
                        _ => _.DuplicateLead != true ? "No" : "YES",
                        _ => _.MyLeads != true ? "No" : "YES",
                        _ => _.LeadTracker != true ? "No" : "YES",
                        _ => _.CancelLead != true ? "No" : "YES",
                        _ => _.RejectLead != true ? "No" : "YES",
                        _ => _.ClosedLead != true ? "No" : "YES",
                        _ => _.Promotions != true ? "No" : "YES",
                        _ => _.PromotionTracker != true ? "No" : "YES",
                        _ => _.ApplicationTracker != true ? "No" : "YES",
                        _ => _.FreebiesTracker != true ? "No" : "YES",
                        _ => _.FinanceTracker != true ? "No" : "YES",
                        _ => _.RefundTracker != true ? "No" : "YES",
                        _ => _.ReferralTracker != true ? "No" : "YES",
                        _ => _.JobActiveTracker != true ? "No" : "YES",
                        _ => _.GridConnectTracker != true ? "No" : "YES",
                        _ => _.STCTracker != true ? "No" : "YES",
                        _ => _.WarrantyTracker != true ? "No" : "YES",
                        _ => _.HoldJobsTracker != true ? "No" : "YES",
                        _ => _.Jobs != true ? "No" : "YES",
                        //_ => _.ServicesTracker != true ? "No" : "YES",
                        _ => _.InvoiceIssued != true ? "No" : "YES",
                        _ => _.InvoicePaid != true ? "No" : "YES",
                        _ => _.PayWay != true ? "No" : "YES",
                        _ => _.InvoiceFileList != true ? "No" : "YES",
                        _ => _.MyInstaller != true ? "No" : "YES",
                        _ => _.JobAssign != true ? "No" : "YES",
                        _ => _.Installation_Map != true ? "No" : "YES",
                        _ => _.JobBooking != true ? "No" : "YES",
                        _ => _.Installation_Calenders != true ? "No" : "YES",
                        _ => _.Installation != true ? "No" : "YES",
                        _ => _.InstallerInvoice_New != true ? "No" : "YES",
                        _ => _.PendingInvoice != true ? "No" : "YES",
                        _ => _.ReadyToPay != true ? "No" : "YES",
                        _ => _.MyLeadsGeneration != true ? "No" : "YES",
                        _ => _.LeadsGeneration_Installation != true ? "No" : "YES",
                        _ => _.LeadsGeneration_Map != true ? "No" : "YES",
                        _ => _.Commission != true ? "No" : "YES",
                        _ => _.ManageService != true ? "No" : "YES",
                        _ => _.MyService != true ? "No" : "YES",
                        _ => _.ServiceMap != true ? "No" : "YES",
                        _ => _.ServiceInstallation != true ? "No" : "YES",
                        _ => _.WarrantyClaim != true ? "No" : "YES",
                        _ => _.ServiceInvoice != true ? "No" : "YES",
                        _ => _.Review != true ? "No" : "YES",
                        _ => _.Notification_SMS != true ? "No" : "YES",
                        _ => _.Notification_Email != true ? "No" : "YES",
                        _ => _.ActivityReport != true ? "No" : "YES",
                        _ => _.ToDoActivityReport != true ? "No" : "YES",
                        _ => _.LeadexpenseReport != true ? "No" : "YES",
                        _ => _.OutStandingReport != true ? "No" : "YES",
                        _ => _.JobCostReport != true ? "No" : "YES",
                        _ => _.LeadAssignReport != true ? "No" : "YES",
                        _ => _.LeadSoldReport != true ? "No" : "YES",
                        _ => _.EmpJobCostReport != true ? "No" : "YES",
                        _ => _.JobCommissionReport != true ? "No" : "YES",
                        _ => _.JobCommissionPaidReport != true ? "No" : "YES",
                        _ => _.ProductSoldReport != true ? "No" : "YES",
                        _ => _.UserCallHistoryReport != true ? "No" : "YES",
                        _ => _.StateWiseCallHistoryReport != true ? "No" : "YES",
                        _ => _.CallFlowQueueCallHistoryReport != true ? "No" : "YES",
                        _ => _.WholeSale_Dashboard != true ? "No" : "YES",
                        _ => _.WholeSaleStatuses != true ? "No" : "YES",
                        _ => _.WholeSale_DocumentTypes != true ? "No" : "YES",
                        _ => _.WholeSale_SmsTemplates != true ? "No" : "YES",
                        _ => _.WholeSale_EmailTemplates != true ? "No" : "YES",
                        _ => _.WholeSale_Leads != true ? "No" : "YES",
                        _ => _.WholeSale_Promotions != true ? "No" : "YES",
                        _ => _.WholeSale_PromotionTracker != true ? "No" : "YES",
                        _ => _.WholeSale_Invoice != true ? "No" : "YES",
                        _ => _.Organization != true ? "No" : "YES",
                        _ => _.Roles != true ? "No" : "YES",
                        _ => _.Users != true ? "No" : "YES",
                        _ => _.Installer != true ? "No" : "YES",
                        _ => _.SalesRep != true ? "No" : "YES",
                        _ => _.SalesManager != true ? "No" : "YES",
                        _ => _.AuditLogs != true ? "No" : "YES",
                        _ => _.Maintenance != true ? "No" : "YES",
                        _ => _.HostSettings != true ? "No" : "YES",
                        _ => _.TenantSettings != true ? "No" : "YES"
                        );

                    //for (var i = 1; i <= userListDtos.Count; i++)
                    //{
                    //    //Formatting cells
                    //    setcell(sheet.GetRow(i).Cells[8], "yyyy-mm-dd");
                    //}

                    for (var i = 0; i < 9; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto MyInstallerExportToFile(List<GetInstallerDto> userListDtos)
        {
            var EmailPermission = _permissionChecker.IsGranted(AppPermissions.Pages_MyInstaller_Export_MobileEmail);

            if (EmailPermission)
            {
                if (userListDtos[0].ismyinstalleruser == false)
                {
                    return CreateExcelPackage(
                            "UserList.xlsx",
                            excelPackage =>
                            {
                                var sheet = excelPackage.CreateSheet(L("Users"));

                                AddHeader(
                                    sheet,
                                    L("Name"),
                                    L("Surname"),
                                    L("UserName"),
                                    L("PhoneNumber"),
                                    L("EmailAddress"),
                                    L("EmailConfirm"),
                                    L("Roles"),
                                    L("Active"),
                                    L("CreationTime")
                                    );

                                AddObjects(
                                    sheet, 2, userListDtos,
                                    _ => _.Name,
                                    _ => _.Surname,
                                    _ => _.UserName,
                                    _ => _.Mobile,
                                    _ => _.EmailAddress,
                                    _ => _.IsEmailConfirmed,
                                    _ => _.Roles.Select(r => r.RoleName).JoinAsString(", "),
                                    _ => _.IsActive,
                                    _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())
                                    );

                                for (var i = 1; i <= userListDtos.Count; i++)
                                {
                                    //Formatting cells
                                    SetCellDataFormat(sheet.GetRow(i).Cells[8], "yyyy-mm-dd");
                                }

                                for (var i = 0; i < 8; i++)
                                {
                                    sheet.AutoSizeColumn(i);
                                }
                            });
                }
                else
                {
                    return CreateExcelPackage(
                                "MyInstallerUserList.xlsx",
                                excelPackage =>
                                {
                                    var sheet = excelPackage.CreateSheet(L("Users"));

                                    AddHeader(
                                        sheet,
                                        L("Name"),
                                        L("Surname"),
                                        L("PhoneNumber"),
                                        L("EmailAddress"),
                                        L("EmailConfirm"),
                                        L("Roles"),
                                        L("Active"),
                                        L("CreationTime")
                                        );

                                    AddObjects(
                                        sheet, 2, userListDtos,
                                        _ => _.Name,
                                        _ => _.Surname,
                                        _ => _.Mobile,
                                        _ => _.EmailAddress,
                                        _ => _.IsEmailConfirmed,
                                        _ => _.Roles.Select(r => r.RoleName).JoinAsString(", "),
                                        _ => _.IsActive,
                                        _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())
                                        );

                                    for (var i = 1; i <= userListDtos.Count; i++)
                                    {
                                        //Formatting cells
                                        SetCellDataFormat(sheet.GetRow(i).Cells[7], "yyyy-mm-dd");
                                    }

                                    for (var i = 0; i < 9; i++)
                                    {
                                        sheet.AutoSizeColumn(i);
                                    }
                                });
                }

            }
            else
            {
                if (userListDtos[0].ismyinstalleruser == false)
                {
                    return CreateExcelPackage(
                            "UserList.xlsx",
                            excelPackage =>
                            {
                                var sheet = excelPackage.CreateSheet(L("Users"));

                                AddHeader(
                                    sheet,
                                    L("Name"),
                                    L("Surname"),
                                    L("UserName"),
                                    L("EmailConfirm"),
                                    L("Roles"),
                                    L("Active"),
                                    L("CreationTime")
                                    );

                                AddObjects(
                                    sheet, 2, userListDtos,
                                    _ => _.Name,
                                    _ => _.Surname,
                                    _ => _.UserName,
                                    _ => _.IsEmailConfirmed,
                                    _ => _.Roles.Select(r => r.RoleName).JoinAsString(", "),
                                    _ => _.IsActive,
                                    _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())
                                    );

                                for (var i = 1; i <= userListDtos.Count; i++)
                                {
                                    //Formatting cells
                                    SetCellDataFormat(sheet.GetRow(i).Cells[6], "yyyy-mm-dd");
                                }

                                for (var i = 0; i < 8; i++)
                                {
                                    sheet.AutoSizeColumn(i);
                                }
                            });
                }
                else
                {
                    return CreateExcelPackage(
                                "MyInstallerUserList.xlsx",
                                excelPackage =>
                                {
                                    var sheet = excelPackage.CreateSheet(L("Users"));

                                    AddHeader(
                                        sheet,
                                        L("Name"),
                                        L("Surname"),
                                        L("EmailConfirm"),
                                        L("Roles"),
                                        L("Active"),
                                        L("CreationTime")
                                        );

                                    AddObjects(
                                        sheet, 2, userListDtos,
                                        _ => _.Name,
                                        _ => _.Surname,
                                        _ => _.IsEmailConfirmed,
                                        _ => _.Roles.Select(r => r.RoleName).JoinAsString(", "),
                                        _ => _.IsActive,
                                        _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())
                                        );

                                    for (var i = 1; i <= userListDtos.Count; i++)
                                    {
                                        //Formatting cells
                                        SetCellDataFormat(sheet.GetRow(i).Cells[5], "yyyy-mm-dd");
                                    }

                                    for (var i = 0; i < 9; i++)
                                    {
                                        sheet.AutoSizeColumn(i);
                                    }
                                });
                }

            }
        }
    }
}
