using System.Collections.Generic;
using TheSolarProduct.Authorization.Users.Dto;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
        FileDto MyInstallerExportToFile(List<GetInstallerDto> userListDtos);

        FileDto UserPermissiomExportToFile(List<UserPermissiomExcelReport> userListDtos);

    }
}