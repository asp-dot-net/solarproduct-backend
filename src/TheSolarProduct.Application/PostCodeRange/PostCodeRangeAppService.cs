﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using System.Linq;
using System.Threading.Tasks;
using TheSolarProduct.PostCodeRange.Dtos;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using System;
using TheSolarProduct.Leads;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using TheSolarProduct.PostCodes;

namespace TheSolarProduct.PostCodeRange
{
    [AbpAuthorize]
    public class PostCodeRangeAppService : TheSolarProductAppServiceBase, IPostCodeRangeAppService
    {
		private readonly IRepository<PostCodeRange> _postCodeRangeRepository;
		private readonly IRepository<Lead> _leadsRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public PostCodeRangeAppService(IRepository<PostCodeRange> postCodeRangeRepository, IRepository<Lead> leadsRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
		{
			_postCodeRangeRepository = postCodeRangeRepository;
			_leadsRepository = leadsRepository;
			_dbcontextprovider = dbcontextprovider;
			_dataVaultActivityLogRepository = dataVaultActivityLogRepository;
        }

		public async Task<PagedResultDto<GetPostCodeRangeForViewDto>> GetAll(GetAllPostCodeRangeForInput input)
        {
			var filtered = _postCodeRangeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.StartPostCode <= Convert.ToInt64(input.Filter) && e.EndPostCode >= Convert.ToInt64(input.Filter));

			var pagedAndFiltered = filtered
				.OrderBy(input.Sorting ?? "id desc")
				.PageBy(input);

			var postCodeRanges = from o in pagedAndFiltered
								select new GetPostCodeRangeForViewDto()
								{
									PostCodeRange = new PostCodeRangeDto
									{
										StartPostCode = o.StartPostCode,
										EndPostCode = o.EndPostCode,
										Area = o.Area,
										Id = o.Id,
										IsActive = o.IsActive,	
									}
								};

			var totalCount = await filtered.CountAsync();

			return new PagedResultDto<GetPostCodeRangeForViewDto>(
				totalCount,
				await postCodeRanges.ToListAsync()
			);
		}

		[AbpAuthorize(AppPermissions.Pages_PostCodeRange_Edit)]
		public async Task<GetPostCodeRangeForEditOutput> GetPostCodeRangeForEdit(EntityDto input)
		{
			var postCodeRange = await _postCodeRangeRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetPostCodeRangeForEditOutput { PostCodeRange = ObjectMapper.Map<CreateOrEditPostCodeRangeDto>(postCodeRange) };

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditPostCodeRangeDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_PostCodeRange_Create)]
		protected virtual async Task Create(CreateOrEditPostCodeRangeDto input)
		{
			var postCodeRange = ObjectMapper.Map<PostCodeRange>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 40;
            dataVaultLog.ActionNote = "Post Code Range Created : " + postCodeRange.StartPostCode + " - " + postCodeRange.EndPostCode;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _postCodeRangeRepository.InsertAsync(postCodeRange);
		}

		[AbpAuthorize(AppPermissions.Pages_PostCodeRange_Edit)]
		protected virtual async Task Update(CreateOrEditPostCodeRangeDto input)
		{
			var postCode = await _postCodeRangeRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 40;
            dataVaultLog.ActionNote = "Post Code Range Updated : " + postCode.StartPostCode + " - " + postCode.EndPostCode;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.StartPostCode != postCode.StartPostCode)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "StartPostCode";
                history.PrevValue = postCode.StartPostCode.ToString();
                history.CurValue = input.StartPostCode.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.EndPostCode != postCode.EndPostCode)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "EndPostCode";
                history.PrevValue = postCode.EndPostCode.ToString();
                history.CurValue = input.EndPostCode.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Area != postCode.Area)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Area";
                history.PrevValue = postCode.Area;
                history.CurValue = input.Area;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != postCode.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = postCode.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, postCode);

			var postCodeLeads = _leadsRepository.GetAll().Where(e => e.PostCode != null && input.StartPostCode <= (e.PostCode != null ? Convert.ToInt64(e.PostCode) : 0) && input.EndPostCode >= (e.PostCode != null ? Convert.ToInt64(e.PostCode) : 0));


			foreach (var item in postCodeLeads)
			{
				item.Area = input.Area;
				await _leadsRepository.UpdateAsync(item);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_PostCodeRange_Delete)]
		public async Task Delete(EntityDto input)
		{
            var postCode = _postCodeRangeRepository.Get(input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 40;
            dataVaultLog.ActionNote = "Post Code Range Deleted : " + postCode.StartPostCode + " - " + postCode.EndPostCode;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _postCodeRangeRepository.DeleteAsync(input.Id);
		}

		public async Task<string> GetAreaByPostCodeRange(string postCode)
        {
			var result = await _postCodeRangeRepository.GetAll().Where(e => e.StartPostCode <= Convert.ToInt64(postCode) && e.EndPostCode >= Convert.ToInt64(postCode)).FirstOrDefaultAsync();

			return result == null ? "" : result.Area;
        }
	}
}
