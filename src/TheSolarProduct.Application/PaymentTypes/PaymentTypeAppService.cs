﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataVaults;
using TheSolarProduct.Dto;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.PaymentStatuses.Dtos;
using TheSolarProduct.PaymentStatuses.Exporting;
using TheSolarProduct.PaymentStatuses;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.PaymentTypes.Dtos;
using TheSolarProduct.PaymentTypes.Exporting;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using System.Linq;
using Microsoft.EntityFrameworkCore;
namespace TheSolarProduct.PaymentTypes
{
    
    [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentType)]
    public class PaymentTypeAppService : TheSolarProductAppServiceBase, IPaymentTypeAppService
    {
        private readonly IPaymentTypeExcelExporter _paymentTypeExcelExporter;
        private readonly IRepository<PaymentType> _paymentTypeRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public PaymentTypeAppService(
              IRepository<PaymentType> paymentTypeRepository,
              IPaymentTypeExcelExporter IPaymentTypeExcelExporter,
              IRepository<UserTeam> userTeamRepository,
              IRepository<UserRole, long> userRoleRepository,
              IRepository<Role> roleRepository,
              IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
              )
        {
            _paymentTypeRepository = paymentTypeRepository;
            _paymentTypeExcelExporter = IPaymentTypeExcelExporter;
            _userTeamRepository = userTeamRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }




        public async Task<PagedResultDto<GePaymentTypeForViewDto>> GetAll(GetAllPyamentTypeInput input)
        {

            var PaymentType = _paymentTypeRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFilteredPaymentType = PaymentType
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var outputPyamentType = from o in pagedAndFilteredPaymentType
                                       select new GePaymentTypeForViewDto()

                                      {
                                           PaymentType = new PaymentTypeDto
                                          {
                                              Id = o.Id,
                                              Name = o.Name,
                                              IsActive = o.IsActive
                                          }
                                      };

            var totalCount = await PaymentType.CountAsync();

            return new PagedResultDto<GePaymentTypeForViewDto>(
                totalCount,
                await outputPyamentType.ToListAsync()
            );
        }



        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentType_Edit)]
        public async Task<GetPaymentTypeForEditOutput> GetPaymentTypeForEdit(EntityDto input)
        {
            var PaymentType = await _paymentTypeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPaymentTypeForEditOutput { PaymentType = ObjectMapper.Map<CreateOrEditPaymentTypeDto>(PaymentType) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditPaymentTypeDto input)
        {


            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentType_Create)]

        protected virtual async Task Create(CreateOrEditPaymentTypeDto input)
        {
            var PaymentType = ObjectMapper.Map<PaymentType>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 65;
            dataVaultLog.ActionNote = "PaymentType Created : " + input.Name;
            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _paymentTypeRepository.InsertAsync(PaymentType);
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseCompany_Edit)]
        protected virtual async Task Update(CreateOrEditPaymentTypeDto input)
        {
            var PaymentType = await _paymentTypeRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 65;
            dataVaultLog.ActionNote = "PaymentType Updated : " + PaymentType.Name;

            dataVaultLog.IsInventory = true;
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != PaymentType.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = PaymentType.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != PaymentType.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = PaymentType.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, PaymentType);

            await _paymentTypeRepository.UpdateAsync(PaymentType);


        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentType_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _paymentTypeRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 65;
            dataVaultLog.ActionNote = "PaymentType Deleted : " + Name;

            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _paymentTypeRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentType_Export)]

        public async Task<FileDto> GetPaymentTypeToExcel(GetAllPaymentTypeForExcelInput input)
        {

            var filteredPaymentType = _paymentTypeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredPaymentType
                         select new GePaymentTypeForViewDto()
                         {
                             PaymentType = new PaymentTypeDto
                             {
                                 Name = o.Name,
                                 Id = o.Id,
                                 IsActive = o.IsActive
                             }
                         });


            var ListDtos = await query.ToListAsync();

            return _paymentTypeExcelExporter.ExportToFile(ListDtos);
        }
    }
}
