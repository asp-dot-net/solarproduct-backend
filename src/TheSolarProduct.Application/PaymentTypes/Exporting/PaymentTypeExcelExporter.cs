﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.PaymentStatuses.Dtos;
using TheSolarProduct.PaymentStatuses.Exporting;
using TheSolarProduct.PaymentTypes.Dtos;
using TheSolarProduct.Storage;

namespace TheSolarProduct.PaymentTypes.Exporting
{
    
    public class PaymentTypeExcelExporter : NpoiExcelExporterBase, IPaymentTypeExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PaymentTypeExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GePaymentTypeForViewDto> PaymentType)
        {
            return CreateExcelPackage(
                "PaymentType.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("PaymentType"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("IsActive")
                    );

                    AddObjects(
                        sheet,
                        2,
                        PaymentType,
                        _ => _.PaymentType.Name,
                        _ => _.PaymentType.IsActive.HasValue ? (_.PaymentType.IsActive.Value ? L("Yes") : L("No")) : L("No")
                    );
                }
            );
        }


    }
}
