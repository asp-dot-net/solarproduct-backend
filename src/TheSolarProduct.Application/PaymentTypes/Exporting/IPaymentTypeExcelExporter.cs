﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.PaymentTypes.Dtos;

namespace TheSolarProduct.PaymentTypes.Exporting
{
    public interface IPaymentTypeExcelExporter
    {
        FileDto ExportToFile(List<GePaymentTypeForViewDto> PaymentType);
    }
}
