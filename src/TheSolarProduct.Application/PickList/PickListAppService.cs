﻿//using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Users;
//using TheSolarProduct.Installer;
using TheSolarProduct.Jobs;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Leads;
using TheSolarProduct.PickList.Dtos;
using Microsoft.EntityFrameworkCore;
//using Hangfire.Common;
//using Stripe;
//using Telerik.Reporting;
using TheSolarProduct.JobHistory;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using System.Dynamic;
using TheSolarProduct.ApplicationSettings;
using Abp.Authorization;
using TheSolarProduct.Organizations;
using TheSolarProduct.LeadHistory;
using Telerik.Reporting;
using PayPalCheckoutSdk.Orders;
//using NPOI.SS.Formula.Functions;
//using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace TheSolarProduct.PickList
{
    [AbpAuthorize]
    public class PickListAppService : TheSolarProductAppServiceBase, IPickListAppService
	{
		private readonly IRepository<Job> _jobRepository;
		private readonly IRepository<User, long> _lookup_userRepository;
		private readonly IRepository<ProductItem, int> _lookup_productItemRepository;
		private readonly IRepository<Warehouselocation> _warehouselocationRepository;
		private readonly IRepository<RoofType, int> _roofTypeRepository;
		private readonly IRepository<ProductItem> _productItemRepository;
		private readonly IRepository<LeadActivityLog> _leadactivityRepository;
		private readonly IRepository<User, long> _userRepository;
		private readonly IRepository<Lead> _leadsRepository;
		private readonly IRepository<ProductType> _productTypeRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbContextProvider;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendOrganizationUnitRepository;

        public PickListAppService(IRepository<Warehouselocation> warehouselocationRepository
			, IRepository<Job> jobRepository
			, IRepository<User, long> lookup_userRepository
			, IRepository<ProductItem, int> lookup_productItemRepository
			, IRepository<RoofType, int> roofTypeRepository,
			IRepository<ProductItem> productItemRepository,
			IRepository<LeadActivityLog> leadactivityRepository,
			IRepository<User, long> userRepository,
			IRepository<Lead> leadsRepository,
            IRepository<ProductType> productTypeRepository,
            IDbContextProvider<TheSolarProductDbContext> dbContextProvider
            , IRepository<ExtendOrganizationUnit, long> extendOrganizationUnitRepository
            )
		{
			_jobRepository = jobRepository;
			_lookup_userRepository = lookup_userRepository;
			_lookup_productItemRepository = lookup_productItemRepository;
			_warehouselocationRepository = warehouselocationRepository;
			_roofTypeRepository = roofTypeRepository;
			_productItemRepository = productItemRepository;
			_leadactivityRepository = leadactivityRepository;
			_userRepository = userRepository;
			_leadsRepository = leadsRepository;
            _productTypeRepository = productTypeRepository;
            _dbContextProvider = dbContextProvider;
            _extendOrganizationUnitRepository = extendOrganizationUnitRepository;

        }

		public async Task SavePickList(CreateOrEditPickListDto input)
		{
            var job = _jobRepository.GetAll().Include(e => e.LeadFk)
                .Where(e => e.Id == input.JobId).FirstOrDefault();

            var Installer = _lookup_userRepository.GetAll()
                .Where(e => e.Id == input.picklistinstallerId).FirstOrDefault();

            var PanelCount = input.JobProductItems.Where(x => x.ProductTypeId == 1).Select(x => x.Quantity).Sum().GetValueOrDefault();
            var InverterCount = input.JobProductItems.Where(x => x.ProductTypeId == 2).Select(x => x.Quantity).Sum().GetValueOrDefault();
            
            var RoofTypeName = _roofTypeRepository.GetAll().Where(e => e.Id == job.RoofTypeId).Select(e => e.Name).FirstOrDefault();

            int orgId = _leadsRepository.GetAll().Where(e => e.Id == job.LeadId).Select(e => e.OrganizationId).FirstOrDefault();

            var orgCode = _extendOrganizationUnitRepository.GetAll().Where(e => e.Id == orgId).Select(e => e.Code).FirstOrDefault();

            //int ProjectNo = Convert.ToInt32(job.JobNumber.Substring(job.JobNumber.Length - 6));

            int ProjectNo = Convert.ToInt32(job.JobNumber.Replace(orgCode, string.Empty));

            int compnyId = 0;
            if(AbpSession.TenantId == 6)
            {
                if(orgId == 1 || orgId == 2)
                {
                    compnyId = 4;
                }
                else if(orgId == 7)
                {
                    compnyId = 1;
                }
            }
            else if(AbpSession.TenantId == 11)
            {
                compnyId = 2;
            }
            else
            {
                compnyId = orgId;
            }

            //String SqlconString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=ariseadmin;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
            String SqlconString = ApplicationSettingConsts.QuickStockSqlconString;

            using (SqlConnection sqlCon = new SqlConnection(SqlconString))
            {
                sqlCon.Open();
                SqlCommand sql_cmnd = new SqlCommand("tblPickListLog_InsertTenantJob", sqlCon);
                sql_cmnd.CommandType = CommandType.StoredProcedure;
                sql_cmnd.Parameters.AddWithValue("@ProjectID", SqlDbType.Int).Value = input.JobId;
                sql_cmnd.Parameters.AddWithValue("@Reason", SqlDbType.Text).Value = input.Reason;
                sql_cmnd.Parameters.AddWithValue("@Note", SqlDbType.Text).Value = "";
                sql_cmnd.Parameters.AddWithValue("@PickListDateTime", SqlDbType.DateTime).Value = DateTime.UtcNow;
                sql_cmnd.Parameters.AddWithValue("@InstallBookedDate", SqlDbType.DateTime).Value = job.InstallationDate;
                sql_cmnd.Parameters.AddWithValue("@InstallerID", SqlDbType.Int).Value = input.picklistinstallerId;
                sql_cmnd.Parameters.AddWithValue("@DesignerID", SqlDbType.Int).Value = input.picklistinstallerId;
                sql_cmnd.Parameters.AddWithValue("@ElectricianID", SqlDbType.Int).Value = input.picklistinstallerId;
                sql_cmnd.Parameters.AddWithValue("@CreatedBy", SqlDbType.Int).Value = AbpSession.UserId;
                sql_cmnd.Parameters.AddWithValue("@InstallerName", SqlDbType.VarChar).Value = Installer.Name + " " + Installer.Surname;
                sql_cmnd.Parameters.AddWithValue("@tenantid", SqlDbType.Int).Value = AbpSession.TenantId;
                sql_cmnd.Parameters.AddWithValue("@PicklistType", SqlDbType.Int).Value = 2;
                //sql_cmnd.Parameters.AddWithValue("@Companyid", SqlDbType.Int).Value = orgId == 7 ? 1 : orgId == 8 ? 2 : orgId == 1 ? 4 : orgId == 2 ? 4 : Convert.ToInt32(orgId);
                sql_cmnd.Parameters.AddWithValue("@Companyid", SqlDbType.Int).Value = compnyId;
                sql_cmnd.Parameters.AddWithValue("@jobnumber", SqlDbType.VarChar).Value = job.JobNumber;
                sql_cmnd.Parameters.AddWithValue("@projectNo", SqlDbType.VarChar).Value = ProjectNo;
                sql_cmnd.Parameters.AddWithValue("@PanelInstalled", SqlDbType.Int).Value = PanelCount;
                sql_cmnd.Parameters.AddWithValue("@InverterInstalled", SqlDbType.Int).Value = InverterCount;
                sql_cmnd.Parameters.AddWithValue("@PickListCategory", SqlDbType.Int).Value = input.PicklistType;
                sql_cmnd.Parameters.AddWithValue("@locationId", SqlDbType.Int).Value = input.picklistwarehouseLocation;
                sql_cmnd.Parameters.AddWithValue("@RoofTypeId", SqlDbType.Int).Value = job.RoofTypeId;
                sql_cmnd.Parameters.AddWithValue("@RoofType", SqlDbType.VarChar).Value = RoofTypeName;
                sql_cmnd.Parameters.AddWithValue("@project", SqlDbType.VarChar).Value = job.Address + " - " + job.Suburb;
                sql_cmnd.Parameters.AddWithValue("@stockWith", SqlDbType.VarChar).Value = input.stockWithId;

                var picklistID = (Int32)sql_cmnd.ExecuteScalar();
                sqlCon.Close();

                sqlCon.Open();
                SqlCommand sql_cmnd2 = new SqlCommand("tbl_PickListLog_Update_IsToponeByProjectId", sqlCon);
                sql_cmnd2.CommandType = CommandType.StoredProcedure;
                sql_cmnd2.Parameters.AddWithValue("@ProjectID", SqlDbType.Int).Value = input.JobId;
                sql_cmnd2.Parameters.AddWithValue("@IsTopone", SqlDbType.Int).Value = 0;
                sql_cmnd2.ExecuteNonQuery();
                sqlCon.Close();

                sqlCon.Open();
                SqlCommand sql_cmnd3 = new SqlCommand("tbl_PickListLog_Update_IsToponeByPickListId", sqlCon);
                sql_cmnd3.CommandType = CommandType.StoredProcedure;
                sql_cmnd3.Parameters.AddWithValue("@PickId", SqlDbType.Int).Value = picklistID;
                sql_cmnd3.Parameters.AddWithValue("@IsTopone", SqlDbType.Int).Value = 1;
                sql_cmnd3.ExecuteNonQuery();
                sqlCon.Close();

                foreach (var item in input.JobProductItems)
                {
                    sqlCon.Open();
                    var itemName = _lookup_productItemRepository.GetAll().Where(e => e.Id == item.ProductItemId).FirstOrDefault().Name;
                    var locationName = _warehouselocationRepository.GetAll().Where(e => e.Id == input.picklistwarehouseLocation).FirstOrDefault().location;
                    SqlCommand sql_cmnd4 = new SqlCommand("tbl_PicklistItemDetail_Insert", sqlCon);
                    sql_cmnd4.CommandType = CommandType.StoredProcedure;
                    sql_cmnd4.Parameters.AddWithValue("@PicklistCategoryId", SqlDbType.Int).Value = item.ProductTypeId;
                    sql_cmnd4.Parameters.AddWithValue("@StockItemId", SqlDbType.Int).Value = item.ProductItemId;
                    sql_cmnd4.Parameters.AddWithValue("@PickId", SqlDbType.Int).Value = picklistID;
                    sql_cmnd4.Parameters.AddWithValue("@OrderQuantity", SqlDbType.Int).Value = item.Quantity;
                    sql_cmnd4.Parameters.AddWithValue("@Picklistitem", SqlDbType.VarChar).Value = itemName;
                    sql_cmnd4.Parameters.AddWithValue("@Picklistlocation", SqlDbType.VarChar).Value = locationName;
                    sql_cmnd4.ExecuteNonQuery();
                    sqlCon.Close();
                }
            }

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 31;
            leadactivity.SectionId = input.SectionId;
            leadactivity.ActionNote = "PickList Created";
            leadactivity.LeadId = (int)job.LeadId;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            //await _leadactivityRepository.InsertAsync(leadactivity);

            var leadactivityId = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

            var List = new List<JobTrackerHistory>();

            JobTrackerHistory history = new JobTrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "PickList Type";
            history.PrevValue = "";
            history.CurValue = input.PicklistType == 1 ? "Installation" : (input.PicklistType == 2 ? "Service" : "");
            history.Action = "Picklist Add";
            history.LastmodifiedDateTime = DateTime.UtcNow;
            history.JobIDId = (int)input.JobId;
            history.JobActionId = leadactivityId;
            List.Add(history);

            history = new JobTrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "Installer";
            history.PrevValue = "";
            history.CurValue = _userRepository.GetAll().Where(e => e.Id == input.picklistinstallerId).Select(e => e.FullName).FirstOrDefault();
            history.Action = "Picklist Add";
            history.LastmodifiedDateTime = DateTime.UtcNow;
            history.JobIDId = (int)input.JobId;
            history.JobActionId = leadactivityId;
            List.Add(history);

            history = new JobTrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "Stock With";
            history.PrevValue = "";
            history.CurValue = input.stockWithId == 1 ? "Installer" : (input.stockWithId == 2 ? "Customer" : (input.stockWithId == 3 ? "Transport" : ""));
            history.Action = "Picklist Add";
            history.LastmodifiedDateTime = DateTime.UtcNow;
            history.JobIDId = (int)input.JobId;
            history.JobActionId = leadactivityId;
            List.Add(history);
            
            history = new JobTrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "Warehouse Location";
            history.PrevValue = "";
            history.CurValue = _warehouselocationRepository.GetAll().Where(e => e.Id == input.picklistwarehouseLocation).FirstOrDefault().location;
            history.Action = "Picklist Add";
            history.LastmodifiedDateTime = DateTime.UtcNow;
            history.JobIDId = (int)input.JobId;
            history.JobActionId = leadactivityId;
            List.Add(history);

            history = new JobTrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "Reason";
            history.PrevValue = "";
            history.CurValue = input.Reason;
            history.Action = "Picklist Add";
            history.LastmodifiedDateTime = DateTime.UtcNow;
            history.JobIDId = (int)input.JobId;
            history.JobActionId = leadactivityId;
            List.Add(history);

            foreach (var item in input.JobProductItems)
            {
                var ProItem = _lookup_productItemRepository.GetAll().Where(e => e.Id == item.ProductItemId).Select(e => new {type = e.ProductTypeFk.Name, e.Name}).FirstOrDefault();


                history = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "Item with Product Type :" + ProItem.type;
                history.PrevValue = "";
                history.CurValue = ProItem.Name + "with Qty " + item.Quantity;
                history.Action = "Picklist Add";
                history.LastmodifiedDateTime = DateTime.UtcNow;
                history.JobIDId = (int)input.JobId;
                history.JobActionId = leadactivityId;
                List.Add(history);

                history = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "Qty";
                history.PrevValue = "";
                history.CurValue = item.Quantity.ToString();
                history.Action = "Picklist Add";
                history.LastmodifiedDateTime = DateTime.UtcNow;
                history.JobIDId = (int)input.JobId;
                history.JobActionId = leadactivityId;
                List.Add(history);

                history = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "Stock On Hand";
                history.PrevValue = "";
                history.CurValue = item.Stock.ToString();
                history.Action = "Picklist Add";
                history.LastmodifiedDateTime = DateTime.UtcNow;
                history.JobIDId = (int)input.JobId;
                history.JobActionId = leadactivityId;
                List.Add(history);
            }

            await _dbContextProvider.GetDbContext().JobTrackerHistorys.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

        }

		public async Task DeletePickList(int PicklistID, int LeadId,int?  sectionId)
		{
            //String SqlconString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=ariseadmin;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
            String SqlconString = ApplicationSettingConsts.QuickStockSqlconString;
            using (SqlConnection sqlCon = new SqlConnection(SqlconString))
            {
                sqlCon.Open();
                SqlCommand sql_cmnd2 = new SqlCommand("tbl_PickListLog_DeleteByID", sqlCon);
                sql_cmnd2.CommandType = CommandType.StoredProcedure;
                sql_cmnd2.Parameters.AddWithValue("@ID", SqlDbType.Int).Value = PicklistID;
                sql_cmnd2.ExecuteNonQuery();
                sqlCon.Close();

                sqlCon.Open();
                SqlCommand sql_cmnd3 = new SqlCommand("tbl_PicklistItemsDetail_Delete", sqlCon);
                sql_cmnd3.CommandType = CommandType.StoredProcedure;
                sql_cmnd3.Parameters.AddWithValue("@PickListItemId", SqlDbType.Int).Value = PicklistID;
                sql_cmnd3.ExecuteNonQuery();
                sqlCon.Close();
            }

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 34;
            leadactivity.SectionId = sectionId;
            leadactivity.ActionNote = "PickList Deleted";
            leadactivity.LeadId = LeadId;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

		public async Task<List<GetPicklistItemInput>> GetPickList(int JobId)
		{
            int orgId = await _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.Id == JobId).Select(e => e.LeadFk.OrganizationId).FirstOrDefaultAsync();

            int compnyId = 0;
            if (AbpSession.TenantId == 6)
            {
                if (orgId == 1 || orgId == 2)
                {
                    compnyId = 4;
                }
                else if (orgId == 7)
                {
                    compnyId = 1;
                }
            }
            else if (AbpSession.TenantId == 11)
            {
                compnyId = 2;
            }
            else
            {
                compnyId = orgId;
            }

            var timeZone = TimeZoneInfo.FindSystemTimeZoneById("E. Australia Standard Time");

            //String SqlconString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=ariseadmin;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
            String SqlconString = ApplicationSettingConsts.QuickStockSqlconString;
            using (SqlConnection sqlCon = new SqlConnection(SqlconString))
            {
                sqlCon.Open();
                SqlCommand sql_cmnd2 = new SqlCommand("tbl_PickListLog_SelectByPId_CompnayID", sqlCon);
                sql_cmnd2.CommandType = CommandType.StoredProcedure;
                sql_cmnd2.Parameters.AddWithValue("@ProjectID", SqlDbType.Int).Value = JobId;
                sql_cmnd2.Parameters.AddWithValue("@CompanyID", SqlDbType.Int).Value = compnyId;
                //sql_cmnd2.Parameters.AddWithValue("@CompanyID", SqlDbType.Int).Value = orgId == 7 ? 1 : orgId == 8 ? 2 : 4;
                //sql_cmnd2.Parameters.AddWithValue("@TenantId", SqlDbType.Int).Value = AbpSession.TenantId;
                var tablePicklist = new DataTable();
                tablePicklist.Load(sql_cmnd2.ExecuteReader());
                var query = from pickItem in tablePicklist.AsEnumerable()
                            select new GetPicklistItemInput
                            {
                                ID = pickItem.Field<int>("ID"),
                                PickListDateTime = pickItem.Field<DateTime>("PickListDateTime"),
                                InstallBookedDate = pickItem.Field<DateTime?>("InstallBookedDate"),
                                DeductOn = pickItem.Field<DateTime?>("DeductOn") != null ? TimeZoneInfo.ConvertTimeToUtc(pickItem.Field<DateTime>("DeductOn"), timeZone) : pickItem.Field<DateTime?>("DeductOn"),
                                InstallerName = pickItem.Field<string>("InstallerName"),
                                ScanCount = pickItem.Field<int>("scaniteamcount"),
                                SystemDetail = pickItem.Field<string>("SystemDetail"),
                                CreatedBy = orgId == 8 && pickItem.Field<DateTime>("PickListDateTime") < Convert.ToDateTime("17/09/2022") ? pickItem.Field<string?>("CreatedBy1") :
                                         orgId == 7 && pickItem.Field<DateTime>("PickListDateTime") < Convert.ToDateTime("04/01/2023") ? pickItem.Field<string?>("CreatedBy1") : _lookup_userRepository.GetAll().Where(e => e.Id == pickItem.Field<int?>("CreatedBy")).FirstOrDefault().Name,
                                Reason = pickItem.Field<string>("Reason")
                            };
                sqlCon.Close();
                return query.ToList();
            }
        }

		public async Task<List<GetPicklistItemForReport>> GetPickListForReport(int Id)
		{
            //String SqlconString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=ariseadmin;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
            String SqlconString = ApplicationSettingConsts.QuickStockSqlconString;
            using (SqlConnection sqlCon = new SqlConnection(SqlconString))
            {
                sqlCon.Open();
                SqlCommand sql_cmnd2 = new SqlCommand("tbl_PicklistItemDetail_GetByPicklistID", sqlCon);
                sql_cmnd2.CommandType = CommandType.StoredProcedure;
                sql_cmnd2.Parameters.AddWithValue("@PicklistId", SqlDbType.Int).Value = Id;
                var tablePicklist = new DataTable();
                tablePicklist.Load(sql_cmnd2.ExecuteReader());
                var query = from pickItem in tablePicklist.AsEnumerable()
                            select new GetPicklistItemForReport
                            {
                                ProductItemId = pickItem.Field<int>("StockItemID"),
                                Description = pickItem.Field<string>("StockDescription"),
                                Item = pickItem.Field<string>("StockItem"),
                                Model = pickItem.Field<string>("StockModel"),
                                Qty = pickItem.Field<int>("OrderQuantity"),
                                PickListId = pickItem.Field<int>("PicklistitemId"),
                                ScanIteamCount = pickItem.Field<int>("scaniteamcount"),
                                ProductTypeId = _productItemRepository.GetAll().Where(e => e.Id == pickItem.Field<int>("StockItemID")).Select(e => e.ProductTypeId).FirstOrDefault(),
                                picklistinstallerId = pickItem.Field<int>("InstallerID"),
                                picklistwarehouseLocation = pickItem.Field<int>("LocationId"),
                                stockWith = pickItem.Field<int>("stockWith"),
                                PickID = pickItem.Field<int>("PickId"),
                            };
                sqlCon.Close();
                return query.ToList();
            }

        }

		public async Task Update(EditPickListDto input)
		{
            var Installer = _lookup_userRepository.GetAll()
                .Where(e => e.Id == input.picklistinstallerId).FirstOrDefault();

            var jobs = _jobRepository.GetAll().Where(e => e.Id == input.JobId).FirstOrDefault();
            if (jobs != null)
            {
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 33;
                leadactivity.SectionId = input.SectionId;
                leadactivity.ActionNote = "PickList Modified";
                leadactivity.LeadId = (int)jobs.LeadId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                var jobactionid = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);
                var list = new List<JobTrackerHistory>();

                //String SqlconString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=ariseadmin;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
                String SqlconString = ApplicationSettingConsts.QuickStockSqlconString;
                using (SqlConnection sqlCon = new SqlConnection(SqlconString))
                {
                    #region Log History
                    if (input.EditedPickList != null)
                    {
                        var ProductType = _productTypeRepository.GetAll();

                        var IdList = input.EditedPickList.Select(e => e.PickListId).ToList();
                        var existingData = GetPickListItemDetailsListByPickId(input.pickID);

                        if (existingData != null)
                        {
                            foreach (var item in existingData)
                            {
                                if (IdList != null)
                                {
                                    bool containsItem = IdList.Any(x => x == item.PicklistItemId);
                                    if (containsItem == false)
                                    {
                                        JobTrackerHistory jobhistory1 = new JobTrackerHistory();
                                        if (AbpSession.TenantId != null)
                                        {
                                            jobhistory1.TenantId = (int)AbpSession.TenantId;
                                        }
                                        jobhistory1.FieldName = "Product Type";
                                        jobhistory1.PrevValue = ProductType.Where(x => x.Id == item.CategoryId).Select(x => x.Name).FirstOrDefault();
                                        jobhistory1.CurValue = "Deleted";
                                        jobhistory1.Action = "PickList Item Delete";
                                        jobhistory1.LastmodifiedDateTime = DateTime.UtcNow;
                                        jobhistory1.JobIDId = (int)input.JobId;
                                        jobhistory1.JobActionId = jobactionid;
                                        list.Add(jobhistory1);

                                        JobTrackerHistory jobhistory2 = new JobTrackerHistory();
                                        if (AbpSession.TenantId != null)
                                        {
                                            jobhistory2.TenantId = (int)AbpSession.TenantId;
                                        }
                                        jobhistory2.FieldName = "Product Item";
                                        jobhistory2.PrevValue = item.StockItem;
                                        jobhistory2.CurValue = "Deleted";
                                        jobhistory2.Action = "PickList Item Delete";
                                        jobhistory2.LastmodifiedDateTime = DateTime.UtcNow;
                                        jobhistory2.JobIDId = (int)input.JobId;
                                        jobhistory2.JobActionId = jobactionid;
                                        list.Add(jobhistory2);

                                        JobTrackerHistory jobhistory3 = new JobTrackerHistory();
                                        if (AbpSession.TenantId != null)
                                        {
                                            jobhistory3.TenantId = (int)AbpSession.TenantId;
                                        }
                                        jobhistory3.FieldName = "Quantity";
                                        jobhistory3.PrevValue = item.OrderQuantity.ToString();
                                        jobhistory3.CurValue = "Deleted";
                                        jobhistory3.Action = "PickList Item Delete";
                                        jobhistory3.LastmodifiedDateTime = DateTime.Now;
                                        jobhistory3.JobIDId = (int)input.JobId;
                                        jobhistory3.JobActionId = jobactionid;
                                        list.Add(jobhistory3);

                                        //_jobProductItemRepository.Delete(x => x.Id == item.Id);
                                        sqlCon.Open();
                                        SqlCommand sql_cmnd2 = new SqlCommand("tbl_PicklistItemDetail_DeleteById", sqlCon);
                                        sql_cmnd2.CommandType = CommandType.StoredProcedure;
                                        sql_cmnd2.Parameters.AddWithValue("@Id", SqlDbType.Int).Value = item.PicklistItemId;
                                        sql_cmnd2.ExecuteNonQuery();
                                        sqlCon.Close();
                                    }
                                }
                            }
                        }

                        foreach (var picklistItem in input.EditedPickList)
                        {
                            if (picklistItem.PickListId != 0)
                            {
                                var existData = existingData.Where(x => x.PicklistItemId == picklistItem.PickListId).FirstOrDefault();

                                sqlCon.Open();
                                SqlCommand sql_cmnd = new SqlCommand("tbl_PicklistItemDetail_UpdateById", sqlCon);
                                sql_cmnd.CommandType = CommandType.StoredProcedure;
                                sql_cmnd.Parameters.AddWithValue("@Id", SqlDbType.Int).Value = picklistItem.PickListId;
                                sql_cmnd.Parameters.AddWithValue("@StockItemId", SqlDbType.Int).Value = picklistItem.ProductItemId;
                                sql_cmnd.Parameters.AddWithValue("@OrderQuantity", SqlDbType.Int).Value = picklistItem.Qty;
                                sql_cmnd.Parameters.AddWithValue("@PicklistItem", SqlDbType.VarChar).Value = picklistItem.Item;
                                sql_cmnd.Parameters.AddWithValue("@Picklistlocation", SqlDbType.Int).Value = input.picklistwarehouseLocation;
                                sql_cmnd.Parameters.AddWithValue("@PicklistCategoryId ", SqlDbType.Int).Value = picklistItem.ProductTypeId;
                                sql_cmnd.ExecuteNonQuery();
                                sqlCon.Close();

                                if (picklistItem.ProductItemId != null && picklistItem.ProductItemId != 0 && existData.StockItemId != picklistItem.ProductItemId)
                                {
                                    if (existData.StockItemId != picklistItem.ProductItemId)
                                    {
                                        JobTrackerHistory jobhistory = new JobTrackerHistory();
                                        if (AbpSession.TenantId != null)
                                        {
                                            jobhistory.TenantId = (int)AbpSession.TenantId;
                                        }
                                        jobhistory.FieldName = "Product Name";
                                        jobhistory.PrevValue = existData.StockItem;
                                        jobhistory.CurValue = picklistItem.Item;
                                        jobhistory.Action = "Picklist Item Edit";
                                        jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                                        jobhistory.JobIDId = (int)input.JobId;
                                        jobhistory.JobActionId = jobactionid;
                                        list.Add(jobhistory);
                                    }
                                }

                                if (existData.OrderQuantity != picklistItem.Qty)
                                {
                                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory.FieldName = "Quantity";
                                    jobhistory.PrevValue = existData.OrderQuantity.ToString();
                                    jobhistory.CurValue = picklistItem.Qty.ToString();
                                    jobhistory.Action = "Picklist Item Edit";
                                    jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                                    jobhistory.JobIDId = (int)input.JobId;
                                    jobhistory.JobActionId = jobactionid;
                                    list.Add(jobhistory);
                                }

                                var cate = existData.CategoryId == 5 ? 6 : existData.CategoryId == 6 ? 5 : existData.CategoryId;
                                if (cate != picklistItem.ProductTypeId)
                                {
                                    var pType = _productTypeRepository.GetAll();
                                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory.FieldName = "Product Type";
                                    jobhistory.PrevValue = ProductType.Where(x => x.Id == existData.CategoryId).Select(x => x.Name).FirstOrDefault();
                                    jobhistory.CurValue = ProductType.Where(x => x.Id == picklistItem.ProductTypeId).Select(x => x.Name).FirstOrDefault();
                                    jobhistory.Action = "Picklist Item Edit";
                                    jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                                    jobhistory.JobIDId = (int)input.JobId;
                                    jobhistory.JobActionId = jobactionid;
                                    list.Add(jobhistory);
                                }

                                JobTrackerHistory jobhistory1 = new JobTrackerHistory();
                                if (AbpSession.TenantId != null)
                                {
                                    jobhistory1.TenantId = (int)AbpSession.TenantId;
                                }
                                jobhistory1.FieldName = "Stock On Hand";
                                jobhistory1.PrevValue = "";
                                jobhistory1.CurValue = picklistItem.Stock.ToString();
                                jobhistory1.Action = "Picklist Item Edit";
                                jobhistory1.LastmodifiedDateTime = DateTime.UtcNow;
                                jobhistory1.JobIDId = (int)input.JobId;
                                jobhistory1.JobActionId = jobactionid;
                                list.Add(jobhistory1);
                            }
                            else
                            {
                                sqlCon.Open();
                                var itemName = _lookup_productItemRepository.GetAll().Where(e => e.Id == picklistItem.ProductItemId).FirstOrDefault().Name;
                                var locationName = _warehouselocationRepository.GetAll().Where(e => e.Id == input.picklistwarehouseLocation).FirstOrDefault().location;
                                SqlCommand sql_cmnd2 = new SqlCommand("tbl_PicklistItemDetail_Insert", sqlCon);
                                sql_cmnd2.CommandType = CommandType.StoredProcedure;
                                sql_cmnd2.Parameters.AddWithValue("@PicklistCategoryId", SqlDbType.Int).Value = picklistItem.ProductTypeId;
                                sql_cmnd2.Parameters.AddWithValue("@StockItemId", SqlDbType.Int).Value = picklistItem.ProductItemId;
                                sql_cmnd2.Parameters.AddWithValue("@PickId", SqlDbType.Int).Value = input.pickID;
                                sql_cmnd2.Parameters.AddWithValue("@OrderQuantity", SqlDbType.Int).Value = picklistItem.Qty;
                                sql_cmnd2.Parameters.AddWithValue("@Picklistitem", SqlDbType.VarChar).Value = itemName;
                                sql_cmnd2.Parameters.AddWithValue("@Picklistlocation", SqlDbType.VarChar).Value = locationName;
                                sql_cmnd2.ExecuteNonQuery();
                                sqlCon.Close();

                                //JobTrackerHistory jobhistory = new JobTrackerHistory();
                                //if (AbpSession.TenantId != null)
                                //{
                                //    jobhistory.TenantId = (int)AbpSession.TenantId;
                                //}
                                //jobhistory.FieldName = "Product Added";
                                //jobhistory.PrevValue = "Added";
                                //jobhistory.CurValue = picklistItem.Item;
                                //jobhistory.Action = "Picklist Item Edit";
                                //jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                                //jobhistory.JobIDId = (int)input.JobId;
                                //jobhistory.JobActionId = jobactionid;
                                //list.Add(jobhistory);

                                
                                    var ProItem = _lookup_productItemRepository.GetAll().Where(e => e.Id == picklistItem.ProductItemId).Select(e => new { type = e.ProductTypeFk.Name, e.Name }).FirstOrDefault();


                                    JobTrackerHistory history = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        history.TenantId = (int)AbpSession.TenantId;
                                    }
                                    history.FieldName = "Item with Product Type :" + ProItem.type;
                                    history.PrevValue = "";
                                    history.CurValue = ProItem.Name ;
                                    history.Action = "Picklist Add";
                                    history.LastmodifiedDateTime = DateTime.UtcNow;
                                    history.JobIDId = (int)input.JobId;
                                    history.JobActionId = jobactionid;
                                    list.Add(history);

                                    history = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        history.TenantId = (int)AbpSession.TenantId;
                                    }
                                    history.FieldName = "Qty";
                                    history.PrevValue = "";
                                    history.CurValue = picklistItem.Qty.ToString();
                                    history.Action = "Picklist Add";
                                    history.LastmodifiedDateTime = DateTime.UtcNow;
                                    history.JobIDId = (int)input.JobId;
                                    history.JobActionId = jobactionid;
                                    list.Add(history);

                                    history = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        history.TenantId = (int)AbpSession.TenantId;
                                    }
                                    history.FieldName = "Stock On Hand";
                                    history.PrevValue = "";
                                    history.CurValue = picklistItem.Stock.ToString();
                                    history.Action = "Picklist Add";
                                    history.LastmodifiedDateTime = DateTime.UtcNow;
                                    history.JobIDId = (int)input.JobId;
                                    history.JobActionId = jobactionid;
                                    list.Add(history);
                                

                            }
                        }


                    }
                    #endregion

                    var PickList = await GetPickListById(input.pickID);
                    if (input.picklistinstallerId != PickList.InstallerId)
                    {
                        JobTrackerHistory jobhistory = new JobTrackerHistory();
                        if (AbpSession.TenantId != null)
                        {
                            jobhistory.TenantId = (int)AbpSession.TenantId;
                        }
                        jobhistory.FieldName = "Insatller";
                        jobhistory.PrevValue = PickList.InstallerName;
                        jobhistory.CurValue = Installer.Name + " " + Installer.Surname;
                        jobhistory.Action = "Picklist Edit";
                        jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                        jobhistory.JobIDId = (int)input.JobId;
                        jobhistory.JobActionId = jobactionid;
                        list.Add(jobhistory);
                    }
                    if (input.picklistwarehouseLocation != PickList.LocationId)
                    {
                        JobTrackerHistory jobhistory = new JobTrackerHistory();
                        if (AbpSession.TenantId != null)
                        {
                            jobhistory.TenantId = (int)AbpSession.TenantId;
                        }
                        jobhistory.FieldName = "Warehouse Location";
                        jobhistory.PrevValue = _warehouselocationRepository.GetAll().Where(e => e.Id == PickList.LocationId).FirstOrDefault().location;
                        jobhistory.CurValue = _warehouselocationRepository.GetAll().Where(e => e.Id == input.picklistwarehouseLocation).FirstOrDefault().location;
                        jobhistory.Action = "Picklist Edit";
                        jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                        jobhistory.JobIDId = (int)input.JobId;
                        jobhistory.JobActionId = jobactionid;
                        list.Add(jobhistory);
                    }
                    if (input.stockWithId != PickList.StockWith)
                    {
                        var pType = _productTypeRepository.GetAll();
                        JobTrackerHistory jobhistory = new JobTrackerHistory();
                        if (AbpSession.TenantId != null)
                        {
                            jobhistory.TenantId = (int)AbpSession.TenantId;
                        }
                        jobhistory.FieldName = "Stock With";
                        jobhistory.PrevValue = PickList.StockWith == 1 ? "With Installer" : PickList.StockWith == 2 ? "With Customer" : PickList.StockWith == 3 ? "With Transport" : "";
                        jobhistory.CurValue = input.stockWithId == 1 ? "With Installer" : input.stockWithId == 2 ? "With Customer" : input.stockWithId == 3 ? "With Transport" : "";
                        jobhistory.Action = "Picklist Item Edit";
                        jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                        jobhistory.JobIDId = (int)input.JobId;
                        jobhistory.JobActionId = jobactionid;
                        list.Add(jobhistory);
                    }

                    sqlCon.Open();
                    SqlCommand sql_cmnd4 = new SqlCommand("tbl_Picklist_UpdateById", sqlCon);
                    sql_cmnd4.CommandType = CommandType.StoredProcedure;
                    sql_cmnd4.Parameters.AddWithValue("@Id", SqlDbType.Int).Value = input.pickID;
                    sql_cmnd4.Parameters.AddWithValue("@InstallerId", SqlDbType.Int).Value = input.picklistinstallerId;
                    sql_cmnd4.Parameters.AddWithValue("@DesignerID", SqlDbType.Int).Value = input.picklistinstallerId;
                    sql_cmnd4.Parameters.AddWithValue("@ElectricianID", SqlDbType.Int).Value = input.picklistinstallerId;
                    sql_cmnd4.Parameters.AddWithValue("@InstallerName", SqlDbType.VarChar).Value = Installer.Name + " " + Installer.Surname;
                    sql_cmnd4.Parameters.AddWithValue("@WarehouseId", SqlDbType.Int).Value = input.picklistwarehouseLocation;
                    sql_cmnd4.Parameters.AddWithValue("@StockWith", SqlDbType.Int).Value = input.stockWithId;
                    sql_cmnd4.ExecuteNonQuery();
                    sqlCon.Close();
                }

                if (list.Count > 0)
                {
                    await _dbContextProvider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
                    await _dbContextProvider.GetDbContext().SaveChangesAsync();
                }
            }
        }

		public async Task<PicklistDto> GetPickListById(int Id)
        {
            //String SqlconString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=ariseadmin;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
            String SqlconString = ApplicationSettingConsts.QuickStockSqlconString;
            using (SqlConnection sqlCon = new SqlConnection(SqlconString))
            {
                sqlCon.Open();
                SqlCommand sql_cmnd2 = new SqlCommand("tbl_Picklistlog_GetById", sqlCon);
                sql_cmnd2.CommandType = CommandType.StoredProcedure;
                sql_cmnd2.Parameters.AddWithValue("@PicklistId", SqlDbType.Int).Value = Id;
                var tablePicklist = new DataTable();
                tablePicklist.Load(sql_cmnd2.ExecuteReader());
                var query = (from pickItem in tablePicklist.AsEnumerable()
                             select new PicklistDto
                             {
                                 PicklistId = pickItem.Field<int>("ID"),
                                 JobId = pickItem.Field<int>("ProjectID"),
                                 InstallBookedDate = pickItem.Field<DateTime>("InstallBookedDate"),
                                 InstallerId = pickItem.Field<int>("InstallerID"),
                                 InstallerName = pickItem.Field<string>("InstallerName"),
                                 CreationTime = pickItem.Field<DateTime>("PickListDateTime"),
                                 CreatedBy = pickItem.Field<int>("CreatedBy"),
                                 DeductBy = pickItem.Field<string>("DeductBy"),
                                 DeductOn = pickItem.Field<DateTime?>("DeductOn"),
                                 LocationId = pickItem.Field<int>("LocationId"),
                                 Reason = pickItem.Field<string>("Reason"),
                                 StockWith = pickItem.Field<int?>("stockWith")
                             });

                sqlCon.Close();

                return query.FirstOrDefault();
            }
        }

		public IQueryable<PicklistItemDto> GetPickListItemDetailsByPickId(int PickId)
		{
            //String SqlconString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=ariseadmin;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
            String SqlconString = ApplicationSettingConsts.QuickStockSqlconString;
            using (SqlConnection sqlCon = new SqlConnection(SqlconString))
            {
                sqlCon.Open();
                SqlCommand sql_cmnd2 = new SqlCommand("tbl_PicklistItemDetail_GetByPicId", sqlCon);
                sql_cmnd2.CommandType = CommandType.StoredProcedure;
                sql_cmnd2.Parameters.AddWithValue("@PicklistId", SqlDbType.Int).Value = PickId;
                var tablePicklist = new DataTable();
                tablePicklist.Load(sql_cmnd2.ExecuteReader());
                var query = (from pickItem in tablePicklist.AsEnumerable()
                             select new PicklistItemDto
                             {
                                 PicklistItemId = pickItem.Field<int>("PicklistitemId"),
                                 PicklistId = pickItem.Field<int>("PickId"),
                                 StockItemId = pickItem.Field<int>("StockItemId"),
                                 StockItem = pickItem.Field<string>("PicklistItem"),
                                 OrderQuantity = pickItem.Field<int>("OrderQuantity"),
                                 CategoryId = pickItem.Field<int>("PicklistCategoryId")
                             });

                IQueryable<PicklistItemDto> itemDtos = query.AsQueryable();

                sqlCon.Close();

                //return new List<PicklistItemDto>(await itemDtos.ToListAsync());
                return itemDtos;
            }

        }
	
        protected List<PicklistItemDto> GetPickListItemDetailsListByPickId(int PickId)
        {
            //String SqlconString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=ariseadmin;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
            String SqlconString = ApplicationSettingConsts.QuickStockSqlconString;
            using (SqlConnection sqlCon = new SqlConnection(SqlconString))
            {
                sqlCon.Open();
                SqlCommand sql_cmnd2 = new SqlCommand("tbl_PicklistItemDetail_GetByPicId", sqlCon);
                sql_cmnd2.CommandType = CommandType.StoredProcedure;
                sql_cmnd2.Parameters.AddWithValue("@PicklistId", SqlDbType.Int).Value = PickId;
                var tablePicklist = new DataTable();
                tablePicklist.Load(sql_cmnd2.ExecuteReader());
                var query = (from pickItem in tablePicklist.AsEnumerable()
                             select new PicklistItemDto
                             {
                                 PicklistItemId = pickItem.Field<int>("PicklistitemId"),
                                 PicklistId = pickItem.Field<int>("PickId"),
                                 StockItemId = pickItem.Field<int>("StockItemId"),
                                 StockItem = pickItem.Field<string>("PicklistItem"),
                                 OrderQuantity = pickItem.Field<int>("OrderQuantity"),
                                 CategoryId = pickItem.Field<int>("PicklistCategoryId")
                             });

                sqlCon.Close();

                return query.ToList();
            }
        }

        public async Task<bool> CheckInstallationPicklistExists(int JobId)
        {
            int orgId = await _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.Id == JobId).Select(e => e.LeadFk.OrganizationId).FirstOrDefaultAsync();

            //String SqlconString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=ariseadmin;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
            String SqlconString = ApplicationSettingConsts.QuickStockSqlconString;
            using (SqlConnection sqlCon = new SqlConnection(SqlconString))
            {
                sqlCon.Open();
                SqlCommand sql_cmnd2 = new SqlCommand("CheckInstallationPicklistExists", sqlCon);
                sql_cmnd2.CommandType = CommandType.StoredProcedure;
                sql_cmnd2.Parameters.AddWithValue("@JobId", SqlDbType.Int).Value = JobId;
                sql_cmnd2.Parameters.AddWithValue("@CompanyID", SqlDbType.Int).Value = orgId == 7 ? 1 : orgId == 8 ? 2 : 4;

                var sqlResult = sql_cmnd2.ExecuteScalar();

                sqlCon.Close();

                var returnVal = Convert.ToBoolean(sqlResult);

                return returnVal;
            }
        }

        public async Task<bool> CheckSecondPicklistItems(int JobId)
        {
            int panelCount = 0;
            int inverterCount = 0;

            int orgId = await _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.Id == JobId).Select(e => e.LeadFk.OrganizationId).FirstOrDefaultAsync();

            int compnyId = 0;
            if (AbpSession.TenantId == 6)
            {
                if (orgId == 1 || orgId == 2)
                {
                    compnyId = 4;
                }
                else if (orgId == 7)
                {
                    compnyId = 1;
                }
            }
            else if (AbpSession.TenantId == 11)
            {
                compnyId = 2;
            }
            else
            {
                compnyId = orgId;
            }

           var DyPicklistList = new List<dynamic>();
            //dynamic DyPicklistList = new ExpandoObject();

            //String SqlconString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=ariseadmin;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
            String SqlconString = ApplicationSettingConsts.QuickStockSqlconString;
            using (SqlConnection sqlCon = new SqlConnection(SqlconString))
            {
                sqlCon.Open();
                SqlCommand sql_cmnd2 = new SqlCommand("tbl_PickListLog_CheckSecondPicklistItems", sqlCon);
                sql_cmnd2.CommandType = CommandType.StoredProcedure;
                sql_cmnd2.Parameters.AddWithValue("@ProjectID", SqlDbType.Int).Value = JobId;
                sql_cmnd2.Parameters.AddWithValue("@CompanyID", SqlDbType.Int).Value = compnyId;
                var tablePicklist = new DataTable();
                tablePicklist.Load(sql_cmnd2.ExecuteReader());
                var query = from pickItem in tablePicklist.AsEnumerable()
                            select new List<dynamic>
                            {
                                pickItem.Field<int>("ID"),  
                                pickItem.Field<int>("PicklistCategoryId")
                            }.ToList();

                sqlCon.Close();

                if(query != null)
                {
                    foreach (var item in query)
                    {
                        if (item[1] == 1)
                        {
                            panelCount++;
                        }
                        if (item[1] == 2)
                        {
                            inverterCount++;
                        }
                    }
                }
            }

            if(panelCount > 0 && inverterCount > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}