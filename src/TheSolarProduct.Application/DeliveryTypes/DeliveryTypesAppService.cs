﻿using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.DeliveryTypes;
using TheSolarProduct.CheckApplications.Exporting;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.DeliveryTypes.Exporting;
using System.Threading.Tasks;
using TheSolarProduct.DeliveryTypes.Dtos;
using Abp.Application.Services.Dto;
using TheSolarProduct.Dto;
using Abp.Linq.Extensions;
using System.Linq;
using System.Linq.Dynamic.Core;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using TheSolarProduct.CheckApplications.Dtos;
using TheSolarProduct.CheckApplications;


namespace TheSolarProduct.DeliveryTypes
{
    public class DeliveryTypesAppService : TheSolarProductAppServiceBase, IDeliveryTypesAppService 
    {
        private readonly IDeliveryTypesExcelExporter _deliverytypeExcelExporter ;

        private readonly IRepository<DeliveryType> _deliverytypesRepository;
       
      
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public DeliveryTypesAppService(IRepository<DeliveryType> deliverytypesRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider, IDeliveryTypesExcelExporter deliverytypeExcelExporter)
        {

            _deliverytypeExcelExporter = deliverytypeExcelExporter;
            _deliverytypesRepository = deliverytypesRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;

        }

        public async Task<PagedResultDto<GetDeliveryTypesForViewDto>> GetAll(GetAllDeliveryTypesInput input)
        {
            var deliverytype = _deliverytypesRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFiltered = deliverytype
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFiltered
                         select new GetDeliveryTypesForViewDto()
                         {
                             DeliveryTypes = new DeliveryTypesDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 IsActive = o.IsActive
                             }
                         };

            var totalCount = await deliverytype.CountAsync();

            return new PagedResultDto<GetDeliveryTypesForViewDto>(totalCount, await output.ToListAsync());
        }

       // [AbpAuthorize(AppPermissions.Pages_CheckApplication_Edit)]
        public async Task<GetDeliveryTypesForEditOutput> GetDeliveryTypesForEdit(EntityDto input)
        {
            var deliverytype = await _deliverytypesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetDeliveryTypesForEditOutput { DeliveryTypes = ObjectMapper.Map<CreateOrEditDeliveryTypesDto>(deliverytype) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditDeliveryTypesDto input)
        {


            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

       // [AbpAuthorize(AppPermissions.Pages_CheckApplication_Create)]

        protected virtual async Task Create(CreateOrEditDeliveryTypesDto input)
        {
            var deliverytype = ObjectMapper.Map<DeliveryType>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 62;
            dataVaultLog.ActionNote = "DeliveryType Created : " + input.Name;
            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _deliverytypesRepository.InsertAsync(deliverytype);
        }


       // [AbpAuthorize(AppPermissions.Pages_CheckApplication_Edit)]
        protected virtual async Task Update(CreateOrEditDeliveryTypesDto input)
        {
            var deliverytype = await _deliverytypesRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 62;
            dataVaultLog.ActionNote = "DeliveryType Updated : " + deliverytype.Name;

            dataVaultLog.IsInventory = true;
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != deliverytype.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = deliverytype.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != deliverytype.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = deliverytype.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, deliverytype);

            await _deliverytypesRepository.UpdateAsync(deliverytype);


        }

       // [AbpAuthorize(AppPermissions.Pages_CheckApplication_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _deliverytypesRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 62;
            dataVaultLog.ActionNote = "DeliveryTpes Deleted : " + Name;

            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _deliverytypesRepository.DeleteAsync(input.Id);
        }

        //[AbpAuthorize(AppPermissions.Pages_CheckApplication_Export)]

        public async Task<FileDto> GetDeliveryTypesToExcel(GetAllDeliveryTypesForExcelInput input)
        {

            var filteredDeliverytypes = _deliverytypesRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredDeliverytypes
                         select new GetDeliveryTypesForViewDto()
                         {
                             DeliveryTypes = new DeliveryTypesDto
                             {
                                 Name = o.Name,
                                 Id = o.Id,
                                 IsActive = o.IsActive
                             }
                         });


            var DeliveryTypesListDtos = await query.ToListAsync();

            return _deliverytypeExcelExporter.ExportToFile(DeliveryTypesListDtos);
            
        }







    }
}
