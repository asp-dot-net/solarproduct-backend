﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.DeliveryTypes.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.TransportCompanies.Dto;
using TheSolarProduct.TransportCompanies.Exporting;

namespace TheSolarProduct.DeliveryTypes.Exporting
{
    public class DeliveryTypesExcelExporter : NpoiExcelExporterBase, IDeliveryTypesExcelExporter
    {


        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public DeliveryTypesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetDeliveryTypesForViewDto> deliverytype)
        {
            return CreateExcelPackage(
                "DeliveryTypes.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet("DeliveryTypes");

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, 2, deliverytype,
                        _ => _.DeliveryTypes.Name
                        );



                });
        }


    }






}

