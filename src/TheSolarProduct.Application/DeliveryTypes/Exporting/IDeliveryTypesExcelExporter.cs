﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DeliveryTypes.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.TransportCompanies.Dto;

namespace TheSolarProduct.DeliveryTypes.Exporting
{
    public interface IDeliveryTypesExcelExporter
    {
        FileDto ExportToFile(List<GetDeliveryTypesForViewDto> deliverytype);
    }
}
