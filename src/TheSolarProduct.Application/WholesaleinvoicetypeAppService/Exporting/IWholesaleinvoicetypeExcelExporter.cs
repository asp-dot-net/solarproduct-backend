﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Currencies.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Wholesaleinvoicetype.Dtos;

namespace TheSolarProduct.WholesaleinvoicetypeAppService.Exporting
{
    public interface IWholesaleinvoicetypeExcelExporter
    {
        FileDto ExportToFile(List<GetWholesaleinvoicetypeForViewDto> currency);
    }
}
