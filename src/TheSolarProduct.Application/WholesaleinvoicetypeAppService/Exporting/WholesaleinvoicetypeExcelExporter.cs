﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Currencies.Dtos;
using TheSolarProduct.Currencies.Exporting;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.Wholesaleinvoicetype.Dtos;

namespace TheSolarProduct.WholesaleinvoicetypeAppService.Exporting
{
    public class WholesaleinvoicetypeExcelExporter : NpoiExcelExporterBase, IWholesaleinvoicetypeExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public WholesaleinvoicetypeExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }


        public FileDto ExportToFile(List<GetWholesaleinvoicetypeForViewDto> invoice)
        {
            return CreateExcelPackage(
                "Wholesaleinvoicetype.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet("WholesaleInvoicetypes");

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, 2, invoice,
                        _ => _.Invoice.Name
                        );



                });
        }
    }
}
