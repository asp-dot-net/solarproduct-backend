﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Wholesaleinvoicetype;
using TheSolarProduct.Wholesaleinvoicetype.Dtos;
using TheSolarProduct.Wholesales.Ecommerces;
using Abp.Collections.Extensions;
using System.Linq;
using TheSolarProduct.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using TheSolarProduct.CheckDepositReceived;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using TheSolarProduct.TransportationCosts;
using Abp;
using TheSolarProduct.CheckApplications.Dtos;
using TheSolarProduct.Currencies.Exporting;
using TheSolarProduct.TransportCompanies.Exporting;
using TheSolarProduct.Currencies.Dtos;
using TheSolarProduct.Currencies;
using TheSolarProduct.WholesaleinvoicetypeAppService.Exporting;
using TheSolarProduct.Wholesales.DataVault;
using TheSolarProduct.WholeSaleStatuses.Dtos;
using TheSolarProduct.WholeSaleStatuses;


namespace TheSolarProduct.WholesaleinvoicetypeAppService
{
    public class WholesaleinvoicetypeAppService : TheSolarProductAppServiceBase, IWholesaleinvoicetypeAppService
    {
        
        private readonly IRepository<WholesaleInvoicetype> _wholesaleInvoicetypeRepository;
        private readonly IWholesaleinvoicetypeExcelExporter _wholesaleinvoicetypeExcelExporter;
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;

        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public WholesaleinvoicetypeAppService(
            
            IRepository<WholesaleInvoicetype> WholesaleInvoicetypeRepository,
           // IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
             IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
            IWholesaleinvoicetypeExcelExporter WholesaleinvoicetypeExcelExporter)
        {
            
            _wholesaleinvoicetypeExcelExporter = WholesaleinvoicetypeExcelExporter;
            _wholesaleInvoicetypeRepository = WholesaleInvoicetypeRepository;
           // _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;

        }

        public async Task<PagedResultDto<GetWholesaleinvoicetypeForViewDto>> GetAll(GetAllWholesaleinvoicetypeInput input)
        {
            var invoice = _wholesaleInvoicetypeRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFiltered = invoice
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFiltered
                         select new GetWholesaleinvoicetypeForViewDto()
                         {
                             Invoice = new WholesaleinvoicetypeDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 IsActive = o.IsActive
                             }
                         };

            var totalCount = await invoice.CountAsync();

            return new PagedResultDto<GetWholesaleinvoicetypeForViewDto>(totalCount, await output.ToListAsync());
        }

        public async Task<GetWholesaleinvoicetypeForEditOutput> GetWholesaleinvoicetypeForEdit(EntityDto input)
        {
            var invoice = await _wholesaleInvoicetypeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetWholesaleinvoicetypeForEditOutput { Invoice = ObjectMapper.Map<CreateOrEditWholesaleinvoicetypesDto>(invoice) };

            return output;
        }



        public async Task CreateOrEdit(CreateOrEditWholesaleinvoicetypesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        protected virtual async Task Create(CreateOrEditWholesaleinvoicetypesDto input)
        {
            var invoice = ObjectMapper.Map<WholesaleInvoicetype>(input);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 5;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "Wholesaleinvoice Created: " + input.Name;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

            await _wholesaleInvoicetypeRepository.InsertAsync(invoice);
        }


        protected virtual async Task Update(CreateOrEditWholesaleinvoicetypesDto input)
        {
            var invoice = await _wholesaleInvoicetypeRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 5;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "Wholesaleinvoice Updated : " + invoice.Name;
            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            var List = new List<WholeSaleDataVaultActivityLogHistory>();
            if (input.Name != invoice.Name)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = invoice.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != invoice.IsActive)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = invoice.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, invoice);

            await _wholesaleInvoicetypeRepository.UpdateAsync(invoice);
        }

        public async Task Delete(EntityDto input)
        {
            var Name = _wholesaleInvoicetypeRepository.Get(input.Id).Name;

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 5;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "Wholesaleinvoice Deleted  : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            await _wholesaleInvoicetypeRepository.DeleteAsync(input.Id);
        }


        public async Task<FileDto> GetInvoiceToExcel(GetAllWholesaleinvoicetypeForExcelInput input)
        {

            var filteredCurrency = _wholesaleInvoicetypeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredCurrency
                         select new GetWholesaleinvoicetypeForViewDto()
                         {
                             Invoice = new WholesaleinvoicetypeDto
                             {
                                 Name = o.Name,
                                 Id = o.Id
                             }
                         });


            var InvoiceListDtos = await query.ToListAsync();

            return _wholesaleinvoicetypeExcelExporter.ExportToFile(InvoiceListDtos);
        }


    }
}
