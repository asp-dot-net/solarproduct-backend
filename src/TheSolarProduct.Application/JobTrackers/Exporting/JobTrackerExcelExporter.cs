﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System.Collections.Generic;
using TheSolarProduct.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Storage;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.JobTrackers.Dtos;
using TheSolarProduct.Jobs;
using System;
using TheSolarProduct.Migrations;

namespace TheSolarProduct.JobTrackers.Exporting
{
    public class JobTrackerExcelExporter : NpoiExcelExporterBase, IJobTrackerExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly PermissionChecker _permissionChecker;

        public JobTrackerExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager,
            PermissionChecker permissionChecker) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _permissionChecker = permissionChecker;
        }

        public FileDto JobActiveTrackerExportToFile(List<GetViewTrackerDto> list, string fileName)
        {
            string[] Header = new string[] { "Job Number", "Job Type", "Job Status", "Company Name", "SMS Send", "Email Send", "Current Lead Owner", "Comment", "Next Followup", "Description" };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("JobActiveTracker"));
                    AddHeader(sheet, Header);
                    AddObjects(
                       sheet, 2, list,
                       _ => _.JobNumber,
                       _ => _.JobType,
                       _ => _.JobStatus,
                       _ => _.CompanyName,
                       _ => _.JobActiveSmsSend == true ? "Yes" : "No",
                       _ => _.JobActiveEmailSend == true ? "Yes" : "No",
                       _ => _.CurruntLeadOwner,
                       _ => _.Comment,
                       _ => _.NextFollowup,
                       _ => _.Discription
                       );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto JobActiveTrackerCheckActiveExportToFile(List<GetAllJobCheckActiveExcel> list, string fileName)
        {
            string[] Header = new string[] { "Job Number", "Deposit Received", "Dist Applied", "Dist Approved", "Dist Expire", "NMI Number", "Approved Reference Number", "Meter Upgrade", "Meter Box Photo", "Signed Quote", "Deposit", "Elec Retailer", "Peak Meter No", "Sales Rep", "State", "Payment Option", "SystemKw", "NotDepRecYet", "NotActiveYet", "NotBookedYet", "Last Comment", "Last Notify" };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("CheckActive"));
                    AddHeader(sheet, Header);
                    AddObjects(
                       sheet, 2, list,
                       _ => _.JobNumber,
                       _ => _.DepositeReceived,
                       _ => _.DistApplied,
                       _ => _.DistApproved,
                       _ => _.DistExpire,
                       _ => _.NMINumber,
                       _ => _.ApprovedReferenceNumber,
                       _ => _.MeterUpgrade,
                       _ => _.MeterBoxPhoto,
                       _ => _.SignedQuote,
                       _ => _.Deposit,
                       _ => _.ElecRetailer,
                       _ => _.PeakMeterNo,
                       _ => _.CurruntLeadOwner,
                       _ => _.State,
                       _ => _.PaymentOption,
                       _ => _.SystemCapacity,
                       _ => _.NotDepRecYet,
                       _ => _.NotActiveYet,
                       _ => _.NotBookedYet,
                       _ => _.ActivityComment,
                       _ => _.Notify
                       );

                    for (var i = 1; i <= list.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[2], "dd/mm/yyyy");
                        SetCellDataFormat(sheet.GetRow(i).Cells[3], "dd/mm/yyyy");
                        SetCellDataFormat(sheet.GetRow(i).Cells[4], "dd/mm/yyyy");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[16], "00.00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[17], "00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[18], "00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[19], "00");
                    }

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto STCTrackerExportToFile(List<GetViewSTCTrackerDto> list, string fileName)
        {
            string[] Header = new string[] { "Job Number", "Job Type", "Address", "Installed Date", "STC App Date", "PVD No", "PVD Status", "REC PVDNumber", "REC PVD Status", "Job Status", "Kw", "STC", "STC Price", "Installer Name", "STC Notes", "NotAppliedDays", "notApprovedDays", "SMS Send", "Email Send", "Comment", "Comment Date" ,};

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("STCTracker"));
                    AddHeader(sheet, Header);
                    AddObjects(
                       sheet, 2, list,
                       _ => _.JobNumber,
                       _ => _.JobTypeName,
                       _ => _.Address,
                       _ => _.InstallationDate != null ? Convert.ToDateTime(_.InstallationDate).ToString() : "",
                       _ => _.STCAppliedDate != null ? Convert.ToDateTime(_.STCAppliedDate).ToString() : "",
                       _ => _.PvdNumber,
                       _ => _.PvdStatusName,
                       _ => _.RECPvdNumber,
                       _ => _.RECPvdStatus,
                       _ => _.JobStatusName,
                       _ => _.SystemCapacity,
                       _ => _.STC,
                       _ => _.STCTotalPrice,
                       _ => _.InstallerName,
                       _ => _.STCNotes,
                       _ => _.NotAppliedDays,
                       _ => _.NotApprovedDays,
                       _ => _.STCSmsSend == true ? "Yes" : "No",
                       _ => _.STCEmailSend == true ? "Yes" : "No",
                       _ => _.ActivityComment,
                       _ => _.ActivityCommentDate != null ? Convert.ToDateTime(_.ActivityCommentDate).ToString() : ""

                       );

                    for (var i = 1; i <= list.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[10], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[11], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[12], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[15], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[16], "0.00");
                    }
                   
                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
        public FileDto GridConnectionExportToFile(List<GetViewGridConnectionTrackerDto> list, string fileName)
        {
            string[] Header = new string[] { "Company Name", "Mobile", "Email", "Address", "PostCode", "State", "JobNumber", "Job Type","InstallerName", "InstallationDate", "SystemCapacity", "NMINumber", "ApplicationRefNo" , "ElecRetailers", "NotAppliedDays" };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("STCTracker"));
                    AddHeader(sheet, Header);
                    AddObjects(
                       sheet, 2, list,
                       _ => _.CompanyName,
                       _ => _.Mobile,
                       _ => _.Email,
                       _ => _.Address,
                       _ => _.PostalCode,
                       _ => _.State,
                       _ => _.JobNumber,
                       _ => _.JobType,
                       _ => _.InstallerName,
                       _ => _.InstallationDate != null ? Convert.ToDateTime(_.InstallationDate).ToString() : "",
                       _ => _.SystemCapacity,
                       _ => _.NMINumber,
                       _ => _.ApplicationRefNo,
                       _ => _.ElecRetailerName,
                       _ => _.NotAppliedDays
                    );

                    for (var i = 1; i <= list.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[9], "dd/mm/yyyy");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[10], "00.00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[14], "00");
                       
                        
                    }

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto GridConnectExportToFile(List<GetAllGridConnectExcelDto> list, string fileName)
        {
            string[] Header = new string[] { "Company Name", "Mobile", "Email", "Address", "PostCode", "State", "JobNumber", "Job Type", "InstallationDate", "SystemCapacity", "NMINumber", "ApplicationRefNo", "ElecRetailers" };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("STCTracker"));
                    AddHeader(sheet, Header);
                    AddObjects(
                       sheet, 2, list,
                       _ => _.CompanyName,
                       _ => _.Mobile,
                       _ => _.Email,
                       _ => _.Address,
                       _ => _.PostCode,
                       _ => _.State,
                       _ => _.JobNumber,
                       _ => _.JobType,
                       _ => _.InstallationDate != null ? Convert.ToDateTime(_.InstallationDate).ToString() : "",
                       _ => _.SystemCapacity,
                       _ => _.NMINumber,
                       _ => _.ApplicationRefNo,
                       _ => _.ElecRetailers
                       );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
        public FileDto FreebiesTrackerExportToFile(List<GetViewFreebiesTrackerDto> list, string fileName)
        {
            string[] Header = new string[] { "Job number","Promotion Name", "Company Name", "Address", "Job Status", "Bal Owing", "Transport Name", "Tracking Number", "Current Lead Owner", "PandingDays", "Next Followup", "Description", "Comment", "SMS Send", "Email Send" };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("FreebiesTracker");
                    AddHeader(sheet, Header);
                    AddObjects(
                       sheet, 2, list,
                       _ => _.JobNumber,
                       _ => _.PromotionName,
                       _ => _.CompanyName,
                       _ => _.Address + " " + _.Suburb + " " + _.State + " " + _.PostalCode ,
                       _ => _.JobStatus,
                       _ => _.TotalCost - _.BalOwing ,
                       _ => _.TransportName,
                       _ => _.TrackingNumber,
                       _ => _.CurruntLeadOwner,
                       _ => _.PandingDays,
                       _ => _.NextFollowup,
                       _ => _.Discription,
                       _ => _.Comment,
                       _ => _.SmsSend != true ? "No" : "Yes",
                       _ => _.EmailSend != true ? "No" : "Yes"
                       );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                    for (var i = 1; i <= list.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[5], "0.00");

                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "0.00");

                    }
                });
        }
    }
}
