﻿using System.Collections.Generic;
using TheSolarProduct.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.JobTrackers.Dtos;

namespace TheSolarProduct.JobTrackers.Exporting
{
    public interface IJobTrackerExcelExporter
    {
        public FileDto JobActiveTrackerExportToFile(List<GetViewTrackerDto> list, string fileName);

        public FileDto JobActiveTrackerCheckActiveExportToFile(List<GetAllJobCheckActiveExcel> list, string fileName);

        public FileDto STCTrackerExportToFile(List<GetViewSTCTrackerDto> list, string fileName);
        public FileDto GridConnectionExportToFile(List<GetViewGridConnectionTrackerDto> list, string fileName);

        public FileDto GridConnectExportToFile(List<GetAllGridConnectExcelDto> list, string fileName);

        public FileDto FreebiesTrackerExportToFile(List<GetViewFreebiesTrackerDto> list, string fileName);
    }
}
