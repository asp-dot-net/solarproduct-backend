﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheSolarProduct.Jobs;
using TheSolarProduct.JobTrackers.Dtos;
using Abp.Application.Services.Dto;
using Abp.Linq.Extensions;
using Abp.Timing.Timezone;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Invoices;
using TheSolarProduct.PVDStatuses;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Quotations;
using TheSolarProduct.PreviousJobStatuses;
using TheSolarProduct.Timing;
using TheSolarProduct.JobTrackers.Exporting;
using TheSolarProduct.Dto;
using TheSolarProduct.Leads;
using Abp.Authorization;
using Hangfire.Common;
using Stripe;
using NPOI.SS.Formula.Functions;
using TheSolarProduct.Jobs.Dtos;
using System.Diagnostics;
using Org.BouncyCastle.Asn1.Cmp;

namespace TheSolarProduct.JobTrackers
{
    [AbpAuthorize]
    public class JobTrackerAppService : TheSolarProductAppServiceBase, IJobTrackerAppService
    {
        private readonly IRepository<Jobs.Job> _jobsRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<Document> _documentRepository;
        private readonly IRepository<Quotation> _quotationRepository;
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly ITimeZoneService _timeZoneService;
        private readonly IRepository<JobPromotion> _jobPromotionRepository;
        private readonly IRepository<InvoiceImportData> _invoiceImportDataRepository;
        private readonly IRepository<JobRefund> _jobRefundRepository;
        private readonly IRepository<JobStatus> _jobStatusRepository;
        private readonly IRepository<PreviousJobStatus> _previousJobStatusRepository;
        private readonly IRepository<InvoicePaymentMethod, int> _invoicePaymentMethodRepository;
        private readonly IRepository<PVDStatus, int> _pvdStatusRepository;
        private readonly IJobTrackerExcelExporter _jobTrackerExcelExporter;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<MeterPhase, int> _lookup_meterPhaseRepository;

        public JobTrackerAppService(IRepository<Jobs.Job> jobsRepository,
            UserManager userManager,
            IRepository<User, long> userRepository,
            IRepository<UserTeam> userTeamRepository,
            IRepository<Team> teamRepository,
            IRepository<Document> documentRepository,
            IRepository<Quotation> quotationRepository,
            IRepository<InvoicePayment> invoicePaymentRepository,
            IRepository<LeadActivityLog> leadactivityRepository,
            ITimeZoneConverter timeZoneConverter,
            ITimeZoneService timeZoneService,
            IRepository<JobPromotion> jobPromotionRepository,
            IRepository<InvoiceImportData> invoiceImportDataRepository,
            IRepository<JobRefund> jobRefundRepository,
            IRepository<JobStatus> jobStatusRepository,
            IRepository<PreviousJobStatus> previousJobStatusRepository,
            IRepository<InvoicePaymentMethod, int> invoicePaymentMethodRepository,
            IRepository<PVDStatus, int> pvdStatusRepository,
            IJobTrackerExcelExporter jobTrackerExcelExporter,
            IRepository<JobProductItem> jobProductItemRepository
            , IRepository<MeterPhase, int> lookup_meterPhaseRepository
            )
        {
            _jobsRepository = jobsRepository;
            _userManager = userManager;
            _userRepository = userRepository;
            _userTeamRepository = userTeamRepository;
            _teamRepository = teamRepository;
            _documentRepository = documentRepository;
            _quotationRepository = quotationRepository;
            _invoicePaymentRepository = invoicePaymentRepository;
            _leadactivityRepository = leadactivityRepository;
            _timeZoneConverter = timeZoneConverter;
            _timeZoneService = timeZoneService;
            _jobPromotionRepository = jobPromotionRepository;
            _invoiceImportDataRepository = invoiceImportDataRepository;
            _jobRefundRepository = jobRefundRepository;
            _jobStatusRepository = jobStatusRepository;
            _previousJobStatusRepository = previousJobStatusRepository;
            _invoicePaymentMethodRepository = invoicePaymentMethodRepository;
            _pvdStatusRepository = pvdStatusRepository;
            _jobTrackerExcelExporter = jobTrackerExcelExporter;
            _jobProductItemRepository = jobProductItemRepository;
            _lookup_meterPhaseRepository = lookup_meterPhaseRepository;
        }

        public async Task<PagedResultDto<GetViewTrackerDto>> GetAllJobActiveTracker(GetAllJobActiveTrackerInput input)
        {
            #region Old Code
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var filteredResult = _jobsRepository.GetAll()
            //                  .Include(e => e.LeadFk)
            //                  .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            //                  .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            //                  .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            //                  .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.JobStatusId != 3 && e.SolarRebateStatus != 3 && e.FirstDepositDate != null);

            //#region Summary Count
            //SummaryCountDto summary = new SummaryCountDto();
            //summary.Total = filteredResult.Count();
            //summary.Active = filteredResult.Where(e => e.ActiveDate != null).Count();

            //summary.ActiveReady = filteredResult.Where(e => e.ActiveDate == null && e.HouseTypeId != null && e.ElecDistributorId != null && e.RoofAngleId != null && e.RoofTypeId != null && e.InstallerNotes != null && e.MeterPhaseId != null && e.NMINumber != null && e.PeakMeterNo != null && e.ApplicationRefNo != null && e.MeterUpgradeId != null && (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == true && e.ElecRetailerId != null && e.DepositeRecceivedDate != null
            //&& _documentRepository.GetAll().Where(j => j.JobId == e.Id).Any()
            //&& _documentRepository.GetAll().Where(j => j.DocumentTypeId == 3 && j.JobId == e.Id).Any()
            //&& (_quotationRepository.GetAll().Where(j => j.JobId == e.Id && j.IsSigned == true).Any() || _documentRepository.GetAll().Where(j => j.JobId == e.Id && j.DocumentTypeId == 5).Any())).Count();

            //summary.Awaiting = filteredResult.Where(e => e.ActiveDate == null && (e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofAngleId == null || e.RoofTypeId == null || e.InstallerNotes == null || e.MeterPhaseId == null || e.NMINumber == null || e.PeakMeterNo == null || e.ApplicationRefNo == null || e.MeterUpgradeId == null || (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == false || e.ElecRetailerId == null || e.DepositeRecceivedDate == null
            //|| _documentRepository.GetAll().Where(j => j.JobId == e.Id).Any() == false
            //|| _documentRepository.GetAll().Where(j => j.DocumentTypeId == 3 && j.JobId == e.Id).Any() == false
            //|| (_quotationRepository.GetAll().Where(j => j.JobId == e.Id && j.IsSigned == true).Any() || _documentRepository.GetAll().Where(j => j.JobId == e.Id && j.DocumentTypeId == 5).Any()) == false)
            //).Count();
            //#endregion

            //var filter = filteredResult
            //            .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email.Contains(input.Filter))
            //            .WhereIf(input.PaymentId != 0 && input.PaymentId != null, e => e.PaymentOptionId == input.PaymentId)
            //            .WhereIf(input.PaymentId != 0 && input.PaymentId != null, e => e.PaymentOptionId == input.PaymentId)
            //            .WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => input.JobStatusIDFilter.Contains((int)e.JobStatusId))

            //            .WhereIf(input.ApplicationStatus == 1, e => e.ActiveDate != null)

            //            .WhereIf(input.ApplicationStatus == 2, e => e.ActiveDate == null && e.HouseTypeId != null && e.ElecDistributorId != null && e.RoofAngleId != null && e.RoofTypeId != null && e.InstallerNotes != null && e.MeterPhaseId != null && e.NMINumber != null && e.PeakMeterNo != null && e.ApplicationRefNo != null && e.MeterUpgradeId != null && (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == true && e.ElecRetailerId != null && e.DepositeRecceivedDate != null
            //             && _documentRepository.GetAll().Where(j => j.JobId == e.Id).Any()
            //             && _documentRepository.GetAll().Where(j => j.DocumentTypeId == 3 && j.JobId == e.Id).Any()
            //             && (_quotationRepository.GetAll().Where(j => j.JobId == e.Id && j.IsSigned == true).Any() || _documentRepository.GetAll().Where(j => j.JobId == e.Id && j.DocumentTypeId == 5).Any()))

            //            .WhereIf(input.ApplicationStatus == 3, e => e.ActiveDate == null && (e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofAngleId == null || e.RoofTypeId == null || e.InstallerNotes == null || e.MeterPhaseId == null || e.NMINumber == null || e.PeakMeterNo == null || e.ApplicationRefNo == null || e.MeterUpgradeId == null || (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == false || e.ElecRetailerId == null || e.DepositeRecceivedDate == null
            //            || _documentRepository.GetAll().Where(j => j.JobId == e.Id).Any() == false
            //            || _documentRepository.GetAll().Where(j => j.DocumentTypeId == 3 && j.JobId == e.Id).Any() == false
            //            || (_quotationRepository.GetAll().Where(j => j.JobId == e.Id && j.IsSigned == true).Any() || _documentRepository.GetAll().Where(j => j.JobId == e.Id && j.DocumentTypeId == 5).Any()) == false)
            //            )

            //            .WhereIf(input.DateType == "DepositeReceivedDate" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "DepositeReceivedDate" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.DateType == "ActiveDate" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "ActiveDate" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.DateType == "FirstDespoitDate" && input.StartDate != null, e => e.FirstDepositDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "FirstDespoitDate" && input.EndDate != null, e => e.FirstDepositDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
            //            ;

            //var pagedAndFilteredResult = filter
            //    .OrderBy(input.Sorting ?? "id desc")
            //    .PageBy(input);

            //var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 5 && e.LeadFk.OrganizationId == input.OrganizationUnit);

            //var results = from o in pagedAndFilteredResult
            //              select new GetViewTrackerDto()
            //              {
            //                  Id = o.Id,
            //                  LeadId = o.LeadFk.Id,
            //                  JobNumber = o.JobNumber,
            //                  JobType = o.JobTypeFk.Name,
            //                  JobStatus = o.JobStatusFk.Name,
            //                  JobStatusColorClass = o.JobStatusFk.ColorClass,
            //                  CompanyName = o.LeadFk.CompanyName,
            //                  JobActiveSmsSend = o.JobActiveSmsSend == null ? false : (bool)o.JobActiveSmsSend,
            //                  JobActiveSmsSendDate = o.JobActiveSmsSendDate,
            //                  JobActiveEmailSend = o.JobActiveEmailSend == null ? false : (bool)o.JobActiveEmailSend,
            //                  JobActiveEmailSendDate = o.JobActiveEmailSendDate,
            //                  CurruntLeadOwner = _userRepository.GetAll().Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),

            //                  NextFollowup = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
            //                  Discription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
            //                  Comment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

            //                  Summary = summary
            //              };

            //var totalCount = await filter.CountAsync();

            //return new PagedResultDto<GetViewTrackerDto>(totalCount, await results.ToListAsync());
            #endregion

            #region New Code
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var filteredResult = _jobsRepository.GetAll()
                              .Include(e => e.LeadFk)
                              .Include(e => e.JobTypeFk)
                              .Include(e => e.JobStatusFk)
                              .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.JobStatusId != 3 && e.SolarRebateStatus != 3 && e.FirstDepositDate != null)
                              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)

                              .WhereIf(input.DateType == "DepositeReceivedDate" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                              .WhereIf(input.DateType == "DepositeReceivedDate" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                              .WhereIf(input.DateType == "ActiveDate" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
                              .WhereIf(input.DateType == "ActiveDate" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date)

                              .WhereIf(input.DateType == "FirstDespoitDate" && input.StartDate != null, e => e.FirstDepositDate.Value.Date >= SDate.Value.Date)
                              .WhereIf(input.DateType == "FirstDespoitDate" && input.EndDate != null, e => e.FirstDepositDate.Value.Date <= EDate.Value.Date)

                               .WhereIf(input.PaymentId != 0 && input.PaymentId != null, e => e.PaymentOptionId == input.PaymentId)
                               .WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => input.JobStatusIDFilter.Contains((int)e.JobStatusId))
                               .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                               .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                               .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                               .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                               .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                                .WhereIf(!string.IsNullOrEmpty(input.State), e => e.LeadFk.State == input.State)
                              .AsNoTracking()
                              .Select(e => new { e.Id, e.JobNumber, e.LeadFk, e.ActiveDate, e.HouseTypeId, e.ElecDistributorId, e.RoofAngleId, e.RoofTypeId, e.InstallerNotes, e.MeterPhaseId, e.NMINumber, e.PeakMeterNo, e.ApplicationRefNo, e.MeterUpgradeId, e.PaymentOptionId, e.FinanceDocumentVerified, e.ElecRetailerId, e.DepositeRecceivedDate, e.JobTypeFk, e.JobStatusId, e.FirstDepositDate, e.JobStatusFk, e.JobActiveSmsSend, e.JobActiveSmsSendDate, e.JobActiveEmailSend, e.JobActiveEmailSendDate, e.LeadId, e.DistApproveDate, e.ExpiryDate, e.State, e.SolarRebateStatus, e.VicRebate });

            //var decument = _documentRepository.GetAll().AsNoTracking().Select(e => new { e.JobId, e.DocumentTypeId });
            //var quotation = _quotationRepository.GetAll().Where(e => e.IsSigned == true).AsNoTracking().Select(e => new { e.JobId, e.IsSigned });

            #region Summary Count


            SummaryCountDto summary = new SummaryCountDto();
            summary.Total = await filteredResult.CountAsync();
            summary.Active = await filteredResult.Where(e => e.ActiveDate == null && (e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofTypeId == null || e.RoofAngleId == null
                                || _quotationRepository.GetAll().Where(q => q.JobId == e.Id).Any() == false
                                || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id && (i.InvoicePaymentMethodFk.PaymentMethod == "EFT" || i.InvoicePaymentMethodFk.PaymentMethod == "Credit Card") && i.IsVerified == false).Any()
                                 || _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id).Any() == false) == false &&
                                 (_lookup_meterPhaseRepository.GetAll().Where(m => m.Id == e.MeterPhaseId).Any() == false || string.IsNullOrEmpty(e.PeakMeterNo) || string.IsNullOrEmpty(e.NMINumber) || _documentRepository.GetAll().Where(d => d.JobId == e.Id).Any() == false || e.ElecRetailerId == null
                                  || string.IsNullOrEmpty(e.ApplicationRefNo)
                                  || e.DistApproveDate == null
                                  || e.MeterUpgradeId == null
                                  || _documentRepository.GetAll().Where(d => d.DocumentTypeId == 3 && d.JobId == e.Id).Any() == false
                                || (_quotationRepository.GetAll().Where(q => q.JobId == e.Id && q.IsSigned == true).Any() == false && _documentRepository.GetAll().Where(d => d.JobId == e.Id && d.DocumentTypeId == 5).Any() == false)
                                 || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 || (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == false
                                 || (e.State == "VIC" ? e.ExpiryDate == null : true) == false
                                || (e.State == "VIC" ? ((e.SolarRebateStatus == 0 || e.SolarRebateStatus != 17) && e.VicRebate != "With Rebate Install" ? false : true) : true) == false) == false).CountAsync();

            //summary.ActiveReady = await filteredResult.Where(e => e.ActiveDate == null && e.HouseTypeId != null && e.ElecDistributorId != null && e.RoofAngleId != null && e.RoofTypeId != null && e.InstallerNotes != null && e.MeterPhaseId != null && e.NMINumber != null && e.PeakMeterNo != null && e.ApplicationRefNo != null && e.MeterUpgradeId != null && (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == true && e.ElecRetailerId != null && e.DepositeRecceivedDate != null
            //&& _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any()
            //&& _documentRepository.GetAll().AsNoTracking().Where(j => j.DocumentTypeId == 3 && j.JobId == e.Id).Any()
            //&& (_quotationRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id && j.IsSigned == true).Any() || _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id && j.DocumentTypeId == 5).Any())).CountAsync();
            summary.ActiveReady = await filteredResult.Where(e => (e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofTypeId == null || e.RoofAngleId == null
                                || _quotationRepository.GetAll().Where(q => q.JobId == e.Id).Any() == false
                                || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 || _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id).Any() == false) == false &&
                                 (_lookup_meterPhaseRepository.GetAll().Where(m => m.Id == e.MeterPhaseId).Any() == false || string.IsNullOrEmpty(e.PeakMeterNo) || string.IsNullOrEmpty(e.NMINumber) || _documentRepository.GetAll().Where(d => d.JobId == e.Id).Any() == false || e.ElecRetailerId == null
                                  || string.IsNullOrEmpty(e.ApplicationRefNo)
                                  || e.DistApproveDate == null
                                  || e.MeterUpgradeId == null
                                  || _documentRepository.GetAll().Where(d => d.DocumentTypeId == 3 && d.JobId == e.Id).Any() == false
                                || (_quotationRepository.GetAll().Where(q => q.JobId == e.Id && q.IsSigned == true).Any() == false && _documentRepository.GetAll().Where(d => d.JobId == e.Id && d.DocumentTypeId == 5).Any() == false)
                                 || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 || (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == false
                                 || (e.State == "VIC" ? e.ExpiryDate == null : true) == false
                                || (e.State == "VIC" ? ((e.SolarRebateStatus == 0 || e.SolarRebateStatus != 17) && e.VicRebate != "With Rebate Install" ? false : true) : true) == false
                                 || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id && (i.InvoicePaymentMethodFk.PaymentMethod == "EFT" || i.InvoicePaymentMethodFk.PaymentMethod == "Credit Card") && i.IsVerified == false).Any()

                                  )).CountAsync();
            //summary.Awaiting = await filteredResult.Where(e => e.ActiveDate == null && (e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofAngleId == null || e.RoofTypeId == null || e.InstallerNotes == null || e.MeterPhaseId == null || e.NMINumber == null || e.PeakMeterNo == null || e.ApplicationRefNo == null || e.MeterUpgradeId == null || (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == false || e.ElecRetailerId == null
            //|| _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() == false
            //|| _documentRepository.GetAll().AsNoTracking().Where(j => j.DocumentTypeId == 3 && j.JobId == e.Id).Any() == false
            //|| (_quotationRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id && j.IsSigned == true).Any()
            //|| _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id && j.DocumentTypeId == 5).Any()) == false) || e.DepositeRecceivedDate == null
            //|| _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id && (i.InvoicePaymentMethodFk.PaymentMethod == "EFT" || i.InvoicePaymentMethodFk.PaymentMethod == "Credit Card") && i.IsVerified == false).Any()
            //).CountAsync();
            summary.Awaiting = await filteredResult.Where(e => e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofTypeId == null || e.RoofAngleId == null
                                || _quotationRepository.GetAll().Where(q => q.JobId == e.Id).Any() == false
                                || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                   //|| _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id && (i.InvoicePaymentMethodFk.PaymentMethod == "EFT" || i.InvoicePaymentMethodFk.PaymentMethod == "Credit Card") && i.IsVerified == false).Any()
                                   || _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id).Any() == false || e.DepositeRecceivedDate == null
                                 ).CountAsync();

            #endregion

            var filter = filteredResult

                        //.WhereIf(input.ApplicationStatus == 1, e => e.ActiveDate != null)
                        .WhereIf(input.ApplicationStatus == 1, e => e.ActiveDate == null && (e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofTypeId == null || e.RoofAngleId == null
                                || _quotationRepository.GetAll().Where(q => q.JobId == e.Id).Any() == false
                                || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id && (i.InvoicePaymentMethodFk.PaymentMethod == "EFT" || i.InvoicePaymentMethodFk.PaymentMethod == "Credit Card") && i.IsVerified == false).Any()
                                 || _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id).Any() == false) == false &&
                                 (_lookup_meterPhaseRepository.GetAll().Where(m => m.Id == e.MeterPhaseId).Any() == false || string.IsNullOrEmpty(e.PeakMeterNo) || string.IsNullOrEmpty(e.NMINumber) || _documentRepository.GetAll().Where(d => d.JobId == e.Id).Any() == false || e.ElecRetailerId == null
                                  || string.IsNullOrEmpty(e.ApplicationRefNo)
                                  || e.DistApproveDate == null
                                  || e.MeterUpgradeId == null
                                  || _documentRepository.GetAll().Where(d => d.DocumentTypeId == 3 && d.JobId == e.Id).Any() == false
                                || (_quotationRepository.GetAll().Where(q => q.JobId == e.Id && q.IsSigned == true).Any() == false && _documentRepository.GetAll().Where(d => d.JobId == e.Id && d.DocumentTypeId == 5).Any() == false)
                                 || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 || (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == false
                                 || (e.State == "VIC" ? e.ExpiryDate == null : true) == false
                                || (e.State == "VIC" ? ((e.SolarRebateStatus == 0 || e.SolarRebateStatus != 17) && e.VicRebate != "With Rebate Install" ? false : true) : true) == false) == false)

            //            .WhereIf(input.ApplicationStatus == 2, e => e.ActiveDate == null && e.HouseTypeId != null && e.ElecDistributorId != null && e.RoofAngleId != null && e.RoofTypeId != null && e.InstallerNotes != null && e.MeterPhaseId != null && e.NMINumber != null && e.PeakMeterNo != null && e.ApplicationRefNo != null && e.MeterUpgradeId != null && (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == true && e.ElecRetailerId != null && e.DepositeRecceivedDate != null
            //&& _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any()
            //&& _documentRepository.GetAll().AsNoTracking().Where(j => j.DocumentTypeId == 3 && j.JobId == e.Id).Any()
            //&& (_quotationRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id && j.IsSigned == true).Any() || _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id && j.DocumentTypeId == 5).Any()))
                        .WhereIf(input.ApplicationStatus == 2, e => (e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofTypeId == null || e.RoofAngleId == null
                                || _quotationRepository.GetAll().Where(q => q.JobId == e.Id).Any() == false
                                || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 || _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id).Any() == false) == false &&
                                 (_lookup_meterPhaseRepository.GetAll().Where(m => m.Id == e.MeterPhaseId).Any() == false || string.IsNullOrEmpty(e.PeakMeterNo) || string.IsNullOrEmpty(e.NMINumber) || _documentRepository.GetAll().Where(d => d.JobId == e.Id).Any() == false || e.ElecRetailerId == null
                                  || string.IsNullOrEmpty(e.ApplicationRefNo)
                                  || e.DistApproveDate == null
                                  || e.MeterUpgradeId == null
                                  || _documentRepository.GetAll().Where(d => d.DocumentTypeId == 3 && d.JobId == e.Id).Any() == false
                                || (_quotationRepository.GetAll().Where(q => q.JobId == e.Id && q.IsSigned == true).Any() == false && _documentRepository.GetAll().Where(d => d.JobId == e.Id && d.DocumentTypeId == 5).Any() == false)
                                 || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 || (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == false
                                 || (e.State == "VIC" ? e.ExpiryDate == null : true) == false
                                || (e.State == "VIC" ? ((e.SolarRebateStatus == 0 || e.SolarRebateStatus != 17) && e.VicRebate != "With Rebate Install" ? false : true) : true) == false
                                 || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id && (i.InvoicePaymentMethodFk.PaymentMethod == "EFT" || i.InvoicePaymentMethodFk.PaymentMethod == "Credit Card") && i.IsVerified == false).Any()
                                ))


            //            .WhereIf(input.ApplicationStatus == 3, e => e.ActiveDate == null && (e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofAngleId == null || e.RoofTypeId == null || e.InstallerNotes == null || e.MeterPhaseId == null || e.NMINumber == null || e.PeakMeterNo == null || e.ApplicationRefNo == null || e.MeterUpgradeId == null || (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == false || e.ElecRetailerId == null
            //|| _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() == false
            //|| _documentRepository.GetAll().AsNoTracking().Where(j => j.DocumentTypeId == 3 && j.JobId == e.Id).Any() == false
            //|| (_quotationRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id && j.IsSigned == true).Any() || _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id && j.DocumentTypeId == 5).Any()) == false) || e.DepositeRecceivedDate == null
            //|| _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id && (i.InvoicePaymentMethodFk.PaymentMethod == "EFT" || i.InvoicePaymentMethodFk.PaymentMethod == "Credit Card") && i.IsVerified == false).Any() == true
            //            )
              .WhereIf(input.ApplicationStatus == 3, e => e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofTypeId == null || e.RoofAngleId == null
                                || _quotationRepository.GetAll().Where(q => q.JobId == e.Id).Any() == false
                                || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 // || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id && (i.InvoicePaymentMethodFk.PaymentMethod == "EFT" || i.InvoicePaymentMethodFk.PaymentMethod == "Credit Card") && i.IsVerified == false).Any()
                                 || _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id).Any() == false || e.DepositeRecceivedDate == null
                        )



                        ;

            var pagedAndFilteredResult = filter
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 5 && e.LeadFk.OrganizationId == input.OrganizationUnit).AsNoTracking()
                .Select(e => new { e.Id, e.LeadId, e.ActionId, e.ActivityDate, e.ActivityNote });

            var results = from o in pagedAndFilteredResult
                          select new GetViewTrackerDto()
                          {
                              Id = o.Id,
                              LeadId = o.LeadFk.Id,
                              JobNumber = o.JobNumber,
                              JobType = o.JobTypeFk.Name,
                              JobStatus = o.JobStatusFk.Name,
                              JobStatusColorClass = o.JobStatusFk.ColorClass,
                              CompanyName = o.LeadFk.CompanyName,
                              JobActiveSmsSend = o.JobActiveSmsSend == null ? false : (bool)o.JobActiveSmsSend,
                              JobActiveSmsSendDate = o.JobActiveSmsSendDate,
                              JobActiveEmailSend = o.JobActiveEmailSend == null ? false : (bool)o.JobActiveEmailSend,
                              JobActiveEmailSendDate = o.JobActiveEmailSendDate,
                              CurruntLeadOwner = User_List.Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),

                              NextFollowup = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                              Discription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              Comment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                              Summary = summary
                          };

            var totalCount = await filter.CountAsync();

            return new PagedResultDto<GetViewTrackerDto>(totalCount, await results.ToListAsync());
            #endregion
        }

        public async Task<PagedResultDto<GetViewApplicationTrackerDto>> GetAllJobApplicationTracker(GetAllJobApplicationTrackerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var Today = _timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId);

            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var filteredResult = _jobsRepository.GetAll()
                              .Include(e => e.LeadFk)
                              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                              .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.JobStatusId != 3 && e.SolarRebateStatus != 3 && e.FirstDepositDate != null)
                              .AsNoTracking()
                              .Select(e => new { e.Id, e.JobNumber, e.LeadFk, e.State, e.ElecDistributorId, e.JobStatusId, e.JobTypeId, e.DepositeRecceivedDate, e.DistApplied,e.InstallationDate, e.ActiveDate, e.ApplicationRefNo, e.MeterPhaseId, e.NMINumber, e.PeakMeterNo, e.DistExpiryDate, e.JobTypeFk, e.JobStatusFk, e.Address, e.Suburb, e.PostalCode, e.ElecDistributorFk, e.DistApproveDate, e.SmsSend, e.SmsSendDate, e.EmailSend, e.EmailSendDate, e.LeadId });

            #region Summary Count
            ApplicationSummaryCountDto summary = new ApplicationSummaryCountDto();
            summary.Total = await filteredResult.CountAsync();

            summary.Awaiting = await filteredResult.Where(e => e.ApplicationRefNo == null && (_documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() == false || e.MeterPhaseId == null || e.NMINumber == null || e.PeakMeterNo == null)).CountAsync();

            summary.Pending = await filteredResult.Where(e => e.ApplicationRefNo == null && _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() && e.MeterPhaseId != null && e.NMINumber == null && e.PeakMeterNo != null).CountAsync();

            summary.Complete = await filteredResult.Where(e => e.ApplicationRefNo != null).CountAsync();

            summary.Expiry = await filteredResult.Where(e => e.ApplicationRefNo != null && (e.DistExpiryDate.Value.AddDays(-30) < Today) && e.JobStatusId < 6).CountAsync();
            #endregion

            var filter = filteredResult
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(input.FilterName == "ApplicationRefNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ApplicationRefNo == input.Filter)
                        .WhereIf(input.FilterName == "NMINumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.NMINumber == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                        .WhereIf(input.ElecDistributorId != 0, e => e.ElecDistributorId == input.ElecDistributorId)
                        .WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => input.JobStatusIDFilter.Contains((int)e.JobStatusId))
                        .WhereIf(input.JobTypeId != 0, e => e.JobTypeId == input.JobTypeId)

                        .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ApplicationDate" && input.StartDate != null, e => e.DistApplied.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ApplicationDate" && input.EndDate != null, e => e.DistApplied.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "InstallationDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "InstallationDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ActiveDate" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ActiveDate" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.ApplicationStatus == 1, e => e.ApplicationRefNo == null
                        && (_documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() == false || e.MeterPhaseId == null || e.NMINumber == null || e.PeakMeterNo == null)) //Application Awaiting

                        .WhereIf(input.ApplicationStatus == 2, e => e.ApplicationRefNo == null
                        && _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() && e.MeterPhaseId != null && e.NMINumber == null && e.PeakMeterNo != null) //Application Pending

                        .WhereIf(input.ApplicationStatus == 3, e => e.ApplicationRefNo != null) //Application Complete

                        .WhereIf(input.ApplicationStatus == 4, e => e.ApplicationRefNo != null && (e.DistExpiryDate.Value.AddDays(-30) < Today) && e.JobStatusId < 6) //Application Expiry
                        ;

            var pagedAndFilteredResult = filter
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var activity = _leadactivityRepository.GetAll().AsNoTracking().Where(e => e.SectionId == 1).Select(e => new { e.ActionId, e.LeadId, e.Id, e.ActivityDate, e.ActivityNote });

            var results = from o in pagedAndFilteredResult
                          select new GetViewApplicationTrackerDto()
                          {
                              Id = o.Id,
                              LeadId = o.LeadFk.Id,
                              JobNumber = o.JobNumber,
                              JobType = o.JobTypeFk.Name,
                              JobStatus = o.JobStatusFk.Name,
                              JobStatusColorClass = o.JobStatusFk.ColorClass,
                              CompanyName = o.LeadFk.CompanyName,
                              Address = o.Address,
                              Suburb = o.Suburb,
                              State = o.State,
                              PostalCode = o.PostalCode,
                              ElecDistributor = o.ElecDistributorFk.Name,
                              DistApplied = o.DistApplied.Value.Date,
                              DistApproveDate = o.DistApproveDate,
                              DistExpiryDate = o.DistExpiryDate,
                              ApplicationRefNo = o.ApplicationRefNo,
                              SmsSend = o.SmsSend,
                              SmsSendDate = o.SmsSendDate,
                              EmailSend = o.EmailSend,
                              EmailSendDate = o.EmailSendDate,
                              CurruntLeadOwner = User_List.Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                              NextFollowup = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                              Discription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              Comment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                              Summary = summary,
                              IsExpired = (o.ApplicationRefNo != null && (o.DistExpiryDate.Value.AddDays(-30) < Today) && o.JobStatusId < 6) ? true : false
                          };

            var totalCount = await filter.CountAsync();

            return new PagedResultDto<GetViewApplicationTrackerDto>(totalCount, await results.ToListAsync());
        }

        public async Task<PagedResultDto<GetViewApplicationTrackerDto>> GetAllJobEssentialTracker(GetAllJobApplicationTrackerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var Today = _timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId);

            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var filteredResult = _jobsRepository.GetAll()
                              .Include(e => e.LeadFk)
                              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                              .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.JobStatusId != 3 && e.SolarRebateStatus != 3 && e.FirstDepositDate != null && e.ElecDistributorFk.Name == "Essential")
                              .AsNoTracking()
                              .Select(e => new { e.Id, e.JobNumber, e.LeadFk, e.State, e.ElecDistributorId, e.JobStatusId, e.JobTypeId, e.DepositeRecceivedDate, e.EssentialApprovalRef,e.EssentialDistApplied, e.DistApplied, e.ActiveDate, e.ApplicationRefNo, e.MeterPhaseId, e.NMINumber, e.PeakMeterNo, e.DistExpiryDate, e.JobTypeFk, e.JobStatusFk, e.Address, e.Suburb, e.PostalCode, e.ElecDistributorFk, e.DistApproveDate, e.SmsSend, e.SmsSendDate, e.EmailSend, e.EmailSendDate, e.LeadId });

            

            var filter = filteredResult
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(input.FilterName == "ApplicationRefNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ApplicationRefNo == input.Filter)
                        .WhereIf(input.FilterName == "NMINumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.NMINumber == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                        .WhereIf(input.ElecDistributorId != 0, e => e.ElecDistributorId == input.ElecDistributorId)
                        .WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => input.JobStatusIDFilter.Contains((int)e.JobStatusId))
                        .WhereIf(input.JobTypeId != 0, e => e.JobTypeId == input.JobTypeId)

                        .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ApplicationDate" && input.StartDate != null, e => e.DistApplied.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ApplicationDate" && input.EndDate != null, e => e.DistApplied.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ExpiryDate" && input.StartDate != null, e => e.DistApplied.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ExpiryDate" && input.EndDate != null, e => e.DistApplied.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ActiveDate" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ActiveDate" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.ApplicationStatus == 1, e => e.EssentialApprovalRef == null
                        && (_documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() == false
                        || !_documentRepository.GetAll().Where(d => d.DocumentTypeId == 2 && d.JobId == e.Id).Any()  // ElectricityBill
                        || !_documentRepository.GetAll().Where(d => d.DocumentTypeId == 7 && d.JobId == e.Id).Any()  // ElectricPoleImg
                        || !_documentRepository.GetAll().Where(d => d.DocumentTypeId == 3 && d.JobId == e.Id).Any()  // MeterBox
                        || !_documentRepository.GetAll().Where(d => d.DocumentTypeId == 19 && d.JobId == e.Id).Any())) // EssentialGreenBox               //Application Awaiting


                        .WhereIf(input.ApplicationStatus == 2, e => e.EssentialApprovalRef == null
                        && _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any()
                && _documentRepository.GetAll().Where(d => d.DocumentTypeId == 2 && d.JobId == e.Id).Any()   // ElectricityBill
                && _documentRepository.GetAll().Where(d => d.DocumentTypeId == 7 && d.JobId == e.Id).Any()   // ElectricPoleImg
                && _documentRepository.GetAll().Where(d => d.DocumentTypeId == 3 && d.JobId == e.Id).Any()   // MeterBox
                && _documentRepository.GetAll().Where(d => d.DocumentTypeId == 19 && d.JobId == e.Id).Any()) // EssentialGreenBo   //Application Pending


                        .WhereIf(input.ApplicationStatus == 3, e => e.EssentialApprovalRef != null);//Application Complete

            #region Summary Count
            ApplicationSummaryCountDto summary = new ApplicationSummaryCountDto();
            summary.Total = await filter.CountAsync();

            summary.Awaiting = await filter.Where(e => e.EssentialApprovalRef == null
             && (_documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() == false
             || !_documentRepository.GetAll().Where(d => d.DocumentTypeId == 2 && d.JobId == e.Id).Any()  // ElectricityBill
              || !_documentRepository.GetAll().Where(d => d.DocumentTypeId == 7 && d.JobId == e.Id).Any()  // ElectricPoleImg
              || !_documentRepository.GetAll().Where(d => d.DocumentTypeId == 3 && d.JobId == e.Id).Any()  // MeterBox
              || !_documentRepository.GetAll().Where(d => d.DocumentTypeId == 19 && d.JobId == e.Id).Any() // EssentialGreenBox
                      )).CountAsync();

            summary.Pending = await filter.Where(e => e.EssentialApprovalRef == null
                && _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any()
                && _documentRepository.GetAll().Where(d => d.DocumentTypeId == 2 && d.JobId == e.Id).Any()   // ElectricityBill
                && _documentRepository.GetAll().Where(d => d.DocumentTypeId == 7 && d.JobId == e.Id).Any()   // ElectricPoleImg
                && _documentRepository.GetAll().Where(d => d.DocumentTypeId == 3 && d.JobId == e.Id).Any()   // MeterBox
                && _documentRepository.GetAll().Where(d => d.DocumentTypeId == 19 && d.JobId == e.Id).Any()  // EssentialGreenBox
                ).CountAsync();
            summary.Complete = await filter.Where(e => e.EssentialApprovalRef != null).CountAsync();


            #endregion


            var pagedAndFilteredResult = filter
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var activity = _leadactivityRepository.GetAll().AsNoTracking().Where(e => e.SectionId == 1).Select(e => new { e.ActionId, e.LeadId, e.Id, e.ActivityDate, e.ActivityNote });

            var results = from o in pagedAndFilteredResult
                          select new GetViewApplicationTrackerDto()
                          {
                              Id = o.Id,
                              JobNumber = o.JobNumber,
                              LeadId = o.LeadFk.Id,
                              Email = o.LeadFk.Email,
                              Mobile = o.LeadFk.Mobile,
                              Address = o.Address,
                              CompanyName = o.LeadFk.CompanyName,
                              EssentialApprovalRef=o.EssentialApprovalRef,
                              EssentialDistApplied=o.EssentialDistApplied,
                              Summary = summary

                          };

            var totalCount = await filter.CountAsync();

            return new PagedResultDto<GetViewApplicationTrackerDto>(totalCount, await results.ToListAsync());
        }


        public async Task<PagedResultDto<GetViewFreebiesTrackerDto>> GetAllFreebiesTracker(GetAllFreebiesTrackerInput input)
        {
            #region Old Code
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var filteredResult = _jobPromotionRepository.GetAll()
            //                  .Include(e => e.JobFk)
            //                  .Include(e => e.PromotionMasterFk)
            //                  .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
            //                  .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
            //                  .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
            //                  .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit && e.JobFk.JobStatusId != 3 && e.JobFk.JobStatusId != 2 && e.JobFk.SolarRebateStatus != 3 && e.PromotionMasterId != null);

            //#region Summary Count
            //FreebiesSummaryCountDto summary = new FreebiesSummaryCountDto();
            //summary.Total = filteredResult.Count();

            //summary.Awaiting = filteredResult.Where(e => e.DispatchedDate == null
            //&& (e.JobFk.TotalCost - (_invoiceImportDataRepository.GetAll().Where(o => o.JobId == e.JobId).Select(e => e.PaidAmmount).Any() ? (_invoiceImportDataRepository.GetAll().Where(o => o.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) : 0) > 0)).Count();

            //summary.Pending = filteredResult.Where(e => e.DispatchedDate == null &&
            //(e.JobFk.TotalCost - (_invoiceImportDataRepository.GetAll().Where(o => o.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) <= 0)).Count();

            //summary.Sent = filteredResult.Where(e => e.DispatchedDate != null).Count();
            //#endregion

            //var filter = filteredResult
            //            .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.JobNumber.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Mobile.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Email.Contains(input.Filter))
            //            .WhereIf(input.PromoType != 0, e => e.PromotionMasterId == input.PromoType)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.JobFk.State == input.State)
            //            .WhereIf(input.TransportType != 0, e => e.FreebieTransportId == input.TransportType)

            //            .WhereIf(input.ApplicationStatus == 1, e => e.DispatchedDate == null
            //            && (e.JobFk.TotalCost - (_invoiceImportDataRepository.GetAll().Where(o => o.JobId == e.JobId).Select(e => e.PaidAmmount).Any() ? (_invoiceImportDataRepository.GetAll().Where(o => o.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) : 0) > 0)) // Awaiting

            //            .WhereIf(input.ApplicationStatus == 2, e => e.DispatchedDate == null &&
            //            (e.JobFk.TotalCost - (_invoiceImportDataRepository.GetAll().Where(o => o.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) <= 0)) // Pending

            //            .WhereIf(input.ApplicationStatus == 3, e => e.DispatchedDate != null) // Sent
            //            .WhereIf(input.ApplicationStatus == 3 && input.StartDate != null, e => e.DispatchedDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.ApplicationStatus == 3 && input.EndDate != null, e => e.DispatchedDate.Value.Date <= EDate.Value.Date)
            //            ;

            //var pagedAndFilteredResult = filter
            //    .OrderBy(input.Sorting ?? "id desc")
            //    .PageBy(input);

            //var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 2 && e.LeadFk.OrganizationId == input.OrganizationUnit);

            //var results = from o in pagedAndFilteredResult
            //              select new GetViewFreebiesTrackerDto()
            //              {
            //                  Id = o.Id,
            //                  JobId = o.JobId,
            //                  LeadId = (int)o.JobFk.LeadId,
            //                  JobNumber = o.JobFk.JobNumber,
            //                  PromotionName = o.PromotionMasterFk.Name,
            //                  CompanyName = o.JobFk.LeadFk.CompanyName,
            //                  Mobile = o.JobFk.LeadFk.Mobile,
            //                  Address = o.JobFk.Address,
            //                  Suburb = o.JobFk.Suburb,
            //                  State = o.JobFk.State,
            //                  PostalCode = o.JobFk.PostalCode,
            //                  Email = o.JobFk.LeadFk.Email,
            //                  JobStatus = o.JobFk.JobStatusFk.Name,
            //                  JobStatusColorClass = o.JobFk.JobStatusFk.ColorClass,
            //                  TotalCost = o.JobFk.TotalCost,
            //                  BalOwing = (_invoiceImportDataRepository.GetAll().Where(e => e.JobId == o.JobId).Select(e => e.PaidAmmount).Sum()),
            //                  TransportName = o.FreebieTransportIdFk.Name,
            //                  TrackingNumber = o.TrackingNumber,
            //                  CurruntLeadOwner = _userRepository.GetAll().Where(e => e.Id == o.JobFk.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
            //                  NextFollowup = activity.Where(e => e.ActionId == 8 && e.LeadId == o.JobFk.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
            //                  Discription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.JobFk.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
            //                  Comment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.JobFk.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
            //                  SmsSend = o.SmsSend,
            //                  SmsSendDate = o.SmsSendDate,
            //                  EmailSend = o.SmsSend,
            //                  EmailSendDate = o.EmailSendDate,

            //                  Summary = summary
            //              };

            //var totalCount = await filter.CountAsync();

            //return new PagedResultDto<GetViewFreebiesTrackerDto>(totalCount, await results.ToListAsync());
            #endregion

            #region Old Code
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var filteredResult = _jobPromotionRepository.GetAll()
                              .Include(e => e.JobFk)
                              .Include(e => e.JobFk.LeadFk)
                              .Include(e => e.PromotionMasterFk)
                              .Include(e => e.JobFk.JobStatusFk)
                              .Include(e => e.FreebieTransportIdFk)
                              .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                              .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit && e.JobFk.JobStatusId != 3 && e.JobFk.JobStatusId != 2 && e.JobFk.SolarRebateStatus != 3 && e.PromotionMasterId != null)
                              .AsNoTracking()
                              .Select(e => new { e.Id, e.JobFk, e.JobId, e.DispatchedDate, e.PromotionMasterId, e.FreebieTransportId, e.PromotionMasterFk, e.FreebieTransportIdFk, e.TrackingNumber, e.SmsSend, e.SmsSendDate, e.EmailSend, e.EmailSendDate, e.JobFk.LeadFk.AssignToUserID, e.JobFk.LeadId, e.SerialNo });

            var invoiceInportDate = _invoiceImportDataRepository.GetAll().AsNoTracking().Select(e => new { e.JobId, e.PaidAmmount });

            #region Summary Count
            FreebiesSummaryCountDto summary = new FreebiesSummaryCountDto();
            summary.Total = await filteredResult.CountAsync();

            summary.Awaiting = await filteredResult.Where(e => e.DispatchedDate == null
            && (e.JobFk.TotalCost - (invoiceInportDate.Where(o => o.JobId == e.JobId).Select(e => e.PaidAmmount).Any() ? (invoiceInportDate.Where(o => o.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) : 0) > 0)).CountAsync();

            summary.Pending = await filteredResult.Where(e => e.DispatchedDate == null &&
            (e.JobFk.TotalCost - (invoiceInportDate.Where(o => o.JobId == e.JobId).AsNoTracking().Select(e => e.PaidAmmount).Sum()) <= 0)).CountAsync();

            summary.Sent = await filteredResult.Where(e => e.DispatchedDate != null).CountAsync();
            #endregion

            var filter = filteredResult
                        .Where(e => e.AssignToUserID != null)
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Email == input.Filter)
                        .WhereIf(input.FilterName == "SerialNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.SerialNo == input.Filter)
                        .WhereIf(input.FilterName == "TrackingNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.TrackingNumber == input.Filter)
                        .WhereIf(input.PromoType != 0, e => e.PromotionMasterId == input.PromoType)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.JobFk.State == input.State)
                        .WhereIf(input.TransportType != 0, e => e.FreebieTransportId == input.TransportType)

                        .WhereIf(input.ApplicationStatus == 1, e => e.DispatchedDate == null
                        && (e.JobFk.TotalCost - (invoiceInportDate.Where(o => o.JobId == e.JobId).Select(e => e.PaidAmmount).Any() ? (invoiceInportDate.Where(o => o.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) : 0) > 0)) // Awaiting

                        .WhereIf(input.ApplicationStatus == 2, e => e.DispatchedDate == null &&
                        (e.JobFk.TotalCost - (invoiceInportDate.Where(o => o.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) <= 0)) // Pending

                        .WhereIf(input.ApplicationStatus == 3, e => e.DispatchedDate != null) // Sent
                        .WhereIf(input.ApplicationStatus == 3 && input.StartDate != null, e => e.DispatchedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.ApplicationStatus == 3 && input.EndDate != null, e => e.DispatchedDate.Value.Date <= EDate.Value.Date)
                        ;

            var pagedAndFilteredResult = filter
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 2 && e.LeadFk.OrganizationId == input.OrganizationUnit).AsNoTracking().Select(e => new { e.ActionId, e.LeadId, e.Id, e.ActivityDate, e.ActivityNote });

            var results = from o in pagedAndFilteredResult
                          select new GetViewFreebiesTrackerDto()
                          {
                              Id = o.Id,
                              JobId = o.JobId,
                              LeadId = (int)o.JobFk.LeadId,
                              JobNumber = o.JobFk.JobNumber,
                              PromotionName = o.PromotionMasterFk.Name,
                              CompanyName = o.JobFk.LeadFk.CompanyName,
                              Mobile = o.JobFk.LeadFk.Mobile,
                              Address = o.JobFk.Address,
                              Suburb = o.JobFk.Suburb,
                              State = o.JobFk.State,
                              PostalCode = o.JobFk.PostalCode,
                              Email = o.JobFk.LeadFk.Email,
                              JobStatus = o.JobFk.JobStatusFk.Name,
                              JobStatusColorClass = o.JobFk.JobStatusFk.ColorClass,
                              TotalCost = o.JobFk.TotalCost,
                              BalOwing = invoiceInportDate.Where(e => e.JobId == o.JobId).Select(e => e.PaidAmmount).Sum(),
                              TransportName = o.FreebieTransportIdFk.Name,
                              TrackingNumber = o.TrackingNumber,
                              //CurruntLeadOwner = User_List.Where(e => e.Id == o.JobFk.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                              CurruntLeadOwner = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                              NextFollowup = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                              Discription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              Comment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              SmsSend = o.SmsSend,
                              SmsSendDate = o.SmsSendDate,
                              EmailSend = o.EmailSend,
                              EmailSendDate = o.EmailSendDate,

                              Summary = summary
                          };

            var totalCount = await filter.CountAsync();

            return new PagedResultDto<GetViewFreebiesTrackerDto>(totalCount, await results.ToListAsync());
            #endregion
        }

        public async Task<PagedResultDto<GetViewFinanceTrackerDto>> GetAllFinanceTracker(GetAllFinanceTrackerInput input)
        {
            #region Old Code
            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var filteredResult = _jobsRepository.GetAll()
            //                  .Include(e => e.LeadFk)
            //                  .Include(e => e.JobStatusFk)
            //                  .Include(e => e.FinanceOptionFk)
            //                  .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            //                  .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            //                  .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            //                  .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.PaymentOptionId != 1 && e.FirstDepositDate != null);

            //#region Summary Count
            //FinanceSummaryCountDto summary = new FinanceSummaryCountDto();
            //summary.Total = filteredResult.Count();

            //summary.Verified = filteredResult.Where(e => e.FinanceDocumentVerified == 1).Count();

            //summary.NotVerified = filteredResult.Where(e => e.FinanceDocumentVerified == 2 || e.FinanceDocumentVerified == null || e.FinanceDocumentVerified == 0).Count();

            //summary.Query = filteredResult.Where(e => e.FinanceDocumentVerified == 3).Count();

            //#endregion

            //var filter = filteredResult
            //            //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.FinancePurchaseNo.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "FinancePurchaseNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.FinancePurchaseNo.Contains(input.Filter))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
            //            .WhereIf(input.PaymentOption != 0, e => e.PaymentOptionId == input.PaymentOption)

            //            .WhereIf(input.IsVerified == 1, e => e.FinanceDocumentVerified == 1) // Verified
            //            .WhereIf(input.IsVerified == 2, e => e.FinanceDocumentVerified == 2 || e.FinanceDocumentVerified == null || e.FinanceDocumentVerified == 0) //Not Verified
            //            .WhereIf(input.IsVerified == 3, e => e.FinanceDocumentVerified == 3) // Query
            //            ;

            //var pagedAndFilteredResult = filter
            //    .OrderBy(input.Sorting ?? "id desc")
            //    .PageBy(input);

            //var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 3 && e.LeadFk.OrganizationId == input.OrganizationUnit);

            //var results = from o in pagedAndFilteredResult
            //              select new GetViewFinanceTrackerDto()
            //              {
            //                  Id = o.Id,
            //                  LeadId = o.LeadFk.Id,
            //                  JobNumber = o.JobNumber,
            //                  FinancePurchaseNo = o.FinancePurchaseNo,
            //                  FinanceDocumentVerified = o.FinanceDocumentVerified == null ? 2 : (int)o.FinanceDocumentVerified,
            //                  JobStatus = o.JobStatusFk.Name,
            //                  JobStatusColorClass = o.JobStatusFk.ColorClass,
            //                  CompanyName = o.LeadFk.CompanyName,
            //                  Mobile = o.LeadFk.Mobile,
            //                  Email = o.LeadFk.Email,
            //                  Address = o.Address,
            //                  Suburb = o.Suburb,
            //                  State = o.State,
            //                  PostalCode = o.PostalCode,
            //                  FinanceSmsSend = o.FinanceSmsSend,
            //                  FinanceSmsSendDate = o.FinanceSmsSendDate,
            //                  FinanceEmailSend = o.FinanceEmailSend,
            //                  FinanceEmailSendDate = o.FinanceEmailSendDate,
            //                  CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
            //                  ActivityReminderTime = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
            //                  ActivityDescription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
            //                  ActivityComment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

            //                  Summary = summary
            //              };

            //var totalCount = await filter.CountAsync();

            //return new PagedResultDto<GetViewFinanceTrackerDto>(totalCount, await results.ToListAsync()
            //);
            #endregion

            #region New Code
            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var filteredResult = _jobsRepository.GetAll()
                              .Include(e => e.LeadFk)
                              .Include(e => e.JobStatusFk)
                              .Include(e => e.FinanceOptionFk)
                              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                              .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.PaymentOptionId != 1 && e.FirstDepositDate != null)
                              .AsNoTracking()
                              .Select(e => new { e.Id, e.LeadFk, e.JobNumber, e.FinancePurchaseNo, e.PaymentOptionId, e.FinanceDocumentVerified, e.State, e.JobStatusFk, e.Address, e.Suburb, e.PostalCode, e.FinanceSmsSend, e.FinanceSmsSendDate, e.FinanceEmailSend, e.FinanceEmailSendDate, e.LeadId });

            #region Summary Count
            FinanceSummaryCountDto summary = new FinanceSummaryCountDto();
            summary.Total = await filteredResult.CountAsync();

            summary.Verified = await filteredResult.Where(e => e.FinanceDocumentVerified == 1).CountAsync();

            summary.NotVerified = await filteredResult.Where(e => (e.FinanceDocumentVerified == 2 || e.FinanceDocumentVerified == null || e.FinanceDocumentVerified == 0) && !string.IsNullOrEmpty(e.FinancePurchaseNo)).CountAsync();

            summary.Awaiting = await filteredResult.Where(e => (e.FinanceDocumentVerified == 2 || e.FinanceDocumentVerified == null || e.FinanceDocumentVerified == 0) && string.IsNullOrEmpty(e.FinancePurchaseNo)).CountAsync();

            summary.Query = await filteredResult.Where(e => e.FinanceDocumentVerified == 3).CountAsync();

            #endregion

            var filter = filteredResult
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(input.FilterName == "FinancePurchaseNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.FinancePurchaseNo == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                        .WhereIf(input.PaymentOption != 0, e => e.PaymentOptionId == input.PaymentOption)

                        .WhereIf(input.IsVerified == 1, e => e.FinanceDocumentVerified == 1) // Verified
                        .WhereIf(input.IsVerified == 2, e => (e.FinanceDocumentVerified == 2 || e.FinanceDocumentVerified == null || e.FinanceDocumentVerified == 0) && !string.IsNullOrEmpty(e.FinancePurchaseNo)) //Not Verified
                        .WhereIf(input.IsVerified == 3, e => e.FinanceDocumentVerified == 3) // Query
                        .WhereIf(input.IsVerified == 4, e => (e.FinanceDocumentVerified == 2 || e.FinanceDocumentVerified == null || e.FinanceDocumentVerified == 0) && string.IsNullOrEmpty(e.FinancePurchaseNo)) // Awaiting
                        ;

            var pagedAndFilteredResult = filter
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 3 && e.LeadFk.OrganizationId == input.OrganizationUnit).AsNoTracking().Select(e => new { e.Id, e.ActionId, e.LeadId, e.ActivityDate, e.ActivityNote });

            var results = from o in pagedAndFilteredResult
                          select new GetViewFinanceTrackerDto()
                          {
                              Id = o.Id,
                              LeadId = o.LeadFk.Id,
                              JobNumber = o.JobNumber,
                              FinancePurchaseNo = o.FinancePurchaseNo,
                              FinanceDocumentVerified = o.FinanceDocumentVerified == null ? 2 : (int)o.FinanceDocumentVerified,
                              JobStatus = o.JobStatusFk.Name,
                              JobStatusColorClass = o.JobStatusFk.ColorClass,
                              CompanyName = o.LeadFk.CompanyName,
                              Mobile = o.LeadFk.Mobile,
                              Email = o.LeadFk.Email,
                              Address = o.Address,
                              Suburb = o.Suburb,
                              State = o.State,
                              PostalCode = o.PostalCode,
                              FinanceSmsSend = o.FinanceSmsSend,
                              FinanceSmsSendDate = o.FinanceSmsSendDate,
                              FinanceEmailSend = o.FinanceEmailSend,
                              FinanceEmailSendDate = o.FinanceEmailSendDate,
                              CurrentLeadOwaner = User_List.Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                              ActivityReminderTime = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                              ActivityDescription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              ActivityComment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                              Summary = summary
                          };

            var totalCount = await filter.CountAsync();

            return new PagedResultDto<GetViewFinanceTrackerDto>(totalCount, await results.ToListAsync()
            );
            #endregion
        }

        public async Task<PagedResultDto<GetViewRefundTrackerDto>> GetAllRefundTracker(GetAllRefundTrackerInput input)
        {
            #region Old Code
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var filteredResult = _jobRefundRepository.GetAll()
            //                  .Include(e => e.RefundReasonFk)
            //                  .Include(e => e.JobFk)
            //                  .Include(e => e.PaymentOptionFk)
            //                  .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
            //                  .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
            //                  .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
            //                  .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit && e.JobFk.FirstDepositDate != null);

            //#region Summary Count
            //RefundSummaryCountDto summary = new RefundSummaryCountDto();
            //summary.Total = filteredResult.Count();
            //summary.TotalAmt = filteredResult.Sum(e => e.Amount == null ? 0 : (decimal)e.Amount);

            //summary.Pending = filteredResult.Where(e => e.PaidDate == null).Count();
            //summary.PendingAmt = filteredResult.Where(e => e.PaidDate == null).Sum(e => e.Amount == null ? 0 : (decimal)e.Amount);

            //summary.Refunded = filteredResult.Where(e => e.PaidDate != null).Count();
            //summary.RefundedAmt = filteredResult.Where(e => e.PaidDate != null).Sum(e => e.PaidAmount == null ? 0 : (decimal)e.PaidAmount);
            //#endregion

            //var filter = filteredResult
            //            .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.JobNumber.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Mobile.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Email.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Phone.Contains(input.Filter))
            //            .WhereIf(input.SalesRep != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesRep)
            //            .WhereIf(input.JobStatus != null && input.JobStatus.Count() > 0, e => input.JobStatus.Contains((int)e.JobFk.JobStatusId))
            //            .WhereIf(input.RefundReason != 0, e => e.RefundReasonId == input.RefundReason)

            //            .WhereIf(input.IsVerify != null, e => e.IsVerify == input.IsVerify)

            //            .WhereIf(input.DateFilter == "Receive" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
            //            .WhereIf(input.DateFilter == "Receive" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

            //            .WhereIf(input.DateFilter == "Complete" && input.StartDate != null, e => e.PaidDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.DateFilter == "Complete" && input.EndDate != null, e => e.PaidDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.ApplicationStatus == 1, e => e.PaidDate != null) // Refunded
            //            .WhereIf(input.ApplicationStatus == 2, e => e.PaidDate == null) //Pending
            //            ;

            //var pagedAndFilteredResult = filter
            //    .OrderBy(input.Sorting ?? "id desc")
            //    .PageBy(input);

            //var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 4 && e.LeadFk.OrganizationId == input.OrganizationUnit);

            //var results = from o in pagedAndFilteredResult
            //              join o6 in _invoicePaymentMethodRepository.GetAll() on o.PaymentOptionId equals o6.Id into j6
            //              from o6 in j6.DefaultIfEmpty()

            //              select new GetViewRefundTrackerDto()
            //              {
            //                  Id = o.Id,
            //                  JobId = o.JobFk.Id,
            //                  LeadId = o.JobFk.LeadFk.Id,
            //                  JobNumber = o.JobFk.JobNumber,
            //                  CompanyName = o.JobFk.LeadFk.CompanyName,
            //                  CreationTime = o.CreationTime,
            //                  Amount = o.Amount,
            //                  PaidAmount = o.PaidAmount,
            //                  BSBNo = o.BSBNo,
            //                  AccountNo = o.AccountNo,
            //                  CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == o.JobFk.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
            //                  RefundReasonName = o.RefundReasonFk.Name,
            //                  PaymentOptionName = o6.PaymentMethod,
            //                  PaidDate = o.PaidDate,
            //                  PreviousProjectStatus = _jobStatusRepository.GetAll().Where(e => e.Id == _previousJobStatusRepository.GetAll().Where(e => e.JobId == o.JobId).OrderByDescending(e => e.Id).Select(e => e.PreviousId).FirstOrDefault()).Select(e => e.Name).FirstOrDefault(),
            //                  ProjectStatus = o.JobFk.JobStatusFk.Name,
            //                  JobStatusColorClass = o.JobFk.JobStatusFk.ColorClass,
            //                  JobRefundSmsSend = o.JobRefundSmsSend,
            //                  JobRefundSmsSendDate = o.JobRefundSmsSendDate,
            //                  JobRefundEmailSend = o.JobRefundEmailSend,
            //                  JobRefundEmailSendDate = o.JobRefundEmailSendDate,
            //                  RequestedBy = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),

            //                  Verifyornot = o.PaidDate != null ? true : o.IsVerify == true ? true : false,

            //                  ActivityReminderTime = activity.Where(e => e.SectionId == 4 && e.ActionId == 8 && e.LeadId == o.JobFk.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
            //                  ActivityDescription = activity.Where(e => e.SectionId == 4 && e.ActionId == 8 && e.LeadId == o.JobFk.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
            //                  ActivityComment = activity.Where(e => e.SectionId == 4 && e.ActionId == 24 && e.LeadId == o.JobFk.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

            //                  Summary = summary
            //              };

            //var totalCount = await filter.CountAsync();

            //return new PagedResultDto<GetViewRefundTrackerDto>(totalCount, await results.ToListAsync()
            //);
            #endregion

            #region New Code
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var filteredResult = _jobRefundRepository.GetAll()
                              .Include(e => e.RefundReasonFk)
                              .Include(e => e.JobFk)
                              .Include(e => e.PaymentOptionFk)
                              .Include(e => e.JobFk.LeadFk)
                              .Include(e => e.JobFk.JobStatusFk)
                              .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                              .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit && e.JobFk.FirstDepositDate != null)
                              .AsNoTracking()
                              .Select(e => new { e.Id, e.JobFk, e.JobId, e.RefundReasonId, e.IsVerify, e.CreationTime, e.PaidDate, e.PaymentOptionId, e.Amount, e.PaidAmount, e.BSBNo, e.AccountNo, e.RefundReasonFk, e.JobRefundSmsSend, e.JobRefundSmsSendDate, e.JobRefundEmailSend, e.JobRefundEmailSendDate, e.CreatorUserId, e.JobFk.LeadId, e.JobFk.LeadFk.AssignToUserID, e.Notes, e.RefundPaymentType,e.LastModificationTime });



            var filter = filteredResult
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Email == input.Filter)
                        .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Phone == input.Filter)
                        .WhereIf(!string.IsNullOrEmpty(input.RefundPaymentType), e => e.RefundPaymentType == input.RefundPaymentType)
                        .WhereIf(input.SalesRep != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesRep)
                        .WhereIf(input.JobStatus != null && input.JobStatus.Count() > 0, e => input.JobStatus.Contains((int)e.JobFk.JobStatusId))
                        .WhereIf(input.RefundReason != 0, e => e.RefundReasonId == input.RefundReason)

                        .WhereIf(input.IsVerify != null, e => e.IsVerify == input.IsVerify)

                        .WhereIf(input.DateFilter == "Receive" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.DateFilter == "Receive" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.DateFilter == "Complete" && input.StartDate != null, e => e.PaidDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateFilter == "Complete" && input.EndDate != null, e => e.PaidDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.ApplicationStatus == 1, e => e.PaidDate != null) // Refunded
                        .WhereIf(input.ApplicationStatus == 2, e => e.PaidDate == null) //Pending


                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.JobFk.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.JobFk.LeadFk.Area == input.AreaNameFilter)
                        ;

            #region Summary Count
            RefundSummaryCountDto summary = new RefundSummaryCountDto();
            summary.Total = await filter.CountAsync();
            summary.TotalAmt = await filter.SumAsync(e => e.Amount == null ? 0 : (decimal)e.Amount);

            summary.Pending = await filter.Where(e => e.PaidDate == null).CountAsync();
            summary.PendingAmt = await filter.Where(e => e.PaidDate == null).SumAsync(e => e.Amount == null ? 0 : (decimal)e.Amount);

            summary.Refunded = await filter.Where(e => e.PaidDate != null).CountAsync();
            summary.RefundedAmt = await filter.Where(e => e.PaidDate != null).SumAsync(e => e.PaidAmount == null ? 0 : (decimal)e.PaidAmount);
            #endregion

            var pagedAndFilteredResult = filter
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 4 && e.LeadFk.OrganizationId == input.OrganizationUnit).AsNoTracking().Select(e => new { e.SectionId, e.ActionId, e.LeadId, e.Id, e.ActivityDate, e.ActivityNote });

            var TotalAmount = filteredResult
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Email == input.Filter)
                        .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Phone == input.Filter)
                        .Where(e => filter.Any(f => f.JobId == e.JobId))
                        .AsNoTracking().GroupBy(e => e.JobId).Select(e => new { e.Key, amt = e.Sum(s => s.Amount) });

            //var activityIds = _leadactivityRepository.GetAll().Where(e => e.ActionNote == "Job Refund Verify").GroupBy(e => e.LeadId).Select(e => e.Max(m => m.Id));

            //var verifyActivity = _leadactivityRepository.GetAll().Where(e => activityIds.Contains(e.Id)).Select(e => new { e.LeadId, e.CreatorUserId, e.Id});

            var results = from o in pagedAndFilteredResult
                          join o6 in _invoicePaymentMethodRepository.GetAll().AsNoTracking() on o.PaymentOptionId equals o6.Id into j6
                          from o6 in j6.DefaultIfEmpty()

                          let amountReq = TotalAmount.Where(e => e.Key == o.JobId).AsNoTracking().Select(e => e.amt).FirstOrDefault()

                          let veryfyuser = User_List.Where(e => e.Id == _leadactivityRepository.GetAll().Where(e => e.LeadId == o.LeadId &&  e.ActionNote == "Job Refund Verify").
                          OrderByDescending(e => e.Id).AsNoTracking().Select(e => e.CreatorUserId).FirstOrDefault()).AsNoTracking().Select(e => e.FullName).FirstOrDefault()
                          //let veryfyuser = verifyActivity.Where(e => e.LeadId == o.LeadId).Select(e => e.CreatorUserId).FirstOrDefault()

                          select new GetViewRefundTrackerDto()
                          {
                              Id = o.Id,
                              JobId = o.JobFk.Id,
                              LeadId = o.JobFk.LeadFk.Id,
                              JobNumber = o.JobFk.JobNumber,
                              CompanyName = o.JobFk.LeadFk.CompanyName,
                              CreationTime = o.CreationTime,
                              Amount = o.Amount,
                              PaidAmount = o.PaidAmount,
                              BSBNo = o.BSBNo,
                              AccountNo = o.AccountNo,
                              CurrentLeadOwaner = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                              RefundReasonName = o.RefundReasonFk.Name,
                              PaymentOptionName = o6.PaymentMethod,
                              PaidDate = o.PaidDate,
                              PreviousProjectStatus = _jobStatusRepository.GetAll().Where(e => e.Id == _previousJobStatusRepository.GetAll().Where(e => e.JobId == o.JobId).OrderByDescending(e => e.Id).AsNoTracking().Select(e => e.PreviousId).FirstOrDefault()).AsNoTracking().Select(e => e.Name).FirstOrDefault(),
                              ProjectStatus = o.JobFk.JobStatusFk.Name,
                              JobStatusColorClass = o.JobFk.JobStatusFk.ColorClass,
                              JobRefundSmsSend = o.JobRefundSmsSend,
                              JobRefundSmsSendDate = o.JobRefundSmsSendDate,
                              JobRefundEmailSend = o.JobRefundEmailSend,
                              JobRefundEmailSendDate = o.JobRefundEmailSendDate,
                              RequestedBy = User_List.Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),

                              Verifyornot = o.PaidDate != null ? true : o.IsVerify == true ? true : false,

                              ActivityReminderTime = activity.Where(e => e.SectionId == 4 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                              ActivityDescription = activity.Where(e => e.SectionId == 4 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              ActivityComment = activity.Where(e => e.SectionId == 4 && e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                              RefundNotes = o.Notes,
                              PaymentType = o.RefundPaymentType,
                              Summary = summary,
                              VerifyBy = veryfyuser != null && o.IsVerify == true ?   veryfyuser : "",
                              TotalAmountRequested = amountReq,
                              IsSelect = false

                              //VerifyBy = verifyActivity.Where(e => e.LeadId == o.LeadId).Any() ? (User_List.Where(e => e.Id == veryfyuser).Any() ? User_List.Where(e => e.Id == veryfyuser).Select(e => e.FullName).FirstOrDefault() : "") : ""
                          };

            var totalCount = await filter.CountAsync();

            return new PagedResultDto<GetViewRefundTrackerDto>(totalCount, await results.ToListAsync()
            );
            #endregion
        }

        public async Task<PagedResultDto<GetViewReferralTrackerDto>> GetAllReferralTracker(GetAllReferralTrackerInput input)
        {
            #region Old Code
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var filteredResult = _jobsRepository.GetAll()
            //                  .Include(e => e.LeadFk)
            //                  .Include(e => e.JobStatusFk)
            //                  .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            //                  .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            //                  .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            //                  .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.FirstDepositDate != null && e.LeadFk.LeadSourceId == 2);

            //#region Summary Count
            //ReferralSummaryCountDto summary = new ReferralSummaryCountDto();
            //summary.Total = filteredResult.Count();
            ////summary.Awaiting = filteredResult.Where(e => e.ReferralPayment == false && (Convert.ToDecimal(e.TotalCost - ((_invoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(x => x.PaidAmmount).Sum()) == null ? 0 : (_invoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(x => x.PaidAmmount).Sum()))) != 0 || e.LeadFk.ReferralLeadId == null || e.AccountName == null || e.AccountNo == null && e.BsbNo == null || e.ReferralAmount == null)).Count();
            ////summary.ReadyToPay = filteredResult.Where(e => e.ReferralPayment == false && Convert.ToDecimal(e.TotalCost - ((_invoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(x => x.PaidAmmount).Sum()) == null ? 0 : (_invoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(x => x.PaidAmmount).Sum()))) == 0 && e.LeadFk.ReferralLeadId != null && e.AccountName != null && e.AccountNo != null && e.BsbNo != null && e.ReferralAmount != null).Count();

            //summary.Awaiting = filteredResult.Where(e => e.ReferralPayment == false && ((e.TotalCost - (_invoiceImportDataRepository.GetAll().Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Any() ? (_invoiceImportDataRepository.GetAll().Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) : 0) > 0) || e.LeadFk.ReferralLeadId == null || e.AccountName == null || e.AccountNo == null && e.BsbNo == null || e.ReferralAmount == null)).Count();

            //summary.ReadyToPay = filteredResult.Where(e => e.ReferralPayment == false && (e.TotalCost - (_invoiceImportDataRepository.GetAll().Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Any() ? (_invoiceImportDataRepository.GetAll().Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) : 0)) <= 0 && e.LeadFk.ReferralLeadId != null && e.AccountName != null && e.AccountNo != null && e.BsbNo != null && e.ReferralAmount != null).Count();

            //summary.Paid = filteredResult.Where(e => e.ReferralPayment == true).Count();
            //#endregion

            //var filter = filteredResult
            //            //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email.Contains(input.Filter))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
            //            .WhereIf(input.SalesRep != 0, e => e.LeadFk.AssignToUserID == input.SalesRep)

            //            .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.DateType == "ActiveDate" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "ActiveDate" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.DateType == "InstallationDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "InstallationDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.PaymentStatus == 1, e => e.ReferralPayment == false && ((e.TotalCost - (_invoiceImportDataRepository.GetAll().Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Any() ? (_invoiceImportDataRepository.GetAll().Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) : 0) > 0) || e.LeadFk.ReferralLeadId == null || e.AccountName == null || e.AccountNo == null && e.BsbNo == null || e.ReferralAmount == null)) // Awaiting

            //            .WhereIf(input.PaymentStatus == 2, e => e.ReferralPayment == false && (e.TotalCost - (_invoiceImportDataRepository.GetAll().Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Any() ? (_invoiceImportDataRepository.GetAll().Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) : 0)) <= 0 && e.LeadFk.ReferralLeadId != null && e.AccountName != null && e.AccountNo != null && e.BsbNo != null && e.ReferralAmount != null) // ReadyToPay

            //            .WhereIf(input.PaymentStatus == 3, e => e.ReferralPayment == true) // Paid
            //            ;

            //var pagedAndFilteredResult = filter
            //    .OrderBy(input.Sorting ?? "id desc")
            //    .PageBy(input);

            //var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 16 && e.LeadFk.OrganizationId == input.OrganizationUnit);

            //var results = from o in pagedAndFilteredResult
            //              let refLeadId = o.LeadFk.ReferralLeadId == null ? 0 : o.LeadFk.ReferralLeadId
            //              let refJobId = o.LeadFk.ReferralLeadId == null ? 0 : o.LeadFk.ReferralLeadId

            //              select new GetViewReferralTrackerDto()
            //              {
            //                  Id = o.Id,
            //                  LeadId = o.LeadFk.Id,
            //                  JobNumber = o.JobNumber,
            //                  CompanyName = o.LeadFk.CompanyName,
            //                  JobStatus = o.JobStatusFk.Name,
            //                  JobStatusColorClass = o.JobStatusFk.ColorClass,
            //                  InstallComplate = o.InstalledcompleteDate,
            //                  TotalCost = o.TotalCost,
            //                  OwningAmt = Convert.ToDecimal(o.TotalCost - ((_invoiceImportDataRepository.GetAll().Where(x => x.JobId == o.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_invoiceImportDataRepository.GetAll().Where(x => x.JobId == o.Id).Select(e => e.PaidAmmount).Sum()))),
            //                  RefferedProjectName = _jobsRepository.GetAll().Where(e => e.LeadId == refLeadId).Select(e => e.JobNumber).FirstOrDefault(),
            //                  RefferedProjectStatus = _jobsRepository.GetAll().Where(e => e.LeadId == refLeadId).Select(e => e.JobStatusFk.Name).FirstOrDefault(),
            //                  RefferedProjectStatusColorClass = _jobsRepository.GetAll().Where(e => e.LeadId == refLeadId).Select(e => e.JobStatusFk.ColorClass).FirstOrDefault(),
            //                  RefferedTotalCost = _jobsRepository.GetAll().Where(e => e.LeadId == refLeadId).Select(e => e.TotalCost).FirstOrDefault(),
            //                  RefferedowningAmt = Convert.ToDecimal(_jobsRepository.GetAll().Where(e => e.LeadId == refLeadId).Select(e => e.TotalCost).FirstOrDefault() - ((_invoiceImportDataRepository.GetAll().Where(x => x.JobId == _jobsRepository.GetAll().Where(e => e.LeadId == refLeadId).Select(e => e.Id).FirstOrDefault()).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_invoiceImportDataRepository.GetAll().Where(x => x.JobId == _jobsRepository.GetAll().Where(e => e.LeadId == refLeadId).Select(e => e.Id).FirstOrDefault()).Select(e => e.PaidAmmount).Sum()))),
            //                  AccountName = o.AccountName,
            //                  BSBNo = o.BsbNo,
            //                  AccountNo = o.AccountNo,
            //                  ActivityReminderTime = activity.Where(e => e.SectionId == 4 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
            //                  ActivityDescription = activity.Where(e => e.SectionId == 4 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
            //                  ActivityComment = activity.Where(e => e.SectionId == 4 && e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

            //                  ReferralAmount = o.ReferralAmount,
            //                  ReferralPayment = o.ReferralPayment,
            //                  IsRefferalVerify = o.IsRefferalVerify,

            //                  Isverifyornot = o.ReferralPayment == false && o.BsbNo != null && o.AccountNo != null && o.TotalCost == ((_invoiceImportDataRepository.GetAll().Where(x => x.JobId == o.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_invoiceImportDataRepository.GetAll().Where(x => x.JobId == o.Id).Select(e => e.PaidAmmount).Sum())) ? true : false,

            //                  Summary = summary
            //              };

            //var totalCount = await filter.CountAsync();

            //return new PagedResultDto<GetViewReferralTrackerDto>(totalCount, await results.ToListAsync());
            #endregion

            #region New Code
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var filteredResult = _jobsRepository.GetAll()
                              .Include(e => e.LeadFk)
                              .Include(e => e.JobStatusFk)
                              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                              .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.FirstDepositDate != null && e.LeadFk.LeadSourceId == 2)
                              .AsNoTracking()
                              .Select(e => new { e.Id, e.LeadId, e.LeadFk, e.JobNumber, e.JobStatusFk, e.State,e.CreationTime, e.TotalCost, e.ReferralPayment, e.AccountName, e.AccountNo, e.BsbNo, e.ReferralAmount, e.DepositeRecceivedDate, e.ActiveDate, e.InstallationDate, e.InstalledcompleteDate, e.IsRefferalVerify, e.ReferralPayDate, e.ReferralRemark, e.BankReferenceNo });

            var invoiceInportData = _invoiceImportDataRepository.GetAll().AsNoTracking().Select(e => new { e.JobId, e.PaidAmmount });

            #region Summary Count
            ReferralSummaryCountDto summary = new ReferralSummaryCountDto();
            summary.Total = await filteredResult.CountAsync();

            summary.Awaiting = await filteredResult.Where(e => e.ReferralPayment == false && ((e.TotalCost - (invoiceInportData.Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Any() ? (invoiceInportData.Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) : 0) > 0) || e.LeadFk.ReferralLeadId == null || e.AccountName == null || e.AccountNo == null && e.BsbNo == null || e.ReferralAmount == null)).CountAsync();

            summary.ReadyToPay = await filteredResult.Where(e => e.ReferralPayment == false && (e.TotalCost - (invoiceInportData.Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Any() ? (invoiceInportData.Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) : 0)) <= 0 && e.LeadFk.ReferralLeadId != null && e.AccountName != null && e.AccountNo != null && e.BsbNo != null && e.ReferralAmount != null).CountAsync();

            summary.Paid = await filteredResult.Where(e => e.ReferralPayment == true).CountAsync();
            #endregion

            var ReferralLeadId = 0;
            if (input.FilterName == "RefferedProjectNo" && !string.IsNullOrWhiteSpace(input.Filter))
            {
                ReferralLeadId = (int)_jobsRepository.GetAll().Where(e => e.JobNumber == input.Filter).AsNoTracking().Select(e => e.LeadId).FirstOrDefault();
            }

            var filter = filteredResult
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(input.FilterName == "RefferedProjectNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.ReferralLeadId == ReferralLeadId)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                        .WhereIf(input.SalesRep != 0, e => e.LeadFk.AssignToUserID == input.SalesRep)

                        .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ActiveDate" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ActiveDate" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "InstallationDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "InstallationDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)

                         .WhereIf(input.DateType == "EntryDate" && input.StartDate != null, e => e.CreationTime.Date >= SDate.Value.Date)
                         .WhereIf(input.DateType == "EntryDate" && input.EndDate != null, e => e.CreationTime.Date <= EDate.Value.Date)


                        .WhereIf(input.PaymentStatus == 1, e => e.ReferralPayment == false && ((e.TotalCost - (invoiceInportData.Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Any() ? (invoiceInportData.Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) : 0) > 0) || e.LeadFk.ReferralLeadId == null || e.AccountName == null || e.AccountNo == null && e.BsbNo == null || e.ReferralAmount == null)) // Awaiting

                        .WhereIf(input.PaymentStatus == 2, e => e.ReferralPayment == false && (e.TotalCost - (invoiceInportData.Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Any() ? (invoiceInportData.Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) : 0)) <= 0 && e.LeadFk.ReferralLeadId != null && e.AccountName != null && e.AccountNo != null && e.BsbNo != null && e.ReferralAmount != null) // ReadyToPay

                        .WhereIf(input.PaymentStatus == 3, e => e.ReferralPayment == true) // Paid
                        .WhereIf(input.Verify == "Verify", e => e.IsRefferalVerify == true) // Paid
                        .WhereIf(input.Verify == "NotVerify", e => e.IsRefferalVerify == false) // Paid
                        ;

            var pagedAndFilteredResult = filter
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 16 && e.LeadFk.OrganizationId == input.OrganizationUnit).AsNoTracking()
                .Select(e => new { e.Id, e.SectionId, e.ActionId, e.LeadId, e.ActivityDate, e.ActivityNote });

            var results = from o in pagedAndFilteredResult
                          let refLeadId = o.LeadFk.ReferralLeadId == null ? 0 : o.LeadFk.ReferralLeadId
                          let refJobId = o.LeadFk.ReferralLeadId == null ? 0 : o.LeadFk.ReferralLeadId

                          select new GetViewReferralTrackerDto()
                          {
                              Id = o.Id,
                              LeadId = o.LeadFk.Id,
                              JobNumber = o.JobNumber,
                              CompanyName = o.LeadFk.CompanyName,
                              JobStatus = o.JobStatusFk.Name,
                              JobStatusColorClass = o.JobStatusFk.ColorClass,
                              InstallComplate = o.InstalledcompleteDate,
                              TotalCost = o.TotalCost,
                              OwningAmt = Convert.ToDecimal(o.TotalCost - ((invoiceInportData.Where(x => x.JobId == o.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (invoiceInportData.Where(x => x.JobId == o.Id).Select(e => e.PaidAmmount).Sum()))),
                              RefferedProjectName = _jobsRepository.GetAll().Where(e => e.LeadId == refLeadId).AsNoTracking().Select(e => e.JobNumber).FirstOrDefault(),
                              RefferedProjectStatus = _jobsRepository.GetAll().Where(e => e.LeadId == refLeadId).AsNoTracking().Select(e => e.JobStatusFk.Name).FirstOrDefault(),
                              RefferedProjectStatusColorClass = _jobsRepository.GetAll().Where(e => e.LeadId == refLeadId).AsNoTracking().Select(e => e.JobStatusFk.ColorClass).FirstOrDefault(),
                              RefferedTotalCost = _jobsRepository.GetAll().Where(e => e.LeadId == refLeadId).AsNoTracking().Select(e => e.TotalCost).FirstOrDefault(),
                              RefferedowningAmt = Convert.ToDecimal(_jobsRepository.GetAll().Where(e => e.LeadId == refLeadId).AsNoTracking().Select(e => e.TotalCost).FirstOrDefault() - ((invoiceInportData.Where(x => x.JobId == _jobsRepository.GetAll().AsNoTracking().Where(e => e.LeadId == refLeadId).Select(e => e.Id).FirstOrDefault()).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (invoiceInportData.Where(x => x.JobId == _jobsRepository.GetAll().Where(e => e.LeadId == refLeadId).AsNoTracking().Select(e => e.Id).FirstOrDefault()).Select(e => e.PaidAmmount).Sum()))),
                              AccountName = o.AccountName,
                              BSBNo = o.BsbNo,
                              AccountNo = o.AccountNo,
                              ActivityReminderTime = activity.Where(e => e.SectionId == 4 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                              ActivityDescription = activity.Where(e => e.SectionId == 4 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              ActivityComment = activity.Where(e => e.SectionId == 4 && e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                              ReferralAmount = o.ReferralAmount,
                              ReferralPayment = o.ReferralPayment,
                              IsRefferalVerify = o.IsRefferalVerify,

                              Isverifyornot = o.ReferralPayment == false && o.BsbNo != null && o.AccountNo != null && (Convert.ToDecimal(o.TotalCost - ((invoiceInportData.Where(x => x.JobId == o.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (invoiceInportData.Where(x => x.JobId == o.Id).Select(e => e.PaidAmmount).Sum()))) ) <= 0? true : false,

                              Summary = summary,
                              PaymentDate = o.ReferralPayDate,
                              BankRefNo = o.BankReferenceNo,
                              PaymentRemark = o.ReferralRemark,
                          };

            var totalCount = await filter.CountAsync();

            return new PagedResultDto<GetViewReferralTrackerDto>(totalCount, await results.ToListAsync());
            #endregion
        }

        public async Task<PagedResultDto<GetViewGridConnectionTrackerDto>> GetAllGridConnectionTracker(GetAllGridConnectionTrackerInput input)
        {
            #region Old Code
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var filteredResult = _jobsRepository.GetAll()
            //                  .Include(e => e.LeadFk)
            //                  .Include(e => e.JobStatusFk)
            //                  .Include(e => e.RoofTypeFk)
            //                  .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            //                  .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            //                  .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            //                  .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);



            //var filter = filteredResult
            //            .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email.Contains(input.Filter))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
            //            .WhereIf(input.ElecDistributorId != 0, e => e.ElecDistributorId == input.ElecDistributorId)
            //            .WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => input.JobStatusIDFilter.Contains((int)e.JobStatusId))
            //            .WhereIf(input.JobTypeId != 0, e => e.JobTypeId == input.JobTypeId)
            //            .WhereIf(input.InstallerId != 0, e => e.InstallerId == input.InstallerId)

            //            .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.DateType == "ActiveDate" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "ActiveDate" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.DateType == "DistAppliExpiryDate" && input.StartDate != null, e => e.DistExpiryDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "DistAppliExpiryDate" && input.EndDate != null, e => e.DistExpiryDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.DateType == "InstallDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "InstallDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.DateType == "InstallCmpltDate" && input.StartDate != null, e => e.InstalledcompleteDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "InstallCmpltDate" && input.EndDate != null, e => e.InstalledcompleteDate.Value.Date <= EDate.Value.Date)


            //            .WhereIf(input.ApplicationStatus == 1, e => (e.JobStatusId == 4 || e.JobStatusId == 5 || e.JobStatusId == 6 || e.JobStatusId == 7 || e.JobStatusId == 8 || e.JobStatusId == 9) && e.InstalledcompleteDate == null) //Awaiting

            //            .WhereIf(input.ApplicationStatus == 2, e => e.MeterApplyRef == null && e.InstalledcompleteDate != null) //Pending

            //            .WhereIf(input.ApplicationStatus == 3, e => e.MeterApplyRef != null && e.InstalledcompleteDate != null) //Complete

            //            .WhereIf(!string.IsNullOrWhiteSpace(input.NMINumber), e => e.NMINumber == input.NMINumber)

            //            ;

            //#region Summary Count
            //GridConnectionSummaryCountDto summary = new GridConnectionSummaryCountDto();
            //summary.Total = filter.Count();

            //summary.Awaiting = filter.Where(e => (e.JobStatusId == 4 || e.JobStatusId == 5 || e.JobStatusId == 6 || e.JobStatusId == 7 || e.JobStatusId == 8 || e.JobStatusId == 9) && e.InstalledcompleteDate == null).Count();

            //summary.Pending = filter.Where(e => e.MeterApplyRef == null && e.InstalledcompleteDate != null).Count();

            //summary.Complete = filter.Where(e => e.MeterApplyRef != null && e.InstalledcompleteDate != null).Count();
            //#endregion

            //var pagedAndFilteredResult = filter
            //    .OrderBy(input.Sorting ?? "id desc")
            //    .PageBy(input);

            //var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 6 && e.LeadFk.OrganizationId == input.OrganizationUnit);

            //var results = from o in pagedAndFilteredResult
            //              select new GetViewGridConnectionTrackerDto()
            //              {
            //                  Id = o.Id,
            //                  LeadId = o.LeadFk.Id,
            //                  JobNumber = o.JobNumber,
            //                  JobType = o.JobTypeFk.Name,
            //                  JobStatus = o.JobStatusFk.Name,
            //                  JobStatusColorClass = o.JobStatusFk.ColorClass,
            //                  CompanyName = o.LeadFk.CompanyName,
            //                  InstallationDate = o.InstallationDate,
            //                  Address = o.Address + ", " + o.Suburb + ", " + o.State + "-" + o.PostalCode,
            //                  Suburb = o.Suburb,
            //                  State = o.State,
            //                  PostalCode = o.PostalCode,
            //                  InstallerName = _userRepository.GetAll().Where(e => e.Id == o.InstallerId).Select(e => e.FullName).FirstOrDefault(),
            //                  InstalledCompleteDate = o.InstalledcompleteDate,
            //                  MeterApplyRef = o.MeterApplyRef,
            //                  InspectionDate = o.InspectionDate,
            //                  ElecDistributorName = o.ElecDistributorFk.Name,
            //                  ElecRetailerName = o.ElecRetailerFk.Name,
            //                  DistApproveDate = o.DistApproveDate,
            //                  DistExpiryDate = o.DistExpiryDate,
            //                  ApplicationRefNo = o.ApplicationRefNo,
            //                  SystemCapacity = o.SystemCapacity,

            //                  GridConnectionSmsSend = o.GridConnectionSmsSend,
            //                  GridConnectionEmailSendDate = o.GridConnectionSmsSendDate,
            //                  GridConnectionEmailSend = o.GridConnectionEmailSend,
            //                  GridConnectionSmsSendDate = o.GridConnectionEmailSendDate,

            //                  ActivityReminderTime = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
            //                  ActivityDescription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
            //                  ActivityComment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

            //                  Summary = summary
            //              };

            //var totalCount = await filter.CountAsync();

            //return new PagedResultDto<GetViewGridConnectionTrackerDto>(totalCount, await results.ToListAsync());
            #endregion

            #region New Code

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var filteredResult = _jobsRepository.GetAll()
                              .Include(e => e.LeadFk)
                              .Include(e => e.JobStatusFk)
                              .Include(e => e.RoofTypeFk)
                              .Include(e => e.ElecDistributorFk)
                              .Include(e => e.ElecRetailerFk)
                              .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit)
                              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                              .AsNoTracking()
                              .Select(e => new { e.Id, e.LeadFk, e.LeadId, e.JobNumber, e.State, e.ElecDistributorId, e.JobStatusId, e.JobTypeId, e.InstallerId, e.DepositeRecceivedDate, e.ActiveDate, e.DistExpiryDate, e.InstallationDate, e.InstalledcompleteDate, e.MeterApplyRef, e.NMINumber, e.JobStatusFk, e.JobTypeFk, e.Address, e.Suburb, e.PostalCode, e.InspectionDate, e.ElecDistributorFk, e.ElecRetailerFk, e.DistApproveDate, e.ApplicationRefNo, e.SystemCapacity, e.GridConnectionSmsSend, e.GridConnectionSmsSendDate, e.GridConnectionEmailSend, e.GridConnectionEmailSendDate })
                              ;

            var filter = filteredResult
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                        .WhereIf(input.ElecDistributorId != 0, e => e.ElecDistributorId == input.ElecDistributorId)
                        .WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => input.JobStatusIDFilter.Contains((int)e.JobStatusId))
                        .WhereIf(input.JobTypeId != 0, e => e.JobTypeId == input.JobTypeId)
                        .WhereIf(input.InstallerId != 0, e => e.InstallerId == input.InstallerId)

                        .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ActiveDate" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ActiveDate" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "DistAppliExpiryDate" && input.StartDate != null, e => e.DistExpiryDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "DistAppliExpiryDate" && input.EndDate != null, e => e.DistExpiryDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "InstallDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "InstallDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "InstallCmpltDate" && input.StartDate != null, e => e.InstalledcompleteDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "InstallCmpltDate" && input.EndDate != null, e => e.InstalledcompleteDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "MeterApplied" && input.StartDate != null, e => e.InspectionDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "MeterApplied" && input.EndDate != null, e => e.InspectionDate.Value.Date <= EDate.Value.Date)


                        .WhereIf(input.ApplicationStatus == 1, e => (e.JobStatusId == 4 || e.JobStatusId == 5 || e.JobStatusId == 6 || e.JobStatusId == 7 || e.JobStatusId == 8 || e.JobStatusId == 9) && e.InstalledcompleteDate == null) //Awaiting

                        .WhereIf(input.ApplicationStatus == 2, e => e.MeterApplyRef == null && e.InstalledcompleteDate != null) //Pending

                        .WhereIf(input.ApplicationStatus == 3, e => e.MeterApplyRef != null && e.InstalledcompleteDate != null) //Complete

                        .WhereIf(!string.IsNullOrWhiteSpace(input.NMINumber), e => e.NMINumber == input.NMINumber)
                        ;

            #region Summary Count
            GridConnectionSummaryCountDto summary = new GridConnectionSummaryCountDto();
            summary.Total = await filter.CountAsync();

            summary.Awaiting = await filter.Where(e => (e.JobStatusId == 4 || e.JobStatusId == 5 || e.JobStatusId == 6 || e.JobStatusId == 7 || e.JobStatusId == 8 || e.JobStatusId == 9) && e.InstalledcompleteDate == null).CountAsync();

            summary.Pending = await filter.Where(e => e.MeterApplyRef == null && e.InstalledcompleteDate != null).CountAsync();

            summary.Complete = await filter.Where(e => e.MeterApplyRef != null && e.InstalledcompleteDate != null).CountAsync();
            #endregion

            var pagedAndFilteredResult = filter
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 6 && e.LeadFk.OrganizationId == input.OrganizationUnit).AsNoTracking()
                .Select(e => new { e.Id, e.ActionId, e.LeadId, e.ActivityDate, e.ActivityNote });

            var results = from o in pagedAndFilteredResult
                          select new GetViewGridConnectionTrackerDto()
                          {
                              Id = o.Id,
                              LeadId = o.LeadFk.Id,
                              JobNumber = o.JobNumber,
                              JobType = o.JobTypeFk.Name,
                              JobStatus = o.JobStatusFk.Name,
                              JobStatusColorClass = o.JobStatusFk.ColorClass,
                              CompanyName = o.LeadFk.CompanyName,
                              InstallationDate = o.InstallationDate,
                              Address = o.Address + ", " + o.Suburb + ", " + o.State + "-" + o.PostalCode,
                              Suburb = o.Suburb,
                              State = o.State,
                              PostalCode = o.PostalCode,
                              InstallerName = User_List.Where(e => e.Id == o.InstallerId).Select(e => e.FullName).FirstOrDefault(),
                              InstalledCompleteDate = o.InstalledcompleteDate,
                              MeterApplyRef = o.MeterApplyRef,
                              InspectionDate = o.InspectionDate,
                              ElecDistributorName = o.ElecDistributorFk.Name,
                              ElecRetailerName = o.ElecRetailerFk.Name,
                              DistApproveDate = o.DistApproveDate,
                              DistExpiryDate = o.DistExpiryDate,
                              ApplicationRefNo = o.ApplicationRefNo,
                              SystemCapacity = o.SystemCapacity,

                              GridConnectionSmsSend = o.GridConnectionSmsSend,
                              GridConnectionEmailSendDate = o.GridConnectionSmsSendDate,
                              GridConnectionEmailSend = o.GridConnectionEmailSend,
                              GridConnectionSmsSendDate = o.GridConnectionEmailSendDate,

                              ActivityReminderTime = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                              ActivityDescription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              ActivityComment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                              Summary = summary
                          };

            var totalCount = await filter.CountAsync();

            return new PagedResultDto<GetViewGridConnectionTrackerDto>(totalCount, await results.ToListAsync());
            #endregion
        }

        public async Task<PagedResultDto<GetViewSTCTrackerDto>> GetAllSTCTracker(GetAllSTCTrackerInput input)
        {
            #region Old Code
            //int stcBlank = 0;
            //int withblank = 0;
            //if (input.PvdStatus != null)
            //{
            //    if (input.PvdStatus.Contains(11))
            //    {
            //        stcBlank = 11;
            //        input.PvdStatus.Remove(11);
            //        withblank = input.PvdStatus.Count;
            //        input.PvdNoStatus = 2;
            //    }
            //}

            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var filteredResult = _jobsRepository.GetAll()
            //                  .Include(e => e.LeadFk)
            //                  .Include(e => e.JobStatusFk)
            //                  .Include(e => e.JobTypeFk)
            //                  .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.PVDStatus != 0)
            //                  .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            //                  .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            //                  .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            //                  ;

            //var filter = filteredResult
            //             .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email.Contains(input.Filter))
            //            .WhereIf(input.PvdStatus != null && input.PvdStatus.Count() > 0 && stcBlank == 0, e => input.PvdStatus.Contains((int)e.PVDStatus))
            //            .WhereIf(stcBlank != 0 && withblank == 0, e => e.PVDStatus == null)
            //            .WhereIf(stcBlank != 0 && withblank > 0, e => (e.PVDStatus == null || input.PvdStatus.Contains((int)e.PVDStatus)))
            //            .WhereIf(input.PvdNoStatus == 1, e => e.PVDNumber != null && e.PVDNumber != "")
            //            .WhereIf(input.PvdNoStatus == 2, e => e.PVDNumber == null || e.PVDNumber == "")
            //            .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
            //            .WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => input.JobStatusIDFilter.Contains((int)e.JobStatusId))
            //            .WhereIf(input.JobTypeId != 0, e => e.JobTypeId == input.JobTypeId)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
            //            .WhereIf(input.PvdNo != null && input.PvdNo != "", e => e.PVDNumber == input.PvdNo)
            //            .WhereIf(input.InstallerId != null && input.InstallerId != 0, e => e.InstallerId == input.InstallerId)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.LeadFk.State == input.State)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.RECStatus), e => e.RECPvdStatus == input.RECStatus)

            //            .WhereIf(input.DateType == "StcAppliedDate" && input.StartDate != null, e => e.STCAppliedDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "StcAppliedDate" && input.EndDate != null, e => e.STCAppliedDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.DateType == "InstallDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "InstallDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.DateType == "InstallCmpltDate" && input.StartDate != null, e => e.InstalledcompleteDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "InstallCmpltDate" && input.EndDate != null, e => e.InstalledcompleteDate.Value.Date <= EDate.Value.Date)
            //            ;

            //#region Summary Count
            //List<STCSummaryCountDto> summary = new List<STCSummaryCountDto>();

            //// STC Total
            //var STCTotalData = new STCSummaryCountDto();
            //STCTotalData.STC = "Total";
            //STCTotalData.TotalSTCAmount = filter.Where(e => e.PVDStatus != null).Select(e => e.STC).Sum();
            //STCTotalData.TotalJob = filter.Count();
            //summary.Add(STCTotalData);

            //// STC Blank Data
            //var STCBlankData = new STCSummaryCountDto();
            //STCBlankData.STC = "Blank";
            //STCBlankData.TotalSTCAmount = filter.Where(e => e.PVDStatus == null).Select(e => e.STC).Sum();
            //STCBlankData.TotalJob = filter.Where(e => e.PVDStatus == null).Select(e => e.Id).Count();
            //summary.Add(STCBlankData);

            //// STC Status Data
            //var PvdStatus = _pvdStatusRepository.GetAll().Distinct().ToList();
            //for (int k = 0; k < PvdStatus.Count; k++)
            //{
            //    var STCData = new STCSummaryCountDto();
            //    if (PvdStatus[k].Id != 11)
            //    {
            //        STCData.STC = PvdStatus[k].Name;

            //        int? pvdid = PvdStatus[k].Id;
            //        STCData.TotalSTCAmount = filter.Where(e => e.PVDStatus == pvdid && e.PVDStatus != 11).Select(e => e.STC).Sum();

            //        STCData.TotalJob = filter.Where(e => e.PVDStatus == pvdid && e.PVDStatus != 11).Select(e => e.Id).Count();

            //        if (STCData.TotalJob > 0)
            //        {
            //            summary.Add(STCData);
            //        }
            //    }
            //}

            ////summary = summary.OrderBy(e => e.TotalSTCAmount).ToList();
            //#endregion

            //var pagedAndFilteredResult = filter
            //    .OrderBy(input.Sorting ?? "id desc")
            //    .PageBy(input);

            //var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 7 && e.LeadFk.OrganizationId == input.OrganizationUnit);

            //var results = from o in pagedAndFilteredResult
            //              join pvdval in _pvdStatusRepository.GetAll() on o.PVDStatus equals pvdval.Id into pvd
            //              from pvdval in pvd.DefaultIfEmpty()

            //              select new GetViewSTCTrackerDto()
            //              {
            //                  Id = o.Id,
            //                  LeadId = o.LeadFk.Id,
            //                  JobNumber = o.JobNumber,
            //                  JobTypeName = o.JobTypeFk.Name,
            //                  StreetNo = o.StreetNo,
            //                  StreetName = o.StreetName,
            //                  StreetType = o.StreetType,
            //                  Address = o.Address + ", " + o.Suburb + ", " + o.State + "-" + o.PostalCode,
            //                  Suburb = o.Suburb,
            //                  State = o.State,
            //                  PostalCode = o.PostalCode,
            //                  InstallationDate = o.InstallationDate,
            //                  STCAppliedDate = o.STCAppliedDate,
            //                  PvdNumber = o.PVDNumber == null || o.PVDNumber == "" ? "Blank" : o.PVDNumber,
            //                  PvdStatusName = pvdval == null || pvdval.Name == null ? "Blank" : pvdval.Name.ToString(),
            //                  JobStatusName = o.JobStatusFk.Name,
            //                  JobStatusColorClass = o.JobStatusFk.ColorClass,
            //                  SystemCapacity = o.SystemCapacity,
            //                  STC = o.STC,
            //                  STCTotalPrice = o.Rebate,
            //                  GreenbotSTC = o.GreenbotSTC,
            //                  InstallerName = _userRepository.GetAll().Where(e => e.Id == o.InstallerId).Select(e => e.FullName).FirstOrDefault(),
            //                  STCNotes = o.STCNotes,
            //                  STCSmsSend = o.StcSmsSend,
            //                  STCSmsSendDate = o.StcSmsSendDate,
            //                  STCEmailSend = o.StcEmailSend,
            //                  STCEmailSendDate = o.StcEmailSendDate,
            //                  CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
            //                  ActivityReminderTime = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
            //                  ActivityDescription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
            //                  ActivityComment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

            //                  LastUpdatedDate = o.LastModificationTime.Value.ToString("dd-MM-yyyy hh:mm:ss"),
            //                  LastRECUpdated = o.LastRECUpdated,
            //                  RECPvdNumber = o.RECPvdNumber,
            //                  RECPvdStatus = o.RECPvdStatus,
            //                  Summary = summary
            //              };

            //var totalCount = await filter.CountAsync();

            //return new PagedResultDto<GetViewSTCTrackerDto>(totalCount, await results.ToListAsync());
            #endregion

            #region New Code

            int stcBlank = 0;
            int withblank = 0;
            if (input.PvdStatus != null)
            {
                if (input.PvdStatus.Contains(11))
                {
                    stcBlank = 11;
                    input.PvdStatus.Remove(11);
                    withblank = input.PvdStatus.Count;
                    input.PvdNoStatus = 2;
                }
            }

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            List<int> batteryInverter = new List<int>() { 1, 3, 4, 6 };

            var filteredResult = _jobsRepository.GetAll()
                              .Include(e => e.LeadFk)
                              .Include(e => e.JobStatusFk)
                              .Include(e => e.JobTypeFk)
                              .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.PVDStatus != 0)
                              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                              .AsNoTracking()
                              .Select(e => new { e.Id, e.LeadId, e.LeadFk, e.JobStatusFk, e.JobTypeFk, e.JobNumber, e.JobStatusId, e.PVDStatus, e.PVDNumber, e.State, e.RECPvdStatus, e.JobTypeId, e.InstallerId, e.STCAppliedDate, e.InstallationDate, e.InstalledcompleteDate, e.STC, e.StreetNo, e.StreetName, e.StreetType, e.Address, e.Suburb, e.PostalCode, e.SystemCapacity, e.Rebate, e.GreenbotSTC, e.STCNotes, e.StcSmsSend, e.StcSmsSendDate, e.StcEmailSend, e.StcEmailSendDate, e.LastModificationTime, e.LastRECUpdated, e.RECPvdNumber })
                              ;

            var filter = filteredResult
                          .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(input.PvdStatus != null && input.PvdStatus.Count() > 0 && stcBlank == 0, e => input.PvdStatus.Contains((int)e.PVDStatus))
                        .WhereIf(stcBlank != 0 && withblank == 0, e => e.PVDStatus == null)
                        .WhereIf(stcBlank != 0 && withblank > 0, e => (e.PVDStatus == null || input.PvdStatus.Contains((int)e.PVDStatus)))
                        .WhereIf(input.PvdNoStatus == 1, e => e.PVDNumber != null && e.PVDNumber != "")
                        .WhereIf(input.PvdNoStatus == 2, e => e.PVDNumber == null || e.PVDNumber == "")
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => input.JobStatusIDFilter.Contains((int)e.JobStatusId))
                        .WhereIf(input.JobTypeId != 0, e => e.JobTypeId == input.JobTypeId)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                        .WhereIf(input.PvdNo != null && input.PvdNo != "", e => e.PVDNumber == input.PvdNo)
                        .WhereIf(input.InstallerId != null && input.InstallerId != 0, e => e.InstallerId == input.InstallerId)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.LeadFk.State == input.State)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RECStatus), e => e.RECPvdStatus == input.RECStatus)

                        .WhereIf(input.DateType == "StcAppliedDate" && input.StartDate != null, e => e.STCAppliedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "StcAppliedDate" && input.EndDate != null, e => e.STCAppliedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "InstallDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "InstallDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "InstallCmpltDate" && input.StartDate != null, e => e.InstalledcompleteDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "InstallCmpltDate" && input.EndDate != null, e => e.InstalledcompleteDate.Value.Date <= EDate.Value.Date)
                        .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5))

                        .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                        .WhereIf(input.BatteryFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                        .WhereIf(input.BatteryFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                        .WhereIf(input.BatteryFilter == 5, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 2).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && batteryInverter.Contains(j.ProductItemFk.ProductTypeId)).Count() == 0) //For Only Inverter and Battery
                        .WhereIf(input.BatteryFilter == 6, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5)) //For with battery
                        ;

            #region Summary Count
            List<STCSummaryCountDto> summary = new List<STCSummaryCountDto>();

            // STC Total
            var STCTotalData = new STCSummaryCountDto();
            STCTotalData.STC = "Total";
            STCTotalData.TotalSTCAmount = await filter.Where(e => e.PVDStatus != null).Select(e => e.STC).SumAsync();
            STCTotalData.TotalJob = await filter.CountAsync();
            summary.Add(STCTotalData);

            // STC Blank Data
            var STCBlankData = new STCSummaryCountDto();
            STCBlankData.STC = "Blank";
            STCBlankData.TotalSTCAmount = await filter.Where(e => e.PVDStatus == null).Select(e => e.STC).SumAsync();
            STCBlankData.TotalJob = await filter.Where(e => e.PVDStatus == null).Select(e => e.Id).CountAsync();
            summary.Add(STCBlankData);

            // STC Status Data
            var PvdStatus = await _pvdStatusRepository.GetAll().AsNoTracking().ToListAsync();
            for (int k = 0; k < PvdStatus.Count; k++)
            {
                var STCData = new STCSummaryCountDto();
                if (PvdStatus[k].Id != 11)
                {
                    STCData.STC = PvdStatus[k].Name;

                    int? pvdid = PvdStatus[k].Id;
                    STCData.TotalSTCAmount = await filter.Where(e => e.PVDStatus == pvdid && e.PVDStatus != 11).Select(e => e.STC).SumAsync();

                    STCData.TotalJob = await filter.Where(e => e.PVDStatus == pvdid && e.PVDStatus != 11).Select(e => e.Id).CountAsync();

                    if (STCData.TotalJob > 0)
                    {
                        summary.Add(STCData);
                    }
                }
            }

            //summary = summary.OrderBy(e => e.TotalSTCAmount).ToList();
            #endregion

            var pagedAndFilteredResult = filter
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 7 && e.LeadFk.OrganizationId == input.OrganizationUnit).AsNoTracking()
                .Select(e => new { e.Id, e.LeadId, e.ActionId, e.ActivityDate, e.ActivityNote });

            var results = from o in pagedAndFilteredResult
                          join pvdval in _pvdStatusRepository.GetAll() on o.PVDStatus equals pvdval.Id into pvd
                          from pvdval in pvd.DefaultIfEmpty()

                          select new GetViewSTCTrackerDto()
                          {
                              Id = o.Id,
                              LeadId = o.LeadFk.Id,
                              JobNumber = o.JobNumber,
                              JobTypeName = o.JobTypeFk.Name,
                              StreetNo = o.StreetNo,
                              StreetName = o.StreetName,
                              StreetType = o.StreetType,
                              Address = o.Address + ", " + o.Suburb + ", " + o.State + "-" + o.PostalCode,
                              Suburb = o.Suburb,
                              State = o.State,
                              PostalCode = o.PostalCode,
                              InstallationDate = o.InstallationDate,
                              STCAppliedDate = o.STCAppliedDate,
                              PvdNumber = o.PVDNumber == null || o.PVDNumber == "" ? "Blank" : o.PVDNumber,
                              PvdStatusName = pvdval == null || pvdval.Name == null ? "Blank" : pvdval.Name.ToString(),
                              JobStatusName = o.JobStatusFk.Name,
                              JobStatusColorClass = o.JobStatusFk.ColorClass,
                              SystemCapacity = o.SystemCapacity,
                              STC = o.STC,
                              STCTotalPrice = o.Rebate,
                              GreenbotSTC = o.GreenbotSTC,
                              InstallerName = User_List.Where(e => e.Id == o.InstallerId).Select(e => e.FullName).FirstOrDefault(),
                              STCNotes = o.STCNotes,
                              STCSmsSend = o.StcSmsSend,
                              STCSmsSendDate = o.StcSmsSendDate,
                              STCEmailSend = o.StcEmailSend,
                              STCEmailSendDate = o.StcEmailSendDate,
                              CurrentLeadOwaner = User_List.Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                              ActivityReminderTime = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                              ActivityDescription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              ActivityComment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              ActivityCommentDate = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),

                              LastUpdatedDate = o.LastModificationTime.Value.ToString("dd-MM-yyyy hh:mm:ss"),
                              LastRECUpdated = o.LastRECUpdated,
                              RECPvdNumber = o.RECPvdNumber,
                              RECPvdStatus = o.RECPvdStatus,
                              Summary = summary
                          };

            var totalCount = await filter.CountAsync();

            return new PagedResultDto<GetViewSTCTrackerDto>(totalCount, await results.ToListAsync());
            #endregion
        }
        public async Task<PagedResultDto<GetViewApplicationFeeTrackerDto>> GetAllJobApplicationFeeTracker(GetAllJobApplicationFeeTrackerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var Today = _timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId);

            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var filteredResult = _jobsRepository.GetAll()
                              .Include(e => e.LeadFk)
                              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                              .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.JobStatusId != 3 && e.SolarRebateStatus != 3 && e.FirstDepositDate != null && e.Applicationfeespaid == 1)
                              .AsNoTracking()
                              .Select(e => new { e.Id, e.JobNumber, e.LeadFk, e.State,e.Applicationfeespaid, e.PaidAmmount,e.Paidby,e.PaidStatus,e.ApplicationRefNo,e.InvRefNo,e.ElecDistributorId,e.InvPaidDate, e.JobStatusId, e.JobTypeId, e.DepositeRecceivedDate, e.DistApplied, e.InstallationDate, e.ActiveDate, e.MeterPhaseId, e.NMINumber, e.PeakMeterNo, e.DistExpiryDate, e.JobTypeFk, e.JobStatusFk, e.Address, e.Suburb, e.PostalCode, e.ElecDistributorFk, e.DistApproveDate, e.SmsSend, e.SmsSendDate, e.EmailSend, e.EmailSendDate, e.LeadId });

            #region Summary Count
            ApplicationFeeSummaryCountDto summary = new ApplicationFeeSummaryCountDto();
            summary.Total = await filteredResult.CountAsync();

            summary.Paid = await filteredResult.Where(e => e.PaidStatus == 1).CountAsync();

            summary.Pending = await filteredResult.Where(e => e.PaidStatus == 2).CountAsync();


            #endregion

            var filter = filteredResult
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(input.FilterName == "ApplicationRefNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ApplicationRefNo == input.Filter)
                        .WhereIf(input.FilterName == "NMINumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.NMINumber == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                        .WhereIf(input.ElecDistributorId != 0, e => e.ElecDistributorId == input.ElecDistributorId)
                        .WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => input.JobStatusIDFilter.Contains((int)e.JobStatusId))
                        .WhereIf(input.JobTypeId != 0, e => e.JobTypeId == input.JobTypeId)
                        .WhereIf(input.PaidStatus != 0, e => e.PaidStatus == input.PaidStatus)
                        //.WhereIf(input.FeesPaid != 0, e => e.Applicationfeespaid == input.FeesPaid)
                        .WhereIf(input.PaidById != 0, e => e.Paidby == input.PaidById)
                        .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ApplicationDate" && input.StartDate != null, e => e.DistApplied.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ApplicationDate" && input.EndDate != null, e => e.DistApplied.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "InstallationDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "InstallationDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ActiveDate" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ActiveDate" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.ApplicationStatus == 1, e => e.ApplicationRefNo == null
                        && (_documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() == false || e.MeterPhaseId == null || e.NMINumber == null || e.PeakMeterNo == null)) //Application Awaiting

                        .WhereIf(input.ApplicationStatus == 2, e => e.ApplicationRefNo == null
                        && _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() && e.MeterPhaseId != null && e.NMINumber == null && e.PeakMeterNo != null) //Application Pending

                        .WhereIf(input.ApplicationStatus == 3, e => e.ApplicationRefNo != null) //Application Complete

                        .WhereIf(input.ApplicationStatus == 4, e => e.ApplicationRefNo != null && (e.DistExpiryDate.Value.AddDays(-30) < Today) && e.JobStatusId < 6) //Application Expiry
                        ;

            var pagedAndFilteredResult = filter
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var activity = _leadactivityRepository.GetAll().AsNoTracking().Where(e => e.SectionId == 1).Select(e => new { e.ActionId, e.LeadId, e.Id, e.ActivityDate, e.ActivityNote });

            var results = from o in pagedAndFilteredResult
                          select new GetViewApplicationFeeTrackerDto()
                          {
                              Id = o.Id,
                              LeadId = o.LeadFk.Id,
                              JobNumber = o.JobNumber,
                              JobType = o.JobTypeFk.Name,
                              JobStatus = o.JobStatusFk.Name,
                              JobStatusColorClass = o.JobStatusFk.ColorClass,
                              CompanyName = o.LeadFk.CompanyName,
                              Address = o.Address,
                              Suburb = o.Suburb,
                              State = o.State,
                              PostalCode = o.PostalCode,
                              ElecDistributor = o.ElecDistributorFk.Name,

                              ApplicationFeesPaid = o.Applicationfeespaid == 1 ? "Yes" : (o.Applicationfeespaid == 0 ? "No" : ""),
                              PaidAmount = o.PaidAmmount,
                              PaidBy = o.Paidby == 1 ? "Customer" : (o.Paidby == 2 ? "Company" : ""),
                              PaidStatus =o.PaidStatus == 1 ? "Paid" : (o.PaidStatus == 2 ? "Pending" : ""),
                              InvRefNo =o.InvRefNo,
                              InvVerifyDate=o.InvPaidDate,
                            
                              SmsSend = o.SmsSend,
                              SmsSendDate = o.SmsSendDate,
                              EmailSend = o.EmailSend,
                              EmailSendDate = o.EmailSendDate,
                              CurruntLeadOwner = User_List.Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                              NextFollowup = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                              Discription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              Comment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                              Summary = summary,
                              IsExpired = (o.ApplicationRefNo != null && (o.DistExpiryDate.Value.AddDays(-30) < Today) && o.JobStatusId < 6) ? true : false
                          };

            var totalCount = await results.CountAsync();

            return new PagedResultDto<GetViewApplicationFeeTrackerDto>(totalCount, await results.ToListAsync());
        }


        #region Export Excel
        public async Task<FileDto> GetAllGridConnectionTrackerExcel(GetAllGridConnectionTrackerExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var Today = _timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId);
            var User_List = _userRepository.GetAll().AsNoTracking();

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredResult = _jobsRepository.GetAll()
                              .Include(e => e.LeadFk)
                              .Include(e => e.JobStatusFk)
                              .Include(e => e.RoofTypeFk)
                              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                              //.Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);

                              .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);



            var filter = filteredResult
                         //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                         .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                        .WhereIf(input.ElecDistributorId != 0, e => e.ElecDistributorId == input.ElecDistributorId)
                        .WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => input.JobStatusIDFilter.Contains((int)e.JobStatusId))
                        .WhereIf(input.JobTypeId != 0, e => e.JobTypeId == input.JobTypeId)
                        .WhereIf(input.InstallerId != 0, e => e.InstallerId == input.InstallerId)

                        .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ActiveDate" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ActiveDate" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "DistAppliExpiryDate" && input.StartDate != null, e => e.DistExpiryDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "DistAppliExpiryDate" && input.EndDate != null, e => e.DistExpiryDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "InstallDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "InstallDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "InstallCmpltDate" && input.StartDate != null, e => e.InstalledcompleteDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "InstallCmpltDate" && input.EndDate != null, e => e.InstalledcompleteDate.Value.Date <= EDate.Value.Date)


                        .WhereIf(input.DateType == "MeterApplied" && input.StartDate != null, e => e.InspectionDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "MeterApplied" && input.EndDate != null, e => e.InspectionDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.ApplicationStatus == 1, e => (e.JobStatusId == 4 || e.JobStatusId == 5 || e.JobStatusId == 6 || e.JobStatusId == 7 || e.JobStatusId == 8 || e.JobStatusId == 9) && e.InstalledcompleteDate == null) //Awaiting

                        .WhereIf(input.ApplicationStatus == 2, e => e.MeterApplyRef == null && e.InstalledcompleteDate != null) //Pending

                        //.WhereIf(input.ApplicationStatus == 3, e => e.MeterApplyRef != null && e.InstalledcompleteDate != null) //Complete
                        .WhereIf(input.ApplicationStatus == 3, e => e.MeterApplyRef != null && e.InstalledcompleteDate != null)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NMINumber), e => e.NMINumber == input.NMINumber)
                        //.Where(e => e.MeterApplyRef == null)
                        ;

            var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 7 && e.LeadFk.OrganizationId == input.OrganizationUnit).AsNoTracking()
                .Select(e => new { e.Id, e.LeadId, e.ActionId, e.ActivityDate, e.ActivityNote });

            //var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 6 && e.LeadFk.OrganizationId == input.OrganizationUnit);

            var results = from o in filter
                          select new GetViewGridConnectionTrackerDto()
                          {
                              Id = o.Id,
                              CompanyName = o.LeadFk.CompanyName,
                              Mobile = o.LeadFk.Mobile,
                              Email = o.LeadFk.Email,
                              Address = o.LeadFk.Address,
                              PostalCode = o.LeadFk.PostCode,
                              State = o.LeadFk.State,
                              JobNumber = o.JobNumber,
                              JobType = o.JobTypeFk.Name,
                              InstallationDate = o.InstallationDate,
                              SystemCapacity = o.SystemCapacity,
                              NMINumber = o.NMINumber,
                              ApplicationRefNo = o.ApplicationRefNo,
                              ElecRetailerName = o.ElecRetailerFk.Name,
                              NotAppliedDays = string.IsNullOrEmpty(o.ApplicationRefNo) && o.InstalledcompleteDate != null ? (int)(Today.Value.Date - o.InstalledcompleteDate.Value.Date).TotalDays : 0,
                              InstallerName = User_List.Where(e => e.Id == o.InstallerId).Select(e => e.FullName).FirstOrDefault()
                              
                          };



            if (input.ExcelOrCsv == 1) // 1 use for excel , 2 use for csv
            {
                return _jobTrackerExcelExporter.GridConnectionExportToFile(await results.ToListAsync(), "GridConnectionTracker.xlsx");
            }
            else
            {
                return _jobTrackerExcelExporter.GridConnectionExportToFile(await results.ToListAsync(), "GridConnectionTracker.csv");
            }
        }

        public async Task<FileDto> GetAllJobActiveTrackerExport(GetAllJobActiveTrackerExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredResult = _jobsRepository.GetAll()
                              .Include(e => e.LeadFk)
                              .Include(e => e.JobTypeFk)
                              .Include(e => e.JobStatusFk)
                              .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.JobStatusId != 3 && e.SolarRebateStatus != 3 && e.FirstDepositDate != null)
                              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)

                              .WhereIf(input.DateType == "DepositeReceivedDate" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                              .WhereIf(input.DateType == "DepositeReceivedDate" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                              .WhereIf(input.DateType == "ActiveDate" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
                              .WhereIf(input.DateType == "ActiveDate" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date)

                              .WhereIf(input.DateType == "FirstDespoitDate" && input.StartDate != null, e => e.FirstDepositDate.Value.Date >= SDate.Value.Date)
                              .WhereIf(input.DateType == "FirstDespoitDate" && input.EndDate != null, e => e.FirstDepositDate.Value.Date <= EDate.Value.Date)

                               .WhereIf(input.PaymentId != 0 && input.PaymentId != null, e => e.PaymentOptionId == input.PaymentId)
                               .WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => input.JobStatusIDFilter.Contains((int)e.JobStatusId))
                               .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                               .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                               .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                               .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                               .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                                .WhereIf(!string.IsNullOrEmpty(input.State), e => e.LeadFk.State == input.State)
                              .AsNoTracking()
                              .Select(e => new { e.Id, e.JobNumber, e.LeadFk, e.ActiveDate, e.HouseTypeId, e.ElecDistributorId, e.RoofAngleId, e.RoofTypeId, e.InstallerNotes, e.MeterPhaseId, e.NMINumber, e.PeakMeterNo, e.ApplicationRefNo, e.MeterUpgradeId, e.PaymentOptionId, e.FinanceDocumentVerified, e.ElecRetailerId, e.DepositeRecceivedDate, e.JobTypeFk, e.JobStatusId, e.FirstDepositDate, e.JobStatusFk, e.JobActiveSmsSend, e.JobActiveSmsSendDate, e.JobActiveEmailSend, e.JobActiveEmailSendDate, e.LeadId, e.DistApproveDate, e.ExpiryDate, e.State, e.SolarRebateStatus, e.VicRebate, e.DistApplied, e.DistExpiryDate });





            var filter = filteredResult

                        //.WhereIf(input.ApplicationStatus == 1, e => e.ActiveDate != null)
                        .WhereIf(input.ApplicationStatus == 1, e => e.ActiveDate == null && (e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofTypeId == null || e.RoofAngleId == null
                                || _quotationRepository.GetAll().Where(q => q.JobId == e.Id).Any() == false
                                || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id && (i.InvoicePaymentMethodFk.PaymentMethod == "EFT" || i.InvoicePaymentMethodFk.PaymentMethod == "Credit Card") && i.IsVerified == false).Any()
                                 || _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id).Any() == false) == false &&
                                 (_lookup_meterPhaseRepository.GetAll().Where(m => m.Id == e.MeterPhaseId).Any() == false || string.IsNullOrEmpty(e.PeakMeterNo) || string.IsNullOrEmpty(e.NMINumber) || _documentRepository.GetAll().Where(d => d.JobId == e.Id).Any() == false || e.ElecRetailerId == null
                                  || string.IsNullOrEmpty(e.ApplicationRefNo)
                                  || e.DistApproveDate == null
                                  || e.MeterUpgradeId == null
                                  || _documentRepository.GetAll().Where(d => d.DocumentTypeId == 3 && d.JobId == e.Id).Any() == false
                                || (_quotationRepository.GetAll().Where(q => q.JobId == e.Id && q.IsSigned == true).Any() == false && _documentRepository.GetAll().Where(d => d.JobId == e.Id && d.DocumentTypeId == 5).Any() == false)
                                 || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 || (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == false
                                 || (e.State == "VIC" ? e.ExpiryDate == null : true) == false
                                || (e.State == "VIC" ? ((e.SolarRebateStatus == 0 || e.SolarRebateStatus != 17) && e.VicRebate != "With Rebate Install" ? false : true) : true) == false) == false)

            //            .WhereIf(input.ApplicationStatus == 2, e => e.ActiveDate == null && e.HouseTypeId != null && e.ElecDistributorId != null && e.RoofAngleId != null && e.RoofTypeId != null && e.InstallerNotes != null && e.MeterPhaseId != null && e.NMINumber != null && e.PeakMeterNo != null && e.ApplicationRefNo != null && e.MeterUpgradeId != null && (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == true && e.ElecRetailerId != null && e.DepositeRecceivedDate != null
            //&& _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any()
            //&& _documentRepository.GetAll().AsNoTracking().Where(j => j.DocumentTypeId == 3 && j.JobId == e.Id).Any()
            //&& (_quotationRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id && j.IsSigned == true).Any() || _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id && j.DocumentTypeId == 5).Any()))
                        .WhereIf(input.ApplicationStatus == 2, e => (e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofTypeId == null || e.RoofAngleId == null
                                || _quotationRepository.GetAll().Where(q => q.JobId == e.Id).Any() == false
                                || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 || _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id).Any() == false) == false &&
                                 (_lookup_meterPhaseRepository.GetAll().Where(m => m.Id == e.MeterPhaseId).Any() == false || string.IsNullOrEmpty(e.PeakMeterNo) || string.IsNullOrEmpty(e.NMINumber) || _documentRepository.GetAll().Where(d => d.JobId == e.Id).Any() == false || e.ElecRetailerId == null
                                  || string.IsNullOrEmpty(e.ApplicationRefNo)
                                  || e.DistApproveDate == null
                                  || e.MeterUpgradeId == null
                                  || _documentRepository.GetAll().Where(d => d.DocumentTypeId == 3 && d.JobId == e.Id).Any() == false
                                || (_quotationRepository.GetAll().Where(q => q.JobId == e.Id && q.IsSigned == true).Any() == false && _documentRepository.GetAll().Where(d => d.JobId == e.Id && d.DocumentTypeId == 5).Any() == false)
                                 || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 || (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == false
                                 || (e.State == "VIC" ? e.ExpiryDate == null : true) == false
                                || (e.State == "VIC" ? ((e.SolarRebateStatus == 0 || e.SolarRebateStatus != 17) && e.VicRebate != "With Rebate Install" ? false : true) : true) == false
                                 || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id && (i.InvoicePaymentMethodFk.PaymentMethod == "EFT" || i.InvoicePaymentMethodFk.PaymentMethod == "Credit Card") && i.IsVerified == false).Any()
                                ))


            //            .WhereIf(input.ApplicationStatus == 3, e => e.ActiveDate == null && (e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofAngleId == null || e.RoofTypeId == null || e.InstallerNotes == null || e.MeterPhaseId == null || e.NMINumber == null || e.PeakMeterNo == null || e.ApplicationRefNo == null || e.MeterUpgradeId == null || (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == false || e.ElecRetailerId == null
            //|| _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() == false
            //|| _documentRepository.GetAll().AsNoTracking().Where(j => j.DocumentTypeId == 3 && j.JobId == e.Id).Any() == false
            //|| (_quotationRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id && j.IsSigned == true).Any() || _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id && j.DocumentTypeId == 5).Any()) == false) || e.DepositeRecceivedDate == null
            //|| _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id && (i.InvoicePaymentMethodFk.PaymentMethod == "EFT" || i.InvoicePaymentMethodFk.PaymentMethod == "Credit Card") && i.IsVerified == false).Any() == true
            //            )
              .WhereIf(input.ApplicationStatus == 3, e => e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofTypeId == null || e.RoofAngleId == null
                                || _quotationRepository.GetAll().Where(q => q.JobId == e.Id).Any() == false
                                || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 // || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id && (i.InvoicePaymentMethodFk.PaymentMethod == "EFT" || i.InvoicePaymentMethodFk.PaymentMethod == "Credit Card") && i.IsVerified == false).Any()
                                 || _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id).Any() == false || e.DepositeRecceivedDate == null
                        )



                        ;

            var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 5 && e.LeadFk.OrganizationId == input.OrganizationUnit).AsNoTracking()
                .Select(e => new { e.Id, e.LeadId, e.ActionId, e.ActivityDate, e.ActivityNote });

            var results = from o in filter
                          select new GetViewTrackerDto()
                          {
                              Id = o.Id,
                              LeadId = o.LeadFk.Id,
                              JobNumber = o.JobNumber,
                              JobType = o.JobTypeFk.Name,
                              JobStatus = o.JobStatusFk.Name,
                              JobStatusColorClass = o.JobStatusFk.ColorClass,
                              CompanyName = o.LeadFk.CompanyName,
                              JobActiveSmsSend = o.JobActiveSmsSend == null ? false : (bool)o.JobActiveSmsSend,
                              JobActiveSmsSendDate = o.JobActiveSmsSendDate,
                              JobActiveEmailSend = o.JobActiveEmailSend == null ? false : (bool)o.JobActiveEmailSend,
                              JobActiveEmailSendDate = o.JobActiveEmailSendDate,
                              CurruntLeadOwner = _userRepository.GetAll().Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),

                              NextFollowup = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy")).FirstOrDefault(),
                              Discription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              Comment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault()
                          };

            //var list = await results.ToListAsync();
            if (input.ExcelOrCsv == 1) // 1 use for excel , 2 use for csv
            {
                return _jobTrackerExcelExporter.JobActiveTrackerExportToFile(await results.ToListAsync(), "JobActiveTracker.xlsx");
            }
            else
            {
                return _jobTrackerExcelExporter.JobActiveTrackerExportToFile(await results.ToListAsync(), "JobActiveTracker.csv");
            }
        }

        public async Task<FileDto> GetAllJobActiveCheckActiveTrackerExport(GetAllJobActiveTrackerExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var TodayDate = (_timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredResult = _jobsRepository.GetAll()
                              .Include(e => e.LeadFk)
                              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                              .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.JobStatusId != 3 && e.SolarRebateStatus != 3 && e.FirstDepositDate != null);

            var filter = filteredResult
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber.Contains(input.Filter))
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile.Contains(input.Filter))
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName.Contains(input.Filter))
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email.Contains(input.Filter))
                        .WhereIf(input.PaymentId != 0 && input.PaymentId != null, e => e.PaymentOptionId == input.PaymentId)
                        .WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => input.JobStatusIDFilter.Contains((int)e.JobStatusId))

                       .WhereIf(input.ApplicationStatus == 1, e => e.ActiveDate == null && (e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofTypeId == null || e.RoofAngleId == null
                                || _quotationRepository.GetAll().Where(q => q.JobId == e.Id).Any() == false
                                || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id && (i.InvoicePaymentMethodFk.PaymentMethod == "EFT" || i.InvoicePaymentMethodFk.PaymentMethod == "Credit Card") && i.IsVerified == false).Any()
                                 || _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id).Any() == false) == false &&
                                 (_lookup_meterPhaseRepository.GetAll().Where(m => m.Id == e.MeterPhaseId).Any() == false || string.IsNullOrEmpty(e.PeakMeterNo) || string.IsNullOrEmpty(e.NMINumber) || _documentRepository.GetAll().Where(d => d.JobId == e.Id).Any() == false || e.ElecRetailerId == null
                                  || string.IsNullOrEmpty(e.ApplicationRefNo)
                                  || e.DistApproveDate == null
                                  || e.MeterUpgradeId == null
                                  || _documentRepository.GetAll().Where(d => d.DocumentTypeId == 3 && d.JobId == e.Id).Any() == false
                                || (_quotationRepository.GetAll().Where(q => q.JobId == e.Id && q.IsSigned == true).Any() == false && _documentRepository.GetAll().Where(d => d.JobId == e.Id && d.DocumentTypeId == 5).Any() == false)
                                 || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 || (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == false
                                 || (e.State == "VIC" ? e.ExpiryDate == null : true) == false
                                || (e.State == "VIC" ? ((e.SolarRebateStatus == 0 || e.SolarRebateStatus != 17) && e.VicRebate != "With Rebate Install" ? false : true) : true) == false) == false)

            //            .WhereIf(input.ApplicationStatus == 2, e => e.ActiveDate == null && e.HouseTypeId != null && e.ElecDistributorId != null && e.RoofAngleId != null && e.RoofTypeId != null && e.InstallerNotes != null && e.MeterPhaseId != null && e.NMINumber != null && e.PeakMeterNo != null && e.ApplicationRefNo != null && e.MeterUpgradeId != null && (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == true && e.ElecRetailerId != null && e.DepositeRecceivedDate != null
            //&& _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any()
            //&& _documentRepository.GetAll().AsNoTracking().Where(j => j.DocumentTypeId == 3 && j.JobId == e.Id).Any()
            //&& (_quotationRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id && j.IsSigned == true).Any() || _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id && j.DocumentTypeId == 5).Any()))
                        .WhereIf(input.ApplicationStatus == 2, e => (e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofTypeId == null || e.RoofAngleId == null
                                || _quotationRepository.GetAll().Where(q => q.JobId == e.Id).Any() == false
                                || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 || _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id).Any() == false) == false &&
                                 (_lookup_meterPhaseRepository.GetAll().Where(m => m.Id == e.MeterPhaseId).Any() == false || string.IsNullOrEmpty(e.PeakMeterNo) || string.IsNullOrEmpty(e.NMINumber) || _documentRepository.GetAll().Where(d => d.JobId == e.Id).Any() == false || e.ElecRetailerId == null
                                  || string.IsNullOrEmpty(e.ApplicationRefNo)
                                  || e.DistApproveDate == null
                                  || e.MeterUpgradeId == null
                                  || _documentRepository.GetAll().Where(d => d.DocumentTypeId == 3 && d.JobId == e.Id).Any() == false
                                || (_quotationRepository.GetAll().Where(q => q.JobId == e.Id && q.IsSigned == true).Any() == false && _documentRepository.GetAll().Where(d => d.JobId == e.Id && d.DocumentTypeId == 5).Any() == false)
                                 || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 || (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == false
                                 || (e.State == "VIC" ? e.ExpiryDate == null : true) == false
                                || (e.State == "VIC" ? ((e.SolarRebateStatus == 0 || e.SolarRebateStatus != 17) && e.VicRebate != "With Rebate Install" ? false : true) : true) == false
                                 || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id && (i.InvoicePaymentMethodFk.PaymentMethod == "EFT" || i.InvoicePaymentMethodFk.PaymentMethod == "Credit Card") && i.IsVerified == false).Any()
                                ))


            //            .WhereIf(input.ApplicationStatus == 3, e => e.ActiveDate == null && (e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofAngleId == null || e.RoofTypeId == null || e.InstallerNotes == null || e.MeterPhaseId == null || e.NMINumber == null || e.PeakMeterNo == null || e.ApplicationRefNo == null || e.MeterUpgradeId == null || (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified == 1 ? true : false) == false || e.ElecRetailerId == null
            //|| _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() == false
            //|| _documentRepository.GetAll().AsNoTracking().Where(j => j.DocumentTypeId == 3 && j.JobId == e.Id).Any() == false
            //|| (_quotationRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id && j.IsSigned == true).Any() || _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id && j.DocumentTypeId == 5).Any()) == false) || e.DepositeRecceivedDate == null
            //|| _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id && (i.InvoicePaymentMethodFk.PaymentMethod == "EFT" || i.InvoicePaymentMethodFk.PaymentMethod == "Credit Card") && i.IsVerified == false).Any() == true
            //            )
              .WhereIf(input.ApplicationStatus == 3, e => e.HouseTypeId == null || e.ElecDistributorId == null || e.RoofTypeId == null || e.RoofAngleId == null
                                || _quotationRepository.GetAll().Where(q => q.JobId == e.Id).Any() == false
                                || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id).Any() == false
                                 // || _invoicePaymentRepository.GetAll().Where(i => i.JobId == e.Id && (i.InvoicePaymentMethodFk.PaymentMethod == "EFT" || i.InvoicePaymentMethodFk.PaymentMethod == "Credit Card") && i.IsVerified == false).Any()
                                 || _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id).Any() == false || e.DepositeRecceivedDate == null
                        )

                        .WhereIf(input.DateType == "DepositeReceivedDate" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "DepositeReceivedDate" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ActiveDate" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ActiveDate" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date).OrderByDescending(e => e.Id)
                        ;

            var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 5 && e.LeadFk.OrganizationId == input.OrganizationUnit);

            var results = from o in filter
                          select new GetAllJobCheckActiveExcel()
                          {
                              JobNumber = o.JobNumber,
                              DepositeReceived = o.DepositeRecceivedDate != null ? Convert.ToDateTime(o.DepositeRecceivedDate).ToString("dd-MM-yyyy") : "",
                              NMINumber = o.NMINumber,
                              ApprovedReferenceNumber = o.ApplicationRefNo,
                              DistApproved = o.DistApproveDate != null ? Convert.ToDateTime(o.DistApproveDate).ToString("dd-MM-yyyy") : "",
                              MeterUpgrade = o.MeterUpgradeId == 1 ? "Yes" : o.MeterUpgradeId == 2 ? "No" : "",
                              MeterBoxPhoto = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 3 && e.JobId == o.Id).Any() ? "Yes" : "No",
                              SignedQuote = (_quotationRepository.GetAll().Where(e => e.JobId == o.Id && e.IsSigned == true).Any() || _documentRepository.GetAll().Where(e => e.JobId == o.Id && e.DocumentTypeId == 5).Any()) ? "Yes" : "No",
                              Deposit = _invoicePaymentRepository.GetAll().Where(e => e.JobId == o.Id).Any() ? "Yes" : "No",
                              ElecRetailer = o.ElecRetailerId != null ? "Yes" : "No",
                              PeakMeterNo = o.PeakMeterNo,
                              CurruntLeadOwner = _userRepository.GetAll().Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                              State = o.State,
                              PaymentOption = o.PaymentOptionFk.Name,
                              SystemCapacity = o.SystemCapacity.ToString(),
                              ActivityComment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              Notify = activity.Where(e => e.ActionId == 9 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              DistApplied = o.DistApplied,
                              DistExpire = o.DistExpiryDate,
                              NotDepRecYet = o.DepositeRecceivedDate == null && o.FirstDepositDate != null ? (int)(TodayDate.Value.Date - o.FirstDepositDate.Value.Date).TotalDays : 0,
                              NotActiveYet = o.ActiveDate == null && o.DepositeRecceivedDate != null ? (int)(TodayDate.Value.Date - o.DepositeRecceivedDate.Value.Date).TotalDays : 0,
                              NotBookedYet = o.InstallationDate == null && o.ActiveDate != null ? (int)(TodayDate.Value.Date - o.ActiveDate.Value.Date).TotalDays : 0

                          };

            //var list = await results.ToListAsync();
            if (input.ExcelOrCsv == 1) // 1 use for excel , 2 use for csv
            {
                return _jobTrackerExcelExporter.JobActiveTrackerCheckActiveExportToFile(await results.ToListAsync(), "CheckActiveJobs.xlsx");
            }
            else
            {
                return _jobTrackerExcelExporter.JobActiveTrackerCheckActiveExportToFile(await results.ToListAsync(), "CheckActiveJobs.csv");
            }
        }

        public async Task<FileDto> GetAllSTCTrackerExport(GetAllSTCTrackerExcelInput input)
        {
            int stcBlank = 0;
            int withblank = 0;
            if (input.PvdStatus != null)
            {
                if (input.PvdStatus.Contains(11))
                {
                    stcBlank = 11;
                    input.PvdStatus.Remove(11);
                    withblank = input.PvdStatus.Count;
                    input.PvdNoStatus = 2;
                }
            }

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var Today = _timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredResult = _jobsRepository.GetAll()
                              .Include(e => e.LeadFk)
                              .Include(e => e.JobStatusFk)
                              .Include(e => e.JobTypeFk)
                              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                              .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.PVDStatus != 0);

            var filter = filteredResult
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filer), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(input.PvdStatus != null && input.PvdStatus.Count() > 0 && stcBlank == 0, e => input.PvdStatus.Contains((int)e.PVDStatus))
                        .WhereIf(stcBlank != 0 && withblank == 0, e => e.PVDStatus == null)
                        .WhereIf(stcBlank != 0 && withblank > 0, e => (e.PVDStatus == null || input.PvdStatus.Contains((int)e.PVDStatus)))
                        .WhereIf(input.PvdNoStatus == 1, e => e.PVDNumber != null && e.PVDNumber != "")
                        .WhereIf(input.PvdNoStatus == 2, e => e.PVDNumber == null || e.PVDNumber == "")
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => input.JobStatusIDFilter.Contains((int)e.JobStatusId))
                        .WhereIf(input.JobTypeId != 0, e => e.JobTypeId == input.JobTypeId)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                        .WhereIf(input.PvdNo != null && input.PvdNo != "", e => e.PVDNumber == input.PvdNo)
                        .WhereIf(input.InstallerId != null && input.InstallerId != 0, e => e.InstallerId == input.InstallerId)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.LeadFk.State == input.State)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RECStatus), e => e.RECPvdStatus == input.RECStatus)

                        .WhereIf(input.DateType == "StcAppliedDate" && input.StartDate != null, e => e.STCAppliedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "StcAppliedDate" && input.EndDate != null, e => e.STCAppliedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "InstallDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "InstallDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "InstallCmpltDate" && input.StartDate != null, e => e.InstalledcompleteDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "InstallCmpltDate" && input.EndDate != null, e => e.InstalledcompleteDate.Value.Date <= EDate.Value.Date).OrderByDescending(e => e.Id)
                        ;

            var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 7 && e.LeadFk.OrganizationId == input.OrganizationUnit);

            var results = from o in filter
                          join pvdval in _pvdStatusRepository.GetAll() on o.PVDStatus equals pvdval.Id into pvd
                          from pvdval in pvd.DefaultIfEmpty()

                          select new GetViewSTCTrackerDto()
                          {
                              Id = o.Id,
                              LeadId = o.LeadFk.Id,
                              JobNumber = o.JobNumber,
                              JobTypeName = o.JobTypeFk.Name,
                              StreetNo = o.StreetNo,
                              StreetName = o.StreetName,
                              StreetType = o.StreetType,
                              Address = o.Address + ", " + o.Suburb + ", " + o.State + "-" + o.PostalCode,
                              Suburb = o.Suburb,
                              State = o.State,
                              PostalCode = o.PostalCode,
                              InstallationDate = o.InstallationDate,
                              STCAppliedDate = o.STCAppliedDate,
                              PvdNumber = o.PVDNumber == null || o.PVDNumber == "" ? "Blank" : o.PVDNumber,
                              PvdStatusName = pvdval == null || pvdval.Name == null ? "Blank" : pvdval.Name.ToString(),
                              RECPvdNumber = o.RECPvdNumber,
                              RECPvdStatus = o.RECPvdStatus,
                              JobStatusName = o.JobStatusFk.Name,
                              JobStatusColorClass = o.JobStatusFk.ColorClass,
                              SystemCapacity = o.SystemCapacity,
                              STC = o.STC,
                              STCTotalPrice = o.Rebate,
                              InstallerName = _userRepository.GetAll().Where(e => e.Id == o.InstallerId).Select(e => e.FullName).FirstOrDefault(),
                              STCNotes = o.STCNotes,
                              STCSmsSend = o.StcSmsSend,
                              STCSmsSendDate = o.StcSmsSendDate,
                              STCEmailSend = o.StcEmailSend,
                              STCEmailSendDate = o.StcEmailSendDate,
                              CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                              ActivityReminderTime = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy")).FirstOrDefault(),
                              ActivityDescription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              ActivityComment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              ActivityCommentDate = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),

                              LastUpdatedDate = o.LastModificationTime.Value.ToString("dd-MM-yyyy"),

                              NotAppliedDays = string.IsNullOrEmpty(o.PVDNumber) && o.InstalledcompleteDate != null ? (int)(Today.Value.Date - o.InstalledcompleteDate.Value.Date).TotalDays : 0,
                              NotApprovedDays = o.PVDStatus != 4 && !string.IsNullOrEmpty(o.PVDNumber) && o.InstallationDate != null ? (int)(Today.Value.Date - o.STCAppliedDate.Value.Date).TotalDays : 0,

                          };

            //var list = await results.ToListAsync();
            if (input.ExcelOrCsv == 1) // 1 use for excel , 2 use for csv
            {
                return _jobTrackerExcelExporter.STCTrackerExportToFile(await results.ToListAsync(), "STCTracker.xlsx");
            }
            else
            {
                return _jobTrackerExcelExporter.STCTrackerExportToFile(await results.ToListAsync(), "STCTracker.csv");
            }
        }

        public async Task<FileDto> GetAllGridConnectExcel(GetAllGridConnectionTrackerExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            //var Today = _timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredResult = _jobsRepository.GetAll()
                              .Include(e => e.LeadFk)
                              .Include(e => e.JobStatusFk)
                              .Include(e => e.RoofTypeFk)
                              .Include(e => e.JobTypeFk)
                              .Include(e => e.ElecRetailerFk)
                              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                              //.Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);

                              .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);



            var filter = filteredResult
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                        // .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        //.WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        //.WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        //.WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                        //.WhereIf(input.ElecDistributorId != 0, e => e.ElecDistributorId == input.ElecDistributorId)
                        //.WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => input.JobStatusIDFilter.Contains((int)e.JobStatusId))
                        //.WhereIf(input.JobTypeId != 0, e => e.JobTypeId == input.JobTypeId)
                        //.WhereIf(input.InstallerId != 0, e => e.InstallerId == input.InstallerId)

                        .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ActiveDate" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ActiveDate" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "DistAppliExpiryDate" && input.StartDate != null, e => e.DistExpiryDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "DistAppliExpiryDate" && input.EndDate != null, e => e.DistExpiryDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "InstallDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "InstallDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "InstallCmpltDate" && input.StartDate != null, e => e.InstalledcompleteDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "InstallCmpltDate" && input.EndDate != null, e => e.InstalledcompleteDate.Value.Date <= EDate.Value.Date)


                        //.WhereIf(input.ApplicationStatus == 1, e => (e.JobStatusId == 4 || e.JobStatusId == 5 || e.JobStatusId == 6 || e.JobStatusId == 7 || e.JobStatusId == 8 || e.JobStatusId == 9) && e.InstalledcompleteDate == null) //Awaiting

                        //.WhereIf(input.ApplicationStatus == 2, e => e.MeterApplyRef == null && e.InstalledcompleteDate != null) //Pending

                        ////.WhereIf(input.ApplicationStatus == 3, e => e.MeterApplyRef != null && e.InstalledcompleteDate != null) //Complete
                        //.WhereIf(input.ApplicationStatus == 3, e => e.MeterApplyRef != null && e.InstalledcompleteDate != null)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.NMINumber), e => e.NMINumber == input.NMINumber)
                        .Where(e => e.MeterApplyRef == null)

                        .AsNoTracking()
                        .Select(e => new { e.LeadFk.CompanyName, e.LeadFk.Mobile, e.LeadFk.Email, e.LeadFk.Address, e.LeadFk.PostCode, e.LeadFk.State, e.JobNumber, JobType = e.JobTypeFk.Name, e.InstallationDate, e.SystemCapacity, e.NMINumber, e.ApplicationRefNo, ElecRetailer = e.ElecRetailerFk.Name })
                        //.Where(e => e.MeterApplyRef == null)
                        ;

            var results = from o in filter
                          select new GetAllGridConnectExcelDto()
                          {
                              //Id = o.Id,
                              CompanyName = o.CompanyName,
                              Mobile = o.Mobile,
                              Email = o.Email,
                              Address = o.Address,
                              PostCode = o.PostCode,
                              State = o.State,
                              JobNumber = o.JobNumber,
                              JobType = o.JobType,
                              InstallationDate = o.InstallationDate,
                              SystemCapacity = o.SystemCapacity,
                              NMINumber = o.NMINumber,
                              ApplicationRefNo = o.ApplicationRefNo,
                              ElecRetailers = o.ElecRetailer

                          };



            if (input.ExcelOrCsv == 1) // 1 use for excel , 2 use for csv
            {
                return _jobTrackerExcelExporter.GridConnectExportToFile(await results.ToListAsync(), "GridConnectionTracker.xlsx");
            }
            else
            {
                return _jobTrackerExcelExporter.GridConnectExportToFile(await results.ToListAsync(), "GridConnectionTracker.csv");
            }
        }

        public async Task<FileDto> GetAllFreebiesTrackerExcel(GetAllFreebiesTrackerExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();
            var Today = _timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId);

            var filteredResult = _jobPromotionRepository.GetAll()
                              .Include(e => e.JobFk)
                              .Include(e => e.JobFk.LeadFk)
                              .Include(e => e.PromotionMasterFk)
                              .Include(e => e.JobFk.JobStatusFk)
                              .Include(e => e.FreebieTransportIdFk)
                              .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                              .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit && e.JobFk.JobStatusId != 3 && e.JobFk.JobStatusId != 2 && e.JobFk.SolarRebateStatus != 3 && e.PromotionMasterId != null)
                              .AsNoTracking()
                              .Select(e => new { e.Id, e.JobFk, e.JobId, e.DispatchedDate, e.PromotionMasterId, e.FreebieTransportId, e.PromotionMasterFk, e.FreebieTransportIdFk, e.TrackingNumber, e.SmsSend, e.SmsSendDate, e.EmailSend, e.EmailSendDate, e.JobFk.LeadFk.AssignToUserID, e.JobFk.LeadId, e.SerialNo });

            var invoiceInportDate = _invoiceImportDataRepository.GetAll().AsNoTracking().Select(e => new { e.JobId, e.PaidAmmount });



            var filter = filteredResult
                        .Where(e => e.AssignToUserID != null)
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Email == input.Filter)
                        .WhereIf(input.FilterName == "SerialNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.SerialNo == input.Filter)
                        .WhereIf(input.FilterName == "TrackingNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.TrackingNumber == input.Filter)
                        .WhereIf(input.PromoType != 0, e => e.PromotionMasterId == input.PromoType)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.JobFk.State == input.State)
                        .WhereIf(input.TransportType != 0, e => e.FreebieTransportId == input.TransportType)

                        .WhereIf(input.ApplicationStatus == 1, e => e.DispatchedDate == null
                        && (e.JobFk.TotalCost - (invoiceInportDate.Where(o => o.JobId == e.JobId).Select(e => e.PaidAmmount).Any() ? (invoiceInportDate.Where(o => o.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) : 0) > 0)) // Awaiting

                        .WhereIf(input.ApplicationStatus == 2, e => e.DispatchedDate == null &&
                        (e.JobFk.TotalCost - (invoiceInportDate.Where(o => o.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) <= 0)) // Pending

                        .WhereIf(input.ApplicationStatus == 3, e => e.DispatchedDate != null) // Sent
                        .WhereIf(input.ApplicationStatus == 3 && input.StartDate != null, e => e.DispatchedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.ApplicationStatus == 3 && input.EndDate != null, e => e.DispatchedDate.Value.Date <= EDate.Value.Date)
                        ;



            var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 2 && e.LeadFk.OrganizationId == input.OrganizationUnit).AsNoTracking().Select(e => new { e.ActionId, e.LeadId, e.Id, e.ActivityDate, e.ActivityNote });

            var results = from o in filter
                          select new GetViewFreebiesTrackerDto()
                          {
                              Id = o.Id,
                              JobId = o.JobId,
                              LeadId = (int)o.JobFk.LeadId,
                              JobNumber = o.JobFk.JobNumber,
                              PromotionName = o.PromotionMasterFk.Name,
                              CompanyName = o.JobFk.LeadFk.CompanyName,
                              Mobile = o.JobFk.LeadFk.Mobile,
                              Address = o.JobFk.Address,
                              Suburb = o.JobFk.Suburb,
                              State = o.JobFk.State,
                              PostalCode = o.JobFk.PostalCode,
                              Email = o.JobFk.LeadFk.Email,
                              JobStatus = o.JobFk.JobStatusFk.Name,
                              JobStatusColorClass = o.JobFk.JobStatusFk.ColorClass,
                              TotalCost = o.JobFk.TotalCost,
                              BalOwing = invoiceInportDate.Where(e => e.JobId == o.JobId).Select(e => e.PaidAmmount).Sum(),
                              TransportName = o.FreebieTransportIdFk.Name,
                              TrackingNumber = o.TrackingNumber,
                              //CurruntLeadOwner = User_List.Where(e => e.Id == o.JobFk.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                              CurruntLeadOwner = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                              NextFollowup = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                              Discription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              Comment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              SmsSend = o.SmsSend,
                              SmsSendDate = o.SmsSendDate,
                              EmailSend = o.EmailSend,
                              EmailSendDate = o.EmailSendDate,
                              PandingDays = (o.JobFk.TotalCost - invoiceInportDate.Where(e => e.JobId == o.JobId).Select(e => e.PaidAmmount).Sum()) == 0 && o.TrackingNumber == null ? (int)(Today.Value.Date - o.JobFk.InstallationDate.Value.Date).TotalDays : 0
                          };



            return _jobTrackerExcelExporter.FreebiesTrackerExportToFile(await results.ToListAsync(), input.Filename);

        }

        #endregion
    }
}
