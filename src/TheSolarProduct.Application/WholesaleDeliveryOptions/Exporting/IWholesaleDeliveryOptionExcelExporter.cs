﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.WholesaleDeliveryOption.Dto;
using TheSolarProduct.Wholesaleinvoicetype.Dtos;

namespace TheSolarProduct.WholesaleDeliveryOptions.Exporting
{
    public interface IWholesaleDeliveryOptionExcelExporter
    {
        FileDto ExportToFile(List<GetWholesaleDeliveryOptiontForViewDto> delivery);
    }
}
