﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.WholesaleDeliveryOption.Dto;
using TheSolarProduct.Wholesaleinvoicetype.Dtos;
using TheSolarProduct.WholesaleinvoicetypeAppService.Exporting;

namespace TheSolarProduct.WholesaleDeliveryOptions.Exporting
{
    public class WholesaleDeliveryOptionExcelExporter : NpoiExcelExporterBase, IWholesaleDeliveryOptionExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public WholesaleDeliveryOptionExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }


        public FileDto ExportToFile(List<GetWholesaleDeliveryOptiontForViewDto> delivery)
        {
            return CreateExcelPackage(
                "WholesaleDeliveryOption.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet("WholesaleInvoicetypes");

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, 2, delivery,
                        _ => _.Deliveryoption.Name
                        );



                });
        }
    }
}
