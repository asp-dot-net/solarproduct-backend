﻿using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Currencies.Exporting;
using TheSolarProduct.Currencies;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.WholesaleDeliveryOption;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct.TransportationCosts;
using System.Threading.Tasks;
using TheSolarProduct.PostCodes.Dtos;
using TheSolarProduct.Common;
using TheSolarProduct.Storage;
using TheSolarProduct.MultiTenancy;
using Abp.Application.Services.Dto;
using TheSolarProduct.Currencies.Dtos;
using TheSolarProduct.WholesaleDeliveryOptions;
using TheSolarProduct.WholesaleDeliveryOption.Dto;
using Abp.Collections.Extensions;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using TheSolarProduct.Dto;
using TheSolarProduct.WholesaleDeliveryOptions.Exporting;
using TheSolarProduct.WholesaleDeliveryOptions.Dto;
using TheSolarProduct.Wholesales.DataVault;






namespace TheSolarProduct.WholesaleDeliveryOptions
{
    public class WholesaleDeliveryOptionAppService : TheSolarProductAppServiceBase, IWholesaleDeliveryOptionAppService
    {
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;
        private readonly IRepository<WholesaleInvoicetype> _wholesaleInvoicetypeRepository;
        private readonly IRepository<Wholesales.Ecommerces.WholesaleDeliveryOption> _deliveryoptionRepository;
        private readonly IWholesaleDeliveryOptionExcelExporter _deliveryoptionExcelExporter;

        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;



        public WholesaleDeliveryOptionAppService(
            IRepository<Wholesales.Ecommerces.WholesaleDeliveryOption> deliveryoptionRepository,
            IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
             IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider, 
            IWholesaleDeliveryOptionExcelExporter DeliveryoptionExcelExporter)
        {

            _deliveryoptionExcelExporter = DeliveryoptionExcelExporter;
            _deliveryoptionRepository = deliveryoptionRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;

        }



        public async Task<PagedResultDto<GetWholesaleDeliveryOptiontForViewDto>> GetAll(GetAllWholesaleDeliveryoptionInput input)
        {
            var delivery = _deliveryoptionRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFiltered = delivery
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFiltered
                         select new GetWholesaleDeliveryOptiontForViewDto()
                         {
                             Deliveryoption = new DeliveryOptionDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 IsActive = o.IsActive
                             }
                         };

            var totalCount = await delivery.CountAsync();

            return new PagedResultDto<GetWholesaleDeliveryOptiontForViewDto>(totalCount, await output.ToListAsync());
        }




        public async Task<GetWholesaleDeliveryOptionForEditOutput> GetWholesaleDeliveryoptionForEdit(EntityDto input)
        {
            var delivery = await _deliveryoptionRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetWholesaleDeliveryOptionForEditOutput { Deliveryoption = ObjectMapper.Map<CreateOrEditWholesaleDeliveryOptionDto>(delivery) };

            return output;
        }


        public async Task CreateOrEdit(CreateOrEditWholesaleDeliveryOptionDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        protected virtual async Task Create(CreateOrEditWholesaleDeliveryOptionDto input)
        {
            var delivery = ObjectMapper.Map<Wholesales.Ecommerces.WholesaleDeliveryOption>(input);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 6;
            dataVaultLog.ActionNote = "Delivary Option Created: " + input.Name;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

            await _deliveryoptionRepository.InsertAsync(delivery);
        }


        protected virtual async Task Update(CreateOrEditWholesaleDeliveryOptionDto input)
        {
            var delivery = await _deliveryoptionRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 6;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "Delivery Option Updated : " + delivery.Name;
            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            var List = new List<WholeSaleDataVaultActivityLogHistory>();
            if (input.Name != delivery.Name)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = delivery.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != delivery.IsActive)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = delivery.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, delivery);

            await _deliveryoptionRepository.UpdateAsync(delivery);
        }



        public async Task Delete(EntityDto input)
        {
            var Name = _deliveryoptionRepository.Get(input.Id).Name;

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 6;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "Delivery Option Deleted  : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            await _deliveryoptionRepository.DeleteAsync(input.Id);
        }


        public async Task<FileDto> GetDeliveryoptionToExcel(GetAllWholesaleDeliveryOptionForExcelInput input)
        {

            var filteredCurrency = _deliveryoptionRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredCurrency
                         select new GetWholesaleDeliveryOptiontForViewDto()
                         {
                             Deliveryoption = new DeliveryOptionDto
                             {
                                 Name = o.Name,
                                 Id = o.Id
                             }
                         });


            var DeliveroptionListDtos = await query.ToListAsync();

            return _deliveryoptionExcelExporter.ExportToFile(DeliveroptionListDtos);
        }


    }



}

