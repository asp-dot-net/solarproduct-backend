﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.Vendors.Dtos;

namespace TheSolarProduct.Vendors.Exporting
{
    public interface IVendorExcelExporter
    {
        FileDto ExportToFile(List<GetVendorForViewDto> Vendors);
    }
}
