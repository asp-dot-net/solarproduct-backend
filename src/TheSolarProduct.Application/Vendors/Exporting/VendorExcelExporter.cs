﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.PurchaseCompanys.Dtos;
using TheSolarProduct.PurchaseCompanys.Exporting;
using TheSolarProduct.Storage;
using TheSolarProduct.Vendors.Dtos;

namespace TheSolarProduct.Vendors.Exporting
{
   
    public class VendorExcelExporter : NpoiExcelExporterBase, IVendorExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public VendorExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetVendorForViewDto> Vendors)
        {
            return CreateExcelPackage(
                "Vendors.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Vendors"));

                    AddHeader(
                        sheet,
                        L("CompanyName"),
                        L("Notes")
                    );

                    AddObjects(
                        sheet,
                        2,
                        Vendors,
                        _ => _.Vendors.CompanyName,
                        _ => _.Vendors.Notes
                    );
                }
            );
        }


    }
}
