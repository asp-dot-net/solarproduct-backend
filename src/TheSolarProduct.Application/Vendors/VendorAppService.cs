﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataVaults;
using TheSolarProduct.Dto;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Vendors.Exporting;
using TheSolarProduct.Vendors.Dtos;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using Telerik.Reporting;


namespace TheSolarProduct.Vendors
{
   
    [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_Vendor)]
    public class VendorAppService : TheSolarProductAppServiceBase, IVendorsAppService
    {
        private readonly IVendorExcelExporter _vendorExcelExporter;

        private readonly IRepository<Vendor> _vendorRepository;
        private readonly IRepository<VendorContact> _vendorContactRepository;
        
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public VendorAppService(
              IRepository<Vendor> vendorRepository,
               IRepository<VendorContact> vendorContactRepository,
              IVendorExcelExporter IVendorExcelExporter,
              IRepository<UserTeam> userTeamRepository,
              IRepository<UserRole, long> userRoleRepository,
              IRepository<Role> roleRepository,
              IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
              )
        {
            _vendorRepository = vendorRepository;
            _vendorContactRepository = vendorContactRepository;
            _vendorExcelExporter = IVendorExcelExporter;
            _userTeamRepository = userTeamRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }




        public async Task<PagedResultDto<GetVendorForViewDto>> GetAll(GetAllVendorsInput input)
        
        {
            var filtered = _vendorRepository.GetAll()
                         .WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.CompanyName == input.Filter)
                         .WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Notes == input.Filter); ;

            var pagedAndFiltered = filtered
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var outputList = from o in pagedAndFiltered
                             select new GetVendorForViewDto()
                             {
                                 Vendors = new VendorsDto
                                 {
                                     CompanyName = o.CompanyName,
                                     Address = o.Address,
                                     Notes = o.Notes,
                                    
                                     Id = o.Id
                                 }
                             };

            var totalCount = await outputList.CountAsync();

            return new PagedResultDto<GetVendorForViewDto>(
                totalCount,
                await outputList.ToListAsync()
            );
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_Vendor_Edit)]
        public async Task<CreateOrEditVendorsDto> GetVendorsForEdit(EntityDto input)
        {
           
            var vendor = await _vendorRepository.FirstOrDefaultAsync(input.Id);
            var vendorDto = ObjectMapper.Map<CreateOrEditVendorsDto>(vendor);

            var contacts = await _vendorContactRepository.GetAll().Where(e => e.VendorId == input.Id).ToListAsync();

            vendorDto.VendorContacts = new List<VendorContactDto>();
            foreach (var contact in contacts)
            {
                var vendorContacts = new VendorContactDto();
                vendorContacts.Id = contact.Id;
                vendorContacts.VendorId = contact.VendorId;
                vendorContacts.FirstName = contact.FirstName;
                vendorContacts.Email = contact.Email;
                vendorContacts.Phone = contact.Phone;
                vendorContacts.LastName = contact.LastName;

                vendorDto.VendorContacts.Add(vendorContacts);
            }

            return vendorDto;
        }


        public async Task CreateOrEdit(CreateOrEditVendorsDto input)
        {


            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_Vendor_Create)]
        protected virtual async Task Create(CreateOrEditVendorsDto input)
        {
            
            var vendor = ObjectMapper.Map<Vendor>(input);

           
            var Vendorid = await _vendorRepository.InsertAndGetIdAsync(vendor);
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 71;
            dataVaultLog.ActionNote = "vendor Created : " + input.CompanyName;
            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            foreach (var contact in input.VendorContacts)
            {
                contact.VendorId = Vendorid;
                var vendorContact = ObjectMapper.Map<VendorContact>(contact);

                await _vendorContactRepository.InsertAsync(vendorContact);
            }

           


       
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_Vendor_Edit)]
        protected virtual async Task Update(CreateOrEditVendorsDto input)
        {
            var Vendor = await _vendorRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 71;
            dataVaultLog.ActionNote = "Vendor Updated : " + Vendor.CompanyName;
            dataVaultLog.IsInventory = true;
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();
            if (input.CompanyName != Vendor.CompanyName)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "CompanyName";
                history.PrevValue = Vendor.CompanyName;
                history.CurValue = input.CompanyName;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Address != Vendor.Address)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Address";
                history.PrevValue = Vendor.Address;
                history.CurValue = input.Address;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.CreditDays != Vendor.CreditDays)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "CreditDays";
                history.PrevValue = Vendor.CreditDays.ToString();
                history.CurValue = input.CreditDays.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Notes != Vendor.Notes)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Notes";
                history.PrevValue = Vendor.Notes;
                history.CurValue = input.Notes;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            var ContactIds = await _vendorContactRepository.GetAll().Where(e => e.VendorId == Vendor.Id).ToListAsync();
            var CurrentContactIds = input.VendorContacts != null ? input.VendorContacts.Select(e => e.Id).ToList() : new List<int?>();
            var listcontact = ContactIds.Where(e => !CurrentContactIds.Contains(e.Id));

            foreach (var contact in listcontact)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();

                history.FieldName = "Contact Deleted";
                history.PrevValue = "";
                history.CurValue = contact.FirstName;
                history.Action = "Edit";
                //history.WholeS = wholeSaleLead.Id;
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);

                await _vendorContactRepository.DeleteAsync(contact);
            }
            foreach (var contact in input.VendorContacts)
            {
                contact.VendorId = Vendor.Id;
                if ((contact.Id != null ? contact.Id : 0) == 0)
                {
                    DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();

                    history.FieldName = "Contact Added";
                    history.PrevValue = "";
                    history.CurValue = contact.FirstName;
                    history.Action = "Edit";
                   
                    history.ActivityLogId = dataVaultLogId;
                    List.Add(history);
                    var vendorContact = ObjectMapper.Map<VendorContact>(contact);
                    await _vendorContactRepository.InsertAsync(vendorContact);
                }

                else
                {
                    var vendorContact = await _vendorContactRepository.GetAsync((int)contact.Id);
                    if (contact.FirstName != vendorContact.FirstName)
                    {
                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                        history.FieldName = "FirstName";
                        history.PrevValue = vendorContact.FirstName;
                        history.CurValue = contact.FirstName;
                        history.Action = "Update";
                        history.ActivityLogId = dataVaultLogId;
                        List.Add(history);
                    }
                    if (contact.LastName != vendorContact.LastName)
                    {
                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                        history.FieldName = "LastName";
                        history.PrevValue = vendorContact.LastName;
                        history.CurValue = contact.LastName;
                        history.Action = "Update";
                        history.ActivityLogId = dataVaultLogId;
                        List.Add(history);
                    }
                    if (contact.Email != vendorContact.Email)
                    {
                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                        history.FieldName = "Email";
                        history.PrevValue = vendorContact.Email;
                        history.CurValue = contact.Email;
                        history.Action = "Update";
                        history.ActivityLogId = dataVaultLogId;
                        List.Add(history);
                    }
                    if (contact.Phone != vendorContact.Phone)
                    {
                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                        history.FieldName = "Phone";
                        history.PrevValue = vendorContact.Phone;
                        history.CurValue = contact.Phone;
                        history.Action = "Update";
                        history.ActivityLogId = dataVaultLogId;
                        List.Add(history);
                    }

                        ObjectMapper.Map(contact, vendorContact); 
                        await _vendorContactRepository.UpdateAsync(vendorContact); 
                    

                }

            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
            ObjectMapper.Map(input, Vendor);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_Vendor_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _vendorRepository.Get(input.Id).CompanyName;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 71;
            dataVaultLog.ActionNote = "Vendor Deleted : " + Name;

            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            await _vendorRepository.DeleteAsync(input.Id);

        }

      
    }
}
