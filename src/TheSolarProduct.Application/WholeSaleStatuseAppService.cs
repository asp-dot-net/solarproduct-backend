﻿using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.WholeSaleStatuses.Dtos;
using Abp.Authorization;
using TheSolarProduct.DataVaults;
using TheSolarProduct.Wholesales.DataVault;
using Abp.EntityFrameworkCore;
using System.Collections.Generic;
using TheSolarProduct.CheckActives;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.WholeSaleStatuses
{
    [AbpAuthorize]
    public class WholeSaleStatuseAppService : TheSolarProductAppServiceBase, IWholeSaleStatuseAppService
    {

        private readonly IRepository<WholeSaleStatus> _wholeSaleStatusRepository;
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public WholeSaleStatuseAppService(
            IRepository<WholeSaleStatus> wholeSaleStatusRepository,
            IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            )
        {
            _wholeSaleStatusRepository = wholeSaleStatusRepository;
            _WholeSaleDataVaultActivityRepository= WholeSaleDataVaultActivityRepository;
            _dbcontextprovider= dbcontextprovider;
        }

        public async Task CreateOrEdit(WholeSaleStatusDto input)
        {
            if (input.Id == null || input.Id == 0)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(WholeSaleStatusDto input)
        {
            var wholeSaleStatus = ObjectMapper.Map<WholeSaleStatus>(input);

            await _wholeSaleStatusRepository.InsertAndGetIdAsync(wholeSaleStatus);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 1;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "wholeSale Status Created : " + input.Name;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

        }

        protected virtual async Task Update(WholeSaleStatusDto input)
        {

            var wholeSaleStatus = await _wholeSaleStatusRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 1;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "wholeSale Status Updated : " + wholeSaleStatus.Name;

            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<WholeSaleDataVaultActivityLogHistory>();

            if (input.Name != wholeSaleStatus.Name)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = wholeSaleStatus.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            

            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, wholeSaleStatus);

        }

        public async Task Delete(EntityDto input)
        {
            var Name = _wholeSaleStatusRepository.Get(input.Id).Name;
            await _wholeSaleStatusRepository.DeleteAsync(input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 1;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "wholeSale Status Deleted : " + Name;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);
        }

        public async Task<PagedResultDto<GetWholeSaleStatusForViewDto>> GetAll(GetAllWholeSaleLeadStatusInput input)
        {
            var filteredWholeSaleStatus = _wholeSaleStatusRepository.GetAll()
                       .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var pagedAndFilteredWholeSaleStatus = filteredWholeSaleStatus
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var wholeSaleStatus = from o in pagedAndFilteredWholeSaleStatus
                                  select new GetWholeSaleStatusForViewDto()
                              {
                                WholeSaleStatus = new WholeSaleStatusDto()
                                {
                                    Id = o.Id,
                                    Name = o.Name,
                                }
                                  
                              };

            var totalCount = await filteredWholeSaleStatus.CountAsync();

            return new PagedResultDto<GetWholeSaleStatusForViewDto>(
               totalCount,
               await wholeSaleStatus.ToListAsync()
           );
        }

        public async Task<WholeSaleStatusDto> GetLeadSourceForEdit(EntityDto input)
        {
            var wholeSaleStatus = await _wholeSaleStatusRepository.FirstOrDefaultAsync(input.Id);

            var output =  ObjectMapper.Map<WholeSaleStatusDto>(wholeSaleStatus);


            return output;
        }
    }
}
