﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using System.Linq;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.InstallationCost.InstallationItemList.Dtos;
using TheSolarProduct.Jobs;
using System.Collections.Generic;
using System;
using Abp.Timing.Timezone;
using TheSolarProduct.CategoryInstallations;
using TheSolarProduct.CategoryInstallationItemLists.Dtos;
using TheSolarProduct.DataVaults;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using Abp.Organizations;
using Abp.Collections.Extensions;

namespace TheSolarProduct.CategoryInstallationItemLists
{
    public class CategoryInstallationItemListAppService : TheSolarProductAppServiceBase, ICategoryInstallationItemListAppService
    {

        private readonly IRepository<CategoryInstallationItemPeriod> _categoryInstallationItemPeriodRepository;
        private readonly IRepository<CategoryInstallationItemList> _categoryInstallationItemListRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;

        public CategoryInstallationItemListAppService(
            IRepository<CategoryInstallationItemPeriod> categoryInstallationItemPeriodRepository,
            IRepository<CategoryInstallationItemList> categoryInstallationItemListRepository,
            ITimeZoneConverter timeZoneConverter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            , IRepository<OrganizationUnit, long> organizationUnitRepository
            ) {
            _categoryInstallationItemListRepository = categoryInstallationItemListRepository;
            _categoryInstallationItemPeriodRepository = categoryInstallationItemPeriodRepository;
            _timeZoneConverter = timeZoneConverter;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _organizationUnitRepository = organizationUnitRepository;

        }

        public async Task<List<CategoryInstallationItemPeriodForViewDto>> CheckExistInstallationItemList(CreateOrEditCategoryInstallationItemPeriodDto input)
        {

            input.StartDate = (DateTime)_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId);
            input.EndDate = (DateTime)_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId);

            var filteredOutput = _categoryInstallationItemPeriodRepository.GetAll().Where(e => ((e.StartDate >= input.StartDate && e.EndDate <= input.EndDate) || (e.StartDate <= input.StartDate && e.EndDate >= input.EndDate)) && e.OrganizationUnit == input.OrganizationUnit);

            if (input.Id != null || input.Id != 0)
            {
                filteredOutput = filteredOutput.Where(e => e.Id != input.Id);
            }

            var output = from o in filteredOutput
                         select new CategoryInstallationItemPeriodForViewDto()
                         {
                             CategoryInstallationItemPeriod = new CategoryInstallationItemPeriodDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 OrganizationUnit = o.OrganizationUnitFk.DisplayName.ToString(),
                                 StartDate = o.StartDate.Date,
                                 EndDate = o.EndDate.Date
                             }
                         };

            return output.ToList();
        }

        public async Task CreateOrEdit(CreateOrEditCategoryInstallationItemPeriodDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_CategoryInstallationItemList_Create)]
        protected virtual async Task Create(CreateOrEditCategoryInstallationItemPeriodDto input)
        {
            input.StartDate = (DateTime)_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId);
            input.EndDate = (DateTime)_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId);

            var installationItemPeriod = ObjectMapper.Map<CategoryInstallationItemPeriod>(input);

            var installationItemPeriodId = await _categoryInstallationItemPeriodRepository.InsertAndGetIdAsync(installationItemPeriod);

            foreach (var item in input.CategoryInstallationItemList)
            {
                var installationItemList = ObjectMapper.Map<CategoryInstallationItemList>(item);
                installationItemList.CategoryInstallationItemPeriodId = installationItemPeriodId;

                await _categoryInstallationItemListRepository.InsertAndGetIdAsync(installationItemList);
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 53;
            dataVaultLog.ActionNote = "Category Installation Item Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_CategoryInstallationItemList_Edit)]
        protected virtual async Task Update(CreateOrEditCategoryInstallationItemPeriodDto input)
        {
            input.StartDate = (DateTime)_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId);
            input.EndDate = (DateTime)_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId);

            var installationItemPeriod = await _categoryInstallationItemPeriodRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 53;
            dataVaultLog.ActionNote = "Category Installation Item Updated : " + installationItemPeriod.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != installationItemPeriod.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = installationItemPeriod.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.OrganizationUnit != installationItemPeriod.OrganizationUnit)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Organization";
                history.PrevValue = _organizationUnitRepository.Get(installationItemPeriod.OrganizationUnit).DisplayName;
                history.CurValue = _organizationUnitRepository.Get(input.OrganizationUnit).DisplayName;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.StartDate.Date != installationItemPeriod.StartDate.Date)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "StartDate";
                history.PrevValue = installationItemPeriod.StartDate.ToString("dd-MM-yyyy");
                history.CurValue = input.StartDate.ToString("dd-MM-yyyy");
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.EndDate.Date != installationItemPeriod.EndDate.Date)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "EndDate";
                history.PrevValue = installationItemPeriod.EndDate.ToString("dd-MM-yyyy");
                history.CurValue = input.EndDate.ToString("dd-MM-yyyy");
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            ObjectMapper.Map(input, installationItemPeriod);

            var Items = _categoryInstallationItemListRepository.GetAll().Where(e => e.CategoryInstallationItemPeriodId == input.Id).ToList();

            var Ids = input.CategoryInstallationItemList.Where(e => e.Id > 0).Select(e => e.Id).ToList();

            var deleteItems = Items.Where(e => !Ids.Contains(e.Id)).ToList();

            foreach (var item in deleteItems)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Item Delete";
                history.PrevValue = item.CategoryName + " " + item.StartValue + " - " + item.EndValue;
                history.CurValue = "";
                history.Action = "Delete";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _categoryInstallationItemListRepository.HardDeleteAsync(e => e.CategoryInstallationItemPeriodId == input.Id);
            foreach (var item in input.CategoryInstallationItemList)
            {
                var installationItemList = ObjectMapper.Map<CategoryInstallationItemList>(item);
                installationItemList.CategoryInstallationItemPeriodId = installationItemPeriod.Id;

                await _categoryInstallationItemListRepository.InsertAndGetIdAsync(installationItemList);

                if (item.Id > 0)
                {
                    var updateItem = Items.Where(e => e.Id == item.Id).FirstOrDefault();

                    if (item.CategoryName != updateItem.CategoryName || item.StartValue != updateItem.StartValue || item.EndValue != updateItem.EndValue)
                    {
                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                        history.FieldName = "Item Updated";
                        history.PrevValue = updateItem.CategoryName + " " + updateItem.StartValue + " - " + updateItem.EndValue;
                        history.CurValue = item.CategoryName + " " + item.StartValue + " - " + item.EndValue;
                        history.Action = "Update";
                        history.ActivityLogId = dataVaultLogId;
                        List.Add(history);
                    }
                    

                }
                else
                {
                    DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                    history.FieldName = "Item Added";
                    history.PrevValue = item.CategoryName + " " + item.StartValue + " - " + item.EndValue;
                    history.CurValue = "";
                    history.Action = "Added";
                    history.ActivityLogId = dataVaultLogId;
                    List.Add(history);
                }


            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_CategoryInstallationItemList_Delete)]
        public async Task Delete(EntityDto input)
        {
            var installationItemPeriod = await _categoryInstallationItemPeriodRepository.GetAsync((int)input.Id);
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 53;
            dataVaultLog.ActionNote = "Category Installation Item Deleted : " + installationItemPeriod.Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _categoryInstallationItemPeriodRepository.DeleteAsync(input.Id);
            await _categoryInstallationItemListRepository.DeleteAsync(e => e.CategoryInstallationItemPeriodId == input.Id);
        }

        public async Task<PagedResultDto<CategoryInstallationItemPeriodForViewDto>> GetAll(GetAllCategoryInstallationItemListInput input)
        {
            var filteredOutput = _categoryInstallationItemPeriodRepository.GetAll()
    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
    .WhereIf(input.OrganizationUnit != 0 && input.OrganizationUnit != null, e => e.OrganizationUnit == input.OrganizationUnit);


            var pagedAndFilteredOutput = filteredOutput
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var today = DateTime.Now;
            var firstDayOfMonth = new DateTime(today.Year, today.Month, 1);
            var output = from o in pagedAndFilteredOutput
                         select new CategoryInstallationItemPeriodForViewDto()
                         {
                             CategoryInstallationItemPeriod = new CategoryInstallationItemPeriodDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 OrganizationUnit = o.OrganizationUnitFk.DisplayName.ToString(),
                                 StartDate = o.StartDate.Date,
                                 EndDate = o.EndDate.Date
                             },
                             IsEdit = o.EndDate.Date < firstDayOfMonth.Date ? false : true
                         };

            var totalCount = await filteredOutput.CountAsync();

            return new PagedResultDto<CategoryInstallationItemPeriodForViewDto>(
                totalCount,
                await output.ToListAsync()
            );
        }


        public async Task<GetCategoryInstallationItemPeriodForEditOutput> GetInstallationItemPeriodForEdit(EntityDto input)
        {
            var result = await _categoryInstallationItemPeriodRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetCategoryInstallationItemPeriodForEditOutput { CategoryInstallationItemPeriod = ObjectMapper.Map<CreateOrEditCategoryInstallationItemPeriodDto>(result) };

            var resultItemList = _categoryInstallationItemListRepository.GetAll().Where(e => e.CategoryInstallationItemPeriodId == input.Id);
            output.CategoryInstallationItemPeriod.CategoryInstallationItemList = (from o in resultItemList
                                                                  select new CreateOrEditCategoryInstallationItemListDto()
                                                                  {
                                                                      Id = o.Id,
                                                                      CategoryInstallationItemPeriodId = o.CategoryInstallationItemPeriodId,
                                                                      CategoryName = o.CategoryName,
                                                                      StartValue = o.StartValue,
                                                                      EndValue = o.EndValue,
                                                                  }).ToList();

            return output;
        }

        public async Task<GetCategoryInstallationItemPeriodForEditOutput> GetInstallationItemPeriodForView(EntityDto input)
        {

            var result = _categoryInstallationItemPeriodRepository.GetAll().Where(e => e.Id == input.Id);
            var resultItemList = _categoryInstallationItemListRepository.GetAll().Where(e => e.CategoryInstallationItemPeriodId == input.Id);

            var output = await (from o in result
                                select new GetCategoryInstallationItemPeriodForEditOutput
                                {
                                    CategoryInstallationItemPeriod = new CreateOrEditCategoryInstallationItemPeriodDto
                                    {
                                        Id = o.Id,
                                        Organization = o.OrganizationUnitFk.DisplayName.ToString(),
                                        Name = o.Name,
                                        StartDate = o.StartDate,
                                        EndDate = o.EndDate,
                                        CategoryInstallationItemList = (from r in resultItemList
                                                                        select new CreateOrEditCategoryInstallationItemListDto()
                                                                        {
                                                                            Id = r.Id,
                                                                            CategoryName = r.CategoryName,
                                                                            StartValue = r.StartValue,
                                                                            EndValue = r.EndValue,
                                                                        }).ToList()
                                    },
                                }).FirstOrDefaultAsync();


            return output;
        }
    }
}
