﻿using System.Collections.Generic;
using TheSolarProduct.RejectReasons.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.RejectReasons.Exporting
{
    public interface IRejectReasonsExcelExporter
    {
        FileDto ExportToFile(List<GetRejectReasonForViewDto> rejectReasons);
    }
}