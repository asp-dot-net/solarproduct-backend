﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.RejectReasons.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.RejectReasons.Exporting
{
    public class RejectReasonsExcelExporter : NpoiExcelExporterBase, IRejectReasonsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RejectReasonsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRejectReasonForViewDto> rejectReasons)
        {
            return CreateExcelPackage(
                "RejectReasons.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("RejectReasons"));

                    AddHeader(
                        sheet,
                        L("RejectReasonName"),
                         L("IsActive")
                        );

                    AddObjects(
                        sheet, 2, rejectReasons,
                        _ => _.RejectReason.RejectReasonName,
                         _ => _.RejectReason.IsActive ? L("Yes") : L("No")
                        );

					
					
                });
        }
    }
}
