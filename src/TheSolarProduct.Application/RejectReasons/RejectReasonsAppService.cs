﻿
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.RejectReasons.Exporting;
using TheSolarProduct.RejectReasons.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.CancelReasons;
using TheSolarProduct.PostCodes;

namespace TheSolarProduct.RejectReasons
{
	[AbpAuthorize(AppPermissions.Pages_RejectReasons)]
    public class RejectReasonsAppService : TheSolarProductAppServiceBase, IRejectReasonsAppService
    {
		 private readonly IRepository<RejectReason> _rejectReasonRepository;
		 private readonly IRejectReasonsExcelExporter _rejectReasonsExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;


        public RejectReasonsAppService(IRepository<RejectReason> rejectReasonRepository, IRejectReasonsExcelExporter rejectReasonsExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider) 
		  {
			_rejectReasonRepository = rejectReasonRepository;
			_rejectReasonsExcelExporter = rejectReasonsExcelExporter;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
			
		  }

		 public async Task<PagedResultDto<GetRejectReasonForViewDto>> GetAll(GetAllRejectReasonsInput input)
         {
			
			var filteredRejectReasons = _rejectReasonRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.RejectReasonName.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.RejectReasonNameFilter),  e => e.RejectReasonName == input.RejectReasonNameFilter);

			var pagedAndFilteredRejectReasons = filteredRejectReasons
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

			var rejectReasons = from o in pagedAndFilteredRejectReasons
                         select new GetRejectReasonForViewDto() {
							RejectReason = new RejectReasonDto
							{
                                RejectReasonName = o.RejectReasonName,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						};

            var totalCount = await filteredRejectReasons.CountAsync();

            return new PagedResultDto<GetRejectReasonForViewDto>(
                totalCount,
                await rejectReasons.ToListAsync()
            );
         }
		 
		 public async Task<GetRejectReasonForViewDto> GetRejectReasonForView(int id)
         {
            var rejectReason = await _rejectReasonRepository.GetAsync(id);

            var output = new GetRejectReasonForViewDto { RejectReason = ObjectMapper.Map<RejectReasonDto>(rejectReason) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_RejectReasons_Edit)]
		 public async Task<GetRejectReasonForEditOutput> GetRejectReasonForEdit(EntityDto input)
         {
            var rejectReason = await _rejectReasonRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetRejectReasonForEditOutput {RejectReason = ObjectMapper.Map<CreateOrEditRejectReasonDto>(rejectReason)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditRejectReasonDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_RejectReasons_Create)]
		 protected virtual async Task Create(CreateOrEditRejectReasonDto input)
         {
            var rejectReason = ObjectMapper.Map<RejectReason>(input);

			
			if (AbpSession.TenantId != null)
			{
				rejectReason.TenantId = (int) AbpSession.TenantId;
			}

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 6;
            dataVaultLog.ActionNote = "Reject Reason Created : " + input.RejectReasonName;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _rejectReasonRepository.InsertAsync(rejectReason);
         }

		 [AbpAuthorize(AppPermissions.Pages_RejectReasons_Edit)]
		 protected virtual async Task Update(CreateOrEditRejectReasonDto input)
         {
            var rejectReason = await _rejectReasonRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 6;
            dataVaultLog.ActionNote = "Reject Reason Updated : " + rejectReason.RejectReasonName;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.RejectReasonName != rejectReason.RejectReasonName)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "RejectReasonName";
                history.PrevValue = rejectReason.RejectReasonName;
                history.CurValue = input.RejectReasonName;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != rejectReason.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = rejectReason.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, rejectReason);
         }

		 [AbpAuthorize(AppPermissions.Pages_RejectReasons_Delete)]
         public async Task Delete(EntityDto input)
         {

            var Name = _rejectReasonRepository.Get(input.Id).RejectReasonName;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 6;
            dataVaultLog.ActionNote = "Reject Reason Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _rejectReasonRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetRejectReasonsToExcel(GetAllRejectReasonsForExcelInput input)
         {
			
			var filteredRejectReasons = _rejectReasonRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.RejectReasonName.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.RejectReasonNameFilter),  e => e.RejectReasonName == input.RejectReasonNameFilter);

			var query = (from o in filteredRejectReasons
                         select new GetRejectReasonForViewDto() { 
							RejectReason = new RejectReasonDto
							{
                                RejectReasonName = o.RejectReasonName,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						 });


            var rejectReasonListDtos = await query.ToListAsync();

            return _rejectReasonsExcelExporter.ExportToFile(rejectReasonListDtos);
         }


    }
}