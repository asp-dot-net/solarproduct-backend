﻿using System.Collections.Generic;
using TheSolarProduct.Departments.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Departments.Exporting
{
    public interface IDepartmentsExcelExporter
    {
        FileDto ExportToFile(List<GetDepartmentForViewDto> departments);
        FileDto ExportToFile(object departmentListDto);
    }
}