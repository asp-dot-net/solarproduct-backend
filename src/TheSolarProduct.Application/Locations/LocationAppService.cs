﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System.Threading.Tasks;
using TheSolarProduct.Jobs;
using TheSolarProduct.Locations.Dtos;
using Abp.Linq.Extensions;
using System.Linq;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using Abp.EntityFrameworkCore;
using TheSolarProduct.Quotations;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using System.Collections.Generic;

namespace TheSolarProduct.Locations
{
    [AbpAuthorize]
    public class LocationAppService : TheSolarProductAppServiceBase, ILocationAppService
    {
        private readonly IRepository<Warehouselocation> _warehouseLocationRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public LocationAppService(IRepository<Warehouselocation> warehouseLocationRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _warehouseLocationRepository = warehouseLocationRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
        }

        public async Task<PagedResultDto<GetLocationForViewDto>> GetAll(GetAllLocationForInput input)
        {
            var filtered = _warehouseLocationRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.location.Contains(input.Filter));

            var pagedAndFiltered = filtered
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFiltered
                           select new GetLocationForViewDto()
                           {
                               Location = new LocationDto
                               {
                                   location = o.location,
                                   Address = o.Address,
                                   state = o.state,
                                   UnitNo=o.UnitNo,
                                   UnitType=o.UnitType,
                                   StreetNo=o.StreetNo,
                                   StreetName=o.StreetName,
                                   StreetType=o.StreetType,
                                   PostCode=o.PostCode,
                                   Latitude=o.Latitude,
                                   Longitude=o.Longitude,
                                   Suburb=o.Suburb,
                                   Id = o.Id,
                               }
                           };

            var totalCount = await output.CountAsync();

            return new PagedResultDto<GetLocationForViewDto>(
                totalCount,
                await output.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_WarehouseLocation_Edit)]
        public async Task<GetLocationForEditOutput> GetLocationForEdit(EntityDto input)
        {
            var result = await _warehouseLocationRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLocationForEditOutput { Location = ObjectMapper.Map<CreateOrEditLocationDto>(result) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditLocationDto input)
        {
            if (input.Id == null)
            {
                //await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        //[AbpAuthorize(AppPermissions.Pages_PostCodeRange_Create)]
        //protected virtual async Task Create(CreateOrEditPostCodeRangeDto input)
        //{
        //    var postCodeRange = ObjectMapper.Map<PostCodeRange>(input);
        //    await _postCodeRangeRepository.InsertAsync(postCodeRange);
        //}

        [AbpAuthorize(AppPermissions.Pages_WarehouseLocation_Edit)]
        protected virtual async Task Update(CreateOrEditLocationDto input)
        {
            var postCode = await _warehouseLocationRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 41;
            dataVaultLog.ActionNote = "Warehouse LocationUpdated : " + postCode.location;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.location != postCode.location)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "location";
                history.PrevValue = postCode.location;
                history.CurValue = input.location;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.state != postCode.state)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "state";
                history.PrevValue = postCode.state;
                history.CurValue = input.state;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Address != postCode.Address)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Address";
                history.PrevValue = postCode.Address;
                history.CurValue = input.Address;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.UnitNo != postCode.UnitNo)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "UnitNo";
                history.PrevValue = postCode.UnitNo;
                history.CurValue = input.UnitNo;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.UnitType != postCode.UnitType)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "UnitType";
                history.PrevValue = postCode.UnitType;
                history.CurValue = input.UnitType;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.StreetNo != postCode.StreetNo)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "StreetNo";
                history.PrevValue = postCode.StreetNo;
                history.CurValue = input.StreetNo;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.StreetName != postCode.StreetName)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "StreetName";
                history.PrevValue = postCode.StreetName;
                history.CurValue = input.StreetName;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.StreetType != postCode.StreetType)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "StreetType";
                history.PrevValue = postCode.StreetType;
                history.CurValue = input.StreetType;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.PostCode != postCode.PostCode)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "PostCode";
                history.PrevValue = postCode.PostCode;
                history.CurValue = input.PostCode;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Latitude != postCode.Latitude)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Latitude";
                history.PrevValue = postCode.Latitude;
                history.CurValue = input.Latitude;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Suburb != postCode.Suburb)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Suburb";
                history.PrevValue = postCode.Suburb;
                history.CurValue = input.Suburb;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Longitude != postCode.Longitude)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Longitude";
                history.PrevValue = postCode.Longitude;
                history.CurValue = input.Longitude;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, postCode);
        }

        //[AbpAuthorize(AppPermissions.Pages_PostCodeRange_Delete)]
        //public async Task Delete(EntityDto input)
        //{
        //    await _postCodeRangeRepository.DeleteAsync(input.Id);
        //}
    }
}
