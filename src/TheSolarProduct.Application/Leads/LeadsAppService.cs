﻿using Abp;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Net.Mail;
using Abp.Notifications;
using Abp.Organizations;
using Abp.Timing;
using Abp.Timing.Timezone;
using Abp.UI;
using Hangfire.Storage.Monitoring;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using NUglify.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.Authorization;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.CancelReasons;
using TheSolarProduct.Dto;
using TheSolarProduct.EmailTemplates;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Invoices;
using TheSolarProduct.JobHistory;
using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.LeadActions;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.LeadHistory;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.Leads.Exporting;
using TheSolarProduct.LeadSources;
using TheSolarProduct.LeadStatuses;
using TheSolarProduct.MyInstallerActivityLogs;
using TheSolarProduct.Notifications;
using TheSolarProduct.Organizations;
using TheSolarProduct.PostCodes;
using TheSolarProduct.PreviousJobStatuses;
using TheSolarProduct.Promotions;
using TheSolarProduct.Quotations;
using TheSolarProduct.RejectReasons;
using TheSolarProduct.ReviewTypes;
using TheSolarProduct.Sections;
using TheSolarProduct.SmsTemplates;
using TheSolarProduct.States;
using TheSolarProduct.StreetNames;
using TheSolarProduct.StreetTypes;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Timing;
using TheSolarProduct.UnitTypes;
using TheSolarProduct.UserActivityLogs;
using TheSolarProduct.UserActivityLogs.Dtos;
using TheSolarProduct.UserWiseEmailOrgs;

namespace TheSolarProduct.Leads
{
    //[AbpAuthorize(AppPermissions.Pages_Leads, AppPermissions.Pages_LeadTracker, AppPermissions.Pages_Lead_Closed, AppPermissions.Pages_MyLeads, AppPermissions.Pages_Leads_Create,
    //	AppPermissions.Pages_Leads_Edit, AppPermissions.Pages_LeadSources_Delete, AppPermissions.Pages_Lead_Duplicate, AppPermissions.Pages_Leads_Assign, AppPermissions.Pages_Jobs)]
    [AbpAuthorize(AppPermissions.Pages)]
    public class LeadsAppService : TheSolarProductAppServiceBase, ILeadsAppService
    {
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<LeadAction, int> _lookup_leadActionRepository;
        private readonly ILeadsExcelExporter _leadsExcelExporter;
        private readonly IRepository<PostCode, int> _lookup_postCodeRepository;
        private readonly IRepository<State, int> _lookup_stateRepository;
        private readonly IRepository<LeadSource, int> _lookup_leadSourceRepository;
        private readonly IRepository<LeadStatus, int> _lookup_leadStatusRepository;
        private readonly IRepository<UnitType, int> _lookup_unitTypeRepository;
        private readonly IRepository<StreetName, int> _lookup_streetNameRepository;
        private readonly IRepository<StreetType, int> _lookup_streetTypeRepository;
        private readonly IRepository<RejectReason, int> _lookup_rejectReasonRepository;
        private readonly IRepository<CancelReason, int> _lookup_cancelReasonRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<JobStatus, int> _lookup_jobStatusRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<OrganizationUnitRole, long> _organizationUnitRoleRepository;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<Team> _teamRepository;
        private readonly IUserEmailer _userEmailer;
        private readonly IRepository<EmailTemplate> _emailTemplateRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<Quotation> _quotationRepository;
        private readonly IRepository<SmsTemplate> _smsTemplateRepository;
        private readonly IRepository<JobPromotion> _jobPromotionRepository;
        private readonly IRepository<FreebieTransport> _freebieTransportRepository;
        private readonly IRepository<JobRefund> _jobRefundRepository;
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
        private readonly IRepository<PreviousJobStatus> _previousJobStatusRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<PromotionUser> _PromotionUsersRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<JobVariation> _jobVariationRepository;
        private readonly IRepository<PostCode> _postCodeRepository;
        private readonly IRepository<LeadtrackerHistory> _LeadtrackerHistoryRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<ProductItem> _ProductItemRepository;
        private readonly NotificationAppService _NotificationAppService;
        private readonly IRepository<Section> _SectionAppService;
        private readonly IRepository<InvoiceImportData> _InvoiceImportDataRepository;
        private readonly IRepository<Document> _DocumentRepository;
        private readonly IRepository<Jobwarranty> _JobwarrantyRepository;
        private readonly IRepository<EmailTempData> _EmailTempDataRepository;
        private readonly IRepository<PromotionMaster> _promotionMasterRepository;
        private readonly IRepository<JobTrackerHistory> _JobHistoryRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendedOrganizationUnitRepository;
        private readonly IRepository<UserWiseEmailOrg> _UserWiseEmailOrgRepository;
        private readonly IRepository<ReviewType> _reviewTypeRepository;
        private readonly IRepository<MyInstallerActivityLog> _myinstalleractivityRepository;
        private readonly IRepository<Warehouselocation> _warehouselocationRepository;
        private readonly IRepository<LeadSource> _leadStatus;
        private readonly ITimeZoneService _timeZoneService;
        private readonly SettingManager _settingManager;
        private readonly IRepository<PylonDocument> _pylonDocumentRepository;
        private readonly IRepository<PostCodeRange.PostCodeRange> _postCodeRangeRepository;
        private readonly IUserActivityLogAppService _userActivityLogServiceProxy;
        public LeadsAppService(IRepository<Lead> leadRepository
            , ILeadsExcelExporter leadsExcelExporter
            , IRepository<PostCode, int> lookup_postCodeRepository
            , IRepository<State, int> lookup_stateRepository
            , IRepository<LeadSource, int> lookup_leadSourceRepository
            , IRepository<LeadStatus, int> lookup_leadStatusRepository
            , IRepository<UnitType, int> lookup_unitTypeRepository
            , IRepository<StreetName, int> lookup_streetNameRepository
            , IRepository<StreetType, int> lookup_streetTypeRepository
            , IRepository<RejectReason, int> lookup_rejectReasonRepository
            , IRepository<CancelReason, int> lookup_cancelReasonRepository
            , IRepository<LeadActivityLog> leadactivityRepository
            , IRepository<LeadAction, int> lookup_leadActionRepository
            , IRepository<User, long> userRepository
            , IRepository<UserRole, long> userroleRepository
            , IRepository<Role> roleRepository
            , IRepository<UserTeam> userTeamRepository
            , IAppNotifier appNotifier
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<OrganizationUnit, long> organizationUnitRepository
            , IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository
            , IRepository<OrganizationUnitRole, long> organizationUnitRoleRepository
            , IApplicationSettingsAppService applicationSettings
            , IEmailSender emailSender
            , IRepository<Team> teamRepository
            , IUserEmailer userEmailer
            , IRepository<EmailTemplate> emailTemplateRepository
            , UserManager userManager
            , IRepository<JobStatus, int> lookup_jobStatusRepository
            , IRepository<Job> jobRepository
            , IRepository<Quotation> quotationRepository,
              IRepository<SmsTemplate> smsTemplateRepository,
              IRepository<JobPromotion> jobPromotionRepository,
              IRepository<FreebieTransport> freebieTransportRepository,
              IRepository<JobRefund> jobRefundRepository,
              IRepository<InvoicePayment> invoicePaymentRepository,
              IRepository<PreviousJobStatus> previousJobStatusRepository
            , ITimeZoneConverter timeZoneConverter,
              IRepository<PromotionUser> PromotionUsersRepository
            , IRepository<JobVariation> jobVariationRepository
            , IRepository<JobProductItem> jobProductItemRepository,
               IRepository<PostCode> postCodeRepository,
               IRepository<LeadtrackerHistory> LeadtrackerHistoryRepository
            , IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
               IRepository<ProductItem> ProductItemRepository,
               NotificationAppService NotificationAppService,
               IRepository<Section> SectionAppService,
               IRepository<InvoiceImportData> InvoiceImportDataRepository,
               IRepository<Document> DocumentRepository,
               IRepository<Jobwarranty> JobwarrantyRepository,
               IRepository<EmailTempData> EmailTempDataRepository,
               IRepository<PromotionMaster> promotionMasterRepository,
               IRepository<JobTrackerHistory> JobHistoryRepository,
                IRepository<ExtendOrganizationUnit, long> extendedOrganizationUnitRepository,
                IRepository<UserWiseEmailOrg> UserWiseEmailOrgRepository,
                IRepository<ReviewType> reviewTypeRepository,
                IRepository<MyInstallerActivityLog> myinstalleractivityRepository,
                ITimeZoneService timeZoneService,
                SettingManager settingManager,
                IRepository<Warehouselocation> warehouselocationRepository
            , IRepository<PylonDocument> pylonDocumentRepository
            , IRepository<PostCodeRange.PostCodeRange> postCodeRangeRepository
            , IRepository<LeadSource> leadStatus
            , IUserActivityLogAppService userActivityLogServiceProxy
            )
        {
            _leadRepository = leadRepository;
            _leadsExcelExporter = leadsExcelExporter;
            _lookup_postCodeRepository = lookup_postCodeRepository;
            _lookup_stateRepository = lookup_stateRepository;
            _lookup_leadSourceRepository = lookup_leadSourceRepository;
            _lookup_leadStatusRepository = lookup_leadStatusRepository;
            _lookup_unitTypeRepository = lookup_unitTypeRepository;
            _lookup_streetNameRepository = lookup_streetNameRepository;
            _lookup_streetTypeRepository = lookup_streetTypeRepository;
            _lookup_rejectReasonRepository = lookup_rejectReasonRepository;
            _lookup_cancelReasonRepository = lookup_cancelReasonRepository;
            _leadactivityRepository = leadactivityRepository;
            _lookup_leadActionRepository = lookup_leadActionRepository;
            _userRepository = userRepository;
            _userroleRepository = userroleRepository;
            _roleRepository = roleRepository;
            _userTeamRepository = userTeamRepository;
            _appNotifier = appNotifier;
            _unitOfWorkManager = unitOfWorkManager;
            _organizationUnitRepository = organizationUnitRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _organizationUnitRoleRepository = organizationUnitRoleRepository;
            _applicationSettings = applicationSettings;
            _emailSender = emailSender;
            _teamRepository = teamRepository;
            _userEmailer = userEmailer;
            _emailTemplateRepository = emailTemplateRepository;
            _userManager = userManager;
            _lookup_jobStatusRepository = lookup_jobStatusRepository;
            _jobRepository = jobRepository;
            _quotationRepository = quotationRepository;
            _smsTemplateRepository = smsTemplateRepository;
            _jobPromotionRepository = jobPromotionRepository;
            _freebieTransportRepository = freebieTransportRepository;
            _jobRefundRepository = jobRefundRepository;
            _invoicePaymentRepository = invoicePaymentRepository;
            _previousJobStatusRepository = previousJobStatusRepository;
            _timeZoneConverter = timeZoneConverter;
            _PromotionUsersRepository = PromotionUsersRepository;
            _postCodeRepository = postCodeRepository;
            _LeadtrackerHistoryRepository = LeadtrackerHistoryRepository;
            _dbcontextprovider = dbcontextprovider;
            _ProductItemRepository = ProductItemRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _NotificationAppService = NotificationAppService;
            _SectionAppService = SectionAppService;
            _InvoiceImportDataRepository = InvoiceImportDataRepository;
            _DocumentRepository = DocumentRepository;
            _JobwarrantyRepository = JobwarrantyRepository;
            _EmailTempDataRepository = EmailTempDataRepository;
            _promotionMasterRepository = promotionMasterRepository;
            _JobHistoryRepository = JobHistoryRepository;
            _extendedOrganizationUnitRepository = extendedOrganizationUnitRepository;
            _UserWiseEmailOrgRepository = UserWiseEmailOrgRepository;
            _reviewTypeRepository = reviewTypeRepository;
            _myinstalleractivityRepository = myinstalleractivityRepository;
            _timeZoneService = timeZoneService;
            _settingManager = settingManager;
            _warehouselocationRepository = warehouselocationRepository;
            _pylonDocumentRepository = pylonDocumentRepository;
            _postCodeRangeRepository = postCodeRangeRepository;
            _leadStatus = leadStatus;
            _userActivityLogServiceProxy = userActivityLogServiceProxy;
        }

        /// <summary>
        /// Manage Leads API(Only UnAssigned Leads)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetLeadForViewDto>> GetAll(GetAllLeadsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var jobnumberlist = new List<int?>();
            if (input.FilterName == "JobNumber")
            {
                jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber == input.Filter).Select(e => e.LeadId).ToList();
            }


            var le = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationUnit && e.AssignToUserID == null && e.HideDublicate != true);
            var filteredLeads = _leadRepository.GetAll()
                        .Include(e => e.LeadStatusFk)
                         //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                         .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                        .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                        .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Phone == input.Filter)
                        .WhereIf(input.FilterName == "Requirements" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Requirements == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StreetNameFilter), e => e.StreetName == input.StreetNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadStatusId != null, e => e.LeadStatusId == input.LeadStatusId)
                        //.WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                        //.WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)

                        //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                        //.WhereIf(input.DuplicateFilter == "web", o => o.IsWebDuplicate == true)

                        .WhereIf(input.DuplicateFilter == "web", o => le.Where(e => e.Id != o.Id && ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Any())

                        //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
                        .WhereIf(input.DuplicateFilter == "db", o => o.IsDuplicate == true)

                        //Not Duplicate Leads
                        .WhereIf(input.DuplicateFilter == "no", o => o.IsDuplicate != true && le.Where(e => e.Id != o.Id && ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Any() != true)

                        .Where(e => e.AssignToUserID == null && e.HideDublicate != true && e.IsFakeLead != true);
            //.Where(e => e.IsDuplicate != true && e.IsWebDuplicate != true)
            //.Where(e => e.CompanyName == "Marko sipka" || e.CompanyName == "Bella" || e.CompanyName == "John Gilchrist" || e.CompanyName == "Ray Nichols");

            var leadSource = _lookup_leadSourceRepository.GetAll();

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var AllLeads = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationUnit);

            var leads = (from o in pagedAndFilteredLeads
                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         join o2 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                             //let dbEmail = (from l in AllLeads where l.Id != o.Id && l.AssignToUserID != null && (l.Email == o.Email && !string.IsNullOrEmpty(l.Email)) select l.Id).Any()

                             //let dbMobile = (from l in AllLeads where l.Id != o.Id && l.AssignToUserID != null && (l.Mobile == o.Mobile && !string.IsNullOrEmpty(l.Mobile)) select l.Id).Any()

                             //let webEmail = (from l in AllLeads where l.Id != o.Id && l.AssignToUserID == null && l.IsDuplicate != true && (l.Email == o.Email && !string.IsNullOrEmpty(l.Email)) select l.Id).Any()

                             //let webMobile = (from l in AllLeads where l.Id != o.Id && l.AssignToUserID == null && l.IsDuplicate != true &&  (l.Mobile == o.Mobile && !string.IsNullOrEmpty(l.Mobile)) select l.Id).Any()

                             //let dbMobileId = (from l in AllLeads where l.Id != o.Id && l.AssignToUserID != null && (l.Mobile == o.Mobile && !string.IsNullOrEmpty(l.Mobile)) orderby l.Id descending select l.Id).FirstOrDefault()

                             //let dbEmailId = (from l in AllLeads where l.Id != o.Id && l.AssignToUserID != null && (l.Email == o.Email && !string.IsNullOrEmpty(l.Email)) orderby l.Id descending select l.Id).FirstOrDefault()

                         select new GetLeadForViewDto()
                         {
                             Lead = new LeadDto
                             {
                                 CompanyName = o.CompanyName,
                                 Email = o.Email,
                                 Phone = o.Phone,
                                 Mobile = o.Mobile,
                                 Address = o.Address,
                                 Requirements = o.Requirements,
                                 Id = o.Id,
                                 PostCode = o.PostCode,
                                 LeadStatusID = o.LeadStatusId,
                                 Suburb = o.Suburb,
                                 LeadSource = o.LeadSource,
                                 State = o.State,
                                 AssignToUserID = o.AssignToUserID,
                                 IsExternalLead = o.IsExternalLead,
                                 OrganizationId = o.OrganizationId,
                                 OrganizationName = s2.DisplayName,
                                 LeadreationDate = o.CreationTime,
                                 FName = o.FormName
                             },
                             LeadSourceColorClass = leadSource.Where(e => e.Id == o.LeadSourceId).Select(e => e.ColorClass).FirstOrDefault(),
                             Id = o.Id,
                             LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),

                             //DublicateLeadId = AllLeads.Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.AssignToUserID != null).OrderByDescending(e => e.Id).Select(e => e.Id).FirstOrDefault(),

                             //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
                             //Duplicate = (o.IsDuplicate == false) ? false : (o.IsDuplicate == true) ? true : _leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.OrganizationId == input.OrganizationUnit && e.AssignToUserID != null).Any(),

                             //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                             //WebDuplicate = (o.IsWebDuplicate == false) ? false : (o.IsWebDuplicate == true) ? true : _leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.AssignToUserID == null && e.OrganizationId == input.OrganizationUnit && e.IsDuplicate != true && e.IsWebDuplicate != true).Any()
                             //WebDuplicate = (o.IsWebDuplicate == false) ? false : (o.IsWebDuplicate == true) ? true : _leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.AssignToUserID == null && e.OrganizationId == input.OrganizationUnit && e.IsDuplicate != true && e.IsWebDuplicate != true).Any()

                             //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
                             //Duplicate = o.IsDuplicate != null ? (bool)o.IsDuplicate : AllLeads.Any(e => e.Id != o.Id && e.AssignToUserID != null && ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile)))
                             //),

                             ////Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                             //WebDuplicate = o.IsWebDuplicate != null ? (bool)o.IsWebDuplicate : AllLeads.Any(e => e.Id != o.Id && e.AssignToUserID == null && e.IsDuplicate != true && ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile)))
                             //)

                             //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
                             //Duplicate = o.IsDuplicate != null ? (bool)o.IsDuplicate : dbEmail != true ? dbMobile != true ? false : true : true
                             Duplicate = o.IsDuplicate,
                             WebDuplicate = o.IsWebDuplicate,
                             DublicateLeadId = o.DublicateLeadId
                             //DublicateLeadId = dbMobileId != 0 ? dbMobileId : dbEmailId

                         });

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetLeadForViewDto>(
                totalCount,
                await leads.ToListAsync()
            );
        }

        /// <summary>
        /// Lead Tracker API(All Assign Leads - Except Web & Database Duplicate)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetLeadForViewDto>> GetLeadTrackerData(GetAllLeadsInput input)
        {
            #region New Code on 04/08/2022
            try
            {
                #region Old Code Comment On 04-08-2023 For Testing
                //var _oldCodeTimer = Stopwatch.StartNew();
                //int UserId = (int)AbpSession.UserId;
                //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

                //var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

                //var User_List = _userRepository.GetAll();
                //var user_teamList = _userTeamRepository.GetAll();

                //var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.LeadFk.IsDuplicate != true && e.LeadFk.IsWebDuplicate != true);

                //var job_list = _jobRepository.GetAll();

                //var TeamId = user_teamList.Where(e => e.UserId == UserId).Select(e => e.TeamId).ToList();
                //var UserList = user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

                //var User = User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
                //IList<string> RoleName = await _userManager.GetRolesAsync(User);

                //var userName = await UserManager.GetUserNameAsync(User);

                //var jobnumberlist = new List<int?>();
                //if (input.Filter != null)
                //{
                //    jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
                //}
                //var FilterTeamList = new List<long?>();
                //if (input.TeamId != null && input.TeamId != 0)
                //{
                //    FilterTeamList = user_teamList.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
                //}

                //var FilterManagerList = new List<long?>();
                //if (input.TeamId != null && input.TeamId != 0)
                //{
                //    var ManagerTeamId = user_teamList.Where(e => TeamId.Contains(input.SalesManagerId)).Select(e => e.TeamId).ToList();
                //    FilterManagerList = user_teamList.Where(e => ManagerTeamId.Contains(input.SalesManagerId)).Select(e => e.UserId).ToList();
                //}

                //var CancelreqListLeadid = new List<int?>();
                //if (input.Cancelrequestfilter != 0)
                //{
                //    CancelreqListLeadid = job_list.Where(e => e.IsJobCancelRequest == true).Select(e => e.LeadId).Distinct().ToList();
                //}

                //int Status = 0;
                //if (input.JobStatusID != null && input.JobStatusID.Count() > 0 && input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0)
                //{
                //    if (input.LeadStatusIDS.Contains(6))
                //    {
                //        Status = 1;
                //    }
                //}

                //var FollowupList = new List<int>();
                //if (input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null)
                //{
                //    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date >= SDate.Value.Date && x.CreationTime.Date <= EDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
                //}
                //else if (input.DateFilterType == "Followup" && input.StartDate != null)
                //{
                //    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date >= SDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
                //}
                //else if (input.DateFilterType == "Followup" && input.EndDate != null)
                //{
                //    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date <= EDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
                //}

                //var leadSource = _lookup_leadSourceRepository.GetAll();

                //var filteredLeads = _leadRepository.GetAll()
                //           .Include(e => e.LeadStatusFk)
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => input.StateNameFilter.Contains(e.State) && !string.IsNullOrEmpty(e.State))
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter)
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                //           .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadStatusId))
                //           .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)

                //           .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                //           .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                //           .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                //           .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                //           .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)

                //           .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                //           .WhereIf(Status == 1, e => job_list.Where(x => input.JobStatusID.Contains((int)x.JobStatusId) && x.LeadId == e.Id).Any())
                //           .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.AssignToUserID))
                //           .WhereIf(input.Cancelrequestfilter != 0, e => CancelreqListLeadid.Contains(e.Id))

                //           .WhereIf(input.DateFilterType == "Assign" && input.StartDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                //           .WhereIf(input.DateFilterType == "Assign" && input.EndDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                //           .WhereIf(input.DateFilterType == "ReAssign" && input.StartDate != null, e => e.LeadAssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                //           .WhereIf(input.DateFilterType == "ReAssign" && input.EndDate != null, e => e.LeadAssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                //           .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                //           .WhereIf(input.DateFilterType == "Creation" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                //           .WhereIf(input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))

                //           .WhereIf(input.DateFilterType == "NextFollowup" && input.StartDate != null, e => e.ActivityDate.Value.Date >= SDate.Value.Date)
                //           .WhereIf(input.DateFilterType == "NextFollowup" && input.EndDate != null, e => e.ActivityDate.Value.Date <= EDate.Value.Date)

                //           .Where(e => e.HideDublicate != true);

                //var pagedAndFilteredLeads = filteredLeads
                //    .OrderBy(input.Sorting ?? "id desc")
                //    .PageBy(input);

                //var summaryCount = new LeadsSummaryCount();
                //summaryCount.New = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 1).Count());
                //summaryCount.UnHandled = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 2).Count());
                //summaryCount.Hot = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 5).Count());
                //summaryCount.Cold = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 3).Count());
                //summaryCount.Warm = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 4).Count());
                //summaryCount.Upgrade = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 6).Count());
                //summaryCount.Rejected = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 7).Count());
                //summaryCount.Cancelled = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 8).Count());
                //summaryCount.Closed = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 9).Count());
                //summaryCount.Total = Convert.ToString(filteredLeads.Count());
                //summaryCount.Assigned = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 10).Count());
                //summaryCount.ReAssigned = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 11).Count());
                //summaryCount.Sold = Convert.ToString(filteredLeads.Where(e => job_list.Where(x => x.FirstDepositDate != null && x.LeadId == e.Id).Any()).Count());

                //var leads = (from o in pagedAndFilteredLeads

                //             join o1 in _lookup_rejectReasonRepository.GetAll() on o.RejectReasonId equals o1.Id into j1
                //             from s1 in j1.DefaultIfEmpty()

                //             join o2 in _lookup_cancelReasonRepository.GetAll() on o.LeadStatusId equals o2.Id into j2
                //             from s2 in j2.DefaultIfEmpty()

                //             let JobStatusId = job_list.Where(e => e.LeadId == o.Id).Select(e => e.JobStatusId).FirstOrDefault()

                //             let IsLeftSalesRep = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.IsActive).FirstOrDefault()

                //             select new GetLeadForViewDto()
                //             {
                //                 Lead = new LeadDto
                //                 {
                //                     CompanyName = o.CompanyName,
                //                     Email = o.Email,
                //                     Phone = o.Phone,
                //                     Mobile = o.Mobile,
                //                     Address = o.Address,
                //                     Id = o.Id,
                //                     PostCode = o.PostCode,
                //                     LeadStatusID = o.LeadStatusId,
                //                     Suburb = o.Suburb,
                //                     LeadSource = o.LeadSource,
                //                     State = o.State,
                //                     AssignToUserID = o.AssignToUserID,
                //                     RejectReason = o.RejectReason,
                //                     IsGoogle = o.IsGoogle,
                //                     ChangeStatusDate = o.ChangeStatusDate,
                //                     CreationTime = o.CreationTime,
                //                     CurrentLeadOwner = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                //                     ReAssignDate = o.ReAssignDate,
                //                 },

                //                 LeadStatusName = o.LeadStatusFk.Status,
                //                 LeadStatusColorClass = o.LeadStatusFk.ColorClass,

                //                 LeadAssignDate = leadactivity_list.Where(e => e.LeadId == o.Id && e.ActionId == 3).OrderByDescending(e => e.Id).Select(e => e.CreationTime).FirstOrDefault(),

                //                 ReminderTime = o.ActivityDate != null ? o.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss") : null,
                //                 ActivityDescription = o.ActivityDescription,
                //                 ActivityComment = o.ActivityNote,

                //                 JobStatusName = job_list.Where(e => e.LeadId == o.Id).Select(e => e.JobStatusFk.Name).FirstOrDefault(),

                //                 RejectReasonName = s1 == null || s1.RejectReasonName == null ? "" : s1.RejectReasonName.ToString(),
                //                 CancelReasonName = s2 == null || s2.CancelReasonName == null ? "" : s2.CancelReasonName.ToString(),

                //                 LastQuoteCreationTime = _quotationRepository.GetAll().Where(e => e.JobFk.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.CreationTime).FirstOrDefault(),

                //                 JobStatusId = JobStatusId,

                //                 IsLeftSalesRep = IsLeftSalesRep,

                //                 IsStop = _PromotionUsersRepository.GetAll().Include(e => e.LeadFk).Where(e => e.PromotionResponseStatusId == 2 && e.LeadId == o.Id).Any(),

                //                 SummaryCount = summaryCount
                //             });

                //var totalCount = await filteredLeads.CountAsync();

                //_oldCodeTimer.Stop();
                #endregion Old

                // Start Optimized Code
                //var _newCodeTimer = Stopwatch.StartNew();
                //DateTime utc = DateTime.UtcNow;
                //input.StartDate = ChangeTime(input.StartDate, utc);
                //input.EndDate = ChangeTime(input.EndDate, utc);

                int UserId = (int)AbpSession.UserId;
                var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
                input.LastActivityDate = (_timeZoneConverter.Convert(input.LastActivityDate, (int)AbpSession.TenantId));

                var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

                var User_List = _userRepository.GetAll().AsNoTracking();
                var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

                var leadactivity_list = _leadactivityRepository.GetAll().AsNoTracking().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.LeadFk.IsDuplicate != true && e.LeadFk.IsWebDuplicate != true).Select(e => new { e.Id, e.LeadId, e.ActionId, e.CreationTime });

                var job_list = _jobRepository.GetAll().AsNoTracking().Select(e => new { e.JobNumber, e.IsJobCancelRequest, e.JobStatusId, e.LeadId, e.FirstDepositDate, e.JobStatusFk });

                var TeamId = await user_teamList.Where(e => e.UserId == UserId).Select(e => e.TeamId).ToListAsync();
                //var UserList = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();
                var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();


                var superSalesMRoleId = _roleRepository.GetAll().Where(e => e.Name == "Super Sales Manager").Select(e => e.Id).FirstOrDefault();

                //var userRole = await _userroleRepository.GetAll().Where(ur => ur.UserId == UserId).Select(e => e.UserId).ToListAsync();

                //var OtherTeam = await user_teamList.Where(e => userRole.Contains((long)e.UserId) && TeamId.Contains(e.TeamId)).Select(e => e.TeamId).ToListAsync();

                //var teamlist = TeamId.Where(e => !OtherTeam.Contains(e)).ToList();

                IList<string> RoleName = await _userManager.GetRolesAsync(User);

                //var UserList = await user_teamList.WhereIf(!RoleName.Contains("Super Sales Manager"), e => teamlist.Contains(e.TeamId) || e.UserId == UserId)
                //    .WhereIf(RoleName.Contains("Super Sales Manager"), e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

                var UserList = new List<long?>();
                if (RoleName.Contains("Super Sales Manager"))
                {
                    UserList = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();
                }
                else if (RoleName.Contains("Sales Manager") && !RoleName.Contains("Super Sales Manager"))
                {
                    var suparSalesManagers = await _userroleRepository.GetAll().Where(e => e.RoleId == superSalesMRoleId).Select(e => e.UserId).Distinct().ToListAsync();
                    UserList = await user_teamList.Where(e => TeamId.Contains(e.TeamId) && !suparSalesManagers.Contains((long)e.UserId)).Select(e => e.UserId).Distinct().ToListAsync();
                }

                //var salesManagerRoleId = _roleRepository.GetAll().Where(e => e.Name == "Sales Manager").Select(e => e.Id).FirstOrDefault();

                //var userRole = await _userroleRepository.GetAll().Where(ur => ur.UserId != UserId && ur.RoleId == salesManagerRoleId).Select(e => e.UserId).ToListAsync();

                //var OtherTeam = await user_teamList.Where(e => userRole.Contains((long)e.UserId) && TeamId.Contains(e.TeamId)).Select(e => e.TeamId).ToListAsync();

                //var teamlist = TeamId.Where(e => !OtherTeam.Contains(e)).ToList();

                //var UserList = await user_teamList.WhereIf(User.ShowAnotherTeamLead != true, e => teamlist.Contains(e.TeamId) || e.UserId == UserId)
                //    .WhereIf(User.ShowAnotherTeamLead == true, e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

                //var userName = await UserManager.GetUserNameAsync(User);

                var jobnumberlist = new List<int?>();
                if (input.Filter != null)
                {
                    jobnumberlist = await job_list.Where(e => e.JobNumber == input.Filter).Select(e => e.LeadId).ToListAsync();
                }
                var FilterTeamList = new List<long?>();
                if (input.TeamId != null && input.TeamId != 0)
                {
                    FilterTeamList = await user_teamList.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToListAsync();
                }

                var FilterManagerList = new List<long?>();
                if (input.TeamId != null && input.TeamId != 0)
                {
                    var ManagerTeamId = await user_teamList.Where(e => TeamId.Contains(input.SalesManagerId)).Select(e => e.TeamId).ToListAsync();
                    FilterManagerList = await user_teamList.Where(e => ManagerTeamId.Contains(input.SalesManagerId)).Select(e => e.UserId).ToListAsync();
                }

                var CancelreqListLeadid = new List<int?>();
                if (input.Cancelrequestfilter != 0)
                {
                    CancelreqListLeadid = await job_list.Where(e => e.IsJobCancelRequest == true).Select(e => e.LeadId).Distinct().ToListAsync();
                }

                int Status = 0;
                if (input.JobStatusID != null && input.JobStatusID.Count() > 0 && input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0)
                {
                    if (input.LeadStatusIDS.Contains(6))
                    {
                        Status = 1;
                    }
                }

                var FollowupList = new List<int>();
                if (input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null)
                {
                    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date && x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
                }
                else if (input.DateFilterType == "Followup" && input.StartDate != null)
                {
                    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
                }
                else if (input.DateFilterType == "Followup" && input.EndDate != null)
                {
                    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
                }

                var leadSource = _lookup_leadSourceRepository.GetAll();

                var filteredLeads = _leadRepository.GetAll()
                           .Include(e => e.LeadStatusFk)
                           .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                           .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                           .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                           .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                           .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Phone == input.Filter)
                           .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                           .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => input.StateNameFilter.Contains(e.State) && !string.IsNullOrEmpty(e.State))
                           .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                           .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadStatusId))
                           .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                           .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)

                           .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                           .WhereIf(RoleName.Contains("User"), e => e.AssignToUserID != null)
                           .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                           .WhereIf(RoleName.Contains("Super Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                           .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                           .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                           .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)
                           .WhereIf(input.UserId != null && input.UserId != 0, e => e.AssignToUserID == input.UserId)

                           .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                           .WhereIf(Status == 1, e => job_list.Where(x => input.JobStatusID.Contains((int)x.JobStatusId) && x.LeadId == e.Id).Any())
                           .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.AssignToUserID))
                           .WhereIf(input.Cancelrequestfilter != 0, e => CancelreqListLeadid.Contains(e.Id))

                           .WhereIf(input.DateFilterType == "Assign" && input.StartDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                           .WhereIf(input.DateFilterType == "Assign" && input.EndDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                           .WhereIf(input.DateFilterType == "ReAssign" && input.StartDate != null, e => e.ReAssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                           .WhereIf(input.DateFilterType == "ReAssign" && input.EndDate != null, e => e.ReAssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                           .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                           .WhereIf(input.DateFilterType == "Creation" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                           .WhereIf(input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))

                           .WhereIf(input.DateFilterType == "NextFollowup" && input.StartDate != null, e => e.ActivityDate.Value.Date >= SDate.Value.Date)
                           .WhereIf(input.DateFilterType == "NextFollowup" && input.EndDate != null, e => e.ActivityDate.Value.Date <= EDate.Value.Date)

                           .WhereIf(input.LastActivityDate != null, e => _leadactivityRepository.GetAll().Where(la => la.LeadId == e.Id).Max(e => e.CreationTime).AddHours(diffHour).Date <= input.LastActivityDate.Value.Date)

                           .Where(e => e.HideDublicate != true)
                           .AsNoTracking()
                           .Select(e => new { e.Id, e.LeadStatusId, e.RejectReasonId, e.AssignToUserID, e.CompanyName, e.Email, e.Phone, e.Mobile, e.Address, e.PostCode, e.Suburb, e.LeadSource, e.State, e.RejectReason, e.IsGoogle, e.ChangeStatusDate, e.CreationTime, e.ReAssignDate, e.LeadStatusFk, e.ActivityDate, e.ActivityDescription, e.ActivityNote });

                var pagedAndFilteredLeads = filteredLeads
                    .OrderBy(input.Sorting ?? "id desc")
                    .PageBy(input);

                var summaryCount = new LeadsSummaryCount();
                summaryCount.New = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 1).CountAsync());
                summaryCount.UnHandled = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 2).CountAsync());
                summaryCount.Hot = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 5).CountAsync());
                summaryCount.Cold = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 3).CountAsync());
                summaryCount.Warm = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 4).CountAsync());
                summaryCount.Upgrade = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 6).CountAsync());
                summaryCount.Rejected = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 7).CountAsync());
                summaryCount.Cancelled = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 8).CountAsync());
                summaryCount.Closed = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 9).CountAsync());
                summaryCount.Total = Convert.ToString(await filteredLeads.CountAsync());
                summaryCount.Assigned = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 10).CountAsync());
                summaryCount.ReAssigned = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 11).CountAsync());
                summaryCount.Sold = Convert.ToString(await filteredLeads.Where(e => job_list.Where(x => x.FirstDepositDate != null && x.LeadId == e.Id).Any()).CountAsync());

                var leads = (from o in pagedAndFilteredLeads

                             join o1 in _lookup_rejectReasonRepository.GetAll() on o.RejectReasonId equals o1.Id into j1
                             from s1 in j1.DefaultIfEmpty()

                             join o2 in _lookup_cancelReasonRepository.GetAll() on o.LeadStatusId equals o2.Id into j2
                             from s2 in j2.DefaultIfEmpty()

                             let JobStatusId = job_list.Where(e => e.LeadId == o.Id).Select(e => e.JobStatusId).FirstOrDefault()

                             let IsLeftSalesRep = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.IsActive).FirstOrDefault()

                             select new GetLeadForViewDto()
                             {
                                 Lead = new LeadDto
                                 {
                                     CompanyName = o.CompanyName,
                                     Email = o.Email,
                                     Phone = o.Phone,
                                     Mobile = o.Mobile,
                                     Address = o.Address,
                                     Id = o.Id,
                                     PostCode = o.PostCode,
                                     LeadStatusID = o.LeadStatusId,
                                     Suburb = o.Suburb,
                                     LeadSource = o.LeadSource,
                                     State = o.State,
                                     AssignToUserID = o.AssignToUserID,
                                     RejectReason = o.RejectReason,
                                     IsGoogle = o.IsGoogle,
                                     ChangeStatusDate = o.ChangeStatusDate,
                                     CreationTime = o.CreationTime.AddHours(-diffHour),
                                     CurrentLeadOwner = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                                     ReAssignDate = o.ReAssignDate
                                 },

                                 LeadStatusName = o.LeadStatusFk.Status,
                                 LeadStatusColorClass = o.LeadStatusFk.ColorClass,

                                 LeadAssignDate = leadactivity_list.Where(e => e.LeadId == o.Id && e.ActionId == 3).OrderByDescending(e => e.Id).Select(e => e.CreationTime).FirstOrDefault(),

                                 ReminderTime = o.ActivityDate != null ? o.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss") : null,
                                 ActivityDescription = o.ActivityDescription,
                                 ActivityComment = o.ActivityNote,

                                 JobStatusName = job_list.Where(e => e.LeadId == o.Id).Select(e => e.JobStatusFk.Name).FirstOrDefault(),

                                 RejectReasonName = s1 == null || s1.RejectReasonName == null ? "" : s1.RejectReasonName.ToString(),
                                 CancelReasonName = s2 == null || s2.CancelReasonName == null ? "" : s2.CancelReasonName.ToString(),

                                 LastQuoteCreationTime = _quotationRepository.GetAll().Where(e => e.JobFk.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.CreationTime).FirstOrDefault(),

                                 JobStatusId = JobStatusId,

                                 IsLeftSalesRep = IsLeftSalesRep,

                                 IsStop = _PromotionUsersRepository.GetAll().Where(e => e.PromotionResponseStatusId == 2 && e.LeadId == o.Id).Any(),

                                 SummaryCount = summaryCount,
                                 LastActivityDate = leadactivity_list.Where(e => e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.CreationTime).FirstOrDefault().ToString("dd-MM-yyyy hh:mm:ss"),

                                 IsTrasferAssignCancel = (job_list.Where(e => e.LeadId == o.Id).Select(e => e.JobStatusId).FirstOrDefault() == 3 && job_list.Where(e => e.LeadId == o.Id).Select(e => e.FirstDepositDate).FirstOrDefault() != null && _jobRefundRepository.GetAll().Where(r => r.JobFk.LeadId == o.Id).Any()) ? false : true,
                        
                             });

                var totalCount = await filteredLeads.CountAsync();
                //_newCodeTimer.Stop();
                // End Optimized Code

                //var _oldExecutionTime = _oldCodeTimer.ElapsedMilliseconds;  // 6000 - 7000

                //var _newExecutionTime = _newCodeTimer.ElapsedMilliseconds;

                return new PagedResultDto<GetLeadForViewDto>(totalCount, await leads.ToListAsync());
            }
            catch (Exception e)
            {
                throw new UserFriendlyException("Opps there is problem!", e.Message);
            }
            #endregion
        }

        /// <summary>
        /// Lead Tracker Export To Excel API
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<FileDto> getLeadTrackerToExcel(LeadExcelExportTto input)
        {
            int UserId = (int)AbpSession.UserId;
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User_List = _userRepository.GetAll().AsNoTracking();
            var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var leadactivity_list = _leadactivityRepository.GetAll().AsNoTracking().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.LeadFk.IsDuplicate != true && e.LeadFk.IsWebDuplicate != true).Select(e => new { e.Id, e.LeadId, e.ActionId, e.CreationTime });

            var job_list = _jobRepository.GetAll().AsNoTracking().Select(e => new { e.JobNumber, e.IsJobCancelRequest, e.JobStatusId, e.LeadId, e.FirstDepositDate, e.JobStatusFk });

            var TeamId = await user_teamList.Where(e => e.UserId == UserId).Select(e => e.TeamId).ToListAsync();
            var UserList = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> RoleName = await _userManager.GetRolesAsync(User);

            var userName = await UserManager.GetUserNameAsync(User);

            var jobnumberlist = new List<int?>();
            if (input.Filter != null)
            {
                jobnumberlist = await job_list.Where(e => e.JobNumber == input.Filter).Select(e => e.LeadId).ToListAsync();
            }
            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = await user_teamList.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToListAsync();
            }

            var FilterManagerList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                var ManagerTeamId = await user_teamList.Where(e => TeamId.Contains(input.SalesManagerId)).Select(e => e.TeamId).ToListAsync();
                FilterManagerList = await user_teamList.Where(e => ManagerTeamId.Contains(input.SalesManagerId)).Select(e => e.UserId).ToListAsync();
            }

            var CancelreqListLeadid = new List<int?>();
            if (input.Cancelrequestfilter != 0)
            {
                CancelreqListLeadid = await job_list.Where(e => e.IsJobCancelRequest == true).Select(e => e.LeadId).Distinct().ToListAsync();
            }

            int Status = 0;
            if (input.JobStatusID != null && input.JobStatusID.Count() > 0 && input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0)
            {
                if (input.LeadStatusIDS.Contains(6))
                {
                    Status = 1;
                }
            }

            var FollowupList = new List<int>();
            if (input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null)
            {
                FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date >= SDate.Value.Date && x.CreationTime.Date <= EDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
            }
            else if (input.DateFilterType == "Followup" && input.StartDate != null)
            {
                FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date >= SDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
            }
            else if (input.DateFilterType == "Followup" && input.EndDate != null)
            {
                FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date <= EDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
            }

            var leadSource = _lookup_leadSourceRepository.GetAll();

            var filteredLeads = _leadRepository.GetAll()
                       .Include(e => e.LeadStatusFk)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                        .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Phone == input.Filter)
                        .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                       .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                       .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => input.StateNameFilter.Contains(e.State) && !string.IsNullOrEmpty(e.State))
                       .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                       .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadStatusId))
                       .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                       .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)

                       .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                       .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                       .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                       .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                       .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)

                       .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                       .WhereIf(Status == 1, e => job_list.Where(x => input.JobStatusID.Contains((int)x.JobStatusId) && x.LeadId == e.Id).Any())
                       .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.AssignToUserID))
                       .WhereIf(input.Cancelrequestfilter != 0, e => CancelreqListLeadid.Contains(e.Id))

                       .WhereIf(input.DateFilterType == "Assign" && input.StartDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                       .WhereIf(input.DateFilterType == "Assign" && input.EndDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                       .WhereIf(input.DateFilterType == "ReAssign" && input.StartDate != null, e => e.LeadAssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                       .WhereIf(input.DateFilterType == "ReAssign" && input.EndDate != null, e => e.LeadAssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                       .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                       .WhereIf(input.DateFilterType == "Creation" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                       .WhereIf(input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))

                       .WhereIf(input.DateFilterType == "NextFollowup" && input.StartDate != null, e => e.ActivityDate.Value.Date >= SDate.Value.Date)
                       .WhereIf(input.DateFilterType == "NextFollowup" && input.EndDate != null, e => e.ActivityDate.Value.Date <= EDate.Value.Date)

                       .Where(e => e.HideDublicate != true)
                       .AsNoTracking()
                       .Select(e => new { e.Id, e.LeadStatusId, e.RejectReasonId, e.AssignToUserID, e.CompanyName, e.Email, e.Phone, e.Mobile, e.Address, e.PostCode, e.Suburb, e.LeadSource, e.State, e.RejectReason, e.IsGoogle, e.ChangeStatusDate, e.CreationTime, e.ReAssignDate, e.LeadStatusFk, e.ActivityDate, e.ActivityDescription, e.ActivityNote, e.GCLId, e.LeadSourceId, e.Area });

            var leads = (from o in filteredLeads

                         join o1 in _lookup_rejectReasonRepository.GetAll() on o.RejectReasonId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _lookup_cancelReasonRepository.GetAll() on o.LeadStatusId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         select new GetLeadForViewDto()
                         {
                             Lead = new LeadDto
                             {
                                 CompanyName = o.CompanyName,
                                 Email = o.Email,
                                 Phone = o.Phone,
                                 Mobile = o.Mobile,
                                 Address = o.Address,
                                 Id = o.Id,
                                 PostCode = o.PostCode,
                                 LeadStatusID = o.LeadStatusId,
                                 Suburb = o.Suburb,
                                 LeadSource = o.LeadSource,
                                 State = o.State,
                                 AssignToUserID = o.AssignToUserID,
                                 RejectReason = o.RejectReason,
                                 IsGoogle = o.IsGoogle,
                                 ChangeStatusDate = o.ChangeStatusDate,
                                 CreationTime = o.CreationTime,
                                 CurrentLeadOwner = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                                 GCLId = o.GCLId,
                                 Area = o.Area
                             },

                             LeadStatusName = o.LeadStatusFk.Status,
                             LeadStatusColorClass = o.LeadStatusFk.ColorClass,
                             LeadSourceColorClass = leadSource.Where(e => e.Id == o.LeadSourceId).Select(e => e.ColorClass).FirstOrDefault(),

                             LeadAssignDate = leadactivity_list.Where(e => e.LeadId == o.Id && e.ActionId == 3).OrderByDescending(e => e.Id).Select(e => e.CreationTime).FirstOrDefault(),

                             ReminderTime = o.ActivityDate != null ? o.ActivityDate.Value.ToString("dd-MM-yyyy") : null,
                             ActivityDescription = o.ActivityDescription,
                             ActivityComment = o.ActivityNote,

                             JobStatusName = job_list.Where(e => e.LeadId == o.Id).Select(e => e.JobStatusFk.Name).FirstOrDefault(),

                             RejectReasonName = s1 == null || s1.RejectReasonName == null ? "" : s1.RejectReasonName.ToString(),
                             CancelReasonName = s2 == null || s2.CancelReasonName == null ? "" : s2.CancelReasonName.ToString(),
                         });

            var leadtrackerListDtos = await leads.ToListAsync();

            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _leadsExcelExporter.LeadTrackerExportToFile(leadtrackerListDtos, "LeadTracker.xlsx");
            }
            else
            {
                return _leadsExcelExporter.LeadTrackerExportToFile(leadtrackerListDtos, "LeadTracker.csv");
                //return _leadsExcelExporter.LeadTrackerCsvExport(leadtrackerListDtos);
            }

        }

        /// <summary>
        /// Main Search Data API
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetLeadForViewDto>> GetSearchData(GetAllSearchInput input)
        {
            var filteredLeads = _leadRepository.GetAll().Where(e => e.Id == input.LeadId);

            var pagedAndFilteredLeads = filteredLeads
                .PageBy(input);

            var leads = from o in pagedAndFilteredLeads

                        join o1 in _jobRepository.GetAll() on o.Id equals o1.LeadId into j1
                        from s1 in j1.DefaultIfEmpty()

                        join o2 in _lookup_jobStatusRepository.GetAll() on s1.JobStatusId equals o2.Id into j2
                        from s2 in j2.DefaultIfEmpty()

                        select new GetLeadForViewDto()
                        {
                            Lead = new LeadDto
                            {
                                CompanyName = o.CompanyName,
                                Email = o.Email,
                                Phone = o.Phone,
                                Mobile = o.Mobile,
                                Address = o.Address,
                                Requirements = o.Requirements,
                                Id = o.Id,
                                PostCode = o.PostCode,
                                LeadStatusID = o.LeadStatusId,
                                Suburb = o.Suburb,
                                LeadSource = o.LeadSource,
                                State = o.State,
                            },
                            JobNumber = s1.JobNumber,
                            JobStatus = s2.Name,
                            JobCreatedDate = s1.CreationTime,
                            JobType = s1.JobTypeFk.Name,
                            jobid = s1.Id,

                        };
            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetLeadForViewDto>(
                totalCount,
                leads.DistinctBy(e => e.Lead.Id).ToList()
            );

        }

        /// <summary>
        /// Duplicate Leads API(Only Data & Web Duplicate Leads)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetDuplicateLeadForViewDto>> GetAllDuplicateLead(GetAllLeadsInput input)
        {
            #region old 
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var filteredLeads = _leadRepository.GetAll()
            //            .Include(e => e.LeadStatusFk)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
            //            .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
            //            //.WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
            //            .WhereIf(input.StartDate != null && input.EndDate != null, e => e.ChangeStatusDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ChangeStatusDate.Value.AddHours(10).Date <= EDate.Value.Date)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
            //            .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
            //            .WhereIf(string.IsNullOrWhiteSpace(input.DuplicateFilter), e => e.IsDuplicate == true || e.IsWebDuplicate == true)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.DuplicateFilter), e => input.DuplicateFilter == "web" ? e.IsWebDuplicate == true : e.IsDuplicate == true);

            //var pagedAndFilteredLeads = filteredLeads
            //    .OrderBy(input.Sorting ?? "ChangeStatusDate desc")
            //    .PageBy(input);

            //var leads = from o in pagedAndFilteredLeads

            //            join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
            //            from s3 in j3.DefaultIfEmpty()

            //            join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
            //            from s4 in j4.DefaultIfEmpty()

            //            select new GetDuplicateLeadForViewDto()
            //            {
            //                DuplicateLead = new DuplicateLeadDto
            //                {
            //                    CompanyName = o.CompanyName,
            //                    Email = o.Email,
            //                    Phone = o.Phone,
            //                    Mobile = o.Mobile,
            //                    Address = o.Address,
            //                    Requirements = o.Requirements,
            //                    Id = o.Id,
            //                    PostCode = o.PostCode,
            //                    LeadStatusID = o.LeadStatusId,
            //                    Suburb = o.Suburb,
            //                    LeadSource = o.LeadSource,
            //                    State = o.State,
            //                    OrganizationId = o.OrganizationId,
            //                    OrganizationName = s4.DisplayName,
            //                    CreationTime = o.CreationTime
            //                },
            //                //CurrentLeadOwner = _userRepository.GetAll().Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
            //                LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
            //                Duplicate = (o.IsDuplicate == false || string.IsNullOrEmpty(o.Email) || string.IsNullOrEmpty(o.Mobile) || o.IsWebDuplicate == true) ? false : (o.IsDuplicate == true) ? true : _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.OrganizationId == input.OrganizationUnit && e.AssignToUserID != null && e.IsWebDuplicate != true && e.CreationTime <= o.CreationTime).Any(),
            //                WebDuplicate = (o.IsWebDuplicate == false || string.IsNullOrEmpty(o.Email) || string.IsNullOrEmpty(o.Mobile) || o.IsDuplicate == true) ? false : (o.IsWebDuplicate == true) ? true : _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.OrganizationId == input.OrganizationUnit && e.IsDuplicate != true && e.CreationTime <= o.CreationTime).Any(),
            //                WebDuplicateCount = _leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.AssignToUserID == null && e.OrganizationId == input.OrganizationUnit && e.ChangeStatusDate <= o.ChangeStatusDate).Count(), //e.IsDuplicate != true && e.IsWebDuplicate != true &&
            //                Facebook = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Facebook").Count()),
            //                Online = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Google").Count()),
            //                Others = Convert.ToString(filteredLeads.Where(e => e.LeadSource != "Facebook" && e.LeadSource != "Google").Count()),
            //                Total = Convert.ToString(filteredLeads.Count()),
            //                DuplicateDetail = (from item in _leadRepository.GetAll()
            //                                   .Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.OrganizationId == input.OrganizationUnit)

            //                                   select new DuplicateDetailDto
            //                                   {
            //                                       Id = item.Id,
            //                                       Address = item.Address,
            //                                       PostCode = item.PostCode,
            //                                       State = item.State,
            //                                       Suburb = item.Suburb,
            //                                       StreetName = item.StreetName,
            //                                       StreetNo = item.StreetNo,
            //                                       StreetType = item.StreetType,
            //                                       UnitNo = item.UnitNo,
            //                                       UnitType = item.UnitType,
            //                                       CompanyName = item.CompanyName,
            //                                       Email = item.Email,
            //                                       Mobile = item.Mobile,
            //                                       Phone = item.Phone,
            //                                       Requirements = item.Requirements,
            //                                       IsDuplicate = item.IsDuplicate,
            //                                       IsWebDuplicate = item.IsWebDuplicate,
            //                                       CreationTime = item.CreationTime,
            //                                       LeadSourceName = _lookup_leadSourceRepository.GetAll().Where(e => e.Name == item.LeadSource).Select(e => e.Name).FirstOrDefault(),
            //                                       OrganizationId = item.OrganizationId,
            //                                       OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == item.OrganizationId).Select(e => e.DisplayName).FirstOrDefault(),
            //                                       //CurrentLeadOwner = string.IsNullOrEmpty(Convert.ToString(item.AssignToUserID)) ? "-" : _userRepository.GetAll().Where(e => e.Id == item.AssignToUserID).Select(e => e.FullName).FirstOrDefault()
            //                                   }).ToList()
            //            };

            //var totalCount = await filteredLeads.CountAsync();

            //return new PagedResultDto<GetDuplicateLeadForViewDto>(
            //    totalCount,
            //    await leads.ToListAsync()
            //);
            #endregion

            #region New
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var job_list = _jobRepository.GetAll().AsNoTracking().Select(e => new { e.JobNumber, e.IsJobCancelRequest, e.JobStatusId, e.LeadId, e.FirstDepositDate, e.JobStatusFk });
            var jobnumberlist = new List<int?>();
            if (input.Filter != null)
            {
                jobnumberlist = await job_list.Where(e => e.JobNumber == input.Filter).Select(e => e.LeadId).ToListAsync();
            }
            var filteredLeads = new List<Lead>();

            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                //filteredLeads = await _leadRepository.GetAll().Include(e => e.LeadStatusFk)
                //    .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                //        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                //        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                //        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                //        .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Phone == input.Filter)
                //        .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                //        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter))
                //        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                //        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                //        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                //        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                //        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                //        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                //        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                //        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                //        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                //        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                //        //.WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                //        //.WhereIf(input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                //        .WhereIf( input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                //           .WhereIf( input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                //        .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                //        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                //        .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                //        .WhereIf(string.IsNullOrWhiteSpace(input.DuplicateFilter), e => e.IsDuplicate == true || e.IsWebDuplicate == true)
                //        .WhereIf(!string.IsNullOrWhiteSpace(input.DuplicateFilter), e => input.DuplicateFilter == "web" ? e.IsWebDuplicate == true : e.IsDuplicate == true)
                //        .WhereIf(input.Process == "Duplicate", e => e.IsDeleted != true && ((e.IsDuplicate == false || string.IsNullOrEmpty(e.Email) || string.IsNullOrEmpty(e.Mobile) || e.IsWebDuplicate == true) ? false : (e.IsDuplicate == true) ? true : _leadRepository.GetAll().Where(l => (l.Email == e.Email || l.Mobile == e.Mobile) && l.Id != e.Id && l.OrganizationId == input.OrganizationUnit && l.AssignToUserID != null && l.IsWebDuplicate != true && l.CreationTime <= e.CreationTime).Any()))
                //         .WhereIf(input.Process == "WebDuplicate", e => e.IsDeleted != true && ((e.IsWebDuplicate == false || string.IsNullOrEmpty(e.Email) || string.IsNullOrEmpty(e.Mobile) || e.IsDuplicate == true) ? false : (e.IsWebDuplicate == true) ? true : _leadRepository.GetAll().Where(l => (l.Email == e.Email || l.Mobile == e.Mobile) && l.Id != e.Id && l.AssignToUserID == null && l.OrganizationId == input.OrganizationUnit && l.IsDuplicate != true && l.CreationTime <= e.CreationTime).Any()))
                //         .WhereIf(input.Process == "FakeLead", e => e.IsDeleted != true && e.IsFakeLead == true)
                //         .WhereIf(input.Process == "Assigned", e => e.IsDeleted != true && e.AssignToUserID != null)
                //         .WhereIf(input.Process == "UnAssigned", e => e.IsDeleted != true && e.AssignToUserID == null)
                //         .WhereIf(input.Process == "Deleted", e => e.IsDeleted == true)
                //         .WhereIf(input.Process == "Process", e => e.IsDeleted != true && e.AssignToUserID == null && 
                //         !((e.IsDuplicate == false || string.IsNullOrEmpty(e.Email) || string.IsNullOrEmpty(e.Mobile) || e.IsWebDuplicate == true) ? false : (e.IsDuplicate == true) ? true : _leadRepository.GetAll().Where(l => (l.Email == e.Email || l.Mobile == e.Mobile) && l.Id != e.Id && l.OrganizationId == input.OrganizationUnit && l.AssignToUserID != null && l.IsWebDuplicate != true && l.CreationTime <= e.CreationTime).Any()) &&
                //         !((e.IsWebDuplicate == false || string.IsNullOrEmpty(e.Email) || string.IsNullOrEmpty(e.Mobile) || e.IsDuplicate == true) ? false : (e.IsWebDuplicate == true) ? true : _leadRepository.GetAll().Where(l => (l.Email == e.Email || l.Mobile == e.Mobile) && l.Id != e.Id && l.AssignToUserID == null && l.OrganizationId == input.OrganizationUnit && l.IsDuplicate != true && l.CreationTime <= e.CreationTime).Any()) && 
                //         e.IsFakeLead != true)
                //        .ToListAsync();//;

                filteredLeads = _leadRepository.GetAll()
                           .Include(e => e.LeadStatusFk)
                           .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                           .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                           .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                           .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                           .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Phone == input.Filter)
                           .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                           .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => input.StateNameFilter.Contains(e.State) && !string.IsNullOrEmpty(e.State))
                           .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                           .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadStatusId))
                           .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                           .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)

                          
                           .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                           .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)
                           .WhereIf(input.UserId != null && input.UserId != 0, e => e.AssignToUserID == input.UserId)

                           .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)

                           .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                           .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                           .WhereIf(input.Process == "Duplicate", e => e.IsDeleted != true && ((e.IsDuplicate == false || string.IsNullOrEmpty(e.Email) || string.IsNullOrEmpty(e.Mobile) || e.IsWebDuplicate == true) ? false : (e.IsDuplicate == true) ? true : _leadRepository.GetAll().Where(l => (l.Email == e.Email || l.Mobile == e.Mobile) && l.Id != e.Id && l.OrganizationId == input.OrganizationUnit && l.AssignToUserID != null && l.IsWebDuplicate != true && l.CreationTime <= e.CreationTime).Any()))
                         .WhereIf(input.Process == "WebDuplicate", e => e.IsDeleted != true && ((e.IsWebDuplicate == false || string.IsNullOrEmpty(e.Email) || string.IsNullOrEmpty(e.Mobile) || e.IsDuplicate == true) ? false : (e.IsWebDuplicate == true) ? true : _leadRepository.GetAll().Where(l => (l.Email == e.Email || l.Mobile == e.Mobile) && l.Id != e.Id && l.AssignToUserID == null && l.OrganizationId == input.OrganizationUnit && l.IsDuplicate != true && l.CreationTime <= e.CreationTime).Any()))
                         .WhereIf(input.Process == "FakeLead", e => e.IsDeleted != true && e.IsFakeLead == true)
                         .WhereIf(input.Process == "Assigned", e => e.IsDeleted != true && e.AssignToUserID != null)
                         .WhereIf(input.Process == "UnAssigned", e => e.IsDeleted != true && e.AssignToUserID == null)
                         .WhereIf(input.Process == "Deleted", e => e.IsDeleted == true)
                         .WhereIf(input.Process == "Process", e => e.IsDeleted != true && e.AssignToUserID == null &&
                         !((e.IsDuplicate == false || string.IsNullOrEmpty(e.Email) || string.IsNullOrEmpty(e.Mobile) || e.IsWebDuplicate == true) ? false : (e.IsDuplicate == true) ? true : _leadRepository.GetAll().Where(l => (l.Email == e.Email || l.Mobile == e.Mobile) && l.Id != e.Id && l.OrganizationId == input.OrganizationUnit && l.AssignToUserID != null && l.IsWebDuplicate != true && l.CreationTime <= e.CreationTime).Any()) &&
                         !((e.IsWebDuplicate == false || string.IsNullOrEmpty(e.Email) || string.IsNullOrEmpty(e.Mobile) || e.IsDuplicate == true) ? false : (e.IsWebDuplicate == true) ? true : _leadRepository.GetAll().Where(l => (l.Email == e.Email || l.Mobile == e.Mobile) && l.Id != e.Id && l.AssignToUserID == null && l.OrganizationId == input.OrganizationUnit && l.IsDuplicate != true && l.CreationTime <= e.CreationTime).Any()) &&
                         e.IsFakeLead != true)
                        .ToList();
                //.Select(e => new { e.Id, e.LeadStatusId, e.RejectReasonId, e.AssignToUserID, e.CompanyName, e.Email, e.Phone, e.Mobile, e.Address, e.PostCode, e.Suburb, e.LeadSource, e.State, e.RejectReason, e.IsGoogle, e.ChangeStatusDate, e.CreationTime, e.ReAssignDate, e.LeadStatusFk, e.ActivityDate, e.ActivityDescription, e.ActivityNote });
            }

            //var filteredLeads = Allleads
            //            //.Include(e => e.LeadStatusFk)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
            //            .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
            //            //.WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
            //            .WhereIf(input.StartDate != null && input.EndDate != null, e => e.ChangeStatusDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ChangeStatusDate.Value.AddHours(10).Date <= EDate.Value.Date)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
            //            .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
            //            .WhereIf(string.IsNullOrWhiteSpace(input.DuplicateFilter), e => e.IsDuplicate == true || e.IsWebDuplicate == true)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.DuplicateFilter), e => input.DuplicateFilter == "web" ? e.IsWebDuplicate == true : e.IsDuplicate == true);

            var pagedAndFilteredLeads = filteredLeads.AsQueryable()
                .OrderBy(input.Sorting ?? "CreationTime desc")
                .PageBy(input);

            var leads = from o in pagedAndFilteredLeads

                        join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                        from s3 in j3.DefaultIfEmpty()

                        join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                        from s4 in j4.DefaultIfEmpty()

                        select new GetDuplicateLeadForViewDto()
                        {
                            DuplicateLead = new DuplicateLeadDto
                            {
                                CompanyName = o.CompanyName,
                                Email = o.Email,
                                Phone = o.Phone,
                                Mobile = o.Mobile,
                                Address = o.Address,
                                Requirements = o.Requirements,
                                Id = o.Id,
                                PostCode = o.PostCode,
                                LeadStatusID = o.LeadStatusId,
                                Suburb = o.Suburb,
                                LeadSource = o.LeadSource,
                                State = o.State,
                                OrganizationId = o.OrganizationId,
                                OrganizationName = s4.DisplayName,
                                CreationTime = o.CreationTime
                            },
                            //CurrentLeadOwner = _userRepository.GetAll().Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                            LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                            Duplicate = (o.IsDuplicate == false || string.IsNullOrEmpty(o.Email) || string.IsNullOrEmpty(o.Mobile) || o.IsWebDuplicate == true) ? false : (o.IsDuplicate == true) ? true : _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.OrganizationId == input.OrganizationUnit && e.AssignToUserID != null && e.IsWebDuplicate != true && e.CreationTime <= o.CreationTime).Any(),
                            WebDuplicate = (o.IsWebDuplicate == false || string.IsNullOrEmpty(o.Email) || string.IsNullOrEmpty(o.Mobile) || o.IsDuplicate == true) ? false : (o.IsWebDuplicate == true) ? true : _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.OrganizationId == input.OrganizationUnit && e.IsDuplicate != true && e.CreationTime <= o.CreationTime).Any(),
                            WebDuplicateCount = _leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.AssignToUserID == null && e.OrganizationId == input.OrganizationUnit && e.ChangeStatusDate <= o.ChangeStatusDate).Count(), //e.IsDuplicate != true && e.IsWebDuplicate != true &&
                            Facebook = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Facebook").Count()),
                            Online = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Google").Count()),
                            Others = Convert.ToString(filteredLeads.Where(e => e.LeadSource != "Facebook" && e.LeadSource != "Google").Count()),
                            Total = Convert.ToString(filteredLeads.Count()),
                            IsFakeLead = o.IsFakeLead == true,
                            IsAssigned = o.AssignToUserID != null,
                            IsDeleted = o.IsDeleted == true,
                            DuplicateDetail = (from item in _leadRepository.GetAll()
                                               .Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.OrganizationId == input.OrganizationUnit)

                                               select new DuplicateDetailDto
                                               {
                                                   Id = item.Id,
                                                   Address = item.Address,
                                                   PostCode = item.PostCode,
                                                   State = item.State,
                                                   Suburb = item.Suburb,
                                                   StreetName = item.StreetName,
                                                   StreetNo = item.StreetNo,
                                                   StreetType = item.StreetType,
                                                   UnitNo = item.UnitNo,
                                                   UnitType = item.UnitType,
                                                   CompanyName = item.CompanyName,
                                                   Email = item.Email,
                                                   Mobile = item.Mobile,
                                                   Phone = item.Phone,
                                                   Requirements = item.Requirements,
                                                   IsDuplicate = item.IsDuplicate,
                                                   IsWebDuplicate = item.IsWebDuplicate,
                                                   CreationTime = item.CreationTime,
                                                   LeadSourceName = _lookup_leadSourceRepository.GetAll().Where(e => e.Name == item.LeadSource).Select(e => e.Name).FirstOrDefault(),
                                                   OrganizationId = item.OrganizationId,
                                                   OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == item.OrganizationId).Select(e => e.DisplayName).FirstOrDefault(),
                                                   //CurrentLeadOwner = string.IsNullOrEmpty(Convert.ToString(item.AssignToUserID)) ? "-" : _userRepository.GetAll().Where(e => e.Id == item.AssignToUserID).Select(e => e.FullName).FirstOrDefault()
                                               }).ToList()
                        };

           // leads = ;

            var totalCount = filteredLeads.Count();

            return new PagedResultDto<GetDuplicateLeadForViewDto>(
                totalCount,
                leads.ToList()
            ) ;
            #endregion
        }

        /// <summary>
        /// Duplicate Lead Export to Excel
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<FileDto> getDublicateLeadsToExcel(DuplicateLeadExceltoExport input)
        {


            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var job_list = _jobRepository.GetAll().AsNoTracking().Select(e => new { e.JobNumber, e.IsJobCancelRequest, e.JobStatusId, e.LeadId, e.FirstDepositDate, e.JobStatusFk });
            var jobnumberlist = new List<int?>();
            if (input.Filter != null)
            {
                jobnumberlist = await job_list.Where(e => e.JobNumber == input.Filter).Select(e => e.LeadId).ToListAsync();
            }
            var filteredLeads = new List<Lead>();

            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {

                filteredLeads = _leadRepository.GetAll()
                           .Include(e => e.LeadStatusFk)
                           .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                           .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                           .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                           .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                           .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Phone == input.Filter)
                           .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                           .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => input.StateNameFilter.Contains(e.State) && !string.IsNullOrEmpty(e.State))
                           .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                           .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadStatusId))
                           .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                           .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)


                           .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                           .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)
                           .WhereIf(input.UserId != null && input.UserId != 0, e => e.AssignToUserID == input.UserId)

                           .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)

                           .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                           .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                            .WhereIf(input.Process == "Duplicate", e => e.IsDeleted != true && ((e.IsDuplicate == false || string.IsNullOrEmpty(e.Email) || string.IsNullOrEmpty(e.Mobile) || e.IsWebDuplicate == true) ? false : (e.IsDuplicate == true) ? true : _leadRepository.GetAll().Where(l => (l.Email == e.Email || l.Mobile == e.Mobile) && l.Id != e.Id && l.OrganizationId == input.OrganizationUnit && l.AssignToUserID != null && l.IsWebDuplicate != true && l.CreationTime <= e.CreationTime).Any()))
                         .WhereIf(input.Process == "WebDuplicate", e => e.IsDeleted != true && ((e.IsWebDuplicate == false || string.IsNullOrEmpty(e.Email) || string.IsNullOrEmpty(e.Mobile) || e.IsDuplicate == true) ? false : (e.IsWebDuplicate == true) ? true : _leadRepository.GetAll().Where(l => (l.Email == e.Email || l.Mobile == e.Mobile) && l.Id != e.Id && l.AssignToUserID == null && l.OrganizationId == input.OrganizationUnit && l.IsDuplicate != true && l.CreationTime <= e.CreationTime).Any()))
                         .WhereIf(input.Process == "FakeLead", e => e.IsDeleted != true && e.IsFakeLead == true)
                         .WhereIf(input.Process == "Assigned", e => e.IsDeleted != true && e.AssignToUserID != null)
                         .WhereIf(input.Process == "UnAssigned", e => e.IsDeleted != true && e.AssignToUserID == null)
                         .WhereIf(input.Process == "Deleted", e => e.IsDeleted == true)
                         .WhereIf(input.Process == "Process", e => e.IsDeleted != true && e.AssignToUserID == null &&
                         !((e.IsDuplicate == false || string.IsNullOrEmpty(e.Email) || string.IsNullOrEmpty(e.Mobile) || e.IsWebDuplicate == true) ? false : (e.IsDuplicate == true) ? true : _leadRepository.GetAll().Where(l => (l.Email == e.Email || l.Mobile == e.Mobile) && l.Id != e.Id && l.OrganizationId == input.OrganizationUnit && l.AssignToUserID != null && l.IsWebDuplicate != true && l.CreationTime <= e.CreationTime).Any()) &&
                         !((e.IsWebDuplicate == false || string.IsNullOrEmpty(e.Email) || string.IsNullOrEmpty(e.Mobile) || e.IsDuplicate == true) ? false : (e.IsWebDuplicate == true) ? true : _leadRepository.GetAll().Where(l => (l.Email == e.Email || l.Mobile == e.Mobile) && l.Id != e.Id && l.AssignToUserID == null && l.OrganizationId == input.OrganizationUnit && l.IsDuplicate != true && l.CreationTime <= e.CreationTime).Any()) &&
                         e.IsFakeLead != true)
                        .ToList();
                //.Select(e => new { e.Id, e.LeadStatusId, e.RejectReasonId, e.AssignToUserID, e.CompanyName, e.Email, e.Phone, e.Mobile, e.Address, e.PostCode, e.Suburb, e.LeadSource, e.State, e.RejectReason, e.IsGoogle, e.ChangeStatusDate, e.CreationTime, e.ReAssignDate, e.LeadStatusFk, e.ActivityDate, e.ActivityDescription, e.ActivityNote });
            }

            var query = (from o in filteredLeads
                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()
                         join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                         from s4 in j4.DefaultIfEmpty()

                         select new GetDuplicateLeadForViewDto()
                         {
                             DuplicateLead = new DuplicateLeadDto
                             {
                                 CompanyName = o.CompanyName,
                                 LeadSource = o.LeadSource,
                                 OrganizationName = s4.DisplayName,
                                 Email = o.Email,
                                 Mobile = o.Mobile,
                                 Address = o.Address,
                                 Suburb = o.Suburb,
                                 State = o.State,
                                 PostCode = o.PostCode,
                                 CreationTime = o.CreationTime,
                             },
                             LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                             Duplicate = (o.IsDuplicate == false || string.IsNullOrEmpty(o.Email) || string.IsNullOrEmpty(o.Mobile) || o.IsWebDuplicate == true) ? false : (o.IsDuplicate == true) ? true : _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.OrganizationId == input.OrganizationUnit && e.AssignToUserID != null && e.IsWebDuplicate != true && e.CreationTime <= o.CreationTime).Any(),
                             WebDuplicate = (o.IsWebDuplicate == false || string.IsNullOrEmpty(o.Email) || string.IsNullOrEmpty(o.Mobile) || o.IsDuplicate == true) ? false : (o.IsWebDuplicate == true) ? true : _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.OrganizationId == input.OrganizationUnit && e.IsDuplicate != true && e.CreationTime <= o.CreationTime).Any(),
                             WebDuplicateCount = _leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.AssignToUserID == null && e.OrganizationId == input.OrganizationUnit && e.ChangeStatusDate <= o.ChangeStatusDate).Count(), //e.IsDuplicate != true && e.IsWebDuplicate != true &&
                             Facebook = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Facebook").Count()),
                             Online = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Google").Count()),
                             Others = Convert.ToString(filteredLeads.Where(e => e.LeadSource != "Facebook" && e.LeadSource != "Google").Count()),
                             Total = Convert.ToString(filteredLeads.Count()),
                             IsFakeLead = o.IsFakeLead == true,
                             IsAssigned = o.AssignToUserID != null,
                             IsDeleted = o.IsDeleted == true,
                             
                         });

            var dublicateleadListDtos =  query.ToList();
            if (input.excelorcsv == 1) 
            {
                return _leadsExcelExporter.DublicateLeadExportToFile(dublicateleadListDtos);
            }
            else
            {
                return _leadsExcelExporter.DublicateLeadCsvExportToFile(dublicateleadListDtos);
            }

        }

        /// <summary>
        /// My Leads API(Only Assigned Leads - Current User)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetLeadForViewDto>> GetAllForMyLead(GetAllLeadsInput input)
        {
            #region Old Code
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //int UserId = (int)AbpSession.UserId;
            //var mylead = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationUnit && e.AssignToUserID == UserId);

            //var statusIds = new List<int>();
            //statusIds.Add(1);
            //statusIds.Add(7);
            //statusIds.Add(8);
            //statusIds.Add(9);

            //var Job_list = _jobRepository.GetAll();
            //var leadactive_list = _leadactivityRepository.GetAll().Where(e => e.LeadFk.AssignToUserID == UserId && e.LeadFk.OrganizationId == input.OrganizationUnit);

            //var jobnumberlist = new List<int?>();
            //jobnumberlist = Job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

            //var FollowupList = new List<int>();
            //if (input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null)
            //{
            //    FollowupList = leadactive_list.WhereIf(input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();

            //}
            //var NextFollowupList = new List<int>();
            //if (input.DateFilterType == "NextFollowup" && input.StartDate != null && input.EndDate != null)
            //{
            //    NextFollowupList = leadactive_list.WhereIf(input.StartDate != null && input.EndDate != null, e => e.ActivityDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActivityDate.Value.AddHours(10).Date <= EDate.Value.Date && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();

            //}
            //var joblist = new List<int?>();
            //if (input.JobStatusID != null && input.JobStatusID.Count() > 0 && input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0)
            //{
            //    if (input.LeadStatusIDS.Contains(6))
            //    {
            //        joblist = Job_list.Where(e => input.JobStatusID.Contains((int)e.JobStatusId)).Select(e => e.LeadId).ToList();
            //    }

            //}

            //if (mylead != null)
            //{
            //    var filteredLeads = mylead
            //                .Include(e => e.LeadStatusFk)
            //                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
            //                .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
            //                .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
            //                .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
            //                .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
            //                .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
            //                .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
            //                .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
            //                .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
            //                .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
            //                .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter)
            //                 .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadStatusId))
            //                .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.LeadAssignDate.Value.AddHours(10).Date >= SDate.Value.Date && e.LeadAssignDate.Value.AddHours(10).Date <= EDate.Value.Date)
            //                .WhereIf(input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))
            //                .WhereIf(input.DateFilterType == "NextFollowup" && input.StartDate != null && input.EndDate != null, e => NextFollowupList.Contains(e.Id))
            //                .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
            //                .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
            //                .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
            //                .Where(e => !statusIds.Contains(e.LeadStatusId))
            //                .WhereIf(input.JobStatusID != null && input.JobStatusID.Count() > 0, e => joblist.Contains(e.Id))
            //                .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
            //                .Where(e => e.AssignToUserID == UserId);

            //    var pagedAndFilteredLeads = filteredLeads
            //        .OrderBy(input.Sorting ?? "LeadAssignDate desc")
            //        .PageBy(input);

            //    var LeadidList = Job_list.Where(e => e.DepositeRecceivedDate != null).Select(e => e.LeadId).ToList();
            //    var leads = from o in pagedAndFilteredLeads

            //                join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
            //                from s3 in j3.DefaultIfEmpty()

            //                join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
            //                from s4 in j4.DefaultIfEmpty()

            //                join o5 in _leadactivityRepository.GetAll() on o.Id equals o5.LeadId into j5
            //                from s5 in j5.DefaultIfEmpty()

            //                join o6 in _jobRepository.GetAll() on o.Id equals o6.LeadId into j6
            //                from s6 in j6.DefaultIfEmpty()

            //                select new GetLeadForViewDto()
            //                {
            //                    Lead = new LeadDto
            //                    {
            //                        CompanyName = o.CompanyName,
            //                        Email = o.Email,
            //                        Phone = o.Phone,
            //                        Mobile = o.Mobile,
            //                        Address = o.Address,
            //                        Requirements = o.Requirements,
            //                        Id = o.Id,
            //                        PostCode = o.PostCode,
            //                        LeadStatusID = o.LeadStatusId,
            //                        Suburb = o.Suburb,
            //                        LeadSource = o.LeadSource,
            //                        State = o.State,
            //                        OrganizationId = o.OrganizationId,
            //                        OrganizationName = s4.DisplayName,
            //                        longitude = o.longitude,
            //                        latitude = o.latitude,
            //                        FollowupDate = s5.CreationTime,
            //                        CreationTime = (DateTime)o.LeadAssignDate,
            //                        IsGoogle = o.IsGoogle
            //                    },
            //                    LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),

            //                    ReminderTime = leadactive_list.Where(e => (e.SectionId == 13 || e.SectionId == 14) && e.ActionId == 8 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
            //                    ActivityDescription = leadactive_list.Where(e => (e.SectionId == 13 || e.SectionId == 14) && e.ActionId == 8 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
            //                    ActivityComment = leadactive_list.Where(e => (e.SectionId == 13 || e.SectionId == 14) && e.ActionId == 24 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

            //                    JobStatusName = _lookup_jobStatusRepository.GetAll().Where(e => e.Id == s6.JobStatusId).Select(e => e.Name).FirstOrDefault(),
            //                };

            //    var totalCount = await filteredLeads.CountAsync();

            //    return new PagedResultDto<GetLeadForViewDto>(
            //        totalCount,
            //        leads.DistinctBy(e => e.Lead.Id).ToList()
            //    );
            //}
            //else
            //{
            //    var pagedAndFilteredLeads = mylead
            //         .OrderBy(input.Sorting ?? "LeadAssignDate desc")
            //         .PageBy(input);

            //    var LeadidList = Job_list.Where(e => e.DepositeRecceivedDate != null).Select(e => e.LeadId).ToList();
            //    var leads = from o in pagedAndFilteredLeads

            //                join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
            //                from s3 in j3.DefaultIfEmpty()

            //                join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
            //                from s4 in j4.DefaultIfEmpty()

            //                join o5 in _leadactivityRepository.GetAll() on o.Id equals o5.LeadId into j5
            //                from s5 in j5.DefaultIfEmpty()

            //                join o6 in _jobRepository.GetAll() on o.Id equals o6.LeadId into j6
            //                from s6 in j6.DefaultIfEmpty()

            //                select new GetLeadForViewDto()
            //                {
            //                    Lead = new LeadDto
            //                    {
            //                        CompanyName = o.CompanyName,
            //                        Email = o.Email,
            //                        Phone = o.Phone,
            //                        Mobile = o.Mobile,
            //                        Address = o.Address,
            //                        Requirements = o.Requirements,
            //                        Id = o.Id,
            //                        PostCode = o.PostCode,
            //                        LeadStatusID = o.LeadStatusId,
            //                        Suburb = o.Suburb,
            //                        LeadSource = o.LeadSource,
            //                        State = o.State,
            //                        OrganizationId = o.OrganizationId,
            //                        OrganizationName = s4.DisplayName,
            //                        longitude = o.longitude,
            //                        latitude = o.latitude,
            //                        FollowupDate = s5.CreationTime,
            //                        CreationTime = (DateTime)o.LeadAssignDate,
            //                        IsGoogle = o.IsGoogle
            //                    },
            //                    LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),

            //                    ReminderTime = leadactive_list.Where(e => (e.SectionId == 13 || e.SectionId == 14) && e.ActionId == 8 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
            //                    ActivityDescription = leadactive_list.Where(e => (e.SectionId == 13 || e.SectionId == 14) && e.ActionId == 8 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
            //                    ActivityComment = leadactive_list.Where(e => (e.SectionId == 13 || e.SectionId == 14) && e.ActionId == 24 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

            //                    JobStatusName = _lookup_jobStatusRepository.GetAll().Where(e => e.Id == s6.JobStatusId).Select(e => e.Name).FirstOrDefault(),

            //                };

            //    var totalCount = 0;

            //    return new PagedResultDto<GetLeadForViewDto>(
            //        totalCount,
            //        leads.DistinctBy(e => e.Lead.Id).ToList()
            //    );
            //}
            #endregion

            #region New Code By Suresh
            //DateTime utc = DateTime.UtcNow;
            //input.StartDate = ChangeTime(input.StartDate, utc);
            //input.EndDate = ChangeTime(input.EndDate, utc);

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            int UserId = (int)AbpSession.UserId;

            var Job_List = _jobRepository.GetAll().AsNoTracking().Select(e => new { e.Id, e.JobNumber, e.JobStatusFk.Name, e.LeadId, e.JobStatusId, e.FirstDepositDate });

            var statusIds = new List<int>();
            statusIds.Add(1);
            statusIds.Add(7);
            statusIds.Add(8);
            statusIds.Add(9);

            var jobnumberlist = new List<int?>();
            if (!string.IsNullOrWhiteSpace(input.Filter) && input.FilterName == "JobNumber")
            {
                jobnumberlist = await Job_List.Where(e => e.JobNumber == input.Filter).Select(e => (int?)e.LeadId).ToListAsync();
            }

            int Status = 0;
            if (input.JobStatusID != null && input.JobStatusID.Count() > 0 && input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0)
            {
                if (input.LeadStatusIDS.Contains(6))
                {
                    //jobStatusList = jobStatusList.Where(e => input.JobStatusID.Contains((int)e.JobStatusId)).Select(e => new { e.Id, e.JobNumber, e.Name, e.LeadId, e.JobStatusId, e.DepositeRecceivedDate, e.FirstDepositDate }).ToList();
                    Status = 1;
                }
            }

            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.LeadFk.AssignToUserID == UserId).AsNoTracking().Select(e => new { e.LeadId, e.CreationTime, e.CreatorUserId, e.ActionId });

            var filteredLeads = _leadRepository.GetAll()
                                .Where(e => e.OrganizationId == input.OrganizationUnit && e.AssignToUserID == UserId && !statusIds.Contains(e.LeadStatusId))
                                //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || jobnumberlist.Contains(e.Id))

                                .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                                .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadStatusId))

                                .WhereIf(Status == 1, e => Job_List.Where(x => input.JobStatusID.Contains((int)x.JobStatusId) && x.LeadId == e.Id).Any())
                                .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                                .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                                .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                                .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                                .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                                .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)

                                .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                                .WhereIf(input.DateFilterType == "Creation" && input.EndDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                                .WhereIf(input.DateFilterType == "NextFollowup" && input.StartDate != null, e => e.ActivityDate.Value.Date >= SDate.Value.Date)
                                .WhereIf(input.DateFilterType == "NextFollowup" && input.EndDate != null, e => e.ActivityDate.Value.Date <= EDate.Value.Date)

                                .WhereIf(input.DateFilterType == "ReAssign" && input.StartDate != null, e => e.ReAssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                                .WhereIf(input.DateFilterType == "ReAssign" && input.EndDate != null, e => e.ReAssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                                .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                                .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                                .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                                .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                                .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                                .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Phone == input.Filter)

                                .AsNoTracking()
                                .Select(e => new { e.Id, e.CompanyName, e.Email, e.Phone, e.Mobile, e.Address, e.PostCode, e.LeadSource, e.State, e.LeadAssignDate, e.IsGoogle, e.LeadStatusId, e.LeadStatusFk, e.Suburb, e.LeadSourceId, e.ActivityDate, e.ActivityDescription, e.ActivityNote })
                                ;

            var pagedAndFilteredLeads = filteredLeads.OrderBy(input.Sorting ?? "LeadAssignDate desc").PageBy(input);

            var LeadidList = await Job_List.Where(e => e.FirstDepositDate != null).Select(e => e.LeadId).ToListAsync();
            var summaryCount = new LeadsSummaryCount();
            summaryCount.New = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 1).CountAsync());
            summaryCount.UnHandled = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 2).CountAsync());
            summaryCount.Hot = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 5).CountAsync());
            summaryCount.Cold = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 3).CountAsync());
            summaryCount.Warm = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 4).CountAsync());
            summaryCount.Upgrade = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 6).CountAsync());
            summaryCount.Rejected = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 7).CountAsync());
            summaryCount.Cancelled = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 8).CountAsync());
            summaryCount.Closed = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 9).CountAsync());
            summaryCount.Total = Convert.ToString(await filteredLeads.CountAsync());
            summaryCount.Assigned = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 10).CountAsync());
            summaryCount.ReAssigned = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 11).CountAsync());

            summaryCount.Sold = Convert.ToString(await filteredLeads.Where(e => Job_List.Where(x => x.FirstDepositDate != null && x.LeadId == e.Id).Any()).CountAsync());

            var leadSource = _lookup_leadSourceRepository.GetAll().AsNoTracking().Select(e => new { e.Id, e.ColorClass });

            var leads = from o in pagedAndFilteredLeads

                        select new GetLeadForViewDto()
                        {
                            Lead = new LeadDto
                            {
                                CompanyName = o.CompanyName,
                                Email = o.Email,
                                Phone = o.Phone,
                                Mobile = o.Mobile,
                                Address = o.Address,
                                Id = o.Id,
                                PostCode = o.PostCode,
                                LeadStatusID = o.LeadStatusId,
                                Suburb = o.Suburb,
                                LeadSource = o.LeadSource,
                                State = o.State,
                                CreationTime = (DateTime)o.LeadAssignDate,
                                IsGoogle = o.IsGoogle
                            },
                            LeadStatusName = o.LeadStatusFk.Status,
                            JobStatusName = Job_List.Where(e => e.LeadId == o.Id).Select(e => e.Name).FirstOrDefault(),

                            LeadStatusColorClass = o.LeadStatusFk.ColorClass,
                            LeadSourceColorClass = leadSource.Where(e => e.Id == o.LeadSourceId).Select(e => e.ColorClass).FirstOrDefault(),

                            ReminderTime = o.ActivityDate != null ? o.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss") : null,
                            ActivityDescription = o.ActivityDescription,
                            ActivityComment = o.ActivityNote,

                            LastQuoteCreationTime = _quotationRepository.GetAll().Where(e => e.JobFk.LeadId == o.Id).OrderByDescending(e => e.Id).AsNoTracking().Select(e => e.CreationTime).FirstOrDefault(),

                            SummaryCount = summaryCount,
                            isChangedFollowup = false,

                            LastActivityDate = leadactivity_list.Where(e => e.LeadId == o.Id).OrderByDescending(e => e.CreationTime).Select(e => e.CreationTime).FirstOrDefault().ToString("dd-MM-yyyy hh:mm:ss"),
                            LastActivityBy = _userRepository.GetAll().Where(e => e.Id == leadactivity_list.Where(e => e.LeadId == o.Id).OrderByDescending(e => e.CreationTime).Select(e => e.CreatorUserId).FirstOrDefault()).FirstOrDefault().Name,
                            NextFollowUpBy = _userRepository.GetAll().Where(e => e.Id == leadactivity_list.Where(e => e.ActionId == 8 && e.LeadId == o.Id).OrderByDescending(e => e.CreationTime).Select(e => e.CreatorUserId).FirstOrDefault()).FirstOrDefault().Name,
                            LastCommentBy = _userRepository.GetAll().Where(e => e.Id == leadactivity_list.Where(e => e.ActionId == 24 && e.LeadId == o.Id).OrderByDescending(e => e.CreationTime).Select(e => e.CreatorUserId).FirstOrDefault()).FirstOrDefault().Name

                        };


            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetLeadForViewDto>(totalCount, await leads.ToListAsync());
            #endregion
        }
        
        /// <summary>
        /// My Leads Export to Excel API
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<FileDto> getMyLeadToExcel(LeadExcelExportTto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            int UserId = (int)AbpSession.UserId;
            var mylead = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationUnit && e.AssignToUserID == UserId);
            var statusIds = new List<int>();
            statusIds.Add(1);
            statusIds.Add(7);
            statusIds.Add(8);
            statusIds.Add(9);
            var Job_list = _jobRepository.GetAll();
            var leadactive_list = _leadactivityRepository.GetAll().Where(e => e.LeadFk.AssignToUserID == UserId && e.LeadFk.OrganizationId == input.OrganizationUnit);
            //      var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.LeadFk.IsDuplicate != true && e.LeadFk.IsWebDuplicate != true);

            var jobnumberlist = new List<int?>();

            jobnumberlist = Job_list.Where(e => e.JobNumber == input.Filter).Select(e => e.LeadId).ToList();
            var FollowupList = new List<int>();
            if (input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null)
            {
                FollowupList = leadactive_list.WhereIf(input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();

            }
            var NextFollowupList = new List<int>();
            if (input.DateFilterType == "NextFollowup" && input.StartDate != null && input.EndDate != null)
            {
                NextFollowupList = leadactive_list.WhereIf(input.StartDate != null && input.EndDate != null, e => e.ActivityDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActivityDate.Value.AddHours(10).Date <= EDate.Value.Date && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();

            }
            var joblist = new List<int?>();
            if (input.JobStatusID != null && input.JobStatusID.Count() > 0 && input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0)
            {
                if (input.LeadStatusIDS.Contains(6))
                {
                    joblist = Job_list.Where(e => input.JobStatusID.Contains((int)e.JobStatusId)).Select(e => e.LeadId).ToList();
                }

            }

            var filteredLeads = mylead
                            .Include(e => e.LeadStatusFk)
                            //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                            .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                            .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                            .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                            .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                            .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                            .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter)
                             // .WhereIf(input.LeadStatusId > 0, e => e.LeadStatusId == input.LeadStatusId)
                             .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadStatusId))
                            .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.LeadAssignDate.Value.AddHours(10).Date >= SDate.Value.Date && e.LeadAssignDate.Value.AddHours(10).Date <= EDate.Value.Date)
                            .WhereIf(input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))
                            .WhereIf(input.DateFilterType == "NextFollowup" && input.StartDate != null && input.EndDate != null, e => NextFollowupList.Contains(e.Id))
                            .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                            .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                            .Where(e => !statusIds.Contains(e.LeadStatusId))
                            .WhereIf(input.JobStatusID != null && input.JobStatusID.Count() > 0, e => joblist.Contains(e.Id))
                            .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                            //.WhereIf(input.LeadSourceIdFilter != 0 && input.LeadSourceIdFilter != null, e => e.LeadSourceId == input.LeadSourceIdFilter)
                            //.WhereIf(!string.IsNullOrWhiteSpace(input.ProjectNumberFilter), e => jobnumberlist.Contains(e.Id))
                            .Where(e => e.AssignToUserID == UserId);

            var query = (from o in filteredLeads

                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                         from s4 in j4.DefaultIfEmpty()

                         join o5 in _leadactivityRepository.GetAll() on o.Id equals o5.LeadId into j5
                         from s5 in j5.DefaultIfEmpty()

                         select new GetLeadForViewDto()
                         {
                             Lead = new LeadDto
                             {
                                 CompanyName = o.CompanyName,
                                 Email = o.Email,
                                 Phone = o.Phone,
                                 Mobile = o.Mobile,
                                 Address = o.Address,
                                 Requirements = o.Requirements,
                                 Id = o.Id,
                                 PostCode = o.PostCode,
                                 LeadStatusID = o.LeadStatusId,
                                 Suburb = o.Suburb,
                                 LeadSource = o.LeadSource,
                                 State = o.State,
                                 OrganizationId = o.OrganizationId,
                                 OrganizationName = s4.DisplayName,
                                 longitude = o.longitude,
                                 latitude = o.latitude,
                                 FollowupDate = s5.CreationTime,
                                 CreationTime = (DateTime)o.LeadAssignDate
                             },
                             LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),

                         });

            var myleadListDtos = await query.ToListAsync();
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _leadsExcelExporter.MyLeadExportToFile(myleadListDtos);
            }
            else
            {
                return _leadsExcelExporter.MyLeadCsvExport(myleadListDtos);
            }

        }

        /// <summary>
        /// Closed Leads API (LeadStatusID = 9)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetLeadForViewDto>> GetAllForMyClosedLead(GetAllLeadsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var jobnumberlist = new List<int?>();
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber == input.Filter).Select(e => e.LeadId).ToList();

            var filteredLeads = _leadRepository.GetAll()
                        .Include(e => e.LeadStatusFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadStatusId != null, e => e.LeadStatusId == input.LeadStatusId)
                        .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                        .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                        .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .Where(e => e.LeadStatusId == 9);

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "ChangeStatusDate desc")
                .PageBy(input);

            var leads = from o in pagedAndFilteredLeads

                        join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                        from s3 in j3.DefaultIfEmpty()

                        join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                        from s4 in j4.DefaultIfEmpty()

                        select new GetLeadForViewDto()
                        {
                            Lead = new LeadDto
                            {
                                CompanyName = o.CompanyName,
                                Email = o.Email,
                                Phone = o.Phone,
                                Mobile = o.Mobile,
                                Address = o.Address,
                                Requirements = o.Requirements,
                                Id = o.Id,
                                PostCode = o.PostCode,
                                LeadStatusID = o.LeadStatusId,
                                Suburb = o.Suburb,
                                LeadSource = o.LeadSource,
                                State = o.State,
                                OrganizationId = o.OrganizationId,
                                OrganizationName = s4.DisplayName
                            },
                            LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                            Duplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID != null && e.OrganizationId == input.OrganizationUnit).Any(),
                            WebDuplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.OrganizationUnit).Any(),
                        };

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetLeadForViewDto>(
                totalCount,
                await leads.ToListAsync()
            );
        }

        /// <summary>
        /// Closed Lead Export to Excel API
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<FileDto> getClosedLeadsToExcel(LeadExcelExportTto input)
        {
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var jobnumberlist = new List<int?>();
            //jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

            //var filteredLeads = _leadRepository.GetAll()
            //            .Include(e => e.LeadStatusFk)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
            //            .WhereIf(input.LeadStatusId != null, e => e.LeadStatusId == input.LeadStatusId)
            //        .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
            //            .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
            //            .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
            //            .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
            //            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
            //            .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
            //            .Where(e => e.LeadStatusId == 9);

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var jobnumberlist = new List<int?>();
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber == input.Filter).Select(e => e.LeadId).ToList();

            var filteredLeads = _leadRepository.GetAll()
                        .Include(e => e.LeadStatusFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadStatusId != null, e => e.LeadStatusId == input.LeadStatusId)
                        .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                        .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                        .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .Where(e => e.LeadStatusId == 9);

            var query = (from o in filteredLeads

                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                         from s4 in j4.DefaultIfEmpty()

                         select new GetLeadForViewDto()
                         {
                             Lead = new LeadDto
                             {
                                 CompanyName = o.CompanyName,
                                 Email = o.Email,
                                 Phone = o.Phone,
                                 Mobile = o.Mobile,
                                 Address = o.Address,
                                 Requirements = o.Requirements,
                                 Id = o.Id,
                                 PostCode = o.PostCode,
                                 LeadStatusID = o.LeadStatusId,
                                 Suburb = o.Suburb,
                                 LeadSource = o.LeadSource,
                                 State = o.State,
                                 OrganizationId = o.OrganizationId,
                                 OrganizationName = s4.DisplayName
                             },
                             LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                             Duplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID != null && e.OrganizationId == input.OrganizationUnit).Any(),
                             WebDuplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.OrganizationUnit).Any(),
                         });

            var closedleadListDtos = await query.ToListAsync();
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _leadsExcelExporter.ClosedLeadExportToFile(closedleadListDtos);
            }
            else
            {
                return _leadsExcelExporter.ClosedLeadCsvExport(closedleadListDtos);
            }

        }

        /// <summary>
        /// Cancel Leads API (LeadStatusID = 8)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetLeadForViewDto>> GetAllForCancelLead(GetAllLeadsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = _userTeamRepository.GetAll().Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }

            var jobnumberlist = new List<int?>();
            jobnumberlist = _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit)
                .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber.Contains(input.Filter))
                 .WhereIf(input.jobStatusIDsFilter != null && input.jobStatusIDsFilter.Count() > 0, e => input.jobStatusIDsFilter.Contains((int)e.JobStatusId))
                       .WhereIf(input.DateFilterType == "FirstDeposite" && input.StartDate != null, e => e.FirstDepositDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateFilterType == "FirstDeposite" && input.EndDate != null, e => e.FirstDepositDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateFilterType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateFilterType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateFilterType == "Active" && input.StartDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.DateFilterType == "Active" && input.EndDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
               .Select(e => e.LeadId).ToList();

            

           
            var filteredLeads = _leadRepository.GetAll()
            //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                         .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                         .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                         .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                         .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                         .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                         .WhereIf(input.FilterName == "Requirements" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Requirements == input.Filter)
                         .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Phone == input.Filter)
                         .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                        //.WhereIf(input.LeadSourceIdFilter != 0 && input.LeadSourceIdFilter != null, e => e.LeadSourceId == input.LeadSourceIdFilter)
                        .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateFilterType == "Cancel" && input.StartDate != null && input.EndDate != null, e => e.ChangeStatusDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ChangeStatusDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        
                       .WhereIf(input.jobStatusIDsFilter != null && input.jobStatusIDsFilter.Count() > 0, e => jobnumberlist.Contains(e.Id))
                       .WhereIf(input.DateFilterType == "FirstDeposite" && input.StartDate != null, e => jobnumberlist.Contains(e.Id))
                        .WhereIf(input.DateFilterType == "FirstDeposite" && input.EndDate != null, e => jobnumberlist.Contains(e.Id))

                        .WhereIf(input.DateFilterType == "Active" && input.StartDate != null, e => jobnumberlist.Contains(e.Id))
                        .WhereIf(input.DateFilterType == "Active" && input.EndDate != null, e => jobnumberlist.Contains(e.Id))

                        .WhereIf(input.DateFilterType == "DepositeReceived" && input.StartDate != null, e => jobnumberlist.Contains(e.Id))
                        .WhereIf(input.DateFilterType == "DepositeReceived" && input.EndDate != null, e => jobnumberlist.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.OrganizationUnit != 0, e => e.OrganizationId == input.OrganizationUnit)
                        .WhereIf(input.CancelReasonId != 0, e => e.CancelReasonId == input.CancelReasonId)
                        .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.AssignToUserID))
                        /*.Where(e => e.LeadStatusId == 8)*/
                        ;
            var filteredsLeads = filteredLeads.Where(e => e.LeadStatusId == 8);
            var pagedAndFilteredLeads = filteredsLeads
                .OrderBy(input.Sorting ?? "ChangeStatusDate desc")
                .PageBy(input);

            var leads = from o in pagedAndFilteredLeads
                            //join o1 in _userRepository.GetAll() on o.LastModifierUserId equals o1.Id into j1
                            //from s1 in j1.DefaultIfEmpty()

                            //join o2 in _lookup_cancelReasonRepository.GetAll() on o.LeadStatusId equals o2.Id into j2
                            //from s2 in j2.DefaultIfEmpty()

                        join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                        from s3 in j3.DefaultIfEmpty()
                        join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                        from s4 in j4.DefaultIfEmpty()

                        select new GetLeadForViewDto()
                        {
                            Lead = new LeadDto
                            {
                                CompanyName = o.CompanyName,
                                Email = o.Email,
                                Phone = o.Phone,
                                Mobile = o.Mobile,
                                Address = o.Address,
                                Requirements = o.Requirements,
                                Id = o.Id,
                                PostCode = o.PostCode,
                                RejectReason = o.RejectReason,
                                RejectReasonId = o.RejectReasonId,
                                CancelReasonId = o.CancelReasonId,
                                LeadStatusID = o.LeadStatusId,
                                Suburb = o.Suburb,
                                LeadSource = o.LeadSource,
                                State = o.State,
                                OrganizationId = o.OrganizationId,
                                OrganizationName = s4.DisplayName,
                                ChangeStatusDate = o.ChangeStatusDate
                            },
                            LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                            LeadSourceName = _lookup_leadSourceRepository.GetAll().Where(e => (e.Id == o.LeadSourceId)).Select(e => e.Name).FirstOrDefault(),
                            CancelReasonName = _lookup_cancelReasonRepository.GetAll().Where(e => (e.Id == o.CancelReasonId)).Select(e => e.CancelReasonName).FirstOrDefault(),
                            LastModifiedUser = _userRepository.GetAll().Where(e => (e.Id == o.LastModifierUserId)).Select(e => e.Name).FirstOrDefault(),
                            //Duplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID != null && e.OrganizationId == input.OrganizationUnit).Any(),
                            //WebDuplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.OrganizationUnit).Any(),
                            //Facebook = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Facebook").Count()),
                            //Online = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Google").Count()),
                            //Tv = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "TV").Count()),
                            //Referral = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Referral").Count()),
                            //Others = Convert.ToString(filteredLeads.Where(e => e.LeadSource != "Facebook" && e.LeadSource != "Google" && e.LeadSource != "Tv" && e.LeadSource != "Refeal").Count()),
                            //Total = Convert.ToString(filteredLeads.Count())
                        };

            var totalCount = filteredsLeads.Count();

            return new PagedResultDto<GetLeadForViewDto>(
                totalCount,
                await leads.ToListAsync()
            );
        }

        public async Task<CancleLeadsSummaryCount> GetAllForCancelLeadCount(GetAllLeadsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = _userTeamRepository.GetAll().Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }

            var jobnumberlist = new List<int?>();
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber == input.Filter).Select(e => e.LeadId).ToList();

            var filteredLeads = _leadRepository.GetAll()
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                         .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                         .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                         .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                         .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                         .WhereIf(input.FilterName == "Requirements" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Requirements == input.Filter)
                         .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Phone == input.Filter)
                         .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                        //.WhereIf(input.LeadSourceIdFilter != 0 && input.LeadSourceIdFilter != null, e => e.LeadSourceId == input.LeadSourceIdFilter)
                        .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateFilterType == "Cancel" && input.StartDate != null && input.EndDate != null, e => e.ChangeStatusDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ChangeStatusDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.OrganizationUnit != 0, e => e.OrganizationId == input.OrganizationUnit)
                        .WhereIf(input.CancelReasonId != 0, e => e.CancelReasonId == input.CancelReasonId)
                        .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.AssignToUserID))
                        .Where(e => e.LeadStatusId == 8);

            var output = new CancleLeadsSummaryCount();
            output.Facebook = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Facebook").Count());
            output.Online = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Google").Count());
            output.Tv = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "TV").Count());
            output.Referral = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Referral").Count());
            output.Others = Convert.ToString(filteredLeads.Where(e => e.LeadSource != "Facebook" && e.LeadSource != "Google" && e.LeadSource != "Tv" && e.LeadSource != "Refeal").Count());
            output.Total = Convert.ToString(filteredLeads.Count());


            return output;

        }

        /// <summary>
        /// Cancel Lead Export to Excel API
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<FileDto> getCancelLeadsToExcel(LeadExcelExportTto input)
        {
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();

            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var jobnumberlist = new List<int?>();
            //jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

            //var filteredLeads = _leadRepository.GetAll()
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
            //            .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
            //            //.WhereIf(input.LeadSourceIdFilter != 0 && input.LeadSourceIdFilter != null, e => e.LeadSourceId == input.LeadSourceIdFilter)
            //            .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
            //            .WhereIf(input.DateFilterType == "Cancel" && input.StartDate != null && input.EndDate != null, e => e.ChangeStatusDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ChangeStatusDate.Value.AddHours(10).Date <= EDate.Value.Date)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
            //            .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
            //            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
            //            .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
            //            .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
            //            .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)
            //            .WhereIf(input.OrganizationUnit != 0, e => e.OrganizationId == input.OrganizationUnit)
            //            .WhereIf(input.CancelReasonId != 0, e => e.CancelReasonId == input.CancelReasonId)
            //            .Where(e => e.LeadStatusId == 8);
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = _userTeamRepository.GetAll().Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }

            var jobnumberlist = new List<int?>();
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber == input.Filter).Select(e => e.LeadId).ToList();

            var filteredLeads = _leadRepository.GetAll()
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                         .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                         .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                         .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                         .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                         .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Phone == input.Filter)
                         .WhereIf(input.FilterName == "Requirements" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Requirements == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                        //.WhereIf(input.LeadSourceIdFilter != 0 && input.LeadSourceIdFilter != null, e => e.LeadSourceId == input.LeadSourceIdFilter)
                        .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateFilterType == "Cancel" && input.StartDate != null && input.EndDate != null, e => e.ChangeStatusDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ChangeStatusDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.OrganizationUnit != 0, e => e.OrganizationId == input.OrganizationUnit)
                        .WhereIf(input.CancelReasonId != 0, e => e.CancelReasonId == input.CancelReasonId)
                        .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.AssignToUserID))
                        .Where(e => e.LeadStatusId == 8);

            var query = (from o in filteredLeads
                         join o1 in _userRepository.GetAll() on o.LastModifierUserId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _lookup_cancelReasonRepository.GetAll() on o.LeadStatusId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()
                         join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                         from s4 in j4.DefaultIfEmpty()

                         select new GetLeadForViewDto()
                         {
                             Lead = new LeadDto
                             {
                                 CompanyName = o.CompanyName,
                                 Email = o.Email,
                                 Phone = o.Phone,
                                 Mobile = o.Mobile,
                                 Address = o.Address,
                                 Requirements = o.Requirements,
                                 Id = o.Id,
                                 PostCode = o.PostCode,
                                 RejectReason = o.RejectReason,
                                 RejectReasonId = o.RejectReasonId,
                                 CancelReasonId = o.CancelReasonId,
                                 LeadStatusID = o.LeadStatusId,
                                 Suburb = o.Suburb,
                                 LeadSource = o.LeadSource,
                                 State = o.State,
                                 OrganizationId = o.OrganizationId,
                                 OrganizationName = s4.DisplayName
                             },
                             LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                             LeadSourceName = _lookup_leadSourceRepository.GetAll().Where(e => (e.Id == o.LeadSourceId)).Select(e => e.Name).FirstOrDefault(),
                             CancelReasonName = _lookup_cancelReasonRepository.GetAll().Where(e => (e.Id == o.CancelReasonId)).Select(e => e.CancelReasonName).FirstOrDefault(),
                             LastModifiedUser = _userRepository.GetAll().Where(e => (e.Id == o.LastModifierUserId)).Select(e => e.Name).FirstOrDefault(),
                             Duplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID != null && e.OrganizationId == input.OrganizationUnit).Any(),
                             WebDuplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.OrganizationUnit).Any(),
                         });

            var cancelleadListDtos = await query.ToListAsync();
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _leadsExcelExporter.CancelLeadExportToFile(cancelleadListDtos);
            }
            else
            {
                return _leadsExcelExporter.CancelLeadCsvExport(cancelleadListDtos);
            }

        }

        /// <summary>
        /// Cancel Leads API (LeadStatusID = 7)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetLeadForViewDto>> GetAllForRejectedLead(GetAllLeadsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var jobnumberlist = new List<int?>();
            jobnumberlist = _jobRepository.GetAll()
                .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber.Contains(input.Filter))
                 .WhereIf(input.jobStatusIDsFilter != null && input.jobStatusIDsFilter.Count() > 0, e => input.jobStatusIDsFilter.Contains((int)e.JobStatusId))
                       .WhereIf(input.DateFilterType == "FirstDeposite" && input.StartDate != null, e => e.FirstDepositDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateFilterType == "FirstDeposite" && input.EndDate != null, e => e.FirstDepositDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateFilterType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateFilterType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateFilterType == "Active" && input.StartDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.DateFilterType == "Active" && input.EndDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
               .Select(e => e.LeadId).ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = _userTeamRepository.GetAll().Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }

            var filteredLeads = _leadRepository.GetAll()
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                         .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                         .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                         .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                         .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Phone == input.Filter)
                         .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                         .WhereIf(input.FilterName == "Requirements" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Requirements == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                       //.WhereIf(input.LeadSourceIdFilter != 0 && input.LeadSourceIdFilter != null, e => e.LeadSourceId == input.LeadSourceIdFilter)
                     
                        .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateFilterType == "Rejected" && input.StartDate != null && input.EndDate != null, e => e.ChangeStatusDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ChangeStatusDate.Value.AddHours(10).Date <= EDate.Value.Date)
                          .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDsFilter.Count() > 0, e => jobnumberlist.Contains(e.Id))
                       .WhereIf(input.DateFilterType == "FirstDeposite" && input.StartDate != null, e => jobnumberlist.Contains(e.Id))
                        .WhereIf(input.DateFilterType == "FirstDeposite" && input.EndDate != null, e => jobnumberlist.Contains(e.Id))

                        .WhereIf(input.DateFilterType == "DepositeReceived" && input.StartDate != null, e => jobnumberlist.Contains(e.Id))
                        .WhereIf(input.DateFilterType == "DepositeReceived" && input.EndDate != null, e => jobnumberlist.Contains(e.Id))

                        .WhereIf(input.DateFilterType == "Active" && input.StartDate != null, e => jobnumberlist.Contains(e.Id))
                        .WhereIf(input.DateFilterType == "Active" && input.EndDate != null, e => jobnumberlist.Contains(e.Id))

                        .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.OrganizationUnit != 0, e => e.OrganizationId == input.OrganizationUnit)
                        .WhereIf(input.RejectReasonId != 0, e => e.RejectReasonId == input.RejectReasonId)
                        .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.AssignToUserID))
                        .Where(e => e.LeadStatusId == 7);

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "ChangeStatusDate desc")
                .PageBy(input);

            var leads = from o in pagedAndFilteredLeads

                            //join o2 in _userRepository.GetAll() on o.LastModifierUserId equals o2.Id into j2
                            //from s2 in j2.DefaultIfEmpty()

                            //join o1 in _lookup_rejectReasonRepository.GetAll() on o.RejectReasonId equals o1.Id into j1
                            //from s1 in j1.DefaultIfEmpty()

                        join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                        from s3 in j3.DefaultIfEmpty()

                        join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                        from s4 in j4.DefaultIfEmpty()

                        select new GetLeadForViewDto()
                        {
                            Lead = new LeadDto
                            {
                                CompanyName = o.CompanyName,
                                Email = o.Email,
                                Phone = o.Phone,
                                Mobile = o.Mobile,
                                Address = o.Address,
                                Requirements = o.Requirements,
                                Id = o.Id,
                                PostCode = o.PostCode,
                                LeadStatusID = o.LeadStatusId,
                                Suburb = o.Suburb,
                                LeadSource = o.LeadSource,
                                State = o.State,
                                RejectReason = o.RejectReason,
                                RejectReasonId = o.RejectReasonId,
                                CancelReasonId = o.CancelReasonId,
                                OrganizationId = o.OrganizationId,
                                OrganizationName = s4.DisplayName,
                                ChangeStatusDate = o.ChangeStatusDate
                            },
                            LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                            LastModifiedUser = _userRepository.GetAll().Where(e => (e.Id == o.LastModifierUserId)).Select(e => e.Name).FirstOrDefault(),
                            LeadSourceName = _lookup_leadSourceRepository.GetAll().Where(e => (e.Id == o.LeadSourceId)).Select(e => e.Name).FirstOrDefault(),
                            RejectReasonName = _lookup_rejectReasonRepository.GetAll().Where(e => (e.Id == o.RejectReasonId)).Select(e => e.RejectReasonName).FirstOrDefault(),
                            //Duplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID != null && e.OrganizationId == input.OrganizationUnit).Any(),
                            //WebDuplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.OrganizationUnit).Any(),
                            //Facebook = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Facebook").Count()),
                            //Online = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Google").Count()),
                            //Tv = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "TV").Count()),
                            //Referral = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Referral").Count()),
                            //Others = Convert.ToString(filteredLeads.Where(e => e.LeadSource != "Facebook" && e.LeadSource != "Google" && e.LeadSource != "Tv" && e.LeadSource != "Refeal").Count()),
                            //Total = Convert.ToString(filteredLeads.Count())
                        };

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetLeadForViewDto>(
                totalCount,
                await leads.ToListAsync()
            );
        }

        public async Task<CancleLeadsSummaryCount> GetAllRejectedLeadCount(GetAllLeadsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var jobnumberlist = new List<int?>();
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber == input.Filter).Select(e => e.LeadId).ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = _userTeamRepository.GetAll().Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }

            var filteredLeads = _leadRepository.GetAll()
                         //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                         .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                         .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                         .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                         .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                         .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Phone == input.Filter)
                         .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                         .WhereIf(input.FilterName == "Requirements" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Requirements == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                        //.WhereIf(input.LeadSourceIdFilter != 0 && input.LeadSourceIdFilter != null, e => e.LeadSourceId == input.LeadSourceIdFilter)
                        .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateFilterType == "Rejected" && input.StartDate != null && input.EndDate != null, e => e.ChangeStatusDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ChangeStatusDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.OrganizationUnit != 0, e => e.OrganizationId == input.OrganizationUnit)
                        .WhereIf(input.RejectReasonId != 0, e => e.RejectReasonId == input.RejectReasonId)
                        .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.AssignToUserID))
                        .Where(e => e.LeadStatusId == 7);

            var output = new CancleLeadsSummaryCount();
            output.Facebook = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Facebook").Count());
            output.Online = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Google").Count());
            output.Tv = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "TV").Count());
            output.Referral = Convert.ToString(filteredLeads.Where(e => e.LeadSource == "Referral").Count());
            output.Others = Convert.ToString(filteredLeads.Where(e => e.LeadSource != "Facebook" && e.LeadSource != "Google" && e.LeadSource != "Tv" && e.LeadSource != "Refeal").Count());
            output.Total = Convert.ToString(filteredLeads.Count());


            return output;

        }

        /// <summary>
        /// Reject Lead Export to Excel API
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<FileDto> getRejectLeadsToExcel(LeadExcelExportTto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var jobnumberlist = new List<int?>();
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber == input.Filter).Select(e => e.LeadId).ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = _userTeamRepository.GetAll().Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }

            var filteredLeads = _leadRepository.GetAll()
                           //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                           .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                         .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                         .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                         .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                         .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Phone == input.Filter)
                         .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                         .WhereIf(input.FilterName == "Requirements" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Requirements == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter).WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                        //.WhereIf(input.LeadSourceIdFilter != 0 && input.LeadSourceIdFilter != null, e => e.LeadSourceId == input.LeadSourceIdFilter)
                        .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.DateFilterType == "Rejected" && input.StartDate != null && input.EndDate != null, e => e.ChangeStatusDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ChangeStatusDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.OrganizationUnit != 0, e => e.OrganizationId == input.OrganizationUnit)
                        .WhereIf(input.RejectReasonId != 0, e => e.RejectReasonId == input.RejectReasonId)
                        .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.AssignToUserID))
                        .Where(e => e.LeadStatusId == 7);

            var query = (from o in filteredLeads

                         join o2 in _userRepository.GetAll() on o.LastModifierUserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o1 in _lookup_rejectReasonRepository.GetAll() on o.RejectReasonId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                         from s4 in j4.DefaultIfEmpty()

                         select new GetLeadForViewDto()
                         {
                             Lead = new LeadDto
                             {
                                 CompanyName = o.CompanyName,
                                 Email = o.Email,
                                 Phone = o.Phone,
                                 Mobile = o.Mobile,
                                 Address = o.Address,
                                 Requirements = o.Requirements,
                                 Id = o.Id,
                                 PostCode = o.PostCode,
                                 LeadStatusID = o.LeadStatusId,
                                 Suburb = o.Suburb,
                                 LeadSource = o.LeadSource,
                                 State = o.State,
                                 RejectReason = o.RejectReason,
                                 RejectReasonId = o.RejectReasonId,
                                 CancelReasonId = o.CancelReasonId,
                                 OrganizationId = o.OrganizationId,
                                 OrganizationName = s4.DisplayName
                             },
                             LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                             LastModifiedUser = _userRepository.GetAll().Where(e => (e.Id == o.LastModifierUserId)).Select(e => e.Name).FirstOrDefault(),
                             LeadSourceName = _lookup_leadSourceRepository.GetAll().Where(e => (e.Id == o.LeadSourceId)).Select(e => e.Name).FirstOrDefault(),
                             RejectReasonName = _lookup_rejectReasonRepository.GetAll().Where(e => (e.Id == o.RejectReasonId)).Select(e => e.RejectReasonName).FirstOrDefault(),
                             Duplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID != null && e.OrganizationId == input.OrganizationUnit).Any(),
                             WebDuplicate = _leadRepository.GetAll().Where(e => (e.Email == o.Email || e.Mobile == o.Mobile) && e.Id != o.Id && e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.OrganizationUnit).Any(),

                         });

            var rejectleadListDtos = await query.ToListAsync();
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _leadsExcelExporter.RejectLeadExportToFile(rejectleadListDtos);
            }
            else
            {
                return _leadsExcelExporter.RejectLeadCsvExport(rejectleadListDtos);
            }

        }

        /// <summary>
        /// Lead Detail View API
        /// </summary>
        /// <param name="id">Lead Id</param>
        /// <returns></returns>
        public async Task<GetLeadForViewDto> GetLeadForView(int id, int? JobPromoId)
        {
            var lead = await _leadRepository.GetAsync(id);

            var output = new GetLeadForViewDto { Lead = ObjectMapper.Map<LeadDto>(lead) };

            var user = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var job_data = _jobRepository.GetAll().Where(e => e.LeadId == id).FirstOrDefault();
            var jobwarranty = _JobwarrantyRepository.GetAll().Where(e => e.JobId == job_data.Id);
            var org = _extendedOrganizationUnitRepository.GetAll();
            var reviewType = _reviewTypeRepository.GetAll();
            if (job_data != null)
            {
                var StatusId = job_data.JobStatusId;
                var TotalCost = job_data.TotalCost;
                output.Reviewlink = reviewType.Where(e => e.Id == job_data.ReviewType).Select(e => e.ReviewLink).FirstOrDefault();
                output.JobStatusName = _lookup_jobStatusRepository.GetAll().Where(e => e.Id == StatusId).Select(e => e.Name).FirstOrDefault();
                output.JobStatusId = job_data.JobStatusId;
                output.JobNumber = job_data.JobNumber;
                output.SystemCapacity = job_data.SystemCapacity;
                output.TotalQuotaion = job_data.TotalCost;
                output.InstallationDate = job_data.InstallationDate;
                output.CreationTime = job_data.CreationTime;
                output.Owning = (TotalCost - ((_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == job_data.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == job_data.Id).Select(e => e.PaidAmmount).Sum())));

                //var InstallationId = _jobRepository.GetAll().Where(e => e.LeadId == id).Select(e => e.InstallerId).FirstOrDefault();
                var InstallationId = job_data.InstallerId;
                if (InstallationId != null)
                {
                    var _lookupInstallerName = await _userRepository.FirstOrDefaultAsync((int)InstallationId);
                    output.InstallerName = _lookupInstallerName?.FullName?.ToString();
                }
                var JobProductIteam = _jobProductItemRepository.GetAll().Where(e => e.JobId == job_data.Id).ToList();
                if (JobProductIteam.Count > 0)
                {
                    var QunityAndModelList = (from o in JobProductIteam
                                              join o1 in _ProductItemRepository.GetAll() on o.ProductItemId equals o1.Id into j1
                                              from s1 in j1.DefaultIfEmpty()
                                              select new
                                              {
                                                  Name = o.Quantity + " x " + s1.Name
                                              }).ToList();
                    output.QunityAndModelList = String.Join(",", QunityAndModelList.Select(p => p.Name));
                }

                output.SignedQuotedDoc = _DocumentRepository.GetAll().Where(e => e.JobId == job_data.Id && e.DocumentTypeId == 5).OrderByDescending(e => e.Id).Select(e => e.FileName).Distinct().FirstOrDefault();
                output.SignedQuotedDocPath = _DocumentRepository.GetAll().Where(e => e.JobId == job_data.Id && e.DocumentTypeId == 5).OrderByDescending(e => e.Id).Select(e => e.FilePath).Distinct().FirstOrDefault();

                output.PanelBrocherDoc = _ProductItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Id == (_jobProductItemRepository.GetAll().Where(e => e.JobId == job_data.Id).Select(e => e.ProductItemId).FirstOrDefault())).Select(e => e.FileName).FirstOrDefault();
                output.PanelBrocherDocPath = _ProductItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Id == (_jobProductItemRepository.GetAll().Where(e => e.JobId == job_data.Id).Select(e => e.ProductItemId).FirstOrDefault())).Select(e => e.FilePath).FirstOrDefault();

                output.InverterBrocherDoc = _ProductItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == (from item in _jobProductItemRepository.GetAll()
                                                                                                                        join i in _ProductItemRepository.GetAll() on item.ProductItemId equals i.Id into
                                                                                                                        itmgoup
                                                                                                                        from i in itmgoup
                                                                                                                        where (i.ProductTypeId == 2 && item.JobId == job_data.Id)
                                                                                                                        select (item.ProductItemId)).FirstOrDefault()).Select(e => e.FileName).FirstOrDefault();
                output.InverterBrocherDocPath = _ProductItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == (from item in _jobProductItemRepository.GetAll()
                                                                                                                            join i in _ProductItemRepository.GetAll() on item.ProductItemId equals i.Id into
                                                                                                                            itmgoup
                                                                                                                            from i in itmgoup
                                                                                                                            where (i.ProductTypeId == 2 && item.JobId == job_data.Id)
                                                                                                                            select (item.ProductItemId)).FirstOrDefault()).Select(e => e.FilePath).FirstOrDefault();

                output.jobid = job_data.Id;
                output.filelist = (from item in _EmailTempDataRepository.GetAll().Where(e => e.JobId == job_data.Id)
                                   select new SendEmailAttachmentDto
                                   {
                                       FileName = item.FileName,
                                       FilePath = item.FilePath,
                                       id = item.Id,
                                   }).ToList();


                output.PanelWarrantyDoc = jobwarranty.Where(e => e.ProductTypeId == 12).OrderByDescending(e => e.Id).Select(e => e.Filename).FirstOrDefault();
                output.PanelWarrantyFileName = jobwarranty.Where(e => e.ProductTypeId == 12).OrderByDescending(e => e.Id).Select(e => e.FileType).FirstOrDefault();
                output.PanelWarrantyDocPath = jobwarranty.Where(e => e.ProductTypeId == 12).OrderByDescending(e => e.Id).Select(e => e.Filepath).FirstOrDefault();
                output.InverterWarrantyDoc = jobwarranty.Where(e => e.ProductTypeId == 13).OrderByDescending(e => e.Id).Select(e => e.Filename).FirstOrDefault();
                output.InverterWarrantyFileName = jobwarranty.Where(e => e.ProductTypeId == 13).OrderByDescending(e => e.Id).Select(e => e.FileType).FirstOrDefault();
                output.InverterWarrantyDocPath = jobwarranty.Where(e => e.ProductTypeId == 13).OrderByDescending(e => e.Id).Select(e => e.Filepath).FirstOrDefault();
                if (output.SignedQuotedDoc != null && output.PanelWarrantyDoc != null && output.InverterWarrantyDoc != null)
                { output.alldoc = true; }
                else { output.alldoc = false; }

                var pylonDocument = _pylonDocumentRepository.GetAll().Where(e => e.JobId == job_data.Id).OrderByDescending(e => e.Id).FirstOrDefault();
                if (pylonDocument != null)
                {
                    output.PylonUrl = pylonDocument.Url;
                }
            }

            output.leadid = lead.Id;
            output.Lead.FName = lead.FormName;

            if (JobPromoId != 0)
            {
                var freebies = _jobPromotionRepository.GetAll().Where(e => e.Id == JobPromoId).FirstOrDefault();
                if (freebies != null)
                {
                    var freebies_List = _freebieTransportRepository.GetAll().Where(e => e.Id == freebies.FreebieTransportId);
                    output.DispatchedDate = freebies.DispatchedDate;
                    output.TrackingNo = freebies.TrackingNumber;
                    output.TransportCompanyName = freebies_List.Select(e => e.Name).FirstOrDefault();
                    output.TransportLink = freebies_List.Select(e => e.TransportLink).FirstOrDefault();
                    output.FreebiesPromoType = _promotionMasterRepository.GetAll().Where(e => e.Id == freebies.PromotionMasterId).Select(e => e.Name).FirstOrDefault();
                }
            }

            if (output.Lead.LeadStatusID != null)
            {
                var _lookupLeadStatus = await _lookup_leadStatusRepository.FirstOrDefaultAsync((int)output.Lead.LeadStatusID);
                output.LeadStatusName = _lookupLeadStatus?.Status?.ToString();
            }

            if (lead.CreatorUserId != null)
            {
                var _lookupUserStatus = await _userRepository.FirstOrDefaultAsync((int)lead.CreatorUserId);
                output.CreatedByName = _lookupUserStatus?.FullName?.ToString();
            }

            if (lead.AssignToUserID != null)
            {
                var _lookupUserStatus = await _userRepository.FirstOrDefaultAsync((int)lead.AssignToUserID);
                output.CurrentAssignUserName = _lookupUserStatus?.FullName?.ToString();
                output.CurrentAssignUserEmail = _lookupUserStatus?.EmailAddress?.ToString();
                output.CurrentAssignUserMobile = _lookupUserStatus?.PhoneNumber?.ToString();
            }
            else
            {
                var DupLead = _leadRepository.GetAll().Where(e => (e.Email == lead.Email || e.Mobile == lead.Mobile) && e.Id != lead.Id && e.OrganizationId == lead.OrganizationId && e.AssignToUserID != null).FirstOrDefault();
                if (DupLead != null)
                {
                    var _lookupUserStatus = await _userRepository.FirstOrDefaultAsync((int)DupLead.AssignToUserID);
                    output.CurrentAssignUserName = _lookupUserStatus?.FullName?.ToString();
                }
            }

            output.CreatedOn = lead.CreationTime;
            output.latitude = lead.latitude;
            output.longitude = lead.longitude;
            output.UserName = user.Name;
            output.UserEmail = user.EmailAddress;
            output.UserPhone = user.PhoneNumber;
            output.OrgName = org.Where(e => e.Id == lead.OrganizationId).Select(e => e.DisplayName).FirstOrDefault();
            output.OrgMobile = org.Where(e => e.Id == lead.OrganizationId).Select(e => e.Mobile).FirstOrDefault();
            output.OrgEmail = org.Where(e => e.Id == lead.OrganizationId).Select(e => e.Email).FirstOrDefault();
            output.OrgLogo = org.Where(e => e.Id == lead.OrganizationId).Select(e => (e.LogoFilePath + e.LogoFileName)).FirstOrDefault();

            output.OrgLogo = output.OrgLogo != null ? output.OrgLogo.Replace("\\", "/") : "";

            return output;
        }

        /// <summary>
        /// Check Lead Existancy (Address, Email, Phone)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<List<GetDuplicateLeadPopupDto>> CheckExistLeadList(CheckExistLeadDto input)
        {
            var User = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

            IList<string> role = await _userManager.GetRolesAsync(User);

            //if (role.Contains("Admin"))
            //{
            //	return new List<GetDuplicateLeadPopupDto>();
            //}
            var Mobile = input.Mobile.Substring(input.Mobile.Length - 9);

            var duplicateLead = _leadRepository.GetAll().Where(e => e.Mobile.Contains(Mobile) || e.Email == input.Email ||
                                (e.UnitNo == input.UnitNo && e.UnitType == input.UnitType && e.StreetNo == input.StreetNo && e.StreetName == input.StreetName
                                && e.StreetType == input.StreetType && e.Suburb == input.Suburb && e.State == input.State && e.PostCode == input.PostCode))
                                .Where(e => e.OrganizationId == input.OrganizationId)
                                .WhereIf(input.Id != null, e => e.Id != input.Id)
                                .Where(e => e.IsDuplicate != true && e.IsWebDuplicate != true);

            var dupLeads = true;
            var dupUserLeads = duplicateLead.Where(e => e.AssignToUserID != User.Id).Select(e => e.Id).ToList();
            if (dupUserLeads.Count > 0)
            {
                var dbDupLeadInstallCount = await _jobRepository.GetAll().CountAsync(e => e.JobStatusId == 8 && dupUserLeads.Contains((int)e.LeadId));
                if (dupUserLeads.Count == dbDupLeadInstallCount)
                {
                    dupLeads = true;
                }
                else
                {
                    dupLeads = false;
                }
            }

            var leads = from o in duplicateLead

                        join o1 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o1.Id into j1
                        from s1 in j1.DefaultIfEmpty()

                        join o2 in _userRepository.GetAll() on o.CreatorUserId equals o2.Id into j2
                        from s2 in j2.DefaultIfEmpty()

                        select new GetDuplicateLeadPopupDto()
                        {
                            CreatedByName = s2.FullName,
                            CreationTime = o.CreationTime,
                            Address = o.Address,
                            CompanyName = o.CompanyName,
                            Email = o.Email,
                            Id = o.Id,
                            Requirements = o.Requirements,
                            Phone = o.Phone,
                            Mobile = o.Mobile,
                            LeadSource = o.LeadSource,
                            LeadStatus = s1.Status,
                            CurrentAssignUserName = _userRepository.GetAll().Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                            UnitNo = o.UnitNo,
                            UnitType = o.UnitType,
                            StreetNo = o.StreetNo,
                            StreetType = o.StreetType,
                            StreetName = o.StreetName,
                            Suburb = o.Suburb,
                            State = o.State,
                            Postcode = o.PostCode,
                            CurrentLeadOwaner = s2.FullName,
                            AddressExist = (o.UnitNo == input.UnitNo && o.UnitType == input.UnitType && o.StreetNo == input.StreetNo && o.StreetName == input.StreetName && o.StreetType == input.StreetType && o.Suburb == input.Suburb && o.State == input.State && o.PostCode == input.PostCode),
                            EmailExist = o.Email == input.Email,
                            MobileExist = o.Mobile == input.Mobile,
                            RoleName = role[0],
                            JobStatus = _jobRepository.GetAll().Where(e => e.LeadId == o.Id).Select(e => e.JobStatusFk.Name).FirstOrDefault(),
                            dupLead = dupLeads
                        };

            return new List<GetDuplicateLeadPopupDto>(await leads.ToListAsync());

            //return new List<GetDuplicateLeadPopupDto>(
            //    await leads.ToListAsync()
            //);
        }

        /// <summary>
        /// Get Duplicate Lead Details (Email or Phone)
        /// </summary>
        /// <param name="id">Lead Id</param>
        /// <returns></returns>
        public async Task<List<GetDuplicateLeadPopupDto>> GetDuplicateLeadForView(int id, int orgId)
        {
            var lead = await _leadRepository.GetAsync(id);

            var duplicateLead = _leadRepository.GetAll().Where(e => ((e.Email == lead.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == lead.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.OrganizationId == orgId && e.Id != lead.Id && e.AssignToUserID != null);

            var leads = from o in duplicateLead
                        join o1 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o1.Id into j1
                        from s1 in j1.DefaultIfEmpty()
                        join o2 in _userRepository.GetAll() on o.CreatorUserId equals o2.Id into j2
                        from s2 in j2.DefaultIfEmpty()

                        join o3 in _jobRepository.GetAll() on o.Id equals o3.LeadId into j3
                        from s3 in j3.DefaultIfEmpty()

                            //join o4 in _lookup_jobStatusRepository.GetAll() on s3.JobStatusId equals o4.Id into j4
                            //from s4 in j4.DefaultIfEmpty()

                        select new GetDuplicateLeadPopupDto()
                        {
                            CreatedByName = s2.FullName, //_userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                            CreationTime = o.CreationTime,
                            Address = o.Address,
                            CompanyName = o.CompanyName,
                            Email = o.Email,
                            Id = o.Id,
                            Requirements = o.Requirements,
                            Phone = o.Phone,
                            Mobile = o.Mobile,
                            LeadSource = o.LeadSource,
                            LeadStatus = s1.Status,
                            CurrentAssignUserName = _userRepository.GetAll().Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                            UnitNo = o.UnitNo,
                            UnitType = o.UnitType,
                            StreetNo = o.StreetNo,
                            StreetType = o.StreetType,
                            StreetName = o.StreetName,
                            Suburb = o.Suburb,
                            State = o.State,
                            Postcode = o.PostCode,
                            ProjectStatus = s3.JobStatusFk.Name,
                            ProjectOpenDate = s3.CreationTime,
                            ProjectNo = s3.JobNumber,
                            LastFollowupDate = _leadactivityRepository.GetAll().Where(e => e.ActionId == 8 && e.LeadId == o.Id).OrderByDescending(x => x.Id).Select(e => e.CreationTime).FirstOrDefault(),
                            Description = _leadactivityRepository.GetAll().Where(e => e.ActionId == 8 && e.LeadId == o.Id).OrderByDescending(x => x.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                            LastQuoteDate = _quotationRepository.GetAll().Where(e => e.JobFk.LeadId == o.Id).OrderByDescending(x => x.Id).Select(e => e.CreationTime).FirstOrDefault()

                        };

            return new List<GetDuplicateLeadPopupDto>(
                await leads.ToListAsync()
            );
        }

        /// <summary>
        /// Get Lead Data for Edit
        /// </summary>
        /// <param name="input">Lead Id</param>
        /// <returns></returns>
        //[AbpAuthorize(AppPermissions.Pages_Leads_Edit, AppPermissions.Pages_MyLeads)]
        public async Task<GetLeadForEditOutput> GetLeadForEdit(EntityDto input)
        {
            var lead = await _leadRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLeadForEditOutput { Lead = ObjectMapper.Map<CreateOrEditLeadDto>(lead) };

            //if (AbpSession.UserId != null)
            //	output.CurrentUserId = (int)AbpSession.UserId;

            var User = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            if (role.Contains("Admin"))
            {
                output.OwnTeamUser = true;
            }
            else if (role.Contains("Sales Manager"))
            {
                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
                var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
                output.OwnTeamUser = UserList.Contains(lead.AssignToUserID);
            }
            else
            {
                output.OwnTeamUser = (User.Id == lead.AssignToUserID);
            }

            //if (output.Lead.Suburb != null)
            //{
            //	var _lookupPostCode = await _lookup_postCodeRepository.FirstOrDefaultAsync((int)output.Lead.Suburb);
            //	output.PostCodeSuburb = _lookupPostCode?.Suburb?.ToString();
            //}

            //if (output.Lead.State != null)
            //{
            //	var _lookupState = await _lookup_stateRepository.FirstOrDefaultAsync((int)output.Lead.State);
            //	output.StateName = _lookupState?.Name?.ToString();
            //}

            //if (output.Lead.LeadSource != null)
            //{
            //	var _lookupLeadSource = await _lookup_leadSourceRepository.FirstOrDefaultAsync((int)output.Lead.LeadSource);
            //	output.LeadSourceName = _lookupLeadSource?.Name?.ToString();
            //}

            if (output.Lead.LeadStatusID != null)
            {
                var _lookupLeadStatus = await _lookup_leadStatusRepository.FirstOrDefaultAsync((int)output.Lead.LeadStatusID);
                output.LeadStatusName = _lookupLeadStatus?.Status?.ToString();
            }

            var job = await _jobRepository.GetAll().Where(e => e.LeadId == lead.Id).FirstOrDefaultAsync();
            if (job != null)
            {
                output.JobId = job.Id;
            }

            return output;
        }

        /// <summary>
        /// Create Or Edit Leads
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task CreateOrEdit(CreateOrEditLeadDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        /// <summary>
        /// Create Lead
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        //[AbpAuthorize(AppPermissions.Pages_Leads_Create, AppPermissions.Pages_MyLeads)]
        protected virtual async Task Create(CreateOrEditLeadDto input)
        {
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

            IList<string> role = await _userManager.GetRolesAsync(assignedToUser);

            input.LeadSourceId = _lookup_leadSourceRepository.GetAll().Where(e => e.Name == input.LeadSource).Select(e => e.Id).FirstOrDefault();
            if (input.StateId == null)
            {
                input.StateId = _lookup_stateRepository.GetAll().Where(e => e.Name == input.State).Select(e => e.Id).FirstOrDefault();
            }
            if (input.SuburbId == null)
            {
                input.StateId = _lookup_postCodeRepository.GetAll().Where(e => e.PostalCode == input.PostCode).Select(e => e.Id).FirstOrDefault();
            }
            //if (input.PostalStateId == null)
            //{
            //	input.StateId = _lookup_stateRepository.GetAll().Where(e => e.Name == input.PostalState).Select(e => e.Id).FirstOrDefault();
            //}
            //if (input.PostalSuburbId == null)
            //{
            //	input.StateId = _lookup_postCodeRepository.GetAll().Where(e => e.PostalCode == input.PostalPostCode).Select(e => e.Id).FirstOrDefault();
            //}
            var lead = ObjectMapper.Map<Lead>(input);
            if (input.IsExternalLead == 3)
            {
                lead.Address = input.Address;
            }
            else
            {
                lead.Address = input.UnitNo + " " + input.UnitType + " " + input.StreetNo + " " + input.StreetName + " " + input.StreetType;
            }
            //lead.PostalAddress = lead.PostalUnitNo + " " + lead.PostalUnitType + " " + lead.PostalStreetNo + " " + lead.PostalStreetName + " " + lead.PostalStreetType;
            lead.IsPromotion = true;
            if (!string.IsNullOrWhiteSpace(input.Suburb))
            {
                string[] splitPipeValue = input.Suburb.Split('|');
                if (splitPipeValue.Length > 0)
                {
                    lead.Suburb = splitPipeValue[0].ToString();
                }
                else
                {
                    lead.Suburb = input.Suburb;
                }
            }

            //1. Create
            //2. Excel Import
            //3. External Link
            //4. Copy Lead
            lead.IsExternalLead = input.IsExternalLead;
            lead.LeadAssignDate = DateTime.UtcNow;

            if (input.from == "mylead")
            {
                lead.AssignToUserID = (int)AbpSession.UserId;
                lead.AssignDate = DateTime.UtcNow;
                lead.LeadStatusId = 2;

                lead.FirstAssignUserId = (long)AbpSession.UserId;
                lead.FirstAssignDate = DateTime.UtcNow;
            }
            else if (input.from == "myleads")
            {
                lead.AssignToUserID = (int)AbpSession.UserId;
                lead.AssignDate = DateTime.UtcNow;
                lead.LeadStatusId = 2;

                lead.FirstAssignUserId = (long)AbpSession.UserId;
                lead.FirstAssignDate = DateTime.UtcNow;
            }
            else
            {
                lead.LeadStatusId = 1;
            }

            lead.CreatorUserId = AbpSession.UserId;

            //Start Check Duplicate
            //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
            var dbDup = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationId && e.AssignToUserID != null && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == input.Mobile && !string.IsNullOrEmpty(e.Mobile)))).FirstOrDefault();
            if (dbDup != null)
            {
                var dbDupLeads = await _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationId && e.AssignToUserID != null && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == input.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Select(e => e.Id).ToListAsync();

                if (input.from == null)
                {
                    var dbDupLeadCount = dbDupLeads.Count();
                    var dbDupLeadInstallCount = await _jobRepository.GetAll().CountAsync(e => e.JobStatusId == 8 && dbDupLeads.Contains((int)e.LeadId));

                    if (dbDupLeadCount != dbDupLeadInstallCount)
                    {
                        lead.IsDuplicate = true;
                        lead.DublicateLeadId = dbDup.Id;
                    }
                    else
                    {
                        lead.IsDuplicate = false;
                    }

                }
                else if (input.from == "myleads" || input.from == "mylead")
                {
                    var dbDupLeadOther = await _leadRepository.GetAll().Where(e => e.AssignToUserID != AbpSession.UserId && dbDupLeads.Contains(e.Id)).Select(e => e.Id).ToListAsync();

                    if (dbDupLeadOther.Count > 0)
                    {
                        var dbDupLeadInstallCount = await _jobRepository.GetAll().CountAsync(e => e.JobStatusId == 8 && dbDupLeadOther.Contains((int)e.LeadId));

                        if (dbDupLeadOther.Count != dbDupLeadInstallCount)
                        {
                            lead.IsDuplicate = true;
                            lead.DublicateLeadId = dbDup.Id;
                        }
                        else
                        {
                            lead.IsDuplicate = false;
                        }
                    }
                    else
                    {
                        lead.IsDuplicate = false;
                    }
                }
                else
                {
                    lead.IsDuplicate = true;
                    lead.DublicateLeadId = dbDup.Id;
                }
            }
            else
            {
                lead.IsDuplicate = false;
            }

            //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
            var webDup = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationId && e.AssignToUserID == null && e.HideDublicate != true && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == input.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Any();
            if (webDup)
            {
                lead.IsWebDuplicate = true;
            }
            else
            {
                lead.IsWebDuplicate = false;
            }

            //var dbDupId = await _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationId && e.AssignToUserID != null && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == input.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Select(e => e.Id).FirstOrDefaultAsync();
            //if (dbDupId != null && dbDupId > 0)
            //{
            //    lead.DublicateLeadId = dbDupId;
            //}
            //End Check Duplicate

            if (role.Contains("Leadgen SalesRep"))
            {
                lead.LeadGenTransferDate = DateTime.UtcNow;
            }
            await _leadRepository.InsertAndGetIdAsync(lead);

            PreviousJobStatus jobStstus = new PreviousJobStatus();
            jobStstus.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                jobStstus.TenantId = (int)AbpSession.TenantId;
            }
            jobStstus.CurrentID = lead.LeadStatusId;
            jobStstus.PreviousId = 0;
            await _previousJobStatusRepository.InsertAsync(jobStstus);

            //1. Create
            //2. Excel Import
            //3. External Link
            //4. Copy Lead
            if (lead.IsExternalLead == 3)
            {
                var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == lead.CreatorUserId).FirstOrDefault();
                string msg = string.Format("New Lead Created {0}.", lead.CompanyName);
                await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
            }

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == lead.CreatorUserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 1;
            leadactivity.SectionId = input.SectionId > 0 ? input.SectionId : 0;
            leadactivity.ActionNote = "Lead Created";
            leadactivity.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }

            // for the history log
            //var mylead = await _leadRepository.FirstOrDefaultAsync((int)input.Id);
            var leadactivityId = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

            var List = new List<LeadtrackerHistory>();


            LeadtrackerHistory history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "CompanyName";
            history.PrevValue = "";
            history.CurValue = input.CompanyName;
            history.Action = "Add";
            history.LeadActionId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            List.Add(history);

            history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "Email";
            history.PrevValue = "";
            history.CurValue = input.Email;
            history.Action = "Add";
            history.LeadActionId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            List.Add(history);


            history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "Mobile";
            history.PrevValue = "";
            history.CurValue = input.Mobile;
            history.Action = "Add";
            history.LeadActionId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            List.Add(history);

            history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "Phone";
            history.PrevValue = "";
            history.CurValue = input.Phone;
            history.Action = "Add";
            history.LeadActionId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            List.Add(history);


            history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "AltPhone";
            history.PrevValue = "";
            history.CurValue = input.AltPhone;
            history.Action = "Add";
            history.LeadActionId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            List.Add(history);

            history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "Lead Source";
            history.PrevValue = "";
            history.CurValue = input.LeadSource;
            history.Action = "Add";
            history.LeadActionId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            List.Add(history);

            history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "Type";
            history.PrevValue = "";
            history.CurValue = input.Type;
            history.Action = "Add";
            history.LeadActionId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            List.Add(history);

            history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "Area";
            history.PrevValue = "";
            history.CurValue = input.Area;
            history.Action = "Add";
            history.LeadActionId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            List.Add(history);

            history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "Address";
            history.PrevValue = "";
            history.CurValue = input.Address;
            history.Action = "Add";
            history.LeadActionId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            List.Add(history);

            history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "UnitNo";
            history.PrevValue = "";
            history.CurValue = input.UnitNo;
            history.Action = "Add";
            history.LeadActionId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            List.Add(history);


            history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "UnitType";
            history.PrevValue = "";
            history.CurValue = input.UnitType;
            history.Action = "Add";
            history.LeadActionId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            List.Add(history);

            history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "StreetNo";
            history.PrevValue = "";
            history.CurValue = input.StreetNo;
            history.Action = "Add";
            history.LeadActionId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            List.Add(history);

            history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "StreetName";
            history.PrevValue = "";
            history.CurValue = input.StreetName;
            history.Action = "Add";
            history.LeadActionId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            List.Add(history);

            history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "StreetType";
            history.PrevValue = "";
            history.CurValue = input.StreetType;
            history.Action = "Add";
            history.LeadActionId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            List.Add(history);

            history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "Suburb";
            history.PrevValue = "";
            history.CurValue = input.Suburb;
            history.Action = "Add";
            history.LeadActionId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            List.Add(history);

            history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "State";
            history.PrevValue = "";
            history.CurValue = input.State;
            history.Action = "Add";
            history.LeadActionId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            List.Add(history);

            history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "PostCode";
            history.PrevValue = "";
            history.CurValue = input.PostCode;
            history.Action = "Add";
            history.LeadActionId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            List.Add(history);

            history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "Requirements";
            history.PrevValue = "";
            history.CurValue = input.Requirements;
            history.Action = "Add";
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = lead.Id;
            history.LeadActionId = leadactivityId;
            List.Add(history);
            await _dbcontextprovider.GetDbContext().LeadtrackerHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
            //await _leadactivityRepository.InsertAsync(leadactivity);

        }

        /// <summary>
        /// Update Lead
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        //[AbpAuthorize(AppPermissions.Pages_Leads_Edit, AppPermissions.Pages_MyLeads)]
        protected virtual async Task Update(CreateOrEditLeadDto input)
        {
            input.LeadSourceId = _lookup_leadSourceRepository.GetAll().Where(e => e.Name == input.LeadSource).Select(e => e.Id).FirstOrDefault();

            if (input.StateId == null)
            {
                input.StateId = _lookup_stateRepository.GetAll().Where(e => e.Name == input.State).Select(e => e.Id).FirstOrDefault();
            }
            if (input.SuburbId == null)
            {
                input.StateId = _lookup_postCodeRepository.GetAll().Where(e => e.PostalCode == input.PostCode).Select(e => e.Id).FirstOrDefault();
            }
            var lead = await _leadRepository.FirstOrDefaultAsync((int)input.Id);

            if (lead.LeadStatusId != input.LeadStatusID)
            {
                PreviousJobStatus jobStstus = new PreviousJobStatus();
                jobStstus.LeadId = lead.Id;
                if (AbpSession.TenantId != null)
                {
                    jobStstus.TenantId = (int)AbpSession.TenantId;
                }
                jobStstus.CurrentID = (int)input.LeadStatusID;
                jobStstus.PreviousId = lead.LeadStatusId;
                await _previousJobStatusRepository.InsertAsync(jobStstus);
                lead.ChangeStatusDate = DateTime.UtcNow;
            }
            input.Address = input.UnitNo + " " + input.UnitType + " " + input.StreetNo + " " + input.StreetName + " " + input.StreetType;
            if (!string.IsNullOrWhiteSpace(input.Suburb))
            {
                string[] splitPipeValue = input.Suburb.Split('|');
                if (splitPipeValue.Length > 0)
                {
                    lead.Suburb = splitPipeValue[0].ToString();
                }
                else
                {
                    lead.Suburb = input.Suburb;
                }
            }

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 2;
            leadactivity.SectionId = input.SectionId;
            leadactivity.ActionNote = "Lead Modified";
            leadactivity.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);
            #region edithistory

            var List = new List<LeadtrackerHistory>();
            if (lead.CompanyName != input.CompanyName)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "CompanyName";
                leadStatus.PrevValue = lead.CompanyName;
                leadStatus.CurValue = input.CompanyName;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Email != input.Email)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Email";
                leadStatus.PrevValue = lead.Email;
                leadStatus.CurValue = input.Email;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Mobile != input.Mobile)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Mobile";
                leadStatus.PrevValue = lead.Mobile;
                leadStatus.CurValue = input.Mobile;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Phone != input.Phone)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Phone";
                leadStatus.PrevValue = lead.Phone;
                leadStatus.CurValue = input.Phone;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.AltPhone != input.AltPhone)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "AltPhone";
                leadStatus.PrevValue = Convert.ToString(lead.AltPhone);
                leadStatus.CurValue = Convert.ToString(input.AltPhone);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.LeadStatusId != input.LeadStatusID)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "LeadStatusId";
                leadStatus.PrevValue = _lookup_leadStatusRepository.GetAll().Where(e => e.Id == lead.LeadStatusId).Select(e => e.Status).FirstOrDefault();
                leadStatus.CurValue = _lookup_leadStatusRepository.GetAll().Where(e => e.Id == input.LeadStatusID).Select(e => e.Status).FirstOrDefault();
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.LeadSource != input.LeadSource)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "LeadSource";
                leadStatus.PrevValue = lead.LeadSource;
                leadStatus.CurValue = input.LeadSource;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Type != input.Type)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Type";
                leadStatus.PrevValue = Convert.ToString(lead.Type);
                leadStatus.CurValue = Convert.ToString(input.Type);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Area != input.Area)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Area";
                leadStatus.PrevValue = Convert.ToString(lead.Area);
                leadStatus.CurValue = Convert.ToString(input.Area);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.ReferralName != input.ReferralName)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "ReferralName";
                leadStatus.PrevValue = Convert.ToString(lead.ReferralName);
                leadStatus.CurValue = Convert.ToString(input.ReferralName);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
            }
            if (lead.ExcelAddress != input.ExcelAddress)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "ExcelAddress";
                leadStatus.PrevValue = lead.ExcelAddress;
                leadStatus.CurValue = input.ExcelAddress;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            }
            if (lead.IsGoogle != input.IsGoogle)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "IsGoogle";
                leadStatus.PrevValue = Convert.ToString(lead.IsGoogle);
                leadStatus.CurValue = Convert.ToString(input.IsGoogle);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }

            if (lead.Address != input.Address)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Address";
                leadStatus.PrevValue = lead.Address;
                leadStatus.CurValue = input.Address;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            }
            if (lead.UnitNo != input.UnitNo)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "UnitNo";
                leadStatus.PrevValue = lead.UnitNo;
                leadStatus.CurValue = input.UnitNo;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.UnitType != input.UnitType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "UnitType";
                leadStatus.PrevValue = lead.UnitType;
                leadStatus.CurValue = input.UnitType;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.StreetNo != input.StreetNo)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "StreetNo";
                leadStatus.PrevValue = lead.StreetNo;
                leadStatus.CurValue = input.StreetNo;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.StreetName != input.StreetName)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "StreetName";
                leadStatus.PrevValue = lead.StreetName;
                leadStatus.CurValue = input.StreetName;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.StreetType != input.StreetType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "StreetType";
                leadStatus.PrevValue = lead.StreetType;
                leadStatus.CurValue = input.StreetType;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            }
            if (lead.Suburb != input.Suburb)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Suburb";
                leadStatus.PrevValue = lead.Suburb;
                leadStatus.CurValue = input.Suburb;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            }
            if (lead.State != input.State)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }

                leadStatus.FieldName = "State";
                leadStatus.PrevValue = lead.State;
                leadStatus.CurValue = input.State;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.PostCode != input.PostCode)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "PostCode";
                leadStatus.PrevValue = lead.PostCode;
                leadStatus.CurValue = input.PostCode;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.latitude != input.latitude)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "latitude";
                leadStatus.PrevValue = Convert.ToString(lead.latitude);
                leadStatus.CurValue = Convert.ToString(input.latitude);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.longitude != input.longitude)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "longitude";
                leadStatus.PrevValue = Convert.ToString(lead.longitude);
                leadStatus.CurValue = Convert.ToString(input.longitude);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }

            if (lead.Requirements != input.Requirements)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Requirements";
                leadStatus.PrevValue = lead.Requirements;
                leadStatus.CurValue = input.Requirements;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.AssignToUserID != input.AssignToUserID)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "AssignToUserID";
                leadStatus.PrevValue = Convert.ToString(from ld in _userRepository.GetAll() where ld.Id == lead.AssignToUserID select ld.Name);
                leadStatus.CurValue = Convert.ToString(from ld in _userRepository.GetAll() where ld.Id == input.AssignToUserID select ld.Name);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            }


            //////////



            //if (lead.IsDuplicate != input.IsDuplicate)
            //{
            //	LeadtrackerHistory leadStatus = new LeadtrackerHistory();
            //	if (AbpSession.TenantId != null)
            //	{
            //		leadStatus.TenantId = (int)AbpSession.TenantId;
            //	}
            //	leadStatus.FieldName = "IsDuplicate";
            //	leadStatus.PrevValue = Convert.ToString(lead.IsDuplicate);
            //	leadStatus.CurValue = Convert.ToString(input.IsDuplicate);
            //	leadStatus.Action = "Edit";
            //	leadStatus.LastmodifiedDateTime = DateTime.Now;
            //	leadStatus.LeadId = lead.Id;
            //	leadStatus.LeadActionId = leadactid;
            //	List.Add(leadStatus);
            //	//await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            //}

           

            if (lead.ABN != input.ABN)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "ABN";
                leadStatus.PrevValue = Convert.ToString(lead.ABN);
                leadStatus.CurValue = Convert.ToString(input.ABN);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Fax != input.Fax)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Fax";
                leadStatus.PrevValue = Convert.ToString(lead.Fax);
                leadStatus.CurValue = Convert.ToString(input.Fax);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }

            if (lead.SystemType != input.SystemType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "SystemType";
                leadStatus.PrevValue = Convert.ToString(lead.SystemType);
                leadStatus.CurValue = Convert.ToString(input.SystemType);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now; leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.RoofType != input.RoofType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "RoofType";
                leadStatus.PrevValue = Convert.ToString(lead.RoofType);
                leadStatus.CurValue = Convert.ToString(input.RoofType);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now; leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }

            if (lead.AngleType != input.AngleType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "AngleType";
                leadStatus.PrevValue = Convert.ToString(lead.AngleType);
                leadStatus.CurValue = Convert.ToString(input.AngleType);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now; leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.StoryType != input.StoryType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "StoryType";
                leadStatus.PrevValue = Convert.ToString(lead.StoryType);
                leadStatus.CurValue = Convert.ToString(input.StoryType);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now; leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }

            if (lead.HouseAgeType != input.HouseAgeType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "HouseAgeType";
                leadStatus.PrevValue = Convert.ToString(lead.HouseAgeType);
                leadStatus.CurValue = Convert.ToString(input.HouseAgeType);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now; leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
            }

            
            if (lead.ReferralLeadId != input.ReferralLeadId)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "ReferralLeadId";
                leadStatus.PrevValue = Convert.ToString(lead.ReferralLeadId);
                leadStatus.CurValue = Convert.ToString(input.ReferralLeadId);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
            }

            await _dbcontextprovider.GetDbContext().LeadtrackerHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            #endregion


            //Start Check Duplicate
            //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
            var dbDup = _leadRepository.GetAll().Where(e => e.Id != input.Id && e.OrganizationId == input.OrganizationId && e.AssignToUserID != null && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == input.Mobile && !string.IsNullOrEmpty(e.Mobile)))).FirstOrDefault();
            if (dbDup != null)
            {
                var dbDupLeads = await _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationId && e.AssignToUserID != null && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == input.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Select(e => e.Id).ToListAsync();

                if (input.from == null)
                {
                    var dbDupLeadCount = dbDupLeads.Count();
                    var dbDupLeadInstallCount = await _jobRepository.GetAll().CountAsync(e => e.JobStatusId == 8 && dbDupLeads.Contains((int)e.LeadId));

                    if (dbDupLeadCount != dbDupLeadInstallCount)
                    {
                        lead.IsDuplicate = true;
                        lead.DublicateLeadId = dbDup.Id;
                    }
                    else
                    {
                        lead.IsDuplicate = false;
                    }

                }
                else if (input.from == "myleads" || input.from == "mylead")
                {
                    var dbDupLeadOther = await _leadRepository.GetAll().Where(e => e.AssignToUserID != AbpSession.UserId && dbDupLeads.Contains(e.Id)).Select(e => e.Id).ToListAsync();

                    if (dbDupLeadOther.Count > 0)
                    {
                        var dbDupLeadInstallCount = await _jobRepository.GetAll().CountAsync(e => e.JobStatusId == 8 && dbDupLeadOther.Contains((int)e.LeadId));

                        if (dbDupLeadOther.Count != dbDupLeadInstallCount)
                        {
                            lead.IsDuplicate = true;
                            lead.DublicateLeadId = dbDup.Id;
                        }
                        else
                        {
                            lead.IsDuplicate = false;
                        }
                    }
                    else
                    {
                        lead.IsDuplicate = false;
                    }
                }
                else
                {
                    lead.IsDuplicate = true;
                    lead.DublicateLeadId = dbDup.Id;
                }
            }
            else
            {
                lead.IsDuplicate = false;
            }

            //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
            var webDup = _leadRepository.GetAll().Where(e => e.Id != input.Id && e.OrganizationId == input.OrganizationId && e.AssignToUserID == null && e.HideDublicate != true && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == input.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Any();
            if (webDup)
            {
                lead.IsWebDuplicate = true;
            }
            else
            {
                lead.IsWebDuplicate = false;
            }
            //End Check Duplicate

            ObjectMapper.Map(input, lead);

            if (input.IsJobAddresschange)
            {
                var job = await _jobRepository.GetAll().Where(e => e.LeadId == lead.Id).FirstOrDefaultAsync();
                job.State = lead.State;
                job.Address = lead.Address;
                job.Suburb = lead.Suburb;
                job.UnitNo = lead.UnitNo;
                job.UnitType = lead.UnitType;
                job.StreetNo = lead.StreetNo;
                job.StreetType = lead.StreetType;
                job.StreetName = lead.StreetName;
                job.PostalCode = lead.PostCode;

                await _jobRepository.UpdateAsync(job);
            }

        }

        public async Task CreateOrEditLead(CreateOrEditForAppLeadDto input)
        {
            if (input.Id == null)
            {
                await CreateLead(input);
            }
            else
            {
                await UpdateLead(input);
            }
        }

        /// <summary>
        /// Create Lead
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Leads_Create, AppPermissions.Pages_MyLeads)]
        protected virtual async Task CreateLead(CreateOrEditForAppLeadDto input)
        {
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

            IList<string> role = await _userManager.GetRolesAsync(assignedToUser);

            input.LeadSourceId = _lookup_leadSourceRepository.GetAll().Where(e => e.Name == input.LeadSource).Select(e => e.Id).FirstOrDefault();
            if (input.StateId == null || input.StateId == 0)
            {
                input.StateId = _lookup_stateRepository.GetAll().Where(e => e.Name == input.State).Select(e => e.Id).FirstOrDefault();
            }
            if (input.SuburbId == null || input.SuburbId == 0)
            {
                input.StateId = _lookup_postCodeRepository.GetAll().Where(e => e.PostalCode == input.PostCode).Select(e => e.Id).FirstOrDefault();
            }
            //if (input.PostalStateId == null)
            //{
            //	input.StateId = _lookup_stateRepository.GetAll().Where(e => e.Name == input.PostalState).Select(e => e.Id).FirstOrDefault();
            //}
            //if (input.PostalSuburbId == null)
            //{
            //	input.StateId = _lookup_postCodeRepository.GetAll().Where(e => e.PostalCode == input.PostalPostCode).Select(e => e.Id).FirstOrDefault();
            //}
            var lead = ObjectMapper.Map<Lead>(input);
            if (input.IsExternalLead == 3)
            {
                lead.Address = input.Address;
            }
            else
            {
                lead.Address = input.UnitNo + " " + input.UnitType + " " + input.StreetNo + " " + input.StreetName + " " + input.StreetType;
            }
            //lead.PostalAddress = lead.PostalUnitNo + " " + lead.PostalUnitType + " " + lead.PostalStreetNo + " " + lead.PostalStreetName + " " + lead.PostalStreetType;
            lead.IsPromotion = true;
            if (!string.IsNullOrWhiteSpace(input.Suburb))
            {
                string[] splitPipeValue = input.Suburb.Split('|');
                if (splitPipeValue.Length > 0)
                {
                    lead.Suburb = splitPipeValue[0].ToString();
                }
                else
                {
                    lead.Suburb = input.Suburb;
                }
            }

            //1. Create
            //2. Excel Import
            //3. External Link
            //4. Copy Lead
            lead.IsExternalLead = input.IsExternalLead;
            lead.LeadAssignDate = DateTime.UtcNow;

            if (input.from == "mylead")
            {
                lead.AssignToUserID = (int)AbpSession.UserId;
                lead.LeadStatusId = 2;
            }
            else if (input.from == "myleads")
            {
                lead.AssignToUserID = (int)AbpSession.UserId;
                lead.LeadStatusId = 2;
            }
            else
            {
                lead.LeadStatusId = 1;
            }

            //Start Check Duplicate
            //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
            var dbDup = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationId && e.AssignToUserID != null && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == input.Mobile && !string.IsNullOrEmpty(e.Mobile)))).FirstOrDefault();
            if (dbDup != null)
            {
                var dbDupLeads = await _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationId && e.AssignToUserID != null && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == input.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Select(e => e.Id).ToListAsync();

                if (input.from == null)
                {
                    var dbDupLeadCount = dbDupLeads.Count();
                    var dbDupLeadInstallCount = await _jobRepository.GetAll().CountAsync(e => e.JobStatusId == 8 && dbDupLeads.Contains((int)e.LeadId));

                    if (dbDupLeadCount != dbDupLeadInstallCount)
                    {
                        lead.IsDuplicate = true;
                        lead.DublicateLeadId = dbDup.Id;
                    }
                    else
                    {
                        lead.IsDuplicate = false;
                    }

                }
                else if (input.from == "myleads" || input.from == "mylead")
                {
                    var dbDupLeadOther = await _leadRepository.GetAll().Where(e => e.AssignToUserID != AbpSession.UserId && dbDupLeads.Contains(e.Id)).Select(e => e.Id).ToListAsync();

                    if (dbDupLeadOther.Count > 0)
                    {
                        var dbDupLeadInstallCount = await _jobRepository.GetAll().CountAsync(e => e.JobStatusId == 8 && dbDupLeadOther.Contains((int)e.LeadId));

                        if (dbDupLeadOther.Count != dbDupLeadInstallCount)
                        {
                            lead.IsDuplicate = true;
                            lead.DublicateLeadId = dbDup.Id;
                        }
                        else
                        {
                            lead.IsDuplicate = false;
                        }
                    }
                    else
                    {
                        lead.IsDuplicate = false;
                    }
                }
                else
                {
                    lead.IsDuplicate = true;
                    lead.DublicateLeadId = dbDup.Id;
                }
            }
            else
            {
                lead.IsDuplicate = false;
            }

            //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
            var webDup = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationId && e.AssignToUserID == null && e.HideDublicate != true && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == input.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Any();
            if (webDup)
            {
                lead.IsWebDuplicate = true;
            }
            else
            {
                lead.IsWebDuplicate = false;
            }
            //End Check Duplicate

            await _leadRepository.InsertAndGetIdAsync(lead);

            PreviousJobStatus jobStstus = new PreviousJobStatus();
            jobStstus.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                jobStstus.TenantId = (int)AbpSession.TenantId;
            }
            jobStstus.CurrentID = lead.LeadStatusId;
            jobStstus.PreviousId = 0;
            await _previousJobStatusRepository.InsertAsync(jobStstus);

            //1. Create
            //2. Excel Import
            //3. External Link
            //4. Copy Lead
            if (lead.IsExternalLead == 3)
            {
                var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == lead.CreatorUserId).FirstOrDefault();
                string msg = string.Format("New Lead Created {0}.", lead.CompanyName);
                await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
            }

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == lead.CreatorUserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 1;
            leadactivity.SectionId = 0;
            leadactivity.ActionNote = "Lead Created";
            leadactivity.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);

        }

        /// <summary>
        /// Update Lead
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Leads_Edit, AppPermissions.Pages_MyLeads)]
        protected virtual async Task UpdateLead(CreateOrEditForAppLeadDto input)
        {
            if (input.StateId == null)
            {
                input.StateId = _lookup_stateRepository.GetAll().Where(e => e.Name == input.State).Select(e => e.Id).FirstOrDefault();
            }
            if (input.SuburbId == null)
            {
                input.StateId = _lookup_postCodeRepository.GetAll().Where(e => e.PostalCode == input.PostCode).Select(e => e.Id).FirstOrDefault();
            }
            var lead = await _leadRepository.FirstOrDefaultAsync((int)input.Id);

            if (lead.LeadStatusId != input.LeadStatusID)
            {
                PreviousJobStatus jobStstus = new PreviousJobStatus();
                jobStstus.LeadId = lead.Id;
                if (AbpSession.TenantId != null)
                {
                    jobStstus.TenantId = (int)AbpSession.TenantId;
                }
                jobStstus.CurrentID = (int)input.LeadStatusID;
                jobStstus.PreviousId = lead.LeadStatusId;
                await _previousJobStatusRepository.InsertAsync(jobStstus);
                lead.ChangeStatusDate = DateTime.UtcNow;
            }
            input.Address = input.UnitNo + " " + input.UnitType + " " + input.StreetNo + " " + input.StreetName + " " + input.StreetType;
            if (!string.IsNullOrWhiteSpace(input.Suburb))
            {
                string[] splitPipeValue = input.Suburb.Split('|');
                if (splitPipeValue.Length > 0)
                {
                    lead.Suburb = splitPipeValue[0].ToString();
                }
                else
                {
                    lead.Suburb = input.Suburb;
                }
            }

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 2;
            leadactivity.SectionId = 0;
            leadactivity.ActionNote = "Lead Modified";
            leadactivity.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);
            #region edithistory

            var List = new List<LeadtrackerHistory>();
            if (lead.CompanyName != input.CompanyName)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "CompanyName";
                leadStatus.PrevValue = lead.CompanyName;
                leadStatus.CurValue = input.CompanyName;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Email != input.Email)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Email";
                leadStatus.PrevValue = lead.Email;
                leadStatus.CurValue = input.Email;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Phone != input.Phone)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Phone";
                leadStatus.PrevValue = lead.Phone;
                leadStatus.CurValue = input.Phone;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Mobile != input.Mobile)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Mobile";
                leadStatus.PrevValue = lead.Mobile;
                leadStatus.CurValue = input.Mobile;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Address != input.Address)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Address";
                leadStatus.PrevValue = lead.Address;
                leadStatus.CurValue = input.Address;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            }
            if (lead.ExcelAddress != input.ExcelAddress)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "ExcelAddress";
                leadStatus.PrevValue = lead.ExcelAddress;
                leadStatus.CurValue = input.ExcelAddress;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            }
            if (lead.Requirements != input.Requirements)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Requirements";
                leadStatus.PrevValue = lead.Requirements;
                leadStatus.CurValue = input.Requirements;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Suburb != input.Suburb)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Suburb";
                leadStatus.PrevValue = lead.Suburb;
                leadStatus.CurValue = input.Suburb;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            }
            if (lead.State != input.State)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }

                leadStatus.FieldName = "State";
                leadStatus.PrevValue = lead.State;
                leadStatus.CurValue = input.State;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.PostCode != input.PostCode)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "PostCode";
                leadStatus.PrevValue = lead.PostCode;
                leadStatus.CurValue = input.PostCode;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.LeadSource != input.LeadSource)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "LeadSource";
                leadStatus.PrevValue = lead.LeadSource;
                leadStatus.CurValue = input.LeadSource;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.UnitNo != input.UnitNo)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "UnitNo";
                leadStatus.PrevValue = lead.UnitNo;
                leadStatus.CurValue = input.UnitNo;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.UnitType != input.UnitType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "UnitType";
                leadStatus.PrevValue = lead.UnitType;
                leadStatus.CurValue = input.UnitType;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.StreetNo != input.StreetNo)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "StreetNo";
                leadStatus.PrevValue = lead.StreetNo;
                leadStatus.CurValue = input.StreetNo;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.StreetName != input.StreetName)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "StreetName";
                leadStatus.PrevValue = lead.StreetName;
                leadStatus.CurValue = input.StreetName;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.StreetType != input.StreetType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "StreetType";
                leadStatus.PrevValue = lead.StreetType;
                leadStatus.CurValue = input.StreetType;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            }
            //if (lead.LeadSourceId != input.LeadSourceId)
            //{
            //    LeadtrackerHistory leadStatus = new LeadtrackerHistory();
            //    if (AbpSession.TenantId != null)
            //    {
            //        leadStatus.TenantId = (int)AbpSession.TenantId;
            //    }
            //    leadStatus.FieldName = "LeadSourceId";
            //    leadStatus.PrevValue = _lookup_leadSourceRepository.GetAll().Where(e => e.Id == lead.LeadSourceId).Select(e => e.Name).FirstOrDefault();
            //    leadStatus.CurValue = _lookup_leadSourceRepository.GetAll().Where(e => e.Id == input.LeadSourceId).Select(e => e.Name).FirstOrDefault();
            //    leadStatus.Action = "Edit";
            //    leadStatus.LastmodifiedDateTime = DateTime.Now;
            //    leadStatus.LeadId = lead.Id;
            //    leadStatus.LeadActionId = leadactid;
            //    List.Add(leadStatus);
            //    //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            //}


            if (lead.LeadStatusId != input.LeadStatusID)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "LeadStatus";
                leadStatus.PrevValue = _lookup_leadStatusRepository.GetAll().Where(e => e.Id == lead.LeadStatusId).Select(e => e.Status).FirstOrDefault();
                leadStatus.CurValue = _lookup_leadStatusRepository.GetAll().Where(e => e.Id == input.LeadStatusID).Select(e => e.Status).FirstOrDefault();
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.AssignToUserID != input.AssignToUserID)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "AssignToUserID";
                leadStatus.PrevValue = Convert.ToString(from ld in _userRepository.GetAll() where ld.Id == lead.AssignToUserID select ld.Name);
                leadStatus.CurValue = Convert.ToString(from ld in _userRepository.GetAll() where ld.Id == input.AssignToUserID select ld.Name);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            }
            if (lead.latitude != input.latitude)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "latitude";
                leadStatus.PrevValue = Convert.ToString(lead.latitude);
                leadStatus.CurValue = Convert.ToString(input.latitude);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.longitude != input.longitude)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "longitude";
                leadStatus.PrevValue = Convert.ToString(lead.longitude);
                leadStatus.CurValue = Convert.ToString(input.longitude);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.IsGoogle != input.IsGoogle)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "IsGoogle";
                leadStatus.PrevValue = Convert.ToString(lead.IsGoogle);
                leadStatus.CurValue = Convert.ToString(input.IsGoogle);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            //if (lead.IsDuplicate != input.IsDuplicate)
            //{
            //	LeadtrackerHistory leadStatus = new LeadtrackerHistory();
            //	if (AbpSession.TenantId != null)
            //	{
            //		leadStatus.TenantId = (int)AbpSession.TenantId;
            //	}
            //	leadStatus.FieldName = "IsDuplicate";
            //	leadStatus.PrevValue = Convert.ToString(lead.IsDuplicate);
            //	leadStatus.CurValue = Convert.ToString(input.IsDuplicate);
            //	leadStatus.Action = "Edit";
            //	leadStatus.LastmodifiedDateTime = DateTime.Now;
            //	leadStatus.LeadId = lead.Id;
            //	leadStatus.LeadActionId = leadactid;
            //	List.Add(leadStatus);
            //	//await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);
            //}

            if (lead.AltPhone != input.AltPhone)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "AltPhone";
                leadStatus.PrevValue = Convert.ToString(lead.AltPhone);
                leadStatus.CurValue = Convert.ToString(input.AltPhone);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }


            if (lead.Type != input.Type)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Type";
                leadStatus.PrevValue = Convert.ToString(lead.Type);
                leadStatus.CurValue = Convert.ToString(input.Type);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Area != input.Area)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Area";
                leadStatus.PrevValue = Convert.ToString(lead.Area);
                leadStatus.CurValue = Convert.ToString(input.Area);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.ABN != input.ABN)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "ABN";
                leadStatus.PrevValue = Convert.ToString(lead.ABN);
                leadStatus.CurValue = Convert.ToString(input.ABN);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.Fax != input.Fax)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Fax";
                leadStatus.PrevValue = Convert.ToString(lead.Fax);
                leadStatus.CurValue = Convert.ToString(input.Fax);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }

            if (lead.SystemType != input.SystemType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "SystemType";
                leadStatus.PrevValue = Convert.ToString(lead.SystemType);
                leadStatus.CurValue = Convert.ToString(input.SystemType);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now; leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.RoofType != input.RoofType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "RoofType";
                leadStatus.PrevValue = Convert.ToString(lead.RoofType);
                leadStatus.CurValue = Convert.ToString(input.RoofType);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now; leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }

            if (lead.AngleType != input.AngleType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "AngleType";
                leadStatus.PrevValue = Convert.ToString(lead.AngleType);
                leadStatus.CurValue = Convert.ToString(input.AngleType);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now; leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }
            if (lead.StoryType != input.StoryType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "StoryType";
                leadStatus.PrevValue = Convert.ToString(lead.StoryType);
                leadStatus.CurValue = Convert.ToString(input.StoryType);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now; leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);
                //await _LeadtrackerHistoryRepository.InsertAsync(leadStatus);

            }

            if (lead.HouseAgeType != input.HouseAgeType)
            {
                LeadtrackerHistory leadStatus = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "HouseAgeType";
                leadStatus.PrevValue = Convert.ToString(lead.HouseAgeType);
                leadStatus.CurValue = Convert.ToString(input.HouseAgeType);
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now; leadStatus.LeadId = lead.Id;
                leadStatus.LeadActionId = leadactid;
                List.Add(leadStatus);


            }



            await _dbcontextprovider.GetDbContext().LeadtrackerHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            #endregion

            //Start Check Duplicate
            //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
            var dbDup = _leadRepository.GetAll().Where(e => e.Id != input.Id && e.OrganizationId == input.OrganizationId && e.AssignToUserID != null && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == input.Mobile && !string.IsNullOrEmpty(e.Mobile)))).FirstOrDefault();
            if (dbDup != null)
            {
                var dbDupLeads = await _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationId && e.AssignToUserID != null && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == input.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Select(e => e.Id).ToListAsync();

                if (input.from == null)
                {
                    var dbDupLeadCount = dbDupLeads.Count();
                    var dbDupLeadInstallCount = await _jobRepository.GetAll().CountAsync(e => e.JobStatusId == 8 && dbDupLeads.Contains((int)e.LeadId));

                    if (dbDupLeadCount != dbDupLeadInstallCount)
                    {
                        lead.IsDuplicate = true;
                        lead.DublicateLeadId = dbDup.Id;
                    }
                    else
                    {
                        lead.IsDuplicate = false;
                    }

                }
                else if (input.from == "myleads" || input.from == "mylead")
                {
                    var dbDupLeadOther = await _leadRepository.GetAll().Where(e => e.AssignToUserID != AbpSession.UserId && dbDupLeads.Contains(e.Id)).Select(e => e.Id).ToListAsync();

                    if (dbDupLeadOther.Count > 0)
                    {
                        var dbDupLeadInstallCount = await _jobRepository.GetAll().CountAsync(e => e.JobStatusId == 8 && dbDupLeadOther.Contains((int)e.LeadId));

                        if (dbDupLeadOther.Count != dbDupLeadInstallCount)
                        {
                            lead.IsDuplicate = true;
                            lead.DublicateLeadId = dbDup.Id;
                        }
                        else
                        {
                            lead.IsDuplicate = false;
                        }
                    }
                    else
                    {
                        lead.IsDuplicate = false;
                    }
                }
                else
                {
                    lead.IsDuplicate = true;
                    lead.DublicateLeadId = dbDup.Id;
                }
            }
            else
            {
                lead.IsDuplicate = false;
            }

            //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
            var webDup = _leadRepository.GetAll().Where(e => e.Id != input.Id && e.OrganizationId == input.OrganizationId && e.AssignToUserID == null && e.HideDublicate != true && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == input.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Any();
            if (webDup)
            {
                lead.IsWebDuplicate = true;
            }
            else
            {
                lead.IsWebDuplicate = false;
            }
            //End Check Duplicate

            ObjectMapper.Map(input, lead);

        }

        /// <summary>
        /// Lead Status Change
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        //  [AbpAuthorize(AppPermissions.Pages_Leads_Edit, AppPermissions.Pages_MyLeads)]
        public virtual async Task ChangeStatus(GetLeadForChangeStatusOutput input)
        {
            var lead = await _leadRepository.FirstOrDefaultAsync((int)input.Id);

            PreviousJobStatus jobStstus = new PreviousJobStatus();
            jobStstus.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                jobStstus.TenantId = (int)AbpSession.TenantId;
            }
            jobStstus.CurrentID = (int)input.LeadStatusID;
            jobStstus.PreviousId = lead.LeadStatusId;
            await _previousJobStatusRepository.InsertAsync(jobStstus);

            lead.CancelReasonId = input.CancelReasonId;
            lead.RejectReasonId = input.RejectReasonId;
            lead.LeadStatusId = (int)input.LeadStatusID;
            lead.ChangeStatusDate = DateTime.UtcNow;
            if (!input.RejectReason.IsNullOrEmpty())
            {
                lead.RejectReason = input.RejectReason;
            }
            await _leadRepository.UpdateAsync(lead);

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 5;
            leadactivity.SectionId = 0;
            if (input.LeadStatusID == 7)
            {
                leadactivity.ActionNote = "Lead Rejected, Reason : " + input.RejectReason;
            }
            else
            {
                leadactivity.ActionNote = "Lead Status Changed";
            }

            leadactivity.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);

            var log = new UserActivityLogDto();
            log.ActionId = 5;
            log.ActionNote = input.LeadStatusID == 7 ? "Lead Rejected, Reason : " + input.RejectReason : "Lead Status Changed";
            log.Section = input.Section;
            await _userActivityLogServiceProxy.Create(log);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Leads_Edit, AppPermissions.Pages_MyLeads)]
        public async Task<GetLeadForChangeStatusOutput> GetLeadForChangeStatus(EntityDto input)
        {
            var lead = await _leadRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLeadForChangeStatusOutput();
            output.Id = lead.Id;
            output.LeadStatusID = lead.LeadStatusId;
            output.CompanyName = lead.CompanyName;

            if (output.LeadStatusID != null)
            {
                var _lookupLeadStatus = await _lookup_leadStatusRepository.FirstOrDefaultAsync((int)output.LeadStatusID);
                output.StatusName = _lookupLeadStatus?.Status?.ToString();
            }

            return output;
        }

        /// <summary>
        /// Delete Multiple Leads
        /// </summary>
        /// <param name="input">Lead Id[]</param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Leads_Delete, AppPermissions.Pages_MyLeads)]
        public async Task Delete(int[] input)
        {
            foreach (var item in input)
            {
                try
                {
                    var JobLeadId = _jobRepository.GetAll().Where(e => e.LeadId == item).Select(e => e.LeadId).FirstOrDefault();

                    if (JobLeadId > 0)
                    {
                        await _jobProductItemRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);
                        await _jobPromotionRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);
                        await _jobVariationRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);

                        await _jobRepository.DeleteAsync((int)JobLeadId);

                        //LeadActivityLog leadactivity = new LeadActivityLog();
                        //leadactivity.ActionId = 1;
                        //leadactivity.SectionId = sectionId;
                        //leadactivity.ActionNote = "Lead Created";
                        //leadactivity.LeadId = lead.Id;
                        //if (AbpSession.TenantId != null)
                        //{
                        //    leadactivity.TenantId = (int)AbpSession.TenantId;
                        //}
                        //await _leadactivityRepository.InsertAsync(leadactivity);
                    }
                }
                catch (Exception e)
                {

                }

                await _leadRepository.DeleteAsync(item);
            }
        }

        /// <summary>
        /// Delete Lead
        /// </summary>
        /// <param name="input">Lead Id</param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Leads_Delete, AppPermissions.Pages_MyLeads)]
        public async Task DeleteLeads(EntityDto input)
        {
            try
            {
                var JobLeadId = _jobRepository.GetAll().Where(e => e.LeadId == input.Id).Select(e => e.LeadId).FirstOrDefault();

                if (JobLeadId > 0)
                {
                    await _jobProductItemRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);
                    await _jobPromotionRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);
                    await _jobVariationRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);

                    await _jobRepository.DeleteAsync((int)JobLeadId);
                }
            }
            catch (Exception e)
            {

            }

            await _leadRepository.DeleteAsync(input.Id);
        }

        /// <summary>
        /// Delete Duplicate Lead
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Leads_Delete, AppPermissions.Pages_MyLeads)]
        public async Task DeleteDuplicateLeads(EntityDto input)
        {
            var lead = await _leadRepository.GetAsync(input.Id);

            var duplicateLead = (_leadRepository.GetAll().Where(e => ((e.Email == lead.Email || e.Mobile == lead.Mobile) && (!string.IsNullOrEmpty(e.Mobile) && !string.IsNullOrEmpty(e.Email))) && e.Id != lead.Id && e.AssignToUserID != null && e.OrganizationId == lead.OrganizationId));
            //_leadRepository.GetAll().Where(e => (e.Phone == lead.Phone || e.Mobile == lead.Mobile || e.Email == lead.Email) && e.Id != lead.Id && e.AssignToUserID != null);


            foreach (var item in duplicateLead)
            {
                try
                {
                    var JobLeadId = _jobRepository.GetAll().Where(e => e.LeadId == item.Id).Select(e => e.LeadId).FirstOrDefault();

                    if (JobLeadId > 0)
                    {
                        await _jobProductItemRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);
                        await _jobPromotionRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);
                        await _jobVariationRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);

                        await _jobRepository.DeleteAsync((int)JobLeadId);
                    }
                }
                catch (Exception e)
                {

                }

                await _leadRepository.DeleteAsync(item.Id);
            }
        }

        /// <summary>
        /// Lead Excel Export
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<FileDto> GetLeadsToExcel(GetAllLeadsForExcelInput input)
        {

            //var filteredLeads = _leadRepository.GetAll()
            //            //.Include(e => e.SuburbFk)
            //            //.Include(e => e.StateFk)
            //            .Include(e => e.LeadStatusFk)
            //            //.Include(e => e.LeadSourceFk)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
            //            //.WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.SuburbFk != null && e.SuburbFk.Suburb == input.PostCodeSuburbFilter)
            //            //.WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.StateFk != null && e.StateFk.Name == input.StateNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.StreetNameFilter), e => e.StreetName == input.StreetNameFilter);
            ////.WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSourceFk != null && e.LeadSourceFk.Name == input.LeadSourceNameFilter);

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var jobnumberlist = new List<int?>();
            jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber == input.Filter).Select(e => e.LeadId).ToList();

            var filteredLeads = _leadRepository.GetAll()
                        .Include(e => e.LeadStatusFk)
                         //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                         .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                         .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                         .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                         .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                         .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                         .WhereIf(input.FilterName == "Requirements" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Requirements == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StreetNameFilter), e => e.StreetName == input.StreetNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadStatusId != null, e => e.LeadStatusId == input.LeadStatusId)
                        .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                        .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                        //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                        .WhereIf(input.DuplicateFilter == "web", o => (_leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.AssignToUserID == null && e.OrganizationId == input.OrganizationUnit && e.IsDuplicate != true && e.IsWebDuplicate != true).Count() > 0))
                        //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
                        .WhereIf(input.DuplicateFilter == "db", o => (_leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.OrganizationId == input.OrganizationUnit && e.AssignToUserID != null).Count() > 0))
                        //Not Duplicate Leads
                        .WhereIf(input.DuplicateFilter == "no", o => !(_leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.AssignToUserID == null && e.OrganizationId == input.OrganizationUnit && e.IsDuplicate != true && e.IsWebDuplicate != true).Count() > 0) && !(_leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.OrganizationId == input.OrganizationUnit && e.AssignToUserID != null).Count() > 0))
                        .Where(e => e.AssignToUserID == null)
                        .Where(e => e.IsDuplicate != true && e.IsWebDuplicate != true);

            var query = (from o in filteredLeads
                             //join o1 in _lookup_postCodeRepository.GetAll() on o.Suburb equals o1.Id into j1
                             //from s1 in j1.DefaultIfEmpty()

                             //join o2 in _lookup_stateRepository.GetAll() on o.State equals o2.Id into j2
                             //from s2 in j2.DefaultIfEmpty()

                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                             //join o4 in _lookup_leadSourceRepository.GetAll() on o.LeadSource equals o4.Id into j4
                             //from s4 in j4.DefaultIfEmpty()

                         select new GetLeadForViewDto()
                         {
                             Lead = new LeadDto
                             {
                                 CompanyName = o.CompanyName,
                                 Email = o.Email,
                                 Phone = o.Phone,
                                 Mobile = o.Mobile,
                                 Address = o.Address,
                                 Requirements = o.Requirements,
                                 Id = o.Id,
                                 PostCode = o.PostCode
                             },
                             //PostCodeSuburb = s1 == null || s1.Suburb == null ? "" : s1.Suburb.ToString(),
                             //StateName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                             LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                             //LeadSourceName = s4 == null || s4.Name == null ? "" : s4.Name.ToString(),
                         });


            var leadListDtos = await query.ToListAsync();
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _leadsExcelExporter.ExportToFile(leadListDtos);
            }
            else
            {
                return _leadsExcelExporter.ExportCsvToFile(leadListDtos);
            }
        }

        /// <summary>
        /// Reject Reason Dropdown
        /// </summary>
        /// <returns></returns>
        public async Task<List<LookupTableDto>> GetAllRejectReasonForTableDropdown()
        {
            return await _lookup_rejectReasonRepository.GetAll()
                .Select(rejectReason => new LookupTableDto
                {
                    Id = rejectReason.Id,
                    DisplayName = rejectReason == null || rejectReason.RejectReasonName == null ? "" : rejectReason.RejectReasonName.ToString()
                }).ToListAsync();
        }

        /// <summary>
        /// Cancel Reason Dropdown
        /// </summary>
        /// <returns></returns>
        public async Task<List<LookupTableDto>> GetAllCancelReasonForTableDropdown()
        {
            return await _lookup_cancelReasonRepository.GetAll()
                .Select(cancelReason => new LookupTableDto
                {
                    Id = cancelReason.Id,
                    DisplayName = cancelReason == null || cancelReason.CancelReasonName == null ? "" : cancelReason.CancelReasonName.ToString()
                }).ToListAsync();
        }

        /// <summary>
        /// Lead Status Dropdown
        /// </summary>
        /// <param name="lead"></param>
        /// <returns></returns>
        public async Task<List<LeadStatusLookupTableDto>> GetAllLeadStatusForTableDropdown(string lead)
        {
            return await _lookup_leadStatusRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(lead), c => c.Status.ToLower().Contains(lead.ToLower()))
                .Select(leadStatus => new LeadStatusLookupTableDto
                {
                    Value = leadStatus.Id,
                    Name = leadStatus == null || leadStatus.Status == null ? "" : leadStatus.Status.ToString()
                }).ToListAsync();
        }

        /// <summary>
        /// Get Value for Auto Complete
        /// </summary>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        public NameValue<string> SendAndGetSelectedValue(NameValue<string> selectedValue)
        {
            return selectedValue;
        }

        /// <summary>
        /// Get Suburb Id from Postcode
        /// </summary>
        /// <param name="PostCode">Postcode</param>
        /// <returns></returns>
        public int GetSuburbId(string PostCode)
        {
            return _lookup_postCodeRepository.GetAll().Where(P => P.PostalCode == PostCode).Select(P => P.Id).FirstOrDefault();
        }

        /// <summary>
        /// Get State Id from State Name
        /// </summary>
        /// <param name="StateName">State Name</param>
        /// <returns></returns>
        public int StateId(string StateName)
        {
            return _lookup_stateRepository.GetAll().Where(P => P.Name == StateName).Select(P => P.Id).FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<GetLeadForAssignOrTransferOutput> GetLeadForAssignOrTransfer(EntityDto input)
        {
            var lead = await _leadRepository.GetAsync(input.Id);
            var output = new GetLeadForAssignOrTransferOutput();
            output.CompanyName = lead.CompanyName;
            output.Id = lead.Id;

            if (output.AssignToUserID != null)
            {
                var _lookupUser = await _userRepository.FirstOrDefaultAsync((int)output.AssignToUserID);
                output.UserName = _lookupUser?.FullName?.ToString();
            }
            return output;
        }

        public async Task<List<LeadUsersLookupTableDto>> GetAllUserLeadAssignDropdown(int OId)
        {
            var User_List = _userRepository
           .GetAll();

            var UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Sales Manager" && uo != null && uo.OrganizationUnitId == OId) //|| us.DisplayName == "Sales Rep"
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            });

            return new List<LeadUsersLookupTableDto>(
                await UserList.ToListAsync()
            );
        }

        public async Task<List<LeadUsersLookupTableDto>> GetSalesManagerUser(int OId)
        {
            int UserId = (int)AbpSession.UserId;
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == UserId).Select(e => e.TeamId).ToList();
            var UsersList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).ToList();

            var User_List = _userRepository
           .GetAll().Where(e => e.IsActive == true);

            var RoleName = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            where (us != null && user.Id == UserId)
                            select (us.DisplayName));

            var CurrentRoleName = "Sales Manager";
            if (RoleName.Contains("Sales Manager"))
            {
                User_List.Where(e => UsersList.Contains(e.Id));
                CurrentRoleName = "Sales Rep";
            }

            var UserList = new List<LeadUsersLookupTableDto>();

            if (RoleName.Contains("Admin"))
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            where us != null && us.DisplayName == CurrentRoleName && uo != null && uo.OrganizationUnitId == OId
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).OrderBy(d => d.DisplayName).ToList();
            }
            else
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()
                            where us != null && us.DisplayName == CurrentRoleName && uo != null && uo.OrganizationUnitId == OId && TeamId.Contains(ut.TeamId)
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).OrderBy(d => d.DisplayName).ToList();
            }


            return new List<LeadUsersLookupTableDto>(
                UserList
            );
        }

        public async Task AssignOrTransferLead(GetLeadForAssignOrTransferOutput input)
        {
            if (input.AssignToUserID == null || input.AssignToUserID == 0) //Transfer Lead By Org
            {
                foreach (var leadid in input.LeadIds)
                {
                    var lead = await _leadRepository.FirstOrDefaultAsync(leadid);

                    if (input.OrganizationID != null)
                    {
                        lead.OrganizationId = (int)input.OrganizationID;
                    }

                    await _leadRepository.UpdateAsync(lead);

                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 4;
                    leadactivity.SectionId = input.SectionId;
                    leadactivity.ActionNote = "Lead Tranfer To Different Organization";
                    leadactivity.LeadId = lead.Id;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    leadactivity.CreatorUserId = AbpSession.UserId;
                    await _leadactivityRepository.InsertAsync(leadactivity);
                    var log = new UserActivityLogDto();
                    log.ActionId = 4;
                    log.ActionNote = "Lead Tranfer To Different Organization";
                    log.Section = input.Section;
                    await _userActivityLogServiceProxy.Create(log);
                }
            }
            else
            {
                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == input.AssignToUserID).FirstOrDefault();

                var timezone = _settingManager.GetSettingValueForTenant(TimingSettingNames.TimeZone, (int)AbpSession.TenantId); //Get TimeInfo By Tenant
                //var tz = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                var tz = TimeZoneInfo.FindSystemTimeZoneById(timezone);


                var UserListsss = (from user in _userRepository.GetAll()
                                   join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                   from ur in urJoined.DefaultIfEmpty()
                                   join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                   from us in usJoined.DefaultIfEmpty()
                                   join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                                   from uo in uoJoined.DefaultIfEmpty()
                                   join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                                   from ut in utJoined.DefaultIfEmpty()
                                   where (us != null && us.DisplayName == "Sales Rep" && uo != null && uo.OrganizationUnitId == input.OrganizationID)
                                   select (user.Id)).ToList();

                IList<string> role = await _userManager.GetRolesAsync(assignedToUser);
                var roleName = "";
                if (role.Contains("Sales Manager"))
                {
                    roleName = "Sales Manager";
                }
                else
                {
                    roleName = "Sales Rep";
                }

                foreach (var leadid in input.LeadIds)
                {
                    var lead = await _leadRepository.FirstOrDefaultAsync(leadid);

                    if (lead.LeadStatusId == 7 || lead.LeadStatusId == 8)
                    {
                        PreviousJobStatus jobStstus = new PreviousJobStatus();
                        jobStstus.LeadId = lead.Id;
                        if (AbpSession.TenantId != null)
                        {
                            jobStstus.TenantId = (int)AbpSession.TenantId;
                        }
                        jobStstus.CurrentID = 11;
                        jobStstus.PreviousId = lead.LeadStatusId;
                        await _previousJobStatusRepository.InsertAsync(jobStstus);
                    }
                    if (lead.LeadStatusId == 1)
                    {
                        PreviousJobStatus jobStstus = new PreviousJobStatus();
                        jobStstus.LeadId = lead.Id;
                        if (AbpSession.TenantId != null)
                        {
                            jobStstus.TenantId = (int)AbpSession.TenantId;
                        }
                        jobStstus.CurrentID = 10;
                        jobStstus.PreviousId = lead.LeadStatusId;
                        await _previousJobStatusRepository.InsertAsync(jobStstus);
                        lead.LeadStatusId = 10;
                    }
                    else
                    {
                        if (lead.AssignToUserID != null)
                        {
                            if (UserListsss.Contains((long)lead.AssignToUserID))
                            {
                                PreviousJobStatus jobStstus = new PreviousJobStatus();
                                jobStstus.LeadId = lead.Id;
                                if (AbpSession.TenantId != null)
                                {
                                    jobStstus.TenantId = (int)AbpSession.TenantId;
                                }
                                jobStstus.CurrentID = 11;
                                jobStstus.PreviousId = lead.LeadStatusId;
                                await _previousJobStatusRepository.InsertAsync(jobStstus);
                                lead.LeadStatusId = 11;
                                lead.LeadAssignDate = DateTime.UtcNow;
                            }
                            else
                            {
                                PreviousJobStatus jobStstus = new PreviousJobStatus();
                                jobStstus.LeadId = lead.Id;
                                if (AbpSession.TenantId != null)
                                {
                                    jobStstus.TenantId = (int)AbpSession.TenantId;
                                }
                                jobStstus.CurrentID = 10;
                                jobStstus.PreviousId = lead.LeadStatusId;
                                await _previousJobStatusRepository.InsertAsync(jobStstus);
                                lead.LeadStatusId = 10;
                                lead.LeadAssignDate = DateTime.UtcNow;
                            }
                        }
                        else
                        {
                            PreviousJobStatus jobStstus = new PreviousJobStatus();
                            jobStstus.LeadId = lead.Id;
                            if (AbpSession.TenantId != null)
                            {
                                jobStstus.TenantId = (int)AbpSession.TenantId;
                            }
                            jobStstus.CurrentID = 10;
                            jobStstus.PreviousId = 0;
                            await _previousJobStatusRepository.InsertAsync(jobStstus);
                            lead.LeadStatusId = 10;
                        }
                    }
                    if (lead.AssignToUserID == null)
                    {
                        lead.AssignToUserID = input.AssignToUserID;
                        lead.LeadAssignDate = DateTime.UtcNow;
                        lead.AssignDate = DateTime.UtcNow;

                        if (input.OrganizationID != null)
                        {
                            lead.OrganizationId = (int)input.OrganizationID;
                        }

                        if (lead.Area != null && lead.PostCode != null)
                        {
                            var postCode = Convert.ToInt64(lead.PostCode);
                            var area = await _postCodeRangeRepository.GetAll().Where(e => e.StartPostCode <= postCode && e.EndPostCode >= postCode).AsNoTracking().Select(e => e.Area).FirstOrDefaultAsync();

                            if (!string.IsNullOrEmpty(area))
                            {
                                lead.Area = area;
                            }
                        }

                        await _leadRepository.UpdateAsync(lead);
                        var log = new UserActivityLogDto();

                        LeadActivityLog leadactivity = new LeadActivityLog();
                        leadactivity.ActionId = 3;
                        leadactivity.SectionId = input.SectionId;
                        if (lead.LeadStatusId == 7)
                        {
                            leadactivity.ActionNote = "Rejected Lead Assign To " + assignedToUser.FullName;
                            log.ActionNote = "Rejected Lead Assign To " + assignedToUser.FullName;
                        }
                        else if (lead.LeadStatusId == 8)
                        {
                            leadactivity.ActionNote = "Canceled Lead Assign To " + assignedToUser.FullName;
                            log.ActionNote = "Canceled Lead Assign To " + assignedToUser.FullName;
                        }
                        else
                        {
                            leadactivity.ActionNote = "Lead Assign To " + assignedToUser.FullName;
                            log.ActionNote = "Lead Assign To " + assignedToUser.FullName;
                        }

                        leadactivity.LeadId = lead.Id;
                        if (AbpSession.TenantId != null)
                        {
                            leadactivity.TenantId = (int)AbpSession.TenantId;
                        }
                        leadactivity.CreatorUserId = AbpSession.UserId;
                        await _leadactivityRepository.InsertAsync(leadactivity);

                        log.ActionId = 3;
                        log.Section = input.Section;
                        await _userActivityLogServiceProxy.Create(log);
                    }
                    else
                    {
                        var log = new UserActivityLogDto();

                        var assignedFromUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
                        LeadActivityLog leadactivity = new LeadActivityLog();
                        leadactivity.ActionId = 4;
                        leadactivity.SectionId = input.SectionId;
                        if (lead.LeadStatusId == 7)
                        {
                            leadactivity.ActionNote = "Rejected Lead Transfer From " + assignedFromUser.FullName + " To " + assignedToUser.FullName;
                            log.ActionNote = "Rejected Lead Transfer From " + assignedFromUser.FullName + " To " + assignedToUser.FullName;
                        }
                        else if (lead.LeadStatusId == 8)
                        {
                            leadactivity.ActionNote = "Canceled Lead Transfer From " + assignedFromUser.FullName + " To " + assignedToUser.FullName;
                            log.ActionNote = "Canceled Lead Transfer From " + assignedFromUser.FullName + " To " + assignedToUser.FullName;
                        }
                        else if (lead.LeadStatusId == 11)
                        {
                            leadactivity.ActionNote = "Lead ReAssign From " + assignedFromUser.FullName + " To " + assignedToUser.FullName;
                            log.ActionNote = "Lead ReAssign From " + assignedFromUser.FullName + " To " + assignedToUser.FullName;
                        }
                        else
                        {
                            leadactivity.ActionNote = "Lead Transfer From " + assignedFromUser.FullName + " To " + assignedToUser.FullName;
                            log.ActionNote = "Lead Transfer From " + assignedFromUser.FullName + " To " + assignedToUser.FullName;
                        }
                        leadactivity.LeadId = lead.Id;
                        if (AbpSession.TenantId != null)
                        {
                            leadactivity.TenantId = (int)AbpSession.TenantId;
                        }
                        leadactivity.CreatorUserId = AbpSession.UserId;
                        await _leadactivityRepository.InsertAsync(leadactivity);

                        lead.AssignToUserID = input.AssignToUserID;
                        lead.LeadAssignDate = DateTime.UtcNow;

                        await _leadRepository.UpdateAsync(lead);

                        log.ActionId = 4;
                        log.Section = input.Section;
                        await _userActivityLogServiceProxy.Create(log);
                    }

                }

                if (input.LeadIds.Count > 0)
                {
                    var User_List = _userRepository.GetAll().ToList();
                    var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == input.AssignToUserID).Select(e => e.TeamId).FirstOrDefault();

                    var UserList = (from user in User_List
                                    join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                    from ur in urJoined.DefaultIfEmpty()

                                    join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                    from us in usJoined.DefaultIfEmpty()

                                    join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                                    from ut in utJoined.DefaultIfEmpty()

                                    where us != null && us.DisplayName == roleName && ut != null && ut.TeamId == TeamId
                                    select user).Distinct();

                    foreach (var item in UserList)
                    {
                        if (item.Id == assignedToUser.Id)
                        {
                            string msg = string.Format("{0} Lead Assigned To You", input.LeadIds.Count);
                            await _appNotifier.LeadAssiged(item, msg, NotificationSeverity.Info);

                            //Send Email
                            //try
                            //{
                            //    var OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationID).Select(e => e.DisplayName).FirstOrDefault();
                            //    await _userEmailer.SendEmailNewLeadAssign(item, Convert.ToString(input.LeadIds.Count));
                            //}
                            //catch (Exception ex)
                            //{

                            //}
                        }
                        else
                        {
                            if (roleName != "Sales Rep")
                            {
                                string msg = string.Format("{0} Lead Assigned To {1}", input.LeadIds.Count, assignedToUser.FullName);
                                await _appNotifier.LeadAssiged(item, msg, NotificationSeverity.Info);
                            }
                            if (roleName != "Sales Rep")
                            {
                                //Send Email
                                //try
                                //{
                                //    var OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationID).Select(e => e.DisplayName).FirstOrDefault();
                                //    await _userEmailer.SendEmailNewLeadToAssignTeamMember(item, Convert.ToString(input.LeadIds.Count), assignedToUser.FullName);
                                //}
                                //catch (Exception ex)
                                //{

                                //}
                            }
                        }
                    }
                }
            }

        }

        public async Task TransferLeadToOtherOrganization(GetLeadForAssignOrTransferOutput input)
        {
            var ChangedOrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationID).Select(e => e.DisplayName).FirstOrDefault();
            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == input.AssignToUserID).FirstOrDefault();

            foreach (var leadid in input.LeadIds)
            {
                var lead = await _leadRepository.FirstOrDefaultAsync(leadid);
                var assignedFromUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
                var PreviousOrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == lead.OrganizationId).Select(e => e.DisplayName).FirstOrDefault();
                lead.LeadAssignDate = DateTime.UtcNow;
                lead.LeadStatusId = 10;
                lead.OrganizationId = (int)input.OrganizationID;

                if (lead.AssignToUserID == null)
                {
                    lead.AssignDate = DateTime.UtcNow;
                }

                await _leadRepository.UpdateAsync(lead);

                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 3;
                leadactivity.SectionId = 0;
                leadactivity.ActionNote = "Lead Assign To " + assignedToUser.FullName + " In " + ChangedOrganizationName + "Organization From " + assignedFromUser.FullName + ", " + PreviousOrganizationName + " Organization";
                leadactivity.LeadId = lead.Id;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                leadactivity.CreatorUserId = AbpSession.UserId;
                await _leadactivityRepository.InsertAsync(leadactivity);
            }

            var User_List = _userRepository.GetAll().ToList();
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == input.AssignToUserID).Select(e => e.TeamId).FirstOrDefault();

            var UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()

                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()

                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()

                            where us != null && us.DisplayName == "Sales Manager" && ut != null && ut.TeamId == TeamId
                            select user);

            foreach (var item in UserList)
            {
                if (item.Id == assignedToUser.Id)
                {
                    string msg = string.Format("{0} New Lead Assigned To You", input.LeadIds.Count);
                    await _appNotifier.LeadAssiged(item, msg, NotificationSeverity.Info);
                }
                else
                {
                    string msg = string.Format("{0} New Lead Assigned To {0}", input.LeadIds.Count, assignedToUser.FullName);
                    await _appNotifier.LeadAssiged(item, msg, NotificationSeverity.Info);
                }
            }
        }

        public async Task<List<GetActivityLogViewDto>> GetLeadActivityLog(GetActivityLogInput input)
        {

            List<int> leadActionids = new List<int>() { 1, 2, 3, 4, 5 };
            List<int> smsActionids = new List<int>() { 6, 20 };
            List<int> jobActionids = new List<int>() { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 };
            List<int> promotionActionids = new List<int>() { 21, 22 };
            List<int> serviceids = new List<int>() { 27, 29, 30 };
            List<int> warrantyCliamids = new List<int>() { 45, 46, 47, 48, 49, 50 };
            input.ServiceId = input.ServiceId != null ? input.ServiceId : 0;

            var allactionids = _lookup_leadActionRepository.GetAll().Select(e => e.Id).ToList();
            var jobHistory = _JobHistoryRepository.GetAll();
            var Result = _leadactivityRepository.GetAll().Include(e => e.LeadFk).Include(e => e.LeadActionIdFk)
                .Where(L => L.LeadId == input.LeadId)
                .WhereIf(input.ActionId == 0, L => allactionids.Contains(L.ActionId))
                .WhereIf(input.ActionId == 1, L => leadActionids.Contains(L.ActionId))
                .WhereIf(input.ActionId == 6, L => smsActionids.Contains(L.ActionId))
                .WhereIf(input.ActionId == 7, L => L.ActionId == 7)
                .WhereIf(input.ActionId == 8, L => L.ActionId == 8)
                .WhereIf(input.ActionId == 9, L => L.ActionId == 9)
                .WhereIf(input.ActionId == 10, L => jobActionids.Contains(L.ActionId))
                .WhereIf(input.ActionId == 11, L => promotionActionids.Contains(L.ActionId))
                .WhereIf(input.ActionId == 24, L => L.ActionId == 24)
                .WhereIf(input.ActionId == 27, L => serviceids.Contains(L.ActionId))
                .WhereIf(input.ActionId == 28, L => L.ActionId == 28)
                .WhereIf(input.ActionId == 33, L => warrantyCliamids.Contains(L.ActionId))
                .WhereIf(input.CurrentPage != false, L => L.SectionId == input.SectionId)
                .WhereIf(input.OnlyMy != false, L => L.CreatorUserId == AbpSession.UserId)
                .WhereIf(input.ServiceId > 0, L => L.ServiceId == input.ServiceId)
                //.WhereIf(input.ServiceId == 0, L => L.ServiceId == null)
                ;

            if (!input.AllActivity)
            {
                Result = Result.OrderByDescending(e => e.Id).Take(10);
            }
            else
            {
                Result = Result.OrderByDescending(e => e.Id);
            }

            var leads = from o in Result
                            //join o1 in _lookup_leadActionRepository.GetAll() on o.ActionId equals o1.Id into j1
                            //from s1 in j1.DefaultIfEmpty()

                            //join o2 in _leadRepository.GetAll() on o.LeadId equals o2.Id into j2
                            //from s2 in j2.DefaultIfEmpty()

                        join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                        from s3 in j3.DefaultIfEmpty()

                        let link = _JobHistoryRepository.GetAll().Where(e => e.JobActionId == o.Id).Any()

                        select new GetActivityLogViewDto()
                        {
                            Id = o.Id,
                            //ActionName = s1 == null || s1.ActionName == null ? "" : s1.ActionName.ToString(),
                            ActionName = o.LeadActionIdFk.ActionName,
                            ActionId = o.ActionId,
                            ActionNote = o.ActionNote,
                            ActivityNote = o.ActionId == 6 || o.ActionId == 7 ? o.Body : o.ActivityNote,
                            LeadId = o.LeadId,
                            CreationTime = o.CreationTime,
                            ActivityDate = o.ActivityDate,
                            Section = _SectionAppService.GetAll().Where(e => e.SectionId == o.SectionId).Select(e => e.SectionName).FirstOrDefault(),
                            ShowLeadJobDetail = o.ActionId == 2 ? 1 : (o.ActionId == 11 || o.ActionId == 13 || o.ActionId == 14 || o.ActionId == 15 || o.ActionId == 16 || o.ActionId == 18 || o.ActionId == 19 || o.ActionId == 26 || o.ActionId == 33 || o.ActionId == 30 || o.ActionId == 46 || o.ActionId == 48 || o.ActionId == 50) ? 2 : o.ActionId == 21 ? 3 : o.ActionId == 7 ? 4 : (o.ActionId == 1 || o.ActionId == 31 || o.ActionId == 10) ? 5: 0,
                            ShowJobLink = link == true ? 1 : 2,
                            ActivitysDate = o.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss"),
                            CreatorUserName = s3 == null || s3.FullName == null ? "" : s3.FullName.ToString(),
                            //LeadCompanyName = s2 == null || s2.CompanyName == null ? "" : s2.CompanyName.ToString()
                            LeadCompanyName = o.LeadFk.CompanyName,
                            promotionId = o.PromotionId
                        };

            return new List<GetActivityLogViewDto>(
                   await leads.ToListAsync()
               );
        }

        public async Task<List<LeadtrackerHistoryDto>> GetLeadActivityLogHistory(GetActivityLogInput input)
        {
            try
            {
                var Result = (from item in _LeadtrackerHistoryRepository.GetAll()
                              join ur in _userRepository.GetAll() on item.CreatorUserId equals ur.Id into urjoined
                              from ur in urjoined.DefaultIfEmpty()
                              where (item.LeadActionId == input.LeadId)
                              select new LeadtrackerHistoryDto()
                              {
                                  Id = item.Id,
                                  FieldName = item.FieldName,
                                  PrevValue = item.PrevValue,
                                  CurValue = item.CurValue,
                                  Lastmodifiedbyuser = ur.Name + " " + ur.Surname
                              }).OrderBy(e => e.Id)
                   .ToList();
                return Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<LeadtrackerHistoryDto>> GetJobActivityLogHistory(GetActivityLogInput input)
        {
            try
            {
                var Result = (from item in _JobHistoryRepository.GetAll()
                              join ur in _userRepository.GetAll() on item.CreatorUserId equals ur.Id into urjoined
                              from ur in urjoined.DefaultIfEmpty()
                              where (item.JobActionId == input.LeadId)
                              select new LeadtrackerHistoryDto()
                              {
                                  Id = item.Id,
                                  FieldName = item.FieldName,
                                  PrevValue = item.PrevValue,
                                  CurValue = item.CurValue,
                                  Lastmodifiedbyuser = ur.Name + " " + ur.Surname
                              })
                   .ToList();
                return Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task AddActivityLog(ActivityLogInput input)
        {
            input.ActivityDate = _timeZoneConverter.Convert(input.ActivityDate, (int)AbpSession.TenantId);
            var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var lead = await _leadRepository.FirstOrDefaultAsync(input.LeadId);
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();

            var activity = ObjectMapper.Map<LeadActivityLog>(input);
            activity.ActionNote = input.ActionNote;
            activity.Subject = input.Subject;
            activity.SectionId = input.SectionId;
            activity.ServiceId = input.ServiceId;
            var SectionName = _SectionAppService.GetAll().Where(e => e.SectionId == input.SectionId).Select(e => e.SectionName).FirstOrDefault();
            if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
            {
                activity.TemplateId = input.EmailTemplateId;
            }
            else if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
            {
                activity.TemplateId = input.SMSTemplateId;
            }
            else
            {
                activity.TemplateId = 0;
            }

            if (AbpSession.TenantId != null)
            {
                input.TenantId = (int)AbpSession.TenantId;
            }
            if (input.ActionId == 9)
            {
                string msg = input.ActionNote + " " + input.ActivityNote + " For " + lead.CompanyName + " By " + CurrentUser.Name + " From " + SectionName;
                await _appNotifier.LeadComment(assignedToUser, msg, NotificationSeverity.Warn);
            }
            if (input.ActionId == 25)
            {
                var assignedToUserForTodo = _userRepository.GetAll().Where(u => u.Id == input.UserId).FirstOrDefault();
                string msg = input.ActionNote + " " + input.ActivityNote + " For " + lead.CompanyName + " By " + CurrentUser.Name + " From " + SectionName;
                await _appNotifier.LeadComment(assignedToUserForTodo, msg, NotificationSeverity.Warn);
            }
            if (input.ActionId == 8)
            {
                var CurrentTime = (_timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId));
                if (Convert.ToDateTime(input.Body) < CurrentTime)
                {
                    throw new UserFriendlyException(L("PleaseSelectGreaterDatethanCurrentDate"));
                }
                activity.ActivityDate = Convert.ToDateTime(input.Body);
                input.Body = "";
                string msg = string.Format("Reminder " + input.ActionNote + " " + input.ActivityNote + " For " + lead.CompanyName + " By " + CurrentUser.Name + " From " + SectionName);
                await _appNotifier.LeadAssiged(CurrentUser, msg, NotificationSeverity.Info);
            }
            if (input.ActionId == 7)
            {
                //MailMessage mail = new MailMessage
                //{
                //    //From = new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                //    //To = { lead.Email },
                //    //Subject = input.Subject,//EmailBody.TemplateName + "The Solar Product Lead Notification",
                //    //Body = input.Body,
                //    //IsBodyHtml = true
                //};
                await _emailSender.SendAsync(new MailMessage
                {
                    From = new MailAddress(input.EmailFrom),
                    To = { lead.Email }, ////{ "hiral.prajapati@meghtechnologies.com" },
                    Subject = input.Subject,//EmailBody.TemplateName + "The Solar Product Lead Notification",
                    Body = input.Body,
                    IsBodyHtml = true
                });
                //foreach (var item in input.AttachmentList)
                //{
                //    //Attach Data as Email Attachment
                //    //MemoryStream pdf = new MemoryStream(bytes);
                //    //Attachment data = new Attachment(pdf, fileName);
                //    //mail.Attachments.Add(data);
                //}

                //await this._emailSender.SendAsync(mail);
                //var EmailBody = _emailTemplateRepository.GetAll().Where(e => e.Id == input.EmailTemplateId).Select(e => new { e.Body, e.TemplateName }).FirstOrDefault();
                //await _emailSender.SendAsync(new MailMessage
                //{
                //	From = new MailAddress(assignedToUser.EmailAddress),
                //	To = { lead.Email },
                //	Subject = "Lead Update",//EmailBody.TemplateName + "The Solar Product Lead Notification",
                //	Body = input.Body,
                //	IsBodyHtml = true
                //});
            }
            activity.Body = Regex.Replace(input.Body, "<.*?>", String.Empty);
            activity.CreatorUserId = AbpSession.UserId;
            await _leadactivityRepository.InsertAndGetIdAsync(activity);
            if (input.ActionId == 6)
            {
                //Send SMS
                SendSMSInput sendSMSInput = new SendSMSInput();
                sendSMSInput.PhoneNumber = lead.Mobile;
                sendSMSInput.Text = input.Body;
                sendSMSInput.ActivityId = activity.Id;
                await _applicationSettings.SendSMS(sendSMSInput);
            }

            if (input.SectionId == 13 || input.SectionId == 14) // Add My Lead and Lead Tracker
            {
                if (input.ActionId == 8)
                {
                    lead.ActivityDate = activity.ActivityDate;
                    lead.ActivityDescription = activity.ActivityNote;
                }
                if (input.ActionId == 24)
                {
                    lead.ActivityNote = activity.ActivityNote;
                }

                await _leadRepository.UpdateAsync(lead);
            }

            if (input.SectionId == 8) // Add My Jobs Tracker
            {
                var job = await _jobRepository.FirstOrDefaultAsync(e => e.LeadId == activity.LeadId);
                if (input.ActionId == 8)
                {
                    job.JobReminderTime = activity.ActivityDate;
                    job.JobReminderDes = activity.ActivityNote;
                }
                if (input.ActionId == 24)
                {
                    job.JobComment = activity.ActivityNote;
                }

                await _jobRepository.UpdateAsync(job);
            }

            //GetUserNotificationsInput input1 = new GetUserNotificationsInput();
            //input1.State = null;
            //input1.StartDate = null;
            //input1.EndDate = null;
            //input1.MaxResultCount = 3;
            //input1.SkipCount = 0;
            //await _NotificationAppService.GetUserNotifications(input1);
        }

        public async Task AddActivityLogWithNotification(ActivityLogInput input)
        {
            input.ActivityDate = _timeZoneConverter.Convert(input.ActivityDate, (int)AbpSession.TenantId);
            var lead = await _leadRepository.FirstOrDefaultAsync(input.LeadId);
            var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var duplicateLead = _leadRepository.GetAll().Where(e => (e.Phone == lead.Phone || e.Mobile == lead.Mobile || e.Email == lead.Email) && e.Id != lead.Id && e.AssignToUserID != null).FirstOrDefault();
            var SectionName = _SectionAppService.GetAll().Where(e => e.SectionId == input.SectionId).Select(e => e.SectionName).FirstOrDefault();
            if (duplicateLead != null)
            {

                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == duplicateLead.AssignToUserID).FirstOrDefault();
                var activity = ObjectMapper.Map<LeadActivityLog>(input);
                activity.ActionNote = input.ActionNote;
                activity.Subject = input.Subject;
                activity.SectionId = input.SectionId;
                activity.ServiceId = input.ServiceId;
                if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
                {
                    activity.TemplateId = input.EmailTemplateId;
                }
                else if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                {
                    activity.TemplateId = input.SMSTemplateId;
                }
                else
                {
                    activity.TemplateId = 0;
                }
                if (AbpSession.TenantId != null)
                {
                    input.TenantId = (int)AbpSession.TenantId;
                }
                activity.CreatorUserId = AbpSession.UserId;
                await _leadactivityRepository.InsertAndGetIdAsync(activity);
                if (input.ActionId == 25)
                {
                    var assignedToUserForTodo = _userRepository.GetAll().Where(u => u.Id == input.UserId).FirstOrDefault();
                    string msg = input.ActionNote + " " + input.ActivityNote + " For " + lead.CompanyName + " By " + CurrentUser.Name + " From " + SectionName;
                    await _appNotifier.LeadComment(assignedToUserForTodo, msg, NotificationSeverity.Warn);
                }
                if (input.ActionId == 8)
                {
                    string msg = string.Format("Reminder " + input.ActionNote + " " + input.ActivityNote + " For " + duplicateLead.CompanyName + " By " + CurrentUser.Name + " From " + SectionName);
                    await _appNotifier.LeadAssiged(CurrentUser, msg, NotificationSeverity.Info);
                }
                if (input.ActionId == 9)
                {
                    string msg = input.ActionNote + " " + input.ActivityNote + " For " + duplicateLead.CompanyName + " By " + CurrentUser.Name + " From " + SectionName;
                    await _appNotifier.LeadComment(assignedToUser, msg, NotificationSeverity.Warn);

                    //Email
                    if (!string.IsNullOrEmpty(assignedToUser.EmailAddress))
                    {
                        await _emailSender.SendAsync(new MailMessage
                        {
                            To = { assignedToUser.EmailAddress },
                            Subject = input.Subject,
                            Body = input.Body.ToString(),
                            IsBodyHtml = true
                        });
                    }

                    //Send SMS
                    if (!string.IsNullOrEmpty(assignedToUser.Mobile))
                    {
                        SendSMSInput sendSMSInput = new SendSMSInput();
                        sendSMSInput.PhoneNumber = assignedToUser.Mobile;
                        sendSMSInput.Text = input.Body;
                        sendSMSInput.ActivityId = activity.Id;
                        await _applicationSettings.SendSMS(sendSMSInput);
                    }
                }
                //GetUserNotificationsInput input1 = new GetUserNotificationsInput();
                //input1.State = null;
                //input1.StartDate = null;
                //input1.EndDate = null;
                //input1.MaxResultCount = 3;
                //input1.SkipCount = 0;
                //await _NotificationAppService.GetUserNotifications(input1);

            }
            else
            {
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
                var activity = ObjectMapper.Map<LeadActivityLog>(input);
                activity.ActionNote = input.ActionNote;
                activity.Subject = input.Subject;
                activity.SectionId = input.SectionId;
                if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
                {
                    activity.TemplateId = input.EmailTemplateId;
                }
                else if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                {
                    activity.TemplateId = input.SMSTemplateId;
                }
                else
                {
                    activity.TemplateId = 0;
                }
                if (AbpSession.TenantId != null)
                {
                    input.TenantId = (int)AbpSession.TenantId;
                }
                activity.CreatorUserId = AbpSession.UserId;
                await _leadactivityRepository.InsertAndGetIdAsync(activity);
                if (input.ActionId == 25)
                {
                    var assignedToUserForTodo = _userRepository.GetAll().Where(u => u.Id == input.UserId).FirstOrDefault();
                    string msg = input.ActionNote + " " + input.ActivityNote + " For " + lead.CompanyName + " By " + CurrentUser.Name + " From " + SectionName;
                    await _appNotifier.LeadComment(assignedToUserForTodo, msg, NotificationSeverity.Warn);
                }
                if (input.ActionId == 8)
                {
                    string msg = string.Format("Reminder " + input.ActionNote + " " + input.ActivityNote + " For " + lead.CompanyName + " By " + CurrentUser.Name + " From " + SectionName);
                    await _appNotifier.LeadAssiged(CurrentUser, msg, NotificationSeverity.Info);
                }
                if (input.ActionId == 9)
                {
                    string msg = input.ActionNote + " " + input.ActivityNote + " For " + lead.CompanyName + " By " + CurrentUser.Name + " From " + SectionName;
                    await _appNotifier.LeadComment(assignedToUser, msg, NotificationSeverity.Warn);
                    //Email
                    if (!string.IsNullOrEmpty(assignedToUser.EmailAddress))
                    {
                        await _emailSender.SendAsync(new MailMessage
                        {
                            To = { assignedToUser.EmailAddress },
                            Subject = input.Subject,
                            Body = input.Body.ToString(),
                            IsBodyHtml = true
                        });
                    }


                    //Send SMS
                    if (!string.IsNullOrEmpty(assignedToUser.Mobile))
                    {
                        SendSMSInput sendSMSInput = new SendSMSInput();
                        sendSMSInput.PhoneNumber = assignedToUser.Mobile;
                        sendSMSInput.Text = input.Body;
                        sendSMSInput.ActivityId = activity.Id;
                        await _applicationSettings.SendSMS(sendSMSInput);
                    }
                }

                //GetUserNotificationsInput input1 = new GetUserNotificationsInput();
                //input1.State = null;
                //input1.StartDate = null;
                //input1.EndDate = null;
                //input1.MaxResultCount = 3;
                //input1.SkipCount = 0;

                //await _NotificationAppService.GetUserNotifications(input1);
            }


        }

        public GetLeadCountsDto GetLeadCounts()
        {
            var Result = new GetLeadCountsDto();

            Result.New = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 1).Count());
            Result.UnHandled = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 2).Count());
            Result.Cold = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 3).Count());
            Result.Warm = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 4).Count());
            Result.Hot = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 5).Count());
            Result.Upgrade = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 6).Count());
            Result.Rejected = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 7).Count());
            Result.Cancelled = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 8).Count());
            Result.Closed = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 9).Count());
            Result.Assigned = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 10).Count());
            Result.Total = Convert.ToString(_leadRepository.GetAll().Count());
            return Result;
        }

        public async Task<List<GetLeadForViewDto>> GetUserLeadForDashboard()
        {
            int UserId = (int)AbpSession.UserId;

            var Leads = _leadRepository.GetAll()
                        .Where(e => e.AssignToUserID == UserId);

            var listdata = Leads.MapTo<List<GetLeadForViewDto>>();

            return new List<GetLeadForViewDto>(listdata);
        }

        public virtual async Task ChangeDuplicateStatus(GetLeadForChangeDuplicateStatusOutput input)
        {
            var lead = await _leadRepository.FirstOrDefaultAsync((int)input.Id);
            lead.IsDuplicate = input.IsDuplicate;
            lead.ChangeStatusDate = DateTime.UtcNow;
            lead.IsWebDuplicate = input.IsWebDuplicate;
            //await _leadRepository.UpdateAsync(lead);

            LeadActivityLog leadactivity = new LeadActivityLog();
            if (input.IsDuplicate != null)
            {
                leadactivity.ActionId = 5;
                if (input.IsDuplicate == true)
                {
                    leadactivity.ActionNote = "Lead Status Changed To Database Duplicate";
                }
                else
                {
                    leadactivity.ActionNote = "Lead Status Changed To Web Duplicate";
                }
            }
            else
            {
                leadactivity.ActionId = 5;
                if (input.IsDuplicate == true)
                {
                    leadactivity.ActionNote = "Lead Status Changed To Not Database Duplicate";
                }
                else
                {
                    leadactivity.ActionNote = "Lead Status Changed To Not Web Duplicate";
                }
            }
            leadactivity.SectionId = 0;
            leadactivity.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            //await _leadactivityRepository.InsertAsync(leadactivity);
        }

        /// <summary>
        /// Change Duplicate Status for multiple Leads
        /// </summary>
        /// <param name="input">Lead Id[], IsDuplicate - True(Web Dup) - False(DataBase Dup)</param>
        /// <returns></returns>
        public virtual async Task ChangeDuplicateStatusForMultipleLeads(GetLeadForChangeDuplicateStatusOutput input)
        {
            foreach (var item in input.LeadId)
            {
                var lead = await _leadRepository.FirstOrDefaultAsync((int)item);
                lead.IsDuplicate = input.IsDuplicate;
                lead.ChangeStatusDate = DateTime.UtcNow;
                lead.IsWebDuplicate = input.IsWebDuplicate;

                await _leadRepository.UpdateAsync(lead);

                LeadActivityLog leadactivity = new LeadActivityLog();
                if (input.IsDuplicate != null)
                {
                    leadactivity.ActionId = 5;
                    if (input.IsDuplicate == true)
                    {
                        leadactivity.ActionNote = "Lead Status Changed To Database Duplicate";
                    }
                    else
                    {
                        leadactivity.ActionNote = "Lead Status Changed To Web Duplicate";
                    }
                }
                else
                {
                    leadactivity.ActionId = 5;
                    if (input.IsDuplicate == true)
                    {
                        leadactivity.ActionNote = "Lead Status Changed To Not Database Duplicate";
                    }
                    else
                    {
                        leadactivity.ActionNote = "Lead Status Changed To Not Web Duplicate";
                    }
                }
                leadactivity.LeadId = lead.Id;
                leadactivity.SectionId = input.SectionId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
        }

        public async Task ChangeDuplicateStatusForMultipleLeadsHide(GetLeadForChangeDuplicateStatusOutput input)
        {
            foreach (var item in input.LeadId)
            {
                var lead = await _leadRepository.FirstOrDefaultAsync((int)item);
                lead.HideDublicate = input.HideDublicate;
                lead.ChangeStatusDate = DateTime.UtcNow;

                await _leadRepository.UpdateAsync(lead);

                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 5;
                leadactivity.ActionNote = "Lead Status Changed To Web Duplicate";

                leadactivity.LeadId = lead.Id;
                leadactivity.SectionId = input.SectionId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }

                await _leadactivityRepository.InsertAsync(leadactivity);


            }
            var log = new UserActivityLogDto();
            log.ActionId = 5;
            log.ActionNote = input.LeadId.Count() +" Lead Status Changed To Web Duplicate";
            log.Section = "Manage Leads";
            await _userActivityLogServiceProxy.Create(log);

            //await UpdateLeads(input);
        }

        public virtual async Task UpdateWebDupLeads(GetLeadForChangeDuplicateStatusOutput input)
        {
            foreach (var item in input.LeadId)
            {
                var lead = await _leadRepository.FirstOrDefaultAsync((int)item);

                var DupLead = _leadRepository.GetAll().Where(e => e.OrganizationId == lead.OrganizationId && e.AssignToUserID == null && e.HideDublicate != true && ((e.Email == lead.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == lead.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Select(e => e.Id);

                if (DupLead.Count() == 1)
                {
                    var Id = DupLead.FirstOrDefault();
                    var leadT = await _leadRepository.FirstOrDefaultAsync(Id);
                    leadT.IsWebDuplicate = null;
                    await _leadRepository.UpdateAsync(leadT);

                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 5;
                    leadactivity.ActionNote = "Lead Status Changed To Duplicate";

                    leadactivity.LeadId = Id;
                    leadactivity.SectionId = input.SectionId;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }

            }
        }

        [UnitOfWork]
        public async Task<List<string>> GetAllUnitType(string unitType)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                return await _lookup_unitTypeRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(unitType), c => c.Name.ToLower().Contains(unitType.ToLower()))
                .Select(unitTypes => unitTypes == null || unitTypes.Name == null ? "" : unitTypes.Name.ToString()).ToListAsync();
            }
        }

        [UnitOfWork]
        public async Task<List<string>> GetAllStreetName(string streetName)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                return await _lookup_streetNameRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(streetName), c => c.Name.ToLower().Contains(streetName.ToLower()))
                .Select(streetNames => streetNames == null || streetNames.Name == null ? "" : streetNames.Name.ToString()).ToListAsync();
            }
        }

        [UnitOfWork]
        public async Task<List<string>> GetAllStreetType(string streetType)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                return await _lookup_streetTypeRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(streetType), c => c.Name.ToLower().Contains(streetType.ToLower()))
                .Select(streetTypes => streetTypes == null || streetTypes.Name == null ? "" : streetTypes.Name.ToString()).ToListAsync();
            }
        }

        [UnitOfWork]
        public async Task<List<string>> GetAllSuburb(string suburb)
        {
            // using (_unitOfWorkManager.Current.SetTenantId(null))
            //{
            var result = _lookup_postCodeRepository.GetAll()
            .WhereIf(!string.IsNullOrWhiteSpace(suburb), c => c.Suburb.ToLower().Contains(suburb.ToLower()));

            var suburbs = from o in result
                          join o1 in _lookup_stateRepository.GetAll() on o.StateId equals o1.Id into j1
                          from s1 in j1.DefaultIfEmpty()
                          select new LeadStatusLookupTableDto()
                          {
                              Value = o.Id,
                              Name = o.Suburb.ToString() + " || " + s1.Name.ToString() + " || " + o.PostalCode
                          };

            return await suburbs
                .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
            //}
        }

        [UnitOfWork]
        public async Task<List<string>> GetOnlySuburb(string suburb)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var result = _lookup_postCodeRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(suburb), c => c.Suburb.ToLower().Contains(suburb.ToLower()));

                var suburbs = from o in result
                              select new LeadStatusLookupTableDto()
                              {
                                  Value = o.Id,
                                  Name = o.Suburb.ToString()
                              };

                return await suburbs
                    .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
            }
        }

        [UnitOfWork]
        public async Task<List<LeadStateLookupTableDto>> GetAllStateForTableDropdown()
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                return await _lookup_stateRepository.GetAll()
                .Select(state => new LeadStateLookupTableDto
                {
                    Id = state.Id,
                    DisplayName = state == null || state.Name == null ? "" : state.Name.ToString().ToUpper()
                }).OrderBy(e => e.Id).ToListAsync();
            }
        }

        public async Task<List<LeadSourceLookupTableDto>> GetLeadSourceDrpdwnForLeadEditInsert()
        {
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            if (role.Contains("Admin"))
            {
                return await _lookup_leadSourceRepository.GetAll()
                            .Select(leadSource => new LeadSourceLookupTableDto
                            {
                                Id = leadSource.Id,
                                DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
                            }).ToListAsync();
            }
            else
            {
                return await _lookup_leadSourceRepository.GetAll().Where(e => e.IsActive == true)
                            .Select(leadSource => new LeadSourceLookupTableDto
                            {
                                Id = leadSource.Id,
                                DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
                            }).ToListAsync();
            }

        }

        public async Task<List<LeadSourceLookupTableDto>> GetAllLeadSourceForTableDropdown()
        {
            return await _lookup_leadSourceRepository.GetAll()
                        .Select(leadSource => new LeadSourceLookupTableDto
                        {
                            Id = leadSource.Id,
                            DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
                        }).ToListAsync();
        }

        public async Task<List<LeadSourceLookupTableDto>> GetAllLeadSourceDropdown()
        {
            return await _lookup_leadSourceRepository.GetAll()
                        .Select(leadSource => new LeadSourceLookupTableDto
                        {
                            Id = leadSource.Id,
                            DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
                        }).ToListAsync();
        }

        public async Task<List<JobStatusTableDto>> GetAllJobStatusTableDropdown()
        {
            return await _lookup_jobStatusRepository.GetAll()
            .Select(jobstaus => new JobStatusTableDto
            {
                Id = jobstaus.Id,
                DisplayName = jobstaus == null || jobstaus.Name == null ? "" : jobstaus.Name.ToString()
            }).ToListAsync();
        }

        public List<NameValue<string>> PromotionGetAllLeadStatus(string lead)
        {
            var exceptStatus = new[] { 6, 9, 7, 8, 1 }; //6=Closed, 9=upgrade, 7=Rejected  8=Canceled , 1=New
            var ListOfLeadStatus = _lookup_leadStatusRepository.GetAll()
                            //.Where(c => c.Id != 6 && c.Id != 9 && c.Id != 7 && c.Id != 8 && c.Id != 1)
                            .Where(c => c.Status.Contains(lead))
                            .Select(leadStatus =>
                                new LeadStatusLookupTableDto
                                {
                                    Value = leadStatus.Id,
                                    Name = leadStatus.Status.ToString()
                                });

            //Making NameValue Collection of Multi-Select DropDown

            var leadStatus = new List<NameValue<string>>();
            foreach (var item in ListOfLeadStatus)
                leadStatus.Add(new NameValue { Name = item.Name, Value = item.Value.ToString() });

            return
                lead.IsNullOrEmpty() ?
                leadStatus.ToList() :
                leadStatus.Where(c => c.Name.ToLower().Contains(lead.ToLower())).ToList();
        }

        public List<NameValue<string>> promotionGetAllJobStatus(string jobStatusName)
        {
            var ListOfJobStatus = _lookup_jobStatusRepository.GetAll()
                            .Where(c => c.Name.Contains(jobStatusName))
                            .Select(jobstatus =>
                                new LeadStatusLookupTableDto
                                {
                                    Value = jobstatus.Id,
                                    Name = jobstatus.Name.ToString()
                                });


            //Making NameValue Collection of Multi-Select DropDown

            var jobstatus = new List<NameValue<string>>();
            foreach (var item in ListOfJobStatus)
                jobstatus.Add(new NameValue { Name = item.Name, Value = item.Value.ToString() });


            return
                jobstatus.IsNullOrEmpty() ?
                jobstatus.ToList() :
                jobstatus.Where(c => c.Name.ToLower().Contains(jobStatusName.ToLower())).ToList();
        }

        public List<NameValue<string>> promotionGetAllLeadSource(string leadSourceName)
        {
            var ListOfLeadSource = _lookup_leadSourceRepository.GetAll()
                            .Where(c => c.Name.Contains(leadSourceName))
                            .Select(leadSource =>
                                new LeadStatusLookupTableDto
                                {
                                    Value = leadSource.Id,
                                    Name = leadSource.Name.ToString()
                                });


            //Making NameValue Collection of Multi-Select DropDown

            var leadSource = new List<NameValue<string>>();
            foreach (var item in ListOfLeadSource)
                leadSource.Add(new NameValue { Name = item.Name, Value = item.Value.ToString() });


            return
                leadSource.IsNullOrEmpty() ?
                leadSource.ToList() :
                leadSource.Where(c => c.Name.ToLower().Contains(leadSourceName.ToLower())).ToList();
        }

        public List<NameValue<string>> promotionGetAllState(string stateName)
        {
            var ListOfState = _lookup_stateRepository.GetAll()
                            .Where(c => c.Name.Contains(stateName))
                            .Select(obj =>
                                new LeadStatusLookupTableDto
                                {
                                    Value = obj.Id,
                                    Name = obj.Name.ToString()
                                });


            //Making NameValue Collection of Multi-Select DropDown

            var States = new List<NameValue<string>>();
            foreach (var item in ListOfState)
                States.Add(new NameValue { Name = item.Name, Value = item.Value.ToString() });


            return
                States.IsNullOrEmpty() ?
                States.ToList() :
                States.Where(c => c.Name.ToLower().Contains(stateName.ToLower())).ToList();
        }

        private int?[] getValuesFromNameValue(List<NameValue<int>> nameValues)
        {

            int?[] arr = new int?[nameValues.Count];


            if (nameValues != null)
            {
                int i = 0;

                foreach (var e in nameValues)
                {
                    arr[i] = e.Value;
                    i++;
                }
            }

            return arr;
        }

        public async Task<List<int>> GetTotalLeadsCountForPromotion(GetAllLeadsInputForPromotion input)
        {
            #region Old Code
            //var SDate = (_timeZoneConverter.Convert(input.StartDateFilter, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDateFilter, (int)AbpSession.TenantId));

            //if (input.TeamIdsFilter != null) Debug.Print(input.TeamIdsFilter != null ? input.TeamIdsFilter.Count().ToString() : "");
            //if (input.LeadStatusIdsFilter != null) Debug.Print(input.LeadStatusIdsFilter != null ? input.LeadStatusIdsFilter.Count().ToString() : "");

            //var leadidcontainupgrade = new List<int?>();
            //var leadidwithoutupgrade = new List<int?>();
            //if (input.JobStatusIdsFilter != null)
            //{
            //    foreach (var iteam in input.LeadStatusIdsFilter)
            //    {
            //        if (iteam == 6)
            //        {
            //            var upgrade = iteam;
            //            leadidcontainupgrade.Add(upgrade);
            //        }
            //        else
            //        {
            //            var upgrade = iteam;
            //            leadidwithoutupgrade.Add(upgrade);
            //        }
            //    }
            //}
            //else if (input.LeadStatusIdsFilter != null && input.JobStatusIdsFilter == null)
            //{
            //    leadidwithoutupgrade = input.LeadStatusIdsFilter;
            //}

            //int UserId = (int)AbpSession.UserId;

            //var arrleads = new List<int?>();

            //if (input.JobStatusIdsFilter != null)
            //{
            //    arrleads = (from o in _leadRepository.GetAll()
            //                join ut in _jobRepository.GetAll() on o.Id equals ut.LeadId into j1
            //                from ut1 in j1.DefaultIfEmpty()
            //                where leadidcontainupgrade.Contains(o.LeadStatusId) && input.JobStatusIdsFilter.Contains(ut1.JobStatusId)
            //                select ut1.LeadId).ToList();
            //}
            ////////customer stop for promotion
            //var stopresponceList = _PromotionUsersRepository.GetAll().Where(e => e.PromotionResponseStatusId == 2).Select(e => e.LeadId).ToList();

            ///////customer not made payment
            ////var Paidjobs = _invoicePaymentRepository.GetAll().Select(e => e.JobId).Distinct().ToList();
            ////var paidleads = _jobRepository.GetAll().Where(e => Paidjobs.Contains(e.Id)).Select(e => e.LeadId).Distinct().ToList();

            ////9=Closed
            //var abc = new List<int>();
            //if (arrleads.Count() > 0 && arrleads.Count() != null)
            //{
            //    var abc1 = (_leadRepository.GetAll()
            //          .Where(e => e.IsPromotion)
            //          .Where(e => arrleads.Contains(e.Id))
            //          .Where(e => e.LeadStatusId != 9 && !stopresponceList.Contains(e.Id)) //&& !paidleads.Contains(e.Id))
            //                                                                               //.WhereIf(leadidwithoutupgrade != null, e => leadidwithoutupgrade.Contains(e.LeadStatusId)) // LeadStatusID
            //          .WhereIf(input.LeadSourceIdsFilter != null && input.LeadSourceIdsFilter.Count() > 0, e => input.LeadSourceIdsFilter.Contains(e.LeadSourceId)) // LeadSourceID
            //          .WhereIf(input.StateIdsFilter != null && input.StateIdsFilter.Count() > 0, e => input.StateIdsFilter.Contains(e.StateId))
            //          .WhereIf(input.StartDateFilter != null, e => e.CreationTime >= SDate.Value.Date)
            //          .WhereIf(input.EndDateFilter != null, e => e.CreationTime <= EDate.Value.Date)
            //          .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
            //          .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
            //          .Where(e => e.OrganizationId == input.Orgaid).Select(e => e.Id).ToList());
            //    //.Union(_leadRepository.GetAll().Where(e => arrleads.Contains(e.Id)).Select(e => e.Id).ToList());

            //    //var abc1 = (_leadRepository.GetAll()
            //    //        .Where(e => arrleads.Contains(e.Id))
            //    //        .Where(e => e.IsPromotion)
            //    //        .Where(e => e.LeadStatusId != 9 && !stopresponceList.Contains(e.Id))
            //    //        ).Select(e => e.Id).ToList();

            //    abc = abc1.ToList();
            //}
            //else
            //{
            //    abc = (_leadRepository.GetAll()
            //       .Where(e => e.IsPromotion)
            //       .Where(e => e.LeadStatusId != 9 && !stopresponceList.Contains(e.Id)) //&& !paidleads.Contains(e.Id))
            //       .WhereIf(leadidwithoutupgrade != null && leadidwithoutupgrade.Count() > 0, e => leadidwithoutupgrade.Contains(e.LeadStatusId)) // LeadStatusID
            //       .WhereIf(input.LeadSourceIdsFilter != null && input.LeadSourceIdsFilter.Count() > 0, e => input.LeadSourceIdsFilter.Contains(e.LeadSourceId)) // LeadSourceID
            //       .WhereIf(input.StateIdsFilter != null && input.StateIdsFilter.Count() > 0, e => input.StateIdsFilter.Contains(e.StateId))
            //       .WhereIf(input.StartDateFilter != null, e => e.CreationTime >= SDate.Value.Date)
            //       .WhereIf(input.EndDateFilter != null, e => e.CreationTime <= EDate.Value.Date)
            //       .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
            //       .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
            //       .Where(e => e.OrganizationId == input.Orgaid).Select(e => e.Id).ToList());
            //}

            //var filteredLeads = _leadRepository.GetAll().Where(e => abc.Contains(e.Id));

            //var pagedAndFilteredLeads = filteredLeads;

            //if (input.TeamIdsFilter != null && input.TeamIdsFilter.Count() > 0)
            //{

            //    var leads = from o in filteredLeads
            //                join ut in _userTeamRepository.GetAll() on o.AssignToUserID equals ut.UserId into j1
            //                from ut1 in j1.DefaultIfEmpty()
            //                where (input.TeamIdsFilter == null || input.TeamIdsFilter.Contains(ut1.TeamId)) && ut1.IsDeleted == false
            //                select new GetLeadForViewDto
            //                {
            //                    Lead = new LeadDto { Id = o.Id }
            //                };

            //    var totalCount = await filteredLeads.CountAsync();

            //    return new PagedResultDto<GetLeadForViewDto>(
            //        totalCount,
            //        leads.ToList()
            //    );
            //}
            //else
            //{
            //    var leads = from o in filteredLeads
            //                select new GetLeadForViewDto
            //                {
            //                    Lead = new LeadDto { Id = o.Id }
            //                };
            //    var totalCount = await filteredLeads.CountAsync();

            //    return new PagedResultDto<GetLeadForViewDto>(
            //        totalCount,
            //        leads.ToList()
            //    );
            //}

            #endregion

            #region New Code By Suresh

            var SDate = (_timeZoneConverter.Convert(input.StartDateFilter, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDateFilter, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var StopResponceList = _PromotionUsersRepository.GetAll().Include(e => e.LeadFk)
                .Where(e => e.PromotionResponseStatusId == 2 && e.LeadFk.OrganizationId == input.Orgaid).Select(e => new { e.LeadId }); //Customer STOP for Promotion

            var Job_List = _jobRepository.GetAll().Include(e => e.LeadFk)
                .Where(e => e.LeadFk.OrganizationId == input.Orgaid)
                .WhereIf(input.DateType == "ProjectOpen" && input.StartDateFilter != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                .WhereIf(input.DateType == "ProjectOpen" && input.EndDateFilter != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                .WhereIf(input.DateType == "InstallDate" && input.StartDateFilter != null, e => e.InstallationDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                .WhereIf(input.DateType == "InstallDate" && input.EndDateFilter != null, e => e.InstallationDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                .Select(e => new { e.Id, e.JobNumber, e.JobStatusFk.Name, e.LeadId, e.JobStatusId, e.DepositeRecceivedDate, e.FirstDepositDate });
            int Status = 0;
            if (input.JobStatusIdsFilter != null && input.JobStatusIdsFilter.Count() > 0 && input.LeadStatusIdsFilter != null && input.LeadStatusIdsFilter.Count() > 0)
            {
                if (input.LeadStatusIdsFilter.Contains(6))
                {
                    Status = 1;
                }
            }

            var filteredLeads = _leadRepository.GetAll().Where(e => StopResponceList.Where(o => (int)o.LeadId == e.Id).Any() == false && e.OrganizationId == input.Orgaid && e.LeadStatusId != 9)

                            .WhereIf(input.LeadSourceIdsFilter != null && input.LeadSourceIdsFilter.Count > 0, e => input.LeadSourceIdsFilter.Contains(e.LeadSourceId))

                            .WhereIf(input.LeadStatusIdsFilter != null && input.LeadStatusIdsFilter.Count() > 0, e => input.LeadStatusIdsFilter.Contains(e.LeadStatusId))
                            .WhereIf(Status == 1, e => Job_List.Where(x => input.JobStatusIdsFilter.Contains((int)x.JobStatusId) && x.LeadId == e.Id).Any())

                            .WhereIf(input.StateIdsFilter != null && input.StateIdsFilter.Count() > 0 && input.StateIdsFilter.Contains(0), e => input.StateIdsFilter.Contains(e.StateId) || e.StateId == null)
                            .WhereIf(input.StateIdsFilter != null && input.StateIdsFilter.Count() > 0 && !input.StateIdsFilter.Contains(0), e => input.StateIdsFilter.Contains(e.StateId))
                            //.WhereIf(input.StateIdsFilter != null , e => e.StateId == null)
                            .WhereIf(input.AreasFilter != null && input.AreasFilter.Count() > 0 && !input.AreasFilter.Contains("NoArea"), e => input.AreasFilter.Contains(e.Area))
                            .WhereIf(input.AreasFilter != null && input.AreasFilter.Count() > 0 && input.AreasFilter.Contains("NoArea"), e => input.AreasFilter.Contains(e.Area) || string.IsNullOrEmpty(e.Area))

                            .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)

                            .WhereIf(input.StartDateFilter != null && input.DateType == "LeadCreated", e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDateFilter != null && input.DateType == "LeadCreated", e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                            .WhereIf(input.DateType == "ProjectOpen" || input.DateType == "InstallDate", e => Job_List.Any(j => j.LeadId == e.Id))

                            .Where(e => e.AssignToUserID != null);

            var leads = new List<int>();
            if (input.TeamIdsFilter != null && input.TeamIdsFilter.Count() > 0)
            {
                leads = await (from o in filteredLeads
                               join ut in _userTeamRepository.GetAll() on o.AssignToUserID equals ut.UserId into j1
                               from ut1 in j1.DefaultIfEmpty()
                               where (input.TeamIdsFilter == null || input.TeamIdsFilter.Contains(ut1.TeamId)) && ut1.IsDeleted == false
                               select o.Id).ToListAsync();
            }
            else
            {
                leads = await (from o in filteredLeads select o.Id).ToListAsync();
            }

            var totalCount = filteredLeads.Count();

            return leads;

            #endregion
        }

        public async Task<List<LeadUsersLookupTableDto>> GetAllTeamsForFilter()
        {
            return await _teamRepository.GetAll()
            .Select(team => new LeadUsersLookupTableDto
            {
                Id = team.Id,
                DisplayName = team == null || team.Name == null ? "" : team.Name.ToString()
            }).ToListAsync();
        }

        public async Task<List<LeadUsersLookupTableDto>> GetSalesManagerForFilter(int OId, int? TeamId)
        {

            var User_List = _userRepository
          .GetAll();
            var User = User_List.Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var Team = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);



            var UserList = new List<LeadUsersLookupTableDto>();
            if (role.Contains("Admin"))
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Sales Manager" && uo.OrganizationUnitId == OId)
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).ToList();
            }
            else
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Sales Manager" && uo != null && uo.OrganizationUnitId == OId && ut.TeamId == Team) // 
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).ToList();
            }


            return UserList;
        }

        public async Task<List<LeadUsersLookupTableDto>> GetSalesRepForFilter(int OId, int? TeamId)
        {
            var User_List = _userRepository
             .GetAll();
            var Teams =  _userTeamRepository.GetAll();
            var User = User_List.Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

            var Team = await _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).WhereIf(TeamId != 0, e => e.TeamId == TeamId).Select(e => e.TeamId).ToListAsync();
            
            //var TeamList = await _teamRepository.GetAll().Where(e => e.UserId == User.Id).WhereIf(TeamId != 0, e => e.TeamId == TeamId).Select(e => e.TeamId).ToListAsync();
            var TeamList = await _teamRepository.GetAll().WhereIf(TeamId != 0, e => e.Id == TeamId).Select(e => (int?)e.Id).ToListAsync();

            IList<string> role = await _userManager.GetRolesAsync(User);

            var UserList = new List<LeadUsersLookupTableDto>();
            if (role.Contains("Admin"))
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Sales Rep" && uo.OrganizationUnitId == OId && TeamList.Contains(ut.TeamId)) //&& ut.TeamId == TeamId
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).OrderBy(d => d.DisplayName).ToList();
            }
            else
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()
                                //where (us != null && us.DisplayName == "Sales Rep" && uo != null && uo.OrganizationUnitId == OId && ut.TeamId == TeamId) // 
                            where (us != null && us.DisplayName == "Sales Rep" && uo != null && uo.OrganizationUnitId == OId && Team.Contains(ut.TeamId)) // 
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).OrderBy(d => d.DisplayName).ToList();
            }

            return UserList;
        }

        public async Task CopyLead(int OId, int LeadId, int? SectionId)
        {
            var OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == OId).Select(e => e.DisplayName).FirstOrDefault();
            var lead = _leadRepository.GetAll().Where(e => e.Id == LeadId).FirstOrDefault();

            var CopiedOrg = _organizationUnitRepository.GetAll().Where(e => e.Id == lead.OrganizationId).Select(e => e.DisplayName).FirstOrDefault();

            lead.Id = 0;
            //1. Create
            //2. Excel Import
            //3. External Link
            //4. Copy Lead
            lead.IsExternalLead = 4;
            lead.OrganizationId = OId;
            lead.CreatorUserId = (int)AbpSession.UserId;
            await _leadRepository.InsertAndGetIdAsync(lead);

            var activity = new LeadActivityLog();
            activity.LeadId = lead.Id;
            activity.ActionId = 1;
            activity.SectionId = SectionId;
            if (AbpSession.TenantId != null)
            {
                activity.TenantId = (int)AbpSession.TenantId;
            }
            activity.ActionNote = "Lead Copy From " + CopiedOrg + " Organization To " + OrganizationName;
            await _leadactivityRepository.InsertAsync(activity);

            var log = new UserActivityLogDto();
            log.ActionId = 1;
            log.ActionNote = "Lead Copy From " + CopiedOrg + " Organization To " + OrganizationName;
            log.Section = "Manage Leads";
            await _userActivityLogServiceProxy.Create(log);

            var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == lead.CreatorUserId).FirstOrDefault();
            string msg = string.Format("New Lead Created {0}.", lead.CompanyName);
            await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
        }

        public bool CheckCopyExist(int OId, int LeadId)
        {
            var Result = false;

            var lead = _leadRepository.GetAll().Where(e => e.Id == LeadId).FirstOrDefault();
            Result = _leadRepository.GetAll().Where(e => e.IsExternalLead == 4 && e.OrganizationId == OId && e.Id != lead.Id && e.Email == lead.Email && e.Phone == lead.Phone && e.Mobile == lead.Mobile).Any();

            return Result;
        }

        public async Task<List<CommonLookupDto>> GetallEmailTemplates(int leadId)
        {
            //var organizationId = _leadRepository.GetAll().Where(e => e.Id == leadId).Select(e => e.OrganizationId).FirstOrDefault();
            var organizationId = _leadRepository.FirstOrDefault(leadId).OrganizationId;

            return await _emailTemplateRepository.GetAll().Where(e => e.OrganizationUnitId == organizationId)
            .Select(emailTemplate => new CommonLookupDto
            {
                Id = emailTemplate.Id,
                DisplayName = emailTemplate == null || emailTemplate.TemplateName == null ? "" : emailTemplate.TemplateName.ToString()
            }).ToListAsync();
        }

        public async Task<List<CommonLookupDto>> getOrgWiseDefultandownemailadd(int leadId)
        {
            var organizationId = _leadRepository.GetAll().Where(e => e.Id == leadId).Select(e => e.OrganizationId).FirstOrDefault();
            //var emailadress = new List<string?>();
            var list = new List<CommonLookupDto>();
            var ownemailadd = _UserWiseEmailOrgRepository.GetAll().Where(e => e.OrganizationUnitId == organizationId && e.UserId == AbpSession.UserId).Select(e => e.EmailFromAdress).FirstOrDefault();
            if (ownemailadd != null)
            {
                CommonLookupDto comon = new CommonLookupDto();
                comon.Id = 0;
                comon.DisplayName = ownemailadd;
                list.Add(comon);
            }
            var defultemail = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == organizationId).Select(e => e.defaultFromAddress).FirstOrDefault();

            if (defultemail != null)
            {
                CommonLookupDto comon = new CommonLookupDto();
                comon.Id = 0;
                comon.DisplayName = defultemail;
                list.Add(comon);
            }
            //EmailAddress.add(_organizationUnitRepository.GetAll(e => e.id))
            return list;
        }

        public bool checkorgwisephonedynamicavaibleornot(int leadId)
        {
            var organizationId = _leadRepository.GetAll().Where(e => e.Id == leadId).Select(e => e.OrganizationId).FirstOrDefault();
            var org = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == organizationId).FirstOrDefault();
            if (org.FoneDynamicsPropertySid != null && org.FoneDynamicsAccountSid != null && org.FoneDynamicsToken != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<List<CommonLookupDto>> GetallSMSTemplates(int leadId)
        {
            var organizationId = _leadRepository.FirstOrDefault(leadId).OrganizationId;

            return await _smsTemplateRepository.GetAll().Where(e => e.OrganizationUnitId == organizationId)
            .Select(smsTemplate => new CommonLookupDto
            {
                Id = smsTemplate.Id,
                DisplayName = smsTemplate == null || smsTemplate.Name == null ? "" : smsTemplate.Name.ToString()
            }).ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetAllLeadAction()
        {
            return await _lookup_leadActionRepository.GetAll()
            .Select(leadAction => new CommonLookupDto
            {
                Id = leadAction.Id,
                DisplayName = leadAction.ActionName
            }).ToListAsync();

            //var TeamList = (from user in _lookup_leadActionRepository.GetAll()
            //                select new LeadUsersLookupTableDto()
            //                {
            //                    Id = user.Id,
            //                    DisplayName = user.ActionName == null || user.ActionName == null ? "" : user.ActionName.ToString()
            //                }).DistinctBy(d => d.Id).ToList();





        }

        public async Task<string> GetCurrentUserRole()
        {
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            return role[0];
        }

        public async Task<string> GetCurrentUserIdName()
        {
            return _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).Select(e => e.UserName).FirstOrDefault();
        }

        public async Task<int> GetCurrentUserId()
        {
            return (int)AbpSession.UserId;
        }

        public async Task<bool> CheckValidation(CreateOrEditLeadDto input)
        {
            var output = true;

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            if (!role.Contains("Admin"))
            {
                output = (!string.IsNullOrEmpty(input.CompanyName) && !string.IsNullOrEmpty(input.Area) && !string.IsNullOrEmpty(input.Type)
                    && !string.IsNullOrEmpty(input.StreetNo) && !string.IsNullOrEmpty(input.StreetName) && !string.IsNullOrEmpty(input.StreetType)
                    && !string.IsNullOrEmpty(input.Suburb) && !string.IsNullOrEmpty(input.State) && !string.IsNullOrEmpty(input.PostCode)
                    && !string.IsNullOrEmpty(input.LeadSource) && (!string.IsNullOrEmpty(input.Email) || !string.IsNullOrEmpty(input.Mobile)));
            }
            else
            {
                output = (!string.IsNullOrEmpty(input.CompanyName) && !string.IsNullOrEmpty(input.LeadSource) && (!string.IsNullOrEmpty(input.Email) || !string.IsNullOrEmpty(input.Mobile)));
            }

            return output;
        }

        /// SalesREpBySalesManagerId
        public async Task<List<LeadUsersLookupTableDto>> GetSalesRepBySalesManagerid(int? id, int? OId)
        {
            var User = _userRepository.GetAll().Where(u => u.Id == id).FirstOrDefault();
            var Team = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var User_List = _userRepository
           .GetAll();
            var UserList = new List<LeadUsersLookupTableDto>();
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Sales Rep" && uo != null && uo.OrganizationUnitId == OId && Team.Contains(ut.TeamId)) // 
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).OrderBy(d => d.DisplayName).ToList();
            }

            return UserList;
        }

        /// login wise team display
        public async Task<List<LeadUsersLookupTableDto>> GetTeamForFilter()
        {
            var User = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var Team = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            var TeamList = new List<LeadUsersLookupTableDto>();
            if (role.Contains("Admin"))
            {
                TeamList = (from user in _teamRepository.GetAll()
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.Name == null || user.Name == null ? "" : user.Name.ToString()
                            }).DistinctBy(d => d.Id).ToList();
            }
            else
            {
                TeamList = (from team in _teamRepository.GetAll()
                            where (team.Id == Team)
                            select new LeadUsersLookupTableDto()
                            {
                                Id = team.Id,
                                DisplayName = team.Name == null || team.Name == null ? "" : team.Name.ToString()
                            }).DistinctBy(d => d.Id).ToList();
            }
            return TeamList;
        }

        /// SMS Reply Grid list
        public async Task<PagedResultDto<SmsReplyDto>> GetSmsReplyList(SmsReplyInputDto input)
        {
            int UserId = (int)AbpSession.UserId;

            var leadid = 0;

            if (input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter))
            {
                leadid = (int)_jobRepository.GetAll().Where(e => e.JobNumber == input.Filter).AsNoTracking().Select(e => e.LeadId).FirstOrDefault();
            }

            //var ids = _leadactivityRepository.GetAll().Where(e => e.ActionId == 6 && e.CreatorUserId == UserId).Select(e => e.Id).ToList();
            var ids = _leadactivityRepository.GetAll().Where(e => (e.ActionId == 6 || e.ActionNote.EndsWith("Signature Request Sent On SMS")) && e.CreatorUserId == UserId).Select(e => e.Id);

            var filteredLeads = _leadactivityRepository.GetAll()
                 .Include(e => e.LeadFk)
                 //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter))
                 .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile.Contains(input.Filter))
                 .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Phone.Contains(input.Filter))
                 .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName.Contains(input.Filter))
                 .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email.Contains(input.Filter))
                 .WhereIf(leadid > 0, e => e.LeadId == leadid)
                 //.WhereIf(input.Markstatusid != 0 && input.Markstatusid == 1, e => ids.Contains((int)e.ReferanceId) && e.IsMark == true)
                 //.WhereIf(input.Markstatusid != 0 && input.Markstatusid == 2, e => ids.Contains((int)e.ReferanceId) && e.IsMark == false)
                 //.Where(e => ids.Contains((int)e.ReferanceId) && e.LeadFk.OrganizationId == input.OrganizationUnit);
                 .WhereIf(input.Markstatusid != 0 && input.Markstatusid == 1, e => ids.Any(i => i == (int)e.ReferanceId) && e.IsMark == true)
                 .WhereIf(input.Markstatusid != 0 && input.Markstatusid == 2, e => ids.Any(i => i == (int)e.ReferanceId) && e.IsMark == false)
                 .Where(e => ids.Any(i => i == (int)e.ReferanceId) && e.LeadFk.OrganizationId == input.OrganizationUnit);

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var result = from o in pagedAndFilteredLeads
                         join o1 in _leadRepository.GetAll() on o.LeadId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _leadactivityRepository.GetAll() on o.ReferanceId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()
                         select new SmsReplyDto
                         {
                             LeadCompanyName = s1.CompanyName,
                             ResponceActionNote = o.ActionNote,
                             ResponceActionId = o.ActionId,
                             ResponceActivityDate = o.ActivityDate,
                             ResponceActivityNote = o.ActivityNote,
                             ResponceCreationTime = o.CreationTime,
                             ResponceBody = o.Body,
                             ActionNote = s2.ActionNote,
                             ActionId = s2.ActionId,
                             ActivityDate = s2.ActivityDate,
                             ActivityNote = s2.ActivityNote,
                             CreationTime = s2.CreationTime,
                             Body = s2.Body,
                             IsMark = o.IsMark,
                             id = o.Id,
                             UserName = s3.Name,
                             LeadId = o.LeadId,
                             SectionName = _SectionAppService.GetAll().Where(e => e.SectionId == o.SectionId).Select(e => e.SectionName).FirstOrDefault()
                         };


            var totalCount = await filteredLeads.CountAsync();
            //var totalCount = filteredLeads.DistinctBy(e => e.LeadId).Count();

            //return new PagedResultDto<SmsReplyDto>(totalCount, result.DistinctBy(e => e.LeadId).ToList());
            return new PagedResultDto<SmsReplyDto>(totalCount, await result.ToListAsync());
        }

        public async Task<List<SmsReplyDto>> GetOldSmsReplyList(int? Id, int? LeadId, int orgId)
        {
            int UserId = (int)AbpSession.UserId;
            //var ids = _leadactivityRepository.GetAll().Where(e => e.ActionId == 6 && e.CreatorUserId == UserId && e.LeadId == LeadId).Select(e => e.Id).ToList();
            var ids = _leadactivityRepository.GetAll().Where(e => (e.ActionId == 6 || e.ActionNote.EndsWith("Signature Request Sent On SMS")) && e.CreatorUserId == UserId && e.LeadId == LeadId).Select(e => e.Id).ToList();

            var filteredLeads = _leadactivityRepository.GetAll()
                 .Include(e => e.LeadFk)
                 .Where(e => ids.Contains((int)e.ReferanceId) && e.LeadFk.OrganizationId == orgId && e.LeadId == LeadId);

            var pagedAndFilteredLeads = filteredLeads.OrderByDescending(e => e.Id);
            var result = new List<SmsReplyDto>();

            result = (from o in pagedAndFilteredLeads
                      join o1 in _leadRepository.GetAll() on o.LeadId equals o1.Id into j1
                      from s1 in j1.DefaultIfEmpty()

                      join o2 in _leadactivityRepository.GetAll() on o.ReferanceId equals o2.Id into j2
                      from s2 in j2.DefaultIfEmpty()

                      join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                      from s3 in j3.DefaultIfEmpty()

                      select new SmsReplyDto
                      {
                          LeadCompanyName = s1.CompanyName,
                          ResponceActionNote = o.ActionNote,
                          ResponceActionId = o.ActionId,
                          ResponceActivityDate = o.ActivityDate,
                          ResponceActivityNote = o.ActivityNote,
                          ResponceCreationTime = o.CreationTime,
                          ResponceBody = o.Body,
                          ActionNote = s2.ActionNote,
                          ActionId = s2.ActionId,
                          ActivityDate = s2.ActivityDate,
                          ActivityNote = s2.ActivityNote,
                          CreationTime = s2.CreationTime,
                          //Body = o.ActivityNote,
                          Body = s2.ActionNote.EndsWith("Signature Request Sent On SMS") ? s2.ActionNote : s2.Body,
                          IsMark = o.IsMark,
                          id = o.Id,
                          UserName = s3.Name,
                          LeadId = o.LeadId,

                          SectionName = _SectionAppService.GetAll().Where(e => e.SectionId == o.SectionId).Select(e => e.SectionName).FirstOrDefault()

                      }).ToList();
            return result;
        }
        /// Email Reply Grid list
        public async Task<PagedResultDto<EmailReplyDto>> GetEmailReplyList(EmailReplyInputDto input)
        {
            int UserId = (int)AbpSession.UserId;
            //     var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == UserId).Select(e => e.TeamId).ToList();
            //     var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //     var User_List = _userRepository
            //.GetAll();
            //     var RoleName = (from user in User_List
            //                     join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
            //                     from ur in urJoined.DefaultIfEmpty()
            //                     join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
            //                     from us in usJoined.DefaultIfEmpty()
            //                     where (us != null && user.Id == UserId)
            //                     select (us.DisplayName));


            //     var leadids = new List<int>();
            //     if (RoleName.Contains("Sales Manager"))
            //     {
            //         leadids = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationUnit && UserList.Contains(e.AssignToUserID)).Select(e => e.Id).ToList();
            //     }
            //     else if (RoleName.Contains("Sales Rep"))
            //     {
            //         leadids = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationUnit && e.AssignToUserID == UserId).Select(e => e.Id).ToList();
            //     }
            //     else
            //     {

            //         leadids = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationUnit).Select(e => e.Id).ToList();
            //     }
            //var leadids = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationUnit).Select(e => e.Id).ToList();

            var filteredLeads = _leadactivityRepository.GetAll()
                                .Include(e => e.LeadFk)

                                   //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter))
                                   .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile.Contains(input.Filter))
                                   .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Phone.Contains(input.Filter))
                                   .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName.Contains(input.Filter))
                                   .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email.Contains(input.Filter))
                                ///.WhereIf(input.OrganizationUnit != null, e => leadids.Contains(e.LeadId))
                                .Where(e => e.ActionId == 7 && e.CreatorUserId == UserId && e.LeadFk.OrganizationId == input.OrganizationUnit);


            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var result = from o in pagedAndFilteredLeads
                         join o1 in _leadRepository.GetAll() on o.LeadId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _emailTemplateRepository.GetAll() on o.TemplateId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         select new EmailReplyDto
                         {
                             LeadCompanyName = s1.CompanyName,
                             ActionNote = o.ActionNote,
                             ActionId = o.ActionId,
                             ActivityDate = o.ActivityDate,
                             ActivityNote = o.ActivityNote,
                             CreationTime = o.CreationTime,
                             Body = o.Body,
                             Id = o.Id,
                             TemplateName = s2.TemplateName == null ? "Custome Template" : s2.TemplateName,
                             UserName = s3.Name,
                             Subject = o.Subject,
                             TemplateId = o.TemplateId,
                             LeadId = o.LeadId
                         };


            var totalCount = filteredLeads.Count();

            return new PagedResultDto<EmailReplyDto>(
                 totalCount, result.DistinctBy(e => e.LeadId).ToList()
             );
        }

        public async Task<List<EmailReplyDto>> GetOldEmailReplyList(int? LeadId, int orgId)
        {
            int UserId = (int)AbpSession.UserId;
            var filteredLeads = _leadactivityRepository.GetAll()
                 .Include(e => e.LeadFk)
                 .Where(e => e.LeadFk.OrganizationId == orgId && e.LeadId == LeadId && e.CreatorUserId == UserId && e.ActionId == 7);

            var pagedAndFilteredLeads = filteredLeads.OrderByDescending(e => e.Id);

            var result = new List<EmailReplyDto>();

            result = (from o in pagedAndFilteredLeads
                      join o1 in _leadRepository.GetAll() on o.LeadId equals o1.Id into j1
                      from s1 in j1.DefaultIfEmpty()

                      join o2 in _emailTemplateRepository.GetAll() on o.TemplateId equals o2.Id into j2
                      from s2 in j2.DefaultIfEmpty()

                      join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                      from s3 in j3.DefaultIfEmpty()

                      select new EmailReplyDto
                      {
                          LeadCompanyName = s1.CompanyName,
                          ActionNote = o.ActionNote,
                          ActionId = o.ActionId,
                          ActivityDate = o.ActivityDate,
                          ActivityNote = o.ActivityNote,
                          CreationTime = o.CreationTime,
                          Body = o.Body,
                          Id = o.Id,
                          TemplateName = s2.TemplateName == null ? "Custome Template" : s2.TemplateName,
                          UserName = s3.Name,
                          Subject = o.Subject,
                          TemplateId = o.TemplateId == null ? 0 : o.TemplateId,
                          Email = o.LeadFk.Email,
                          LeadId = o.LeadId
                      }).ToList();
            return result;
        }

        /// mark same as read api 
        /// id use for activity id.. 0 means not read 1 means mark as read
        public async Task MarkReadSms(int id)
        {

            var leadactivity = await _leadactivityRepository.FirstOrDefaultAsync((int)id);
            leadactivity.IsMark = true;
            await _leadactivityRepository.UpdateAsync(leadactivity);

        }

        public async Task MarkReadSmsInBulk(int Readorunreadtag, List<int> ids)
        {
            foreach (var item in ids)
            {
                var activityListIds = _leadactivityRepository.GetAll().Where(e => e.LeadId == (int)item).Select(e => e.Id).ToList();

                foreach (var item1 in activityListIds)
                {
                    var leadactivity = await _leadactivityRepository.FirstOrDefaultAsync((int)item1);
                    if (Readorunreadtag == 1)
                    {
                        leadactivity.IsMark = true;
                    }
                    else
                    {
                        leadactivity.IsMark = false;
                    }
                    await _leadactivityRepository.UpdateAsync(leadactivity);
                }
            }
        }

        /// get leadactivitystatus by id
        /// id use for activity id.. 

        public async Task<SmsReplyDto> GetLeadActivitybyID(int id)
        {
            var output = new SmsReplyDto();

            var ids = _leadactivityRepository.GetAll().Where(L => L.Id == id).Select(e => e.ReferanceId).FirstOrDefault();
            output.ResponceBody = _leadactivityRepository.GetAll().Where(L => L.Id == id).Select(e => e.Body).FirstOrDefault();
            output.Body = _leadactivityRepository.GetAll().Where(e => e.Id == ids).Select(e => e.Body).FirstOrDefault();
            output.TemplateId = _leadactivityRepository.GetAll().Where(e => e.Id == id).Select(e => e.TemplateId).FirstOrDefault();
            return output;
        }

        //public async Task<List<GetActivityLogViewDto>> GetLeadActivitybyID(int id)
        //{
        //	return await _leadactivityRepository.GetAll().Where(L => L.Id == id)
        //		.Select(users => new GetActivityLogViewDto
        //		{
        //			ActionId = users.ActionId,
        //			ActionNote = users.ActionNote,
        //			LeadId = users.LeadId,
        //			CreationTime = users.CreationTime,
        //			Body = users.Body
        //		}).ToListAsync();
        //
        /// <summary>
        /// Get UnRead SMS Count
        /// </summary>
        /// <returns></returns>
        public async Task<int> GetUnReadSMSCount()
        {
            #region Old Code
            //var result = 0;

            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            //var orgid = _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == AbpSession.UserId).Select(e => e.OrganizationUnitId).FirstOrDefault();

            //var leadids = new List<int>();
            //if (role.Contains("Sales Manager"))
            //{
            //    leadids = _leadRepository.GetAll().Where(e => UserList.Contains(e.AssignToUserID) && e.OrganizationId == orgid).Select(e => e.Id).ToList();
            //}
            //else if (role.Contains("Sales Rep"))
            //{
            //    leadids = _leadRepository.GetAll().Where(e => e.AssignToUserID == User.Id && e.OrganizationId == orgid).Select(e => e.Id).ToList();
            //}
            //else
            //{
            //    leadids = _leadRepository.GetAll().Where(e => e.AssignToUserID != null && e.OrganizationId == orgid).Select(e => e.Id).ToList();
            //}

            //result = _leadactivityRepository.GetAll().Where(e => leadids.Contains(e.LeadId) && e.IsMark == false && e.ActionId == 20 && e.CreatorUserId == AbpSession.UserId).Count();

            //return result;
            #endregion

            #region New Code on 05-08-2022
            var result = 0;

            var orgids = _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == AbpSession.UserId).Select(e => (long)e.OrganizationUnitId).ToList();
            var ids = _leadactivityRepository.GetAll().Where(e => (e.ActionId == 6 || e.ActionNote.EndsWith("Signature Request Sent On SMS")) && e.CreatorUserId == AbpSession.UserId).Select(e => e.Id).ToList();

            //result = await _leadactivityRepository.GetAll().Where(e => e.IsMark == false && e.ActionId == 20 && e.CreatorUserId == AbpSession.UserId && orgids.Contains(e.LeadFk.OrganizationId)).CountAsync();
            //result = await _leadactivityRepository.GetAll().Where(e => e.IsMark == false && e.ActionId == 6 && e.CreatorUserId == AbpSession.UserId && orgids.Contains(e.LeadFk.OrganizationId)).CountAsync();
            result = _leadactivityRepository.GetAll()
                .Include(e => e.LeadFk)
                .Where(e => ids.Contains((int)e.ReferanceId) && e.IsMark == false && orgids.Contains(e.LeadFk.OrganizationId)).DistinctBy(e => e.LeadId).Count();

            return result;
            #endregion
        }

        public async Task<int> GetUnReadPromotionCount()
        {
            #region Old Code
            //var result = 0;
            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            //var orgid = _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == AbpSession.UserId).Select(e => e.OrganizationUnitId).FirstOrDefault();

            //var leadids = new List<int>();
            //if (role.Contains("Sales Manager"))
            //{
            //    leadids = _leadRepository.GetAll().Where(e => UserList.Contains(e.AssignToUserID) && e.OrganizationId == orgid).Select(e => e.Id).ToList();
            //}
            //else if (role.Contains("Sales Rep"))
            //{
            //    leadids = _leadRepository.GetAll().Where(e => e.AssignToUserID == User.Id && e.OrganizationId == orgid).Select(e => e.Id).ToList();
            //}
            //else
            //{
            //    leadids = _leadRepository.GetAll().Where(e => e.AssignToUserID != null && e.OrganizationId == orgid).Select(e => e.Id).ToList();
            //}

            //result = _leadactivityRepository.GetAll().Where(e => leadids.Contains(e.LeadId) && e.IsMark == false && e.ActionId == 22).Count();

            //return result;
            #endregion

            #region New Code on 05-08-2022
            var result = 0;

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault(); //Get Current User
            IList<string> role = await _userManager.GetRolesAsync(User); // Get Current User Role
            var orgids = _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == AbpSession.UserId).Select(e => (long)e.OrganizationUnitId).ToList();

            if (role.Contains("Admin"))
            {
                //result = await _leadactivityRepository.GetAll().Where(e => e.IsMark == false && e.ActionId == 22).CountAsync();
                result = await _leadactivityRepository.CountAsync(e => e.IsMark == false && e.ActionId == 22 && e.LeadFk.AssignToUserID != null);
            }
            else if (role.Contains("Sales Manager"))
            {
                //var orgids = _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == AbpSession.UserId).Select(e => (long)e.OrganizationUnitId).ToList();

                var leadids = new List<int>();
                var UserTeam = _userTeamRepository.GetAll();
                var TeamId = UserTeam.Where(e => e.UserId == User.Id).Select(e => e.TeamId).Distinct().ToList();
                var UserList = UserTeam.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

                //leadids = _leadRepository.GetAll().Where(e => UserList.Contains(e.AssignToUserID) && orgids.Contains(e.OrganizationId)).Select(e => e.Id).ToList();

                result = await _leadactivityRepository.CountAsync(e => e.IsMark == false && e.ActionId == 22 && UserList.Contains(e.LeadFk.AssignToUserID) && orgids.Contains(e.LeadFk.OrganizationId));
            }
            else if (role.Contains("Sales Rep"))
            {
                //result = _leadactivityRepository.GetAll().Where(e => leadids.Contains(e.LeadId) && e.IsMark == false && e.ActionId == 22).Count();

                //var leadids = _leadRepository.GetAll().Where(e => e.AssignToUserID == User.Id).Select(e => e.Id).ToList();
                //result = await _leadactivityRepository.CountAsync(e => e.IsMark == false && e.ActionId == 22 && leadids.Contains(e.LeadId));
                result = await _leadactivityRepository.CountAsync(e => e.IsMark == false && e.ActionId == 22 && e.LeadFk.AssignToUserID == User.Id);
            }
            //var leadactive_list = _leadactivityRepository.GetAll().Where(e => orgids.Contains(e.LeadFk.OrganizationId));
            //var unreadLedActivityids = leadactive_list.Where(e => e.ActionId == 22 && e.IsMark == false).Select(e => e.ReferanceId).Distinct().ToList();
            //result = leadactive_list.Where(e => unreadLedActivityids.Contains(e.Id)).Select(e => e.PromotionUserId).Distinct().ToList().Count();
            return result;
            #endregion
        }

        public async Task<PagedResultDto<GetLeadForViewDto>> GetMyReminderData(GetAllLeadsInput input)
        {
            int UserId = 9; // (int)AbpSession.UserId;

            var List = _leadactivityRepository.GetAll().Where(e => e.ActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).ToList();
            var NextFollowupList = List.Where(e => e.ActivityDate.Value.AddHours(10).Date >= input.StartDate.Value.Date && e.ActivityDate.Value.AddHours(10).Date <= input.EndDate.Value.Date).Select(e => e.LeadId);

            var filteredLeads = _leadRepository.GetAll()
                       .WhereIf(input.DateFilterType == "NextFollowup", e => NextFollowupList.Contains(e.Id))
                       .Where(e => e.AssignToUserID == UserId);

            var leads = (from o in filteredLeads
                         select new GetLeadForViewDto()
                         {
                             Lead = new LeadDto
                             {
                                 CompanyName = o.CompanyName,
                             },
                             LastComment = _leadactivityRepository.GetAll().Where(e => e.LeadId == o.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                             LastCommentDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == o.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),

                         });

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetLeadForViewDto>(
                totalCount,
                leads.DistinctBy(e => e.Lead.Id).ToList()
            );
        }

        public string GetareaBysuburbPostandstate(string suburb, string state, string postCode)
        {
            int stateid = _lookup_stateRepository.GetAll().Where(e => e.Name == state).Select(e => e.Id).FirstOrDefault();
            var Result = _postCodeRepository.GetAll().Where(e => e.Suburb == suburb.ToUpper() && e.StateId == stateid && e.PostalCode == postCode.ToUpper()).Select(e => e.Areas).FirstOrDefault();

            return Result;
        }

        /// ReAssign Cancel or Reject Lead api 
        /// id use for Leadid
        public async Task ReAssignCancelOrRejectLead(int leadid)
        {

            var lead = await _leadRepository.FirstOrDefaultAsync((int)leadid);
            PreviousJobStatus LeadStstus = new PreviousJobStatus();
            LeadStstus.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                LeadStstus.TenantId = (int)AbpSession.TenantId;
            }
            LeadStstus.CurrentID = 11;
            LeadStstus.PreviousId = lead.LeadStatusId;
            await _previousJobStatusRepository.InsertAsync(LeadStstus);
            lead.LeadStatusId = 11;
            await _leadRepository.UpdateAsync(lead);

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 11;
            leadactivity.SectionId = 0;
            leadactivity.ActionNote = "Lead Reassign";
            leadactivity.LeadId = (int)leadid;
            leadactivity.ActivityDate = DateTime.UtcNow;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);

        }

        public async Task<List<LeadUsersLookupTableDto>> GetUserByTeamId(int? teamid)
        {
            if (teamid != 0)
            {
                var UserList = new List<LeadUsersLookupTableDto>();
                {
                    UserList = (from user in _userRepository.GetAll()
                                join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                                from ut in utJoined.DefaultIfEmpty()
                                where (ut.TeamId == teamid)
                                select new LeadUsersLookupTableDto()
                                {
                                    Id = user.Id,
                                    DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                                }).DistinctBy(d => d.Id).ToList();
                }
                return UserList;
            }
            else
            {
                var UserList = new List<LeadUsersLookupTableDto>();
                {
                    UserList = (from user in _userRepository.GetAll()
                                select new LeadUsersLookupTableDto()
                                {
                                    Id = user.Id,
                                    DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                                }).DistinctBy(d => d.Id).ToList();
                }
                return UserList;
            }
        }

        public async Task UpdateIsMark()
        {
            var data = _leadactivityRepository.GetAll().Where(e => e.ActionId == 22 && e.IsMark != true).Select(e => e.Id).ToList().Take(10);

            foreach (var item in data)
            {
                var listdata = _leadactivityRepository.GetAll().Where(e => e.Id == item).FirstOrDefault();
                listdata.IsMark = true;
                await _leadactivityRepository.UpdateAsync(listdata);
            }
        }

        public async Task UpdateLeadActivityDetailForTodo(CreateEditActivityLogDto input)
        {
            var lead = await _leadactivityRepository.FirstOrDefaultAsync((int)input.Id);
            var email = _userRepository.GetAll().Where(e => e.Id == lead.CreatorUserId).Select(e => e.EmailAddress).FirstOrDefault();
            var user = _userRepository.GetAll().Where(e => e.Id == lead.CreatorUserId).FirstOrDefault();
            var logginuser = _userRepository.GetAll().Where(u => u.Id == lead.ReferanceId).Select(e => e.UserName).FirstOrDefault();
            MailMessage mail = new MailMessage
            {
                From = new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                To = { email },
                Subject = "Task Responce",
                Body = input.Body,
                IsBodyHtml = true
            };
            await this._emailSender.SendAsync(mail);
            string msg = string.Format("Task Responce" + input.Body + "by" + logginuser);
            await _appNotifier.LeadAssiged(user, msg, NotificationSeverity.Info);
            lead.Body = input.Body;
            lead.IsMark = true;
            await _leadactivityRepository.UpdateAsync(lead);
        }

        public async Task<List<LeadExpenseAddDto>> getAllexpenseTable()
        {

            var entity = _lookup_stateRepository.GetAll().Where(e => e.IsReport == true).OrderBy(p => p.Id).ToList();
            var activity = _lookup_leadSourceRepository.GetAll().OrderBy(p => p.Id).ToList();


            DataTable dt = new DataTable();
            DataRow dr;
            dt.Columns.Add("LeadSource");
            dt.Columns.Add("LeadSourceId");

            for (int i = 0; i < entity.Count; i++)
            {
                dt.Columns.Add(entity[i].Name);
                dt.Columns.Add(entity[i].Name + "," + entity[i].Id);
            }

            foreach (var item in activity)
            {
                dr = dt.NewRow();

                dr["LeadSource"] = item.Name;
                dr["LeadSourceId"] = item.Id;
                for (int i = 0; i < entity.Count; i++)
                {
                    var entityid = entity[i].Id;
                    dr[entity[i].Name] = "0";
                    dr[entity[i].Name + "," + entity[i].Id] = entity[i].Id.ToString();
                }
                dt.Rows.Add(dr);
            }

            List<LeadExpenseAddDto> leadlist = new List<LeadExpenseAddDto>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                LeadExpenseAddDto statewiselist = new LeadExpenseAddDto();
                statewiselist.LeadSourceId = Convert.ToInt32(dt.Rows[i]["LeadSourceId"]);
                statewiselist.Leadsource = dt.Rows[i]["LeadSource"].ToString();
                List<StateForLeadExpenseDto> Statelist = new List<StateForLeadExpenseDto>();
                for (int j = 2; j < dt.Columns.Count - 1; j++)
                {
                    StateForLeadExpenseDto st = new StateForLeadExpenseDto();
                    st.Amount = Convert.ToDecimal(dt.Rows[i][j]);
                    st.stateid = Convert.ToInt32(dt.Rows[i][j + 1]);
                    j++;
                    Statelist.Add(st);
                }
                statewiselist.leadstates = Statelist;
                leadlist.Add(statewiselist);
            }
            return leadlist;
        }

        public async Task<List<LeadExpenseAddDto>> getAllexpenseTableById()
        {
            List<LeadExpenseAddDto> leadlist = new List<LeadExpenseAddDto>();

            return leadlist;
        }

        public async Task AsssignOrTransferLeads(AssignOrTransferLeadInput input)
        {
            var ChangedOrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationID).Select(e => e.DisplayName).FirstOrDefault();
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == input.AssignToUserID).FirstOrDefault();

            foreach (var item in input.LeadIds)
            {
                var lead = await _leadRepository.FirstOrDefaultAsync((int)item);

                var ActionNote = "";
                var ActionId = 0;

                var assignedFromUser = _userRepository.GetAll().Include(e => e.Roles).Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
                var PreviousOrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == lead.OrganizationId).Select(e => e.DisplayName).FirstOrDefault();

                IList<string> RoleName = await _userManager.GetRolesAsync(assignedFromUser);

                if (input.LeadActionId == 1) // Assign To Sales Manager 
                {
                    if (lead.OrganizationId != input.OrganizationID)
                    {
                        ActionNote = "Lead " + (RoleName.Contains("Sales Rep") ? "ReAssign" : "Assign") + " To Sales Manager " + assignedToUser.FullName + " In " + ChangedOrganizationName + " Organization From " + assignedFromUser.FullName + ", " + PreviousOrganizationName + " Organization";
                    }
                    else
                    {
                        ActionNote = "Lead " + (RoleName.Contains("Sales Rep") ? "ReAssign" : "Assign") + " To Sales Manager " + assignedToUser.FullName + " From " + assignedFromUser.FullName;
                    }

                    if (RoleName.Contains("Sales Rep"))
                    {
                        ActionId = 35;
                        lead.ReAssignDate = DateTime.UtcNow;
                    }
                    else
                    {
                        ActionId = 3;
                        lead.AssignDate = DateTime.UtcNow;
                    }

                }
                else if (input.LeadActionId == 2) // Assign To Sales Rep
                {
                    if (lead.OrganizationId != input.OrganizationID)
                    {
                        ActionNote = "Lead " + (RoleName.Contains("Sales Rep") ? "ReAssign" : "Assign") + " To Sales Rep " + assignedToUser.FullName + " In " + ChangedOrganizationName + " Organization From " + assignedFromUser.FullName + ", " + PreviousOrganizationName + " Organization";
                    }
                    else
                    {
                        ActionNote = "Lead " + (RoleName.Contains("Sales Rep") ? "ReAssign" : "Assign") + " To Sales Rep " + assignedToUser.FullName + " From " + assignedFromUser.FullName;
                    }

                    if (RoleName.Contains("Sales Rep"))
                    {
                        ActionId = 35;
                        lead.ReAssignDate = DateTime.UtcNow;
                    }
                    else
                    {
                        ActionId = 3;
                        lead.AssignDate = DateTime.UtcNow;

                        if (lead.FirstAssignUserId == null)
                        {
                            lead.FirstAssignDate = DateTime.UtcNow;
                            lead.FirstAssignUserId = input.AssignToUserID;
                        }
                    }
                }
                else if (input.LeadActionId == 3) // Assign To Users
                {
                    if (lead.OrganizationId != input.OrganizationID)
                    {
                        ActionNote = "Lead " + (RoleName.Contains("Sales Rep") ? "ReAssign" : "Assign") + " To Users " + assignedToUser.FullName + " In " + ChangedOrganizationName + " Organization From " + assignedFromUser.FullName + ", " + PreviousOrganizationName + " Organization";
                    }
                    else
                    {
                        ActionNote = "Lead " + (RoleName.Contains("Sales Rep") ? "ReAssign" : "Assign") + " To Users " + assignedToUser.FullName + " From " + assignedFromUser.FullName;
                    }

                    if (RoleName.Contains("Sales Rep"))
                    {
                        ActionId = 35;
                        lead.ReAssignDate = DateTime.UtcNow;
                    }
                    else
                    {
                        ActionId = 3;
                        lead.AssignDate = DateTime.UtcNow;
                    }
                }
                else if (input.LeadActionId == 4) // Transfer To Lead Gen
                {
                    ActionId = 36;
                    if (lead.OrganizationId != input.OrganizationID)
                    {
                        ActionNote = "Lead Transfer To Lead Gen Manager " + assignedToUser.FullName + " In " + ChangedOrganizationName + " Organization From " + assignedFromUser.FullName + ", " + PreviousOrganizationName + " Organization";
                    }
                    else
                    {
                        ActionNote = "Lead Transfer To Lead Gen Manager " + assignedToUser.FullName + " From " + assignedFromUser.FullName;
                    }
                    lead.LeadGenTransferDate = DateTime.UtcNow;
                }

                lead.AssignToUserID = input.AssignToUserID;
                lead.OrganizationId = (int)input.OrganizationID;
                await _leadRepository.UpdateAsync(lead);

                var strmsg = input.LeadActionId == 4 ? "Lead transfer To You" : "Lead Assigned To You";
                string msg = string.Format("{0} " + strmsg, input.LeadIds.Count);
                await _appNotifier.LeadAssiged(assignedToUser, msg, NotificationSeverity.Info);

                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = ActionId;
                leadactivity.SectionId = input.SectionId;
                leadactivity.ActionNote = ActionNote;
                leadactivity.LeadId = lead.Id;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                leadactivity.CreatorUserId = AbpSession.UserId;
                await _leadactivityRepository.InsertAsync(leadactivity);

                var log = new UserActivityLogDto();
                log.ActionId = ActionId;
                log.ActionNote = ActionNote;
                log.Section = input.Section;
                await _userActivityLogServiceProxy.Create(log);
            }

        }

        public async Task UpdateFacebookLead()
        {
            var fbLeadSource = new List<int?> { 4, 18, 24, 26 };

            var fbLeads = _leadRepository.GetAll().Where(e => e.LeadStatusId == 1 && e.AssignToUserID == null && e.HideDublicate == null && e.IsWebDuplicate == null && e.IsDuplicate == null && fbLeadSource.Contains(e.LeadSourceId)).ToList();

            if (fbLeads != null)
            {
                var IdList = fbLeads.Select(e => e.Id).ToList();
                foreach (var item in fbLeads)
                {
                    //Removing Special Char from Mobile Number
                    item.Mobile = Regex.Replace(item.Mobile, "[^0-9]", "");

                    if (item.Mobile.Length < 10 && item.Mobile.Length != 0)
                    {
                        item.Mobile = item.Mobile.ToString().PadLeft(10, '0');
                    }
                    else if (item.Mobile.Length > 9 && item.Mobile.Length != 0)
                    {
                        var m = item.Mobile.Substring(item.Mobile.Length - 9);
                        item.Mobile = m.ToString().PadLeft(10, '0');
                    }
                    else
                    {
                        item.Mobile = item.Mobile;
                    }

                    //Start Check Duplicate
                    //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
                    var dbDup = _leadRepository.GetAll().Where(e => !IdList.Contains(e.Id) && e.OrganizationId == item.OrganizationId && e.AssignToUserID != null && ((e.Email == item.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == item.Mobile && !string.IsNullOrEmpty(e.Mobile)))).FirstOrDefault();
                    if (dbDup != null)
                    {
                        var dbDupLeads = await _leadRepository.GetAll().Where(e => e.OrganizationId == item.OrganizationId && e.AssignToUserID != null && ((e.Email == item.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == item.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Select(e => e.Id).ToListAsync();

                        var dbDupLeadCount = dbDupLeads.Count();
                        var dbDupLeadInstallCount = await _jobRepository.GetAll().CountAsync(e => e.JobStatusId == 8 && dbDupLeads.Contains((int)e.LeadId));

                        if (dbDupLeadCount != dbDupLeadInstallCount)
                        {
                            item.IsDuplicate = true;
                            item.DublicateLeadId = dbDup.Id;
                        }
                        else
                        {
                            item.IsDuplicate = false;
                        }
                    }
                    else
                    {
                        item.IsDuplicate = false;
                    }

                    //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                    var webDup = _leadRepository.GetAll().Where(e => !IdList.Contains(e.Id) && e.OrganizationId == item.OrganizationId && e.AssignToUserID == null && e.HideDublicate != true && ((e.Email == item.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == item.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Any();
                    if (webDup)
                    {
                        item.IsWebDuplicate = true;
                    }
                    else
                    {
                        item.IsWebDuplicate = false;
                    }
                    //End Check Duplicate

                    await _leadRepository.UpdateAsync(item);
                    IdList.Remove(item.Id);
                }
                
                var log = new UserActivityLogDto();
                log.ActionId = 82;
                log.ActionNote = fbLeads.Count() + " Update Facebook Lead";
                log.Section = "Manage Leads";
                await _userActivityLogServiceProxy.Create(log);
            }

            
        }

        public virtual async Task UpdateFakeLeadFlag(GetLeadForChangeFakeLeadStatusInput input)
        {
            foreach (var item in input.LeadId)
            {
                var lead = await _leadRepository.FirstOrDefaultAsync((int)item);

                var FakeLead = _leadRepository.GetAll().Where(e => e.OrganizationId == lead.OrganizationId && e.AssignToUserID == null && e.IsFakeLead != true).Select(e => e.Id);
                //var FakeLead = _leadRepository.GetAll().Where(e => e.OrganizationId == lead.OrganizationId && e.AssignToUserID == null && e.IsFakeLead != true && ((e.Email == lead.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == lead.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Select(e => e.Id);

                if (FakeLead.Count() > 1)
                {
                    var Id = item;
                    var leadT = await _leadRepository.FirstOrDefaultAsync((int)Id);
                    leadT.IsFakeLead = input.IsFakeLead;
                    await _leadRepository.UpdateAsync(leadT);

                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 5;
                    leadactivity.ActionNote = "Lead Status Changed To Fake Lead";

                    leadactivity.LeadId = (int)Id;
                    leadactivity.SectionId = input.SectionId;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }

            }
            var log = new UserActivityLogDto();
            log.ActionId = 5;
            log.ActionNote = input.LeadId.Count() + " Lead Status Changed To Fake Lead";
            log.Section = "Manage Leads";
            await _userActivityLogServiceProxy.Create(log);
        }

        public async Task<PagedResultDto<GetLeadForViewDto>> Get3rdPartyLeadTrackerData(GetAll3rdPartyLeadsInput input)
        {
            #region New Code on 04/08/2022
            try
            {
                #region Old Code Comment On 04-08-2023 For Testing
                //var _oldCodeTimer = Stopwatch.StartNew();
                //int UserId = (int)AbpSession.UserId;
                //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

                //var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

                //var User_List = _userRepository.GetAll();
                //var user_teamList = _userTeamRepository.GetAll();

                //var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.LeadFk.IsDuplicate != true && e.LeadFk.IsWebDuplicate != true);

                //var job_list = _jobRepository.GetAll();

                //var TeamId = user_teamList.Where(e => e.UserId == UserId).Select(e => e.TeamId).ToList();
                //var UserList = user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

                //var User = User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
                //IList<string> RoleName = await _userManager.GetRolesAsync(User);

                //var userName = await UserManager.GetUserNameAsync(User);

                //var jobnumberlist = new List<int?>();
                //if (input.Filter != null)
                //{
                //    jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
                //}
                //var FilterTeamList = new List<long?>();
                //if (input.TeamId != null && input.TeamId != 0)
                //{
                //    FilterTeamList = user_teamList.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
                //}

                //var FilterManagerList = new List<long?>();
                //if (input.TeamId != null && input.TeamId != 0)
                //{
                //    var ManagerTeamId = user_teamList.Where(e => TeamId.Contains(input.SalesManagerId)).Select(e => e.TeamId).ToList();
                //    FilterManagerList = user_teamList.Where(e => ManagerTeamId.Contains(input.SalesManagerId)).Select(e => e.UserId).ToList();
                //}

                //var CancelreqListLeadid = new List<int?>();
                //if (input.Cancelrequestfilter != 0)
                //{
                //    CancelreqListLeadid = job_list.Where(e => e.IsJobCancelRequest == true).Select(e => e.LeadId).Distinct().ToList();
                //}

                //int Status = 0;
                //if (input.JobStatusID != null && input.JobStatusID.Count() > 0 && input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0)
                //{
                //    if (input.LeadStatusIDS.Contains(6))
                //    {
                //        Status = 1;
                //    }
                //}

                //var FollowupList = new List<int>();
                //if (input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null)
                //{
                //    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date >= SDate.Value.Date && x.CreationTime.Date <= EDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
                //}
                //else if (input.DateFilterType == "Followup" && input.StartDate != null)
                //{
                //    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date >= SDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
                //}
                //else if (input.DateFilterType == "Followup" && input.EndDate != null)
                //{
                //    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date <= EDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
                //}

                //var leadSource = _lookup_leadSourceRepository.GetAll();

                //var filteredLeads = _leadRepository.GetAll()
                //           .Include(e => e.LeadStatusFk)
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => input.StateNameFilter.Contains(e.State) && !string.IsNullOrEmpty(e.State))
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter)
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                //           .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadStatusId))
                //           .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                //           .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)

                //           .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                //           .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                //           .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                //           .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                //           .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)

                //           .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                //           .WhereIf(Status == 1, e => job_list.Where(x => input.JobStatusID.Contains((int)x.JobStatusId) && x.LeadId == e.Id).Any())
                //           .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.AssignToUserID))
                //           .WhereIf(input.Cancelrequestfilter != 0, e => CancelreqListLeadid.Contains(e.Id))

                //           .WhereIf(input.DateFilterType == "Assign" && input.StartDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                //           .WhereIf(input.DateFilterType == "Assign" && input.EndDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                //           .WhereIf(input.DateFilterType == "ReAssign" && input.StartDate != null, e => e.LeadAssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                //           .WhereIf(input.DateFilterType == "ReAssign" && input.EndDate != null, e => e.LeadAssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                //           .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                //           .WhereIf(input.DateFilterType == "Creation" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                //           .WhereIf(input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))

                //           .WhereIf(input.DateFilterType == "NextFollowup" && input.StartDate != null, e => e.ActivityDate.Value.Date >= SDate.Value.Date)
                //           .WhereIf(input.DateFilterType == "NextFollowup" && input.EndDate != null, e => e.ActivityDate.Value.Date <= EDate.Value.Date)

                //           .Where(e => e.HideDublicate != true);

                //var pagedAndFilteredLeads = filteredLeads
                //    .OrderBy(input.Sorting ?? "id desc")
                //    .PageBy(input);

                //var summaryCount = new LeadsSummaryCount();
                //summaryCount.New = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 1).Count());
                //summaryCount.UnHandled = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 2).Count());
                //summaryCount.Hot = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 5).Count());
                //summaryCount.Cold = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 3).Count());
                //summaryCount.Warm = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 4).Count());
                //summaryCount.Upgrade = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 6).Count());
                //summaryCount.Rejected = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 7).Count());
                //summaryCount.Cancelled = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 8).Count());
                //summaryCount.Closed = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 9).Count());
                //summaryCount.Total = Convert.ToString(filteredLeads.Count());
                //summaryCount.Assigned = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 10).Count());
                //summaryCount.ReAssigned = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 11).Count());
                //summaryCount.Sold = Convert.ToString(filteredLeads.Where(e => job_list.Where(x => x.FirstDepositDate != null && x.LeadId == e.Id).Any()).Count());

                //var leads = (from o in pagedAndFilteredLeads

                //             join o1 in _lookup_rejectReasonRepository.GetAll() on o.RejectReasonId equals o1.Id into j1
                //             from s1 in j1.DefaultIfEmpty()

                //             join o2 in _lookup_cancelReasonRepository.GetAll() on o.LeadStatusId equals o2.Id into j2
                //             from s2 in j2.DefaultIfEmpty()

                //             let JobStatusId = job_list.Where(e => e.LeadId == o.Id).Select(e => e.JobStatusId).FirstOrDefault()

                //             let IsLeftSalesRep = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.IsActive).FirstOrDefault()

                //             select new GetLeadForViewDto()
                //             {
                //                 Lead = new LeadDto
                //                 {
                //                     CompanyName = o.CompanyName,
                //                     Email = o.Email,
                //                     Phone = o.Phone,
                //                     Mobile = o.Mobile,
                //                     Address = o.Address,
                //                     Id = o.Id,
                //                     PostCode = o.PostCode,
                //                     LeadStatusID = o.LeadStatusId,
                //                     Suburb = o.Suburb,
                //                     LeadSource = o.LeadSource,
                //                     State = o.State,
                //                     AssignToUserID = o.AssignToUserID,
                //                     RejectReason = o.RejectReason,
                //                     IsGoogle = o.IsGoogle,
                //                     ChangeStatusDate = o.ChangeStatusDate,
                //                     CreationTime = o.CreationTime,
                //                     CurrentLeadOwner = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                //                     ReAssignDate = o.ReAssignDate,
                //                 },

                //                 LeadStatusName = o.LeadStatusFk.Status,
                //                 LeadStatusColorClass = o.LeadStatusFk.ColorClass,

                //                 LeadAssignDate = leadactivity_list.Where(e => e.LeadId == o.Id && e.ActionId == 3).OrderByDescending(e => e.Id).Select(e => e.CreationTime).FirstOrDefault(),

                //                 ReminderTime = o.ActivityDate != null ? o.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss") : null,
                //                 ActivityDescription = o.ActivityDescription,
                //                 ActivityComment = o.ActivityNote,

                //                 JobStatusName = job_list.Where(e => e.LeadId == o.Id).Select(e => e.JobStatusFk.Name).FirstOrDefault(),

                //                 RejectReasonName = s1 == null || s1.RejectReasonName == null ? "" : s1.RejectReasonName.ToString(),
                //                 CancelReasonName = s2 == null || s2.CancelReasonName == null ? "" : s2.CancelReasonName.ToString(),

                //                 LastQuoteCreationTime = _quotationRepository.GetAll().Where(e => e.JobFk.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.CreationTime).FirstOrDefault(),

                //                 JobStatusId = JobStatusId,

                //                 IsLeftSalesRep = IsLeftSalesRep,

                //                 IsStop = _PromotionUsersRepository.GetAll().Include(e => e.LeadFk).Where(e => e.PromotionResponseStatusId == 2 && e.LeadId == o.Id).Any(),

                //                 SummaryCount = summaryCount
                //             });

                //var totalCount = await filteredLeads.CountAsync();

                //_oldCodeTimer.Stop();
                #endregion Old

                // Start Optimized Code
                //var _newCodeTimer = Stopwatch.StartNew();
                //DateTime utc = DateTime.UtcNow;
                //input.StartDate = ChangeTime(input.StartDate, utc);
                //input.EndDate = ChangeTime(input.EndDate, utc);

                int UserId = (int)AbpSession.UserId;
                var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

                var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

                var User_List = _userRepository.GetAll().AsNoTracking();
                var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

                var leadactivity_list = _leadactivityRepository.GetAll().AsNoTracking().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.LeadFk.IsDuplicate != true && e.LeadFk.IsWebDuplicate != true).Select(e => new { e.Id, e.LeadId, e.ActionId, e.CreationTime });

                var job_list = _jobRepository.GetAll().AsNoTracking().Select(e => new { e.JobNumber, e.IsJobCancelRequest, e.JobStatusId, e.LeadId, e.FirstDepositDate, e.JobStatusFk });

                var TeamId = await user_teamList.Where(e => e.UserId == UserId).Select(e => e.TeamId).ToListAsync();
                var UserList = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

                var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
                IList<string> RoleName = await _userManager.GetRolesAsync(User);

                var userName = await UserManager.GetUserNameAsync(User);

                var jobnumberlist = new List<int?>();
                if (input.Filter != null)
                {
                    jobnumberlist = await job_list.Where(e => e.JobNumber == input.Filter).Select(e => e.LeadId).ToListAsync();
                }
                var FilterTeamList = new List<long?>();
                if (input.TeamId != null && input.TeamId != 0)
                {
                    FilterTeamList = await user_teamList.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToListAsync();
                }

                var FilterManagerList = new List<long?>();
                if (input.TeamId != null && input.TeamId != 0)
                {
                    var ManagerTeamId = await user_teamList.Where(e => TeamId.Contains(input.SalesManagerId)).Select(e => e.TeamId).ToListAsync();
                    FilterManagerList = await user_teamList.Where(e => ManagerTeamId.Contains(input.SalesManagerId)).Select(e => e.UserId).ToListAsync();
                }

                var CancelreqListLeadid = new List<int?>();
                if (input.Cancelrequestfilter != 0)
                {
                    CancelreqListLeadid = await job_list.Where(e => e.IsJobCancelRequest == true).Select(e => e.LeadId).Distinct().ToListAsync();
                }

                int Status = 0;
                if (input.JobStatusID != null && input.JobStatusID.Count() > 0 && input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0)
                {
                    if (input.LeadStatusIDS.Contains(6))
                    {
                        Status = 1;
                    }
                }

                var FollowupList = new List<int>();
                if (input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null)
                {
                    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date >= SDate.Value.Date && x.CreationTime.Date <= EDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
                }
                else if (input.DateFilterType == "Followup" && input.StartDate != null)
                {
                    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date >= SDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
                }
                else if (input.DateFilterType == "Followup" && input.EndDate != null)
                {
                    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date <= EDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
                }

                var leadSource = _lookup_leadSourceRepository.GetAll();

                var filteredLeads = _leadRepository.GetAll()
                           .Include(e => e.LeadStatusFk)
                           //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                           .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                           .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                           .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                           .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                           .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Phone == input.Filter)
                           .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                           .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => input.StateNameFilter.Contains(e.State) && !string.IsNullOrEmpty(e.State))
                           .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                           //.WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                           .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadStatusId))
                           .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                           .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)

                           // .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                           .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                           .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                           .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                           .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)

                           .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                           .WhereIf(Status == 1, e => job_list.Where(x => input.JobStatusID.Contains((int)x.JobStatusId) && x.LeadId == e.Id).Any())
                           .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.AssignToUserID))
                           .WhereIf(input.Cancelrequestfilter != 0, e => CancelreqListLeadid.Contains(e.Id))

                           .WhereIf(input.DateFilterType == "Assign" && input.StartDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                           .WhereIf(input.DateFilterType == "Assign" && input.EndDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                           .WhereIf(input.DateFilterType == "ReAssign" && input.StartDate != null, e => e.LeadAssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                           .WhereIf(input.DateFilterType == "ReAssign" && input.EndDate != null, e => e.LeadAssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                           .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                           .WhereIf(input.DateFilterType == "Creation" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                           .WhereIf(input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))

                           .WhereIf(input.DateFilterType == "NextFollowup" && input.StartDate != null, e => e.ActivityDate.Value.Date >= SDate.Value.Date)
                           .WhereIf(input.DateFilterType == "NextFollowup" && input.EndDate != null, e => e.ActivityDate.Value.Date <= EDate.Value.Date)
                           .WhereIf(input.Mylead == true, e => e.CreatorUserId == UserId)

                           .Where(e => e.HideDublicate != true && e.LeadSource == "CIMET")
                           .AsNoTracking()
                           .Select(e => new { e.Id, e.LeadStatusId, e.RejectReasonId, e.AssignToUserID, e.CompanyName, e.Email, e.Phone, e.Mobile, e.Address, e.PostCode, e.Suburb, e.LeadSource, e.State, e.RejectReason, e.IsGoogle, e.ChangeStatusDate, e.CreationTime, e.ReAssignDate, e.LeadStatusFk, e.ActivityDate, e.ActivityDescription, e.ActivityNote });

                var pagedAndFilteredLeads = filteredLeads
                    .OrderBy(input.Sorting ?? "id desc")
                    .PageBy(input);

                var summaryCount = new LeadsSummaryCount();
                summaryCount.New = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 1).CountAsync());
                summaryCount.UnHandled = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 2).CountAsync());
                summaryCount.Hot = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 5).CountAsync());
                summaryCount.Cold = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 3).CountAsync());
                summaryCount.Warm = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 4).CountAsync());
                summaryCount.Upgrade = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 6).CountAsync());
                summaryCount.Rejected = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 7).CountAsync());
                summaryCount.Cancelled = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 8).CountAsync());
                summaryCount.Closed = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 9).CountAsync());
                summaryCount.Total = Convert.ToString(await filteredLeads.CountAsync());
                summaryCount.Assigned = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 10).CountAsync());
                summaryCount.ReAssigned = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 11).CountAsync());
                summaryCount.Sold = Convert.ToString(await filteredLeads.Where(e => job_list.Where(x => x.FirstDepositDate != null && x.LeadId == e.Id).Any()).CountAsync());

                var leads = (from o in pagedAndFilteredLeads

                             join o1 in _lookup_rejectReasonRepository.GetAll() on o.RejectReasonId equals o1.Id into j1
                             from s1 in j1.DefaultIfEmpty()

                             join o2 in _lookup_cancelReasonRepository.GetAll() on o.LeadStatusId equals o2.Id into j2
                             from s2 in j2.DefaultIfEmpty()

                             let JobStatusId = job_list.Where(e => e.LeadId == o.Id).Select(e => e.JobStatusId).FirstOrDefault()

                             let IsLeftSalesRep = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.IsActive).FirstOrDefault()

                             select new GetLeadForViewDto()
                             {
                                 Lead = new LeadDto
                                 {
                                     CompanyName = o.CompanyName,
                                     Email = o.Email,
                                     Phone = o.Phone,
                                     Mobile = o.Mobile,
                                     Address = o.Address,
                                     Id = o.Id,
                                     PostCode = o.PostCode,
                                     LeadStatusID = o.LeadStatusId,
                                     Suburb = o.Suburb,
                                     LeadSource = o.LeadSource,
                                     State = o.State,
                                     AssignToUserID = o.AssignToUserID,
                                     RejectReason = o.RejectReason,
                                     IsGoogle = o.IsGoogle,
                                     ChangeStatusDate = o.ChangeStatusDate,
                                     CreationTime = o.CreationTime,
                                     CurrentLeadOwner = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                                     ReAssignDate = o.ReAssignDate,
                                 },

                                 LeadStatusName = o.LeadStatusFk.Status,
                                 LeadStatusColorClass = o.LeadStatusFk.ColorClass,

                                 LeadAssignDate = leadactivity_list.Where(e => e.LeadId == o.Id && e.ActionId == 3).OrderByDescending(e => e.Id).Select(e => e.CreationTime).FirstOrDefault(),

                                 ReminderTime = o.ActivityDate != null ? o.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss") : null,
                                 ActivityDescription = o.ActivityDescription,
                                 ActivityComment = o.ActivityNote,

                                 JobStatusName = job_list.Where(e => e.LeadId == o.Id).Select(e => e.JobStatusFk.Name).FirstOrDefault(),

                                 RejectReasonName = s1 == null || s1.RejectReasonName == null ? "" : s1.RejectReasonName.ToString(),
                                 CancelReasonName = s2 == null || s2.CancelReasonName == null ? "" : s2.CancelReasonName.ToString(),

                                 LastQuoteCreationTime = _quotationRepository.GetAll().Where(e => e.JobFk.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.CreationTime).FirstOrDefault(),

                                 JobStatusId = JobStatusId,

                                 IsLeftSalesRep = IsLeftSalesRep,

                                 IsStop = _PromotionUsersRepository.GetAll().Where(e => e.PromotionResponseStatusId == 2 && e.LeadId == o.Id).Any(),

                                 SummaryCount = summaryCount
                             });

                var totalCount = await filteredLeads.CountAsync();
                //_newCodeTimer.Stop();
                // End Optimized Code

                //var _oldExecutionTime = _oldCodeTimer.ElapsedMilliseconds;  // 6000 - 7000

                //var _newExecutionTime = _newCodeTimer.ElapsedMilliseconds;

                return new PagedResultDto<GetLeadForViewDto>(totalCount, await leads.ToListAsync());
            }
            catch (Exception e)
            {
                throw new UserFriendlyException("Opps there is problem!", e.Message);
            }
            #endregion
        }

        public async Task UpdateEnableAutoAssignLead(int oid, bool enable)
        {
            var organization = await _extendedOrganizationUnitRepository.GetAsync(oid);
            organization.EnableAutoAssignLead = enable;
            await _extendedOrganizationUnitRepository.UpdateAsync(organization);
        }

        public async Task<PagedResultDto<GetLeadForViewDto>> GetCIMETTrackerData(GetAll3rdPartyLeadsInput input)
        {
            #region New Code on 04/08/2022
            try
            {
               
                int UserId = (int)AbpSession.UserId;
                var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

                var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

                var User_List = _userRepository.GetAll().AsNoTracking();
                var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

                var leadactivity_list = _leadactivityRepository.GetAll().AsNoTracking().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.LeadFk.IsDuplicate != true && e.LeadFk.IsWebDuplicate != true).Select(e => new { e.Id, e.LeadId, e.ActionId, e.CreationTime,e.TemplateId });
                var leadactivity_listt = _leadactivityRepository.GetAll().Where(x => x.ActionId == 6 &&  x.LeadFk.OrganizationId == input.OrganizationUnit && x.TemplateId == 32 && x.LeadFk.IsWebDuplicate != true && x.CreationTime.Date >= SDate.Value.Date && x.CreationTime.Date <= EDate.Value.Date);
                var job_list = _jobRepository.GetAll().AsNoTracking().Select(e => new { e.JobNumber, e.IsJobCancelRequest, e.JobStatusId, e.LeadId, e.FirstDepositDate, e.JobStatusFk });

                var TeamId = await user_teamList.Where(e => e.UserId == UserId).Select(e => e.TeamId).ToListAsync();
                var UserList = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

                var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
                IList<string> RoleName = await _userManager.GetRolesAsync(User);

                var userName = await UserManager.GetUserNameAsync(User);

                var jobnumberlist = new List<int?>();
                if (input.Filter != null)
                {
                    jobnumberlist = await job_list.Where(e => e.JobNumber == input.Filter).Select(e => e.LeadId).ToListAsync();
                }
                var FilterTeamList = new List<long?>();
                if (input.TeamId != null && input.TeamId != 0)
                {
                    FilterTeamList = await user_teamList.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToListAsync();
                }

                var FilterManagerList = new List<long?>();
                if (input.TeamId != null && input.TeamId != 0)
                {
                    var ManagerTeamId = await user_teamList.Where(e => TeamId.Contains(input.SalesManagerId)).Select(e => e.TeamId).ToListAsync();
                    FilterManagerList = await user_teamList.Where(e => ManagerTeamId.Contains(input.SalesManagerId)).Select(e => e.UserId).ToListAsync();
                }

                var CancelreqListLeadid = new List<int?>();
                if (input.Cancelrequestfilter != 0)
                {
                    CancelreqListLeadid = await job_list.Where(e => e.IsJobCancelRequest == true).Select(e => e.LeadId).Distinct().ToListAsync();
                }

                int Status = 0;
                if (input.JobStatusID != null && input.JobStatusID.Count() > 0 && input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0)
                {
                    if (input.LeadStatusIDS.Contains(6))
                    {
                        Status = 1;
                    }
                }

                var FollowupList = new List<int>();
                if (input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null)
                {
                    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date >= SDate.Value.Date && x.CreationTime.Date <= EDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
                }
                else if (input.DateFilterType == "Followup" && input.StartDate != null)
                {
                    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date >= SDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
                }
                else if (input.DateFilterType == "Followup" && input.EndDate != null)
                {
                    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date <= EDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
                }
                List<int> smsDateFilteredLeads = new List<int>();

                if (input.DateFilterType == "SMSDate" && input.StartDate != null && input.EndDate != null)
                {
                    smsDateFilteredLeads = leadactivity_listt
                        .Where(x => x.ActionId == 6  && x.TemplateId == 32  && x.CreationTime.Date >= SDate.Value.Date && x.CreationTime.Date <= EDate.Value.Date)
                        .DistinctBy(e => e.LeadId)
                        .Select(x => x.LeadId)
                        .ToList();
                }
                else if (input.DateFilterType == "SMSDate" && input.StartDate != null)
                {
                    smsDateFilteredLeads = leadactivity_listt
                        .Where(x => x.ActionId == 6 && x.TemplateId == 32 && x.CreationTime.Date >= SDate.Value.Date)
                        .DistinctBy(e => e.LeadId)
                        .Select(x => x.LeadId)
                        .ToList();
                }
                else if (input.DateFilterType == "SMSDate" && input.EndDate != null)
                {
                    smsDateFilteredLeads = leadactivity_listt
                        .Where(x => x.ActionId == 6 && x.TemplateId == 32 && x.CreationTime.Date <= EDate.Value.Date)
                        .DistinctBy(e => e.LeadId)
                        .Select(x => x.LeadId)
                        .ToList();
                }
                var leadSource = _lookup_leadSourceRepository.GetAll();

                var filteredLeads = _leadRepository.GetAll()
                           .Include(e => e.LeadStatusFk)
                           //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                           .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                           .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                           .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                           .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                           .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Phone == input.Filter)
                           .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                           .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => input.StateNameFilter.Contains(e.State) && !string.IsNullOrEmpty(e.State))
                           .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                           //.WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSource == input.LeadSourceNameFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.LeadStatusName)
                           .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadStatusId))
                           .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                           .WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
                           .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)

                           // .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                           .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                           .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                           .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.AssignToUserID == input.SalesRepId)
                           .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.AssignToUserID == input.SalesManagerId)

                           .WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit)
                           .WhereIf(Status == 1, e => job_list.Where(x => input.JobStatusID.Contains((int)x.JobStatusId) && x.LeadId == e.Id).Any())
                           .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.AssignToUserID))
                           .WhereIf(input.Cancelrequestfilter != 0, e => CancelreqListLeadid.Contains(e.Id))

                           .WhereIf(input.DateFilterType == "Assign" && input.StartDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                           .WhereIf(input.DateFilterType == "Assign" && input.EndDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                           .WhereIf(input.DateFilterType == "ReAssign" && input.StartDate != null, e => e.LeadAssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                           .WhereIf(input.DateFilterType == "ReAssign" && input.EndDate != null, e => e.LeadAssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                           .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                           .WhereIf(input.DateFilterType == "Creation" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                           .WhereIf(input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))
                           .WhereIf(input.DateFilterType == "SMSDate" && input.StartDate != null && input.EndDate != null, e => smsDateFilteredLeads.Contains(e.Id))

                           .WhereIf(input.DateFilterType == "NextFollowup" && input.StartDate != null, e => e.ActivityDate.Value.Date >= SDate.Value.Date)
                           .WhereIf(input.DateFilterType == "NextFollowup" && input.EndDate != null, e => e.ActivityDate.Value.Date <= EDate.Value.Date)
                           .WhereIf(input.Mylead == true, e => e.CreatorUserId == UserId)

                           .Where(e => e.HideDublicate != true && e.SMSEmailCIMAT > 0)
                           .AsNoTracking()
                           .Select(e => new { e.Id, e.LeadStatusId, e.RejectReasonId, e.AssignToUserID, e.CompanyName, e.Email, e.Phone, e.Mobile, e.Address, e.PostCode, e.Suburb, e.LeadSource, e.State, e.RejectReason, e.IsGoogle, e.ChangeStatusDate, e.CreationTime, e.ReAssignDate, e.LeadStatusFk, e.ActivityDate, e.ActivityDescription, e.ActivityNote, e.SMSEmailCIMAT });

                var pagedAndFilteredLeads = filteredLeads
                    .OrderBy(input.Sorting ?? "id desc")
                    .PageBy(input);

                var summaryCount = new LeadsSummaryCount();
                summaryCount.New = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 1).CountAsync());
                summaryCount.UnHandled = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 2).CountAsync());
                summaryCount.Hot = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 5).CountAsync());
                summaryCount.Cold = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 3).CountAsync());
                summaryCount.Warm = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 4).CountAsync());
                summaryCount.Upgrade = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 6).CountAsync());
                summaryCount.Rejected = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 7).CountAsync());
                summaryCount.Cancelled = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 8).CountAsync());
                summaryCount.Closed = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 9).CountAsync());
                summaryCount.Total = Convert.ToString(await filteredLeads.CountAsync());
                summaryCount.Assigned = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 10).CountAsync());
                summaryCount.ReAssigned = Convert.ToString(await filteredLeads.Where(e => e.LeadStatusId == 11).CountAsync());
                summaryCount.Sold = Convert.ToString(await filteredLeads.Where(e => job_list.Where(x => x.FirstDepositDate != null && x.LeadId == e.Id).Any()).CountAsync());

                var leads = (from o in pagedAndFilteredLeads

                             join o1 in _lookup_rejectReasonRepository.GetAll() on o.RejectReasonId equals o1.Id into j1
                             from s1 in j1.DefaultIfEmpty()

                             join o2 in _lookup_cancelReasonRepository.GetAll() on o.LeadStatusId equals o2.Id into j2
                             from s2 in j2.DefaultIfEmpty()

                             let JobStatusId = job_list.Where(e => e.LeadId == o.Id).Select(e => e.JobStatusId).FirstOrDefault()

                             let IsLeftSalesRep = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.IsActive).FirstOrDefault()

                             select new GetLeadForViewDto()
                             {
                                 Lead = new LeadDto
                                 {
                                     CompanyName = o.CompanyName,
                                     Email = o.Email,
                                     Phone = o.Phone,
                                     Mobile = o.Mobile,
                                     Address = o.Address,
                                     Id = o.Id,
                                     PostCode = o.PostCode,
                                     LeadStatusID = o.LeadStatusId,
                                     Suburb = o.Suburb,
                                     LeadSource = o.LeadSource,
                                     State = o.State,
                                     AssignToUserID = o.AssignToUserID,
                                     RejectReason = o.RejectReason,
                                     IsGoogle = o.IsGoogle,
                                     ChangeStatusDate = o.ChangeStatusDate,
                                     CreationTime = o.CreationTime,
                                     CurrentLeadOwner = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                                     ReAssignDate = o.ReAssignDate,
                                 },

                                 LeadStatusName = o.LeadStatusFk.Status,
                                 LeadStatusColorClass = o.LeadStatusFk.ColorClass,

                                 LeadAssignDate = leadactivity_list.Where(e => e.LeadId == o.Id && e.ActionId == 3).OrderByDescending(e => e.Id).Select(e => e.CreationTime).FirstOrDefault(),

                                 ReminderTime = o.ActivityDate != null ? o.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss") : null,
                                 ActivityDescription = o.ActivityDescription,
                                 ActivityComment = o.ActivityNote,

                                 JobStatusName = job_list.Where(e => e.LeadId == o.Id).Select(e => e.JobStatusFk.Name).FirstOrDefault(),

                                 RejectReasonName = s1 == null || s1.RejectReasonName == null ? "" : s1.RejectReasonName.ToString(),
                                 CancelReasonName = s2 == null || s2.CancelReasonName == null ? "" : s2.CancelReasonName.ToString(),

                                 LastQuoteCreationTime = _quotationRepository.GetAll().Where(e => e.JobFk.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.CreationTime).FirstOrDefault(),

                                 JobStatusId = JobStatusId,

                                 IsLeftSalesRep = IsLeftSalesRep,

                                 IsStop = _PromotionUsersRepository.GetAll().Where(e => e.PromotionResponseStatusId == 2 && e.LeadId == o.Id).Any(),

                                 Type = o.SMSEmailCIMAT == 1 ? "SMS" : "Email",

                                 SummaryCount = summaryCount
                             });

                var totalCount = await filteredLeads.CountAsync();
                //_newCodeTimer.Stop();
                // End Optimized Code

                //var _oldExecutionTime = _oldCodeTimer.ElapsedMilliseconds;  // 6000 - 7000

                //var _newExecutionTime = _newCodeTimer.ElapsedMilliseconds;

                return new PagedResultDto<GetLeadForViewDto>(totalCount, await leads.ToListAsync());
            }
            catch (Exception e)
            {
                throw new UserFriendlyException("Opps there is problem!", e.Message);
            }
            #endregion
        }
        public async Task UpdateProjectOpenDate(int? id, DateTime projectOpenDate,int sectionId)
        {
            var projectOpen = (_timeZoneConverter.Convert(projectOpenDate, (int)AbpSession.TenantId));
            var job = await _jobRepository.FirstOrDefaultAsync(e=>e.LeadId == (int)id);
          
           

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 2;
            leadactivity.SectionId = sectionId;
            leadactivity.ActionNote = "Project Opened Date Modified";
            leadactivity.LeadId = (int)id;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);
            
            var List = new List<LeadtrackerHistory>();


            LeadtrackerHistory history = new LeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "CreationTime";
            history.PrevValue = job.CreationTime.ToString();
            history.CurValue = projectOpen.ToString();
            history.Action = "Update";
            history.LeadActionId = leadactid;
            history.LastmodifiedDateTime = DateTime.Now;
            history.LeadId = (int)id;
            List.Add(history);
            await _dbcontextprovider.GetDbContext().LeadtrackerHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
            job.CreationTime = (DateTime)projectOpen;
            await _jobRepository.UpdateAsync(job);

        }
    }
}