﻿using System.Collections.Generic;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Leads.Importing.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Leads.Importing
{
    public class InvalidLeadExporter : NpoiExcelExporterBase, IInvalidLeadExporter, ITransientDependency
	{
		public InvalidLeadExporter(ITempFileCacheManager tempFileCacheManager)
			: base(tempFileCacheManager)
		{
		}

        public FileDto ExportToFile(List<ImportLeadDto> leadListDtos)
        {

            return CreateExcelPackage(
           "InvalidLeads.xlsx",
            excelPackage =>
            {
                var sheet = excelPackage.CreateSheet("InvalidLeads");

                AddHeader(
                      sheet,
                      L("FirstName"),
                      L("LastName"),
                      L("Email"),
                      L("Phone"),
                      L("Mobile"),
                      L("Address"),
                      L("Suburb"),
                      L("State"),
                      L("Postcode"),
                      L("LeadSource"),
                      L("System"),
                      L("Roof"),
                      L("Angle"),
                      L("Story"),
                      L("HouseAge"),
                      L("Notes"),
                      L("Exception")
                  );

                AddObjects(
                      sheet, 2, leadListDtos,
                      _ => _.FirstName,
                      _ => _.LastName,
                      _ => _.Email,
                      _ => _.Phone,
                      _ => _.Mobile,
                      _ => _.Address,
                      _ => _.Suburb,
                      _ => _.State,
                      _ => _.PostCode,
                      _ => _.LeadSource,
                      _ => _.SystemType,
                      _ => _.RoofType,
                      _ => _.AngleType,
                      _ => _.StoryType,
                      _ => _.HouseAgeType,
                      _ => _.Requirements,
                      _ => _.Exception
                  );

                  //for (var i = 0; i < 16; i++)
                  //{
                  //	sheet.AutoSizeColumn(i);
                  //}
              });
        }
    }
}
