﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Leads.Importing.Dto
{
	public class ImportLeadDto
	{
		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string Email { get; set; }
		
		public string Phone { get; set; }

		public string Mobile { get; set; }

		public string Requirements { get; set; }

		public string UnitNo { get; set; }

		public string UnitType { get; set; }

		public string StreetNo { get; set; }

		public string StreetName { get; set; }

		public string StreetType { get; set; }

		public string Suburb { get; set; }

		public string State { get; set; }

		public string PostCode { get; set; }

		public string LeadSource { get; set; }

		public string Exception { get; set; }

		public bool CanBeImported()
		{
			return string.IsNullOrEmpty(Exception);
		}

		public virtual string Address { get; set; }

		public virtual string SystemType { get; set; }

		public virtual string RoofType { get; set; }

		public virtual string AngleType { get; set; }

		public virtual string StoryType { get; set; }

		public virtual string HouseAgeType { get; set; }

		public virtual int IsExternalLead { get; set; }

		public virtual int OrganizationId { get; set; }
	}
}
