﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Leads.Importing.Dto;

namespace TheSolarProduct.Leads.Importing
{
	public interface ILeadListExcelDataReader : ITransientDependency
	{
		List<ImportLeadDto> GetLeadFromExcel(byte[] fileBytes);
	}
}
