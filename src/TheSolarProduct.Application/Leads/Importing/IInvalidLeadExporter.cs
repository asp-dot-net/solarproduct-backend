﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.Leads.Importing.Dto;

namespace TheSolarProduct.Leads.Importing
{
	public interface IInvalidLeadExporter
	{
		FileDto ExportToFile(List<ImportLeadDto> leadListDtos);
	}
}
