﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.ObjectMapping;
using Abp.Threading;
using System;
using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.Leads.Importing.Dto;
using TheSolarProduct.LeadSources;
using TheSolarProduct.Storage;
using System.Linq;
using TheSolarProduct.Notifications;
using Abp.Localization;
using TheSolarProduct.PostCodes;
using TheSolarProduct.States;
using TheSolarProduct.LeadActivityLogs;
using Abp.Runtime.Session;
using Microsoft.EntityFrameworkCore;
using Abp.UI;
using TheSolarProduct.Authorization.Users;
using Abp.Authorization;
using TheSolarProduct.Authorization;

namespace TheSolarProduct.Leads.Importing
{
    public class ImportLeadsToExcelJob : BackgroundJob<ImportLeadsFromExcelJobArgs>, ITransientDependency
	{
		private readonly IRepository<Lead> _leadRepository;
		private readonly IRepository<LeadSource> _leadSourceRepository;
		private readonly IRepository<LeadActivityLog> _leadactivityRepository;
		private readonly IRepository<PostCode> _postCodeRepository;
		private readonly IRepository<State> _stateRepository;
		private readonly ILeadListExcelDataReader _leadListExcelDataReader;
		private readonly IBinaryObjectManager _binaryObjectManager;
		private readonly IObjectMapper _objectMapper;
		private readonly IAppNotifier _appNotifier;
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private readonly IInvalidLeadExporter _invalidLeadExporter;
		private readonly IAbpSession _abpSession;
		private readonly ILeadsAppService _leadServiceRepository;
		private readonly IRepository<User, long> _userRepository;

		public ImportLeadsToExcelJob(
			IRepository<Lead> leadRepository,
			IRepository<LeadSource> leadSourceRepository,
			IRepository<LeadActivityLog> leadactivityRepository,
			IRepository<PostCode> postCodeRepository,
			IRepository<State> stateRepository,
			ILeadListExcelDataReader leadListExcelDataReader,
			IBinaryObjectManager binaryObjectManager,
			IObjectMapper objectMapper,
			IAppNotifier appNotifier,
			IUnitOfWorkManager unitOfWorkManager,
			IInvalidLeadExporter invalidLeadExporter,
			IAbpSession abpSession,
			ILeadsAppService leadServiceRepository
			, IRepository<User, long> userRepository
			)
		{
			_leadRepository = leadRepository;
			_leadSourceRepository = leadSourceRepository;
			_leadactivityRepository = leadactivityRepository;
			_postCodeRepository = postCodeRepository;
			_stateRepository = stateRepository;
			_leadListExcelDataReader = leadListExcelDataReader;
			_binaryObjectManager = binaryObjectManager;
			_objectMapper = objectMapper;
			_appNotifier = appNotifier;
			_unitOfWorkManager = unitOfWorkManager;
			_invalidLeadExporter = invalidLeadExporter;
			_abpSession = abpSession;
			_leadServiceRepository = leadServiceRepository;
			_userRepository = userRepository;
		}

		[UnitOfWork]
		public override void Execute(ImportLeadsFromExcelJobArgs args)
		{
			var leads = GetLeadListFromExcelOrNull(args);
			if (leads == null || !leads.Any())
			{
				SendInvalidExcelNotification(args);
				return;
			}
			else
			{
				var invalidLeads = new List<ImportLeadDto>();

				foreach (var Lead in leads)
				{
					if (Lead.CanBeImported())
					{
						try
						{
							AsyncHelper.RunSync(() => CreateLeadAsync(Lead, args));
						}
						catch (UserFriendlyException exception)
						{
							//Lead.Exception = exception.Message;
							invalidLeads.Add(Lead);
						}
						catch (Exception e)
						{
							//Lead.Exception = e.ToString();
							invalidLeads.Add(Lead);
						}
					}
					else
					{
						invalidLeads.Add(Lead);
					}
				}

				using (var uow = _unitOfWorkManager.Begin())
				{
					using (CurrentUnitOfWork.SetTenantId(args.TenantId))
					{
						AsyncHelper.RunSync(() => ProcessImportLeadsResultAsync(args, invalidLeads));
					}

					uow.Complete();
				}
			}
		}

		private List<ImportLeadDto> GetLeadListFromExcelOrNull(ImportLeadsFromExcelJobArgs args)
		{
			using (var uow = _unitOfWorkManager.Begin())
			{
				using (CurrentUnitOfWork.SetTenantId(args.TenantId))
				{
					try
					{
						var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
						return _leadListExcelDataReader.GetLeadFromExcel(file.Bytes);
					}
					catch (Exception)
					{
						return null;
					}
					finally
					{
						uow.Complete();
					}
				}
			}
		}

		private async Task CreateLeadAsync(ImportLeadDto input, ImportLeadsFromExcelJobArgs args)
		{
			Lead lead = new Lead();
			
			var leadSourceId = 0;
			using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
			{
                if (input.LeadSource != null)
                {
                    leadSourceId = await _leadSourceRepository.GetAll().Where(e => e.Name.ToUpper() == input.LeadSource.ToUpper()).Select(e => e.Id).FirstOrDefaultAsync();
                }

                if (leadSourceId == 0)
                {
					input.Exception = "Lead Source Not Found";
					throw new Exception("Lead Source Not Found");
				}
                lead.TenantId = (int)args.TenantId;
				lead.CompanyName = input.FirstName + " " + input.LastName;
				lead.Email = input.Email;
				if(input.Phone.Length < 10 && input.Phone.Length != 0)
				{
					lead.Phone = input.Phone.ToString().PadLeft(10, '0');
				}
				else
				{
					lead.Phone = input.Phone;
				}
				if (input.Mobile.Length < 10 && input.Mobile.Length != 0)
				{
					lead.Mobile = input.Mobile.ToString().PadLeft(10, '0');
				}
                else if(input.Mobile.Length > 9 && input.Mobile.Length != 0)
                {
					var m = input.Mobile.Substring(input.Mobile.Length - 9);
                    lead.Mobile = m.ToString().PadLeft(10, '0');
                }
                else
				{
					lead.Mobile = input.Mobile;
				}
				lead.Requirements = input.Requirements;
				lead.ExcelAddress = input.Address + " " + input.Suburb + " , " + input.State + " - " + input.PostCode;
				lead.Address = input.Address;
				lead.Suburb = input.Suburb;
				
				lead.State = input.State;
				
				lead.PostCode = (input.PostCode == null) ? null : input.PostCode;
				lead.LeadSource = input.LeadSource;
				if (leadSourceId != 0)
				{
					lead.LeadSourceId = leadSourceId;
				}
				lead.LeadStatusId = 1;

				//1. Create
				//2. Excel Import
				//3. External Link
				//4. Copy Lead
				lead.IsExternalLead = 2;
				lead.IsPromotion = true;
				lead.CreatorUserId = (int)args.User.UserId;
				lead.OrganizationId = args.OrganizationId;

                //Start Check Duplicate
                //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
                var dbDup = _leadRepository.GetAll().Where(e => e.OrganizationId == args.OrganizationId && e.AssignToUserID != null && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == lead.Mobile && !string.IsNullOrEmpty(e.Mobile)))).FirstOrDefault();
                if (dbDup != null)
                {
                    lead.IsDuplicate = true;
                    lead.DublicateLeadId = dbDup.Id;
                }
                else
                {
                    lead.IsDuplicate = false;
                }

                //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                var webDup = _leadRepository.GetAll().Where(e => e.OrganizationId == args.OrganizationId && e.AssignToUserID == null && e.HideDublicate != true && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == lead.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Any();
                if (webDup)
                {
                    lead.IsWebDuplicate = true;
                }
                else
                {
                    lead.IsWebDuplicate = false;
                }
                //End Check Duplicate

                await _leadRepository.InsertAndGetIdAsync(lead);

				var UserName = _userRepository.GetAll().Where(e => e.Id == (int)args.User.UserId).Select(e => e.FullName).FirstOrDefault();
				LeadActivityLog leadactivity = new LeadActivityLog();
				leadactivity.ActionId = 1;
				leadactivity.ActionNote = "Lead Imported";
				leadactivity.LeadId = lead.Id;
				leadactivity.TenantId = (int)args.TenantId;
				leadactivity.CreatorUserId = (int)args.User.UserId;
				await _leadactivityRepository.InsertAsync(leadactivity);

			}
		}

		private void SendInvalidExcelNotification(ImportLeadsFromExcelJobArgs args)
		{
			using (var uow = _unitOfWorkManager.Begin())
			{
				using (CurrentUnitOfWork.SetTenantId(args.TenantId))
				{
					AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
						args.User,
						new LocalizableString("FileCantBeConvertedToLead", TheSolarProductConsts.LocalizationSourceName),
						null,
						Abp.Notifications.NotificationSeverity.Warn));
				}
				uow.Complete();
			}
		}

		private async Task ProcessImportLeadsResultAsync(ImportLeadsFromExcelJobArgs args,
			List<ImportLeadDto> invalidLeads)
		{
			if (invalidLeads.Any())
			{
				var file = _invalidLeadExporter.ExportToFile(invalidLeads);
				await _appNotifier.SomeLeadsCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
			}
			else
			{
				await _appNotifier.SendMessageAsync(
					args.User,
					new LocalizableString("AllLeadsSuccessfullyImportedFromExcel", TheSolarProductConsts.LocalizationSourceName),
					null,
					Abp.Notifications.NotificationSeverity.Success);
			}
		}
	}
}
