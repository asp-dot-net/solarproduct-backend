﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Invoices.Dtos;
using System.Linq;
using System;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using Abp;
using System.Data.Entity.Core.Objects;
using NUglify.Helpers;
using Abp.Collections.Extensions;
using Stripe;
using NPOI.SS.Formula.Functions;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.IO;
using Abp.AspNetZeroCore.Net;
using NPOI.XSSF.Streaming.Values;
using NPOI.XWPF.UserModel;
using TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos;
using TheSolarProduct.Reports.LeadAssigns.Dtos;
using TheSolarProduct.Jobs;
using TheSolarProduct.Reports.LeadSolds.Dtos;
using TheSolarProduct.CallHistory;
using TheSolarProduct.LeadGeneration.Dtos;
using Telerik.Reporting;
using TheSolarProduct.JobTrackers.Dtos;
using TheSolarProduct.Migrations;
using TheSolarProduct.MultiTenancy.Accounting;
using Hangfire.Common;

namespace TheSolarProduct.Leads.Exporting
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class LeadsExcelExporter : NpoiExcelExporterBase, ILeadsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly PermissionChecker _permissionChecker;
        private readonly ITempFileCacheManager _tempFileCacheManager;

        public LeadsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager,
            PermissionChecker permissionChecker) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _permissionChecker = permissionChecker;
            _tempFileCacheManager = tempFileCacheManager;
        }

        public FileDto LeadTrackerExportToFile(List<GetLeadForViewDto> leadtrackerListDtos, string fileName)
        {
            string[] Header = null;
            Func<GetLeadForViewDto, object>[] Objects = null;

           
            var EmailPermission = _permissionChecker.IsGranted(AppPermissions.Pages_LeadTracker_Export_MobileEmail);

            
            var LeadSourcePermission = _permissionChecker.IsGranted(AppPermissions.Pages_MyLeads_ShowLeadSource);

            int colCount = 0;

            
            if (EmailPermission)
            {
               
                Header = LeadSourcePermission
                    ? new string[] { L("Customer"), L("LeadStatus"), L("Mobile"), L("Address"),
                             L("Suburb"), L("State"), L("PostalCode"), L("LeadSource"),
                             L("NextFollowupDate"), L("Description"), L("Comment"),
                             L("AssignDate"), L("LeadOwner"), L("CreationTime"), L("GCLId"),
                             L("Area"), L("JobStatus") }
                    : new string[] { L("Customer"), L("LeadStatus"), L("Mobile"), L("Address"),
                             L("Suburb"), L("State"), L("PostalCode"),
                             L("NextFollowupDate"), L("Description"), L("Comment"),
                             L("AssignDate"), L("LeadOwner"), L("CreationTime"), L("GCLId"),
                             L("Area"), L("JobStatus") };

                
                Objects = LeadSourcePermission
                    ? new Func<GetLeadForViewDto, object>[] {
                _ => _.Lead.CompanyName,
                _ => _.LeadStatusName,
                _ => _.Lead.Mobile,
                _ => _.Lead.Address,
                _ => _.Lead.Suburb,
                _ => _.Lead.State,
                _ => _.Lead.PostCode,
                _ => _.Lead.LeadSource, 
                _ => _.ReminderTime,
                _ => _.ActivityDescription,
                _ => _.ActivityComment,
                _ => _.LeadAssignDate != null ? Convert.ToDateTime(_timeZoneConverter.Convert(_.LeadAssignDate, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "",
                _ => _.Lead.CurrentLeadOwner,
                _ => _.Lead.CreationTime != null ? Convert.ToDateTime(_timeZoneConverter.Convert(_.Lead.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "",
                _ => _.Lead.GCLId,
                _ => _.Lead.Area,
                _ => _.JobStatusName
                    }
                    : new Func<GetLeadForViewDto, object>[] {
                _ => _.Lead.CompanyName,
                _ => _.LeadStatusName,
                _ => _.Lead.Mobile,
                _ => _.Lead.Address,
                _ => _.Lead.Suburb,
                _ => _.Lead.State,
                _ => _.Lead.PostCode,
                _ => _.Lead.LeadSource, 
                _ => _.ReminderTime,
                _ => _.ActivityDescription,
                _ => _.ActivityComment,
                _ => _.LeadAssignDate != null ? Convert.ToDateTime(_timeZoneConverter.Convert(_.LeadAssignDate, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "",
                _ => _.Lead.CurrentLeadOwner,
                _ => _.Lead.CreationTime != null ? Convert.ToDateTime(_timeZoneConverter.Convert(_.Lead.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "",
                _ => _.Lead.GCLId,
                _ => _.Lead.Area,
                _ => _.JobStatusName
                    };
                colCount = Header.Count(); 

            }
            else
            {
                
                Header = LeadSourcePermission
                    ? new string[] { L("Customer"), L("LeadStatus"), L("Address"), L("Suburb"),
                             L("State"), L("PostalCode"), L("LeadSource"), L("NextFollowupDate"),
                             L("Description"), L("Comment"), L("AssignDate"), L("LeadOwner"),
                             L("CreationTime"), L("GCLId"), L("Area"), L("JobStatus") }
                    : new string[] { L("Customer"), L("LeadStatus"), L("Address"), L("Suburb"),
                             L("State"), L("PostalCode"), L("NextFollowupDate"),
                             L("Description"), L("Comment"), L("AssignDate"), L("LeadOwner"),
                             L("CreationTime"), L("GCLId"), L("Area"), L("JobStatus") };

                Objects = LeadSourcePermission
                    ? new Func<GetLeadForViewDto, object>[] {
                _ => _.Lead.CompanyName,
                _ => _.LeadStatusName,
                _ => _.Lead.Address,
                _ => _.Lead.Suburb,
                _ => _.Lead.State,
                _ => _.Lead.PostCode,
                _ => _.Lead.LeadSource,
                _ => _.ReminderTime,
                _ => _.ActivityDescription,
                _ => _.ActivityComment,
                _ => _.LeadAssignDate != null ? Convert.ToDateTime(_timeZoneConverter.Convert(_.LeadAssignDate, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "",
                _ => _.Lead.CurrentLeadOwner,
                _ => _.Lead.CreationTime != null ? Convert.ToDateTime(_timeZoneConverter.Convert(_.Lead.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "",
                _ => _.Lead.GCLId,
                _ => _.Lead.Area,
                _ => _.JobStatusName
                    }
                    : new Func<GetLeadForViewDto, object>[] {
                _ => _.Lead.CompanyName,
                _ => _.LeadStatusName,
                _ => _.Lead.Address,
                _ => _.Lead.Suburb,
                _ => _.Lead.State,
                _ => _.Lead.PostCode,
                _ => _.Lead.LeadSource,
                _ => _.ReminderTime,
                _ => _.ActivityDescription,
                _ => _.ActivityComment,
                _ => _.LeadAssignDate != null ? Convert.ToDateTime(_timeZoneConverter.Convert(_.LeadAssignDate, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "",
                _ => _.Lead.CurrentLeadOwner,
                _ => _.Lead.CreationTime != null ? Convert.ToDateTime(_timeZoneConverter.Convert(_.Lead.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "",
                _ => _.Lead.GCLId,
                _ => _.Lead.Area,
                _ => _.JobStatusName
                    };
                colCount = Header.Count();  
            }

           
            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("LeadTracker"));
                    AddHeader(sheet, Header);
                    AddObjects(sheet, 1, leadtrackerListDtos, Objects);

                    
                    for (int i = 0; i < colCount; i++)
                    {
                        if (EmailPermission && i != 9 && i != 10)
                        {
                            sheet.AutoSizeColumn(i);
                        }
                        else
                        {
                            sheet.DefaultColumnWidth = 15;
                        }

                        if (EmailPermission == false && i != 8 && i != 9)
                        {
                            sheet.AutoSizeColumn(i);
                        }
                        else
                        {
                            sheet.DefaultColumnWidth = 15;
                        }
                    }
                });
        }


        //public FileDto LeadTrackerCsvExport(List<GetLeadForViewDto> leadtrackerListDtos)
        //{
        //    return CreateExcelPackage(
        //        "LeadTracker.csv",
        //        excelPackage =>
        //        {
        //            var sheet = excelPackage.CreateSheet(L("LeadTracker"));
        //            AddHeader(
        //                sheet,
        //                L("Customer"),
        //                L("LeadStatus"),
        //                L("Mobile"),
        //                L("Address"),
        //                L("Suburb"),
        //                L("State"),
        //                L("PostalCode"),
        //                L("FollowUpDate"),
        //                L("LeadSource"),
        //                L("Organization"),
        //                L("CreationTime"),
        //                L("LeadOwner"),
        //                L("JobNumber"),
        //                L("JobStatus")
        //                );

        //            AddObjects(
        //               sheet, 2, leadtrackerListDtos,
        //               _ => _.Lead.CompanyName,
        //               _ => _.LeadStatusName,
        //               _ => _.Lead.Mobile,
        //               _ => _.Lead.Address,
        //               _ => _.Lead.PostCode,
        //               _ => _.Lead.State,
        //               _ => _.Lead.PostCode,
        //               _ => _.Lead.FollowupDate,
        //               _ => _.Lead.LeadSource,
        //               _ => _.Lead.OrganizationName,
        //               _ => _timeZoneConverter.Convert(_.Lead.CreationTime, _abpSession.TenantId, _abpSession.GetUserId()),
        //               _ => _.Lead.CurrentLeadOwner,
        //               _ => _.Lead.JobNumber,
        //               _ => _.Lead.JobStatus
        //               );
        //        });
        //}

        public FileDto JobGridExportToFile(List<GetJobForExcelViewDto> jobgridrListDtos, string fileName)
        {
            string[] Header = null;
            Header = new string[] { "Job Number", "Job Status","Sales Rap Name", "Company Name", "Team", "State", "Pincode", "Quote", "Source", "SystemCap", "BatteyKW", "HouseType", "RoofType", "Roof Angle","Job Type", "Price type"
                ,  "Meter Upgrdae", "Meter Phase"  , "Old System Detail", "Type", "Area","Price", "DepRec", "FinanceWith", "Fin Payment Type", "Deposit", "InstallBookDate", "FirstDepDate", "FirstDepAmount"
                , "NextFollowupDate", "Description", "Leaddate", "JobOpenDate", "DiffInDays","First2Depodit","Deposit2Active","Active2Install", "Job Notes", "No Of Panels", "No Of STC", "Cancel Reason", "Cancel Reason Notes", "Previous Job Status" };

            var MaxCount = 0;
            for (int i = 0; i < jobgridrListDtos.Count; i++)
            {
                if (jobgridrListDtos[i].ProductItems != null)
                {

                    if (MaxCount < jobgridrListDtos[i].ProductItems.Count)
                    {
                        MaxCount = jobgridrListDtos[i].ProductItems.Count;
                    }
                }
            }

            for (int i = 1; i <= MaxCount; i++)
            {
                Header = new List<string>(Header) { "Product Type", "Product Item " + i, "Product Model " + i, "Product Size " + i, "Quantity " + i }.ToArray();
            }


            //Header = new List<string>(Header) { "BatteyKW" }.ToArray();

            IWorkbook workbook = new XSSFWorkbook();

            var sheet = workbook.CreateSheet(fileName);
            AddHeader(sheet, Header);
            //return CreateExcelPackage(
            //    fileName,
            //    excelPackage =>
            //    {
            //        var sheet = excelPackage.CreateSheet(L("JobGrid"));
            //        AddHeader(
            //              sheet,
            //                Header
            //            );

            //AddObjects(
            //   sheet, 2, jobgridrListDtos,
            //   _ => _.Job.JobNumber,
            //   _ => _.JobStatusName,
            //   _ => _.SalesRapName,
            //   _ => _.Lead.CompanyName,
            //   _ => _.Team,
            //   _ => _.Job.CurrentLeadOwaner,
            //   _ => _.Job.State,
            //   _ => _.Job.PostalCode,
            //   _ => _.Job.ManualQuote,
            //   _ => _.Lead.LeadSource,
            //   _ => Convert.ToDecimal(_.Job.SystemCapacity != null ? _.Job.SystemCapacity  : 0) ,
            //   _ => _.HouseType,
            //   _ => _.RoofType,
            //   _ => Convert.ToDecimal(_.Job.TotalCost != null ? _.Job.TotalCost : 0),
            //   _ => _.Job.DepositeRecceivedDate != null ? Convert.ToDateTime(_timeZoneConverter.Convert(_.Job.DepositeRecceivedDate, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "", /// DepositeRecceivedDate
            //   _ => _.PaymentOption,
            //   _ => _.Job.InstallationDate != null ? Convert.ToDateTime(_timeZoneConverter.Convert(_.Job.InstallationDate, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "", /// InstallationDate
            //   _ => _.Job.FirstDepositDate != null ? Convert.ToDateTime(_timeZoneConverter.Convert(_.Job.FirstDepositDate, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "", // firstdep
            //   _ => Convert.ToDecimal(_.FirstDepositAmount != null ? _.FirstDepositAmount : 0),
            //   _ => _.ReminderTime, //nextfollowupdate
            //   _ => _.ActivityDescription,
            //   _ => _.Lead.CreationTime != null ? Convert.ToDateTime(_timeZoneConverter.Convert(_.Lead.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "", //LeadCreationDate
            //   _ => _.Job.CreationTime != null ? Convert.ToDateTime(_timeZoneConverter.Convert(_.Job.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "", //Jobcreation
            //   _ => 0,
            //   _ => _.Job.Note,
            //   _ => Convert.ToInt32(_.noOfPanel != null ? _.noOfPanel : 0),
            //   _ => Convert.ToInt32(_.Job.Stc != null ? _.Job.Stc : 0),
            //   _ => _.JobCancelReason,
            //   _ => _.JobCancelReasonNotes,
            //   _ => _.PreviousJobStatus
            //   );

            //for (var i = 1; i <= jobgridrListDtos.Count; i++)
            //{
            //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "0.00");

            //    SetintCellDataFormat(sheet.GetRow(i).Cells[12], "00");

            //    SetCellDataFormat(sheet.GetRow(i).Cells[13], "dd-MM-yyyy");

            //    SetCellDataFormat(sheet.GetRow(i).Cells[15], "dd-MM-yyyy");

            //    SetCellDataFormat(sheet.GetRow(i).Cells[16], "dd-MM-yyyy");

            //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[17], "0.00");

            //    SetCellDataFormat(sheet.GetRow(i).Cells[20], "dd-MM-yyyy");

            //    SetCellDataFormat(sheet.GetRow(i).Cells[21], "dd-MM-yyyy");

            //    SetintCellDataFormat(sheet.GetRow(i).Cells[22], "00");
            //}

            for (var i = 0; i < jobgridrListDtos.Count; i++)
            {
                var filxedCol = 43;
                var row = sheet.CreateRow(i + 1);

                var cell = row.CreateCell(0);
                cell.SetCellValue(jobgridrListDtos[i].Job.JobNumber);
                cell = row.CreateCell(1);
                cell.SetCellValue(jobgridrListDtos[i].JobStatusName);
                cell = row.CreateCell(2);
                cell.SetCellValue(jobgridrListDtos[i].SalesRapName);
                cell = row.CreateCell(3);
                cell.SetCellValue(jobgridrListDtos[i].Lead.CompanyName);
                cell = row.CreateCell(4);
                cell.SetCellValue(jobgridrListDtos[i].Team);
                //cell = row.CreateCell(5);
                //cell.SetCellValue(jobgridrListDtos[i].Job.CurrentLeadOwaner);
                cell = row.CreateCell(5);
                cell.SetCellValue(jobgridrListDtos[i].Job.State);
                cell = row.CreateCell(6);
                cell.SetCellValue(jobgridrListDtos[i].Job.PostalCode);
                cell = row.CreateCell(7);
                cell.SetCellValue(jobgridrListDtos[i].Job.ManualQuote);
                cell = row.CreateCell(8);
                cell.SetCellValue(jobgridrListDtos[i].Lead.LeadSource);
                cell = row.CreateCell(9);
                cell.SetCellValue((double)(jobgridrListDtos[i].Job.SystemCapacity != null ? jobgridrListDtos[i].Job.SystemCapacity : 0));
                cell = row.CreateCell(10);
                cell.SetCellValue((double)(jobgridrListDtos[i].Job.BatteryKw != null ? jobgridrListDtos[i].Job.BatteryKw : 0));
                cell = row.CreateCell(11);
                cell.SetCellValue(jobgridrListDtos[i].HouseType);
                cell = row.CreateCell(12);
                cell.SetCellValue(jobgridrListDtos[i].RoofType);
                cell = row.CreateCell(13);
                cell.SetCellValue(jobgridrListDtos[i].RoofAngle); cell = row.CreateCell(30);
                cell = row.CreateCell(14);
                cell.SetCellValue(jobgridrListDtos[i].Job.Jobtype);
                cell = row.CreateCell(15);
                cell.SetCellValue(jobgridrListDtos[i].Job.PriceType);
                cell = row.CreateCell(16);
                cell.SetCellValue(jobgridrListDtos[i].MeterUpgrade);
                cell = row.CreateCell(17);
                cell.SetCellValue((int)(jobgridrListDtos[i].MeterPhase != null ? jobgridrListDtos[i].MeterPhase : 0));  
                cell = row.CreateCell(18);
                cell.SetCellValue(jobgridrListDtos[i].OldSys);
                cell = row.CreateCell(19);
                cell.SetCellValue(jobgridrListDtos[i].Lead.Type);
                cell = row.CreateCell(20);
                cell.SetCellValue(jobgridrListDtos[i].Lead.Area);
                cell = row.CreateCell(21);
                cell.SetCellValue((double)(jobgridrListDtos[i].Job.TotalCost != null ? jobgridrListDtos[i].Job.TotalCost : 0));
                cell = row.CreateCell(22);
                cell.SetCellValue(jobgridrListDtos[i].Job.DepositeRecceivedDate != null ?Convert.ToDateTime(_timeZoneConverter.Convert(jobgridrListDtos[i].Job.DepositeRecceivedDate, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "");
                cell = row.CreateCell(23);
                cell.SetCellValue(jobgridrListDtos[i].PaymentOption);
                cell = row.CreateCell(24);
                cell.SetCellValue(jobgridrListDtos[i].FinPaymentType);
                cell = row.CreateCell(25);
                cell.SetCellValue(jobgridrListDtos[i].Deposit);
                cell = row.CreateCell(26);
                cell.SetCellValue(jobgridrListDtos[i].Job.InstallationDate != null ? Convert.ToDateTime(_timeZoneConverter.Convert(jobgridrListDtos[i].Job.InstallationDate, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "");
                cell = row.CreateCell(27);
                cell.SetCellValue(jobgridrListDtos[i].Job.FirstDepositDate != null ? Convert.ToDateTime(_timeZoneConverter.Convert(jobgridrListDtos[i].Job.FirstDepositDate, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "");
                cell = row.CreateCell(28);
                cell.SetCellValue((double)(jobgridrListDtos[i].FirstDepositAmount != null ? jobgridrListDtos[i].FirstDepositAmount : 0));
                cell = row.CreateCell(29);
                cell.SetCellValue(jobgridrListDtos[i].ReminderTime);
                cell = row.CreateCell(30);
                cell.SetCellValue(jobgridrListDtos[i].ActivityDescription);
                cell = row.CreateCell(31);
                cell.SetCellValue(jobgridrListDtos[i].Lead.CreationTime != null ? Convert.ToDateTime(_timeZoneConverter.Convert(jobgridrListDtos[i].Lead.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "");
                cell = row.CreateCell(32);
                cell.SetCellValue(jobgridrListDtos[i].Job.CreationTime != null ? Convert.ToDateTime(_timeZoneConverter.Convert(jobgridrListDtos[i].Job.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "");
                cell = row.CreateCell(33);
                cell.SetCellValue((int)(jobgridrListDtos[i].DiffDays != null? jobgridrListDtos[i].DiffDays : 0));
                cell = row.CreateCell(34);
                cell.SetCellValue((int)(jobgridrListDtos[i].First2Depodit != null ? jobgridrListDtos[i].First2Depodit : 0));
                cell = row.CreateCell(35);
                cell.SetCellValue((int)(jobgridrListDtos[i].Deposit2Active != null ? jobgridrListDtos[i].Deposit2Active : 0));
                cell = row.CreateCell(36);
                cell.SetCellValue((int)(jobgridrListDtos[i].Active2Install != null ? jobgridrListDtos[i].Active2Install : 0));
                cell = row.CreateCell(37);
                cell.SetCellValue(jobgridrListDtos[i].Job.Note);
                cell = row.CreateCell(38);
                cell.SetCellValue((int)(jobgridrListDtos[i].noOfPanel != null ? jobgridrListDtos[i].noOfPanel : 0));
                cell = row.CreateCell(39);
                cell.SetCellValue((int)(jobgridrListDtos[i].Job.Stc != null ? jobgridrListDtos[i].Job.Stc : 0));
                cell = row.CreateCell(40);
                cell.SetCellValue(jobgridrListDtos[i].JobCancelReason);
                cell = row.CreateCell(41);
                cell.SetCellValue(jobgridrListDtos[i].JobCancelReasonNotes);
                cell = row.CreateCell(42);
                cell.SetCellValue(jobgridrListDtos[i].PreviousJobStatus);

                if (jobgridrListDtos[i].ProductItems != null)
                {

                    if (jobgridrListDtos[i].ProductItems.Where(e => e.Id == 5).Any())
                    {
                        cell = row.CreateCell(40);
                        cell.SetCellValue((double)jobgridrListDtos[i].ProductItems.Where(e => e.Id == 5).Sum(e => e.betteryKW));
                    }
                    else
                    {
                        cell = row.CreateCell(40);
                        cell.SetCellValue((double)0);
                    }
                    for (int j = 0; j < jobgridrListDtos[i].ProductItems.Count(); j++)
                    {

                        cell = row.CreateCell(filxedCol+1);
                        cell.SetCellValue(jobgridrListDtos[i].ProductItems[j].ProductType);
                        cell = row.CreateCell(filxedCol + 2);
                        cell.SetCellValue(jobgridrListDtos[i].ProductItems[j].ProductItem);
                        cell = row.CreateCell(filxedCol + 3);
                        cell.SetCellValue(jobgridrListDtos[i].ProductItems[j].ProductModel);
                        cell = row.CreateCell(filxedCol + 4);
                        cell.SetCellValue(jobgridrListDtos[i].ProductItems[j].ProductSize != null ? (double)jobgridrListDtos[i].ProductItems[j].ProductSize : 0);
                        //cell.SetCellValue(jobProduct[i].ProductItems[j].ProductSize.ToString());
                        cell = row.CreateCell(filxedCol + 5);
                        cell.SetCellValue(jobgridrListDtos[i].ProductItems[j].Quantity != null ? (int)jobgridrListDtos[i].ProductItems[j].Quantity : 0);
                        //cell.SetCellValue(jobProduct[i].ProductItems[j].Quantity.ToString());

                        filxedCol = filxedCol + 5;
                    }

                }

            }for (var i = 0; i < jobgridrListDtos.Count; i++)
            {
                var filxedCol = 40;
                var row = sheet.CreateRow(i + 1);

                var cell = row.CreateCell(0);
                cell.SetCellValue(jobgridrListDtos[i].Job.JobNumber);
                cell = row.CreateCell(1);
                cell.SetCellValue(jobgridrListDtos[i].JobStatusName);
                cell = row.CreateCell(2);
                cell.SetCellValue(jobgridrListDtos[i].SalesRapName);
                cell = row.CreateCell(3);
                cell.SetCellValue(jobgridrListDtos[i].Lead.CompanyName);
                cell = row.CreateCell(4);
                cell.SetCellValue(jobgridrListDtos[i].Team);
                //cell = row.CreateCell(5);
                //cell.SetCellValue(jobgridrListDtos[i].Job.CurrentLeadOwaner);
                cell = row.CreateCell(5);
                cell.SetCellValue(jobgridrListDtos[i].Job.State);
                cell = row.CreateCell(6);
                cell.SetCellValue(jobgridrListDtos[i].Job.PostalCode);
                cell = row.CreateCell(7);
                cell.SetCellValue(jobgridrListDtos[i].Job.ManualQuote);
                cell = row.CreateCell(8);
                cell.SetCellValue(jobgridrListDtos[i].Lead.LeadSource);
                cell = row.CreateCell(9);
                cell.SetCellValue((double)(jobgridrListDtos[i].Job.SystemCapacity != null ? jobgridrListDtos[i].Job.SystemCapacity : 0));
                if (jobgridrListDtos[i].ProductItems != null)
                {

                    if (jobgridrListDtos[i].ProductItems.Where(e => e.Id == 5).Any())
                    {
                        cell = row.CreateCell(10);
                        cell.SetCellValue((double)jobgridrListDtos[i].ProductItems.Where(e => e.Id == 5).Sum(e => e.betteryKW));
                    }
                    else
                    {
                        cell = row.CreateCell(10);
                        cell.SetCellValue((double)0);
                    }
                }
                else
                {
                    cell = row.CreateCell(10);
                    cell.SetCellValue((double)0);
                }
                cell = row.CreateCell(11);
                cell.SetCellValue(jobgridrListDtos[i].HouseType);
                cell = row.CreateCell(12);
                cell.SetCellValue(jobgridrListDtos[i].RoofType);
                cell = row.CreateCell(13);
                cell.SetCellValue(jobgridrListDtos[i].RoofAngle); cell = row.CreateCell(30);
                cell = row.CreateCell(14);
                cell.SetCellValue(jobgridrListDtos[i].Job.Jobtype);
                cell = row.CreateCell(15);
                cell.SetCellValue(jobgridrListDtos[i].Job.PriceType);
                cell = row.CreateCell(16);
                cell.SetCellValue(jobgridrListDtos[i].MeterUpgrade);
                cell = row.CreateCell(17);
                cell.SetCellValue((int)(jobgridrListDtos[i].MeterPhase != null ? jobgridrListDtos[i].MeterPhase : 0));  
                cell = row.CreateCell(18);
                cell.SetCellValue(jobgridrListDtos[i].OldSys);
                cell = row.CreateCell(19);
                cell.SetCellValue(jobgridrListDtos[i].Lead.Type);
                cell = row.CreateCell(20);
                cell.SetCellValue(jobgridrListDtos[i].Lead.Area);
                cell = row.CreateCell(21);
                cell.SetCellValue((double)(jobgridrListDtos[i].Job.TotalCost != null ? jobgridrListDtos[i].Job.TotalCost : 0));
                cell = row.CreateCell(22);
                cell.SetCellValue(jobgridrListDtos[i].Job.DepositeRecceivedDate != null ?Convert.ToDateTime(_timeZoneConverter.Convert(jobgridrListDtos[i].Job.DepositeRecceivedDate, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "");
                cell = row.CreateCell(23);
                cell.SetCellValue(jobgridrListDtos[i].PaymentOption);
                cell = row.CreateCell(24);
                cell.SetCellValue(jobgridrListDtos[i].FinPaymentType);
                cell = row.CreateCell(25);
                cell.SetCellValue(jobgridrListDtos[i].Deposit);
                cell = row.CreateCell(26);
                cell.SetCellValue(jobgridrListDtos[i].Job.InstallationDate != null ? Convert.ToDateTime(_timeZoneConverter.Convert(jobgridrListDtos[i].Job.InstallationDate, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "");
                cell = row.CreateCell(27);
                cell.SetCellValue(jobgridrListDtos[i].Job.FirstDepositDate != null ? Convert.ToDateTime(_timeZoneConverter.Convert(jobgridrListDtos[i].Job.FirstDepositDate, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "");
                cell = row.CreateCell(28);
                cell.SetCellValue((double)(jobgridrListDtos[i].FirstDepositAmount != null ? jobgridrListDtos[i].FirstDepositAmount : 0));
                cell = row.CreateCell(29);
                cell.SetCellValue(jobgridrListDtos[i].ReminderTime);
                cell = row.CreateCell(30);
                cell.SetCellValue(jobgridrListDtos[i].ActivityDescription);
                cell = row.CreateCell(31);
                cell.SetCellValue(jobgridrListDtos[i].Lead.CreationTime != null ? Convert.ToDateTime(_timeZoneConverter.Convert(jobgridrListDtos[i].Lead.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "");
                cell = row.CreateCell(32);
                cell.SetCellValue(jobgridrListDtos[i].Job.CreationTime != null ? Convert.ToDateTime(_timeZoneConverter.Convert(jobgridrListDtos[i].Job.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())).ToString("dd-MM-yyyy") : "");
                cell = row.CreateCell(33);
                cell.SetCellValue((int)(jobgridrListDtos[i].DiffDays != null? jobgridrListDtos[i].DiffDays : 0));
                cell = row.CreateCell(34);
                cell.SetCellValue((int)(jobgridrListDtos[i].First2Depodit > 0 ? jobgridrListDtos[i].First2Depodit : 0));
                cell = row.CreateCell(35);
                cell.SetCellValue((int)(jobgridrListDtos[i].Deposit2Active > 0 ? jobgridrListDtos[i].Deposit2Active : 0));
                cell = row.CreateCell(36);
                cell.SetCellValue((int)(jobgridrListDtos[i].Active2Install > 0 ? jobgridrListDtos[i].Active2Install : 0));
                cell = row.CreateCell(37);
                cell.SetCellValue(jobgridrListDtos[i].Job.Note);
                cell = row.CreateCell(38);
                cell.SetCellValue((int)(jobgridrListDtos[i].noOfPanel != null ? jobgridrListDtos[i].noOfPanel : 0));
                cell = row.CreateCell(39);
                cell.SetCellValue((int)(jobgridrListDtos[i].Job.Stc != null ? jobgridrListDtos[i].Job.Stc : 0));
                cell = row.CreateCell(40);
                cell.SetCellValue(jobgridrListDtos[i].JobCancelReason);
                cell = row.CreateCell(41);
                cell.SetCellValue(jobgridrListDtos[i].JobCancelReasonNotes);
                cell = row.CreateCell(42);
                cell.SetCellValue(jobgridrListDtos[i].PreviousJobStatus);

                if (jobgridrListDtos[i].ProductItems != null)
                {

                    //if (jobgridrListDtos[i].ProductItems.Where(e => e.Id == 5).Any())
                    //{
                    //    cell = row.CreateCell(40);
                    //    cell.SetCellValue((double)jobgridrListDtos[i].ProductItems.Where(e => e.Id == 5).Sum(e => e.betteryKW));
                    //}
                    //else
                    //{
                    //    cell = row.CreateCell(40);
                    //    cell.SetCellValue((double)0);
                    //}
                    for (int j = 0; j < jobgridrListDtos[i].ProductItems.Count(); j++)
                    {

                        cell = row.CreateCell(filxedCol+1);
                        cell.SetCellValue(jobgridrListDtos[i].ProductItems[j].ProductType);
                        cell = row.CreateCell(filxedCol + 2);
                        cell.SetCellValue(jobgridrListDtos[i].ProductItems[j].ProductItem);
                        cell = row.CreateCell(filxedCol + 3);
                        cell.SetCellValue(jobgridrListDtos[i].ProductItems[j].ProductModel);
                        cell = row.CreateCell(filxedCol + 4);
                        cell.SetCellValue(jobgridrListDtos[i].ProductItems[j].ProductSize != null ? (double)jobgridrListDtos[i].ProductItems[j].ProductSize : 0);
                        //cell.SetCellValue(jobProduct[i].ProductItems[j].ProductSize.ToString());
                        cell = row.CreateCell(filxedCol + 5);
                        cell.SetCellValue(jobgridrListDtos[i].ProductItems[j].Quantity != null ? (int)jobgridrListDtos[i].ProductItems[j].Quantity : 0);
                        //cell.SetCellValue(jobProduct[i].ProductItems[j].Quantity.ToString());

                        filxedCol = filxedCol + 5;
                    }

                }

            }

            for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {

                        if (i != 19 && i != 23)
                        {
                            sheet.AutoSizeColumn(i);
                        }
                        else
                        {
                            sheet.DefaultColumnWidth = 15;
                        }
                        
                    }

            // });

            var file = new FileDto(fileName, MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var stream = new MemoryStream())
            {
                workbook.Write(stream);
                _tempFileCacheManager.SetFile(file.FileToken, stream.ToArray());
            }

            return file;

        }

        //public FileDto JobGridExportToCsvFile(List<GetJobForViewDto> jobgridrListDtos)
        //{
        //    return CreateExcelPackage(
        //        "JobGrid.csv",
        //        excelPackage =>
        //        {
        //            var sheet = excelPackage.CreateSheet(L("JobGrid"));
        //            AddHeader(
        //                  sheet,
        //                L("ProjectNo"),
        //                L("Man"),
        //                L("Project"),
        //                L("Contact"),
        //                L("Mobile"),
        //                L("ContactEmail"),
        //                L("Team"),
        //                L("EmployeeName"),
        //                L("State"),
        //                L("Pincode"),
        //                L("Quote"),
        //                L("Source"),
        //                L("Status"),
        //                L("SystemCap"),
        //                L("Panels"),
        //                L("PanelName"),
        //                L("Inverter"),
        //                L("InverterName"),
        //                L("HouseType"),
        //                L("RoofType"),
        //                L("Price"),
        //                L("DepRec"),
        //                L("FinanceWith"),
        //                L("InstallBookDate"),
        //                L("FirstDepDate"),
        //                L("FirstDepAmount"),
        //                L("NextFollowupDate"),
        //                L("Description"),
        //                L("Leaddate"),
        //                L("ProjOpenDate"),
        //                L("DiffInDays"),
        //                L("ProjectNotes"),
        //                L("ActiveDate"),
        //                L("CancelDate")
        //                );

        //            AddObjects(
        //               sheet, 2, jobgridrListDtos,
        //               _ => _.Job.JobNumber,
        //               _ => null,
        //               _ => _.Job.Address,
        //               _ => _.LeadCompanyName,
        //               _ => _.Mobile,
        //               _ => _.Email,
        //               _ => _.Team,
        //               _ => _.Job.CurrentLeadOwaner, //EmployeeName
        //               _ => _.Job.State,
        //               _ => _.Job.PostalCode,
        //               _ => _.Job.ManualQuote,
        //               _ => _.LeadSourceName,
        //               _ => _.LeadStatus,
        //               _ => _.Job.SystemCapacity,
        //               _ => _.TotalNoOfPanel,
        //               _ => _.Panels,
        //                _ => _.TotalNoOfInvert,
        //               _ => _.Inverters,
        //               _ => _.HouseType,
        //               _ => _.RoofType,
        //               _ => _.Job.TotalCost,
        //               _ => _timeZoneConverter.Convert(_.Job.DepositeRecceivedDate, _abpSession.TenantId, _abpSession.GetUserId()), /// DepositeRecceivedDate
        //               _ => _.FinanceWith,
        //               _ => _timeZoneConverter.Convert(_.Job.InstallationDate, _abpSession.TenantId, _abpSession.GetUserId()), /// InstallationDate
        //               _ => _timeZoneConverter.Convert(_.FirstDepDate, _abpSession.TenantId, _abpSession.GetUserId()), // firstdep
        //               _ => _.FirstDepAmmount,
        //               _ => _timeZoneConverter.Convert(_.LastCommentDate, _abpSession.TenantId, _abpSession.GetUserId()), //nextfollowupdate
        //               _ => _.LastComment,
        //               _ => _timeZoneConverter.Convert(_.LeadCreationDate, _abpSession.TenantId, _abpSession.GetUserId()), //LeadCreationDate
        //               _ => _timeZoneConverter.Convert(_.Job.CreationTime, _abpSession.TenantId, _abpSession.GetUserId()), //Jobcreation
        //               _ => _.DateDiff,
        //               _ => _.Job.Note,
        //               _ => _timeZoneConverter.Convert(_.Job.ActiveDate, _abpSession.TenantId, _abpSession.GetUserId()),
        //               _ => _timeZoneConverter.Convert(_.CancelDate, _abpSession.TenantId, _abpSession.GetUserId())
        //               );

        //            for (var i = 1; i <= jobgridrListDtos.Count; i++)
        //            {
        //                SetCellDataFormat(sheet.GetRow(i).Cells[21], "dd-MM-yyyy");
        //            }
        //            sheet.AutoSizeColumn(21);
        //            for (var i = 1; i <= jobgridrListDtos.Count; i++)
        //            {
        //                SetCellDataFormat(sheet.GetRow(i).Cells[23], "dd-MM-yyyy");
        //            }
        //            sheet.AutoSizeColumn(23);
        //            for (var i = 1; i <= jobgridrListDtos.Count; i++)
        //            {
        //                SetCellDataFormat(sheet.GetRow(i).Cells[24], "dd-MM-yyyy");
        //            }
        //            sheet.AutoSizeColumn(24);
        //            for (var i = 1; i <= jobgridrListDtos.Count; i++)
        //            {
        //                SetCellDataFormat(sheet.GetRow(i).Cells[26], "dd-MM-yyyy");
        //            }
        //            sheet.AutoSizeColumn(26);
        //            for (var i = 1; i <= jobgridrListDtos.Count; i++)
        //            {
        //                SetCellDataFormat(sheet.GetRow(i).Cells[28], "dd-MM-yyyy");
        //            }
        //            sheet.AutoSizeColumn(28);
        //            for (var i = 1; i <= jobgridrListDtos.Count; i++)
        //            {
        //                SetCellDataFormat(sheet.GetRow(i).Cells[29], "dd-MM-yyyy");
        //            }
        //            sheet.AutoSizeColumn(29);

        //            for (var i = 1; i <= jobgridrListDtos.Count; i++)
        //            {
        //                SetCellDataFormat(sheet.GetRow(i).Cells[32], "dd-MM-yyyy");
        //            }
        //            sheet.AutoSizeColumn(32);
        //            for (var i = 1; i <= jobgridrListDtos.Count; i++)
        //            {
        //                SetCellDataFormat(sheet.GetRow(i).Cells[33], "dd-MM-yyyy");
        //            }
        //            sheet.AutoSizeColumn(33);
        //        });
        //}

        public FileDto JobGridProductDetailsExport(List<GetJobProductDetailsForExcel> jobProduct, string fileName)
        {
            string[] Header = null;
            Header = new string[] { "Job Number", "Job Status", "Sales Rap Name", "Team", "State", "System Capacity", "Amount", "Finance", "Region", "Job Type" };

            var MaxCount = 0;
            for (int i = 0; i < jobProduct.Count; i++)
            {
                if (jobProduct[i].ProductItems != null)
                {

                    if (MaxCount < jobProduct[i].ProductItems.Count)
                    {
                        MaxCount = jobProduct[i].ProductItems.Count;
                    }
                }
            }

            for (int i = 1; i <= MaxCount; i++)
            {
                Header = new List<string>(Header) { "Product Type", "Product Item " + i, "Product Model " + i, "Product Size " + i, "Quantity " + i }.ToArray();
            }

            IWorkbook workbook = new XSSFWorkbook();

            var sheet = workbook.CreateSheet(L("JobGridProductDetails"));
            AddHeader(sheet, Header);

            for (var i = 0; i < jobProduct.Count; i++)
            {
                var filxedCol = 9;
                var row = sheet.CreateRow(i + 1);

                var cell = row.CreateCell(0);
                cell.SetCellValue(jobProduct[i].JobNumber);
                cell = row.CreateCell(1);
                cell.SetCellValue(jobProduct[i].JobStatus);
                cell = row.CreateCell(2);
                cell.SetCellValue(jobProduct[i].SalesRapName);
                 cell = row.CreateCell(3);
                cell.SetCellValue(jobProduct[i].Team);
                cell = row.CreateCell(4);
                cell.SetCellValue(jobProduct[i].State);
                cell = row.CreateCell(5);
                cell.SetCellValue((double)(jobProduct[i].SystemCapacity != null ? jobProduct[i].SystemCapacity : 0));
                //cell.SetCellValue(jobProduct[i].SystemCapacity.ToString());
                cell = row.CreateCell(6);
                cell.SetCellValue(jobProduct[i].Amount != null ? (double)jobProduct[i].Amount : 0);
                //cell.SetCellValue(jobProduct[i].Amount.ToString());
                cell = row.CreateCell(7);
                cell.SetCellValue(jobProduct[i].FinanceType);
                cell = row.CreateCell(8);
                cell.SetCellValue(jobProduct[i].Region);
                cell = row.CreateCell(9);
                cell.SetCellValue(jobProduct[i].JobType);

                //for (int j = 0; j < jobProduct[i].ProductItems.Count(); j++)
                //{
                //    cell = row.CreateCell(filxedCol);
                //    cell.SetCellValue(jobProduct[i].ProductItems[j].ProductType);
                //    cell = row.CreateCell(filxedCol + 1);
                //    cell.SetCellValue(jobProduct[i].ProductItems[j].ProductItem);
                //    cell = row.CreateCell(filxedCol + 2);
                //    cell.SetCellValue(jobProduct[i].ProductItems[j].ProductModel);
                //    cell = row.CreateCell(filxedCol + 3);
                //    cell.SetCellValue(jobProduct[i].ProductItems[j].ProductSize != null ? (double)jobProduct[i].ProductItems[j].ProductSize : 0);
                //    //cell.SetCellValue(jobProduct[i].ProductItems[j].ProductSize.ToString());
                //    cell = row.CreateCell(filxedCol + 4);
                //    cell.SetCellValue(jobProduct[i].ProductItems[j].Quantity != null ? (int)jobProduct[i].ProductItems[j].Quantity : 0);
                //    //cell.SetCellValue(jobProduct[i].ProductItems[j].Quantity.ToString());

                //    filxedCol = filxedCol + 5;
                //}
                if (jobProduct[i].ProductItems != null)
                {
                    for (int j = 0; j < jobProduct[i].ProductItems.Count(); j++)
                    {

                        cell = row.CreateCell(filxedCol);
                        cell.SetCellValue(jobProduct[i].ProductItems[j].ProductType);
                        cell = row.CreateCell(filxedCol + 1);
                        cell.SetCellValue(jobProduct[i].ProductItems[j].ProductItem);
                        cell = row.CreateCell(filxedCol + 2);
                        cell.SetCellValue(jobProduct[i].ProductItems[j].ProductModel);
                        cell = row.CreateCell(filxedCol + 3);
                        cell.SetCellValue(jobProduct[i].ProductItems[j].ProductSize != null ? (double)jobProduct[i].ProductItems[j].ProductSize : 0);
                        //cell.SetCellValue(jobProduct[i].ProductItems[j].ProductSize.ToString());
                        cell = row.CreateCell(filxedCol + 4);
                        cell.SetCellValue(jobProduct[i].ProductItems[j].Quantity != null ? (int)jobProduct[i].ProductItems[j].Quantity : 0);
                        //cell.SetCellValue(jobProduct[i].ProductItems[j].Quantity.ToString());

                        filxedCol = filxedCol + 5;
                    }
                }
            }

            for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
            {
                sheet.AutoSizeColumn(i);
            }

            var file = new FileDto(fileName, MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var stream = new MemoryStream())
            {
                workbook.Write(stream);
                _tempFileCacheManager.SetFile(file.FileToken, stream.ToArray());
            }

            return file;
        }

        public FileDto JobGridProductItemExport(List<GetJobProductItemsForExcel> jobProduct, string fileName)
        {
            string[] Header = null;
            Func<GetJobProductItemsForExcel, object>[] Objects = null;

            Header = new string[] { "JobNumber", "Product Type", "ProductItem", "ProductModel", "ProductSize", "Quantity", "State" ,"Teams" };

            IWorkbook workbook = new XSSFWorkbook();

            var sheet = workbook.CreateSheet(fileName);
            AddHeader(sheet, Header);

            //Objects = new Func<GetJobProductItemsForExcel, object>[] {
            //        _ => _.JobNumber,
            //        _ => _.ProductType,
            //        _ => _.ProductItem,
            //        _ => _.ProductModel,
            //        _ => _.ProductSize,
            //        _ => _.Quantity
            //};

            //return CreateExcelPackage(
            //    fileName,
            //    excelPackage =>
            //    {
            //        var sheet = excelPackage.CreateSheet("JobProductItems");
            //        AddHeader(sheet, Header);

            //        AddObjects(sheet, 1, jobProduct, Objects);

            //        for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
            //        {
            //            sheet.AutoSizeColumn(i);
            //        }

            //        //for (var i = 1; i <= InvoiceIssuedListDtos.Count; i++) // You can define up to 64000 style in a .xlsx Workbook
            //        //{
            //        //    if (EmailPermission)
            //        //    {
            //        //        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[6], "0.00");
            //        //    }
            //        //    else
            //        //    {
            //        //        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[5], "0.00");
            //        //    }
            //        //}
            //    });
            for (var i = 0; i < jobProduct.Count; i++)
            {
                var row = sheet.CreateRow(i + 1);

                var cell = row.CreateCell(0);
                cell.SetCellValue(jobProduct[i].JobNumber);
                cell = row.CreateCell(1);
                cell.SetCellValue(jobProduct[i].ProductType);
                cell = row.CreateCell(2);
                cell.SetCellValue(jobProduct[i].ProductItem);
                cell = row.CreateCell(3);
                cell.SetCellValue(jobProduct[i].ProductModel);
                cell = row.CreateCell(4);
                cell.SetCellValue((double)(jobProduct[i].ProductSize != null ? jobProduct[i].ProductSize : 0));
                cell = row.CreateCell(5);
                cell.SetCellValue(jobProduct[i].Quantity != null ? (int)jobProduct[i].Quantity : 0);
                cell = row.CreateCell(6);
                cell.SetCellValue(jobProduct[i].State);
                cell = row.CreateCell(7);
                cell.SetCellValue(jobProduct[i].Teams);
            }

            for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
            {
                sheet.AutoSizeColumn(i);
            }

            var file = new FileDto(fileName, MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var stream = new MemoryStream())
            {
                workbook.Write(stream);
                _tempFileCacheManager.SetFile(file.FileToken, stream.ToArray());
            }

            return file;
        }

        public FileDto InvoiceIssuedExportToFile(List<GetInvoiceIssuedForViewDto> InvoiceIssuedListDtos, string fileName)
        {
            string[] Header = null;
            Func<GetInvoiceIssuedForViewDto, object>[] Objects = null;

            var EmailPermission = _permissionChecker.IsGranted(AppPermissions.Pages_InvoiceIssuedTracker_Export_MobileEmail);

            if (EmailPermission)
            {
                Header = new string[] { "Job Number", "State", "Finance", "Job Status", "Contact", "Mobile", "SalesRep", "Installer", "Installed", "Total", "Balance", "Invoice Notes", "Comment", "Next Followup", "Description", "KW", "STC" };

                //colCount = Header.Count();

                Objects = new Func<GetInvoiceIssuedForViewDto, object>[] {
                    _ => _.Job.JobNumber,
                    _ => _.Job.State,
                    _ => _.PaymentMethod,
                    _ => _.ProjectStatus,
                    _ => _.CompanyName,
                    _ => _.Mobile,
                    _ => _.SalesRep,
                    _ => _.Installer,
                    _ => _.Job.InstallationDate != null ? Convert.ToDateTime(_.Job.InstallationDate).ToString("dd-MM-yyyy") : "",
                    _ => _.TotalCost != null ? Convert.ToDecimal(_.TotalCost) : 0.0m,
                    _ => _.NewOwning != null ? Convert.ToDecimal(_.NewOwning) : 0.0m,
                    _ => _.Job.InvoiceNotes,
                    _ => _.ActivityComment,
                    _ => _.ActivityReminderTime != null ? Convert.ToDateTime(_.ActivityReminderTime).ToString("dd-MM-yyyy") : "",
                    _ => _.ActivityDescription,
                    _ => _.Job.SystemCapacity,
                    _ => _.Job.Stc

                };
            }
            else
            {
                //colCount = Header.Count();

                Header = new string[] { "Job Number", "State", "Finance", "Job Status", "Contact", "SalesRep", "Installer", "Installed", "Total", "Balance", "Invoice NOtes", "Comment", "Next Followup", "Description", "KW", "STC" };

                Objects = new Func<GetInvoiceIssuedForViewDto, object>[] {
                    _ => _.Job.JobNumber,
                    _ => _.Job.State,
                    _ => _.PaymentMethod,
                    _ => _.ProjectStatus,
                    _ => _.CompanyName,
                    _ => _.SalesRep,
                    _ => _.Installer,
                    _ => _.Job.InstallationDate != null ? Convert.ToDateTime(_.Job.InstallationDate).ToString("dd-MM-yyyy") : "",
                    _ => _.TotalCost != null ? Convert.ToDecimal(_.TotalCost) : 0.0m,
                    _ => _.NewOwning != null ? Convert.ToDecimal(_.NewOwning) : 0.0m,
                    _ => _.Job.InvoiceNotes,
                    _ => _.ActivityComment,
                    _ => _.ActivityReminderTime != null ? Convert.ToDateTime(_.ActivityReminderTime).ToString("dd-MM-yyyy") : "",
                    _ => _.ActivityDescription,
                    _ => _.Job.SystemCapacity,
                    _ => _.Job.Stc
                };
            }

            return CreateExcelPackage(
               fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("InvoiceIssued");
                    AddHeader(sheet, Header);

                    AddObjects(sheet, 1, InvoiceIssuedListDtos, Objects);
                    if (EmailPermission)
                    {
                        for (var i = 1; i <= InvoiceIssuedListDtos.Count; i++)
                        {
                            SetCellDataFormat(sheet.GetRow(i).Cells[8], "dd/mm/yyyy");
                            SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "00.00");
                            SetdecimalCellDataFormat(sheet.GetRow(i).Cells[10], "00.00");
                            SetCellDataFormat(sheet.GetRow(i).Cells[13], "dd/mm/yyyy");
                        }
                    }
                    else
                    {
                        for (var i = 1; i <= InvoiceIssuedListDtos.Count; i++)
                        {
                            SetCellDataFormat(sheet.GetRow(i).Cells[7], "dd/mm/yyyy");
                            SetdecimalCellDataFormat(sheet.GetRow(i).Cells[8], "00.00");
                            SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "00.00");
                            SetCellDataFormat(sheet.GetRow(i).Cells[12], "dd/mm/yyyy");
                        }
                    }

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }


                    //for (var i = 1; i <= InvoiceIssuedListDtos.Count; i++)
                    //{
                    //    if (EmailPermission)
                    //    {
                    //        SetCellDataFormat(sheet.GetRow(i).Cells[7], "dd MMM yyyy");

                    //        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[8], "0.00");

                    //        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "0.00");

                    //        SetCellDataFormat(sheet.GetRow(i).Cells[12], "dd-MM-yyyy");
                    //    }
                    //    else
                    //    {
                    //        SetCellDataFormat(sheet.GetRow(i).Cells[6], "dd MMM yyyy");

                    //        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[7], "0.00");

                    //        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[8], "0.00");

                    //        SetCellDataFormat(sheet.GetRow(i).Cells[11], "dd-MM-yyyy");
                    //    }
                    //}
                });
        }

        //public FileDto InvoiceIssuedExportCSVFile(List<GetInvoiceIssuedForViewDto> InvoiceIssuedListDtos)
        //{
        //    return CreateExcelPackage(
        //        "InvoiceIssued.xlsx",
        //        excelPackage =>
        //        {
        //            var sheet = excelPackage.CreateSheet("GridConnectionTracker");
        //            AddHeader(
        //                  sheet,
        //                "ProjectNo",
        //                 L("State"),
        //                L("PaymentMethod"),
        //                L("ProjectStatus"),
        //                L("CompanyName"),
        //                L("Mobile"),
        //                L("InstallerName"),
        //                "InstallationDate",
        //                L("TotalCost"),
        //                "Owing",
        //                L("CreationDate")
        //                );

        //            AddObjects(
        //               sheet, 2, InvoiceIssuedListDtos,
        //               _ => _.Job.JobNumber,
        //                _ => _.StateName,
        //                _ => _.PaymentMethod,
        //                _ => _.ProjectStatus,
        //                 _ => _.CompanyName,
        //                  _ => _.Mobile,
        //                 _ => _.Installer,
        //              _ => _.Job.InstallationDate,
        //                 _ => _.TotalCost,
        //                 _ => _.NewOwning,
        //                _ => _.CreationDate

        //               );
        //            for (var i = 1; i <= InvoiceIssuedListDtos.Count; i++)
        //            {
        //                SetdecimalCellDataFormat(sheet.GetRow(i).Cells[8], "0.00");
        //            }
        //            sheet.AutoSizeColumn(8);
        //            for (var i = 1; i <= InvoiceIssuedListDtos.Count; i++)
        //            {
        //                SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "0.00");
        //            }
        //            sheet.AutoSizeColumn(9);
        //            for (var i = 1; i <= InvoiceIssuedListDtos.Count; i++)
        //            {
        //                SetCellDataFormat(sheet.GetRow(i).Cells[7], "yyyy-mm-dd");
        //            }
        //            sheet.AutoSizeColumn(7);
        //            for (var i = 1; i <= InvoiceIssuedListDtos.Count; i++)
        //            {
        //                SetCellDataFormat(sheet.GetRow(i).Cells[10], "yyyy-mm-dd");
        //            }
        //            sheet.AutoSizeColumn(10);
        //        });
        //}

        public FileDto InvoiceIssuedDepositeExportToFile(List<GetInvoiceIssuedForViewDto> InvoiceIssuedListDtos, string fileName)
        {
            string[] Header = null;
            Func<GetInvoiceIssuedForViewDto, object>[] Objects = null;

            var EmailPermission = _permissionChecker.IsGranted(AppPermissions.Pages_InvoiceIssuedTracker_Export_MobileEmail);

            if (EmailPermission)
            {
                Header = new string[] { L("JobNumber"), L("CompanyName"), L("Mobile"), L("SalesRep"), L("State"), L("DepositRequired"), L("JobStatus") };

                Objects = new Func<GetInvoiceIssuedForViewDto, object>[] {
                    _ => _.Job.JobNumber,
                    _ => _.CompanyName,
                    _ => _.Mobile,
                    _ => _.Installer,
                    _ => _.Job.State,
                    _ => _.DepositRequired != null ? Convert.ToDecimal(_.DepositRequired) : 0.0m,
                    _ => _.ProjectStatus
                };
            }
            else
            {
                Header = new string[] { L("JobNumber"), L("CompanyName"), L("SalesRep"), L("State"), L("DepositRequired"), L("JobStatus") };

                Objects = new Func<GetInvoiceIssuedForViewDto, object>[] {
                    _ => _.Job.JobNumber,
                    _ => _.CompanyName,
                    _ => _.Installer,
                    _ => _.Job.State,
                    _ => _.DepositRequired != null ? Convert.ToDecimal(_.DepositRequired) : 0.0m,
                    _ => _.ProjectStatus
                };
            }

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("Deposit");
                    AddHeader(sheet, Header);

                    AddObjects(sheet, 1, InvoiceIssuedListDtos, Objects);

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                    //for (var i = 1; i <= InvoiceIssuedListDtos.Count; i++) // You can define up to 64000 style in a .xlsx Workbook
                    //{
                    //    if (EmailPermission)
                    //    {
                    //        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[6], "0.00");
                    //    }
                    //    else
                    //    {
                    //        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[5], "0.00");
                    //    }
                    //}
                });
        }

        public FileDto InvoiceIssuedOwingExportToFile(List<GetInvoiceIssuedForViewDto> InvoiceIssuedListDtos, string fileName)
        {
            string[] Header = new string[] { L("InvoiceNumber"), L("State"), L("FinanceWith"), L("JobStatus"), L("CompanyName"), L("InstallBookingDate"), L("TotalQuotePrice"), L("BalOwing"), L("SolarVICRebate"), L("SolarVICLoanDiscont") };

            Func<GetInvoiceIssuedForViewDto, object>[] Objects = new Func<GetInvoiceIssuedForViewDto, object>[] {
                _ => _.Job.JobNumber,
                _ => _.Job.State,
                _ => _.FinanceWith,
                _ => _.ProjectStatus,
                _ => _.CompanyName,
                _ => _.Job.InstallationDate != null ? Convert.ToDateTime(_.Job.InstallationDate).ToString("dd-MM-yyyy") : "",
                _ => _.TotalCost != null ? Convert.ToDecimal(_.TotalCost) : 0.0m,
                _ => _.NewOwning != null ? Convert.ToDecimal(_.NewOwning) : 0.0m,
                _ => _.Job.SolarVICRebate,
                _ => _.Job.SolarVICLoanDiscont
            };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("Owing");
                    AddHeader(sheet, Header);

                    AddObjects(sheet, 1, InvoiceIssuedListDtos, Objects);

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                    //for (var i = 1; i <= InvoiceIssuedListDtos.Count; i++) // You can define up to 64000 style in a .xlsx Workbook
                    //{
                    //    SetCellDataFormat(sheet.GetRow(i).Cells[5], "dd MMM yyyy");

                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[6], "0.00");

                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[7], "0.00");

                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[8], "0.00");

                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "0.00");
                    //}
                });
        }



        public FileDto ExportToFile(List<GetLeadForViewDto> leads)
        {
            var EmailPermission = _permissionChecker.IsGranted(AppPermissions.Pages_ManageLeads_Export_MobileEmail);
            var LeadSourcePermission = _permissionChecker.IsGranted(AppPermissions.Pages_MyLeads_ShowLeadSource);

            if (EmailPermission)
            {
                return CreateExcelPackage(
                "Leads.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Leads"));

                    AddHeader(
                        sheet,
                        LeadSourcePermission
                        ? new string[]
                        {
                    L("CopanyName"),
                    L("Email"),
                    L("Phone"),
                    L("Mobile"),
                    L("Address"),
                    L("Requirements"),
                    (L("PostCode")) + L("Suburb"),
                    (L("State")) + L("Name"),
                    (L("PostCode")) + L("PostalCode"),
                    (L("LeadSource")) + L("Name")
                        }
                        : new string[]
                        {
                    L("CopanyName"),
                    L("Email"),
                    L("Phone"),
                    L("Mobile"),
                    L("Address"),
                    L("Requirements"),
                    (L("PostCode")) + L("Suburb"),
                    (L("State")) + L("Name"),
                    (L("PostCode")) + L("PostalCode")
                        }
                    );

                    AddObjects(
                        sheet, 2, leads,
                        LeadSourcePermission
                        ? new Func<GetLeadForViewDto, object>[]
                        {
                    _ => _.Lead.CompanyName,
                    _ => _.Lead.Email,
                    _ => _.Lead.Phone,
                    _ => _.Lead.Mobile,
                    _ => _.Lead.Address,
                    _ => _.Lead.Requirements,
                    _ => _.PostCodeSuburb,
                    _ => _.StateName,
                    _ => _.Lead.PostCode,
                    _ => _.LeadSourceName
                        }
                        : new Func<GetLeadForViewDto, object>[]
                        {
                    _ => _.Lead.CompanyName,
                    _ => _.Lead.Email,
                    _ => _.Lead.Phone,
                    _ => _.Lead.Mobile,
                    _ => _.Lead.Address,
                    _ => _.Lead.Requirements,
                    _ => _.PostCodeSuburb,
                    _ => _.StateName,
                    _ => _.Lead.PostCode
                        }
                    );
                });
            }
            else
            {
                return CreateExcelPackage(
                "Leads.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Leads"));

                    AddHeader(
                        sheet,
                        LeadSourcePermission
                        ? new string[]
                        {
                    L("CopanyName"),
                    L("Address"),
                    L("Requirements"),
                    (L("PostCode")) + L("Suburb"),
                    (L("State")) + L("Name"),
                    (L("PostCode")) + L("PostalCode"),
                    (L("LeadSource")) + L("Name")
                        }
                        : new string[]
                        {
                    L("CopanyName"),
                    L("Address"),
                    L("Requirements"),
                    (L("PostCode")) + L("Suburb"),
                    (L("State")) + L("Name"),
                    (L("PostCode")) + L("PostalCode")
                        }
                    );

                    AddObjects(
                        sheet, 2, leads,
                        LeadSourcePermission
                        ? new Func<GetLeadForViewDto, object>[]
                        {
                    _ => _.Lead.CompanyName,
                    _ => _.Lead.Address,
                    _ => _.Lead.Requirements,
                    _ => _.PostCodeSuburb,
                    _ => _.StateName,
                    _ => _.Lead.PostCode,
                    _ => _.LeadSourceName
                        }
                        : new Func<GetLeadForViewDto, object>[]
                        {
                    _ => _.Lead.CompanyName,
                    _ => _.Lead.Address,
                    _ => _.Lead.Requirements,
                    _ => _.PostCodeSuburb,
                    _ => _.StateName,
                    _ => _.Lead.PostCode
                        }
                    );
                });
            }
        }


        public FileDto ExportCsvToFile(List<GetLeadForViewDto> leads)
        {
            var LeadSourcePermission = _permissionChecker.IsGranted(AppPermissions.Pages_MyLeads_ShowLeadSource);
            var EmailPermission = _permissionChecker.IsGranted(AppPermissions.Pages_ManageLeads_Export_MobileEmail);
            if (EmailPermission)
            {
                return CreateExcelPackage(
                "Leads.csv",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("Leads"));

                    AddHeader(
                        sheet,
                        L("CopanyName"),
                        L("Email"),
                        L("Phone"),
                        L("Mobile"),
                        L("Address"),
                        L("Requirements"),
                        (L("PostCode")) + L("Suburb"),
                        (L("State")) + L("Name"),
                        (L("PostCode")) + L("PostalCode"),
                        (L("LeadSource")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, leads,
                        _ => _.Lead.CompanyName,
                        _ => _.Lead.Email,
                        _ => _.Lead.Phone,
                        _ => _.Lead.Mobile,
                        _ => _.Lead.Address,
                        _ => _.Lead.Requirements,
                        _ => _.PostCodeSuburb,
                        _ => _.StateName,
                        _ => _.Lead.PostCode,
                        _ => _.LeadSourceName
                        );



                });
            }
            else
            {
                return CreateExcelPackage(
                "Leads.csv",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("Leads"));

                    AddHeader(
                        sheet,
                        L("CopanyName"),
                        L("Address"),
                        L("Requirements"),
                        (L("PostCode")) + L("Suburb"),
                        (L("State")) + L("Name"),
                        (L("PostCode")) + L("PostalCode"),
                        (L("LeadSource")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, leads,
                        _ => _.Lead.CompanyName,
                        _ => _.Lead.Address,
                        _ => _.Lead.Requirements,
                        _ => _.PostCodeSuburb,
                        _ => _.StateName,
                        _ => _.Lead.PostCode,
                        _ => _.LeadSourceName
                        );



                });
            }
        }

        public FileDto DublicateLeadExportToFile(List<GetDuplicateLeadForViewDto> dublicateleadListDtos)
        {
            var EmailPermission = _permissionChecker.IsGranted(AppPermissions.Pages_Lead_Duplicate_Export_MobileEmail);
            var LeadSourcePermission = _permissionChecker.IsGranted(AppPermissions.Pages_MyLeads_ShowLeadSource);

            if (EmailPermission)
            {
                return CreateExcelPackage(
                "DublicateLead.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("DublicateLead");

                    AddHeader(
                        sheet,
                        LeadSourcePermission
                        ? new string[]
                        {
                    L("Customer"),
                    L("Source"),
                    L("Organization"),
                    L("Email"),
                    L("Mobile"),
                    L("Address"),
                    L("Suburb"),
                    L("State"),
                    L("PostCode"),
                    L("CreationTime")
                        }
                        : new string[]
                        {
                    L("Customer"),
                    L("Organization"),
                    L("Email"),
                    L("Mobile"),
                    L("Address"),
                    L("Suburb"),
                    L("State"),
                    L("PostCode"),
                    L("CreationTime")
                        }
                    );

                    AddObjects(
                        sheet, 2, dublicateleadListDtos,
                        LeadSourcePermission
                        ? new Func<GetDuplicateLeadForViewDto, object>[]
                        {
                    _ => _.DuplicateLead.CompanyName,
                    _ => _.DuplicateLead.LeadSource,
                    _ => _.DuplicateLead.OrganizationName,
                    _ => _.DuplicateLead.Email,
                    _ => _.DuplicateLead.Mobile,
                    _ => _.DuplicateLead.Address,
                    _ => _.DuplicateLead.Suburb,
                    _ => _.StateName,
                    _ => _.DuplicateLead.PostCode,
                    _ => _timeZoneConverter.Convert(_.DuplicateLead.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())
                        }
                        : new Func<GetDuplicateLeadForViewDto, object>[]
                        {
                    _ => _.DuplicateLead.CompanyName,
                    _ => _.DuplicateLead.OrganizationName,
                    _ => _.DuplicateLead.Email,
                    _ => _.DuplicateLead.Mobile,
                    _ => _.DuplicateLead.Address,
                    _ => _.DuplicateLead.Suburb,
                    _ => _.StateName,
                    _ => _.DuplicateLead.PostCode,
                    _ => _timeZoneConverter.Convert(_.DuplicateLead.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())
                        }
                    );

                    var dateColumnIndex = LeadSourcePermission ? 9 : 8; // Adjust index based on permission
                    for (var i = 1; i <= dublicateleadListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[dateColumnIndex], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(dateColumnIndex + 1);
                });
            }
            else
            {
                return CreateExcelPackage(
                "DublicateLead.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("DublicateLead");

                    AddHeader(
                        sheet,
                        LeadSourcePermission
                        ? new string[]
                        {
                    L("Customer"),
                    L("Source"),
                    L("Organization"),
                    L("Address"),
                    L("Suburb"),
                    L("State"),
                    L("PostCode"),
                    L("CreationTime")
                        }
                        : new string[]
                        {
                    L("Customer"),
                    L("Organization"),
                    L("Address"),
                    L("Suburb"),
                    L("State"),
                    L("PostCode"),
                    L("CreationTime")
                        }
                    );

                    AddObjects(
                        sheet, 2, dublicateleadListDtos,
                        LeadSourcePermission
                        ? new Func<GetDuplicateLeadForViewDto, object>[]
                        {
                    _ => _.DuplicateLead.CompanyName,
                    _ => _.DuplicateLead.LeadSource,
                    _ => _.DuplicateLead.OrganizationName,
                    _ => _.DuplicateLead.Address,
                    _ => _.DuplicateLead.Suburb,
                    _ => _.StateName,
                    _ => _.DuplicateLead.PostCode,
                    _ => _timeZoneConverter.Convert(_.DuplicateLead.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())
                        }
                        : new Func<GetDuplicateLeadForViewDto, object>[]
                        {
                    _ => _.DuplicateLead.CompanyName,
                    _ => _.DuplicateLead.OrganizationName,
                    _ => _.DuplicateLead.Address,
                    _ => _.DuplicateLead.Suburb,
                    _ => _.StateName,
                    _ => _.DuplicateLead.PostCode,
                    _ => _timeZoneConverter.Convert(_.DuplicateLead.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())
                        }
                    );

                    var dateColumnIndex = LeadSourcePermission ? 8 : 7; // Adjust index based on permission
                    for (var i = 1; i <= dublicateleadListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[dateColumnIndex], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(dateColumnIndex + 1);
                });
            }
        }

        public FileDto DublicateLeadCsvExportToFile(List<GetDuplicateLeadForViewDto> dublicateleadListDtos)
        {
            var EmailPermission = _permissionChecker.IsGranted(AppPermissions.Pages_Lead_Duplicate_Export_MobileEmail);
            if (EmailPermission)
            {
                return CreateExcelPackage(
                "DublicateLead.csv",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet("DublicateLead");

                    AddHeader(
                        sheet,
                        L("Coustomer"),
                        L("Source"),
                        L("Organization"),
                        L("Email"),
                        L("Phone"),
                        L("Mobile"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostCode"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, dublicateleadListDtos,
                        _ => _.DuplicateLead.CompanyName,
                         _ => _.DuplicateLead.LeadSource,
                         _ => _.DuplicateLead.OrganizationName,
                        _ => _.DuplicateLead.Email,
                        _ => _.DuplicateLead.Phone,
                        _ => _.DuplicateLead.Mobile,
                        _ => _.DuplicateLead.Address,
                        _ => _.DuplicateLead.Suburb,
                        _ => _.StateName,
                        _ => _.DuplicateLead.PostCode,
                         _ => _timeZoneConverter.Convert(_.DuplicateLead.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())
                        );

                    //for (var i = 1; i <= dublicateleadListDtos.Count; i++)
                    //{
                    //    SetCellDataFormat(sheet.GetRow(i).Cells[11], "yyyy-mm-dd");
                    //}
                    for (var i = 1; i <= dublicateleadListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[10], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(10);

                });
            }
            else
            {
                return CreateExcelPackage(
                "DublicateLead.csv",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet("DublicateLead");

                    AddHeader(
                        sheet,
                        L("Coustomer"),
                        L("Source"),
                        L("Organization"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostCode"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, dublicateleadListDtos,
                        _ => _.DuplicateLead.CompanyName,
                         _ => _.DuplicateLead.LeadSource,
                         _ => _.DuplicateLead.OrganizationName,
                        _ => _.DuplicateLead.Address,
                        _ => _.DuplicateLead.Suburb,
                        _ => _.StateName,
                        _ => _.DuplicateLead.PostCode,
                         _ => _timeZoneConverter.Convert(_.DuplicateLead.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())
                        );

                    //for (var i = 1; i <= dublicateleadListDtos.Count; i++)
                    //{
                    //    SetCellDataFormat(sheet.GetRow(i).Cells[11], "yyyy-mm-dd");
                    //}
                    for (var i = 1; i <= dublicateleadListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[7], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(10);

                });
            }
        }
        public FileDto ClosedLeadExportToFile(List<GetLeadForViewDto> closedleadListDtos)
        {
            var EmailPermission = _permissionChecker.IsGranted(AppPermissions.Pages_Lead_Closed_Export_MobileEmail);
            if (EmailPermission)
            {
                return CreateExcelPackage(
                "ClosedLead.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("ClosedLead"));

                    AddHeader(
                        sheet,
                        L("Customer"),
                        L("Organization"),
                        L("Email"),
                        L("Phone"),
                        L("Mobile"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostalCode")
                        );

                    AddObjects(
                       sheet, 2, closedleadListDtos,
                       _ => _.Lead.CompanyName,
                       _ => _.Lead.OrganizationName,
                       _ => _.Lead.Email,
                       _ => _.Lead.Phone,
                       _ => _.Lead.Mobile,
                       _ => _.Lead.Address,
                       _ => _.PostCodeSuburb,
                       _ => _.StateName,
                       _ => _.Lead.PostCode
                       );


                });
            }
            else
            {
                return CreateExcelPackage(
                "ClosedLead.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("ClosedLead"));

                    AddHeader(
                        sheet,
                        L("Customer"),
                        L("Organization"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostalCode")
                        );

                    AddObjects(
                       sheet, 2, closedleadListDtos,
                       _ => _.Lead.CompanyName,
                       _ => _.Lead.OrganizationName,
                       _ => _.Lead.Address,
                       _ => _.PostCodeSuburb,
                       _ => _.StateName,
                       _ => _.Lead.PostCode
                       );


                });
            }
                
        }
        public FileDto ClosedLeadCsvExport(List<GetLeadForViewDto> closedleadListDtos)
        {
            var EmailPermission = _permissionChecker.IsGranted(AppPermissions.Pages_Lead_Closed_Export_MobileEmail);
            if (EmailPermission)
            {
                return CreateExcelPackage(
                "ClosedLead.csv",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("ClosedLead"));

                    AddHeader(
                        sheet,
                        L("Customer"),
                        L("Organization"),
                        L("Email"),
                        L("Phone"),
                        L("Mobile"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostalCode")
                        );

                    AddObjects(
                       sheet, 2, closedleadListDtos,
                       _ => _.Lead.CompanyName,
                       _ => _.Lead.OrganizationName,
                       _ => _.Lead.Email,
                       _ => _.Lead.Phone,
                       _ => _.Lead.Mobile,
                       _ => _.Lead.Address,
                       _ => _.PostCodeSuburb,
                       _ => _.StateName,
                       _ => _.Lead.PostCode
                       );


                });
            }
            else
            {
                return CreateExcelPackage(
                "ClosedLead.csv",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("ClosedLead"));

                    AddHeader(
                        sheet,
                        L("Customer"),
                        L("Organization"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostalCode")
                        );

                    AddObjects(
                       sheet, 2, closedleadListDtos,
                       _ => _.Lead.CompanyName,
                       _ => _.Lead.OrganizationName,
                       _ => _.Lead.Address,
                       _ => _.PostCodeSuburb,
                       _ => _.StateName,
                       _ => _.Lead.PostCode
                       );


                });
            }

        }
        public FileDto CancelLeadExportToFile(List<GetLeadForViewDto> cancelleadListDtos)
        {
            return CreateExcelPackage(
                "CancelLead.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("CancelLead"));
                    AddHeader(
                        sheet,
                        L("Customer"),
                        L("Organization"),
                        L("Email"),
                        L("LeadSource"),
                        L("Mobile"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostalCode"),
                        L("CancelReason"),
                        L("Note"),
                        L("LastModifiedBy")
                        );

                    AddObjects(
                       sheet, 2, cancelleadListDtos,
                       _ => _.Lead.CompanyName,
                       _ => _.Lead.OrganizationName,
                       _ => _.Lead.Email,
                       _ => _.LeadSourceName,
                        _ => _.Lead.Mobile,
                       _ => _.Lead.Address,
                       _ => _.PostCodeSuburb,
                       _ => _.StateName,
                       _ => _.Lead.PostCode,
                       _ => _.CancelReasonName,
                       _ => _.Lead.RejectReason,
                       _ => _.LastModifiedUser
                       );
                });
        }
        public FileDto CancelLeadCsvExport(List<GetLeadForViewDto> cancelleadListDtos)
        {
            return CreateExcelPackage(
                "CancelLead.csv",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("CancelLead"));
                    AddHeader(
                        sheet,
                        L("Customer"),
                        L("Organization"),
                        L("Email"),
                        L("LeadSource"),
                        L("Mobile"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostalCode"),
                        L("CancelReason"),
                        L("Note"),
                        L("LastModifiedBy")
                        );

                    AddObjects(
                       sheet, 2, cancelleadListDtos,
                       _ => _.Lead.CompanyName,
                       _ => _.Lead.OrganizationName,
                       _ => _.Lead.Email,
                       _ => _.LeadSourceName,
                        _ => _.Lead.Mobile,
                       _ => _.Lead.Address,
                       _ => _.PostCodeSuburb,
                       _ => _.StateName,
                       _ => _.Lead.PostCode,
                       _ => _.CancelReasonName,
                       _ => _.Lead.RejectReason,
                       _ => _.LastModifiedUser
                       );
                });
        }
        public FileDto RejectLeadExportToFile(List<GetLeadForViewDto> rejectleadListDtos)
        {
            return CreateExcelPackage(
                "RejectLead.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("RejectLead"));
                    AddHeader(
                        sheet,
                        L("Customer"),
                        L("Organization"),
                        L("Email"),
                        L("LeadSource"),
                        L("Mobile"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostalCode"),
                        L("RejectReason"),
                        L("Note"),
                        L("LastModifiedBy")
                        );

                    AddObjects(
                       sheet, 2, rejectleadListDtos,
                       _ => _.Lead.CompanyName,
                       _ => _.Lead.OrganizationName,
                       _ => _.Lead.Email,
                       _ => _.LeadSourceName,
                       _ => _.Lead.Mobile,
                       _ => _.Lead.Address,
                       _ => _.PostCodeSuburb,
                       _ => _.StateName,
                       _ => _.Lead.PostCode,
                       _ => _.CancelReasonName,
                       _ => _.Lead.RejectReason,
                       _ => _.LastModifiedUser
                       );
                });
        }

        public FileDto RejectLeadCsvExport(List<GetLeadForViewDto> rejectleadListDtos)
        {
            return CreateExcelPackage(
                "RejectLead.csv",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("RejectLead"));
                    AddHeader(
                        sheet,
                        L("Customer"),
                        L("Organization"),
                        L("Email"),
                        L("LeadSource"),
                        L("Mobile"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostalCode"),
                        L("RejectReason"),
                        L("Note"),
                        L("LastModifiedBy")
                        );

                    AddObjects(
                       sheet, 2, rejectleadListDtos,
                       _ => _.Lead.CompanyName,
                       _ => _.Lead.OrganizationName,
                       _ => _.Lead.Email,
                       _ => _.LeadSourceName,
                       _ => _.Lead.Mobile,
                       _ => _.Lead.Address,
                       _ => _.PostCodeSuburb,
                       _ => _.StateName,
                       _ => _.Lead.PostCode,
                       _ => _.CancelReasonName,
                       _ => _.Lead.RejectReason,
                       _ => _.LastModifiedUser
                       );
                });
        }


        public FileDto MyLeadExportToFile(List<GetLeadForViewDto> myleadListDtos)
        {
            var LeadSourcePermission = _permissionChecker.IsGranted(AppPermissions.Pages_MyLeads_ShowLeadSource);

            return CreateExcelPackage(
                "MyLead.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("MyLead"));

                    if (LeadSourcePermission)
                    {
                        AddHeader(
                            sheet,
                            L("Customer"),
                            L("Mobile"),
                            L("Address"),
                            L("Suburb"),
                            L("State"),
                            L("PostalCode"),
                            L("FollowUpDate"),
                            L("LeadSource"),
                            L("Organization"),
                            L("LeadAssignDate")
                        );

                        AddObjects(
                           sheet, 2, myleadListDtos,
                           _ => _.Lead.CompanyName,
                           _ => _.Lead.Mobile,
                           _ => _.Lead.Address,
                           _ => _.PostCodeSuburb,
                           _ => _.StateName,
                           _ => _.Lead.PostCode,
                           _ => _timeZoneConverter.Convert(_.Lead.FollowupDate, _abpSession.TenantId, _abpSession.GetUserId()),
                           _ => _.LeadSourceName, // Included LeadSource if permission granted
                           _ => _.Lead.OrganizationName,
                           _ => _timeZoneConverter.Convert(_.Lead.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())
                        );
                    }
                    else
                    {
                        AddHeader(
                            sheet,
                            L("Customer"),
                            L("Mobile"),
                            L("Address"),
                            L("Suburb"),
                            L("State"),
                            L("PostalCode"),
                            L("FollowUpDate"),
                            L("Organization"),
                            L("LeadAssignDate")
                        );

                        AddObjects(
                           sheet, 2, myleadListDtos,
                           _ => _.Lead.CompanyName,
                           _ => _.Lead.Mobile,
                           _ => _.Lead.Address,
                           _ => _.PostCodeSuburb,
                           _ => _.StateName,
                           _ => _.Lead.PostCode,
                           _ => _timeZoneConverter.Convert(_.Lead.FollowupDate, _abpSession.TenantId, _abpSession.GetUserId()),
                           _ => _.Lead.OrganizationName,
                           _ => _timeZoneConverter.Convert(_.Lead.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())
                        );
                    }

                    var followUpDateColumnIndex = LeadSourcePermission ? 6 : 6; // Adjust if LeadSource is included
                    var assignDateColumnIndex = LeadSourcePermission ? 9 : 8;  // Adjust if LeadSource is excluded

                    for (var i = 1; i <= myleadListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[followUpDateColumnIndex], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(followUpDateColumnIndex);

                    for (var i = 1; i <= myleadListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[assignDateColumnIndex], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(assignDateColumnIndex);
                });
        }


        public FileDto MyLeadCsvExport(List<GetLeadForViewDto> myleadListDtos)
        {
            return CreateExcelPackage(
                "MyLead.csv",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("MyLead"));
                    AddHeader(
                        sheet,
                        L("Customer"),
                        L("Mobile"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostalCode"),
                        L("FollowUpDate"),
                        L("LeadSource"),
                        L("Organization"),
                        L("LeadAssignDate")
                        );

                    AddObjects(
                       sheet, 2, myleadListDtos,
                       _ => _.Lead.CompanyName,
                       _ => _.Lead.Mobile,
                       _ => _.Lead.Address,
                       _ => _.PostCodeSuburb,
                       _ => _.StateName,
                       _ => _.Lead.PostCode,
                       _ => _.Lead.FollowupDate,
                       _ => _.LeadSourceName,
                       _ => _.Lead.OrganizationName,
                       _ => _timeZoneConverter.Convert(_.Lead.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())
                       );
                });
        }
        public FileDto ApplicationTrackerExportToFile(List<GetJobForViewDto> applicationtrackerListDtos)
        {
            return CreateExcelPackage(
                "ApplicationTracker.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("ApplicationTracker"));
                    AddHeader(
                        sheet,
                        L("ProjectNo"),
                        L("JobType"),
                        L("JobStatus"),
                        L("CompanyName"),
                        L("Address"),
                        "EleDist",
                        "DistAppliedDate",
                        "ApproveDate",
                        "DistExpDate",
                        "NotAppliedDays",
                        "NotApprovedDays",
                        "ExpireDays",
                        "AppRefNo",
                        "SmsSend",
                        "EmailSend",
                        "Comment",
                        "Next Followup",
                        "Discription"
                        );

                    AddObjects(
                       sheet, 2, applicationtrackerListDtos,
                       _ => _.Job.JobNumber,
                       _ => _.JobTypeName,
                       _ => _.JobStatusName,
                       _ => _.LeadCompanyName,
                       _ => _.Job.Address,
                       _ => _.ElecDistributorName,
                       _ => _.Job.DistAppliedDate,
                       _ => _.Job.DistApproveDate,
                       _ => _.Job.DistExpiryDate,
                       _ => _.NotAppliedDays,
                       _ => _.NotApprovedDays,
                       _ => _.ExpiredDays,
                       _ => _.Job.ApplicationRefNo,
                       _ => _.Job.SmsSend != true ? "No" : "Yes",
                       _ => _.Job.EmailSend != true ? "No" : "Yes",
                       _ => _.Job.Comment,
                       _ => _.Job.NextFollowup,
                       _ => _.Job.Discription
                       );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                    for (var i = 1; i <= applicationtrackerListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[6], "dd/mm/yyyy");
                        SetCellDataFormat(sheet.GetRow(i).Cells[7], "dd/mm/yyyy");
                        SetCellDataFormat(sheet.GetRow(i).Cells[8], "dd/mm/yyyy");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[10], "00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[11], "00");
                    }
                });
        }
        public FileDto ApplicationTrackerCsvExport(List<GetJobForViewDto> applicationtrackerListDtos)
        {
            return CreateExcelPackage(
                "ApplicationTracker.csv",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("ApplicationTracker"));
                    AddHeader(
                        sheet,
                        L("ProjectNo"),
                        L("JobType"),
                        L("JobStatus"),
                        L("CompanyName"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostalCode"),
                        L("EleDist"),
                        L("ApplicationRefNo"),
                        L("LeadOwner")
                        );

                    AddObjects(
                       sheet, 2, applicationtrackerListDtos,
                       _ => _.Job.JobNumber,
                       _ => _.JobTypeName,
                       _ => _.JobStatusName,
                       _ => _.LeadCompanyName,
                         _ => _.Job.Address,
                        _ => _.Job.Suburb,
                        _ => _.Job.State,
                       _ => _.Job.PostalCode,
                         _ => _.ElecDistributorName,
                       _ => _.Job.ApplicationRefNo,
                       _ => _.Job.CurrentLeadOwaner
                       );
                });
        }
        public FileDto EssentialTrackerExportToFile(List<GetJobForViewDto> EssentialTrackerListDtos)
        {
            return CreateExcelPackage(
                "EssentialTracker.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("EssentialTracker"));
                    AddHeader(
                        sheet,
                        L("ProjectNo"),
                        "CompanyName",
                        "Email",
                        "Mobile",
                        "Address"

                        );

                    AddObjects(
                       sheet, 2, EssentialTrackerListDtos,
                       _ => _.Job.JobNumber,
                       _ => _.Job.CompanyName,
                        _ => _.Job.Email,
                         _ => _.Job.Mobile,
                        _ => _.Job.Address + " " + _.Job.Suburb + " " + _.State + " " + _.Job.PostalCode


                       );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                   
                });
        }
        public FileDto FinanaceTrackerExportToFile(List<GetViewFinanceTrackerDto> financetrackerListDtos, string filename)
        {
            return CreateExcelPackage(
                filename,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("FinanceTracker");
                    AddHeader(
                        sheet,
                        L("ProjectNo"),
                        "FinancePurchaseNo",
                        "DocumentVerified",
                        "JobStatus",
                        "CompanyName",
                        "Address",
                        "SmsSend",
                        "EmailSend",
                        "CurrentLeadOwaner",
                        "NotVerifiedDays",
                        "ReminderDate",
                        "Reminder",
                        "Comment"
                        );

                    AddObjects(
                       sheet, 2, financetrackerListDtos,
                       _ => _.JobNumber,
                       _ => _.FinancePurchaseNo,
                       _ => _.FinanceDocumentVerified == 3 ? "Query" : (_.FinanceDocumentVerified == 1 ? "Verify" : "NotVerify"),
                       _ => _.JobStatus,
                       _ => _.CompanyName,
                        _ => _.Address + " " + _.Suburb + " " + _.State + " " + _.PostalCode,
                         _ => _.FinanceSmsSend != true ? "No" : "Yes",
                         _ => _.FinanceEmailSend != true ? "No" : "Yes",
                         _ => _.CurrentLeadOwaner,
                        _ => _.NotVerifiedDays,
                        _ => _.ActivityReminderTime,
                        _ => _.ActivityDescription,
                       _ => _.ActivityComment
                       );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                    for (var i = 1; i <= financetrackerListDtos.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "00");
                        SetCellDataFormat(sheet.GetRow(i).Cells[10], "dd/mm/yyyy");
                    }
                });
        }
        public FileDto FinanaceTrackerCsvExport(List<GetJobForViewDto> financetrackerListDtos)
        {
            return CreateExcelPackage(
                "FinanceTracker.csv",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("FinanceTracker"));
                    AddHeader(
                        sheet,
                        L("ProjectNo"),
                        L("JobType"),
                        L("JobStatus"),
                        L("CompanyName"),
                         L("FinancePurchaseNo"),
                         L("Mobile"),
                         L("Email"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostalCode"),
                        L("LeadOwner")
                        );

                    AddObjects(
                       sheet, 2, financetrackerListDtos,
                       _ => _.Job.JobNumber,
                       _ => _.JobTypeName,
                       _ => _.JobStatusName,
                       _ => _.LeadCompanyName,
                        _ => _.Job.FinancePurchaseNo,
                        _ => _.Email,
                         _ => _.Mobile,
                         _ => _.Job.Address,
                        _ => _.Job.Suburb,
                        _ => _.Job.State,
                       _ => _.Job.PostalCode,
                       _ => _.Job.CurrentLeadOwaner
                       );
                });
        }
        //public FileDto JobActiveTrackerExportToFile(List<GetJobForViewDto> activetrackerListDtos)
        //{
        //    return CreateExcelPackage(
        //           "JobActiveTracker.xlsx",
        //        excelPackage =>
        //        {
        //            var sheet = excelPackage.CreateSheet("JobActiveTracker");
        //            AddHeader(
        //                sheet,
        //                L("ProjectNo"),
        //                L("JobType"),
        //                L("JobStatus"),
        //                L("CompanyName"),
        //                L("LeadOwner")
        //                );

        //            AddObjects(
        //               sheet, 2, activetrackerListDtos,
        //               _ => _.Job.JobNumber,
        //               _ => _.JobTypeName,
        //               _ => _.JobStatusName,
        //               _ => _.LeadCompanyName,
        //               _ => _.Job.CurrentLeadOwaner
        //               );
        //        });
        //}
        //public FileDto JobActiveTrackerCsvExport(List<GetJobForViewDto> activetrackerListDtos)
        //{
        //    return CreateExcelPackage(
        //           "JobActiveTracker.csv",
        //        excelPackage =>
        //        {
        //            var sheet = excelPackage.CreateSheet("JobActiveTracker");
        //            AddHeader(
        //                sheet,
        //                L("ProjectNo"),
        //                L("JobType"),
        //                L("JobStatus"),
        //                L("CompanyName"),
        //                L("LeadOwner")
        //                );

        //            AddObjects(
        //               sheet, 2, activetrackerListDtos,
        //               _ => _.Job.JobNumber,
        //               _ => _.JobTypeName,
        //               _ => _.JobStatusName,
        //               _ => _.LeadCompanyName,
        //               _ => _.Job.CurrentLeadOwaner
        //               );
        //        });
        //}


        public FileDto STCExportToFile(List<GetJobForViewDto> stcListDtos)
        {
            return CreateExcelPackage(
                "STCTracker.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("STCTracker");
                    AddHeader(
                          sheet,
                        L("ProjectNo"),
                        L("JobType"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostalCode"), L("InstallationDate"),
                        L("STCAppData"),
                        L("PVDNo"),
                        L("PVDStatus"),
                        L("JobStatus"),
                         L("KW"),
                         L("STC"),
                        L("StcTotalPrice"),
                        L("InstallerName"),
                        L("STCNotes"),
                        L("LeadOwner"),
                        L("Reminder")
                        );

                    AddObjects(
                       sheet, 2, stcListDtos,
                       _ => _.Job.JobNumber,
                       _ => _.JobTypeName,
                       _ => _.Job.Address,
                       _ => _.Job.Suburb,
                       _ => _.Job.State,
                       _ => _.Job.PostalCode,
                       _ => _timeZoneConverter.Convert(_.Job.InstallationDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _timeZoneConverter.Convert(_.Job.STCAppliedDate, _abpSession.TenantId, _abpSession.GetUserId()),
                       _ => _.Job.PVDNumber,
                       _ => _.Job.PVDStatusName,
                       _ => _.JobStatusName,
                       _ => _.Job.SystemCapacity,
                       _ => _.Job.Stc,
                        _ => _.Job.StcTotalPrice,
                       _ => _.Job.InstallerName,
                       _ => _.Job.STCNotes,
                        _ => _.Job.CurrentLeadOwaner,
                        _ => _.LastComment
                       );

                    //for (var i = 1; i <= stcListDtos.Count; i++)
                    //{
                    //    SetCellDataFormat(sheet.GetRow(i).Cells[7], "yyyy-mm-dd");
                    //}
                    //sheet.AutoSizeColumn(7);
                    for (var i = 1; i <= stcListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[6], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(6);
                    for (var i = 1; i <= stcListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[7], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(7);
                    for (var i = 1; i <= stcListDtos.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[11], "0.00");
                    }
                    sheet.AutoSizeColumn(11);
                    for (var i = 1; i <= stcListDtos.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[12], "0.00");
                    }
                    sheet.AutoSizeColumn(12);
                    for (var i = 1; i <= stcListDtos.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[13], "0.00");
                    }
                    sheet.AutoSizeColumn(13);
                });
        }
        public FileDto STCCsvExport(List<GetJobForViewDto> stcListDtos)
        {
            return CreateExcelPackage(
                "STCTracker.csv",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("STCTracker");
                    AddHeader(
                          sheet,
                        L("ProjectNo"),
                        L("JobType"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostalCode"), L("InstallationDate"),
                        L("STCAppData"),
                        L("PVDNo"),
                        L("PVDStatus"),
                        L("JobStatus"),
                         L("KW"),
                         L("STC"),
                        L("StcTotalPrice"),
                        L("InstallerName"),
                        L("STCNotes"),
                        L("LeadOwner"),
                        L("Reminder")
                        );

                    AddObjects(
                       sheet, 2, stcListDtos,
                       _ => _.Job.JobNumber,
                       _ => _.JobTypeName,
                       _ => _.Job.Address,
                       _ => _.Job.Suburb,
                       _ => _.Job.State,
                       _ => _.Job.PostalCode,
                       _ => _timeZoneConverter.Convert(_.Job.InstallationDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _timeZoneConverter.Convert(_.Job.STCAppliedDate, _abpSession.TenantId, _abpSession.GetUserId()),
                       _ => _.Job.PVDNumber,
                       _ => _.Job.PVDStatusName,
                       _ => _.JobStatusName,
                       _ => _.Job.SystemCapacity,
                       _ => _.Job.Stc,
                        _ => _.Job.StcTotalPrice,
                       _ => _.Job.InstallerName,
                       _ => _.Job.STCNotes,
                        _ => _.Job.CurrentLeadOwaner,
                        _ => _.LastComment
                       );

                    for (var i = 1; i <= stcListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[7], "yyyy-mm-dd");
                    }
                    sheet.AutoSizeColumn(7);
                });
        }
        public FileDto GridConnectionExportToFile(List<GetJobForViewDto> gridconnectionListDtos)
        {
            return CreateExcelPackage(
                "GridConnectionTracker.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("GridConnectionTracker");
                    AddHeader(
                          sheet,
                        L("ProjectNo"),
                        L("JobType"),
                        L("JobStatus"),
                        L("CompanyName"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostalCode"),
                        L("InstallerName"),
                       L("DistApplyDate"),

                        L("ApplicationRefNo"),
                        L("LeadOwner"),
                        L("Stc"),
                        L("MeterAppliedDate"),
                         L("ReminderDate"),
                        L("FollowUp")
                        );

                    AddObjects(
                       sheet, 2, gridconnectionListDtos,
                       _ => _.Job.JobNumber,
                       _ => _.JobTypeName,
                       _ => _.JobStatusName,
                       _ => _.LeadCompanyName,
                       _ => _.Job.Address,
                       _ => _.Job.Suburb,
                       _ => _.Job.State,
                       _ => _.Job.PostalCode,
                       _ => _.Job.InstallerName,
                         _ => _timeZoneConverter.Convert(_.Job.DistAppliedDate, _abpSession.TenantId, _abpSession.GetUserId()),
                       _ => _.Job.ApplicationRefNo,
                       _ => _.Job.CurrentLeadOwaner,
                       _ => _.Job.SystemCapacity,
                        _ => _.Job.MeterApplyRef,
                          //_ => _.LastCommentDate,
                          _ => _timeZoneConverter.Convert(_.LastCommentDate, _abpSession.TenantId, _abpSession.GetUserId()),
                          _ => _.LastComment
                       );
                    //for (var i = 1; i <= gridconnectionListDtos.Count; i++)
                    //{
                    //    SetCellDataFormat(sheet.GetRow(i).Cells[10], "yyyy-mm-dd");
                    //}
                    //sheet.AutoSizeColumn(10);
                    for (var i = 1; i <= gridconnectionListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[9], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(9);
                    for (var i = 1; i <= gridconnectionListDtos.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[12], "0.00");
                    }
                    sheet.AutoSizeColumn(12);
                    for (var i = 1; i <= gridconnectionListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[14], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(14);
                });
        }

        public FileDto GridConnectionCsvExport(List<GetJobForViewDto> gridconnectionListDtos)
        {
            return CreateExcelPackage(
                "GridConnectionTracker.csv",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("GridConnectionTracker");
                    AddHeader(
                          sheet,
                        L("ProjectNo"),
                        L("JobType"),
                        L("JobStatus"),
                        L("CompanyName"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostalCode"),
                        L("InstallerName"),
                       L("DistApplyDate"),

                        L("ApplicationRefNo"),
                        L("LeadOwner"),
                        L("Stc"),
                        L("MeterAppliedDate"),
                         L("ReminderDate"),
                        L("FollowUp")
                        );

                    AddObjects(
                       sheet, 2, gridconnectionListDtos,
                       _ => _.Job.JobNumber,
                       _ => _.JobTypeName,
                       _ => _.JobStatusName,
                       _ => _.LeadCompanyName,
                       _ => _.Job.Address,
                       _ => _.Job.Suburb,
                       _ => _.Job.State,
                       _ => _.Job.PostalCode,
                       _ => _.Job.InstallerName,
                         _ => _timeZoneConverter.Convert(_.Job.DistAppliedDate, _abpSession.TenantId, _abpSession.GetUserId()),
                       _ => _.Job.ApplicationRefNo,
                       _ => _.Job.CurrentLeadOwaner,
                       _ => _.Job.SystemCapacity,
                        _ => _.Job.MeterApplyRef,

                          _ => _.LastCommentDate,
                          _ => _.LastComment
                       );
                    for (var i = 1; i <= gridconnectionListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[10], "yyyy-mm-dd");
                    }
                    sheet.AutoSizeColumn(10);
                });
        }





        public FileDto LeadExpenseExportToFile(string id)
        {
            return CreateExcelPackage(
                "LeadExpenseReport.xlsx",
                excelPackage =>
                {
                    //var sheet = excelPackage.CreateSheet("LeadExpenseReport");
                    //AddHeader(
                    //      sheet,
                    //    L("LeadSource"),
                    //    L("Cost")
                    //    //L("Address"),
                    //    //L("Suburb"),
                    //    //L("State"),
                    //    //L("PostalCode"), L("InstallationDate"),
                    //    //L("STCAppData"),
                    //    //L("PVDNo"),
                    //    //L("PVDStatus"),
                    //    //L("JobStatus"),
                    //    // L("KW"),
                    //    // L("STC"),
                    //    //L("StcTotalPrice"),
                    //    //L("InstallerName"),
                    //    //L("STCNotes"),
                    //    //L("LeadOwner"),
                    //    //L("Reminder")
                    //    );

                    //AddObjects(
                    //   sheet, 2, expenseListDtos,
                    //   _ => _.Leadsource,
                    //   _ => _.leadstates
                    //   //_ => _.Job.Address,
                    //   //_ => _.Job.Suburb,
                    //   //_ => _.Job.State,
                    //   //_ => _.Job.PostalCode,
                    //   //_ => _timeZoneConverter.Convert(_.Job.InstallationDate, _abpSession.TenantId, _abpSession.GetUserId()),
                    //   // _ => _timeZoneConverter.Convert(_.Job.STCAppliedDate, _abpSession.TenantId, _abpSession.GetUserId()),
                    //   //_ => _.Job.PVDNumber,
                    //   //_ => _.Job.PVDStatusName,
                    //   //_ => _.JobStatusName,
                    //   //_ => _.Job.SystemCapacity,
                    //   //_ => _.Job.Stc,
                    //   // _ => _.Job.StcTotalPrice,
                    //   //_ => _.Job.InstallerName,
                    //   //_ => _.Job.STCNotes,
                    //   // _ => _.Job.CurrentLeadOwaner,
                    //   // _ => _.LastComment
                    //   );

                    //for (var i = 1; i <= expenseListDtos.Count; i++)
                    //{
                    //    SetCellDataFormat(sheet.GetRow(i).Cells[7], "yyyy-mm-dd");
                    //}
                    //sheet.AutoSizeColumn(7);
                });
        }
        public FileDto reffreallistExportToFile(List<GetViewReferralTrackerDto> refferallistDtos, string fileName)
        {
            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("ReferralTracker");
                    AddHeader(
                          sheet,
                        L("ProjectNo"),
                        L("CompanyName"),
                        L("JobStatus"),
                        L("InstallCompleteDate"),
                        L("SystemPrice"),
                        L("BalOwing"),
                        "Referred by(Project No)",
                        "Referred Project Status')",
                        L("SystemPrice"),
                        L("BalOwing"),
                        L("AccountName"),
                        L("BSBNo"),
                        L("AccountNo"),
                        L("ReferralAmount"),
                        L("Comment"),
                       L("ReminderDate"),
                        L("Description"),
                        "PaymentDate",
                        "Bank Ref No",
                        "Payment Remark"

                        );

                    AddObjects(
                       sheet, 2, refferallistDtos,
                          _ => _.JobNumber,
                          _ => _.CompanyName,
                          _ => _.JobStatus,
                          _ => _timeZoneConverter.Convert(_.InstallComplate, _abpSession.TenantId, _abpSession.GetUserId()),
                          _ => _.TotalCost,
                          _ => _.OwningAmt,
                          _ => _.RefferedProjectName,
                          _ => _.RefferedProjectStatus,
                          _ => _.RefferedTotalCost,
                          _ => _.RefferedowningAmt,
                          _ => _.AccountName,
                          _ => _.BankRefNo,
                          _ => _.AccountNo,
                          _ => _.ReferralAmount,
                          _ => _.ActivityComment,
                          _ => _.ActivityReminderTime,
                          _ => _.ActivityDescription,
                          _ => _timeZoneConverter.Convert(_.PaymentDate, _abpSession.TenantId, _abpSession.GetUserId()),
                          _ => _.BankRefNo,
                          _ => _.PaymentRemark

                       );
                    //for (var i = 1; i <= refferallistDtos.Count; i++)
                    //{
                    //    SetCellDataFormat(sheet.GetRow(i).Cells[3], "yyyy-mm-dd");
                    //}
                    //sheet.AutoSizeColumn(3);
                    //for (var i = 1; i <= refferallistDtos.Count; i++)
                    //{
                    //    SetCellDataFormat(sheet.GetRow(i).Cells[15], "yyyy-mm-dd");
                    //}
                    //sheet.AutoSizeColumn(15);
                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                    for (var i = 1; i <= refferallistDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[3], "dd/mm/yyyy");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[4], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[5], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[8], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[13], "0.00");
                        SetCellDataFormat(sheet.GetRow(i).Cells[17], "dd/mm/yyyy");
                    }
                    
                });
        }

        public FileDto reffreallistExportToCSVFile(List<ReferralGridDto> refferallistDtos)
        {
            return CreateExcelPackage(
                "ReferralTracker.csv",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("ReferralTracker");
                    AddHeader(
                          sheet,
                        L("ProjectNo"),
                        L("CompanyName"),
                        L("JobStatus"),
                        L("InstallCompleteDate"),
                        L("SystemPrice"),
                        L("BalOwing"),
                        "Referred by(Project No)",
                        "Referred Project Status')",
                        L("SystemPrice"),
                        L("BalOwing"),
                        L("AccountName"),
                        L("BSBNo"),
                        L("AccountNo"),
                        L("ReferralAmount"),
                        L("Comment"),
                       L("ReminderDate"),
                        L("Description")

                        );

                    AddObjects(
                       sheet, 2, refferallistDtos,
                          _ => _.ProjectName,
                          _ => _.companyName,
                          _ => _.jobstatus,
                             _ => _timeZoneConverter.Convert(_.InstallComplate, _abpSession.TenantId, _abpSession.GetUserId()),
                          _ => _.TotalCost,
                          _ => _.owningAmt,
                          _ => _.RefferedProjectName,
                          _ => _.RefferedProjectStatus,
                          _ => _.RefferedTotalCost,
                          _ => _.RefferedowningAmt,
                          _ => _.AccountName,
                          _ => _.BsbNo,
                          _ => _.AccountNo,
                          _ => _.ReferralAmount,
                          _ => _.ActivityComment,
                          _ => _timeZoneConverter.Convert(_.ActivityReminderTime, _abpSession.TenantId, _abpSession.GetUserId()),
                          _ => _.ActivityDescription
                       );
                    for (var i = 1; i <= refferallistDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[3], "yyyy-mm-dd");
                    }
                    sheet.AutoSizeColumn(3);
                    for (var i = 1; i <= refferallistDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[15], "yyyy-mm-dd");
                    }
                    sheet.AutoSizeColumn(15);
                });
        }



        public FileDto HoldJobTrackerExportToFile(List<GetJobForViewDto> jobgridrListDtos)
        {
            return CreateExcelPackage(
                "HoldJobTracker.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("HoldJobTracker");
                    AddHeader(
                          sheet,
                        "ProjectNo",
                        "Man",
                        "Contact",
                        "Mobile",
                        "ContactEmail",
                        "Team",
                        "EmployeeName",
                        "Project",
                        "State",
                        "Pincode",
                        "Quote",
                        "Source",
                        "Status",
                        "SystemCap",
                        "Panels",
                        "PanelName",
                        "Inverter",
                        "InverterName",
                        "HouseType",
                        "RoofType",
                        "Price",
                        "DepRec",
                        "FinanceWith",
                        "InstallBookDate",
                        "FirstDepDate",
                        "FirstDepAmount",
                        "NextFollowupDate",
                        "Description",
                        "Leaddate",
                        "ProjOpenDate",
                        "DiffInDays",
                        "ProjectNotes"
                        );

                    AddObjects(
                       sheet, 2, jobgridrListDtos,
                       _ => _.Job.JobNumber,
                       _ => null,
                       _ => _.LeadCompanyName,
                       _ => _.Mobile,
                       _ => _.Email,
                       _ => _.Team,
                       _ => _.Job.CurrentLeadOwaner, //EmployeeName
                       _ => _.Job.Address,
                       _ => _.Job.State,
                       _ => _.Job.PostalCode,
                       _ => _.Job.ManualQuote,
                       _ => _.LeadSourceName,
                       _ => _.LeadStatus,
                       _ => _.Job.SystemCapacity,
                       _ => _.TotalNoOfPanel,
                       _ => _.Panels,
                        _ => _.TotalNoOfInvert,
                       _ => _.Inverters,
                       _ => _.HouseType,
                       _ => _.RoofType,
                       _ => _.Job.TotalCost,
                       _ => _timeZoneConverter.Convert(_.Job.DepositeRecceivedDate, _abpSession.TenantId, _abpSession.GetUserId()), /// DepositeRecceivedDate
                       _ => _.FinanceWith,
                       _ => _timeZoneConverter.Convert(_.Job.InstallationDate, _abpSession.TenantId, _abpSession.GetUserId()), /// InstallationDate
                       _ => _timeZoneConverter.Convert(_.FirstDepDate, _abpSession.TenantId, _abpSession.GetUserId()), // firstdep
                       _ => _.FirstDepAmmount,
                       _ => _timeZoneConverter.Convert(_.LastCommentDate, _abpSession.TenantId, _abpSession.GetUserId()), //nextfollowupdate
                       _ => _.LastComment,
                       _ => _timeZoneConverter.Convert(_.LeadCreationDate, _abpSession.TenantId, _abpSession.GetUserId()), //LeadCreationDate
                       _ => _timeZoneConverter.Convert(_.Job.CreationTime, _abpSession.TenantId, _abpSession.GetUserId()), //Jobcreation
                       _ => _.DateDiff,
                       _ => _.Job.Note
                       );
                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[13], "0.00");
                    }
                    sheet.AutoSizeColumn(13);
                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetintCellDataFormat(sheet.GetRow(i).Cells[14], "00");
                    }
                    sheet.AutoSizeColumn(14);
                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetintCellDataFormat(sheet.GetRow(i).Cells[16], "00");
                    }
                    sheet.AutoSizeColumn(16);
                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[20], "0.00");
                    }
                    sheet.AutoSizeColumn(20);
                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[21], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(21);
                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[23], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(23);
                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[24], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(24);
                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[25], "0.00");
                    }
                    sheet.AutoSizeColumn(25);
                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[26], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(26);
                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[28], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(28);
                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[29], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(29);
                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetintCellDataFormat(sheet.GetRow(i).Cells[30], "00");
                    }
                    sheet.AutoSizeColumn(30);
                    //for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    //{
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[4], "0");
                    //}
                    //sheet.AutoSizeColumn(4);
                    //for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    //{
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[8], "0");
                    //}
                    //sheet.AutoSizeColumn(8);
                });
        }

        public FileDto HoldJobTrackerExportToCsvFile(List<GetJobForViewDto> jobgridrListDtos)
        {
            return CreateExcelPackage(
                "HoldJobTracker.csv",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("HoldJobTracker");
                    AddHeader(
                          sheet,
                        L("ProjectNo"),
                        L("Man"),
                        L("Project"),
                        L("Contact"),
                        L("Mobile"),
                        L("ContactEmail"),
                        L("Team"),
                        L("EmployeeName"),
                        L("State"),
                        L("Pincode"),
                        L("Quote"),
                        L("Source"),
                        L("Status"),
                        L("SystemCap"),
                        L("Panels"),
                        L("PanelName"),
                        L("Inverter"),
                        L("InverterName"),
                        L("HouseType"),
                        L("RoofType"),
                        L("Price"),
                        L("DepRec"),
                        L("FinanceWith"),
                        L("InstallBookDate"),
                        L("FirstDepDate"),
                        L("FirstDepAmount"),
                        L("NextFollowupDate"),
                        L("Description"),
                        L("Leaddate"),
                        L("ProjOpenDate"),
                        L("DiffInDays"),
                        L("ProjectNotes")
                        );

                    AddObjects(
                       sheet, 2, jobgridrListDtos,
                       _ => _.Job.JobNumber,
                       _ => null,
                       _ => _.Job.Address,
                       _ => _.LeadCompanyName,
                       _ => _.Mobile,
                       _ => _.Email,
                       _ => _.Team,
                       _ => _.Job.CurrentLeadOwaner, //EmployeeName
                       _ => _.Job.State,
                       _ => _.Job.PostalCode,
                       _ => _.Job.ManualQuote,
                       _ => _.LeadSourceName,
                       _ => _.LeadStatus,
                       _ => _.Job.SystemCapacity,
                       _ => _.TotalNoOfPanel,
                       _ => _.Panels,
                        _ => _.TotalNoOfInvert,
                       _ => _.Inverters,
                       _ => _.HouseType,
                       _ => _.RoofType,
                       _ => _.Job.TotalCost,
                       _ => _timeZoneConverter.Convert(_.Job.DepositeRecceivedDate, _abpSession.TenantId, _abpSession.GetUserId()), /// DepositeRecceivedDate
                       _ => _.FinanceWith,
                       _ => _timeZoneConverter.Convert(_.Job.InstallationDate, _abpSession.TenantId, _abpSession.GetUserId()), /// InstallationDate
                       _ => _timeZoneConverter.Convert(_.FirstDepDate, _abpSession.TenantId, _abpSession.GetUserId()), // firstdep
                       _ => _.FirstDepAmmount,
                       _ => _timeZoneConverter.Convert(_.LastCommentDate, _abpSession.TenantId, _abpSession.GetUserId()), //nextfollowupdate
                       _ => _.LastComment,
                       _ => _timeZoneConverter.Convert(_.LeadCreationDate, _abpSession.TenantId, _abpSession.GetUserId()), //LeadCreationDate
                       _ => _timeZoneConverter.Convert(_.Job.CreationTime, _abpSession.TenantId, _abpSession.GetUserId()), //Jobcreation
                       _ => _.DateDiff,
                       _ => _.Job.Note
                       );

                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[21], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(21);
                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[23], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(23);
                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[24], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(24);
                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[26], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(26);
                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[28], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(28);
                    for (var i = 1; i <= jobgridrListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[29], "dd-MM-yyyy");
                    }
                    sheet.AutoSizeColumn(29);
                });
        }


        public FileDto UserCallHistoryExport(List<GetAllUserCallHistoryDto> callHistory, string fileName)
        {
            string[] Header = new string[] { "User", "Missed", "Outbound", "Inbound", "Notanswered" };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("CallHistory"));
                    AddHeader(sheet, Header);

                    AddObjects(
                       sheet, 2, callHistory,
                       _ => _.UserName,
                       _ => _.Missed,
                       _ => _.Outbound,
                       _ => _.Inbound,
                       _ => _.Notanswered
                       );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                    for (var i = 1; i <= callHistory.Count; i++)
                    {
                        SetintCellDataFormat(sheet.GetRow(i).Cells[1], "00");

                        SetintCellDataFormat(sheet.GetRow(i).Cells[2], "00");

                        SetintCellDataFormat(sheet.GetRow(i).Cells[3], "00");

                        SetintCellDataFormat(sheet.GetRow(i).Cells[4], "00");
                    }

                });

        }
        
        public FileDto UserCallHistoryDetailsExport(List<GetCallHistoryDetailDto> callHistory, string fileName)
        {
            string[] Header = new string[] { "Mobile", "Duration", "CreationTime" };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("CallHistory"));
                    AddHeader(sheet, Header);

                    AddObjects(
                       sheet, 2, callHistory,
                       _ => _.Mobile,
                       _ => _.Duration,
                       _ => _.CreationTime
                       );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                    for (var i = 1; i <= callHistory.Count; i++)
                    {
                        SetintCellDataFormat(sheet.GetRow(i).Cells[1], "00");

                        SetintCellDataFormat(sheet.GetRow(i).Cells[2], "00");
                    }

                });

        }


        public FileDto StateWiseCallHistoryExport(List<GetAllStateCallHistoryDto> callHistory, string fileName)
        {
            string[] Header = new string[] { "FromLocationLocality", "FromLocationAdministrativeArea", "CreateTime", "StartTime", "EndTime", "AnswerDuration", "cost" };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("StateCallHistory");
                    AddHeader(sheet, Header);

                    AddObjects(
                       sheet, 2, callHistory,
                       _ => _.FromLocationLocality,
                       _ => _.FromLocationAdministrativeArea,
                       _ => _.CreationDate,
                       _ => _.StartTime,
                       _ => _.EndTime,
                       //_ => !string.IsNullOrEmpty(_.AnswerDuration) ? Convert.ToDecimal(_.AnswerDuration) : 0,
                       _ => Convert.ToDecimal(_.AnswerDuration) ,
                       _ => Convert.ToDecimal(_.Cost) 
                       );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                    for (var i = 1; i <= callHistory.Count; i++)
                    {

                        SetCellDataFormat(sheet.GetRow(i).Cells[2], "dd-mm-yyyy hh:mm");

                        SetCellDataFormat(sheet.GetRow(i).Cells[3], "dd-mm-yyyy hh:mm");

                        SetCellDataFormat(sheet.GetRow(i).Cells[4], "dd-mm-yyyy hh:mm");

                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[5], "00.00");

                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[6], "00.00");
                    }

                });

        }

        public FileDto LeadAssignExport(List<GetAllLeadAssignDto> callHistory, string fileName)
        {
            string[] Header = new string[] { "User", "TV","TVSold", "TVRatio", "Google", "GoogleSold", "GoogleRatio", "Facebook", "FacebookSold", "FacebookRatio", "Refferal", "RefferalSold", "RefferalRatio", "Others", "OthersSold", "OthersRatio" };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("CallHistory"));
                    AddHeader(sheet, Header);

                    AddObjects(
                       sheet, 2, callHistory,
                       _ => _.UserName,
                       _ => _.TV,
                       _ => _.TVSold,
                       _ => _.TVRatio,
                       _ => _.Google,
                       _ => _.GoogleSold,
                       _ => _.GoogleRatio,
                       _ => _.Facebook,
                       _ => _.FacebookSold,
                       _ => _.FacebookRatio,
                       _ => _.Refferal,
                       _ => _.RefferalSold,
                       _ => _.RefferalRatio,
                       _ => _.Others,
                       _ => _.OthersSold,
                       _ => _.OthersRatio
                       );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                    for (var i = 1; i <= callHistory.Count; i++)
                    {
                        SetintCellDataFormat(sheet.GetRow(i).Cells[1], "00");

                        SetintCellDataFormat(sheet.GetRow(i).Cells[2], "00");

                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[3], "0.00");

                        SetintCellDataFormat(sheet.GetRow(i).Cells[4], "00");

                        SetintCellDataFormat(sheet.GetRow(i).Cells[5], "00");

                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[6], "0.00");

                        SetintCellDataFormat(sheet.GetRow(i).Cells[7], "00");

                        SetintCellDataFormat(sheet.GetRow(i).Cells[8], "00");

                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "0.00");

                        SetintCellDataFormat(sheet.GetRow(i).Cells[10], "00");

                        SetintCellDataFormat(sheet.GetRow(i).Cells[11], "00");

                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[12], "0.00");

                        SetintCellDataFormat(sheet.GetRow(i).Cells[13], "00");

                        SetintCellDataFormat(sheet.GetRow(i).Cells[14], "00");

                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[15], "0.00");
                    }

                });

        }

        public FileDto CallFlowQueueExport(List<GetAllCallQueueHistoryDto> callHistory, string fileName)
        {
            string[] Header = new string[] { "3CX Call Queue", "Call Count" };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("CallHistory"));
                    AddHeader(sheet, Header);

                    AddObjects(
                       sheet, 2, callHistory,
                       _ => _.Name,
                       _ => _.Count
                       );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                    for (var i = 1; i <= callHistory.Count; i++)
                    {
                        SetintCellDataFormat(sheet.GetRow(i).Cells[1], "00");
                    }

                });
        }

        public FileDto CallFlowQueueDetailExport(List<GetAllCallFlowQueueCountDetailsDto> callHistory, string fileName)
        {
            string[] Header = new string[] { "User", "Total Calls" };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("CallHistory"));
                    AddHeader(sheet, Header);

                    AddObjects(
                       sheet, 2, callHistory,
                       _ => _.User,
                       _ => _.Count
                       );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                    for (var i = 1; i <= callHistory.Count; i++)
                    {
                        SetintCellDataFormat(sheet.GetRow(i).Cells[1], "00");
                    }

                });
        }

        public FileDto CallFlowQueueCallHistoryDetailExport(List<GetAllCallFlowQueueCallHistoryDetailsDto> callHistory, string fileName)
        {
            string[] Header = new string[] { "Mobile", "StartTime", "EndTime", "Duration" };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("CallHistory"));
                    AddHeader(sheet, Header);

                    AddObjects(
                       sheet, 2, callHistory,
                       _ => _.Mobile,
                       _ => _.StartTime,
                       _ => _.EndTime,
                       _ => _.Duration
                       );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                    for (var i = 1; i <= callHistory.Count; i++)
                    {
                        SetintCellDataFormat(sheet.GetRow(i).Cells[1], "00");

                        SetintCellDataFormat(sheet.GetRow(i).Cells[2], "00");

                        SetintCellDataFormat(sheet.GetRow(i).Cells[3], "00");
                    }

                });
        }

    public FileDto LocalityCountOfStateHistoryExport(List<GetAllLocalityCountOfState> callHistory, string fileName)
        {
            string[] Header = new string[] { "Locality", "Count" };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("CallHistory"));
                    AddHeader(sheet, Header);

                    AddObjects(
                       sheet, 2, callHistory,
                       _ => _.FromLocationLocality,
                       _ => _.Count
                       );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                    for (var i = 1; i <= callHistory.Count; i++)
                    {
                        SetintCellDataFormat(sheet.GetRow(i).Cells[1], "00");
                    }

                });
        }

        public FileDto LeadSoldExport(List<GetAllLeadSoldDto> leadSold, string fileName)
        {
            string[] Header = new string[] { "User Name", "Lead Source", "Lead Assign", "Sold Lead", "Ratio" , "Missing"};

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("CallHistory"));
                    AddHeader(sheet, Header);
                    AddObjects(
                       sheet, 2, leadSold,
                       _ => _.UserName,
                       _ => _.LeadSorce,
                       _ => _.LeadAssign,
                       _ => _.SoldLead,
                       _ => _.Ratio,
                       _ => _.Missing
                       );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                    for (var i = 1; i <= leadSold.Count; i++)
                    {
                        //SetintCellDataFormat(sheet.GetRow(i).Cells[1], "00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[2], "00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[3], "00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[4], "00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[5], "00.00");
                    }

                });
        }
    public FileDto CommissionExport(List<GetAllCommisionViewDto> listDto, string fileName)
        {
            var EmailPermission = _permissionChecker.IsGranted(AppPermissions.Pages_LeadGeneration_Commission_Export_MobileEmail);

            if (EmailPermission)
            {
                string[] Header = new string[] { "Appointment For", "Lead Status", "Mobile", "Address", "Subrub", "State", "PostCode", "Lead Source", "Lead Assign Date", "Lead Owner", "Creation Time" };

                return CreateExcelPackage(
                    fileName,
                    excelPackage =>
                    {
                        var sheet = excelPackage.CreateSheet(L("CommissionReport"));
                        AddHeader(sheet, Header);
                        AddObjects(
                           sheet, 2, listDto,
                           _ => _.AppointementFor,
                           _ => _.LeadStatus,
                           _ => _.Mobile,
                           _ => _.Address,
                           _ => _.Suburb,
                           _ => _.State,
                           _ => _.PostCode,
                           _ => _.LeadSource,
                           _ => _.AssignDate,
                           _ => _.CurrentLeadOwner,
                           _ => _.CreationDate
                           );

                        for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                        {
                            sheet.AutoSizeColumn(i);
                        }

                        for (var i = 1; i <= listDto.Count; i++)
                        {
                            SetCellDataFormat(sheet.GetRow(i).Cells[8], "dd/MM/yyyy");
                            SetCellDataFormat(sheet.GetRow(i).Cells[10], "dd/MM/yyyy");
                        }
                    });
            }
            else
            {
                string[] Header = new string[] { "Appointment For", "Lead Status", "Address", "Subrub", "State", "PostCode", "Lead Source", "Lead Assign Date", "Lead Owner", "Creation Time" };

                return CreateExcelPackage(
                    fileName,
                    excelPackage =>
                    {
                        var sheet = excelPackage.CreateSheet(L("CommissionReport"));
                        AddHeader(sheet, Header);
                        AddObjects(
                           sheet, 2, listDto,
                           _ => _.AppointementFor,
                           _ => _.LeadStatus,
                           _ => _.Address,
                           _ => _.Suburb,
                           _ => _.State,
                           _ => _.PostCode,
                           _ => _.LeadSource,
                           _ => _.AssignDate,
                           _ => _.CurrentLeadOwner,
                           _ => _.CreationDate
                           );

                        for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                        {
                            sheet.AutoSizeColumn(i);
                        }

                        for (var i = 1; i <= listDto.Count; i++)
                        {
                            SetCellDataFormat(sheet.GetRow(i).Cells[7], "dd/MM/yyyy");
                            SetCellDataFormat(sheet.GetRow(i).Cells[9], "dd/MM/yyyy");
                        }
                    });
            }
                
        }

        public FileDto GetAllPvdStatusForGridExcel(List<PvdStatusDto> datalist, string fileName)
        {
            string[] Header = new string[] { "Name", "Status Id" };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("StcStatus");
                    AddHeader(sheet, Header);
                    AddObjects(
                       sheet, 2, datalist,
                       _ => _.DisplayName,
                       _ => _.Id
                       );

                    for (var i = 1; i <= datalist.Count; i++)
                    {
                        SetintCellDataFormat(sheet.GetRow(i).Cells[1], "00");
                    }

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto ApplicationFeeTrackerExportToFile(List<GetViewApplicationFeeTrackerDto> applicationFeetrackerListDtos)
        {
            return CreateExcelPackage(
                "ApplicationFeeTracker.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("ApplicationFeeTracker");
                    AddHeader(
                        sheet,
                        L("ProjectNo"),
                        L("JobType"),
                        L("JobStatus"),
                        L("CompanyName"),
                        L("Address"),
                        "EleDist",
                        "ApplicationFeesPaid",
                        "PaidAmount",
                        "PaidBy",
                        "PaidStatus",
                        "InvRefNo",
                        "InvVerifyDate",
                        "SmsSend",
                        "EmailSend",
                        "CurrentLeadOwner",
                        "Comment",
                        "Next Followup",
                        "Discription"
                        );

                    AddObjects(
                       sheet, 2, applicationFeetrackerListDtos,
                       _ => _.JobNumber,
                       _ => _.JobType,
                       _ => _.JobStatus,
                       _ => _.CompanyName,
                       _ => _.Address,
                       _ => _.ElecDistributor,
                       _ => _.ApplicationFeesPaid,
                       _ => _.PaidAmount,
                       _ => _.PaidBy,
                       _ => _.PaidStatus,
                       _ => _.InvRefNo,
                       _ => _.InvVerifyDate,
                       _ => _.SmsSend != true ? "No" : "Yes",
                       _ => _.EmailSend != true ? "No" : "Yes",
                       _ => _.CurruntLeadOwner,
                       _ => _.Comment,
                       _ => _.NextFollowup,
                       _ => _.Discription
                       );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                    for (var i = 1; i <= applicationFeetrackerListDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[11], "dd/mm/yyyy");
                        //SetCellDataFormat(sheet.GetRow(i).Cells[7], "dd/mm/yyyy");
                        //SetCellDataFormat(sheet.GetRow(i).Cells[8], "dd/mm/yyyy");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[7], "0.00");
                        //SetdecimalCellDataFormat(sheet.GetRow(i).Cells[10], "00");
                        //SetdecimalCellDataFormat(sheet.GetRow(i).Cells[11], "00");
                    }
                });
        }


    }

}
