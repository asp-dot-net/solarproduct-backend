﻿using System.Collections.Generic;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos;
using NPOI.SS.Formula.Functions;
using TheSolarProduct.Reports.LeadAssigns.Dtos;
using TheSolarProduct.Reports.LeadSolds.Dtos;
using TheSolarProduct.LeadGeneration.Dtos;
using TheSolarProduct.JobTrackers.Dtos;

namespace TheSolarProduct.Leads.Exporting
{
    public interface ILeadsExcelExporter
    {
        FileDto LeadTrackerExportToFile(List<GetLeadForViewDto> leadtrackerListDtos, string fileName);
        //FileDto LeadTrackerCsvExport(List<GetLeadForViewDto> leadtrackerListDtos);

        FileDto JobGridExportToFile(List<GetJobForExcelViewDto> jobgridrListDtos, string fileName);
        //FileDto JobGridExportToCsvFile(List<GetJobForViewDto> jobgridrListDtos);

        FileDto JobGridProductDetailsExport(List<GetJobProductDetailsForExcel> jobProduct, string fileName);

        FileDto JobGridProductItemExport(List<GetJobProductItemsForExcel> jobProduct, string fileName);

        FileDto InvoiceIssuedExportToFile(List<GetInvoiceIssuedForViewDto> InvoiceIssuedListDtos, string fileName);
        //FileDto InvoiceIssuedExportCSVFile(List<GetInvoiceIssuedForViewDto> InvoiceIssuedListDtos);

        FileDto InvoiceIssuedDepositeExportToFile(List<GetInvoiceIssuedForViewDto> InvoiceIssuedListDtos, string fileName);

        FileDto InvoiceIssuedOwingExportToFile(List<GetInvoiceIssuedForViewDto> InvoiceIssuedListDtos, string fileName);
        


        FileDto ExportToFile(List<GetLeadForViewDto> leads);

        FileDto ExportCsvToFile(List<GetLeadForViewDto> leads);
        
        FileDto DublicateLeadExportToFile(List<GetDuplicateLeadForViewDto> dublicateleadListDtos);
        FileDto DublicateLeadCsvExportToFile(List<GetDuplicateLeadForViewDto> dublicateleadListDtos);

        FileDto ClosedLeadExportToFile(List<GetLeadForViewDto> closedleadListDtos);
        FileDto ClosedLeadCsvExport(List<GetLeadForViewDto> closedleadListDtos);

        FileDto CancelLeadExportToFile(List<GetLeadForViewDto> cancelleadListDtos);
        FileDto CancelLeadCsvExport(List<GetLeadForViewDto> cancelleadListDtos);

        FileDto RejectLeadExportToFile(List<GetLeadForViewDto> rejectleadListDtos);
        FileDto RejectLeadCsvExport(List<GetLeadForViewDto> rejectleadListDtos);

        
        

        FileDto MyLeadExportToFile(List<GetLeadForViewDto> myleadListDtos);
        FileDto MyLeadCsvExport(List<GetLeadForViewDto> myleadListDtos);

        FileDto ApplicationTrackerExportToFile(List<GetJobForViewDto> applicationtrackerListDtos);
        FileDto ApplicationTrackerCsvExport(List<GetJobForViewDto> applicationtrackerListDtos);

        FileDto FinanaceTrackerExportToFile(List<GetViewFinanceTrackerDto> financetrackerListDtos,string filename);
        FileDto FinanaceTrackerCsvExport(List<GetJobForViewDto> financetrackerListDtos);

        //FileDto JobActiveTrackerExportToFile(List<GetJobForViewDto> activetrackerListDtos);
        //FileDto JobActiveTrackerCsvExport(List<GetJobForViewDto> activetrackerListDtos);
        
        
        FileDto STCExportToFile(List<GetJobForViewDto> stcListDtos);

        FileDto GridConnectionExportToFile(List<GetJobForViewDto> gridconnectionListDtos);
                    
       
        FileDto LeadExpenseExportToFile(string id);

        FileDto reffreallistExportToFile(List<GetViewReferralTrackerDto> refferallistDtos,string fileName);
        FileDto reffreallistExportToCSVFile(List<ReferralGridDto> refferallistDtos);

        FileDto HoldJobTrackerExportToFile(List<GetJobForViewDto> jobgridrListDtos);
        FileDto HoldJobTrackerExportToCsvFile(List<GetJobForViewDto> jobgridrListDtos);


        FileDto UserCallHistoryExport(List<GetAllUserCallHistoryDto> callHistory, string fileName);

        FileDto UserCallHistoryDetailsExport(List<GetCallHistoryDetailDto> callHistory, string fileName);

        FileDto StateWiseCallHistoryExport(List<GetAllStateCallHistoryDto> callHistory, string fileName);

        FileDto LeadAssignExport(List<GetAllLeadAssignDto> callHistory, string fileName);

        FileDto CallFlowQueueExport(List<GetAllCallQueueHistoryDto> callHistory, string fileName);

        FileDto CallFlowQueueDetailExport(List<GetAllCallFlowQueueCountDetailsDto> callHistory, string fileName);

        FileDto CallFlowQueueCallHistoryDetailExport(List<GetAllCallFlowQueueCallHistoryDetailsDto> callHistory, string fileName);

        FileDto LocalityCountOfStateHistoryExport(List<GetAllLocalityCountOfState> callHistory, string fileName);

        FileDto LeadSoldExport(List<GetAllLeadSoldDto> leadSold, string fileName);

        FileDto CommissionExport(List<GetAllCommisionViewDto> listDto, string fileName);

        FileDto GetAllPvdStatusForGridExcel(List<PvdStatusDto> datalist, string fileName);
        FileDto EssentialTrackerExportToFile(List<GetJobForViewDto> EssentialTrackerListDtos);

        FileDto ApplicationFeeTrackerExportToFile(List<GetViewApplicationFeeTrackerDto> applicationFeetrackerListDtos);
    }
}