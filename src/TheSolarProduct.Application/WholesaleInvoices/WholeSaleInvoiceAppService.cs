﻿using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using Abp.Timing.Timezone;
using TheSolarProduct.Timing;
using Abp.Collections.Extensions;
using TheSolarProduct.WholeSaleLeads;
using TheSolarProduct.Wholesales;
using TheSolarProduct.ECommerces.Client.Dto;
using TheSolarProduct.WholesaleInvoices.Dtos;
using TheSolarProduct.WholeSaleLeads.Dtos;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct.EcommerceProducctSpecification.Dtos;
using TheSolarProduct.DeliveryTypes;
using TheSolarProduct.Wholesales.DataVault;
using TheSolarProduct.DataVaults;
using TheSolarProduct.InstallationCost.InstallationItemLists;
using TheSolarProduct.InstallationCost.InstallationItemList.Dtos;
using TheSolarProduct.Jobs;
using TheSolarProduct.StockOrders;
using System.Collections.Generic;
using TheSolarProduct.QuickStocks;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using Stripe;
using TheSolarProduct.Installer.Dtos;
using TheSolarProduct.UserWiseEmailOrgs;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Leads;
using Telerik.Reporting;

namespace TheSolarProduct.WholesaleInvoices
{
    public class WholeSaleInvoiceAppService : TheSolarProductAppServiceBase, IWholeSaleInvoiceAppService
    {

        /*private readonly IRepository<InstallationItemLists.InstallationItemPeriod> _installationItemPeriodRepository;*/
        private readonly IRepository<WholeSaleLead> _wholeSaleLeadRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly ITimeZoneService _timeZoneService;


        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserWiseEmailOrg> _userOrgRepository;
        private readonly IRepository<WholesaleInvoice> _wholeSaleInvoiceRepository;
        private readonly IRepository<EcommerceCartProductItem> _ecommerceCartProductItemRepository;
        private readonly IRepository<EcommerceProductItemPriceCategory> _ecommerceProductItemPriceCategoryRepository;
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;


        public WholeSaleInvoiceAppService(

           /* IRepository<InstallationItemLists.InstallationItemPeriod> installationItemPeriodRepository*/
            IRepository<WholeSaleLead> wholeSaleLeadRepository
            , ITimeZoneConverter timeZoneConverter
            , ITimeZoneService timeZoneService
            , IRepository<WholesaleInvoice> wholeSaleInvoiceRepository
            , IRepository<EcommerceCartProductItem> ecommerceCartProductItemRepository
            , IRepository<EcommerceProductItemPriceCategory> ecommerceProductItemPriceCategoryRepository
            ,IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository
              ,IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
             ,IRepository<UserWiseEmailOrg> userOrgRepository
            , IRepository<User, long> userRepository


            )
        {
            _wholeSaleLeadRepository = wholeSaleLeadRepository;
            _timeZoneConverter = timeZoneConverter;
            _timeZoneService = timeZoneService;
            _wholeSaleInvoiceRepository = wholeSaleInvoiceRepository;
            _ecommerceCartProductItemRepository = ecommerceCartProductItemRepository;
            _ecommerceProductItemPriceCategoryRepository = ecommerceProductItemPriceCategoryRepository;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;
            _dbcontextprovider = dbcontextprovider;
            _userRepository = userRepository;
            _userOrgRepository = userOrgRepository;

        }


        public async Task CreateOrEdit(WholesaleInvoiceDto input)
        {
            if (input.Id == null || input.Id == 0)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(WholesaleInvoiceDto input)
        {

            var winvoice = ObjectMapper.Map<WholesaleInvoice>(input);


            var invoiceItemId = await _wholeSaleInvoiceRepository.InsertAndGetIdAsync(winvoice);

            foreach (var item in input.ProductitemDetailDto)
            {
                /*item.WholesaleInvoiceId = invoiceItemId;*/

                 var invoiceItemList = ObjectMapper.Map<EcommerceCartProductItem>(item);
                /* var invoiceItemList = new EcommerceCartProductItem();*/
                invoiceItemList.WholesaleInvoiceId = invoiceItemId;

                invoiceItemList.EcommerceProductItemId = item.EcommerceProductItemId;
                invoiceItemList.QTY = item.QTY;
                invoiceItemList.Price = item.Price;


                invoiceItemList.Purcahse = true;

                 await _ecommerceCartProductItemRepository.InsertAndGetIdAsync(invoiceItemList);


                /* item.InvoiceTypeId = wholesaleinvoiceid;
                 var wholeinvoice = ObjectMapper.Map<EcommerceCartProductItem>(item);

                 await _ecommerceCartProductItemRepository.InsertAsync(wholeinvoice);*/
            }

            /*var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created Invoice";
            *//* dataVaultLog.SectionId = 11;*//*
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "WolesaleInvoice created: " + input.CustomerName;
            await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);*/

        }

        protected virtual async Task Update(WholesaleInvoiceDto input)
        {
            var wholeSaleLead = await _wholeSaleInvoiceRepository.FirstOrDefaultAsync((int)input.Id);

            ObjectMapper.Map(input, wholeSaleLead);
            await _wholeSaleInvoiceRepository.UpdateAsync(wholeSaleLead);



/*
            ObjectMapper.Map(input, wholeSaleLead);*/

            var InvoiceItemIds = await _ecommerceCartProductItemRepository.GetAll().Where(e => e.EcommerceProductItemId == wholeSaleLead.Id).ToListAsync();
            var Ids = input.ProductitemDetailDto.Where(e => e.Id > 0).Select(e => e.Id).ToList();

            var deleteItems = InvoiceItemIds.Where(e => !Ids.Contains(e.Id)).ToList();



            foreach (var item in InvoiceItemIds)
            {
                /*WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Item Delete";
                history.PrevValue = item.WholesaleInvoiceFK.InvoiceNo;
                history.CurValue = "";
                history.Action = "Delete";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);*/
                await _ecommerceCartProductItemRepository.HardDeleteAsync(e => e.WholesaleInvoiceId == input.Id);

            }



            foreach (var item in input.ProductitemDetailDto)
            {
                var invoiceItemList = ObjectMapper.Map<EcommerceCartProductItem>(item);
                invoiceItemList.WholesaleInvoiceId = wholeSaleLead.Id;

                invoiceItemList.Purcahse = true;
                item.EcommerceProductItemId = wholeSaleLead.Id;
                await _ecommerceCartProductItemRepository.InsertAndGetIdAsync(invoiceItemList);

                //if ((item.Id != null ? item.Id : 0) == 0)
                //{
                //    await _ecommerceCartProductItemRepository.InsertAndGetIdAsync(invoiceItemList);


                //}
                //else

                //{
                //    var invoiceproductItem = await _ecommerceCartProductItemRepository.GetAsync((int)item.Id);
                //    ObjectMapper.Map(item, invoiceproductItem);
                //    await _ecommerceCartProductItemRepository.UpdateAsync(invoiceproductItem);


                //}
            }
        }



        public async Task Delete(EntityDto input)
        {
         
            await _wholeSaleInvoiceRepository.DeleteAsync(input.Id);
            await _ecommerceCartProductItemRepository.DeleteAsync(e => e.EcommerceProductItemId == input.Id);
        }

        public async Task<PagedResultDto<GetWholeSaleInvoiceForViewDto>> GetAll(GetAllWholeSaleInvoiceInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var filteredWholeSale = _wholeSaleInvoiceRepository.GetAll().Include(e => e.WholeSaleLeadFK)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Talephone.Contains(input.Filter) || e.Mobile.Contains(input.Filter))
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFK.Mobile.Contains(input.Filter))
                       .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFK.Talephone.Contains(input.Filter))
                       .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFK.CompanyName.Contains(input.Filter))
                       .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFK.Email.Contains(input.Filter))
                       .WhereIf(input.FilterName == "FirstName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFK.FirstName.Contains(input.Filter))
                       .WhereIf(input.FilterName == "ABNNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFK.ABNNumber == input.Filter)
                       .WhereIf(!string.IsNullOrEmpty(input.Address), e => (e.WholeSaleLeadFK.PostalUnitNo + " " + e.WholeSaleLeadFK.PostalUnitType + " " + e.WholeSaleLeadFK.PostalStreetNo + " " + e.WholeSaleLeadFK.PostalStreetName + " " + e.WholeSaleLeadFK.PostalStreetType).Contains(input.Address))
                       .WhereIf(input.StateId != 0, e => e.WholeSaleLeadFK.PostalStateId == input.StateId)
                       .WhereIf(!string.IsNullOrEmpty(input.PostCode), e => e.WholeSaleLeadFK.PostalPostCode == input.PostCode)
                       .WhereIf(input.Datefilter == "Creation" && input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                       .WhereIf(input.Datefilter == "Creation" && input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                       .WhereIf(input.Datefilter == "FirstAssignDate" && input.StartDate != null, e => e.WholeSaleLeadFK.FirstAssignDate >= input.StartDate)
                       .WhereIf(input.Datefilter == "FirstAssignDate" && input.EndDate != null, e => e.WholeSaleLeadFK.FirstAssignDate <= input.EndDate)
                       //.WhereIf(role.Contains("Wholesale Manager"), e => e.BDMId == AbpSession.UserId)
                       //.WhereIf(role.Contains("Wholesale SalesRep"), e => e.SalesRapId == AbpSession.UserId)
                       //.WhereIf(input.Datefilter == "Followup" && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))
                       //.WhereIf(input.Datefilter == "NextFollowup" && input.StartDate != null, e => e.ActivityDate.Value.Date >= SDate.Value.Date)
                       //.WhereIf(input.Datefilter == "NextFollowup" && input.EndDate != null, e => e.ActivityDate.Value.Date <= EDate.Value.Date)
                       //.WhereIf(input.Datefilter == "NextFollowup" && input.EndDate != null, e => e.ActivityDate.Value.Date <= EDate.Value.Date)
                       //.WhereIf(input.SalesMangerId != null && input.SalesMangerId != 0, e => e.BDMId == input.SalesMangerId)
                       //.WhereIf(input.SalesRapId != null && input.SalesRapId != 0, e => e.SalesRapId == input.SalesRapId)
                       //.WhereIf(input.StatusId != null && input.StatusId != 0, e => e.WholeSaleStatusId == input.StatusId)
                       //.WhereIf(input.MyLead == true, e => e.CreatorUserId == AbpSession.UserId)
                       //.WhereIf(input.CreatedBy > 0, e => e.CreatorUserId == input.CreatedBy)
                       .Where(e => e.WholeSaleLeadFK.OrganizationId == input.OrganizationId)
                       .Select(e => new {e.Id, e.WholesaleLeadId, e.InvoiceNo, WholesaleInvoicetypeName = e.WholesaleInvoicetypeFK.Name, e.Reference, e.STCID,
                                            e.NoOfSTC, e.DeliveryDate, e.DeliveryTime, e.DeliveryOptions, e.InvoiceAmount, e.ActualFreigthCharges
                                           , e.WholeSaleLeadFK.FirstName, e.WholeSaleLeadFK.LastName, e.StoreLocationFK.location,
                                             WholesaleJobTypeName = e.WholesaleJobTypeFK.Name
                                            
                       });

            var pagedAndFilteredWholeSaleStatus = filteredWholeSale
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);
            var wholeSaleStatus = from o in pagedAndFilteredWholeSaleStatus
                                  select new GetWholeSaleInvoiceForViewDto()
                                  {
                                      WholesaleInvoiceDto = new WholesaleInvoiceDto()
                                      {
                                          Id = o.Id,
                                          WholesaleLeadId = o.WholesaleLeadId,
                                          InvoiceNo = o.InvoiceNo,
                                          InvoiceType = o.WholesaleInvoicetypeName,
                                          Reference = o.Reference,
                                          STCID = o.STCID,
                                          NoOfSTC = o.NoOfSTC,
                                          DeliveryOptions = o.DeliveryOptions,
                                         // Name = o.DeliveryOptions,
                                          DeliveryDate = o.DeliveryDate,
                                          DeliveryTime = o.DeliveryTime,
                                          InvoiceAmount = o.InvoiceAmount,
                                          ActualFreigthCharges = o.ActualFreigthCharges,
                                      },
                                      CustomerName = o.FirstName + o.LastName,
                                      Location = o.location,
                                      JobType = o.WholesaleJobTypeName,
                                      
                                  };

            var totalCount = await filteredWholeSale.CountAsync();


           var result = await wholeSaleStatus.ToListAsync();
            return new PagedResultDto<GetWholeSaleInvoiceForViewDto>(
               totalCount,
               result
           );
        }




        /*public async Task<GetWholeSaleInvoiceForViewDto> GetInvoiceItemPeriodForView(EntityDto input)
        {
            var result = _wholeSaleInvoiceRepository.GetAll().Where(e => e.Id == input.Id);
            var resultItemList = _ecommerceCartProductItemRepository.GetAll().Where(e => e.WholesaleInvoiceId == input.Id);

            var output = await (from o in result
                                select new GetWholeSaleInvoiceForViewDto
                                {
                                    WholesaleInvoiceDto = new WholesaleInvoiceDto
                                    {
                                        Id = o.Id,
                                        WholesaleLeadId = o.WholesaleLeadId,
                                        InvoiceNo = o.InvoiceNo,
                                        InvoiceTypeId = o.InvoiceTypeId,
                                        Reference = o.Reference,
                                        STCID = o.STCID,
                                        NoOfSTC = o.NoOfSTC,
                                        DeliveryOptions = o.DeliveryOptions,
                                        // Name = o.DeliveryOptions,
                                        DeliveryDate = o.DeliveryDate,
                                        DeliveryTime = o.DeliveryTime,
                                        InvoiceAmount = o.InvoiceAmount,
                                        ActualFreigthCharges = o.ActualFreigthCharges,

                                    },

                                  
                                }).FirstOrDefaultAsync();

            output.WholesaleInvoiceDto = (from o in resultItemList
                                           select new GetWholeSaleInvoiceForViewDto()
                                           {
                                               Id = o.Id,
                                               *//*Id = o.EcommerceProductItemFK.ProductItemID,*//*
                                               categoryId = o.EcommerceProductItemFK.ProductItemFk.ProductTypeId,
                                               ProductName = o.EcommerceProductItemFK.ProductItemFk.Name,

                                               EcommerceProductItemId = o.EcommerceProductItemId,
                                               *//* ProductItemId = (int)o.EcommerceProductItemFK.ProductItemID,*/

        /* ProductTypeId = o.EcommerceProductItemFK.ProductItemFk.ProductTypeId,*//*
        // ProductItemId =o.EcommerceProductItemFK.ProductItemFk.product,
        Model = o.EcommerceProductItemFK.ProductItemFk.Model,
        QTY = o.QTY,
        Price = price * o.QTY,
    }).ToList();

return output;
}
*/
        public async Task<List<WholesaleInvoiceDto>> GetWholesaleInvoicesByLeadId(int wholesaleLeadId)
        {
            var wholesaleInvoices = await _wholeSaleInvoiceRepository.GetAll()
                .Include(e => e.WholeSaleLeadFK)
                .Include(e => e.WholesaleInvoicetypeFK)
                .Where(e => e.WholesaleLeadId == wholesaleLeadId)
                .Select(e => new WholesaleInvoiceDto
                {
                    InvoiceNo = e.InvoiceNo,
                    CustomerName = e.WholeSaleLeadFK.FirstName + " " + e.WholeSaleLeadFK.LastName,
                    InvoiceType = e.WholesaleInvoicetypeFK.Name,
                    InvoiceDate = e.CreationTime,
                    InvoiceAmount = e.InvoiceAmount,
                    InvoiceAmountWithGST = e.InvoiceAmountWithGST,
                })
                .ToListAsync();

            return wholesaleInvoices;
        }



        public async Task<WholesaleInvoiceDto> GetWholeSaleLeadForEdit(EntityDto input)
        {


            var wholeSaleLead = await _wholeSaleInvoiceRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<WholesaleInvoiceDto>(wholeSaleLead);

            var wholeSaleLeadName = await _wholeSaleLeadRepository.FirstOrDefaultAsync((int)wholeSaleLead.WholesaleLeadId);

            output.CustomerName = (wholeSaleLeadName.FirstName ?? "") + (wholeSaleLeadName.LastName ?? "");

            var priceCat = await _wholeSaleLeadRepository.GetAll().Where(e => e.Id == wholeSaleLead.WholesaleLeadId).Select(e  => e.PriceCategoryId).FirstOrDefaultAsync();

            var EcomCart = _ecommerceCartProductItemRepository.GetAll().Where(e => e.WholesaleInvoiceId == wholeSaleLead.Id);

            output.ProductitemDetailDto = (from o in EcomCart
                                       let price = priceCat > 0 ? _ecommerceProductItemPriceCategoryRepository.GetAll().Where(e => e.EcommerceProductItemId == o.EcommerceProductItemFK.Id && e.ECommercePriceCategoryId == priceCat).Select(e => e.Price).FirstOrDefault() : 0
                                       select new EcommerceCartProductItemDto()
                                       {
                                          Id = o.Id,
                                           /*Id = o.EcommerceProductItemFK.ProductItemID,*/
                                           categoryId = o.EcommerceProductItemFK.ProductItemFk.ProductTypeId,
                                           ProductName = o.EcommerceProductItemFK.ProductItemFk.Name,

                                           EcommerceProductItemId = o.EcommerceProductItemId,

                                           Model = o.EcommerceProductItemFK.ProductItemFk.Model,
                                           QTY = o.QTY,
                                           Price = price * o.QTY,
                                          
                                       }).ToList();

            return output;
        }

        public async Task<List<CommonLookupInvoice>> GetAllInvoiceWithFullName()
        {

            var InstallerList = await (from u
                                  in _wholeSaleLeadRepository.GetAll()

                                       select new CommonLookupInvoice
                                       {
                                           Id = u.Id,
                                           CustomerName =(u.FirstName ?? "") + (u.LastName ?? "").ToString(),
                                           /* CompanyName = u.CompanyName*/
                                       }).ToListAsync();
            return InstallerList;

        }



    }
}
