﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;


using TheSolarProduct.JobCancellationReasons.Dtos;
using TheSolarProduct.Jobs;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using Stripe;

namespace TheSolarProduct.JobCancellationReasons
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_datavault_JobCancellationReason, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    public class JobCancellationReasonAppService : TheSolarProductAppServiceBase, IJobCancellationReasonAppService
    {
        private readonly IRepository<JobCancellationReason> _jobCancellationReasonsRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public JobCancellationReasonAppService(IRepository<JobCancellationReason> jobCancellationReasonsRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _jobCancellationReasonsRepository = jobCancellationReasonsRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task CreateOrEdit(CreateOrEditJobCancellationReasonDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_datavault_JobCancellationReason_Create)]
        protected virtual async Task Create(CreateOrEditJobCancellationReasonDto input)
        {

            var documenttype = ObjectMapper.Map<JobCancellationReason>(input);

            if (AbpSession.TenantId != null)
            {
                documenttype.TenantId = (int)AbpSession.TenantId;
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 9;
            dataVaultLog.ActionNote = "Job Cancellation Reason Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _jobCancellationReasonsRepository.InsertAsync(documenttype);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_datavault_JobCancellationReason_Edit)]
        protected virtual async Task Update(CreateOrEditJobCancellationReasonDto input)
        {
            var documenttype = await _jobCancellationReasonsRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 9;
            dataVaultLog.ActionNote = "Job Cancellation Reason Updated : " + documenttype.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != documenttype.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = documenttype.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != documenttype.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = documenttype.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, documenttype);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_datavault_JobCancellationReason_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _jobCancellationReasonsRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 9;
            dataVaultLog.ActionNote = "Job Cancellation Reason Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _jobCancellationReasonsRepository.DeleteAsync(input.Id);
        }

        public async Task<PagedResultDto<GetJobCancellationReasonForViewDto>> GetAll(GetAllJobCancellationReasonInput input)
        {
            var filteredDepartments = _jobCancellationReasonsRepository.GetAll()
                       .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var pagedAndFilteredDepartments = filteredDepartments
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var JobCancellationReason = from o in pagedAndFilteredDepartments
                                        select new GetJobCancellationReasonForViewDto()
                                        {
                                            JobCancellationReason = new JobCancellationReasonDto
                                            {
                                                Name = o.Name,
                                                Id = o.Id,
                                                IsActive = o.IsActive,
                                            }
                                        };

            var totalCount = await filteredDepartments.CountAsync();

            return new PagedResultDto<GetJobCancellationReasonForViewDto>(
                totalCount,
                await JobCancellationReason.ToListAsync()
            );
        }
                
        [AbpAuthorize(AppPermissions.Pages_Tenant_datavault_JobCancellationReason_Edit)]
        public async Task<GetJobCancellationReasonforEditOutput> GetJobCancellationReasonForEdit(EntityDto input)
        {
            var JobCancellationReason = await _jobCancellationReasonsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetJobCancellationReasonforEditOutput { JobCancellationReasonDto = ObjectMapper.Map<CreateOrEditJobCancellationReasonDto>(JobCancellationReason) };

            return output;
        }

        public async Task<GetJobCancellationReasonForViewDto> GetJobCancellationReasonForView(int id)
        {
            var JobCancellationReason = await _jobCancellationReasonsRepository.GetAsync(id);

            var output = new GetJobCancellationReasonForViewDto { JobCancellationReason = ObjectMapper.Map<JobCancellationReasonDto>(JobCancellationReason) };

            return output;
        }
    }
}
