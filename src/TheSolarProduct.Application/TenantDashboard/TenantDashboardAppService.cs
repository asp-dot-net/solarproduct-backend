﻿using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Organizations;
using Abp.Timing;
using Abp.Timing.Timezone;
using Microsoft.EntityFrameworkCore;
using NUglify.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Invoices;
using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Leads;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.LeadSources;
using TheSolarProduct.LeadStatuses;
using TheSolarProduct.MultiTenancy.HostDashboard.Dto;
using TheSolarProduct.Promotions;
using TheSolarProduct.Quotations;
using TheSolarProduct.TenantDashboard.Dto;
using TheSolarProduct.TheSolarDemo;

namespace TheSolarProduct.TenantDashboard
{
    public class TenantDashboardAppService : TheSolarProductAppServiceBase, ITenantDashboardAppService
    {
        //[AbpAuthorize(AppPermissions.HelloWorldPermission)]//check permission
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<Jobs.Job> _jobRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        //private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrgRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<Document> _documentRepository;
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
        private readonly IRepository<JobRefund> _jobRefundRepository;
        private readonly IRepository<JobPromotion> _jobPromotionRepository;
        private readonly IRepository<InstallerInvoice> _installerInoviceRepository;
        private readonly IRepository<PromotionUser> _promotionUserRepository;
        private readonly IRepository<Promotion> _promotionRepository;
        private readonly IRepository<LeadSource, int> _lookup_leadSourceRepository;
        private readonly IRepository<LeadStatus, int> _lookup_leadStatusRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<JobStatus, int> _lookup_jobStatusRepository;
        public TenantDashboardAppService(IRepository<Lead> leadRepository
            , IRepository<OrganizationUnit, long> organizationUnitRepository
            , IRepository<Jobs.Job> jobRepository
            , IRepository<LeadActivityLog> leadactivityRepository
            , IRepository<User, long> userRepository
            , IRepository<UserOrganizationUnit, long> userOrgRepository
            , IRepository<Document> documentRepository
            , IRepository<InvoicePayment> invoicePaymentRepository
            , IRepository<JobRefund> jobRefundRepository
            , IRepository<JobPromotion> jobPromotionRepository
            , IRepository<InstallerInvoice> installerInoviceRepository
            , IRepository<PromotionUser> promotionUserRepository
            , IRepository<Promotion> promotionRepository
            , IRepository<LeadSource, int> lookup_leadSourceRepository
            , IRepository<LeadStatus, int> lookup_leadStatusRepository,
            UserManager userManager,
            IRepository<UserTeam> userTeamRepository,
            ITimeZoneConverter timeZoneConverter,
            IRepository<JobStatus, int> lookup_jobStatusRepository,
            IRepository<JobProductItem> jobProductItemRepository
            )
        {
            _leadRepository = leadRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _jobRepository = jobRepository;
            //_userTeamRepository = userTeamRepository;
            _leadactivityRepository = leadactivityRepository;
            _userRepository = userRepository;
            _userOrgRepository = userOrgRepository;
            _documentRepository = documentRepository;
            _invoicePaymentRepository = invoicePaymentRepository;
            _jobRefundRepository = jobRefundRepository;
            _jobPromotionRepository = jobPromotionRepository;
            _installerInoviceRepository = installerInoviceRepository;
            _promotionUserRepository = promotionUserRepository;
            _promotionRepository = promotionRepository;
            _lookup_leadSourceRepository = lookup_leadSourceRepository;
            _lookup_leadStatusRepository = lookup_leadStatusRepository;
            _userManager = userManager;
            _userTeamRepository = userTeamRepository;
            _timeZoneConverter = timeZoneConverter;
            _lookup_jobStatusRepository = lookup_jobStatusRepository;
            _jobProductItemRepository = jobProductItemRepository;
        }

        public List<LeadSummary> GetLeadSummary(DateTime startDate, DateTime endDate)
        {
            var SDate = (_timeZoneConverter.Convert(startDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(endDate, (int)AbpSession.TenantId));

            //var leads = (from o in _organizationUnitRepository.GetAll()
            //             join o3 in _leadRepository.GetAll() on o.Id equals o3.OrganizationId into ou
            //             from _ou in ou.DefaultIfEmpty()
            //             where (_ou.CreationTime >= startDate && _ou.CreationTime <= endDate)
            //             select new //LeadSummary()
            //             {
            //                 OrgName = o.DisplayName,
            //                 Lead = _ou.Id,
            //                 DuplicateLead = _ou.IsDuplicate == null ? false : _ou.IsDuplicate.Value,
            //                 WebDuplicateLead = _ou.IsWebDuplicate == null ? false : _ou.IsWebDuplicate.Value,
            //                 OrgId = o.Id
            //                 //TotalLead = ds.Count()
            //             });
            var leads = (from o in _organizationUnitRepository.GetAll()
                         join o3 in _leadRepository.GetAll() on o.Id equals o3.OrganizationId into ou
                         from _ou in ou.DefaultIfEmpty()
                         where (_ou.CreationTime.Date >= SDate.Value.Date && _ou.CreationTime.Date <= EDate.Value.Date)
                         select new
                         {
                             OrgName = o.DisplayName,
                             OrgId = o.Id
                         }).Distinct();
            //var grouping = from db in leads
            //               group db by db.OrgName into g
            //               select new LeadSummary
            //               {
            //                   OrgName = g.FirstOrDefault().OrgName,
            //                   DuplicateLead = g.Where(x => x.DuplicateLead == true || x.WebDuplicateLead == true).Count(),
            //                   Lead = g.Where(x => x.DuplicateLead == false || x.WebDuplicateLead == false).Count(),
            //                   TotalLead = g.Count()
            //               };
            List<LeadSummary> leadss = new List<LeadSummary>();
            var distOrgId = leads.Select(x => x.OrgId);
            foreach (var item in distOrgId)
            {
                LeadSummary ls = new LeadSummary();
                var rec = leads.Where(x => x.OrgId == item).ToList();
                if (rec != null && rec.Count > 0 && rec.FirstOrDefault() != null && rec.FirstOrDefault().OrgId > 0)
                {
                    ls.OrgId = (int)rec.FirstOrDefault().OrgId;
                    ls.OrgName = rec.FirstOrDefault().OrgName;
                    ls.DuplicateLead = _leadRepository.GetAll().Where(x => (((x.IsDuplicate == null ? false : x.IsDuplicate.Value) == true)
                    || ((x.IsWebDuplicate == null ? false : x.IsWebDuplicate.Value) == true))
                    && x.OrganizationId == item && (x.CreationTime >= SDate.Value.Date && x.CreationTime <= EDate.Value.Date)).Count();
                    ls.Lead = _leadRepository.GetAll().Where(x => (((x.IsDuplicate == null ? false : x.IsDuplicate.Value) == false)
                    && ((x.IsWebDuplicate == null ? false : x.IsWebDuplicate.Value) == false))
                    && x.OrganizationId == item && (x.CreationTime >= SDate.Value.Date && x.CreationTime <= EDate.Value.Date)).Count();
                    ls.TotalLead = ls.DuplicateLead + ls.Lead;
                    leadss.Add(ls);
                }
            }
            if (leadss.Count > 0)
            {
                LeadSummary leadsum = new LeadSummary();
                leadsum.OrgName = "Total";
                leadsum.Lead = leadss.Sum(x => x.Lead);
                leadsum.DuplicateLead = leadss.Sum(x => x.DuplicateLead);
                leadsum.TotalLead = leadss.Sum(x => x.TotalLead);
                leadss.Add(leadsum);
            }
            //from product in _leadRepository.GetAll()
            //join organizationUnit in _organizationUnitRepository.GetAll() on product.OrganizationUnitId equals organizationUnit.Id
            //where organizationUnit.Code.StartsWith(code)
            //select product;
            return leadss.ToList();
        }

        public List<LeadSummaryDetails> GetLeadSummaryDetails(DateTime startDate, DateTime endDate)
        {
            var SDate = (_timeZoneConverter.Convert(startDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(endDate, (int)AbpSession.TenantId));

            var leads = (from o in _organizationUnitRepository.GetAll()
                         select new LeadSummaryDetails
                         {
                             OrgName = o.DisplayName,
                             Jobs = _jobRepository.GetAll().Include(x => x.LeadFk).Where(x => x.LeadId == x.LeadFk.Id
                               && (x.LeadFk.CreationTime.Date >= SDate.Value.Date && x.LeadFk.CreationTime.Date <= EDate.Value.Date)
                               && x.LeadFk.OrganizationId == o.Id).Count(),

                             Lead = _leadRepository.GetAll().Where(x => (x.CreationTime.Date >= SDate.Value.Date && x.CreationTime.Date <= EDate.Value.Date)
                             && x.OrganizationId == o.Id).Count(),

                             DepositeReceived = _jobRepository.GetAll().Include(x => x.LeadFk).Where(x => x.LeadId == x.LeadFk.Id
                              && (x.LeadFk.CreationTime.Date >= SDate.Value.Date && x.LeadFk.CreationTime.Date <= EDate.Value.Date) && x.LeadFk.OrganizationId == o.Id
                              && x.JobStatusId == 4).Count(),

                             Active = _jobRepository.GetAll().Include(x => x.LeadFk).Where(x => x.LeadId == x.LeadFk.Id
                              && (x.LeadFk.CreationTime.Date >= SDate.Value.Date && x.LeadFk.CreationTime.Date <= EDate.Value.Date) && x.LeadFk.OrganizationId == o.Id
                              && x.JobStatusId == 5).Count(),

                             InstallationBook = _jobRepository.GetAll().Include(x => x.LeadFk).Where(x => x.LeadId == x.LeadFk.Id
                                 && (x.LeadFk.CreationTime.Date >= SDate.Value.Date && x.LeadFk.CreationTime.Date <= EDate.Value.Date) && x.LeadFk.OrganizationId == o.Id
                                 && x.JobStatusId == 6).Count(),

                             InstallationCompleted = _jobRepository.GetAll().Include(x => x.LeadFk).Where(x => x.LeadId == x.LeadFk.Id
                               && (x.LeadFk.CreationTime.Date >= SDate.Value.Date && x.LeadFk.CreationTime.Date <= EDate.Value.Date) && x.LeadFk.OrganizationId == o.Id
                               && x.JobStatusId == 7).Count(),

                             Completed = _jobRepository.GetAll().Include(x => x.LeadFk).Where(x => x.LeadId == x.LeadFk.Id
                               && (x.LeadFk.CreationTime.Date >= SDate.Value.Date && x.LeadFk.CreationTime.Date <= EDate.Value.Date) && x.LeadFk.OrganizationId == o.Id
                               && x.JobStatusId == 8).Count()
                         }).ToList();
            //if (leads.Count > 0)
            //{
            //    LeadSummary leadsum = new LeadSummary();
            //    leadsum.OrgName = "Total";
            //    leadsum.Lead = leadss.Sum(x => x.Lead);
            //    leadsum.DuplicateLead = leadss.Sum(x => x.DuplicateLead);
            //    leadsum.TotalLead = leadss.Sum(x => x.TotalLead);
            //    leadss.Add(leadsum);
            //}
            return leads.ToList();
        }


        //int UserId = (int)AbpSession.UserId;
        ///public List<MyRemindersDto> MyReminder()
        ///{
        //var _lead = (from dr in _leadRepository.GetAll()
        //            .Where(x => x.AssignToUserID == UserId)

        //             join dr1 in _leadactivityRepository.GetAll()
        //             .Where(dr1 => (dr1.ActivityDate.Value.Date >= startDate.Date && dr1.ActivityDate.Value.Date <= endDate.Date) && dr1.ActionId == 8)
        //             on dr.Id equals dr1.LeadId

        //             select new MyRemindersDto
        //             {
        //                 CustomerName = dr.CompanyName,
        //                 Comment = dr1.ActivityNote,
        //                 ReminderDate = dr1.ActivityDate
        //             });

        //return _lead.ToList();
        /// }


        public async Task<List<MyRemindersDto>> MyReminder()
        {
            //DateTime startDate = DateTime.UtcNow.AddHours(10);
            DateTime startDate = DateTime.UtcNow;
            var SDate = (_timeZoneConverter.Convert(startDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var Leads = _leadRepository.GetAll()
            //            .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
            //            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
            //            .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
            //            .Where(e => e.LeadStatusId != 8).Select(e => e.Id).ToList();

            //var _lead = _leadactivityRepository.GetAll().
            //            Where(e => e.ActivityDate.Value.Date == startDate.Date && e.ActionId == 8 && Leads.Contains(e.LeadId));

            var Result = new List<MyRemindersDto>();

            //Result = (from l in _lead
            //          select new MyRemindersDto()
            //          {
            //              CustomerName = _leadRepository.GetAll().Where(e => e.Id == l.LeadId).Select(e => e.CompanyName).FirstOrDefault(),
            //              Comment = l.ActivityNote,
            //              ReminderDate = l.ActivityDate,
            //              Source = _leadRepository.GetAll().Where(e => e.Id == l.LeadId).Select(e => e.LeadSource).FirstOrDefault(),
            //              email = _leadRepository.GetAll().Where(e => e.Id == l.LeadId).Select(e => e.Email).FirstOrDefault(),
            //              Mobileno = _leadRepository.GetAll().Where(e => e.Id == l.LeadId).Select(e => e.Mobile).FirstOrDefault(),
            //          }).ToList();

            var _lead = _leadactivityRepository.GetAll().Include(e => e.LeadFk).Select(e => new { e.LeadFk.CompanyName, e.LeadFk.Email, e.LeadFk.Mobile, e.LeadFk.LeadSource, e.ActivityNote, e.ActivityDate, e.ActionId, e.LeadFk })
                .Where(e => e.ActivityDate.Value.Date == SDate.Value.Date && e.ActionId == 8)
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id);

            Result = (from l in _lead
                      select new MyRemindersDto()
                      {
                          CustomerName = l.CompanyName,
                          Comment = l.ActivityNote,
                          ReminderDate = l.ActivityDate,
                          Source = l.LeadSource,
                          email = l.Email,
                          Mobileno = l.Mobile,
                      }).ToList();


            return Result;
        }

        public async Task<List<SalesDetailDto>> SalesDetail()
        {

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var Leads = (from o in _leadRepository.GetAll()
            //            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
            //            select new
            //            {
            //            UserID = o.CreatorUserId,
            //            }).Distinct();
            List<SalesDetailDto> result = new List<SalesDetailDto>();
            if (role.Contains("Sales Manager"))
            {
                foreach (var item in UserList)
                {
                    var rec = _leadRepository.GetAll().Where(x => x.AssignToUserID == item).ToList();
                    var leadids = rec.Select(e => e.Id).ToList();
                    //var joblist = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId)).Select(e => e.JobStatusId).ToList();
                    //var joblist = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId)).ToList();

                    var joblist = (from o in _jobRepository.GetAll()
                                .Where(e => leadids.Contains((int)e.LeadId))
                                   select new
                                   {

                                       Id = o.Id,
                                       JobStatusId = o.JobStatusId,
                                       TotalCost = o.TotalCost,
                                   }).ToList();

                    if (rec != null)
                    {
                        SalesDetailDto ls = new SalesDetailDto();
                        {
                            ls.Name = _userRepository.GetAll().Where(e => e.Id == item).Select(e => e.FullName).FirstOrDefault();
                            ls.New = rec.Where(e => e.LeadStatusId == 1).Count();
                            ls.UnHandled = rec.Where(e => e.LeadStatusId == 2).Count();
                            ls.Cold = rec.Where(e => e.LeadStatusId == 3).Count();
                            ls.Warm = rec.Where(e => e.LeadStatusId == 4).Count();
                            ls.Hot = rec.Where(e => e.LeadStatusId == 5).Count();
                            ls.Upgrade = rec.Where(e => e.LeadStatusId == 6).Count();
                            ls.Rejected = rec.Where(e => e.LeadStatusId == 7).Count();
                            ls.Canceled = rec.Where(e => e.LeadStatusId == 8).Count();
                            ls.Closed = rec.Where(e => e.LeadStatusId == 9).Count();
                            ls.Assigned = rec.Where(e => e.LeadStatusId == 10).Count();
                            ls.ReAssigned = rec.Where(e => e.LeadStatusId == 11).Count();
                            ls.Planned = joblist.Where(e => e.JobStatusId == 1).Count();
                            ls.OnHold = joblist.Where(e => e.JobStatusId == 2).Count();
                            ls.Cancel = joblist.Where(e => e.JobStatusId == 3).Count();
                            ls.DepositeReceived = joblist.Where(e => e.JobStatusId == 4).Count();
                            ls.Active = joblist.Where(e => e.JobStatusId == 5).Count();
                            ls.JobBook = joblist.Where(e => e.JobStatusId == 6).Count();
                            ls.JobIncomplete = joblist.Where(e => e.JobStatusId == 7).Count();
                            ls.JobInstalled = joblist.Where(e => e.JobStatusId == 8).Count();

                            ls.PlannedTotalCost = joblist.Where(e => e.JobStatusId == 1).Select(e => e.TotalCost).Sum();
                            ls.OnHoldTotalCost = joblist.Where(e => e.JobStatusId == 2).Select(e => e.TotalCost).Sum();
                            ls.CancelTotalCost = joblist.Where(e => e.JobStatusId == 3).Select(e => e.TotalCost).Sum();
                            ls.DepositeReceivedTotalCost = joblist.Where(e => e.JobStatusId == 4).Select(e => e.TotalCost).Sum();
                            ls.ActiveTotalCost = joblist.Where(e => e.JobStatusId == 5).Select(e => e.TotalCost).Sum();
                            ls.JobBookTotalCost = joblist.Where(e => e.JobStatusId == 6).Select(e => e.TotalCost).Sum();
                            ls.JobIncompleteTotalCost = joblist.Where(e => e.JobStatusId == 7).Select(e => e.TotalCost).Sum();
                            ls.JobInstalledTotalCost = joblist.Where(e => e.JobStatusId == 8).Select(e => e.TotalCost).Sum();



                            ls.TotalJobs = joblist.Select(e => e.Id).Count();
                            ls.TotalCost = joblist.Select(e => e.TotalCost).Sum();
                            ls.TotalJobsRefundAmount = _jobRefundRepository.GetAll().Where(e => leadids.Contains((int)e.JobFk.LeadId) && e.JobFk.JobStatusId != 3).Select(e => e.Id).Count();
                            ls.TotalJobsCost = (ls.TotalCost - (ls.CancelTotalCost + ls.TotalJobsRefundAmount));

                            result.Add(ls);
                        }
                    }
                }

                SalesDetailDto leadsum = new SalesDetailDto();
                leadsum.Name = "Total";
                leadsum.New = result.Sum(x => x.New);
                leadsum.UnHandled = result.Sum(x => x.UnHandled);
                leadsum.Cold = result.Sum(x => x.Cold);
                leadsum.Warm = result.Sum(x => x.Warm);
                leadsum.Hot = result.Sum(x => x.Hot);
                leadsum.Upgrade = result.Sum(x => x.Upgrade);
                leadsum.Rejected = result.Sum(x => x.Rejected);
                leadsum.Canceled = result.Sum(x => x.Canceled);
                leadsum.Closed = result.Sum(x => x.Closed);
                leadsum.Assigned = result.Sum(x => x.Assigned);
                leadsum.ReAssigned = result.Sum(x => x.ReAssigned);
                leadsum.Planned = result.Sum(x => x.Planned);
                leadsum.OnHold = result.Sum(x => x.OnHold);
                leadsum.Cancel = result.Sum(x => x.Cancel);
                leadsum.DepositeReceived = result.Sum(x => x.DepositeReceived);
                leadsum.Active = result.Sum(x => x.Active);
                leadsum.JobBook = result.Sum(x => x.JobBook);
                leadsum.JobIncomplete = result.Sum(x => x.JobIncomplete);
                leadsum.JobInstalled = result.Sum(x => x.JobInstalled);
                leadsum.TotalJobs = result.Sum(x => x.TotalJobs);
                leadsum.TotalJobsCost = result.Sum(x => x.TotalJobsCost);
                leadsum.PlannedTotalCost = result.Sum(x => x.PlannedTotalCost);
                leadsum.OnHoldTotalCost = result.Sum(x => x.OnHoldTotalCost);
                leadsum.CancelTotalCost = result.Sum(x => x.CancelTotalCost);
                leadsum.DepositeReceivedTotalCost = result.Sum(x => x.DepositeReceivedTotalCost);
                leadsum.ActiveTotalCost = result.Sum(x => x.ActiveTotalCost);
                leadsum.JobBookTotalCost = result.Sum(x => x.JobBookTotalCost);
                leadsum.JobIncompleteTotalCost = result.Sum(x => x.JobIncompleteTotalCost);
                leadsum.JobInstalledTotalCost = result.Sum(x => x.JobInstalledTotalCost);

                result.Add(leadsum);
            }

            return result.ToList();
        }

        public async Task<SalesDetailDto> GetLeadCountsForSalesRep(int? Id)
        {
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var Result = new SalesDetailDto();
            if (role.Contains("Sales Rep"))
            {
                List<int> jobstst = new List<int>() { 4, 5 };
                List<int> jobch = new List<int>() { 2,3};

                List<int> jobbook = new List<int>() { 6,7 };
                var rec = _leadRepository.GetAll().Where(x => x.AssignToUserID == User.Id).ToList();
                var leadids = _leadRepository.GetAll().Where(x => x.AssignToUserID == User.Id).Select(e => e.Id).ToList();

                // Lead Statistics Count
                Result.Assigned = rec.Where(e => e.LeadStatusId == 10).Count();
                Result.Cold = rec.Where(e => e.LeadStatusId == 3).Count();
                Result.Warm = rec.Where(e => e.LeadStatusId == 4).Count();
                Result.Hot = rec.Where(e => e.LeadStatusId == 5).Count();
                Result.UnHandled = rec.Where(e => e.LeadStatusId == 2).Count();
                Result.Canceled = rec.Where(e => e.LeadStatusId == 8).Count();
                Result.TotalLeads = Result.Assigned + Result.Cold + Result.Warm + Result.Hot + Result.UnHandled + Result.Canceled;

                //Result.New = rec.Where(e => e.LeadStatusId == 1).Count();
                //Result.Upgrade = rec.Where(e => e.LeadStatusId == 6).Count();
                //Result.Rejected = rec.Where(e => e.LeadStatusId == 7).Count();
                //Result.Closed = rec.Where(e => e.LeadStatusId == 9).Count();
                //Result.ReAssigned = rec.Where(e => e.LeadStatusId == 11).Count();

                // Sales Detail Count
                Result.Planned = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId) && e.JobStatusId == 1).Count();
                Result.DepositeReceived = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId) && jobstst.Contains((int)e.JobStatusId)).Count();
                Result.JobBook = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId) && jobbook.Contains((int)e.JobStatusId)).Count();
                Result.JobInstalled = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId) && e.JobStatusId == 8).Count();
                Result.TotalJobs = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId)).Select(e => e.Id).Count();
                Result.Cancel = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId) && jobch.Contains((int)e.JobStatusId)).Count();

                //Result.OnHold = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId) && e.JobStatusId == 2).Count();
                //Result.Active = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId) && e.JobStatusId == 5).Count();
                //Result.JobIncomplete = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId) && e.JobStatusId == 7).Count();
                //Result.PlannedTotalCost = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId) && e.JobStatusId == 1).Select(e => e.TotalCost).Sum();
                //Result.OnHoldTotalCost = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId) && e.JobStatusId == 2).Select(e => e.TotalCost).Sum();
                //Result.CancelTotalCost = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId) && e.JobStatusId == 3).Select(e => e.TotalCost).Sum();
                //Result.DepositeReceivedTotalCost = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId) && jobstst.Contains((int) e.JobStatusId)).Select(e => e.TotalCost).Sum();
                //Result.ActiveTotalCost = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId) && e.JobStatusId == 5).Select(e => e.TotalCost).Sum();
                //Result.JobBookTotalCost = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId) && jobbook.Contains((int)e.JobStatusId)).Select(e => e.TotalCost).Sum();
                //Result.JobIncompleteTotalCost = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId) && e.JobStatusId == 7).Select(e => e.TotalCost).Sum();
                //Result.JobInstalledTotalCost = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId) && e.JobStatusId == 8).Select(e => e.TotalCost).Sum();
                 
                //Result.TotalCost = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId)).Select(e => e.TotalCost).Sum();
                //Result.TotalJobsRefundAmount = _jobRefundRepository.GetAll().Where(e => leadids.Contains((int)e.JobFk.LeadId) && e.JobFk.JobStatusId != 3).Select(e => e.Id).Count();
                //Result.TotalJobsCost = (Result.TotalCost - (Result.CancelTotalCost + Result.TotalJobsRefundAmount));
            }
            else
            {
                var rec = _leadRepository.GetAll().WhereIf(Id != 0, x => x.AssignToUserID == Id).ToList()
                    .WhereIf(Id == 0, x => UserList.Contains(x.AssignToUserID)).ToList();
                //var leadids = _leadRepository.GetAll().WhereIf(Id != 0 , x => x.AssignToUserID == Id).Select(e => e.Id).ToList();

                var leadids = rec.Select(e => e.Id).ToList();
                //var joblist = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId)).Select(e => e.JobStatusId).ToList();
                //var joblist = _jobRepository.GetAll().Where(e => leadids.Contains((int)e.LeadId)).ToList();

                var joblist = (from o in _jobRepository.GetAll()
                            .Where(e => leadids.Contains((int)e.LeadId))
                               select new
                               {

                                   Id = o.Id,
                                   JobStatusId = o.JobStatusId,
                                   TotalCost = o.TotalCost,
                               }).ToList();



                Result.New = rec.Where(e => e.LeadStatusId == 1).Count();
                Result.UnHandled = rec.Where(e => e.LeadStatusId == 2).Count();
                Result.Cold = rec.Where(e => e.LeadStatusId == 3).Count();
                Result.Warm = rec.Where(e => e.LeadStatusId == 4).Count();
                Result.Hot = rec.Where(e => e.LeadStatusId == 5).Count();
                Result.Upgrade = rec.Where(e => e.LeadStatusId == 6).Count();
                Result.Rejected = rec.Where(e => e.LeadStatusId == 7).Count();
                Result.Canceled = rec.Where(e => e.LeadStatusId == 8).Count();
                Result.Closed = rec.Where(e => e.LeadStatusId == 9).Count();
                Result.Assigned = rec.Where(e => e.LeadStatusId == 10).Count();
                Result.ReAssigned = rec.Where(e => e.LeadStatusId == 11).Count();
                Result.Planned = joblist.Where(e => e.JobStatusId == 1).Count();
                Result.OnHold = joblist.Where(e => e.JobStatusId == 2).Count();
                Result.Cancel = joblist.Where(e => e.JobStatusId == 3).Count();
                Result.DepositeReceived = joblist.Where(e => e.JobStatusId == 4).Count();
                Result.Active = joblist.Where(e => e.JobStatusId == 5).Count();
                Result.JobBook = joblist.Where(e => e.JobStatusId == 6).Count();
                Result.JobIncomplete = joblist.Where(e => e.JobStatusId == 7).Count();
                Result.JobInstalled = joblist.Where(e => e.JobStatusId == 8).Count();


                Result.PlannedTotalCost = joblist.Where(e => e.JobStatusId == 1).Select(e => e.TotalCost).Sum();
                Result.OnHoldTotalCost = joblist.Where(e => e.JobStatusId == 2).Select(e => e.TotalCost).Sum();
                Result.CancelTotalCost = joblist.Where(e => e.JobStatusId == 3).Select(e => e.TotalCost).Sum();
                Result.DepositeReceivedTotalCost = joblist.Where(e => e.JobStatusId == 4).Select(e => e.TotalCost).Sum();
                Result.ActiveTotalCost = joblist.Where(e => e.JobStatusId == 5).Select(e => e.TotalCost).Sum();
                Result.JobBookTotalCost = joblist.Where(e => e.JobStatusId == 6).Select(e => e.TotalCost).Sum();
                Result.JobIncompleteTotalCost = joblist.Where(e => e.JobStatusId == 7).Select(e => e.TotalCost).Sum();
                Result.JobInstalledTotalCost = joblist.Where(e => e.JobStatusId == 8).Select(e => e.TotalCost).Sum();


                Result.TotalJobs = joblist.Select(e => e.Id).Count();
                Result.TotalCost = joblist.Select(e => e.TotalCost).Sum();
                Result.TotalJobsRefundAmount = _jobRefundRepository.GetAll().Where(e => leadids.Contains((int)e.JobFk.LeadId) && e.JobFk.JobStatusId != 3).Select(e => e.Id).Count();
                Result.TotalJobsCost = (Result.TotalCost - (Result.CancelTotalCost + Result.TotalJobsRefundAmount));
            }
            return Result;
        }

        public OrganizationWiseApplicationTracker OrganizationWiseTrackerCt()
        {
            OrganizationWiseApplicationTracker obj = new OrganizationWiseApplicationTracker();

            int UserId = (int)AbpSession.UserId;
            var OrgList = (from uou in _userOrgRepository.GetAll()
                           join org in _organizationUnitRepository.GetAll() on uou.OrganizationUnitId equals org.Id
                           where uou.UserId == UserId
                           select new OrganizationName { OrgName = org.DisplayName, Id = org.Id });
            obj.Organizations = OrgList.ToList();
            List<TrackerInfo> tiList = new List<TrackerInfo>();
            foreach (var item in OrgList)
            {
                TrackerInfo ti = new TrackerInfo();
                ti.OrgName = item.OrgName;
                ti.OrgId = item.Id;
                var Doclist = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 2).Select(e => e.JobId).ToList();
                ti.PendingApplication = _jobRepository.GetAll().Where(e => e.JobStatusId >= 4 && e.MeterPhaseId != 0 && e.MeterPhaseId != null && e.PeakMeterNo != null && e.NMINumber != null && Doclist.Contains(e.Id) && e.LeadFk.OrganizationId == item.Id && e.ApplicationRefNo == null).Count();
                ti.PendingGridConnectionApplication = 0;
                ti.PendingSTC = 0;
                ti.PendingProjectInvoice = _invoicePaymentRepository.GetAll().Where(j => (_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == item.Id).Where(e => e.IsVerified == false).Count();
                ti.PendingInstallerInvoice = _installerInoviceRepository.GetAll().Where(j => (_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == item.Id).Count();
                ti.FinanceRequest = _jobRepository.GetAll().Where(e => e.PaymentOptionId != null && e.PaymentOptionId != 1 && e.LeadFk.OrganizationId == item.Id).Count();
                ti.RefundRequest = _jobRefundRepository.GetAll().Where(j => (_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == item.Id).Count();
                ti.Freebies = _jobPromotionRepository.GetAll().Where(e => e.JobFk.LeadFk.OrganizationId == item.Id && e.JobFk.TotalCost <= (_invoicePaymentRepository.GetAll().Where(i => i.JobId == e.JobFk.Id).Select(e => e.InvoicePayTotal).Sum())).Count();
                ti.UnassignedLeads = _leadRepository.GetAll().Where(x => x.OrganizationId == item.Id && x.AssignToUserID == null).Count();
                ti.NewLeads = _leadRepository.GetAll().Where(x => x.OrganizationId == item.Id && x.LeadStatusId == 1).Count();
                ti.AssignedNewLeads = (from dr in _leadRepository.GetAll().Where(x => x.OrganizationId == item.Id && x.LeadStatusId == 10 && x.AssignToUserID != null)
                                       join u in _userRepository.GetAll() on dr.AssignToUserID.Value equals u.Id
                                       select new NewLeadUserNames
                                       {
                                           name = u.FullName,
                                           NewLeadCount = _leadRepository.GetAll().Where(x => x.OrganizationId == item.Id && x.LeadStatusId == 10 && x.AssignToUserID == dr.Id).Count()
                                       }).ToList();
                ti.PendingInstallation = _jobRepository.GetAll().Where(e => e.JobStatusId == 5 && e.LeadFk.OrganizationId == item.Id && (e.InstallationDate != null && e.InstallationDate.Value == DateTime.Now.Date)).Count();
                ti.TodaysInstallation = _jobRepository.GetAll().Where(e => e.JobStatusId == 5 && e.LeadFk.OrganizationId == item.Id && (e.InstallationDate != null && e.InstallationDate.Value == DateTime.Now.Date) && e.JobStatusId == 8).Count() + "/" +
                    _jobRepository.GetAll().Where(e => e.JobStatusId == 5 && e.LeadFk.OrganizationId == item.Id && (e.InstallationDate != null && e.InstallationDate.Value == DateTime.Now.Date)).Count();
                ti.PromoReply = _promotionUserRepository.GetAll().Where(x => x.ResponseDate.Date == DateTime.Now.Date).Count();
                tiList.Add(ti);
            }
            obj.TrackerInformation = tiList;
            return obj;
        }

        public DataTable OrganizationWiseTrackerCount()
        {
            OrganizationWiseApplicationTracker obj = new OrganizationWiseApplicationTracker();

            int UserId = (int)AbpSession.UserId;
            var OrgList = (from uou in _userOrgRepository.GetAll()
                           join org in _organizationUnitRepository.GetAll() on uou.OrganizationUnitId equals org.Id
                           where uou.UserId == UserId
                           select new OrganizationName { OrgName = org.DisplayName, Id = org.Id }).OrderBy(x => x.Id);
            obj.Organizations = OrgList.ToList();
            List<TrackerInfo> tiList = new List<TrackerInfo>();
            DataTable dt = new DataTable();
            dt.Columns.Add("Tracker");
            foreach (var item in OrgList)
            {
                dt.Columns.Add(item.OrgName);
            }
            DataRow drPA = dt.NewRow();
            foreach (var item in OrgList)
            {
                drPA[item.OrgName] = (item.OrgName);
            }
            dt.Rows.Add(drPA);
            var Doclist = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 2).Select(e => e.JobId).ToList();
            drPA = dt.NewRow();
            drPA["Tracker"] = "Pending Application";
            foreach (var item in OrgList)
            {
                int PendingApplication = _jobRepository.GetAll().Where(e => e.JobStatusId >= 4 && e.MeterPhaseId != 0 && e.MeterPhaseId != null && e.PeakMeterNo != null && e.NMINumber != null && Doclist.Contains(e.Id) && e.LeadFk.OrganizationId == item.Id && e.ApplicationRefNo == null).Count();
                drPA[item.OrgName] = PendingApplication + "";
            }
            dt.Rows.Add(drPA);

            drPA = dt.NewRow();
            drPA["Tracker"] = "Pending Grid Connection Application";
            foreach (var item in OrgList)
            {
                int PendingGridConnectionApplication = 0;
                drPA[item.OrgName] = PendingGridConnectionApplication + "";
            }
            dt.Rows.Add(drPA);

            drPA = dt.NewRow();
            drPA["Tracker"] = "Pending STC";
            foreach (var item in OrgList)
            {
                int PendingSTC = 0;
                drPA[item.OrgName] = PendingSTC + "";
            }
            dt.Rows.Add(drPA);

            drPA = dt.NewRow();
            drPA["Tracker"] = "Pending Project Invoice";
            foreach (var item in OrgList)
            {
                int PendingProjectInvoice = _invoicePaymentRepository.GetAll().Where(j => (_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == item.Id).Where(e => e.IsVerified == false).Count();
                drPA[item.OrgName] = PendingProjectInvoice + "";
            }
            dt.Rows.Add(drPA);

            drPA = dt.NewRow();
            drPA["Tracker"] = "Pending Installer Invoice";
            foreach (var item in OrgList)
            {
                int PendingInstallerInvoice = _installerInoviceRepository.GetAll().Where(j => (_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == item.Id).Count();
                drPA[item.OrgName] = PendingInstallerInvoice + "";
            }
            dt.Rows.Add(drPA);

            drPA = dt.NewRow();
            drPA["Tracker"] = "Finance Request";
            foreach (var item in OrgList)
            {
                int FinanceRequest = _jobRepository.GetAll().Where(e => e.PaymentOptionId != null && e.PaymentOptionId != 1 && e.LeadFk.OrganizationId == item.Id).Count();
                drPA[item.OrgName] = FinanceRequest + "";
            }
            dt.Rows.Add(drPA);

            drPA = dt.NewRow();
            drPA["Tracker"] = "Refund Request";
            foreach (var item in OrgList)
            {
                int RefundRequest = _jobRefundRepository.GetAll().Where(j => (_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == item.Id).Count();
                drPA[item.OrgName] = RefundRequest + "";
            }
            dt.Rows.Add(drPA);

            drPA = dt.NewRow();
            drPA["Tracker"] = "Freebies";
            foreach (var item in OrgList)
            {
                int Freebies = _jobPromotionRepository.GetAll().Where(e => e.JobFk.LeadFk.OrganizationId == item.Id && e.JobFk.TotalCost <= (_invoicePaymentRepository.GetAll().Where(i => i.JobId == e.JobFk.Id).Select(e => e.InvoicePayTotal).Sum())).Count();
                drPA[item.OrgName] = Freebies + "";
            }
            dt.Rows.Add(drPA);

            drPA = dt.NewRow();
            drPA["Tracker"] = "Unassigned Leads";
            foreach (var item in OrgList)
            {
                int UnassignedLeads = _leadRepository.GetAll().Where(x => x.OrganizationId == item.Id && x.AssignToUserID == null).Count();
                drPA[item.OrgName] = UnassignedLeads + "";
            }
            dt.Rows.Add(drPA);

            drPA = dt.NewRow();
            drPA["Tracker"] = "New Leads";
            foreach (var item in OrgList)
            {
                int NewLeads = _leadRepository.GetAll().Where(x => x.OrganizationId == item.Id && x.LeadStatusId == 1).Count();
                drPA[item.OrgName] = NewLeads + "";
            }
            dt.Rows.Add(drPA);

            //drPA = dt.NewRow();
            //foreach (var item in OrgList)
            //{
            //    int AssignedNewLeads = (from dr in _leadRepository.GetAll().Where(x => x.OrganizationId == item.Id && x.LeadStatusId == 10 && x.AssignToUserID != null)
            //                            join u in _userRepository.GetAll() on dr.AssignToUserID.Value equals u.Id
            //                            select new NewLeadUserNames
            //                            {
            //                                name = u.FullName,
            //                                NewLeadCount = _leadRepository.GetAll().Where(x => x.OrganizationId == item.Id && x.LeadStatusId == 10 && x.AssignToUserID == dr.Id).Count()
            //                            }).ToList();
            //    drPA[item.OrgName] = NewLeads + "";
            //}
            //dt.Rows.Add(drPA);

            drPA = dt.NewRow();
            drPA["Tracker"] = "Pending Installation";
            foreach (var item in OrgList)
            {
                int PendingInstallation = _jobRepository.GetAll().Where(e => e.JobStatusId == 5 && e.LeadFk.OrganizationId == item.Id && (e.InstallationDate != null && e.InstallationDate.Value == DateTime.Now.Date)).Count();
                drPA[item.OrgName] = PendingInstallation + "";
            }
            dt.Rows.Add(drPA);

            drPA = dt.NewRow();
            drPA["Tracker"] = "Todays Installation";
            foreach (var item in OrgList)
            {
                string TodaysInstallation = _jobRepository.GetAll().Where(e => e.JobStatusId == 5 && e.LeadFk.OrganizationId == item.Id && (e.InstallationDate != null && e.InstallationDate.Value == DateTime.Now.Date) && e.JobStatusId == 8).Count() + "/" +
                    _jobRepository.GetAll().Where(e => e.JobStatusId == 5 && e.LeadFk.OrganizationId == item.Id && (e.InstallationDate != null && e.InstallationDate.Value == DateTime.Now.Date)).Count();
                drPA[item.OrgName] = TodaysInstallation + "";
            }
            dt.Rows.Add(drPA);

            drPA = dt.NewRow();
            drPA["Tracker"] = "Promo Reply";
            foreach (var item in OrgList)
            {
                int PromoReply = _promotionUserRepository.GetAll().Where(x => x.ResponseDate.Date == DateTime.Now.Date).Count();
                drPA[item.OrgName] = PromoReply + "";
            }
            dt.Rows.Add(drPA);

            //foreach (var item in OrgList)
            //{
            //    TrackerInfo ti = new TrackerInfo();                
            //    tiList.Add(ti);
            //}
            //obj.TrackerInformation = tiList;
            return dt;
        }

        public async Task<TrackerCountList> TrackerList()
        {
            var List = new TrackerCountList();

            var Doclist = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 2).Select(e => e.JobId).ToList();

            List.Application = _jobRepository.GetAll().Where(e => e.JobStatusId >= 4 && e.MeterPhaseId != 0 && e.MeterPhaseId != null && e.PeakMeterNo != null && e.NMINumber != null && Doclist.Contains(e.Id) && e.LeadFk.OrganizationId == 1 && e.ApplicationRefNo == null).Count();
            List.GridConnection = 0;
            List.STC = 0;
            List.ProjectInvoice = _invoicePaymentRepository.GetAll().Where(j => (_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == 1).Where(e => e.IsVerified == false).Count(); ;
            List.InstallerInvoice = _installerInoviceRepository.GetAll().Where(j => (_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == 1).Count();
            List.Finance = _jobRepository.GetAll().Where(e => e.PaymentOptionId != null && e.PaymentOptionId != 1 && e.LeadFk.OrganizationId == 1).Count();
            List.Refund = _jobRefundRepository.GetAll().Where(j => (_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == 1).Count();
            List.Freebies = _jobPromotionRepository.GetAll().Where(e => e.JobFk.LeadFk.OrganizationId == 1 && e.JobFk.TotalCost <= (_invoicePaymentRepository.GetAll().Where(i => i.JobId == e.JobFk.Id).Select(e => e.InvoicePayTotal).Sum())).Count();
            List.UnAssigned = _leadRepository.GetAll().Where(x => x.OrganizationId == 1 && x.AssignToUserID == null).Count();
            List.New = _leadRepository.GetAll().Where(x => x.OrganizationId == 1 && x.LeadStatusId == 1).Count();
            List.PendingInstallation = _jobRepository.GetAll().Where(e => e.JobStatusId == 5 && e.LeadFk.OrganizationId == 1 && (e.InstallationDate != null && e.InstallationDate.Value == DateTime.Now.Date)).Count();
            List.TodaysInstallation = _jobRepository.GetAll().Where(e => e.JobStatusId == 5 && e.LeadFk.OrganizationId == 1 && (e.InstallationDate != null && e.InstallationDate.Value == DateTime.Now.Date) && e.JobStatusId == 8).Count() + "/" +
                    _jobRepository.GetAll().Where(e => e.JobStatusId == 5 && e.LeadFk.OrganizationId == 1 && (e.InstallationDate != null && e.InstallationDate.Value == DateTime.Now.Date)).Count();
            List.PromoReply = _promotionUserRepository.GetAll().Where(x => x.ResponseDate.Date == DateTime.Now.Date).Count();

            return List;
        }


        public async Task<List<TrackerCountList>> PivotTrackerList()
        {
            var List = new TrackerCountList();
            var Doclist = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 2).Select(e => e.JobId).ToList();
            var myList = _organizationUnitRepository.GetAll().ToList();

            var leads = (from o in _organizationUnitRepository.GetAll()
                         select new
                         {
                             OrgName = o.DisplayName,
                             OrgId = o.Id
                         }).ToList();

            var result = new List<TrackerCountList>();

            foreach (var item in leads)
            {
                var result1 = new TrackerCountList();
                result1.Application = _jobRepository.GetAll().Where(e => e.JobStatusId >= 4 && e.MeterPhaseId != 0 && e.MeterPhaseId != null && e.PeakMeterNo != null && e.NMINumber != null && Doclist.Contains(e.Id) && e.LeadFk.OrganizationId == item.OrgId && e.ApplicationRefNo == null).Count();
                result1.GridConnection = 0;
                result1.STC = 0;
                result1.ProjectInvoice = _invoicePaymentRepository.GetAll().Where(j => (_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == item.OrgId).Where(e => e.IsVerified == false).Count(); ;
                result1.InstallerInvoice = _installerInoviceRepository.GetAll().Where(j => (_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == item.OrgId).Count();
                result1.Finance = _jobRepository.GetAll().Where(e => e.PaymentOptionId != null && e.PaymentOptionId != 1 && e.LeadFk.OrganizationId == 1).Count();
                result1.Refund = _jobRefundRepository.GetAll().Where(j => (_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == item.OrgId).Count();
                result1.Freebies = _jobPromotionRepository.GetAll().Where(e => e.JobFk.LeadFk.OrganizationId == item.OrgId && e.JobFk.TotalCost <= (_invoicePaymentRepository.GetAll().Where(i => i.JobId == e.JobFk.Id).Select(e => e.InvoicePayTotal).Sum())).Count();
                result1.UnAssigned = _leadRepository.GetAll().Where(x => x.OrganizationId == item.OrgId && x.AssignToUserID == null).Count();
                result1.New = _leadRepository.GetAll().Where(x => x.OrganizationId == item.OrgId && x.LeadStatusId == 1).Count();
                result1.PendingInstallation = _jobRepository.GetAll().Where(e => e.JobStatusId == 5 && e.LeadFk.OrganizationId == item.OrgId && (e.InstallationDate != null && e.InstallationDate.Value == DateTime.Now.Date)).Count();
                result1.TodaysInstallation = _jobRepository.GetAll().Where(e => e.JobStatusId == 5 && e.LeadFk.OrganizationId == item.OrgId && (e.InstallationDate != null && e.InstallationDate.Value == DateTime.Now.Date) && e.JobStatusId == 8).Count() + "/" +
                     _jobRepository.GetAll().Where(e => e.JobStatusId == 5 && e.LeadFk.OrganizationId == item.OrgId && (e.InstallationDate != null && e.InstallationDate.Value == DateTime.Now.Date)).Count();
                result1.PromoReply = _promotionUserRepository.GetAll().Where(x => x.ResponseDate.Date == DateTime.Now.Date).Count();
                result1.Organization = item.OrgName;

                result.Add(result1);
            }



            return result.ToList();

        }




        public Task<List<CommonLookupDto>> LeadStatusWiseCount()
        {
            var qry = _leadRepository.GetAll()
                .GroupBy(v => new { v.LeadStatusId })
                    .Select(g => new CommonLookupDto
                    {
                        DisplayName = _lookup_leadStatusRepository.GetAll().Where(e => e.Id == Convert.ToInt32(g.Key)).Select(e => e.Status).FirstOrDefault(),
                        Id = g.Count()
                    });

            return qry.ToListAsync();
        }

        public LeadStatusWiseCount LeadSourceWiseCount(int OrganizationId, DateTime startDate, DateTime endDate)
        {
            var SDate = (_timeZoneConverter.Convert(startDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(endDate, (int)AbpSession.TenantId));

            var result = _leadRepository.GetAll().Where(e => e.OrganizationId == OrganizationId && e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date);

            var output = new LeadStatusWiseCount();

            output.Total = result.Count();

            output.ACT = result.Where(e => e.State == "ACT").Count();
            output.NSW = result.Where(e => e.State == "NSW").Count();
            output.NT = result.Where(e => e.State == "NT").Count();
            output.QLD = result.Where(e => e.State == "QLD").Count();
            output.SA = result.Where(e => e.State == "SA").Count();
            output.TAS = result.Where(e => e.State == "TAS").Count();
            output.VIC = result.Where(e => e.State == "VIC").Count();
            output.WA = result.Where(e => e.State == "WA").Count();

            output.New = result.Where(e => e.LeadStatusId == 1).Count();
            output.UnHandled = result.Where(e => e.LeadStatusId == 2).Count();
            output.Cold = result.Where(e => e.LeadStatusId == 3).Count();
            output.Warm = result.Where(e => e.LeadStatusId == 4).Count();
            output.Hot = result.Where(e => e.LeadStatusId == 5).Count();
            output.Upgrade = result.Where(e => e.LeadStatusId == 6).Count();
            output.Rejected = result.Where(e => e.LeadStatusId == 7).Count();
            output.Canceled = result.Where(e => e.LeadStatusId == 8).Count();
            output.Closed = result.Where(e => e.LeadStatusId == 9).Count();
            output.Assigned = result.Where(e => e.LeadStatusId == 10).Count();
            output.ReAssigned = result.Where(e => e.LeadStatusId == 11).Count();

            output.Metro = result.Where(e => e.Area == "Metro").Count();
            output.Regional = result.Where(e => e.Area == "Regional").Count();

            output.Res = result.Where(e => e.Type == "Res").Count();
            output.Com = result.Where(e => e.Type == "Com").Count();

            output.Google = result.Where(e => e.LeadSourceId == 1).Count();
            output.Referral = result.Where(e => e.LeadSourceId == 2).Count();
            output.TV = result.Where(e => e.LeadSourceId == 3).Count();
            output.Facebook = result.Where(e => e.LeadSourceId == 4).Count();
            output.Others = result.Where(e => e.LeadSourceId == 5).Count();
            output.Lead1 = result.Where(e => e.LeadSourceId == 6).Count();
            output.Lead2 = result.Where(e => e.LeadSourceId == 7).Count();
            output.Lead3 = result.Where(e => e.LeadSourceId == 8).Count();
            output.Lead4 = result.Where(e => e.LeadSourceId == 9).Count();
            output.Lead5 = result.Where(e => e.LeadSourceId == 10).Count();
            output.Lead6 = result.Where(e => e.LeadSourceId == 11).Count();
            output.Lead7 = result.Where(e => e.LeadSourceId == 12).Count();
            output.Lead8 = result.Where(e => e.LeadSourceId == 13).Count();
            output.Lead9 = result.Where(e => e.LeadSourceId == 14).Count();
            output.Lead10 = result.Where(e => e.LeadSourceId == 15).Count();
            output.SBFacebook = result.Where(e => e.LeadSourceId == 18).Count();
            output.SBGoogle = result.Where(e => e.LeadSourceId == 19).Count();
            output.AriseProspects = result.Where(e => e.LeadSourceId == 20).Count();

            return output;
        }

        //Area PieChart Api
        public async Task<GetEditionTenantStatisticsOutput> GetareaStatisticsData(int OrganizationId, DateTime startDate, DateTime endDate)
        {
            return new GetEditionTenantStatisticsOutput(
                await GetTotalAreaData(OrganizationId, startDate, endDate)
            );
        }
        private async Task<List<TenantEdition>> GetTotalAreaData(int OrganizationId, DateTime startDate, DateTime endDate)
        {
            var SDate = (_timeZoneConverter.Convert(startDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(endDate, (int)AbpSession.TenantId));

            return (await _leadRepository.GetAll()
                .Where(e => e.OrganizationId == OrganizationId && e.Area != null && e.Area != "" && e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date)
                .Select(t => new { t.Area })
                .ToListAsync()
                )
                .GroupBy(t => t.Area)
                .Select(t => new TenantEdition
                {
                    Label = t.First().Area,
                    Value = t.Count()
                })
                .OrderBy(t => t.Label)
                .ToList();
        }


        //Type PieChart Api
        public async Task<GetEditionTenantStatisticsOutput> GettypeStatisticsData(int OrganizationId, DateTime startDate, DateTime endDate)
        {
            return new GetEditionTenantStatisticsOutput(
                await GetTotalTypeData(OrganizationId, startDate, endDate)
            );
        }
        private async Task<List<TenantEdition>> GetTotalTypeData(int OrganizationId, DateTime startDate, DateTime endDate)
        {
            var SDate = (_timeZoneConverter.Convert(startDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(endDate, (int)AbpSession.TenantId));

            return (await _leadRepository.GetAll()
                .Where(e => e.OrganizationId == OrganizationId && e.Type != null && e.Type != "" && e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date)
                .Select(t => new { t.Type })
                .ToListAsync()
                )
                .GroupBy(t => t.Type)
                .Select(t => new TenantEdition
                {
                    Label = t.First().Type,
                    Value = t.Count()
                })
                .OrderBy(t => t.Label)
                .ToList();
        }

        //State PieChart Api
        public async Task<GetEditionTenantStatisticsOutput> GetstateStatisticsData(int OrganizationId, DateTime startDate, DateTime endDate)
        {
            return new GetEditionTenantStatisticsOutput(
                await GetTotalStateData(OrganizationId, startDate, endDate)
            );
        }
        private async Task<List<TenantEdition>> GetTotalStateData(int OrganizationId, DateTime startDate, DateTime endDate)
        {
            var SDate = (_timeZoneConverter.Convert(startDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(endDate, (int)AbpSession.TenantId));

            return (await _leadRepository.GetAll()
                .Where(e => e.OrganizationId == OrganizationId && e.State != null && e.State != "" && e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date)
                .Select(t => new { t.State })
                .ToListAsync()
                )
                .GroupBy(t => t.State)
                .Select(t => new TenantEdition
                {
                    Label = t.First().State,
                    Value = t.Count()
                })
                .OrderBy(t => t.Label)
                .ToList();
        }


        //LeadStatus PieChart Api
        public async Task<GetEditionTenantStatisticsOutput> GetLeadStatusStatisticsData(int OrganizationId, DateTime startDate, DateTime endDate)
        {
            return new GetEditionTenantStatisticsOutput(
                await GetTotalLeadstatusData(OrganizationId, startDate, endDate)
            );
        }
        private async Task<List<TenantEdition>> GetTotalLeadstatusData(int OrganizationId, DateTime startDate, DateTime endDate)
        {
            var SDate = (_timeZoneConverter.Convert(startDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(endDate, (int)AbpSession.TenantId));

            return (await _leadRepository.GetAll()
                .Where(e => e.OrganizationId == OrganizationId && e.LeadStatusId != 0 && e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date)
                .Select(t => new { t.LeadStatusId })
                .ToListAsync()
                )
                .GroupBy(t => t.LeadStatusId)
                .Select(t => new TenantEdition
                {
                    Label = _lookup_leadStatusRepository.GetAll().Where(e => e.Id == Convert.ToInt32(t.Key)).Select(e => e.Status).FirstOrDefault(),
                    Value = t.Count()
                })
                .OrderBy(t => t.Label)
                .ToList();
        }

        //Leadsource PieChart Api
        public async Task<GetEditionTenantStatisticsOutput> GetLeadSourceStatisticsData(int OrganizationId, DateTime startDate, DateTime endDate)
        {
            return new GetEditionTenantStatisticsOutput(
                await GetTotalLeadsourceData(OrganizationId, startDate, endDate)
            );
        }
        private async Task<List<TenantEdition>> GetTotalLeadsourceData(int OrganizationId, DateTime startDate, DateTime endDate)
        {
            var SDate = (_timeZoneConverter.Convert(startDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(endDate, (int)AbpSession.TenantId));

            return (await _leadRepository.GetAll()
                .Where(e => e.OrganizationId == OrganizationId && e.LeadSourceId != 0 && e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date)
                .Select(t => new { t.LeadSourceId })
                .ToListAsync()
                )
                .GroupBy(t => t.LeadSourceId)
                .Select(t => new TenantEdition
                {
                    Label = _lookup_leadSourceRepository.GetAll().Where(e => e.Id == Convert.ToInt32(t.Key)).Select(e => e.Name).FirstOrDefault(),
                    Value = t.Count()
                })
                .OrderBy(t => t.Label)
                .ToList();
        }


        public List<LeadSummary> GetOrgList(DateTime startDate, DateTime endDate)
        {
            var SDate = (_timeZoneConverter.Convert(startDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(endDate, (int)AbpSession.TenantId));

            var leads = (from o in _organizationUnitRepository.GetAll()
                         select new
                         {
                             OrgName = o.DisplayName,
                             OrgId = o.Id
                         }).ToList();

            var result = new List<LeadSummary>();

            foreach (var item in leads)
            {
                var result1 = new LeadSummary();

                var Leads = _leadRepository.GetAll().Where(e => e.OrganizationId == item.OrgId && e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= SDate.Value.Date).ToList();

                result1.DuplicateLead = Leads.Where(e => e.IsDuplicate == true || e.IsWebDuplicate == true).Count();
                result1.Lead = Leads.Where(e => (e.IsDuplicate == null && e.IsWebDuplicate == null) || (e.IsDuplicate == false || e.IsWebDuplicate == false)).Count();
                result1.TotalLead = Leads.Count();
                result1.OrgId = (int)item.OrgId;
                result1.OrgName = item.OrgName;

                result.Add(result1);
            }

            LeadSummary leadsum = new LeadSummary();
            leadsum.OrgName = "Total";
            leadsum.Lead = result.Sum(x => x.Lead);
            leadsum.DuplicateLead = result.Sum(x => x.DuplicateLead);
            leadsum.TotalLead = result.Sum(x => x.TotalLead);
            result.Add(leadsum);


            return result;
        }


        public async Task<List<InvoiceStatusDetailDto>> GetInvoiceStatusDetail(DateTime startDate, DateTime endDate)
        {
            var SDate = (_timeZoneConverter.Convert(startDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(endDate, (int)AbpSession.TenantId));

            //var listofincoiceid = _invoicePaymentRepository.GetAll().Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date).Select(e => e.JobId).Distinct().ToList();

            var filteredInvoiceIssued = _jobRepository.GetAll()
                                      .Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date);
            //.Where(e => listofincoiceid.Contains(e.Id));

            var jobids = filteredInvoiceIssued.Select(e => e.Id).ToList();

            var OrgList = (from uou in _organizationUnitRepository.GetAll()
                           select new OrganizationName { OrgName = uou.DisplayName, Id = uou.Id });

            List<InvoiceStatusDetailDto> result = new List<InvoiceStatusDetailDto>();
            //foreach (var iteam in OrgList)
            //{

            InvoiceStatusDetailDto leadsum = new InvoiceStatusDetailDto();
            leadsum.OrgName = "SolarBridge";
            leadsum.Name = "Total Payment";
            leadsum.Planned = filteredInvoiceIssued.Where(e => e.JobStatusId == 1 && e.LeadFk.OrganizationId == 1).Select(e => e.TotalCost).Sum();
            leadsum.OnHold = filteredInvoiceIssued.Where(e => e.JobStatusId == 2 && e.LeadFk.OrganizationId == 1).Select(e => e.TotalCost).Sum();
            leadsum.Cancel = filteredInvoiceIssued.Where(e => e.JobStatusId == 3 && e.LeadFk.OrganizationId == 1).Select(e => e.TotalCost).Sum();
            leadsum.DepositeReceived = filteredInvoiceIssued.Where(e => e.JobStatusId == 4 && e.LeadFk.OrganizationId == 1).Select(e => e.TotalCost).Sum();
            leadsum.Active = filteredInvoiceIssued.Where(e => e.JobStatusId == 5 && e.LeadFk.OrganizationId == 1).Select(e => e.TotalCost).Sum();
            leadsum.JobBook = filteredInvoiceIssued.Where(e => e.JobStatusId == 6 && e.LeadFk.OrganizationId == 1).Select(e => e.TotalCost).Sum();
            leadsum.JobIncomplete = filteredInvoiceIssued.Where(e => e.JobStatusId == 7 && e.LeadFk.OrganizationId == 1).Select(e => e.TotalCost).Sum();
            leadsum.JobInstalled = filteredInvoiceIssued.Where(e => e.JobStatusId == 8 && e.LeadFk.OrganizationId == 1).Select(e => e.TotalCost).Sum();
            leadsum.Total = filteredInvoiceIssued.Where(e => e.LeadFk.OrganizationId == 1).Select(e => e.TotalCost).Sum();
            result.Add(leadsum);

            InvoiceStatusDetailDto recvsum = new InvoiceStatusDetailDto();
            recvsum.Name = "Payment Received";
            recvsum.Planned = _invoicePaymentRepository.GetAll().Where(e => jobids.Contains((int)e.JobId) && e.JobFk.JobStatusId == 1 && e.JobFk.LeadFk.OrganizationId == 1).Select(e => e.InvoicePayTotal).Sum();
            recvsum.OnHold = _invoicePaymentRepository.GetAll().Where(e => jobids.Contains((int)e.JobId) && e.JobFk.JobStatusId == 2 && e.JobFk.LeadFk.OrganizationId == 1).Select(e => e.InvoicePayTotal).Sum();
            recvsum.Cancel = _invoicePaymentRepository.GetAll().Where(e => jobids.Contains((int)e.JobId) && e.JobFk.JobStatusId == 3 && e.JobFk.LeadFk.OrganizationId == 1).Select(e => e.InvoicePayTotal).Sum();
            recvsum.DepositeReceived = _invoicePaymentRepository.GetAll().Where(e => jobids.Contains((int)e.JobId) && e.JobFk.JobStatusId == 4 && e.JobFk.LeadFk.OrganizationId == 1).Select(e => e.InvoicePayTotal).Sum();
            recvsum.Active = _invoicePaymentRepository.GetAll().Where(e => jobids.Contains((int)e.JobId) && e.JobFk.JobStatusId == 5 && e.JobFk.LeadFk.OrganizationId == 1).Select(e => e.InvoicePayTotal).Sum();
            recvsum.JobBook = _invoicePaymentRepository.GetAll().Where(e => jobids.Contains((int)e.JobId) && e.JobFk.JobStatusId == 6 && e.JobFk.LeadFk.OrganizationId == 1).Select(e => e.InvoicePayTotal).Sum();
            recvsum.JobIncomplete = _invoicePaymentRepository.GetAll().Where(e => jobids.Contains((int)e.JobId) && e.JobFk.JobStatusId == 7 && e.JobFk.LeadFk.OrganizationId == 1).Select(e => e.InvoicePayTotal).Sum();
            recvsum.JobInstalled = _invoicePaymentRepository.GetAll().Where(e => jobids.Contains((int)e.JobId) && e.JobFk.JobStatusId == 8 && e.JobFk.LeadFk.OrganizationId == 1).Select(e => e.InvoicePayTotal).Sum();
            recvsum.Total = _invoicePaymentRepository.GetAll().Where(e => jobids.Contains((int)e.JobId) && e.JobFk.LeadFk.OrganizationId == 1).Select(e => e.InvoicePayTotal).Sum();
            result.Add(recvsum);

            InvoiceStatusDetailDto pendingsum = new InvoiceStatusDetailDto();
            pendingsum.Name = "Pending";
            pendingsum.Planned = (leadsum.Planned - recvsum.Planned);
            pendingsum.OnHold = (leadsum.OnHold - recvsum.OnHold);
            pendingsum.Cancel = (leadsum.Cancel - recvsum.Cancel);
            pendingsum.DepositeReceived = (leadsum.DepositeReceived - recvsum.DepositeReceived);
            pendingsum.Active = (leadsum.Active - recvsum.Active);
            pendingsum.JobBook = (leadsum.JobBook - recvsum.JobBook);
            pendingsum.JobIncomplete = (leadsum.JobIncomplete - recvsum.JobIncomplete);
            pendingsum.JobInstalled = (leadsum.JobInstalled - recvsum.JobInstalled);
            pendingsum.Total = (leadsum.Total - recvsum.Total);
            result.Add(pendingsum);
            //}
            return result.ToList();
        }

        public async Task<List<ToDoDetailDto>> GetToDoDetail(int? Id)
        {
            var User = AbpSession.UserId;
             


            var Result = (from l in _leadactivityRepository.GetAll()
                          //.WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                          //.WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                          //.WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                           
                           where(l.ActionId == 25 && l.IsTodoComplete != true && l.LeadFk.AssignToUserID== User) select new ToDoDetailDto()
                          {
                              CustomerName = _leadRepository.GetAll().Where(e => e.Id == l.LeadId).Select(e => e.CompanyName).FirstOrDefault(),
                              ActivityNote = l.ActivityNote,
                              CreationDate = l.CreationTime,
                              CreatedUser = _userRepository.GetAll().Where(e => e.Id == l.CreatorUserId).Select(e => e.UserName).FirstOrDefault(),
                              Id = l.Id,
                              jobnumber = _jobRepository.GetAll().Where(e => e.LeadId == l.LeadId).Select(e => e.JobNumber).FirstOrDefault()
                          }).ToList();


            return Result;
            //}

        }


        public LeadStatusWiseCount LeadSourceCountwiserank(DateTime startDate, DateTime endDate)
        {
            try
            {
                var SDate = (_timeZoneConverter.Convert(startDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(endDate, (int)AbpSession.TenantId));


                var result = (from jb in _jobRepository.GetAll()
                                  //join pr in _jobProductItemRepository.GetAll()
                                  //on jb.Id equals pr.JobId 
                              where (jb.DepositeRecceivedDate != null && jb.DepositeRecceivedDate.Value.Month == SDate.Value.Month && jb.DepositeRecceivedDate.Value.Year >= EDate.Value.Year)

                              select new
                              {
                                  StatusId = jb.LeadFk.AssignToUserID,
                                  Statuscount = jb.Id,
                                  panels = _jobProductItemRepository.GetAll().Where(e => e.JobId == jb.Id && e.ProductItemFk.ProductTypeId == 1).Select(e => e.Quantity).FirstOrDefault()
                                  // Statuscount = (int)grouped.Count(T=>T.Id!=null)
                              })
                              //.OrderByDescending(x => x.Statuscount)
                              .ToList();

                var groupedData = result.GroupBy(p => p.StatusId).Select(g => new { name = g.Key, score = g.Sum(p => p.panels ) }).OrderByDescending(x => x.score);
                var persons = groupedData.Select((p, i) => new Person { Rank = i + 1, assignedid = p.name == null ? 0 : (int)p.name, Score = p.score == null ? 0 : (int)p.score });
                //var persons = groupedData.Select((p, i) => new Person { Rank = i + 1, assignedid = p.name, Score = p.score });

                //var data = _jobRepository.GetAll().Where(e => e.DepositeRecceivedDate >= SDate && e.DepositeRecceivedDate >= EDate).Select(e => e.Id).Count();
                //var result = _leadRepository.GetAll().Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date);

                var output = new LeadStatusWiseCount();
                var userid = (int)AbpSession.UserId;

                output.Total = persons.Where(e => e.assignedid == userid).Select(e => e.Rank).FirstOrDefault();
                output.Lead1 = persons.Where(e => e.assignedid == userid).Select(e => e.Score).FirstOrDefault();
                output.username = _userRepository.GetAll().Where(e => e.Id == userid).Select(e => e.UserName).FirstOrDefault();
                return output;
            }
            catch (Exception e) { throw e; }
        }

        public List<LeadStatusWiseCountDetailDto> LeadSourceDetailMonthly(DateTime startDate, DateTime endDate)
        {
            try
            {
                var userid = (int)AbpSession.UserId;
                var SDate = (_timeZoneConverter.Convert(startDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(endDate, (int)AbpSession.TenantId));


                var result = (from jb in _jobRepository.GetAll()
                                  //join pr in _jobProductItemRepository.GetAll()
                                  //on jb.Id equals pr.JobId 
                              where (jb.DepositeRecceivedDate != null && jb.DepositeRecceivedDate.Value.Month == SDate.Value.Month && jb.DepositeRecceivedDate.Value.Year >= EDate.Value.Year)

                              select new LeadStatusWiseCountDetailDto 
                              {

                                  jobnumber = jb.JobNumber,
                                  panels = _jobProductItemRepository.GetAll().Where(e => e.JobId == jb.Id && e.ProductItemFk.ProductTypeId == 1).Select(e => e.Quantity).FirstOrDefault()


                              })

                              .ToList();

                //var groupedData = result.GroupBy(p => p.StatusId).Select(g => new { name = g.Key, score = g.Count(p => p.Statuscount != 0) }).OrderByDescending(x => x.score);
                //var persons = groupedData.Select((p, i) => new Person { Rank = i + 1, assignedid = (int)p.name, Score = p.score });

                return result;

            }
            catch (Exception e) { throw e; }
        }
        public List<LeadStatusWiseCountDetailDto> LeadSourceDetailDaily(DateTime startDate, DateTime endDate)
        {
            try
            {
                var userid = (int)AbpSession.UserId;
                var SDate = (_timeZoneConverter.Convert(startDate, (int)AbpSession.TenantId));
                DateTime dt = SDate.Value.AddDays(7);
                var EDate = (_timeZoneConverter.Convert(dt, (int)AbpSession.TenantId));


                var result = (from jb in _jobRepository.GetAll()
                                  //join pr in _jobProductItemRepository.GetAll()
                                  //on jb.Id equals pr.JobId 
                              where (jb.DepositeRecceivedDate != null && jb.DepositeRecceivedDate.Value.AddHours(10) >= SDate && jb.DepositeRecceivedDate.Value.AddHours(10) <=endDate  )

                              select new LeadStatusWiseCountDetailDto
                              {

                                  jobnumber = jb.JobNumber,
                                  panels = _jobProductItemRepository.GetAll().Where(e => e.JobId == jb.Id && e.ProductItemFk.ProductTypeId == 1).Select(e => e.Quantity).FirstOrDefault()


                              })

                              .ToList();

                //var groupedData = result.GroupBy(p => p.StatusId).Select(g => new { name = g.Key, score = g.Count(p => p.Statuscount != 0) }).OrderByDescending(x => x.score);
                //var persons = groupedData.Select((p, i) => new Person { Rank = i + 1, assignedid = (int)p.name, Score = p.score });

                return result;

            }
            catch (Exception e) { throw e; }
        }

        public async Task<LeadStatuswisecountDto> GetLeadStatusCountWise(int? Id, DateTime startDate, DateTime endDate)
        {
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var SDate = (_timeZoneConverter.Convert(startDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(endDate, (int)AbpSession.TenantId));
            IList<string> role = await _userManager.GetRolesAsync(User);

            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var Result = new LeadStatuswisecountDto();
            var rec = _leadRepository.GetAll().Where(x => x.AssignToUserID == User.Id ).ToList();

            var totalrec = rec.Count();

            List<int> tv = new List<int>() { 3 };
            List<int> facebook = new List<int>() { 4,17,18,24,26 };
            List<int> google = new List<int>() { 1,19,23,25 };
            List<int> Refferal = new List<int>() { 2 };
            List<int> others = new List<int>() { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 20, 21, 22 };

            var Tv = Convert.ToDecimal(rec.Where(e => e.LeadSourceId == 3).Count());
            var FaceBook = Convert.ToDecimal(rec.Where(e => facebook.Contains((int)e.LeadSourceId)).Count());
            var Google = Convert.ToDecimal(rec.Where(e => google.Contains((int)e.LeadSourceId)).Count());
            var Referal = Convert.ToDecimal(rec.Where(e => Refferal.Contains((int)e.LeadSourceId)).Count());
           var Others = Convert.ToDecimal(rec.Where(e => others.Contains((int)e.LeadSourceId)).Count());


            Result.Tv =  Tv > 0 ? Math.Round((Tv / totalrec * 100),2) : 0;
            Result.FaceBook =  FaceBook > 0 ? Math.Round((FaceBook / totalrec * 100), 2) : 0;
            Result.Google =  Google > 0 ? Math.Round((Google / totalrec * 100), 2) : 0;
            Result.Refferal =  Referal > 0 ? Math.Round((Referal / totalrec * 100), 2) : 0;
            Result.Others = Others > 0 ? Math.Round((Others / totalrec * 100), 2) : 0;
            return Result;
        }
        public async Task<LeadStatuswisecountDto> GetLeadStatusCountlist(int? Id, DateTime startDate, DateTime endDate)
        {
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var SDate = (_timeZoneConverter.Convert(startDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(endDate, (int)AbpSession.TenantId));
            //IList<string> role = await _userManager.GetRolesAsync(User);

            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var Result = new LeadStatuswisecountDto();
            var rec = _leadRepository.GetAll().Select(e => new { e.Id, e.LeadStatusId, e.LeadSourceId, e.AssignToUserID }).Where(x => x.AssignToUserID == User.Id).ToList();
            
            var jobdata = (from job in _jobRepository.GetAll()
                           join lead in _leadRepository.GetAll() on job.LeadId equals lead.Id
                           where (job.DepositeRecceivedDate != null && lead.AssignToUserID == User.Id && lead.LeadStatusId == 6)
                           select new
                           {
                               Id = lead.Id,
                               LeadSourceId = lead.LeadSourceId
                           });

            var totalrec = rec.Count();

            List<int> tv = new List<int>() { 3 };
            List<int> facebook = new List<int>() { 4, 18, 24, 26 };
            List<int> google = new List<int>() { 1, 19, 23, 25 };
            List<int> Refferal = new List<int>() { 2 };
            List<int> others = new List<int>() { 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 20, 21, 22 };

            // Lead Status Count
            Result.Tv = Convert.ToDecimal(rec.Where(e => e.LeadSourceId == 3).Count());
            Result.FaceBook = Convert.ToDecimal(rec.Where(e => facebook.Contains((int)e.LeadSourceId)).Count());
            Result.Google = Convert.ToDecimal(rec.Where(e => google.Contains((int)e.LeadSourceId)).Count());
            Result.Refferal = Convert.ToDecimal(rec.Where(e => e.LeadSourceId == 2).Count());
            Result.Others = Convert.ToDecimal(rec.Where(e => others.Contains((int)e.LeadSourceId)).Count());

            // Lead Upgrade Count
            Result.Tvupgrade = Convert.ToDecimal(rec.Where(e => e.LeadSourceId == 3 && e.LeadStatusId == 6).Count());
            Result.FaceBookupgrade = Convert.ToDecimal(rec.Where(e => facebook.Contains((int)e.LeadSourceId) && e.LeadStatusId == 6).Count());
            Result.Googleupgrade = Convert.ToDecimal(rec.Where(e => google.Contains((int)e.LeadSourceId) && e.LeadStatusId == 6).Count());
            Result.Refferalupgrade = Convert.ToDecimal(rec.Where(e => e.LeadSourceId == 2 && e.LeadStatusId == 6).Count());
            Result.Othersupgrade = Convert.ToDecimal(rec.Where(e => others.Contains((int)e.LeadSourceId) && e.LeadStatusId == 6).Count());

            // Lead Sold Count
            Result.Tvsales = Convert.ToDecimal(jobdata.Where(e => e.LeadSourceId == 3).Count());
            Result.FaceBooksales = Convert.ToDecimal(jobdata.Where(e => facebook.Contains((int)e.LeadSourceId)).Count());
            Result.Googlesales = Convert.ToDecimal(jobdata.Where(e => google.Contains((int)e.LeadSourceId)).Count());
            Result.Refferalsales = Convert.ToDecimal(jobdata.Where(e => Refferal.Contains((int)e.LeadSourceId)).Count());
            Result.Otherssales = Convert.ToDecimal(jobdata.Where(e => others.Contains((int)e.LeadSourceId)).Count());
           

            Result.TvRatio = Result.Tv > 0 && Result.Tvsales > 0 ? Math.Round(((100 / Result.Tv) * Result.Tvsales), 2) : 0;
            Result.FaceBookRatio = Result.FaceBook > 0 && Result.FaceBooksales > 0 ? Math.Round(((100 / Result.FaceBook) * Result.FaceBooksales), 2) : 0;
            Result.GoogleRatio = Result.Google > 0 && Result.Googlesales>0 ? Math.Round(((100 / Result.Google) * Result.Googlesales), 2) : 0;
            Result.RefferalRatio = Result.Refferal > 0 && Result.Refferalsales>0 ? Math.Round(((100/ Result.Refferal) * Result.Refferalsales), 2) : 0;
            Result.OthersRatio = Result.Others > 0 && Result.Otherssales>0 ? Math.Round(((100 / Result.Others) * Result.Otherssales), 2) : 0;

            return Result;
        }

    }
}
