﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System.Threading.Tasks;
using TheSolarProduct.InstallationCost.StateWiseInstallationCost.Dtos;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using System.Linq;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using System.Collections.Generic;
using TheSolarProduct.InstallationCost.OtherCharges;
using TheSolarProduct.States;
using TheSolarProduct.InstallationCost.StateWiseInstallationCosts;

namespace TheSolarProduct.InstallationCost.StateWiseInstallationCost
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class StateWiseInstallationCostAppService : TheSolarProductAppServiceBase, IStateWiseInstallationCostAppService
    {
        private readonly IRepository<StateWiseInstallationCosts.StateWiseInstallationCost> _stateWiseInstallationCostRepository;
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public StateWiseInstallationCostAppService(IRepository<StateWiseInstallationCosts.StateWiseInstallationCost> stateWiseInstallationCostRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider, IRepository<State> stateRepository)
        {
            _stateWiseInstallationCostRepository = stateWiseInstallationCostRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _stateRepository = stateRepository;
        }

        public async Task CreateOrEdit(CreateOrEditStateWiseInstallationCostDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_StateWiseInstallationCost_Create)]
        protected virtual async Task Create(CreateOrEditStateWiseInstallationCostDto input)
        {
            var stateWiseInstallationCost = ObjectMapper.Map<StateWiseInstallationCosts.StateWiseInstallationCost>(input);

            var state = _stateRepository.Get(stateWiseInstallationCost.StateId);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 47;
            dataVaultLog.ActionNote = "State Wise Installation Cost Created : " + state.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _stateWiseInstallationCostRepository.InsertAsync(stateWiseInstallationCost);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_StateWiseInstallationCost_Edit)]
        protected virtual async Task Update(CreateOrEditStateWiseInstallationCostDto input)
        {
            var stateWiseInstallationCost = await _stateWiseInstallationCostRepository.FirstOrDefaultAsync((int)input.Id);
            var state = _stateRepository.Get(stateWiseInstallationCost.StateId).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 47;
            dataVaultLog.ActionNote = "State Wise Installation Cost Updated : " + state;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.StateId != stateWiseInstallationCost.StateId)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "State";
                history.PrevValue = state;
                history.CurValue = _stateRepository.Get(input.StateId).Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Metro != stateWiseInstallationCost.Metro)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Metro";
                history.PrevValue = stateWiseInstallationCost.Metro.ToString();
                history.CurValue = input.Metro.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Regional != stateWiseInstallationCost.Regional)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Regional";
                history.PrevValue = stateWiseInstallationCost.Regional.ToString();
                history.CurValue = input.Regional.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, stateWiseInstallationCost);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_StateWiseInstallationCost_Delete)]
        public async Task Delete(EntityDto input)
        {
            var cost = _stateWiseInstallationCostRepository.Get(input.Id);
            var state = _stateRepository.Get(cost.StateId).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 47;
            dataVaultLog.ActionNote = "State Wise Installation Cost Deleted : " + state;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _stateWiseInstallationCostRepository.DeleteAsync(input.Id);
        }

        public async Task<PagedResultDto<GetStateWiseInstallationCostForVIewDto>> GetAll(GetAllStateWiseInstallationCostInput input)
        {
            var filteredStateWiseInstallationCost = _stateWiseInstallationCostRepository.GetAll().WhereIf(input.StateId != null && input.StateId != 0, e => e.StateId == input.StateId);

            var pagedAndFilteredStateWiseInstallationCost = filteredStateWiseInstallationCost
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var otherCharge = from o in pagedAndFilteredStateWiseInstallationCost
                              select new GetStateWiseInstallationCostForVIewDto()
                              {
                                  StateWiseInstallationCost = new StateWiseInstallationCostDto
                                  {
                                      Id = o.Id,
                                      State = o.StateFk.Name,
                                      Metro = o.Metro,
                                      Regional = o.Regional
                                  }
                              };

            var totalCount = await filteredStateWiseInstallationCost.CountAsync();

            return new PagedResultDto<GetStateWiseInstallationCostForVIewDto>(
                totalCount,
                await otherCharge.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_StateWiseInstallationCost_Edit)]
        public async Task<GetStateWiseInstallationCostForEditOutput> GetStateWiseInstallationCostForEdit(EntityDto input)
        {
            var stateWiseInstallationCost = await _stateWiseInstallationCostRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetStateWiseInstallationCostForEditOutput { StateWiseInstallationCost = ObjectMapper.Map<CreateOrEditStateWiseInstallationCostDto>(stateWiseInstallationCost) };

            return output;
        }
    }
}
