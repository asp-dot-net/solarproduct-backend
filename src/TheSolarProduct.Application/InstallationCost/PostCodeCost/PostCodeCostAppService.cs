﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using System.Linq;
using TheSolarProduct.InstallationCost.PostCodeCost.Dtos;
using TheSolarProduct.DataVaults;
using Abp.EntityFrameworkCore;
using System.Collections.Generic;
using TheSolarProduct.InstallationCost.StateWiseInstallationCosts;
using TheSolarProduct.PostCodes;
using TheSolarProduct.EntityFrameworkCore;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using Abp.Timing.Timezone;
using System;
using TheSolarProduct.InstallationCost.InstallationItemLists;

namespace TheSolarProduct.InstallationCost.PostCodeCost
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class PostCodeCostAppService : TheSolarProductAppServiceBase, IPostCodeCostAppService
    {
        private readonly IRepository<PostCodePricePeriod> _postCodePricePeriodRepository;
        private readonly IRepository<PostoCodePrice> _postoCodePriceRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly ITimeZoneConverter _timeZoneConverter;

        public PostCodeCostAppService(
              IRepository<PostCodePricePeriod> postCodePricePeriodRepository,
              IRepository<PostoCodePrice> postoCodePriceRepository,
              IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
              ITimeZoneConverter timeZoneConverter
        )
        {
            _postCodePricePeriodRepository = postCodePricePeriodRepository;
            _postoCodePriceRepository = postoCodePriceRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _timeZoneConverter = timeZoneConverter;
        }

        public async Task<bool> CheckExistingPostCode(CreateOrEditPostCodePricePeriodDto input)
        {
            var result = false;

            input.Date = _timeZoneConverter.Convert(input.Date, (int)AbpSession.TenantId);

            var month = input.Date.Value.Month;
            var year = input.Date.Value.Year;

            if (input.Id == null)
            {
                result = await _postCodePricePeriodRepository.GetAll().Where(e => e.Month == month && e.Year == year).AnyAsync();
            }
            else
            {
                result = await _postCodePricePeriodRepository.GetAll().Where(e => e.Id != input.Id && e.Month == month && e.Year == year).AnyAsync();
            }

            return result;
        }

        public async Task CreateOrEdit(CreateOrEditPostCodePricePeriodDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_PostCodeCost_Create)]
        protected virtual async Task Create(CreateOrEditPostCodePricePeriodDto input)
        {
            input.Date = _timeZoneConverter.Convert(input.Date, (int)AbpSession.TenantId);

            var month = input.Date.Value.Month;
            var year = input.Date.Value.Year;

            var postCodePrice = ObjectMapper.Map<PostCodePricePeriod>(input);
            postCodePrice.Month = month;
            postCodePrice.Year = year;
            var postCodePricePeriodId = await _postCodePricePeriodRepository.InsertAndGetIdAsync(postCodePrice);

            foreach (var item in input.PostCodePriceList)
            {
                var postCodePriceList = ObjectMapper.Map<PostoCodePrice>(item);
                postCodePriceList.PostCodePricePeriodId = postCodePricePeriodId;

                await _postoCodePriceRepository.InsertAndGetIdAsync(postCodePriceList);
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 54;
            dataVaultLog.ActionNote = "Post Code Price Period Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_PostCodeCost_Edit)]
        protected virtual async Task Update(CreateOrEditPostCodePricePeriodDto input)
        {
            input.Date = _timeZoneConverter.Convert(input.Date, (int)AbpSession.TenantId);

            var month = input.Date.Value.Month;
            var year = input.Date.Value.Year;

            var postCodePricePeriod = await _postCodePricePeriodRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 54;
            dataVaultLog.ActionNote = "Post Code Price Updated : " + postCodePricePeriod.Name;
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != postCodePricePeriod.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = postCodePricePeriod.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (month != postCodePricePeriod.Month)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Month";
                history.PrevValue = postCodePricePeriod.Month.ToString();
                history.CurValue = month.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (year != postCodePricePeriod.Year)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Year";
                history.PrevValue = postCodePricePeriod.Year.ToString();
                history.CurValue = year.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            

            ObjectMapper.Map(input, postCodePricePeriod);

            var Items = _postoCodePriceRepository.GetAll().Where(e => e.PostCodePricePeriodId == input.Id).ToList();

            var Ids = input.PostCodePriceList.Where(e => e.Id > 0).Select(e => e.Id).ToList();

            var deleteItems = Items.Where(e => !Ids.Contains(e.Id)).ToList();

            foreach (var item in deleteItems)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Item Delete";
                history.PrevValue = item.PostCode + " " + item.Price;
                history.CurValue = "";
                history.Action = "Delete";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _postoCodePriceRepository.HardDeleteAsync(e => e.PostCodePricePeriodId == input.Id);
            foreach (var item in input.PostCodePriceList)
            {
                var postCodePriceList = ObjectMapper.Map<PostoCodePrice>(item);
                postCodePriceList.PostCodePricePeriodId = postCodePricePeriod.Id;

                await _postoCodePriceRepository.InsertAndGetIdAsync(postCodePriceList);

                if (item.Id > 0)
                {
                    var updateItem = Items.Where(e => e.Id == item.Id).FirstOrDefault();

                    if (item.PostCode != updateItem.PostCode || item.Price != updateItem.Price)
                    {
                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                        history.FieldName = "Item Updated";
                        history.PrevValue = updateItem.PostCode + " " + updateItem.Price ;
                        history.CurValue = item.PostCode + " " + item.Price;
                        history.Action = "Update";
                        history.ActivityLogId = dataVaultLogId;
                        List.Add(history);
                    }


                }
                else
                {
                    DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                    history.FieldName = "Item Added";
                    history.PrevValue = item.PostCode + " " + item.Price;
                    history.CurValue = "";
                    history.Action = "Added";
                    history.ActivityLogId = dataVaultLogId;
                    List.Add(history);
                }
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
        }

        public async Task Delete(EntityDto input)
        {
            var postCodePricePeriod = _postCodePricePeriodRepository.Get(input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 54;
            dataVaultLog.ActionNote = "Post Code Price Period Deleted : " + postCodePricePeriod.Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _postCodePricePeriodRepository.DeleteAsync(e => e.Id == input.Id);
            await _postoCodePriceRepository.DeleteAsync(e => e.PostCodePricePeriodId == input.Id);

        }

        public async Task<PagedResultDto<GetPostCodeCostForVIewDto>> GetAll(GetAllPostCodeCostInput input)
        {
            var filteredPostCodePricePeriod = _postCodePricePeriodRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.filter), e => e.Name == input.filter.Trim());

            var pagedAndFilteredPostCodePricePeriod = filteredPostCodePricePeriod
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);
            var today = DateTime.Now.ToString("MMMM") + " " + DateTime.Now.Year;

            var postCodePrice = from o in pagedAndFilteredPostCodePricePeriod
                                select new GetPostCodeCostForVIewDto()
                                {
                                    PostoCodePrice = new PostCodePricePeriodDto
                                    {
                                        Id = o.Id,
                                        Name = o.Name,
                                        Period = (new DateTime(o.Year, o.Month, 01).ToString("MMMM")) + " " + o.Year
                                    },
                                    IsEdit = (new DateTime(o.Year, o.Month, 01).ToString("MMMM")) + " " + o.Year == DateTime.Now.ToString("MMMM") + " " + DateTime.Now.Year,
                                    
                                };

            var totalCount = await filteredPostCodePricePeriod.CountAsync();

            return new PagedResultDto<GetPostCodeCostForVIewDto>(totalCount, await postCodePrice.ToListAsync());
        }

        public async Task<CreateOrEditPostCodePricePeriodDto> GetPostCodeCostForEdit(EntityDto input)
        {
            var filteredPostCodePrice = await _postCodePricePeriodRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<CreateOrEditPostCodePricePeriodDto>(filteredPostCodePrice);
            output.Date = new DateTime(filteredPostCodePrice.Year, filteredPostCodePrice.Month, 01);

            var postCodeList = _postoCodePriceRepository.GetAll().Where(e => e.PostCodePricePeriodId == input.Id);

            output.PostCodePriceList = (from o in postCodeList
                                        select new CreateOrEditPostCodePriceListDto()
                                        {
                                            Id = o.Id,
                                            PostCode = o.PostCode,
                                            Price = o.Price,

                                        }).ToList();

            return output;
        }

        public async Task<List<CreateOrEditPostCodePriceListDto>> GetPrevPostCodePriceList(DateTime? date)
        {
            date = _timeZoneConverter.Convert(date, (int)AbpSession.TenantId);

            var prevDate = date.Value.AddMonths(-1);

            var month = prevDate.Month;
            var year = prevDate.Year;

            var postCodeList = _postoCodePriceRepository.GetAll().Where(e => e.PostCodePricePeriodFk.Month == month && e.PostCodePricePeriodFk.Year == year);

            var output = (from o in postCodeList
                          select new CreateOrEditPostCodePriceListDto()
                          {
                              PostCode = o.PostCode,
                              Price = o.Price,

                          });

            return await output.ToListAsync();
        }
    }
}
