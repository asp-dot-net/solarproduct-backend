﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Threading.Tasks;
using TheSolarProduct.InstallationCost.OtherCharges.Dtos;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using System.Linq;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.InstallationCost.STCCost.Dtos;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using System.Collections.Generic;
using TheSolarProduct.InstallationCost.OtherCharges;

namespace TheSolarProduct.InstallationCost.STCCost
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class STCCostAppService : TheSolarProductAppServiceBase, ISTCCostAppService
    {
        private readonly IRepository<STCCost> _stcCostRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        public STCCostAppService(IRepository<STCCost> STCCostRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _stcCostRepository = STCCostRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
        }
        public async Task CreateOrEdit(CreateOrEditSTCCostDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_STCCost_Create)]
        protected virtual async Task Create(CreateOrEditSTCCostDto input)
        {
            var _STCCost = ObjectMapper.Map<STCCost>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 51;
            dataVaultLog.ActionNote = "STC Cost Created : " + input.Cost;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _stcCostRepository.InsertAsync(_STCCost);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_STCCost_Edit)]
        protected virtual async Task Update(CreateOrEditSTCCostDto input)
        {
            var _STCCost = await _stcCostRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 51;
            dataVaultLog.ActionNote = "STC Cost Updated : " + _STCCost.Cost;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Cost != _STCCost.Cost)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = _STCCost.Cost.ToString();
                history.CurValue = input.Cost.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, _STCCost);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_STCCost_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Cost = _stcCostRepository.Get(input.Id).Cost;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 51;
            dataVaultLog.ActionNote = "STC Cost Deleted : " + Cost;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _stcCostRepository.DeleteAsync(input.Id);
        }

        public async Task<PagedResultDto<GetSTCCostForViewDto>> GetAll(GetAllSTCCostInput input)
        {
            var filteredOtherCharge = _stcCostRepository.GetAll();//.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Cost.Contains(input.Filter));

            var pagedAndFilteredOtherCharge = filteredOtherCharge
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var _STCCost = from o in pagedAndFilteredOtherCharge
                              select new GetSTCCostForViewDto()
                              {
                                  STCCost = new STCCostDto
                                  {
                                      Cost = o.Cost,
                                      Id = o.Id
                                  }
                              };

            var totalCount = await filteredOtherCharge.CountAsync();

            return new PagedResultDto<GetSTCCostForViewDto>(
                totalCount,
                await _STCCost.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_STCCost_Edit)]
        public async Task<GetSTCCostForEditOutput> GetSTCCostForEdit(EntityDto input)
        {
            var _STCCost = await _stcCostRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetSTCCostForEditOutput { STCCosts = ObjectMapper.Map<CreateOrEditSTCCostDto>(_STCCost) };

            return output;
        }
    }
}
