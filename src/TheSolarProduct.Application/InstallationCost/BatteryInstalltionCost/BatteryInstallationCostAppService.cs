﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using System.Linq;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.InstallationCost.BatteryCost;
using TheSolarProduct.InstallationCost.BatteryCost.Dtos;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using System.Collections.Generic;
using TheSolarProduct.InstallationCost.STCCost;

namespace TheSolarProduct.InstallationCost.BatteryInstalltionCost
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class BatteryInstallationCostAppService : TheSolarProductAppServiceBase, IBatteryInstallationCostAppService
    {
        private readonly IRepository<BatteryInstallationCost.BatteryInstallationCost> _BatteryInstallationCostRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public BatteryInstallationCostAppService(IRepository<BatteryInstallationCost.BatteryInstallationCost> BatteryInstallationCostRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _BatteryInstallationCostRepository = BatteryInstallationCostRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }
        public async Task CreateOrEdit(CreateOrEditBatteryInstallationCostDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_BatteryInstallationCost_Create)]
        protected virtual async Task Create(CreateOrEditBatteryInstallationCostDto input)
        {
            var _BatteryInstallationCost = ObjectMapper.Map<BatteryInstallationCost.BatteryInstallationCost>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 52;
            dataVaultLog.ActionNote = "Battery Installation Cost Created : " + input.FixCost;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _BatteryInstallationCostRepository.InsertAsync(_BatteryInstallationCost);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_BatteryInstallationCost_Edit)]
        protected virtual async Task Update(CreateOrEditBatteryInstallationCostDto input)
        {
            var _BatteryInstallationCost = await _BatteryInstallationCostRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 52;
            dataVaultLog.ActionNote = "Battery Installation Cost Updated : " + _BatteryInstallationCost.FixCost;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.FixCost != _BatteryInstallationCost.FixCost)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "FixCost";
                history.PrevValue = _BatteryInstallationCost.FixCost.ToString();
                history.CurValue = input.FixCost.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Kw != _BatteryInstallationCost.Kw)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Kw";
                history.PrevValue = _BatteryInstallationCost.Kw.ToString();
                history.CurValue = input.Kw.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.ExtraCost != _BatteryInstallationCost.ExtraCost)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "ExtraCost";
                history.PrevValue = _BatteryInstallationCost.ExtraCost.ToString();
                history.CurValue = input.ExtraCost.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, _BatteryInstallationCost);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_BatteryInstallationCost_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Cost = _BatteryInstallationCostRepository.Get(input.Id).FixCost;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 52;
            dataVaultLog.ActionNote = "Battery Installation Cost Deleted : " + Cost;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _BatteryInstallationCostRepository.DeleteAsync(input.Id);
        }

        public async Task<PagedResultDto<GetBatteryInstallationCostForViewDto>> GetAll(GetAllBatteryInstallationCostInput input)
        {
            var filteredBatteryInstallation= _BatteryInstallationCostRepository.GetAll();
            //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Cost.Contains(input.Filter));

            var pagedAndFilteredBatteryInstallation = filteredBatteryInstallation
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var _BatteryInstallationCost = from o in pagedAndFilteredBatteryInstallation
                                           select new GetBatteryInstallationCostForViewDto()
                                           {
                                               BatteryInstallationCost = new BatteryInstallationCostDto
                                               {
                                                   FixCost = o.FixCost,
                                                   Kw = o.Kw,
                                                   ExtraCost = o.ExtraCost,
                                                   Id = o.Id
                                               }
                                           };

            var totalCount = await filteredBatteryInstallation.CountAsync();

            return new PagedResultDto<GetBatteryInstallationCostForViewDto>(
                totalCount,
                await _BatteryInstallationCost.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_BatteryInstallationCost_Edit)]
        public async Task<GetBatteryInstallationCostForEditOutput> GetBatteryInstallationCostForEdit(EntityDto input)
        {
            var _BatteryInstallationCost = await _BatteryInstallationCostRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetBatteryInstallationCostForEditOutput { BatteryInstallationCosts = ObjectMapper.Map<CreateOrEditBatteryInstallationCostDto>(_BatteryInstallationCost) };

            return output;
        }

    }

}
