﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using System.Linq;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.InstallationCost.InstallationItemList.Dtos;
using TheSolarProduct.Jobs;
using System.Collections.Generic;
using System;
using Abp.Timing.Timezone;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.InstallationCost.StateWiseInstallationCosts;
using PayPalCheckoutSdk.Orders;
using Abp.Organizations;

namespace TheSolarProduct.InstallationCost.InstallationItemList
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class InstallationItemListAppService : TheSolarProductAppServiceBase, IInstallationItemListAppService
    {
        private readonly IRepository<InstallationItemLists.InstallationItemPeriod> _installationItemPeriodRepository;
        private readonly IRepository<InstallationItemLists.InstallationItemList> _installationItemListRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<ProductItem> _productItemRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;

        public InstallationItemListAppService(IRepository<InstallationItemLists.InstallationItemPeriod> installationItemPeriodRepository, IRepository<InstallationItemLists.InstallationItemList> installationItemListRepository, IRepository<JobProductItem> jobProductItemRepository, IRepository<Job> jobRepository, IRepository<ProductItem> productItemRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            , ITimeZoneConverter timeZoneConverter, IRepository<OrganizationUnit, long> organizationUnitRepository)

        {
            _installationItemPeriodRepository = installationItemPeriodRepository;
            _installationItemListRepository = installationItemListRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _jobRepository = jobRepository;
            _productItemRepository = productItemRepository;
            _timeZoneConverter = timeZoneConverter;
            _dbcontextprovider = dbcontextprovider;
            _organizationUnitRepository = organizationUnitRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
        }

        public async Task CreateOrEdit(CreateOrEditInstallationItemPeriodDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_InstallationItemList_Create)]
        protected virtual async Task Create(CreateOrEditInstallationItemPeriodDto input)
        {
            //input.StartDate = (DateTime)_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId).Value.Date;
            //input.EndDate = (DateTime)_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId).Value.Date;
            var curruntDate = _timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId);

            var monthStartDate = new DateTime(curruntDate.Value.Year, input.Month, 1);
            var monthEndDate = new DateTime(monthStartDate.Year, monthStartDate.Month, DateTime.DaysInMonth(monthStartDate.Year, monthStartDate.Month));

            var installationItemPeriod = ObjectMapper.Map<InstallationItemLists.InstallationItemPeriod>(input);

            installationItemPeriod.StartDate = monthStartDate;
            installationItemPeriod.EndDate = monthEndDate;
            var installationItemPeriodId = await _installationItemPeriodRepository.InsertAndGetIdAsync(installationItemPeriod);

            foreach (var item in input.InstallationItemList)
            {
                var installationItemList = ObjectMapper.Map<InstallationItemLists.InstallationItemList>(item);
                installationItemList.InstallationItemPeriodId = installationItemPeriodId;

                await _installationItemListRepository.InsertAndGetIdAsync(installationItemList);
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 48;
            dataVaultLog.ActionNote = "Installation Item List Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_InstallationItemList_Edit)]
        protected virtual async Task Update(CreateOrEditInstallationItemPeriodDto input)
        {
            //input.StartDate = (DateTime)_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId).Value.Date;
            //input.EndDate = (DateTime)_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId).Value.Date;

            var installationItemPeriod = await _installationItemPeriodRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 48;
            dataVaultLog.ActionNote = "Installation Item List Updated : " + installationItemPeriod.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != installationItemPeriod.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = installationItemPeriod.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            //if (input.OrganizationUnit != installationItemPeriod.OrganizationUnit)
            //{
            //    DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
            //    history.FieldName = "Organization";
            //    history.PrevValue = _organizationUnitRepository.Get(installationItemPeriod.OrganizationUnit).DisplayName;
            //    history.CurValue = _organizationUnitRepository.Get(input.OrganizationUnit).DisplayName;
            //    history.Action = "Update";
            //    history.ActivityLogId = dataVaultLogId;
            //    List.Add(history);
            //}

            //if (input.StartDate != installationItemPeriod.StartDate)
            //{
            //    DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
            //    history.FieldName = "StartDate";
            //    history.PrevValue = installationItemPeriod.StartDate.ToString("dd-MM-yyyy");
            //    history.CurValue = input.StartDate.ToString("dd-MM-yyyy");
            //    history.Action = "Update";
            //    history.ActivityLogId = dataVaultLogId;
            //    List.Add(history);
            //}

            //if (input.EndDate != installationItemPeriod.EndDate)
            //{
            //    DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
            //    history.FieldName = "EndDate";
            //    history.PrevValue = installationItemPeriod.EndDate.ToString("dd-MM-yyyy");
            //    history.CurValue = input.EndDate.ToString("dd-MM-yyyy");
            //    history.Action = "Update";
            //    history.ActivityLogId = dataVaultLogId;
            //    List.Add(history);
            //}

            ObjectMapper.Map(input, installationItemPeriod);

            var Items = _installationItemListRepository.GetAll().Include(e => e.ProductItemFk).Where(e => e.InstallationItemPeriodId == input.Id).ToList();

            var Ids = input.InstallationItemList.Where(e => e.Id > 0).Select(e => e.Id).ToList();

            var deleteItems = Items.Where(e => !Ids.Contains(e.Id)).ToList();

            foreach (var item in deleteItems)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Item Delete";
                history.PrevValue = item.ProductItemFk.Name + " " + item.PricePerWatt + " " + item.UnitPrice;
                history.CurValue = "";
                history.Action = "Delete";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }


            await _installationItemListRepository.HardDeleteAsync(e => e.InstallationItemPeriodId == input.Id);
            foreach (var item in input.InstallationItemList)
            {
                var installationItemList = ObjectMapper.Map<InstallationItemLists.InstallationItemList>(item);
                installationItemList.InstallationItemPeriodId = installationItemPeriod.Id;

                await _installationItemListRepository.InsertAndGetIdAsync(installationItemList);

                if (item.Id > 0)
                {
                    var updateItem = Items.Where(e => e.Id == item.Id).FirstOrDefault();

                    if (item.ProductItemId != updateItem.ProductItemId || item.PricePerWatt != updateItem.PricePerWatt || item.UnitPrice != updateItem.UnitPrice)
                    {
                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                        history.FieldName = "Item Updated";
                        history.PrevValue = updateItem.ProductItemFk.Name + " " + updateItem.PricePerWatt + " " + updateItem.UnitPrice;
                        history.CurValue = item.ProductItem + " " + item.PricePerWatt + " " + item.UnitPrice;
                        history.Action = "Update";
                        history.ActivityLogId = dataVaultLogId;
                        List.Add(history);
                    }
                    

                }
                else
                {
                    DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                    history.FieldName = "Item Added";
                    history.PrevValue = item.ProductItem + " " + item.PricePerWatt + " " + item.UnitPrice;
                    history.CurValue = "";
                    history.Action = "Added";
                    history.ActivityLogId = dataVaultLogId;
                    List.Add(history);
                }


            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_InstallationItemList_Delete)]
        public async Task Delete(EntityDto input)
        {
            var installationItemPeriod = await _installationItemPeriodRepository.FirstOrDefaultAsync((int)input.Id);
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 48;
            dataVaultLog.ActionNote = "Installation Item List Deleted : " + installationItemPeriod.Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _installationItemPeriodRepository.DeleteAsync(input.Id);
            await _installationItemListRepository.DeleteAsync(e => e.InstallationItemPeriodId == input.Id);
        }

        public async Task<PagedResultDto<InstallationItemPeriodForViewDto>> GetAll(GetAllInstallationItemListInput input)
        {
            var filteredOutput = _installationItemPeriodRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
               .WhereIf(input.OrganizationUnit != 0 && input.OrganizationUnit != null, e => e.OrganizationUnit == input.OrganizationUnit);

            var pagedAndFilteredOutput = filteredOutput
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var today = DateTime.Now;
            var firstDayOfMonth = new DateTime(today.Year, today.Month, 1);
            var output = from o in pagedAndFilteredOutput
                         select new InstallationItemPeriodForViewDto()
                         {
                             InstallationItemPeriod = new InstallationItemPeriodDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 OrganizationUnit = o.OrganizationUnitFk.DisplayName.ToString(),
                                 StartDate = o.StartDate.Date,
                                 EndDate = o.EndDate.Date
                             },
                             IsEdit = o.EndDate.Date < firstDayOfMonth.Date ? false : true
                         };

            var totalCount = await filteredOutput.CountAsync();

            return new PagedResultDto<InstallationItemPeriodForViewDto>(
                totalCount,
                await output.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_InstallationItemList_Edit)]
        public async Task<GetInstallationItemPeriodForEditOutput> GetInstallationItemPeriodForEdit(EntityDto input)
        {
            var result = await _installationItemPeriodRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetInstallationItemPeriodForEditOutput { InstallationItemPeriod = ObjectMapper.Map<CreateOrEditInstallationItemPeriodDto>(result) };

            output.InstallationItemPeriod.Month = result.StartDate.Month;

            var resultItemList = _installationItemListRepository.GetAll().Where(e => e.InstallationItemPeriodId == input.Id);
            output.InstallationItemPeriod.InstallationItemList = (from o in resultItemList
                                                                  select new CreateOrEditInstallationItemListDto()
                                                                  {
                                                                      Id = o.Id,
                                                                      InstallationItemPeriodId = o.InstallationItemPeriodId,
                                                                      ProductTypeId = o.ProductItemFk.ProductTypeId,
                                                                      ProductItemId = o.ProductItemId,
                                                                      ProductItem = o.ProductItemFk.Name,
                                                                      Size = o.ProductItemFk.Size,
                                                                      PricePerWatt = o.PricePerWatt,
                                                                      UnitPrice = o.UnitPrice,
                                                                      
                                                                  }).ToList();

            return output;
        }

        public async Task<GetInstallationPeriodForViewDto> GetInstallationItemPeriodForView(EntityDto input)
        {
            var result = _installationItemPeriodRepository.GetAll().Where(e => e.Id == input.Id);
            var resultItemList = _installationItemListRepository.GetAll().Where(e => e.InstallationItemPeriodId == input.Id);

            var output = await (from o in result
                                select new GetInstallationPeriodForViewDto
                                {
                                    InstallationItemPeriod = new InstallationItemPeriodDto
                                    {
                                        Id = o.Id,
                                        OrganizationUnit = o.OrganizationUnitFk.DisplayName.ToString(),
                                        Name = o.Name,
                                        StartDate = o.StartDate,
                                        EndDate = o.EndDate
                                    },
                                }).FirstOrDefaultAsync();

            output.InstallationItemList = (from o in resultItemList
                                           select new GetInstallationListForViewDto()
                                           {
                                               Id = o.Id,
                                               ProductType = o.ProductItemFk.ProductTypeFk.Name,
                                               ProductItem = o.ProductItemFk.Name,
                                               PricePerWatt = o.PricePerWatt,
                                               UnitPrice = o.UnitPrice
                                           }).ToList();

            return output;
        }

        public async Task<List<CreateOrEditInstallationItemListDto>> GetInstallationItemListForCreateView(int month, int? org)
        {
            var curruntDate = _timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId);

            var monthStartDate = new DateTime(curruntDate.Value.Year, month, 1);

            var installationItemPeriod = _installationItemPeriodRepository.GetAll().Where(e => e.EndDate < monthStartDate && e.OrganizationUnit == org);

            var installationItemPeriodId = 0;
            if (installationItemPeriod.Count() > 0)
            {
                installationItemPeriodId = installationItemPeriod.Max(e => e.Id);
            }

            var installationItemLists = _installationItemListRepository.GetAll().Where(e => e.InstallationItemPeriodId == installationItemPeriodId);

            var output = from o in installationItemLists
                         select new CreateOrEditInstallationItemListDto()
                         {
                             ProductTypeId = o.ProductItemFk.ProductTypeId,
                             ProductItemId = o.ProductItemFk.Id,
                             ProductItem = o.ProductItemFk.Name,
                             PricePerWatt = o.PricePerWatt,
                             UnitPrice = o.UnitPrice
                         };


            return output.ToList();

        }

        public async Task<List<InstallationItemPeriodForViewDto>> CheckExistInstallationItemList(CreateOrEditInstallationItemPeriodDto input)
        {
            //input.StartDate = (DateTime)_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId);
            //input.EndDate = (DateTime)_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId);

            var monthStartDate = new DateTime();
            var monthEndDate = new DateTime();
            if (input.Id != null && input.Id != 0)
            {
                var insItemPeriod = await _installationItemPeriodRepository.FirstOrDefaultAsync((int)input.Id);

                monthStartDate = insItemPeriod.StartDate.Date;
                monthEndDate = insItemPeriod.EndDate.Date;
            }
            else
            {
                var curruntDate = _timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId);

                monthStartDate = new DateTime(curruntDate.Value.Year, input.Month, 1);
                monthEndDate = new DateTime(monthStartDate.Year, monthStartDate.Month, DateTime.DaysInMonth(monthStartDate.Year, monthStartDate.Month));
            }

            var filteredOutput = _installationItemPeriodRepository.GetAll().Where(e => ((e.StartDate >= monthStartDate && e.EndDate <= monthEndDate) || (e.StartDate <= monthStartDate && e.EndDate >= monthEndDate)) && e.OrganizationUnit == input.OrganizationUnit);

            if (input.Id != null || input.Id != 0)
            {
                filteredOutput = filteredOutput.Where(e => e.Id != input.Id);
            }

            var output = from o in filteredOutput
                         select new InstallationItemPeriodForViewDto()
                         {
                             InstallationItemPeriod = new InstallationItemPeriodDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 OrganizationUnit = o.OrganizationUnitFk.DisplayName.ToString(),
                                 StartDate = o.StartDate.Date,
                                 EndDate = o.EndDate.Date
                             }
                         };

            return await output.ToListAsync();
        }
    }
}