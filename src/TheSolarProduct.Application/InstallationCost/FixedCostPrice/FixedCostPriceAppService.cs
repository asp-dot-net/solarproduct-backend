﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using System.Linq;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.InstallationCost.FixedCostPrice.Dtos;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using System.Collections.Generic;
using TheSolarProduct.InstallationCost.StateWiseInstallationCosts;

namespace TheSolarProduct.InstallationCost.FixedCostPrice
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class FixedCostPriceAppService : TheSolarProductAppServiceBase, IFixedCostPriceAppService
    {
        private readonly IRepository<FixedCostPrices.FixedCostPrice> _fixedCostPriceRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public FixedCostPriceAppService(IRepository<FixedCostPrices.FixedCostPrice> fixedCostPriceRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _fixedCostPriceRepository = fixedCostPriceRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
        }

        public async Task CreateOrEdit(CreateOrEditFixedCostPriceDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_FixedCostPrice_Create)]
        protected virtual async Task Create(CreateOrEditFixedCostPriceDto input)
        {
            var fixedCostPrice = ObjectMapper.Map<FixedCostPrices.FixedCostPrice>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 49;
            dataVaultLog.ActionNote = "Fixed Cost Price Created : " + input.Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _fixedCostPriceRepository.InsertAsync(fixedCostPrice);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_FixedCostPrice_Edit)]
        protected virtual async Task Update(CreateOrEditFixedCostPriceDto input)
        {
            var fixedCostPrice = await _fixedCostPriceRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 49;
            dataVaultLog.ActionNote = "Fixed Cost Price Updated : " + input.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != fixedCostPrice.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = fixedCostPrice.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Type != fixedCostPrice.Type)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Type";
                history.PrevValue = fixedCostPrice.Type;
                history.CurValue = input.Type;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Cost != fixedCostPrice.Cost)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Cost";
                history.PrevValue = fixedCostPrice.Cost.ToString();
                history.CurValue = input.Cost.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, fixedCostPrice);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_FixedCostPrice_Delete)]
        public async Task Delete(EntityDto input)
        {
            var name = _fixedCostPriceRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 49;
            dataVaultLog.ActionNote = "Fixed Cost Price Deleted : " + name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _fixedCostPriceRepository.DeleteAsync(input.Id);
        }

        public async Task<PagedResultDto<GetFixedCostPriceForViewDto>> GetAll(GetAllFixedCostPriceInput input)
        {
            var filteredOutput = _fixedCostPriceRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var pagedAndFilteredOutput= filteredOutput
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFilteredOutput
                         select new GetFixedCostPriceForViewDto()
                              {
                                  FixedCostPrice = new FixedCostPriceDto
                                  {
                                      Id = o.Id,
                                      Name = o.Name,
                                      Type = o.Type,
                                      Cost = o.Cost
                                  }
                              };

            var totalCount = await filteredOutput.CountAsync();

            return new PagedResultDto<GetFixedCostPriceForViewDto>(
                totalCount,
                await output.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_FixedCostPrice_Edit)]
        public async Task<GetFixedCostPriceForEditOutput> GetFixedCostPriceForEdit(EntityDto input)
        {
            var result = await _fixedCostPriceRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetFixedCostPriceForEditOutput { FixedCostPrice = ObjectMapper.Map<CreateOrEditFixedCostPriceDto>(result) };

            return output;
        }
    }
}
