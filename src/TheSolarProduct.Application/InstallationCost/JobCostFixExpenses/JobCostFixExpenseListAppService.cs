﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheSolarProduct.DataVaults;
using TheSolarProduct.Expense;
using TheSolarProduct.InstallationCost.JobCostFixExpenses.Dto;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using System.Linq;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.InstallationCost.InstallationItemList.Dtos;
using Abp.Linq.Extensions;
using TheSolarProduct.InstallationCost.PostCodeCost;
using TheSolarProduct.InstallationCost.PostCodeCost.Dtos;
using TheSolarProduct.ThirdPartyApis.REC.Dtos;
using Stripe;

namespace TheSolarProduct.InstallationCost.JobCostFixExpenses
{
    public class JobCostFixExpenseListAppService : TheSolarProductAppServiceBase, IJobCostFixExpenseListAppService
    {
        private readonly IRepository<JobCostFixExpensePeriod> _jobCostFixExpensePeriodRepository;
        private readonly IRepository<JobCostFixExpenseList> _jobCostFixExpenseListRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IRepository<JobCostFixExpenseType> _jobCostFixExpenseTypeRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public JobCostFixExpenseListAppService(IRepository<JobCostFixExpensePeriod> jobCostFixExpensePeriodRepository
            , IRepository<JobCostFixExpenseList> jobCostFixExpenseListRepository
            , ITimeZoneConverter timeZoneConverter
            , IRepository<DataVaultActivityLog> dataVaultActivityLogRepository
            , IRepository<JobCostFixExpenseType> jobCostFixExpenseTypeRepository
            , IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _jobCostFixExpensePeriodRepository = jobCostFixExpensePeriodRepository;
            _jobCostFixExpenseListRepository = jobCostFixExpenseListRepository;
            _timeZoneConverter = timeZoneConverter;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _jobCostFixExpenseTypeRepository = jobCostFixExpenseTypeRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task CreateOrEdit(CreateOrEditJobCostFixExpensePeriodDto input)
        {
            if (input.Id > 0)
            {
                await Update(input);
            }
            else
            {
                await Create(input);
            }
        }
        protected virtual async Task Create(CreateOrEditJobCostFixExpensePeriodDto input)
        {
           
            input.Date = _timeZoneConverter.Convert(input.Date, (int)AbpSession.TenantId);

            var month = input.Date.Value.Month;
            var year = input.Date.Value.Year;

            var JobCostFixExpense = ObjectMapper.Map<JobCostFixExpensePeriod>(input);
            JobCostFixExpense.Month = month;
            JobCostFixExpense.Year = year;
            var jobCostFixExpensePeriodId = await _jobCostFixExpensePeriodRepository.InsertAndGetIdAsync(JobCostFixExpense);

            foreach (var item in input.JobCostFixExpenseList)
            {
                var jobCostFixExpenseList = ObjectMapper.Map<JobCostFixExpenseList>(item);
                jobCostFixExpenseList.JobCostFixExpensePeriodId = jobCostFixExpensePeriodId;

                await _jobCostFixExpenseListRepository.InsertAndGetIdAsync(jobCostFixExpenseList);
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 76;
            dataVaultLog.ActionNote = "Job Cost Fix Expence Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);
        }

        protected virtual async Task Update(CreateOrEditJobCostFixExpensePeriodDto input)
        {
            input.Date = _timeZoneConverter.Convert(input.Date, (int)AbpSession.TenantId);

            var month = input.Date.Value.Month;
            var year = input.Date.Value.Year;
            input.Year = year;
            input.Month = month;
            var jobCostFixExpensePeriod = await _jobCostFixExpensePeriodRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 76;
            dataVaultLog.ActionNote = "Job Cost Fix Expence Updated : " + jobCostFixExpensePeriod.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != jobCostFixExpensePeriod.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = jobCostFixExpensePeriod.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (month != jobCostFixExpensePeriod.Month)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Month";
                history.PrevValue = jobCostFixExpensePeriod.Month.ToString();
                history.CurValue = month.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (year != jobCostFixExpensePeriod.Year)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Year";
                history.PrevValue = jobCostFixExpensePeriod.Year.ToString();
                history.CurValue = year.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }


            ObjectMapper.Map(input, jobCostFixExpensePeriod);

            var Items = _jobCostFixExpenseListRepository.GetAll().Include(e => e.JobCostFixExpenseTypeFK).Where(e => e.JobCostFixExpensePeriodId == input.Id).ToList();

            var Ids = input.JobCostFixExpenseList.Where(e => e.Id > 0).Select(e => e.Id).ToList();

            var deleteItems = Items.Where(e => !Ids.Contains(e.Id)).ToList();

            foreach (var item in deleteItems)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Item Delete";
                history.PrevValue = item.JobCostFixExpenseTypeFK.Type + " " + item.Amount ;
                history.CurValue = "";
                history.Action = "Delete";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }


            await _jobCostFixExpenseListRepository.HardDeleteAsync(e => e.JobCostFixExpensePeriodId == input.Id);
            foreach (var item in input.JobCostFixExpenseList)
            {
                var installationItemList = ObjectMapper.Map<JobCostFixExpenseList>(item);
                installationItemList.JobCostFixExpensePeriodId = jobCostFixExpensePeriod.Id;

                await _jobCostFixExpenseListRepository.InsertAndGetIdAsync(installationItemList);

                var expenceType = await _jobCostFixExpenseTypeRepository.GetAsync(item.JobCostFixExpenseTypeId);

                if (item.Id > 0)
                {
                    var updateItem = Items.Where(e => e.Id == item.Id).FirstOrDefault();

                    if (item.JobCostFixExpenseTypeId != updateItem.JobCostFixExpenseTypeId || item.Amount != updateItem.Amount)
                    {
                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                        history.FieldName = "Item Updated";
                        history.PrevValue = updateItem.JobCostFixExpenseTypeFK.Type + " " + updateItem.Amount ;
                        history.CurValue = expenceType.Type + " " + item.Amount ;
                        history.Action = "Update";
                        history.ActivityLogId = dataVaultLogId;
                        List.Add(history);
                    }


                }
                else
                {
                    DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                    history.FieldName = "Item Added";
                    history.PrevValue = expenceType.Type + " " + item.Amount;
                    history.CurValue = "";
                    history.Action = "Added";
                    history.ActivityLogId = dataVaultLogId;
                    List.Add(history);
                }


            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
        }


        public async Task Delete(EntityDto input)
        {
            var installationItemPeriod = await _jobCostFixExpensePeriodRepository.FirstOrDefaultAsync((int)input.Id);
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 76;
            dataVaultLog.ActionNote = "Job Cost Fix Expence Deleted : " + installationItemPeriod.Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _jobCostFixExpensePeriodRepository.DeleteAsync(input.Id);
            await _jobCostFixExpenseListRepository.DeleteAsync(e => e.JobCostFixExpensePeriodId == input.Id);
        }

        public async Task<PagedResultDto<GetAllJobCostFixExpenseListViewDto>> GetAll(GetJobCostFixExpenseListInput input)
        {
            var filteredOutput = _jobCostFixExpensePeriodRepository.GetAll().
                WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                .Where(e => e.OrganizationUnit == input.OrganizationUnit); ;

            var pagedAndFilteredOutput = filteredOutput
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var today = DateTime.Now;
            var firstDayOfMonth = new DateTime(today.Year, today.Month, 1);
            var output = from o in pagedAndFilteredOutput
                         select new GetAllJobCostFixExpenseListViewDto()
                         {
                                 Id = o.Id,
                                 Name = o.Name,
                                 OrganizationUnit = o.OrganizationUnitFk.DisplayName.ToString(),
                                 Period = (new DateTime(o.Year, o.Month, 01).ToString("MMMM")) + " " + o.Year
                                // Period = (new DateTime(o.Year == 0 ? today.Year : o.Year,o.Month == 0 ? today.Month : o.Month, 1).ToString("MMMM"))+ " " + (o.Year == 0 ? today.Year : o.Year)
                             //IsEdit = o.EndDate.Date < firstDayOfMonth.Date ? false : true
                         };

            var totalCount = await filteredOutput.CountAsync();

            return new PagedResultDto<GetAllJobCostFixExpenseListViewDto>(
                totalCount,
                await output.ToListAsync()
            );
        }

        public async Task<CreateOrEditJobCostFixExpensePeriodDto> GetJobCostFixExpensePeriodForEdit(EntityDto input)
        {
            var result = await _jobCostFixExpensePeriodRepository.GetAll().Include(e => e.OrganizationUnitFk).Where(e => e.Id == input.Id).FirstOrDefaultAsync();

            var output =  ObjectMapper.Map<CreateOrEditJobCostFixExpensePeriodDto>(result) ;
            output.Date = new DateTime(result.Year, result.Month, 01);
            

            output.Organization = result.OrganizationUnitFk.DisplayName;

            var resultItemList = _jobCostFixExpenseListRepository.GetAll().Include(e => e.JobCostFixExpenseTypeFK).Where(e => e.JobCostFixExpensePeriodId == input.Id);
            output.JobCostFixExpenseList = (from o in resultItemList
                                                                  select new CreateOrEditJobCostFixExpenseListDto()
                                                                  {
                                                                      Id = o.Id,
                                                                      JobCostFixExpensePeriodId = o.JobCostFixExpensePeriodId,
                                                                      JobCostFixExpenseTypeId = o.JobCostFixExpenseTypeId,
                                                                      Amount = o.Amount,
                                                                      JobCostFixExpenseType = o.JobCostFixExpenseTypeFK.Type
                                                                  }).ToList();

            return output;
        }

        public async Task<bool>  CheckExistJobCostFixExpenseList(CreateOrEditJobCostFixExpensePeriodDto input)
        {

            var result = false;

            input.Date = _timeZoneConverter.Convert(input.Date, (int)AbpSession.TenantId);

            var month = input.Date.Value.Month;
            var year = input.Date.Value.Year;

            if (input.Id == null)
            {
                result = await _jobCostFixExpensePeriodRepository.GetAll().Where(e => e.Month == month && e.Year == year).AnyAsync();
            }
            else
            {
                result = await _jobCostFixExpensePeriodRepository.GetAll().Where(e => e.Id != input.Id && e.Month == month && e.Year == year).AnyAsync();
            }

            return result;
        }

        public async Task<List<CreateOrEditJobCostFixExpenseListDto>> GetJobCostFixExpenseForCreateView(DateTime? date, int? org)
        { 
            date = _timeZoneConverter.Convert(date, (int)AbpSession.TenantId);

            var prevDate = date.Value.AddMonths(-1);

            var month = prevDate.Month;
            var year = prevDate.Year;

            var jobCostFixExpense = _jobCostFixExpenseListRepository.GetAll().Where(e => e.JobCostFixExpensePeriodFK.Month == month && e.JobCostFixExpensePeriodFK.Year == year && e.JobCostFixExpensePeriodFK.OrganizationUnit == org);

            var output = from o in jobCostFixExpense
                         select new CreateOrEditJobCostFixExpenseListDto()
                         {
                             JobCostFixExpenseTypeId = o.JobCostFixExpenseTypeId,
                             JobCostFixExpensePeriodId = o.JobCostFixExpensePeriodId,
                             Amount = o.Amount
                         };


            return await output.ToListAsync();
        }

    }
}
