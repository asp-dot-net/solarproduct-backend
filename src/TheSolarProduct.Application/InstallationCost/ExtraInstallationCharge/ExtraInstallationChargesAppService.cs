﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using System.Linq;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.InstallationCost.ExtraInstallationCharge.Dtos;
using TheSolarProduct.Jobs;
using TheSolarProduct.InstallationCost.OtherCharges;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using System.Collections.Generic;
using TheSolarProduct.InstallationCost.FixedCostPrices;
using TheSolarProduct.States;
using TheSolarProduct.InstallationCost.ExtraInstallationCharges;

namespace TheSolarProduct.InstallationCost.ExtraInstallationCharge
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class ExtraInstallationChargesAppService : TheSolarProductAppServiceBase, IExtraInstallationChargesAppService
    {
        private readonly IRepository<ExtraInstallationCharges.ExtraInstallationCharge> _extraInstallationChargeRepository;
        private readonly IRepository<HouseType> _houseTypeRepository;
        private readonly IRepository<Variation> _variationRepository;
        private readonly IRepository<RoofAngle> _roofAngleRepository;
        private readonly IRepository<OtherCharge> _otherChargeRepository;
        private readonly IRepository<RoofType> _roofTypeRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<State> _stateRepository;


        public ExtraInstallationChargesAppService(IRepository<ExtraInstallationCharges.ExtraInstallationCharge> extraInstallationChargeRepository, IRepository<HouseType> houseTypeRepository,  IRepository<Variation> variationRepository, IRepository<RoofAngle> roofAngleRepository, IRepository<OtherCharge> otherChargeRepository, IRepository<RoofType> roofTypeRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider, IRepository<State> stateRepository)
        {
            _extraInstallationChargeRepository = extraInstallationChargeRepository;
            _houseTypeRepository = houseTypeRepository;
            _variationRepository = variationRepository;
            _roofAngleRepository = roofAngleRepository;
            _otherChargeRepository = otherChargeRepository;
            _roofTypeRepository = roofTypeRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _stateRepository = stateRepository;
        }

        public async Task CreateOrEdit(CreateOrEditExtraInstallationChargeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_ExtraInstallationCharges_Create)]
        protected virtual async Task Create(CreateOrEditExtraInstallationChargeDto input)
        {
            foreach (var item in input.StateIds)
            {
                input.StateId = item;
                var extraInstallationCharge = ObjectMapper.Map<ExtraInstallationCharges.ExtraInstallationCharge>(input);

                var dataVaultLog = new DataVaultActivityLog();
                dataVaultLog.Action = "Created";
                dataVaultLog.SectionId = 50;
                dataVaultLog.ActionNote = "Extra Installation Charges Created : " + input.Category;

                await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

                await _extraInstallationChargeRepository.InsertAsync(extraInstallationCharge);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_ExtraInstallationCharges_Edit)]
        protected virtual async Task Update(CreateOrEditExtraInstallationChargeDto input)
        {
            var extraInstallationCharge = await _extraInstallationChargeRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 50;
            dataVaultLog.ActionNote = "Extra Installation Charges Updated : " + extraInstallationCharge.Category;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Category != extraInstallationCharge.Category)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Category";
                history.PrevValue = extraInstallationCharge.Category;
                history.CurValue = input.Category;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);

                DataVaultActivityLogHistory history1 = new DataVaultActivityLogHistory();
                history1.FieldName = "Name";
                history1.PrevValue = extraInstallationCharge.Category == "House Type" ? _houseTypeRepository.GetAll().Where(e => e.Id == extraInstallationCharge.NameId).Select(e => e.Name).FirstOrDefault() : extraInstallationCharge.Category == "Roof Angle" ? _roofAngleRepository.GetAll().Where(e => e.Id == extraInstallationCharge.NameId).Select(e => e.Name).FirstOrDefault() : extraInstallationCharge.Category == "Price Variation" ? _variationRepository.GetAll().Where(e => e.Id == extraInstallationCharge.NameId).Select(e => e.Name).FirstOrDefault() : extraInstallationCharge.Category == "Other Charges" ? _otherChargeRepository.GetAll().Where(e => e.Id == extraInstallationCharge.NameId).Select(e => e.Name).FirstOrDefault() : extraInstallationCharge.Category == "Roof Type" ? _roofTypeRepository.GetAll().Where(e => e.Id == extraInstallationCharge.NameId).Select(e => e.Name).FirstOrDefault() : "";
                history1.CurValue = input.Category == "House Type" ? _houseTypeRepository.GetAll().Where(e => e.Id == input.NameId).Select(e => e.Name).FirstOrDefault() : input.Category == "Roof Angle" ? _roofAngleRepository.GetAll().Where(e => e.Id == input.NameId).Select(e => e.Name).FirstOrDefault() : input.Category == "Price Variation" ? _variationRepository.GetAll().Where(e => e.Id == input.NameId).Select(e => e.Name).FirstOrDefault() : input.Category == "Other Charges" ? _otherChargeRepository.GetAll().Where(e => e.Id == input.NameId).Select(e => e.Name).FirstOrDefault() : input.Category == "Roof Type" ? _roofTypeRepository.GetAll().Where(e => e.Id == input.NameId).Select(e => e.Name).FirstOrDefault() : ""; ;
                history1.Action = "Update";
                history1.ActivityLogId = dataVaultLogId;
                List.Add(history1);
            }
            else if (input.NameId != extraInstallationCharge.NameId)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = extraInstallationCharge.Category == "House Type" ? _houseTypeRepository.GetAll().Where(e => e.Id == extraInstallationCharge.NameId).Select(e => e.Name).FirstOrDefault() : extraInstallationCharge.Category == "Roof Angle" ? _roofAngleRepository.GetAll().Where(e => e.Id == extraInstallationCharge.NameId).Select(e => e.Name).FirstOrDefault() : extraInstallationCharge.Category == "Price Variation" ? _variationRepository.GetAll().Where(e => e.Id == extraInstallationCharge.NameId).Select(e => e.Name).FirstOrDefault() : extraInstallationCharge.Category == "Other Charges" ? _otherChargeRepository.GetAll().Where(e => e.Id == extraInstallationCharge.NameId).Select(e => e.Name).FirstOrDefault() : extraInstallationCharge.Category == "Roof Type" ? _roofTypeRepository.GetAll().Where(e => e.Id == extraInstallationCharge.NameId).Select(e => e.Name).FirstOrDefault() : "";
                history.CurValue = input.Category == "House Type" ? _houseTypeRepository.GetAll().Where(e => e.Id == input.NameId).Select(e => e.Name).FirstOrDefault() : input.Category == "Roof Angle" ? _roofAngleRepository.GetAll().Where(e => e.Id == input.NameId).Select(e => e.Name).FirstOrDefault() : input.Category == "Price Variation" ? _variationRepository.GetAll().Where(e => e.Id == input.NameId).Select(e => e.Name).FirstOrDefault() : input.Category == "Other Charges" ? _otherChargeRepository.GetAll().Where(e => e.Id == input.NameId).Select(e => e.Name).FirstOrDefault() : input.Category == "Roof Type" ? _roofTypeRepository.GetAll().Where(e => e.Id == input.NameId).Select(e => e.Name).FirstOrDefault() : ""; ;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Type != extraInstallationCharge.Type)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Type";
                history.PrevValue = extraInstallationCharge.Type;
                history.CurValue = input.Type;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.StateId != extraInstallationCharge.StateId)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "State";
                history.PrevValue = _stateRepository.Get(extraInstallationCharge.StateId).Name;
                history.CurValue = _stateRepository.Get(input.StateId).Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }


            

            if (input.MetroAmount != extraInstallationCharge.MetroAmount)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "MetroAmount";
                history.PrevValue = extraInstallationCharge.MetroAmount.ToString();
                history.CurValue = input.MetroAmount.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.RegionalAmount != extraInstallationCharge.RegionalAmount)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "RegionalAmount";
                history.PrevValue = extraInstallationCharge.RegionalAmount.ToString();
                history.CurValue = input.RegionalAmount.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, extraInstallationCharge);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_ExtraInstallationCharges_Delete)]
        public async Task Delete(EntityDto input)
        {
            var name = _extraInstallationChargeRepository.Get(input.Id).Category;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 50;
            dataVaultLog.ActionNote = "Extra Installation Charges Deleted : " + name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _extraInstallationChargeRepository.DeleteAsync(input.Id);
        }

        public async Task<PagedResultDto<GetExtraInstallationChargesForViewDto>> GetAll(GetExtraInstallationChargesInput input)
        {
            var filteredOutput = _extraInstallationChargeRepository.GetAll()
                                .WhereIf(!string.IsNullOrEmpty(input.Category), e => e.Category == input.Category)
                                .WhereIf(input.Name > 0, e => e.NameId == input.Name);

            var pagedAndFilteredOutput = filteredOutput
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFilteredOutput
                         select new GetExtraInstallationChargesForViewDto()
                         {
                             ExtraInstallationCharges = new ExtraInstallationChargesDto
                             {
                                 Id = o.Id,
                                 Category = o.Category,
                                 Name = o.Category == "House Type" ? _houseTypeRepository.GetAll().Where(e => e.Id == o.NameId).Select(e => e.Name).FirstOrDefault() : o.Category == "Roof Angle" ? _roofAngleRepository.GetAll().Where(e => e.Id == o.NameId).Select(e => e.Name).FirstOrDefault() : o.Category == "Price Variation" ? _variationRepository.GetAll().Where(e => e.Id == o.NameId).Select(e => e.Name).FirstOrDefault() : o.Category == "Other Charges" ? _otherChargeRepository.GetAll().Where(e => e.Id == o.NameId).Select(e => e.Name).FirstOrDefault() : o.Category == "Roof Type" ? _roofTypeRepository.GetAll().Where(e => e.Id == o.NameId).Select(e => e.Name).FirstOrDefault() : "",
                                 State = o.StateFk.Name,
                                 Type = o.Type,
                                 MetroAmount = o.MetroAmount,
                                 RegionalAmount = o.RegionalAmount
                             }
                         };

            var totalCount = await filteredOutput.CountAsync();

            return new PagedResultDto<GetExtraInstallationChargesForViewDto>(
                totalCount,
                await output.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_ExtraInstallationCharges_Edit)]
        public async Task<GetExtraInstallationChargeForEditOutput> GetExtraInstallationChargesForEdit(EntityDto input)
        {
            var result = await _extraInstallationChargeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetExtraInstallationChargeForEditOutput { ExtraInstallationCharge = ObjectMapper.Map<CreateOrEditExtraInstallationChargeDto>(result) };

            return output;
        }
    }
}
