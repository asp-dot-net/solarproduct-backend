﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Threading.Tasks;
using TheSolarProduct.InstallationCost.OtherCharges.Dtos;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using System.Linq;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataVaults;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using System.Collections.Generic;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.InstallationCost.OtherCharges
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class OtherChargesAppService : TheSolarProductAppServiceBase, IOtherChargesAppService
    {
        private readonly IRepository<OtherCharge> _otherChargeRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public OtherChargesAppService(IRepository<OtherCharge> otherChargeRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _otherChargeRepository = otherChargeRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;   
        }

        public async Task CreateOrEdit(CreateOrEditOtherChargesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_OtherCharges_Create)]
        protected virtual async Task Create(CreateOrEditOtherChargesDto input)
        {
            var otherCharge = ObjectMapper.Map<OtherCharge>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 46;
            dataVaultLog.ActionNote = "Other Charge Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);


            await _otherChargeRepository.InsertAsync(otherCharge);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_OtherCharges_Edit)]
        protected virtual async Task Update(CreateOrEditOtherChargesDto input)
        {
            var otherCharge = await _otherChargeRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 46;
            dataVaultLog.ActionNote = "Other Charge Updated : " + otherCharge.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != otherCharge.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = otherCharge.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, otherCharge);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_OtherCharges_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _otherChargeRepository.Get(input.Id).Name;
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 46;
            dataVaultLog.ActionNote = "Other Charge Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _otherChargeRepository.DeleteAsync(input.Id);
        }

        public async Task<PagedResultDto<GetOtherChargesForViewDto>> GetAll(GetAllOtherChargesInput input)
        {
            var filteredOtherCharge = _otherChargeRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var pagedAndFilteredOtherCharge = filteredOtherCharge
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var otherCharge = from o in pagedAndFilteredOtherCharge
                              select new GetOtherChargesForViewDto()
                              {
                                  OtherCharges = new OtherChargesDto
                                  {
                                      Name = o.Name,
                                      Id = o.Id
                                  }
                              };

            var totalCount = await filteredOtherCharge.CountAsync();

            return new PagedResultDto<GetOtherChargesForViewDto>(
                totalCount,
                await otherCharge.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InstallationCost_OtherCharges_Edit)]
        public async Task<GetOtherChargesForEditOutput> GetOtherChargesForEdit(EntityDto input)
        {
            var otherCharge = await _otherChargeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetOtherChargesForEditOutput { OtherCharges = ObjectMapper.Map<CreateOrEditOtherChargesDto>(otherCharge) };

            return output;
        }
    }
}
