﻿using Abp.AutoMapper;
using Abp.Hangfire;
using Abp.Hangfire.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using TheSolarProduct.Authorization;
using TheSolarProduct.Notifications;

namespace TheSolarProduct
{
    /// <summary>
    /// Application layer module of the application.
    /// </summary>
    [DependsOn(
        typeof(TheSolarProductApplicationSharedModule),
        typeof(TheSolarProductCoreModule),
        typeof(AbpHangfireAspNetCoreModule)
        )]
    public class TheSolarProductApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Adding authorization providers
            Configuration.Authorization.Providers.Add<AppAuthorizationProvider>();

            //Adding custom AutoMapper configuration
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.CreateMappings);

            //
            Configuration.Notifications.Notifiers.Add<AppNotifier>();

            Configuration.BackgroundJobs.UseHangfire();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TheSolarProductApplicationModule).GetAssembly());
        }
    }
}