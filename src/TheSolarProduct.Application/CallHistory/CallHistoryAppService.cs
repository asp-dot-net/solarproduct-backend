﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.CallHistory.Dtos;
using TheSolarProduct.Leads;
using TheSolarProduct.Authorization.Users;
using System.Data.SqlClient;
using System.Data;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using TheSolarProduct.WholeSaleLeads;
using TheSolarProduct.StockOrders;
using TheSolarProduct.Vendors;

namespace TheSolarProduct.CallHistory
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class CallHistoryAppService : TheSolarProductAppServiceBase, ICallHistoryAppService
    {
        private readonly IRepository<CallHistory> _callHistoryRepository;
        private readonly IRepository<CallType> _callTypeRepository;
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<WholeSaleLead> _wholeSaleLeadRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<PurchaseOrder> _purchaseOrderRepository;
        private readonly IRepository<Vendor> _vendorReposiorty;

        public CallHistoryAppService(IRepository<CallHistory> callHistoryRepository, IRepository<Vendor> vendorReposiortory, IRepository<CallType> callTypeRepository, IRepository<Lead> leadRepository, IRepository<User, long> userRepository, IRepository<WholeSaleLead> wholeSaleLeadRepository, IRepository<PurchaseOrder> purchaseOrderRepository)
        {
            _callHistoryRepository = callHistoryRepository;
            _callTypeRepository = callTypeRepository;
            _leadRepository = leadRepository;
            _userRepository = userRepository;
            _wholeSaleLeadRepository = wholeSaleLeadRepository;
            _purchaseOrderRepository = purchaseOrderRepository;
            _vendorReposiorty = vendorReposiortory;

        }

        public async Task<List<GetCallHistoryViewDto>> GetCallHistory(GetCallHistoryInput input)
        {
            #region our Db
            var leads = await _leadRepository.FirstOrDefaultAsync(input.LeadId);
            var extention = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).Select(e => e.ExtensionNumber).FirstOrDefault();

            var _leadNumber = leads.Mobile != null ? leads.Mobile : leads.Phone != null ? leads.Phone : "";
            _leadNumber = !string.IsNullOrWhiteSpace(_leadNumber) ? _leadNumber.Substring(_leadNumber.Length - 9) : "";

            var Users = _userRepository.GetAll();
            var orgUser = Users.Where(e => e.OrganizationUnits.Where(e => e.OrganizationUnitId == leads.OrganizationId).Any() && !string.IsNullOrEmpty(e.ExtensionNumber)).Select(e => e.ExtensionNumber).ToList();

            var callHistory = new List<GetCallHistoryViewDto>();

            //IQueryable<GetCallHistoryViewDto> result;
            //using (var uow = UnitOfWorkManager.Begin())
            //{
            //    using (CurrentUnitOfWork.SetTenantId(null))
            //    {
            //        using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            //        {


            //            callHistory = result.ToList();
            //            uow.Complete();
            //        }
            //    }
            //}

            var filtered = _callHistoryRepository.GetAll().Where(e => e.Number.EndsWith(_leadNumber))
                         .WhereIf(!string.IsNullOrWhiteSpace(input.CallType), e => false || e.CallType == input.CallType)
                         .WhereIf(input.MyHistorty == true && !string.IsNullOrEmpty(extention), e => e.Agent.Contains(extention))
                         .Where(e => orgUser.Contains(e.Agent)).OrderByDescending(e => e.CallStartTimeUTC);

            var result = from o in filtered
                     join o1 in _callTypeRepository.GetAll() on o.CallType equals o1.Name into ct
                     from s1 in ct.DefaultIfEmpty()

                     select new GetCallHistoryViewDto()
                     {
                         CallType = o.CallType,
                         ColorClass = s1.ColorClass,
                         CallTypeIcon = s1.IconClass,
                         CallStartTimeUTC = o.CallStartTimeUTC,
                         Duration = o.Duration,
                         AgentName = Users.Where(e => e.ExtensionNumber == o.Agent).Select(e => e.FullName).FirstOrDefault(),
                         RecordingSource = "",
                         Agent = o.Agent
                     };

            //var resultOtp = (from o in callHistory

            //              select new GetCallHistoryViewDto()
            //              {
            //                  CallType = o.CallType,
            //                  ColorClass = o.ColorClass,
            //                  CallTypeIcon = o.CallTypeIcon,
            //                  CallStartTimeUTC = o.CallStartTimeUTC,
            //                  Duration = o.Duration,
            //                  AgentName = Users.Where(e => e.ExtensionNumber == o.Agent).Select(e => e.FullName).FirstOrDefault(),
            //                  RecordingSource = "",
            //                  Agent = o.Agent
            //              }).ToList();

            //return resultOtp;
            return await result.ToListAsync();
            #endregion

            #region From Other Server
            //var leads = await _leadRepository.FirstOrDefaultAsync(input.LeadId);
            //var extention = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).Select(e => e.ExtensionNumber).FirstOrDefault();

            //String SqlconString = "Data Source=52.140.99.10;Initial Catalog=SP_SolarProduct;Persist Security Info=True;User Id=ariseadmin;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
            //using (SqlConnection sqlCon = new SqlConnection(SqlconString))
            //{
            //    sqlCon.Open();
            //    SqlCommand sql_cmnd2 = new SqlCommand("GetCallHistory", sqlCon);
            //    sql_cmnd2.CommandType = CommandType.StoredProcedure;
            //    sql_cmnd2.Parameters.AddWithValue("@Mobile", SqlDbType.VarChar).Value = leads.Mobile != null ? leads.Mobile : leads.Phone != null ? leads.Phone : "";
            //    sql_cmnd2.Parameters.AddWithValue("@CallType", SqlDbType.VarChar).Value = input.CallType == null ? "" : input.CallType;
            //    sql_cmnd2.Parameters.AddWithValue("@extention", SqlDbType.VarChar).Value = input.MyHistorty == true ? extention : "";
            //    var table = new DataTable();
            //    table.Load(sql_cmnd2.ExecuteReader());
            //    sqlCon.Close();

            //    var result1 = from item in table.AsEnumerable()
            //                  select new GetCallHistoryViewDto()
            //                  {
            //                      CallType = item.Field<string>("CallType"),
            //                      ColorClass = item.Field<string>("ColorClass"),
            //                      CallTypeIcon = item.Field<string>("IconClass"),
            //                      CallStartTimeUTC = item.Field<DateTime>("CallStartTimeUTC"),
            //                      Duration = item.Field<string>("Duration"),
            //                      AgentName = item.Field<string>("Agent") != "0" ? Users.Where(e => e.ExtensionNumber == item.Field<string>("Agent")).Select(e => e.FullName).FirstOrDefault() : "",
            //                      RecordingSource = "",
            //                      Agent = item.Field<string>("Agent")
            //                  };

            //    //IQueryable<GetCallHistoryViewDto> itemDtos = result.AsQueryable();

            //    var res = _userRepository.GetAll().Where(e => e.OrganizationUnits.Where(e => e.OrganizationUnitId == leads.OrganizationId).Any() && !string.IsNullOrEmpty(e.ExtensionNumber)).Select(e => e.ExtensionNumber).ToList();

            //    var History = result1.ToList().Where(e => res.Contains(e.Agent));

            //    //return result.ToList();
            //    return History.ToList();
            //}
            #endregion
        }

        public async Task<List<GetCallHistoryViewDto>> GetWholeSaleCallHistory(GetCallHistoryInput input)
        {
            #region our Db
            var leads = await _wholeSaleLeadRepository.FirstOrDefaultAsync(input.LeadId);
            var extention = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).Select(e => e.ExtensionNumber).FirstOrDefault();

            var _leadNumber = leads.Mobile != null ? leads.Mobile : leads.Talephone != null ? leads.Talephone : "";
            _leadNumber = !string.IsNullOrWhiteSpace(_leadNumber) ? _leadNumber.Substring(_leadNumber.Length - 9) : "";

            var Users = _userRepository.GetAll();
            var orgUser = Users.Where(e => e.OrganizationUnits.Where(e => e.OrganizationUnitId == leads.OrganizationId).Any() && !string.IsNullOrEmpty(e.ExtensionNumber)).Select(e => e.ExtensionNumber).ToList();

            var callHistory = new List<GetCallHistoryViewDto>();

            //IQueryable<GetCallHistoryViewDto> result;
            //using (var uow = UnitOfWorkManager.Begin())
            //{
            //    using (CurrentUnitOfWork.SetTenantId(null))
            //    {
            //        using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            //        {


            //            callHistory = result.ToList();
            //            uow.Complete();
            //        }
            //    }
            //}

            var filtered = _callHistoryRepository.GetAll().Where(e => e.Number.EndsWith(_leadNumber))
                         .WhereIf(!string.IsNullOrWhiteSpace(input.CallType), e => false || e.CallType == input.CallType)
                         .WhereIf(input.MyHistorty == true && !string.IsNullOrEmpty(extention), e => e.Agent.Contains(extention))
                         .Where(e => orgUser.Contains(e.Agent)).OrderByDescending(e => e.CallStartTimeUTC);

            var result = from o in filtered
                     join o1 in _callTypeRepository.GetAll() on o.CallType equals o1.Name into ct
                     from s1 in ct.DefaultIfEmpty()

                     select new GetCallHistoryViewDto()
                     {
                         CallType = o.CallType,
                         ColorClass = s1.ColorClass,
                         CallTypeIcon = s1.IconClass,
                         CallStartTimeUTC = o.CallStartTimeUTC,
                         Duration = o.Duration,
                         AgentName = Users.Where(e => e.ExtensionNumber == o.Agent).Select(e => e.FullName).FirstOrDefault(),
                         RecordingSource = "",
                         Agent = o.Agent
                     };

            //var resultOtp = (from o in callHistory

            //              select new GetCallHistoryViewDto()
            //              {
            //                  CallType = o.CallType,
            //                  ColorClass = o.ColorClass,
            //                  CallTypeIcon = o.CallTypeIcon,
            //                  CallStartTimeUTC = o.CallStartTimeUTC,
            //                  Duration = o.Duration,
            //                  AgentName = Users.Where(e => e.ExtensionNumber == o.Agent).Select(e => e.FullName).FirstOrDefault(),
            //                  RecordingSource = "",
            //                  Agent = o.Agent
            //              }).ToList();

            //return resultOtp;
            return await result.ToListAsync();
            #endregion

            #region From Other Server
            //var leads = await _leadRepository.FirstOrDefaultAsync(input.LeadId);
            //var extention = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).Select(e => e.ExtensionNumber).FirstOrDefault();

            //String SqlconString = "Data Source=52.140.99.10;Initial Catalog=SP_SolarProduct;Persist Security Info=True;User Id=ariseadmin;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
            //using (SqlConnection sqlCon = new SqlConnection(SqlconString))
            //{
            //    sqlCon.Open();
            //    SqlCommand sql_cmnd2 = new SqlCommand("GetCallHistory", sqlCon);
            //    sql_cmnd2.CommandType = CommandType.StoredProcedure;
            //    sql_cmnd2.Parameters.AddWithValue("@Mobile", SqlDbType.VarChar).Value = leads.Mobile != null ? leads.Mobile : leads.Phone != null ? leads.Phone : "";
            //    sql_cmnd2.Parameters.AddWithValue("@CallType", SqlDbType.VarChar).Value = input.CallType == null ? "" : input.CallType;
            //    sql_cmnd2.Parameters.AddWithValue("@extention", SqlDbType.VarChar).Value = input.MyHistorty == true ? extention : "";
            //    var table = new DataTable();
            //    table.Load(sql_cmnd2.ExecuteReader());
            //    sqlCon.Close();

            //    var result1 = from item in table.AsEnumerable()
            //                  select new GetCallHistoryViewDto()
            //                  {
            //                      CallType = item.Field<string>("CallType"),
            //                      ColorClass = item.Field<string>("ColorClass"),
            //                      CallTypeIcon = item.Field<string>("IconClass"),
            //                      CallStartTimeUTC = item.Field<DateTime>("CallStartTimeUTC"),
            //                      Duration = item.Field<string>("Duration"),
            //                      AgentName = item.Field<string>("Agent") != "0" ? Users.Where(e => e.ExtensionNumber == item.Field<string>("Agent")).Select(e => e.FullName).FirstOrDefault() : "",
            //                      RecordingSource = "",
            //                      Agent = item.Field<string>("Agent")
            //                  };

            //    //IQueryable<GetCallHistoryViewDto> itemDtos = result.AsQueryable();

            //    var res = _userRepository.GetAll().Where(e => e.OrganizationUnits.Where(e => e.OrganizationUnitId == leads.OrganizationId).Any() && !string.IsNullOrEmpty(e.ExtensionNumber)).Select(e => e.ExtensionNumber).ToList();

            //    var History = result1.ToList().Where(e => res.Contains(e.Agent));

            //    //return result.ToList();
            //    return History.ToList();
            //}
            #endregion
        }
        public async Task<List<GetCallHistoryViewDto>> GetPurchaseOrderCallHistory(GetCallHistoryInput input)
        {
            #region our Db
            var PurchaseOrder = await _purchaseOrderRepository.FirstOrDefaultAsync(input.LeadId);
            var extention = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).Select(e => e.ExtensionNumber).FirstOrDefault();
            // Get the vendor associated with the purchase order
            var vendor = await _vendorReposiorty.FirstOrDefaultAsync((int)PurchaseOrder.VendorId);
            //var vendorPhoneNumber = vendor?.Phone;
            var _PurchaseOrderNumber = vendor.Phone ?? "";
            _PurchaseOrderNumber = !string.IsNullOrWhiteSpace(_PurchaseOrderNumber) ? _PurchaseOrderNumber.Substring(_PurchaseOrderNumber.Length - 9) : "";

            var Users = _userRepository.GetAll();
            var orgUser = Users.Where(e => e.OrganizationUnits.Where(e => e.OrganizationUnitId == PurchaseOrder.OrganizationId).Any() && !string.IsNullOrEmpty(e.ExtensionNumber)).Select(e => e.ExtensionNumber).ToList();

            var callHistory = new List<GetCallHistoryViewDto>();

            var filtered = _callHistoryRepository.GetAll().Where(e => e.Number.EndsWith(_PurchaseOrderNumber))
                         .WhereIf(!string.IsNullOrWhiteSpace(input.CallType), e => false || e.CallType == input.CallType)
                         .WhereIf(input.MyHistorty == true && !string.IsNullOrEmpty(extention), e => e.Agent.Contains(extention))
                         .Where(e => orgUser.Contains(e.Agent)).OrderByDescending(e => e.CallStartTimeUTC);

            var result = from o in filtered
                         join o1 in _callTypeRepository.GetAll() on o.CallType equals o1.Name into ct
                         from s1 in ct.DefaultIfEmpty()

                         select new GetCallHistoryViewDto()
                         {
                             CallType = o.CallType,
                             ColorClass = s1.ColorClass,
                             CallTypeIcon = s1.IconClass,
                             CallStartTimeUTC = o.CallStartTimeUTC,
                             Duration = o.Duration,
                             AgentName = Users.Where(e => e.ExtensionNumber == o.Agent).Select(e => e.FullName).FirstOrDefault(),
                             RecordingSource = "",
                             Agent = o.Agent
                         };
            return await result.ToListAsync();
            #endregion

            #region From Other Server
           
            #endregion
        }
    }
}
