﻿using System.Collections.Generic;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Linq.Extensions;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
//using System.Globalization;
using Abp.Timing.Timezone;
//using RestSharp.Authenticators;
using TheSolarProduct.Timing;
//using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
//using TheSolarProduct.PickList.Dtos;
//using System.Collections;
//using Abp.Domain.Entities;
//using Microsoft.AspNetCore.Mvc;
//using NUglify.JavaScript.Syntax;
using TheSolarProduct.TransportationCosts.Dtos;
using System.Data.Entity;
using TheSolarProduct.WholeSaleLeads.Dtos;
using Twilio.Rest.Api.V2010;
using TheSolarProduct.WholeSaleLeads;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.LeadSources;

namespace TheSolarProduct.TransportationCosts
{
    public class TransportationCostAppService : TheSolarProductAppServiceBase, ITransportationCostAppService
    {
        private readonly IRepository<TransportationCost> _transportCostRepository;
        private readonly IRepository<TransportationCostJob> _transportationCostJobRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly ITimeZoneService _timeZoneService;


        public TransportationCostAppService(
            IRepository<TransportationCost> transportCostRepository
            , IRepository<TransportationCostJob> transportationCostJobRepository
            , ITimeZoneConverter timeZoneConverter
            , ITimeZoneService timeZoneService
            )
        { 
            _transportCostRepository = transportCostRepository; 
            _transportationCostJobRepository = transportationCostJobRepository;
            _timeZoneConverter = timeZoneConverter;
            _timeZoneService = timeZoneService;
        }

        public async Task CreateOrEdit(CreateOrEditTransportCost input)
        {
            if(input.Id > 0)
            {
                await Update(input);
            }
            else
            {
                await Create(input);
            }
        }

        public async Task Create(CreateOrEditTransportCost input)
        {
            var transportCost = ObjectMapper.Map<TransportationCost>(input);

            var transportId = await _transportCostRepository.InsertAndGetIdAsync(transportCost);

            foreach(var item in input.jobnumber)
            {
                var transportCostJob = new TransportationCostJob();
                transportCostJob.JobId = item.Id;
                transportCostJob.TransportationCostId = transportId;
                await _transportationCostJobRepository.InsertAsync(transportCostJob);
            }

        }

        public async Task Update(CreateOrEditTransportCost input)
        {
            var transportCost = await _transportCostRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, transportCost);

            await _transportCostRepository.UpdateAsync(transportCost);

            var oldIds =  _transportationCostJobRepository.GetAll().Where(e => e.TransportationCostId == transportCost.Id).Select(e => e.JobId).ToList();

            var newIds = input.jobnumber != null ? input.jobnumber.Select(e => e.Id).ToList() : new List<int>();

            var deletedIds = oldIds.Where(e => newIds.Contains((int)e)).ToList();

            foreach(var id in deletedIds)
            {
                await _transportationCostJobRepository.DeleteAsync(e => e.TransportationCostId == input.Id && e.JobId == id);
            }

            foreach(var id in newIds)
            {
                if (!oldIds.Contains(id))
                {
                    var transportCostJob = new TransportationCostJob();
                    transportCostJob.JobId = id;
                    transportCostJob.TransportationCostId = input.Id;
                    await _transportationCostJobRepository.InsertAsync(transportCostJob);
                }
            }

        }

        public async Task Delete(EntityDto input)
        {
            await _transportationCostJobRepository.DeleteAsync(e => e.TransportationCostId == input.Id);

            await _transportCostRepository.DeleteAsync(input.Id);
        }

        public async Task<PagedResultDto<GetAllTransportCostForViewDto>> GetAll(GetTransportCostInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var ids = _transportationCostJobRepository.GetAll().WhereIf(input.FilterName == "Jobnumber" && !string.IsNullOrEmpty(input.FilterText), e => e.JobFK.JobNumber == input.FilterText).Select(e => e.TransportationCostId).Distinct().ToList();

            var filteredTransportCost = _transportCostRepository.GetAll().Include(e => e.TransportCompanyFK)
                                        .WhereIf(input.FilterName == "Jobnumber" && !string.IsNullOrEmpty(input.FilterText), e => ids.Contains(e.Id))
                                        .WhereIf(input.FilterName == "ContactNo" && !string.IsNullOrEmpty(input.FilterText), e => e.Mobile == input.FilterText)
                                        .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                                        .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date);

            var pagedAndFilteredTransportCost = filteredTransportCost
               .OrderBy(input.Sorting ?? "id desc")
               .PageBy(input);



            var result = from o in pagedAndFilteredTransportCost

                          //let jobs = _transportationCostJobRepository.GetAll().Include(e => e.JobFK).Where(e => e.TransportationCostId == o.Id).Select(e => e.JobId)

                         select new GetAllTransportCostForViewDto()
                         {
                             Id = o.Id,
                             //Jobs = _transportationCostJobRepository.GetAll().Include(e => e.JobFK).Where(e => e.TransportationCostId == o.Id).Select(e => e.JobFK.JobNumber).ToList(),
                             Mobile = o.Mobile,
                             TrackingNo = o.TrackingNo,
                             Amount = o.Amount,
                             Created = o.CreationTime,
                             TransportCompany = o.TransportCompanyFK.CompanyName
                         };

            var totalCount = filteredTransportCost.Count();

            var costjob = result.ToList();

            foreach(var item in costjob)
            {
                var data = _transportationCostJobRepository.GetAll().Include(e => e.JobFK).Where(e => e.TransportationCostId == item.Id).Select(e => e.JobFK.JobNumber).ToList();
                item.Jobs = data !=null ? (data.Count() > 0 ? string.Join(",",data) : "") : "";
            }

            return new PagedResultDto<GetAllTransportCostForViewDto>(
               totalCount,
               costjob
           );

        }

        public async Task<CreateOrEditTransportCost> GetTransportCostForEdit(EntityDto input)
        {
            var transportCost = await _transportCostRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<CreateOrEditTransportCost>(transportCost);

            output.jobnumber = (from o in _transportationCostJobRepository.GetAll().Include(e => e.JobFK).Where(e => e.TransportationCostId == input.Id)
                              select new CommonLookupDto()
                              {
                                  Id = (int)o.JobId,
                                  DisplayName = o.JobFK.JobNumber,
                              }).ToList();

            return output;
        }
    }
}
