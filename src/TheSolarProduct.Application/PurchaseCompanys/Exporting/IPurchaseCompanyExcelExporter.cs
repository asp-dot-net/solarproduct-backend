﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.PurchaseCompanys.Dtos;

namespace TheSolarProduct.PurchaseCompanys.Exporting
{
    public interface IPurchaseCompanyExcelExporter
    {
        FileDto ExportToFile(List<GetPurchaseCompanyForViewDto> purchaseCompany);
    }
}
