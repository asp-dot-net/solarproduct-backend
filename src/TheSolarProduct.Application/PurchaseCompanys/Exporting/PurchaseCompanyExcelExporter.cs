﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckApplications.Dtos;
using TheSolarProduct.CheckApplications.Exporting;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.PurchaseCompanys.Dtos;
using TheSolarProduct.Storage;

namespace TheSolarProduct.PurchaseCompanys.Exporting
{
    
    public class PurchaseCompanyExcelExporter : NpoiExcelExporterBase, IPurchaseCompanyExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PurchaseCompanyExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetPurchaseCompanyForViewDto> purchaseCompany)
        {
            return CreateExcelPackage(
                "PurchaseCompany.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("PurchaseCompany"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("IsActive"),
                         L("Address"),
                        L("Email"),
                         L("Phone")
                       
                    );

                    AddObjects(
                        sheet,
                        2,
                        purchaseCompany,
                        _ => _.PurchaseCompany.Name,
                        _ => _.PurchaseCompany.IsActive.HasValue ? (_.PurchaseCompany.IsActive.Value ? L("Yes") : L("No")) : L("No"),
                         _ => _.PurchaseCompany.Address,
                          _ => _.PurchaseCompany.Email,
                           _ => _.PurchaseCompany.Phone
                    );
                }
            );
        }


    }
}
