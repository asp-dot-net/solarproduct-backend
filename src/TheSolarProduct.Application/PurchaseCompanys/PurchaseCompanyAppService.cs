﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataVaults;
using TheSolarProduct.Dto;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.PurchaseCompanys.Exporting;
using TheSolarProduct.PurchaseCompanies;
using TheSolarProduct.PurchaseCompanys.Dtos;
using Abp.Linq.Extensions;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct.Storage;
using TheSolarProduct.MultiTenancy;
using System.Collections;
using TheSolarProduct.Common;

namespace TheSolarProduct.PurchaseCompanys
{
   
    [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseCompany)]
    public class PurchaseCompanyAppService : TheSolarProductAppServiceBase, IPurchaseCompanyAppService
    {
        private readonly IPurchaseCompanyExcelExporter _purchaseCompanyExcelExporter;

        private readonly IRepository<PurchaseCompany> _purchaseCompanyRepository;
        private readonly TempFileCacheManager _tempFileCacheManager;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<Tenant> _tenantRepository;

        public PurchaseCompanyAppService(
              IRepository<PurchaseCompany> purchaseCompanyRepository,
              IPurchaseCompanyExcelExporter IPurchaseCompanyExcelExporter,
               TempFileCacheManager tempFileCacheManager,
               ICommonLookupAppService CommonDocumentSaveRepository,
              IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
             IRepository<Tenant> tenantRepository
              )
        {
            _purchaseCompanyRepository = purchaseCompanyRepository;
            _purchaseCompanyExcelExporter = IPurchaseCompanyExcelExporter;
            _tenantRepository = tenantRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }




        public async Task<PagedResultDto<GetPurchaseCompanyForViewDto>> GetAll(GetAllPurchaseCompanyInput input)
        {

            var PurchaseCompany = _purchaseCompanyRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFilteredChcekApplication = PurchaseCompany
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var outputPurchaseCompany = from o in pagedAndFilteredChcekApplication
                                         select new GetPurchaseCompanyForViewDto()

                                         {
                                             PurchaseCompany = new PurchaseCompanyDto
                                             {
                                                 Id = o.Id,
                                                 Name = o.Name,
                                                 IsActive = o.IsActive,
                                                 Address = o.Address,
                                                 Email = o.Email,
                                                 Phone = o.Phone,
                                                 Logo=o.Logo,
                                             }
                                         };

            var totalCount = await PurchaseCompany.CountAsync();

            return new PagedResultDto<GetPurchaseCompanyForViewDto>(
                totalCount,
                await outputPurchaseCompany.ToListAsync()
            );
        }

       

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseCompany_Edit)]
        public async Task<GetPurchaseCompanyForEditOutput> GetPurchaseCompanyForEdit(EntityDto input)
        {
            var PurchaseCompany = await _purchaseCompanyRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPurchaseCompanyForEditOutput { PurchaseCompany = ObjectMapper.Map<CreateOrEditPurchaseCompanyDto>(PurchaseCompany) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditPurchaseCompanyDto input)
        {


            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseCompany_Create)]

        protected virtual async Task Create(CreateOrEditPurchaseCompanyDto input)
        {

            var FileName = DateTime.Now.Ticks + "_" + input.Name.Replace(" ", "") + ".png";
            //var InstallerByteArray = _tempFileCacheManager.GetFile(input.FileToken);
            var ByteArray = _tempFileCacheManager.GetFile(input.FileToken);
            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();

            var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, FileName, "PurchaseCompany", 0, 0, AbpSession.TenantId);
           
            input.Logo = "\\Documents\\" + TenantName + "\\PurchaseCompany" + "\\" + FileName;

            var purchaseCompany = ObjectMapper.Map<PurchaseCompany>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 59;
            dataVaultLog.ActionNote = "purchaseCompany Created : " + input.Name;
            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _purchaseCompanyRepository.InsertAsync(purchaseCompany);
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseCompany_Edit)]
        protected virtual async Task Update(CreateOrEditPurchaseCompanyDto input)
        {
            var purchaseCompany = await _purchaseCompanyRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 59;
            dataVaultLog.ActionNote = "purchaseCompany Updated : " + purchaseCompany.Name;

            dataVaultLog.IsInventory = true;
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != purchaseCompany.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = purchaseCompany.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Email != purchaseCompany.Email)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Email";
                history.PrevValue = purchaseCompany.Email;
                history.CurValue = input.Email;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Address != purchaseCompany.Address)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Address";
                history.PrevValue = purchaseCompany.Address;
                history.CurValue = input.Address;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Phone != purchaseCompany.Phone)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = purchaseCompany.Phone;
                history.CurValue = input.Phone;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != purchaseCompany.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = purchaseCompany.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (!string.IsNullOrEmpty(input.FileToken))
            {
                var FileName = DateTime.Now.Ticks + "_" + input.Name.Replace(" ", "") + ".png";
                var ByteArray = _tempFileCacheManager.GetFile(input.FileToken);
                var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();

                var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, FileName, "PurchaseCompany", 0, 0, AbpSession.TenantId);

                input.Logo = "\\Documents\\" + TenantName + "\\PurchaseCompany" + "\\" + FileName;

                
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, purchaseCompany);

            await _purchaseCompanyRepository.UpdateAsync(purchaseCompany);


        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseCompany_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _purchaseCompanyRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 59;
            dataVaultLog.ActionNote = "purchaseCompany Deleted : " + Name;

            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _purchaseCompanyRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseCompany_Export)]

        public async Task<FileDto> GetPurchaseCompanyToExcel(GetAllPurchaseCompanyForExcelInput input)
        {

            var filteredPurchaseCompany = _purchaseCompanyRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredPurchaseCompany
                         select new GetPurchaseCompanyForViewDto()
                         {
                             PurchaseCompany = new PurchaseCompanyDto
                             {
                                 Name = o.Name,
                                 Id = o.Id,
                                 IsActive = o.IsActive,
                                  Address = o.Address,
                                 Email = o.Email,
                                 Phone = o.Phone,
                             }
                         });


            var ListDtos = await query.ToListAsync();

            return _purchaseCompanyExcelExporter.ExportToFile(ListDtos);
        }
    }
}
