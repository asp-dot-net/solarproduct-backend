﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.StockOrders.Importing.Dto;
using TheSolarProduct.WholeSaleLeads.Importing.Dtos;

namespace TheSolarProduct.WholeSaleLeads.Importing
{
    public interface IWholeSaleLeadExcelDataReader : ITransientDependency
    {
        List<ImportWholeSaleLeadDto> GetWholeSaleLeadFromExcel(byte[] fileBytes);
    }
}
