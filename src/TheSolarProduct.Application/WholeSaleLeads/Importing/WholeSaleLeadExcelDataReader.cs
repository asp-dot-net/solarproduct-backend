﻿using Abp.Localization.Sources;
using Abp.Localization;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Invoices.Importing.Dto;
using TheSolarProduct.Invoices.Importing;
using TheSolarProduct.WholeSaleLeads.Importing.Dtos;
using System.Linq;

namespace TheSolarProduct.WholeSaleLeads.Importing
{
    
    public class WholeSaleLeadExcelDataReader : NpoiExcelImporterBase<ImportWholeSaleLeadDto>, IWholeSaleLeadExcelDataReader
    {
        private readonly ILocalizationSource _localizationSource;

        
        public WholeSaleLeadExcelDataReader(ILocalizationManager localizationManager)
        {
            _localizationSource = localizationManager.GetSource(TheSolarProductConsts.LocalizationSourceName);
        }

        public List<ImportWholeSaleLeadDto> GetWholeSaleLeadFromExcel(byte[] fileBytes)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow);
        }

        private ImportWholeSaleLeadDto ProcessExcelRow(ISheet worksheet, int row)
        {
          
            var exceptionMessage = new StringBuilder();
            var wholeSaleLead = new ImportWholeSaleLeadDto();

            try
            {

                worksheet.GetRow(row).Cells[0].SetCellType(CellType.String);
                wholeSaleLead.FirstName = worksheet.GetRow(row).Cells[0]?.StringCellValue;

                worksheet.GetRow(row).Cells[1].SetCellType(CellType.String);
                wholeSaleLead.LastName = worksheet.GetRow(row).Cells[1]?.StringCellValue;
                
                worksheet.GetRow(row).Cells[2].SetCellType(CellType.String);
                wholeSaleLead.CompanyName = worksheet.GetRow(row).Cells[2]?.StringCellValue;

                worksheet.GetRow(row).Cells[3].SetCellType(CellType.String);
                wholeSaleLead.MobileNo = worksheet.GetRow(row).Cells[3]?.StringCellValue;

                worksheet.GetRow(row).Cells[4].SetCellType(CellType.String);
                wholeSaleLead.Email = worksheet.GetRow(row).Cells[4]?.StringCellValue;


            }

            catch (System.Exception exception)
            {
                wholeSaleLead.Exception = exception.Message;
            }

            return wholeSaleLead;
        }

        private string GetLocalizedExceptionMessagePart(string parameter)
        {
            return _localizationSource.GetString("{0}IsInvalid", _localizationSource.GetString(parameter)) + "; ";
        }

        private string GetRequiredValueFromRowOrNull(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].StringCellValue;
            if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue))
            {
                return cellValue;
            }

            exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
            return null;
        }

        private double GetValue(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].NumericCellValue;

            return cellValue;

        }

        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
            return cell == null || string.IsNullOrWhiteSpace(cell.StringCellValue);
        }
    }
}
