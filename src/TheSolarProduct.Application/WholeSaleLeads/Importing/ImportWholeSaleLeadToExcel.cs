﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.Runtime.Session;
using Abp.Threading;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Invoices.Importing.Dto;
using TheSolarProduct.Invoices.Importing;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Notifications;
using TheSolarProduct.Storage;
using Abp.ObjectMapping;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.WholeSaleLeads.Dtos;
using TheSolarProduct.WholeSaleLeads.Importing.Dtos;
using TheSolarProduct.WholeSaleLeadActivityLogs;


namespace TheSolarProduct.WholeSaleLeads.Importing
{
    public class ImportWholeSaleLeadToExcel : BackgroundJob<ImportWholeSaleLeadFromExcelArgs>, ITransientDependency
    {
        private readonly IWholeSaleLeadExcelDataReader _wholeSaleLeadExcelDataReader;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IObjectMapper _objectMapper;
        private readonly IAppNotifier _appNotifier;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly InvalidWholeSaleLeadExporter _invalidWholeSaleLeadExporter;
        private readonly IAbpSession _abpSession;
        private readonly IRepository<User, long> _userRepository;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<WholeSaleLead> _wholeSaleLeadRepository;
        private readonly IRepository<Organizations.ExtendOrganizationUnit, long> _extendOrganizationUnitRepository;
        private readonly IRepository<WholeSaleLeadActivityLog> _leadactivityRepository;

        public ImportWholeSaleLeadToExcel(
            IWholeSaleLeadExcelDataReader wholeSaleLeadExcelDataReader,
            IBinaryObjectManager binaryObjectManager,
            IObjectMapper objectMapper,
            IAppNotifier appNotifier,
            IUnitOfWorkManager unitOfWorkManager,
            InvalidWholeSaleLeadExporter invalidWholeSaleLeadExporter,
            IAbpSession abpSession,
            IRepository<User, long> userRepository,
            IWebHostEnvironment env,
            IRepository<Tenant> tenantRepository,
            
            IRepository<Organizations.ExtendOrganizationUnit, long> extendOrganizationUnitRepository
            , IRepository<WholeSaleLead> wholeSaleLeadRepository
            , IRepository<WholeSaleLeadActivityLog> leadactivityRepository
            )
        {
            _wholeSaleLeadExcelDataReader = wholeSaleLeadExcelDataReader;
            _binaryObjectManager = binaryObjectManager;
            _objectMapper = objectMapper;
            _appNotifier = appNotifier;
            _invalidWholeSaleLeadExporter = invalidWholeSaleLeadExporter;
            _abpSession = abpSession;
            _userRepository = userRepository;
            _unitOfWorkManager = unitOfWorkManager;
            
            _env = env;
            _tenantRepository = tenantRepository;
            //_InvoiceFilesRepository = InvoiceFilesRepository;
            _extendOrganizationUnitRepository = extendOrganizationUnitRepository;
            _wholeSaleLeadRepository = wholeSaleLeadRepository;
            _leadactivityRepository = leadactivityRepository;
        }

        [UnitOfWork]
        public override void Execute(ImportWholeSaleLeadFromExcelArgs args)
        {

            //var fileObject = new BinaryObject(args.TenantId, args.FileBytes);
            //_binaryObjectManager.SaveAsync(fileObject);

            var WholeSaleLeads = GetWholeSaleLeadFromExcelOrNull(args);
            if (WholeSaleLeads == null || !WholeSaleLeads.Any())
            {
                SendInvalidExcelNotification(args);
                return;
            }
            else
            {
                var invalidWholeSaleLeads = new List<ImportWholeSaleLeadDto>();

                foreach (var WholeSaleLead in WholeSaleLeads)
                {
                    if (WholeSaleLead.CanBeImported())
                    {
                        try
                        {
                            AsyncHelper.RunSync(() => CreateInvoicesAsync(WholeSaleLead, args));
                        }
                        catch (UserFriendlyException exception)
                        {
                            invalidWholeSaleLeads.Add(WholeSaleLead);
                        }
                        catch (Exception e)
                        {
                            invalidWholeSaleLeads.Add(WholeSaleLead);
                        }
                    }
                    else
                    {
                        invalidWholeSaleLeads.Add(WholeSaleLead);
                    }
                }

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                    {
                        AsyncHelper.RunSync(() => ProcessImportWholSaleLeadResultAsync(args, invalidWholeSaleLeads));
                    }
                    uow.Complete();
                }
            }
        }

        private List<ImportWholeSaleLeadDto> GetWholeSaleLeadFromExcelOrNull(ImportWholeSaleLeadFromExcelArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    try
                    {
                        var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
                        return _wholeSaleLeadExcelDataReader.GetWholeSaleLeadFromExcel(file.Bytes);
                    }
                    catch (Exception e)
                    {
                        return null;
                    }
                    finally
                    {
                        uow.Complete();
                    }
                }
            }
        }

        private async Task CreateInvoicesAsync(ImportWholeSaleLeadDto input, ImportWholeSaleLeadFromExcelArgs args)
        {
            using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
            {
                
                var isDuplicate = await _wholeSaleLeadRepository.GetAll()
                    .AnyAsync(wsl => wsl.Mobile == input.MobileNo || wsl.Email == input.Email);

                if (!isDuplicate)
                {
                    var wholeSaleLead = new WholeSaleLead
                    {
                        FirstName = input.FirstName,
                        LastName = input.LastName,
                        CompanyName = input.CompanyName,
                        Mobile = input.MobileNo,
                        Email=input.Email,
                        OrganizationId=args.OrganizationId,
                        AssignDate = DateTime.UtcNow,
                        AssignUserId = (int)args.User.UserId,
                        FirstAssignDate = DateTime.UtcNow,
                        FirstAssignUserId =(int)args.User.UserId,
                        CreatorUserId = (int)args.User.UserId,
                        TenantId = (int)args.TenantId,

                };

                    var wholeSaleLeadId = await _wholeSaleLeadRepository.InsertAndGetIdAsync(wholeSaleLead);
                    await CurrentUnitOfWork.SaveChangesAsync();

                    WholeSaleLeadActivityLog leadActivitys = new WholeSaleLeadActivityLog
                    {
                        ActionId = 53,
                        SectionId = 35,
                        ActionNote = "WholeSaleLead Created",
                        WholeSaleLeadId = wholeSaleLeadId,
                        TenantId = (int)args.TenantId
                    };

                    await _leadactivityRepository.InsertAsync(leadActivitys);
                }
                else
                {
                   
                }
            }
        }


        private void SendInvalidExcelNotification(ImportWholeSaleLeadFromExcelArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
                        args.User,
                        new LocalizableString("FileCantBeConvertedToWhoSaleLead", TheSolarProductConsts.LocalizationSourceName),
                        null,
                        Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }

        private async Task ProcessImportWholSaleLeadResultAsync(ImportWholeSaleLeadFromExcelArgs args,
          List<ImportWholeSaleLeadDto> invalidWholeSaleLeads)
        {
            if (invalidWholeSaleLeads.Any())
            {
                var file = _invalidWholeSaleLeadExporter.ExportToFile(invalidWholeSaleLeads);
                await _appNotifier.SomeInstallationCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
            }
            else
            {
                await _appNotifier.SendMessageAsync(
                    args.User,
                    new LocalizableString("WhosaleLeadAddedSuccessfullyImportedFromExcel", TheSolarProductConsts.LocalizationSourceName),
                    null,
                    Abp.Notifications.NotificationSeverity.Success);
            }
        }
    }
}
