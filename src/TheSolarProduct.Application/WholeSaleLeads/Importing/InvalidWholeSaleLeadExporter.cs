﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Invoices.Importing.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.WholeSaleLeads.Importing.Dtos;

namespace TheSolarProduct.WholeSaleLeads.Importing
{
    
    public class InvalidWholeSaleLeadExporter : NpoiExcelExporterBase, ITransientDependency
    {
        public InvalidWholeSaleLeadExporter(ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager)
        {
        }

        public FileDto ExportToFile(List<ImportWholeSaleLeadDto> stcListDtos)
        {

            return CreateExcelPackage(
         "InvalidSTCS.xlsx",
          excelPackage =>
          {
              var sheet = excelPackage.CreateSheet("InvalidSTC");


              AddHeader(sheet,
                  "FirstName",
                  "LastName",
                  "CompanyName",
                  "MobileNo",
                  "Email",
                  "Exception"
                  );


              AddObjects(
                    sheet, 2, stcListDtos,
                    _ => _.FirstName,
                    _ => _.LastName,
                    _ => _.CompanyName,
                    _ => _.MobileNo,
                    _=>_.Email,
                    _ => _.Exception
                );




          });

        }
    }
}
