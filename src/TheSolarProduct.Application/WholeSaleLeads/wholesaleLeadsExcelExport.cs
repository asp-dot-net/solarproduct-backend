﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.WholeSaleLeads.Dtos;

namespace TheSolarProduct.WholeSaleLeads
{
    public class WholesaleLeadsExcelExport : NpoiExcelExporterBase, IWholesaleLeadsExcelExport
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        //private readonly PermissionChecker _permissionChecker;
        private readonly ITempFileCacheManager _tempFileCacheManager;

        public WholesaleLeadsExcelExport(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager
            //,PermissionChecker permissionChecker
            ) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            //_permissionChecker = permissionChecker;
            _tempFileCacheManager = tempFileCacheManager;
        }
        public FileDto LeadTrackerExportToFile(List<GetWholeSaleLeadForViewDto> leadListDtos)
        {
            return CreateExcelPackage(
                "WholesaleLeads.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("WholeSale"));

                    AddHeader(
                        sheet,
                        "Full Name",
                        "Company Name",
                        "Email",
                        "Mobile",
                        "BDM",
                        "SalesRapName",
                        "WholeSale Status",
                        "Postal Address",
                        "Postal State",
                        "Delivary Address",
                        "Delivary State",
                        "ReminderDate",
                        "Reminder",
                        "Comment"
                        );

                    AddObjects(
                        sheet, 2, leadListDtos,
                        _ => _.createOrEditWholeSaleLeadDto.FirstName + " " + _.createOrEditWholeSaleLeadDto.LastName,
                        _ => _.createOrEditWholeSaleLeadDto.CompanyName,
                        _ => _.createOrEditWholeSaleLeadDto.Email,
                        _ => _.createOrEditWholeSaleLeadDto.Mobile,
                        _ => _.createOrEditWholeSaleLeadDto.BDM,
                        _ => _.createOrEditWholeSaleLeadDto.SalesRapName,
                        _ => _.createOrEditWholeSaleLeadDto.WholeSaleStatus,
                        _ => _.createOrEditWholeSaleLeadDto.PostalAddress,
                        _ => _.createOrEditWholeSaleLeadDto.PostalState,
                        _ => _.createOrEditWholeSaleLeadDto.DelivaryAddress,
                        _ => _.createOrEditWholeSaleLeadDto.DelivaryState,
                        _ => _.ReminderTime,
                        _ => _.ActivityComment
                        );

                    for (var i = 1; i <= sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);

                    }

                });
        }
    }
}
