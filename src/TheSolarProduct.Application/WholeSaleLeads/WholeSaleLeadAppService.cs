﻿using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.WholeSaleLeads.Dtos;
using System;
using System.Collections.Generic;
using TheSolarProduct.Leads.Dtos;
using Abp.Timing.Timezone;
using TheSolarProduct.Timing;
using TheSolarProduct.WholeSalePromotions;
using TheSolarProduct.WholeSaleLeadActivityLogs;
using TheSolarProduct.Leads;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.UserWiseEmailOrgs;
using TheSolarProduct.Organizations;
using TheSolarProduct.EmailTemplates.Dtos;
using TheSolarProduct.WholeSaleLeadHistory;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using System.IO;
using TheSolarProduct.Storage;
using TheSolarProduct.Common;
using TheSolarProduct.WholeSaleLeadDocs;
using Microsoft.AspNetCore.Hosting;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.WholeSaleSmsTemplates;
using TheSolarProduct.WholeSaleSmsTemplates.Dtos;
using TheSolarProduct.WholeSaleStatuses;
using TheSolarProduct.LeadActions;
using TheSolarProduct.Sections;
using TheSolarProduct.States;
using NUglify.Helpers;
using Abp.Notifications;
using Abp.Organizations;
using TheSolarProduct.Notifications;
using TheSolarProduct.WholeSaleEmailTemplates;
using TheSolarProduct.WholeSaleEmailTemplates.Dto;
using Abp.Authorization;
using TheSolarProduct.Wholesales.Ecommerces;
using Abp.Net.Mail;
using System.Net.Mail;
using NPOI.SS.Formula.Functions;
using TheSolarProduct.Businesses;
using TheSolarProduct.Migrations;
using Section = TheSolarProduct.Sections.Section;
using System.Globalization;
using TheSolarProduct.Common.Dto;
using Abp.Authorization.Users;
using TheSolarProduct.Dto;
using Microsoft.Azure.KeyVault.Models;
using TheSolarProduct.LeadHistory;
using TheSolarProduct.LeadStatuses;
using Abp.Collections.Extensions;
using Abp.Authorization.Roles;

namespace TheSolarProduct.WholeSaleLeads
{
    [AbpAuthorize]
    public class WholeSaleLeadAppService : TheSolarProductAppServiceBase, IWholeSaleLeadAppService
    {
        private readonly IRepository<User, long> _userRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<WholeSaleLead> _wholeSaleLeadRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly ITimeZoneService _timeZoneService;
        private readonly IRepository<WholeSalePromotionUser> _wholeSalePromotionUsersRepository;
        private readonly IRepository<WholeSaleLeadActivityLog> _leadactivityRepository;
        private readonly IRepository<WholeSaleSmsTemplate> _wholeSalesmsTemplateRepository;
        private readonly IRepository<UserWiseEmailOrg> _UserWiseEmailOrgRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendedOrganizationUnitRepository;
        private readonly IRepository<WholeSaleEmailTemplate> _emailTemplateRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly IRepository<WholeSaleLeadDoc> _wholeSaleLeadDocRepository;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<WholeSaleStatus> _wholeSaleStatusRepository;
        private readonly IRepository<LeadAction, int> _lookup_leadActionRepository;
        private readonly IRepository<Section> _sectionRepository;
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<WholeSaleLeadtrackerHistory> _wholeSaleLeadtrackerHistoryRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<WholeSaleLeadDocumentType> _wholeSaleLeadDocumentTypeRepository;
        private readonly IRepository<WholeSaleLeadContact> _wholeSaleLeadContactRepository;
        private readonly IRepository<ECommercePriceCategory> _eCommercePriceCategoryRepository;
        private readonly IRepository<TradingSTCWhom> _tradingSTCWhomRepository;
        private readonly IRepository<EcommerceUserRegisterRequest> _ecommerceUserRegisterRequestRepository;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<Business> _businessRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IWholesaleLeadsExcelExport _wholesaleLeadsExcelExport;
        private readonly IRepository<RequestWholesaleLeadTransfer> _requestWholesaleLeadTransferRepository;

        public WholeSaleLeadAppService(
            IRepository<User,long> userRepository
            , UserManager userManager
            , IRepository<WholeSaleLead> wholeSaleLeadRepository
            , ITimeZoneConverter timeZoneConverter
            , ITimeZoneService timeZoneService
            , IRepository<WholeSalePromotionUser> wholeSalePromotionUsersRepository
            , IRepository<WholeSaleLeadActivityLog> leadactivityRepository
            , IRepository<WholeSaleSmsTemplate> wholeSalesmsTemplateRepository
            , IRepository<UserWiseEmailOrg> userWiseEmailOrgRepository
            , IRepository<ExtendOrganizationUnit, long> extendedOrganizationUnitRepository
            , IRepository<WholeSaleEmailTemplate> emailTemplateRepository
            , IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            , ITempFileCacheManager tempFileCacheManager
            , ICommonLookupAppService CommonDocumentSaveRepository
            , IRepository<WholeSaleLeadDoc> wholeSaleLeadDocRepository
            , IWebHostEnvironment env
            , IRepository<WholeSaleStatus> wholeSaleStatusRepository
            , IRepository<LeadAction, int> lookup_leadActionRepository
            , IRepository<Section> sectionRepository
            , IRepository<State> stateRepository
            , IRepository<WholeSaleLeadtrackerHistory> wholeSaleLeadtrackerHistoryRepository
            , IRepository<OrganizationUnit, long> organizationUnitRepository
            , IAppNotifier appNotifier
            , IRepository<WholeSaleLeadDocumentType> wholeSaleLeadDocumentTypeRepository
            , IRepository<WholeSaleLeadContact> wholeSaleLeadContactRepository
            , IRepository<ECommercePriceCategory> eCommercePriceCategoryRepository
            , IRepository<TradingSTCWhom> tradingSTCWhomRepository
            , IRepository<EcommerceUserRegisterRequest> ecommerceUserRegisterRequestRepository
            , IEmailSender emailSender
            , IRepository<Business> businessRepository
            , IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository
            , IWholesaleLeadsExcelExport wholesaleLeadsExcelExport
            , IRepository<RequestWholesaleLeadTransfer> requestWholesaleLeadTransferRepository
            )
        {
            _userRepository = userRepository;
            _userManager = userManager;
            _wholeSaleLeadRepository = wholeSaleLeadRepository;
            _timeZoneConverter = timeZoneConverter;
            _timeZoneService = timeZoneService;
            _wholeSalePromotionUsersRepository = wholeSalePromotionUsersRepository;
            _leadactivityRepository = leadactivityRepository;
            _wholeSalesmsTemplateRepository = wholeSalesmsTemplateRepository;
            _UserWiseEmailOrgRepository = userWiseEmailOrgRepository;
            _extendedOrganizationUnitRepository = extendedOrganizationUnitRepository;
            _emailTemplateRepository = emailTemplateRepository;
            _dbcontextprovider = dbcontextprovider;
            _tempFileCacheManager = tempFileCacheManager;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _wholeSaleLeadDocRepository = wholeSaleLeadDocRepository;
            _env = env;
            _wholeSaleStatusRepository = wholeSaleStatusRepository;
            _lookup_leadActionRepository = lookup_leadActionRepository;
            _sectionRepository = sectionRepository;
            _stateRepository = stateRepository;
            _wholeSaleLeadtrackerHistoryRepository = wholeSaleLeadtrackerHistoryRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _appNotifier = appNotifier;
            _wholeSaleLeadDocumentTypeRepository = wholeSaleLeadDocumentTypeRepository;
            _wholeSaleLeadContactRepository = wholeSaleLeadContactRepository;
            _eCommercePriceCategoryRepository = eCommercePriceCategoryRepository;
            _tradingSTCWhomRepository = tradingSTCWhomRepository;
            _ecommerceUserRegisterRequestRepository = ecommerceUserRegisterRequestRepository;
            _emailSender = emailSender;
            _businessRepository = businessRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _wholesaleLeadsExcelExport = wholesaleLeadsExcelExport;
            _requestWholesaleLeadTransferRepository = requestWholesaleLeadTransferRepository;
        }
        public async Task CreateOrEdit(CreateOrEditWholeSaleLeadDto input)
        {
            if (input.Id == null || input.Id == 0)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        protected virtual async Task Create(CreateOrEditWholeSaleLeadDto input)
        {
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

            //IList<string> role = await _userManager.GetRolesAsync(assignedToUser);

            
            var wholeSaleLead = ObjectMapper.Map<WholeSaleLead>(input);
         

            wholeSaleLead.AssignDate = DateTime.UtcNow;
            wholeSaleLead.AssignUserId = (int)AbpSession.UserId;
            wholeSaleLead.FirstAssignDate = DateTime.UtcNow;
            wholeSaleLead.FirstAssignUserId = (int)AbpSession.UserId;

            wholeSaleLead.CreatorUserId = AbpSession.UserId;
            wholeSaleLead.TenantId = (int)AbpSession.TenantId;
            
            if(input.IsDelivery == true)
            {
                wholeSaleLead.DelivaryLatitude = input.PostalLatitude;
                wholeSaleLead.DelivaryLongitude = input.PostalLongitude;
                wholeSaleLead.DelivaryPostCode = input.PostalPostCode;
                wholeSaleLead.DelivaryStateId = input.PostalStateId;
                wholeSaleLead.DelivaryStreetName = input.PostalStreetName;
                wholeSaleLead.DelivaryStreetNo = input.PostalStreetNo;
                wholeSaleLead.DelivaryStreetType = input.PostalStreetType;
                wholeSaleLead.DelivaryUnitNo = input.PostalUnitNo;
                wholeSaleLead.DelivaryUnitType = input.PostalUnitType;
            }

            var wholeSaleLeadid = await _wholeSaleLeadRepository.InsertAndGetIdAsync(wholeSaleLead);

            foreach(var contact in input.WholeSaleLeadContacts)
            {
                contact.WholesaleLeadId = wholeSaleLeadid;
                var wholeSaleLeadcontact = ObjectMapper.Map<WholeSaleLeadContact>(contact);

                await _wholeSaleLeadContactRepository.InsertAsync(wholeSaleLeadcontact);
            }

            WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
            leadactivity.ActionId = 53;
            leadactivity.SectionId = 35;
            leadactivity.ActionNote = "WholeSaleLead Created";
            leadactivity.WholeSaleLeadId = wholeSaleLeadid;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
           
            var leadactivityId = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

            var List = new List<WholeSaleLeadtrackerHistory>();


            WholeSaleLeadtrackerHistory history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "ABNNumber";
            history.PrevValue = "";
            history.CurValue = input.ABNNumber;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "FirstName";
            history.PrevValue = "";
            history.CurValue = input.FirstName;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "LastName";
            history.PrevValue = "";
            history.CurValue = input.LastName;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "CompanyName";
            history.PrevValue = "";
            history.CurValue = input.CompanyName;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "Mobile";
            history.PrevValue = "";
            history.CurValue = input.Mobile;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "Talephone";
            history.PrevValue = "";
            history.CurValue = input.Talephone;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "CustomerType";
            history.PrevValue = "";
            history.CurValue = input.CustomerType;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "CustomerType";
            history.PrevValue = "";
            history.CurValue = input.CustomerType;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "WholeSaleStatus";
            history.PrevValue = "";
            history.CurValue = input.WholeSaleStatusId > 0 ? _wholeSaleStatusRepository.GetAll().Where(e => e.Id == input.WholeSaleStatusId).FirstOrDefault().Name : "";
            history.Action = "Add";
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            history.WholeSaleLeadActivityId = leadactivityId;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "PostalUnitNo";
            history.PrevValue = "";
            history.CurValue = input.PostalUnitNo;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "PostalUnitType";
            history.PrevValue = "";
            history.CurValue = input.PostalUnitType;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "PostalStreetNo";
            history.PrevValue = "";
            history.CurValue = input.PostalStreetNo;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "PostalStreetName";
            history.PrevValue = "";
            history.CurValue = input.PostalStreetName;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "PostalStreetType";
            history.PrevValue = "";
            history.CurValue = input.PostalStreetType;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "PostalStateId";
            history.PrevValue = "";
            history.CurValue = input.PostalStateId.ToString();
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "PostalPostCode";
            history.PrevValue = "";
            history.CurValue = input.PostalPostCode;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "PostalPostCode";
            history.PrevValue = "";
            history.CurValue = input.PostalPostCode;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "PostalLatitude";
            history.PrevValue = "";
            history.CurValue = input.PostalLatitude;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "PostalLongitude";
            history.PrevValue = "";
            history.CurValue = input.PostalLongitude;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "DelivaryUnitNo";
            history.PrevValue = "";
            history.CurValue = input.DelivaryUnitNo;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);
            
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "DelivaryUnitType";
            history.PrevValue = "";
            history.CurValue = input.DelivaryUnitType;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);
            

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "DelivaryStreetNo";
            history.PrevValue = "";
            history.CurValue = input.DelivaryStreetNo;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "DelivaryStreetName";
            history.PrevValue = "";
            history.CurValue = input.DelivaryStreetName;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "DelivaryStreetType";
            history.PrevValue = "";
            history.CurValue = input.DelivaryStreetType;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "DelivaryStateId";
            history.PrevValue = "";
            history.CurValue = input.DelivaryStateId.ToString();
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "DelivaryPostCode";
            history.PrevValue = "";
            history.CurValue = input.DelivaryPostCode;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);
            

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "DelivaryLatitude";
            history.PrevValue = "";
            history.CurValue = input.DelivaryLatitude;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "DelivaryLongitude";
            history.PrevValue = "";
            history.CurValue = input.DelivaryLongitude;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "BankName";
            history.PrevValue = "";
            history.CurValue = input.BankName;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            // Adding history for AccountName
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "AccountName";
            history.PrevValue = "";
            history.CurValue = input.AccountName;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            // Adding history for BSBNo
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "BSBNo";
            history.PrevValue = "";
            history.CurValue = input.BSBNo;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            // Adding history for AccoutNo
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "AccoutNo";
            history.PrevValue = "";
            history.CurValue = input.AccoutNo;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            // Adding history for CreditAmount
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "CreditAmount";
            history.PrevValue = "";
            history.CurValue = input.CreditAmount.ToString();
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            // Adding history for CreditDays
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "CreditDays";
            history.PrevValue = "";
            history.CurValue = input.CreditDays.ToString();
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            // Adding history for AssignUserId
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "AssignUserId";
            history.PrevValue = "";
            history.CurValue = input.AssignUserId.ToString();
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            // Adding history for AssignDate
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "AssignDate";
            history.PrevValue = "";
            history.CurValue = input.AssignDate.ToString();
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            // Adding history for FirstAssignUserId
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "FirstAssignUserId";
            history.PrevValue = "";
            history.CurValue = input.FirstAssignUserId.ToString();
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            // Adding history for FirstAssignDate
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "FirstAssignDate";
            history.PrevValue = "";
            history.CurValue = input.FirstAssignDate.ToString();
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            // Adding history for BDMId
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "BDMId";
            history.PrevValue = "";
            history.CurValue = input.BDMId.ToString();
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            // Adding history for AditionalNotes
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "AditionalNotes";
            history.PrevValue = "";
            history.CurValue = input.AditionalNotes;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

           
            // Adding history for SalesRapId
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "SalesRapId";
            history.PrevValue = "";
            history.CurValue = input.SalesRapId.ToString();
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            // Adding history for TradinSTC
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "TradinSTC";
            history.PrevValue = "";
            history.CurValue = input.TradinSTC.ToString();
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);


            // Adding history for TradinSTCWhomId
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "TradinSTCWhomId";
            history.PrevValue = "";
            history.CurValue = input.TradinSTCWhomId.ToString();
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "TradinSTCNote";
            history.PrevValue = "";
            history.CurValue = input.TradinSTCNote;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            // Adding history for PriceCategoryId
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "PriceCategoryId";
            history.PrevValue = "";
            history.CurValue = input.PriceCategoryId.ToString();
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);


            // Adding history for PostalSuburb
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "PostalSuburb";
            history.PrevValue = "";
            history.CurValue = input.PostalSuburb;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            // Adding history for PostalSuburbId
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "PostalSuburbId";
            history.PrevValue = "";
            history.CurValue = input.PostalSuburbId.ToString();
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            // Adding history for DelivarySuburb
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "DelivarySuburb";
            history.PrevValue = "";
            history.CurValue = input.DelivarySuburb;
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            // Adding history for DelivarySuburbId
            history = new WholeSaleLeadtrackerHistory();
            if (AbpSession.TenantId != null)
            {
                history.TenantId = (int)AbpSession.TenantId;
            }
            history.FieldName = "DelivarySuburbId";
            history.PrevValue = "";
            history.CurValue = input.DelivarySuburbId.ToString();
            history.Action = "Add";
            history.WholeSaleLeadActivityId = leadactivityId;
            history.LastmodifiedDateTime = DateTime.Now;
            history.WholeSaleLeadId = wholeSaleLead.Id;
            List.Add(history);

            await _dbcontextprovider.GetDbContext().WholeSaleLeadtrackerHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
        }

        protected virtual async Task Update(CreateOrEditWholeSaleLeadDto input)
        {
            var wholeSaleLead = await _wholeSaleLeadRepository.FirstOrDefaultAsync((int)input.Id);
            if (input.IsDelivery == true)
            {
                input.DelivaryLatitude = input.PostalLatitude;
                input.DelivaryLongitude = input.PostalLongitude;
                input.DelivaryPostCode = input.PostalPostCode;
                input.DelivaryStateId = input.PostalStateId;
                input.DelivaryStreetName = input.PostalStreetName;
                input.DelivaryStreetNo = input.PostalStreetNo;
                input.DelivaryStreetType = input.PostalStreetType;
                input.DelivaryUnitNo = input.PostalUnitNo;
                input.DelivaryUnitType = input.PostalUnitType;
            }
            

            WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
            leadactivity.ActionId = 54;
            leadactivity.SectionId = 35;
            leadactivity.ActionNote = "WholeSaleLead Modified";
            leadactivity.WholeSaleLeadId = wholeSaleLead.Id;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);

            var List = new List<WholeSaleLeadtrackerHistory>();

            if (wholeSaleLead.CompanyName != input.CompanyName)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "CompanyName";
                leadStatus.PrevValue = wholeSaleLead.CompanyName;
                leadStatus.CurValue = input.CompanyName;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.ABNNumber != input.ABNNumber)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "ABNNumber";
                leadStatus.PrevValue = wholeSaleLead.ABNNumber;
                leadStatus.CurValue = input.ABNNumber;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.FirstName != input.FirstName)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "FirstName";
                leadStatus.PrevValue = wholeSaleLead.FirstName;
                leadStatus.CurValue = input.FirstName;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.LastName != input.LastName)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "LastName";
                leadStatus.PrevValue = wholeSaleLead.LastName;
                leadStatus.CurValue = input.LastName;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.Mobile != input.Mobile)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Mobile";
                leadStatus.PrevValue = wholeSaleLead.Mobile;
                leadStatus.CurValue = input.Mobile;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.Talephone != input.Talephone)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Talephone";
                leadStatus.PrevValue = wholeSaleLead.Talephone;
                leadStatus.CurValue = input.Talephone;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.CustomerType != input.CustomerType)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "CustomerType";
                leadStatus.PrevValue = wholeSaleLead.CustomerType;
                leadStatus.CurValue = input.CustomerType;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.WholeSaleStatusId != input.WholeSaleStatusId)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "WholeSaleStatus";
                leadStatus.PrevValue = wholeSaleLead.WholeSaleStatusId > 0? _wholeSaleStatusRepository.GetAll().Where(e => e.Id == wholeSaleLead.WholeSaleStatusId).FirstOrDefault().Name : "";
                leadStatus.CurValue = input.WholeSaleStatusId > 0 ? _wholeSaleStatusRepository.GetAll().Where(e => e.Id == input.WholeSaleStatusId).FirstOrDefault().Name : "";
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.PostalUnitNo != input.PostalUnitNo)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "PostalUnitNo";
                leadStatus.PrevValue = wholeSaleLead.PostalUnitNo;
                leadStatus.CurValue = input.PostalUnitNo;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.PostalUnitType != input.PostalUnitType)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "PostalUnitType";
                leadStatus.PrevValue = wholeSaleLead.PostalUnitType;
                leadStatus.CurValue = input.PostalUnitType;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.PostalStreetNo != input.PostalStreetNo)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "PostalStreetNo";
                leadStatus.PrevValue = wholeSaleLead.PostalStreetNo;
                leadStatus.CurValue = input.PostalStreetNo;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.PostalStreetName != input.PostalStreetName)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "PostalStreetName";
                leadStatus.PrevValue = wholeSaleLead.PostalStreetName;
                leadStatus.CurValue = input.PostalStreetName;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.PostalStreetType != input.PostalStreetType)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "PostalStreetType";
                leadStatus.PrevValue = wholeSaleLead.PostalStreetType;
                leadStatus.CurValue = input.PostalStreetType;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.PostalStateId != input.PostalStateId)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "PostalStateId";
                leadStatus.PrevValue = wholeSaleLead.PostalStateId.ToString();
                leadStatus.CurValue = input.PostalStateId.ToString();
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.PostalPostCode != input.PostalPostCode)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "PostalPostCode";
                leadStatus.PrevValue = wholeSaleLead.PostalPostCode;
                leadStatus.CurValue = input.PostalPostCode;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.PostalLatitude != input.PostalLatitude)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "PostalLatitude";
                leadStatus.PrevValue = wholeSaleLead.PostalLatitude;
                leadStatus.CurValue = input.PostalLatitude;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.PostalLongitude != input.PostalLongitude)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "PostalLongitude";
                leadStatus.PrevValue = wholeSaleLead.PostalLongitude;
                leadStatus.CurValue = input.PostalLongitude;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            } 
            if (wholeSaleLead.DelivaryUnitNo != input.DelivaryUnitNo)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "DelivaryUnitNo";
                leadStatus.PrevValue = wholeSaleLead.DelivaryUnitNo;
                leadStatus.CurValue = input.DelivaryUnitNo;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.DelivaryUnitType != input.DelivaryUnitType)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "DelivaryUnitType";
                leadStatus.PrevValue = wholeSaleLead.DelivaryUnitType;
                leadStatus.CurValue = input.DelivaryUnitType;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.DelivaryStreetNo != input.DelivaryStreetNo)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "DelivaryStreetNo";
                leadStatus.PrevValue = wholeSaleLead.DelivaryStreetNo;
                leadStatus.CurValue = input.DelivaryStreetNo;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.DelivaryStreetName != input.DelivaryStreetName)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "DelivaryStreetName";
                leadStatus.PrevValue = wholeSaleLead.DelivaryStreetName;
                leadStatus.CurValue = input.DelivaryStreetName;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.DelivaryStreetType != input.DelivaryStreetType)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "DelivaryStreetType";
                leadStatus.PrevValue = wholeSaleLead.DelivaryStreetType;
                leadStatus.CurValue = input.DelivaryStreetType;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.DelivaryStateId != input.DelivaryStateId)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "DelivaryStateId";
                leadStatus.PrevValue = wholeSaleLead.DelivaryStateId.ToString();
                leadStatus.CurValue = input.DelivaryStateId.ToString();
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.DelivaryPostCode != input.DelivaryPostCode)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "DelivaryPostCode";
                leadStatus.PrevValue = wholeSaleLead.DelivaryPostCode;
                leadStatus.CurValue = input.DelivaryPostCode;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.DelivaryLatitude != input.DelivaryLatitude)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "DelivaryLatitude";
                leadStatus.PrevValue = wholeSaleLead.DelivaryLatitude;
                leadStatus.CurValue = input.DelivaryLatitude;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.DelivaryLongitude != input.DelivaryLongitude)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "DelivaryLongitude";
                leadStatus.PrevValue = wholeSaleLead.DelivaryLongitude;
                leadStatus.CurValue = input.DelivaryLongitude;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.BankName != input.BankName)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "BankName";
                leadStatus.PrevValue = wholeSaleLead.BankName;
                leadStatus.CurValue = input.BankName;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.AccountName != input.AccountName)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "AccountName";
                leadStatus.PrevValue = wholeSaleLead.AccountName;
                leadStatus.CurValue = input.AccountName;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.BSBNo != input.BSBNo)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "BSBNo";
                leadStatus.PrevValue = wholeSaleLead.BSBNo;
                leadStatus.CurValue = input.BSBNo;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.AccoutNo != input.AccoutNo)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "AccoutNo";
                leadStatus.PrevValue = wholeSaleLead.AccoutNo;
                leadStatus.CurValue = input.AccoutNo;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.CreditAmount != input.CreditAmount)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "CreditAmount";
                leadStatus.PrevValue = wholeSaleLead.CreditAmount.ToString();
                leadStatus.CurValue = input.CreditAmount.ToString();
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.CreditDays != input.CreditDays)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "CreditDays";
                leadStatus.PrevValue = wholeSaleLead.CreditDays.ToString();
                leadStatus.CurValue = input.CreditDays.ToString();
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.AssignUserId != input.AssignUserId)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "AssignUserId";
                leadStatus.PrevValue = wholeSaleLead.AssignUserId.ToString();
                leadStatus.CurValue = input.AssignUserId.ToString();
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.AssignDate != input.AssignDate)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "AssignDate";
                leadStatus.PrevValue = wholeSaleLead.AssignDate.ToString();
                leadStatus.CurValue = input.AssignDate.ToString();
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.FirstAssignUserId != input.FirstAssignUserId)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "FirstAssignUserId";
                leadStatus.PrevValue = wholeSaleLead.FirstAssignUserId.ToString();
                leadStatus.CurValue = input.FirstAssignUserId.ToString();
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.FirstAssignDate != input.FirstAssignDate)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "FirstAssignDate";
                leadStatus.PrevValue = wholeSaleLead.FirstAssignDate.ToString();
                leadStatus.CurValue = input.FirstAssignDate.ToString();
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.BDMId != input.BDMId)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "BDM";
                leadStatus.PrevValue = wholeSaleLead.BDMId > 0 ? _userRepository.GetAll().Where(e => e.Id == wholeSaleLead.BDMId).FirstOrDefault().Name : "";
                leadStatus.CurValue = input.BDMId > 0 ? _userRepository.GetAll().Where(e => e.Id == input.BDMId).FirstOrDefault().Name : "";
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.AditionalNotes != input.AditionalNotes)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "AditionalNotes";
                leadStatus.PrevValue = wholeSaleLead.AditionalNotes;
                leadStatus.CurValue = input.AditionalNotes;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.Email != input.Email)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "Email";
                leadStatus.PrevValue = wholeSaleLead.Email;
                leadStatus.CurValue = input.Email;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.SalesRapId != input.SalesRapId)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "SalseRap";
                leadStatus.PrevValue = wholeSaleLead.SalesRapId > 0 ? _userRepository.GetAll().Where(e => e.Id == wholeSaleLead.SalesRapId).FirstOrDefault().Name : "";
                leadStatus.CurValue = input.SalesRapId > 0 ? _userRepository.GetAll().Where(e => e.Id == input.SalesRapId).FirstOrDefault().Name : "";
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.TradinSTC != input.TradinSTC)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "TradinSTC";
                leadStatus.PrevValue = wholeSaleLead.TradinSTC != true ? "No" : "YEs";
                leadStatus.CurValue = input.TradinSTC != true ? "No" : "YEs";
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.TradinSTCPrice != input.TradinSTCPrice)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "TradinSTCPrice";
                leadStatus.PrevValue = wholeSaleLead.TradinSTCPrice.ToString();
                leadStatus.CurValue = input.TradinSTCPrice.ToString();
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.TradinSTCWhomId != input.TradinSTCWhomId)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "TradinSTCWhom";
                leadStatus.PrevValue = wholeSaleLead.TradinSTCWhomId > 0 ? _tradingSTCWhomRepository.Get((int)wholeSaleLead.TradinSTCWhomId).Name : "";
                leadStatus.CurValue = input.TradinSTCWhomId > 0 ? _tradingSTCWhomRepository.Get((int)input.TradinSTCWhomId).Name : "";
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.TradinSTCNote != input.TradinSTCNote)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "TradinSTCNote";
                leadStatus.PrevValue = wholeSaleLead.TradinSTCNote;
                leadStatus.CurValue = input.TradinSTCNote;
                leadStatus.Action = "Edit";
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }
            if (wholeSaleLead.PriceCategoryId != input.PriceCategoryId)
            {
                WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadStatus.TenantId = (int)AbpSession.TenantId;
                }
                leadStatus.FieldName = "PriceCategory";
                leadStatus.PrevValue =  wholeSaleLead.PriceCategoryId > 0 ? _eCommercePriceCategoryRepository.GetAll().Where(e => e.Id == wholeSaleLead.PriceCategoryId).Select(e => e.PriceCategory).FirstOrDefault() : "";
                leadStatus.CurValue = input.PriceCategoryId > 0 ? _eCommercePriceCategoryRepository.GetAll().Where(e => e.Id == input.PriceCategoryId).Select(e => e.PriceCategory).FirstOrDefault() : "";
                leadStatus.Action = "Edit"; 
                leadStatus.LastmodifiedDateTime = DateTime.Now;
                leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                leadStatus.WholeSaleLeadActivityId = leadactid;
                List.Add(leadStatus);
            }

            var ContactIds = await _wholeSaleLeadContactRepository.GetAll().Where(e=> e.WholesaleLeadId == wholeSaleLead.Id).ToListAsync();
            var CurrentContactIds = input.WholeSaleLeadContacts !=null ? input.WholeSaleLeadContacts.Select(e=> e.Id).ToList() : new List<int?>();
            var listcontact = ContactIds.Where(e => !CurrentContactIds.Contains(e.Id));
            foreach (var contact in listcontact)
            {

                    WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        leadStatus.TenantId = (int)AbpSession.TenantId;
                    }
                    leadStatus.FieldName = "Contact Deleted";
                    leadStatus.PrevValue = "";
                    leadStatus.CurValue = contact.Name;
                    leadStatus.Action = "Edit";
                    leadStatus.LastmodifiedDateTime = DateTime.Now;
                    leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                    leadStatus.WholeSaleLeadActivityId = leadactid;
                    List.Add(leadStatus);
                    await _wholeSaleLeadContactRepository.DeleteAsync(contact);
            }
                
            foreach (var contact in input.WholeSaleLeadContacts)
            {
                contact.WholesaleLeadId = wholeSaleLead.Id;
                if ((contact.Id !=null ? contact.Id : 0) == 0)
                {
                    var wholeSaleLeadcontact = ObjectMapper.Map<WholeSaleLeadContact>(contact);
                    
                    WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        leadStatus.TenantId = (int)AbpSession.TenantId;
                    }
                    leadStatus.FieldName = "Contact Added";
                    leadStatus.PrevValue = "";
                    leadStatus.CurValue = contact.Name;
                    leadStatus.Action = "Edit";
                    leadStatus.LastmodifiedDateTime = DateTime.Now;
                    leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                    leadStatus.WholeSaleLeadActivityId = leadactid;
                    List.Add(leadStatus);
                    await _wholeSaleLeadContactRepository.InsertAsync(wholeSaleLeadcontact);
                }

                else
                {
                    var wholeSaleLeadcontact = await _wholeSaleLeadContactRepository.GetAsync((int)contact.Id);
                    if (contact.Name != wholeSaleLeadcontact.Name)
                    {
                        WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                        if (AbpSession.TenantId != null)
                        {
                            leadStatus.TenantId = (int)AbpSession.TenantId;
                        }
                        leadStatus.FieldName = "Contact Name";
                        leadStatus.PrevValue = wholeSaleLeadcontact.Name;
                        leadStatus.CurValue = contact.Name;
                        leadStatus.Action = "Edit";
                        leadStatus.LastmodifiedDateTime = DateTime.Now;
                        leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                        leadStatus.WholeSaleLeadActivityId = leadactid;
                        List.Add(leadStatus);
                    }

                    if (contact.Email != wholeSaleLeadcontact.Email)
                    {
                        WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                        if (AbpSession.TenantId != null)
                        {
                            leadStatus.TenantId = (int)AbpSession.TenantId;
                        }
                        leadStatus.FieldName = "Contact Email";
                        leadStatus.PrevValue = wholeSaleLeadcontact.Email;
                        leadStatus.CurValue = contact.Email;
                        leadStatus.Action = "Edit";
                        leadStatus.LastmodifiedDateTime = DateTime.Now;
                        leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                        leadStatus.WholeSaleLeadActivityId = leadactid;
                        List.Add(leadStatus);
                    }
                    if (contact.Phone != wholeSaleLeadcontact.Phone)
                    {
                        WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                        if (AbpSession.TenantId != null)
                        {
                            leadStatus.TenantId = (int)AbpSession.TenantId;
                        }
                        leadStatus.FieldName = "Contact Phone";
                        leadStatus.PrevValue = wholeSaleLeadcontact.Phone;
                        leadStatus.CurValue = contact.Phone;
                        leadStatus.Action = "Edit";
                        leadStatus.LastmodifiedDateTime = DateTime.Now;
                        leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                        leadStatus.WholeSaleLeadActivityId = leadactid;
                        List.Add(leadStatus);
                    }
                    if (contact.Mobile != wholeSaleLeadcontact.Mobile)
                    {
                        WholeSaleLeadtrackerHistory leadStatus = new WholeSaleLeadtrackerHistory();
                        if (AbpSession.TenantId != null)
                        {
                            leadStatus.TenantId = (int)AbpSession.TenantId;
                        }
                        leadStatus.FieldName = "Contact Mobile";
                        leadStatus.PrevValue = wholeSaleLeadcontact.Mobile;
                        leadStatus.CurValue = contact.Mobile;
                        leadStatus.Action = "Edit";
                        leadStatus.LastmodifiedDateTime = DateTime.Now;
                        leadStatus.WholeSaleLeadId = wholeSaleLead.Id;
                        leadStatus.WholeSaleLeadActivityId = leadactid;
                        List.Add(leadStatus);
                    }
                    

                    ObjectMapper.Map(contact, wholeSaleLeadcontact);

                }

            }

            await _dbcontextprovider.GetDbContext().WholeSaleLeadtrackerHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();


            ObjectMapper.Map(input, wholeSaleLead);
        }

        public async Task<PagedResultDto<GetWholeSaleLeadForViewDto>> GetAll(GetAllWholeSaleLeadInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);
            var user = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(user);

            var actvitylog_list = _leadactivityRepository.GetAll().Where(e => e.SectionId == 35 && e.WholeSaleLeadFk.OrganizationId == input.OrganizationId);

            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.WholeSaleLeadFk.OrganizationId == input.OrganizationId);

            var FollowupList = new List<int>();
            if (input.Datefilter == "Followup" && input.StartDate != null && input.EndDate != null)
            {
                FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date >= SDate.Value.Date && x.CreationTime.Date <= EDate.Value.Date).DistinctBy(e => e.WholeSaleLeadId).Select(x => x.WholeSaleLeadId).ToList();
            }
            else if (input.Datefilter == "Followup" && input.StartDate != null)
            {
                FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date >= SDate.Value.Date).DistinctBy(e => e.WholeSaleLeadId).Select(x => x.WholeSaleLeadId).ToList();
            }
            else if (input.Datefilter == "Followup" && input.EndDate != null)
            {
                FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date <= EDate.Value.Date).DistinctBy(e => e.WholeSaleLeadId).Select(x => x.WholeSaleLeadId).ToList();
            }

            var filteredWholeSale = _wholeSaleLeadRepository.GetAll()
                         //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Talephone.Contains(input.Filter) || e.Mobile.Contains(input.Filter))
                         .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile.Contains(input.Filter))
                        .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Talephone.Contains(input.Filter))
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName.Contains(input.Filter))
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email.Contains(input.Filter))
                        .WhereIf(input.FilterName == "FirstName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.FirstName.Contains(input.Filter))
                        .WhereIf(input.FilterName == "ABNNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ABNNumber == input.Filter)
                        .WhereIf(!string.IsNullOrEmpty(input.Address), e => (e.PostalUnitNo + " " + e.PostalUnitType + " " + e.PostalStreetNo + " " + e.PostalStreetName + " " + e.PostalStreetType).Contains(input.Address))
                        .WhereIf(input.StateId != 0, e => e.PostalStateId == input.StateId)
                        .WhereIf(!string.IsNullOrEmpty(input.PostCode), e => e.PostalPostCode == input.PostCode)
                        .WhereIf(input.Datefilter == "Creation" && input.StartDate !=null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.Datefilter == "Creation" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                        .WhereIf(input.Datefilter == "FirstAssignDate" && input.StartDate != null, e => e.FirstAssignDate >= input.StartDate)
                        .WhereIf(input.Datefilter == "FirstAssignDate" && input.EndDate != null, e => e.FirstAssignDate <= input.EndDate)
                        .WhereIf(role.Contains("Wholesale Manager"), e => e.BDMId == AbpSession.UserId)
                        .WhereIf(role.Contains("Wholesale SalesRep"), e => e.SalesRapId == AbpSession.UserId)
                        .WhereIf(input.Datefilter == "Followup" && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))
                        .WhereIf(input.Datefilter == "NextFollowup" && input.StartDate != null, e => e.ActivityDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.Datefilter == "NextFollowup" && input.EndDate != null, e => e.ActivityDate.Value.Date <= EDate.Value.Date)
                        .WhereIf(input.Datefilter == "NextFollowup" && input.EndDate != null, e => e.ActivityDate.Value.Date <= EDate.Value.Date)
                        .WhereIf(input.SalesMangerId != null && input.SalesMangerId != 0, e => e.BDMId == input.SalesMangerId)
                        .WhereIf(input.SalesRapId != null && input.SalesRapId != 0, e => e.SalesRapId == input.SalesRapId)
                        .WhereIf(input.StatusId != null && input.StatusId != 0, e => e.WholeSaleStatusId == input.StatusId)
                        .WhereIf(input.MyLead == true , e => e.CreatorUserId == AbpSession.UserId)
                        .WhereIf(input.CreatedBy > 0, e => e.CreatorUserId == input.CreatedBy)
                        //.WhereIf(input.IsTransfer == 1 || input.IsTransfer == 2 , e => e.IsTransfer == input.IsTransfer)
                        //.WhereIf(input.IsTransfer == 3 , e => e.IsTransfer == 0 || e.IsTransfer == null)
                        .WhereIf(input.IsTransfer == 1, e => e.IsTransfer > 0)
                        .WhereIf(input.IsTransfer == 2, e => e.IsTransfer == 0 || e.IsTransfer == null)
                        .WhereIf(input.LastActivityDate != null, e => _leadactivityRepository.GetAll().Where(la => la.WholeSaleLeadId == e.Id).Max(e => e.CreationTime).AddHours(diffHour).Date <= input.LastActivityDate.Value.Date)
                        .Where(e => e.OrganizationId == input.OrganizationId);

            var pagedAndFilteredWholeSaleStatus = filteredWholeSale
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var summaryCount = new List<WholeSaleLeadsSummaryCount>();
            var summary = new WholeSaleLeadsSummaryCount();
            summary.Status = "Total";
            summary.totalLead = filteredWholeSale.Count();
            summaryCount.Add(summary);

            summary = new WholeSaleLeadsSummaryCount();
            summary.Status = "Blank";
            summary.totalLead = filteredWholeSale.Where(e => e.WholeSaleStatusId == null).Count();
            summaryCount.Add(summary);

            var statuses = await _wholeSaleStatusRepository.GetAllListAsync();

            foreach(var stat in statuses)
            {
                summary = new WholeSaleLeadsSummaryCount();
                summary.Status = stat.Name;
                summary.totalLead = filteredWholeSale.Where(e => e.WholeSaleStatusId == stat.Id).Count();
                summaryCount.Add(summary);
            }

            //summaryCount.Hot = Convert.ToString(filteredWholeSale.Where(e => e.WholeSaleStatusId == 4).Count());
            //summaryCount.Cold = Convert.ToString(filteredWholeSale.Where(e => e.WholeSaleStatusId == 3).Count());
            //summaryCount.Warm = Convert.ToString(filteredWholeSale.Where(e => e.WholeSaleStatusId == 1).Count());
            //summaryCount.DNC = Convert.ToString(filteredWholeSale.Where(e => e.WholeSaleStatusId == 6).Count());
            //summaryCount.Existing = Convert.ToString(filteredWholeSale.Where(e => e.WholeSaleStatusId == 5).Count());
            //summaryCount.Total = Convert.ToString(filteredWholeSale.Count());

            var wholeSaleStatus = from o in pagedAndFilteredWholeSaleStatus
                                  select new GetWholeSaleLeadForViewDto()
                                  {
                                      createOrEditWholeSaleLeadDto = new CreateOrEditWholeSaleLeadDto()
                                      {
                                          Id = o.Id,
                                          FirstName = o.FirstName,
                                          LastName = o.LastName,
                                          CompanyName = o.CompanyName,
                                          WholeSaleStatus = o.WholeSaleStatusFK.Name,
                                          PostalAddress = o.PostalUnitNo + " " + o.PostalUnitType + " " + o.PostalStreetNo + " " + o.PostalStreetName + " " + o.PostalStreetType,
                                          PostalState = o.PostalStateFK.Name,
                                          DelivaryAddress = o.DelivaryUnitNo + " " + o.DelivaryUnitType + " " + o.DelivaryStreetNo + " " + o.DelivaryStreetName + " " + o.DelivaryStreetType,
                                          DelivaryState = o.DelivaryStateFK.Name,
                                          WholeSaleStatusId = o.WholeSaleStatusId,
                                          BDM = _userRepository.GetAll().Where(e => e.Id == o.BDMId).FirstOrDefault().Name,
                                          SalesRapName = _userRepository.GetAll().Where(e => e.Id == o.SalesRapId).FirstOrDefault().Name,
                                          WholesaleClientId = o.WholesaleClientId,
                                          Email = o.Email,
                                          Mobile = o.Mobile
                                      },
                                      ReminderTime = o.ActivityDate != null ? o.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss") : null,
                                      ActivityDescription = o.ActivityDescription,
                                      ActivityComment = o.ActivityComment,

                                      IsCredit = _businessRepository.GetAll().Where(e => e.WholeSaleLeadId == o.Id).Any(),





                                      //IsCredit = o.Id != null ? (IsCredit == true && IsCredit == false) ;

                                      //if (object.where(o=>o.id==IdToCheck).count()==0)
                                      //  {
                                      //   isCredit = true;
                                      //  }
                                      // else { isCredit = False}
                                      //}


                                      //SummaryCount = summaryCount

                                      //ReminderTime = actvitylog_list.Where(e => e.ActionId == 8 && e.WholeSaleLeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                                      //ActivityDescription = actvitylog_list.Where(e => e.ActionId == 8 && e.WholeSaleLeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                      //ActivityComment = actvitylog_list.Where(e => e.ActionId == 24 && e.WholeSaleLeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                      CommentBy = _userRepository.GetAll().Where(e => e.Id == actvitylog_list.Where(e => e.ActionId == 24 && e.WholeSaleLeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.CreatorUserId).FirstOrDefault()).FirstOrDefault().Name,
                                      Followupby = _userRepository.GetAll().Where(e => e.Id == actvitylog_list.Where(e => e.ActionId == 8 && e.WholeSaleLeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.CreatorUserId).FirstOrDefault()).FirstOrDefault().Name,
                                      Transfer = o.IsTransfer == 1 ? "Trnsfer To BDM" : (o.IsTransfer == 2 ? "Transfer To SalesRap" : "")
                                  };

            var totalCount = await filteredWholeSale.CountAsync();

            var result = await wholeSaleStatus.ToListAsync();

            if (totalCount > 0)
            {
                result[0].SummaryCount = summaryCount;
            }

            return new PagedResultDto<GetWholeSaleLeadForViewDto>(
               totalCount,
               result
           );
        }

        public async Task<CreateOrEditWholeSaleLeadDto> GetWholeSaleLeadForEdit(EntityDto input)
        {
            var wholeSaleLead = await _wholeSaleLeadRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<CreateOrEditWholeSaleLeadDto>(wholeSaleLead);
            var org = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == wholeSaleLead.OrganizationId).FirstOrDefault();

            output.OrgName = org.DisplayName;
            output.OrgMobile = org.Mobile;
            output.OrgEmail = org.Email;
            output.OrgLogo = org.LogoFilePath + org.LogoFileName;

            if(wholeSaleLead.AssignUserId > 0)
            {
                var _lookupUserStatus = await _userRepository.FirstOrDefaultAsync((int)wholeSaleLead.AssignUserId);
                output.AssignUser = _lookupUserStatus.FullName?.ToString();
            }
            if (wholeSaleLead.FirstAssignUserId > 0)
            {
                var _lookupFirstUserStatus = await _userRepository.FirstOrDefaultAsync((int)wholeSaleLead.FirstAssignUserId);
                output.FirstAssignUser = _lookupFirstUserStatus.FullName?.ToString();
            }
            if (wholeSaleLead.SalesRapId > 0)
            {
                var SalesRap = _userRepository.GetAll().Where(e => e.Id == wholeSaleLead.SalesRapId).FirstOrDefault();
                output.SalesRapName = SalesRap?.FullName?.ToString();
                output.SalesRapMobile = SalesRap?.Mobile;
                output.SalesRapEmail = SalesRap?.EmailAddress;
            }

            var contacts = await _wholeSaleLeadContactRepository.GetAll().Where(e => e.WholesaleLeadId == input.Id).ToListAsync();
            output.WholeSaleLeadContacts = new List<WholeSaleLeadContactDto>();
            foreach ( var contact in contacts )
            {
                var wholesaleconctact = new WholeSaleLeadContactDto();
                wholesaleconctact.Id = contact.Id;
                wholesaleconctact.WholesaleLeadId = contact.WholesaleLeadId;
                wholesaleconctact.Name = contact.Name;
                wholesaleconctact.Email = contact.Email;
                wholesaleconctact.Phone = contact.Phone;
                wholesaleconctact.Mobile = contact.Mobile;
                wholesaleconctact.IsWhatsapp = contact.IsWhatsapp;

                output.WholeSaleLeadContacts.Add(wholesaleconctact);
            }

            

            return output;
        }

        public async Task<List<GetWholeSaleLeadForViewDto>> CheckExistWholeSaleLeadsList(CreateOrEditWholeSaleLeadDto input)
        {

            var Mobile = input.Mobile.Substring(input.Mobile.Length - 9);

            var duplicateLead = _wholeSaleLeadRepository.GetAll().Where(e => e.Mobile.Contains(Mobile) || e.Email == input.Email || e.ABNNumber == input.ABNNumber)
                                .Where(e => e.OrganizationId == input.OrganizationId)
                                .WhereIf(input.Id != null, e => e.Id != input.Id);

            var leads = from o in duplicateLead
                        select new GetWholeSaleLeadForViewDto()
                        {
                            createOrEditWholeSaleLeadDto = new CreateOrEditWholeSaleLeadDto()
                            {
                                Id = o.Id,
                                FirstName = o.FirstName,
                                LastName = o.LastName,
                                CompanyName = o.CompanyName,
                                WholeSaleStatus = o.WholeSaleStatusFK.Name,
                                PostalAddress = o.PostalUnitNo + " " + o.PostalUnitType + " " + o.PostalStreetNo + " " + o.PostalStreetName + " " + o.PostalStreetType,
                                PostalState = o.PostalStateFK.Name,
                                DelivaryAddress = o.DelivaryUnitNo + " " + o.DelivaryUnitType + " " + o.DelivaryStreetNo + " " + o.DelivaryStreetName + " " + o.DelivaryStreetType,
                                DelivaryState = o.DelivaryStateFK.Name,
                                Email = o.Email,
                                Mobile = o.Mobile,
                                SalesRapName = o.SalesRapId > 0 ? _userRepository.GetAll().Where(e => e.Id == o.SalesRapId).Select(e => e.FullName).FirstOrDefault() : "",
                                BDM = o.SalesRapId > 0 ? _userRepository.GetAll().Where(e => e.Id == o.BDMId).Select(e => e.FullName).FirstOrDefault() : "",
                                ABNNumber = o.ABNNumber
                            },
                            CurrentAssignUserName = _userRepository.GetAll().Where(e => e.Id == o.AssignUserId).Select(e => e.FullName).FirstOrDefault(),

                        };

            return new List<GetWholeSaleLeadForViewDto>(await leads.ToListAsync());
        }

        public async Task Delete(EntityDto input)
        {
            await _wholeSaleLeadRepository.DeleteAsync(input.Id);

        }

        public async Task<List<int>> GetTotalWholeSaleLeadsCountForWholeSalePromotion(GetAllWholeSaleLeadsInputForWholeSalePromotion input)
        {

            #region New Code By Suresh
            var SDate = (_timeZoneConverter.Convert(input.StartDateFilter, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDateFilter, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var StopResponceList = _wholeSalePromotionUsersRepository.GetAll().Include(e => e.WholeSaleLeadFk)
                .Where(e => e.WholeSalePromotionResponseStatusId == 2 ).Select(e => new { e.WholeSaleLeadId }); //Customer STOP for Promotion
            

            var filteredLeads = _wholeSaleLeadRepository.GetAll().Where(e => StopResponceList.Where(o => (int)o.WholeSaleLeadId == e.Id).Any() == false && e.WholeSaleStatusId != 9)

                            .WhereIf(input.LeadStatusIdsFilter != null && input.LeadStatusIdsFilter.Count() > 0, e => input.LeadStatusIdsFilter.Contains(e.WholeSaleStatusId))

                            .WhereIf(input.StateIdsFilter != null && input.StateIdsFilter.Count() > 0, e => input.StateIdsFilter.Contains(e.PostalStateId))

                            .WhereIf(input.StartDateFilter != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDateFilter != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                            .Where(e => e.OrganizationId == input.OrganizationId);

            var leads = await (from o in filteredLeads select o.Id).ToListAsync();

            var totalCount = filteredLeads.Count();

            return leads;

            #endregion
        }

        public async Task<List<CommonLookupDto>> GetallSMSTemplates(int wholeSaleLeadId)
        {
            var organizationId = _wholeSaleLeadRepository.FirstOrDefault(wholeSaleLeadId).OrganizationId;

            return await _wholeSalesmsTemplateRepository.GetAll().Where(e => e.OrganizationUnitId == organizationId)
            .Select(smsTemplate => new CommonLookupDto
            {
                Id = smsTemplate.Id,
                DisplayName = smsTemplate == null || smsTemplate.Name == null ? "" : smsTemplate.Name.ToString()
            }).ToListAsync();
        }

        public async Task<List<CommonLookupDto>> getOrgWiseDefultandownemailadd(int wholeSaleLeadId)
        {
            var organizationId = _wholeSaleLeadRepository.GetAll().Where(e => e.Id == wholeSaleLeadId).Select(e => e.OrganizationId).FirstOrDefault();
            //var emailadress = new List<string?>();
            var list = new List<CommonLookupDto>();
            var ownemailadd = _UserWiseEmailOrgRepository.GetAll().Where(e => e.OrganizationUnitId == organizationId && e.UserId == AbpSession.UserId).Select(e => e.EmailFromAdress).FirstOrDefault();
            if (ownemailadd != null)
            {
                CommonLookupDto comon = new CommonLookupDto();
                comon.Id = 0;
                comon.DisplayName = ownemailadd;
                list.Add(comon);
            }
            var defultemail = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == organizationId).Select(e => e.defaultFromAddress).FirstOrDefault();

            if (defultemail != null)
            {
                CommonLookupDto comon = new CommonLookupDto();
                comon.Id = 0;
                comon.DisplayName = defultemail;
                list.Add(comon);
            }
            //EmailAddress.add(_organizationUnitRepository.GetAll(e => e.id))
            return list;
        }

        public async Task<List<CommonLookupDto>> GetallEmailTemplates(int wholeSaleLeadId)
        {
            //var organizationId = _leadRepository.GetAll().Where(e => e.Id == leadId).Select(e => e.OrganizationId).FirstOrDefault();
            var organizationId = _wholeSaleLeadRepository.FirstOrDefault(wholeSaleLeadId).OrganizationId;

            return await _emailTemplateRepository.GetAll().Where(e => e.OrganizationUnitId == organizationId)
            .Select(emailTemplate => new CommonLookupDto
            {
                Id = emailTemplate.Id,
                DisplayName = emailTemplate == null || emailTemplate.TemplateName == null ? "" : emailTemplate.TemplateName.ToString()
            }).ToListAsync();
        }

        public async Task<GetWholeSaleEmailTemplateForEditOutput> GetEmailTemplateForEditForEmail(EntityDto input)
        {
            var Email = await _emailTemplateRepository.FirstOrDefaultAsync(input.Id);
            var output = new GetWholeSaleEmailTemplateForEditOutput { EmailTemplateDto = ObjectMapper.Map<CreateOrEditWholeSaleEmailTemplateDto>(Email) };
            return output;
        }

        public async Task<GetWholeSaleSmsTemplateForEditOutput> GetSmsTemplateForEditForSms(EntityDto input)
        {
            var smsTemplate = await _wholeSalesmsTemplateRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetWholeSaleSmsTemplateForEditOutput { wholeSaleSmsTemplate = ObjectMapper.Map<CreateOrEditWholeSaleSmsTemplateDto>(smsTemplate) };

            return output;
        }

        public async Task SaveWholeSaleDocument(string FileToken, string FileName, int id, int? docType, int sectionId)
        {
            var fileBytes = _tempFileCacheManager.GetFile(FileToken);

            var ext = Path.GetExtension(FileName);


            string FileNames = DateTime.Now.Ticks + "_" + "WholeSaleLead" + ext;


            byte[] ByteArray = _tempFileCacheManager.GetFile(FileToken);
            var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(fileBytes, FileNames, "WholeSaleLead", 0, 0, AbpSession.TenantId, id);

            WholeSaleLeadDoc wholeSaleLeadDocs = new WholeSaleLeadDoc();
            wholeSaleLeadDocs.FileName = FileNames;
            wholeSaleLeadDocs.FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\WholeSaleLead\\";
            wholeSaleLeadDocs.WholeSaleLeadId = id;
            wholeSaleLeadDocs.TenantId = (int)AbpSession.TenantId;
            wholeSaleLeadDocs.WholeSaleLeadDocumentTypeId = docType;
            await _wholeSaleLeadDocRepository.InsertAsync(wholeSaleLeadDocs);

            WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
            leadactivity.ActionId = 56;
            leadactivity.SectionId = sectionId;
            leadactivity.ActionNote = "WholeSaleLead Document Added " + FileNames;
            leadactivity.WholeSaleLeadId = id;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);

        }

        public async Task<List<GetWholeSaleLeadDoc>> GetallWholeSaledoc(int? id)
        {
            var servicess = (from o in _wholeSaleLeadDocRepository.GetAll().Where(e => e.WholeSaleLeadId == id)
                             select new GetWholeSaleLeadDoc
                             {
                                 Id = o.Id,
                                 WholeSaleLeadId = o.WholeSaleLeadId,
                                 FileName = o.FileName,
                                 FilePath = o.FilePath,
                                 CreationTime = o.CreationTime,
                                 CreationUser = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                                 DocTitle = o.WholeSaleDocumentTypeFk.Title
                             }).ToList();

            return servicess;
        }

        public async Task DeleteWholeSaleDoc(int? id, int wholeSaleLeadId, int sectionId)
        {
            var docs = _wholeSaleLeadDocRepository.GetAll().Where(e => e.Id == id).FirstOrDefault();

            await _wholeSaleLeadDocRepository.DeleteAsync((int)id);

            WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
            leadactivity.ActionId = 57;
            leadactivity.SectionId = sectionId;
            leadactivity.ActionNote = "WholeSaleLead Document Deleted " + docs.FileName;
            leadactivity.WholeSaleLeadId = wholeSaleLeadId;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);

            var Path = ApplicationSettingConsts.DocumentPath;
            
            var fileSavePath = Path + docs.FilePath + docs.FileName;

            if (System.IO.File.Exists(fileSavePath))
            {
                try
                {
                    System.IO.File.Delete(fileSavePath);
                }
                catch (Exception ex)
                {
                    Exception e;
                }
            }
           
        }

        public async Task<GetWholeSaleLeadForViewDto> GetWholeSaleLeadForView(int id)
        {
            var lead = await _wholeSaleLeadRepository.GetAsync(id);

            var output = new GetWholeSaleLeadForViewDto { createOrEditWholeSaleLeadDto = ObjectMapper.Map<CreateOrEditWholeSaleLeadDto>(lead) };

            var user = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //var job_data = _jobRepository.GetAll().Where(e => e.LeadId == id).FirstOrDefault();
            //var jobwarranty = _JobwarrantyRepository.GetAll().Where(e => e.JobId == job_data.Id);
            var org = _extendedOrganizationUnitRepository.GetAll();
            //var reviewType = _reviewTypeRepository.GetAll();

            output.createOrEditWholeSaleLeadDto.Id = lead.Id;
            output.createOrEditWholeSaleLeadDto.CompanyName = lead.CompanyName;
            output.createOrEditWholeSaleLeadDto.FirstName = lead.FirstName;
            output.createOrEditWholeSaleLeadDto.LastName = lead.LastName;

            if (output.createOrEditWholeSaleLeadDto.WholeSaleStatusId != null)
            {
                var _lookupLeadStatus = await _wholeSaleStatusRepository.FirstOrDefaultAsync((int)output.createOrEditWholeSaleLeadDto.WholeSaleStatusId);
                output.LeadStatusName = _lookupLeadStatus?.Name?.ToString();
            }

            if (lead.CreatorUserId != null)
            {
                var _lookupUserStatus = await _userRepository.FirstOrDefaultAsync((int)lead.CreatorUserId);
                output.CreatedByName = _lookupUserStatus?.FullName?.ToString();
            }

            if (lead.AssignUserId != null)
            { 
                var _lookupUserStatus = await _userRepository.FirstOrDefaultAsync((int)lead.SalesRapId);
                output.CurrentAssignUserName = _lookupUserStatus?.FullName?.ToString();
                output.CurrentAssignUserEmail = _lookupUserStatus?.EmailAddress?.ToString();
                output.CurrentAssignUserMobile = _lookupUserStatus?.PhoneNumber?.ToString();
            }
            if (lead.SalesRapId == null)
            {
                var _lookupUserStatus = await _userRepository.FirstOrDefaultAsync((int)lead.AssignUserId);
                output.CurrentAssignUserName = _lookupUserStatus?.FullName?.ToString();
                output.CurrentAssignUserEmail = _lookupUserStatus?.EmailAddress?.ToString();
                output.CurrentAssignUserMobile = _lookupUserStatus?.PhoneNumber?.ToString();
            }
            else
            {
                var DupLead = _wholeSaleLeadRepository.GetAll().Where(e => (e.Email == lead.Email || e.Mobile == lead.Mobile) && e.Id != lead.Id && e.OrganizationId == lead.OrganizationId && e.AssignUserId != null).FirstOrDefault();
                if (DupLead != null)
                {
                    var _lookupUserStatus = await _userRepository.FirstOrDefaultAsync((int)DupLead.SalesRapId);
                    output.CurrentAssignUserName = _lookupUserStatus?.FullName?.ToString();
                }
            }
            output.PostalState = lead.PostalStateId > 0 ? _stateRepository.GetAll().Where(e => e.Id ==lead.PostalStateId).FirstOrDefault().Name : "";
            output.CreatedOn = lead.CreationTime;
            output.latitude = lead.PostalLatitude;
            output.longitude = lead.PostalLongitude;
            output.UserName = user.Name;
            output.UserEmail = user.EmailAddress;
            output.UserPhone = user.PhoneNumber;
            output.OrgName = org.Where(e => e.Id == lead.OrganizationId).Select(e => e.DisplayName).FirstOrDefault();
            output.OrgMobile = org.Where(e => e.Id == lead.OrganizationId).Select(e => e.Mobile).FirstOrDefault();
            output.OrgEmail = org.Where(e => e.Id == lead.OrganizationId).Select(e => e.Email).FirstOrDefault();
            output.OrgLogo = org.Where(e => e.Id == lead.OrganizationId).Select(e => (e.LogoFilePath + e.LogoFileName)).FirstOrDefault();

            output.OrgLogo = output.OrgLogo != null ? output.OrgLogo.Replace("\\", "/") : "";

            var contacts = await _wholeSaleLeadContactRepository.GetAll().Where(e => e.WholesaleLeadId == lead.Id).ToListAsync();
            output.createOrEditWholeSaleLeadDto.WholeSaleLeadContacts = new List<WholeSaleLeadContactDto>();
            foreach (var contact in contacts)
            {
                var wholesaleconctact = new WholeSaleLeadContactDto();
                wholesaleconctact.Id = contact.Id;
                wholesaleconctact.WholesaleLeadId = contact.WholesaleLeadId;
                wholesaleconctact.Name = contact.Name;
                wholesaleconctact.Email = contact.Email;
                wholesaleconctact.Phone = contact.Phone;
                wholesaleconctact.Mobile = contact.Mobile;
                wholesaleconctact.IsWhatsapp = contact.IsWhatsapp;

                output.createOrEditWholeSaleLeadDto.WholeSaleLeadContacts.Add(wholesaleconctact);
            }

            if (lead.AssignUserId > 0)
            {
                var _lookupUserStatus = await _userRepository.FirstOrDefaultAsync((int)lead.AssignUserId);
                output.createOrEditWholeSaleLeadDto.AssignUser = _lookupUserStatus.FullName?.ToString();
            }
            if (lead.FirstAssignUserId > 0)
            {
                var _lookupFirstUserStatus = await _userRepository.FirstOrDefaultAsync((int)lead.FirstAssignUserId);
                output.createOrEditWholeSaleLeadDto.FirstAssignUser = _lookupFirstUserStatus.FullName?.ToString();
            }
            if (lead.SalesRapId > 0)
            {
                var SalesRap = _userRepository.GetAll().Where(e => e.Id == lead.SalesRapId).FirstOrDefault();
                output.createOrEditWholeSaleLeadDto.SalesRapName = SalesRap?.FullName?.ToString();
                output.createOrEditWholeSaleLeadDto.SalesRapMobile = SalesRap?.Mobile;
                output.createOrEditWholeSaleLeadDto.SalesRapEmail = SalesRap?.EmailAddress;
            }
            if(lead.BDMId > 0)
            {
                var bdm = _userRepository.GetAll().Where(e => e.Id == lead.BDMId).FirstOrDefault();
                output.createOrEditWholeSaleLeadDto.BDM = bdm?.FullName?.ToString();
            }

            output.createOrEditWholeSaleLeadDto.PriceCategory = lead.PriceCategoryFK != null ? lead.PriceCategoryFK.PriceCategory : "";

            output.createOrEditWholeSaleLeadDto.PostalAddress = lead.PostalUnitNo + " " + lead.PostalUnitType + " " + lead.PostalStreetNo + " " + lead.PostalStreetName + " " + lead.PostalStreetType;
            output.createOrEditWholeSaleLeadDto.PostalState = lead.PostalStateFK != null? lead.PostalStateFK.Name : "";
            output.createOrEditWholeSaleLeadDto.DelivaryAddress = lead.DelivaryUnitNo + " " + lead.DelivaryUnitType + " " + lead.DelivaryStreetNo + " " + lead.DelivaryStreetName + " " + lead.DelivaryStreetType;
            output.createOrEditWholeSaleLeadDto.DelivaryState = lead.DelivaryStateFK != null ? lead.DelivaryStateFK.Name : "";

            output.createOrEditWholeSaleLeadDto.WholeSaleStatus = lead.WholeSaleStatusFK != null ? lead.WholeSaleStatusFK.Name : "";
            output.createOrEditWholeSaleLeadDto.TradinSTCWhom = lead.TradingSTCWhomFK != null ? lead.TradingSTCWhomFK.Name : "";

            return output;
        }

        public async Task<List<GetActivityLogViewDto>> GetWholeSaleLeadActivityLog(GetActivityLogInput input)
        {

            List<int> leadActionids = new List<int>() { 53,54,56,57 };
            List<int> smsActionids = new List<int>() { 6, 20 };
            List<int> promotionActionids = new List<int>() { 51,52 };
            //List<int> serviceids = new List<int>() { 27, 29, 30 };
            //List<int> warrantyCliamids = new List<int>() { 45, 46, 47, 48, 49, 50 };
            input.ServiceId = input.ServiceId != null ? input.ServiceId : 0;

            var allactionids = _lookup_leadActionRepository.GetAll().Select(e => e.Id).ToList();
            
            var Result = _leadactivityRepository.GetAll()
                .Include(e => e.WholeSaleLeadFk).Include(e => e.LeadActionIdFk)
                .Where(L => L.WholeSaleLeadId == input.LeadId)
                .WhereIf(input.ActionId == 0, L => allactionids.Contains(L.ActionId))
                .WhereIf(input.ActionId == 1, L => leadActionids.Contains(L.ActionId))
                .WhereIf(input.ActionId == 6, L => smsActionids.Contains(L.ActionId))
                .WhereIf(input.ActionId == 7, L => L.ActionId == 7)
                .WhereIf(input.ActionId == 8, L => L.ActionId == 8)
                .WhereIf(input.ActionId == 9, L => L.ActionId == 9)
                .WhereIf(input.ActionId == 11, L => promotionActionids.Contains(L.ActionId))
                .WhereIf(input.ActionId == 24, L => L.ActionId == 24)
                .WhereIf(input.ActionId == 28, L => L.ActionId == 28)
                .WhereIf(input.CurrentPage != false, L => L.SectionId == input.SectionId)
                //.WhereIf(input.OnlyMy != false, L => L.CreatorUserId == AbpSession.UserId)
                //.WhereIf(input.ServiceId > 0, L => L. == input.ServiceId)
                //.WhereIf(input.ServiceId == 0, L => L.ServiceId == null)
                ;

            if (!input.AllActivity)
            {
                Result = Result.OrderByDescending(e => e.Id).Take(10);
            }
            else
            {
                Result = Result.OrderByDescending(e => e.Id);
            }

            var leads = from o in Result
                            //join o1 in _lookup_leadActionRepository.GetAll() on o.ActionId equals o1.Id into j1
                            //from s1 in j1.DefaultIfEmpty()

                            //join o2 in _leadRepository.GetAll() on o.LeadId equals o2.Id into j2
                            //from s2 in j2.DefaultIfEmpty()

                        join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                        from s3 in j3.DefaultIfEmpty()

                        select new GetActivityLogViewDto()
                        {
                            Id = o.Id,
                            //ActionName = s1 == null || s1.ActionName == null ? "" : s1.ActionName.ToString(),
                            ActionName = o.LeadActionIdFk.ActionName,
                            ActionId = o.ActionId,
                            ActionNote = o.ActionNote,
                            ActivityNote = o.ActionId == 6 || o.ActionId == 7 ? o.Body : o.ActivityNote,
                            LeadId = o.WholeSaleLeadId,
                            CreationTime = o.CreationTime,
                            ActivityDate = o.ActivityDate,
                            Section = _sectionRepository.GetAll().Where(e => e.SectionId == o.SectionId).Select(e => e.SectionName).FirstOrDefault(),
                            ShowLeadJobDetail = o.ActionId == 54 ? 2 : (o.ActionId == 52 || o.ActionId == 63 ? 3 : ( o.ActionId == 7 ? 4 : 0)) ,
                            //ShowJobLink = link == true ? 1 : 2,
                            ActivitysDate = o.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss"),
                            CreatorUserName = s3 == null || s3.FullName == null ? "" : s3.FullName.ToString(),
                            //LeadCompanyName = s2 == null || s2.CompanyName == null ? "" : s2.CompanyName.ToString()
                            LeadCompanyName = o.WholeSaleLeadFk.CompanyName,
                            promotionId = o.PromotionId
                        };

            return new List<GetActivityLogViewDto>(
                   await leads.ToListAsync()
               );
        }

        public virtual async Task ChangeStatus(GetLeadForChangeStatusOutput input)
        {
            var lead = await _wholeSaleLeadRepository.FirstOrDefaultAsync((int)input.Id);

            //PreviousJobStatus jobStstus = new PreviousJobStatus();
            //jobStstus.LeadId = lead.Id;
            //if (AbpSession.TenantId != null)
            //{
            //    jobStstus.TenantId = (int)AbpSession.TenantId;
            //}
            //jobStstus.CurrentID = (int)input.LeadStatusID;
            //jobStstus.PreviousId = (int)lead.WholeSaleStatusId;
            //await _previousJobStatusRepository.InsertAsync(jobStstus);

            //lead.CancelReasonId = input.CancelReasonId;
            //lead.RejectReasonId = input.RejectReasonId;
            //lead.LeadStatusId = (int)input.LeadStatusID;
            //lead.ChangeStatusDate = DateTime.UtcNow;
            //if (!input.RejectReason)
            //{
            //    lead.RejectReason = input.RejectReason;
            //}


            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
            leadactivity.ActionId = 5;
            leadactivity.SectionId = 0;
            if (input.LeadStatusID == 7)
            {
                leadactivity.ActionNote = "Lead Rejected, Reason : " + input.RejectReason;
            }
            else
            {
                leadactivity.ActionNote = "Lead Status Changed";
            }

            leadactivity.WholeSaleLeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);



            lead.WholeSaleStatusId = input.LeadStatusID;
            await _wholeSaleLeadRepository.UpdateAsync(lead);



        }

        public async Task<List<LeadtrackerHistoryDto>> GetWholeSaleLeadActivityLogHistory(GetActivityLogInput input)
        {
            try
            {
                var Result = (from item in _wholeSaleLeadtrackerHistoryRepository.GetAll()
                              join ur in _userRepository.GetAll() on item.CreatorUserId equals ur.Id into urjoined
                              from ur in urjoined.DefaultIfEmpty()
                              where (item.WholeSaleLeadActivityId == input.LeadId)
                              select new LeadtrackerHistoryDto()
                              {
                                  Id = item.Id,
                                  FieldName = item.FieldName,
                                  PrevValue = item.PrevValue,
                                  CurValue = item.CurValue,
                                  Lastmodifiedbyuser = ur.Name + " " + ur.Surname
                              })
                    .OrderByDescending(e => e.Id).ToList();
                return Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task AsssignOrTransferLeads(AssignOrDeleteWholeSaleLeadInput input)
        {
            var ChangedOrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationID).Select(e => e.DisplayName).FirstOrDefault();
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == input.AssignToUserID).FirstOrDefault();

            foreach (var item in input.WholeSaleLeadIds)
            {
                var wholeSaleLead = await _wholeSaleLeadRepository.FirstOrDefaultAsync((int)item);

                var ActionNote = "";
                var ActionId = 0;

                var assignedFromUser = _userRepository.GetAll().Include(e => e.Roles).Where(u => u.Id == wholeSaleLead.AssignUserId).FirstOrDefault();
                var PreviousOrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == wholeSaleLead.OrganizationId).Select(e => e.DisplayName).FirstOrDefault();

                IList<string> RoleName = await _userManager.GetRolesAsync(assignedFromUser);

                if (input.LeadActionId == 1) // Assign To Sales Manager 
                {
                    if (wholeSaleLead.OrganizationId != input.OrganizationID)
                    {
                        ActionNote = "Lead " + (RoleName.Contains("Wholesale SalesRep") ? "ReAssign" : "Assign") + " To Sales Manager " + assignedToUser.FullName + " In " + ChangedOrganizationName + " Organization From " + assignedFromUser.FullName + ", " + PreviousOrganizationName + " Organization";
                    }
                    else
                    {
                        ActionNote = "Lead " + (RoleName.Contains("Wholesale SalesRep") ? "ReAssign" : "Assign") + " To Sales Manager " + assignedToUser.FullName + " From " + assignedFromUser.FullName;
                    }

                    if (RoleName.Contains("Wholesale SalesRep"))
                    {
                        ActionId = 60;
                        wholeSaleLead.ReAssignDate = DateTime.UtcNow;
                    }
                    else
                    {
                        ActionId = 59;
                        wholeSaleLead.AssignDate = DateTime.UtcNow;
                    }
                    wholeSaleLead.AssignUserId = input.AssignToUserID;


                }
                else if (input.LeadActionId == 2) // Assign To Sales Rep
                {
                    if (wholeSaleLead.OrganizationId != input.OrganizationID)
                    {
                        ActionNote = "Wholesale Lead " + (RoleName.Contains("Wholesale SalesRepp") ? "ReAssign" : "Assign") + " To Sales Rep " + assignedToUser.FullName + " In " + ChangedOrganizationName + " Organization From " + assignedFromUser.FullName + ", " + PreviousOrganizationName + " Organization";
                    }
                    else
                    {
                        ActionNote = "Wholesale Lead " + (RoleName.Contains("Wholesale SalesRep") ? "ReAssign" : "Assign") + " To Sales Rep " + assignedToUser.FullName + " From " + assignedFromUser.FullName;
                    }

                    if (RoleName.Contains("Wholesale SalesRep"))
                    {
                        ActionId = 60;
                        wholeSaleLead.ReAssignDate = DateTime.UtcNow;
                    }
                    else
                    {
                        ActionId = 59;
                        wholeSaleLead.AssignDate = DateTime.UtcNow;

                        if (wholeSaleLead.FirstAssignUserId == null)
                        {
                            wholeSaleLead.FirstAssignDate = DateTime.UtcNow;
                            wholeSaleLead.FirstAssignUserId = input.AssignToUserID;
                        }
                    }
                    wholeSaleLead.AssignUserId = input.AssignToUserID;

                }
                else if (input.LeadActionId == 4) // Transfer To BDM
                {
                    var PrevBDM = _userRepository.GetAll().Include(e => e.Roles).Where(u => u.Id == wholeSaleLead.BDMId).FirstOrDefault();

                    ActionNote = "Wholesale Lead Transfer To BDM " +  assignedToUser.FullName + ((wholeSaleLead.BDMId > 0) != null ? " From " + PrevBDM.FullName : "");
                    ActionId = 66;
                    wholeSaleLead.BDMId = input.AssignToUserID;
                    wholeSaleLead.IsTransfer = 1;

                }
                else if (input.LeadActionId == 5) // Transfer To Sales Rep
                {
                    var PrevSalesRap = _userRepository.GetAll().Include(e => e.Roles).Where(u => u.Id == wholeSaleLead.SalesRapId).FirstOrDefault();
                     ActionNote = "Wholesale Lead Transfer To Sales Rep " +  assignedToUser.FullName + " From " + PrevSalesRap.FullName;
                    ActionId = 67;
                    wholeSaleLead.SalesRapId = input.AssignToUserID;
                    wholeSaleLead.IsTransfer = 2;

                }

                wholeSaleLead.OrganizationId = (int)input.OrganizationID;
                await _wholeSaleLeadRepository.UpdateAsync(wholeSaleLead);

                if (input.LeadActionId == 3)
                {
                    ActionId = 61;

                    ActionNote = "Wholesale Lead Deleted ";
                    await _wholeSaleLeadRepository.DeleteAsync(item);
                }

                WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
                leadactivity.ActionId = ActionId;
                leadactivity.SectionId = input.SectionId;
                leadactivity.ActionNote = ActionNote;
                leadactivity.WholeSaleLeadId = wholeSaleLead.Id;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                leadactivity.CreatorUserId = AbpSession.UserId;
                await _leadactivityRepository.InsertAsync(leadactivity);
            }

            if(input.LeadActionId == 1 || input.LeadActionId == 2)
            {
                var strmsg = "WholeSale Lead Assigned To You";
                string msg = string.Format("{0} " + strmsg, input.WholeSaleLeadIds.Count);
                await _appNotifier.LeadAssiged(assignedToUser, msg, NotificationSeverity.Info);
            }


        }

        public async Task<List<CommonLookupDto>> GetallWholeSaleDocument()
        {
            return await _wholeSaleLeadDocumentTypeRepository.GetAll()
            .Select(e => new CommonLookupDto
            {
                Id = e.Id,
                DisplayName = e.Title
            }).ToListAsync();
        }

        public async Task<List<GetAllWholeSaleLeadByLeadStatusOutput>> GetallWholeSaleDashboard()
        {
            var wholesaleStatus = _wholeSaleStatusRepository.GetAll();

            var wholesaleLeads = _wholeSaleLeadRepository.GetAll();

            var result = from o in wholesaleStatus
                         select new GetAllWholeSaleLeadByLeadStatusOutput
                         {
                             Id = o.Id,
                             Status = o.Name,
                             WholeSaleLeads = (from e in wholesaleLeads.Where(e => e.WholeSaleStatusId == o.Id)
                                               select new CreateOrEditWholeSaleLeadDto
                                               {
                                                   FirstName = e.FirstName,
                                                   LastName = e.LastName,
                                                   CreateUserName = _userRepository.GetAll().Where(u => u.Id == e.CreatorUserId).FirstOrDefault().FullName,


                                               }).ToList()
                         };

            return result.ToList();

        }

        public async Task<string> AddtoUserRequest(int leadId)
        {
            var wholesaleLead = await _wholeSaleLeadRepository.FirstOrDefaultAsync(leadId);
            var existsUser = _ecommerceUserRegisterRequestRepository.GetAll().Any(e => e.Email == wholesaleLead.Email);

            if (!existsUser)
            {
                var UserName = wholesaleLead.Email.Split("@")[0];
                var Password = await _userManager.CreateRandomPassword();

                var user = new EcommerceUserRegisterRequest();
                user.Email = wholesaleLead.Email;
                user.FirstName = wholesaleLead.FirstName;
                user.LastName = wholesaleLead.LastName;
                user.IsApproved = true;
                user.UserName = UserName;
                user.Password = Password;
                var clientid = await _ecommerceUserRegisterRequestRepository.InsertAndGetIdAsync(user);

                wholesaleLead.WholesaleClientId = clientid;

                await _wholeSaleLeadRepository.UpdateAsync(wholesaleLead);

                var Body = "http://achievers.thesolarproduct.com/profile-authentication";
                var OrgLogo = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == wholesaleLead.OrganizationId).Select(e => (e.LogoFilePath + e.LogoFileName)).FirstOrDefault();
                OrgLogo = ApplicationSettingConsts.ViewDocumentPath + (OrgLogo != null ? OrgLogo.Replace("\\", "/") : "");
                var FromEmail = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == wholesaleLead.OrganizationId).Select(e => e.defaultFromAddress).FirstOrDefault();
                var FinalBody = "<!doctype html><html lang='en'><head><meta charset='utf-8'><meta name='viewport' content='width=device-width,initial-scale=1,shrink-to-fit=no'><meta name='viewport' content='width=device-width,initial-scale=1,shrink-to-fit=no'><link rel='preconnect' href='https://fonts.googleapis.com'><link rel='preconnect' href='https://fonts.gstatic.com' crossorigin><link href='https://fonts.googleapis.com/css2?family=Roboto&display=swap' rel='stylesheet'><title>Inline CSS Email</title></head><style>@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu72xKOzY.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu5mxKOzY.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-stle:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7mxKOzY.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu4WxKOzY.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7WxKOzY.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7GxKOzY.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(http://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu4mxK.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;rc:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}body{margin:0;padding:0;background:#48afb5;font-family:Roboto}</style><body style='background:#48afb5;margin:0;padding:0'><div style='background:#48afb5;margin:0;padding:20px 0'><div style='margin:10px auto;max-width:600px;min-width:320px'><div style='margin:0 10px;max-width:600px;min-width:320px;padding:0;background-color:#fff;border:1px solid #e2ebf1;border-radius:10px;overflow:hidden'><table style='width:100%;border:0;border-spacing:0;margin:0 auto'><tr><td style='text-align:center;padding:30px 0 20px'><img src='" + OrgLogo + "' style='max-width:200px'></td></tr><tr><td style='font-size:22px;font-weight:700;font-family:Roboto,sans-serif;color:#000;padding:0 30px;text-align:center;font-weight:900'>Dear " + user.FirstName + " " + user.LastName + ",</td></tr><tr><td style='height:30px'></td></tr><tr><td style='text-align:center;font-size:14px;font-weight:400;font-family:Roboto,sans-serif;color:#1a1a1a;padding:0 0'><div style='font-size:1.1rem;font-family:Roboto,sans-serif;color:#1a1a1a;margin:0 25px 30px;line-height:28px'><br> Your User Request Is Approved.</div><div> UserName : " + user.UserName + " <br/> PassWord : " + user.Password + "</div><div style='padding:20px 0'><div style='font-size:.9rem;font-weight:400;font-family:Roboto,sans-serif;margin-bottom:30px;line-height:24px'></div><div style='font-size:14px;font-weight:400;font-family:Roboto,sans-serif;line-height:24px'><a href='" + Body + "' style='text-decoration:none;text-decoration:none;background:#48afb5;font-weight:700;color:#fff;letter-spacing:1px;font-size:16px;text-transform:uppercase;border-radius:25px;height:50px;width:320px;display:inline-block;line-height:50px'>Click here to Login</a></div></div></td></tr></table></div></div></div></body></html>";

                await _emailSender.SendAsync(new MailMessage
                {
                    From = new MailAddress(FromEmail),
                    To = { wholesaleLead.Email },
                    Subject = "Your User Requset is Approved",
                    Body = FinalBody,
                    IsBodyHtml = true
                });

                return "Success";
            }
            else
            {
                return "Email Exists";
            }


        }

        public async Task<List<GetWholeSaleLeadHeaderSerachDto>> GetWholeSaleLeadSearchFilter(string str, string filterName)
        {
            TextInfo textInfo = new CultureInfo("en-GB", false).TextInfo;

            var orgid = _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == AbpSession.UserId).Select(e => e.OrganizationUnitId).ToList();

            var leads = _wholeSaleLeadRepository.GetAll()
                .Where(e => orgid.Contains(e.OrganizationId) && e.AssignUserId != null)
                .WhereIf(filterName == "AbnNumber", e => e.ABNNumber == str)
                .WhereIf(filterName == "MobileNo", e => e.Mobile == str || e.Talephone == str)
                .WhereIf(filterName == "CompanyName", e => e.CompanyName.Contains(str))
                .WhereIf(filterName == "Email", e => e.Email == str)
                .WhereIf(filterName == "PhoneNo", e => e.Talephone == str);

            var listOfResult = (from lead in leads
                                       

                                let companyName = (string.IsNullOrEmpty(lead.CompanyName) ? "" : Convert.ToString(lead.CompanyName))
                                let customerName = (string.IsNullOrEmpty(lead.FirstName) && (string.IsNullOrEmpty(lead.LastName)) ? "" : Convert.ToString(lead.FirstName)+ " " + Convert.ToString(lead.LastName))
                                let ABNNumber = (string.IsNullOrEmpty(lead.ABNNumber) ? "" : Convert.ToString(lead.ABNNumber))
                                let FullAddress = (lead.PostalUnitNo + " " + lead.PostalUnitType + " " + lead.PostalStreetNo + " " + lead.PostalStreetName + " " + lead.PostalStreetType).Trim().ToLower()

                                select new GetWholeSaleLeadHeaderSerachDto
                                {
                                    Id = lead.Id,
                                    CustomerName = string.IsNullOrEmpty(customerName) ? "" : textInfo.ToTitleCase(customerName.Trim().ToLower()),
                                    CompanyName = string.IsNullOrEmpty(companyName) ? "" : textInfo.ToTitleCase(companyName.Trim().ToLower()),
                                    ABNNumber = ABNNumber,
                                    Mobile = lead.Mobile,
                                    Email = lead.Email,
                                    //Address = textInfo.ToTitleCase((Address + Suburb + State + PostalCode).ToString().Trim().ToLower()),
                                    Address = string.IsNullOrEmpty(FullAddress) ? "" : textInfo.ToTitleCase(FullAddress),
                                    //SalesRep = ur.FullName
                                });

            return await listOfResult.ToListAsync();
        }

        public async Task<PagedResultDto<GetWholeSaleLeadForViewDto>> GetAllWholeSaleLeadSearch(int Id)
        {

            var filteredWholeSale = _wholeSaleLeadRepository.GetAll().Where(e => e.Id == Id).Select(e => new {e.Id, e.CompanyName, e.FirstName, e.LastName, e.WholeSaleStatusFK, e.CreationTime, e.ABNNumber});
                         //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Talephone.Contains(input.Filter) || e.Mobile.Contains(input.Filter))
                         

            //var pagedAndFilteredWholeSaleStatus = filteredWholeSale
            //    .OrderBy(input.Sorting ?? "id desc")
            //    .PageBy(input);

            //var summaryCount = new List<WholeSaleLeadsSummaryCount>();
            //var summary = new WholeSaleLeadsSummaryCount();
            //summary.Status = "Total";
            //summary.totalLead = filteredWholeSale.Count();
            //summaryCount.Add(summary);

            //summary = new WholeSaleLeadsSummaryCount();
            //summary.Status = "Blank";
            //summary.totalLead = filteredWholeSale.Where(e => e.WholeSaleStatusId == null).Count();
            //summaryCount.Add(summary);

            //var statuses = await _wholeSaleStatusRepository.GetAllListAsync();

            //foreach (var stat in statuses)
            //{
            //    summary = new WholeSaleLeadsSummaryCount();
            //    summary.Status = stat.Name;
            //    summary.totalLead = filteredWholeSale.Where(e => e.WholeSaleStatusId == stat.Id).Count();
            //    summaryCount.Add(summary);
            //}

            //summaryCount.Hot = Convert.ToString(filteredWholeSale.Where(e => e.WholeSaleStatusId == 4).Count());
            //summaryCount.Cold = Convert.ToString(filteredWholeSale.Where(e => e.WholeSaleStatusId == 3).Count());
            //summaryCount.Warm = Convert.ToString(filteredWholeSale.Where(e => e.WholeSaleStatusId == 1).Count());
            //summaryCount.DNC = Convert.ToString(filteredWholeSale.Where(e => e.WholeSaleStatusId == 6).Count());
            //summaryCount.Existing = Convert.ToString(filteredWholeSale.Where(e => e.WholeSaleStatusId == 5).Count());
            //summaryCount.Total = Convert.ToString(filteredWholeSale.Count());

            var wholeSaleStatus = from o in filteredWholeSale
                                  select new GetWholeSaleLeadForViewDto()
                                  {
                                      createOrEditWholeSaleLeadDto = new CreateOrEditWholeSaleLeadDto()
                                      {
                                          Id = o.Id,
                                          FirstName = o.FirstName,
                                          LastName = o.LastName,
                                          CompanyName = o.CompanyName,
                                          WholeSaleStatus = o.WholeSaleStatusFK.Name,
                                          //PostalAddress = o.PostalUnitNo + " " + o.PostalUnitType + " " + o.PostalStreetNo + " " + o.PostalStreetName + " " + o.PostalStreetType,
                                          //PostalState = o.PostalStateFK.Name,
                                          //DelivaryAddress = o.DelivaryUnitNo + " " + o.DelivaryUnitType + " " + o.DelivaryStreetNo + " " + o.DelivaryStreetName + " " + o.DelivaryStreetType,
                                          //DelivaryState = o.DelivaryStateFK.Name,
                                          //WholeSaleStatusId = o.WholeSaleStatusId,
                                          //BDM = _userRepository.GetAll().Where(e => e.Id == o.BDMId).FirstOrDefault().Name,
                                          //SalesRapName = _userRepository.GetAll().Where(e => e.Id == o.SalesRapId).FirstOrDefault().Name,
                                          //WholesaleClientId = o.WholesaleClientId,
                                          createTime = o.CreationTime.ToString("dd-MM-yyyy hh:mm:ss"),
                                          ABNNumber = o.ABNNumber
                                      },
                                      //ReminderTime = o.ActivityDate != null ? o.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss") : null,
                                      //ActivityDescription = o.ActivityDescription,
                                      //ActivityComment = o.ActivityComment,

                                      //IsCredit = _businessRepository.GetAll().Where(e => e.WholeSaleLeadId == o.Id).Any()





                                      //IsCredit = o.Id != null ? (IsCredit == true && IsCredit == false) ;

                                      //if (object.where(o=>o.id==IdToCheck).count()==0)
                                      //  {
                                      //   isCredit = true;
                                      //  }
                                      // else { isCredit = False}
                                      //}


                                      //SummaryCount = summaryCount

                                      //ReminderTime = actvitylog_list.Where(e => e.ActionId == 8 && e.WholeSaleLeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                                      //ActivityDescription = actvitylog_list.Where(e => e.ActionId == 8 && e.WholeSaleLeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                      //ActivityComment = actvitylog_list.Where(e => e.ActionId == 24 && e.WholeSaleLeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                  };

            var totalCount = await filteredWholeSale.CountAsync();

            var result = await wholeSaleStatus.ToListAsync();

            //if (totalCount > 0)
            //{
            //    result[0].SummaryCount = summaryCount;
            //}

            return new PagedResultDto<GetWholeSaleLeadForViewDto>(
               totalCount,
               result
           );
        }

        public async Task<FileDto> GetLaedsForExport(GetAllWholeSaleLeadExportInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var user = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(user);

            var actvitylog_list = _leadactivityRepository.GetAll().Where(e => e.SectionId == 35 && e.WholeSaleLeadFk.OrganizationId == input.OrganizationId);

            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.WholeSaleLeadFk.OrganizationId == input.OrganizationId);

            var FollowupList = new List<int>();
            if (input.Datefilter == "Followup" && input.StartDate != null && input.EndDate != null)
            {
                FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date >= SDate.Value.Date && x.CreationTime.Date <= EDate.Value.Date).DistinctBy(e => e.WholeSaleLeadId).Select(x => x.WholeSaleLeadId).ToList();
            }
            else if (input.Datefilter == "Followup" && input.StartDate != null)
            {
                FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date >= SDate.Value.Date).DistinctBy(e => e.WholeSaleLeadId).Select(x => x.WholeSaleLeadId).ToList();
            }
            else if (input.Datefilter == "Followup" && input.EndDate != null)
            {
                FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date <= EDate.Value.Date).DistinctBy(e => e.WholeSaleLeadId).Select(x => x.WholeSaleLeadId).ToList();
            }

            var filteredWholeSale = _wholeSaleLeadRepository.GetAll()
                         //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Talephone.Contains(input.Filter) || e.Mobile.Contains(input.Filter))
                         .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile.Contains(input.Filter))
                        .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Talephone.Contains(input.Filter))
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName.Contains(input.Filter))
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email.Contains(input.Filter))
                        .WhereIf(input.FilterName == "FirstName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.FirstName.Contains(input.Filter))
                        .WhereIf(input.FilterName == "ABNNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ABNNumber == input.Filter)
                        .WhereIf(!string.IsNullOrEmpty(input.Address), e => (e.PostalUnitNo + " " + e.PostalUnitType + " " + e.PostalStreetNo + " " + e.PostalStreetName + " " + e.PostalStreetType).Contains(input.Address))
                        .WhereIf(input.StateId != 0, e => e.PostalStateId == input.StateId)
                        .WhereIf(!string.IsNullOrEmpty(input.PostCode), e => e.PostalPostCode == input.PostCode)
                        .WhereIf(input.Datefilter == "Creation" && input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                        .WhereIf(input.Datefilter == "Creation" && input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.Datefilter == "FirstAssignDate" && input.StartDate != null, e => e.FirstAssignDate >= input.StartDate)
                        .WhereIf(input.Datefilter == "FirstAssignDate" && input.EndDate != null, e => e.FirstAssignDate <= input.EndDate)
                        .WhereIf(role.Contains("Wholesale Manager"), e => e.BDMId == AbpSession.UserId)
                        .WhereIf(role.Contains("Wholesale SalesRep"), e => e.SalesRapId == AbpSession.UserId)
                        .WhereIf(input.Datefilter == "Followup" && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))
                        .WhereIf(input.Datefilter == "NextFollowup" && input.StartDate != null, e => e.ActivityDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.Datefilter == "NextFollowup" && input.EndDate != null, e => e.ActivityDate.Value.Date <= EDate.Value.Date)
                        .WhereIf(input.Datefilter == "NextFollowup" && input.EndDate != null, e => e.ActivityDate.Value.Date <= EDate.Value.Date)
                        .WhereIf(input.SalesMangerId != null && input.SalesMangerId != 0, e => e.BDMId == input.SalesMangerId)
                        .WhereIf(input.SalesRapId != null && input.SalesRapId != 0, e => e.SalesRapId == input.SalesRapId)
                        .WhereIf(input.StatusId != null && input.StatusId != 0, e => e.WholeSaleStatusId == input.StatusId)
                        .WhereIf(input.MyLead == true, e => e.CreatorUserId == AbpSession.UserId)
                        .WhereIf(input.CreatedBy > 0, e => e.CreatorUserId == input.CreatedBy)
                        .Where(e => e.OrganizationId == input.OrganizationId);


            var wholeSaleStatus = from o in filteredWholeSale
                                  select new GetWholeSaleLeadForViewDto()
                                  {
                                      createOrEditWholeSaleLeadDto = new CreateOrEditWholeSaleLeadDto()
                                      {
                                          Id = o.Id,
                                          FirstName = o.FirstName,
                                          LastName = o.LastName,
                                          CompanyName = o.CompanyName,
                                          WholeSaleStatus = o.WholeSaleStatusFK.Name,
                                          PostalAddress = o.PostalUnitNo + " " + o.PostalUnitType + " " + o.PostalStreetNo + " " + o.PostalStreetName + " " + o.PostalStreetType,
                                          PostalState = o.PostalStateFK.Name,
                                          DelivaryAddress = o.DelivaryUnitNo + " " + o.DelivaryUnitType + " " + o.DelivaryStreetNo + " " + o.DelivaryStreetName + " " + o.DelivaryStreetType,
                                          DelivaryState = o.DelivaryStateFK.Name,
                                          WholeSaleStatusId = o.WholeSaleStatusId,
                                          BDM = _userRepository.GetAll().Where(e => e.Id == o.BDMId).FirstOrDefault().Name,
                                          SalesRapName = _userRepository.GetAll().Where(e => e.Id == o.SalesRapId).FirstOrDefault().Name,
                                          WholesaleClientId = o.WholesaleClientId,
                                          Email = o.Email,
                                          Mobile = o.Mobile
                                      },
                                      ReminderTime = o.ActivityDate != null ? o.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss") : null,
                                      ActivityDescription = o.ActivityDescription,
                                      ActivityComment = o.ActivityComment,

                                      IsCredit = _businessRepository.GetAll().Where(e => e.WholeSaleLeadId == o.Id).Any()





                                      //IsCredit = o.Id != null ? (IsCredit == true && IsCredit == false) ;

                                      //if (object.where(o=>o.id==IdToCheck).count()==0)
                                      //  {
                                      //   isCredit = true;
                                      //  }
                                      // else { isCredit = False}
                                      //}


                                      //SummaryCount = summaryCount

                                      //ReminderTime = actvitylog_list.Where(e => e.ActionId == 8 && e.WholeSaleLeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                                      //ActivityDescription = actvitylog_list.Where(e => e.ActionId == 8 && e.WholeSaleLeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                      //ActivityComment = actvitylog_list.Where(e => e.ActionId == 24 && e.WholeSaleLeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                  };


            var result = await wholeSaleStatus.ToListAsync();


            return _wholesaleLeadsExcelExport.LeadTrackerExportToFile(result);
        }

        public async Task<int?> GetLeadCountByUserId()
        {
            var leadcount = _wholeSaleLeadRepository.GetAll().Where(e => e.SalesRapId == AbpSession.UserId).Count();

            return leadcount;
        }

        public async Task RequestToTransfer(int leadId)
        {
            var request = new RequestWholesaleLeadTransfer();
            request.LeadId = leadId;
            request.Approval = "Panding";
            request.Type = "Transfer";
            await _requestWholesaleLeadTransferRepository.InsertAsync(request);

            var user = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
             
            IList<string> role = await _userManager.GetRolesAsync(user);

            WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
            leadactivity.ActionId = 73;
            leadactivity.SectionId = 41;
            leadactivity.ActionNote = (role.Contains("Wholesale Manager") ? "WholeSaleLead Tranfer Reqest by BDM : " : "WholeSaleLead Tranfer Reqest by SalesRep : ") + user.FullName;
            leadactivity.WholeSaleLeadId = leadId;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }


        public async Task RequestToMove(int leadId,int sectionId)
        {
            var request = new RequestWholesaleLeadTransfer();
            request.LeadId = leadId;
            request.Approval = "Panding";
            request.Type = "Move";
            await _requestWholesaleLeadTransferRepository.InsertAsync(request);

            var user = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(user);

            WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
            leadactivity.ActionId = 73;
            leadactivity.SectionId = sectionId;
            leadactivity.ActionNote = (role.Contains("Wholesale Manager") ? "WholeSaleLead Move Reqest by BDM : " : "WholeSaleLead Move Reqest by SalesRep : ") + user.FullName;
            leadactivity.WholeSaleLeadId = leadId;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        public async Task<PagedResultDto<GetAllRequestToWholeSaleLeadTransferDto>> GetAllRequestToWholeSaleLeadTransfer(GetAllRequestToWholeSaleLeadInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var user = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(user);



                var filteredWholeSale = _requestWholesaleLeadTransferRepository.GetAll().Include(e => e.LeadFk)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Talephone.Contains(input.Filter) || e.Mobile.Contains(input.Filter))

                        .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                        .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(role.Contains("Wholesale Manager"), e => e.LeadFk.BDMId == AbpSession.UserId)
                        .WhereIf(role.Contains("Wholesale SalesRep"), e => e.LeadFk.SalesRapId == AbpSession.UserId)
                     
                        .Where(e => e.LeadFk.OrganizationId == input.OrganizationId && e.Approval == "Panding" && e.Type==input.Type);
                         

            var pagedAndFilteredWholeSaleStatus = filteredWholeSale
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var result = from o in pagedAndFilteredWholeSaleStatus
                         select new GetAllRequestToWholeSaleLeadTransferDto()
                         {
                             Id = o.Id,
                             CreatedBy = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                             CreatedDate = o.CreationTime,
                             LeadId = o.Id,
                             Name = o.LeadFk.FirstName + " " + o.LeadFk.LastName,
                             CompanyName = o.LeadFk.CompanyName,
                             ABNNumber = o.LeadFk.ABNNumber
                         };

            var totalCount = await filteredWholeSale.CountAsync();



            return new PagedResultDto<GetAllRequestToWholeSaleLeadTransferDto>(
               totalCount,
               await result.ToListAsync()
           ); 



        }

        //public async Task RequestToTransferApproveNotApprove(int Id, string approve)
        //{
        //   var Request = await _requestWholesaleLeadTransferRepository.GetAll().Include(e => e.LeadFk).Where(e => e.Id == Id).FirstOrDefaultAsync();

        //    var assignedFromUser = _userRepository.GetAll().Include(e => e.Roles).Where(u => u.Id == Request.CreatorUserId).FirstOrDefault();

        //    var PrevAssignedFromUser = _userRepository.GetAll().Include(e => e.Roles).Where(u => u.Id == Request.LeadFk.BDMId).FirstOrDefault();

        //    IList<string> RoleName = await _userManager.GetRolesAsync(assignedFromUser);

        //    var ActionId = 0;
        //    var ActionNote = "";



        //        //var assignedFromUser = _userRepository.GetAll().Include(e => e.Roles).Where(u => u.Id == wholeSaleLead.AssignUserId).FirstOrDefault();
        //       // var PreviousOrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == wholeSaleLead.OrganizationId).Select(e => e.DisplayName).FirstOrDefault();


        //        if (approve=="Approve")
        //        {
        //            Request.Approval = "Approve";

        //            if (RoleName.Contains("Wholesale SalesRep"))
        //            {
        //                ActionNote = "Wholesale Lead Transfer To Sales Rap " + assignedFromUser.FullName + " From " + PrevAssignedFromUser.FullName;
        //                ActionId = 67;
        //                Request.LeadFk.SalesRapId = (int?)Request.CreatorUserId;
        //            }
        //            else if (RoleName.Contains("Wholesale Manager"))
        //            {

        //                ActionNote = "Wholesale Lead Transfer To BDM " + assignedFromUser.FullName + " From " + PrevAssignedFromUser.FullName;
        //                ActionId = 66;
        //                Request.LeadFk.BDMId = (int?)Request.CreatorUserId;
        //            }

        //        }
        //        else
        //        {
        //            ActionNote = "Request Rejected For Transfer Requested by " + assignedFromUser.FullName;
        //            ActionId = 74;
        //            Request.Approval = "NotApprove";
        //        }

        //        WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
        //        leadactivity.ActionId = ActionId;
        //        leadactivity.SectionId = 42;
        //        leadactivity.ActionNote = ActionNote;
        //        leadactivity.WholeSaleLeadId = (int)Request.LeadId;
        //        if (AbpSession.TenantId != null)
        //        {
        //            leadactivity.TenantId = (int)AbpSession.TenantId;
        //        }
        //        leadactivity.CreatorUserId = AbpSession.UserId;
        //        await _leadactivityRepository.InsertAsync(leadactivity);

        //        await _requestWholesaleLeadTransferRepository.UpdateAsync(Request);
        //    }
        //}



        public async Task RequestToTransferApproveNotApprove(List<int> Ids, string approve)
        {
            foreach (var Id in Ids)
            {
                var Request = await _requestWholesaleLeadTransferRepository.GetAll().Include(e => e.LeadFk).Where(e => e.Id == Id).FirstOrDefaultAsync();

                var assignedFromUser = _userRepository.GetAll().Include(e => e.Roles).Where(u => u.Id == Request.CreatorUserId).FirstOrDefault();

                var PrevAssignedFromUser = Request.LeadFk.BDMId > 0 ? _userRepository.GetAll().Include(e => e.Roles).Where(u => u.Id == Request.LeadFk.BDMId).Select(e => e.FullName).FirstOrDefault() : "";

                var wholesalelead = _wholeSaleLeadRepository.Get((int)Request.LeadId);

                IList<string> RoleName = await _userManager.GetRolesAsync(assignedFromUser);

                var ActionId = 0;
                var ActionNote = "";

                if (approve == "Approve")
                {
                    Request.Approval = "Approve";

                    if (RoleName.Contains("Wholesale SalesRep"))
                    {
                        wholesalelead.SalesRapId = (int?)Request.CreatorUserId;

                        WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
                        leadactivity.ActionId = 67;
                        leadactivity.SectionId = 42;
                        leadactivity.ActionNote = string.IsNullOrEmpty(PrevAssignedFromUser) ? "Wholesale Lead Transfer To Sales Rap " + assignedFromUser.FullName : "Wholesale Lead Transfer To Sales Rap " + assignedFromUser.FullName + " From " + PrevAssignedFromUser;
                        leadactivity.WholeSaleLeadId = (int)Request.LeadId;
                        if (AbpSession.TenantId != null)
                        {
                            leadactivity.TenantId = (int)AbpSession.TenantId;
                        }
                        leadactivity.CreatorUserId = AbpSession.UserId;
                        await _leadactivityRepository.InsertAsync(leadactivity);

                        await _requestWholesaleLeadTransferRepository.UpdateAsync(Request);

                        await _wholeSaleLeadRepository.UpdateAsync(wholesalelead);
                    }
                    else if (RoleName.Contains("Wholesale Manager"))
                    {
                        wholesalelead.BDMId = (int?)Request.CreatorUserId;
                        WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
                        leadactivity.ActionId = 66;
                        leadactivity.SectionId = 42;
                        leadactivity.ActionNote = string.IsNullOrEmpty(PrevAssignedFromUser) ? "Wholesale Lead Transfer To BDM " + assignedFromUser.FullName : "Wholesale Lead Transfer To BDM " + assignedFromUser.FullName + " From " + PrevAssignedFromUser; 
                        leadactivity.WholeSaleLeadId = (int)Request.LeadId;
                        if (AbpSession.TenantId != null)
                        {
                            leadactivity.TenantId = (int)AbpSession.TenantId;
                        }
                        leadactivity.CreatorUserId = AbpSession.UserId;
                        await _leadactivityRepository.InsertAsync(leadactivity);

                        await _requestWholesaleLeadTransferRepository.UpdateAsync(Request);

                        await _wholeSaleLeadRepository.UpdateAsync(wholesalelead);
                    }

                }
                else
                {
                    Request.Approval = "NotApprove";
                    WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
                    leadactivity.ActionId = 74;
                    leadactivity.SectionId = 42;
                    leadactivity.ActionNote = "Request Rejected For Transfer Requested by " + assignedFromUser.FullName;
                    leadactivity.WholeSaleLeadId = (int)Request.LeadId;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    leadactivity.CreatorUserId = AbpSession.UserId;
                    await _leadactivityRepository.InsertAsync(leadactivity);

                    await _requestWholesaleLeadTransferRepository.UpdateAsync(Request);

                    await _wholeSaleLeadRepository.UpdateAsync(wholesalelead);
                }

                
            }

        }

        public async Task RequestToMoveSalesRap(List<int> Ids, string approve , int OrganizationID ,int salesrapid)
        {

            foreach (var item in Ids)
            {
                var Request = await _requestWholesaleLeadTransferRepository.GetAll().Include(e => e.LeadFk).Where(e => e.Id == item).FirstOrDefaultAsync();

                var assignedFromUser = _userRepository.GetAll().Include(e => e.Roles).Where(u => u.Id == Request.CreatorUserId).FirstOrDefault();
               // var PreviousOrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == wholeSaleLead.OrganizationId).Select(e => e.DisplayName).FirstOrDefault();

                var wholesaleLead = _wholeSaleLeadRepository.GetAll().Where(u => u.Id == Request.LeadId).FirstOrDefault();

                IList<string> RoleName = await _userManager.GetRolesAsync(assignedFromUser);

                var ActionId = 0;
                var ActionNote = "";
              if (approve == "Approve")
                {
                    Request.Approval = "Approve";
                   if(wholesaleLead.SalesRapId!=null)
                    {
                        var PrevAssignedFromUser = _userRepository.GetAll().Include(e => e.Roles).Where(u => u.Id == Request.LeadFk.SalesRapId).FirstOrDefault();
                        ActionNote = "Wholesale Lead Move To " + assignedFromUser.FullName + " From " + PrevAssignedFromUser.FullName; ;
                        
                    }
                    else
                    {
                        ActionNote = "Wholesale Lead Move To " + assignedFromUser.FullName;
                    }
                    ActionId = 67;

                    wholesaleLead.OrganizationId = OrganizationID;
                    wholesaleLead.SalesRapId = salesrapid;


                }
                else
                {
                    ActionNote = "Request Rejected For Move Requested by " + assignedFromUser.FullName;
                    ActionId = 74;
                    Request.Approval = "NotApprove";
                }


                WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
                leadactivity.ActionId = ActionId;
                leadactivity.SectionId = 42;
                leadactivity.ActionNote = ActionNote;
                leadactivity.WholeSaleLeadId = (int)Request.LeadId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                leadactivity.CreatorUserId = AbpSession.UserId;
                await _leadactivityRepository.InsertAsync(leadactivity);

                await _requestWholesaleLeadTransferRepository.UpdateAsync(Request);
                await _wholeSaleLeadRepository.UpdateAsync(wholesaleLead);
            }
        }



        public async Task<List<string>> getAllEmailList(int leadId)
        {
            var wholeSaleLead = await _wholeSaleLeadRepository.FirstOrDefaultAsync(leadId);

            var contacts = await _wholeSaleLeadContactRepository.GetAll().Where(e => e.WholesaleLeadId == leadId).Select(e => e.Email).ToListAsync();

            var output = new List<string>();

            if (!string.IsNullOrEmpty(wholeSaleLead.Email)){
                output.Add(wholeSaleLead.Email);
            }
            
            foreach(var contact in contacts)
            {
                if (!string.IsNullOrEmpty(contact))
                {
                    output.Add(contact);
                }
            }

            return output;

        }

        public async Task<List<CommonLookupDto>> GetCreateUsers()
        {
            var wholeSaleLeads = await _wholeSaleLeadRepository.GetAll().Select(e => e.CreatorUserId).Distinct().ToListAsync();

            var Users = await _userRepository.GetAll().Where(e => wholeSaleLeads.Contains(e.CreatorUserId)).Select(e => new {e.Id, e.FullName}).ToListAsync();

            var result = from o in Users
                         select new CommonLookupDto
                         {
                             Id = (int)o.Id,
                             DisplayName = o.FullName
                         };

            return result.OrderBy(e => e.DisplayName).ToList();
        }

        public async Task<int?> GetLeadCountByUserIdFromUser()
        {
            var User =await _userRepository.GetAsync((long)AbpSession.UserId);

            return User.LeadCount;
        }

    }
}
