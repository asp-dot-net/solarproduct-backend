﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Linq.Extensions;
using Abp.Organizations;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EmailTemplates.Dtos;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Invoices;

namespace TheSolarProduct.EmailTemplates
{
    [AbpAuthorize(AppPermissions.Pages_Leads, AppPermissions.Pages_Tenant_DataVaults_EmailTemplates)]
    public class EmailTemplateAppService : TheSolarProductAppServiceBase, IEmailTemplateAppService
    {
        private readonly IRepository<EmailTemplate> _emailTemplateRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public EmailTemplateAppService(IRepository<EmailTemplate> emailTemplateRepository, IRepository<OrganizationUnit, long> organizationUnitRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _emailTemplateRepository = emailTemplateRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Create)]
        public async Task CreateOrEdit(CreateOrEditEmailTemplateDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Create)]
        protected virtual async Task Create(CreateOrEditEmailTemplateDto input)
        {
            var emailTemplate = ObjectMapper.Map<EmailTemplate>(input);

            if (AbpSession.TenantId != null)
            {
                emailTemplate.TenantId = (int)AbpSession.TenantId;
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 26;
            dataVaultLog.ActionNote = "Email Templates Created : " + input.TemplateName;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _emailTemplateRepository.InsertAsync(emailTemplate);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Edit)]
        protected virtual async Task Update(CreateOrEditEmailTemplateDto input)
        {
            var emailTemplate = await _emailTemplateRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 26;
            dataVaultLog.ActionNote = "Email Templates Updated : " + emailTemplate.TemplateName;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.TemplateName != emailTemplate.TemplateName)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "TemplateName";
                history.PrevValue = emailTemplate.TemplateName;
                history.CurValue = input.TemplateName;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Body != emailTemplate.Body)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Body";
                history.PrevValue = emailTemplate.Body;
                history.CurValue = input.Body;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Subject != emailTemplate.Subject)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Subject";
                history.PrevValue = emailTemplate.Subject;
                history.CurValue = input.Subject;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.OrganizationUnitId != emailTemplate.OrganizationUnitId)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Organization";
                history.PrevValue = _organizationUnitRepository.Get((long)emailTemplate.OrganizationUnitId).DisplayName;
                history.CurValue = _organizationUnitRepository.Get((long)input.OrganizationUnitId).DisplayName;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, emailTemplate);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _emailTemplateRepository.Get(input.Id).TemplateName;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 26;
            dataVaultLog.ActionNote = "Email Templates Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _emailTemplateRepository.DeleteAsync(input.Id);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates)]
        public async Task<PagedResultDto<GetEmailTemplateDto>> GetAll(GetAllEmailTemplateInput input)
        {
            var filteredEmailTemplates = _emailTemplateRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.TemplateName.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.TemplateName == input.NameFilter);
            var pagedAndFilteredEmailTemplates = filteredEmailTemplates.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var emailTemplates = from o in pagedAndFilteredEmailTemplates
                                 select new GetEmailTemplateDto()
                                 {
                                     EmailTemplate = new EmailTemplateDto
                                     {
                                         TemplateName = o.TemplateName,
                                         Body = o.Body,
                                         Subject = o.Subject,
                                         Id = o.Id
                                     },

                                     Organization = _organizationUnitRepository.GetAll().Where(e => e.Id == o.OrganizationUnitId).FirstOrDefault().DisplayName
                                 };

            var totalCount = await filteredEmailTemplates.CountAsync();

            return new PagedResultDto<GetEmailTemplateDto>(
                totalCount,
                await emailTemplates.ToListAsync()
            );
        }
        
        [AbpAuthorize(AppPermissions.Pages_Leads,AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Edit)]
        public async Task<GetEmailTemplateForEdit> GetEmailTemplateForEdit(EntityDto input)
        {
            var Email = await _emailTemplateRepository.FirstOrDefaultAsync(input.Id);
            var output = new GetEmailTemplateForEdit { EmailTemplate = ObjectMapper.Map<CreateOrEditEmailTemplateDto>(Email) };
            return output;
        }
    }
}
