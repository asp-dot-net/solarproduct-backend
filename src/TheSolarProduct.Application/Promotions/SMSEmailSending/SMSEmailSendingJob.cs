﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Localization;
using Abp.Net.Mail;
using Abp.Runtime.Security;
using Abp.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.EmailTemplates;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Leads;
using TheSolarProduct.Notifications;

namespace TheSolarProduct.Promotions.SMSEmailSending
{
	public class SMSEmailSendingJob : BackgroundJob<SMSEmailSendingJobArgs>, ITransientDependency
	{
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private readonly IRepository<Promotion> _promotionRepository;
		private readonly IRepository<PromotionUser> _promotionUserRepository;
		private readonly IRepository<EmailTemplate> _emailTemplateRepository;
		private readonly IRepository<User, long> _lookup_userRepository;
		private readonly IRepository<LeadActivityLog> _leadactivityRepository;
		private readonly IRepository<Lead> _leadRepository;
		private readonly IApplicationSettingsAppService _applicationSettings;
		private readonly IEmailSender _emailSender;
		private readonly IAppNotifier _appNotifier;

		public SMSEmailSendingJob(IUnitOfWorkManager unitOfWorkManager
			, IRepository<Promotion> promotionRepository
			, IRepository<PromotionUser> promotionUserRepository
			, IRepository<EmailTemplate> emailTemplateRepository
			, IRepository<User, long> lookup_userRepository
			, IRepository<LeadActivityLog> leadactivityRepository
			, IRepository<Lead> leadRepository
			, IApplicationSettingsAppService applicationSettings
			, IEmailSender emailSender
			, IAppNotifier appNotifier)
		{
			_unitOfWorkManager = unitOfWorkManager;
			_promotionRepository = promotionRepository;
			_promotionUserRepository = promotionUserRepository;
			_emailTemplateRepository = emailTemplateRepository;
			_lookup_userRepository = lookup_userRepository;
			_leadactivityRepository = leadactivityRepository;
			_leadRepository = leadRepository;
			_applicationSettings = applicationSettings;
			_emailSender = emailSender;
			_appNotifier = appNotifier;
		}

		[UnitOfWork]
		public override async void Execute(SMSEmailSendingJobArgs args)
		{
			using (var uow = _unitOfWorkManager.Begin())
			{
				//using (CurrentUnitOfWork.SetTenantId(args.TenantId))
				//{
				//	int i = 0;
				//	var PromotionDetail = _promotionRepository.GetAll().Where(e => e.Id == args.PromotionId).FirstOrDefault();
				//	var PromotionUserData = _promotionUserRepository.GetAll().Where(e => e.PromotionId == args.PromotionId).ToList();

				//	foreach (var Promotion in PromotionUserData)
				//	{
				//		if (Promotion.LeadId > 0)
				//		{
				//			try
				//			{
				//				// Add Activity
				//				var UserDetail = _lookup_userRepository.GetAll().Where(u => u.Id == args.User.UserId).FirstOrDefault();
				//				LeadActivityLog leadactivity = new LeadActivityLog();
				//				leadactivity.ActionId = 21;
				//				leadactivity.ActionNote = "Promotion " + PromotionDetail.Title + " has been sent";
				//				leadactivity.LeadId = (int)Promotion.LeadId;
				//				leadactivity.Body = PromotionDetail.Description;
				//				leadactivity.TenantId = (int)args.TenantId;
				//				int promoActivityID = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

				//				//await uow.CompleteAsync();

				//				// Send SMS
				//				var mobilenumber = _leadRepository.GetAll().Where(e => e.Id == Promotion.LeadId).FirstOrDefault().Mobile;
				//				var Email = _leadRepository.GetAll().Where(e => e.Id == Promotion.LeadId).FirstOrDefault().Email;

				//				if (PromotionDetail.PromotionTypeId == 1)
				//				{
				//					if (!string.IsNullOrEmpty(mobilenumber))
				//					{
				//						try
				//						{
				//							//SendSMSInput sendSMSInput = new SendSMSInput();
				//							//sendSMSInput.PhoneNumber = mobilenumber;
				//							//sendSMSInput.Text = PromotionDetail.Description;
				//							//sendSMSInput.ActivityId = promoActivityID;
				//							//sendSMSInput.promoresponseID = Promotion.Id;
				//							//await _applicationSettings.SendSMS(sendSMSInput);
				//						}
				//						catch (Exception e)
				//						{

				//						}
				//					}
				//				}
				//				if (PromotionDetail.PromotionTypeId == 2)
				//				{
				//					if (!string.IsNullOrEmpty(Email))
				//					{
				//						var KeyValue = (int)args.TenantId + "," + Promotion.Id;

				//						//Token With Tenant Id 7 Promotion User Primary Id for subscribe & UnSubscribe
				//						var token = HttpUtility.UrlEncode(SimpleStringCipher.Instance.Encrypt(KeyValue, AppConsts.DefaultPassPhrase));

				//						//Subscription Link
				//						string SubscribeLink = "http://thesolarproduct.com/account/customer-subscribe?STR=" + token;

				//						//UnSubscription Link
				//						string UnSubscribeLink = "http://thesolarproduct.com/account/customer-unsubscribe?STR=" + token;

				//						//Footer Design for subscribe & UnSubscribe
				//						var footer = "<div style=\"border:1px solid #ccc;padding:10px; background:#f2f2f2;\">If you are intrested in this offer, please click <a href=\"" + SubscribeLink + "\">" +
				//							"<img height=\"20px\" src=\"http://thesolarproduct.com/assets/common/images/like.png\"></a>.<br/>" +
				//							"If you do not wish to receive any further communications, please click <a href=\"" + UnSubscribeLink + "\">" +
				//							"<img height=\"20px\" src=\"http://thesolarproduct.com/assets/common/images/dislike.png\"></a>.</div>";

				//						//Merge Email Body And Footer for subscribe & UnSubscribe
				//						string FinalEmailBody = PromotionDetail.Description + footer;
				//						try
				//						{
				//							//MailMessage mail = new MailMessage
				//							//{
				//							//	From = new MailAddress(UserDetail.EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
				//							//	To = { Email },
				//							//	Subject = PromotionDetail.Title,
				//							//	Body = FinalEmailBody,
				//							//	IsBodyHtml = true
				//							//};
				//							//await this._emailSender.SendAsync(mail);
				//						}
				//						catch (Exception e)
				//						{

				//						}
				//					}
				//				}
				//			}
				//			catch (Exception e)
				//			{
				//				i = i++;
				//			}
				//		}
				//	}

				//	AsyncHelper.RunSync(() => ProcessResultAsync(args));
				//}

				uow.Complete();
			}
		}


		//private async Task ProcessResultAsync(SMSEmailSendingJobArgs args)
		//{
		//	await _appNotifier.SendMessageAsync(
		//		args.User,
		//		new LocalizableString("AllPromotionsSentSuccessFully", TheSolarProductConsts.LocalizationSourceName),
		//		null,
		//		Abp.Notifications.NotificationSeverity.Success);

		//}
	}
}
