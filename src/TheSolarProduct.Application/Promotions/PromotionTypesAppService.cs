﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Promotions.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace TheSolarProduct.Promotions
{
	[AbpAuthorize(AppPermissions.Pages_PromotionTypes)]
    public class PromotionTypesAppService : TheSolarProductAppServiceBase, IPromotionTypesAppService
    {
		 private readonly IRepository<PromotionType> _promotionTypeRepository;
		 

		  public PromotionTypesAppService(IRepository<PromotionType> promotionTypeRepository ) 
		  {
			_promotionTypeRepository = promotionTypeRepository;
			
		  }

		 public async Task<PagedResultDto<GetPromotionTypeForViewDto>> GetAll(GetAllPromotionTypesInput input)
         {
			
			var filteredPromotionTypes = _promotionTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter);

			var pagedAndFilteredPromotionTypes = filteredPromotionTypes
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

			var promotionTypes = from o in pagedAndFilteredPromotionTypes
                         select new GetPromotionTypeForViewDto() {
							PromotionType = new PromotionTypeDto
							{
                                Name = o.Name,
                                Id = o.Id
							}
						};

            var totalCount = await filteredPromotionTypes.CountAsync();

            return new PagedResultDto<GetPromotionTypeForViewDto>(
                totalCount,
                await promotionTypes.ToListAsync()
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_PromotionTypes_Edit)]
		 public async Task<GetPromotionTypeForEditOutput> GetPromotionTypeForEdit(EntityDto input)
         {
            var promotionType = await _promotionTypeRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetPromotionTypeForEditOutput {PromotionType = ObjectMapper.Map<CreateOrEditPromotionTypeDto>(promotionType)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditPromotionTypeDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_PromotionTypes_Create)]
		 protected virtual async Task Create(CreateOrEditPromotionTypeDto input)
         {
            var promotionType = ObjectMapper.Map<PromotionType>(input);

			

            await _promotionTypeRepository.InsertAsync(promotionType);
         }

		 [AbpAuthorize(AppPermissions.Pages_PromotionTypes_Edit)]
		 protected virtual async Task Update(CreateOrEditPromotionTypeDto input)
         {
            var promotionType = await _promotionTypeRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, promotionType);
         }

		 [AbpAuthorize(AppPermissions.Pages_PromotionTypes_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _promotionTypeRepository.DeleteAsync(input.Id);
         } 
    }
}