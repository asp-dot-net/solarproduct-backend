﻿using TheSolarProduct.Promotions;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Promotions.Exporting;
using TheSolarProduct.Promotions.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Leads.Dtos;
using Abp.Authorization.Users;
using TheSolarProduct.Authorization.Users;
using Castle.Facilities.TypedFactory.Internal;
using Org.BouncyCastle.Crypto.Paddings;
using MimeKit.Cryptography;
using System.Runtime.CompilerServices;
using PayPalCheckoutSdk.Orders;
using System.Diagnostics;
using System.Net;
using Microsoft.VisualBasic;
using NUglify.Helpers;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.Leads;
using TheSolarProduct.ApplicationSettings;
using System.Net.Mail;
using Abp.Net.Mail;
using Abp.Runtime.Security;
using System.Text.RegularExpressions;
using Abp.Domain.Uow;
using TheSolarProduct.EmailTemplates;
using Microsoft.AspNetCore.Http;
using System.Web;
using System.Configuration;
using System.Transactions;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Promotions.SMSEmailSending;
using Abp.Runtime.Session;
using Abp.BackgroundJobs;
using Abp.Timing.Timezone;
using Abp.EntityFrameworkCore;
using Abp.Dependency;
using TheSolarProduct.SysJobs;
using TheSolarProduct.Notifications;
using Abp.Notifications;
using TheSolarProduct.Invoices;
using TheSolarProduct.Jobs;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.TheSolarDemo;
using Abp.Organizations;
using System.Drawing.Drawing2D;
using NPOI.SS.Formula.Functions;
using TheSolarProduct.PromotionHistorys;
using TheSolarProduct.WholeSalePromotions.Dtos;
using TheSolarProduct.Organizations;
using Microsoft.AspNetCore.Mvc.Diagnostics;

namespace TheSolarProduct.Promotions
{
    //[AbpAuthorize(AppPermissions.Pages_Promotions)]
    public class PromotionsAppService : TheSolarProductAppServiceBase, IPromotionsAppService
    {
        private readonly IRepository<Promotion> _promotionRepository;
        private readonly IPromotionsExcelExporter _promotionsExcelExporter;
        private readonly IRepository<PromotionType, int> _lookup_promotionTypeRepository;
        private readonly IRepository<PromotionUser> _PromotionUsersRepository;
        private readonly IRepository<PromotionResponseStatus> _PromotionResponseStatusRepository;
        private readonly IRepository<User, long> _lookup_userRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<Lead> _leadRepository;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<PromotionUser> _promotionUserRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<EmailTemplate> _emailTemplateRepository;
        protected readonly IBackgroundJobManager BackgroundJobManager;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private TheSolarProductDbContext _database;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<User, long> _userRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<PromotionHistory> _promotionHistoryRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendedOrganizationUnitRepository;
        public object URLEncoder { get; private set; }

        public PromotionsAppService(IRepository<Promotion> promotionRepository
            , IRepository<LeadActivityLog> leadactivityRepository
            , IPromotionsExcelExporter promotionsExcelExporter
            , IRepository<PromotionType, int> lookup_promotionTypeRepository
            , IRepository<PromotionResponseStatus> PromotionResponseStatusRepository
            , IRepository<PromotionUser> PromotionUsersRepository
            , IRepository<User, long> lookup_userRepository
            , IApplicationSettingsAppService applicationSettings
            , IRepository<Lead> leadRepository
            , IEmailSender emailSender
            , IRepository<PromotionUser> promotionUserRepository
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<EmailTemplate> emailTemplateRepository
            , IBackgroundJobManager backgroundJobManager
            , ITimeZoneConverter timeZoneConverter
            , IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
            IRepository<User, long> userRepository,
            IAppNotifier appNotifier,
            IRepository<InvoicePayment> invoicePaymentRepository,
            IRepository<Job> jobRepository,
            IRepository<UserRole, long> userRoleRepository,
             IRepository<Role> roleRepository,
             IRepository<UserTeam> userTeamRepository,
             IRepository<OrganizationUnit, long> organizationUnitRepository,
             IRepository<PromotionHistory> promotionHistoryRepository
            , IRepository<ExtendOrganizationUnit, long> extendedOrganizationUnitRepository

            )
        {
            _promotionRepository = promotionRepository;
            _promotionsExcelExporter = promotionsExcelExporter;
            _lookup_promotionTypeRepository = lookup_promotionTypeRepository;
            _PromotionResponseStatusRepository = PromotionResponseStatusRepository;
            _PromotionUsersRepository = PromotionUsersRepository;
            _lookup_userRepository = lookup_userRepository;
            _leadactivityRepository = leadactivityRepository;
            _leadRepository = leadRepository;
            _applicationSettings = applicationSettings;
            _emailSender = emailSender;
            _promotionUserRepository = promotionUserRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _emailTemplateRepository = emailTemplateRepository;
            BackgroundJobManager = backgroundJobManager;
            _timeZoneConverter = timeZoneConverter;
            _dbcontextprovider = dbcontextprovider;
            _userRepository = userRepository;
            _appNotifier = appNotifier;
            _invoicePaymentRepository = invoicePaymentRepository;
            _jobRepository = jobRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _userTeamRepository = userTeamRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _promotionHistoryRepository = promotionHistoryRepository;
            _extendedOrganizationUnitRepository = extendedOrganizationUnitRepository;
        }

        /// <summary>
        /// Get Promotion List
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetPromotionForViewDto>> GetAll(GetAllPromotionsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.FromDateFilter, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.ToDateFilter, (int)AbpSession.TenantId));

            var filteredPromotions = _promotionRepository.GetAll()
                .Include(e => e.PromotionTypeFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter), e => e.Title.Contains(input.TitleFilter))
                        .WhereIf(input.FromDateFilter != null && input.ToDateFilter != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.PromotionTitleFilter != 0, e => e.Id == input.PromotionTitleFilter)
                        .WhereIf(input.MinAmountFilter != null && input.MaxAmountFilter != null, e => e.PromoCharge >= input.MinAmountFilter && e.PromoCharge <= input.MaxAmountFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.Contains(input.DescriptionFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionTypeNameFilter), e => e.PromotionTypeFk != null && e.PromotionTypeFk.Name == input.PromotionTypeNameFilter)
                        .WhereIf(input.PromoType != 0, e => e.PromotionTypeId == input.PromoType)
                        .WhereIf(input.OrganizationUnit != null && input.OrganizationUnit != 0, e => e.OrganizationID == input.OrganizationUnit);

            if (filteredPromotions != null && filteredPromotions.Count() > 0)
            {
                var JobStatus = _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.JobStatusId == 1 && e.LeadFk.OrganizationId == input.OrganizationUnit).Select(e => e.LeadId).ToList();
                var deposite = _invoicePaymentRepository.GetAll().Where(e => e.InvoicePaymentStatusId == 2).Select(e => e.JobFk.LeadId).Distinct().ToList();
                var promotionids = filteredPromotions.Select(e => e.Id).Distinct().ToList();
                var promotionuser_list = _PromotionUsersRepository.GetAll().Include(e => e.LeadFk).Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);

                var jobs = _jobRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);

                var pagedAndFilteredPromotions = filteredPromotions
                    .OrderBy(input.Sorting ?? "Id desc")
                    .PageBy(input);

                var tot = (from pu in promotionuser_list
                           join promo in filteredPromotions on pu.PromotionId equals promo.Id
                           where pu.LeadFk.IsDeleted == false
                           select pu.Id).Count();

                var totPromo = (from pu in promotionuser_list
                                join promo in filteredPromotions on pu.PromotionId equals promo.Id
                                where pu.LeadFk.IsDeleted == false && pu.ResponseMessage != null
                                select pu.Id).Count();

                var interestedPromo = (from pu in promotionuser_list
                                       join promo in filteredPromotions on pu.PromotionId equals promo.Id
                                       where pu.LeadFk.IsDeleted == false && pu.LeadFk.IsDeleted == false && pu.ResponseMessage != null && pu.PromotionResponseStatusId == 1
                                       select pu.Id).Count();

                var notInterestedPromo = (from pu in promotionuser_list
                                          join promo in filteredPromotions on pu.PromotionId equals promo.Id
                                          where pu.LeadFk.IsDeleted == false && pu.ResponseMessage != null && pu.PromotionResponseStatusId == 2
                                          select pu.Id).Count();

                var otherPromo = (from pu in promotionuser_list
                                  join promo in filteredPromotions on pu.PromotionId equals promo.Id
                                  where pu.LeadFk.IsDeleted == false && pu.ResponseMessage != null && pu.PromotionResponseStatusId == 3
                                  select pu.Id).Count();

                var unhandle = (from pu in promotionuser_list
                                join promo in filteredPromotions on pu.PromotionId equals promo.Id
                                where pu.LeadFk.IsDeleted == false && pu.ResponseMessage != null
                                && !jobs.Any(e => e.IsDeleted == false && e.LeadId == pu.LeadId)
                                select pu.LeadId).Count();

                var promoProject = (from pu in promotionuser_list
                                    join promo in filteredPromotions on pu.PromotionId equals promo.Id
                                    join job in jobs on pu.LeadId equals job.LeadId
                                    where pu.LeadFk.IsDeleted == false && pu.ResponseMessage != null && job.FirstDepositDate == null
                                    select pu.LeadId).Count();

                var soldPromo = (from pu in promotionuser_list
                                 join promo in filteredPromotions on pu.PromotionId equals promo.Id
                                 join job in jobs on pu.LeadId equals job.LeadId
                                 where pu.LeadFk.IsDeleted == false && pu.ResponseMessage != null && job.FirstDepositDate != null
                                 select pu.LeadId).Count();

                //var tot = filteredPromotions.Select(e => e.LeadCount).Sum();
                //var totPromo = promotionuser_list.Where(e => promotionids.Contains((int)e.PromotionId) && e.ResponseMessage != null).Select(e => e.Id).Count();
                //var interestedPromo = promotionuser_list.Where(e => promotionids.Contains((int)e.PromotionId) && e.ResponseMessage != null && e.PromotionResponseStatusId == 1).Select(e => e.Id).Count();

                //var notInterestedPromo = promotionuser_list.Where(e => promotionids.Contains((int)e.PromotionId) && e.ResponseMessage != null && e.PromotionResponseStatusId == 2).Select(e => e.Id).Count();
                //var otherPromo = promotionuser_list.Where(e => promotionids.Contains((int)e.PromotionId) && e.ResponseMessage != null && e.PromotionResponseStatusId == 3).Select(e => e.Id).Count();
                //var unhandle = promotionuser_list.Where(e => promotionids.Contains((int)e.PromotionId) && e.ResponseMessage != null && e.LeadFk.LeadStatusId != 6).Select(e => e.LeadId).Distinct().Count();
                //var soldPromo = promotionuser_list.Where(e => promotionids.Contains((int)e.PromotionId) && e.ResponseMessage != null && deposite.Contains((int)e.LeadId)).Select(e => e.LeadId).Distinct().Count();
                //var promoProject = promotionuser_list.Where(e => promotionids.Contains((int)e.PromotionId) && e.ResponseMessage != null && JobStatus.Contains((int)e.LeadId)).Select(e => e.LeadId).Distinct().Count();

                var promotions = from o in pagedAndFilteredPromotions

                                 join o2 in _lookup_userRepository.GetAll() on o.CreatorUserId equals o2.Id

                                 select new GetPromotionForViewDto()
                                 {
                                     Promotion = new PromotionDto
                                     {
                                         Title = o.Title,
                                         PromoCharge = o.PromoCharge,
                                         Description = o.Description,
                                         CreationTime = o.CreationTime,
                                         OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == o.OrganizationID).Select(e => e.DisplayName).FirstOrDefault(),
                                         Id = o.Id
                                     },
                                     InterestedCount = promotionuser_list.Where(s => s.PromotionId == o.Id && s.PromotionResponseStatusId == 1 && s.LeadFk.IsDeleted == false).Count(),
                                     NotInterestedCount = promotionuser_list.Where(s => s.PromotionId == o.Id && s.PromotionResponseStatusId == 2 && s.LeadFk.IsDeleted == false).Count(),
                                     OtherCount = promotionuser_list.Where(s => s.PromotionId == o.Id && s.PromotionResponseStatusId == 3 && s.LeadFk.IsDeleted == false).Count(),
                                     NoReply = promotionuser_list.Where(s => s.PromotionId == o.Id && s.ResponseMessage == null && s.LeadFk.IsDeleted == false).Count(),

                                     //TotalLeads = o.LeadCount,
                                     TotalLeads = promotionuser_list.Where(s => s.PromotionId == o.Id && s.LeadFk.IsDeleted == false).Count(),
                                     PromotionTypeName = o.PromotionTypeFk.Name,
                                     CreatorUserName = o2.UserName,

                                     //Summury
                                     TotalSendPromotion = tot,
                                     TotalPromotion = totPromo,
                                     summaryInterestedCount = interestedPromo,
                                     summaryNotInterestedCount = notInterestedPromo,
                                     summaryOtherCount = otherPromo,
                                     Unhandle = unhandle,
                                     PromotionProject = promoProject,
                                     PromotionSold = soldPromo
                                 };

                var totalCount = await filteredPromotions.CountAsync();

                return new PagedResultDto<GetPromotionForViewDto>(
                    totalCount,
                    await promotions.ToListAsync()
                );
            }
            else
            {
                return new PagedResultDto<GetPromotionForViewDto>(0, null);
            }
        }

        /// <summary>
        /// Promotion Detail View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<GetPromotionForViewDto> GetPromotionForView(int id)
        {
            var promotion = await _promotionRepository.GetAsync(id);

            var output = new GetPromotionForViewDto { Promotion = ObjectMapper.Map<PromotionDto>(promotion) };

            if (output.Promotion.PromotionTypeId != null)
            {
                var _lookupPromotionType = await _lookup_promotionTypeRepository.FirstOrDefaultAsync((int)output.Promotion.PromotionTypeId);
                output.PromotionTypeName = _lookupPromotionType?.Name?.ToString();
                output.Promotion.Description = WebUtility.HtmlDecode(output.Promotion.Description);
            }

            return output;
        }

        /// <summary>
        /// Get Promotion Data For Edit ==> Not in Use
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        ///[AbpAuthorize(AppPermissions.Pages_Promotions_Edit)]
        public async Task<GetPromotionForEditOutput> GetPromotionForEdit(EntityDto input)
        {
            var promotion = await _promotionRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPromotionForEditOutput { Promotion = ObjectMapper.Map<CreateOrEditPromotionDto>(promotion) };

            if (output.Promotion.PromotionTypeId != null)
            {
                var _lookupPromotionType = await _lookup_promotionTypeRepository.FirstOrDefaultAsync((int)output.Promotion.PromotionTypeId);
                output.PromotionTypeName = _lookupPromotionType?.Name?.ToString();
            }

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditPromotionDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
        }

        /// <summary>
        /// Create Promotion
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Promotions_Create)]
        protected virtual async Task Create(CreateOrEditPromotionDto input)
        {
            //SaveOrTest == 1 To Send Promotion To All Leads
            //SaveOrTest == 2 To Send Promotion To Only Test Users(Data Given in MobileNosList & Emails)

            input.StartDateFilter = (_timeZoneConverter.Convert(input.StartDateFilter, (int)AbpSession.TenantId));
            input.EndDateFilter = (_timeZoneConverter.Convert(input.EndDateFilter, (int)AbpSession.TenantId));

            if (input.SaveOrTest == 1)
            {
                //Send Promotion to all Leads
                var promotion = ObjectMapper.Map<Promotion>(input);

                if (AbpSession.TenantId != null)
                {
                    promotion.TenantId = (int)AbpSession.TenantId;
                }

                string LeadStatusIdsFilter = "";
                string LeadSourceIdsFilter = "";
                string StateIdsFilter = "";
                string TeamIdsFilter = "";
                string JobStatusIdsFilter = "";

                if (input.LeadStatusIdsFilter != null)
                {
                    foreach (var item in input.LeadStatusIdsFilter)
                    {
                        LeadStatusIdsFilter = LeadStatusIdsFilter + item;
                    }
                }
                if (input.LeadSourceIdsFilter != null)
                {
                    foreach (var item in input.LeadSourceIdsFilter)
                    {
                        LeadSourceIdsFilter = LeadSourceIdsFilter + item;
                    }
                }
                if (input.StateIdsFilter != null)
                {
                    foreach (var item in input.StateIdsFilter)
                    {
                        StateIdsFilter = StateIdsFilter + item;
                    }
                }
                if (input.TeamIdsFilter != null)
                {
                    foreach (var item in input.TeamIdsFilter)
                    {
                        TeamIdsFilter = TeamIdsFilter + item;
                    }
                }
                if (input.JobStatusIdsFilter != null)
                {
                    foreach (var item in input.JobStatusIdsFilter)
                    {
                        JobStatusIdsFilter = JobStatusIdsFilter + item;
                    }
                }
                
                promotion.LeadStatusIdsFilter = LeadStatusIdsFilter;
                promotion.LeadSourceIdsFilter = LeadSourceIdsFilter;
                promotion.StateIdsFilter = StateIdsFilter;
                promotion.TeamIdsFilter = TeamIdsFilter;
                promotion.JobStatusIdsFilter = JobStatusIdsFilter;
                promotion.AreaNameFilter = input.Area;
                promotion.TypeNameFilter = input.Type;
                //Insert Data in Promotion Table With Promotion Details & Lead Count
                int newPromotionId = _promotionRepository.InsertAndGetId(promotion);

                var promotionHistory = new PromotionHistory();
                if (AbpSession.TenantId != null)
                {
                    promotionHistory.TenantId = (int)AbpSession.TenantId;
                }
                promotionHistory.LeadIds = input.SelectedLeadIdsForPromotion;
                promotionHistory.PromotionType = "Retail";
                promotionHistory.PromotionId = newPromotionId;
                promotionHistory.PromotionSendingId = (int)input.PromotionTypeId;
                promotionHistory.OrganizationId = input.OrganizationID;
                promotionHistory.Title = input.Title;
                promotionHistory.Description = input.Description;
                promotionHistory.LeadStatuses = input.LeadStatuses;
                promotionHistory.States = input.States;
                promotionHistory.StartDate = input.StartDateFilter;
                promotionHistory.EndDate = input.EndDateFilter;
                promotionHistory.Charges = input.PromoCharge;
                promotionHistory.TotalCharCount = input.totalCharCount;
                promotionHistory.TotalCredit = input.totalCredit;
                promotionHistory.LeadSources = input.LeadSources;
                promotionHistory.Teams = input.Teams;
                promotionHistory.Type = input.Type;
                promotionHistory.Area = input.Area;

                await _promotionHistoryRepository.InsertAsync(promotionHistory);

                string[] arrSelectedLeadIds = input.SelectedLeadIdsForPromotion.Split(',').Distinct().ToArray();

                var List = new List<PromotionUser>();

                var ActivityList = new List<LeadActivityLog>();

                var sendBulkSMSInput = new SendBulkSMSInput();

                var promotionSMSEmailList = new List<PromotionSMSEmailList>();

                List<SendSMSInput> msg = new List<SendSMSInput>();

                if (!string.IsNullOrEmpty(Convert.ToString(newPromotionId)))
                {
                    //List for Promotion User Table for all leads
                    foreach (string leadId in arrSelectedLeadIds)
                    {
                        if (!string.IsNullOrEmpty(leadId))
                        {
                            int LeadId = Convert.ToInt32(leadId);

                            var ListItem = new PromotionUser();
                            ListItem.CreationTime = promotion.CreationTime;
                            ListItem.CreatorUserId = promotion.CreatorUserId;
                            ListItem.IsDeleted = false;
                            ListItem.PromotionId = newPromotionId;
                            ListItem.LeadId = LeadId;
                            
                            ListItem.CreatorUserId = promotion.CreatorUserId;
                            ListItem.TenantId = promotion.TenantId;
                            List.Add(ListItem);
                        }
                    }

                    //Bulk Insert Data in Promotion User Table
                    await _dbcontextprovider.GetDbContext().PromotionUsers.AddRangeAsync(List);
                    await _dbcontextprovider.GetDbContext().SaveChangesAsync();
                }

                if (!string.IsNullOrEmpty(Convert.ToString(newPromotionId)))
                {
                    var PromotionUserList = await _promotionUserRepository.GetAll().Where(e => e.PromotionId == promotion.Id).Select(e => new { e.Id, e.LeadId }).ToListAsync();

                    //List for Lead Activity Log Table for all leads
                    foreach (var pu in PromotionUserList)
                    {
                        if (pu.LeadId > 0)
                        {
                            int LeadId = Convert.ToInt32(pu.LeadId);

                            var ActivityListItem = new LeadActivityLog();
                            ActivityListItem.ActionId = input.PromotionTypeId == 1 ? 21 : 71;
                            ActivityListItem.ActionNote = "Promotion " + input.Description + " has been sent";
                            ActivityListItem.LeadId = LeadId;
                            ActivityListItem.Body = input.Description;
                            ActivityListItem.TenantId = promotion.TenantId;
                            ActivityListItem.CreatorUserId = promotion.CreatorUserId;
                            ActivityListItem.CreationTime = promotion.CreationTime;
                            ActivityListItem.IsDeleted = false;
                            ActivityListItem.PromotionId = promotion.Id;
                            ActivityListItem.PromotionUserId = pu.Id;
                            if (input.IsCimat == true)
                            {
                                ActivityListItem.IsCimat = true;
                            }
                            ActivityList.Add(ActivityListItem);
                        }
                    }

                    //Bulk Insert Data in Lead Activity Log Table
                    await _dbcontextprovider.GetDbContext().LeadActivityLogs.AddRangeAsync(ActivityList);
                    await _dbcontextprovider.GetDbContext().SaveChangesAsync();
                }

                if (!string.IsNullOrEmpty(Convert.ToString(newPromotionId)))
                {
                    var ActivityLogList = await _leadactivityRepository.GetAll().Where(e => e.PromotionId == promotion.Id).Select(e => new { e.Id, e.LeadId, e.PromotionUserId }).ToListAsync();

                    //List for Lead Activity Log Table for all leads
                    foreach (var al in ActivityLogList)
                    {
                        if (al.LeadId > 0)
                        {
                            int LeadId = Convert.ToInt32(al.LeadId);
                            var promotionSMSEmailListItem = new PromotionSMSEmailList();
                            var LeadDetail = _leadRepository.GetAll().Where(e => e.Id == LeadId).FirstOrDefault();
                            if (input.IsCimat == true)
                            {
                                LeadDetail.SMSEmailCIMAT = 1;
                                await _leadRepository.UpdateAsync(LeadDetail);
                            }
                            var SalesRep = _userRepository.GetAll().Where(e => e.Id == LeadDetail.AssignToUserID).FirstOrDefault();

                            var bodyDescription = input.Description.Replace("{{Customer.Name}}", LeadDetail.CompanyName)
                                                    .Replace("{{Customer.Address}}", LeadDetail.Address)
                                                    .Replace("{{Customer.State}}", LeadDetail.State)
                                                    .Replace("{{Customer.Suburb}}", LeadDetail.Suburb)
                                                    .Replace("{{Customer.PostCode}}", LeadDetail.PostCode)
                                                    .Replace("{{Customer.Mobile}}", LeadDetail.Mobile)
                                                    .Replace("{{Customer.Email}}", LeadDetail.Email)
                                                    .Replace("{{SalesRep.CimetLink}}", !string.IsNullOrEmpty(SalesRep.CimetLink) ? SalesRep.CimetLink : "");

                            //Send Promotion to Only Test Users(Data Given in MobileNosList & Emails)
                            if (input.PromotionTypeId == 1)
                            {
                                if (!string.IsNullOrEmpty(LeadDetail.Mobile))
                                {
                                    var sendSMSInput = new SendSMSInput();
                                    sendSMSInput.PhoneNumber = LeadDetail.Mobile;
                                    sendSMSInput.Text = bodyDescription;
                                    sendSMSInput.ActivityId = al.Id;
                                    sendSMSInput.promoresponseID = (int)al.PromotionUserId;
                                    msg.Add(sendSMSInput);
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(LeadDetail.Email))
                                {
                                    var KeyValue = Convert.ToString(AbpSession.TenantId) + "," + (int)al.PromotionUserId;


                                    //Token With Tenant Id & Promotion User Primary Id for subscribe & UnSubscribe
                                    var token = HttpUtility.UrlEncode(SimpleStringCipher.Instance.Encrypt(KeyValue, AppConsts.DefaultPassPhrase));

                                    //Subscription Link
                                    string SubscribeLink = "https://thesolarproduct.com/account/customer-subscribe?STR=" + token;

                                    //UnSubscription Link
                                    string UnSubscribeLink = "https://thesolarproduct.com/account/customer-unsubscribe?STR=" + token;

                                    //Footer Design for subscribe & UnSubscribe
                                    //var footer = "<div style='padding:20px; background: rgb(57,163,247); background: linear-gradient(321deg, rgba(57,163,247,1) 0%, rgba(40,76,224,1) 45%, rgba(118,63,200,1) 100%); color:#fff; display:flex; align-items:center; justify-content: space-between; margin-top:15px;'><div style='color:#fff; font-size:1.6rem;'>Are you intrested in this offer?</div><div style='display:flex; align-items:center; justify-content: space-between;'><a href='"+ SubscribeLink + "' style='color:#fff; background:#1a1a1a; border:1px solid #1a1a1a; padding:15px 25px; border-radius:28px; font-size: 1.2rem; margin-right:10px; text-decoration: none; display:flex; align-items:center;'><img src='https://thesolarproduct.com/assets/common/images/thumbs-up.png' style='height:25px; margin-right:8px;'> Yes, Interested</a><a href='"+ UnSubscribeLink + "' style='color:#fff; background:#3293e2; border:1px solid #fff; padding:15px 25px; border-radius:28px; font-size: 1.2rem; text-decoration: none; display:flex; align-items:center;'><img src='https://thesolarproduct.com/assets/common/images/icon-unsubscribe.png' style='height:25px; margin-right:8px;'> Unsubscribe Now</a></div></div>";

                                    //var footer = "<div style='padding:20px; background: rgb(57,163,247); background: linear-gradient(321deg, rgba(57,163,247,1) 0%, rgba(40,76,224,1) 45%, rgba(118,63,200,1) 100%); color:#fff; display:flex; align-items:center; justify-content: space-between; text-align:center;'><table style='border:0; font-family:arial,helvetica,sans-serif; width:auto; margin:0 auto;'><tr><td style='color:#fff; font-size:1.3rem; vertical-align: middle;'>Are you intrested in this offer?</td><td style='width:10px;'></td><td style='vertical-align: middle;'><table style='border:0; font-family:arial,helvetica,sans-serif;'><tr><td><a href='" + SubscribeLink + "' style='font-family:arial,helvetica,sans-serif;color:#fff; background:#1a1a1a; border:1px solid #1a1a1a; padding:15px 25px; border-radius:28px; font-size: 1rem; margin-right:10px; text-decoration: none; display:flex; align-items:center;'> Yes, Interested</a></td><td><a href='" + UnSubscribeLink + "' style='font-family:arial,helvetica,sans-serif;color:#fff; background:#3293e2; padding:15px 25px; border-radius:28px; font-size: 1rem; text-decoration: none; display:flex; align-items:center;'> Unsubscribe Now</a></td></tr></table></td></tr></table></div>";
                                    var footer = "<div style='padding:10px; background: rgb(57,163,247); background: linear-gradient(321deg, rgba(57,163,247,1) 0%, rgba(40,76,224,1) 45%, rgba(118,63,200,1) 100%); color:#fff; display:flex; align-items:center; justify-content: space-between; text-align:center;'><table style='border:0; font-family:arial,helvetica,sans-serif; width:auto; margin:0 auto;'><tr><td style='color:#fff; font-size:1.3rem; vertical-align: middle;'>Are you intrested in this offer?</td><td style='width:10px;'></td><td style='vertical-align: middle;'><table style='border:0; font-family:arial,helvetica,sans-serif;'><tr><td><a href='" + SubscribeLink + "' style='font-family:arial,helvetica,sans-serif;color:#fff; background:#1a1a1a; border:1px solid #1a1a1a; padding:10px 25px; border-radius:10px; font-size: 1rem; margin-right:10px; text-decoration: none; display:flex; align-items:center;'> Yes, Interested</a></td><td><a href='" + UnSubscribeLink + "' style='font-family:arial,helvetica,sans-serif;color:#fff; background:#3293e2; padding:10px 25px; border-radius:10px; font-size: 1rem; text-decoration: none; display:flex; align-items:center;'> Unsubscribe Now</a></td></tr></table></td></tr></table></div>";
                                    //var footer = "<div style='padding:20px; background: rgb(57,163,247); background: linear-gradient(321deg, rgba(57,163,247,1) 0%, rgba(40,76,224,1) 45%, rgba(118,63,200,1) 100%); color:#fff; display:flex; align-items:center; justify-content: space-between; text-align:center;'><table style='border:0; font-family:arial,helvetica,sans-serif; width:auto; margin:0 auto;'><tr><td style='color:#fff; font-size:1.3rem; vertical-align: middle;'>Are you intrested in this offer?</td><td style='width:10px;'></td><td style='vertical-align: middle;'><table style='border:0; font-family:arial,helvetica,sans-serif;'><tr><td><a href='" + SubscribeLink + "' style='font-family:arial,helvetica,sans-serif;color:#fff; background:#1a1a1a; border:1px solid #1a1a1a; padding:15px 25px; border-radius:28px; font-size: 1rem; margin-right:10px; text-decoration: none; display:flex; align-items:center;'><img src='https://thesolarproduct.com/assets/common/images/thumbs-up.png' style='height:22px; margin-right:8px;'> Yes, Interested</a></td><td><a href='" + UnSubscribeLink + "' style='font-family:arial,helvetica,sans-serif;color:#fff; background:#3293e2; padding:15px 25px; border-radius:28px; font-size: 1rem; text-decoration: none; display:flex; align-items:center;'><img src='https://thesolarproduct.com/assets/common/images/icon-unsubscribe.png' style='height:22px; margin-right:8px;'> Unsubscribe Now</a></td></tr></table></td></tr></table></div>";


                                    //Merge Email Body And Footer for subscribe & UnSubscribe
                                    string FinalEmailBody = input.IsFooterdAttached == true ? bodyDescription + footer : bodyDescription;

                                    try
                                    {
                                        await _emailSender.SendAsync(new MailMessage
                                        {
                                            From = new MailAddress(input.EmailFrom),
                                            To = { LeadDetail.Email }, ////{ "hiral.prajapati@meghtechnologies.com" },
                                            Subject = input.Title,
                                            Body = FinalEmailBody,
                                            IsBodyHtml = true
                                        });
                                        //MailMessage mail = new MailMessage
                                        //{
                                        //    From = new MailAddress("info@solarproduct.com.au"), //new MailAddress(assignedToUser.EmailAddress),
                                        //    To = { "hiral.prajapati@meghtechnologies.com" },
                                        //    Subject = input.Title,
                                        //    Body = FinalEmailBody,
                                        //    IsBodyHtml = true
                                        //};
                                        //await this._emailSender.SendAsync(mail);
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                        }

                        ////Resolver for HangfireJob(Must have to use before Job Call)
                        //var leadReminderService = IocManager.Instance.Resolve<IClientDomainService>();

                        ////Hangfire Job(Fire And Forget) 
                        //var jobId = Hangfire.BackgroundJob.Enqueue(() => leadReminderService.SendPromotion(promotion.TenantId, promotion.Id, (int)AbpSession.UserId));
                    }

                    if (msg.Count > 0)
                    {
                        sendBulkSMSInput.Messages = msg;
                        await _applicationSettings.SendBulkSMS(sendBulkSMSInput);
                    }
                }
            }
            else
            {
                //Send Promotion to Only Test Users(Data Given in MobileNosList & Emails)
                if (input.PromotionTypeId == 1)
                {
                    string[] MobileNosList = input.MobileNos.Split(',').Distinct().ToArray();
                    foreach (string mobileno in MobileNosList)
                    {
                        if (!string.IsNullOrEmpty(mobileno))
                        {
                            try
                            {
                                SendSMSInput sendSMSInput = new SendSMSInput();
                                sendSMSInput.PhoneNumber = mobileno;
                                sendSMSInput.Text = input.Description;
                                await _applicationSettings.SendSMS(sendSMSInput);
                            }
                            catch (Exception e)
                            {

                            }
                        }
                    }
                }
                else
                {

                    if (!string.IsNullOrEmpty(input.Emails))
                    {
                        var KeyValue = Convert.ToString(AbpSession.TenantId);

                        //Token With Tenant Id & Promotion User Primary Id for subscribe & UnSubscribe
                        var token = HttpUtility.UrlEncode(SimpleStringCipher.Instance.Encrypt(KeyValue, AppConsts.DefaultPassPhrase));

                        //Subscription Link
                        string SubscribeLink = "https://thesolarproduct.com/account/customer-subscribe?STR=" + token;

                        //UnSubscription Link
                        string UnSubscribeLink = "https://thesolarproduct.com/account/customer-unsubscribe?STR=" + token;

                        //Footer Design for subscribe & UnSubscribe
                        //var footer = "<div style=\"border:1px solid #ccc;padding:10px; background:#f2f2f2;\">If you are intrested in this offer, please click <a href=\"" + SubscribeLink + "\">" +
                        //	"<img height=\"20px\" src=\"https://thesolarproduct.com/assets/common/images/like.png\"></a>.<br/>" +
                        //	"If you do not wish to receive any further communications, please click <a href=\"" + UnSubscribeLink + "\">" +
                        //	"<img height=\"20px\" src=\"https://thesolarproduct.com/assets/common/images/dislike.png\"></a>.</div>";


                        var footer = "<div style='padding:20px; background: rgb(57,163,247); background: linear-gradient(321deg, rgba(57,163,247,1) 0%, rgba(40,76,224,1) 45%, rgba(118,63,200,1) 100%); color:#fff; display:flex; align-items:center; justify-content: space-between; text-align:center;'><table style='border:0; font-family:arial,helvetica,sans-serif; width:auto; margin:0 auto;'><tr><td style='color:#fff; font-size:1.3rem; vertical-align: middle;'>Are you intrested in this offer?</td><td style='width:10px;'></td><td style='vertical-align: middle;'><table style='border:0; font-family:arial,helvetica,sans-serif;'><tr><td><a href='" + SubscribeLink + "' style='font-family:arial,helvetica,sans-serif;color:#fff; background:#1a1a1a; border:1px solid #1a1a1a; padding:15px 25px; border-radius:28px; font-size: 1rem; margin-right:10px; text-decoration: none; display:flex; align-items:center;'><img src='https://thesolarproduct.com/assets/common/images/thumbs-up.png' style='height:22px; margin-right:8px;'> Yes, Interested</a></td><td><a href='" + UnSubscribeLink + "' style='font-family:arial,helvetica,sans-serif;color:#fff; background:#3293e2; padding:15px 25px; border-radius:28px; font-size: 1rem; text-decoration: none; display:flex; align-items:center;'><img src='https://thesolarproduct.com/assets/common/images/icon-unsubscribe.png' style='height:22px; margin-right:8px;'> Unsubscribe Now</a></td></tr></table></td></tr></table></div>";

                        //Merge Email Body And Footer for subscribe & UnSubscribe
                        string FinalEmailBody = input.Description + footer;

                        try
                        {
                            MailMessage mail = new MailMessage
                            {
                                From = new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                                To = { input.Emails },
                                Subject = input.Title,
                                Body = FinalEmailBody,
                                IsBodyHtml = true
                            };
                            await this._emailSender.SendAsync(mail);
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }
            }

        }

        /// <summary>
        /// Edit Promotion Details ==> Not in Use
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        ///[AbpAuthorize(AppPermissions.Pages_Promotions_Edit)]
        protected virtual async Task Update(CreateOrEditPromotionDto input)
        {
            var promotion = await _promotionRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, promotion);
        }

        /// <summary>
        /// Delete Promotion ==> Not in Use
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Promotions_Delete)]
        public async Task Delete(int promoId)
        {

            var promoreply = _PromotionUsersRepository.GetAll().Where(e => e.PromotionId == promoId).ToList();
            await _promotionRepository.DeleteAsync(promoId);
            foreach (var item in promoreply)
            {
                await _promotionUserRepository.DeleteAsync(item);
            }

        }

        /// <summary>
        /// Export Promotion Data to Excel
        /// </summary>
        /// <param name="input"></param>
        /// <returns>FileDto</returns>
        public async Task<FileDto> GetPromotionsToExcel(GetAllPromotionsForExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.FromDateFilter, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.ToDateFilter, (int)AbpSession.TenantId));

            var orginization = new List<int?>();
            if (input.OrganizationUnit != null && input.OrganizationUnit != 0)
            {
                orginization = _PromotionUsersRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit).Select(e => e.PromotionId).ToList();
            }
            var filteredPromotions = _promotionRepository.GetAll().OrderByDescending(e => e.CreationTime)
                .Include(e => e.PromotionTypeFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter), e => e.Title == input.TitleFilter)
                        .WhereIf(input.FromDateFilter != null && input.ToDateFilter != null,
                                    e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date &&
                                    e.CreationTime.AddHours(10).Date <= EDate.Value.Date)

                        .WhereIf(input.MinAmountFilter != null && input.MaxAmountFilter != null,
                                    e => e.PromoCharge >= input.MinAmountFilter &&
                                    e.PromoCharge <= input.MaxAmountFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.Contains(input.DescriptionFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionTypeNameFilter), e => e.PromotionTypeFk != null && e.PromotionTypeFk.Name == input.PromotionTypeNameFilter)
                        .WhereIf(input.OrganizationUnit != null && input.OrganizationUnit != 0, e => orginization.Contains(e.Id)).OrderByDescending(e => e.Id);

            var promotions = from o in filteredPromotions
                             join o1 in _lookup_promotionTypeRepository.GetAll() on o.PromotionTypeId equals o1.Id into j1
                             from s1 in j1.DefaultIfEmpty()
                             join o2 in _lookup_userRepository.GetAll() on o.CreatorUserId equals o2.Id

                             select new GetPromotionForViewDto()
                             {
                                 Promotion = new PromotionDto
                                 {
                                     Title = o.Title,
                                     PromoCharge = o.PromoCharge,
                                     Description = o.Description,
                                     //Id = o.Id,
                                     CreationTime = o.CreationTime,
                                     //TotalLeads = o.LeadCount
                                 },
                                 InterestedCount = _PromotionUsersRepository.GetAll().Where(s => s.PromotionId == o.Id && s.PromotionResponseStatusId == 1).Count(),
                                 NotInterestedCount = _PromotionUsersRepository.GetAll().Where(s => s.PromotionId == o.Id && s.PromotionResponseStatusId == 2).Count(),
                                 OtherCount = _PromotionUsersRepository.GetAll().Where(s => s.PromotionId == o.Id && s.PromotionResponseStatusId == 3).Count(),
                                 NoReply = _PromotionUsersRepository.GetAll().Where(s => s.PromotionId == o.Id && s.ResponseMessage == null).Count(),
                                 TotalLeads = _PromotionUsersRepository.GetAll().Where(s => s.PromotionId == o.Id).Count(),
                                 PromotionTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                                 CreatorUserName = o2.UserName
                             };

            return _promotionsExcelExporter.ExportToFile(await promotions.ToListAsync());
        }

        /// <summary>
        /// Promotion Type Dropdown
        /// </summary>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Promotions)]
        public async Task<List<PromotionPromotionTypeLookupTableDto>> GetAllPromotionTypeForTableDropdown()
        {
            return await _lookup_promotionTypeRepository.GetAll()
                .Select(promotionType => new PromotionPromotionTypeLookupTableDto
                {
                    Id = promotionType.Id,
                    DisplayName = promotionType == null || promotionType.Name == null ? "" : promotionType.Name.ToString()
                }).ToListAsync();
        }

        /// <summary>
        /// Subscribe And UnSubscribe for Promotion
        /// </summary>
        /// <param name="STR">Encrypted Link (Tenant Id & Promotion Id)</param>
        /// <param name="PromotionResponseStatusId">1 = Subscribe,2 = UnSubscribe</param>
        /// <returns></returns>
        public async Task SubscribeUnsubscribePromo(string STR, int PromotionResponseStatusId)
        {
            //Encrypt TenantId & PromotionId
            var IDs = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(STR, AppConsts.DefaultPassPhrase));
            var ID = IDs.Split(",");
            int TenantId = Convert.ToInt32(ID[0]);
            int Promoid = Convert.ToInt32(ID[1]);

            //Get Set from GIven Tenant Id
            using (_unitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var promotionUser = await _promotionUserRepository.FirstOrDefaultAsync((int)Promoid);
                promotionUser.PromotionResponseStatusId = PromotionResponseStatusId;
                if (PromotionResponseStatusId == 1)
                {
                    promotionUser.ResponseMessage = "Yes";
                }
                if (PromotionResponseStatusId == 2)
                {
                    promotionUser.ResponseMessage = "Stop";
                }
                promotionUser.ResponseDate = DateTime.UtcNow;
                await _promotionUserRepository.UpdateAsync(promotionUser);

                var prmotionuserid = _leadactivityRepository.GetAll().Where(e => e.PromotionUserId == (int)Promoid).Select(e => e.Id).FirstOrDefault();

                var activity = new LeadActivityLog();
                activity.ActionNote = "Prmotion Email Reply Given By Customer";
                activity.TenantId = TenantId;
                activity.ActionId = 72;
                activity.CreatorUserId = 1;
                activity.LeadId = (int)promotionUser.LeadId;
                //activity.MessageId = smsID;
                activity.ReferanceId = prmotionuserid;
                activity.IsMark = false;
                await _leadactivityRepository.InsertAsync(activity);

                var Activityid = _leadactivityRepository.GetAll().Where(e => e.Id == prmotionuserid).FirstOrDefault();
                var Lead = _leadRepository.GetAll().Where(e => e.Id == Activityid.LeadId).FirstOrDefault();
                //            var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == AssignUserID).FirstOrDefault();
                //string msg = string.Format("Promotion Reply Received");
                //await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);

                string msg = string.Format("Promotion Email Replay Received");

                var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == Lead.AssignToUserID).FirstOrDefault();
                await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
                //var teamid = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
                //var roleids = _roleRepository.GetAll().Where(e => e.Name == "Admin").Select(e => e.Id).ToList();
                //var userids = _userRoleRepository.GetAll().Where(e => roleids.Contains(e.RoleId)).Select(e => e.UserId).ToList();

                //foreach (var id in userids)
                //{
                //    var NotifyToUsertoadmin = _userRepository.GetAll().Where(u => u.Id == id).FirstOrDefault();
                //    await _appNotifier.NewLead(NotifyToUsertoadmin, msg, NotificationSeverity.Info);
                //}

                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == Lead.AssignToUserID).Select(e => e.TeamId).ToList();
                var ManagerUserid = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
                foreach (var id in ManagerUserid)
                {
                    var NotifyToManager = _userRepository.GetAll().Where(u => u.Id == id).FirstOrDefault();
                    await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
                }
            }
        }

        /// <summary>
        /// Export Excel With Mobile & Email 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<FileDto> CreateWithExcel(CreateOrEditPromotionDto input)
        {
            //Send Promotion to all Leads
            var promotion = ObjectMapper.Map<Promotion>(input);

            if (AbpSession.TenantId != null)
            {
                promotion.TenantId = (int)AbpSession.TenantId;
            }

            string LeadStatusIdsFilter = "";
            string LeadSourceIdsFilter = "";
            string StateIdsFilter = "";
            string TeamIdsFilter = "";
            string JobStatusIdsFilter = "";

            //if (input.LeadStatusIdsFilter != null)
            //{
            //    foreach (var item in input.LeadStatusIdsFilter)
            //    {
            //        LeadStatusIdsFilter = LeadStatusIdsFilter + item;
            //    }
            //}
            //if (input.LeadSourceIdsFilter != null)
            //{
            //    foreach (var item in input.LeadSourceIdsFilter)
            //    {
            //        LeadSourceIdsFilter = LeadSourceIdsFilter + item;
            //    }
            //}
            //if (input.StateIdsFilter != null)
            //{
            //    foreach (var item in input.StateIdsFilter)
            //    {
            //        StateIdsFilter = StateIdsFilter + item;
            //    }
            //}
            //if (input.TeamIdsFilter != null)
            //{
            //    foreach (var item in input.TeamIdsFilter)
            //    {
            //        TeamIdsFilter = TeamIdsFilter + item;
            //    }
            //}
            //if (input.JobStatusIdsFilter != null)
            //{
            //    foreach (var item in input.JobStatusIdsFilter)
            //    {
            //        JobStatusIdsFilter = JobStatusIdsFilter + item;
            //    }
            //}
            promotion.LeadStatusIdsFilter = input.LeadStatuses;
            promotion.LeadSourceIdsFilter = input.LeadSources;
            promotion.StateIdsFilter = input.States;
            promotion.TeamIdsFilter = input.Teams;
            promotion.JobStatusIdsFilter = input.JobStatues;

            //Insert Data in Promotion Table With Promotion Details & Lead Count
            int newPromotionId = _promotionRepository.InsertAndGetId(promotion);

            string[] arrSelectedLeadIds = input.SelectedLeadIdsForPromotion.Split(',').Distinct().ToArray();

            var List = new List<PromotionUser>();

            var ActivityList = new List<LeadActivityLog>();

            var sendBulkSMSInput = new SendBulkSMSInput();

            var promotionSMSEmailList = new List<PromotionSMSEmailList>();

            //List for Promotion User Table for all leads
            foreach (string leadId in arrSelectedLeadIds)
            {
                if (!string.IsNullOrEmpty(leadId))
                {
                    int LeadId = Convert.ToInt32(leadId);

                    var ListItem = new PromotionUser();
                    ListItem.CreationTime = promotion.CreationTime;
                    ListItem.CreatorUserId = promotion.CreatorUserId;
                    ListItem.IsDeleted = false;
                    ListItem.PromotionId = newPromotionId;
                    ListItem.LeadId = LeadId;
                    ListItem.CreatorUserId = promotion.CreatorUserId;
                    ListItem.TenantId = promotion.TenantId;
                    List.Add(ListItem);

                    var ActivityListItem = new LeadActivityLog();
                    ActivityListItem.ActionId = 21;
                    ActivityListItem.ActionNote = "Promotion " + input.Description + " has been sent";
                    ActivityListItem.LeadId = LeadId;
                    ActivityListItem.Body = input.Description;
                    ActivityListItem.TenantId = promotion.TenantId;
                    ActivityListItem.CreatorUserId = promotion.CreatorUserId;
                    ActivityListItem.CreationTime = promotion.CreationTime;
                    ActivityListItem.IsDeleted = false;
                    ActivityList.Add(ActivityListItem);

                    var promotionSMSEmailListItem = new PromotionSMSEmailList();
                    var LeadData = _leadRepository.GetAll().Where(e => e.Id == LeadId).Select(e => new { e.Mobile, e.Email }).FirstOrDefault();
                    promotionSMSEmailListItem.SMS = LeadData.Mobile;
                    promotionSMSEmailListItem.Email = LeadData.Email;
                    promotionSMSEmailList.Add(promotionSMSEmailListItem);
                }
            }

            //Bulk Insert Data in Promotion User Table
            await _dbcontextprovider.GetDbContext().PromotionUsers.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ////Bulk Insert Data in Lead Activity Log Table
            await _dbcontextprovider.GetDbContext().LeadActivityLogs.AddRangeAsync(ActivityList);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            return _promotionsExcelExporter.ExportSMSEmailListFile(promotionSMSEmailList);
        }

        public async Task<GetPromotionHistorybyPromotionIdOutPut> GetPromotionHistorybyPromotionId(int promotionId)
        {
            var promotionhistory = _promotionHistoryRepository.GetAll().Where(e => e.PromotionId == promotionId && e.PromotionType == "Retail").FirstOrDefault();

            var result = new GetPromotionHistorybyPromotionIdOutPut();
            if (promotionhistory != null)
            {
                result = new GetPromotionHistorybyPromotionIdOutPut
                {
                    Id = promotionhistory.Id,
                    Description = promotionhistory.Description,
                    TotalCredit = promotionhistory.TotalCredit,
                    TotalCharCount = promotionhistory.TotalCharCount,
                    LeadIds = promotionhistory.LeadIds,
                    Title = promotionhistory.Title,
                    PromotionSendingId = promotionhistory.PromotionSendingId,
                    LeadStatuses = promotionhistory.LeadStatuses,
                    States = promotionhistory.States,
                    StartDate = promotionhistory.StartDate,
                    EndDate = promotionhistory.EndDate,
                    Charges = promotionhistory.Charges,
                    Area = promotionhistory.Area,
                    Type = promotionhistory.Type,
                    LeadSources = promotionhistory.LeadSources,
                    Teams = promotionhistory.Teams,
                };
            }


            return result;
        }

        public async Task<string> GetOrganizationLogo(string STR)
        {
            var logo = "";
            var IDs = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(STR, AppConsts.DefaultPassPhrase));
            var ID = IDs.Split(",");
            int TenantId = Convert.ToInt32(ID[0]);
            int Promoid = Convert.ToInt32(ID[1]);

            //Get Set from GIven Tenant Id
            using (_unitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var promotionUser = await _promotionUserRepository.FirstOrDefaultAsync((int)Promoid);
                var lead = await _leadRepository.GetAsync((int)promotionUser.LeadId);
                var org = _extendedOrganizationUnitRepository.GetAll();

                logo = org.Where(e => e.Id == lead.OrganizationId).Select(e => (e.LogoFilePath + e.LogoFileName)).FirstOrDefault();
            }
            return logo;
        }
    }
}