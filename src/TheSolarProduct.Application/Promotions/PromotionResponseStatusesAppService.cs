﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Promotions.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace TheSolarProduct.Promotions
{
	//[AbpAuthorize(AppPermissions.Pages_PromotionResponseStatuses)]
    public class PromotionResponseStatusesAppService : TheSolarProductAppServiceBase, IPromotionResponseStatusesAppService
    {
		 private readonly IRepository<PromotionResponseStatus> _promotionResponseStatusRepository;
		 

		  public PromotionResponseStatusesAppService(IRepository<PromotionResponseStatus> promotionResponseStatusRepository ) 
		  {
			_promotionResponseStatusRepository = promotionResponseStatusRepository;
			
		  }

		 public async Task<PagedResultDto<GetPromotionResponseStatusForViewDto>> GetAll(GetAllPromotionResponseStatusesInput input)
         {
			
			var filteredPromotionResponseStatuses = _promotionResponseStatusRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter);

			var pagedAndFilteredPromotionResponseStatuses = filteredPromotionResponseStatuses
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

			var promotionResponseStatuses = from o in pagedAndFilteredPromotionResponseStatuses
                         select new GetPromotionResponseStatusForViewDto() {
							PromotionResponseStatus = new PromotionResponseStatusDto
							{
                                Name = o.Name,
                                Id = o.Id
							}
						};

            var totalCount = await filteredPromotionResponseStatuses.CountAsync();

            return new PagedResultDto<GetPromotionResponseStatusForViewDto>(
                totalCount,
                await promotionResponseStatuses.ToListAsync()
            );
         }
		 
		 public async Task<GetPromotionResponseStatusForViewDto> GetPromotionResponseStatusForView(int id)
         {
            var promotionResponseStatus = await _promotionResponseStatusRepository.GetAsync(id);

            var output = new GetPromotionResponseStatusForViewDto { PromotionResponseStatus = ObjectMapper.Map<PromotionResponseStatusDto>(promotionResponseStatus) };
			
            return output;
         }
		 
		/// [AbpAuthorize(AppPermissions.Pages_PromotionResponseStatuses_Edit)]
		 public async Task<GetPromotionResponseStatusForEditOutput> GetPromotionResponseStatusForEdit(EntityDto input)
         {
            var promotionResponseStatus = await _promotionResponseStatusRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetPromotionResponseStatusForEditOutput {PromotionResponseStatus = ObjectMapper.Map<CreateOrEditPromotionResponseStatusDto>(promotionResponseStatus)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditPromotionResponseStatusDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		// [AbpAuthorize(AppPermissions.Pages_PromotionResponseStatuses_Create)]
		 protected virtual async Task Create(CreateOrEditPromotionResponseStatusDto input)
         {
            var promotionResponseStatus = ObjectMapper.Map<PromotionResponseStatus>(input);

			
			//if (AbpSession.TenantId != null)
			//{
			//	promotionResponseStatus.TenantId = (int) AbpSession.TenantId;
			//}
		

            await _promotionResponseStatusRepository.InsertAsync(promotionResponseStatus);
         }

		 //[AbpAuthorize(AppPermissions.Pages_PromotionResponseStatuses_Edit)]
		 protected virtual async Task Update(CreateOrEditPromotionResponseStatusDto input)
         {
            var promotionResponseStatus = await _promotionResponseStatusRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, promotionResponseStatus);
         }
		
		 //[AbpAuthorize(AppPermissions.Pages_PromotionResponseStatuses_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _promotionResponseStatusRepository.DeleteAsync(input.Id);
         } 
    }
}