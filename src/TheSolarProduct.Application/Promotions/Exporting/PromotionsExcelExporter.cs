﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Promotions.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Promotions.Exporting
{
	public class PromotionsExcelExporter : NpoiExcelExporterBase, IPromotionsExcelExporter
	{

		private readonly ITimeZoneConverter _timeZoneConverter;
		private readonly IAbpSession _abpSession;

		public PromotionsExcelExporter(
			ITimeZoneConverter timeZoneConverter,
			IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :
	base(tempFileCacheManager)
		{
			_timeZoneConverter = timeZoneConverter;
			_abpSession = abpSession;
		}
		
		public FileDto ExportToFile(List<GetPromotionForViewDto> promotions)
		{
			return CreateExcelPackage(
				"Promotions.xlsx",
				excelPackage =>
				{
					var sheet = excelPackage.CreateSheet(L("Promotions"));

					AddHeader(
						sheet,
						"Promotion Type",
						"Title",
						"Promo Charge",
						"Created By",
						"Created On",
						"Interested",
						"Not Interested",
						"Other",
						"No Reply",
						"Total Leads"
						);

					AddObjects(
						sheet, 1, promotions,
						_ => _.PromotionTypeName,
						_ => _.Promotion.Title,
						_ => string.Concat("$", _.Promotion.PromoCharge),
						_ => _.CreatorUserName,
						_ => _.Promotion.CreationTime,
						_ => _.InterestedCount,
						_ => _.NotInterestedCount,
						_ => _.OtherCount,
						_ => _.NoReply,
						_ => _.TotalLeads
						);

					for (var i = 1; i <= promotions.Count; i++)
					{
						SetCellDataFormat(sheet.GetRow(i).Cells[4], "dd/MM/yyyy");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[2], "00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[5], "00");
						SetintCellDataFormat(sheet.GetRow(i).Cells[6], "00");
						SetintCellDataFormat(sheet.GetRow(i).Cells[7], "00");
						SetintCellDataFormat(sheet.GetRow(i).Cells[8], "00");
						SetintCellDataFormat(sheet.GetRow(i).Cells[9], "00");
                    }
                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
		}

		public FileDto ExportSMSEmailListFile(List<PromotionSMSEmailList> promotionSMSEmailList)
		{
			return CreateExcelPackage(
				"PromotionsSMSEmailList.xlsx",
				excelPackage =>
				{
					var sheet = excelPackage.CreateSheet(L("PromotionsSMSEmailList"));

					AddHeader(
						sheet,
						L("Mobile"),
						L("Email")
						);

					AddObjects(
						sheet, 2, promotionSMSEmailList,
						_ => _.SMS,
						_ => _.Email
						);
				});
		}
	}
}
