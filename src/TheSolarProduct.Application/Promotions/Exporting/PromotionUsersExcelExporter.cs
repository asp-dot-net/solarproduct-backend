﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Promotions.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using System;
using TheSolarProduct.Authorization;
using TheSolarProduct.Invoices.Dtos;
using Abp.Authorization;

namespace TheSolarProduct.Promotions.Exporting
{
    public class PromotionUsersExcelExporter : NpoiExcelExporterBase, IPromotionUsersExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly PermissionChecker _permissionChecker;

        public PromotionUsersExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager, PermissionChecker permissionChecker) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _permissionChecker = permissionChecker;
        }

        public FileDto ExportToFile(List<GetPromotionUserForViewDto> promotionUsers, string fileName)
        {
            string[] Header = null;
            Func<GetPromotionUserForViewDto, object>[] Objects = null;

            var EmailPermission = _permissionChecker.IsGranted(AppPermissions.Pages_PromotionUsers_Excel_MobileEmail);

            if (EmailPermission)
            {
                Header = new string[] { "Customer", "Sales Rep", "Mobile/Email", "Response", "Responce On", "Lead Status", "JobNumber", "Job Status", "Title", "Type", "Send On", "Comment", "Next Followup", "Description" };

                Objects = new Func<GetPromotionUserForViewDto, object>[] {
                    _ => _.LeadCopanyName,
                    _ => _.CurrentLeadOwner,
                    _ => string.Concat(_.Mobile, "/", _.EMail),
                    _ => _.PromotionUser.ResponseMessage,
                    _ => _timeZoneConverter.Convert(_.PromotionUser.ResponseDate, _abpSession.TenantId, _abpSession.GetUserId()),
                    _ => _.LeadStatus,
                    _ => _.ProjectNumber,
                    _ => _.ProjectStatus,
                    _ => _.PromotionTitle,
                    _ => _.PromotionTypeName,
                    _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId()),
                    _ => _.ActivityComment,
                    _ => _.ActivityReminderTime,
                    _ => _.ActivityDescription
                };
            }
            else
            {
                Header = new string[] { "Customer", "Sales Rep", "Response", "Responce On", "Lead Status", "JobNumber", "Job Status", "Title", "Type", "Send On", "Comment", "Next Followup", "Description" };

                Objects = new Func<GetPromotionUserForViewDto, object>[] {
                    _ => _.LeadCopanyName,
                    _ => _.CurrentLeadOwner,
                    _ => _.PromotionUser.ResponseMessage,
                    _ => _timeZoneConverter.Convert(_.PromotionUser.ResponseDate, _abpSession.TenantId, _abpSession.GetUserId()),
                    _ => _.LeadStatus,
                    _ => _.ProjectNumber,
                    _ => _.ProjectStatus,
                    _ => _.PromotionTitle,
                    _ => _.PromotionTypeName,
                    _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId()),
                    _ => _.ActivityComment,
                    _ => _.ActivityReminderTime,
                    _ => _.ActivityDescription
                };
            }

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("PromotionUsers"));

                    AddHeader(sheet, Header);

                    AddObjects(sheet, 1, promotionUsers, Objects);

                    for (var i = 1; i <= promotionUsers.Count; i++)
                    {
                        if (EmailPermission)
                        {
                            SetCellDataFormat(sheet.GetRow(i).Cells[4], "dd-MM-yyyy");
                            SetCellDataFormat(sheet.GetRow(i).Cells[10], "dd-MM-yyyy");
                            SetCellDataFormat(sheet.GetRow(i).Cells[12], "dd-MM-yyyy");
                        }
                        else
                        {
                            SetCellDataFormat(sheet.GetRow(i).Cells[3], "dd-MM-yyyy");
                            SetCellDataFormat(sheet.GetRow(i).Cells[9], "dd-MM-yyyy");
                            SetCellDataFormat(sheet.GetRow(i).Cells[11], "dd-MM-yyyy");
                        }
                    }

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        if (EmailPermission && i != 3)
                        {
                            sheet.AutoSizeColumn(i);
                        }
                        else
                        {
                            sheet.DefaultColumnWidth = 15;
                        }

                        if (EmailPermission == false && i != 2)
                        {
                            sheet.AutoSizeColumn(i);
                        }
                        else
                        {
                            sheet.DefaultColumnWidth = 15;
                        }
                    }
                });
        }

        //public FileDto ExportCsvToFile(List<GetPromotionUserForViewDto> promotionUsers)
        //{
        //    return CreateExcelPackage(
        //        "PromotionUsers.csv",
        //        excelPackage =>
        //        {

        //            var sheet = excelPackage.CreateSheet(L("PromotionUsers"));

        //            AddHeader(
        //                sheet,
        //                L("SalesTeam"),
        //                 L("SalesRep"),
        //                  (L("ProjectNo")),
        //                   (L("ProjectStatus")),
        //                      (L("Lead")),
        //                        L("Contact"),
        //                           L("ResponseMessage"),
        //                L("ResponseDate"),
        //                 L("Interested"),
        //                   L("Mobile"),
        //                    L("Type"),
        //                      L("CustEntered"),
        //                        L("ProjectOpenDate"),
        //                         L("FollowupComment"),
        //                  L("FollowupDate"),
        //                L("CopanyName")




        //                );

        //            AddObjects(
        //                sheet, 2, promotionUsers,
        //                 _ => _.SalesTeam,
        //                  _ => _.CurrentLeadOwner,
        //                   _ => _.ProjectNumber,
        //                     _ => _.ProjectStatus,
        //                 _ => _.LeadStatus,
        //                   _ => _.EMail,
        //                _ => _.PromotionUser.ResponseMessage,
        //                _ => _timeZoneConverter.Convert(_.PromotionUser.ResponseDate, _abpSession.TenantId, _abpSession.GetUserId()),
        //                _ => _.Interested,
        //                    _ => _.Mobile,
        //                     _ => _.PromotionResponseStatusName,
        //                       _ => _timeZoneConverter.Convert(_.CustEntered, _abpSession.TenantId, _abpSession.GetUserId()),
        //                        _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId()),
        //                          _ => _.LastComment,
        //                            _ => _timeZoneConverter.Convert(_.LastCommentDate, _abpSession.TenantId, _abpSession.GetUserId()),
        //                 _ => _.LeadCopanyName


        //                );


        //            for (var i = 1; i <= promotionUsers.Count; i++)
        //            {
        //                SetCellDataFormat(sheet.GetRow(i).Cells[8], "yyyy-mm-dd");
        //            }
        //            sheet.AutoSizeColumn(8);
        //            for (var i = 1; i <= promotionUsers.Count; i++)
        //            {
        //                SetCellDataFormat(sheet.GetRow(i).Cells[11], "yyyy-mm-dd");
        //            }
        //            sheet.AutoSizeColumn(11);
        //            for (var i = 1; i <= promotionUsers.Count; i++)
        //            {
        //                SetCellDataFormat(sheet.GetRow(i).Cells[12], "yyyy-mm-dd");
        //            }
        //            sheet.AutoSizeColumn(12);
        //            for (var i = 1; i <= promotionUsers.Count; i++)
        //            {
        //                SetCellDataFormat(sheet.GetRow(i).Cells[13], "yyyy-mm-dd");
        //            }
        //            sheet.AutoSizeColumn(13);
        //        });
        //}
    }
}
