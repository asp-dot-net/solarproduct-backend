﻿using System.Collections.Generic;
using TheSolarProduct.Promotions.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Promotions.Exporting
{
    public interface IPromotionsExcelExporter
    {
        FileDto ExportToFile(List<GetPromotionForViewDto> promotions);

        FileDto ExportSMSEmailListFile(List<PromotionSMSEmailList> promotionSMSEmailList);
    }
}