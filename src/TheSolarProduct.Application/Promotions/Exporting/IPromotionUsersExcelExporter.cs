﻿using System.Collections.Generic;
using TheSolarProduct.Promotions.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Promotions.Exporting
{
    public interface IPromotionUsersExcelExporter
    {
        FileDto ExportToFile(List<GetPromotionUserForViewDto> promotionUsers, string fileName);
        //FileDto ExportCsvToFile(List<GetPromotionUserForViewDto> promotionUsers);
    }
}