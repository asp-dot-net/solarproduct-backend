﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckActives.Dtos;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.CheckActives.Exporting
{
    public class CheckActiveExcelExporter : NpoiExcelExporterBase, ICheckActiveExcelExporter
    {


        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public CheckActiveExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
                                                base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetCheckActiveForViewDto> CheckActives)
        {
            return CreateExcelPackage(
                "CheckActive.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("CheckActive"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("IsActive")
                    );

                    AddObjects(
                        sheet,
                        2,
                        CheckActives,
                        _ => _.CheckActive.Name,
                        _ => _.CheckActive.IsActive ? L("Yes") : L("No")
                    );
                }
            );
        }


    }
}

