﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckActives.Dtos;
using TheSolarProduct.CheckApplications.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.CheckActives.Exporting
{
    public interface ICheckActiveExcelExporter
    {
        FileDto ExportToFile(List<GetCheckActiveForViewDto> CheckActives);
    }
    
}
