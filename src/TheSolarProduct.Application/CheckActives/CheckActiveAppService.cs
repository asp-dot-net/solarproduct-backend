﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization;
using TheSolarProduct.CheckApplications.Dtos;
using TheSolarProduct.CheckApplications.Exporting;
using TheSolarProduct.CheckApplications;
using TheSolarProduct.DataVaults;
using TheSolarProduct.Dto;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.CheckActives.Dtos;
using Abp.Collections.Extensions;
using System.Linq;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using TheSolarProduct.CheckActives.Exporting;


namespace TheSolarProduct.CheckActives
{

    [AbpAuthorize(AppPermissions.Pages_CheckActive)]
    public class CheckActiveAppService : TheSolarProductAppServiceBase, ICheckActiveAppService
    {
        //private readonly ICheckActiveExcelExporter _checkActiveExcelExporter;
        private readonly ICheckActiveExcelExporter _checkActiveExcelExporter;
        private readonly IRepository<CheckActive> _checkActiveRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public CheckActiveAppService(
              ICheckActiveExcelExporter checkActiveExcelExporter,
              IRepository<CheckActive> checkActiveRepository,
              IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
              )
        {
            _checkActiveExcelExporter = checkActiveExcelExporter;
            _checkActiveRepository = checkActiveRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task<PagedResultDto<GetCheckActiveForViewDto>> GetAll(GetAllChcekActivesInput input)
        {

            var CheckActive = _checkActiveRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFilteredChcekApplication = CheckActive
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var outputCheckActive = from o in pagedAndFilteredChcekApplication
                                    select new GetCheckActiveForViewDto()
                                    {
                                        CheckActive = new CheckActiveDto
                                        {
                                            Id = o.Id,
                                            Name = o.Name,
                                            IsActive = o.IsActive
                                        }
                                    };

            var totalCount = await CheckActive.CountAsync();

            return new PagedResultDto<GetCheckActiveForViewDto>(
                totalCount,
                await outputCheckActive.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_CheckActive_Edit)]
        public async Task<GetCheckActiveForEditOutput> GetCheckActiveForEdit(EntityDto input)
        {
            var CheckActive = await _checkActiveRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetCheckActiveForEditOutput { CheckActive = ObjectMapper.Map<CreateOrEditCheckActiveDto>(CheckActive) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditCheckActiveDto input)
        {


            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_CheckActive_Create)]

        protected virtual async Task Create(CreateOrEditCheckActiveDto input)
        {
            var checkActive = ObjectMapper.Map<CheckActive>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 58;
            dataVaultLog.ActionNote = "CheckActive Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _checkActiveRepository.InsertAsync(checkActive);
        }


        [AbpAuthorize(AppPermissions.Pages_CheckActive_Edit)]
        protected virtual async Task Update(CreateOrEditCheckActiveDto input)
        {
            var CheckActive = await _checkActiveRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 58;
            dataVaultLog.ActionNote = "CheckActive Updated : " + CheckActive.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != CheckActive.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = CheckActive.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != CheckActive.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = CheckActive.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, CheckActive);

            await _checkActiveRepository.UpdateAsync(CheckActive);


        }

        [AbpAuthorize(AppPermissions.Pages_CheckActive_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _checkActiveRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 58;
            dataVaultLog.ActionNote = "CheckActive Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _checkActiveRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_CheckActive_Export)]

        public async Task<FileDto> GetChcekActiveToExcel(GetAllCheckActiveForExcelInput input)
        {

            var filteredCheckActive = _checkActiveRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredCheckActive
                         select new GetCheckActiveForViewDto()
                         {
                             CheckActive = new CheckActiveDto
                             {
                                 Name = o.Name,
                                 Id = o.Id,
                                 IsActive = o.IsActive
                             }
                         });


            var ListDtos = await query.ToListAsync();

            return _checkActiveExcelExporter.ExportToFile(ListDtos);
        }
    }

}
