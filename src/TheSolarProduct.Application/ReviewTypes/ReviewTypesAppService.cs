﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ServiceSources.Dtos;
using TheSolarProduct.ServiceSources.Exporting;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.Dto;
using TheSolarProduct.ServiceTypes;
using TheSolarProduct.ServiceTypes.Dtos;
using TheSolarProduct.ReviewTypes;
using TheSolarProduct.ReviewTypes.Dtos;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.Reviews
{
    [AbpAuthorize(AppPermissions.Pages_ReviewType, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    public class ReviewTypesAppService : TheSolarProductAppServiceBase, IReviewTypesAppService
    {
        private readonly IRepository<ReviewType> _reviewTypesRepository;
        private readonly IServiceSourcesExcelExporter _serviceSourcesExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public ReviewTypesAppService(IRepository<ReviewType> reviewTypesRepository, IServiceSourcesExcelExporter serviceSourcesExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _reviewTypesRepository = reviewTypesRepository;
            _serviceSourcesExcelExporter = serviceSourcesExcelExporter;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task<PagedResultDto<GetReviewTypeForViewDto>> GetAll(GetAllReviewTypeInput input)
        {

            var filteredReviewtypes = _reviewTypesRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

            var pagedAndFilteredServicetypes = filteredReviewtypes
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var reviewtype = from o in pagedAndFilteredServicetypes
                              select new GetReviewTypeForViewDto()
                              {
                                  reviewTypes = new ReviewTypeDto
                                  {
                                      Name = o.Name,
                                      ReviewLink = o.ReviewLink,
                                      Id = o.Id,IsActive= o.IsActive,
                                  }
                              };

            var totalCount = await filteredReviewtypes.CountAsync();

            return new PagedResultDto<GetReviewTypeForViewDto>(
                totalCount,
                await reviewtype.ToListAsync()
            );
        }

        public async Task<GetReviewTypeForViewDto> GetReviewTypeForView(int id)
        {
            var reviewTypes = await _reviewTypesRepository.GetAsync(id);

            var output = new GetReviewTypeForViewDto { reviewTypes = ObjectMapper.Map<ReviewTypeDto>(reviewTypes) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_ReviewType_Edit)]
        public async Task<GetReviewTypeForEditOutput> GetReviewTypeForEdit(EntityDto input)
        {
            var reviewTypes = await _reviewTypesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetReviewTypeForEditOutput { reviewTypes = ObjectMapper.Map<CreateOrEditReviewTypeDto>(reviewTypes) };

            return output;
        }
        public async Task CreateOrEdit(CreateOrEditReviewTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_ReviewType_Create)]
        protected virtual async Task Create(CreateOrEditReviewTypeDto input)
        {
            var reviewTypes = ObjectMapper.Map<ReviewType>(input);

            if (AbpSession.TenantId != null)
            {
                reviewTypes.TenantId = (int)AbpSession.TenantId;
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 37;
            dataVaultLog.ActionNote = "Review Type Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _reviewTypesRepository.InsertAsync(reviewTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_ReviewType_Edit)]
        protected virtual async Task Update(CreateOrEditReviewTypeDto input)
        {
            var reviewtypes = await _reviewTypesRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 37;
            dataVaultLog.ActionNote = "Review Type Updated : " + reviewtypes.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != reviewtypes.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = reviewtypes.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.ReviewLink != reviewtypes.ReviewLink)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "ReviewLink";
                history.PrevValue = reviewtypes.ReviewLink;
                history.CurValue = input.ReviewLink;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != reviewtypes.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = reviewtypes.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, reviewtypes);
        }

        [AbpAuthorize(AppPermissions.Pages_ReviewType_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _reviewTypesRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 37;
            dataVaultLog.ActionNote = "Review Type Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _reviewTypesRepository.DeleteAsync(input.Id);
        }
        //public async Task<FileDto> GetServiceTypeToExcel(GetAllServiceTypeForExcelInput input)
        //{

        //    var filteredServiceSources = _reviewTypesRepository.GetAll()
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

        //    var query = (from o in filteredServiceSources
        //                 select new GetServiceTypeForViewDto()
        //                 {
        //                     serviceTypes = new ServiceTypeDto
        //                     {
        //                         Name = o.Name,
        //                         Id = o.Id
        //                     }
        //                 });

        //    var serviceSourcesListDtos = await query.ToListAsync();

        //    return _serviceSourcesExcelExporter.ExportToFile(serviceSourcesListDtos);
        //}
    }
}
