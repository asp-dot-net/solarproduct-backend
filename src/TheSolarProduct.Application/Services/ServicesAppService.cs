﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.Linq.Extensions;
using Abp.Net.Mail;
using Abp.Notifications;
using Abp.Organizations;
using Abp.Timing.Timezone;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net.Mail;
using System.Threading.Tasks;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Common;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Installer;
using TheSolarProduct.JobHistory;
using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Leads;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.Notifications;
using TheSolarProduct.ServiceCategorys;
using TheSolarProduct.ServiceDocs;
using TheSolarProduct.ServiceDocumentRequests.Dtos;
using TheSolarProduct.ServicePrioritys;
using TheSolarProduct.Services.Dtos;
using TheSolarProduct.ServiceStatuses;
using TheSolarProduct.ServiceSubCategorys;
using TheSolarProduct.Storage;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.UserActivityLogs.Dtos;
using TheSolarProduct.UserActivityLogs;

namespace TheSolarProduct.Services
{
    [AbpAuthorize]
    public class ServicesAppService : TheSolarProductAppServiceBase, IServicesAppServicecs
    {

        private const int MaxFileBytes = 5242880; //5MB
        private readonly IRepository<Service> _serviceRepository;
        private readonly IRepository<Lead, int> _lookup_leadRepository;
        private readonly IRepository<Jobs.Job, int> _lookup_JobRepository;
        private readonly IRepository<ServiceCategory> _lookup_ServiceCategorysRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<User, long> _userRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<InstallerDetail> _installerDetailRepository;
        private readonly IRepository<ProductItem> _productItemRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<ServiceDoc> _ServiceDocRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly IRepository<ServicePriority> _servicePriorityRepository;
        private readonly IRepository<ServiceStatus> _serviceStatusRepository;
        private readonly IRepository<ServiceSubCategory> _serviceSubCategoryRepository;
        private readonly IRepository<ServiceInvo> _serviceInvoRepository;
        private readonly IUserActivityLogAppService _userActivityLogServiceProxy;

        public ServicesAppService(IRepository<Service> serviceRepository,
            IRepository<Lead, int> lookup_leadRepository,
            IRepository<ServiceCategory> lookup_ServiceCategorysRepository,
            ITimeZoneConverter timeZoneConverter,
            IRepository<User, long> userRepository,
            UserManager userManager,
            IRepository<UserTeam> userTeamRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IAppNotifier appNotifier,
            IRepository<Jobs.Job, int> lookup_JobRepository,
            IApplicationSettingsAppService applicationSettings,
            IRepository<LeadActivityLog> leadactivityRepository,
            IEmailSender emailSender,
            IRepository<UserRole, long> userRoleRepository,
            IRepository<Role> roleRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
            IRepository<InstallerDetail> installerDetailRepository,
            IRepository<ProductItem> productItemRepository,
            IRepository<JobProductItem> jobProductItemRepository,
            IRepository<ServiceDoc> ServiceDocRepository,
            ITempFileCacheManager tempFileCacheManager,
            IWebHostEnvironment env,
            IRepository<Tenant> tenantRepository,
            ICommonLookupAppService CommonDocumentSaveRepository,
            IRepository<ServicePriority> servicePriorityRepository,
            IRepository<ServiceStatus> serviceStatusRepository,
            IRepository<ServiceSubCategory> serviceSubCategoryRepository
            ,IRepository<ServiceInvo> serviceInvoRepository
            , IUserActivityLogAppService userActivityLogServiceProxy

           )
        {
            _serviceRepository = serviceRepository;
            _lookup_leadRepository = lookup_leadRepository;
            _lookup_ServiceCategorysRepository = lookup_ServiceCategorysRepository;
            _timeZoneConverter = timeZoneConverter;
            _userRepository = userRepository;
            _userManager = userManager;
            _userTeamRepository = userTeamRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _appNotifier = appNotifier;
            _lookup_JobRepository = lookup_JobRepository;
            _applicationSettings = applicationSettings;
            _leadactivityRepository = leadactivityRepository;
            _emailSender = emailSender;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _dbcontextprovider = dbcontextprovider;
            _installerDetailRepository = installerDetailRepository;
            _productItemRepository = productItemRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _ServiceDocRepository = ServiceDocRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _env = env;
            _tenantRepository = tenantRepository;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _serviceRepository = serviceRepository;
            _servicePriorityRepository = servicePriorityRepository;
            _serviceStatusRepository = serviceStatusRepository;
            _serviceSubCategoryRepository = serviceSubCategoryRepository;
            _serviceInvoRepository = serviceInvoRepository;
            _userActivityLogServiceProxy = userActivityLogServiceProxy;
        }
        public async Task CreateOrEditService(CreateOrEditServiceDto input, int sectionID, string Section)
        {
            if (input.Id == null || input.Id == 0)
            {
                await Create(input, sectionID,Section);
            }
            else
            {
                await Update(input, sectionID, Section);
            }
        }

        protected virtual async Task Create(CreateOrEditServiceDto input, int sectionID, string Section)
        {
            input.InstallerDate = _timeZoneConverter.Convert(input.InstallerDate, (int)AbpSession.TenantId);
            input.PickUpDate = _timeZoneConverter.Convert(input.PickUpDate, (int)AbpSession.TenantId);

            var service = ObjectMapper.Map<Service>(input);

            if (AbpSession.TenantId != null)
            {
                service.TenantId = (int)AbpSession.TenantId;
            }
            var Leads = _lookup_leadRepository.GetAll().Where(e => (e.Mobile == input.Mobile || e.Email == input.Email) && e.OrganizationId != 0).FirstOrDefault();
           
            if (input.JobNumber != null)
            {
                if (input.JobNumber.Contains("|"))
                {
                    string[] splitPipeValue = input.JobNumber.Split('|');
                    var jobnumber = splitPipeValue[0].ToString();
                    var job = _lookup_JobRepository.GetAll().Where(e => e.JobNumber == jobnumber).FirstOrDefault();
                    service.JobNumber = jobnumber;
                    service.LeadId = job.LeadId;
                    service.JobId = job.Id;
                }
                else
                {
                    var job = _lookup_JobRepository.GetAll().Where(e => e.JobNumber == input.JobNumber).FirstOrDefault();
                    service.LeadId = job.LeadId;
                    service.JobId = job.Id;
                    service.JobNumber = input.JobNumber;
                }

            }
            else if (Leads != null)
            {
                service.LeadId = Leads.Id;
                service.JobId = _lookup_JobRepository.GetAll().Where(e => e.LeadId == Leads.Id).Select(e => e.Id).FirstOrDefault();
                service.JobNumber = _lookup_JobRepository.GetAll().Where(e => e.Id == service.JobId).Select(e => e.JobNumber).FirstOrDefault();
            }
            else
            {
                service.LeadId = 0;
                service.JobId = 0;
            }
            if (input.IsExternalLead == 2)
            {
                service.ServiceAssignId = input.ServiceAssignId;
                service.ServiceAssignDate = DateTime.UtcNow;
            }
            else
            {
                service.ServiceAssignId = 0;
            }
            Random generator = new Random();
            service.TicketNo = generator.Next(0, 1000000).ToString("D6");
            int serviceId = _serviceRepository.InsertAndGetId(service);
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionNote = "Service Created";
            leadactivity.ActionId = 29;
            leadactivity.SectionId = sectionID;
            leadactivity.LeadId = (int)service.LeadId;
            leadactivity.ServiceId = serviceId;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            leadactivity.CreatorUserId = AbpSession.UserId;
            await _leadactivityRepository.InsertAsync(leadactivity);

            var log = new UserActivityLogDto();
            log.ActionId = 29;
            log.ActionNote = "Service Created" ;
            log.Section = Section;
            await _userActivityLogServiceProxy.Create(log);
        }

        protected virtual async Task Update(CreateOrEditServiceDto input, int sectionID, string Section)
        {
            try
            {
                input.InstallerDate = _timeZoneConverter.Convert(input.InstallerDate, (int)AbpSession.TenantId);
                input.PickUpDate = _timeZoneConverter.Convert(input.PickUpDate, (int)AbpSession.TenantId);

                var service = await _serviceRepository.FirstOrDefaultAsync((int)input.Id);
                var Leads = _lookup_leadRepository.GetAll().Where(e => (e.Mobile == input.Mobile || e.Email == input.Email) && e.OrganizationId != 0).FirstOrDefault();
                if (input.JobNumber != null)
                {
                    if (input.JobNumber.Contains("|"))
                    {
                        string[] splitPipeValue = input.JobNumber.Split('|');
                        var jobnumber = splitPipeValue[0].ToString();
                        var job = _lookup_JobRepository.GetAll().Where(e => e.JobNumber == jobnumber).FirstOrDefault();
                        service.JobNumber = jobnumber;
                        service.LeadId = job.LeadId;
                        service.JobId = job.Id;
                    }
                    else
                    {
                        var job = _lookup_JobRepository.GetAll().Where(e => e.JobNumber == input.JobNumber).FirstOrDefault();
                        service.LeadId = job.LeadId;
                        service.JobId = job.Id;
                        service.JobNumber = input.JobNumber;
                    }
                }
                else if (Leads != null)
                {
                    service.LeadId = Leads.Id;
                    service.JobId = _lookup_JobRepository.GetAll().Where(e => e.LeadId == Leads.Id).Select(e => e.Id).FirstOrDefault();
                    service.JobNumber = _lookup_JobRepository.GetAll().Where(e => e.Id == service.JobId).Select(e => e.JobNumber).FirstOrDefault();
                }
                else
                {
                    service.LeadId = 0;
                    service.JobId = 0;
                }
              
                var actionID = 30; 
                var actionNote = "Service Updated"; 
                if(string.IsNullOrEmpty(service.CaseNotes) && !string.IsNullOrEmpty(input.CaseNotes))
                {
                    if(service.CaseNotes != input.CaseNotes)
                    {
                        actionID = 45;
                        actionNote = "Service Notes Added";
                    }
                }
                else if(service.CaseNotes != input.CaseNotes || service.CaseNotes != input.CaseNotes || service.CaseStatus != input.CaseStatus || service.TrackingNo != input.TrackingNo || service.TrackingNotes != input.TrackingNotes || input.StockArrivalDate != service.StockArrivalDate)
                {
                        actionID = 46;
                        actionNote = "Service Notes updated";
                    
                }
                if(string.IsNullOrEmpty(service.Pickby) && !string.IsNullOrEmpty(input.Pickby))
                {
                    if(service.Pickby != input.Pickby)
                    {
                        actionID = 47;
                        actionNote = "Service Pickup Added";
                        input.ServiceStatusId = 6;

                    }
                }
                else if(service.Pickby != input.Pickby || service.PickUpDate != input.PickUpDate || service.PickupMethod != input.PickupMethod || service.PickupNotes != input.PickupNotes )
                {
                    actionID = 48;
                    actionNote = "Service Pickup updated";
                    input.ServiceStatusId = 6;
                }
                if (service.InstallerDate == null && input.InstallerDate != null)
                {
                    if (service.InstallerDate != input.InstallerDate)
                    {
                        actionID = 47;
                        actionNote = "Service Installer Added";
                        input.ServiceStatusId = 4;

                    }
                }
                else if (service.InstallerDate != input.InstallerDate || service.InstallerId != input.InstallerId || service.InstallerNotes != input.InstallerNotes)
                {
                    actionID = 48;
                    actionNote = "Service Installer updated";
                    input.ServiceStatusId = 4;
                }

                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionNote = actionNote;
                leadactivity.ActionId = actionID;
                leadactivity.SectionId = sectionID;
                leadactivity.LeadId = (int)service.LeadId;
                leadactivity.ServiceId = service.Id;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                leadactivity.CreatorUserId = AbpSession.UserId;
                var jobactionid = _leadactivityRepository.InsertAndGetId(leadactivity);

                var log = new UserActivityLogDto();
                log.ActionId = actionID;
                log.ActionNote = actionNote;
                log.Section = Section;
                await _userActivityLogServiceProxy.Create(log);

                var list = new List<JobTrackerHistory>();
                if (service.ServiceSource != input.ServiceSource)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "ServiceSource";
                    jobhistory.PrevValue = service.ServiceSource;
                    jobhistory.CurValue = input.ServiceSource;
                    jobhistory.Action = "Service ServiceSource";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.State != input.State)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "State";
                    jobhistory.PrevValue = service.State;
                    jobhistory.CurValue = input.State;
                    jobhistory.Action = "Service State";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.ServicePriorityId != input.ServicePriorityId)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "ServicePriority";
                    jobhistory.PrevValue =  _servicePriorityRepository.GetAll().Where(e => e.Id == service.ServicePriorityId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.CurValue = _servicePriorityRepository.GetAll().Where(e => e.Id == input.ServicePriorityId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.Action = "Service ServicePriorityId";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.ServiceStatusId != input.ServiceStatusId)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "ServiceStatus";
                    jobhistory.PrevValue = _serviceStatusRepository.GetAll().Where(e => e.Id == service.ServiceStatusId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.CurValue = _serviceStatusRepository.GetAll().Where(e => e.Id == input.ServiceStatusId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.Action = "Service ServiceStatus";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.Name != input.Name)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Name";
                    jobhistory.PrevValue = service.Name;
                    jobhistory.CurValue = input.Name;
                    jobhistory.Action = "Service Name";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.Mobile != input.Mobile)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Mobile";
                    jobhistory.PrevValue = service.Mobile;
                    jobhistory.CurValue = input.Mobile;
                    jobhistory.Action = "Service Mobile";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.Email != input.Email)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Email";
                    jobhistory.PrevValue = service.Email;
                    jobhistory.CurValue = input.Email;
                    jobhistory.Action = "Service Email";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.Address != input.Address)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Address";
                    jobhistory.PrevValue = service.Address;
                    jobhistory.CurValue = input.Address;
                    jobhistory.Action = "Service Address";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.JobNumber != input.JobNumber)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "JobNumber";
                    jobhistory.PrevValue = service.JobNumber;
                    jobhistory.CurValue = input.JobNumber;
                    jobhistory.Action = "Service JobNumber";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.Notes != input.Notes)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Notes";
                    jobhistory.PrevValue = service.Notes;
                    jobhistory.CurValue = input.Notes;
                    jobhistory.Action = "Service Notes";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.ServiceLine != input.ServiceLine)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "ServiceLine";
                    jobhistory.PrevValue = service.ServiceLine;
                    jobhistory.CurValue = input.ServiceLine;
                    jobhistory.Action = "Service ServiceLine";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.ServiceCategoryName != input.ServiceCategoryName)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "ServiceCategory";
                    jobhistory.PrevValue = service.ServiceCategoryName;
                    jobhistory.CurValue = input.ServiceCategoryName;
                    jobhistory.Action = "Service ServiceCategory";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.ServiceAssignId != input.ServiceAssignId)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "ServiceAssign";
                    jobhistory.PrevValue = _userRepository.GetAll().Where(e => e.Id == service.ServiceAssignId).Select(e => e.FullName).FirstOrDefault();
                    jobhistory.CurValue = _userRepository.GetAll().Where(e => e.Id == input.ServiceAssignId).Select(e => e.FullName).FirstOrDefault();
                    jobhistory.Action = "Service ServiceAssign";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.OtherEmail != input.OtherEmail)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "OtherEmail";
                    jobhistory.PrevValue = service.OtherEmail;
                    jobhistory.CurValue = input.OtherEmail;
                    jobhistory.Action = "Service OtherEmail";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.ServiceSubCategoryId != input.ServiceSubCategoryId)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "ServiceSubCategory";
                    jobhistory.PrevValue = _serviceSubCategoryRepository.GetAll().Where(e => e.Id == service.ServiceSubCategoryId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.CurValue = _serviceSubCategoryRepository.GetAll().Where(e => e.Id == input.ServiceSubCategoryId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.Action = "Service ServiceSubCategory";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.IsWarrantyClaim != input.IsWarrantyClaim)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "IsWarrantyClaim";
                    jobhistory.PrevValue = service.IsWarrantyClaim.ToString();
                    jobhistory.CurValue = input.IsWarrantyClaim.ToString();
                    jobhistory.Action = "Service IsWarrantyClaim";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.CaseNo != input.CaseNo)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "CaseNo";
                    jobhistory.PrevValue = service.CaseNo;
                    jobhistory.CurValue = input.CaseNo;
                    jobhistory.Action = "Service CaseNo";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                 if (service.CaseNotes != input.CaseNotes)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "CaseNotes";
                    jobhistory.PrevValue = service.CaseNotes;
                    jobhistory.CurValue = input.CaseNotes;
                    jobhistory.Action = "Service CaseNotes";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                 if (service.CaseStatus != input.CaseStatus)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "CaseStatus";
                    jobhistory.PrevValue = service.CaseStatus;
                    jobhistory.CurValue = input.CaseStatus;
                    jobhistory.Action = "Service CaseStatus";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                 if (service.TrackingNo != input.TrackingNo)
                 {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "TrackingNo";
                    jobhistory.PrevValue = service.TrackingNo;
                    jobhistory.CurValue = input.TrackingNo;
                    jobhistory.Action = "Service TrackingNo";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                 if (service.TrackingNotes != input.TrackingNotes)
                 {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "TrackingNotes";
                    jobhistory.PrevValue = service.TrackingNotes;
                    jobhistory.CurValue = input.TrackingNotes;
                    jobhistory.Action = "Service TrackingNotes";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                 if (service.Pickby != input.Pickby )
                 {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Pickby";
                    jobhistory.PrevValue = service.Pickby;
                    jobhistory.CurValue = input.Pickby;
                    jobhistory.Action = "Service Pickby";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if  (service.PickUpDate != input.PickUpDate )
                 {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "PickUpDate";
                    jobhistory.PrevValue = service.PickUpDate.ToString();
                    jobhistory.CurValue = input.PickUpDate.ToString();
                    jobhistory.Action = "Service PickUpDate";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if  (service.PickupMethod != input.PickupMethod )
                 {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "PickupMethod";
                    jobhistory.PrevValue = service.PickupMethod;
                    jobhistory.CurValue = input.PickupMethod;
                    jobhistory.Action = "Service PickupMethod";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.PickupNotes != input.PickupNotes)
                 {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "PickupNotes";
                    jobhistory.PrevValue = service.PickupNotes;
                    jobhistory.CurValue = input.PickupNotes;
                    jobhistory.Action = "Service PickupNotes";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.StockArrivalDate != input.StockArrivalDate)
                 {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "StockArrivalDate";
                    jobhistory.PrevValue = service.StockArrivalDate.ToString();
                    jobhistory.CurValue = input.StockArrivalDate.ToString();
                    jobhistory.Action = "Service StockArrivalDate";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (service.InstallerDate != input.InstallerDate )
                 {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "InstallerDate";
                    jobhistory.PrevValue = service.InstallerDate.ToString();
                    jobhistory.CurValue = input.InstallerDate.ToString();
                    jobhistory.Action = "Service InstallerDate";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                
                if (service.InstallerId != input.InstallerId )
                 {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Installer";
                    jobhistory.PrevValue = _userRepository.GetAll().Where(e => e.Id == service.InstallerId).Select(e => e.FullName).FirstOrDefault();
                    jobhistory.CurValue = _userRepository.GetAll().Where(e => e.Id == input.InstallerId).Select(e => e.FullName).FirstOrDefault();
                    jobhistory.Action = "Service Installer";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                
                if (service.InstallerNotes != input.InstallerNotes)
                 {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "InstallerNotes";
                    jobhistory.PrevValue = service.InstallerNotes;
                    jobhistory.CurValue = input.InstallerNotes;
                    jobhistory.Action = "Service InstallerNotes";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = service.JobFk.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                
                if (list.Count > 0)
                {
                    await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
                    await _dbcontextprovider.GetDbContext().SaveChangesAsync();
                }

                ObjectMapper.Map(input, service);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// ManageService Listing
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetServiceForViewDto>> GetAll(GetAllServicessInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            var filteredJobs = _serviceRepository.GetAll()
                               .Include(e => e.LeadFk)
                               //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Email.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.JobNumber.Contains(input.Filter))
                               .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                               .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                               .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Phone == input.Filter)
                               .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                               .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                               .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.State == input.stateNameFilter)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.AdressFilter), e => e.Address == input.AdressFilter)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.ServiceCategory), e => e.ServiceCategoryName == input.ServiceCategory)
                              .Where(e => e.OrganizationId == input.OrganizationUnit && e.IsMergeRecord != true && e.IsDuplicate != true && (e.ServiceAssignId == 0 || e.ServiceAssignId == null));

            var pagedAndFilteredJobs = filteredJobs
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var jobs = from o in pagedAndFilteredJobs
                       join o1 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o1.Id into j1
                       from s1 in j1.DefaultIfEmpty()
                       select new GetServiceForViewDto()
                       {
                           service = new ServiceDto
                           {
                               Id = o.Id,
                               State = o.State,
                               Name = o.Name,
                               Mobile = o.Mobile,
                               Address = o.Address,
                               Email = o.Email,
                               JobNumber = o.JobNumber,
                               Notes = o.Notes,
                               ServiceAssignDate = o.ServiceAssignDate,
                               //ServiceInstaller = o.ServiceInstaller,
                               ServiceLine = o.ServiceLine,
                               ServiceCategoryName = o.ServiceCategoryName,
                               ServiceSource = o.ServiceSource,
                               JobId = o.JobId,
                               LeadId = o.LeadId,
                               TicketNo = o.TicketNo,

                           },
                           OrganizationName = s1 == null || s1.DisplayName == null ? "" : s1.DisplayName.ToString(),
                           IsMerge = _serviceRepository.GetAll().Where(x => x.Email == o.Email && x.Mobile == o.Mobile && x.ServiceCategoryName == o.ServiceCategoryName && x.Id != o.Id && x.OrganizationId == input.OrganizationUnit && x.ServiceAssignId != 0).Any(),
                           Duplicate = (o.IsDuplicate == false) ? false : (o.IsDuplicate == true) ? true : _serviceRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.OrganizationId == input.OrganizationUnit && (e.ServiceAssignId != 0 || e.ServiceAssignId != null)).Any(),
                       };

            var totalCount = await filteredJobs.CountAsync();

            return new PagedResultDto<GetServiceForViewDto>(
                totalCount,
                await jobs.ToListAsync()
            );
        }

        /// <summary>
        /// Get Duplicate Lead Details (Email or Phone)
        /// </summary>
        /// <param name="id">Lead Id</param>
        /// <returns></returns>
        public async Task<List<GetDuplicateServicePopupDto>> GetDuplicateServiceForView(int id, int orgId)
        {
            var lead = await _serviceRepository.GetAsync(id);
            var JobList = _lookup_JobRepository.GetAll();
            var duplicateService = _serviceRepository.GetAll().Where(e => ((e.Email == lead.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == lead.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.OrganizationId == orgId && e.Id != id && (e.ServiceAssignId != 0 || e.ServiceAssignId != null));

            var leads = from o in duplicateService

                        join o2 in _userRepository.GetAll() on o.CreatorUserId equals o2.Id into j2
                        from s2 in j2.DefaultIfEmpty()

                        select new GetDuplicateServicePopupDto()
                        {
                            CreatedByName = s2.FullName, //_userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                            CreationTime = o.CreationTime,
                            Address = o.Address,
                            CompanyName = o.Name,
                            Email = o.Email,
                            Id = o.Id,
                            Mobile = o.Mobile,
                            CurrentAssignUserName = _userRepository.GetAll().Where(e => e.Id == o.ServiceAssignId).Select(e => e.FullName).FirstOrDefault(),
                            State = o.State,
                            ProjectNo = JobList.Where(x => x.Id == o.JobId).Select(e => e.JobNumber).FirstOrDefault(),
                            ProjectStatus = o.JobFk.JobStatusFk.Name,
                            ProjectOpenDate = JobList.Where(x => x.Id == o.JobId).Select(e => e.CreationTime).FirstOrDefault(),
                            ServiceSource = o.ServiceSource,
                            ServiceCategory = o.ServiceCategoryName,
                            ServiceStatus = o.StatusFk.Name,
                            ServicePriority = o.PriorityFk.Name,
                            ServiceSubCategory = o.ServiceSubCategoryFk.Name,
                            OtherEmail = o.OtherEmail,
                            Notes = o.Notes,
                            ServiceLine = o.ServiceLine
                        };

            return new List<GetDuplicateServicePopupDto>(
                await leads.ToListAsync()
            );
        }

        public virtual async Task ChangeDuplicateStatus(int id, bool? IsDuplicate)
        {
            var service = await _serviceRepository.FirstOrDefaultAsync((int)id);
            service.IsDuplicate = IsDuplicate;
            await _serviceRepository.UpdateAsync(service);

            LeadActivityLog leadactivity = new LeadActivityLog();
            if (IsDuplicate == true)
            {
                leadactivity.ActionNote = "Service Status Changed To Duplicate";
            }
            else
            {
                leadactivity.ActionNote = "Service Status Changed To Not Duplicate";
            }

            leadactivity.SectionId = 24;
            leadactivity.ActionId = 5;
            leadactivity.LeadId = (int)service.LeadId;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        public async Task DeleteDuplicateServices(EntityDto input)
        {
            //var lead = await _serviceRepository.GetAsync(input.Id);

            //var duplicateLead = (_serviceRepository.GetAll().Where(e => ((e.Email == lead.Email || e.Mobile == lead.Mobile) && (!string.IsNullOrEmpty(e.Mobile) && !string.IsNullOrEmpty(e.Email))) && e.Id != lead.Id && (e.ServiceAssignId != null || e.ServiceAssignId != 0) && e.OrganizationId == lead.OrganizationId));
            //_leadRepository.GetAll().Where(e => (e.Phone == lead.Phone || e.Mobile == lead.Mobile || e.Email == lead.Email) && e.Id != lead.Id && e.AssignToUserID != null);


            //foreach (var item in duplicateLead)
            //{
                await _serviceRepository.DeleteAsync(input.Id);
            //}
        }

        public async Task mergeServices(int id)
        {
            var service = await _serviceRepository.GetAsync(id);
            var mergeservice = (_serviceRepository.GetAll().AsNoTracking().Where(e => ((e.Email == service.Email || e.Mobile == service.Mobile) && (!string.IsNullOrEmpty(e.Mobile) && !string.IsNullOrEmpty(e.Email))) && e.Id != service.Id && e.ServiceCategoryName == service.ServiceCategoryName && e.OrganizationId == service.OrganizationId && e.ServiceAssignId != 0)).OrderByDescending(e => e.Id).FirstOrDefault();
            mergeservice.IsMergeCount = (mergeservice.IsMergeCount + 1);
            mergeservice.ServicePriorityId = 3;
            mergeservice.Name = service.Name;
            mergeservice.ServiceStatusId = mergeservice.ServiceStatusId;
            mergeservice.ServiceSource = mergeservice.ServiceSource;
            mergeservice.State = service.State;
            mergeservice.Address = service.Address;
            mergeservice.Notes = service.Notes;
            mergeservice.ServiceLine = service.ServiceLine;
            _serviceRepository.UpdateAsync(mergeservice);

            //var deleteMergerecord = (_serviceRepository.GetAll().AsNoTracking().Where(e => ((e.Email == service.Email || e.Mobile == service.Mobile) && (!string.IsNullOrEmpty(e.Mobile) && !string.IsNullOrEmpty(e.Email))) && e.Id != service.Id && e.ServiceCategoryName == service.ServiceCategoryName && e.OrganizationId == service.OrganizationId)).ToList();

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 27;
            leadactivity.SectionId = 25;
            leadactivity.ActionNote = "Service Record Merge";
            leadactivity.LeadId = Convert.ToInt32(service.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            _leadactivityRepository.InsertAndGetId(leadactivity);
        }


        public async Task deletemergeRecords(int id)
        {
            try
            {
                var serviceupdate = _serviceRepository.GetAll().AsNoTracking().Where(x => x.Id == id).FirstOrDefault();
                serviceupdate.IsMergeRecord = true;
                _serviceRepository.Update(serviceupdate);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            //}
        }
        public async Task UpdateAssignUserID(List<int> serviceId, int? userId)
        {
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == userId).FirstOrDefault();
            var assignedFromUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).Select(e => e.FullName).FirstOrDefault();

            foreach (var id in serviceId)
            {
                var service = await _serviceRepository.FirstOrDefaultAsync((int)id);
                service.ServiceAssignId = userId;
                service.ServiceAssignDate = DateTime.UtcNow;
                service.ServiceStatusId = 4;
                if (service.IsMergeCount == null)
                {
                    service.IsMergeCount = 0;
                }
                await _serviceRepository.UpdateAsync(service);

                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionNote = "JobAssign To" + assignedToUser.FullName;
                leadactivity.ActionId = 27;
                leadactivity.SectionId = 22;
                leadactivity.LeadId = (int)service.LeadId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                leadactivity.CreatorUserId = AbpSession.UserId;
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
            string msg = string.Format(serviceId.Count + " Service Assign" + " BY " + assignedFromUser);
            await _appNotifier.LeadAssiged(assignedToUser, msg, NotificationSeverity.Info);
        }


        /// <summary>
        /// Check Lead Existancy (Address, Email, Phone)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<List<GetDuplicateServicePopupDto>> CheckExistServiceList(CreateOrEditServiceDto input)
        {
            var User = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var JobList = _lookup_JobRepository.GetAll();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var Mobile = input.Mobile.Substring(input.Mobile.Length - 9);
            var duplicateService = _serviceRepository.GetAll().Where(e => e.Mobile.Contains(Mobile) || e.Email == input.Email)
                                .Where(e => e.OrganizationId == input.OrganizationId)
                                .WhereIf(input.Id != null, e => e.Id != input.Id)
                                .Where(e => e.IsDuplicate != true);

            var services = from o in duplicateService

                           join o2 in _userRepository.GetAll() on o.CreatorUserId equals o2.Id into j2
                           from s2 in j2.DefaultIfEmpty()

                           select new GetDuplicateServicePopupDto()
                           {
                               CreatedByName = s2.FullName, //_userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                               CreationTime = o.CreationTime,
                               Address = o.Address,
                               CompanyName = o.Name,
                               Email = o.Email,
                               Id = o.Id,
                               Mobile = o.Mobile,
                               CurrentAssignUserName = _userRepository.GetAll().Where(e => e.Id == o.ServiceAssignId).Select(e => e.FullName).FirstOrDefault(),
                               State = o.State,
                               ProjectNo = JobList.Where(x => x.Id == o.JobId).Select(e => e.JobNumber).FirstOrDefault(),
                               ProjectStatus = o.JobFk.JobStatusFk.Name,
                               ProjectOpenDate = JobList.Where(x => x.Id == o.JobId).Select(e => e.CreationTime).FirstOrDefault(),
                               ServiceSource = o.ServiceSource,
                               ServiceCategory = o.ServiceCategoryName,
                               ServiceStatus = o.StatusFk.Name,
                               ServicePriority = o.PriorityFk.Name,
                               EmailExist = o.Email == input.Email,
                               MobileExist = o.Mobile == input.Mobile,
                           };


            return new List<GetDuplicateServicePopupDto>(
                await services.ToListAsync()
            );
        }

        /// <summary>
        /// MyServiceListing
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetServiceForViewDto>> GetAllForMyService(GetAllMyServicessInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            int UserId = (int)AbpSession.UserId;

            var userList = _userRepository.GetAll();

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            var leadactivitylog = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.SectionId == 22);
            //var managerusers = new List<long>();
            //if (role.Contains("Service Manager"))
            //{
            //    managerusers = (from user in userList
            //                    join ur in _userRoleRepository.GetAll() on user.Id equals ur.UserId into urJoined
            //                    from ur in urJoined.DefaultIfEmpty()
            //                    join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
            //                    from us in usJoined.DefaultIfEmpty()
            //                    where (us != null && us.DisplayName == "Service Manager")
            //                    select (user.Id)).Distinct().ToList();
            //}
           
            var filteredMyservice = _serviceRepository.GetAll()
                               .Include(e => e.LeadFk)
                               .Include(e => e.StatusFk)
                               .Include(e => e.PriorityFk)
                                //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Email.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.JobNumber.Contains(input.Filter))
                                .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                               .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                               .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Phone == input.Filter)
                               .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                               .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                               .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.State == input.stateNameFilter)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.AdressFilter), e => e.Address == input.AdressFilter)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.ServiceCategory), e => e.ServiceCategoryName == input.ServiceCategory)
                              .WhereIf(role.Contains("Service Manager") || string.IsNullOrWhiteSpace(input.Filter), e => e.ServiceAssignId != 0)
                              .WhereIf(role.Contains("Admin") || string.IsNullOrWhiteSpace(input.Filter), e => e.ServiceAssignId != 0)
                              .WhereIf(!role.Contains("Service Manager") && !role.Contains("Admin") && string.IsNullOrWhiteSpace(input.Filter), e => e.ServiceAssignId == UserId)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.ServiceSource), e => e.ServiceSource == input.ServiceSource)
                              .WhereIf(input.ServicestatusFilter != 0, e => e.ServiceStatusId == input.ServicestatusFilter)
                              .WhereIf(input.Servicepriority != 0, e => e.ServicePriorityId == input.Servicepriority)
                              .WhereIf(input.DateFilter == "CreationDate", e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                              .WhereIf(input.DateFilter == "ClosedDate", e => e.LastModificationTime.Value.Date >= SDate.Value.Date && e.LastModificationTime.Value.Date <= EDate.Value.Date && e.ServiceStatusId == 6)
                              .Where(e => e.OrganizationId == input.OrganizationUnit);

            var pagedAndFilteredJobs = filteredMyservice
            .OrderBy(input.Sorting ?? "id desc")
            .PageBy(input);

            var myservice = from o in pagedAndFilteredJobs

                            select new GetServiceForViewDto()
                            {
                                service = new ServiceDto
                                {
                                    Id = o.Id,
                                    State = o.State,
                                    Name = o.Name,
                                    Mobile = o.Mobile,
                                    Address = o.Address,
                                    Email = o.Email,
                                    JobNumber = o.JobNumber,
                                    Notes = o.Notes,
                                    ServiceAssignDate = o.ServiceAssignDate,
                                    //ServiceInstaller = o.ServiceInstaller,
                                    ServiceLine = o.ServiceLine,
                                    ServiceCategoryName = o.ServiceCategoryName,
                                    ServiceSource = o.ServiceSource,
                                    JobId = o.JobId,
                                    LeadId = o.LeadId,
                                    OrganizationId = o.OrganizationId,
                                    TicketNo = o.TicketNo,
                                    TenentId = o.TenantId
                                },
                                ReminderTime = leadactivitylog.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId && e.ServiceId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                                ActivityDescription = leadactivitylog.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId && e.ServiceId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                ActivityComment = leadactivitylog.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId && e.ServiceId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                AssignUser = userList.Where(e => e.Id == o.ServiceAssignId).Select(e => e.UserName).FirstOrDefault(),
                                ServiceStatus = o.StatusFk.Name,
                                ServicePriority = o.PriorityFk.Name,
                            };
            var totalCount = await filteredMyservice.CountAsync();

            return new PagedResultDto<GetServiceForViewDto>(
                totalCount,
                myservice.ToList()
            );
        }
        public async Task<GetServiceForCount> GetAllForMyServiceCount(GetAllMyServicessInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            int UserId = (int)AbpSession.UserId;

            var userList = _userRepository.GetAll();

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            var leadactivitylog = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.SectionId == 22);
            //var managerusers = new List<long>();
            //if (role.Contains("Service Manager"))
            //{
            //    managerusers = (from user in userList
            //                    join ur in _userRoleRepository.GetAll() on user.Id equals ur.UserId into urJoined
            //                    from ur in urJoined.DefaultIfEmpty()
            //                    join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
            //                    from us in usJoined.DefaultIfEmpty()
            //                    where (us != null && us.DisplayName == "Service Manager")
            //                    select (user.Id)).Distinct().ToList();
            //}

            var filteredMyservice = _serviceRepository.GetAll()
                               .Include(e => e.LeadFk)
                               .Include(e => e.StatusFk)
                               .Include(e => e.PriorityFk)
                              //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Email.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.JobNumber.Contains(input.Filter))
                              .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                               .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                               .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Phone == input.Filter)
                               .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                               .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                               .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.State == input.stateNameFilter)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.AdressFilter), e => e.Address == input.AdressFilter)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.ServiceCategory), e => e.ServiceCategoryName == input.ServiceCategory)
                              .WhereIf(role.Contains("Service Manager") , e => e.ServiceAssignId != 0)
                              .WhereIf(role.Contains("Admin"), e => e.ServiceAssignId != 0)
                              .WhereIf(!role.Contains("Service Manager") && !role.Contains("Admin"), e => e.ServiceAssignId == UserId)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.ServiceSource), e => e.ServiceSource == input.ServiceSource)
                              .WhereIf(input.ServicestatusFilter != 0, e => e.ServiceStatusId == input.ServicestatusFilter)
                              .WhereIf(input.Servicepriority != 0, e => e.ServicePriorityId == input.Servicepriority)
                              .Where(e => e.OrganizationId == input.OrganizationUnit);

            var output = new GetServiceForCount();
            output.Open = Convert.ToString(filteredMyservice.Where(e => e.ServiceStatusId == 2).Count());
            output.Pending = Convert.ToString(filteredMyservice.Where(e => e.ServiceStatusId == 3).Count());
            output.Assign = Convert.ToString(filteredMyservice.Where(e => e.ServiceStatusId == 4).Count());
            output.Resolved = Convert.ToString(filteredMyservice.Where(e => e.ServiceStatusId == 5).Count());
            output.Closed = Convert.ToString(filteredMyservice.Where(e => e.ServiceStatusId == 6).Count());
            return output;

        }
        
        //[AbpAuthorize(AppPermissions.Pages_FinanceOptions_Edit)]
        public async Task<GetMyServiceForEditOutput> GetserviceForEdit(EntityDto input)
        {
            var servicess = await _serviceRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetMyServiceForEditOutput { service = ObjectMapper.Map<CreateOrEditServiceDto>(servicess) };
            output.service.ServiceAssign = _userRepository.GetAll().Where(e => e.Id == output.service.ServiceAssignId).Select(e => e.FullName).FirstOrDefault();
            return output;
        }

        /// Sms send From allTrackers
        public async Task SendSms(SmsEmailDto input)
        {

            var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)input.LeadId);
            if (!string.IsNullOrEmpty(lead.Mobile))
            {

                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 6;
                leadactivity.ActionNote = "Sms Send From My service";
                leadactivity.LeadId = (int)input.LeadId;
                leadactivity.Subject = input.Body;
                leadactivity.ActivityDate = DateTime.UtcNow;
                leadactivity.SectionId = input.TrackerId;
                leadactivity.ServiceId = input.ServiceId;
                if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                {
                    leadactivity.TemplateId = input.SMSTemplateId;
                }
                else
                {
                    leadactivity.TemplateId = 0;
                }
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                leadactivity.Body = input.Body;
                await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                SendSMSInput sendSMSInput = new SendSMSInput();
                sendSMSInput.PhoneNumber = lead.Mobile;
                sendSMSInput.Text = input.Body;
                sendSMSInput.ActivityId = leadactivity.Id;
                sendSMSInput.SectionID = (int)input.TrackerId;

                await _applicationSettings.SendSMS(sendSMSInput);
            }
        }

        /// Email send 
        public async Task SendEmail(SmsEmailDto input)
        {


            var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)input.LeadId);
            int? TemplateId;
            if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
            {
                TemplateId = input.EmailTemplateId;
            }
            else
            {
                TemplateId = 0;
            }

            //var MainFolder = ApplicationSettingConsts.DocumentPath; //From Const
           
            if (!string.IsNullOrEmpty(lead.Email))
            {
               
                MailMessage mail = new MailMessage
                {
                    From = new MailAddress(input.EmailFrom),//new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                    To = { input.EmailTo },
                    Subject = input.Subject,
                    //EmailBody.TemplateName + "The Solar Product Lead Notification",
                    Body = input.Body,
                    
                //Body = "http://thesolarproduct.com/account/customer-photo",
                    IsBodyHtml = true
                };
                foreach (var file in input.Documents)
                {
                    Attachment images = new Attachment(file.FilePath + "\\" + file.DisplayName);

                    mail.Attachments.Add(images);
                }
                await this._emailSender.SendAsync(mail);

                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 7;
                leadactivity.ActionNote = "Email Send From My Service";
                leadactivity.LeadId = (int)input.LeadId;
                leadactivity.TemplateId = TemplateId;
                leadactivity.Subject = input.Subject;
                leadactivity.Body = input.Body;
                leadactivity.SectionId = input.TrackerId;
                leadactivity.ActivityDate = DateTime.UtcNow;
                leadactivity.ServiceId = input.ServiceId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
        }

        public async Task DeleteManageServices(List<int> serviceId)
        {
            foreach (var item in serviceId)
            {
                await _serviceRepository.DeleteAsync(item);
            }
        }

        public async Task<List<GetServiceMapDto>> GetServiceMap(GetAllInputServiceMapDto input)
        {
            //var InstallerUser = await _userRepository.GetAllListAsync();
            try
            {

                var InstallerUser = _userRepository.GetAll().Select(e => e.Id).ToList();
                var installerpostcode = _installerDetailRepository.GetAll().Where(j => InstallerUser.Contains(j.UserId)).Select(e => Convert.ToInt32(e.PostCode)).ToList();
                var jobList = _lookup_JobRepository.GetAll();
                var Jobpostcode = jobList.Where(j => j.JobStatusId == 6).Select(e => Convert.ToInt32(e.PostalCode)).ToList();

                var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

                var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
                IList<string> role = await _userManager.GetRolesAsync(User);
                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
                var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
                var ProductItemList = _productItemRepository.GetAll();
                var PanelDetails = new List<int?>();
                if (input.Panelmodel != null)
                {
                    var Panel = ProductItemList.Where(e => e.ProductTypeId == 1 && e.Model.Contains(input.Panelmodel)).Select(e => e.Id).FirstOrDefault();
                    PanelDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Panel).Select(e => e.JobId).ToList();
                }

                var InvertDetails = new List<int?>();
                if (input.Invertmodel != null)
                {
                    var Inverter = ProductItemList.Where(e => e.ProductTypeId == 2 && e.Model.Contains(input.Invertmodel)).Select(e => e.Id).FirstOrDefault();
                    InvertDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Inverter).Select(e => e.JobId).ToList();
                }


                DateTime currentDate = DateTime.Now;
                DateTime currentnewDate = currentDate.Date.AddDays(-30);


                var filteredJobs = _serviceRepository.GetAll()
                     //.WhereIf(!string.IsNullOrWhiteSpace(input.FilterText), e => false || e.JobNumber.Contains(input.FilterText) || e.LeadFk.Mobile.Contains(input.FilterText) || e.LeadFk.Email.Contains(input.FilterText) || e.LeadFk.Phone.Contains(input.FilterText) || e.LeadFk.CompanyName.Contains(input.FilterText))
                     .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobNumber.Contains(input.FilterText))
                    .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.LeadFk.Mobile.Contains(input.FilterText))
                    .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.LeadFk.Phone.Contains(input.FilterText))
                    .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.LeadFk.CompanyName.Contains(input.FilterText))
                    .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.LeadFk.Email.Contains(input.FilterText))
                    .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.Address.Contains(input.FilterText))
                    .WhereIf(input.HousetypeId != 0, e => e.JobFk.HouseTypeId == input.HousetypeId)
                    .WhereIf(input.Rooftypeid != 0, e => e.JobFk.RoofTypeId == input.Rooftypeid)
                    .WhereIf(input.Jobstatusid != 0, e => e.JobFk.JobStatusId == input.Jobstatusid)
                    .WhereIf(input.Paymenttypeid != 0, e => e.JobFk.PaymentOptionId == input.Paymenttypeid)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.JobFk.PostalCode, input.PostalCodeFrom) >= 0)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.JobFk.PostalCode, input.PostalCodeTo) <= 0)
                    /////e => e.PostalCode >= input.PostalCodeFrom && e.PostalCode <= input.PostalCodeTo)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Panelmodel), e => PanelDetails.Contains(e.JobId))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Invertmodel), e => InvertDetails.Contains(e.JobId))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.SteetAddressFilter), e => e.LeadFk.Address == input.SteetAddressFilter)
                    .WhereIf(input.DateFilterType == "Service" && input.StartDate != null && input.EndDate != null, e => e.InstallerDate.Value.Date.AddHours(10).Date >= SDate.Value.Date && e.InstallerDate.Value.Date.AddHours(10).Date <= EDate.Value.Date)
                    .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.InstallerDate != null);

                var jobs = (from o in filteredJobs

                            join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                            from s6 in j6.DefaultIfEmpty()

                            let test = getDistanceFromLatLonInKm(o.JobFk.Latitude == null ? s6.latitude : o.JobFk.Latitude, o.JobFk.Longitude == null ? s6.longitude : o.JobFk.Longitude, o.State)
                            let nearest2postcode = getNearest(o.JobFk.PostalCode, installerpostcode)
                            let nearest2jobscode = getNearestJobPostalCode(o.JobFk.PostalCode, Jobpostcode)
                            let first = nearest2postcode.First()
                            let last = nearest2postcode.Last()
                            let firstjobcode = nearest2jobscode.First()
                            let lastjobcode = nearest2jobscode.Last()
                            select new GetServiceMapDto()
                            {
                                LeadId = s6.Id,
                                label = o.JobNumber,
                                Company = s6.CompanyName + " - ",
                                Status = o.JobFk.JobStatusFk.Name,
                                Adress = o.Address + " " + o.JobFk.Suburb + " " + o.JobFk.State + " " + o.JobFk.PostalCode,
                                State = o.State,
                                PhoneEmail = s6.Mobile + '/' + s6.Email,
                                DepRecDate = o.JobFk.DepositeRecceivedDate,
                                ActiveDate = o.JobFk.ActiveDate,
                                HouseType = o.JobFk.HouseTypeFk.Name,
                                RoofType = o.JobFk.RoofTypeFk.Name,
                                AreaType = o.LeadFk.Area,
                                Latitude = o.JobFk.Latitude == null ? s6.latitude : o.JobFk.Latitude,
                                Longitude = o.JobFk.Longitude == null ? s6.longitude : o.JobFk.Longitude,
                                Distance = test,
                                ProductIteams = (from o in _jobProductItemRepository.GetAll().Where(e => e.JobId == o.Id)
                                                 join o2 in ProductItemList on o.ProductItemId equals o2.Id
                                                 select new GetJobProductItemForEditOutput()
                                                 {
                                                     JobNote = o2.ProductTypeFk.Name,
                                                     ProductItemName = o2.Name,
                                                     Model = o2.Model,
                                                     Quantity = o.Quantity
                                                 }).ToList(),
                                postalcodesss = nearest2postcode,
                                //FirstNearestInstallerName = firstuser,
                                fistpostalcode = first,
                                secondpostalcode = last,
                                fistJob = firstjobcode,
                                secondJob = lastjobcode,
                                Id = o.Id,
                                JobStatusId = o.JobFk.JobStatusId,
                            }).ToList();

                foreach (var item in jobs)
                {
                    item.FirstNearestInstallerName = _userRepository.GetAll().Where(e => e.Id == (_installerDetailRepository.GetAll().Where(j => j.PostCode == item.fistpostalcode).Select(j => j.UserId).Distinct().FirstOrDefault())).Select(e => e.FullName).FirstOrDefault();
                    item.SecondNearestInstallerName = _userRepository.GetAll().Where(e => e.Id == (_installerDetailRepository.GetAll().Where(j => j.PostCode == item.secondpostalcode).Select(j => j.UserId).Distinct().FirstOrDefault())).Select(e => e.FullName).FirstOrDefault();
                    item.fistJob = jobList.Where(e => e.PostalCode == item.fistJob).Select(e => e.JobNumber).FirstOrDefault();
                    item.secondJob = jobList.Where(e => e.PostalCode == item.secondJob).Select(e => e.JobNumber).FirstOrDefault();
                        
                }

                return jobs;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static double getDistanceFromLatLonInKm(string lat1, string lon1, string States)
        {
            if (lat1 != "" && lon1 != "")
            {
                var R = 6371; // Radius of the earth in km
                var lat2 = 0.0;
                var lon2 = 0.0;

                if (States == "QLD")
                {
                    lat2 = -27.580041680084136;
                    lon2 = 153.03702238177127;
                }
                if (States == "VIC")
                {
                    lat2 = -37.81759262390686;
                    lon2 = 144.8550323686331;
                }
                if (States == "NSW")
                {
                    lat2 = -33.74722076106781;
                    lon2 = 150.90180287033405;
                }
                if (States == "SA")
                {
                    lat2 = -34.828917615461556;
                    lon2 = 138.600999853177;
                }
                if (States == "WA")
                {
                    lat2 = -32.06865473895976;
                    lon2 = 115.9103034972597;
                }

                var d1 = Convert.ToDouble(lat1) * (Math.PI / 180.0);
                var num1 = Convert.ToDouble(lon1) * (Math.PI / 180.0);
                var d2 = lat2 * (Math.PI / 180.0);
                var num2 = lon2 * (Math.PI / 180.0) - num1;
                var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) +
                         Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);
                var ans = 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
                return Math.Round((ans / 1000), 2);
            }
            else
            {
                return 0.00;
            }
        }

        private static List<string> getNearest(string post, List<int> installerpostcode)
        {
            var s = Convert.ToInt32(post);
            var Listpostcodes = installerpostcode.OrderBy(x => Math.Abs(x - s)).Take(2).ToList();
            var nearest2postcode = (from i in Listpostcodes select i.ToString()).ToList();
            return nearest2postcode;
        }
        private static List<string> getNearestJobPostalCode(string post, List<int> jobpostcode)
        {
            var s = Convert.ToInt32(post);
            var Listpostcodes = jobpostcode.OrderBy(x => Math.Abs(x - s)).Take(2).ToList();
            var nearest2jobpost = (from i in Listpostcodes select i.ToString()).ToList();
            return nearest2jobpost;
        }
        
        public async Task<List<GetMyServiceDocs>> Getallservicedoc(int? id)
        {
            var servicess = (from o in _ServiceDocRepository.GetAll().Where(e => e.ServiceId == id)
                                      select new GetMyServiceDocs
                                      {
                                          serviceId = o.Id,
                                          FileName = o.FileName,
                                          FilePath = o.FilePath,
                                          CreationTime = o.CreationTime,
                                          CreationUser = _userRepository.GetAll().Where(e=>e.Id == o.CreatorUserId).Select(e=>e.FullName).FirstOrDefault(),
                                      }).ToList();

            return servicess;
        }

        public async Task SaveServiceDocument(string FileToken, string FileName, int id)
        {
                var fileBytes = _tempFileCacheManager.GetFile(FileToken);

            var ext = Path.GetExtension(FileName);

           
                string FileNames = DateTime.Now.Ticks + "_" + "Service" + ext;
            

            byte[] ByteArray = _tempFileCacheManager.GetFile(FileToken);
            var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(fileBytes, FileNames, "Service", 0, 0, AbpSession.TenantId);

            ServiceDoc serviceDocs = new ServiceDoc();
            serviceDocs.FileName = FileNames;
            serviceDocs.FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\Service\\";
            serviceDocs.ServiceId = id;
            serviceDocs.TenantId = (int)AbpSession.TenantId;
            await _ServiceDocRepository.InsertAsync(serviceDocs);

        }

        public async Task DeleteServiceDoc(int? id)
        {
            var docs = _ServiceDocRepository.GetAll().Where(e => e.Id == id).FirstOrDefault();

            var Path = ApplicationSettingConsts.DocumentPath;
            var fileSavePath = Path + docs.FilePath + docs.FileName;
            if (System.IO.File.Exists(fileSavePath))
            {
                try
                {
                    await _ServiceDocRepository.DeleteAsync((int)id);
                    System.IO.File.Delete(fileSavePath);
                }
                catch (Exception ex)
                {
                    Exception e;
                }
            }
        }



        public List<ServiceInstallationCalendarDto> GetServiceInstallation(int installerId, DateTime CalendarStartDate, int? orgID, string areaNameFilter, string state)
        {
            var dates = new List<DateTime>();
            var startDate = (_timeZoneConverter.Convert(CalendarStartDate, (int)AbpSession.TenantId));
            var endDate = startDate.Value.AddDays(6).Date;
            for (var dt = startDate; dt <= endDate; dt = dt.Value.AddDays(1))
            {
                dates.Add(dt.Value.Date);
            }
            var installationList = (from dt in dates
                                    select new ServiceInstallationCalendarDto
                                    {
                                        InstallationDate = dt.Date,
                                        InstallationCount = _serviceRepository.GetAll()
                                                .WhereIf(installerId > 0, x => x.InstallerId == installerId)
                                                .WhereIf(!string.IsNullOrWhiteSpace(state), e => e.LeadFk.State == state)
                                                .WhereIf(!string.IsNullOrWhiteSpace(areaNameFilter), e => e.LeadFk.Area == areaNameFilter)
                                                .Where(x => x.InstallerDate.Value.Date == Convert.ToDateTime(dt.Date.ToShortDateString()) && x.OrganizationId == orgID).Count(),

                                        Jobs = (from jobs in _serviceRepository.GetAll().Where(x => x.InstallerDate.Value.Date == Convert.ToDateTime(dt.Date.ToShortDateString()))
                                                .WhereIf(installerId > 0, x => x.InstallerId == installerId)
                                                .WhereIf(!string.IsNullOrWhiteSpace(areaNameFilter), e => e.LeadFk.Area == areaNameFilter)
                                                .WhereIf(!string.IsNullOrWhiteSpace(state), e => e.LeadFk.State == state)
                                                .WhereIf(orgID != 0, x => x.OrganizationId == orgID)
                                                select new ServiceInstallationJobsDto
                                                {
                                                    JobId = jobs.Id,
                                                    LeadId = jobs.LeadId,
                                                    JobNumber = jobs.JobNumber,
                                                    Suburb = jobs.JobFk.Suburb,
                                                    PostalCode = jobs.JobFk.PostalCode,
                                                    State = jobs.State,
                                                    Address = jobs.Address,
                                                    InstallationTime = jobs.InstallerDate.ToString(),
                                                    ServiceSource = jobs.ServiceSource,
                                                    ServiceStatus = jobs.StatusFk.Name,
                                                    ServicePriority = jobs.PriorityFk.Name,
                                                    ServiceCategoryName = jobs.ServiceCategoryName,
                                                    InstallerName = _userRepository.GetAll().Where(x => x.Id == jobs.InstallerId).FirstOrDefault().FullName,
                                                    CustomerName = _lookup_leadRepository.GetAll().Where(x => x.Id == jobs.LeadId).FirstOrDefault().CompanyName
                                                }).ToList()
                                    });


            return new List<ServiceInstallationCalendarDto>(
                installationList.ToList()
            );
        }

        public async Task DeleteMyServices(int serviceId)
        {
          await _serviceRepository.DeleteAsync(serviceId);
        }

        public async Task<PagedResultDto<GetServiceForViewDto>> GetAllForWarrentyClaimMyService(GetAllMyServicessInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            int UserId = (int)AbpSession.UserId;

            var userList = _userRepository.GetAll();

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            var leadactivitylog = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.SectionId == 33);
            //var managerusers = new List<long>();
            //if (role.Contains("Service Manager"))
            //{
            //    managerusers = (from user in userList
            //                    join ur in _userRoleRepository.GetAll() on user.Id equals ur.UserId into urJoined
            //                    from ur in urJoined.DefaultIfEmpty()
            //                    join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
            //                    from us in usJoined.DefaultIfEmpty()
            //                    where (us != null && us.DisplayName == "Service Manager")
            //                    select (user.Id)).Distinct().ToList();
            //}
            var invoiceService = _serviceInvoRepository.GetAll().Select(e => e.ServiceID).ToList();

            var filteredMyservice = _serviceRepository.GetAll()
                               .Include(e => e.LeadFk)
                               .Include(e => e.StatusFk)
                               .Include(e => e.PriorityFk)
                              //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Email.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.JobNumber.Contains(input.Filter))
                              .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                              .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                              .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Phone == input.Filter)
                              .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                              .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                              .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.State == input.stateNameFilter)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.AdressFilter), e => e.Address == input.AdressFilter)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.ServiceCategory), e => e.ServiceCategoryName == input.ServiceCategory)
                              .WhereIf(role.Contains("Service Manager") || string.IsNullOrWhiteSpace(input.Filter), e => e.ServiceAssignId != 0)
                              .WhereIf(role.Contains("Admin") || string.IsNullOrWhiteSpace(input.Filter), e => e.ServiceAssignId != 0)
                              .WhereIf(!role.Contains("Service Manager") && !role.Contains("Admin") && string.IsNullOrWhiteSpace(input.Filter), e => e.ServiceAssignId == UserId)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.ServiceSource), e => e.ServiceSource == input.ServiceSource)
                              .WhereIf(input.ServicestatusFilter != 0, e => e.ServiceStatusId == input.ServicestatusFilter)
                              .WhereIf(input.Servicepriority != 0, e => e.ServicePriorityId == input.Servicepriority)
                              .WhereIf(input.DateFilter == "CreationDate", e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                              .WhereIf(input.DateFilter == "ClosedDate", e => e.LastModificationTime.Value.Date >= SDate.Value.Date && e.LastModificationTime.Value.Date <= EDate.Value.Date && e.ServiceStatusId == 6)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.InvoiceCreated) && input.InvoiceCreated == "Yes", e => invoiceService.Contains(e.Id))
                              .WhereIf(!string.IsNullOrWhiteSpace(input.InvoiceCreated) && input.InvoiceCreated == "No", e => !invoiceService.Contains(e.Id))
                              .Where(e => e.OrganizationId == input.OrganizationUnit && e.IsWarrantyClaim == true);

            var pagedAndFilteredJobs = filteredMyservice
            .OrderBy(input.Sorting ?? "id desc")
            .PageBy(input);

            var myservice = from o in pagedAndFilteredJobs

                            select new GetServiceForViewDto()
                            {
                                service = new ServiceDto
                                {
                                    Id = o.Id,
                                    State = o.State,
                                    Name = o.Name,
                                    Mobile = o.Mobile,
                                    Address = o.Address,
                                    Email = o.Email,
                                    JobNumber = o.JobNumber,
                                    Notes = o.Notes,
                                    ServiceAssignDate = o.ServiceAssignDate,
                                    //ServiceInstaller = o.ServiceInstaller,
                                    ServiceLine = o.ServiceLine,
                                    ServiceCategoryName = o.ServiceCategoryName,
                                    ServiceSource = o.ServiceSource,
                                    JobId = o.JobId,
                                    LeadId = o.LeadId,
                                    OrganizationId = o.OrganizationId,
                                    TicketNo = o.TicketNo,
                                    TenentId = o.TenantId
                                },
                                ReminderTime = leadactivitylog.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId && e.ServiceId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                                ActivityDescription = leadactivitylog.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId && e.ServiceId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                ActivityComment = leadactivitylog.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId && e.ServiceId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                AssignUser = userList.Where(e => e.Id == o.ServiceAssignId).Select(e => e.UserName).FirstOrDefault(),
                                ServiceStatus = o.StatusFk.Name,
                                ServicePriority = o.PriorityFk.Name,
                            };
            var totalCount = await filteredMyservice.CountAsync();

            return new PagedResultDto<GetServiceForViewDto>(
                totalCount,
                myservice.ToList()
            );
        }

        public async Task<GetServiceForCount> GetAllForWarrentyClaimMyServiceCount(GetAllMyServicessInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            int UserId = (int)AbpSession.UserId;

            var userList = _userRepository.GetAll();

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            var leadactivitylog = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.SectionId == 22);
            //var managerusers = new List<long>();
            //if (role.Contains("Service Manager"))
            //{
            //    managerusers = (from user in userList
            //                    join ur in _userRoleRepository.GetAll() on user.Id equals ur.UserId into urJoined
            //                    from ur in urJoined.DefaultIfEmpty()
            //                    join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
            //                    from us in usJoined.DefaultIfEmpty()
            //                    where (us != null && us.DisplayName == "Service Manager")
            //                    select (user.Id)).Distinct().ToList();
            //}
            var invoiceService = _serviceInvoRepository.GetAll().Select(e => e.ServiceID).ToList();

            var filteredMyservice = _serviceRepository.GetAll()
                               .Include(e => e.LeadFk)
                               .Include(e => e.StatusFk)
                               .Include(e => e.PriorityFk)
                               //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Email.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.JobNumber.Contains(input.Filter))
                               .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                              .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                              .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Phone == input.Filter)
                              .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                              .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                              .WhereIf(input.FilterName == "Address" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Address == input.Filter)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.State == input.stateNameFilter)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.AdressFilter), e => e.Address == input.AdressFilter)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.ServiceCategory), e => e.ServiceCategoryName == input.ServiceCategory)
                              .WhereIf(role.Contains("Service Manager"), e => e.ServiceAssignId != 0)
                              .WhereIf(role.Contains("Admin"), e => e.ServiceAssignId != 0)
                              .WhereIf(!role.Contains("Service Manager") && !role.Contains("Admin"), e => e.ServiceAssignId == UserId)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.ServiceSource), e => e.ServiceSource == input.ServiceSource)
                              .WhereIf(input.ServicestatusFilter != 0, e => e.ServiceStatusId == input.ServicestatusFilter)
                              .WhereIf(input.Servicepriority != 0, e => e.ServicePriorityId == input.Servicepriority)
                              .WhereIf(!string.IsNullOrWhiteSpace(input.InvoiceCreated) && input.InvoiceCreated == "Yes", e => invoiceService.Contains(e.Id))
                              .WhereIf(!string.IsNullOrWhiteSpace(input.InvoiceCreated) && input.InvoiceCreated == "No", e => !invoiceService.Contains(e.Id))
                              .Where(e => e.OrganizationId == input.OrganizationUnit && e.IsWarrantyClaim == true);

            var output = new GetServiceForCount();
            output.Open = Convert.ToString(filteredMyservice.Where(e => e.ServiceStatusId == 2).Count());
            output.Pending = Convert.ToString(filteredMyservice.Where(e => e.ServiceStatusId == 3).Count());
            output.Assign = Convert.ToString(filteredMyservice.Where(e => e.ServiceStatusId == 4).Count());
            output.Resolved = Convert.ToString(filteredMyservice.Where(e => e.ServiceStatusId == 5).Count());
            output.Closed = Convert.ToString(filteredMyservice.Where(e => e.ServiceStatusId == 6).Count());
            return output;

        }

        public Task DeleteServiceTempFolder(List<ServiceDocumnetLookupDto> Documents)
        {           
            
            var fileNameList = Documents.Where(e => e.Filetype == "M").ToList();
            foreach (var file in fileNameList)
            {
                if (System.IO.File.Exists(file.FilePath + file.DisplayName))
                {                                                                                                                                                                                                                                            
                    // If file found, delete it    
                    System.IO.File.Delete(file.FilePath + file.DisplayName);
                   
                }
            }
            return Task.CompletedTask;
        }
    }
}
