﻿using Abp.Domain.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;
using TheSolarProduct.ActivityLogs.Dto;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Invoices;
using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Leads;
using TheSolarProduct.Organizations;
using TheSolarProduct.ReviewTypes;
using TheSolarProduct.Sections;
using System.Net.Mail;
using Abp.Net.Mail;
using System.Collections.Generic;
using Abp.Notifications;
using TheSolarProduct.Notifications;
using TheSolarProduct.WholeSaleLeadActivityLogs;
using Abp.UI;
using System.Diagnostics;
using Abp.Timing.Timezone;
using System.Data.Entity;
using Hangfire.Storage;
using Abp.Authorization;
using Stripe;
using TheSolarProduct.Installer;
using TheSolarProduct.SmsTemplates;
using TheSolarProduct.EmailTemplates;

namespace TheSolarProduct.ActivityLogs
{
    [AbpAuthorize]
    public class ActivityLogAppService : TheSolarProductAppServiceBase, IActivityLogAppService
    {
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<ProductItem> _ProductItemRepository;
        private readonly IRepository<InvoiceImportData> _InvoiceImportDataRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendedOrganizationUnitRepository;
        private readonly IRepository<ReviewType> _reviewTypeRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IRepository<JobPromotion> _jobPromotionRepository;
        private readonly IRepository<FreebieTransport> _freebieTransportRepository;
        private readonly IRepository<PromotionMaster> _promotionMasterRepository;
        private readonly IRepository<JobRefund> _jobRefundRepository;
        private readonly IRepository<Section> _SectionRepository;
        private readonly IRepository<InstallerInvoice> _jobInstallerInvoiceRepository;
        private readonly IEmailSender _emailSender;
        private readonly IAppNotifier _appNotifier;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<EmailTempData> _EmailTempDataRepository;
        private readonly IRepository<InstallerDetail, int> _installerDetail;
        private readonly IRepository<SmsTemplate> _smsTemplateRepository;
        private readonly IRepository<EmailTemplate> _emailTemplateRepository;


        public ActivityLogAppService(
            IRepository<Lead> leadRepository,
            IRepository<User, long> userRepository,
            IRepository <Job> jobRepository,
            IRepository<JobProductItem> jobProductItemRepository,
            IRepository<ProductItem> ProductItemRepository,
            IRepository<InvoiceImportData> InvoiceImportDataRepository,
            IRepository<ExtendOrganizationUnit, long> extendedOrganizationUnitRepository,
            IRepository<ReviewType> reviewTypeRepository,
            IRepository<LeadActivityLog> leadactivityRepository,
            IApplicationSettingsAppService applicationSettings,
            IRepository<JobPromotion> jobPromotionRepository,
            IRepository<FreebieTransport> freebieTransportRepository,
            IRepository<PromotionMaster> promotionMasterRepository,
            IRepository<JobRefund> jobRefundRepository,
            IRepository<Section> SectionRepository,
            IRepository<InstallerInvoice> jobInstallerInvoiceRepository,
            IEmailSender emailSender,
            IAppNotifier appNotifier,
            ITimeZoneConverter timeZoneConverter,
            IRepository<EmailTempData> EmailTempDataRepository,
            IRepository<InstallerDetail, int> installerDetail,
            IRepository<SmsTemplate> smsTemplateRepository,
            IRepository<EmailTemplate> emailTemplateRepository
            ) 
        { 
            _leadRepository = leadRepository;
            _userRepository = userRepository;
            _jobRepository = jobRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _ProductItemRepository = ProductItemRepository;
            _InvoiceImportDataRepository = InvoiceImportDataRepository;
            _extendedOrganizationUnitRepository = extendedOrganizationUnitRepository;
            _reviewTypeRepository = reviewTypeRepository;
            _leadactivityRepository = leadactivityRepository;
            _applicationSettings = applicationSettings;
            _jobPromotionRepository = jobPromotionRepository;
            _freebieTransportRepository = freebieTransportRepository;
            _promotionMasterRepository = promotionMasterRepository;
            _jobRefundRepository = jobRefundRepository;
            _SectionRepository = SectionRepository;
            _jobInstallerInvoiceRepository = jobInstallerInvoiceRepository;
            _emailSender = emailSender;
            _appNotifier = appNotifier;
            _timeZoneConverter = timeZoneConverter;
            _EmailTempDataRepository = EmailTempDataRepository;
            _installerDetail = installerDetail;
            _smsTemplateRepository = smsTemplateRepository;
            _emailTemplateRepository = emailTemplateRepository;
        }

        public async Task<GetLeadForSMSEmailTemplateDto> GetLeadForSMSEmailTemplate(int id, int? JobPromoId, int? InstallInoiceId)
        {
            var user = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).Select(e => new { e.Id, e.EmailAddress, e.Mobile, e.Name }).FirstOrDefault();

            var lead = await _leadRepository.GetAsync(id);
            var _lookupUserStatus = await _userRepository.FirstOrDefaultAsync((int)lead.AssignToUserID);
            var job_data = _jobRepository.GetAll()
                .Include(e => e.PaymentOptionFk)
                .Include(e => e.FinanceOptionFk)
                .Include(e => e.DepositOptionFk)
                .Where(e => e.LeadId == id)
                .Select(e => new { e.Id, e.ReviewType, e.InstallerId, e.JobNumber, e.SystemCapacity, e.TotalCost, e.InspectionDate, PaymentOption = e.PaymentOptionFk.Name, FinPaymentType= e.FinanceOptionFk.Name,
                    DepositOption = e.DepositOptionFk.Name, e.FinancePurchaseNo, e.FinanceAmount, e.FinanceDepositeAmount, e.FinanceNetAmount, e.PaymentTerm, e.RepaymentAmt}).FirstOrDefault();
            var ReviewLink = "";
            var _lookupInstallerName = "";
            decimal? InvoiceAmount = 0;
            decimal? ApproedAmount = 0;
            decimal owning = 0;

            var org = _extendedOrganizationUnitRepository.Get(lead.OrganizationId);
            var qunityAndModelList = "";

            if (job_data != null)
            {
                ReviewLink = job_data.ReviewType != null ? _reviewTypeRepository.GetAll().Where(e => e.Id == job_data.ReviewType).Select(e => e.ReviewLink).FirstOrDefault() : "";
                if (job_data.InstallerId != null)
                {
                    var ins = await _userRepository.FirstOrDefaultAsync((int)job_data?.InstallerId);
                    _lookupInstallerName = ins !=null ? ins.FullName: "";

                }
                owning = (decimal)_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == job_data.Id).Select(e => e.PaidAmmount).Sum();
                var JobProductIteam = _jobProductItemRepository.GetAll().Where(e => e.JobId == job_data.Id).Select(e => new { e.Id, e.ProductItemId, e.Quantity }).ToList();
                if (JobProductIteam.Count > 0)
                {
                    var QunityAndModelList = (from o in JobProductIteam
                                              join o1 in _ProductItemRepository.GetAll() on o.ProductItemId equals o1.Id into j1
                                              from s1 in j1.DefaultIfEmpty()
                                              select new
                                              {
                                                  Name = o.Quantity + " x " + s1.Name
                                              }).ToList();
                    qunityAndModelList = String.Join(",", QunityAndModelList.Select(p => p.Name));

                }

            }



            var dto = new GetLeadForSMSEmailTemplateDto
            {
                Id = lead.Id,
                CompanyName = lead.CompanyName,
                Mobile = lead.Mobile,
                Email = lead.Email,
                Phone = lead.Phone,
                Address = lead.Address + " " + lead.Suburb + ", " + lead.State + " - " + lead.PostCode,
                CurrentAssignUserName = _lookupUserStatus?.FullName?.ToString(),
                CurrentAssignUserEmail = _lookupUserStatus?.EmailAddress?.ToString(),
                CurrentAssignUserMobile = _lookupUserStatus?.PhoneNumber?.ToString(),
                CurrentAssignUserCimetLink = _lookupUserStatus?.CimetLink,
                JobNumber = job_data?.JobNumber,
                SystemCapacity = job_data?.SystemCapacity != null ? job_data?.SystemCapacity + " KW" : job_data?.SystemCapacity.ToString(),
                QunityAndModelList = qunityAndModelList,
                TotalQuotaion = job_data?.TotalCost.ToString(),
                InstallationDate = job_data?.InspectionDate,
                InstallerName = _lookupInstallerName,
                UserName = user.Name,
                UserEmail = user.EmailAddress,
                UserMobile = user.Mobile,
                Owing = (job_data?.TotalCost - owning)?.ToString(),
                Orglogo = org.LogoFilePath + org.LogoFileName,
                OrgEmail = org.Email,
                OrgMobile = org.Mobile,
                OrgName = org.DisplayName,
                Link = ReviewLink,
                jobId = job_data?.Id,
                PaymentOption = job_data?.PaymentOption,
                FinPaymentType = job_data?.FinPaymentType,
                DepositOption = job_data?.DepositOption,
                FinancePurchaseNo = job_data?.FinancePurchaseNo ,
                FinanceAmount = job_data?.FinanceAmount,
                FinanceDepositeAmount = job_data?.FinanceDepositeAmount,
                FinanceNetAmount = job_data?.FinanceNetAmount,
                PaymentTerm = job_data?.PaymentTerm,
                RepaymentAmount = job_data?.RepaymentAmt

            };

            if (JobPromoId != 0)
            {
                var freebies = _jobPromotionRepository.GetAll().Where(e => e.Id == JobPromoId).Select(e => new { e.Id, e.FreebieTransportId, e.DispatchedDate, e.TrackingNumber, e.PromotionMasterId }).FirstOrDefault();
                if (freebies != null)
                {
                    var freebies_List = _freebieTransportRepository.GetAll().Where(e => e.Id == freebies.FreebieTransportId).Select(e => new { e.Name, e.TransportLink }).FirstOrDefault();
                    dto.DispatchedDate = (DateTime)freebies.DispatchedDate;
                    dto.TrackingNo = freebies.TrackingNumber;
                    dto.TransportCompanyName = freebies_List.Name;
                    dto.TransportLink = freebies_List.TransportLink;
                    dto.PromoType = _promotionMasterRepository.GetAll().Where(e => e.Id == freebies.PromotionMasterId).Select(e => e.Name).FirstOrDefault();
                }
            }


            if (InstallInoiceId > 0)
            {
                var installerinvoice = await _jobInstallerInvoiceRepository.FirstOrDefaultAsync((int)InstallInoiceId);
                if(installerinvoice != null)
                {
                    dto.InvoiceAmount = installerinvoice.Amount;
                    dto.ApproedAmount = installerinvoice.ApproedAmount;
                }

            }


            return dto;

        }

        public async Task<GetLeadForActivityOutput> GetLeadForActivity(int id)
        {
            var lead = await _leadRepository.GetAsync(id);
            var job_data = _jobRepository.GetAll().Where(e => e.LeadId == id).Select(e =>  e.JobNumber).FirstOrDefault();
            var _lookupUserStatus = await _userRepository.FirstOrDefaultAsync((int)lead.AssignToUserID);


            var dto = new GetLeadForActivityOutput
            {
                CompanyName = lead.CompanyName,
                JobNumber = job_data!=null ? job_data : "",
                CurrentAssignUserName = _lookupUserStatus?.FullName,
            };

            return dto;
        }

        public async Task<GetLeadForSMSEmailDto> GetLeadForSMSEmailActivityInvoiceIssued(int id)
        {
            var lead = await _leadRepository.GetAsync(id);
            var job_data = _jobRepository.GetAll().Where(e => e.LeadId == id).Select(e => new {e.Id,e.JobNumber}).FirstOrDefault();
            var _filelist = new List<SendEmailAttachmentDto>();
            var _jobnumber = "";
            if(job_data != null)
            {
                _filelist = (from item in _EmailTempDataRepository.GetAll().Where(e => e.JobId == job_data.Id)
                             select new SendEmailAttachmentDto
                             {
                                 FileName = item.FileName,
                                 FilePath = item.FilePath,
                                 id = item.Id,
                             }).ToList();
                _jobnumber = job_data.JobNumber;
            }

            var dto = new GetLeadForSMSEmailDto
            {
                CompanyName = lead.CompanyName,
                Mobile = lead.Mobile,
                Email = lead.Email,
                filelist = _filelist,
                Jobnumber = _jobnumber
            };

            return dto;
        }
        public async Task<GetLeadForSMSEmailDto> GetLeadForSMSEmailActivity(int id)
        {
            var lead = await _leadRepository.GetAsync(id);
            var job_data = _jobRepository.GetAll().Where(e => e.LeadId == id).Select(e => e.JobNumber).FirstOrDefault();

            var dto = new GetLeadForSMSEmailDto
            {
                CompanyName = lead.CompanyName,
                Mobile = lead.Mobile,
                Email = lead.Email,
                Jobnumber = string.IsNullOrEmpty(job_data) ? "" : job_data
            };

            return dto;
        }


        public async Task SendSms(SmsEmailDto input)
        {
            var leadid = 0;
            var mobilenumber = "";

            var sectionName = _SectionRepository.GetAll().Where(e =>e.SectionId == input.TrackerId).FirstOrDefault().SectionName;

                var lead = await _leadRepository.GetAsync((int)input.LeadId);
            var Tamplate = _smsTemplateRepository.GetAll().Where(e => e.Name == "CIMET" && e.OrganizationUnitId == lead.OrganizationId).Select(e => e.Id).FirstOrDefault();

            if (input.SMSTemplateId == Tamplate)
            {
                lead.SMSEmailCIMAT = 1;
                await _leadRepository.UpdateAsync(lead);
            }

            if (input.TrackerId == 2)
            {
                var jobPromotion = await _jobPromotionRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditJobPromotionDto>(jobPromotion);
                var link = _freebieTransportRepository.GetAll().Where(e => e.Id == output.FreebieTransportId).Select(e => e.TransportLink).FirstOrDefault();
                leadid = (int)_jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                mobilenumber = _leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Mobile).FirstOrDefault();
                if (!string.IsNullOrEmpty(mobilenumber))
                {
                    output.SmsSend = true;
                    output.SmsSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobPromotion);
                }

            }
            else if (input.TrackerId == 4)
            {
                var jobRefund = await _jobRefundRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditJobRefundDto>(jobRefund);
                leadid = (int)_jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                mobilenumber = _leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Mobile).FirstOrDefault();
                if (!string.IsNullOrEmpty(mobilenumber))
                {
                    output.JobRefundSmsSend = true;
                    output.JobRefundSmsSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobRefund);
                }
            }
            else if (input.TrackerId == 26)  // InstallerInvoice
            {
                var installerinvoice = await _jobInstallerInvoiceRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditJobInstallerInvoiceDto>(installerinvoice);
                leadid = (int)_jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                mobilenumber = _leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Mobile).FirstOrDefault();
                if (!string.IsNullOrEmpty(mobilenumber))
                {
                    output.SmsSend = true;
                    output.SmsSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, installerinvoice);
                }
            }
            else
            {
                var Jobid = _jobRepository.GetAll().Where(e => e.LeadId == input.LeadId).Select(e => e.Id).FirstOrDefault();
                var job = await _jobRepository.FirstOrDefaultAsync(Jobid);
                var output = ObjectMapper.Map<CreateOrEditJobDto>(job);
                leadid = (int)input.LeadId;
                mobilenumber = _leadRepository.GetAll().Where(e => e.Id == input.LeadId).Select(e => e.Mobile).FirstOrDefault();
                if (!string.IsNullOrEmpty(mobilenumber))
                {
                    if (input.TrackerId == 11)
                    {
                        output.PendingInstallerSmsSend = true;
                        output.PendingInstallerSmsSendDate = DateTime.UtcNow;
                    }

                    if (input.TrackerId == 1 && job.DistApproveDate != null) // ApplicarionTrackerid
                    {
                        output.SmsSend = true;
                        output.SmsSendDate = DateTime.UtcNow;
                    }

                    if (input.TrackerId == 3) // FinanceTrackerid
                    {
                        output.FinanceSmsSend = true;
                        output.FinanceSmsSendDate = DateTime.UtcNow;
                    }

                    if (input.TrackerId == 5)  // JobActiveTrackerid
                    {
                        output.JobActiveSmsSend = true;
                        output.JobActiveSmsSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 6)  // GridConnectionid
                    {
                        output.GridConnectionSmsSend = true;
                        output.GridConnectionSmsSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 7)  // StcTrackerid
                    {
                        output.StcSmsSend = true;
                        output.StcSmsSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 8)  // JobGridId
                    {
                        output.JobGridSmsSend = true;
                        output.JobGridSmsSendDate = DateTime.UtcNow;
                    }

                   

                    ObjectMapper.Map(output, job);

                }
            }
            if (!string.IsNullOrEmpty(mobilenumber))
            {
                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 6;
                leadactivity.ActionNote = "Sms Send From " + sectionName;
                leadactivity.LeadId = (int)leadid;
                leadactivity.Subject = input.Body;
                leadactivity.ActivityDate = DateTime.UtcNow;
                leadactivity.SectionId = input.TrackerId;
                leadactivity.ServiceId = input.ServiceId;
                if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                {
                    leadactivity.TemplateId = input.SMSTemplateId;
                }
                if(input.SMSTemplateId == Tamplate)
                {
                    leadactivity.IsCimat =true;
                }
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                else
                {
                    leadactivity.TemplateId = 0;
                }
                leadactivity.Body = input.Body;
                await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                SendSMSInput sendSMSInput = new SendSMSInput();
                sendSMSInput.PhoneNumber = mobilenumber;
                sendSMSInput.Text = input.Body;
                sendSMSInput.ActivityId = leadactivity.Id;
                await _applicationSettings.SendSMS(sendSMSInput);
            }
                
        }

        public async Task SendEmail(SmsEmailDto input)
        {
            var Email = "";
            var leadid = 0;
            var sectionName = _SectionRepository.GetAll().Where(e => e.SectionId == input.TrackerId).FirstOrDefault().SectionName;
                var lead = await _leadRepository.GetAsync((int)input.LeadId);

            if (input.TrackerId == 2)
            {
                var jobPromotion = await _jobPromotionRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditJobPromotionDto>(jobPromotion);
                var link = _freebieTransportRepository.GetAll().Where(e => e.Id == output.FreebieTransportId).Select(e => e.TransportLink).FirstOrDefault();
                leadid = (int)_jobRepository.GetAll().Where(e => e.Id == output.JobId).FirstOrDefault().LeadId;
                Email = _leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Email).FirstOrDefault();
                //var lead = await _leadRepository.FirstOrDefaultAsync((int)leadid);
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
                if (!string.IsNullOrEmpty(Email))
                {
                    output.EmailSend = true;
                    output.EmailSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobPromotion);
                }

            }
            else if (input.TrackerId == 4)
            {
                var jobRefund = await _jobRefundRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditJobRefundDto>(jobRefund);
                leadid = (int)_jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                Email = _leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Email).FirstOrDefault();
                //var lead = await _leadRepository.FirstOrDefaultAsync((int)leadid);
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
                if (!string.IsNullOrEmpty(Email))
                {
                    output.JobRefundEmailSend = true;
                    output.JobRefundSmsSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobRefund);
                }
            }
            else if (input.TrackerId == 10)
            {
                var inmportdatas = await _InvoiceImportDataRepository.FirstOrDefaultAsync((int)input.ID);
                /// var output = ObjectMapper.Map<CreateOrEditImportDto>(inmportdatas);
                leadid = (int)_jobRepository.GetAll().Where(e => e.Id == input.LeadId).Select(e => e.LeadId).FirstOrDefault();
                Email = _leadRepository.GetAll().Where(e => e.Id == input.LeadId).Select(e => e.Email).FirstOrDefault();
                //var lead = await _leadRepository.FirstOrDefaultAsync((int)input.LeadId);
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();

                List<SendEmailAttachmentDto> emailattch = new List<SendEmailAttachmentDto>();
                SendEmailAttachmentDto doc = new SendEmailAttachmentDto();
                if (input.TaxInvoiceDocPath != null)
                {
                    doc.FileName = input.TaxInvoiceFileName;
                    doc.FilePath = input.TaxInvoiceDocPath;
                    emailattch.Add(doc);
                }
            }
            else if (input.TrackerId == 26)
            {
                var installerinvoice = await _jobInstallerInvoiceRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditJobInstallerInvoiceDto>(installerinvoice);
                leadid = (int)_jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                Email = _leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Email).FirstOrDefault();
                //var lead = await _leadRepository.FirstOrDefaultAsync((int)leadid);
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
                if (!string.IsNullOrEmpty(Email))
                {
                    output.installerEmailSend = true;
                    output.EmaiSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, installerinvoice);
                }
            }
            else
            {
                //var lead = await _leadRepository.FirstOrDefaultAsync((int)input.LeadId);
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
                leadid = (int)input.LeadId;
                Email =lead.Email;
                var Jobid = _jobRepository.GetAll().Where(e => e.LeadId == input.LeadId).Select(e => e.Id).FirstOrDefault();
                var job = await _jobRepository.FirstOrDefaultAsync(Jobid);
                var output = ObjectMapper.Map<CreateOrEditJobDto>(job);
                if (!string.IsNullOrEmpty(Email))
                {
                    if (input.TrackerId == 1 && job.DistApproveDate != null)
                    {
                        output.EmailSend = true;
                        output.EmailSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 3)
                    {
                        output.FinanceEmailSend = true;
                        output.FinanceEmailSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 5)
                    {
                        output.JobActiveEmailSend = true;
                        output.JobActiveEmailSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 6)
                    {
                        output.GridConnectionEmailSend = true;
                        output.GridConnectionEmailSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 7)
                    {
                        output.StcEmailSend = true;
                        output.StcEmailSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 8)
                    {
                        output.JobGridEmailSend = true;
                        output.JobGridEmailSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 11)
                    {
                        output.PendingInstallerEmailSend = true;
                        output.PendingInstallerEmailSendDate = DateTime.UtcNow;
                    }
                    ObjectMapper.Map(output, job);
                }
                    
            }
            int? TemplateId;

            var Tamplate = _emailTemplateRepository.GetAll().Where(e => e.TemplateName == "CIMET" && e.OrganizationUnitId == lead.OrganizationId).Select(e => e.Id).FirstOrDefault();

            if (input.SMSTemplateId == Tamplate)
            {
                lead.SMSEmailCIMAT = 2;
                await _leadRepository.UpdateAsync(lead);
            }

            if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
            {
                TemplateId = input.EmailTemplateId;
                //var Template = _temp
            }
            else
            {
                TemplateId = 0;
            }

            if (!string.IsNullOrEmpty(Email))
            {
                if (input.cc != null && input.Bcc != null)
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { Email }, //{ "viral.jain@meghtechnologies.com" }, //
                        CC = { input.cc },
                        Bcc = { input.Bcc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                }

                else if (input.cc != null && input.cc != "" && input.Bcc == null)
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { Email }, //{ "viral.jain@meghtechnologies.com" }, //
                        CC = { input.cc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                }

                else if (input.cc == null && input.Bcc != null && input.Bcc != "")
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { Email }, //{ "viral.jain@meghtechnologies.com" }, //
                        Bcc = { input.Bcc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                }

                else
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { Email }, //{ "hiral.prajapati@meghtechnologies.com" }, //

                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                }

                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 7;
                leadactivity.ActionNote = "Email Send From " + sectionName;
                leadactivity.LeadId = (int)leadid;
                leadactivity.TemplateId = TemplateId;
                leadactivity.Subject = input.Subject;
                leadactivity.Body = input.Body;
                leadactivity.ActivityDate = DateTime.UtcNow;
                leadactivity.SectionId = input.TrackerId;
                leadactivity.ServiceId = input.ServiceId;
                if (input.SMSTemplateId == Tamplate)
                {
                    leadactivity.IsCimat = true;
                }
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }


        }
    
        public async Task AddNotifyActivityLog(ActivitylogInput input)
        {
            var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var lead = await _leadRepository.FirstOrDefaultAsync((int)input.leadId);
            var sectionName = _SectionRepository.GetAll().Where(e => e.SectionId == input.SectionId).FirstOrDefault().SectionName;
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();

            string msg = "Notification " + input.ActivityNote + " For " + lead.CompanyName + " By " + CurrentUser.Name + " From " + sectionName;
            await _appNotifier.LeadComment(assignedToUser, msg, NotificationSeverity.Warn);

            var activity = new LeadActivityLog();
            activity.ActionId = 9;
            activity.ActionNote = "Notification";
            activity.SectionId = input.SectionId;
            activity.CreatorUserId = AbpSession.UserId;
            activity.ActivityNote = input.ActivityNote;
            activity.LeadId = (int)input.leadId;
            if (input.serviceId > 0)
            {
                activity.ServiceId = input.serviceId;

            }

            await _leadactivityRepository.InsertAndGetIdAsync(activity);

        }

        public async Task AddReminderActivityLog(ActivitylogInput input)
        {
            input.ActivityDate = _timeZoneConverter.Convert(input.ActivityDate, (int)AbpSession.TenantId);

            var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var lead = await _leadRepository.FirstOrDefaultAsync((int)input.leadId);
            var sectionName = _SectionRepository.GetAll().Where(e => e.SectionId == input.SectionId).FirstOrDefault().SectionName;
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();

            DateTime utc = DateTime.UtcNow;
            var CurrentTime = (_timeZoneConverter.Convert(utc, (int)AbpSession.TenantId));

            if (input.ActivityDate < CurrentTime)
            {
                throw new UserFriendlyException(L("PleaseSelectGreaterDatethanCurrentDate"));
            }
            var activity = new LeadActivityLog();
            activity.ActionId = 8;
            activity.ActionNote = "Reminder Set";
            activity.SectionId = input.SectionId;
            activity.CreatorUserId = AbpSession.UserId;
            activity.ActivityNote = input.ActivityNote;
            activity.ActivityDate = input.ActivityDate;
            activity.LeadId = (int)input.leadId;
            if (input.serviceId > 0)
            {
                activity.ServiceId = input.serviceId;
            }

            await _leadactivityRepository.InsertAndGetIdAsync(activity);

            if (input.SectionId == 13 || input.SectionId == 14) // Add My Lead and Lead Tracker
            {
                lead.ActivityDate = activity.ActivityDate;
                lead.ActivityDescription = activity.ActivityNote;
                await _leadRepository.UpdateAsync(lead);

            }
            if (input.SectionId == 8) // Add My Jobs Tracker
            {
                var job = await _jobRepository.FirstOrDefaultAsync(e => e.LeadId == activity.LeadId);
                job.JobReminderTime = activity.ActivityDate;
                job.JobReminderDes = activity.ActivityNote;

                await _jobRepository.UpdateAsync(job);
            }

            string msg = string.Format("Reminder " + input.ActivityNote + " For " + lead.CompanyName + " By " + CurrentUser.Name + " From " + sectionName);
            await _appNotifier.LeadAssiged(CurrentUser, msg, NotificationSeverity.Info);
        }

        public async Task AddToDoActivityLog(ActivitylogInput input)
        {
            var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var lead = await _leadRepository.FirstOrDefaultAsync((int)input.leadId);
            var sectionName = _SectionRepository.GetAll().Where(e => e.SectionId == input.SectionId).FirstOrDefault().SectionName;
            var assignedToUserForTodo = _userRepository.GetAll().Where(u => u.Id == input.UserId).FirstOrDefault();
            
            var activity = new LeadActivityLog();
            activity.ActionId = 25;
            activity.ActionNote = "ToDo";
            activity.SectionId = input.SectionId;
            activity.CreatorUserId = AbpSession.UserId;
            activity.ActivityNote = input.ActivityNote;
            activity.LeadId = (int)input.leadId;
            if (input.serviceId > 0)
            {
                activity.ServiceId = input.serviceId;

            }

            await _leadactivityRepository.InsertAndGetIdAsync(activity);

            string msg = "ToDo " + input.ActivityNote + " For " + lead.CompanyName + " By " + CurrentUser.Name + " From " + sectionName;
            await _appNotifier.LeadComment(assignedToUserForTodo, msg, NotificationSeverity.Warn);
        }

        public async Task AddCommentActivityLog(ActivitylogInput input)
        {
            var lead = await _leadRepository.FirstOrDefaultAsync((int)input.leadId);
           
            var activity = new LeadActivityLog();
            activity.ActionId = 24;
            activity.ActionNote = "Comment";
            activity.SectionId = input.SectionId;
            activity.CreatorUserId = AbpSession.UserId;
            activity.ActivityNote = input.ActivityNote;
            activity.LeadId = (int)input.leadId;
            if(input.serviceId > 0)
            {
                activity.ServiceId = input.serviceId;

            }

            await _leadactivityRepository.InsertAndGetIdAsync(activity);
            if (input.SectionId == 13 || input.SectionId == 14) // Add My Lead and Lead Tracker
            {
                 lead.ActivityNote = activity.ActivityNote;
                await _leadRepository.UpdateAsync(lead);
            }

            if (input.SectionId == 8) // Add My Jobs Tracker
            {
                var job = await _jobRepository.FirstOrDefaultAsync(e => e.LeadId == activity.LeadId);

                job.JobComment = activity.ActivityNote;

                await _jobRepository.UpdateAsync(job);
            }
        }

        public async  Task<GetInstallerForActivityOutput> GetInstallerForActivity(int id)
        {
            var user = await UserManager.GetUserByIdAsync(id);
            var installerDetail = _installerDetail.GetAll().Where(e => e.UserId == id).FirstOrDefault();

            var output = new GetInstallerForActivityOutput();

            long? createduserId = _userRepository.GetAll().Where(u => u.Id == id).Select(u => u.CreatorUserId).FirstOrDefault();
            output.CreatedUser = createduserId != null ? _userRepository.GetAll().Where(u => u.Id == createduserId).Select(u => u.UserName).FirstOrDefault() : null;
            output.Email = user.EmailAddress;
            output.Mobile =user.Mobile;
            output.CompanyName = user.CompanyName;

            return output;

        }


    }
}
