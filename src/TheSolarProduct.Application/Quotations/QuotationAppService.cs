﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Quotations.Dtos;
using TheSolarProduct.Storage;
using Abp.UI;
using TheSolarProduct.Jobs;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Reports;
using System.IO;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.ApplicationSettings.Dto;
using Abp.Net.Mail;
using System.Net.Mail;
using TheSolarProduct.Leads;
using Abp.Runtime.Security;
using Microsoft.AspNetCore.Hosting;
using TheSolarProduct.MultiTenancy;
using System.Drawing.Imaging;
using TheSolarProduct.Invoices;
using TheSolarProduct.TheSolarDemo;
using Abp.Notifications;
using Abp.Authorization.Users;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Notifications;
using Abp.Domain.Uow;
using System.Web;
using Abp.Organizations;
using TheSolarProduct.Organizations;
using TheSolarProduct.Common;
using System.IO;
using Microsoft.AspNetCore.Identity;
using Abp.Domain.Entities;
using TheSolarProduct.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using System.Text;
using Api2Pdf;
using TheSolarProduct.EntityFrameworkCore.Repositories;
using System.Globalization;
using TheSolarProduct.UserWiseEmailOrgs;
using System.Text.RegularExpressions;
using Abp.Timing.Timezone;
using System.Collections;
using TheSolarProduct.LeadDetails.Dtos;
using TheSolarProduct.States;
using Z.BulkOperations.Internal.Data.SqlClient;

namespace TheSolarProduct.Quotations
{
    public class QuotationsAppService : TheSolarProductAppServiceBase, IQuotationsAppService
    {
        private const string pathfordoc = ApplicationSettingConsts.DocumentPath;
        private const string viewDocPath = ApplicationSettingConsts.ViewDocumentPath;

        private const int MaxFileBytes = 5242880; //5MB
        private readonly IRepository<Quotation> _quotationRepository;
        private readonly IRepository<DocumentType> _documentTypeRepository;
        private readonly IRepository<Document> _documentRepository;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IRepository<Job, int> _jobRepository;
        private readonly IRepository<DocumentType, int> _lookup_documentTypeRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IReportAppService _reportAppService;
        private readonly IJobsAppService _jobAppService;
        private readonly IAppFolders _appFolders;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<Lead> _leadRepository;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<RoofType, int> _roofTypeRepository;
        private readonly IRepository<RoofAngle, int> _roofAngleRepository;
        private readonly IRepository<ElecDistributor, int> _elecDistributorRepository;
        private readonly IRepository<ElecRetailer, int> _elecRetailerRepository;
        private readonly IRepository<ProductItem> _ProductItemRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<Variation> _variationRepository;
        private readonly IRepository<JobVariation> _jobVariationRepository;
        private readonly IRepository<QuotationLinkHistory> _quotationLinkHistoryRepository;
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
        private readonly IRepository<FinanceOption> _financeOptionRepository;
        private readonly IRepository<DepositOption> _depositOptionRepository;
        private readonly IRepository<PaymentOption> _paymentOptionRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<HouseType, int> _houseTypeRepository;
        private readonly IRepository<OrganizationUnit, long> _OrganizationRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendedOrganizationUnitRepository;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly IRepository<QuotationDetail> _quotationDetailRepqository;
        private readonly IRepository<QuotationQunityAndModelDetail> _quotationQunityAndModelDetailRepqository;
        private readonly IRepository<UserWiseEmailOrg> _userWiseEmailOrgRepqository;
        private readonly IRepository<State> _stateRepqository;

        private readonly IDbContextProvider<TheSolarProductDbContext> _dbContextProvider;

        private readonly ITimeZoneConverter _timeZoneConverter;

        private readonly CommonManager _commonManager;

        public QuotationsAppService(IRepository<Quotation> quotationRepository,
            IRepository<DocumentType> documentTypeRepository,
            IRepository<Document> documentRepository,
            IBinaryObjectManager binaryObjectManager,
            ITempFileCacheManager tempFileCacheManager,
            IRepository<Job, int> lookup_jobRepository,
            IRepository<DocumentType, int> lookup_documentTypeRepository
            , IRepository<User, long> userRepository
            , IRepository<LeadActivityLog> leadactivityRepository
            , IReportAppService reportAppService
            , IJobsAppService jobAppService
            , IAppFolders appFolders
            , IApplicationSettingsAppService applicationSettings
            , IEmailSender emailSender
            , IRepository<Lead> leadRepository
            , IWebHostEnvironment env
            , IRepository<Tenant> tenantRepository
            , IRepository<RoofType, int> roofTypeRepository
            , IRepository<RoofAngle, int> roofAngleRepository
            , IRepository<ElecDistributor, int> elecDistributorRepository
            , IRepository<ElecRetailer, int> elecRetailerRepository
            , IRepository<ProductItem> ProductItemRepository
            , IRepository<JobProductItem> jobProductItemRepository
            , IRepository<Variation> variationRepository
            , IRepository<JobVariation> jobVariationRepository
            , IRepository<QuotationLinkHistory> quotationLinkHistoryRepository
            , IRepository<InvoicePayment> invoicePaymentRepository
            , IRepository<FinanceOption> financeOptionRepository
            , IRepository<DepositOption> depositOptionRepository
            , IRepository<PaymentOption> paymentOptionRepository
            , IRepository<UserTeam> userTeamRepository
            , IRepository<UserRole, long> userroleRepository
            , IRepository<Role> roleRepository
            , IAppNotifier appNotifier
            , IRepository<HouseType, int> houseTypeRepository
            , IUnitOfWorkManager unitOfWorkManager,
            IRepository<OrganizationUnit, long> OrganizationRepository,
            IRepository<ExtendOrganizationUnit, long> extendedOrganizationUnitRepository,
            ICommonLookupAppService CommonDocumentSaveRepository,
            IRepository<QuotationDetail> quotationDetailRepository,
            IRepository<QuotationQunityAndModelDetail> quotationQunityAndModelDetailRepqository,
            IDbContextProvider<TheSolarProductDbContext> dbContextProvider
            //IRepository<QuotationTemplate> quotationTemplateRepqository
            //, CommonManager commonManager
            , IRepository<UserWiseEmailOrg> userWiseEmailOrgRepqository,
            ITimeZoneConverter timeZoneConverter
            , IRepository<State> stateRepqository
            )
        {
            _quotationRepository = quotationRepository;
            _documentTypeRepository = documentTypeRepository;
            _documentRepository = documentRepository;
            _binaryObjectManager = binaryObjectManager;
            _tempFileCacheManager = tempFileCacheManager;
            _jobRepository = lookup_jobRepository;
            _lookup_documentTypeRepository = lookup_documentTypeRepository;
            _userRepository = userRepository;
            _leadactivityRepository = leadactivityRepository;
            _reportAppService = reportAppService;
            _jobAppService = jobAppService;
            _appFolders = appFolders;
            _applicationSettings = applicationSettings;
            _emailSender = emailSender;
            _leadRepository = leadRepository;
            _env = env;
            _tenantRepository = tenantRepository;
            _roofTypeRepository = roofTypeRepository;
            _roofAngleRepository = roofAngleRepository;
            _elecDistributorRepository = elecDistributorRepository;
            _elecRetailerRepository = elecRetailerRepository;
            _ProductItemRepository = ProductItemRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _variationRepository = variationRepository;
            _jobVariationRepository = jobVariationRepository;
            _quotationLinkHistoryRepository = quotationLinkHistoryRepository;
            _invoicePaymentRepository = invoicePaymentRepository;
            _financeOptionRepository = financeOptionRepository;
            _depositOptionRepository = depositOptionRepository;
            _paymentOptionRepository = paymentOptionRepository;
            _userTeamRepository = userTeamRepository;
            _userroleRepository = userroleRepository;
            _roleRepository = roleRepository;
            _appNotifier = appNotifier;
            _houseTypeRepository = houseTypeRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _OrganizationRepository = OrganizationRepository;
            _extendedOrganizationUnitRepository = extendedOrganizationUnitRepository;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _quotationDetailRepqository = quotationDetailRepository;
            _quotationQunityAndModelDetailRepqository = quotationQunityAndModelDetailRepqository;
            _dbContextProvider = dbContextProvider;
            //_quotationTemplateRepqository = quotationTemplateRepqository;
            //_commonManager = commonManager;
            _userWiseEmailOrgRepqository = userWiseEmailOrgRepqository;
            _timeZoneConverter = timeZoneConverter;
            _stateRepqository = stateRepqository;
        }

        [AbpAuthorize]
        public async Task<PagedResultDto<GetQuotationForViewDto>> GetAll(GetAllQuotationInput input)
        {
            var filteredQuotations = _quotationRepository.GetAll()
                        .WhereIf(input.JobId > 0, e => false || e.JobId == input.JobId);

            var pagedAndFilteredQuotations = filteredQuotations
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var quotations = from o in pagedAndFilteredQuotations

                             join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                             from s3 in j3.DefaultIfEmpty()

                             select new GetQuotationForViewDto()
                             {
                                 Quotation = new QuotationDto
                                 {
                                     DocumentId = o.DocumentId,
                                     JobId = o.JobId,
                                     IsSigned = o.IsSigned,
                                     QuoteDate = o.QuoteDate,
                                     Id = o.Id,
                                     CreatedBy = s3.FullName,
                                     QuoteFilePath = o.QuoteFilePath,
                                     QuoteFileName = o.QuoteFileName,
                                     SignedQuoteFileName = o.SignedQuoteFileName,
                                     QuoteNumber = o.QuoteNumber
                                 },

                                 NewQuote = _quotationDetailRepqository.GetAll().Where(e => e.QuotationId == o.Id).Any()
                             };

            var totalCount = await filteredQuotations.CountAsync();

            return new PagedResultDto<GetQuotationForViewDto>(
                totalCount,
                await quotations.ToListAsync()
            );
        }

        [AbpAuthorize]
        public async Task<GetQuotationForViewDto> GetQuotationForView(int id)
        {
            var quotation = await _quotationRepository.GetAsync(id);

            var output = new GetQuotationForViewDto { Quotation = ObjectMapper.Map<QuotationDto>(quotation) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Quotations_Edit, AppPermissions.Pages_Jobs)]
        public async Task<GetQuotationForEditOutput> GetQuotationForEdit(EntityDto input)
        {
            var depositOption = await _quotationRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetQuotationForEditOutput { DepositOption = ObjectMapper.Map<CreateOrEditQuotationDto>(depositOption) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditQuotationDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Quotations_Create, AppPermissions.Pages_Jobs)]
        protected virtual async Task Create(CreateOrEditQuotationDto input)
        {
            var NearMapExist = _documentRepository.GetAll().Where(e => (e.DocumentTypeId == 1 || e.DocumentTypeId == 15 || e.DocumentTypeId == 16) && e.JobId == input.JobId).Any();
            if (NearMapExist)
            {
                var jobProductItem = await _jobProductItemRepository.GetAll().Include(e => e.ProductItemFk).Where(e => e.JobId == input.JobId).AsNoTracking().Select(e => new { e.ProductItemFk.ProductTypeId, e.ProductItemFk.FileName }).ToListAsync();

                var dataSheetMsg = String.Empty;
                foreach (var item in jobProductItem)
                {
                    if (item.ProductTypeId == 1 && string.IsNullOrEmpty(item.FileName))
                    {
                        dataSheetMsg = "Product Data Sheet is not Available";
                    }
                    if (item.ProductTypeId == 2 && string.IsNullOrEmpty(item.FileName))
                    {
                        dataSheetMsg = "Product Data Sheet is not Available";
                    }
                    if (item.ProductTypeId == 5 && string.IsNullOrEmpty(item.FileName))
                    {
                        dataSheetMsg = "Product Data Sheet is not Available";
                    }
                }

                if (String.IsNullOrEmpty(dataSheetMsg))
                {
                    var job = _jobRepository.GetAll().Where(e => e.Id == input.JobId).FirstOrDefault();
                    var organizationid = _leadRepository.GetAll().Where(e => e.Id == job.LeadId).Select(e => e.OrganizationId).FirstOrDefault();
                    var LastQuoteNumber = _quotationRepository.GetAll().Where(e => e.JobFk.LeadFk.OrganizationId == organizationid).OrderByDescending(e => e.Id).Select(e => e.QuoteNumber).FirstOrDefault();

                    var PrevQuoteNumber1 = 10001;
                    if (LastQuoteNumber != null)
                    {
                        PrevQuoteNumber1 = Convert.ToInt32(LastQuoteNumber) + 1;
                    }

                    var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                    var Path = _env.WebRootPath;
                    var MainFolder = Path;
                    MainFolder = MainFolder + "\\Documents" + "\\" + TenantName + "\\" + job.JobNumber + "\\Reports";

                    var quotation = ObjectMapper.Map<Quotation>(input);
                    quotation.QuoteDate = DateTime.UtcNow;
                    quotation.QuoteFilePath = "\\Documents" + "\\" + TenantName + "\\" + job.JobNumber + "\\Reports\\";
                    quotation.QuoteNumber = Convert.ToString(PrevQuoteNumber1);

                    var quotationId = await _quotationRepository.InsertAndGetIdAsync(quotation);

                    #region Add quotation data in new tab
                    QuotationDataDto quotationDataDto = await GetQuotationData(organizationid, quotationId, job, quotation.CreationTime);
                    var quotationData = ObjectMapper.Map<QuotationDetail>(quotationDataDto);
                    var quotationDataId = await _quotationDetailRepqository.InsertAndGetIdAsync(quotationData);

                    foreach (var item in quotationDataDto.qunityAndModelLists)
                    {
                        var modelDetail = ObjectMapper.Map<QuotationQunityAndModelDetail>(item);
                        modelDetail.QuotationDetailId = quotationDataId;
                        await _quotationQunityAndModelDetailRepqository.InsertAsync(modelDetail);
                    }
                    #endregion

                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 12;
                    leadactivity.SectionId = input.SectionId;
                    leadactivity.ActionNote = "New Quotation Added";
                    leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }
                else
                {
                    throw new UserFriendlyException("Ooppps! There is a problem!", dataSheetMsg);
                }

            }
            else
            {
                throw new UserFriendlyException("Ooppps! There is a problem!", "Near Map Or Pylon Document Is Not Uploaded.");
            }
        }

        [AbpAuthorize]
        public async Task SendQuotation(CreateOrEditQuotationDto input)
        {
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.SectionId = 0;
            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

            var quotation = _quotationRepository.GetAll().Where(e => e.JobId == input.JobId).OrderByDescending(e => e.Id).FirstOrDefault();

            if (quotation.IsSigned == true)
            {
                throw new UserFriendlyException("Last Quotation Already Signed, Please Generate New Quotation.");
            }

            var job = _jobRepository.GetAll().Where(e => e.Id == quotation.JobId).FirstOrDefault();
            var lead = _leadRepository.GetAll().Where(e => e.Id == job.LeadId).FirstOrDefault();
            var Body = job.TenantId + "," + job.Id + "," + quotation.Id;
            var smsBody = "";

            //Token With Tenant Id & Promotion User Primary Id for subscribe & UnSubscribe
            var token = HttpUtility.UrlEncode(SimpleStringCipher.Instance.Encrypt(Body, AppConsts.DefaultPassPhrase));
            var token1 = SimpleStringCipher.Instance.Encrypt(Body, AppConsts.DefaultPassPhrase);

            string BaseURL = System.Configuration.ConfigurationManager.AppSettings["ClientRootAddress"];

            Body = "https://thesolarproduct.com/account/customer-signature?STR=" + token;

            QuotationLinkHistory quotationLinkHistory = new QuotationLinkHistory();
            quotationLinkHistory.Expired = false;
            quotationLinkHistory.QuotationId = quotation.Id;
            quotationLinkHistory.Token = token1;
            await _quotationLinkHistoryRepository.InsertAsync(quotationLinkHistory);
            string FinalBody = "";
            string FinalSMSBody = "";

            var LeadAssignUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();

            var adress = lead.Address + "," + lead.Suburb + "." + lead.State + " " + lead.PostCode;
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            adress = textInfo.ToTitleCase(adress.ToLower());

            var orgName = _OrganizationRepository.GetAll().Where(e => e.Id == lead.OrganizationId).Select(e => e.DisplayName).FirstOrDefault();
            var FromEmail = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == lead.OrganizationId).Select(e => e.defaultFromAddress).FirstOrDefault();
            var ClickHere = Body;
            var leadassignusername = LeadAssignUser == null ? "" : LeadAssignUser.Name;
            //FinalSMSBody = "Dear " + lead.CompanyName +
            //    "\n\nYour Solar Advisor " + leadassignusername + " From " + orgName + " PTY LTD is requesting a signature for Project No. " + quotation.JobFk.JobNumber + " On Quote No. " + quotation.QuoteNumber + " @ Your property " + adress + "." + "\n\nTo Upload your signature please click below link \n" + "<a href=\"" + ClickHere + "\"></a>" +
            //    "\n\nRegards\n\n" + orgName;

            FinalSMSBody = "Greetings " + lead.CompanyName +
                "\n\nBelow is the link to Your Solar System Quotation/Contract No. " + quotation.JobFk.JobNumber + " as discussed and agreed upon. Kindly review the same at the link below & sign at your earliest convenience.\n\n" + ClickHere + "\n\nKindly get back to me in case of any queries/concerns.\nPlease Note that the link is valid for 24 hours and can be used to sign the quote only once.\n\nHappy Savings\n" + leadassignusername + "\n" + orgName;

            var OrgLogo = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == lead.OrganizationId).Select(e => (e.LogoFilePath + e.LogoFileName)).FirstOrDefault();
            OrgLogo = ApplicationSettingConsts.ViewDocumentPath + (OrgLogo != null ? OrgLogo.Replace("\\", "/") : "");

            FinalBody = "<!doctype html><html lang='en'><head><meta charset='utf-8'><meta name='viewport' content='width=device-width,initial-scale=1,shrink-to-fit=no'><meta name='viewport' content='width=device-width,initial-scale=1,shrink-to-fit=no'><link rel='preconnect' href='https://fonts.googleapis.com'><link rel='preconnect' href='https://fonts.gstatic.com' crossorigin><link href='https://fonts.googleapis.com/css2?family=Roboto&display=swap' rel='stylesheet'><title>Inline CSS Email</title></head><style>@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu72xKOzY.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu5mxKOzY.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7mxKOzY.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu4WxKOzY.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7WxKOzY.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7GxKOzY.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu4mxK.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}body{margin:0;padding:0;background:#48afb5;font-family:Roboto}</style><body style='background:#48afb5;margin:0;padding:0'><div style='background:#48afb5;margin:0;padding:20px 0'><div style='margin:10px auto;max-width:600px;min-width:320px'><div style='margin:0 10px;max-width:600px;min-width:320px;padding:0;background-color:#fff;border:1px solid #e2ebf1;border-radius:10px;overflow:hidden'><table style='width:100%;border:0;border-spacing:0;margin:0 auto'><tr><td style='text-align:center;padding:30px 0 20px'><img src='" + OrgLogo + "' style='max-width:200px'></td></tr><tr><td style='text-align:center;padding:30px 0 50px'><img src='https://thesolarproduct.com/assets/common/images/e-template-images/customer-signature.png' style='width:25%'></td></tr><tr><td style='font-size:22px;font-weight:700;font-family:Roboto,sans-serif;color:#000;padding:0 30px;text-align:center;font-weight:900'>Dear " + lead.CompanyName + ",</td></tr><tr><td style='height:30px'></td></tr><tr><td style='text-align:center;font-size:14px;font-weight:400;font-family:Roboto,sans-serif;color:#1a1a1a;padding:0 0'><div style='font-size:1.1rem;font-family:Roboto,sans-serif;color:#1a1a1a;margin:0 25px 30px;line-height:28px'>Congratulations! As discussed and agreed upon, here is the LINK to your personalised Solar System Quotation/Contract No. <strong style='border-bottom:1px dotted #000'>" + quotation.JobFk.JobNumber + "</strong>.<br> Requested to go through the details for your reference and to confirm the same so that we can proceed with the other steps necessary to execute your solar installation at the earliest.</div><div style='padding:20px 0'><div style='font-size:.9rem;font-weight:400;font-family:Roboto,sans-serif;margin-bottom:30px;line-height:24px'><a href='" + Body + "' style='text-decoration:none;text-decoration:none;background:#48afb5;font-weight:700;color:#fff;letter-spacing:1px;font-size:16px;text-transform:uppercase;border-radius:25px;height:50px;width:320px;display:inline-block;line-height:50px'>Click Here to Sign</a></div><div style='font-size:18px;font-weight:400;font-family:Roboto,sans-serif;color:#000;margin-bottom:20px;line-height:24px;font-weight:700'>OR Visit</div><div style='font-size:14px;font-weight:400;font-family:Roboto,sans-serif;line-height:24px'><a href='" + Body + "' style='text-decoration:none;color:#000;font-weight:900'>" + Body + "</a></div></div><div style='font-size:1rem;font-weight:400;font-family:Roboto,sans-serif;color:#1a1a1a;margin:30px 25px 25px;line-height:24px'><strong>Note: </strong>Please Be Informed That The Link Is Valid Only For <strong>24 hours.</strong><br>Please revert to me with any queries/concerns that you may have.</div><div style='background:#f5f5f5;font-family:Roboto,sans-serif;font-size:14px;color:#666;line-height:22px;border-top:1px solid #e2ebf1;padding:10px 0'>Happy Savings,<br>" + leadassignusername + "<br>" + orgName + "</td></tr></table></div></div></div></body></html>";

            if (input.QuoteMode == "SMS")
            {
                leadactivity.IsMark = false;
            }
            else
            {
                leadactivity.IsMark = null;
            }
            if (input.QuoteMode == "sendQuote")
            {
                leadactivity.Subject = input.Subject;
            }
            leadactivity.ActionId = 12;
            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            leadactivity.SectionId = input.SectionId;
            await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

            if (input.QuoteMode == "SMS")
            {
                if (!string.IsNullOrEmpty(lead.Mobile))
                {
                    SendSMSInput sendSMSInput = new SendSMSInput();

                    sendSMSInput.ActivityId = leadactivity.Id;
                    sendSMSInput.PhoneNumber = lead.Mobile; //"+919974242686"; //
                    sendSMSInput.Text = FinalSMSBody;
                    sendSMSInput.SectionID = (int)input.SectionId;
                    await _applicationSettings.SendSMS(sendSMSInput);
                    leadactivity.ActionNote = "Customer Signature Request Sent On SMS";
                }
            }
            else if (input.QuoteMode == "Email")
            {
                await _emailSender.SendAsync(new MailMessage
                {
                    From = new MailAddress(FromEmail),
                    To = { lead.Email },
                    Subject = orgName + " | Solar Proposal | Request to Confirm Your Solar System Details on Job Number: " + quotation.JobFk.JobNumber,
                    Body = FinalBody,
                    IsBodyHtml = true
                });
                leadactivity.ActionNote = "Customer Signature Request Sent On Email";
            }
            else if (input.QuoteMode == "Send")
            {
                var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                var JobNumber = _jobRepository.GetAll().Where(e => e.Id == input.JobId).Select(e => e.JobNumber).FirstOrDefault();
                //var Path = _env.WebRootPath;
                //var MainFolder = Path;
                //MainFolder = MainFolder + "\\Documents" + "\\" + TenantName + "\\" + JobNumber + "\\Reports\\";
                var Path = "https://documents.thesolarproduct.com/";
                var MainFolder = Path + "Documents/" + TenantName + "/" + JobNumber + "/Reports/";

                MailMessage mail = new MailMessage
                {
                    From = new MailAddress(FromEmail),
                    To = { lead.Email }, ////{ "hiral.prajapati@meghtechnologies.com" },
                    Subject = "Customer Signed Quote",
                    Body = "Please Find Attached Quotation PDF",
                    IsBodyHtml = true
                };
                var AttchPath = MainFolder + quotation.SignedQuoteFileName;
                //mail.Attachments.Add(new Attachment(AttchPath));

                //var url = "https://documents.thesolarproduct.com/Documents/solarproduct/SB500000/Reports/637864369923965555_Quotation.pdf";
                var url = AttchPath.ToString();
                System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                using (System.Net.HttpWebResponse HttpWResp = (System.Net.HttpWebResponse)req.GetResponse())
                using (Stream responseStream = HttpWResp.GetResponseStream())
                using (MemoryStream ms = new MemoryStream())
                {
                    responseStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    Attachment attachment = new Attachment(ms, "Quote.pdf");
                    mail.Attachments.Add(attachment);
                    await this._emailSender.SendAsync(mail);
                }

                leadactivity.ActionNote = "Quotation Sent On Email";
            }
            else if (input.QuoteMode == "sendQuote")
            {
                if (!string.IsNullOrEmpty(input.EmailFrom))
                {
                    FromEmail = input.EmailFrom;
                }

                var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                var Job = _jobRepository.GetAll().Where(e => e.Id == input.JobId).Select(e => new { e.JobNumber, e.Id }).FirstOrDefault();
                //var Path = _env.WebRootPath;
                var Path = pathfordoc;
                var MainFolder = Path;
                MainFolder = MainFolder + "\\Documents" + "\\" + TenantName + "\\" + Job.JobNumber + "\\Reports\\";

                var FileUrl = _CommonDocumentSaveRepository.DownloadQuote(Job.JobNumber, input.quotationString, "Quotation");

                var DocList = await _jobAppService.ProductItemAttachmentList((int)input.JobId);

                if (input.CC != "" && input.BCC != "")
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(FromEmail),
                        To = { input.EmailTo },
                        CC = { input.CC },
                        Bcc = { input.BCC },
                        Subject = input.Subject,
                        Body = input.EmailBody,
                        IsBodyHtml = true
                    };

                    foreach (var item in DocList)
                    {
                        mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
                    }

                    //Old Quote 
                    //var AttchPath = MainFolder + quotation.QuoteFileName;
                    //mail.Attachments.Add(new Attachment(AttchPath));

                    //New Quote 
                    var url = FileUrl;
                    System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                    using (System.Net.HttpWebResponse HttpWResp = (System.Net.HttpWebResponse)req.GetResponse())
                    using (Stream responseStream = HttpWResp.GetResponseStream())
                    using (MemoryStream ms = new MemoryStream())
                    {
                        responseStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        Attachment attachment = new Attachment(ms, Job.JobNumber + "_Quote.pdf");
                        mail.Attachments.Add(attachment);
                        await this._emailSender.SendAsync(mail);
                    }

                    //await this._emailSender.SendAsync(mail);
                }
                else if (input.CC != "" && input.BCC == "")
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(FromEmail),
                        To = { input.EmailTo },
                        CC = { input.CC },
                        Subject = input.Subject,
                        Body = input.EmailBody,
                        IsBodyHtml = true
                    };

                    //var DocList = await _jobAppService.ProductItemAttachmentList((int)input.JobId);

                    foreach (var item in DocList)
                    {
                        mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
                    }

                    //Old Quote 
                    //var AttchPath = MainFolder + quotation.QuoteFileName;
                    //mail.Attachments.Add(new Attachment(AttchPath));

                    //New Quote 
                    var url = FileUrl;
                    System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                    using (System.Net.HttpWebResponse HttpWResp = (System.Net.HttpWebResponse)req.GetResponse())
                    using (Stream responseStream = HttpWResp.GetResponseStream())
                    using (MemoryStream ms = new MemoryStream())
                    {
                        responseStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        Attachment attachment = new Attachment(ms, Job.JobNumber + "_Quote.pdf");
                        mail.Attachments.Add(attachment);
                        await this._emailSender.SendAsync(mail);
                    }

                    //await this._emailSender.SendAsync(mail);
                }
                else if (input.CC == "" && input.BCC != "")
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(FromEmail),
                        To = { input.EmailTo },
                        Bcc = { input.BCC },
                        Subject = input.Subject,
                        Body = input.EmailBody,
                        IsBodyHtml = true
                    };

                    //var DocList = await _jobAppService.ProductItemAttachmentList((int)input.JobId);

                    foreach (var item in DocList)
                    {
                        mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
                    }

                    //Old Quote 
                    //var AttchPath = MainFolder + quotation.QuoteFileName;
                    //mail.Attachments.Add(new Attachment(AttchPath));

                    //New Quote 
                    var url = FileUrl;
                    System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                    using (System.Net.HttpWebResponse HttpWResp = (System.Net.HttpWebResponse)req.GetResponse())
                    using (Stream responseStream = HttpWResp.GetResponseStream())
                    using (MemoryStream ms = new MemoryStream())
                    {
                        responseStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        Attachment attachment = new Attachment(ms, Job.JobNumber + "_Quote.pdf");
                        mail.Attachments.Add(attachment);
                        await this._emailSender.SendAsync(mail);
                    }

                    //await this._emailSender.SendAsync(mail);
                }
                else
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(FromEmail),
                        To = { input.EmailTo },
                        Subject = input.Subject,
                        Body = input.EmailBody,
                        IsBodyHtml = true
                    };

                    //var DocList = await _jobAppService.ProductItemAttachmentList((int)input.JobId);

                    foreach (var item in DocList)
                    {
                        mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
                    }

                    //Old Quote 
                    //var AttchPath = MainFolder + quotation.QuoteFileName;
                    //mail.Attachments.Add(new Attachment(AttchPath));

                    //New Quote 
                    var url = FileUrl;
                    System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                    using (System.Net.HttpWebResponse HttpWResp = (System.Net.HttpWebResponse)req.GetResponse())
                    using (Stream responseStream = HttpWResp.GetResponseStream())
                    using (MemoryStream ms = new MemoryStream())
                    {
                        responseStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        Attachment attachment = new Attachment(ms, Job.JobNumber + "_Quote.pdf");
                        mail.Attachments.Add(attachment);
                        await this._emailSender.SendAsync(mail);
                    }

                    //await this._emailSender.SendAsync(mail);
                }
                leadactivity.ActionNote = "Quotation Sent On Email";
            }

            else if (input.QuoteMode == "sendQuoteOnlyEmail")
            {
                if (!string.IsNullOrEmpty(input.EmailFrom))
                {
                    FromEmail = input.EmailFrom;
                }

                var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                var Job = _jobRepository.GetAll().Where(e => e.Id == input.JobId).Select(e => new { e.JobNumber, e.Id }).FirstOrDefault();
                //var Path = _env.WebRootPath;
                var Path = pathfordoc;
                var MainFolder = Path;
                MainFolder = MainFolder + "\\Documents" + "\\" + TenantName + "\\" + Job.JobNumber + "\\Reports\\";

                var FileUrl = _CommonDocumentSaveRepository.DownloadQuote(Job.JobNumber, input.quotationString, "Quotation");

                // var DocList = await _jobAppService.ProductItemAttachmentList((int)input.JobId);

                if (input.CC != "" && input.BCC != "")
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(FromEmail),
                        To = { input.EmailTo },
                        CC = { input.CC },
                        Bcc = { input.BCC },
                        Subject = input.Subject,
                        Body = input.EmailBody,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);

                    //foreach (var item in DocList)
                    //{
                    //    mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
                    //}

                    //Old Quote 
                    //var AttchPath = MainFolder + quotation.QuoteFileName;
                    //mail.Attachments.Add(new Attachment(AttchPath));

                    //New Quote 
                    //var url = FileUrl;
                    //System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                    //using (System.Net.HttpWebResponse HttpWResp = (System.Net.HttpWebResponse)req.GetResponse())
                    //using (Stream responseStream = HttpWResp.GetResponseStream())
                    //using (MemoryStream ms = new MemoryStream())
                    //{
                    //    responseStream.CopyTo(ms);
                    //    ms.Seek(0, SeekOrigin.Begin);
                    //    Attachment attachment = new Attachment(ms, "Quote_" + Job.JobNumber + ".pdf");
                    //    mail.Attachments.Add(attachment);
                    //    await this._emailSender.SendAsync(mail);
                    //}

                    //await this._emailSender.SendAsync(mail);
                }
                else if (input.CC != "" && input.BCC == "")
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(FromEmail),
                        To = { input.EmailTo },
                        CC = { input.CC },
                        Subject = input.Subject,
                        Body = input.EmailBody,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);

                    //var DocList = await _jobAppService.ProductItemAttachmentList((int)input.JobId);

                    //foreach (var item in DocList)
                    //{
                    //    mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
                    //}

                    //Old Quote 
                    //var AttchPath = MainFolder + quotation.QuoteFileName;
                    //mail.Attachments.Add(new Attachment(AttchPath));

                    //New Quote 
                    //var url = FileUrl;
                    //System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                    //using (System.Net.HttpWebResponse HttpWResp = (System.Net.HttpWebResponse)req.GetResponse())
                    //using (Stream responseStream = HttpWResp.GetResponseStream())
                    //using (MemoryStream ms = new MemoryStream())
                    //{
                    //    responseStream.CopyTo(ms);
                    //    ms.Seek(0, SeekOrigin.Begin);
                    //    Attachment attachment = new Attachment(ms, "Quote_" + Job.JobNumber + ".pdf");
                    //    mail.Attachments.Add(attachment);
                    //    await this._emailSender.SendAsync(mail);
                    //}

                    //await this._emailSender.SendAsync(mail);
                }
                else if (input.CC == "" && input.BCC != "")
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(FromEmail),
                        To = { input.EmailTo },
                        Bcc = { input.BCC },
                        Subject = input.Subject,
                        Body = input.EmailBody,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);

                    //var DocList = await _jobAppService.ProductItemAttachmentList((int)input.JobId);

                    //foreach (var item in DocList)
                    //{
                    //    mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
                    //}

                    //Old Quote 
                    //var AttchPath = MainFolder + quotation.QuoteFileName;
                    //mail.Attachments.Add(new Attachment(AttchPath));

                    //New Quote 
                    //var url = FileUrl;
                    //System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                    //using (System.Net.HttpWebResponse HttpWResp = (System.Net.HttpWebResponse)req.GetResponse())
                    //using (Stream responseStream = HttpWResp.GetResponseStream())
                    //using (MemoryStream ms = new MemoryStream())
                    //{
                    //    responseStream.CopyTo(ms);
                    //    ms.Seek(0, SeekOrigin.Begin);
                    //    Attachment attachment = new Attachment(ms, "Quote_" + Job.JobNumber + ".pdf");
                    //    mail.Attachments.Add(attachment);
                    //    await this._emailSender.SendAsync(mail);
                    //}

                    //await this._emailSender.SendAsync(mail);
                }
                else
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(FromEmail),
                        To = { input.EmailTo },
                        Subject = input.Subject,
                        Body = input.EmailBody,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);

                    //var DocList = await _jobAppService.ProductItemAttachmentList((int)input.JobId);

                    //foreach (var item in DocList)
                    //{
                    //    mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
                    //}

                    //Old Quote 
                    //var AttchPath = MainFolder + quotation.QuoteFileName;
                    //mail.Attachments.Add(new Attachment(AttchPath));

                    //New Quote 
                    //var url = FileUrl;
                    //System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                    //using (System.Net.HttpWebResponse HttpWResp = (System.Net.HttpWebResponse)req.GetResponse())
                    //using (Stream responseStream = HttpWResp.GetResponseStream())
                    //using (MemoryStream ms = new MemoryStream())
                    //{
                    //    responseStream.CopyTo(ms);
                    //    ms.Seek(0, SeekOrigin.Begin);
                    //    Attachment attachment = new Attachment(ms, "Quote_" + Job.JobNumber + ".pdf");
                    //    mail.Attachments.Add(attachment);
                    //    await this._emailSender.SendAsync(mail);
                    //}

                    //await this._emailSender.SendAsync(mail);
                }
                leadactivity.ActionNote = "Quotation Sent On Email";
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Quotations_Edit, AppPermissions.Pages_Jobs)]
        protected virtual async Task Update(CreateOrEditQuotationDto input)
        {
            var quotation = await _quotationRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, quotation);
        }

        [AbpAuthorize(AppPermissions.Pages_Quotations_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _quotationRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize]
        public async Task DeleteJobDocument(EntityDto input, int sectionId)
        {
            var jobdocuments = _documentRepository.GetAll().Include(e => e.JobFk)
               .Include(e => e.DocumentTypeFk)
               .OrderByDescending(e => e.Id)
               .Where(x => x.Id == input.Id).FirstOrDefault();

            var MainFolder = ApplicationSettingConsts.DocumentPath;
            var filepath = MainFolder + jobdocuments.FilePath + jobdocuments.FileName;

            File.Delete(filepath);

            await _documentRepository.DeleteAsync(input.Id);

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 44;
            leadactivity.SectionId = sectionId;
            leadactivity.ActionNote = "Document Deleted";
            leadactivity.LeadId = (int)(jobdocuments.JobFk.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        [AbpAuthorize]
        public async Task<List<GetDocumentTypeForView>> GetAllDocumentType()
        {
            var documentType = _documentTypeRepository.GetAll();
            var filtereddocType = from o in documentType
                                  select new GetDocumentTypeForView()
                                  {
                                      Id = o.Id,
                                      DocumentTitle = o.Title
                                  };
            return filtereddocType.ToList();
        }

        [AbpAuthorize]
        public async Task<List<GetJobDocumentForView>> GetJobDocumentsByJobId(int jobId)
        {
            var jobdocuments = _documentRepository.GetAll().Include(e => e.JobFk)
                .Include(e => e.DocumentTypeFk)
                .OrderByDescending(e => e.Id)
                .Where(x => x.JobId == jobId);

            var filteredJobs = from o in jobdocuments
                                   //join o1 in _jobRepository.GetAll() on o.JobId equals o1.Id into j1
                                   //from s1 in j1.DefaultIfEmpty()

                                   //join o2 in _lookup_documentTypeRepository.GetAll() on o.DocumentTypeId equals o2.Id into j2
                                   //from s2 in j2.DefaultIfEmpty()

                               join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                               from s3 in j3.DefaultIfEmpty()

                               join o4 in _userRepository.GetAll() on new { User = o.LastModifierUserId } equals new { User = (long?)o4.Id } into j4
                               from s4 in j4.DefaultIfEmpty()

                               select new GetJobDocumentForView()
                               {
                                   Id = o.Id,
                                   DocumentTypeId = o.DocumentTypeId,
                                   MediaId = o.MediaId,
                                   JobId = o.JobId,
                                   FileName = o.FileName,
                                   FileType = o.FileType,
                                   DocumentPath = o.FilePath,
                                   DocumentTitle = o.DocumentTypeFk.Title,
                                   CreatedBy = s4 == null ? s3.FullName : s4.FullName,
                                   CreationTime = o.LastModificationTime == null ? o.CreationTime : o.LastModificationTime
                               };

            return filteredJobs.OrderByDescending(e => e.CreationTime).ToList();
        }

        public async Task SaveDocument(UploadDocumentInput input)
        {
            byte[] ByteArray = _tempFileCacheManager.GetFile(input.FileToken);

            if (input.FileType != "application/vnd.openxmlformats-officedocument.wordprocessingml.document" && input.DocumentTypeId == 1)
            {
                throw new UserFriendlyException("Ooppps! There is a problem!", "Near Map sould be in word file type!");
            }
            var Jobs = _jobRepository.GetAll().Where(e => e.Id == input.JobId).FirstOrDefault();
            
            var DocumentName = _documentTypeRepository.GetAll()
             .Where(e => e.Id == input.DocumentTypeId)
             .Select(e => e.Title.Replace(" ", ""))
             .FirstOrDefault();

            var fileExtension = Path.GetExtension(input.FileName);
            var fileName = DateTime.Now.Ticks + "_" + Jobs.JobNumber + "_" + DocumentName + fileExtension;
            
            var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, fileName, "Quotation", input.JobId, input.DocumentTypeId, AbpSession.TenantId);
            
            //Word To Pddf File
            if (filepath.FinalFilePath != "" && input.DocumentTypeId == 1)
            {
                var url = viewDocPath + "/Documents/" + filepath.TenantName + "/" + filepath.JobNumber + "/NearMap/" + fileName;
                var pdf = await GenerateAnyToPdf(url, filepath.filename);

                File.Delete(filepath.FinalFilePath);

                string extension = filepath.filename.Substring(filepath.filename.LastIndexOf("."));
                using (var client = new System.Net.WebClient())
                {
                    var content = client.DownloadData(pdf);

                    fileName = fileName.Replace(extension, ".pdf");

                    string FilePath = pathfordoc + "/Documents/" + filepath.TenantName + "/" + filepath.JobNumber + "/NearMap/" + fileName;

                    System.IO.File.WriteAllBytes(string.Format("{0}", FilePath), content);
                }
            }

            var document = new Document()
            {
                Id = input.Id,
                DocumentTypeId = input.DocumentTypeId,
                JobId = input.JobId,
                //FileName = input.DocumentTypeId == 1 ? FName : input.FileName,
                FileName = fileName,
                FileType = input.FileType,
                //FilePath = input.DocumentTypeId == 1 ? "\\Documents" + "\\" + filepath.TenantName + "\\" + filepath.JobNumber + "\\NearMap\\" : "\\Documents" + "\\" + filepath.TenantName + "\\" + filepath.JobNumber + "\\Documents\\",
                FilePath = input.DocumentTypeId == 1 ? "\\Documents" + "\\" + filepath.TenantName + "\\" + filepath.JobNumber + "\\NearMap\\" : "\\Documents" + "\\" + filepath.TenantName + "\\" + filepath.JobNumber + "\\Documents\\",
                //NearMapImage1 = IMG0,
                //NearMapImage2 = IMG1,
                //NearMapImage3 = IMG2
            };
            await _documentRepository.InsertAsync(document);


            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.SectionId = input.SectionId;
            leadactivity.ActionId = 17;
            leadactivity.ActionNote = "Job Document Uploaded";
            leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        [UnitOfWork]
        public async Task<SignatureRequestDto> QuotationData(string STR)
        {
            SignatureRequestDto signatureRequestDto = new SignatureRequestDto();

            if (STR != null)
            {
                var IDs = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(STR, AppConsts.DefaultPassPhrase));
                ///var IDs = SimpleStringCipher.Instance.Decrypt(STR, AppConsts.DefaultPassPhrase);
                var ID = IDs.Split(",");
                int TenantId = Convert.ToInt32(ID[0]);
                int JobId = Convert.ToInt32(ID[1]);
                int Quotation = Convert.ToInt32(ID[2]);

                using (_unitOfWorkManager.Current.SetTenantId(TenantId))
                {
                    var JobDetail = _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.Id == JobId).FirstOrDefault();
                    //var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
                    //signatureRequestDto.CustName = Lead.CompanyName;
                    bool IsExpiried = false;
                    bool IsSigned = false;

                    var IsExpired = _quotationLinkHistoryRepository.GetAll().Where(e => e.Token == STR && e.QuotationId == Quotation).OrderByDescending(e => e.Id).FirstOrDefault();

                    var QuoteDetails = await _quotationDetailRepqository.GetAll().Where(e => e.QuotationId == Quotation).FirstOrDefaultAsync();
                    var QunityAndModelDetail = await _quotationQunityAndModelDetailRepqository.GetAll().Where(e => e.QuotationDetailId == QuoteDetails.Id).ToListAsync();
                    //var 

                    if (IsExpired != null)
                    {
                        if (IsExpired.CreationTime.AddHours(24) <= DateTime.UtcNow)
                        {
                            IsExpiried = true;
                        }
                        if (IsExpired.Expired == true)
                        {
                            IsSigned = true;
                        }

                        if (IsExpiried == false && IsSigned == false)
                        {
                            //var RoofType = _roofTypeRepository.GetAll().Where(e => e.Id == JobDetail.RoofTypeId).Select(e => e.Name).FirstOrDefault();
                            //var RoofAngle = _roofAngleRepository.GetAll().Where(e => e.Id == JobDetail.RoofAngleId).Select(e => e.Name).FirstOrDefault();
                            //var HouseType = _houseTypeRepository.GetAll().Where(e => e.Id == JobDetail.HouseTypeId).Select(e => e.Name).FirstOrDefault();
                            //var Dist = _elecDistributorRepository.GetAll().Where(e => e.Id == JobDetail.ElecDistributorId).Select(e => e.Name).FirstOrDefault();
                            //var Retailer = _elecRetailerRepository.GetAll().Where(e => e.Id == JobDetail.ElecRetailerId).Select(e => e.Name).FirstOrDefault();
                            ////var NoofPanel = _jobProductItemRepository.GetAll().Where(e => e.JobId == JobDetail.Id && e.ProductItemId == 1).Select(e => e.Quantity).FirstOrDefault();
                            //var ProductItemList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).Select(e => e.ProductItemId).ToList();
                            //var PanelDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 1).FirstOrDefault();
                            //var ProductItemsList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).ToList();
                            //var PanelQTY = new int?();
                            //if (PanelDetail != null)
                            //{
                            //    PanelQTY = ProductItemsList.Where(e => e.ProductItemId == PanelDetail.Id).Select(e => e.Quantity).FirstOrDefault();
                            //}
                            //var InverterDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 2).FirstOrDefault();
                            //var QuoteNo = _quotationRepository.GetAll().Where(e => e.Id == Quotation).Select(e => e.QuoteNumber).FirstOrDefault();
                            //var NearMap = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 1).OrderByDescending(e => e.Id).FirstOrDefault();
                            //var MinusList = _variationRepository.GetAll().Where(e => e.Action == "Minus").Select(e => e.Id).ToList();
                            //var PlusList = _variationRepository.GetAll().Where(e => e.Action == "Plus").Select(e => e.Id).ToList();
                            //var Addition = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && PlusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
                            //var Discount = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && MinusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
                            //var Installer = _userRepository.GetAll().Where(e => e.Id == JobDetail.InstallerId).FirstOrDefault();
                            //var TotalDeposite = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id).Select(e => e.InvoicePayTotal).Sum();
                            //var Finance = _financeOptionRepository.GetAll().Where(e => e.Id == JobDetail.FinanceOptionId).Select(e => e.Name).FirstOrDefault();
                            //var Dep = _depositOptionRepository.GetAll().Where(e => e.Id == JobDetail.DepositOptionId).Select(e => e.Name).FirstOrDefault();
                            //var Payment = _paymentOptionRepository.GetAll().Where(e => e.Id == JobDetail.PaymentOptionId).Select(e => e.Name).FirstOrDefault();


                            //signatureRequestDto.Expiry = false;
                            //var test = "";
                            //int i = 0;
                            //foreach (var item in Addition)
                            //{
                            //    if (i == 0)
                            //    {
                            //        test = Convert.ToString(item);
                            //    }
                            //    else
                            //    {
                            //        test = test + "+" + Convert.ToString(item);
                            //    }
                            //    i++;
                            //}
                            //var test1 = "";
                            //int j = 0;
                            //foreach (var item in Discount)
                            //{
                            //    if (j == 0)
                            //    {
                            //        test1 = Convert.ToString(item);
                            //    }
                            //    else
                            //    {
                            //        test1 = test1 + "+" + Convert.ToString(item);
                            //    }
                            //    j++;
                            //}
                            //signatureRequestDto.QuoteNo = QuoteNo;
                            //signatureRequestDto.FinanceOption = string.IsNullOrEmpty(Finance) ? null : Convert.ToString(Finance).ToUpper();
                            //signatureRequestDto.DepositOption = Dep;
                            //signatureRequestDto.PaymentOption = Payment;
                            //signatureRequestDto.PaymentOptionId = JobDetail.PaymentOptionId;
                            //if (Installer != null)
                            //    signatureRequestDto.InstName = Installer.FullName;

                            //signatureRequestDto.CustMobile = Lead.Mobile;
                            //signatureRequestDto.CustEmail = Lead.Email;
                            //signatureRequestDto.Address1 = Lead.Address;
                            //signatureRequestDto.Address2 = Lead.Suburb + ", " + Lead.State + " - " + Lead.PostCode;
                            //signatureRequestDto.SystemCapecity = Convert.ToString(JobDetail.SystemCapacity);
                            //signatureRequestDto.TotalCost = Convert.ToString(JobDetail.TotalCost);

                            //signatureRequestDto.MeterPhase = Convert.ToString(JobDetail.MeterPhaseId);
                            //signatureRequestDto.SubTotal = Convert.ToString(JobDetail.BasicCost);
                            //signatureRequestDto.SpecialDiscount = test1;
                            //signatureRequestDto.AdditionalCharges = test;
                            //signatureRequestDto.DepositePaid = Convert.ToString(TotalDeposite);
                            //signatureRequestDto.Organization = Organization;
                            //signatureRequestDto.State = Lead.State;
                            //if (Lead.State == "VIC")
                            //{
                            //    var solrvicrebate = JobDetail.SolarVICRebate == null ? 0 : JobDetail.SolarVICRebate;
                            //    var solrvicrebatedescount = JobDetail.SolarVICLoanDiscont == null ? 0 : JobDetail.SolarVICLoanDiscont;
                            //    signatureRequestDto.SolarVICRebate = Convert.ToString(solrvicrebate);
                            //    signatureRequestDto.SolarVICLoanDiscont = Convert.ToString(solrvicrebatedescount);
                            //    signatureRequestDto.Balance = Convert.ToString(JobDetail.TotalCost - (solrvicrebate + solrvicrebatedescount + TotalDeposite));
                            //}
                            //else
                            //{
                            //    signatureRequestDto.Balance = Convert.ToString(JobDetail.TotalCost - TotalDeposite);
                            //}

                            //if (JobDetail.MeterUpgradeId == 1)
                            //{
                            //    signatureRequestDto.Upgrades = "Yes";
                            //}
                            //else
                            //{
                            //    signatureRequestDto.Upgrades = "No";
                            //}

                            //signatureRequestDto.NonOfStory = HouseType;

                            //signatureRequestDto.RoofType = RoofType;
                            //signatureRequestDto.RoofPinch = RoofAngle;
                            //signatureRequestDto.EnergyDist = Dist;
                            //signatureRequestDto.EnergyRet = Retailer;
                            //if (JobDetail.InstallationDate != null)
                            //{
                            //    signatureRequestDto.InstallationDate = JobDetail.InstallationDate.Value.ToString("DD/MM/yyyy");
                            //}
                            //if (PanelDetail != null)
                            //{
                            //    signatureRequestDto.PanelName = PanelDetail.Name;
                            //    signatureRequestDto.PanelBrand = PanelDetail.Manufacturer;
                            //    signatureRequestDto.PanelModel = PanelDetail.Model;
                            //}
                            //if (InverterDetail != null)
                            //{
                            //    signatureRequestDto.InverterName = InverterDetail.Name;
                            //    signatureRequestDto.InverterBrand = InverterDetail.Manufacturer;
                            //    signatureRequestDto.InverterModel = InverterDetail.Model;
                            //}
                            //signatureRequestDto.Notes = JobDetail.Note;
                            //signatureRequestDto.NoOfPanel = Convert.ToString(PanelQTY);
                            //signatureRequestDto.NoOfSTC = Convert.ToString(JobDetail.STC);
                            //signatureRequestDto.NmiNumber = Convert.ToString(JobDetail.NMINumber);
                            //signatureRequestDto.Date = DateTime.UtcNow.ToLongDateString();

                            signatureRequestDto.Expiry = false;
                            signatureRequestDto.CustName = QuoteDetails.Name;
                            signatureRequestDto.CustMobile = QuoteDetails.Mobile;
                            signatureRequestDto.CustEmail = QuoteDetails.Email;
                            signatureRequestDto.Address1 = QuoteDetails.Address1;
                            signatureRequestDto.Address2 = QuoteDetails.Address2;
                            signatureRequestDto.State = JobDetail.State;
                            signatureRequestDto.JobNotes = JobDetail.Note;

                            signatureRequestDto.MeterPhase = QuoteDetails.MeaterPhase;
                            signatureRequestDto.Upgrades = QuoteDetails.MeaterUpgrade;
                            signatureRequestDto.RoofType = QuoteDetails.RoofTypes;
                            signatureRequestDto.RoofPinch = QuoteDetails.RoofPinch;
                            signatureRequestDto.EnergyDist = QuoteDetails.EnergyDist;
                            signatureRequestDto.EnergyRet = QuoteDetails.EnergyRetailer;
                            signatureRequestDto.NonOfStory = QuoteDetails.NoofStory;
                            signatureRequestDto.NMINumber = QuoteDetails.NMINumber;
                            signatureRequestDto.Capecity = QuoteDetails.Capecity;
                            //signatureRequestDto.BatteryCapecity = QuoteDetails.BatteryCapecity;

                            signatureRequestDto.JobNumber = QuoteDetails.QuoteNumber;

                            if (QunityAndModelDetail.Any())
                            {
                                signatureRequestDto.QunityAndModelLists = ObjectMapper.Map<List<QunityAndModelList>>(QunityAndModelDetail);
                            }

                            signatureRequestDto.SubTotal = QuoteDetails.SubTotal;
                            signatureRequestDto.STC = QuoteDetails.STC;
                            signatureRequestDto.STCDesc = QuoteDetails.STCDesc;
                            signatureRequestDto.GrandTotal = QuoteDetails.GrandTotal;
                            signatureRequestDto.SolarVICRebate = QuoteDetails.SolarVICRebate;
                            signatureRequestDto.SolarVICLoanDiscount = QuoteDetails.SolarVICLoanDiscount;
                            signatureRequestDto.AdditionalCharges = QuoteDetails.AdditionalCharges;

                            signatureRequestDto.DepositRequired = QuoteDetails.Deposite;
                            signatureRequestDto.DepositeText = QuoteDetails.DepositeText;
                            signatureRequestDto.Balance = QuoteDetails.Balance;

                            signatureRequestDto.Discount = QuoteDetails.SpecialDiscount;

                            signatureRequestDto.Total = QuoteDetails.Total;

                            if (!string.IsNullOrEmpty(signatureRequestDto.GrandTotal))
                            {
                                var _gt = Convert.ToDecimal(signatureRequestDto.GrandTotal);
                                var _ac = !string.IsNullOrEmpty(QuoteDetails.AdditionalCharges) ? Convert.ToDecimal(QuoteDetails.AdditionalCharges) : 0;
                                var _disct = !string.IsNullOrEmpty(QuoteDetails.SpecialDiscount) ? Convert.ToDecimal(QuoteDetails.SpecialDiscount) : 0;

                                var tot = (_gt + _ac) - _disct;

                                if (JobDetail.State == "VIC")
                                {
                                    var Rebate = !string.IsNullOrEmpty(QuoteDetails.SolarVICRebate) ? Convert.ToDecimal(QuoteDetails.SolarVICRebate) : 0;
                                    var Loan = !string.IsNullOrEmpty(QuoteDetails.SolarVICLoanDiscount) ? Convert.ToDecimal(QuoteDetails.SolarVICLoanDiscount) : 0;

                                    tot = tot - Rebate - Loan;
                                }

                                signatureRequestDto.Total = tot.ToString("F");
                            }

                            signatureRequestDto.OrganizationId = JobDetail.LeadFk.OrganizationId;
                            var OrgLogo = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == JobDetail.LeadFk.OrganizationId).Select(e => (e.LogoFilePath + e.LogoFileName)).FirstOrDefault();
                            signatureRequestDto.OrgLogo = OrgLogo != null ? OrgLogo.Replace("\\", "/") : "";

                            var Organization = _OrganizationRepository.GetAll().Where(e => e.Id == JobDetail.LeadFk.OrganizationId).Select(e => e.DisplayName).FirstOrDefault();
                            signatureRequestDto.Organization = Organization;

                            signatureRequestDto.VicRebate = QuoteDetails.VICRebate;

                            return signatureRequestDto;

                        }
                        else
                        {
                            signatureRequestDto.Expiry = IsExpiried;
                            signatureRequestDto.Signed = IsSigned;
                            return signatureRequestDto;
                        }
                    }
                    else
                    {
                        signatureRequestDto.Expiry = IsExpiried;
                        signatureRequestDto.Signed = IsSigned;
                        return signatureRequestDto;
                    }
                }
            }
            else
            {
                signatureRequestDto.Expiry = true;
                signatureRequestDto.Signed = false;
                return signatureRequestDto;
            }
        }

        public async Task SaveSignature(SaveCustomerSignature input)
        {
            try
            {
                var FileName = DateTime.Now.Ticks + "_CustomerSignature.png";
                var IMage = input.ImageData.Split(new[] { ',' }, 21);
                var buffer = new byte[2097152];
                byte[] ByteArray = Convert.FromBase64String(IMage[1]);
                var IDs = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(input.EncString, AppConsts.DefaultPassPhrase));
                ///var IDs = SimpleStringCipher.Instance.Decrypt(input.EncString, AppConsts.DefaultPassPhrase);
                var ID = IDs.Split(",");
                int TenantId = Convert.ToInt32(ID[0]);
                int JobId = Convert.ToInt32(ID[1]);
                int Quotation = Convert.ToInt32(ID[2]);

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (_unitOfWorkManager.Current.SetTenantId(TenantId))
                    {
                        var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, FileName, "CustomerSignature", JobId, 0, AbpSession.TenantId);

                        var quotation = _quotationRepository.GetAll().Where(e => e.Id == Quotation).OrderByDescending(e => e.Id).FirstOrDefault();
                        var leadid = _jobRepository.GetAll().Where(e => e.Id == quotation.JobId).Select(e => e.LeadId).FirstOrDefault();
                        var organizationid = _leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.OrganizationId).FirstOrDefault();
                        //var SignedQuoteFileName = _reportAppService.Quotation(JobId, Quotation, filepath.FinalFilePath, quotation.QuoteNumber, organizationid) + ".pdf";

                        quotation.CustSignLatitude = input.CustSignLatitude;
                        quotation.CustSignLongitude = input.CustSignLongitude;
                        quotation.CustSignIP = input.CustSignIP;
                        quotation.CustSignFileName = FileName;
                        quotation.IsSigned = true;
                        quotation.QuoteAcceptDate = DateTime.UtcNow;
                        quotation.CustSignFilePath = "/Documents/" + filepath.TenantName + "/" + filepath.JobNumber + "/CustomerSignature/";
                        await _quotationRepository.UpdateAsync(quotation);

                        var Link = _quotationLinkHistoryRepository.GetAll().Where(e => e.Token == input.EncString && e.QuotationId == Quotation);
                        foreach (var item in Link)
                        {
                            item.Expired = true;
                            await _quotationLinkHistoryRepository.UpdateAsync(item);
                        }

                        //await _jobAppService.JobActive(JobId);

                        var Lead = _leadRepository.GetAll().Where(e => e.Id == _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.LeadId).FirstOrDefault()).FirstOrDefault();
                        //var User_List = _userRepository.GetAll().ToList();
                        //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == Lead.AssignToUserID).Select(e => e.TeamId).FirstOrDefault();
                        var AssignedUser = _userRepository.GetAll().Where(e => e.Id == Lead.AssignToUserID).FirstOrDefault();

                        string msg = string.Format("Customer Siagnture Received For {0} Job", filepath.JobNumber);
                        await _appNotifier.LeadAssiged(AssignedUser, msg, NotificationSeverity.Info);

                        //var UserList = (from user in User_List
                        //                join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                        //                from ur in urJoined.DefaultIfEmpty()

                        //                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                        //                from us in usJoined.DefaultIfEmpty()

                        //                join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                        //                from ut in utJoined.DefaultIfEmpty()

                        //                where us != null && us.DisplayName == "Sales Manager" && ut != null && ut.TeamId == TeamId
                        //                select user);

                        //foreach (var item in UserList)
                        //{
                        //    await _appNotifier.LeadAssiged(item, msg, NotificationSeverity.Info);
                        //}
                        uow.Complete();
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        protected virtual async Task<QuotationDataDto> GetQuotationData(int OrgId, int QuotationId, Job JobDetail, DateTime date)
        {
            QuotationDataDto qData = new QuotationDataDto();
            int JobId = JobDetail.Id;
            var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
            var RoofType = _roofTypeRepository.GetAll().Where(e => e.Id == JobDetail.RoofTypeId).Select(e => e.Name).FirstOrDefault();
            var RoofAngle = _roofAngleRepository.GetAll().Where(e => e.Id == JobDetail.RoofAngleId).Select(e => e.Name).FirstOrDefault();
            var HouseType = _houseTypeRepository.GetAll().Where(e => e.Id == JobDetail.HouseTypeId).Select(e => e.Name).FirstOrDefault();
            var Dist = _elecDistributorRepository.GetAll().Where(e => e.Id == JobDetail.ElecDistributorId).Select(e => e.Name).FirstOrDefault();
            var Retailer = _elecRetailerRepository.GetAll().Where(e => e.Id == JobDetail.ElecRetailerId).Select(e => e.Name).FirstOrDefault();
            //var ProductItemList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).Select(e => e.ProductItemId).ToList();
            //var ProductItemsList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).ToList();
            //var PanelDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 1).FirstOrDefault();
            //var InverterDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 2).ToList();
            var NearMap = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 1 && e.JobId == JobId).OrderByDescending(e => e.Id).FirstOrDefault();
            var MinusList = _variationRepository.GetAll().Where(e => e.Action == "Minus").Select(e => e.Id).ToList();
            var PlusList = _variationRepository.GetAll().Where(e => e.Action == "Plus").Select(e => e.Id).ToList();
            var Addition = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && PlusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
            var Discount = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && MinusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
            var SalesRep = _userRepository.GetAll().Where(e => e.Id == Lead.AssignToUserID).FirstOrDefault();
            var TotalDeposite = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id && e.InvoicePaymentStatusId == 2).Select(e => e.InvoicePayTotal).Sum();
            var Finance = _financeOptionRepository.GetAll().Where(e => e.Id == JobDetail.FinanceOptionId).Select(e => e.Name).FirstOrDefault();
            var Dep = _depositOptionRepository.GetAll().Where(e => e.Id == JobDetail.DepositOptionId).Select(e => e.Name).FirstOrDefault();
            var Payment = _paymentOptionRepository.GetAll().Where(e => e.Id == JobDetail.PaymentOptionId).Select(e => e.Name).FirstOrDefault();

            var Document = _documentRepository.GetAll().Where(e => e.JobId == JobDetail.Id && e.DocumentTypeId == 15).FirstOrDefault();

            //var QunityAndModelList = (from o in _jobProductItemRepository.GetAll()
            //						  join o1 in _ProductItemRepository.GetAll() on o.ProductItemId equals o1.Id into j1
            //						  from s1 in j1.DefaultIfEmpty()
            //						  where (o.JobId == JobDetail.Id)
            //						  select new
            //						  {
            //							  Name = o.Quantity + " x " + s1.Name
            //						  }).ToList();

            var jobProduct = _jobProductItemRepository.GetAll().Include(e => e.ProductItemFk).Where(e => e.JobId == JobDetail.Id).Select(e => new { e.ProductItemFk.ProductTypeId, e.Quantity, e.ProductItemFk.Name, e.ProductItemFk.Size });

            qData.qunityAndModelLists = (from o in jobProduct
                                         select new QunityAndModelList
                                         {
                                             Name = o.Quantity + " x " + o.Name
                                         }).ToList();

            var btryItem = jobProduct.Where(e => e.ProductTypeId == 5).ToList();
            decimal? batryKw = 0.0m;
            foreach (var item in btryItem)
            {
                batryKw += (item.Quantity * item.Size);
            }

            qData.BatteryCapecity = btryItem.Count > 0 ? batryKw.ToString() : "";

            //var systemWith = new List<int> { 1, 5 };
            //if(jobProduct.Any(e => systemWith.Contains(e.ProductTypeId)))
            //if (jobProduct.Any(e => e.ProductTypeId == 1 && e.ProductTypeId == 5))
            //{
            //    qData.SystemWith = "With Battery";
            //}
            //else if (jobProduct.Count() == jobProduct.Count(e => e.ProductTypeId == 5))
            //{
            //    qData.SystemWith = "Only Battery";
            //}
            //else if (jobProduct.Count() == jobProduct.Count(e => e.ProductTypeId == 2))
            //{
            //    qData.SystemWith = "Only Inverter";
            //}
            //else if (jobProduct.Count(e => e.ProductTypeId != 5) == jobProduct.Count())
            //{
            //    qData.SystemWith = "Without Battery";
            //}
            //else if (jobProduct.Count(e => e.ProductTypeId != 1) == jobProduct.Count())
            //{
            //    qData.SystemWith = "Without Panel";
            //}

            if (jobProduct.Count() == jobProduct.Count(e => e.ProductTypeId == 2))
            {
                qData.SystemWith = "Only Inverter";
            }
            else if (jobProduct.Count() == jobProduct.Count(e => e.ProductTypeId == 5))
            {
                qData.SystemWith = "Only Battery";
            }
            else if (jobProduct.Any(e => e.ProductTypeId == 5) && jobProduct.Count(e => e.ProductTypeId == 1) == 0)
            {
                qData.SystemWith = "Panel Without Battery";
            }
            else if (jobProduct.Any(e => e.ProductTypeId == 5) && jobProduct.Any(e => e.ProductTypeId == 1))
            {
                qData.SystemWith = "Panel With Battery";
            }

            //qData.qunityAndModelLists = (from o in _jobProductItemRepository.GetAll()
            //                             join o1 in _ProductItemRepository.GetAll() on o.ProductItemId equals o1.Id into j1
            //                             from s1 in j1.DefaultIfEmpty()
            //                             where (o.JobId == JobDetail.Id)
            //                             select new QunityAndModelList
            //                             {
            //                                 Name = o.Quantity + " x " + s1.Name
            //                             }).ToList();


            var test = 0;
            int i = 0;
            foreach (var item in Addition)
            {
                if (i == 0)
                {
                    test = Convert.ToInt32(item);
                }
                else
                {
                    test = test + Convert.ToInt32(item);
                }
                i++;
            }
            var test1 = 0;
            int j = 0;
            foreach (var item in Discount)
            {
                if (j == 0)
                {
                    test1 = Convert.ToInt32(item);
                }
                else
                {
                    test1 = test1 + Convert.ToInt32(item);
                }
                j++;
            }

            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            qData.JobId = JobId;
            qData.QuotationId = QuotationId;
            qData.QuoteNumber = JobDetail.JobNumber;
            qData.Date = date.ToString("MMM dd, yyyy");
            qData.Name = textInfo.ToTitleCase(Lead.CompanyName.ToLower());
            qData.Mobile = Lead.Mobile;
            qData.Email = Lead.Email;

            qData.Notes = !string.IsNullOrEmpty(JobDetail.Note) ? Regex.Replace(JobDetail.Note, @"\r\n?|\n", "<br />") : "";
            var Rebate = JobDetail.Rebate == null ? 0 : JobDetail.Rebate;

            if (TotalDeposite == 0 || TotalDeposite == null)
            {
                qData.DepositeText = "Deposit Required";
                //qData.Deposite = JobDetail.DepositRequired == 0 || JobDetail.DepositRequired == null ? Math.Round((double)JobDetail.BasicCost * 10 / 100) + ".00" : Math.Round((double)JobDetail.DepositRequired) + ".00";
                qData.Deposite = JobDetail.DepositRequired.ToString();
            }
            else
            {
                qData.DepositeText = "Deposit Paid";
                qData.Deposite = TotalDeposite.ToString();
            }

            //if (OrgId == 1 || OrgId == 7)
            if (JobDetail.State != "VIC")
            {
                if (JobDetail.Address != null)
                {
                    qData.Address1 = textInfo.ToTitleCase(Convert.ToString(JobDetail.Address.ToLower()).Trim());
                }
                qData.Address2 = textInfo.ToTitleCase((JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode).ToLower());

                qData.Capecity = Convert.ToString(JobDetail.SystemCapacity);

                qData.Total = Convert.ToString(JobDetail.BasicCost) == null ? "0.00" : Convert.ToString(JobDetail.BasicCost);

                if (TotalDeposite == 0 || TotalDeposite == null)
                {
                    qData.Balance = Convert.ToString((JobDetail.BasicCost) + test - test1) == null ? "0.00" : Convert.ToString((JobDetail.BasicCost) + test - test1);
                }
                else
                {
                    qData.Balance = Convert.ToString((JobDetail.BasicCost) + test - test1 - TotalDeposite) == null ? "0.00" : Convert.ToString((JobDetail.BasicCost) + test - test1 - TotalDeposite);
                }

                if (JobDetail.PaymentOptionId == 1)
                {
                    qData.PaymentMethod = "Cash";
                }
                else
                {
                    qData.PaymentMethod = Payment + " with " + Dep + " for " + Finance;
                }

                qData.SpecialDiscount = test1 == 0 ? "0.00" : test1 + ".00";

            }
            //else if (OrgId == 2 || OrgId == 8)
            else if (JobDetail.State == "VIC")
            {
                qData.DepositRequired = Convert.ToString(JobDetail.DepositRequired);

                if (JobDetail.Address != null)
                {
                    qData.Address1 = textInfo.ToTitleCase(Convert.ToString(JobDetail.Address.ToLower()).Trim());
                }
                qData.Address2 = textInfo.ToTitleCase((JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode).ToLower());

                qData.Capecity = Convert.ToString(JobDetail.SystemCapacity);

                qData.Total = Convert.ToString(JobDetail.BasicCost + Rebate) == null ? "0.00" : Convert.ToString(JobDetail.BasicCost + Rebate);

                qData.SolarVICRebate = Convert.ToString(JobDetail.SolarVICRebate) == null ? "0.00" : Convert.ToString(JobDetail.SolarVICRebate);
                qData.SolarVICLoanDiscount = Convert.ToString(JobDetail.SolarVICLoanDiscont) == null ? "0.00" : Convert.ToString(JobDetail.SolarVICLoanDiscont);
                var stcintentives = Convert.ToDecimal((JobDetail.BasicCost + Rebate) - (Rebate)) == null ? 0 : Convert.ToDecimal((JobDetail.BasicCost + Rebate) - (Rebate));
                var SolarVICRebates = Convert.ToDecimal(JobDetail.SolarVICRebate) == null ? 0 : Convert.ToDecimal(JobDetail.SolarVICRebate);
                var SolarVICLoanDiscounts = Convert.ToDecimal(JobDetail.SolarVICLoanDiscont) == null ? 0 : Convert.ToDecimal(JobDetail.SolarVICLoanDiscont);

                qData.STCIncetive = Convert.ToString((JobDetail.BasicCost + Rebate) - (Rebate) + ((test - test1))) == null ? "0.00" : Convert.ToString(((JobDetail.BasicCost + Rebate) - (Rebate) + ((test - test1))) - SolarVICRebates - SolarVICLoanDiscounts);

                qData.FinalPrice = Convert.ToString(stcintentives - SolarVICRebates - SolarVICLoanDiscounts);

                var totaalvicRebate = ((JobDetail.SolarVICRebate == null ? 0 : JobDetail.SolarVICRebate) + (JobDetail.SolarVICLoanDiscont == null ? 0 : JobDetail.SolarVICLoanDiscont));

                decimal? FinalDeposite;
                if (TotalDeposite == 0 || TotalDeposite == null)
                {
                    decimal? finalcost = ((JobDetail.BasicCost) - (totaalvicRebate));
                    qData.Deposite = Convert.ToString(JobDetail.DepositRequired);
                    FinalDeposite = JobDetail.DepositRequired;
                }
                else
                {
                    qData.Deposite = Convert.ToString(TotalDeposite);
                    FinalDeposite = TotalDeposite;
                }

                if (TotalDeposite == 0 || TotalDeposite == null)
                {
                    qData.Balance = Convert.ToString((JobDetail.BasicCost) + test - test1 - (totaalvicRebate)) == null ? "0.00" : Convert.ToString((JobDetail.BasicCost) + test - test1 - (totaalvicRebate));
                }
                else
                {
                    qData.Balance = Convert.ToString((JobDetail.BasicCost) + test - test1 - (totaalvicRebate) - TotalDeposite) == null ? "0.00" : Convert.ToString((JobDetail.BasicCost) + test - test1 - (totaalvicRebate) - TotalDeposite);
                }

                qData.SpecialDiscount = test1 == 0 ? "0.00" : test1 + ".00";
            }

            if (SalesRep != null)
            {
                qData.InstName = SalesRep.FullName;
                if (SalesRep.Mobile != null)
                {
                    qData.InstMobile = SalesRep.PhoneNumber + "/" + SalesRep.Mobile;
                }
                else
                {
                    qData.InstMobile = SalesRep.PhoneNumber;
                }

                //qData.InstEmail = SalesRep.EmailAddress;
                var orgEmail = _userWiseEmailOrgRepqository.GetAll().Where(e => e.UserId == SalesRep.Id && e.OrganizationUnitId == Lead.OrganizationId).FirstOrDefault();
                if (orgEmail != null)
                {
                    qData.InstEmail = orgEmail.EmailFromAdress;
                }
            }

            qData.AdditionalCharges = test == 0 ? "0.00" : test + ".00";
            if (JobDetail.MeterUpgradeId == 1)
            {
                qData.MeaterUpgrade = "Yes";
            }
            else
            {
                qData.MeaterUpgrade = "No";
            }

            qData.NMINumber = JobDetail.NMINumber;

            var isbattryRebate = await _stateRepqository.GetAll().Where(e => e.Name == JobDetail.State).FirstOrDefaultAsync();

            qData.BatteryRebate = 0;
            if (isbattryRebate.IsActiveBatteryRebate)
            {
                if (JobDetail.IsBatteryRebate == true)
                {
                    qData.BatteryRebate = JobDetail.BatteryRebate;
                }
            }


            qData.SubTotal = Convert.ToString(JobDetail.BasicCost + Rebate + qData.BatteryRebate) == null ? "0.00" : Convert.ToString(JobDetail.BasicCost + Rebate + qData.BatteryRebate);
            qData.STC = Convert.ToString(Rebate) == null ? "0.00" : Convert.ToString(Rebate);
            qData.STCDesc = JobDetail.STC + " X $" + JobDetail.STCPrice;
            qData.GrandTotal = Convert.ToString((JobDetail.BasicCost + Rebate + qData.BatteryRebate) - (Rebate) - (qData.BatteryRebate)) == null ? "0.00" : Convert.ToString((JobDetail.BasicCost + Rebate + qData.BatteryRebate) - (Rebate) - (qData.BatteryRebate));

            //Start Battery Rebate

            //End

            qData.MeaterPhase = Convert.ToString(JobDetail.MeterPhaseId);

            qData.NoofStory = HouseType;
            qData.RoofTypes = RoofType;
            qData.RoofPinch = RoofAngle;
            qData.EnergyDist = Dist;
            qData.EnergyRetailer = Retailer;
            qData.MeterNo = JobDetail.PeakMeterNo;

            qData.VicRebate = JobDetail.VicRebate;

            qData.InverterLocation = JobDetail.InverterLocation;
            qData.TotalCost = Convert.ToString(JobDetail.TotalCost) == null ? "0.00" : Convert.ToString(JobDetail.TotalCost);

            if (Document != null)
            {
                if (!string.IsNullOrEmpty(Document.FilePath) && !string.IsNullOrEmpty(Document.FileName))
                {
                    qData.NearMap1 = Document.FilePath + "\\" + Document.FileName;
                }
            }

            //if (NearMap != null)
            //{
            //    if (!string.IsNullOrEmpty(NearMap.NearMapImage1))
            //    {
            //        //Image image1 = Image.FromFile(pathfordoc + NearMap.FilePath + "\\" + NearMap.NearMapImage1);
            //        //qData.NearMap1 = pathfordoc + NearMap.FilePath + "\\" + NearMap.NearMapImage1;
            //    }
            //    if (!string.IsNullOrEmpty(NearMap.NearMapImage2))
            //    {
            //        //Image image2 = Image.FromFile(pathfordoc + NearMap.FilePath + "\\" + NearMap.NearMapImage2);
            //        qData.NearMap2 = pathfordoc + NearMap.FilePath + "\\" + NearMap.NearMapImage2;
            //    }
            //    if (!string.IsNullOrEmpty(NearMap.NearMapImage3))
            //    {
            //        //Image image3 = Image.FromFile(pathfordoc + NearMap.FilePath + "\\" + NearMap.NearMapImage3);
            //        qData.NearMap3 = pathfordoc + NearMap.FilePath + "\\" + NearMap.NearMapImage3;
            //    }
            //}

            await Task.CompletedTask;
            return qData;
        }

        [AbpAuthorize]
        public async Task<QuotationDataDto> GetQuotationDataByQuoateIdJobId(int jobId, int? qouteId)
        {
            //qouteId = _quotationDetailRepqository.GetAll().Where(e => e.JobId == jobId).OrderBy(e => e.QuotationId).Select(e => e.QuotationId).FirstOrDefault();
            qouteId = qouteId == null ? _quotationDetailRepqository.GetAll().Where(e => e.JobId == jobId).OrderByDescending(e => e.QuotationId).Select(e => e.QuotationId).FirstOrDefault() : qouteId;

            var Job = await _jobRepository.GetAll().Include(e => e.PaymentOptionFk)
                .Include(e => e.FinanceOptionFk)
                .Include(e => e.DepositOptionFk)
                .Where(e => e.Id == jobId).AsNoTracking()
                .Select(e => new
                {
                    e.LeadId,
                    PaymentOption = e.PaymentOptionFk.Name,
                    FinPaymentType = e.FinanceOptionFk.Name,
                    DepositOption = e.DepositOptionFk.Name,
                    e.FinancePurchaseNo,
                    e.FinanceAmount,
                    e.FinanceDepositeAmount,
                    e.FinanceNetAmount,
                    e.PaymentTerm,
                    e.RepaymentAmt,
                    e.RebateRefNo
                }).FirstOrDefaultAsync();

            var productItem = _jobProductItemRepository.GetAll().Where(e => e.ProductItemFk.ProductTypeId == 1 && e.JobId == jobId).AsNoTracking()
                            .Select(e => new { e.ProductItemFk.ExtendedWarranty, e.ProductItemFk.PerformanceWarranty, e.ProductItemFk.ProductWarranty, e.ProductItemFk.WorkmanshipWarranty, e.ProductItemFk.ManuWebsite, e.ProductItemFk.ManuPhone, e.ProductItemFk.ManuEmail, e.ProductItemFk.ManuMobile }).FirstOrDefault();

            var QuoteDetails = await _quotationDetailRepqository.GetAll().Where(e => e.JobId == jobId && e.QuotationId == qouteId).FirstOrDefaultAsync();
            var QunityAndModelDetail = await _quotationQunityAndModelDetailRepqository.GetAll().Where(e => e.QuotationDetailId == QuoteDetails.Id).ToListAsync();

            var output = ObjectMapper.Map<QuotationDataDto>(QuoteDetails);



            if (QunityAndModelDetail.Any())
            {
                output.qunityAndModelLists = ObjectMapper.Map<List<QunityAndModelList>>(QunityAndModelDetail);

                if (output.qunityAndModelLists.Count == 2)
                {
                    output.qunityAndModelLists = output.qunityAndModelLists.OrderByDescending(e => e.Id).ToList();
                }
            }

            var orgId = _leadRepository.GetAll().Where(e => e.Id == Job.LeadId).Select(e => e.OrganizationId).FirstOrDefault();

            if (orgId == 8) //SolarMiner
            {
                output.RepresentativeSignSrc = "https://thesolarproduct.com/assets/common/images/sm/signature.png";
            }

            var SignQuote = _quotationRepository.GetAll().Where(e => e.Id == QuoteDetails.QuotationId).FirstOrDefault();
            if (SignQuote != null)
            {
                if (!string.IsNullOrEmpty(SignQuote.CustSignFileName))
                {
                    output.SignName = QuoteDetails.Name;

                    output.SignDate = SignQuote.QuoteAcceptDate == null ? "" : Convert.ToDateTime(_timeZoneConverter.Convert(SignQuote.QuoteAcceptDate, (int)AbpSession.TenantId)).ToString("MMM dd, yyyy hh:mm tt");

                    //output.SignSrc = "/Documents/solarproduct/"+ Job.JobNumber + "/CustomerSignature/" + SignQuote.CustSignFileName;
                    output.SignSrc = SignQuote.CustSignFilePath + SignQuote.CustSignFileName;
                }
            }

            var quoteTemplate = GetTemplate(orgId, 1);
            if (!string.IsNullOrEmpty(quoteTemplate.ViewHtml))
            {
                output.ViewHtml = quoteTemplate.ViewHtml;
            }

            output.PaymentOption = Job.PaymentOption;
            output.FinPaymentType = Job.FinPaymentType;
            output.DepositOption = Job.DepositOption;
            output.FinancePurchaseNo = Job.FinancePurchaseNo;
            output.FinanceAmount = Job.FinanceAmount;
            output.FinanceDepositeAmount = Job.FinanceDepositeAmount;
            output.FinanceNetAmount = Job.FinanceNetAmount;
            output.PaymentTerm = Job.PaymentTerm;
            output.RepaymentAmount = Job.RepaymentAmt;
            output.WorkmanshipWarranty = productItem?.WorkmanshipWarranty;
            output.ProductWarranty = productItem?.ProductWarranty;
            output.PerformanceWarranty = productItem?.PerformanceWarranty;
            output.ExtendedWarranty = productItem?.ExtendedWarranty;
            output.RebateRefNo = !string.IsNullOrEmpty(Job.RebateRefNo) ? "Important Note : - " + Job.RebateRefNo + " is Battery Reference for QLD rebate application" : "";
            output.ManuWebsite = productItem?.ManuWebsite;
            output.ManuEmail = productItem?.ManuEmail;
            output.ManuMobile = productItem?.ManuMobile;
            output.ManuPhone = productItem?.ManuPhone;

            //var systemWith = new List<int> { 1, 5 };
            //var jobProduct = _jobProductItemRepository.GetAll().Include(e => e.ProductItemFk).Where(e => e.JobId == jobId && systemWith.Contains(e.ProductItemFk.ProductTypeId)).Select(e => new { e.ProductItemFk.ProductTypeId, e.Quantity, e.ProductItemFk.Name, e.ProductItemFk.Size });

            ////if (jobProduct.Any(e => systemWith.Contains(e.ProductTypeId)))
            ////{
            ////}
            ////else 
            //if (jobProduct.Count() == jobProduct.Count(e => e.ProductTypeId == 5))
            //{
            //    output.SystemWith = "Only Battery";
            //}
            //else if (jobProduct.Count(e => e.ProductTypeId == 5) == 0)
            //{
            //    output.SystemWith = "Without Battery";
            //}
            //else
            //{
            //    output.SystemWith = "With Battery";
            //}


            return output;
        }

        public async Task<List<GetDocumentTypeForView>> GetActiveDocumentType()
        {
            var documentType = _documentTypeRepository.GetAll().Where(e => e.IsActive == true);
            var filtereddocType = from o in documentType
                                  select new GetDocumentTypeForView()
                                  {
                                      Id = o.Id,
                                      DocumentTitle = o.Title
                                  };

            return filtereddocType.ToList();
        }
        public async Task<List<GetDocumentTypeForView>> GetEssentialActiveDocumentType()
        {
            var documentType = _documentTypeRepository.GetAll().Where(e => e.Id == 2 || e.Id == 3 || e.Id == 7 || e.Id == 19);
            var filtereddocType = from o in documentType
                                  select new GetDocumentTypeForView()
                                  {
                                      Id = o.Id,
                                      DocumentTitle = o.Title
                                  };

            return filtereddocType.ToList();
        }
    }
}