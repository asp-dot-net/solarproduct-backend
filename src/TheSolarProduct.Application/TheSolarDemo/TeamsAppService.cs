﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.TheSolarDemo.Exporting;
using TheSolarProduct.TheSolarDemo.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp;
using Microsoft.EntityFrameworkCore.Internal;
using Castle.Windsor.Installer;
using TheSolarProduct;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.TheSolarProduct.Dtos;
using Abp.Authorization.Users;
using Abp.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Authorization.Roles;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.LeadSources;
using PayPalCheckoutSdk.Orders;
using TheSolarProduct.ServiceCategorys;

namespace TheSolarProduct.TheSolarDemo
{
    [AbpAuthorize(AppPermissions.Pages_Teams)]
    public class TeamsAppService : TheSolarProductAppServiceBase, ITeamsAppService
    {
        private readonly ITeamsExcelExporter _teamsExcelExporter;

        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public TeamsAppService(
              IRepository<Team> teamRepository,
              ITeamsExcelExporter teamsExcelExporter,
              IRepository<UserTeam> userTeamRepository,
              IRepository<UserRole, long> userRoleRepository,
              IRepository<Role> roleRepository,
              IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
              )
        {
            _teamRepository = teamRepository;
            _teamsExcelExporter = teamsExcelExporter;
            _userTeamRepository = userTeamRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public List<NameValue<string>> GetAllTeamsAssignedTo(GetAllUserTeamsInput input)
        {
            var filteredUserTeams = from ut in _userTeamRepository.GetAll()
                                    join t in _teamRepository.GetAll() on ut.TeamId equals t.Id
                                    where ut.UserId == input.UserId
                                    select new { t.Id, t.Name };


            var teams = new List<NameValue<string>>();
            foreach (var item in filteredUserTeams)
            {
                teams.Add(new NameValue { Name = item.Name, Value = item.Id.ToString() });
            }


            return teams.ToList();

        }

        public List<NameValue<string>> GetAllTeams(string searchTerm)
        {
            var qryAllTeams = from t in _teamRepository.GetAll()
                                  //where t.TenantId == AbpSession.TenantId
                              select new { t.Id, t.Name };

            //Making NameValue Collection of Multi-Select DropDown

            var teams = new List<NameValue<string>>();
            foreach (var item in qryAllTeams)
                teams.Add(new NameValue { Name = item.Name, Value = item.Id.ToString() });


            return (
                searchTerm.IsNullOrEmpty() ?
                teams.ToList() :
                teams.Where(c => c.Name.ToLower().Contains(searchTerm.ToLower())).ToList());

        }
        public List<NameValue<string>> GetTeams(string searchTerm, string userId)
        {
            var teamlist = _teamRepository.GetAll();
            var teams = new List<NameValue<string>>();
            foreach (var item in teamlist)
            {
                teams.Add(new NameValue { Name = item.Name, Value = item.Id.ToString() });
            }

            //var qryAllTeams = from t in _teamRepository.GetAll()
            //          join ut in _userTeamRepository.GetAll() on t.Id equals ut.TeamId into x from rt in _userTeamRepository.GetAll().DefaultIfEmpty()
            //          join aur in _userRoleRepository.GetAll() on rt.UserId equals aur.UserId into y from rt1 in _userRoleRepository.GetAll().DefaultIfEmpty()
            //          join ar in _roleRepository.GetAll() on rt1.RoleId equals ar.Id into z from rt2 in _roleRepository.GetAll().DefaultIfEmpty()
            //          //where t.TenantId == AbpSession.TenantId
            //          select new { t.Id, t.Name, rt.UserId, RoleName = rt2.Name };


            ////List of Teams Having SalesManagers
            //var qryTeamsWithSalesManagers = from t in qryAllTeams where t.RoleName == "Sales Manager" select t;


            ////List of Teams Having No SalesManagers
            //var ListOfTeam_ManagerIsAssigned =
            //           (from t in qryAllTeams where !qryTeamsWithSalesManagers.Any(v => v.Id == t.Id) select new { t.Id, t.Name })
            //    .Distinct()
            //    .Concat(from t in qryAllTeams where t.UserId.ToString() == userId select new { t.Id, t.Name })
            //    .Distinct()
            //    .ToList();


            ////Making NameValue Collection of Multi-Select DropDown
            //var teams = new List<NameValue<string>>();
            //foreach (var item in ListOfTeam_ManagerIsAssigned)
            //    teams.Add(new NameValue { Name = item.Name, Value = item.Id.ToString() });


            return (
                searchTerm.IsNullOrEmpty() ?
                teams.ToList() :
                teams.Where(c => c.Name.ToLower().Contains(searchTerm.ToLower())).ToList());

        }

        public async Task<PagedResultDto<GetTeamForViewDto>> GetAll(GetAllTeamsInput input)
        {

            var filteredTeams = _teamRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

            var pagedAndFilteredTeams = filteredTeams
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var teams = from o in pagedAndFilteredTeams
                        select new GetTeamForViewDto()
                        {
                            Team = new TeamDto
                            {
                                Name = o.Name,
                                Id = o.Id,
                                IsActive = o.IsActive,
                            }
                        };

            var totalCount = await filteredTeams.CountAsync();

            return new PagedResultDto<GetTeamForViewDto>(
                totalCount,
                await teams.ToListAsync()
            );
        }

        public async Task<GetTeamForViewDto> GetTeamForView(int id)
        {
            var team = await _teamRepository.GetAsync(id);

            var output = new GetTeamForViewDto { Team = ObjectMapper.Map<TeamDto>(team) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Teams_Edit)]
        public async Task<GetTeamForEditOutput> GetTeamForEdit(EntityDto input)
        {
            var team = await _teamRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetTeamForEditOutput { Team = ObjectMapper.Map<CreateOrEditTeamDto>(team) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditTeamDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Teams_Create)]
        protected virtual async Task Create(CreateOrEditTeamDto input)
        {
            var team = ObjectMapper.Map<Team>(input);


            //if (AbpSession.TenantId != null)
            //{
            //	team.TenantId = (int?) AbpSession.TenantId;
            //}
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 3;
            dataVaultLog.ActionNote = "Team Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _teamRepository.InsertAsync(team);
        }

        [AbpAuthorize(AppPermissions.Pages_Teams_Edit)]
        protected virtual async Task Update(CreateOrEditTeamDto input)
        {
            var team = await _teamRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 3;
            dataVaultLog.ActionNote = "Team Updated : " + team.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != team.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = team.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != team.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = team.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, team);

            await _teamRepository.UpdateAsync(team);
        }

        [AbpAuthorize(AppPermissions.Pages_Teams_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _teamRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 3;
            dataVaultLog.ActionNote = "Team Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _teamRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetTeamsToExcel(GetAllTeamsForExcelInput input)
        {

            var filteredTeams = _teamRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

            var query = (from o in filteredTeams
                         select new GetTeamForViewDto()
                         {
                             Team = new TeamDto
                             {
                                 Name = o.Name,
                                 Id = o.Id,
                                 IsActive = o.IsActive
                             }
                         });


            var teamListDtos = await query.ToListAsync();

            return _teamsExcelExporter.ExportToFile(teamListDtos);
        }


    }
}