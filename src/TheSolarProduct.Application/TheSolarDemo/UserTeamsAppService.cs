﻿using TheSolarProduct.Authorization.Users;
using System.Collections.Generic;
using TheSolarProduct.TheSolarDemo;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.TheSolarDemo.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct;
using TheSolarProduct.TheSolarProduct.Dtos;
using Abp;

namespace TheSolarDemo.TheSolarDemo
{
    [AbpAuthorize(AppPermissions.Pages_UserTeams)]
    public class UserTeamsAppService : TheSolarProductAppServiceBase, IUserTeamsAppService
    {
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<User, long> _lookup_userRepository;
        private readonly IRepository<Team, int> _lookup_teamRepository;


        public UserTeamsAppService(IRepository<UserTeam> userTeamRepository, IRepository<User, long> lookup_userRepository, IRepository<Team, int> lookup_teamRepository)
        {
            _userTeamRepository = userTeamRepository;
            _lookup_userRepository = lookup_userRepository;
            _lookup_teamRepository = lookup_teamRepository;

        }
       
        public async Task<PagedResultDto<GetUserTeamForViewDto>> GetAll(GetAllUserTeamsInput input)
        {

            var filteredUserTeams = _userTeamRepository.GetAll()
                        .Include(e => e.UserFk)
                        .Include(e => e.TeamFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.UserId != null, e => e.UserId != null && e.UserId == input.UserId)
                        .WhereIf(input.TeamId != null, e => e.TeamId != null && e.TeamId == input.TeamId)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserFk != null && e.UserFk.Name == input.UserNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TeamNameFilter), e => e.TeamFk != null && e.TeamFk.Name == input.TeamNameFilter);

            var pagedAndFilteredUserTeams = filteredUserTeams
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var userTeams = from o in pagedAndFilteredUserTeams
                            join o1 in _lookup_userRepository.GetAll() on o.UserId equals o1.Id into j1
                            from s1 in j1.DefaultIfEmpty()
                            join o2 in _lookup_teamRepository.GetAll() on o.TeamId equals o2.Id into j2
                            from s2 in j2.DefaultIfEmpty()

                            select new GetUserTeamForViewDto()
                            {
                                UserTeam = new UserTeamDto
                                {
                                    Id = o.Id,
                                    TeamId = o.TeamId,
                                    UserId = o.UserId
                                },
                                UserName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                                TeamName = s2 == null || s2.Name == null ? "" : s2.Name.ToString()
                            };

            var totalCount = await filteredUserTeams.CountAsync();

            return new PagedResultDto<GetUserTeamForViewDto>(
                totalCount,
                await userTeams.ToListAsync()
            );
        }

        public async Task<GetUserTeamForViewDto> GetUserTeamForView(int id)
        {
            var userTeam = await _userTeamRepository.GetAsync(id);

            var output = new GetUserTeamForViewDto { UserTeam = ObjectMapper.Map<UserTeamDto>(userTeam) };

            if (output.UserTeam.UserId != null)
            {
                var _lookupUser = await _lookup_userRepository.FirstOrDefaultAsync((long)output.UserTeam.UserId);
                output.UserName = _lookupUser?.Name?.ToString();
            }

            if (output.UserTeam.TeamId != null)
            {
                var _lookupTeam = await _lookup_teamRepository.FirstOrDefaultAsync((int)output.UserTeam.TeamId);
                output.TeamName = _lookupTeam?.Name?.ToString();
            }

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_UserTeams_Edit)]
        public async Task<GetUserTeamForEditOutput> GetUserTeamForEdit(EntityDto input)
        {
            var userTeam = await _userTeamRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetUserTeamForEditOutput { UserTeam = ObjectMapper.Map<CreateOrEditUserTeamDto>(userTeam) };
            if (output.UserTeam != null)
            {
                if (output.UserTeam.UserId != null)
                {
                    var _lookupUser = await _lookup_userRepository.FirstOrDefaultAsync((long)output.UserTeam.UserId);
                    output.UserName = _lookupUser?.Name?.ToString();
                }

                if (output.UserTeam.TeamId != null)
                {
                    var _lookupTeam = await _lookup_teamRepository.FirstOrDefaultAsync((int)output.UserTeam.TeamId);
                    output.TeamName = _lookupTeam?.Name?.ToString();
                }
            }

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditUserTeamDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_UserTeams_Create)]
        protected virtual async Task Create(CreateOrEditUserTeamDto input)
        {
            var userTeam = ObjectMapper.Map<UserTeam>(input);


            if (AbpSession.TenantId != null)
            {
                userTeam.TenantId = (int?)AbpSession.TenantId;
            }


            await _userTeamRepository.InsertAsync(userTeam);
        }

        [AbpAuthorize(AppPermissions.Pages_UserTeams_Edit)]
        protected virtual async Task Update(CreateOrEditUserTeamDto input)
        {
            var userTeam = await _userTeamRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, userTeam);
        }

        [AbpAuthorize(AppPermissions.Pages_UserTeams_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _userTeamRepository.DeleteAsync(input.Id);
        }
        [AbpAuthorize(AppPermissions.Pages_UserTeams)]
        public async Task<List<UserTeamUserLookupTableDto>> GetAllUserForTableDropdown()
        {
            return await _lookup_userRepository.GetAll()
                .Select(user => new UserTeamUserLookupTableDto
                {
                    Id = user.Id,
                    DisplayName = user == null || user.Name == null ? "" : user.Name.ToString()
                }).ToListAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_UserTeams)]
        public async Task<List<UserTeamTeamLookupTableDto>> GetAllTeamForTableDropdown()
        {
            return await _lookup_teamRepository.GetAll()
                .Select(team => new UserTeamTeamLookupTableDto
                {
                    Id = team.Id,
                    DisplayName = team == null || team.Name == null ? "" : team.Name.ToString()
                }).ToListAsync();
        }

    }
}