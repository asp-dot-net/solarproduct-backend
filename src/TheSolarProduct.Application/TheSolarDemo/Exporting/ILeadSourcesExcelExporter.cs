﻿using System.Collections.Generic;
using TheSolarProduct.Dto;
using TheSolarProduct.LeadSources.Dtos;

namespace TheSolarProduct.TheSolarDemo.Exporting
{
    public interface ILeadSourcesExcelExporter
    {
        FileDto ExportToFile(List<GetLeadSourceForViewDto> leadSources);
    }
}