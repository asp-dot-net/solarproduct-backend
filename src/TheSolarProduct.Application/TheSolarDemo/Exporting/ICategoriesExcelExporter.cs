﻿using System.Collections.Generic;
using TheSolarProduct.TheSolarDemo.Dtos;
using TheSolarProduct.Dto;
namespace TheSolarProduct.TheSolarDemo.Exporting
{
    public interface ICategoriesExcelExporter
    {
        FileDto ExportToFile(List<GetCategoryForViewDto> categories);
    }
}