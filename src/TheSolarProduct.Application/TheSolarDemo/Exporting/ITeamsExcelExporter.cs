﻿using System.Collections.Generic;
using TheSolarProduct.TheSolarDemo.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.TheSolarDemo.Exporting
{
    public interface ITeamsExcelExporter
    {
        FileDto ExportToFile(List<GetTeamForViewDto> teams);
    }
}