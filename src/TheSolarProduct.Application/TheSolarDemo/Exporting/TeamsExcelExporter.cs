﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.TheSolarDemo.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.TheSolarDemo.Exporting;

namespace TheSolarDemo.TheSolarDemo.Exporting
{
    public class TeamsExcelExporter : NpoiExcelExporterBase, ITeamsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public TeamsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetTeamForViewDto> teams)
        {
            return CreateExcelPackage(
                "Teams.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("Teams"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("IsActive")
                        );

                    AddObjects(
                        sheet, 2, teams,
                        _ => _.Team.Name,
                        _ => _.Team.IsActive ? L("Yes") : L("No")
                        );

					
					
                });
        }
    }
}
