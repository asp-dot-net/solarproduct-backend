﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataVaults;
using TheSolarProduct.Dto;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.PaymentStatuses.Dtos;
using TheSolarProduct.TheSolarDemo;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.PaymentMethods;
using TheSolarProduct.PaymentMethodes.Exporting;
using TheSolarProduct.PaymentMethodes.Dtos;


namespace TheSolarProduct.PaymentMethodes
{
    
    [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentMethod)]
    public class PaymentMethodAppService : TheSolarProductAppServiceBase, IPaymentMethodAppService
    {
        private readonly IPaymentMethodExcelExporter _paymentMethodExcelExporter;

        private readonly IRepository<PaymentMethod> _paymentMethodRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public PaymentMethodAppService(
              IRepository<PaymentMethod> paymentMethodRepository,
              IPaymentMethodExcelExporter IPaymentMethodExcelExporter,
              IRepository<UserTeam> userTeamRepository,
              IRepository<UserRole, long> userRoleRepository,
              IRepository<Role> roleRepository,
              IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
              )
        {
            _paymentMethodRepository = paymentMethodRepository;
            _paymentMethodExcelExporter = IPaymentMethodExcelExporter;
            _userTeamRepository = userTeamRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }




        public async Task<PagedResultDto<GetPaymentMethodForViewDto>> GetAll(GetAllPaymentMethodInput input)
        {

            var PaymentMethod = _paymentMethodRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFilteredPaymentMethod = PaymentMethod
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var outputPaymentMethod = from o in pagedAndFilteredPaymentMethod
                                      select new GetPaymentMethodForViewDto()

                                      {
                                          PaymentMethod = new PaymentMethodDto
                                          {
                                              Id = o.Id,
                                              Name = o.Name,
                                              IsActive = o.IsActive
                                          }
                                      };

            var totalCount = await PaymentMethod.CountAsync();

            return new PagedResultDto<GetPaymentMethodForViewDto>(
                totalCount,
                await outputPaymentMethod.ToListAsync()
            );
        }



        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentMethod_Edit)]
        public async Task<GetPaymentMethodForEditOutput> GetPaymentMethodForEdit(EntityDto input)
        {
            var PaymentMethod = await _paymentMethodRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPaymentMethodForEditOutput { PaymentMethod = ObjectMapper.Map<CreateOrEditPaymentMethod>(PaymentMethod) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditPaymentMethod input)
        {


            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentMethod_Create)]

        protected virtual async Task Create(CreateOrEditPaymentMethod input)
        {
            var PaymentMethod = ObjectMapper.Map<PaymentMethod>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 64;
            dataVaultLog.ActionNote = "PaymentMethod Created : " + input.Name;
            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _paymentMethodRepository.InsertAsync(PaymentMethod);
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentMethod_Edit)]
        protected virtual async Task Update(CreateOrEditPaymentMethod input)
        {
            var PaymentMethod = await _paymentMethodRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 64;
            dataVaultLog.ActionNote = "PaymentMethod Updated : " + PaymentMethod.Name;

            dataVaultLog.IsInventory = true;
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != PaymentMethod.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = PaymentMethod.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != PaymentMethod.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = PaymentMethod.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, PaymentMethod);

            await _paymentMethodRepository.UpdateAsync(PaymentMethod);


        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentMethod_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _paymentMethodRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 64;
            dataVaultLog.ActionNote = "PaymentMethod Deleted : " + Name;

            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _paymentMethodRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentMethod_Export)]

        public async Task<FileDto> GetPaymentMethodToExcel(GetAllPaymentMethodForExcelInput input)
        {

            var filteredPaymentMethod = _paymentMethodRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredPaymentMethod
                         select new GetPaymentMethodForViewDto()
                         {
                             PaymentMethod = new PaymentMethodDto
                             {
                                 Name = o.Name,
                                 Id = o.Id,
                                 IsActive = o.IsActive
                             }
                         });


            var ListDtos = await query.ToListAsync();

            return _paymentMethodExcelExporter.ExportToFile(ListDtos);
        }
    }
}
