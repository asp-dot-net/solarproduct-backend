﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.PaymentMethodes.Dtos;

namespace TheSolarProduct.PaymentMethodes.Exporting
{
    public interface IPaymentMethodExcelExporter
    {
        FileDto ExportToFile(List<GetPaymentMethodForViewDto> PaymentMethod);
    }
}
