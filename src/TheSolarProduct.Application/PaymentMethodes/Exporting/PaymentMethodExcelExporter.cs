﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.PaymentMethodes.Dtos;
using TheSolarProduct.PaymentStatuses.Dtos;
using TheSolarProduct.PaymentStatuses.Exporting;
using TheSolarProduct.Storage;

namespace TheSolarProduct.PaymentMethodes.Exporting
{
    
    public class PaymentMethodExcelExporter : NpoiExcelExporterBase, IPaymentMethodExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PaymentMethodExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetPaymentMethodForViewDto> PaymentMethod)
        {
            return CreateExcelPackage(
                "PaymentMethod.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("PaymentMethod"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("IsActive")
                    );

                    AddObjects(
                        sheet,
                        2,
                         PaymentMethod,
                        _ => _.PaymentMethod.Name,
                        _ => _.PaymentMethod.IsActive.HasValue ? (_.PaymentMethod.IsActive.Value ? L("Yes") : L("No")) : L("No")
                    );
                }
            );
        }


    }
}
