﻿using System.Collections.Generic;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Jobs.Dtos;

namespace TheSolarProduct.Invoices.Exporting
{
    public interface IInvoicePaymentsExcelExporter
    {
        FileDto ExportToFile(List<GetInvoicePaymentForViewDto> invoicePayments);

        FileDto invoiceimportdataExportToFile(List<InstallerInvoiceImportDataViewDto> importdatalistList, string fileName);

        //FileDto invoiceimportdataCsvExport(List<InstallerInvoiceImportDataViewDto> importdatalistList);
        
        FileDto invoicePayWayimportdataExportToFile(List<PayWayViewDataDto> importdatalistList, string fileName);
        
        //FileDto invoicePayWayimportdataCsvExport(List<PayWayViewDataDto> importdatalistList);
    }
}