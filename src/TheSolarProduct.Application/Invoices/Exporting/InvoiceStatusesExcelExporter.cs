﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Invoices.Exporting
{
    public class InvoiceStatusesExcelExporter : NpoiExcelExporterBase, IInvoiceStatusesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public InvoiceStatusesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetInvoiceStatusForViewDto> invoiceStatuses)
        {
            return CreateExcelPackage(
                "InvoiceStatuses.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("InvoiceStatuses"));

                    AddHeader(
                        sheet,
                        L("Name"),
                         L("IsActive")
                        );

                    AddObjects(
                        sheet, 2, invoiceStatuses,
                        _ => _.InvoiceStatus.Name,
                         _ => _.InvoiceStatus.IsActive ? L("Yes") : L("No")
                        );

                });
        }
    }
}