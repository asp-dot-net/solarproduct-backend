﻿using System.Collections.Generic;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Invoices.Exporting
{
    public interface IInvoicePaymentMethodsExcelExporter
    {
        FileDto ExportToFile(List<GetInvoicePaymentMethodForViewDto> invoicePaymentMethods);
    }
}