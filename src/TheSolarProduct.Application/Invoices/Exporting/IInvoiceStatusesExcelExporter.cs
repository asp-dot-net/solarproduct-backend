﻿using System.Collections.Generic;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Invoices.Exporting
{
    public interface IInvoiceStatusesExcelExporter
    {
        FileDto ExportToFile(List<GetInvoiceStatusForViewDto> invoiceStatuses);
    }
}