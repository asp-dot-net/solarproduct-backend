﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Invoices.Exporting
{
    public class InvoicePaymentMethodsExcelExporter : NpoiExcelExporterBase, IInvoicePaymentMethodsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public InvoicePaymentMethodsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetInvoicePaymentMethodForViewDto> invoicePaymentMethods)
        {
            return CreateExcelPackage(
                "InvoicePaymentMethods.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("InvoicePaymentMethods"));

                    AddHeader(
                        sheet,
                        L("PaymentMethod"),
                        L("ShortCode"),
                        L("Active")
                        );

                    AddObjects(
                        sheet, 2, invoicePaymentMethods,
                        _ => _.InvoicePaymentMethod.PaymentMethod,
                        _ => _.InvoicePaymentMethod.ShortCode,
                        _ => _.InvoicePaymentMethod.Active
                        );

					
					
                });
        }
    }
}
