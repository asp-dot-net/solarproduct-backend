﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.Jobs.Dtos;
using System;
using NPOI.HPSF;

namespace TheSolarProduct.Invoices.Exporting
{
    public class InvoicePaymentsExcelExporter : NpoiExcelExporterBase, IInvoicePaymentsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public InvoicePaymentsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto invoiceimportdataExportToFile(List<InstallerInvoiceImportDataViewDto> importdatalistListDtos, string fileName)
        {
            string[] Header = new string[] { "Date", "Job Number", "Pay By", "Description", "Paid Ammount", "SSCharge", "Alloted By", "Receipt Number", "Purchase Number", "Invoice Notes Decription" };

            Func<InstallerInvoiceImportDataViewDto, object>[] Objects = new Func<InstallerInvoiceImportDataViewDto, object>[] {
                _ => _timeZoneConverter.Convert(_.Importdata.Date, _abpSession.TenantId, _abpSession.GetUserId()),
                _ => _.Importdata.JobNumber,
                _ => _.Importdata.PaymentMethodName,
                _ => _.Importdata.Description,
                _ => _.Importdata.PaidAmmount,
                _ => _.Importdata.SSCharge,
                _ => _.Importdata.AllotedBy,
                _ => _.Importdata.ReceiptNumber,
                _ => _.Importdata.PurchaseNumber,
                _ => _.Importdata.InvoiceNotDescription
            };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("InvoicePaid");

                    AddHeader(sheet, Header);

                    AddObjects(sheet, 1, importdatalistListDtos, Objects);

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                    for (var i = 1; i <= importdatalistListDtos.Count; i++) // You can define up to 64000 style in a .xlsx Workbook
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[0], "dd/mm/yyyy");

                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[4], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[5], "0.00");
                    }
                });
        }

        //public FileDto invoiceimportdataCsvExport(List<InstallerInvoiceImportDataViewDto> importdatalistListDtos)
        //{
        //    return CreateExcelPackage(
        //        "InvoicePaid.csv",
        //        excelPackage =>
        //        {

        //            var sheet = excelPackage.CreateSheet("InvoicePaid");

        //            AddHeader(
        //                sheet,
        //                "Date",
        //                "ProjectNo",
        //                "PayBy",
        //                "Description",
        //                "PaidAmmount",
        //                "SSCharge",
        //                "AllotedBy",
        //                "ReceiptNumber",
        //                "Purchase Number",
        //                "Invoice Notes Decription"
        //                );

        //            AddObjects(
        //                sheet, 2, importdatalistListDtos,
        //                  _ => _timeZoneConverter.Convert(_.Importdata.Date, _abpSession.TenantId, _abpSession.GetUserId()),
        //                 _ => _.Importdata.JobNumber,
        //                 _ => _.Importdata.PaymentMethodName,
        //                 _ => _.Importdata.Description,
        //                  _ => _.Importdata.PaidAmmount,
        //                 _ => _.Importdata.SSCharge,
        //                 _ => _.Importdata.AllotedBy,
        //                 _ => _.Importdata.ReceiptNumber,
        //                 _ => _.Importdata.PurchaseNumber,
        //                 _ => _.Importdata.InvoiceNotDescription
        //                );
        //            for (var i = 1; i <= importdatalistListDtos.Count; i++)
        //            {
        //                SetCellDataFormat(sheet.GetRow(i).Cells[0], "yyyy-mm-dd");
        //            }
        //            sheet.AutoSizeColumn(0);
        //            for (var i = 1; i <= importdatalistListDtos.Count; i++)
        //            {
        //                SetintCellDataFormat(sheet.GetRow(i).Cells[4], "00");
        //            }
        //            sheet.AutoSizeColumn(4);
        //            //for (var i = 1; i <= invoicePayments.Count; i++) ;
        //            //{
        //            //    SetCellDataFormat(sheet.GetRow(i).Cells[12], "yyyy-mm-dd");
        //            //}
        //            //sheet.AutoSizeColumn(12);
        //        });
        //}

        public FileDto invoicePayWayimportdataExportToFile(List<PayWayViewDataDto> importdatalistListDtos, string fileName)
        {
            string[] Header = new string[] { "Date", "Job Number", "Pay By", "Paid Ammount", "Status", "Response Code", "Response Text", "Transaction Type", "ReceiptNumber" };

            Func<PayWayViewDataDto, object>[] Objects = new Func<PayWayViewDataDto, object>[] {
                _ => _timeZoneConverter.Convert(_.payway.CreatedDate, _abpSession.TenantId, _abpSession.GetUserId()),
                _ => _.payway.JobNumber,
                _ => _.payway.PaymentMethodName,
                _ => _.payway.principalAmount,
                _ => _.payway.status,
                _ => _.payway.responseCode,
                _ => _.payway.responseText,
                _ => _.payway.transactionType,
                _ => _.payway.ReceiptNumber
            };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("InvoicePayWay");

                    AddHeader(sheet, Header);

                    AddObjects(sheet, 1, importdatalistListDtos, Objects);

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                    for (var i = 1; i <= importdatalistListDtos.Count; i++) // You can define up to 64000 style in a .xlsx Workbook
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[0], "dd MMM yyyy");

                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[3], "0.00");
                    }
                });
        }
        
        //public FileDto invoicePayWayimportdataCsvExport(List<PayWayViewDataDto> importdatalistListDtos)
        //{
        //    return CreateExcelPackage(
        //        "InvoicePayWay.csv",
        //        excelPackage =>
        //        {

        //            var sheet = excelPackage.CreateSheet("InvoicePayWay");

        //            AddHeader(
        //                sheet,
        //                "Date",
        //                "ProjectNo",
        //                "PayBy",
        //                "Description",
        //                "PaidAmmount",
        //                "SSCharge",
        //                "AllotedBy",
        //                "ReceiptNumber",
        //                "Purchase Number",
        //                "Status",
        //                "ResponseCode",
        //                "ResponseText",
        //                "TransactionType"
        //                );

        //            AddObjects(
        //                sheet, 2, importdatalistListDtos,
        //                  _ => _timeZoneConverter.Convert(_.payway.CreatedDate, _abpSession.TenantId, _abpSession.GetUserId()),
        //                 _ => _.payway.JobNumber,
        //                 _ => _.payway.PaymentMethodName,
        //                 _ => _.payway.Description,
        //                 _ => _.payway.principalAmount,
        //                 _ => _.payway.SSCharge,
        //                 _ => _.payway.AllotedBy,
        //                 _ => _.payway.ReceiptNumber,
        //                 _ => _.payway.PurchaseNumber,
        //                  _ => _.payway.status,
        //                 _ => _.payway.responseCode,
        //                 _ => _.payway.responseText,
        //                 _ => _.payway.transactionType
        //                );


        //            //for (var i = 1; i <= importdatalistListDtos.Count; i++)
        //            //{
        //            //    SetCellDataFormat(sheet.GetRow(i).Cells[0], "yyyy-mm-dd");
        //            //}
        //            //sheet.AutoSizeColumn(0);

        //            for (var i = 1; i <= importdatalistListDtos.Count; i++)
        //            {
        //                SetCellDataFormat(sheet.GetRow(i).Cells[0], "dd-MM-yyyy");
        //            }
        //            sheet.AutoSizeColumn(0);
        //            for (var i = 1; i <= importdatalistListDtos.Count; i++)
        //            {
        //                SetdecimalCellDataFormat(sheet.GetRow(i).Cells[4], "0.00");
        //            }
        //            sheet.AutoSizeColumn(4);
        //        });
        //}


        public FileDto ExportToFile(List<GetInvoicePaymentForViewDto> invoicePayments)
        {
            return CreateExcelPackage(
                "InvoicePayments.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("InvoicePayments"));

                    AddHeader(
                        sheet,
                        L("Customer"),
                        L("invoiceNo"),
                        L("IsVerified"),
                        L("InvoicePayDate"),
                        L("InvoicePayGST"),
                        L("InvoicePayTotal"),
                        L("ActualPayDate"),
                        L("CCSurcharge"),
                        L("ReceivedBy"),
                        L("PaymentNote"),
                        L("Leadowner")
                        );

                    AddObjects(
                        sheet, 2, invoicePayments,
                         _ => _.CompanyName + _.Mobile + _.Email + _.Address,
                         _ => _.JobNumber,
                         _ => _.InvoicePayment.IsVerified,
                         _ => _timeZoneConverter.Convert(_.InvoicePayment.InvoicePayDate, _abpSession.TenantId, _abpSession.GetUserId()),
                         _ => _.InvoicePayment.InvoicePayGST,
                         _ => _.InvoicePayment.InvoicePayTotal,
                         _ => _timeZoneConverter.Convert(_.InvoicePayment.ActualPayDate, _abpSession.TenantId, _abpSession.GetUserId()),
                         _ => _.InvoicePayment.CCSurcharge,
                         _ => _.UserName,
                         _ => _.InvoicePayment.PaymentNote,
                         _ => _.CurrentLeadOwaner

                        );


                    for (var i = 1; i <= invoicePayments.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[4], "yyyy-mm-dd");
                    }
                    sheet.AutoSizeColumn(4); for (var i = 1; i <= invoicePayments.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[7], "yyyy-mm-dd");
                    }
                    sheet.AutoSizeColumn(7);
                    //for (var i = 1; i <= invoicePayments.Count; i++) ;
                    //{
                    //    SetCellDataFormat(sheet.GetRow(i).Cells[12], "yyyy-mm-dd");
                    //}
                    //sheet.AutoSizeColumn(12);
                });
        }



        
    }
}
