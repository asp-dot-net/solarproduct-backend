﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Invoices.Exporting;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.Invoices
{
	[AbpAuthorize(AppPermissions.Pages_InvoicePaymentMethods)]
    public class InvoicePaymentMethodsAppService : TheSolarProductAppServiceBase, IInvoicePaymentMethodsAppService
    {
		 private readonly IRepository<InvoicePaymentMethod> _invoicePaymentMethodRepository;
		 private readonly IInvoicePaymentMethodsExcelExporter _invoicePaymentMethodsExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;


        public InvoicePaymentMethodsAppService(IRepository<InvoicePaymentMethod> invoicePaymentMethodRepository, IInvoicePaymentMethodsExcelExporter invoicePaymentMethodsExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider) 
		  {
			_invoicePaymentMethodRepository = invoicePaymentMethodRepository;
			_invoicePaymentMethodsExcelExporter = invoicePaymentMethodsExcelExporter;
			_dataVaultActivityLogRepository = dataVaultActivityLogRepository;
			_dbcontextprovider = dbcontextprovider;
			
		  }

		 public async Task<PagedResultDto<GetInvoicePaymentMethodForViewDto>> GetAll(GetAllInvoicePaymentMethodsInput input)
         {
			
			var filteredInvoicePaymentMethods = _invoicePaymentMethodRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.PaymentMethod.Contains(input.Filter) || e.ShortCode.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.PaymentMethodFilter),  e => e.PaymentMethod == input.PaymentMethodFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.ShortCodeFilter),  e => e.ShortCode == input.ShortCodeFilter)
						.WhereIf(input.ActiveFilter > -1,  e => (input.ActiveFilter == 1 && e.Active) || (input.ActiveFilter == 0 && !e.Active) );

			var pagedAndFilteredInvoicePaymentMethods = filteredInvoicePaymentMethods
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

			var invoicePaymentMethods = from o in pagedAndFilteredInvoicePaymentMethods
                         select new GetInvoicePaymentMethodForViewDto() {
							InvoicePaymentMethod = new InvoicePaymentMethodDto
							{
                                PaymentMethod = o.PaymentMethod,
                                ShortCode = o.ShortCode,
                                Active = o.Active,
                                Id = o.Id
							}
						};

            var totalCount = await filteredInvoicePaymentMethods.CountAsync();

            return new PagedResultDto<GetInvoicePaymentMethodForViewDto>(
                totalCount,
                await invoicePaymentMethods.ToListAsync()
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_InvoicePaymentMethods_Edit)]
		 public async Task<GetInvoicePaymentMethodForEditOutput> GetInvoicePaymentMethodForEdit(EntityDto input)
         {
            var invoicePaymentMethod = await _invoicePaymentMethodRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetInvoicePaymentMethodForEditOutput {InvoicePaymentMethod = ObjectMapper.Map<CreateOrEditInvoicePaymentMethodDto>(invoicePaymentMethod)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditInvoicePaymentMethodDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_InvoicePaymentMethods_Create)]
		 protected virtual async Task Create(CreateOrEditInvoicePaymentMethodDto input)
         {
            var invoicePaymentMethod = ObjectMapper.Map<InvoicePaymentMethod>(input);

			
			if (AbpSession.TenantId != null)
			{
				invoicePaymentMethod.TenantId = (int) AbpSession.TenantId;
			}

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 24;
            dataVaultLog.ActionNote = "Invoice Payment Methods Created : " + input.PaymentMethod;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _invoicePaymentMethodRepository.InsertAsync(invoicePaymentMethod);
         }

		 [AbpAuthorize(AppPermissions.Pages_InvoicePaymentMethods_Edit)]
		 protected virtual async Task Update(CreateOrEditInvoicePaymentMethodDto input)
         {
            var invoicePaymentMethod = await _invoicePaymentMethodRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 24;
            dataVaultLog.ActionNote = "Invoice Payment Methods Updated : " + invoicePaymentMethod.PaymentMethod;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.PaymentMethod != invoicePaymentMethod.PaymentMethod)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "PaymentMethod";
                history.PrevValue = invoicePaymentMethod.PaymentMethod;
                history.CurValue = input.PaymentMethod;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.ShortCode != invoicePaymentMethod.ShortCode)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "ShortCode";
                history.PrevValue = invoicePaymentMethod.ShortCode;
                history.CurValue = input.ShortCode;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Active != invoicePaymentMethod.Active)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Active";
                history.PrevValue = invoicePaymentMethod.Active.ToString();
                history.CurValue = input.Active.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }


            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, invoicePaymentMethod);
         }

		 [AbpAuthorize(AppPermissions.Pages_InvoicePaymentMethods_Delete)]
         public async Task Delete(EntityDto input)
         {
            var paymentMethod = _invoicePaymentMethodRepository.Get(input.Id).PaymentMethod;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 24;
            dataVaultLog.ActionNote = "Invoice Payment Methods Deleted : " + paymentMethod;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _invoicePaymentMethodRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetInvoicePaymentMethodsToExcel(GetAllInvoicePaymentMethodsForExcelInput input)
         {
			
			var filteredInvoicePaymentMethods = _invoicePaymentMethodRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.PaymentMethod.Contains(input.Filter) || e.ShortCode.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.PaymentMethodFilter),  e => e.PaymentMethod == input.PaymentMethodFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.ShortCodeFilter),  e => e.ShortCode == input.ShortCodeFilter)
						.WhereIf(input.ActiveFilter > -1,  e => (input.ActiveFilter == 1 && e.Active) || (input.ActiveFilter == 0 && !e.Active) );

			var query = (from o in filteredInvoicePaymentMethods
                         select new GetInvoicePaymentMethodForViewDto() { 
							InvoicePaymentMethod = new InvoicePaymentMethodDto
							{
                                PaymentMethod = o.PaymentMethod,
                                ShortCode = o.ShortCode,
                                Active = o.Active,
                                Id = o.Id
							}
						 });


            var invoicePaymentMethodListDtos = await query.ToListAsync();

            return _invoicePaymentMethodsExcelExporter.ExportToFile(invoicePaymentMethodListDtos);
         }


    }
}