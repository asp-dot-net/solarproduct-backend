﻿using TheSolarProduct.Jobs;
using System.Collections.Generic;
using TheSolarProduct.Authorization.Users;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Invoices.Exporting;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Leads;
using TheSolarProduct.LeadActivityLogs;
using System.Transactions;
using Abp.Domain.Uow;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.SmsTemplates;
using TheSolarProduct.EmailTemplates;
using TheSolarProduct.ApplicationSettings;
using Abp.Net.Mail;
using TheSolarProduct.ApplicationSettings.Dto;
using System.Net.Mail;
using Abp.Timing.Timezone;
using TheSolarProduct.Leads.Exporting;
using TheSolarProduct.JobHistory;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Common;
using NPOI.SS.Formula.Functions;
using TheSolarProduct.Timing;

namespace TheSolarProduct.Invoices
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class InvoicePaymentsAppService : TheSolarProductAppServiceBase, IInvoicePaymentsAppService
    {
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
        private readonly IInvoicePaymentsExcelExporter _invoicePaymentsExcelExporter;
        private readonly IRepository<Job, int> _lookup_jobRepository;
        private readonly IRepository<Lead, int> _lookup_leadRepository;
        private readonly IRepository<User, long> _lookup_userRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<InvoicePaymentMethod, int> _lookup_invoicePaymentMethodRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IJobsAppService _jobAppService;
        private readonly IRepository<User, long> _userRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<InvoiceStatus> _invoiceStatusRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<SmsTemplate> _smsTemplateRepository;
        private readonly IRepository<EmailTemplate> _emailTemplateRepository;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<JobStatus, int> _lookup_jobStatusRepository;
        private readonly IRepository<PaymentOption, int> _lookup_paymentOptionRepository;
        private readonly IRepository<JobRefund> _jobRefundRepository;
        private readonly IRepository<JobType, int> _lookup_jobTypeRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<InvoiceImportData> _InvoiceImportDataRepository;
        private readonly ILeadsExcelExporter _leadsExcelExporter;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly ICommonLookupAppService _commonLookupAppService;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<ProductItem> _productItemRepository;
        private readonly IRepository<Variation> _variationRepository;
        private readonly IRepository<JobVariation> _jobVariationRepository;
        private readonly ITimeZoneService _timeZoneService;

        public InvoicePaymentsAppService(IRepository<InvoicePayment> invoicePaymentRepository
            , IInvoicePaymentsExcelExporter invoicePaymentsExcelExporter
            , IRepository<Job, int> lookup_jobRepository
            , IRepository<User, long> lookup_userRepository
            , IRepository<InvoicePaymentMethod, int> lookup_invoicePaymentMethodRepository
            , IRepository<Lead, int> lookup_leadRepository
            , IRepository<LeadActivityLog> leadactivityRepository
            , IUnitOfWorkManager unitOfWorkManager
            , IJobsAppService jobAppService
            , IRepository<User, long> userRepository
            , IRepository<InvoiceStatus> invoiceStatusRepository
            , UserManager userManager
            , IRepository<UserTeam> userTeamRepository,
            IRepository<SmsTemplate> smsTemplateRepository,
            IRepository<EmailTemplate> emailTemplateRepository,
            IApplicationSettingsAppService applicationSettings,
            IEmailSender emailSender,
            IRepository<JobStatus, int> lookup_jobStatusRepository,
            IRepository<PaymentOption, int> lookup_paymentOptionRepository,
            IRepository<JobRefund> jobRefundRepository,
            IRepository<JobType, int> lookup_jobTypeRepository,
            ITimeZoneConverter timeZoneConverter,
            IRepository<InvoiceImportData> InvoiceImportDataRepository,
            ILeadsExcelExporter leadsExcelExporter,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
            ICommonLookupAppService commonLookupAppService,
            IRepository<JobProductItem> jobProductItemRepository,
            IRepository<ProductItem> productItemRepository,
            IRepository<Variation> variationRepository,
            IRepository<JobVariation> jobVariationRepository,
            ITimeZoneService timeZoneService
            )
        {
            _invoicePaymentRepository = invoicePaymentRepository;
            _invoicePaymentsExcelExporter = invoicePaymentsExcelExporter;
            _lookup_jobRepository = lookup_jobRepository;
            _lookup_leadRepository = lookup_leadRepository;
            _lookup_userRepository = lookup_userRepository;
            _lookup_invoicePaymentMethodRepository = lookup_invoicePaymentMethodRepository;
            _leadactivityRepository = leadactivityRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _jobAppService = jobAppService;
            _userRepository = userRepository;
            _invoiceStatusRepository = invoiceStatusRepository;
            _userManager = userManager;
            _userTeamRepository = userTeamRepository;
            _smsTemplateRepository = smsTemplateRepository;
            _emailTemplateRepository = emailTemplateRepository;
            _applicationSettings = applicationSettings;
            _emailSender = emailSender;
            _lookup_jobStatusRepository = lookup_jobStatusRepository;
            _lookup_paymentOptionRepository = lookup_paymentOptionRepository;
            _jobRefundRepository = jobRefundRepository;
            _lookup_jobTypeRepository = lookup_jobTypeRepository;
            _timeZoneConverter = timeZoneConverter;
            _InvoiceImportDataRepository = InvoiceImportDataRepository;
            _leadsExcelExporter = leadsExcelExporter;
            _dbcontextprovider = dbcontextprovider;
            _commonLookupAppService = commonLookupAppService;
            _jobProductItemRepository = jobProductItemRepository;
            _productItemRepository = productItemRepository;
            _variationRepository = variationRepository;
            _jobVariationRepository = jobVariationRepository;
            _timeZoneService = timeZoneService;
        }

        public async Task<List<GetInvoicePaymentForViewDto>> GetInvoicesByJobId(int jobid)
        {

            var filteredInvoicePayments = _invoicePaymentRepository.GetAll()
                        .Include(e => e.JobFk)
                        .Include(e => e.UserFk)
                        .Include(e => e.VerifiedByFk)
                        .Include(e => e.RefundByFk)
                        .Include(e => e.InvoicePaymentMethodFk)
                        .OrderByDescending(e => e.Id)
                        .WhereIf(jobid > 0, e => e.JobFk != null && e.JobId == jobid);

            var invoicePayments = from o in filteredInvoicePayments
                                  join o1 in _lookup_jobRepository.GetAll() on o.JobId equals o1.Id into j1
                                  from s1 in j1.DefaultIfEmpty()

                                  join o2 in _lookup_userRepository.GetAll() on o.UserId equals o2.Id into j2
                                  from s2 in j2.DefaultIfEmpty()

                                  join o3 in _lookup_userRepository.GetAll() on o.VerifiedBy equals o3.Id into j3
                                  from s3 in j3.DefaultIfEmpty()

                                  join o4 in _lookup_userRepository.GetAll() on o.RefundBy equals o4.Id into j4
                                  from s4 in j4.DefaultIfEmpty()

                                  join o5 in _lookup_invoicePaymentMethodRepository.GetAll() on o.InvoicePaymentMethodId equals o5.Id into j5
                                  from s5 in j5.DefaultIfEmpty()

                                  join o6 in _lookup_leadRepository.GetAll() on o.JobFk.LeadId equals o6.Id into j6
                                  from s6 in j6.DefaultIfEmpty()

                                  join o7 in _invoiceStatusRepository.GetAll() on o.InvoicePaymentStatusId equals o7.Id into j7
                                  from s7 in j7.DefaultIfEmpty()

                                  select new GetInvoicePaymentForViewDto()
                                  {
                                      InvoicePayment = new InvoicePaymentDto
                                      {
                                          InvoicePayExGST = o.InvoicePayExGST,
                                          InvoicePayGST = o.InvoicePayGST,
                                          InvoicePayTotal = o.InvoicePayTotal,
                                          InvoicePayDate = o.InvoicePayDate,
                                          CCSurcharge = o.CCSurcharge,
                                          VerifiedOn = o.VerifiedOn,
                                          PaymentNumber = o.PaymentNumber,
                                          IsVerified = o.IsVerified,
                                          ReceiptNumber = o.ReceiptNumber,
                                          ActualPayDate = o.ActualPayDate,
                                          PaidComment = o.PaidComment,
                                          InvoicePaymentMethodType = s5.PaymentMethod,
                                          Id = o.Id,
                                          PaymentNote = o.PaymentNote,
                                          InvoicePaymentStatusName = s7.Name
                                      },
                                      CompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                                      Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                                      Phone = s6 == null || s6.Phone == null ? "" : s6.Phone.ToString(),
                                      Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                                      Address = s6 == null || s6.Address == null ? "" : s6.Address.ToString(),
                                      UserName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                                      UserName2 = s3 == null || s3.Name == null ? "" : s3.Name.ToString(),
                                      UserName3 = s4 == null || s4.Name == null ? "" : s4.Name.ToString(),
                                      InvoicePaymentMethodPaymentMethod = s5 == null || s5.PaymentMethod == null ? "" : s5.PaymentMethod.ToString(),
                                      CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                                  };


            return invoicePayments.ToList();
        }

        public async Task<PagedResultDto<GetInvoicePaymentForViewDto>> GetAll(GetAllInvoicePaymentsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();


            var filteredInvoicePayments = _invoicePaymentRepository.GetAll()
                        .Include(e => e.JobFk)
                        .Include(e => e.UserFk)
                        .Include(e => e.VerifiedByFk)
                        .Include(e => e.RefundByFk)
                        .Include(e => e.InvoicePaymentMethodFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.PaymentNote.Contains(input.Filter) || e.ReceiptNumber.Contains(input.Filter) || e.PaidComment.Contains(input.Filter) || e.JobFk.JobNumber.Contains(input.Filter)
                        || e.JobFk.LeadFk.Mobile.Contains(input.Filter) || e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter))
                        .WhereIf(input.MinPaymentNumberFilter != null, e => e.PaymentNumber >= input.MinPaymentNumberFilter)
                        .WhereIf(input.MaxPaymentNumberFilter != null, e => e.PaymentNumber <= input.MaxPaymentNumberFilter)
                        .WhereIf(input.IsVerifiedFilter > -1, e => (input.IsVerifiedFilter == 1 && e.IsVerified) || (input.IsVerifiedFilter == 0 && !e.IsVerified))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ReceiptNumberFilter), e => e.ReceiptNumber == input.ReceiptNumberFilter)
                        .WhereIf(input.MinActualPayDateFilter != null, e => e.ActualPayDate >= input.MinActualPayDateFilter)
                        .WhereIf(input.MaxActualPayDateFilter != null, e => e.ActualPayDate <= input.MaxActualPayDateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PaidCommentFilter), e => e.PaidComment == input.PaidCommentFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobNoteFilter), e => e.JobFk != null && e.JobFk.Note == input.JobNoteFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserFk != null && e.UserFk.Name == input.UserNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.UserName2Filter), e => e.VerifiedByFk != null && e.VerifiedByFk.Name == input.UserName2Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.UserName3Filter), e => e.RefundByFk != null && e.RefundByFk.Name == input.UserName3Filter)
                        .WhereIf(input.JobPaymentOption > 0, e => e.JobFk != null && e.JobFk.PaymentOptionId == input.JobPaymentOption)
                        .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesManagerId)

                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.JobFk.LeadFk.PostCode == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.JobFk.LeadFk.State == input.StateNameFilter)
                        //.WhereIf(input.JobStatusID != null && input.JobStatusID != 0, e => e.JobFk.JobStatusId == input.JobStatusID)
                        .WhereIf(input.JobStatusID != null && input.JobStatusID.Count() > 0, e => input.JobStatusID.Contains((int)e.JobFk.JobStatusId))
                        .WhereIf(input.InvoicePaymentMethodId != null && input.InvoicePaymentMethodId != 0, e => e.InvoicePaymentMethodId == input.InvoicePaymentMethodId)
                        .WhereIf(input.InvoicePaymentStatusId != null && input.InvoicePaymentStatusId != 0, e => e.InvoicePaymentStatusId == input.InvoicePaymentStatusId)
                        .WhereIf(input.FinanceOptionId != null && input.FinanceOptionId != 0, e => e.JobFk.FinanceOptionId == input.FinanceOptionId)
                        .WhereIf(input.InvoiceDateNameFilter == "Creation" && SDate != null && EDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.InvoiceDateNameFilter == "ActualDate" && SDate != null && EDate != null, e => e.ActualPayDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActualPayDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit);

            var pagedAndFilteredInvoicePayments = filteredInvoicePayments
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);
            var Jobids = filteredInvoicePayments.Select(e => e.JobId).ToList();
            var invoicePayments = from o in pagedAndFilteredInvoicePayments
                                  join o1 in _lookup_jobRepository.GetAll() on o.JobId equals o1.Id into j1
                                  from s1 in j1.DefaultIfEmpty()

                                  join o2 in _lookup_userRepository.GetAll() on o.UserId equals o2.Id into j2
                                  from s2 in j2.DefaultIfEmpty()

                                  join o3 in _lookup_userRepository.GetAll() on o.VerifiedBy equals o3.Id into j3
                                  from s3 in j3.DefaultIfEmpty()

                                  join o4 in _lookup_userRepository.GetAll() on o.RefundBy equals o4.Id into j4
                                  from s4 in j4.DefaultIfEmpty()

                                  join o5 in _lookup_invoicePaymentMethodRepository.GetAll() on o.InvoicePaymentMethodId equals o5.Id into j5
                                  from s5 in j5.DefaultIfEmpty()

                                  join o6 in _lookup_leadRepository.GetAll() on o.JobFk.LeadId equals o6.Id into j6
                                  from s6 in j6.DefaultIfEmpty()

                                  join o7 in _invoiceStatusRepository.GetAll() on o.InvoicePaymentStatusId equals o7.Id into j7
                                  from s7 in j7.DefaultIfEmpty()

                                  select new GetInvoicePaymentForViewDto()
                                  {
                                      InvoicePayment = new InvoicePaymentDto
                                      {
                                          InvoicePayExGST = o.InvoicePayExGST,
                                          InvoicePayGST = o.InvoicePayGST,
                                          InvoicePayTotal = o.InvoicePayTotal,
                                          InvoicePayDate = o.InvoicePayDate,
                                          CCSurcharge = o.CCSurcharge,
                                          VerifiedOn = o.VerifiedOn,
                                          PaymentNumber = o.PaymentNumber,
                                          IsVerified = o.IsVerified,
                                          ReceiptNumber = o.ReceiptNumber,
                                          ActualPayDate = o.ActualPayDate,
                                          PaidComment = o.PaidComment,
                                          PaymentNote = o.PaymentNote,
                                          Id = o.Id,
                                          InvoiceNo = o.InvoiceNo,
                                          JobId = s1.Id,
                                          InvoicePaymentStatusName = s7.Name,
                                          InvoiceSmsSend = o.InvoiceSmsSend,
                                          InvoiceSmsSendDate = o.InvoiceSmsSendDate,
                                          InvoiceEmailSend = o.InvoiceEmailSend,
                                          InvoiceEmailSendDate = o.InvoiceEmailSendDate,
                                      },
                                      LeadId = s6.Id,
                                      CompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                                      Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                                      Phone = s6 == null || s6.Phone == null ? "" : s6.Phone.ToString(),
                                      Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                                      Address = s6 == null || s6.Address == null ? "" : s6.Address.ToString(),
                                      UserName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                                      UserName2 = s3 == null || s3.Name == null ? "" : s3.Name.ToString(),
                                      UserName3 = s4 == null || s4.Name == null ? "" : s4.Name.ToString(),
                                      InvoicePaymentMethodPaymentMethod = s5 == null || s5.PaymentMethod == null ? "" : s5.PaymentMethod.ToString(),
                                      JobNumber = s1 == null || s1.JobNumber == null ? "" : s1.JobNumber.ToString(),
                                      CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == o.JobFk.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),

                                      TotalpaymentData = filteredInvoicePayments.Count(),
                                      TotalpaymentVerifyData = filteredInvoicePayments.Where(e => e.IsVerified == true).Count(),
                                      TotalpaymentnotVerifyData = filteredInvoicePayments.Where(e => e.IsVerified == false).Count(),
                                      TotalAmountOfInvoice = filteredInvoicePayments.Select(e => e.JobFk.TotalCost).Sum(),
                                      TotalRefundamount = _jobRefundRepository.GetAll().Where(e => Jobids.Contains(e.JobId)).Select(e => e.Amount).Sum(),
                                      TotalAmountReceived = filteredInvoicePayments.Select(e => e.InvoicePayTotal).Sum(),
                                      TotalCancelAmount = filteredInvoicePayments.Where(e => e.JobFk.JobStatusId == 3).Select(e => e.JobFk.TotalCost).Sum(),
                                      TotalVICAmountOfInvoice = filteredInvoicePayments.Where(e => e.JobFk.LeadFk.State == "VIC").Select(e => e.JobFk.SolarVICRebate).Sum(),
                                      TotalCustAmountOfInvoice = filteredInvoicePayments.Where(e => e.JobFk.LeadFk.State == "VIC").Select(e => e.JobFk.TotalCost).Sum(),
                                      TotalCustAmountReceived = filteredInvoicePayments.Where(e => e.JobFk.LeadFk.State == "VIC").Select(e => e.InvoicePayTotal).Sum(),
                                  };

            var totalCount = await filteredInvoicePayments.CountAsync();

            return new PagedResultDto<GetInvoicePaymentForViewDto>(
                totalCount,
                await invoicePayments.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_InvoicePayments_Edit, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker, AppPermissions.Pages_InvoiceTracker_Verify)]
        public async Task<GetInvoicePaymentForEditOutput> GetInvoicePaymentForEdit(EntityDto input)
        {
            var invoicePayment = await _invoicePaymentRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetInvoicePaymentForEditOutput { InvoicePayment = ObjectMapper.Map<CreateOrEditInvoicePaymentDto>(invoicePayment) };

            var leadid = _lookup_jobRepository.GetAll().Where(e => e.Id == invoicePayment.JobId).Select(e => e.LeadId).FirstOrDefault();
            output.JobNumber = _lookup_jobRepository.GetAll().Where(e => e.Id == invoicePayment.JobId).Select(e => e.JobNumber).FirstOrDefault();
            output.LeadCompanyName = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.CompanyName).FirstOrDefault();

            if (output.InvoicePayment.JobId != null)
            {
                var _lookupJob = await _lookup_jobRepository.FirstOrDefaultAsync((int)output.InvoicePayment.JobId);
                output.JobNote = _lookupJob?.Note?.ToString();
            }

            if (output.InvoicePayment.UserId != null)
            {
                var _lookupUser = await _lookup_userRepository.FirstOrDefaultAsync((long)output.InvoicePayment.UserId);
                output.UserName = _lookupUser?.Name?.ToString();
            }

            if (output.InvoicePayment.VerifiedBy != null)
            {
                var _lookupUser = await _lookup_userRepository.FirstOrDefaultAsync((long)output.InvoicePayment.VerifiedBy);
                output.UserName2 = _lookupUser?.Name?.ToString();
            }

            if (output.InvoicePayment.RefundBy != null)
            {
                var _lookupUser = await _lookup_userRepository.FirstOrDefaultAsync((long)output.InvoicePayment.RefundBy);
                output.UserName3 = _lookupUser?.Name?.ToString();
            }

            if (output.InvoicePayment.InvoicePaymentMethodId != null)
            {
                var _lookupInvoicePaymentMethod = await _lookup_invoicePaymentMethodRepository.FirstOrDefaultAsync((int)output.InvoicePayment.InvoicePaymentMethodId);
                output.InvoicePaymentMethodPaymentMethod = _lookupInvoicePaymentMethod?.PaymentMethod?.ToString();
            }

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditInvoicePaymentDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_InvoicePayments_Create, AppPermissions.Pages_Jobs)]
        protected virtual async Task Create(CreateOrEditInvoicePaymentDto input)
        {
            input.InvoicePayDate = _timeZoneConverter.Convert(input.InvoicePayDate, (int)AbpSession.TenantId); 

            var invoicePayment = ObjectMapper.Map<InvoicePayment>(input);

            invoicePayment.InvoicePayDate = invoicePayment.InvoicePayDate.Value.Date;

            using (var uow = _unitOfWorkManager.Begin(TransactionScopeOption.RequiresNew))
            {
                invoicePayment.UserId = (int)AbpSession.UserId;
                if (AbpSession.TenantId != null)
                {
                    invoicePayment.TenantId = (int)AbpSession.TenantId;
                }

                await _invoicePaymentRepository.InsertAsync(invoicePayment);
                await uow.CompleteAsync();
            }
            //await _jobAppService.JobDeposite((int)input.JobId);
            //await _jobAppService.JobActive((int)input.JobId);
            var Jobs = _lookup_jobRepository.GetAll().Where(e => e.Id == input.JobId).FirstOrDefault();

            var InvoiceCount = _invoicePaymentRepository.GetAll().Where(e => e.JobId == input.JobId).Count();
            if (InvoiceCount == 1)
            {
                Jobs.FirstDepositDate = input.InvoicePayDate.Value.Date;
                await _lookup_jobRepository.UpdateAsync(Jobs);
            }

            var UserDetail = _lookup_userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 16;
            leadactivity.SectionId = input.SectionId;
            leadactivity.ActionNote = "Invoice Created With Amount $" + Convert.ToString(invoicePayment.InvoicePayTotal);
            leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        [AbpAuthorize(AppPermissions.Pages_InvoicePayments_Edit, AppPermissions.Pages_Jobs, AppPermissions.Pages_InvoiceTracker_Verify)]
        protected virtual async Task Update(CreateOrEditInvoicePaymentDto input)
        {
            var invoicePayment = await _invoicePaymentRepository.FirstOrDefaultAsync((int)input.Id);

            var Jobs = _lookup_jobRepository.GetAll().Where(e => e.Id == input.JobId).FirstOrDefault();
            var UserDetail = _lookup_userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            //await _jobAppService.JobDeposite((int)input.JobId);
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 16;
            leadactivity.SectionId = input.SectionId;
            //leadactivity.ActionNote = "Invoice Verified With Amount $" + Convert.ToString(invoicePayment.InvoicePayTotal);
            leadactivity.ActionNote = "Invoice Modified";
            leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var jobactionid = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

            var list = new List<JobTrackerHistory>();
            if (invoicePayment.InvoicePayDate.Value.Date != input.InvoicePayDate.Value.Date)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Invoice Pay Date";
                jobhistory.PrevValue = invoicePayment.InvoicePayDate.ToString();
                jobhistory.CurValue = input.InvoicePayDate.ToString();
                jobhistory.Action = "Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = (int)invoicePayment.JobId;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (invoicePayment.InvoicePayTotal != input.InvoicePayTotal)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Invoice Pay Total";
                jobhistory.PrevValue = invoicePayment.InvoicePayTotal.ToString();
                jobhistory.CurValue = input.InvoicePayTotal.ToString();
                jobhistory.Action = "Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = (int)invoicePayment.JobId;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (invoicePayment.InvoicePayGST != input.InvoicePayGST)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Invoice Pay GST";
                jobhistory.PrevValue = invoicePayment.InvoicePayGST.ToString();
                jobhistory.CurValue = input.InvoicePayGST.ToString();
                jobhistory.Action = "Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = (int)invoicePayment.JobId;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (invoicePayment.InvoicePaymentMethodId != null && invoicePayment.InvoicePaymentMethodId != 0 && invoicePayment.InvoicePaymentMethodId != input.InvoicePaymentMethodId)
            {
                var InvoicePaymentList = _lookup_invoicePaymentMethodRepository.GetAll();
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Payment Method";
                jobhistory.PrevValue = InvoicePaymentList.Where(x => x.Id == invoicePayment.InvoicePaymentMethodId).Select(x => x.PaymentMethod).FirstOrDefault();
                jobhistory.CurValue = InvoicePaymentList.Where(x => x.Id == input.InvoicePaymentMethodId).Select(x => x.PaymentMethod).FirstOrDefault();
                jobhistory.Action = "Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = (int)invoicePayment.JobId;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (invoicePayment.ReceiptNumber != input.ReceiptNumber)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Receipt Number";
                jobhistory.PrevValue = invoicePayment.ReceiptNumber;
                jobhistory.CurValue = input.ReceiptNumber;
                jobhistory.Action = "Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = (int)invoicePayment.JobId;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (invoicePayment.TransactionCode != input.TransactionCode)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Transaction Code";
                jobhistory.PrevValue = invoicePayment.TransactionCode;
                jobhistory.CurValue = input.TransactionCode;
                jobhistory.Action = "Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = (int)invoicePayment.JobId;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (invoicePayment.CCSurcharge != input.CCSurcharge)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Surcharge";
                jobhistory.PrevValue = invoicePayment.CCSurcharge.ToString();
                jobhistory.CurValue = input.CCSurcharge.ToString();
                jobhistory.Action = "Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = (int)invoicePayment.JobId;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (invoicePayment.InvoicePaymentStatusId != null && invoicePayment.InvoicePaymentStatusId != 0 && invoicePayment.InvoicePaymentStatusId != input.InvoicePaymentStatusId)
            {
                var InvoiceStatusList = _invoiceStatusRepository.GetAll();
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Payment Type";
                jobhistory.PrevValue = InvoiceStatusList.Where(x => x.Id == invoicePayment.InvoicePaymentMethodId).Select(x => x.Name).FirstOrDefault();
                jobhistory.CurValue = InvoiceStatusList.Where(x => x.Id == input.InvoicePaymentMethodId).Select(x => x.Name).FirstOrDefault();
                jobhistory.Action = "Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = (int)invoicePayment.JobId;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (invoicePayment.PaymentNote != input.PaymentNote)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Payment Note";
                jobhistory.PrevValue = invoicePayment.PaymentNote;
                jobhistory.CurValue = input.PaymentNote;
                jobhistory.Action = "Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = (int)invoicePayment.JobId;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (invoicePayment.CCSurcharge != input.CCSurcharge)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Payment CCSurcharge";
                jobhistory.PrevValue = invoicePayment.CCSurcharge.ToString();
                jobhistory.CurValue = input.CCSurcharge.ToString();
                jobhistory.Action = "Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = (int)invoicePayment.JobId;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            input.InvoicePayDate = input.InvoicePayDate.Value.Date;
            ObjectMapper.Map(input, invoicePayment);
        }

        [AbpAuthorize(AppPermissions.Pages_InvoicePayments_Delete, AppPermissions.Pages_LeadDetails_InvoicePaymentDelete)]
        public async Task Delete(EntityDto input, int? sectionId)
        {
            var Invoice = await _invoicePaymentRepository.GetAsync(input.Id);
            var Jobs = _lookup_jobRepository.GetAll().Where(e => e.Id == Invoice.JobId).FirstOrDefault();

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 16;
            leadactivity.SectionId = sectionId;
            leadactivity.ActionNote = "Invoice Deleted With Amount $" + Convert.ToString(Invoice.InvoicePayTotal);
            leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);

            using (var uow = _unitOfWorkManager.Begin(TransactionScopeOption.RequiresNew))
            {
                await _invoicePaymentRepository.DeleteAsync(input.Id);
                await uow.CompleteAsync();
            }

            var InvoiceCount = _invoicePaymentRepository.GetAll().Where(e => e.JobId == Invoice.JobId).Count();
            if (InvoiceCount == 0)
            {
                Jobs.FirstDepositDate = null;
                await _lookup_jobRepository.UpdateAsync(Jobs);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_InvoicePayments_Verify)]
        public async Task VerifyInvoicePayment(CreateOrEditInvoicePaymentDto input)
        {
            int UserId = (int)AbpSession.UserId;
            var invoicePayment = await _invoicePaymentRepository.FirstOrDefaultAsync((int)input.Id);
            invoicePayment.VerifiedBy = UserId;
            invoicePayment.VerifiedOn = DateTime.UtcNow;
            invoicePayment.IsVerified = true;
            invoicePayment.ActualPayDate = input.ActualPayDate;
            invoicePayment.PaidComment = input.PaidComment;
            await _invoicePaymentRepository.UpdateAsync(invoicePayment);

            var Jobs = _lookup_jobRepository.GetAll().Where(e => e.Id == input.JobId).FirstOrDefault();

            var UserDetail = _lookup_userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 16;
            leadactivity.SectionId = 0;
            leadactivity.ActionNote = "Invoice Amount " + Convert.ToString(invoicePayment.InvoicePayTotal) + " Verified";
            leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        public async Task<FileDto> GetInvoicePaymentsToExcel(GetAllInvoicePaymentsForExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.MinRefundDateFilter, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.MaxRefundDateFilter, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredInvoicePayments = _invoicePaymentRepository.GetAll()
                        .Include(e => e.JobFk)
                        .Include(e => e.UserFk)
                        .Include(e => e.VerifiedByFk)
                        .Include(e => e.RefundByFk)
                        .Include(e => e.InvoicePaymentMethodFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.PaymentNote.Contains(input.Filter) || e.ReceiptNumber.Contains(input.Filter) || e.PaidComment.Contains(input.Filter))
                        .WhereIf(input.MinRefundDateFilter != null && SDate != null, e => e.RefundDate >= input.MinRefundDateFilter)
                        .WhereIf(input.MaxRefundDateFilter != null && EDate != null, e => e.RefundDate <= input.MaxRefundDateFilter)
                        .WhereIf(input.MinPaymentNumberFilter != null, e => e.PaymentNumber >= input.MinPaymentNumberFilter)
                        .WhereIf(input.MaxPaymentNumberFilter != null, e => e.PaymentNumber <= input.MaxPaymentNumberFilter)
                        .WhereIf(input.IsVerifiedFilter > -1, e => (input.IsVerifiedFilter == 1 && e.IsVerified) || (input.IsVerifiedFilter == 0 && !e.IsVerified))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ReceiptNumberFilter), e => e.ReceiptNumber == input.ReceiptNumberFilter)
                        .WhereIf(input.MinActualPayDateFilter != null, e => e.ActualPayDate >= input.MinActualPayDateFilter)
                        .WhereIf(input.MaxActualPayDateFilter != null, e => e.ActualPayDate <= input.MaxActualPayDateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PaidCommentFilter), e => e.PaidComment == input.PaidCommentFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobNoteFilter), e => e.JobFk != null && e.JobFk.Note == input.JobNoteFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserFk != null && e.UserFk.Name == input.UserNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.UserName2Filter), e => e.VerifiedByFk != null && e.VerifiedByFk.Name == input.UserName2Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.UserName3Filter), e => e.RefundByFk != null && e.RefundByFk.Name == input.UserName3Filter)
                        .WhereIf(input.JobPaymentOption > 0, e => e.JobFk != null && e.JobFk.PaymentOptionId == input.JobPaymentOption)
                        .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesManagerId)
                        .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit);


            var query = (from o in filteredInvoicePayments
                         join o1 in _lookup_jobRepository.GetAll() on o.JobId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _lookup_userRepository.GetAll() on o.UserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o3 in _lookup_userRepository.GetAll() on o.VerifiedBy equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         join o4 in _lookup_userRepository.GetAll() on o.RefundBy equals o4.Id into j4
                         from s4 in j4.DefaultIfEmpty()

                         join o5 in _lookup_invoicePaymentMethodRepository.GetAll() on o.InvoicePaymentMethodId equals o5.Id into j5
                         from s5 in j5.DefaultIfEmpty()

                         join o6 in _lookup_leadRepository.GetAll() on o.JobFk.LeadId equals o6.Id into j6
                         from s6 in j6.DefaultIfEmpty()

                         join o7 in _invoiceStatusRepository.GetAll() on o.InvoicePaymentStatusId equals o7.Id into j7
                         from s7 in j7.DefaultIfEmpty()

                         select new GetInvoicePaymentForViewDto()
                         {
                             InvoicePayment = new InvoicePaymentDto
                             {
                                 InvoicePayExGST = o.InvoicePayExGST,
                                 InvoicePayGST = o.InvoicePayGST,
                                 InvoicePayTotal = o.InvoicePayTotal,
                                 InvoicePayDate = o.InvoicePayDate,
                                 CCSurcharge = o.CCSurcharge,
                                 VerifiedOn = o.VerifiedOn,
                                 PaymentNumber = o.PaymentNumber,
                                 IsVerified = o.IsVerified,
                                 ReceiptNumber = o.ReceiptNumber,
                                 ActualPayDate = o.ActualPayDate,
                                 PaidComment = o.PaidComment,
                                 PaymentNote = o.PaymentNote,
                                 Id = o.Id,
                                 InvoiceNo = o.InvoiceNo,
                                 JobId = s1.Id,
                                 InvoicePaymentStatusName = s7.Name,
                             },
                             CompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                             Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                             Phone = s6 == null || s6.Phone == null ? "" : s6.Phone.ToString(),
                             Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                             Address = s6 == null || s6.Address == null ? "" : s6.Address.ToString(),
                             UserName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                             UserName2 = s3 == null || s3.Name == null ? "" : s3.Name.ToString(),
                             UserName3 = s4 == null || s4.Name == null ? "" : s4.Name.ToString(),
                             InvoicePaymentMethodPaymentMethod = s5 == null || s5.PaymentMethod == null ? "" : s5.PaymentMethod.ToString(),
                             JobNumber = s1 == null || s1.JobNumber == null ? "" : s1.JobNumber.ToString(),
                             CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == o.JobFk.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                         });


            var invoicePaymentListDtos = await query.ToListAsync();

            return _invoicePaymentsExcelExporter.ExportToFile(invoicePaymentListDtos);
        }

        //public async Task<List<InvoicePaymentJobLookupTableDto>> GetAllJobForTableDropdown()
        //{
        //    return await _lookup_jobRepository.GetAll()
        //        .Select(job => new InvoicePaymentJobLookupTableDto
        //        {
        //            Id = job.Id,
        //            DisplayName = job == null || job.Note == null ? "" : job.Note.ToString()
        //        }).ToListAsync();
        //}

        public async Task<List<InvoicePaymentUserLookupTableDto>> GetAllUserForTableDropdown()
        {
            return await _lookup_userRepository.GetAll()
                .Select(user => new InvoicePaymentUserLookupTableDto
                {
                    Id = user.Id,
                    DisplayName = user == null || user.Name == null ? "" : user.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<InvoicePaymentInvoicePaymentMethodLookupTableDto>> GetAllInvoicePaymentMethodForTableDropdown()
        {
            return await _lookup_invoicePaymentMethodRepository.GetAll()
                .Select(invoicePaymentMethod => new InvoicePaymentInvoicePaymentMethodLookupTableDto
                {
                    Id = invoicePaymentMethod.Id,
                    DisplayName = invoicePaymentMethod == null || invoicePaymentMethod.PaymentMethod == null ? "" : invoicePaymentMethod.PaymentMethod.ToString()
                }).ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetAllInvoiceStatusForTableDropdown()
        {
            return await _invoiceStatusRepository.GetAll()
                .Select(user => new CommonLookupDto
                {
                    Id = user.Id,
                    DisplayName = user == null || user.Name == null ? "" : user.Name.ToString()
                }).ToListAsync();
        }

        public async Task JobInvoiceSendSms(int id, int smstempid)
        {
            var jobinvoice = await _invoicePaymentRepository.FirstOrDefaultAsync(id);
            var smsText = _smsTemplateRepository.GetAll().Where(e => e.Id == smstempid).Select(e => e.Text).FirstOrDefault();
            var output = ObjectMapper.Map<CreateOrEditInvoicePaymentDto>(jobinvoice);
            var leadid = _lookup_jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
            var mobilenumber = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Mobile).FirstOrDefault();
            var Body = smsText;
            if (!string.IsNullOrEmpty(mobilenumber))
            {
                SendSMSInput sendSMSInput = new SendSMSInput();
                sendSMSInput.PhoneNumber = mobilenumber;
                sendSMSInput.Text = Body;
                await _applicationSettings.SendSMS(sendSMSInput);
                output.InvoiceSmsSend = true;
                output.InvoiceSmsSendDate = DateTime.UtcNow;
                ObjectMapper.Map(output, jobinvoice);
            }
        }

        public async Task JobInvoiceSendEmail(int id, int emailtempid)
        {
            var jobinvoice = await _invoicePaymentRepository.FirstOrDefaultAsync(id);
            var emailText = _emailTemplateRepository.GetAll().Where(e => e.Id == emailtempid).Select(e => e.TemplateName).FirstOrDefault();
            var output = ObjectMapper.Map<CreateOrEditInvoicePaymentDto>(jobinvoice);
            var leadid = _lookup_jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
            var Email = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Email).FirstOrDefault();
            var Body = emailText;
            if (!string.IsNullOrEmpty(Email))
            {
                MailMessage mail = new MailMessage
                {
                    To = { Email },
                    Subject = "Status Update",
                    Body = Body,
                    IsBodyHtml = true
                };
                await this._emailSender.SendAsync(mail);
                output.InvoiceEmailSend = true;
                output.InvoiceEmailSendDate = DateTime.UtcNow;
                ObjectMapper.Map(output, jobinvoice);
            }
        }

        ///invoice Issued Tracker Grid
        public async Task<PagedResultDto<GetInvoiceIssuedForViewDto>> GetAllInvoiceIssuedData(GetAllInvoiceIssuedInput input)
        {
            #region Old Code
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            //var User = await _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = await _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            //var UserList = await _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            //var filteredInvoiceIssued = _lookup_jobRepository.GetAll().Include(e => e.LeadFk).Include(e => e.JobStatusFk).Include(e => e.PaymentOptionFk)
            //            .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && (e.FirstDepositDate != null || e.InstallationDate != null))
            //            .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName.Contains(input.Filter))
            //            .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email.Contains(input.Filter))
            //            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            //            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            //            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            //            .WhereIf(input.JobPaymentOption > 0, e => e.PaymentOptionId == input.JobPaymentOption)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.LeadFk.Suburb == input.PostCodeSuburbFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.LeadFk.State == input.StateNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.PostalCode, input.PostalCodeFrom) >= 0)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.PostalCode, input.PostalCodeTo) <= 0)
            //            .WhereIf(input.JobStatusID != null && input.JobStatusID.Count() > 0, e => input.JobStatusID.Contains((int)e.JobStatusId))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
            //            .WhereIf(input.vicrebate != "All", e => e.VicRebate == input.vicrebate)
            //            .WhereIf(input.solarRebateStatus != 0, e => e.SolarRebateStatus == input.solarRebateStatus)
            //            .WhereIf(input.PaymentMethodId != null && input.PaymentMethodId != 0, e => _invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id && x.InvoicePaymentMethodId == input.PaymentMethodId).Any())

            //            .WhereIf(input.InvoiceDateNameFilter == "Creation" && input.StartDate != null, e => (_invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.CreationTime).FirstOrDefault().AddHours(diffHour).Date) >= SDate.Value.Date)
            //            .WhereIf(input.InvoiceDateNameFilter == "Creation" && input.EndDate != null, e => (_invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.CreationTime).FirstOrDefault().AddHours(diffHour).Date) <= EDate.Value.Date)

            //            .WhereIf(input.InvoiceDateNameFilter == "InstallDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.InvoiceDateNameFilter == "InstallDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.InvoiceDateNameFilter == "InstallCompleteDate" && input.StartDate != null, e => e.InstalledcompleteDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.InvoiceDateNameFilter == "InstallCompleteDate" && input.EndDate != null, e => e.InstalledcompleteDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.InvoiceDateNameFilter == "BankDate" && input.StartDate != null, e => (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.Date).FirstOrDefault().Value.Date) >= SDate.Value.Date)
            //            .WhereIf(input.InvoiceDateNameFilter == "BankDate" && input.EndDate != null, e => (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.Date).FirstOrDefault().Value.Date) <= EDate.Value.Date)

            //            .WhereIf(input.InvoiceDateNameFilter == "UploadDate" && input.StartDate != null, e => (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.CreationTime).FirstOrDefault().AddHours(diffHour).Date) >= SDate.Value.Date)
            //            .WhereIf(input.InvoiceDateNameFilter == "UploadDate" && input.EndDate != null, e => (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.CreationTime).FirstOrDefault().AddHours(diffHour).Date) <= EDate.Value.Date)

            //            .WhereIf(input.Invoicenamefilter == "Owing", e => (e.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Any() ? _InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum() : 0)) > 0)
            //            .WhereIf(input.Invoicenamefilter == "FullPaid", e => (e.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Any() ? _InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum() : 0)) == 0)
            //            .WhereIf(input.Invoicenamefilter == "OverPayment", e => (e.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Any() ? _InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum() : 0)) < 0)

            //            .WhereIf(input.Installerid != null && input.Installerid != 0, e => e.InstallerId == input.Installerid)
            //            .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
            //            ;

            //var TotalAmountOfInvoice = filteredInvoiceIssued.Select(e => e.TotalCost).Sum();

            //var NewTotalAmountReceived = (from o in filteredInvoiceIssued
            //                              join Invoice in _InvoiceImportDataRepository.GetAll() on o.Id equals Invoice.JobId
            //                              select Invoice.PaidAmmount
            //                          ).Sum();

            //var TotalVICAmountOfInvoice = ((filteredInvoiceIssued.Select(e => e.SolarVICRebate).Sum() == null) ? 0 : (filteredInvoiceIssued.Select(e => e.SolarVICRebate).Sum()));
            //var TotalVICLoanOfInvoice = ((filteredInvoiceIssued.Select(e => e.SolarVICLoanDiscont).Sum() == null) ? 0 : (filteredInvoiceIssued.Select(e => e.SolarVICLoanDiscont).Sum()));

            //var TotalCustAmountReceived = (from o in filteredInvoiceIssued
            //                               join Invoice in _InvoiceImportDataRepository.GetAll() on o.Id equals Invoice.JobId
            //                               where Invoice.InvoicePaymentmothodName != "SVR"
            //                               select Invoice.PaidAmmount
            //                          ).Sum();

            //var TotalVICAmmointReceived = (from o in filteredInvoiceIssued
            //                               join Invoice in _InvoiceImportDataRepository.GetAll() on o.Id equals Invoice.JobId
            //                               where Invoice.InvoicePaymentmothodName == "SVR"
            //                               select Invoice.PaidAmmount
            //                          ).Sum();

            //var TotalSystemCapacityKW = filteredInvoiceIssued.Select(e => e.SystemCapacity).Sum();


            //var pagedAndFilteredInvoiceIssued = filteredInvoiceIssued
            //    .OrderBy(input.Sorting ?? "id desc")
            //    .PageBy(input);

            //var leadactive_list = _leadactivityRepository.GetAll().Where(e => e.SectionId == 9 && e.LeadFk.OrganizationId == input.OrganizationUnit);

            //var invoiceissued = from o in pagedAndFilteredInvoiceIssued

            //                    select new GetInvoiceIssuedForViewDto()
            //                    {
            //                        Job = new JobDto
            //                        {
            //                            RegPlanNo = o.RegPlanNo,
            //                            LotNumber = o.LotNumber,
            //                            Suburb = o.Suburb,
            //                            State = o.State,
            //                            UnitNo = o.UnitNo,
            //                            UnitType = o.UnitType,
            //                            NMINumber = o.NMINumber,
            //                            Id = o.Id,
            //                            ApplicationRefNo = o.ApplicationRefNo,
            //                            DistAppliedDate = o.DistAppliedDate,
            //                            ExpiryDate = o.ExpiryDate,
            //                            Notes = o.Note,
            //                            InstallerNotes = o.InstallerNotes,
            //                            JobNumber = o.JobNumber,
            //                            InstallationDate = o.InstallationDate,
            //                            InvoiceNotes = o.InvoiceNotes
            //                        },
            //                        LeadId = o.LeadId,
            //                        CompanyName = o.LeadFk.CompanyName,

            //                        Mobile = o.LeadFk.Mobile,
            //                        ProjectStatus = o.JobStatusFk.Name,
            //                        JobStatusColorClass = o.JobStatusFk.ColorClass,
            //                        Installer = _userRepository.GetAll().Where(e => e.Id == o.InstallerId).Select(e => e.FullName).FirstOrDefault(),
            //                        StateName = o.LeadFk.State,
            //                        ActivityReminderTime = leadactive_list.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
            //                        ActivityDescription = leadactive_list.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
            //                        ActivityComment = leadactive_list.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
            //                        TotalCost = o.TotalCost,

            //                        NewOwning = (o.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.PaidAmmount).Any() ? (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.PaidAmmount).Sum()) : 0)),

            //                        PaymentMethod = o.PaymentOptionFk.Name,

            //                        SalesRep = _userRepository.GetAll().Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),

            //                        TotalAmountOfInvoice = TotalAmountOfInvoice,
            //                        NewTotalAmountReceived = NewTotalAmountReceived == null ? 0 : NewTotalAmountReceived,
            //                        TotalVICAmountOfInvoice = TotalVICAmountOfInvoice == null ? 0 : TotalVICAmountOfInvoice,
            //                        TotalVICLoanOfInvoice = TotalVICLoanOfInvoice == null ? 0 : TotalVICLoanOfInvoice,
            //                        TotalCustAmountReceived = TotalCustAmountReceived == null ? 0 : TotalCustAmountReceived,
            //                        TotalVICAmmointReceived = TotalVICAmmointReceived == null ? 0 : TotalVICAmmointReceived,
            //                        TotalSystemCapacity = TotalSystemCapacityKW == null ? 0 : TotalSystemCapacityKW
            //                    };

            //var totalCount = await filteredInvoiceIssued.CountAsync();

            //return new PagedResultDto<GetInvoiceIssuedForViewDto>(totalCount, await invoiceissued.ToListAsync());
            #endregion

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var filteredInvoiceIssued = _lookup_jobRepository.GetAll().Include(e => e.LeadFk).Include(e => e.JobStatusFk).Include(e => e.PaymentOptionFk)
                        .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && (e.FirstDepositDate != null || e.InstallationDate != null))
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.JobPaymentOption > 0, e => e.PaymentOptionId == input.JobPaymentOption)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.LeadFk.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.LeadFk.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.PostalCode, input.PostalCodeFrom) >= 0)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.PostalCode, input.PostalCodeTo) <= 0)
                        .WhereIf(input.JobStatusID != null && input.JobStatusID.Count() > 0, e => input.JobStatusID.Contains((int)e.JobStatusId))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
                        .WhereIf(input.vicrebate != "All", e => e.VicRebate == input.vicrebate)
                        .WhereIf(input.solarRebateStatus != 0, e => e.SolarRebateStatus == input.solarRebateStatus)
                        .WhereIf(input.PaymentMethodId != null && input.PaymentMethodId != 0, e => _invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id && x.InvoicePaymentMethodId == input.PaymentMethodId).Any())

                        .WhereIf(input.InvoiceDateNameFilter == "Creation" && input.StartDate != null, e => (_invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.CreationTime).FirstOrDefault().AddHours(diffHour).Date) >= SDate.Value.Date)
                        .WhereIf(input.InvoiceDateNameFilter == "Creation" && input.EndDate != null, e => (_invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.CreationTime).FirstOrDefault().AddHours(diffHour).Date) <= EDate.Value.Date)

                        .WhereIf(input.InvoiceDateNameFilter == "InstallDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.InvoiceDateNameFilter == "InstallDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.InvoiceDateNameFilter == "InstallCompleteDate" && input.StartDate != null, e => e.InstalledcompleteDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.InvoiceDateNameFilter == "InstallCompleteDate" && input.EndDate != null, e => e.InstalledcompleteDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.InvoiceDateNameFilter == "BankDate" && input.StartDate != null, e => (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.Date).FirstOrDefault().Value.Date) >= SDate.Value.Date)
                        .WhereIf(input.InvoiceDateNameFilter == "BankDate" && input.EndDate != null, e => (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.Date).FirstOrDefault().Value.Date) <= EDate.Value.Date)

                        .WhereIf(input.InvoiceDateNameFilter == "UploadDate" && input.StartDate != null, e => (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.CreationTime).FirstOrDefault().AddHours(diffHour).Date) >= SDate.Value.Date)
                        .WhereIf(input.InvoiceDateNameFilter == "UploadDate" && input.EndDate != null, e => (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.CreationTime).FirstOrDefault().AddHours(diffHour).Date) <= EDate.Value.Date)

                        .WhereIf(input.Invoicenamefilter == "Owing", e => (e.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Any() ? _InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum() : 0)) > 0)
                        .WhereIf(input.Invoicenamefilter == "FullPaid", e => (e.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Any() ? _InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum() : 0)) == 0)
                        .WhereIf(input.Invoicenamefilter == "OverPayment", e => (e.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Any() ? _InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum() : 0)) < 0)

                        .WhereIf(input.Installerid != null && input.Installerid != 0, e => e.InstallerId == input.Installerid)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .AsNoTracking()
                        .Select(e => new { e.Id, e.LeadId, e.LeadFk, e.TotalCost, e.SolarVICRebate, e.SolarVICLoanDiscont, e.SystemCapacity, e.JobStatusFk, e.InstallerId, e.PaymentOptionFk, e.RegPlanNo, e.LotNumber, e.Suburb, e.State, e.UnitNo, e.UnitType, e.NMINumber, e.ApplicationRefNo, e.DistAppliedDate, e.ExpiryDate, e.Note, e.InstallerNotes, e.JobNumber, e.InstallationDate, e.InvoiceNotes })
                        ;

            var InvoiceImportData = _InvoiceImportDataRepository.GetAll().AsNoTracking().Select(e => new { e.Id, e.JobId, e.PaidAmmount, e.InvoicePaymentmothodName });

            var TotalAmountOfInvoice = await filteredInvoiceIssued.Select(e => e.TotalCost).SumAsync();

            var NewTotalAmountReceived = await (from o in filteredInvoiceIssued
                                                join Invoice in InvoiceImportData on o.Id equals Invoice.JobId
                                                select Invoice.PaidAmmount
                                      ).SumAsync();

            var TotalVICAmountOfInvoice = ((await filteredInvoiceIssued.Select(e => e.SolarVICRebate).SumAsync() == null) ? 0 : (await filteredInvoiceIssued.Select(e => e.SolarVICRebate).SumAsync()));
            var TotalVICLoanOfInvoice = ((await filteredInvoiceIssued.Select(e => e.SolarVICLoanDiscont).SumAsync() == null) ? 0 : (await filteredInvoiceIssued.Select(e => e.SolarVICLoanDiscont).SumAsync()));

            var TotalCustAmountReceived = await (from o in filteredInvoiceIssued
                                                 join Invoice in InvoiceImportData on o.Id equals Invoice.JobId
                                                 where Invoice.InvoicePaymentmothodName != "SVR"
                                                 select Invoice.PaidAmmount
                                      ).SumAsync();

            var TotalVICAmmointReceived = await (from o in filteredInvoiceIssued
                                                 join Invoice in InvoiceImportData on o.Id equals Invoice.JobId
                                                 where Invoice.InvoicePaymentmothodName == "SVR"
                                                 select Invoice.PaidAmmount
                                      ).SumAsync();

            var TotalSystemCapacityKW = await filteredInvoiceIssued.Select(e => e.SystemCapacity).SumAsync();

            var pagedAndFilteredInvoiceIssued = filteredInvoiceIssued
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var leadactive_list = _leadactivityRepository.GetAll().Where(e => e.SectionId == 9 && e.LeadFk.OrganizationId == input.OrganizationUnit).AsNoTracking().Select(e => new { e.Id, e.ActionId, e.LeadId, e.ActivityDate, e.ActivityNote });

            var invoiceissued = from o in pagedAndFilteredInvoiceIssued

                                select new GetInvoiceIssuedForViewDto()
                                {
                                    Job = new JobDto
                                    {
                                        RegPlanNo = o.RegPlanNo,
                                        LotNumber = o.LotNumber,
                                        Suburb = o.Suburb,
                                        State = o.State,
                                        UnitNo = o.UnitNo,
                                        UnitType = o.UnitType,
                                        NMINumber = o.NMINumber,
                                        Id = o.Id,
                                        ApplicationRefNo = o.ApplicationRefNo,
                                        DistAppliedDate = o.DistAppliedDate,
                                        ExpiryDate = o.ExpiryDate,
                                        Notes = o.Note,
                                        InstallerNotes = o.InstallerNotes,
                                        JobNumber = o.JobNumber,
                                        InstallationDate = o.InstallationDate,
                                        InvoiceNotes = o.InvoiceNotes
                                    },
                                    LeadId = o.LeadId,
                                    CompanyName = o.LeadFk.CompanyName,

                                    Mobile = o.LeadFk.Mobile,
                                    ProjectStatus = o.JobStatusFk.Name,
                                    JobStatusColorClass = o.JobStatusFk.ColorClass,
                                    Installer = User_List.Where(e => e.Id == o.InstallerId).Select(e => e.FullName).FirstOrDefault(),
                                    StateName = o.LeadFk.State,
                                    ActivityReminderTime = leadactive_list.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                                    ActivityDescription = leadactive_list.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                    ActivityComment = leadactive_list.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                    TotalCost = o.TotalCost,

                                    NewOwning = (o.TotalCost - (InvoiceImportData.Where(e => e.JobId == o.Id).Select(e => e.PaidAmmount).Any() ? (InvoiceImportData.Where(e => e.JobId == o.Id).Select(e => e.PaidAmmount).Sum()) : 0)),

                                    PaymentMethod = o.PaymentOptionFk.Name,

                                    SalesRep = User_List.Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),

                                    TotalAmountOfInvoice = TotalAmountOfInvoice,
                                    NewTotalAmountReceived = NewTotalAmountReceived == null ? 0 : NewTotalAmountReceived,
                                    TotalVICAmountOfInvoice = TotalVICAmountOfInvoice == null ? 0 : TotalVICAmountOfInvoice,
                                    TotalVICLoanOfInvoice = TotalVICLoanOfInvoice == null ? 0 : TotalVICLoanOfInvoice,
                                    TotalCustAmountReceived = TotalCustAmountReceived == null ? 0 : TotalCustAmountReceived,
                                    TotalVICAmmointReceived = TotalVICAmmointReceived == null ? 0 : TotalVICAmmointReceived,
                                    TotalSystemCapacity = TotalSystemCapacityKW == null ? 0 : TotalSystemCapacityKW
                                };

            var totalCount = await filteredInvoiceIssued.CountAsync();

            return new PagedResultDto<GetInvoiceIssuedForViewDto>(totalCount, await invoiceissued.ToListAsync());
        }

        public async Task<FileDto> GetAllInvoiceIssuedDataExcel(GetAllInvoiceIssuedInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredInvoiceIssued = _lookup_jobRepository.GetAll().Include(e => e.LeadFk).Include(e => e.JobStatusFk).Include(e => e.PaymentOptionFk)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter)
                        //|| e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.JobPaymentOption > 0, e => e.PaymentOptionId == input.JobPaymentOption)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.LeadFk.Suburb == input.PostCodeSuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.LeadFk.State == input.StateNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.PostalCode, input.PostalCodeFrom) >= 0)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.PostalCode, input.PostalCodeTo) <= 0)
                        .WhereIf(input.JobStatusID != null && input.JobStatusID.Count() > 0, e => input.JobStatusID.Contains((int)e.JobStatusId))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
                        .WhereIf(input.vicrebate != "All", e => e.VicRebate == input.vicrebate)
                        .WhereIf(input.solarRebateStatus != 0, e => e.SolarRebateStatus == input.solarRebateStatus)
                        .WhereIf(input.PaymentMethodId != null && input.PaymentMethodId != 0, e => _invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id && x.InvoicePaymentMethodId == input.PaymentMethodId).Any())

                        .WhereIf(input.InvoiceDateNameFilter == "Creation" && input.StartDate != null, e => (_invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.CreationTime).FirstOrDefault().AddHours(diffHour).Date) >= SDate.Value.Date)
                        .WhereIf(input.InvoiceDateNameFilter == "Creation" && input.EndDate != null, e => (_invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.CreationTime).FirstOrDefault().AddHours(diffHour).Date) <= EDate.Value.Date)

                        .WhereIf(input.InvoiceDateNameFilter == "InstallDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.InvoiceDateNameFilter == "InstallDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.InvoiceDateNameFilter == "InstallCompleteDate" && input.StartDate != null, e => e.InstalledcompleteDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.InvoiceDateNameFilter == "InstallCompleteDate" && input.EndDate != null, e => e.InstalledcompleteDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.InvoiceDateNameFilter == "BankDate" && input.StartDate != null, e => (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.Date).FirstOrDefault().Value.Date) >= SDate.Value.Date)
                        .WhereIf(input.InvoiceDateNameFilter == "BankDate" && input.EndDate != null, e => (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.Date).FirstOrDefault().Value.Date) <= EDate.Value.Date)

                        .WhereIf(input.InvoiceDateNameFilter == "UploadDate" && input.StartDate != null, e => (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.CreationTime).FirstOrDefault().AddHours(diffHour).Date) >= SDate.Value.Date)
                        .WhereIf(input.InvoiceDateNameFilter == "UploadDate" && input.EndDate != null, e => (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.CreationTime).FirstOrDefault().AddHours(diffHour).Date) <= EDate.Value.Date)

                        .WhereIf(input.Invoicenamefilter == "Owing", e => (e.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Any() ? _InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum() : 0)) > 0)
                        .WhereIf(input.Invoicenamefilter == "FullPaid", e => (e.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Any() ? _InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum() : 0)) == 0)
                        .WhereIf(input.Invoicenamefilter == "OverPayment", e => (e.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Any() ? _InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum() : 0)) < 0)

                        .WhereIf(input.Installerid != null && input.Installerid != 0, e => e.InstallerId == input.Installerid)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)

                        .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && (e.FirstDepositDate != null || e.InstallationDate != null)).OrderBy(e => e.Id);

            //var jobids = filteredInvoiceIssued.Select(e => e.Id).ToList();

            var leadactive_list = _leadactivityRepository.GetAll().Where(e => e.SectionId == 9 && e.LeadFk.OrganizationId == input.OrganizationUnit);

            var invoiceissued = from o in filteredInvoiceIssued

                                select new GetInvoiceIssuedForViewDto()
                                {
                                    Job = new JobDto
                                    {
                                        RegPlanNo = o.RegPlanNo,
                                        LotNumber = o.LotNumber,
                                        Suburb = o.Suburb,
                                        State = o.State,
                                        UnitNo = o.UnitNo,
                                        UnitType = o.UnitType,
                                        NMINumber = o.NMINumber,
                                        Id = o.Id,
                                        ApplicationRefNo = o.ApplicationRefNo,
                                        DistAppliedDate = o.DistAppliedDate,
                                        ExpiryDate = o.ExpiryDate,
                                        Notes = o.Note,
                                        InstallerNotes = o.InstallerNotes,
                                        JobNumber = o.JobNumber,
                                        InstallationDate = o.InstallationDate,
                                        InvoiceNotes = o.InvoiceNotes,
                                        Stc = o.STC,
                                        SystemCapacity = o.SystemCapacity

                                    },
                                    LeadId = o.LeadId,
                                    CompanyName = o.LeadFk.CompanyName,

                                    Mobile = o.LeadFk.Mobile,
                                    ProjectStatus = o.JobStatusFk.Name,
                                    JobStatusColorClass = o.JobStatusFk.ColorClass,
                                    Installer = _userRepository.GetAll().Where(e => e.Id == o.InstallerId).Select(e => e.FullName).FirstOrDefault(),
                                    StateName = o.LeadFk.State,
                                    ActivityReminderTime = leadactive_list.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                                    ActivityDescription = leadactive_list.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                    ActivityComment = leadactive_list.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                    TotalCost = o.TotalCost,

                                    NewOwning = (o.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.PaidAmmount).Any() ? (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.PaidAmmount).Sum()) : 0)),

                                    PaymentMethod = o.PaymentOptionFk.Name,

                                    SalesRep = _userRepository.GetAll().Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault()

                                };

            //var invoiceissued = from o in filteredInvoiceIssued

            //                    join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
            //                    from s6 in j6.DefaultIfEmpty()

            //                    join o8 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o8.Id into j8
            //                    from s8 in j8.DefaultIfEmpty()

            //                    select new GetInvoiceIssuedForViewDto()
            //                    {
            //                        Job = new JobDto
            //                        {
            //                            RegPlanNo = o.RegPlanNo,
            //                            LotNumber = o.LotNumber,
            //                            Suburb = o.Suburb,
            //                            State = o.State,
            //                            UnitNo = o.UnitNo,
            //                            UnitType = o.UnitType,
            //                            NMINumber = o.NMINumber,
            //                            Id = o.Id,
            //                            ApplicationRefNo = o.ApplicationRefNo,
            //                            DistAppliedDate = o.DistAppliedDate,
            //                            ExpiryDate = o.ExpiryDate,
            //                            Notes = o.Note,
            //                            InstallerNotes = o.InstallerNotes,
            //                            JobNumber = o.JobNumber,
            //                            InstallationDate = o.InstallationDate
            //                        },
            //                        LeadId = s6.Id,
            //                        CompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
            //                        Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
            //                        Phone = s6 == null || s6.Phone == null ? "" : s6.Phone.ToString(),
            //                        Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
            //                        Address = s6 == null || s6.Address == null ? "" : s6.Address.ToString(),
            //                        ProjectStatus = s8 == null || s8.Name == null ? "" : s8.Name.ToString(),
            //                        Installer = _userRepository.GetAll().Where(e => e.Id == o.InstallerId).Select(e => e.FullName).FirstOrDefault(),
            //                        StateName = s6 == null || s6.State == null ? "" : s6.State.ToString(),
            //                        TotalCost = o.TotalCost,
            //                        Owning = (o.TotalCost - (_invoicePaymentRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.InvoicePayTotal).Sum())),
            //                        NewOwning = (o.TotalCost - ((_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.PaidAmmount).Sum()))),
            //                        PaymentMethod = _lookup_paymentOptionRepository.GetAll().Where(e => e.Id == o.PaymentOptionId).Select(e => e.Name).FirstOrDefault(),
            //                        CreationDate = _invoicePaymentRepository.GetAll().Where(e => e.JobId == o.Id).OrderByDescending(e => e.Id).Select(e => e.CreationTime).FirstOrDefault(),
            //                        InvoiceID = _invoicePaymentRepository.GetAll().Where(e => e.JobId == o.Id).OrderByDescending(e => e.Id).Select(e => e.Id).FirstOrDefault(),
            //                        TotalpaymentData = _invoicePaymentRepository.GetAll().Where(e => jobids.Contains((int)e.JobId)).Count(),
            //                        TotalpaymentVerifyData = _invoicePaymentRepository.GetAll().Where(e => e.IsVerified == true && jobids.Contains((int)e.JobId)).Count(),
            //                        TotalpaymentnotVerifyData = _invoicePaymentRepository.GetAll().Where(e => e.IsVerified == false && jobids.Contains((int)e.JobId)).Count(),
            //                        TotalAmountOfInvoice = filteredInvoiceIssued.Select(e => e.TotalCost).Sum(),
            //                        TotalRefundamount = _jobRefundRepository.GetAll().Where(e => jobids.Contains((int)e.JobId)).Select(e => e.Amount).Sum(),
            //                        TotalAmountReceived = _invoicePaymentRepository.GetAll().Where(e => jobids.Contains((int)e.JobId)).Select(e => e.InvoicePayTotal).Sum(),
            //                        TotalCancelAmount = filteredInvoiceIssued.Where(e => e.JobStatusId == 3).Select(e => e.TotalCost).Sum(),
            //                        TotalVICAmountOfInvoice = filteredInvoiceIssued.Where(e => e.LeadFk.State == "VIC").Select(e => e.SolarVICRebate).Sum(),
            //                        TotalCustAmountOfInvoice = filteredInvoiceIssued.Where(e => e.LeadFk.State == "VIC").Select(e => e.TotalCost).Sum(),
            //                        TotalCustAmountReceived = _invoicePaymentRepository.GetAll().Where(e => jobids.Contains((int)e.JobId) && e.JobFk.LeadFk.State == "VIC").Select(e => e.InvoicePayTotal).Sum(),
            //                    };

            var gridconnectionListDtos = await invoiceissued.ToListAsync();
            if (input.excelorcsv == 1)
            {
                //return _leadsExcelExporter.InvoiceIssuedExportToFile(gridconnectionListDtos);
                return _leadsExcelExporter.InvoiceIssuedExportToFile(gridconnectionListDtos, "InvoiceIssued.xlsx");
            }
            else
            {
                return _leadsExcelExporter.InvoiceIssuedExportToFile(gridconnectionListDtos, "InvoiceIssued.csv");
                //return _leadsExcelExporter.InvoiceIssuedExportCSVFile(gridconnectionListDtos);
            }

        }

        public async Task<FileDto> GetAllInvoiceIssuedOwningDataExcel(GetAllInvoiceIssuedInput input)
        {
            var filteredInvoiceIssued = _lookup_jobRepository.GetAll().Include(e => e.LeadFk).Include(e => e.JobStatusFk).Include(e => e.PaymentOptionFk)
                .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.FirstDepositDate != null)
                //.Where(o => (o.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == o.Id).Any() ? (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.PaidAmmount).Sum()) : 0)) > 0)
                ;

            var invoiceissued = from o in filteredInvoiceIssued
                                select new GetInvoiceIssuedForViewDto()
                                {
                                    Job = new JobDto
                                    {
                                        InstallationDate = o.InstallationDate,
                                        State = o.State,
                                        JobNumber = o.JobNumber,
                                        SolarVICRebate = o.SolarVICRebate,
                                        SolarVICLoanDiscont = o.SolarVICLoanDiscont
                                    },

                                    FinanceWith = o.PaymentOptionFk.Name,
                                    CompanyName = o.LeadFk.CompanyName,
                                    ProjectStatus = o.JobStatusFk.Name,
                                    TotalCost = o.TotalCost,
                                    NewOwning = (o.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == o.Id).Any() ? (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.PaidAmmount).Sum()) : 0)),
                                };

            var invoiceissuedListDtos = await invoiceissued.ToListAsync();

            return _leadsExcelExporter.InvoiceIssuedOwingExportToFile(invoiceissuedListDtos, "Owing.xlsx");
        }

        public async Task<FileDto> getAllInvoiceIssueddepositeDataExcel(GetAllInvoiceIssuedInput input)
        {
            //var listofincoiceid = _InvoiceImportDataRepository.GetAll().Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit).Select(e => e.JobId).ToList();
            //var filteredInvoiceIssued = _lookup_jobRepository.GetAll().Where(e => !listofincoiceid.Contains(e.Id));

            //var JobStatus = new List<int?> { 1, 2, 3, 4, 5 };

            var filteredInvoiceIssued = _lookup_jobRepository.GetAll().Include(e => e.LeadFk).Include(e => e.JobStatusFk)
                                        .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);

            var invoiceissued = from o in filteredInvoiceIssued

                                select new GetInvoiceIssuedForViewDto()
                                {
                                    Job = new JobDto
                                    {
                                        Id = o.Id,
                                        JobNumber = o.JobNumber,
                                        State = o.State,
                                    },

                                    ProjectStatus = o.JobStatusFk.Name,
                                    CompanyName = o.LeadFk.CompanyName,
                                    Mobile = o.LeadFk.Mobile,
                                    Installer = _userRepository.GetAll().Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                                    DepositRequired = o.DepositRequired
                                };

            var invoiceissuedData = await invoiceissued.ToListAsync();

            return _leadsExcelExporter.InvoiceIssuedDepositeExportToFile(invoiceissuedData, "Deposit.xlsx");
        }

        public async Task<List<ImportDataViewDto>> GetAllinvoiceiisuedpaymentView(int jobid)
        {
            var paymentviewdata = _InvoiceImportDataRepository.GetAll().Where(e => e.JobId == jobid).ToList();

            var fielist = (from o in paymentviewdata
                           select new ImportDataViewDto()
                           {

                               CreatedUser = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                               Date = o.Date,
                               JobNumber = o.JobNumber,
                               PaymentMethodName = o.InvoicePaymentmothodName,
                               Description = o.Description,
                               PaidAmmount = o.PaidAmmount,
                               AllotedBy = o.AllotedBy,
                               InvoiceNotDescription = o.InvoiceNotesDescription,
                               CreatedDate = o.CreationTime,
                               SSCharge = o.SSCharge,
                               ReceiptNumber = o.ReceiptNumber,
                               PurchaseNumber = o.PurchaseNumber
                           }).ToList();

            return new List<ImportDataViewDto>(fielist.ToList());
        }
   
        public async Task<Decimal> CustomerPaidForActualAmount(int jobid)
        {
            decimal data = (decimal)_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == jobid).Sum(e => e.PaidAmmount);

            return data;
        }
    
    }
}