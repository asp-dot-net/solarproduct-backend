﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Invoices.Exporting;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.UI;
using TheSolarProduct.Storage;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.CheckActives;

namespace TheSolarProduct.Invoices
{
    [AbpAuthorize(AppPermissions.Pages_InvoiceStatuses)]
    public class InvoiceStatusesAppService : TheSolarProductAppServiceBase, IInvoiceStatusesAppService
    {
        private readonly IRepository<InvoiceStatus> _invoiceStatusRepository;
        private readonly IInvoiceStatusesExcelExporter _invoiceStatusesExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public InvoiceStatusesAppService(IRepository<InvoiceStatus> invoiceStatusRepository, IInvoiceStatusesExcelExporter invoiceStatusesExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _invoiceStatusRepository = invoiceStatusRepository;
            _invoiceStatusesExcelExporter = invoiceStatusesExcelExporter;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;

        }

        public async Task<PagedResultDto<GetInvoiceStatusForViewDto>> GetAll(GetAllInvoiceStatusesInput input)
        {

            var filteredInvoiceStatuses = _invoiceStatusRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

            var pagedAndFilteredInvoiceStatuses = filteredInvoiceStatuses
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var invoiceStatuses = from o in pagedAndFilteredInvoiceStatuses
                                  select new
                                  {

                                      o.Name,
                                      Id = o.Id,
                                      o.IsActive,
                                  };

            var totalCount = await filteredInvoiceStatuses.CountAsync();

            var dbList = await invoiceStatuses.ToListAsync();
            var results = new List<GetInvoiceStatusForViewDto>();

            foreach (var o in dbList)
            {
                var res = new GetInvoiceStatusForViewDto()
                {
                    InvoiceStatus = new InvoiceStatusDto
                    {

                        Name = o.Name,
                        Id = o.Id,
                        IsActive = o.IsActive,  
                    }
                };

                results.Add(res);
            }

            return new PagedResultDto<GetInvoiceStatusForViewDto>(
                totalCount,
                results
            );

        }

        public async Task<GetInvoiceStatusForViewDto> GetInvoiceStatusForView(int id)
        {
            var invoiceStatus = await _invoiceStatusRepository.GetAsync(id);

            var output = new GetInvoiceStatusForViewDto { InvoiceStatus = ObjectMapper.Map<InvoiceStatusDto>(invoiceStatus) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_InvoiceStatuses_Edit)]
        public async Task<GetInvoiceStatusForEditOutput> GetInvoiceStatusForEdit(EntityDto input)
        {
            var invoiceStatus = await _invoiceStatusRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetInvoiceStatusForEditOutput { InvoiceStatus = ObjectMapper.Map<CreateOrEditInvoiceStatusDto>(invoiceStatus) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditInvoiceStatusDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_InvoiceStatuses_Create)]
        protected virtual async Task Create(CreateOrEditInvoiceStatusDto input)
        {
            var invoiceStatus = ObjectMapper.Map<InvoiceStatus>(input);

            if (AbpSession.TenantId != null)
            {
                invoiceStatus.TenantId = (int)AbpSession.TenantId;
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 25;
            dataVaultLog.ActionNote = "Invoice Statuses Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _invoiceStatusRepository.InsertAsync(invoiceStatus);

        }

        [AbpAuthorize(AppPermissions.Pages_InvoiceStatuses_Edit)]
        protected virtual async Task Update(CreateOrEditInvoiceStatusDto input)
        {
            var invoiceStatus = await _invoiceStatusRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 25;
            dataVaultLog.ActionNote = "Invoice Statuses Updated : " + invoiceStatus.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != invoiceStatus.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = invoiceStatus.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != invoiceStatus.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = invoiceStatus.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, invoiceStatus);

        }

        [AbpAuthorize(AppPermissions.Pages_InvoiceStatuses_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _invoiceStatusRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 25;
            dataVaultLog.ActionNote = "Invoice Statuses Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _invoiceStatusRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetInvoiceStatusesToExcel(GetAllInvoiceStatusesForExcelInput input)
        {

            var filteredInvoiceStatuses = _invoiceStatusRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

            var query = (from o in filteredInvoiceStatuses
                         select new GetInvoiceStatusForViewDto()
                         {
                             InvoiceStatus = new InvoiceStatusDto
                             {
                                 Name = o.Name,
                                 Id = o.Id,
                                 IsActive = o.IsActive,
                             }
                         });

            var invoiceStatusListDtos = await query.ToListAsync();

            return _invoiceStatusesExcelExporter.ExportToFile(invoiceStatusListDtos);
        }

    }
}