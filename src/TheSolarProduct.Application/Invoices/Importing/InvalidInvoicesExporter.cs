﻿using System.Collections.Generic;
using Abp.Collections.Extensions;
using Abp.Dependency;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Invoices.Importing.Dto;
using TheSolarProduct.Leads.Importing.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Invoices.Importing
{
	public class InvalidInvoicesExporter : NpoiExcelExporterBase, IInvalidInvoicesExporter, ITransientDependency
	{
		public InvalidInvoicesExporter(ITempFileCacheManager tempFileCacheManager)
			: base(tempFileCacheManager)
		{
		}

        public FileDto ExportToFile(List<ImportInvoicesDto> invoiceListDtos)
        {

            return CreateExcelPackage(
           "InvalidInvoices.xlsx",
            excelPackage =>
            {
                var sheet = excelPackage.CreateSheet("InvalidInvoices");

                AddHeader(
                      sheet,
                      L("Date"),
                      L("ProjectNo"),
                      L("PayBy"),
                      L("Description"),
                      L("AmountPaid"),
                      L("SSCharge"),
                      L("AllocatedBy"),
                      L("InvoiceNotesDescription"),
                      L("Exception")
                  );

                AddObjects(
                      sheet, 2, invoiceListDtos,
                      _ => _.Date,
                      _ => _.ProjectNo,
                      _ => _.PayBy,
                      _ => _.Description,
                      _ => _.AmountPaid,
                      _ => _.SSCharge,
                      _ => _.AllocatedBy,
                      _ => _.InvoiceNotesDescription,
                      _ => _.Exception
                  );

                  //for (var i = 0; i < 16; i++)
                  //{
                  //	sheet.AutoSizeColumn(i);
                  //}
              });
        }
    }
}
