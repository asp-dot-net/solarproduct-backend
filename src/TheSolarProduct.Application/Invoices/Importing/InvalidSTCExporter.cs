﻿using System.Collections.Generic;
using Abp.Collections.Extensions;
using Abp.Dependency;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Invoices.Importing.Dto;
using TheSolarProduct.Leads.Importing.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Invoices.Importing
{
	public class InvalidSTCExporter : NpoiExcelExporterBase, IInvalidSTCExporter, ITransientDependency
	{
		public InvalidSTCExporter(ITempFileCacheManager tempFileCacheManager)
			: base(tempFileCacheManager)
		{
		}

        public FileDto ExportToFile(List<ImportSTCDto> stcListDtos)
        {

            return CreateExcelPackage(
         "InvalidSTCS.xlsx",
          excelPackage =>
          {
              var sheet = excelPackage.CreateSheet("InvalidSTC");

              //AddHeader(
              //      sheet,
              //      L("PVDNumber"),
              //      L("ProjectNumber"),
              //      L("ManualQuoteNumber"),
              //      L("ProjManual"),
              //      L("PVDStatus"),
              //      L("STCApplied")
              //  );

              //AddObjects(
              //      sheet, 2, stcListDtos,
              //      _ => _.PVDNumber,
              //      _ => _.ProjectNumber,
              //      _ => _.ManualQuote,
              //      _ => null,
              //      _ => _.PVDStatus,
              //      _ => _.STCAppliedDate,
              //      _ => _.Exception
              //  );

              AddHeader(sheet,
                  "PVDNumber",
                  "JobNumber",
                  "NoOfPanels",
                  "CreationDate",
                  "RECCreated",
                  "RECPassed",
                  "RECPanding",
                  "RECFailed",
                  "RECRegistered",
                  "LastAuditedDate",
                  "BlukUploadId",
                  "Exception"
                  );


              AddObjects(
                    sheet, 2, stcListDtos,
                    _ => _.PVDNumber,
                    _ => _.JobNumber,
                    _ => _.NoOfPanels,
                    _ => _.CreationDate,
                    _ => _.RECCreated,
                    _ => _.RECPassed,
                    _ => _.RECPanding,
                    _ => _.RECFailed,
                    _ => _.RECRegistered,
                    _ => _.LastAuditedDate,
                    _ => _.BlukUploadId,
                    _ => _.Exception
                );




          });

        }
    }
}
