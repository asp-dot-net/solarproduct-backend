﻿using Abp.Authorization;
using Abp.Localization;
using Abp.Localization.Sources;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Invoices.Importing.Dto;

namespace TheSolarProduct.Invoices.Importing
{
    public class InstallationInvoiceListExcelDataReader : NpoiExcelImporterBase<ImportInstalltionInvoiceDto>, IInstallationInvoiceListExcelDataReader
    {
        private readonly ILocalizationSource _localizationSource;

        public InstallationInvoiceListExcelDataReader(ILocalizationManager localizationManager)
        {
            _localizationSource = localizationManager.GetSource(TheSolarProductConsts.LocalizationSourceName);
        }

        public List<ImportInstalltionInvoiceDto> GetInstallationInvoiceFromExcel(byte[] fileBytes)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow);
        }

        private ImportInstalltionInvoiceDto ProcessExcelRow(ISheet worksheet, int row)
        {
            //if (IsRowEmpty(worksheet, row))
            //{
            //    return null;
            //}

            var exceptionMessage = new StringBuilder();
            var installtionInvoice = new ImportInstalltionInvoiceDto();

            //IRow roww = worksheet.GetRow(row);
            //List<ICell> cells = roww.Cells;
            //List<string> rowData = new List<string>();

            //for (int colNumber = 0; colNumber < 11; colNumber++)
            //{
            //    ICell cell = roww.GetCell(colNumber, MissingCellPolicy.CREATE_NULL_AS_BLANK);
            //    rowData.Add(cell.ToString());
            //}

            //try
            //{
            //    installtionInvoice.JobNumber = rowData[0];
            //    installtionInvoice.InvoiceNo = Convert.ToString(rowData[1]);
            //    installtionInvoice.Date = DateTime.Parse(rowData[2], CultureInfo.CreateSpecificCulture("en-AU"));
            //    installtionInvoice.BankRefrenceNo = Convert.ToString(rowData[3]);
            //    installtionInvoice.Remark = Convert.ToString(rowData[4]);
            //}

            //catch (System.Exception exception)
            //{
            //    installtionInvoice.Exception = exception.Message;
            //}

            //return installtionInvoice;

            try
            {

                worksheet.GetRow(row).Cells[0].SetCellType(CellType.String);
                //var abc = worksheet.GetRow(row).Cells[0].NumericCellValue;
                //var date23 = DateTime.FromOADate(worksheet.GetRow(row).Cells[0].NumericCellValue);
                installtionInvoice.JobNumber = worksheet.GetRow(row).Cells[0]?.StringCellValue;

                worksheet.GetRow(row).Cells[1].SetCellType(CellType.String);
                installtionInvoice.InvoiceNo = worksheet.GetRow(row).Cells[1]?.StringCellValue;
                //worksheet.GetRow(row).Cells[2].SetCellType(CellType.String);
                worksheet.GetRow(row).Cells[2].SetCellType(CellType.Numeric);
                var date23 = DateTime.FromOADate(worksheet.GetRow(row).Cells[2].NumericCellValue);
                installtionInvoice.Date = Convert.ToDateTime(date23);

                worksheet.GetRow(row).Cells[3].SetCellType(CellType.String);
                installtionInvoice.BankRefrenceNo = worksheet.GetRow(row).Cells[3]?.StringCellValue;

                worksheet.GetRow(row).Cells[4].SetCellType(CellType.String);
                installtionInvoice.Remark = worksheet.GetRow(row).Cells[4]?.StringCellValue;

                //worksheet.GetRow(row).Cells[5].SetCellType(CellType.String);
                //invoice.SSCharge = worksheet.GetRow(row).Cells[5]?.StringCellValue;
                //worksheet.GetRow(row).Cells[6].SetCellType(CellType.String);
                //invoice.AllocatedBy = worksheet.GetRow(row).Cells[6]?.StringCellValue;
                //worksheet.GetRow(row).Cells[7].SetCellType(CellType.String);
                //invoice.ReceiptNumber = worksheet.GetRow(row).Cells[7]?.StringCellValue;
                //worksheet.GetRow(row).Cells[8].SetCellType(CellType.String);
                //invoice.PurchaseNumber = worksheet.GetRow(row).Cells[8]?.StringCellValue;
                //worksheet.GetRow(row).Cells[9].SetCellType(CellType.String);
                //invoice.InvoiceNotesDescription = worksheet.GetRow(row).Cells[9]?.StringCellValue;
            }

            catch (System.Exception exception)
            {
                installtionInvoice.Exception = exception.Message;
            }

            return installtionInvoice;
        }

        private string GetLocalizedExceptionMessagePart(string parameter)
        {
            return _localizationSource.GetString("{0}IsInvalid", _localizationSource.GetString(parameter)) + "; ";
        }

        private string GetRequiredValueFromRowOrNull(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].StringCellValue;
            if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue))
            {
                return cellValue;
            }

            exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
            return null;
        }

        private double GetValue(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].NumericCellValue;

            return cellValue;

        }

        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
            return cell == null || string.IsNullOrWhiteSpace(cell.StringCellValue);
        }
    }
}
