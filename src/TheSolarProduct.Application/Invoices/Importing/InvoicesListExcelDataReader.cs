﻿using Abp.Localization;
using Abp.Localization.Sources;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using NPOI.HSSF.Record;
using NPOI.SS.UserModel;
using NPOI.XSSF.Streaming.Values;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Invoices.Importing.Dto;

namespace TheSolarProduct.Invoices.Importing
{
    public class InvoicesListExcelDataReader : NpoiExcelImporterBase<ImportInvoicesDto>, IInvoicesListExcelDataReader
    {
        private readonly ILocalizationSource _localizationSource;
        private readonly IWebHostEnvironment _env;

        public InvoicesListExcelDataReader(ILocalizationManager localizationManager, IWebHostEnvironment env)
        {
            _localizationSource = localizationManager.GetSource(TheSolarProductConsts.LocalizationSourceName);
            _env = env;

        }

        public List<ImportInvoicesDto> GetInvoicesFromExcel(byte[] fileBytes)
        {
            //var Path1 = _env.WebRootPath;
            //var MainFolder = Path1;
            //MainFolder = MainFolder;
            //string path = MainFolder + "\\" + "GetInvoicesListFromExcelOrNull_Test.txt";
            //// This text is added only once to the file.
            //if (!File.Exists(path))
            //{
            //    // Create a file to write to.
            //    using (StreamWriter sw = File.CreateText(path))
            //    {
            //        sw.WriteLine(fileBytes);
            //    }
            //}

            // This text is always added, making the file longer over time
            // if it is not deleted.
            //using (StreamWriter sw = File.AppendText(path))
            //{
            //    sw.WriteLine(fileBytes);
            //}
            return ProcessExcelFile(fileBytes, ProcessExcelRow);
        }

        private ImportInvoicesDto ProcessExcelRow(ISheet worksheet, int row)
        {
            //if (IsRowEmpty(worksheet, row))
            //{
            //    return null;
            //}

            var exceptionMessage = new StringBuilder();
            var invoice = new ImportInvoicesDto();

            //IRow roww = worksheet.GetRow(row);
            //List<ICell> cells = roww.Cells;
            //List<string> rowData = new List<string>();
            //List<DateTime> rowDataDate = new List<DateTime>();

            //for (int colNumber = 0; colNumber <= 9; colNumber++)
            //{
            //    ICell cell = roww.GetCell(colNumber, MissingCellPolicy.CREATE_NULL_AS_BLANK);
            //    rowData.Add(cell.ToString());
            //}

            try
            {
                
                worksheet.GetRow(row).Cells[0].SetCellType(CellType.Numeric);
                var abc = worksheet.GetRow(row).Cells[0].NumericCellValue;
                var date23 = DateTime.FromOADate(worksheet.GetRow(row).Cells[0].NumericCellValue);
                invoice.Date = Convert.ToDateTime(date23);
                worksheet.GetRow(row).Cells[1].SetCellType(CellType.String);
                invoice.ProjectNo = worksheet.GetRow(row).Cells[1]?.StringCellValue;
                worksheet.GetRow(row).Cells[2].SetCellType(CellType.String);
                invoice.PayBy = worksheet.GetRow(row).Cells[2]?.StringCellValue;
                worksheet.GetRow(row).Cells[3].SetCellType(CellType.String);
                invoice.Description = worksheet.GetRow(row).Cells[3]?.StringCellValue;
                worksheet.GetRow(row).Cells[4].SetCellType(CellType.String);
                invoice.AmountPaid = worksheet.GetRow(row).Cells[4]?.StringCellValue;
                worksheet.GetRow(row).Cells[5].SetCellType(CellType.String);
                invoice.SSCharge = worksheet.GetRow(row).Cells[5]?.StringCellValue;
                worksheet.GetRow(row).Cells[6].SetCellType(CellType.String);
                invoice.AllocatedBy = worksheet.GetRow(row).Cells[6]?.StringCellValue;
                worksheet.GetRow(row).Cells[7].SetCellType(CellType.String);
                invoice.ReceiptNumber = worksheet.GetRow(row).Cells[7]?.StringCellValue;
                worksheet.GetRow(row).Cells[8].SetCellType(CellType.String);
                invoice.PurchaseNumber = worksheet.GetRow(row).Cells[8]?.StringCellValue;
                worksheet.GetRow(row).Cells[9].SetCellType(CellType.String);
                invoice.InvoiceNotesDescription = worksheet.GetRow(row).Cells[9]?.StringCellValue;
            }

            catch (System.Exception exception)
            {
                invoice.Exception = exception.Message;
            }

            return invoice;
        }

        private string GetLocalizedExceptionMessagePart(string parameter)
        {
            return _localizationSource.GetString("{0}IsInvalid", _localizationSource.GetString(parameter)) + "; ";
        }

        private string GetRequiredValueFromRowOrNull(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].StringCellValue;
            if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue))
            {
                return cellValue;
            }

            exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
            return null;
        }

        private double GetValue(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].NumericCellValue;

            return cellValue;

        }

        //private bool IsRowEmpty(ISheet worksheet, int row)
        //{
        //    DataFormatter formatter = new DataFormatter();
        //    var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
        //    return cell == null || string.IsNullOrWhiteSpace(cell.StringCellValue) || cell.DateCellValue == null;
        //}
        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
            DataFormatter formatter = new DataFormatter();
            string val = formatter.FormatCellValue(cell);
            return val == null || string.IsNullOrWhiteSpace(val);
            //var val = "";
            //switch (cell.CellType)
            //{
            //    case CellType.Numeric:
            //        if (DateUtil.IsCellDateFormatted(cell))
            //        {
            //            return val == DateTime.FromOADate(cell.NumericCellValue).ToString();
            //            //try
            //            //{
            //            //    return cell.DateCellValue.ToString();
            //            //}
            //            //catch (NullReferenceException)
            //            //{
            //            //    return DateTime.FromOADate(cell.NumericCellValue).ToString();
            //            //}
            //        }
            //        return val == cell.NumericCellValue.ToString();

            //    case CellType.String:
            //        return val == cell.StringCellValue;

            //    case CellType.Boolean:
            //        return val == cell.BooleanCellValue.ToString();

            //    default:
            //        return val == string.Empty;
            //}
        }
    }
}
