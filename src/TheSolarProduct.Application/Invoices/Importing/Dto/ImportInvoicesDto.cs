﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Invoices.Importing.Dto
{
	public class ImportInvoicesDto
	{
		public DateTime? Date { get; set; }
		public string ProjectNo { get; set; }
		public string PayBy { get; set; }
		public string Description { get; set; }
		public string AmountPaid { get; set; }

		public string AllocatedBy { get; set; }
		public string ReceiptNumber { get; set; }
		public string PurchaseNumber { get; set; }
		public string InvoiceNotesDescription { get; set; }
		public string SSCharge { get; set; }
		public string Exception { get; set; }

		public bool CanBeImported()
		{
			return string.IsNullOrEmpty(Exception);
		}

		
	}
}
