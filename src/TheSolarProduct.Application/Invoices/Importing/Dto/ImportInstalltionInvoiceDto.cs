﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Invoices.Importing.Dto
{
    public class ImportInstalltionInvoiceDto
    {
        public string InvoiceNo { get; set; }

        public string JobNumber { get; set; }

        public DateTime? Date { get; set; }

        public string BankRefrenceNo { get; set; }

        public string Remark { get; set; }

        public string Exception { get; set; }

        public bool CanBeImported()
        {
            return string.IsNullOrEmpty(Exception);
        }
    }
}
