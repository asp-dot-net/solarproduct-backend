﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Invoices.Importing.Dto;

namespace TheSolarProduct.Invoices.Importing
{
	public interface ISTCListExcelDataReader : ITransientDependency
	{
		List<ImportSTCDto> GetSTCFromExcel(byte[] fileBytes);
	}
}
