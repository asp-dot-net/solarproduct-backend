﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.ObjectMapping;
using Abp.Threading;
using System;
using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using TheSolarProduct.Storage;
using System.Linq;
using TheSolarProduct.Notifications;
using Abp.Localization;
using Abp.Runtime.Session;
using Abp.UI;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Invoices.Importing.Dto;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Jobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using TheSolarProduct.MultiTenancy;
using System.IO;
using TheSolarProduct.RECDatas;
using TheSolarProduct.Leads;
using NPOI.SS.Formula.Functions;
using System.Text.RegularExpressions;
using Abp.Authorization;
using TheSolarProduct.Authorization;

namespace TheSolarProduct.Invoices.Importing
{
    public class ImportSTCToExcelJob : BackgroundJob<ImportSTCFromExcelJobArgs>, ITransientDependency
    {
        private readonly ISTCListExcelDataReader _stcListExcelDataReader;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IObjectMapper _objectMapper;
        private readonly IAppNotifier _appNotifier;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IInvalidSTCExporter _invalidSTCExporter;
        private readonly IAbpSession _abpSession;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
        private readonly IRepository<InvoicePaymentMethod, int> _lookup_invoicePaymentMethodRepository;
        private readonly IRepository<InvoiceStatus> _invoiceStatusRepository;
        private readonly IRepository<InvoiceImportData> _invoiceImportDataRepository;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<InvoiceFile> _InvoiceFilesRepository;
        private readonly IRepository<RECData> _recRepository;
        private readonly IRepository<Organizations.ExtendOrganizationUnit, long> _extendOrganizationUnitRepository;


        public ImportSTCToExcelJob(
            ISTCListExcelDataReader stcListExcelDataReader,
            IBinaryObjectManager binaryObjectManager,
            IObjectMapper objectMapper,
            IAppNotifier appNotifier,
            IUnitOfWorkManager unitOfWorkManager,
            IInvalidSTCExporter invalidSTCExporter,
            IAbpSession abpSession,
            IRepository<User, long> userRepository,
            IRepository<InvoicePayment> invoicePaymentRepository,
            IRepository<Job> jobRepository,
            IRepository<InvoicePaymentMethod, int> lookup_invoicePaymentMethodRepository,
            IRepository<InvoiceStatus> invoiceStatusRepository,
            IRepository<InvoiceImportData> invoiceImportDataRepository,
            IWebHostEnvironment env,
            IRepository<Tenant> tenantRepository,
            IRepository<InvoiceFile> InvoiceFilesRepository,
            IRepository<RECData> recRepository,
            IRepository<Organizations.ExtendOrganizationUnit, long> extendOrganizationUnitRepository

            )
        {
            _stcListExcelDataReader = stcListExcelDataReader;
            _binaryObjectManager = binaryObjectManager;
            _objectMapper = objectMapper;
            _appNotifier = appNotifier;
            _invalidSTCExporter = invalidSTCExporter;
            _abpSession = abpSession;
            _userRepository = userRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _invoicePaymentRepository = invoicePaymentRepository;
            _jobRepository = jobRepository;
            _lookup_invoicePaymentMethodRepository = lookup_invoicePaymentMethodRepository;
            _invoiceStatusRepository = invoiceStatusRepository;
            _invoiceImportDataRepository = invoiceImportDataRepository;
            _env = env;
            _tenantRepository = tenantRepository;
            _InvoiceFilesRepository = InvoiceFilesRepository;
            _recRepository = recRepository;
            _extendOrganizationUnitRepository = extendOrganizationUnitRepository;
        }

        [UnitOfWork]
        public override void Execute(ImportSTCFromExcelJobArgs args)
        {

            //var fileObject = new BinaryObject(args.TenantId, args.FileBytes);
            //_binaryObjectManager.SaveAsync(fileObject);

            var stcs = GetSTCListFromExcelOrNull(args);
            if (stcs == null || !stcs.Any())
            {
                SendInvalidExcelNotification(args);
                return;
            }
            else
            {
                var invalidstc = new List<ImportSTCDto>();

                foreach (var stc in stcs)
                {
                    if (stc.CanBeImported())
                    {
                        try
                        {
                            AsyncHelper.RunSync(() => CreateInvoicesAsync(stc, args));
                        }
                        catch (UserFriendlyException exception)
                        {
                            invalidstc.Add(stc);
                        }
                        catch (Exception e)
                        {
                            invalidstc.Add(stc);
                        }
                    }
                    else
                    {
                        invalidstc.Add(stc);
                    }
                }

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                    {
                        AsyncHelper.RunSync(() => ProcessImportSTCResultAsync(args, invalidstc));
                    }
                    uow.Complete();
                }
            }
        }

        private List<ImportSTCDto> GetSTCListFromExcelOrNull(ImportSTCFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    try
                    {
                        var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
                        return _stcListExcelDataReader.GetSTCFromExcel(file.Bytes);
                    }
                    catch (Exception e)
                    {
                        return null;
                    }
                    finally
                    {
                        uow.Complete();
                    }
                }
            }
        }

        private async Task CreateInvoicesAsync(ImportSTCDto input, ImportSTCFromExcelJobArgs args)
        {
            //Job lead = new Job();
            //var Suburb = 0;
            //var State = 0;
            //var leadSourceId = 0;
            //RECData recData = new RECData();
            using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
            {
                var recData = await _recRepository.GetAll().Where(e => e.JobNumber.ToUpper() == input.JobNumber.Trim().ToUpper()).FirstOrDefaultAsync();

                if (recData != null)
                {
                    recData.PVDNumber = input.PVDNumber;
                    recData.NoOfPanels = input.NoOfPanels;
                    recData.CreationDate = input.CreationDate;
                    recData.RECCreated = input.RECCreated;
                    recData.RECPassed = input.RECPassed;
                    recData.RECPanding = input.RECPanding;
                    recData.RECFailed = input.RECFailed;
                    recData.RECRegistered = input.RECRegistered;
                    recData.LastAuditedDate = input.LastAuditedDate;
                    recData.BlukUploadId = input.BlukUploadId;
                    recData.Exception = input.Exception;
                    await _recRepository.UpdateAsync(recData);
                }
                else
                {
                    RECData newRECData = new RECData();
                    newRECData.JobNumber = input.JobNumber;
                    newRECData.PVDNumber = input.PVDNumber;
                    newRECData.NoOfPanels = input.NoOfPanels;
                    newRECData.CreationDate = input.CreationDate;
                    newRECData.RECCreated = input.RECCreated;
                    newRECData.RECPassed = input.RECPassed;
                    newRECData.RECPanding = input.RECPanding;
                    newRECData.RECFailed = input.RECFailed;
                    newRECData.RECRegistered = input.RECRegistered;
                    newRECData.LastAuditedDate = input.LastAuditedDate;
                    newRECData.BlukUploadId = input.BlukUploadId;
                    newRECData.Exception = input.Exception;
                    await _recRepository.InsertAsync(newRECData);
                }

                var jobNoCode = _extendOrganizationUnitRepository.GetAll().Where(e => e.Id == args.OrganizationId).Select(e => e.Code).FirstOrDefault();
                var JobNuber = jobNoCode + input.JobNumber;
                var job = await _jobRepository.GetAll().Where(e => e.JobNumber == JobNuber).FirstOrDefaultAsync();
                if (job != null)
                {
                    job.RECPvdNumber = input.PVDNumber.ToUpper();
                    if (input.RECPassed > 0)
                    {
                        job.RECPvdStatus = "Rec Approved";
                    }
                    else if (input.RECPanding > 0)
                    {
                        job.RECPvdStatus = "Rec Pending";
                    }
                    else if (input.RECFailed > 0)
                    {
                        job.RECPvdStatus = "Rec Failed";
                    }
                    job.LastRECUpdated = DateTime.UtcNow;
                    await _jobRepository.UpdateAsync(job);
                }
            }
        }

        private void SendInvalidExcelNotification(ImportSTCFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
                        args.User,
                        new LocalizableString("FileCantBeConvertedToSTC", TheSolarProductConsts.LocalizationSourceName),
                        null,
                        Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }

        private async Task ProcessImportSTCResultAsync(ImportSTCFromExcelJobArgs args,
          List<ImportSTCDto> invalidstc)
        {
            if (invalidstc.Any())
            {
                var file = _invalidSTCExporter.ExportToFile(invalidstc);
                await _appNotifier.SomeSTCCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
            }
            else
            {
                await _appNotifier.SendMessageAsync(
                    args.User,
                    new LocalizableString("AllSTCSuccessfullyImportedFromExcel", TheSolarProductConsts.LocalizationSourceName),
                    null,
                    Abp.Notifications.NotificationSeverity.Success);
            }
        }
    }
}
