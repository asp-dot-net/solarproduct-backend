﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.Invoices.Importing.Dto;

namespace TheSolarProduct.Invoices.Importing
{
	public interface IInvalidSTCExporter
	{
		FileDto ExportToFile(List<ImportSTCDto> stcListDtos);
	}
}
