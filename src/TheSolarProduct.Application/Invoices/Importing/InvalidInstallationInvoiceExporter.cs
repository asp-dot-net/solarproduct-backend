﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Invoices.Importing.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Invoices.Importing
{
    public class InvalidInstallationInvoiceExporter : NpoiExcelExporterBase, ITransientDependency
    {
        public InvalidInstallationInvoiceExporter(ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager)
        {
        }

        public FileDto ExportToFile(List<ImportInstalltionInvoiceDto> stcListDtos)
        {

            return CreateExcelPackage(
         "InvalidSTCS.xlsx",
          excelPackage =>
          {
              var sheet = excelPackage.CreateSheet("InvalidSTC");


              AddHeader(sheet,
                  "InvoiceNo",
                  "JobNumber",
                  "Date",
                  "BankRefrenceNo",
                  "Remark",
                  "Exception"
                  );


              AddObjects(
                    sheet, 2, stcListDtos,
                    _ => _.InvoiceNo,
                    _ => _.JobNumber,
                    _ => _.Date,
                    _ => _.BankRefrenceNo,
                    _ => _.Remark,
                    _ => _.Exception
                );




          });

        }
    }
}
