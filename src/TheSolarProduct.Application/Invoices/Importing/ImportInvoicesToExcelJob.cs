﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.ObjectMapping;
using Abp.Threading;
using System;
using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using TheSolarProduct.Storage;
using System.Linq;
using TheSolarProduct.Notifications;
using Abp.Localization;
using Abp.Runtime.Session;
using Abp.UI;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Invoices.Importing.Dto;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Jobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using TheSolarProduct.MultiTenancy;
using System.IO;
using TheSolarProduct.Common;
using Abp.EntityFrameworkCore.EFPlus;
using Abp.Authorization;
using TheSolarProduct.Authorization;

namespace TheSolarProduct.Invoices.Importing
{
    public class ImportInvoicesToExcelJob : BackgroundJob<ImportInvoicesFromExcelJobArgs>, ITransientDependency
    {
        private readonly IInvoicesListExcelDataReader _invoicesListExcelDataReader;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IObjectMapper _objectMapper;
        private readonly IAppNotifier _appNotifier;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IInvalidInvoicesExporter _invalidInvoicesExporter;
        private readonly IAbpSession _abpSession;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
        private readonly IRepository<InvoicePaymentMethod, int> _lookup_invoicePaymentMethodRepository;
        private readonly IRepository<InvoiceStatus> _invoiceStatusRepository;
        private readonly IRepository<InvoiceImportData> _invoiceImportDataRepository;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<InvoiceFile> _InvoiceFilesRepository;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;


        public ImportInvoicesToExcelJob(
            IInvoicesListExcelDataReader invoicesListExcelDataReader,
            IBinaryObjectManager binaryObjectManager,
            IObjectMapper objectMapper,
            IAppNotifier appNotifier,
            IUnitOfWorkManager unitOfWorkManager,
            IInvalidInvoicesExporter invalidInvoicesExporter,
            IAbpSession abpSession,
            IRepository<User, long> userRepository,
            IRepository<InvoicePayment> invoicePaymentRepository,
            IRepository<Job> jobRepository,
            IRepository<InvoicePaymentMethod, int> lookup_invoicePaymentMethodRepository,
            IRepository<InvoiceStatus> invoiceStatusRepository,
            IRepository<InvoiceImportData> invoiceImportDataRepository,
            IWebHostEnvironment env,
            IRepository<Tenant> tenantRepository,
            IRepository<InvoiceFile> InvoiceFilesRepository,
            ICommonLookupAppService CommonDocumentSaveRepository

            )
        {
            _invoicesListExcelDataReader = invoicesListExcelDataReader;
            _binaryObjectManager = binaryObjectManager;
            _objectMapper = objectMapper;
            _appNotifier = appNotifier;
            _invalidInvoicesExporter = invalidInvoicesExporter;
            _abpSession = abpSession;
            _userRepository = userRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _invoicePaymentRepository = invoicePaymentRepository;
            _jobRepository = jobRepository;
            _lookup_invoicePaymentMethodRepository = lookup_invoicePaymentMethodRepository;
            _invoiceStatusRepository = invoiceStatusRepository;
            _invoiceImportDataRepository = invoiceImportDataRepository;
            _env = env;
            _tenantRepository = tenantRepository;
            _InvoiceFilesRepository = InvoiceFilesRepository;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
        }

        [UnitOfWork]
        public override void Execute(ImportInvoicesFromExcelJobArgs args)
        {

            //var fileObject = new BinaryObject(args.TenantId, args.FileBytes);
            //_binaryObjectManager.SaveAsync(fileObject);

            var invoices = GetInvoicesListFromExcelOrNull(args);
            if (invoices == null || !invoices.Any())
            {
                SendInvalidExcelNotification(args);
                return;
            }
            else
            {
                var invalidinvoice = new List<ImportInvoicesDto>();

                foreach (var invoice in invoices)
                {
                    if (invoice.CanBeImported())
                    {
                        try
                        {
                            AsyncHelper.RunSync(() => CreateInvoicesAsync(invoice, args));
                        }
                        catch (UserFriendlyException exception)
                        {
                            //Lead.Exception = exception.Message;
                            invalidinvoice.Add(invoice);
                        }
                        catch (Exception e)
                        {
                            //Lead.Exception = e.ToString();
                            invalidinvoice.Add(invoice);
                        }
                    }
                    else
                    {
                        invalidinvoice.Add(invoice);
                    }
                }
               
                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                    {
                        AsyncHelper.RunSync(() => ProcessImportInvoicesResultAsync(args, invalidinvoice));
                    }

                    uow.Complete();
                }
            }
        }

        private List<ImportInvoicesDto> GetInvoicesListFromExcelOrNull(ImportInvoicesFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    //Random rnd = new Random();
                   
                    try
                    {
                        var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
                        //System.IO.File.WriteAllBytes("C:/inetpub/wwwroot/" + rnd.ToString() + ".xls", file.Bytes);
                        return _invoicesListExcelDataReader.GetInvoicesFromExcel(file.Bytes);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                    finally
                    {
                        uow.Complete();
                    }
                }
            }
        }

        private async Task CreateInvoicesAsync(ImportInvoicesDto input, ImportInvoicesFromExcelJobArgs args)
        {


            InvoiceImportData invoicepayment = new InvoiceImportData();
            //var suburb = 0;
            //var state = 0;
            var jobid = 0;
            var InvoicePaymentMethodId = 0;
            var tenant = _abpSession.TenantId;
            var sscharge = 0.0M;

            using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
            {
                if (input.SSCharge == null)
                {
                    sscharge = 0;
                }
                else { 
                 sscharge = Convert.ToDecimal(input.SSCharge);

                }

                if (input.ProjectNo != null)
                {
                    jobid = await _jobRepository.GetAll().Where(e => e.JobNumber == input.ProjectNo).Select(e => e.Id).FirstOrDefaultAsync();
                }

                if (jobid == 0)
                {
                    input.Exception = "ProjectNo Not Found";
                    throw new Exception("ProjectNo Not Found");
                }
                if (input.ProjectNo != null)
                {
                     InvoicePaymentMethodId = await _lookup_invoicePaymentMethodRepository.GetAll().Where(e => e.ShortCode == input.PayBy).Select(e => e.Id).FirstOrDefaultAsync();
                }
                if (InvoicePaymentMethodId == 0)
                {
                    input.Exception = "Pay By Not Found";
                    throw new Exception("Pay By Not Found");
                }
              
                invoicepayment.TenantId = (int)args.TenantId;
                invoicepayment.Date = input.Date;
                invoicepayment.JobNumber = input.ProjectNo;
                invoicepayment.JobId = jobid;
                invoicepayment.InvoicePaymentmothodName = input.PayBy;
                invoicepayment.InvoicePaymentMethodId = InvoicePaymentMethodId;
                invoicepayment.Description = input.Description;
                invoicepayment.PaidAmmount = Convert.ToDecimal(input.AmountPaid);
                invoicepayment.AllotedBy = input.AllocatedBy;
                invoicepayment.PurchaseNumber = input.PurchaseNumber;
                invoicepayment.ReceiptNumber = input.ReceiptNumber;
                invoicepayment.InvoiceNotesDescription = input.InvoiceNotesDescription;
                invoicepayment.SSCharge = sscharge;
                invoicepayment.CreatorUserId = (int)args.User.UserId;

                await _invoiceImportDataRepository.InsertAsync(invoicepayment);

                if(!string.IsNullOrEmpty(invoicepayment.ReceiptNumber))
                {
                    var _ip = await _invoicePaymentRepository.GetAll().Where(e => e.ReceiptNumber == invoicepayment.ReceiptNumber && e.InvoicePayTotal == invoicepayment.PaidAmmount && e.JobId == jobid).FirstOrDefaultAsync();
                    
                    if(_ip != null)
                    {
                        _ip.IsVerified = true;
                        await _invoicePaymentRepository.UpdateAsync(_ip);
                    }
                }
            }
        }

        private void SendInvalidExcelNotification(ImportInvoicesFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
                        args.User,
                        new LocalizableString("FileCantBeConvertedToInvoice", TheSolarProductConsts.LocalizationSourceName),
                        null,
                        Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }

        private async Task ProcessImportInvoicesResultAsync(ImportInvoicesFromExcelJobArgs args,
          List<ImportInvoicesDto> invalidinvoices)
        {
            if (invalidinvoices.Any())
            {
                var file = _invalidInvoicesExporter.ExportToFile(invalidinvoices);
                await _appNotifier.SomeLeadsCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);


            }
            else
            {
                await _appNotifier.SendMessageAsync(
                    args.User,
                    new LocalizableString("AllinvoiceSuccessfullyImportedFromExcel", TheSolarProductConsts.LocalizationSourceName),
                    null,
                    Abp.Notifications.NotificationSeverity.Success);

                var ext = ".xls";
                string filename = DateTime.Now.Ticks + "_" + "InvoiceImportFiles" + ext;

                var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(args.FileBytes, filename, "InvoiceImportFiles", 0, 0,args.TenantId);
                InvoiceFile files = new InvoiceFile();
                files.CreatorUserId = (int)args.User.UserId;
                files.TenantId = (int)args.TenantId;
                files.FileName = filepath.filename;
                files.FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\" + "InvoiceImportFiles" + "\\";
                await _InvoiceFilesRepository.InsertAsync(files);
            }
        }

        //private List<ImportInvoicesDto> GetInvoicesListFromExcelOrNull(ImportInvoicesFromExcelJobArgs args)
        //{
        //    using (var uow = _unitOfWorkManager.Begin())
        //    {
        //        using (CurrentUnitOfWork.SetTenantId(args.TenantId))
        //        {
        //            List<ImportInvoicesDto> newList = new List<ImportInvoicesDto>();
        //            try
        //            {
        //                var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
        //                return _invoicesListExcelDataReader.GetInvoicesFromExcel(file.Bytes);
        //            }
        //            catch (Exception ex)
        //            {
        //                newList[0].Exception = ex.ToString();
        //                string path = @"D:\Tejal\Solar_Product\aspnet-core\src\TheSolarProduct.Web.Host\wwwroot\Documents\GetInvoicesListFromExcelOrNull_Test.txt";
        //                // This text is added only once to the file.
        //                if (!File.Exists(path))
        //                {
        //                    // Create a file to write to.
        //                    using (StreamWriter sw = File.CreateText(path))
        //                    {
        //                        sw.WriteLine("=============Error Logging ===========");
        //                        sw.WriteLine("===========Start============= " + DateTime.Now);
        //                        sw.WriteLine(ex.GetType().FullName);
        //                        sw.WriteLine("Message : " + ex.Message);
        //                        sw.WriteLine("StackTrace : " + ex.StackTrace);
        //                        sw.WriteLine("===========End============= " + DateTime.Now);
        //                    }
        //                }

        //                // This text is always added, making the file longer over time
        //                // if it is not deleted.
        //                using (StreamWriter sw = File.AppendText(path))
        //                {
        //                    sw.WriteLine("=============Error Logging ===========");
        //                    sw.WriteLine("===========Start============= " + DateTime.Now);
        //                    sw.WriteLine(ex.GetType().FullName);
        //                    sw.WriteLine("Message : " + ex.Message);
        //                    sw.WriteLine("StackTrace : " + ex.StackTrace);
        //                    sw.WriteLine("===========End============= " + DateTime.Now);

        //                }
        //                return newList;
        //            }
        //            finally
        //            {
        //                uow.Complete();
        //            }
        //        }
        //    }
        //}

        //public string SendInvalidExcelNotification(ImportInvoicesFromExcelJobArgs args)
        //{
        //    string result = "";
        //    using (var uow = _unitOfWorkManager.Begin())
        //    {
        //        try
        //        {
        //            using (CurrentUnitOfWork.SetTenantId(args.TenantId))
        //            {
        //                AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
        //                    args.User,
        //                    new LocalizableString("FileCantBeConvertedToInvoice", TheSolarProductConsts.LocalizationSourceName),
        //                    null,
        //                    Abp.Notifications.NotificationSeverity.Warn));
        //            }
        //            uow.Complete();
        //        }
        //        catch (Exception ex)
        //        {
        //            result = ex.ToString();
        //            string path = @"D:\Tejal\Solar_Product\aspnet-core\src\TheSolarProduct.Web.Host\wwwroot\Documents\SendInvalidExcelNotification_Test.txt";
        //            // This text is added only once to the file.
        //            if (!File.Exists(path))
        //            {
        //                // Create a file to write to.
        //                using (StreamWriter sw = File.CreateText(path))
        //                {
        //                    sw.WriteLine("=============Error Logging ===========");
        //                    sw.WriteLine("===========Start============= " + DateTime.Now);
        //                    sw.WriteLine(ex.GetType().FullName);
        //                    sw.WriteLine("Message : " + ex.Message);
        //                    sw.WriteLine("StackTrace : " + ex.StackTrace);
        //                    sw.WriteLine("===========End============= " + DateTime.Now);
        //                }
        //            }

        //            // This text is always added, making the file longer over time
        //            // if it is not deleted.
        //            using (StreamWriter sw = File.AppendText(path))
        //            {
        //                sw.WriteLine("=============Error Logging ===========");
        //                sw.WriteLine("===========Start============= " + DateTime.Now);
        //                sw.WriteLine(ex.GetType().FullName);
        //                sw.WriteLine("Message : " + ex.Message);
        //                sw.WriteLine("StackTrace : " + ex.StackTrace);
        //                sw.WriteLine("===========End============= " + DateTime.Now);

        //            }
        //        }
        //    }
        //    return result;
        //}
    }
}
