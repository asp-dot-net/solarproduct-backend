﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.Invoices.Importing.Dto;

namespace TheSolarProduct.Invoices.Importing
{
	public interface IInvalidInvoicesExporter
	{
		FileDto ExportToFile(List<ImportInvoicesDto> invoiceListDtos);
	}
}
