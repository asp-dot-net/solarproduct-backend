﻿using System.Collections.Generic;
using TheSolarProduct.Expense.Exporting;
using TheSolarProduct.Dto;
using TheSolarProduct.Expense.Dtos;
using TheSolarProduct.Leads.Dtos;

namespace TheSolarProduct.Expense.Exporting
{
    public interface ILeadExpensesExcelExporter
    {
        FileDto ExportToFile(List<GetLeadExpenseForViewDto> leadExpenses, string filename);
        FileDto ReportExportToFile(List<LeadExpenseReportDto> leadExpenses);
    }
}