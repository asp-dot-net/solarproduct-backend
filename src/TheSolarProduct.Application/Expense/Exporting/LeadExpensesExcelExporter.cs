﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Expense.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.Leads.Dtos;
using System.Linq;
using TheSolarProduct.CallHistory;
using System;
using TheSolarProduct.LeadSources;
using TheSolarProduct.Leads;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.Expense.Exporting
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class LeadExpensesExcelExporter : NpoiExcelExporterBase, ILeadExpensesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public LeadExpensesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetLeadExpenseForViewDto> leadExpenses, string filename)
        {
            return CreateExcelPackage(
               filename,
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("LeadExpenses"));

                    AddHeader(
                        sheet,
                        L("FromDate"),
                        L("ToDate"),
                        L("Amount"),
                        (L("LeadSource")) + L("SourceName")
                        );

                    AddObjects(
                        sheet, 2, leadExpenses,
                        _ => _timeZoneConverter.Convert(_.LeadExpense.FromDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _timeZoneConverter.Convert(_.LeadExpense.ToDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.LeadExpense.Amount,
                        _ => _.LeadSourceSourceName
                        );

                    for (var i = 1; i <= leadExpenses.Count; i++)
                    {
                        SetintCellDataFormat(sheet.GetRow(i).Cells[0], "yyyy-mm-dd");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[1], "yyyy-mm-dd");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[2], "0.00");
                    }

                    //for (var i = 1; i <= leadExpenses.Count; i++)
                    //{
                    //    SetCellDataFormat(sheet.GetRow(i).Cells[1], );
                    //}
                    //sheet.AutoSizeColumn(1); 
                    //for (var i = 1; i <= leadExpenses.Count; i++)
                    //{
                    //    SetCellDataFormat(sheet.GetRow(i).Cells[2], "yyyy-mm-dd");
                    //}
                    //sheet.AutoSizeColumn(2);
                    //for (var i = 1; i <= leadExpenses.Count; i++)
                    //{
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[3], "0.00");
                    //}
                    //sheet.AutoSizeColumn(3);
                });
        }
        public FileDto ReportExportToFile(List<LeadExpenseReportDto> leadExpenses)
        {
            string[] Header = new string[] { "LeadSource", "Total_Spend", "Total_Lead", "Total_CostLead", "Total_KwSold", "Total_CostKW", "Total_ProjOpen", "Total_ProjOpenPer", "Total_DepRec", "Total_DepRecPer", "Total_PanelSold", "Total_CostPanel"
                                                          ,"NSW_Spend", "NSW_Lead", "NSW_CostLead", "NSW_KwSold", "NSW_CostKW", "NSW_ProjOpen", "NSW_ProjOpenPer", "NSW_DepRec", "NSW_DepRecPer", "NSW_PanelSold", "NSW_CostPanel"
                                                          ,"QLD_Spend", "QLD_Lead", "QLD_CostLead", "QLD_KwSold", "QLD_CostKW", "QLD_ProjOpen", "QLD_ProjOpenPer", "QLD_DepRec", "QLD_DepRecPer", "QLD_PanelSold", "QLD_CostPanel"
                                                          ,"WA_Spend", "WA_Lead", "WA_CostLead", "WA_KwSold", "WA_CostKW", "WA_ProjOpen", "WA_ProjOpenPer", "WA_DepRec", "WA_DepRecPer", "WA_PanelSold", "WA_CostPanel"
                                                          ,"SA_Spend", "SA_Lead", "SA_CostLead", "SA_KwSold", "SA_CostKW", "SA_ProjOpen", "SA_ProjOpenPer", "SA_DepRec", "SA_DepRecPer", "SA_PanelSold", "SA_CostPanel"
                                                          ,"VIC_Spend", "VIC_Lead", "VIC_CostLead", "VIC_KwSold", "VIC_CostKW", "VIC_ProjOpen", "VIC_ProjOpenPer", "VIC_DepRec", "VIC_DepRecPer", "VIC_PanelSold", "VIC_CostPanel"};

            List<LeadExpenseReportExportDtos> exportDtos = new List<LeadExpenseReportExportDtos>();
            List<string> leadSourceNames = new List<string>();
            leadSourceNames.Add("Arise Google");
            leadSourceNames.Add("Referral");
            leadSourceNames.Add("TV");
            leadSourceNames.Add("Arise Facebook");
            LeadExpenseReportExportDtos Otherlead = new LeadExpenseReportExportDtos();
            Otherlead.Leadsource = "Others";
            foreach (var item in leadExpenses)
            {
                LeadExpenseReportExportDtos lead = new LeadExpenseReportExportDtos();

                lead.Leadsource = item.Leadsource ;
                foreach (var item2 in item.leadstates)
                {

                    if (leadSourceNames.Contains(item.Leadsource))
                    {
                        if (item2.statename == "Total")
                        {

                            lead.Totaltotallead = item2.totallead;
                            lead.TotaltotalCost = item2.totalCost;
                            lead.TotalCostLead = item2.CostLead;
                            lead.TotalKwSold = item2.KwSold;
                            lead.TotalperKwSold = item2.perKwCost;
                            lead.TotalProjectOpen = item2.ProjectOpen;
                            lead.TotalProjectOpenpercent = item2.ProjectOpenpercent;
                            lead.TotalDeprecCount = item2.DeprecCount;
                            lead.TotalDeprecKWSold = item2.DeprecKWSold;
                            lead.TotalPannelsold = item2.Pannelsold;
                            lead.TotalCostpannel = item2.Costpannel;
                        }
                        if (item2.statename == "NSW")
                        {

                            lead.NSWtotallead = item2.totallead;
                            lead.NSWtotalCost = item2.totalCost;
                            lead.NSWCostLead = item2.CostLead;
                            lead.NSWKwSold = item2.KwSold;
                            lead.NSWperKwSold = item2.perKwCost;
                            lead.NSWProjectOpen = item2.ProjectOpen;
                            lead.NSWProjectOpenpercent = item2.ProjectOpenpercent;
                            lead.NSWDeprecCount = item2.DeprecCount;
                            lead.NSWDeprecKWSold = item2.DeprecKWSold;
                            lead.NSWPannelsold = item2.Pannelsold;
                            lead.NSWCostpannel = item2.Costpannel;
                        }
                        if (item2.statename == "QLD")
                        {

                            lead.QLDtotallead = item2.totallead;
                            lead.QLDtotalCost = item2.totalCost;
                            lead.QLDCostLead = item2.CostLead;
                            lead.QLDKwSold = item2.KwSold;
                            lead.QLDperKwSold = item2.perKwCost;
                            lead.QLDProjectOpen = item2.ProjectOpen;
                            lead.QLDProjectOpenpercent = item2.ProjectOpenpercent;
                            lead.QLDDeprecCount = item2.DeprecCount;
                            lead.QLDDeprecKWSold = item2.DeprecKWSold;
                            lead.QLDPannelsold = item2.Pannelsold;
                            lead.QLDCostpannel = item2.Costpannel;
                        }
                        if (item2.statename == "SA")
                        {

                            lead.SAtotallead = item2.totallead;
                            lead.SAtotalCost = item2.totalCost;
                            lead.SACostLead = item2.CostLead;
                            lead.SAKwSold = item2.KwSold;
                            lead.SAperKwSold = item2.perKwCost;
                            lead.SAProjectOpen = item2.ProjectOpen;
                            lead.SAProjectOpenpercent = item2.ProjectOpenpercent;
                            lead.SADeprecCount = item2.DeprecCount;
                            lead.SADeprecKWSold = item2.DeprecKWSold;
                            lead.SAPannelsold = item2.Pannelsold;
                            lead.SACostpannel = item2.Costpannel;
                        }
                        if (item2.statename == "VIC")
                        {

                            lead.VICtotallead = item2.totallead;
                            lead.VICtotalCost = item2.totalCost;
                            lead.VICCostLead = item2.CostLead;
                            lead.VICKwSold = item2.KwSold;
                            lead.VICperKwSold = item2.perKwCost;
                            lead.VICProjectOpen = item2.ProjectOpen;
                            lead.VICProjectOpenpercent = item2.ProjectOpenpercent;
                            lead.VICDeprecCount = item2.DeprecCount;
                            lead.VICDeprecKWSold = item2.DeprecKWSold;
                            lead.VICPannelsold = item2.Pannelsold;
                            lead.VICCostpannel = item2.Costpannel;
                        }
                        if (item2.statename == "WA")
                        {

                            lead.WAtotallead = item2.totallead;
                            lead.WAtotalCost = item2.totalCost;
                            lead.WACostLead = item2.CostLead;
                            lead.WAKwSold = item2.KwSold;
                            lead.WAperKwSold = item2.perKwCost;
                            lead.WAProjectOpen = item2.ProjectOpen;
                            lead.WAProjectOpenpercent = item2.ProjectOpenpercent;
                            lead.WADeprecCount = item2.DeprecCount;
                            lead.WADeprecKWSold = item2.DeprecKWSold;
                            lead.WAPannelsold = item2.Pannelsold;
                            lead.WACostpannel = item2.Costpannel;
                        }
                    }
                    else
                    {
                        if (item2.statename == "Total")
                        {

                            Otherlead.Totaltotallead = Otherlead.Totaltotallead + item2.totallead;
                            Otherlead.TotaltotalCost = Otherlead.TotaltotalCost + item2.totalCost;
                            Otherlead.TotalCostLead = Otherlead.TotalCostLead + item2.CostLead;
                            Otherlead.TotalKwSold = Otherlead.TotalKwSold + item2.KwSold;
                            Otherlead.TotalperKwSold = Otherlead.TotalperKwSold + item2.perKwCost;
                            Otherlead.TotalProjectOpen = Otherlead.TotalProjectOpen + item2.ProjectOpen;
                            Otherlead.TotalProjectOpenpercent = Otherlead.TotalProjectOpenpercent + item2.ProjectOpenpercent;
                            Otherlead.TotalDeprecCount = Otherlead.TotalDeprecCount + item2.DeprecCount;
                            Otherlead.TotalDeprecKWSold = Otherlead.TotalDeprecKWSold + item2.DeprecKWSold;
                            Otherlead.TotalPannelsold = Otherlead.TotalPannelsold + item2.Pannelsold;
                            Otherlead.TotalCostpannel = Otherlead.TotalCostpannel + item2.Costpannel;
                        }
                        if (item2.statename == "NSW")
                        {
                            Otherlead.NSWtotallead = Otherlead.NSWtotallead + item2.totallead;
                            Otherlead.NSWtotalCost = Otherlead.NSWtotalCost + item2.totalCost;
                            Otherlead.NSWCostLead = Otherlead.NSWCostLead + item2.CostLead;
                            Otherlead.NSWKwSold = Otherlead.NSWKwSold + item2.KwSold;
                            Otherlead.NSWperKwSold = Otherlead.NSWperKwSold + item2.perKwCost;
                            Otherlead.NSWProjectOpen = Otherlead.NSWProjectOpen + item2.ProjectOpen;
                            Otherlead.NSWProjectOpenpercent = Otherlead.NSWProjectOpenpercent + item2.ProjectOpenpercent;
                            Otherlead.NSWDeprecCount = Otherlead.NSWDeprecCount + item2.DeprecCount;
                            Otherlead.NSWDeprecKWSold = Otherlead.NSWDeprecKWSold + item2.DeprecKWSold;
                            Otherlead.NSWPannelsold = Otherlead.NSWPannelsold + item2.Pannelsold;
                            Otherlead.NSWCostpannel = Otherlead.NSWCostpannel + item2.Costpannel;

                        }
                        if (item2.statename == "QLD")
                        {
                            Otherlead.QLDtotallead = Otherlead.QLDtotallead + item2.totallead;
                            Otherlead.QLDtotalCost = Otherlead.QLDtotalCost + item2.totalCost;
                            Otherlead.QLDCostLead = Otherlead.QLDCostLead + item2.CostLead;
                            Otherlead.QLDKwSold = Otherlead.QLDKwSold + item2.KwSold;
                            Otherlead.QLDperKwSold = Otherlead.QLDperKwSold + item2.perKwCost;
                            Otherlead.QLDProjectOpen = Otherlead.QLDProjectOpen + item2.ProjectOpen;
                            Otherlead.QLDProjectOpenpercent = Otherlead.QLDProjectOpenpercent + item2.ProjectOpenpercent;
                            Otherlead.QLDDeprecCount = Otherlead.QLDDeprecCount + item2.DeprecCount;
                            Otherlead.QLDDeprecKWSold = Otherlead.QLDDeprecKWSold + item2.DeprecKWSold;
                            Otherlead.QLDPannelsold = Otherlead.QLDPannelsold + item2.Pannelsold;
                            Otherlead.QLDCostpannel = Otherlead.QLDCostpannel + item2.Costpannel;
                        }
                        if (item2.statename == "SA")
                        {
                            Otherlead.SAtotallead = Otherlead.SAtotallead + item2.totallead;
                            Otherlead.SAtotalCost = Otherlead.SAtotalCost + item2.totalCost;
                            Otherlead.SACostLead = Otherlead.SACostLead + item2.CostLead;
                            Otherlead.SAKwSold = Otherlead.SAKwSold + item2.KwSold;
                            Otherlead.SAperKwSold = Otherlead.SAperKwSold + item2.perKwCost;
                            Otherlead.SAProjectOpen = Otherlead.SAProjectOpen + item2.ProjectOpen;
                            Otherlead.SAProjectOpenpercent = Otherlead.SAProjectOpenpercent + item2.ProjectOpenpercent;
                            Otherlead.SADeprecCount = Otherlead.SADeprecCount + item2.DeprecCount;
                            Otherlead.SADeprecKWSold = Otherlead.SADeprecKWSold + item2.DeprecKWSold;
                            Otherlead.SAPannelsold = Otherlead.SAPannelsold + item2.Pannelsold;
                            Otherlead.SACostpannel = Otherlead.SACostpannel + item2.Costpannel;
                        }
                        if (item2.statename == "VIC")
                        {
                            Otherlead.VICtotallead = Otherlead.VICtotallead + item2.totallead;
                            Otherlead.VICtotalCost = Otherlead.VICtotalCost + item2.totalCost;
                            Otherlead.VICCostLead = Otherlead.VICCostLead + item2.CostLead;
                            Otherlead.VICKwSold = Otherlead.VICKwSold + item2.KwSold;
                            Otherlead.VICperKwSold = Otherlead.VICperKwSold + item2.perKwCost;
                            Otherlead.VICProjectOpen = Otherlead.VICProjectOpen + item2.ProjectOpen;
                            Otherlead.VICProjectOpenpercent = Otherlead.VICProjectOpenpercent + item2.ProjectOpenpercent;
                            Otherlead.VICDeprecCount = Otherlead.VICDeprecCount + item2.DeprecCount;
                            Otherlead.VICDeprecKWSold = Otherlead.VICDeprecKWSold + item2.DeprecKWSold;
                            Otherlead.VICPannelsold = Otherlead.VICPannelsold + item2.Pannelsold;
                            Otherlead.VICCostpannel = Otherlead.VICCostpannel + item2.Costpannel;
                        }
                        if (item2.statename == "WA")
                        {
                            Otherlead.WAtotallead = Otherlead.WAtotallead + item2.totallead;
                            Otherlead.WAtotalCost = Otherlead.WAtotalCost + item2.totalCost;
                            Otherlead.WACostLead = Otherlead.WACostLead + item2.CostLead;
                            Otherlead.WAKwSold = Otherlead.WAKwSold + item2.KwSold;
                            Otherlead.WAperKwSold = Otherlead.WAperKwSold + item2.perKwCost;
                            Otherlead.WAProjectOpen = Otherlead.WAProjectOpen + item2.ProjectOpen;
                            Otherlead.WAProjectOpenpercent = Otherlead.WAProjectOpenpercent + item2.ProjectOpenpercent;
                            Otherlead.WADeprecCount = Otherlead.WADeprecCount + item2.DeprecCount;
                            Otherlead.WADeprecKWSold = Otherlead.WADeprecKWSold + item2.DeprecKWSold;
                            Otherlead.WAPannelsold = Otherlead.WAPannelsold + item2.Pannelsold;
                            Otherlead.WACostpannel = Otherlead.WACostpannel + item2.Costpannel;

                        }

                    }
                }

                if (leadSourceNames.Contains(item.Leadsource))
                {
                    exportDtos.Add(lead);
                }



            }

            exportDtos.Add(Otherlead);

            return CreateExcelPackage(
                "LeadExpenses.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("LeadExpenses"));

                    AddHeader(sheet, Header);

                    AddObjects(
                         sheet, 1, exportDtos,
                        _ => _.Leadsource,
                        _ => _.TotaltotalCost,
                        _ => _.Totaltotallead,
                        _ => _.TotalCostLead,
                        _ => _.TotalKwSold,
                        _ => _.TotalperKwSold,
                        _ => _.TotalProjectOpen,
                        _ => _.TotalProjectOpenpercent,
                        _ => _.TotalDeprecCount,
                        _ => _.TotalDeprecKWSold,
                        _ => _.TotalPannelsold,
                        _ => _.TotalCostpannel,
                        _ => _.NSWtotalCost,
                        _ => _.NSWtotallead,
                        _ => _.NSWCostLead,
                        _ => _.NSWKwSold,
                        _ => _.NSWperKwSold,
                        _ => _.NSWProjectOpen,
                        _ => _.NSWProjectOpenpercent,
                        _ => _.NSWDeprecCount,
                        _ => _.NSWDeprecKWSold,
                        _ => _.NSWPannelsold,
                        _ => _.NSWCostpannel,
                        _ => _.QLDtotalCost,
                        _ => _.QLDtotallead,
                        _ => _.QLDCostLead,
                        _ => _.QLDKwSold,
                        _ => _.QLDperKwSold,
                        _ => _.QLDProjectOpen,
                        _ => _.QLDProjectOpenpercent,
                        _ => _.QLDDeprecCount,
                        _ => _.QLDDeprecKWSold,
                        _ => _.QLDPannelsold,
                        _ => _.QLDCostpannel,
                        _ => _.WAtotalCost,
                        _ => _.WAtotallead,
                        _ => _.WACostLead,
                        _ => _.WAKwSold,
                        _ => _.WAperKwSold,
                        _ => _.WAProjectOpen,
                        _ => _.WAProjectOpenpercent,
                        _ => _.WADeprecCount,
                        _ => _.WADeprecKWSold,
                        _ => _.WAPannelsold,
                        _ => _.WACostpannel,
                        _ => _.SAtotalCost,
                        _ => _.SAtotallead,
                        _ => _.SACostLead,
                        _ => _.SAKwSold,
                        _ => _.SAperKwSold,
                        _ => _.SAProjectOpen,
                        _ => _.SAProjectOpenpercent,
                        _ => _.SADeprecCount,
                        _ => _.SADeprecKWSold,
                        _ => _.SAPannelsold,
                        _ => _.SACostpannel,
                        _ => _.VICtotalCost,
                        _ => _.VICtotallead,
                        _ => _.VICCostLead,
                        _ => _.VICKwSold,
                        _ => _.VICperKwSold,
                        _ => _.VICProjectOpen,
                        _ => _.VICProjectOpenpercent,
                        _ => _.VICDeprecCount,
                        _ => _.VICDeprecKWSold,
                        _ => _.VICPannelsold,
                        _ => _.VICCostpannel

                         );


                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                    int data = 1;

                    for (var i = 1; i <= exportDtos.Count; i++)
                    {
                        for (var j = 1; j <= 66; j++)
                        {
                            if(data==1 || data == 2 || data == 6 || data == 8 || data == 10)
                            {
                                SetintCellDataFormat(sheet.GetRow(i).Cells[j], "00");
                                data++;
                                continue;
                            }
                            if (data == 3 || data == 4 || data == 5 || data == 7 || data == 9)
                            {
                                SetdecimalCellDataFormat(sheet.GetRow(i).Cells[j], "0.00");
                                data++;
                                continue;
                            }
                            if(data == 11)
                            {
                                SetdecimalCellDataFormat(sheet.GetRow(i).Cells[j], "0.00");
                                data = 1;
                                continue;
                            }

                        }
                    }


                });
        }
    }
}
