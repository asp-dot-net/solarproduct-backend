﻿using TheSolarProduct.TheSolarDemo;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Expense.Exporting;
using TheSolarProduct.Expense.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Authorization.Users;
using Microsoft.AspNetCore.Components.Forms;
using System.Text;
using Abp.Notifications;
using TheSolarProduct.Notifications;
using TheSolarProduct;
using TheSolarProduct.LeadSources;
using TheSolarProduct.Leads.Dtos;
using Abp.Organizations;
using TheSolarProduct.States;
using System.Data;
using TheSolarProduct.Leads;
using TheSolarProduct.Jobs;
using TheSolarProduct.Invoices;
using Abp.Timing.Timezone;
using Abp.Domain.Uow;
using TheSolarProduct.Leads.Exporting;
using NPOI.OpenXmlFormats.Wordprocessing;
using System.Reflection;
using TheSolarProduct.DataVaults;
using TheSolarProduct.LeadActions;
using TheSolarProduct.LeadStatuses;
using TheSolarProduct.WholeSaleLeads;
using Telerik.Reporting;
using PayPalCheckoutSdk.Orders;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using Org.BouncyCastle.Crypto;
using Abp.Linq.Extensions;
using NPOI.SS.Formula.Functions;
namespace TheSolarProduct.Expense
{
    public class LeadExpensesAppService : TheSolarProductAppServiceBase, ILeadExpensesAppService
    {
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<LeadExpense> _leadExpenseRepository;
        private readonly ILeadExpensesExcelExporter _leadExpensesExcelExporter;
        private readonly IRepository<LeadSource, int> _lookup_leadSourceRepository;
        private readonly IUserEmailer _userEmailer;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly IAppNotifier _appNotifier;
        private readonly ILeadsExcelExporter _leadsExcelExporter;
        private readonly IRepository<LeadExpenseInvestment> _LeadExpenseInvestmentRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<State, int> _lookup_stateRepository;
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<ProductItem> _ProductItemRepository;
        private readonly IRepository<LeadSourceOrganizationUnit> _leadSourceOrganizationUnitRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IRepository<DataVaultActivityLogHistory> _dataVaultActivityLogHistoryRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public LeadExpensesAppService(IRepository<Lead> leadRepository, IRepository<Job> jobRepository,
                IRepository<LeadExpense> leadExpenseRepository,
                ILeadExpensesExcelExporter leadExpensesExcelExporter, ILeadsExcelExporter leadsExcelExporter,
                IRepository<LeadSource, int> lookup_leadSourceRepository,
                IUserEmailer userEmailer, IRepository<InvoicePayment> invoicePaymentRepository,
                INotificationSubscriptionManager notificationSubscriptionManager,
                IAppNotifier appNotifier, IRepository<LeadExpenseInvestment> LeadExpenseInvestmentRepository,
                IRepository<OrganizationUnit, long> organizationUnitRepository, IRepository<State, int> lookup_stateRepository, ITimeZoneConverter timeZoneConverter, IRepository<JobProductItem> jobProductItemRepository, IRepository<ProductItem> ProductItemRepository
                , IRepository<LeadSourceOrganizationUnit> leadSourceOrganizationUnitRepository
            , IRepository<DataVaultActivityLog> dataVaultActivityLogRepository
            , IRepository<DataVaultActivityLogHistory> dataVaultActivityLogHistoryRepository
            , IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            )
        {
            _jobRepository = jobRepository;
            _leadExpenseRepository = leadExpenseRepository;
            _leadExpensesExcelExporter = leadExpensesExcelExporter;
            _lookup_leadSourceRepository = lookup_leadSourceRepository;
            _userEmailer = userEmailer;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _appNotifier = appNotifier;
            _LeadExpenseInvestmentRepository = LeadExpenseInvestmentRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _lookup_stateRepository = lookup_stateRepository;
            _leadRepository = leadRepository;
            _invoicePaymentRepository = invoicePaymentRepository;
            _timeZoneConverter = timeZoneConverter;
            _jobProductItemRepository = jobProductItemRepository;
            _ProductItemRepository = ProductItemRepository;
            _leadsExcelExporter = leadsExcelExporter;
            _leadSourceOrganizationUnitRepository = leadSourceOrganizationUnitRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dataVaultActivityLogHistoryRepository = dataVaultActivityLogHistoryRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task<PagedResultDto<GetLeadExpenseForViewDto>> GetAll(GetAllLeadExpensesInput input)
        {
            try
            {
                input.FromDateFilter = (_timeZoneConverter.Convert(input.FromDateFilter, (int)AbpSession.TenantId)).Value.Date;
                input.ToDateFilter = (_timeZoneConverter.Convert(input.ToDateFilter, (int)AbpSession.TenantId)).Value.Date;

                input.LeadSourceSourceIdFilter = input.LeadSourceSourceIdFilter != null ? input.LeadSourceSourceIdFilter : 0;
                var leadsourceId = new List<int?>();
                if(input.LeadSourceSourceIdFilter > 0)
                {
                    leadsourceId =  _leadExpenseRepository.GetAll().Where(e => e.LeadSourceId == input.LeadSourceSourceIdFilter).Select(e => e.LeadinvestmentId).ToList();
                }
                var filteredLeadExpenses = _LeadExpenseInvestmentRepository.GetAll()
                .WhereIf(input.FromDateFilter != null && input.ToDateFilter != null, e => (e.FromDate.Date >= input.FromDateFilter.Value.Date && e.ToDate.Date <= input.ToDateFilter.Value.Date) || (e.FromDate.Date <= input.FromDateFilter.Value.Date && e.ToDate.Date >= input.ToDateFilter.Value.Date))
                .WhereIf(input.LeadSourceSourceIdFilter > 0, e => leadsourceId.Contains(e.Id))
                .WhereIf(input.OrganizationId > 0 , e => e.OrganizationId == input.OrganizationId);

                var today = DateTime.Now;
                var firstDayOfMonth = new DateTime(today.Year, today.Month, 1);

                

                decimal? summaryAmount = 0;
                decimal? summaryNSWAmount = 0;
                decimal? summaryQLDAmount = 0;
                decimal? summarySAAmount = 0;
                decimal? summaryWAAmount = 0;
                decimal? summaryVICAmount = 0;

                if (filteredLeadExpenses.Count() > 0)
                {
                    var Ids = filteredLeadExpenses.Select(e => e.Id).ToList();
                    summaryAmount = (decimal?)filteredLeadExpenses.Sum(e => e.Investment);
                    summaryNSWAmount = _leadExpenseRepository.GetAll().Where(e => Ids.Contains((int)e.LeadinvestmentId) && e.StateId == 2).Sum(e => e.Amount);
                    summaryQLDAmount = _leadExpenseRepository.GetAll().Where(e => Ids.Contains((int)e.LeadinvestmentId) && e.StateId == 4).Sum(e => e.Amount);
                    summarySAAmount = _leadExpenseRepository.GetAll().Where(e => Ids.Contains((int)e.LeadinvestmentId) && e.StateId == 5).Sum(e => e.Amount);
                    summaryWAAmount = _leadExpenseRepository.GetAll().Where(e => Ids.Contains((int)e.LeadinvestmentId) && e.StateId == 8).Sum(e => e.Amount);
                    summaryVICAmount = _leadExpenseRepository.GetAll().Where(e => Ids.Contains((int)e.LeadinvestmentId) && e.StateId == 7).Sum(e => e.Amount);
                }
                var leadExpenses = from o in filteredLeadExpenses
                                   join o1 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o1.Id into j1
                                   from o1 in j1.DefaultIfEmpty()

                                   select new GetLeadExpenseForViewDto()
                                   {
                                       FromDate = o.FromDate.Date,
                                       ToDate = o.ToDate.Date,
                                       Amount = (decimal)o.Investment,
                                       Id = o.Id,
                                       Organization = o1.DisplayName,
                                       ForPeriod = o.MonthYear,
                                       IsEdit = o.ToDate.Date < firstDayOfMonth.Date ? false : true,
                                       TotalSummaryAmount = summaryAmount,
                                       TotalNSWAmount = summaryNSWAmount,
                                       TotalQLDAmount = summaryQLDAmount,
                                       TotalSAAmount = summarySAAmount,
                                       TotalWAAmount = summaryWAAmount,
                                       TotalVICAmount = summaryVICAmount,
                                   };

                var pagedAndFilteredLeadExpenses = leadExpenses
                    .OrderBy(input.Sorting ?? "id desc")
                    .PageBy(input);

                var totalCount = await filteredLeadExpenses.CountAsync();

               

                return new PagedResultDto<GetLeadExpenseForViewDto>(
                    totalCount,
                    await pagedAndFilteredLeadExpenses.ToListAsync()
                );
            }
            catch (Exception e) { throw e; }
        }



        public async Task CreateOrEdit(CreateOrEditLeadExpenseDto input)
        {
            System.Diagnostics.Debug.Print(input.FromDate.ToString());

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_LeadExpenses_Create)]
        protected virtual async Task Create(CreateOrEditLeadExpenseDto input)
        {
            var leadExpense = ObjectMapper.Map<LeadExpense>(input);


            if (AbpSession.TenantId != null)
            {
                leadExpense.TenantId = (int?)AbpSession.TenantId;
            }


            //Send EMail to AdminUser
            var adminUser = await UserManager.GetAdminAsync();

            //Notifications
            //await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(adminUser.ToUserIdentifier());
            string msg = string.Format("Lead Expense Rs {0} added for the period {1} to {2}.", leadExpense.Amount, leadExpense.FromDate.ToShortDateString(), leadExpense.ToDate.ToShortDateString());
            await _appNotifier.LeadExpenseAdded(adminUser, msg, NotificationSeverity.Info);

            StringBuilder strinMessagegBuilder = new StringBuilder();
            strinMessagegBuilder.Append(string.Format("<div>Lead Expense : {0} <div>", leadExpense.Amount));

            await _userEmailer.TryToSendLeadExpenseAsync(adminUser.EmailAddress, "Lead Expense added", strinMessagegBuilder.ToString());
            //await _userEmailer.TryToSendLeadExenseEMailToAdminUser(adminUser.EmailAddress,  string.Format("Lead Expense : {0}", leadExpense.Amount));



            await _leadExpenseRepository.InsertAsync(leadExpense);

        }

        [AbpAuthorize(AppPermissions.Pages_LeadExpenses_Edit)]
        protected virtual async Task Update(CreateOrEditLeadExpenseDto input)
        {
            var leadExpense = await _leadExpenseRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, leadExpense);


        }

        [AbpAuthorize(AppPermissions.Pages_LeadExpenses_Delete)]
        public async Task Delete(EntityDto input)
        {
            var ex = await _LeadExpenseInvestmentRepository.GetAsync(input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 2;
            dataVaultLog.ActionNote = "Lead Expense Deleted : " + ex.FromDate.ToString("dd-MM-yyyy") + " to " + ex.ToDate.ToString("dd-MM-yyyy");

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _LeadExpenseInvestmentRepository.DeleteAsync(input.Id);

            var deletedt = _leadExpenseRepository.GetAll().Where(e => e.LeadinvestmentId == input.Id).ToList();
            foreach (var ddata in deletedt)
            {
                await _leadExpenseRepository.DeleteAsync(ddata);
            }
        }

        public async Task<FileDto> GetLeadExpensesToExcel(GetAllLeadExpensesForExcelInput input)
        {
            input.FromDateFilter = (_timeZoneConverter.Convert(input.FromDateFilter, (int)AbpSession.TenantId)).Value.Date;
            input.ToDateFilter = (_timeZoneConverter.Convert(input.ToDateFilter, (int)AbpSession.TenantId)).Value.Date;

            var filteredLeadExpenses = _leadExpenseRepository.GetAll()
                        .Include(e => e.LeadSourceFk)
                        .WhereIf(input.FromDateFilter != null && input.ToDateFilter != null, e => (e.LeadinvestmentFk.FromDate.Date >= input.FromDateFilter.Value.Date && e.LeadinvestmentFk.ToDate.Date <= input.ToDateFilter.Value.Date) || (e.LeadinvestmentFk.FromDate.Date <= input.FromDateFilter.Value.Date && e.LeadinvestmentFk.ToDate.Date >= input.ToDateFilter.Value.Date))
                .WhereIf(input.LeadSourceSourceIdFilter > 0, e => e.LeadSourceId == input.LeadSourceSourceIdFilter)
                .WhereIf(input.OrganizationId > 0, e => e.LeadinvestmentFk.OrganizationId == input.OrganizationId);

            var query = (from o in filteredLeadExpenses
                         join o1 in _lookup_leadSourceRepository.GetAll() on o.LeadSourceId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         select new GetLeadExpenseForViewDto()
                         {
                             LeadExpense = new LeadExpenseDto
                             {
                                 FromDate = o.FromDate,
                                 ToDate = o.ToDate,
                                 Amount = o.Amount,
                                 Id = o.Id
                             },
                             LeadSourceSourceName = s1 == null || s1.Name == null ? "" : s1.Name.ToString()
                         });


            var leadExpenseListDtos = await query.ToListAsync();

            return _leadExpensesExcelExporter.ExportToFile(leadExpenseListDtos, input.ExcelOrCSV);
        }


        [AbpAuthorize(AppPermissions.Pages_LeadExpenses)]
        public async Task<List<LeadExpenseLeadSourceLookupTableDto>> GetAllLeadSourceForTableDropdown()
        {

            return await _lookup_leadSourceRepository.GetAll()
                .Select(leadSource => new LeadExpenseLeadSourceLookupTableDto
                {
                    Id = leadSource.Id,
                    DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
                }).ToListAsync();
        }

        public async Task CreateExpense(List<CreateOrEditLeadExpenseDto> input)
        {
            try
            {
                bool first = true;
                var investid = 0;
                foreach (var item in input)
                {
                    item.FromDate = (DateTime)(_timeZoneConverter.Convert(item.FromDate, (int)AbpSession.TenantId)).Value.Date;
                    item.ToDate = (DateTime)(_timeZoneConverter.Convert(item.ToDate, (int)AbpSession.TenantId)).Value.Date;

                    int daydiff = (int)((item.ToDate.Date - item.FromDate.Date.AddDays(-1)).TotalDays);

                    if (first)
                    {
                        LeadExpenseInvestment objleadinvest = new LeadExpenseInvestment();
                        if (AbpSession.TenantId != null)
                        {
                            objleadinvest.TenantId = (int?)AbpSession.TenantId;
                        }
                        objleadinvest.OrganizationId = (int)item.organizationId;
                        objleadinvest.FromDate = item.FromDate.Date;
                        objleadinvest.ToDate = item.ToDate;
                        objleadinvest.MonthYear = item.ToDate.ToString("MMMM") + "-" + item.ToDate.Year.ToString();
                        objleadinvest.Investment = (float)item.TotalAmount;
                        investid = _LeadExpenseInvestmentRepository.InsertAndGetId(objleadinvest);
                        first = false;

                        var dataVaultLog = new DataVaultActivityLog();
                        dataVaultLog.Action = "Created";
                        dataVaultLog.SectionId = 1;
                        dataVaultLog.ActionNote = "Lead Expence Created : " + item.FromDate.ToString("dd-MM-yyyy") + " to " + item.ToDate.ToString("dd-MM-yyyy");
                        await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

                    }

                    List<statedetail> stdetail = new List<statedetail>();
                    statedetail Nsw = new statedetail();
                    Nsw.StateId = 2;
                    Nsw.Amount = item.NswAmount;
                    Nsw.dailyamount = item.NswAmount / daydiff;
                    stdetail.Add(Nsw);
                    statedetail qld = new statedetail();
                    qld.StateId = 4;
                    qld.Amount = item.QldAmount;
                    qld.dailyamount = item.QldAmount / daydiff;
                    stdetail.Add(qld);
                    statedetail sa = new statedetail();
                    sa.StateId = 5;
                    sa.Amount = item.SaAmount;
                    sa.dailyamount = item.SaAmount / daydiff;
                    stdetail.Add(sa);
                    statedetail wa = new statedetail();
                    wa.StateId = 8;
                    wa.Amount = item.WaAmount;
                    wa.dailyamount = item.WaAmount / daydiff;
                    stdetail.Add(wa);
                    statedetail vic = new statedetail();
                    vic.StateId = 7;
                    vic.Amount = item.VICAmount;
                    vic.dailyamount = item.VICAmount / daydiff;
                    stdetail.Add(vic);


                    for (int i = 0; i < stdetail.Count; i++)
                    {
                        LeadExpense obj = new LeadExpense();
                        obj.StateId = stdetail[i].StateId;
                        obj.Amount = stdetail[i].Amount;
                        obj.dailyamount = stdetail[i].dailyamount;
                        obj.LeadSourceId = item.LeadSourceId;
                        obj.FromDate = item.FromDate;
                        obj.ToDate = item.ToDate;
                        obj.LeadinvestmentId = investid;
                        if (AbpSession.TenantId != null)
                        {
                            obj.TenantId = (int?)AbpSession.TenantId;
                        }

                        _leadExpenseRepository.Insert(obj);
                    }

                }
            }
            catch (Exception e) { throw e; }

        }
        [AbpAuthorize(AppPermissions.Pages_LeadExpenses_Edit)]
        public async Task<GetLeadExpenseForEditOutput> GetLeadExpenseForEdit(EntityDto input)
        {
            var leadExpense = await _leadExpenseRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLeadExpenseForEditOutput { LeadExpense = ObjectMapper.Map<CreateOrEditLeadExpenseDto>(leadExpense) };

            if (output.LeadExpense.LeadSourceId != null)
            {
                var _lookupLeadSource = await _lookup_leadSourceRepository.FirstOrDefaultAsync((int)output.LeadExpense.LeadSourceId);
                output.LeadSourceSourceName = _lookupLeadSource?.Name?.ToString();
                //output.LeadExpense.DateRange  = output.LeadExpense.FromDate.ToString("d") + " - " + output.LeadExpense.ToDate.ToString("d");
            }

            return output;
        }
        
        public async Task<List<LeadExpenseAddDto>> getAllexpenseTableForEdit(int investmentid)
        {
            var entity = _lookup_stateRepository.GetAll().Where(e => e.IsReport == true).OrderBy(p => p.Id).ToList();
            var activity = _lookup_leadSourceRepository.GetAll().OrderBy(p => p.Id).ToList();
            DataTable dt = new DataTable();
            DataRow dr;
            dt.Columns.Add("InvestmentId");
            dt.Columns.Add("FromDate");
            dt.Columns.Add("ToDate");
            dt.Columns.Add("LeadSource");
            dt.Columns.Add("LeadSourceId");
            //dt.Columns.Add("stateId");
            //dt.Columns.Add("EntitiesId");
            for (int i = 0; i < entity.Count; i++)
            {
                dt.Columns.Add(entity[i].Name);
                dt.Columns.Add(entity[i].Name + "," + entity[i].Id);
            }

            foreach (var item in activity)
            {
                dr = dt.NewRow();
                string entcode = "";
                DateTime fromdate = DateTime.Now;
                DateTime todate = DateTime.Now;
                var entitydata = (from Pa in _leadExpenseRepository.GetAll()
                                  join ent in _lookup_stateRepository.GetAll() on Pa.StateId equals ent.Id into entJoined
                                  from ent in entJoined.DefaultIfEmpty()
                                  where (Pa.LeadSourceId == item.Id && Pa.LeadinvestmentId == investmentid)
                                  select new { Pa.StateId, ent.Name, Pa.LeadSourceId, Pa.LeadinvestmentId, Pa.FromDate, Pa.ToDate }).Distinct().ToList();

                for (int j = 0; j < entitydata.Count; j++)
                {
                    entcode += entitydata[j].LeadinvestmentId;
                    fromdate = entitydata[j].FromDate;
                    todate = entitydata[j].ToDate;
                    break;
                }

                dr["InvestmentId"] = entcode;
                dr["FromDate"] = fromdate;
                dr["ToDate"] = todate;
                dr["LeadSourceId"] = item.Id;
                dr["LeadSource"] = item.Name;

                for (int i = 0; i < entity.Count; i++)
                {
                    var categoryid = entity[i].Id;
                    var linkdata = (from us in _lookup_leadSourceRepository.GetAll()
                                    join Pa in _leadExpenseRepository.GetAll() on us.Id equals Pa.LeadSourceId into PaJoined
                                    from Pa in PaJoined.DefaultIfEmpty()
                                    where (Pa.StateId == categoryid && Pa.LeadSourceId == item.Id)
                                    select new { stid = Pa.StateId, amount = Pa.Amount, leadname = us.Name, leadid = Pa.Id }).FirstOrDefault();
                    dr[entity[i].Name] = linkdata.amount;
                    dr[entity[i].Name + "," + entity[i].Id] = linkdata.leadid;
                }
                dt.Rows.Add(dr);
            }
            List<LeadExpenseAddDto> leadlist = new List<LeadExpenseAddDto>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                LeadExpenseAddDto statewiselist = new LeadExpenseAddDto();
                statewiselist.InvestmentId = Convert.ToInt32(dt.Rows[i]["InvestmentId"]);
                statewiselist.LeadSourceId = Convert.ToInt32(dt.Rows[i]["LeadSourceId"]);
                statewiselist.Leadsource = dt.Rows[i]["LeadSource"].ToString();
                statewiselist.FromDate = Convert.ToDateTime(dt.Rows[i]["FromDate"]);
                statewiselist.ToDate = Convert.ToDateTime(dt.Rows[i]["ToDate"]);
                List<StateForLeadExpenseDto> Statelist = new List<StateForLeadExpenseDto>();
                for (int j = 5; j < dt.Columns.Count - 1; j++)
                {
                    StateForLeadExpenseDto st = new StateForLeadExpenseDto();
                    st.Amount = Convert.ToDecimal(dt.Rows[i][j]);
                    st.stateid = Convert.ToInt32(dt.Rows[i][j + 1]);
                    j++;
                    Statelist.Add(st);
                }
                statewiselist.leadstates = Statelist;
                leadlist.Add(statewiselist);
            }
            return leadlist;
        }

        public async Task<List<CreateOrEditLeadExpenseDto>> getAllexpenseTableFor_Edit(DateTime frmdate, int investmentid)
        {
            //DateTime fromdt = frmdate.AddDays(1);
            
            List<CreateOrEditLeadExpenseDto> leadlist = new List<CreateOrEditLeadExpenseDto>();
            
            var leadexp = _LeadExpenseInvestmentRepository.GetAll().Where(e => e.Id == investmentid).FirstOrDefault();
            //var leadexp = _LeadExpenseInvestmentRepository.GetAll().Where(e => e.FromDate.Month == fromdt.Month && e.FromDate.Year == fromdt.Year && e.Id == investmentid).FirstOrDefault();

            //var leadsourcedata = _leadExpenseRepository.GetAll().Where(e => e.FromDate.Month == fromdt.Month && e.FromDate.Year == fromdt.Year && e.LeadinvestmentId == investmentid).Select(e => e.LeadSourceId).Distinct().ToList();

            var leadsourcedata = _leadExpenseRepository.GetAll().Where(e => e.LeadinvestmentId == investmentid).Select(e => e.LeadSourceId).Distinct().ToList();

            foreach (var item in leadsourcedata)
            {
                CreateOrEditLeadExpenseDto leaddt = new CreateOrEditLeadExpenseDto();

                //var leaddata = _leadExpenseRepository.GetAll().Where(e => e.FromDate.Month == fromdt.Month && e.FromDate.Year == fromdt.Year && e.LeadinvestmentId == investmentid && e.LeadSourceId == item).ToList();
                
                var leaddata = _leadExpenseRepository.GetAll().Where(e => e.LeadinvestmentId == investmentid && e.LeadSourceId == item).ToList();
                //leaddt.TotalNswAmount = leaddata.Where(e => e.Id == 2).Sum(e => e.Amount);
                //leaddt.TotalSAAmount = leaddata.Where(e => e.Id == 5).Sum(e => e.Amount);
                //leaddt.TotalWAAmount = leaddata.Where(e => e.Id == 8).Sum(e => e.Amount);
                //leaddt.TotalQLDAmount = leaddata.Where(e => e.Id == 4).Sum(e => e.Amount);
                //leaddt.TotalVICAmount = leaddata.Where(e => e.Id == 7).Sum(e => e.Amount);

                foreach (var dt in leaddata)
                {
                    leaddt.LeadSourceId = dt.LeadSourceId;
                    leaddt.LeadExpenseId = dt.LeadinvestmentId;
                    leaddt.TotalAmount = (decimal)leadexp.Investment;
                    leaddt.organizationId = leadexp.OrganizationId;
                    if (dt.StateId == 2) { leaddt.NswAmount = dt.Amount; leaddt.NswId = dt.Id; }
                    if (dt.StateId == 4) { leaddt.QldAmount = dt.Amount; leaddt.QldId = dt.Id; }
                    if (dt.StateId == 5) { leaddt.SaAmount = dt.Amount; leaddt.SaId = dt.Id; }
                    if (dt.StateId == 8) { leaddt.WaAmount = dt.Amount; leaddt.WaId = dt.Id; }
                    if (dt.StateId == 7) { leaddt.VICAmount = dt.Amount; leaddt.VICId = dt.Id; }
                    leaddt.Id = dt.Id;
                }
                leadlist.Add(leaddt);
            }

            return leadlist;
        }
        public async Task<TotalCountDto> getTotalCount(int investmentid)
        {
            //var output = new TotalCountDto();
            var output = (from o in _LeadExpenseInvestmentRepository.GetAll()
                          where (o.Id == investmentid)                                                 

                          select new TotalCountDto
                          {
                              TotalAmount = (decimal)o.Investment,
                              FromDate = o.FromDate,
                              ToDate = o.ToDate,
                              TotalNswAmount = _leadExpenseRepository.GetAll().Where(e => e.LeadinvestmentId == o.Id && e.StateId == 2).Sum(e => e.Amount),
                              TotalQLDAmount = _leadExpenseRepository.GetAll().Where(e => e.LeadinvestmentId == o.Id && e.StateId == 4).Sum(e => e.Amount),
                              TotalVICAmount = _leadExpenseRepository.GetAll().Where(e => e.LeadinvestmentId == o.Id && e.StateId == 7).Sum(e => e.Amount),
                              TotalSAAmount = _leadExpenseRepository.GetAll().Where(e => e.LeadinvestmentId == o.Id && e.StateId == 5).Sum(e => e.Amount),
                              TotalWAAmount = _leadExpenseRepository.GetAll().Where(e => e.LeadinvestmentId == o.Id && e.StateId == 8).Sum(e => e.Amount),

                          }).FirstOrDefault();

            //output.TotalAmount = (decimal)(_LeadExpenseInvestmentRepository.GetAll().Where(e => e.Id == investmentid).Select(e => e.Investment).FirstOrDefault());

            return output;
        }

        public async Task EditExpense(List<LeadExpenseAddDto> input)
        {
            bool first = true;
            var investid = 0;
           
            foreach (var item in input)
            {
                var SDate = (_timeZoneConverter.Convert(item.FromDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(item.ToDate, (int)AbpSession.TenantId));
                if (first)
                {

                    //LeadExpenseInvestment objleadinvest = new LeadExpenseInvestment();
                    var objleadinvest = _LeadExpenseInvestmentRepository.GetAll().Where(e => e.Id == item.InvestmentId).FirstOrDefault();
                    

                   

                    if (AbpSession.TenantId != null)
                    {
                        objleadinvest.TenantId = (int?)AbpSession.TenantId;
                    }
                    objleadinvest.OrganizationId = (int)item.OrganizationId;
                    objleadinvest.FromDate = (DateTime)SDate;
                    objleadinvest.ToDate = item.ToDate;
                    objleadinvest.MonthYear = ((DateTime)SDate).ToString("MMMM") + "-" + ((DateTime)SDate).Year.ToString();
                    objleadinvest.Investment = (float)item.TotalAmount;
                    _LeadExpenseInvestmentRepository.Update(objleadinvest);
                    first = false;
                }
                foreach (var st in item.leadstates)
                {
                    //LeadExpense obj = new LeadExpense();
                    var obj = _leadExpenseRepository.GetAll().Where(e => e.Id == st.stateid).FirstOrDefault();
                    //obj.StateId = st.stateid;
                    obj.Amount = st.Amount;
                    obj.LeadSourceId = item.LeadSourceId;
                    obj.FromDate = (DateTime)SDate;
                    obj.ToDate = item.ToDate;
                    obj.LeadinvestmentId = item.InvestmentId;
                    if (AbpSession.TenantId != null)
                    {
                        obj.TenantId = (int?)AbpSession.TenantId;
                    }

                    _leadExpenseRepository.Update(obj);
                }
            }
        }

        public async Task<List<LeadExpenseReportDto>> getAllexpenseTableForReport(GetAllexpenseInput input)
        {
            try
            {
                var fromdate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var edate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

                int daydiff = (int)((Convert.ToDateTime(edate) - Convert.ToDateTime(fromdate)).TotalDays);
                var coltotallead = 0;
                decimal coltotalCost = 0;
                decimal colCostLead = 0;
                decimal colKwSold = 0;
                decimal colBatteryKwSold = 0;
                decimal colInstallKwSold = 0;

                decimal colperKwCost = 0;
                decimal colperInstallKwCost = 0;


                decimal colProjectOpen = 0;

                decimal colProjectOpenpercent = 0;
                decimal colDeprecCount = 0;
                decimal colDeprecKWSold = 0;
                decimal colPannelsold = 0;
                decimal colCostpannel = 0;
                var rowtotallead = 0;
                decimal rowtotalCost = 0;
                decimal rowCostLead = 0;
                decimal rowKwSold = 0;

                decimal rowperKwCost = 0;
                decimal rowProjectOpen = 0;

                decimal rowProjectOpenpercent = 0;
                decimal rowDeprecCount = 0;
                decimal rowDeprecKWSold = 0;
                decimal rowPannelsold = 0;
                decimal rowCostpannel = 0;

                decimal spend = 0;
                decimal kws = 0;
                decimal project = 0;
                decimal depositerec = 0;
                decimal pannelsold = 0;
                decimal spenddt = 0;
                decimal installkws = 0;

                var orgnize = _leadSourceOrganizationUnitRepository.GetAll()
                    .WhereIf(input.leadSources != null && input.leadSources.Count() > 0, e => input.leadSources.Contains((int)e.LeadSourceId))
                    .Where(e => e.OrganizationUnitId == input.organizationId).Select(e => e.LeadSourceId).ToList();

                var total = input.states != null &&  input.states.Contains(0);
                var entity = _lookup_stateRepository.GetAll()
                    .WhereIf(input.states != null && input.states.Count() > 0, e => input.states.Contains((int)e.Id))
                    .Where(e => e.IsReport == true).OrderBy(p => p.Id).ToList();

                var activity = _lookup_leadSourceRepository.GetAll()
                     .WhereIf(input.leadSources != null && input.leadSources.Count() > 0, e => input.leadSources.Contains((int)e.Id))
                     .Where(e => orgnize.Contains(e.Id))
                     .OrderBy(p => p.Id).ToList();

                var Areas = new List<string>();
                if (input.areaFilter == "All")
                {
                    Areas.Add("");
                    Areas.Add("Metro");
                    Areas.Add("Regional");
                }
                else
                {
                    Areas.Add(input.areaFilter);
                }

                var stateList = entity.Select(e=>e.Name).ToList();
                var stateIdList = entity.Select(e=>e.Id).ToList();
                List<LeadExpenseReportDto> leadlist1 = new List<LeadExpenseReportDto>();
                foreach (var item in activity)
                {
                    LeadExpenseReportDto statewiselist = new LeadExpenseReportDto();

                    statewiselist.Leadsource = item.Name.ToString();
                    coltotallead = 0;
                    coltotalCost = 0;
                    colCostLead = 0;
                    colKwSold = 0;
                    colInstallKwSold = 0;
                    colperInstallKwCost = 0;
                    colperKwCost = 0;
                    colProjectOpen = 0;
                    colProjectOpenpercent = 0;
                    colDeprecCount = 0;
                    colDeprecKWSold = 0;
                    colPannelsold = 0;
                    colCostpannel = 0;
                    int firstdt = 0;

                    spend = 0;
                    kws = 0;
                    project = 0;
                    depositerec = 0;
                    pannelsold = 0;
                    spenddt = 0;
                    installkws = 0;
                    decimal colbatteyKW = 0;
                    decimal colbatteyKW_installed = 0;
                    decimal batteyKW = 0;
                    decimal batteyKW_installed = 0;

                    List<StateForLeadExpenseReportDto> Statelist = new List<StateForLeadExpenseReportDto>();
                    if (total == true)
                    {
                        //coltotallead = (_leadRepository.GetAll().Where(e => e.CreationTime.AddHours(10).Date >= fromdate.Value.Date && e.CreationTime.AddHours(10).Date <= edate.Value.Date && e.LeadSourceId == item.Id && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.organizationId).Select(e => e.Id).Count());
                        coltotallead = (_leadRepository.GetAll().Where(e => e.CreationTime.AddHours(10).Date >= fromdate.Value.Date && e.CreationTime.AddHours(10).Date <= edate.Value.Date && e.LeadSourceId == item.Id && e.HideDublicate != true && e.OrganizationId == input.organizationId && e.AssignToUserID != null && Areas.Contains(e.Area != null ? e.Area : "")).Select(e => e.Id).Count());
                        //coltotallead = (_leadRepository.GetAll().Where(e => e.CreationTime.AddHours(10).Date >= fromdate.Value.Date && e.CreationTime.AddHours(10).Date <= edate.Value.Date && e.LeadSourceId == item.Id && e.HideDublicate != true && e.OrganizationId == input.organizationId && stateList.Contains(e.State) && e.AssignToUserID != null).Select(e => e.Id).Count());
                        coltotalCost = ((from us in _lookup_leadSourceRepository.GetAll()
                                         join Pa in _leadExpenseRepository.GetAll().Where(le => le.LeadinvestmentFk.OrganizationId == input.organizationId) on us.Id equals Pa.LeadSourceId into PaJoined
                                         from Pa in PaJoined.DefaultIfEmpty()
                                         where (Pa.LeadSourceId == item.Id && Pa.FromDate.Date >= fromdate.Value.Date && Pa.ToDate.Date <= edate)
                                         //&& stateIdList.Contains((int)Pa.StateId)
                                         // select (Pa.dailyamount)).Sum()) * daydiff;
                                         select (Pa.Amount)).Sum());
                        colCostLead = coltotalCost > 0 && coltotallead > 0 ? Math.Round(coltotalCost / coltotallead, 2) : 0;
                        //colKwSold = Convert.ToDecimal((from o in _jobRepository.GetAll()
                        //                               join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                        //                               from i in j1
                        //                               join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
                        //                               from pay in inv1
                        //                               join propay in _jobProductItemRepository.GetAll() on o.Id equals propay.JobId into inv2
                        //                               from propay in inv2
                        //                               join pnnl in _ProductItemRepository.GetAll() on propay.ProductItemId equals pnnl.Id into pnnls
                        //                               from pnnl in pnnls
                        //                               where (o.DepositeRecceivedDate.Value.AddHours(10).Date >= fromdate.Value.Date && o.DepositeRecceivedDate.Value.AddHours(10).Date <= edate.Value.Date && i.LeadSourceId == item.Id && pnnl.ProductTypeId == 1 && i.OrganizationId == input.organizationId)
                        //                               select o.SystemCapacity).Sum());

                        //colKwSold = Convert.ToDecimal((from o in _jobRepository.GetAll()
                        //                               join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                        //                               from i in j1
                        //                                   //join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
                        //                                   //from pay in inv1
                        //                               where (o.FirstDepositDate.Value.Date >= fromdate.Value.Date && o.FirstDepositDate.Value.Date <= edate.Value.Date && i.LeadSourceId == item.Id && i.OrganizationId == input.organizationId
                        //                                //&& stateList.Contains(i.State)
                        //                                && Areas.Contains(i.Area != null ? i.Area : "")
                        //                               )
                        //                               select (o.SystemCapacity)).Sum());

                        if (input.WithBattery)
                        {
                            colbatteyKW = Convert.ToDecimal(_jobProductItemRepository.GetAll().Where(e => e.JobFk.LeadFk.LeadSourceId == item.Id && e.JobFk.LeadFk.OrganizationId == input.organizationId && Areas.Contains(e.JobFk.LeadFk.Area ?? "") && e.ProductItemFk.ProductTypeId == 5)
                                                    .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.JobFk.DepositeRecceivedDate.Value.Date >= fromdate.Value.Date)
                                                    .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.JobFk.DepositeRecceivedDate.Value.Date <= edate.Value.Date)
                                                    .WhereIf(input.DateType == "FirstDeposit" && input.StartDate != null, e => e.JobFk.FirstDepositDate.Value.Date >= fromdate.Value.Date)
                                                    .WhereIf(input.DateType == "FirstDeposit" && input.EndDate != null, e => e.JobFk.FirstDepositDate.Value.Date <= edate.Value.Date)
                                                    .Select(e => e.Quantity * e.ProductItemFk.Size)
                                                    .Sum());

                            colbatteyKW_installed = Convert.ToDecimal(_jobProductItemRepository.GetAll().Where(e => e.JobFk.LeadFk.LeadSourceId == item.Id && e.JobFk.LeadFk.OrganizationId == input.organizationId && Areas.Contains(e.JobFk.LeadFk.Area ?? "") && e.ProductItemFk.ProductTypeId == 5)
                                                    .WhereIf(input.StartDate != null, e => e.JobFk.InstallationDate.Value.Date >= fromdate.Value.Date)
                                                    .WhereIf(input.EndDate != null, e => e.JobFk.InstallationDate.Value.Date <= edate.Value.Date)
                                                    .Select(e => e.Quantity * e.ProductItemFk.Size)
                                                    .Sum());
                        }

                        colKwSold = Convert.ToDecimal((from o in _jobRepository.GetAll()
                                                       join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                                       from i in j1
                                                       where (i.LeadSourceId == item.Id
                                                              && i.OrganizationId == input.organizationId
                                                              && Areas.Contains(i.Area ?? ""))
                                                       select o)
                                                    .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= fromdate.Value.Date)
                                                    .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e =>  e.DepositeRecceivedDate.Value.Date <= edate.Value.Date)
                                                    .WhereIf(input.DateType == "FirstDeposit" && input.StartDate != null, e =>  e.FirstDepositDate.Value.Date >= fromdate.Value.Date)
                                                    .WhereIf(input.DateType == "FirstDeposit" && input.EndDate != null, e =>  e.FirstDepositDate.Value.Date <= edate.Value.Date)
                                                    .Select(e => e.SystemCapacity)
                                                    .Sum()) + colbatteyKW;

                        

                        colperKwCost = coltotalCost > 0 && colKwSold > 0 ? Math.Round(coltotalCost / colKwSold, 2) : 0;
                        colProjectOpen = ((from o in _jobRepository.GetAll()
                                           join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                           from i in j1
                                           where (o.CreationTime.AddHours(10).Date >= fromdate.Value.Date && o.CreationTime.AddHours(10).Date <= edate.Value.Date && i.LeadSourceId == item.Id && i.OrganizationId == input.organizationId
                                            // && stateList.Contains(i.State)
                                            && Areas.Contains(i.Area != null ? i.Area : "")
                                           )
                                           select (o.Id)).Count());
                        colProjectOpenpercent = colProjectOpen > 0 && coltotallead > 0 ? Math.Round(Convert.ToDecimal(colProjectOpen / coltotallead * 100), 2) : 0;

                        //colDeprecCount = ((from o in _jobRepository.GetAll()
                        //                   join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                        //                   from i in j1
                        //                       //join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
                        //                       //from pay in inv1
                        //                   where (o.DepositeRecceivedDate.Value.Date >= fromdate.Value.Date && o.DepositeRecceivedDate.Value.Date <= edate.Value.Date && i.LeadSourceId == item.Id && i.OrganizationId == input.organizationId
                        //                    // && stateList.Contains(i.State)
                        //                    && Areas.Contains(i.Area != null ? i.Area : "")
                        //                   )
                        //                   select (o.Id)).Count());

                        colDeprecCount = (from o in _jobRepository.GetAll()
                                          join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                          from i in j1
                                          where (i.LeadSourceId == item.Id
                                                 && i.OrganizationId == input.organizationId
                                                 && Areas.Contains(i.Area ?? ""))select o)
                  
                    .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= fromdate.Value.Date)
                    .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= edate.Value.Date)
                    .WhereIf(input.DateType == "FirstDeposit" && input.StartDate != null, e => e.FirstDepositDate.Value.Date >= fromdate.Value.Date)
                    .WhereIf(input.DateType == "FirstDeposit" && input.EndDate != null, e => e.FirstDepositDate.Value.Date <= edate.Value.Date)
                  .Count();

                        colDeprecKWSold = colDeprecCount > 0 && coltotallead > 0 ? Math.Round(Convert.ToDecimal((colDeprecCount / coltotallead) * 100), 2) : 0;
                        //colPannelsold = Convert.ToDecimal(((from o in _jobRepository.GetAll()
                        //                                    join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                        //                                    from i in j1
                        //                                    join pay in _jobProductItemRepository.GetAll() on o.Id equals pay.JobId into inv1
                        //                                    from pay in inv1
                        //                                    join pnnl in _ProductItemRepository.GetAll() on pay.ProductItemId equals pnnl.Id into pnnls
                        //                                    from pnnl in pnnls
                        //                                    where (o.DepositeRecceivedDate.Value.Date >= fromdate.Value.Date && o.DepositeRecceivedDate.Value.Date <= edate.Value.Date && i.LeadSourceId == item.Id && pnnl.ProductTypeId == 1 && i.OrganizationId == input.organizationId
                        //                                     //&& stateList.Contains(i.State)
                        //                                     && Areas.Contains(i.Area != null ? i.Area : "")
                        //                                    )
                        //                                    select (pay.Quantity)).Sum()));
                        colPannelsold = Convert.ToDecimal(
                            (from o in _jobRepository.GetAll()
                             join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                             from i in j1
                             join pay in _jobProductItemRepository.GetAll() on o.Id equals pay.JobId into inv1
                             from pay in inv1
                             join pnnl in _ProductItemRepository.GetAll() on pay.ProductItemId equals pnnl.Id into pnnls
                             from pnnl in pnnls
                             where (i.LeadSourceId == item.Id
                                    && pnnl.ProductTypeId == 1
                                    && i.OrganizationId == input.organizationId
                                    && Areas.Contains(i.Area ?? ""))
                             select new { o, pay }) 
                            .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.o.DepositeRecceivedDate.HasValue && e.o.DepositeRecceivedDate.Value.Date >= fromdate.Value.Date)
                            .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.o.DepositeRecceivedDate.HasValue && e.o.DepositeRecceivedDate.Value.Date <= edate.Value.Date)
                            .WhereIf(input.DateType == "FirstDeposit" && input.StartDate != null, e => e.o.FirstDepositDate.HasValue && e.o.FirstDepositDate.Value.Date >= fromdate.Value.Date)
                            .WhereIf(input.DateType == "FirstDeposit" && input.EndDate != null, e => e.o.FirstDepositDate.HasValue && e.o.FirstDepositDate.Value.Date <= edate.Value.Date)
                            .Sum(e => e.pay.Quantity)); 



                        colInstallKwSold = Convert.ToDecimal((from o in _jobRepository.GetAll()
                                                              join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                                              from i in j1
                                                                  //join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
                                                                  //from pay in inv1
                                                              where (o.InstallationDate.Value.Date >= fromdate.Value.Date && o.InstallationDate.Value.Date <= edate.Value.Date && i.LeadSourceId == item.Id && i.OrganizationId == input.organizationId
                                                               //&& stateList.Contains(i.State)
                                                               && Areas.Contains(i.Area != null ? i.Area : "")
                                                              )
                                                              select (o.SystemCapacity)).Sum()) + colbatteyKW_installed;


                        colperInstallKwCost = coltotalCost > 0 && colInstallKwSold > 0 ? Math.Round(coltotalCost / colInstallKwSold, 2) : 0;

                        colCostpannel = coltotalCost > 0 && colPannelsold > 0 ? Math.Round(Convert.ToDecimal((coltotalCost / colPannelsold)), 2) : 0;


                        StateForLeadExpenseReportDto sttotal = new StateForLeadExpenseReportDto();
                        sttotal.statename = "Total";
                        sttotal.totallead = coltotallead;
                        sttotal.totalCost = coltotalCost;
                        sttotal.CostLead = colCostLead;
                        sttotal.KwSold = colKwSold;
                        sttotal.perKwCost = colperKwCost;
                        sttotal.ProjectOpen = colProjectOpen;
                        sttotal.ProjectOpenpercent = colProjectOpenpercent;
                        sttotal.DeprecCount = colDeprecCount;
                        sttotal.DeprecKWSold = colDeprecKWSold;
                        sttotal.Pannelsold = (int)colPannelsold;
                        sttotal.Costpannel = colCostpannel;
                        sttotal.InstallKwSold = colInstallKwSold;
                        sttotal.perInstallKwCost = colperInstallKwCost;
                        Statelist.Add(sttotal);
                    }

                    for (int j = 0; j < entity.Count; j++)
                    {
                        //if (firstdt == 0)
                        //{
                            
                            
                        //    firstdt = 1;
                        //}

                        if (input.states != null)
                        {
                            if(entity.Count > 0) 
                            {
                                var categoryid = entity[j].Id;
                                var statename = entity[j].Name;

                                spend = Convert.ToDecimal((from l in _lookup_leadSourceRepository.GetAll()
                                                           join Pa in _leadExpenseRepository.GetAll().Where(le => le.LeadinvestmentFk.OrganizationId == input.organizationId) on l.Id equals Pa.LeadSourceId into PaJoined
                                                           from Pa in PaJoined.DefaultIfEmpty()
                                                           where (Pa.StateId == categoryid && Pa.LeadSourceId == item.Id && Pa.FromDate.Date >= fromdate.Value.Date && Pa.ToDate.Date <= edate)
                                                           select (Pa.Amount)).Sum());

                                //kws = Convert.ToDecimal((from o in _jobRepository.GetAll()
                                //                         join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                //                         from i in j1
                                //                             //join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
                                //                             //from pay in inv1
                                //                         where (o.FirstDepositDate.Value.Date >= fromdate.Value.Date && o.FirstDepositDate.Value.Date <= edate.Value.Date && i.LeadSourceId == item.Id && i.State == statename && i.OrganizationId == input.organizationId
                                //                         && Areas.Contains(i.Area != null ? i.Area : ""))
                                //                         select (o.SystemCapacity)).Sum());

                                if (input.WithBattery)
                                {
                                    batteyKW = Convert.ToDecimal(_jobProductItemRepository.GetAll().Where(e => e.JobFk.LeadFk.LeadSourceId == item.Id && e.JobFk.LeadFk.State == statename && e.JobFk.LeadFk.OrganizationId == input.organizationId && Areas.Contains(e.JobFk.LeadFk.Area ?? "") && e.ProductItemFk.ProductTypeId == 5)
                                                            .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.JobFk.DepositeRecceivedDate.Value.Date >= fromdate.Value.Date)
                                                            .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.JobFk.DepositeRecceivedDate.Value.Date <= edate.Value.Date)
                                                            .WhereIf(input.DateType == "FirstDeposit" && input.StartDate != null, e => e.JobFk.FirstDepositDate.Value.Date >= fromdate.Value.Date)
                                                            .WhereIf(input.DateType == "FirstDeposit" && input.EndDate != null, e => e.JobFk.FirstDepositDate.Value.Date <= edate.Value.Date)
                                                            .Select(e => e.Quantity * e.ProductItemFk.Size)
                                                            .Sum());

                                    batteyKW_installed = Convert.ToDecimal(_jobProductItemRepository.GetAll().Where(e => e.JobFk.LeadFk.LeadSourceId == item.Id && e.JobFk.LeadFk.State == statename && e.JobFk.LeadFk.OrganizationId == input.organizationId && Areas.Contains(e.JobFk.LeadFk.Area ?? "") && e.ProductItemFk.ProductTypeId == 5)
                                                            .WhereIf(input.StartDate != null, e => e.JobFk.InstallationDate.Value.Date >= fromdate.Value.Date)
                                                            .WhereIf(input.EndDate != null, e => e.JobFk.InstallationDate.Value.Date <= edate.Value.Date)
                                                            .Select(e => e.Quantity * e.ProductItemFk.Size)
                                                            .Sum());
                                }

                                kws = Convert.ToDecimal((from o in _jobRepository.GetAll()
                                                         join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                                         from i in j1
                                                         where (i.LeadSourceId == item.Id && i.State == statename
                                                                && i.OrganizationId == input.organizationId
                                                                && Areas.Contains(i.Area ?? ""))
                                                         select o)
                                                    .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= fromdate.Value.Date)
                                                    .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= edate.Value.Date)
                                                    .WhereIf(input.DateType == "FirstDeposit" && input.StartDate != null, e => e.FirstDepositDate.Value.Date >= fromdate.Value.Date)
                                                    .WhereIf(input.DateType == "FirstDeposit" && input.EndDate != null, e => e.FirstDepositDate.Value.Date <= edate.Value.Date)
                                                    .Select(e => e.SystemCapacity)
                                                    .Sum()) + batteyKW;

                                project = ((from o in _jobRepository.GetAll()
                                            join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                            from i in j1
                                                //join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
                                                //from pay in inv1
                                            where (o.CreationTime.AddHours(10).Date >= fromdate.Value.Date && o.CreationTime.AddHours(10).Date <= edate.Value.Date && i.LeadSourceId == item.Id && i.State == statename && i.OrganizationId == input.organizationId && Areas.Contains(i.Area != null ? i.Area : ""))
                                            select (o.Id)).Count());

                                //depositerec = ((from o in _jobRepository.GetAll()
                                //                join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                //                from i in j1
                                //                    //join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
                                //                    //from pay in inv1
                                //                where (o.DepositeRecceivedDate.Value.Date >= fromdate.Value.Date && o.DepositeRecceivedDate.Value.Date <= edate.Value.Date && i.LeadSourceId == item.Id && i.State == statename && i.OrganizationId == input.organizationId && Areas.Contains(i.Area != null ? i.Area : ""))
                                //                select (o.Id)).Count());
                                depositerec = (from o in _jobRepository.GetAll()
                                               join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                               from i in j1
                                               where (i.LeadSourceId == item.Id && i.State == statename
                                                      && i.OrganizationId == input.organizationId
                                                      && Areas.Contains(i.Area ?? ""))
                                               select o)
                                                .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= fromdate.Value.Date)
                                                .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= edate.Value.Date)
                                                .WhereIf(input.DateType == "FirstDeposit" && input.StartDate != null, e => e.FirstDepositDate.Value.Date >= fromdate.Value.Date)
                                                .WhereIf(input.DateType == "FirstDeposit" && input.EndDate != null, e => e.FirstDepositDate.Value.Date <= edate.Value.Date)
                                                .Count();

                                //pannelsold = Convert.ToDecimal((from o in _jobRepository.GetAll()
                                //                                join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                //                                from i in j1
                                //                                join pay in _jobProductItemRepository.GetAll() on o.Id equals pay.JobId into inv1
                                //                                from pay in inv1
                                //                                join pnnl in _ProductItemRepository.GetAll() on pay.ProductItemId equals pnnl.Id into pnnls
                                //                                from pnnl in pnnls
                                //                                where (o.DepositeRecceivedDate.Value.AddHours(10).Date >= fromdate.Value.Date && o.DepositeRecceivedDate.Value.AddHours(10).Date <= edate.Value.Date && i.LeadSourceId == item.Id && o.State == statename && pnnl.ProductTypeId == 1 && i.OrganizationId == input.organizationId && Areas.Contains(i.Area != null ? i.Area : ""))
                                //                                select (pay.Quantity)).Sum());
                                pannelsold = Convert.ToDecimal(
                                                                (from o in _jobRepository.GetAll()
                                                                 join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                                                 from i in j1
                                                                 join pay in _jobProductItemRepository.GetAll() on o.Id equals pay.JobId into inv1
                                                                 from pay in inv1
                                                                 join pnnl in _ProductItemRepository.GetAll() on pay.ProductItemId equals pnnl.Id into pnnls
                                                                 from pnnl in pnnls
                                                                 where (i.LeadSourceId == item.Id && o.State == statename
                                                                        && pnnl.ProductTypeId == 1
                                                                        && i.OrganizationId == input.organizationId
                                                                        && Areas.Contains(i.Area ?? ""))
                                                                 select new { o, pay })
                                                                .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.o.DepositeRecceivedDate.HasValue && e.o.DepositeRecceivedDate.Value.Date >= fromdate.Value.Date)
                                                                .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.o.DepositeRecceivedDate.HasValue && e.o.DepositeRecceivedDate.Value.Date <= edate.Value.Date)
                                                                .WhereIf(input.DateType == "FirstDeposit" && input.StartDate != null, e => e.o.FirstDepositDate.HasValue && e.o.FirstDepositDate.Value.Date >= fromdate.Value.Date)
                                                                .WhereIf(input.DateType == "FirstDeposit" && input.EndDate != null, e => e.o.FirstDepositDate.HasValue && e.o.FirstDepositDate.Value.Date <= edate.Value.Date)
                                                                .Sum(e => e.pay.Quantity));


                                installkws = Convert.ToDecimal((from o in _jobRepository.GetAll()
                                                                join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                                                from i in j1
                                                                    //join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
                                                                    //from pay in inv1
                                                                where (o.InstallationDate.Value.Date >= fromdate.Value.Date && o.InstallationDate.Value.Date <= edate.Value.Date && i.LeadSourceId == item.Id && i.State == statename && i.OrganizationId == input.organizationId && Areas.Contains(i.Area != null ? i.Area : ""))
                                                                select (o.SystemCapacity)).Sum()) + batteyKW_installed;

                                spenddt = spend != null ? spend : 0;
                                var linkdata = (from us in _lookup_leadSourceRepository.GetAll()
                                                where us.Id == (_lookup_leadSourceRepository.GetAll().Select(e => e.Id).FirstOrDefault())

                                                select new
                                                {
                                                    //stid = 1,
                                                    // daydif = (int)((Convert.ToDateTime(Pa.ToDate) - Convert.ToDateTime(Pa.FromDate)).TotalDays),
                                                    // dailydiff= Pa.Amount / (int)((Convert.ToDateTime(Pa.ToDate) - Convert.ToDateTime(Pa.FromDate)).TotalDays),
                                                    //  amount = (Pa.Amount / (int)((Convert.ToDateTime(Pa.ToDate) - Convert.ToDateTime(Pa.FromDate)).TotalDays))*daydiff,
                                                    //amount = Pa.dailyamount * daydiff,
                                                    amount = spenddt,
                                                    leadname = us.Name,
                                                    //leadid = 1,
                                                    //totallead = (_leadRepository.GetAll().Where(e => e.CreationTime.AddHours(10).Date >= fromdate.Value.Date && e.CreationTime.AddHours(10).Date <= edate.Value.Date && e.LeadSourceId == item.Id && e.State == statename && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.organizationId).Select(e => e.Id).Count()),
                                                    totallead = (_leadRepository.GetAll().Where(e => e.CreationTime.AddHours(10).Date >= fromdate.Value.Date && e.CreationTime.AddHours(10).Date <= edate.Value.Date && e.LeadSourceId == item.Id && e.State == statename && e.HideDublicate != true && e.OrganizationId == input.organizationId && e.AssignToUserID != null && Areas.Contains(e.Area != null ? e.Area : "")).Select(e => e.Id).Count()),
                                                    KWSold = kws > 0 ? kws : 0,
                                                    perKwCost = kws > 0 && spenddt > 0 ? kws / spenddt : kws,
                                                    openproject = project > 0 ? project : 0,
                                                    openprojectper = project > 0 ? project / 100 : 0,
                                                    DepositeRecived = depositerec > 0 ? depositerec : 0,
                                                    PannelSold = pannelsold > 0 ? pannelsold : 0,
                                                    InstallKWSold = installkws > 0 ? installkws : 0,
                                                    perInstallKwCost = installkws > 0 && spenddt > 0 ? installkws / spenddt : installkws,
                                                    //depositeperc = Convert.ToDecimal(((from o in _jobRepository.GetAll()
                                                    //                                   join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                                    //                                   from i in j1
                                                    //                                   join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
                                                    //                                   from pay in inv1
                                                    //                                   where (o.DepositeRecceivedDate.Value.Date >= fromdate.Value.Date && o.DepositeRecceivedDate.Value.Date <= edate.Value.Date && i.LeadSourceId == item.Id && i.State == statename && ((_invoicePaymentRepository.GetAll().Where(x => x.JobId == o.Id).Select(e => e.InvoicePayTotal).Sum()) != o.TotalCost) && i.OrganizationId == input.organizationId && Areas.Contains(i.Area != null ? i.Area : ""))
                                                    //                                   select (o.SystemCapacity)).Sum())),


                                                }).FirstOrDefault();

                                if (linkdata != null)
                                {
                                    StateForLeadExpenseReportDto st = new StateForLeadExpenseReportDto();
                                    st.statename = statename;
                                    st.totallead = Convert.ToInt32(linkdata.totallead);
                                    st.totalCost = Math.Round(linkdata.amount);
                                    st.CostLead = st.totallead > 0 ? Math.Round(Convert.ToDecimal(st.totalCost / st.totallead), 2) : 0;
                                    st.KwSold = Math.Round(Convert.ToDecimal(linkdata.KWSold), 2);
                                    //st.perKwCost = Math.Round(Convert.ToDecimal(linkdata.perKwCost), 2);
                                    st.perKwCost = st.KwSold > 0 ? Math.Round(Convert.ToDecimal(st.totalCost / st.KwSold), 2) : 0;
                                    st.InstallKwSold = Math.Round(Convert.ToDecimal(linkdata.InstallKWSold), 2);
                                    //st.perKwCost = Math.Round(Convert.ToDecimal(linkdata.perKwCost), 2);
                                    st.perInstallKwCost = st.InstallKwSold > 0 ? Math.Round(Convert.ToDecimal(st.totalCost / st.InstallKwSold), 2) : 0;
                                    st.ProjectOpen = Math.Round(Convert.ToDecimal(linkdata.openproject), 2);
                                    //st.ProjectOpenpercent = Math.Round(Convert.ToDecimal(linkdata.openprojectper), 2);
                                    st.ProjectOpenpercent = st.totallead > 0 ? Math.Round(Convert.ToDecimal((st.ProjectOpen / st.totallead) * 100), 2) : 0;
                                    st.DeprecCount = Math.Round(Convert.ToDecimal(linkdata.DepositeRecived), 2);
                                    st.DeprecKWSold = st.totallead > 0 ? Math.Round(Convert.ToDecimal((st.DeprecCount / st.totallead) * 100), 2) : 0;
                                    st.Pannelsold = Convert.ToInt32(linkdata.PannelSold);
                                    st.Costpannel = st.Pannelsold > 0 ? Math.Round(Convert.ToDecimal(st.totalCost / st.Pannelsold), 2) : 0;
                                    //coltotallead = coltotallead + st.totallead;
                                    //coltotalCost = coltotalCost + st.totalCost;
                                    //colCostLead = colCostLead + st.CostLead;
                                    //colKwSold = colKwSold + st.KwSold; 
                                    //colperKwCost = colperKwCost+ st.perKwCost;
                                    //colProjectOpen = colProjectOpen+ st.ProjectOpen; 
                                    //colProjectOpenpercent = colProjectOpenpercent + st.ProjectOpenpercent;
                                    //colDeprecCount = colDeprecCount + st.DeprecCount;
                                    //colDeprecKWSold = colDeprecKWSold + st.DeprecKWSold;
                                    //colPannelsold = colPannelsold + st.Pannelsold;
                                    //colCostpannel = colCostpannel + st.Costpannel;


                                    Statelist.Add(st);
                                }
                                else
                                {
                                    StateForLeadExpenseReportDto st = new StateForLeadExpenseReportDto();
                                    st.statename = statename;
                                    st.totallead = 0;
                                    st.totalCost = 0;
                                    st.CostLead = 0;
                                    st.KwSold = 0;
                                    //st.perKwCost = Math.Round(Convert.ToDecimal(linkdata.perKwCost), 2);
                                    st.perKwCost = 0;
                                    st.InstallKwSold = 0;
                                    //st.perKwCost = Math.Round(Convert.ToDecimal(linkdata.perKwCost), 2);
                                    st.perInstallKwCost = 0;
                                    st.ProjectOpen = 0;
                                    //st.ProjectOpenpercent = Math.Round(Convert.ToDecimal(linkdata.openprojectper), 2);
                                    st.ProjectOpenpercent = 0;
                                    st.DeprecCount = 0;
                                    st.DeprecKWSold = 0;
                                    st.Pannelsold = 0;
                                    st.Costpannel = 0;
                                    Statelist.Add(st);
                                }
                            }
                            
                        }
                        

                        
                        


                    }


                    statewiselist.leadstates = Statelist;

                    leadlist1.Add(statewiselist);
                }
                decimal nswtotallead = 0;
                decimal nswtotalCost = 0;
                decimal nswwCostLead = 0;
                decimal rownswKwSold = 0;
                decimal nswcolCostLead = 0;
                decimal nswcolKwSold = 0;
                decimal nswcolInstallKwSold = 0;
                decimal nswcolProjectOpen = 0;
                decimal nswcolperKwSold = 0;
                decimal nswcolperInstallKwSold = 0;
                decimal nswcolProjectOpenpercent = 0;
                decimal nswcolDeprecCount = 0;
                decimal nswcolDeprecKWSold = 0;
                decimal nswcolPannelsold = 0;
                decimal nswcolCostpannel = 0;

                decimal qldtotallead = 0;
                decimal qldtotalCost = 0;
                decimal qldcolCostLead = 0;
                decimal qldcolKwSold = 0;
                decimal qldcolperKwSold = 0;
                decimal qldcolInstallKwSold = 0;
                decimal qldcolperInstallKwSold = 0;
                decimal qldcolperKwCost = 0;
                decimal qldcolProjectOpen = 0;

                decimal qldcolProjectOpenpercent = 0;
                decimal qldcolDeprecCount = 0;
                decimal qldcolDeprecKWSold = 0;
                decimal qldcolPannelsold = 0;
                decimal qldcolCostpannel = 0;

                decimal satotallead = 0;
                decimal satotalCost = 0;
                decimal sacolCostLead = 0;
                decimal sacolKwSold = 0;
                decimal sacolperKwSold = 0;
                decimal sacolInstallKwSold = 0;
                decimal sacolperInstallKwSold = 0;
                decimal sacolperKwCost = 0;
                decimal sacolProjectOpen = 0;

                decimal sacolProjectOpenpercent = 0;
                decimal sacolDeprecCount = 0;
                decimal sacolDeprecKWSold = 0;
                decimal sacolPannelsold = 0;
                decimal sacolCostpannel = 0;

                decimal watotallead = 0;
                decimal watotalCost = 0;
                decimal wacolCostLead = 0;
                decimal wacolKwSold = 0;
                decimal wacolperKwSold = 0;
                decimal wacolInstallKwSold = 0;
                decimal wacolperInstallKwSold = 0;
                decimal wacolperKwCost = 0;
                decimal wacolProjectOpen = 0;

                decimal wacolProjectOpenpercent = 0;
                decimal wacolDeprecCount = 0;
                decimal wacolDeprecKWSold = 0;
                decimal wacolPannelsold = 0;
                decimal wacolCostpannel = 0;

                decimal victotallead = 0;
                decimal victotalCost = 0;
                decimal viccolCostLead = 0;
                decimal viccolKwSold = 0;
                decimal viccolperKwSold = 0;
                decimal viccolInstallKwSold = 0;
                decimal viccolperInstallKwSold = 0;
                decimal viccolperKwCost = 0;
                decimal viccolProjectOpen = 0;

                decimal viccolProjectOpenpercent = 0;
                decimal viccolDeprecCount = 0;
                decimal viccolDeprecKWSold = 0;
                decimal viccolPannelsold = 0;
                decimal viccolCostpannel = 0;

                decimal fintotallead = 0;
                decimal fintotalCost = 0;
                decimal fincolCostLead = 0;
                decimal fincolKwSold = 0;
                decimal fincolperKwSold = 0;
                decimal fincolInstallKwSold = 0;
                decimal fincolperInstallKwSold = 0;
                decimal fincolperKwCost = 0;
                decimal fincolProjectOpen = 0;

                decimal fincolProjectOpenpercent = 0;
                decimal fincolDeprecCount = 0;
                decimal fincolDeprecKWSold = 0;
                decimal fincolPannelsold = 0;
                decimal fincolCostpannel = 0;

                for (int m = 0; m < leadlist1.Count; m++)
                {
                    for (int k = 0; k < leadlist1[m].leadstates.Count; k++)
                    {
                        if (leadlist1[m].leadstates[k].statename == "NSW")
                        {


                            nswtotalCost = nswtotalCost + leadlist1[m].leadstates[k].totalCost;

                            nswtotallead = nswtotallead + leadlist1[m].leadstates[k].totallead;

                            nswcolCostLead = nswcolCostLead + leadlist1[m].leadstates[k].CostLead;

                            nswcolKwSold = nswcolKwSold + leadlist1[m].leadstates[k].KwSold;

                            nswcolperKwSold = nswcolperKwSold + leadlist1[m].leadstates[k].perKwCost;
                            nswcolInstallKwSold = nswcolInstallKwSold + leadlist1[m].leadstates[k].InstallKwSold;

                            nswcolperInstallKwSold = nswcolperInstallKwSold + leadlist1[m].leadstates[k].perInstallKwCost;
                            nswcolProjectOpen = nswcolProjectOpen + leadlist1[m].leadstates[k].ProjectOpen;

                            nswcolProjectOpenpercent = nswcolProjectOpenpercent + leadlist1[m].leadstates[k].ProjectOpenpercent;

                            nswcolDeprecCount = nswcolDeprecCount + leadlist1[m].leadstates[k].DeprecCount;

                            nswcolDeprecKWSold = nswcolDeprecKWSold + leadlist1[m].leadstates[k].DeprecKWSold;

                            nswcolPannelsold = nswcolPannelsold + leadlist1[m].leadstates[k].Pannelsold;

                            nswcolCostpannel = nswcolCostpannel + leadlist1[m].leadstates[k].Costpannel;


                        }

                        if (leadlist1[m].leadstates[k].statename == "QLD")
                        {
                            qldtotalCost = qldtotalCost + leadlist1[m].leadstates[k].totalCost;

                            qldtotallead = qldtotallead + leadlist1[m].leadstates[k].totallead;

                            qldcolCostLead = qldcolCostLead + leadlist1[m].leadstates[k].CostLead;

                            qldcolKwSold = qldcolKwSold + leadlist1[m].leadstates[k].KwSold;
                            qldcolperKwSold = qldcolperKwSold + leadlist1[m].leadstates[k].perKwCost;

                            qldcolInstallKwSold = qldcolInstallKwSold + leadlist1[m].leadstates[k].InstallKwSold;
                            qldcolperInstallKwSold = qldcolperInstallKwSold + leadlist1[m].leadstates[k].perInstallKwCost;

                            qldcolProjectOpen = qldcolProjectOpen + leadlist1[m].leadstates[k].ProjectOpen;

                            qldcolProjectOpenpercent = qldcolProjectOpenpercent + leadlist1[m].leadstates[k].ProjectOpenpercent;

                            qldcolDeprecCount = qldcolDeprecCount + leadlist1[m].leadstates[k].DeprecCount;

                            qldcolDeprecKWSold = qldcolDeprecKWSold + leadlist1[m].leadstates[k].DeprecKWSold;

                            qldcolPannelsold = qldcolPannelsold + leadlist1[m].leadstates[k].Pannelsold;

                            qldcolCostpannel = qldcolCostpannel + leadlist1[m].leadstates[k].Costpannel;


                        }
                        if (leadlist1[m].leadstates[k].statename == "SA")
                        {

                            satotalCost = satotalCost + leadlist1[m].leadstates[k].totalCost;

                            satotallead = satotallead + leadlist1[m].leadstates[k].totallead;

                            sacolCostLead = sacolCostLead + leadlist1[m].leadstates[k].CostLead;

                            sacolKwSold = sacolKwSold + leadlist1[m].leadstates[k].KwSold;
                            sacolperKwSold = sacolperKwSold + leadlist1[m].leadstates[k].perKwCost;

                            sacolInstallKwSold = sacolInstallKwSold + leadlist1[m].leadstates[k].InstallKwSold;
                            sacolperInstallKwSold = sacolperInstallKwSold + leadlist1[m].leadstates[k].perInstallKwCost;

                            sacolProjectOpen = sacolProjectOpen + leadlist1[m].leadstates[k].ProjectOpen;

                            sacolProjectOpenpercent = sacolProjectOpenpercent + leadlist1[m].leadstates[k].ProjectOpenpercent;

                            sacolDeprecCount = sacolDeprecCount + leadlist1[m].leadstates[k].DeprecCount;

                            sacolDeprecKWSold = sacolDeprecKWSold + leadlist1[m].leadstates[k].DeprecKWSold;

                            sacolPannelsold = sacolPannelsold + leadlist1[m].leadstates[k].Pannelsold;

                            sacolCostpannel = sacolCostpannel + leadlist1[m].leadstates[k].Costpannel;
                        }
                        if (leadlist1[m].leadstates[k].statename == "VIC")
                        {

                            victotalCost = victotalCost + leadlist1[m].leadstates[k].totalCost;

                            victotallead = victotallead + leadlist1[m].leadstates[k].totallead;

                            viccolCostLead = viccolCostLead + leadlist1[m].leadstates[k].CostLead;

                            viccolKwSold = viccolKwSold + leadlist1[m].leadstates[k].KwSold;
                            viccolperKwSold = viccolperKwSold + leadlist1[m].leadstates[k].perKwCost;

                            viccolInstallKwSold = viccolInstallKwSold + leadlist1[m].leadstates[k].InstallKwSold;
                            viccolperInstallKwSold = viccolperInstallKwSold + leadlist1[m].leadstates[k].perInstallKwCost;

                            viccolProjectOpen = viccolProjectOpen + leadlist1[m].leadstates[k].ProjectOpen;

                            viccolProjectOpenpercent = viccolProjectOpenpercent + leadlist1[m].leadstates[k].ProjectOpenpercent;

                            viccolDeprecCount = viccolDeprecCount + leadlist1[m].leadstates[k].DeprecCount;

                            viccolDeprecKWSold = viccolDeprecKWSold + leadlist1[m].leadstates[k].DeprecKWSold;

                            viccolPannelsold = viccolPannelsold + leadlist1[m].leadstates[k].Pannelsold;

                            viccolCostpannel = viccolCostpannel + leadlist1[m].leadstates[k].Costpannel;
                        }
                        if (leadlist1[m].leadstates[k].statename == "WA")
                        {

                            watotalCost = watotalCost + leadlist1[m].leadstates[k].totalCost;

                            watotallead = watotallead + leadlist1[m].leadstates[k].totallead;

                            wacolCostLead = wacolCostLead + leadlist1[m].leadstates[k].CostLead;

                            wacolKwSold = wacolKwSold + leadlist1[m].leadstates[k].KwSold;
                            wacolperKwSold = wacolperKwSold + leadlist1[m].leadstates[k].perKwCost;

                            wacolInstallKwSold = wacolInstallKwSold + leadlist1[m].leadstates[k].InstallKwSold;
                            wacolperInstallKwSold = wacolperInstallKwSold + leadlist1[m].leadstates[k].perInstallKwCost;

                            wacolProjectOpen = wacolProjectOpen + leadlist1[m].leadstates[k].ProjectOpen;

                            wacolProjectOpenpercent = wacolProjectOpenpercent + leadlist1[m].leadstates[k].ProjectOpenpercent;

                            wacolDeprecCount = wacolDeprecCount + leadlist1[m].leadstates[k].DeprecCount;

                            wacolDeprecKWSold = wacolDeprecKWSold + leadlist1[m].leadstates[k].DeprecKWSold;

                            wacolPannelsold = wacolPannelsold + leadlist1[m].leadstates[k].Pannelsold;

                            wacolCostpannel = wacolCostpannel + leadlist1[m].leadstates[k].Costpannel;
                        }
                        if (leadlist1[m].leadstates[k].statename == "Total")
                        {

                            fintotalCost = fintotalCost + leadlist1[m].leadstates[k].totalCost;

                            fintotallead = fintotallead + leadlist1[m].leadstates[k].totallead;

                            fincolCostLead = fincolCostLead + leadlist1[m].leadstates[k].CostLead;

                            fincolKwSold = fincolKwSold + leadlist1[m].leadstates[k].KwSold;
                            fincolperKwSold = fincolperKwSold + leadlist1[m].leadstates[k].perKwCost;

                            fincolInstallKwSold = fincolInstallKwSold + leadlist1[m].leadstates[k].InstallKwSold;
                            fincolperInstallKwSold = fincolperInstallKwSold + leadlist1[m].leadstates[k].perInstallKwCost;

                            fincolProjectOpen = fincolProjectOpen + leadlist1[m].leadstates[k].ProjectOpen;

                            fincolProjectOpenpercent = fincolProjectOpenpercent + leadlist1[m].leadstates[k].ProjectOpenpercent;

                            fincolDeprecCount = fincolDeprecCount + leadlist1[m].leadstates[k].DeprecCount;

                            fincolDeprecKWSold = fincolDeprecKWSold + leadlist1[m].leadstates[k].DeprecKWSold;

                            fincolPannelsold = fincolPannelsold + leadlist1[m].leadstates[k].Pannelsold;

                            fincolCostpannel = fincolCostpannel + leadlist1[m].leadstates[k].Costpannel;
                        }
                    }


                }
                List<StateForLeadExpenseReportDto> Statelist1 = new List<StateForLeadExpenseReportDto>();
                for (int p = 0; p < entity.Count + 1; p++)
                {
                    StateForLeadExpenseReportDto st = new StateForLeadExpenseReportDto();
                    if (p == 0 && total == true)
                    {
                        //  st.statename = statename;{
                        st.totallead = fintotallead;
                        st.totalCost = Math.Round(fintotalCost);
                        //st.CostLead = fincolCostLead;
                        st.CostLead = fintotallead > 0 ? Math.Round(fintotalCost / fintotallead, 2) : 0;
                        st.KwSold = fincolKwSold;
                        //st.perKwCost = Math.Round(fincolperKwSold, 2);
                        st.perKwCost = fincolKwSold > 0 ? Math.Round(fintotalCost / fincolKwSold, 2) : 0;

                        st.InstallKwSold = fincolInstallKwSold;
                        st.perInstallKwCost = fincolInstallKwSold > 0 ? Math.Round(fintotalCost / fincolInstallKwSold, 2) : 0;

                        st.ProjectOpen = Math.Round(Convert.ToDecimal(fincolProjectOpen), 2);
                        //st.ProjectOpenpercent = fincolProjectOpenpercent;
                        st.ProjectOpenpercent = fintotallead > 0 ? Math.Round((fincolProjectOpen * 100) / fintotallead, 2) : 0;
                        st.DeprecCount = fincolDeprecCount;
                        //st.DeprecKWSold = fincolDeprecKWSold;
                        st.DeprecKWSold = fintotallead > 0 ? Math.Round((fincolDeprecCount * 100) / fintotallead, 2) : 0;
                        st.Pannelsold = (int)fincolPannelsold;
                        //st.Costpannel = fincolCostpannel;
                        st.Costpannel = fincolPannelsold > 0 ? Math.Round(fintotalCost / fincolPannelsold, 2) : 0;
                        Statelist1.Add(st);
                    }
                    else if (input.states != null && p > 0)
                    {
                        if(input.states.Count() > 0)
                        {
                            if (entity[p-1].Name == "NSW")
                            {
                                //  st.statename = statename;{
                                st.totallead = nswtotallead;
                                st.totalCost = Math.Round(nswtotalCost);
                                //st.CostLead = Math.Round(nswcolCostLead, 2);
                                st.CostLead = nswtotallead > 0 ? Math.Round(nswtotalCost / nswtotallead, 2) : 0;
                                st.KwSold = nswcolKwSold;
                                //st.perKwCost = Math.Round(nswcolperKwSold, 2);
                                st.perKwCost = nswcolKwSold > 0 ? Math.Round(nswtotalCost / nswcolKwSold, 2) : 0;

                                st.InstallKwSold = nswcolInstallKwSold;
                                st.perInstallKwCost = nswcolInstallKwSold > 0 ? Math.Round(nswtotalCost / nswcolInstallKwSold, 2) : 0;

                                st.ProjectOpen = Math.Round(Convert.ToDecimal(nswcolProjectOpen), 2);
                                //st.ProjectOpenpercent = nswcolProjectOpenpercent;
                                st.ProjectOpenpercent = nswtotallead > 0 ? Math.Round((nswcolProjectOpen * 100) / nswtotallead, 2) : 0;
                                st.DeprecCount = nswcolDeprecCount;
                                //st.DeprecKWSold = nswcolDeprecKWSold;
                                st.DeprecKWSold = nswtotallead > 0 ? Math.Round((nswcolDeprecCount * 100) / nswtotallead, 2) : 0;
                                st.Pannelsold = (int)nswcolPannelsold;
                                //st.Costpannel = Math.Round(nswcolCostpannel, 2);
                                st.Costpannel = nswcolPannelsold > 0 ? Math.Round(nswtotalCost / nswcolPannelsold, 2) : 0;
                                Statelist1.Add(st);
                            }
                            else if (entity[p-1].Name == "QLD")
                            {
                                st.totallead = qldtotallead;
                                st.totalCost = Math.Round(qldtotalCost);
                                //st.CostLead = Math.Round(qldcolCostLead, 2);
                                st.CostLead = qldtotallead > 0 ? Math.Round(qldtotalCost / qldtotallead, 2) : 0;
                                st.KwSold = qldcolKwSold;
                                //st.perKwCost = qldcolperKwSold;
                                st.perKwCost = qldcolKwSold > 0 ? Math.Round(qldtotalCost / qldcolKwSold, 2) : 0;

                                st.InstallKwSold = qldcolInstallKwSold;
                                st.perInstallKwCost = qldcolInstallKwSold > 0 ? Math.Round(qldtotalCost / qldcolInstallKwSold, 2) : 0;

                                st.ProjectOpen = Math.Round(Convert.ToDecimal(qldcolProjectOpen), 2);
                                //st.ProjectOpenpercent = qldcolProjectOpenpercent;
                                st.ProjectOpenpercent = qldtotallead > 0 ? Math.Round((qldcolProjectOpen * 100) / qldtotallead, 2) : 0;
                                st.DeprecCount = qldcolDeprecCount;
                                //st.DeprecKWSold = qldcolDeprecKWSold;
                                st.DeprecKWSold = qldtotallead > 0 ? Math.Round((qldcolDeprecCount * 100) / qldtotallead, 2) : 0;
                                st.Pannelsold = (int)qldcolPannelsold;
                                //st.Costpannel = qldcolCostpannel;
                                st.Costpannel = qldcolPannelsold > 0 ? Math.Round(qldtotalCost / qldcolPannelsold, 2) : 0;
                                Statelist1.Add(st);
                            }
                            else if (entity[p-1].Name == "SA")
                            {
                                st.totallead = satotallead;
                                st.totalCost = Math.Round(satotalCost);
                                //st.CostLead = sacolCostLead;
                                st.CostLead = satotallead > 0 ? Math.Round(satotalCost / satotallead, 2) : 0;
                                st.KwSold = sacolKwSold;
                                //st.perKwCost = sacolperKwSold;
                                st.perKwCost = sacolKwSold > 0 ? Math.Round(satotalCost / sacolKwSold, 2) : 0;

                                st.InstallKwSold = sacolInstallKwSold;
                                st.perInstallKwCost = sacolInstallKwSold > 0 ? Math.Round(satotalCost / sacolInstallKwSold, 2) : 0;

                                st.ProjectOpen = Math.Round(Convert.ToDecimal(sacolProjectOpen), 2);
                                //st.ProjectOpenpercent = sacolProjectOpenpercent;
                                st.ProjectOpenpercent = satotallead > 0 ? Math.Round((sacolProjectOpen * 100) / satotallead, 2) : 0;
                                st.DeprecCount = sacolDeprecCount;
                                //st.DeprecKWSold = sacolDeprecKWSold;
                                st.DeprecKWSold = satotallead > 0 ? Math.Round((sacolDeprecCount * 100) / satotallead, 2) : 0;
                                st.Pannelsold = (int)sacolPannelsold;
                                //st.Costpannel = sacolCostpannel;
                                st.Costpannel = sacolPannelsold > 0 ? Math.Round(satotalCost / sacolPannelsold, 2) : 0;
                                Statelist1.Add(st);
                            }
                            else if (entity[p-1].Name == "VIC")
                            {
                                //  st.statename = statename;{
                                st.totallead = victotallead;
                                st.totalCost = Math.Round(victotalCost);
                                //st.CostLead = viccolCostLead;
                                st.CostLead = victotallead > 0 ? Math.Round(victotalCost / victotallead, 2) : 0;
                                st.KwSold = viccolKwSold;
                                //st.perKwCost = wacolperKwSold;
                                st.perKwCost = viccolKwSold > 0 ? Math.Round(victotalCost / viccolKwSold, 2) : 0;

                                st.InstallKwSold = viccolInstallKwSold;
                                //st.perKwCost = wacolperKwSold;
                                st.perInstallKwCost = viccolInstallKwSold > 0 ? Math.Round(victotalCost / viccolInstallKwSold, 2) : 0;

                                st.ProjectOpen = Math.Round(Convert.ToDecimal(viccolProjectOpen), 2);
                                //st.ProjectOpenpercent = viccolProjectOpenpercent;
                                st.ProjectOpenpercent = victotallead > 0 ? Math.Round((viccolProjectOpen * 100) / victotallead, 2) : 0;
                                st.DeprecCount = viccolDeprecCount;
                                //st.DeprecKWSold = viccolDeprecKWSold;
                                st.DeprecKWSold = victotallead > 0 ? Math.Round((viccolDeprecCount * 100) / victotallead, 2) : 0;
                                st.Pannelsold = (int)viccolPannelsold;
                                //st.Costpannel = viccolCostpannel;
                                st.Costpannel = viccolPannelsold > 0 ? Math.Round(victotalCost / viccolPannelsold, 2) : 0;
                                Statelist1.Add(st);
                            }
                            else if (entity[p-1].Name == "WA")
                            {
                                st.totallead = watotallead;
                                st.totalCost = Math.Round(watotalCost);
                                //st.CostLead = wacolCostLead;
                                st.CostLead = watotallead > 0 ? Math.Round(watotalCost / watotallead, 2) : 0;
                                st.KwSold = wacolKwSold;
                                //st.perKwCost = wacolperKwSold;
                                st.perKwCost = wacolKwSold > 0 ? Math.Round(watotalCost / wacolKwSold, 2) : 0;

                                st.InstallKwSold = wacolInstallKwSold;
                                st.perInstallKwCost = wacolInstallKwSold > 0 ? Math.Round(watotalCost / wacolInstallKwSold, 2) : 0;

                                st.ProjectOpen = Math.Round(Convert.ToDecimal(wacolProjectOpen), 2);
                                //st.ProjectOpenpercent = wacolProjectOpenpercent;
                                st.ProjectOpenpercent = watotallead > 0 ? Math.Round((wacolProjectOpen * 100) / watotallead, 2) : 0;
                                st.DeprecCount = wacolDeprecCount;
                                //st.DeprecKWSold = wacolDeprecKWSold;
                                st.DeprecKWSold = watotallead > 0 ? Math.Round((wacolDeprecCount * 100) / watotallead, 2) : 0;
                                st.Pannelsold = (int)wacolPannelsold;
                                //st.Costpannel = wacolCostpannel;
                                st.Costpannel = wacolPannelsold > 0 ? Math.Round(watotalCost / wacolPannelsold, 2) : 0;
                                Statelist1.Add(st);
                            }

                        }
                    }
                    
                }


                

                    LeadExpenseReportDto statewiselist1 = new LeadExpenseReportDto();
                    statewiselist1.Leadsource = "Total";
                    statewiselist1.leadstates = Statelist1;




                    leadlist1.Add(statewiselist1);
                

                var count = 0;
                var tCount  =0;
                if(leadlist1.Count() > 0)
                {
                    if (leadlist1[0].leadstates.Count() > 0)
                    {
                        tCount = leadlist1[0].leadstates.Count() * 13;
                    }
                }

                List<LeadExpenseReportDto> leadlistRemove = new List<LeadExpenseReportDto>();

                foreach (var ls in leadlist1)
                {
                    count = 0;
                    foreach (var st in ls.leadstates)
                    {
                        if(st.totalCost <= 0)
                        {
                            count++;
                        }
                        if(st.totallead <= 0)
                        {
                            count++;
                        }
                        if(st.CostLead <= 0)
                        {
                            count++;
                        }
                        if(st.KwSold <= 0)
                        {
                            count++;
                        }
                        if(st.perKwCost <= 0)
                        {
                            count++;
                        }

                        if(st.InstallKwSold <= 0)
                        {
                            count++;
                        }
                        if(st.perInstallKwCost <= 0)
                        {
                            count++;
                        }

                        if(st.ProjectOpen <= 0)
                        {
                            count++;
                        }
                        if(st.ProjectOpenpercent <= 0)
                        {
                            count++;
                        }
                        if(st.DeprecCount <= 0)
                        {
                            count++;
                        }
                        if(st.DeprecKWSold <= 0)
                        {
                            count++;
                        }
                        if(st.Pannelsold <= 0)
                        {
                            count++;
                        }
                        if(st.Costpannel <= 0)
                        {
                            count++;
                        }
                    }
                    if(tCount == count)
                    {
                        leadlistRemove.Add(ls);
                    }
                }
                leadlist1.RemoveAll(r => leadlistRemove.Any(a => a == r));
                
                return leadlist1;


            }
            catch (Exception e) { throw e; }
        }



        public async Task<List<LeadStateLookupTableDto>> GetAllStateForTableDropdownForReport(GetAllexpenseInput input)
        {
            try
            {
                return await _lookup_stateRepository.GetAll().Where(e => e.IsReport == true)
                     .WhereIf(input.states != null && input.states.Count() > 0, e => input.states.Contains((int)e.Id))
                .Select(state => new LeadStateLookupTableDto
                {
                    Id = state.Id,
                    DisplayName = state == null || state.Name == null ? "" : state.Name.ToString().ToUpper()
                }).OrderBy(e => e.Id).ToListAsync();
            }
            catch (Exception e) { throw e; }
        }

        public async Task<List<LeadStateLookupTableDto>> GetAllStateForTableDropdownReportHeader(GetAllexpenseInput input)
        {
            try
            {
                return await _lookup_stateRepository.GetAll().Where(e => e.IsReport == true)
                     .WhereIf(input.states != null && input.states.Count() > 0, e => input.states.Contains((int)e.Id))
                .Select(state => new LeadStateLookupTableDto
                {
                    Id = state.Id,
                    DisplayName = state == null || state.Name == null ? "" : state.Name.ToString().ToUpper()
                }).OrderBy(e => e.Id).ToListAsync();
            }
            catch (Exception e) { throw e; }
        }
        public async Task editExpenselist(List<CreateOrEditLeadExpenseDto> input)
        {
            try
            {
                bool first = true;
                var investid = 0;
                var dataVaultLogId = 0;
                var List = new List<DataVaultActivityLogHistory>();

                var ListExpenseExist = _leadExpenseRepository.GetAll().Where(e => e.LeadinvestmentId == input[0].LeadExpenseId).ToList();
                var ListExpense = ListExpenseExist.GroupBy(e => e.LeadSourceId).Select(e => new { LeadSourceId = e.Key, Id = e.Max(m => m.Id) }).ToList();
                var ids = new List<int?>();

                await _leadExpenseRepository.HardDeleteAsync(e => e.LeadinvestmentId == input[0].LeadExpenseId);
                foreach (var item in input)
                {
                    ids.Add(item.NswId);
                    ids.Add(item.QldId);
                    ids.Add(item.SaId);
                    ids.Add(item.WaId);
                    ids.Add(item.VICId);

                    item.FromDate = (DateTime)_timeZoneConverter.Convert(item.FromDate, (int)AbpSession.TenantId).Value.Date;
                    item.ToDate = (DateTime)_timeZoneConverter.Convert(item.ToDate, (int)AbpSession.TenantId).Value.Date;
                    
                    int daydiff = (int)((item.ToDate.Date - item.FromDate.Date.AddDays(-1)).TotalDays);

                    if (first)
                    {
                        var expdata = _LeadExpenseInvestmentRepository.GetAll().Where(e => e.Id == item.LeadExpenseId).FirstOrDefault();

                        var dataVaultLog = new DataVaultActivityLog();
                        dataVaultLog.Action = "Updated";
                        dataVaultLog.SectionId = 1;
                        dataVaultLog.ActionNote = "Lead Expence Update : " + expdata.FromDate.ToString("dd-MM-yyyy") + " to " + expdata.ToDate.ToString("dd-MM-yyyy");
                        dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

                        if (item.organizationId != expdata.OrganizationId)
                        {
                            DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                            history.FieldName = "Organization";
                            history.PrevValue = _organizationUnitRepository.Get(expdata.OrganizationId).DisplayName;
                            history.CurValue = _organizationUnitRepository.Get((long)item.organizationId).DisplayName;
                            history.Action = "Update";
                            history.ActivityLogId = dataVaultLogId;
                            List.Add(history);
                        }
                        if (item.FromDate != expdata.FromDate)
                        {
                            DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                            history.FieldName = "FromDate";
                            history.PrevValue = expdata.FromDate.ToString("dd-MM-yyyy");
                            history.CurValue = item.FromDate.ToString("dd-MM-yyyy");
                            history.Action = "Update";
                            history.ActivityLogId = dataVaultLogId;
                            List.Add(history);
                        }
                        if (item.ToDate != expdata.ToDate)
                        {
                            DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                            history.FieldName = "ToDate";
                            history.PrevValue = expdata.ToDate.ToString("dd-MM-yyyy");
                            history.CurValue = item.ToDate.ToString("dd-MM-yyyy");
                            history.Action = "Update";
                            history.ActivityLogId = dataVaultLogId;
                            List.Add(history);
                        }

                        if ((float)item.TotalAmount != expdata.Investment)
                        {
                            DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                            history.FieldName = "TotalAmount";
                            history.PrevValue = expdata.Investment.ToString();
                            history.CurValue = item.TotalAmount.ToString();
                            history.Action = "Update";
                            history.ActivityLogId = dataVaultLogId;
                            List.Add(history);
                        }


                        expdata.FromDate = item.FromDate;
                        expdata.ToDate = item.ToDate;
                        expdata.MonthYear = item.ToDate.ToString("MMMM") + "-" + item.ToDate.Year.ToString();
                        expdata.Investment = (float)item.TotalAmount;

                        _LeadExpenseInvestmentRepository.Update(expdata);
                        investid = (int)item.LeadExpenseId;
                        first = false;
                    }

                    List<statedetail> stdetail = new List<statedetail>();
                    statedetail Nsw = new statedetail();
                    Nsw.StateId = 2;
                    Nsw.Amount = item.NswAmount;
                    Nsw.dailyamount = item.NswAmount / daydiff;
                    stdetail.Add(Nsw);
                    statedetail qld = new statedetail();
                    qld.StateId = 4;
                    qld.Amount = item.QldAmount;
                    qld.dailyamount = item.QldAmount / daydiff;
                    stdetail.Add(qld);
                    statedetail sa = new statedetail();
                    sa.StateId = 5;
                    sa.Amount = item.SaAmount;
                    sa.dailyamount = item.SaAmount / daydiff;
                    stdetail.Add(sa);
                    statedetail wa = new statedetail();
                    wa.StateId = 8;
                    wa.Amount = item.WaAmount;
                    wa.dailyamount = item.WaAmount / daydiff;
                    stdetail.Add(wa);
                    statedetail vic = new statedetail();
                    vic.StateId = 7;
                    vic.Amount = item.VICAmount;
                    vic.dailyamount = item.VICAmount / daydiff;
                    stdetail.Add(vic);
                    // var deletedt = _leadExpenseRepository.GetAll().Where(e => e.LeadinvestmentId == investid && e.LeadSourceId == item.LeadSourceId).ToList();
                    if (item.NswId > 0)
                    {
                        var exitdata = ListExpenseExist.Where(e => e.Id == item.NswId || e.Id == item.QldId || e.Id == item.SaId || e.Id == item.WaId || e.Id == item.VICId).ToList();
                        if(exitdata.Count > 0)
                        {
                            if (exitdata[0].LeadSourceId != item.LeadSourceId)
                            {
                                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                                history.FieldName = "LeadSource";
                                history.PrevValue = _lookup_leadSourceRepository.Get((int)exitdata[0].LeadSourceId).Name;
                                history.CurValue = _lookup_leadSourceRepository.Get((int)item.LeadSourceId).Name;
                                history.Action = "Update";
                                history.ActivityLogId = dataVaultLogId;
                                List.Add(history);
                            }

                            foreach (var dt in exitdata)
                            {
                                //decimal NswAmt = 0;
                                //decimal QldAmt = 0;
                                //decimal saAmt = 0;
                                //decimal waAmt = 0;

                                if (dt.StateId == 2) {
                                    if (dt.Amount != item.NswAmount)
                                    {
                                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                                        history.FieldName = "NSW";
                                        history.PrevValue = dt.Amount.ToString();
                                        history.CurValue = item.NswAmount.ToString();
                                        history.Action = "Update";
                                        history.ActivityLogId = dataVaultLogId;
                                        List.Add(history);
                                    } 
                                }
                                if (dt.StateId == 4) {
                                    if (dt.Amount != item.QldAmount)
                                    {
                                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                                        history.FieldName = "QLD";
                                        history.PrevValue = dt.Amount.ToString();
                                        history.CurValue = item.QldAmount.ToString();
                                        history.Action = "Update";
                                        history.ActivityLogId = dataVaultLogId;
                                        List.Add(history);
                                    }
                                }
                                if (dt.StateId == 5) {
                                    if (dt.Amount != item.SaAmount)
                                    {
                                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                                        history.FieldName = "SA";
                                        history.PrevValue = dt.Amount.ToString();
                                        history.CurValue = item.SaAmount.ToString();
                                        history.Action = "Update";
                                        history.ActivityLogId = dataVaultLogId;
                                        List.Add(history);
                                    }
                                }
                                if (dt.StateId == 8) {
                                    if (dt.Amount != item.WaAmount)
                                    {
                                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                                        history.FieldName = "WA";
                                        history.PrevValue = dt.Amount.ToString();
                                        history.CurValue = item.WaAmount.ToString();
                                        history.Action = "Update";
                                        history.ActivityLogId = dataVaultLogId;
                                        List.Add(history);                                        
                                    }
                                }

                                if (dt.StateId == 7)
                                {
                                    if (dt.Amount != item.VICAmount)
                                    {
                                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                                        history.FieldName = "VIC";
                                        history.PrevValue = dt.Amount.ToString();
                                        history.CurValue = item.VICAmount.ToString();
                                        history.Action = "Update";
                                        history.ActivityLogId = dataVaultLogId;
                                        List.Add(history);
                                    }
                                }


                            }
                        }
                    }
                    else
                    {
                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                        history.FieldName = "Item Added";
                        history.PrevValue = _lookup_leadSourceRepository.Get((int)item.LeadSourceId).Name;
                        history.CurValue = "";
                        history.Action = "Update";
                        history.ActivityLogId = dataVaultLogId;
                        List.Add(history);
                    }

                    //foreach (var ddata in deletedt)
                    //{
                    //    _leadExpenseRepository.HardDelete(ddata);

                    //}
                    for (int i = 0; i < stdetail.Count; i++)
                    {

                        LeadExpense obj = new LeadExpense();
                        obj.StateId = stdetail[i].StateId;
                        obj.Amount = stdetail[i].Amount;
                        obj.dailyamount = stdetail[i].dailyamount;
                        obj.LeadSourceId = item.LeadSourceId;
                        obj.FromDate = item.FromDate;
                        obj.ToDate = item.ToDate;
                        obj.LeadinvestmentId = investid;
                        if (AbpSession.TenantId != null)
                        {
                            obj.TenantId = (int?)AbpSession.TenantId;
                        }

                        _leadExpenseRepository.Insert(obj);
                    }

                }
            
                var deleteExpense = ListExpense.Where(e => !ids.Contains(e.Id)).ToList();

                foreach(var dt in deleteExpense)
                {
                    DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                    history.FieldName = "Item Deleted";
                    history.PrevValue = _lookup_leadSourceRepository.Get((int)dt.LeadSourceId).Name;
                    history.CurValue = "";
                    history.Action = "Update";
                    history.ActivityLogId = dataVaultLogId;
                    List.Add(history);
                }

                await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
                await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            }
            catch (Exception e) { throw e; }

        }
        //public async   Task<FileDto> getAllexpenseTableForReportExcel(GetAllexpenseInput input)
        //{
        //    try
        //    {
        //        int daydiff = (int)((Convert.ToDateTime(input.EndDate) - Convert.ToDateTime(input.StartDate)).TotalDays);
        //        decimal coltotallead = 0;
        //        decimal coltotalCost = 0;
        //        decimal colCostLead = 0;
        //        decimal colKwSold = 0;

        //        decimal colperKwCost = 0;
        //        decimal colProjectOpen = 0;

        //        decimal colProjectOpenpercent = 0;
        //        decimal colDeprecCount = 0;
        //        decimal colDeprecKWSold = 0;
        //        decimal colPannelsold = 0;
        //        decimal colCostpannel = 0;
        //        decimal rowtotallead = 0;
        //        decimal rowtotalCost = 0;
        //        decimal rowCostLead = 0;
        //        decimal rowKwSold = 0;

        //        decimal rowperKwCost = 0;
        //        decimal rowProjectOpen = 0;

        //        decimal rowProjectOpenpercent = 0;
        //        decimal rowDeprecCount = 0;
        //        decimal rowDeprecKWSold = 0;
        //        decimal rowPannelsold = 0;
        //        decimal rowCostpannel = 0;

        //        var fromdate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
        //        var edate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
        //        var entity = _lookup_stateRepository.GetAll()
        //            .WhereIf(input.states != null && input.states.Count() > 0, e => input.states.Contains((int)e.Id))
        //            .Where(e => e.IsReport == true).OrderBy(p => p.Id).ToList();
        //        var activity = _lookup_leadSourceRepository.GetAll()
        //             .WhereIf(input.leadSources != null && input.leadSources.Count() > 0, e => input.leadSources.Contains((int)e.Id))
        //             .OrderBy(p => p.Id).ToList();

        //        List<LeadExpenseReportDto> leadlist1 = new List<LeadExpenseReportDto>();
        //        foreach (var item in activity)
        //        {
        //            LeadExpenseReportDto statewiselist = new LeadExpenseReportDto();

        //            statewiselist.Leadsource = item.Name.ToString();
        //            coltotallead = 0;
        //            coltotalCost = 0;
        //            colCostLead = 0;
        //            colKwSold = 0;
        //            colperKwCost = 0;
        //            colProjectOpen = 0;
        //            colProjectOpenpercent = 0;
        //            colDeprecCount = 0;
        //            colDeprecKWSold = 0;
        //            colPannelsold = 0;
        //            colCostpannel = 0;
        //            List<StateForLeadExpenseReportDto> Statelist = new List<StateForLeadExpenseReportDto>();
        //            for (int j = 0; j < entity.Count; j++)
        //            {
        //                var categoryid = entity[j].Id;
        //                var statename = entity[j].Name;

        //                var linkdata = (from us in _lookup_leadSourceRepository.GetAll()
        //                                join Pa in _leadExpenseRepository.GetAll() on us.Id equals Pa.LeadSourceId into PaJoined
        //                                from Pa in PaJoined.DefaultIfEmpty()
        //                                where (Pa.StateId == categoryid && Pa.LeadSourceId == item.Id)
        //                                let kws = Convert.ToDecimal((from o in _jobRepository.GetAll()
        //                                                             join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
        //                                                             from i in j1
        //                                                             join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
        //                                                             from pay in inv1
        //                                                             where (i.CreationTime.AddHours(10).Date >= fromdate.Value.Date && i.CreationTime.AddHours(10).Date <= edate.Value.Date && i.LeadSourceId == item.Id && pay.InvoicePaymentStatusId == 2 && i.State == statename)
        //                                                             select o.SystemCapacity).Sum())

        //                                let project = ((from o in _jobRepository.GetAll()
        //                                                join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
        //                                                from i in j1
        //                                                join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
        //                                                from pay in inv1
        //                                                where (o.CreationTime.AddHours(10).Date >= fromdate.Value.Date && o.CreationTime.AddHours(10).Date <= edate.Value.Date && i.LeadSourceId == item.Id && i.State == statename)
        //                                                select (o.Id)).Count())


        //                                let depositerec = ((from o in _jobRepository.GetAll()
        //                                                    join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
        //                                                    from i in j1
        //                                                    join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
        //                                                    from pay in inv1
        //                                                    where (i.CreationTime.AddHours(10).Date >= fromdate.Value.Date && i.CreationTime.AddHours(10).Date <= edate.Value.Date && i.LeadSourceId == item.Id && pay.InvoicePaymentStatusId == 2 && i.State == statename)
        //                                                    select (o.Id)).Count())

        //                                let pannelsold = ((from o in _jobRepository.GetAll()
        //                                                   join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
        //                                                   from i in j1
        //                                                   join pay in _jobProductItemRepository.GetAll() on o.Id equals pay.JobId into inv1
        //                                                   from pay in inv1
        //                                                   join pnnl in _ProductItemRepository.GetAll() on pay.ProductItemId equals pnnl.Id into pnnls
        //                                                   from pnnl in pnnls
        //                                                   where (o.DepositeRecceivedDate.Value.AddHours(10).Date >= fromdate.Value.Date && o.DepositeRecceivedDate.Value.AddHours(10).Date <= edate.Value.Date && i.LeadSourceId == item.Id && o.State == statename && pnnl.ProductTypeId == 1)
        //                                                   select (pay.Quantity)).Sum())
        //                                select new
        //                                {
        //                                    stid = Pa.StateId,

        //                                    amount = Pa.Amount / daydiff,
        //                                    leadname = us.Name,
        //                                    leadid = Pa.Id,
        //                                    totallead = (_leadRepository.GetAll().Where(e => e.CreationTime.AddHours(10).Date >= fromdate.Value.Date && e.CreationTime.AddHours(10).Date <= edate.Value.Date && e.LeadSourceId == item.Id && e.State == statename).Select(e => e.Id).Count()),
        //                                    KWSold = kws > 0 ? kws : 0,
        //                                    perKwCost = kws > 0 && Pa.Amount > 0 ? kws / Pa.Amount : kws,
        //                                    openproject = project > 0 ? project : 0,
        //                                    openprojectper = project > 0 ? project / 100 : 0,
        //                                    DepositeRecived = depositerec > 0 ? depositerec : 0,
        //                                    PannelSold = pannelsold > 0 ? pannelsold : 0,
        //                                    depositeperc = Convert.ToDecimal(((from o in _jobRepository.GetAll()
        //                                                                       join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
        //                                                                       from i in j1
        //                                                                       join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
        //                                                                       from pay in inv1
        //                                                                       where (i.CreationTime.AddHours(10).Date >= fromdate.Value.Date && i.CreationTime.AddHours(10).Date <= edate.Value.Date && i.LeadSourceId == item.Id && pay.InvoicePaymentStatusId == 2 && i.State == statename && ((_invoicePaymentRepository.GetAll().Where(x => x.JobId == o.Id).Select(e => e.InvoicePayTotal).Sum()) != o.TotalCost))
        //                                                                       select (o.SystemCapacity)).Sum()))

        //                                }).FirstOrDefault();
        //                if (linkdata != null)
        //                {
        //                    StateForLeadExpenseReportDto st = new StateForLeadExpenseReportDto();
        //                    st.statename = statename;
        //                    st.totallead = Convert.ToInt32(linkdata.totallead);
        //                    st.totalCost = Math.Round(linkdata.amount);
        //                    st.CostLead = st.totallead > 0 ? Math.Round(Convert.ToDecimal(st.totalCost / st.totallead), 2) : 0;
        //                    st.KwSold = Math.Round(Convert.ToDecimal(linkdata.KWSold), 2);
        //                    //st.perKwCost = Math.Round(Convert.ToDecimal(linkdata.perKwCost), 2);
        //                    st.perKwCost = st.KwSold > 0 ? Math.Round(Convert.ToDecimal(st.totalCost / st.KwSold), 2) : 0;
        //                    st.ProjectOpen = Math.Round(Convert.ToDecimal(linkdata.openproject), 2);
        //                    //st.ProjectOpenpercent = Math.Round(Convert.ToDecimal(linkdata.openprojectper), 2);
        //                    st.ProjectOpenpercent = st.totallead > 0 ? Math.Round(Convert.ToDecimal((st.ProjectOpen / st.totallead) * 100), 2) : 0;
        //                    st.DeprecCount = Math.Round(Convert.ToDecimal(linkdata.DepositeRecived), 2);
        //                    st.DeprecKWSold = st.totallead > 0 ? Math.Round(Convert.ToDecimal((st.DeprecCount / st.totallead) * 100), 2) : 0;
        //                    st.Pannelsold = Convert.ToInt32(linkdata.PannelSold);
        //                    st.Costpannel = st.Pannelsold > 0 ? Math.Round(Convert.ToDecimal(st.totalCost / st.Pannelsold), 2) : 0;
        //                    coltotallead = coltotallead + st.totallead;
        //                    coltotalCost = coltotalCost + st.totalCost;
        //                    colCostLead = colCostLead + st.CostLead;
        //                    colKwSold = colKwSold + st.KwSold;
        //                    colperKwCost = colperKwCost + st.perKwCost;
        //                    colProjectOpen = colProjectOpen + st.ProjectOpen;
        //                    colProjectOpenpercent = colProjectOpenpercent + st.ProjectOpenpercent;
        //                    colDeprecCount = colDeprecCount + st.DeprecCount;
        //                    colDeprecKWSold = colDeprecKWSold + st.DeprecKWSold;
        //                    colPannelsold = colPannelsold + st.Pannelsold;
        //                    colCostpannel = colCostpannel + st.Costpannel;

        //                    Statelist.Add(st);
        //                }
        //                else
        //                {
        //                    StateForLeadExpenseReportDto st = new StateForLeadExpenseReportDto();
        //                    st.statename = statename;
        //                    st.totallead = 0;
        //                    st.totalCost = 0;
        //                    st.CostLead = 0;
        //                    st.KwSold = 0;
        //                    //st.perKwCost = Math.Round(Convert.ToDecimal(linkdata.perKwCost), 2);
        //                    st.perKwCost = 0;
        //                    st.ProjectOpen = 0;
        //                    //st.ProjectOpenpercent = Math.Round(Convert.ToDecimal(linkdata.openprojectper), 2);
        //                    st.ProjectOpenpercent = 0;
        //                    st.DeprecCount = 0;
        //                    st.DeprecKWSold = 0;
        //                    st.Pannelsold = 0;
        //                    st.Costpannel = 0;
        //                    Statelist.Add(st);
        //                }


        //            }

        //            StateForLeadExpenseReportDto sttotal = new StateForLeadExpenseReportDto();
        //            sttotal.statename = "Total";
        //            sttotal.totallead = coltotallead;
        //            sttotal.totalCost = coltotalCost;
        //            sttotal.CostLead = coltotalCost > 0 ? Math.Round((coltotalCost / coltotallead), 2) : 0;
        //            sttotal.KwSold = colKwSold;
        //            sttotal.perKwCost = colperKwCost > 0 ? Math.Round((coltotalCost / colperKwCost), 2) : 0;
        //            sttotal.ProjectOpen = colProjectOpen;
        //            sttotal.ProjectOpenpercent = colProjectOpen > 0 ? Math.Round((colProjectOpen / coltotalCost), 2) : 0; ;
        //            sttotal.DeprecCount = colDeprecCount;
        //            sttotal.DeprecKWSold = coltotalCost > 0 ? Math.Round(Convert.ToDecimal((colDeprecCount / coltotallead) * 100), 2) : 0;
        //            sttotal.Pannelsold = (int)colPannelsold;
        //            sttotal.Costpannel = colPannelsold > 0 ? Math.Round(Convert.ToDecimal(coltotalCost / colPannelsold), 2) : 0;
        //            Statelist.Add(sttotal);
        //            statewiselist.leadstates = Statelist;

        //            leadlist1.Add(statewiselist);
        //        }
        //        decimal nswtotallead = 0;
        //        decimal nswtotalCost = 0;
        //        decimal nswwCostLead = 0;
        //        decimal rownswKwSold = 0;
        //        decimal nswcolCostLead = 0;
        //        decimal nswcolKwSold = 0;
        //        decimal nswcolProjectOpen = 0;
        //        decimal nswcolperKwSold = 0;
        //        decimal nswcolProjectOpenpercent = 0;
        //        decimal nswcolDeprecCount = 0;
        //        decimal nswcolDeprecKWSold = 0;
        //        decimal nswcolPannelsold = 0;
        //        decimal nswcolCostpannel = 0;

        //        decimal qldtotallead = 0;
        //        decimal qldtotalCost = 0;
        //        decimal qldcolCostLead = 0;
        //        decimal qldcolKwSold = 0;
        //        decimal qldcolperKwSold = 0;
        //        decimal qldcolperKwCost = 0;
        //        decimal qldcolProjectOpen = 0;

        //        decimal qldcolProjectOpenpercent = 0;
        //        decimal qldcolDeprecCount = 0;
        //        decimal qldcolDeprecKWSold = 0;
        //        decimal qldcolPannelsold = 0;
        //        decimal qldcolCostpannel = 0;

        //        decimal satotallead = 0;
        //        decimal satotalCost = 0;
        //        decimal sacolCostLead = 0;
        //        decimal sacolKwSold = 0;
        //        decimal sacolperKwSold = 0;
        //        decimal sacolperKwCost = 0;
        //        decimal sacolProjectOpen = 0;

        //        decimal sacolProjectOpenpercent = 0;
        //        decimal sacolDeprecCount = 0;
        //        decimal sacolDeprecKWSold = 0;
        //        decimal sacolPannelsold = 0;
        //        decimal sacolCostpannel = 0;

        //        decimal watotallead = 0;
        //        decimal watotalCost = 0;
        //        decimal wacolCostLead = 0;
        //        decimal wacolKwSold = 0;
        //        decimal wacolperKwSold = 0;
        //        decimal wacolperKwCost = 0;
        //        decimal wacolProjectOpen = 0;

        //        decimal wacolProjectOpenpercent = 0;
        //        decimal wacolDeprecCount = 0;
        //        decimal wacolDeprecKWSold = 0;
        //        decimal wacolPannelsold = 0;
        //        decimal wacolCostpannel = 0;


        //        decimal fintotallead = 0;
        //        decimal fintotalCost = 0;
        //        decimal fincolCostLead = 0;
        //        decimal fincolKwSold = 0;
        //        decimal fincolperKwSold = 0;
        //        decimal fincolperKwCost = 0;
        //        decimal fincolProjectOpen = 0;

        //        decimal fincolProjectOpenpercent = 0;
        //        decimal fincolDeprecCount = 0;
        //        decimal fincolDeprecKWSold = 0;
        //        decimal fincolPannelsold = 0;
        //        decimal fincolCostpannel = 0;

        //        for (int m = 0; m < leadlist1.Count; m++)
        //        {
        //            for (int k = 0; k < leadlist1[m].leadstates.Count; k++)
        //            {
        //                if (leadlist1[m].leadstates[k].statename == "NSW")
        //                {


        //                    nswtotalCost = nswtotalCost + leadlist1[m].leadstates[k].totalCost;

        //                    nswtotallead = nswtotallead + leadlist1[m].leadstates[k].totallead;

        //                    nswcolCostLead = nswtotalCost + leadlist1[m].leadstates[k].totalCost;

        //                    nswcolKwSold = nswcolKwSold + leadlist1[m].leadstates[k].KwSold;

        //                    nswcolProjectOpen = nswcolProjectOpen + leadlist1[m].leadstates[k].ProjectOpen;

        //                    nswcolProjectOpenpercent = nswcolProjectOpenpercent + leadlist1[m].leadstates[k].ProjectOpenpercent;

        //                    nswcolDeprecCount = nswcolDeprecCount + leadlist1[m].leadstates[k].DeprecCount;

        //                    nswcolDeprecKWSold = nswcolDeprecKWSold + leadlist1[m].leadstates[k].DeprecKWSold;

        //                    nswcolPannelsold = nswcolPannelsold + leadlist1[m].leadstates[k].Pannelsold;

        //                    nswcolCostpannel = nswcolCostpannel + leadlist1[m].leadstates[k].Costpannel;


        //                }

        //                if (leadlist1[m].leadstates[k].statename == "QLD")
        //                {
        //                    qldtotalCost = qldtotalCost + leadlist1[m].leadstates[k].totalCost;

        //                    qldtotallead = qldtotallead + leadlist1[m].leadstates[k].totallead;

        //                    qldcolCostLead = qldtotalCost + leadlist1[m].leadstates[k].totalCost;

        //                    qldcolKwSold = qldcolKwSold + leadlist1[m].leadstates[k].KwSold;

        //                    qldcolProjectOpen = qldcolProjectOpen + leadlist1[m].leadstates[k].ProjectOpen;

        //                    qldcolProjectOpenpercent = qldcolProjectOpenpercent + leadlist1[m].leadstates[k].ProjectOpenpercent;

        //                    qldcolDeprecCount = qldcolDeprecCount + leadlist1[m].leadstates[k].DeprecCount;

        //                    qldcolDeprecKWSold = qldcolDeprecKWSold + leadlist1[m].leadstates[k].DeprecKWSold;

        //                    qldcolPannelsold = qldcolPannelsold + leadlist1[m].leadstates[k].Pannelsold;

        //                    qldcolCostpannel = qldcolCostpannel + leadlist1[m].leadstates[k].Costpannel;


        //                }
        //                if (leadlist1[m].leadstates[k].statename == "SA")
        //                {

        //                    satotalCost = satotalCost + leadlist1[m].leadstates[k].totalCost;

        //                    satotallead = satotallead + leadlist1[m].leadstates[k].totallead;

        //                    sacolCostLead = satotalCost + leadlist1[m].leadstates[k].totalCost;

        //                    sacolKwSold = sacolKwSold + leadlist1[m].leadstates[k].KwSold;

        //                    sacolProjectOpen = sacolProjectOpen + leadlist1[m].leadstates[k].ProjectOpen;

        //                    sacolProjectOpenpercent = sacolProjectOpenpercent + leadlist1[m].leadstates[k].ProjectOpenpercent;

        //                    sacolDeprecCount = sacolDeprecCount + leadlist1[m].leadstates[k].DeprecCount;

        //                    sacolDeprecKWSold = sacolDeprecKWSold + leadlist1[m].leadstates[k].DeprecKWSold;

        //                    sacolPannelsold = sacolPannelsold + leadlist1[m].leadstates[k].Pannelsold;

        //                    sacolCostpannel = sacolCostpannel + leadlist1[m].leadstates[k].Costpannel;
        //                }
        //                if (leadlist1[m].leadstates[k].statename == "WA")
        //                {

        //                    watotalCost = watotalCost + leadlist1[m].leadstates[k].totalCost;

        //                    watotallead = watotallead + leadlist1[m].leadstates[k].totallead;

        //                    wacolCostLead = watotalCost + leadlist1[m].leadstates[k].totalCost;

        //                    wacolKwSold = wacolKwSold + leadlist1[m].leadstates[k].KwSold;

        //                    wacolProjectOpen = wacolProjectOpen + leadlist1[m].leadstates[k].ProjectOpen;

        //                    wacolProjectOpenpercent = wacolProjectOpenpercent + leadlist1[m].leadstates[k].ProjectOpenpercent;

        //                    wacolDeprecCount = wacolDeprecCount + leadlist1[m].leadstates[k].DeprecCount;

        //                    wacolDeprecKWSold = wacolDeprecKWSold + leadlist1[m].leadstates[k].DeprecKWSold;

        //                    wacolPannelsold = wacolPannelsold + leadlist1[m].leadstates[k].Pannelsold;

        //                    wacolCostpannel = wacolCostpannel + leadlist1[m].leadstates[k].Costpannel;
        //                }
        //                if (leadlist1[m].leadstates[k].statename == "Total")
        //                {

        //                    fintotalCost = fintotalCost + leadlist1[m].leadstates[k].totalCost;

        //                    fintotallead = fintotallead + leadlist1[m].leadstates[k].totallead;

        //                    fincolCostLead = fintotalCost + leadlist1[m].leadstates[k].totalCost;

        //                    fincolKwSold = fincolKwSold + leadlist1[m].leadstates[k].KwSold;

        //                    fincolProjectOpen = fincolProjectOpen + leadlist1[m].leadstates[k].ProjectOpen;

        //                    fincolProjectOpenpercent = fincolProjectOpenpercent + leadlist1[m].leadstates[k].ProjectOpenpercent;

        //                    fincolDeprecCount = fincolDeprecCount + leadlist1[m].leadstates[k].DeprecCount;

        //                    fincolDeprecKWSold = fincolDeprecKWSold + leadlist1[m].leadstates[k].DeprecKWSold;

        //                    fincolPannelsold = fincolPannelsold + leadlist1[m].leadstates[k].Pannelsold;

        //                    fincolCostpannel = fincolCostpannel + leadlist1[m].leadstates[k].Costpannel;
        //                }
        //            }


        //        }
        //        List<StateForLeadExpenseReportDto> Statelist1 = new List<StateForLeadExpenseReportDto>();
        //        for (int p = 0; p < entity.Count + 1; p++)
        //        {
        //            StateForLeadExpenseReportDto st = new StateForLeadExpenseReportDto();
        //            if (p == entity.Count)
        //            {
        //                //  st.statename = statename;{
        //                st.totallead = fintotallead;
        //                st.totalCost = Math.Round(fintotalCost);
        //                st.CostLead = fincolCostLead;
        //                st.KwSold = fincolKwSold;

        //                st.perKwCost = fincolperKwSold;
        //                st.ProjectOpen = Math.Round(Convert.ToDecimal(fincolProjectOpen), 2);

        //                st.ProjectOpenpercent = fincolProjectOpenpercent;
        //                st.DeprecCount = fincolDeprecCount;
        //                st.DeprecKWSold = fincolDeprecKWSold;
        //                st.Pannelsold = (int)fincolPannelsold;
        //                st.Costpannel = fincolCostpannel;
        //                Statelist1.Add(st);
        //            }
        //            else if (p == 0)
        //            {
        //                //  st.statename = statename;{
        //                st.totallead = nswtotallead;
        //                st.totalCost = Math.Round(nswtotalCost);
        //                st.CostLead = nswwCostLead;
        //                st.KwSold = nswcolKwSold;

        //                st.perKwCost = nswcolperKwSold;
        //                st.ProjectOpen = Math.Round(Convert.ToDecimal(nswcolProjectOpen), 2);

        //                st.ProjectOpenpercent = nswcolProjectOpenpercent;
        //                st.DeprecCount = nswcolDeprecCount;
        //                st.DeprecKWSold = nswcolDeprecKWSold;
        //                st.Pannelsold = (int)nswcolPannelsold;
        //                st.Costpannel = nswcolCostpannel;
        //                Statelist1.Add(st);
        //            }
        //            else if (p == 1)
        //            {
        //                //  st.statename = statename;{
        //                st.totallead = qldtotallead;
        //                st.totalCost = Math.Round(qldtotalCost);
        //                st.CostLead = qldcolCostLead;
        //                st.KwSold = qldcolKwSold;

        //                st.perKwCost = qldcolperKwSold;
        //                st.ProjectOpen = Math.Round(Convert.ToDecimal(qldcolProjectOpen), 2);

        //                st.ProjectOpenpercent = qldcolProjectOpenpercent;
        //                st.DeprecCount = qldcolDeprecCount;
        //                st.DeprecKWSold = qldcolDeprecKWSold;
        //                st.Pannelsold = (int)qldcolPannelsold;
        //                st.Costpannel = qldcolCostpannel;
        //                Statelist1.Add(st);
        //            }
        //            else if (p == 2)
        //            {
        //                //  st.statename = statename;{
        //                st.totallead = satotallead;
        //                st.totalCost = Math.Round(satotalCost);
        //                st.CostLead = sacolCostLead;
        //                st.KwSold = sacolKwSold;

        //                st.perKwCost = sacolperKwSold;
        //                st.ProjectOpen = Math.Round(Convert.ToDecimal(sacolProjectOpen), 2);

        //                st.ProjectOpenpercent = sacolProjectOpenpercent;
        //                st.DeprecCount = sacolDeprecCount;
        //                st.DeprecKWSold = sacolDeprecKWSold;
        //                st.Pannelsold = (int)sacolPannelsold;
        //                st.Costpannel = sacolCostpannel;
        //                Statelist1.Add(st);
        //            }
        //            else if (p == 3)
        //            {
        //                //  st.statename = statename;{
        //                st.totallead = watotallead;
        //                st.totalCost = Math.Round(watotalCost);
        //                st.CostLead = wacolCostLead;
        //                st.KwSold = wacolKwSold;

        //                st.perKwCost = wacolperKwSold;
        //                st.ProjectOpen = Math.Round(Convert.ToDecimal(wacolProjectOpen), 2);

        //                st.ProjectOpenpercent = wacolProjectOpenpercent;
        //                st.DeprecCount = wacolDeprecCount;
        //                st.DeprecKWSold = wacolDeprecKWSold;
        //                st.Pannelsold = (int)wacolPannelsold;
        //                st.Costpannel = wacolCostpannel;
        //                Statelist1.Add(st);
        //            }

        //        }
        //        LeadExpenseReportDto statewiselist1 = new LeadExpenseReportDto();
        //        statewiselist1.Leadsource = "Total";
        //        statewiselist1.leadstates = Statelist1;


        //        leadlist1.Add(statewiselist1);
        //       // return leadlist1;
        //        return _leadsExcelExporter.LeadExpenseExportToFile(leadlist1);

        //    }
        //    catch (Exception e) { throw e; }
        //}
        public async Task<FileDto> getAllexpenseTableForReportExcel(string id)
        {
            try
            {

                // return leadlist1;
                return _leadsExcelExporter.LeadExpenseExportToFile(id);

            }
            catch (Exception e) { throw e; }
        }
        public async Task deleteExpense(int leadid, int leadexpenseid)
        {
            try
            {
                int id = Convert.ToInt32(leadexpenseid);

                var deletedt = _leadExpenseRepository.GetAll().Where(e => e.LeadinvestmentId == id && e.LeadSourceId == leadid).ToList();
                foreach (var ddata in deletedt)
                {
                    _leadExpenseRepository.HardDelete(ddata);

                }

            }
            catch (Exception e) { throw e; }

        }
        public GetLeadExpenseInvestmentsDto LeadExpenseExsistenceById(string monthYear)
        {
            GetLeadExpenseInvestmentsDto items = new GetLeadExpenseInvestmentsDto();
            var Isexsistdata = _LeadExpenseInvestmentRepository.GetAll().Where(x => x.MonthYear == monthYear).Any();
            if (Isexsistdata)
            {
                items = (from lead in _LeadExpenseInvestmentRepository.GetAll()
                         where lead.MonthYear == monthYear
                         select new GetLeadExpenseInvestmentsDto
                         {
                             Id = lead.Id,
                             OrganizationId = lead.OrganizationId,
                             FromDate = lead.FromDate,
                             ToDate = lead.ToDate,
                             MonthYear = lead.MonthYear,
                             Investment = lead.Investment,
                             Isexsistdata = Isexsistdata
                         }).FirstOrDefault();

                //items = _LeadExpenseInvestmentRepository.GetAll().Where(x => x.MonthYear == monthYear).FirstOrDefault();
            }
            return items;
        }

        public async Task<FileDto> getAllexpenseTableForReportExport(GetAllexpenseInput input)
        {
            try
            {
                var fromdate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var edate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

                int daydiff = (int)((Convert.ToDateTime(edate) - Convert.ToDateTime(fromdate)).TotalDays);
                var coltotallead = 0;
                decimal coltotalCost = 0;
                decimal colCostLead = 0;
                decimal colKwSold = 0;

                decimal colperKwCost = 0;


                decimal colProjectOpen = 0;

                decimal colProjectOpenpercent = 0;
                decimal colDeprecCount = 0;
                decimal colDeprecKWSold = 0;
                decimal colPannelsold = 0;
                decimal colCostpannel = 0;
                var rowtotallead = 0;
                decimal rowtotalCost = 0;
                decimal rowCostLead = 0;
                decimal rowKwSold = 0;

                decimal rowperKwCost = 0;
                decimal rowProjectOpen = 0;

                decimal rowProjectOpenpercent = 0;
                decimal rowDeprecCount = 0;
                decimal rowDeprecKWSold = 0;
                decimal rowPannelsold = 0;
                decimal rowCostpannel = 0;

                decimal spend = 0;
                decimal kws = 0;
                decimal project = 0;
                decimal depositerec = 0;
                decimal pannelsold = 0;
                decimal spenddt = 0;

                var orgnize = _leadSourceOrganizationUnitRepository.GetAll()
                    .WhereIf(input.leadSources != null && input.leadSources.Count() > 0, e => input.leadSources.Contains((int)e.LeadSourceId))
                    .Where(e => e.OrganizationUnitId == input.organizationId).Select(e => e.LeadSourceId).ToList();
                var entity = _lookup_stateRepository.GetAll()
                    .WhereIf(input.states != null && input.states.Count() > 0, e => input.states.Contains((int)e.Id))
                    .Where(e => e.IsReport == true).OrderBy(p => p.Id).ToList();
                var activity = _lookup_leadSourceRepository.GetAll()
                     .WhereIf(input.leadSources != null && input.leadSources.Count() > 0, e => input.leadSources.Contains((int)e.Id))
                     .Where(e => orgnize.Contains(e.Id))
                     .OrderBy(p => p.Id).ToList();
                var stateList = entity.Select(e => e.Name).ToList();
                var stateIdList = entity.Select(e => e.Id).ToList();
                List<LeadExpenseReportDto> leadlist1 = new List<LeadExpenseReportDto>();
                foreach (var item in activity)
                {
                    LeadExpenseReportDto statewiselist = new LeadExpenseReportDto();

                    statewiselist.Leadsource = item.Name.ToString();
                    coltotallead = 0;
                    coltotalCost = 0;
                    colCostLead = 0;
                    colKwSold = 0;
                    colperKwCost = 0;
                    colProjectOpen = 0;
                    colProjectOpenpercent = 0;
                    colDeprecCount = 0;
                    colDeprecKWSold = 0;
                    colPannelsold = 0;
                    colCostpannel = 0;
                    int firstdt = 0;

                    spend = 0;
                    kws = 0;
                    project = 0;
                    depositerec = 0;
                    pannelsold = 0;
                    spenddt = 0;

                    List<StateForLeadExpenseReportDto> Statelist = new List<StateForLeadExpenseReportDto>();
                    for (int j = 0; j < entity.Count; j++)
                    {
                        var categoryid = entity[j].Id;
                        var statename = entity[j].Name;

                        spend = Convert.ToDecimal((from l in _lookup_leadSourceRepository.GetAll()
                                                   join Pa in _leadExpenseRepository.GetAll() on l.Id equals Pa.LeadSourceId into PaJoined
                                                   from Pa in PaJoined.DefaultIfEmpty()
                                                   where (Pa.StateId == categoryid && Pa.LeadSourceId == item.Id && Pa.FromDate.Date >= fromdate.Value.Date && Pa.ToDate.Date <= edate)
                                                   select (Pa.Amount)).Sum());

                        kws = Convert.ToDecimal((from o in _jobRepository.GetAll()
                                                 join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                                 from i in j1
                                                     //join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
                                                     //from pay in inv1
                                                 where (o.FirstDepositDate.Value.Date >= fromdate.Value.Date && o.FirstDepositDate.Value.Date <= edate.Value.Date && i.LeadSourceId == item.Id && i.State == statename && i.OrganizationId == input.organizationId)
                                                 select (o.SystemCapacity)).Sum());
                        project = ((from o in _jobRepository.GetAll()
                                    join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                    from i in j1
                                        //join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
                                        //from pay in inv1
                                    where (o.CreationTime.AddHours(10).Date >= fromdate.Value.Date && o.CreationTime.AddHours(10).Date <= edate.Value.Date && i.LeadSourceId == item.Id && i.State == statename && i.OrganizationId == input.organizationId)
                                    select (o.Id)).Count());

                        depositerec = ((from o in _jobRepository.GetAll()
                                        join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                        from i in j1
                                            //join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
                                            //from pay in inv1
                                        where (o.DepositeRecceivedDate.Value.Date >= fromdate.Value.Date && o.DepositeRecceivedDate.Value.Date <= edate.Value.Date && i.LeadSourceId == item.Id && i.State == statename && i.OrganizationId == input.organizationId)
                                        select (o.Id)).Count());

                        pannelsold = Convert.ToDecimal((from o in _jobRepository.GetAll()
                                                        join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                                        from i in j1
                                                        join pay in _jobProductItemRepository.GetAll() on o.Id equals pay.JobId into inv1
                                                        from pay in inv1
                                                        join pnnl in _ProductItemRepository.GetAll() on pay.ProductItemId equals pnnl.Id into pnnls
                                                        from pnnl in pnnls
                                                        where (o.DepositeRecceivedDate.Value.AddHours(10).Date >= fromdate.Value.Date && o.DepositeRecceivedDate.Value.AddHours(10).Date <= edate.Value.Date && i.LeadSourceId == item.Id && o.State == statename && pnnl.ProductTypeId == 1 && i.OrganizationId == input.organizationId)
                                                        select (pay.Quantity)).Sum());

                        spenddt = spend != null ? spend : 0;
                        var linkdata = (from us in _lookup_leadSourceRepository.GetAll()
                                        where us.Id == (_lookup_leadSourceRepository.GetAll().Select(e => e.Id).FirstOrDefault())

                                        select new
                                        {
                                            //stid = 1,
                                            // daydif = (int)((Convert.ToDateTime(Pa.ToDate) - Convert.ToDateTime(Pa.FromDate)).TotalDays),
                                            // dailydiff= Pa.Amount / (int)((Convert.ToDateTime(Pa.ToDate) - Convert.ToDateTime(Pa.FromDate)).TotalDays),
                                            //  amount = (Pa.Amount / (int)((Convert.ToDateTime(Pa.ToDate) - Convert.ToDateTime(Pa.FromDate)).TotalDays))*daydiff,
                                            //amount = Pa.dailyamount * daydiff,
                                            amount = spenddt,
                                            leadname = us.Name,
                                            //leadid = 1,
                                            //totallead = (_leadRepository.GetAll().Where(e => e.CreationTime.AddHours(10).Date >= fromdate.Value.Date && e.CreationTime.AddHours(10).Date <= edate.Value.Date && e.LeadSourceId == item.Id && e.State == statename && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.organizationId).Select(e => e.Id).Count()),
                                            totallead = (_leadRepository.GetAll().Where(e => e.CreationTime.AddHours(10).Date >= fromdate.Value.Date && e.CreationTime.AddHours(10).Date <= edate.Value.Date && e.LeadSourceId == item.Id && e.State == statename && e.HideDublicate != true && e.OrganizationId == input.organizationId).Select(e => e.Id).Count()),
                                            KWSold = kws > 0 ? kws : 0,
                                            perKwCost = kws > 0 && spenddt > 0 ? kws / spenddt : kws,
                                            openproject = project > 0 ? project : 0,
                                            openprojectper = project > 0 ? project / 100 : 0,
                                            DepositeRecived = depositerec > 0 ? depositerec : 0,
                                            PannelSold = pannelsold > 0 ? pannelsold : 0,

                                            depositeperc = Convert.ToDecimal(((from o in _jobRepository.GetAll()
                                                                               join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                                                               from i in j1
                                                                               join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
                                                                               from pay in inv1
                                                                               where (o.DepositeRecceivedDate.Value.Date >= fromdate.Value.Date && o.DepositeRecceivedDate.Value.Date <= edate.Value.Date && i.LeadSourceId == item.Id && i.State == statename && ((_invoicePaymentRepository.GetAll().Where(x => x.JobId == o.Id).Select(e => e.InvoicePayTotal).Sum()) != o.TotalCost) && i.OrganizationId == input.organizationId)
                                                                               select (o.SystemCapacity)).Sum())),


                                        }).FirstOrDefault();



                        //coltotallead = (_leadRepository.GetAll().Where(e => e.CreationTime.AddHours(10).Date >= fromdate.Value.Date && e.CreationTime.AddHours(10).Date <= edate.Value.Date && e.LeadSourceId == item.Id && e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationId == input.organizationId).Select(e => e.Id).Count());
                        coltotallead = (_leadRepository.GetAll().Where(e => e.CreationTime.AddHours(10).Date >= fromdate.Value.Date && e.CreationTime.AddHours(10).Date <= edate.Value.Date && e.LeadSourceId == item.Id && e.HideDublicate != true && e.OrganizationId == input.organizationId && stateList.Contains(e.State)).Select(e => e.Id).Count());
                        coltotalCost = ((from us in _lookup_leadSourceRepository.GetAll()
                                         join Pa in _leadExpenseRepository.GetAll() on us.Id equals Pa.LeadSourceId into PaJoined
                                         from Pa in PaJoined.DefaultIfEmpty()
                                         where (Pa.LeadSourceId == item.Id && Pa.FromDate.Date >= fromdate.Value.Date && Pa.ToDate.Date <= edate)
                                         && stateIdList.Contains((int)Pa.StateId)
                                         // select (Pa.dailyamount)).Sum()) * daydiff;
                                         select (Pa.Amount)).Sum());
                        colCostLead = coltotalCost > 0 && coltotallead > 0 ? Math.Round(coltotalCost / coltotallead, 2) : 0;
                        //colKwSold = Convert.ToDecimal((from o in _jobRepository.GetAll()
                        //                               join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                        //                               from i in j1
                        //                               join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
                        //                               from pay in inv1
                        //                               join propay in _jobProductItemRepository.GetAll() on o.Id equals propay.JobId into inv2
                        //                               from propay in inv2
                        //                               join pnnl in _ProductItemRepository.GetAll() on propay.ProductItemId equals pnnl.Id into pnnls
                        //                               from pnnl in pnnls
                        //                               where (o.DepositeRecceivedDate.Value.AddHours(10).Date >= fromdate.Value.Date && o.DepositeRecceivedDate.Value.AddHours(10).Date <= edate.Value.Date && i.LeadSourceId == item.Id && pnnl.ProductTypeId == 1 && i.OrganizationId == input.organizationId)
                        //                               select o.SystemCapacity).Sum());

                        colKwSold = Convert.ToDecimal((from o in _jobRepository.GetAll()
                                                       join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                                       from i in j1
                                                           //join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
                                                           //from pay in inv1
                                                       where (o.FirstDepositDate.Value.Date >= fromdate.Value.Date && o.FirstDepositDate.Value.Date <= edate.Value.Date && i.LeadSourceId == item.Id && i.OrganizationId == input.organizationId && stateList.Contains(i.State))
                                                       select (o.SystemCapacity)).Sum());


                        colperKwCost = coltotalCost > 0 && colKwSold > 0 ? Math.Round(coltotalCost / colKwSold, 2) : 0;
                        colProjectOpen = ((from o in _jobRepository.GetAll()
                                           join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                           from i in j1
                                           where (o.CreationTime.AddHours(10).Date >= fromdate.Value.Date && o.CreationTime.AddHours(10).Date <= edate.Value.Date && i.LeadSourceId == item.Id && i.OrganizationId == input.organizationId && stateList.Contains(i.State))
                                           select (o.Id)).Count());
                        colProjectOpenpercent = colProjectOpen > 0 && coltotallead > 0 ? Math.Round(Convert.ToDecimal(colProjectOpen / coltotallead * 100), 2) : 0;

                        colDeprecCount = ((from o in _jobRepository.GetAll()
                                           join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                           from i in j1
                                               //join pay in _invoicePaymentRepository.GetAll() on o.Id equals pay.JobId into inv1
                                               //from pay in inv1
                                           where (o.DepositeRecceivedDate.Value.Date >= fromdate.Value.Date && o.DepositeRecceivedDate.Value.Date <= edate.Value.Date && i.LeadSourceId == item.Id && i.OrganizationId == input.organizationId && stateList.Contains(i.State))
                                           select (o.Id)).Count());
                        colDeprecKWSold = colDeprecCount > 0 && coltotallead > 0 ? Math.Round(Convert.ToDecimal((colDeprecCount / coltotallead) * 100), 2) : 0;
                        colPannelsold = Convert.ToDecimal(((from o in _jobRepository.GetAll()
                                                            join i in _leadRepository.GetAll() on o.LeadId equals i.Id into j1
                                                            from i in j1
                                                            join pay in _jobProductItemRepository.GetAll() on o.Id equals pay.JobId into inv1
                                                            from pay in inv1
                                                            join pnnl in _ProductItemRepository.GetAll() on pay.ProductItemId equals pnnl.Id into pnnls
                                                            from pnnl in pnnls
                                                            where (o.DepositeRecceivedDate.Value.Date >= fromdate.Value.Date && o.DepositeRecceivedDate.Value.Date <= edate.Value.Date && i.LeadSourceId == item.Id && pnnl.ProductTypeId == 1 && i.OrganizationId == input.organizationId && stateList.Contains(i.State))
                                                            select (pay.Quantity)).Sum()));
                        colCostpannel = coltotalCost > 0 && colPannelsold > 0 ? Math.Round(Convert.ToDecimal((coltotalCost / colPannelsold)), 2) : 0;
                        if (firstdt == 0)
                        {
                            StateForLeadExpenseReportDto sttotal = new StateForLeadExpenseReportDto();
                            sttotal.statename = "Total";
                            sttotal.totallead = coltotallead;
                            sttotal.totalCost = coltotalCost;
                            sttotal.CostLead = colCostLead;
                            sttotal.KwSold = colKwSold;
                            sttotal.perKwCost = colperKwCost;
                            sttotal.ProjectOpen = colProjectOpen;
                            sttotal.ProjectOpenpercent = colProjectOpenpercent;
                            sttotal.DeprecCount = colDeprecCount;
                            sttotal.DeprecKWSold = colDeprecKWSold;
                            sttotal.Pannelsold = (int)colPannelsold;
                            sttotal.Costpannel = colCostpannel;
                            Statelist.Add(sttotal);
                            firstdt = 1;
                        }
                        if (linkdata != null)
                        {
                            StateForLeadExpenseReportDto st = new StateForLeadExpenseReportDto();
                            st.statename = statename;
                            st.totallead = Convert.ToInt32(linkdata.totallead);
                            st.totalCost = Math.Round(linkdata.amount);
                            st.CostLead = st.totallead > 0 ? Math.Round(Convert.ToDecimal(st.totalCost / st.totallead), 2) : 0;
                            st.KwSold = Math.Round(Convert.ToDecimal(linkdata.KWSold), 2);
                            //st.perKwCost = Math.Round(Convert.ToDecimal(linkdata.perKwCost), 2);
                            st.perKwCost = st.KwSold > 0 ? Math.Round(Convert.ToDecimal(st.totalCost / st.KwSold), 2) : 0;
                            st.ProjectOpen = Math.Round(Convert.ToDecimal(linkdata.openproject), 2);
                            //st.ProjectOpenpercent = Math.Round(Convert.ToDecimal(linkdata.openprojectper), 2);
                            st.ProjectOpenpercent = st.totallead > 0 ? Math.Round(Convert.ToDecimal((st.ProjectOpen / st.totallead) * 100), 2) : 0;
                            st.DeprecCount = Math.Round(Convert.ToDecimal(linkdata.DepositeRecived), 2);
                            st.DeprecKWSold = st.totallead > 0 ? Math.Round(Convert.ToDecimal((st.DeprecCount / st.totallead) * 100), 2) : 0;
                            st.Pannelsold = Convert.ToInt32(linkdata.PannelSold);
                            st.Costpannel = st.Pannelsold > 0 ? Math.Round(Convert.ToDecimal(st.totalCost / st.Pannelsold), 2) : 0;
                            //coltotallead = coltotallead + st.totallead;
                            //coltotalCost = coltotalCost + st.totalCost;
                            //colCostLead = colCostLead + st.CostLead;
                            //colKwSold = colKwSold + st.KwSold; 
                            //colperKwCost = colperKwCost+ st.perKwCost;
                            //colProjectOpen = colProjectOpen+ st.ProjectOpen; 
                            //colProjectOpenpercent = colProjectOpenpercent + st.ProjectOpenpercent;
                            //colDeprecCount = colDeprecCount + st.DeprecCount;
                            //colDeprecKWSold = colDeprecKWSold + st.DeprecKWSold;
                            //colPannelsold = colPannelsold + st.Pannelsold;
                            //colCostpannel = colCostpannel + st.Costpannel;


                            Statelist.Add(st);
                        }
                        else
                        {
                            StateForLeadExpenseReportDto st = new StateForLeadExpenseReportDto();
                            st.statename = statename;
                            st.totallead = 0;
                            st.totalCost = 0;
                            st.CostLead = 0;
                            st.KwSold = 0;
                            //st.perKwCost = Math.Round(Convert.ToDecimal(linkdata.perKwCost), 2);
                            st.perKwCost = 0;
                            st.ProjectOpen = 0;
                            //st.ProjectOpenpercent = Math.Round(Convert.ToDecimal(linkdata.openprojectper), 2);
                            st.ProjectOpenpercent = 0;
                            st.DeprecCount = 0;
                            st.DeprecKWSold = 0;
                            st.Pannelsold = 0;
                            st.Costpannel = 0;
                            Statelist.Add(st);
                        }


                    }


                    statewiselist.leadstates = Statelist;

                    leadlist1.Add(statewiselist);
                }


                return _leadExpensesExcelExporter.ReportExportToFile(leadlist1);



            }
            catch (Exception e) { throw e; }
        }


    }
}