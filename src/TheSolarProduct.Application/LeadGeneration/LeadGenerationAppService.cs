﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Jobs;
using TheSolarProduct.JobTrackers.Dtos;
using TheSolarProduct.LeadGeneration.Dtos;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.TheSolarDemo;
using System.Linq;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using TheSolarProduct.Timing;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Leads;
using Hangfire.Storage.Monitoring;
using TheSolarProduct.LeadSources;
using Abp.Notifications;
using TheSolarProduct.Notifications;
using TheSolarProduct.LeadGenAppointments;
using TheSolarProduct.JobCancellationReasons.Dtos;
using TheSolarProduct.Installation.Dtos;
using TheSolarProduct.Authorization.Roles;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.Leads.Exporting;
using TheSolarProduct.Dto;
using PayPalCheckoutSdk.Orders;
using static TheSolarProduct.Installation.InstallationAppService;
using TheSolarProduct.Installer;
using TheSolarProduct.Jobs.Dtos;
//using Hangfire.Common;

namespace TheSolarProduct.LeadGeneration
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class LeadGenerationAppService : TheSolarProductAppServiceBase, ILeadGenerationAppService
    {
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<Job> _jobsRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<Team> _teamRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly ITimeZoneService _timeZoneService;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<LeadSource> _leadSourceRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<LeadGenAppointment> _leadGenAppointmentRepository;
        private readonly ILeadsExcelExporter _leadsExcelExporter;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<InstallerDetail> _installerDetailRepository;
        private readonly IRepository<ProductItem, int> _lookup_productItemRepository;

        public LeadGenerationAppService(
            IRepository<Lead> leadRepository,
            IRepository<Job> jobsRepository,
            UserManager userManager,
            IRepository<User, long> userRepository,
            IRepository<UserTeam> userTeamRepository,
            IRepository<Team> teamRepository,
            ITimeZoneConverter timeZoneConverter,
            ITimeZoneService timeZoneService,
            IRepository<LeadActivityLog> leadactivityRepository,
            IRepository<LeadSource> leadSourceRepository,
            IAppNotifier appNotifier,
            IRepository<LeadGenAppointment> leadGenAppointmentRepository,
            ILeadsExcelExporter leadsExcelExporter,
            IRepository<JobProductItem> jobProductItemRepository,
             IRepository<InstallerDetail> installerDetailRepository,
             IRepository<ProductItem, int> lookup_productItemRepository
            )
        {
            _leadRepository = leadRepository;
            _jobsRepository = jobsRepository;
            _userManager = userManager;
            _userRepository = userRepository;
            _userTeamRepository = userTeamRepository;
            _teamRepository = teamRepository;
            _timeZoneConverter = timeZoneConverter;
            _timeZoneService = timeZoneService;
            _leadactivityRepository = leadactivityRepository;
            _leadSourceRepository = leadSourceRepository;
            _appNotifier = appNotifier;
            _leadGenAppointmentRepository = leadGenAppointmentRepository;
            _leadsExcelExporter = leadsExcelExporter;
            _jobProductItemRepository = jobProductItemRepository;
            _installerDetailRepository = installerDetailRepository;
            _lookup_productItemRepository = lookup_productItemRepository;
        }

        public async Task<PagedResultDto<GetLeadGenerationForViewDto>> GetAllMyLeadGeneration(GellAllMyLeadGenerationInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User_List = _userRepository.GetAll();

            var User = User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var job_list = _jobsRepository.GetAll().Include(e => e.LeadFk).Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);
            var jobnumberlist = new List<int?>();
            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber == input.Filter).Select(e => e.LeadId).ToList();
            }

            //var FollowupList = new List<int>();
            //if (input.DateType == "Followup" && input.StartDate != null && input.EndDate != null)
            //{
            //    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date >= SDate.Value.Date && x.CreationTime.Date <= EDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
            //}
            //else if (input.DateType == "Followup" && input.StartDate != null)
            //{
            //    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date >= SDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
            //}
            //else if (input.DateType == "Followup" && input.EndDate != null)
            //{
            //    FollowupList = leadactivity_list.Where(x => x.ActionId == 8 && x.CreationTime.Date <= EDate.Value.Date).DistinctBy(e => e.LeadId).Select(x => x.LeadId).ToList();
            //}

            var leadactivity_list = _leadactivityRepository.GetAll().Include(e => e.LeadFk).Where(x => x.SectionId == 32 && x.LeadFk.OrganizationId == input.OrganizationUnit);
            var Lastleadactivity_list = _leadactivityRepository.GetAll().Include(e => e.LeadFk).Where(x => x.SectionId == 14 && x.LeadFk.OrganizationId == input.OrganizationUnit);

            var filteredResult = _leadRepository.GetAll()
                                .Include(e => e.LeadStatusFk)
                                .WhereIf(role.Contains("Leadgen Manager"), e => UserList.Contains(e.AssignToUserID))
                                .WhereIf(role.Contains("Leadgen SalesRep"), e => e.AssignToUserID == User.Id)
                                .Where(e => e.OrganizationId == input.OrganizationUnit && e.AssignToUserID != null && e.LeadGenTransferDate != null);

            var filteredLeads = filteredResult
                      //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                      .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.CompanyName == input.Filter)
                      .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Email == input.Filter)
                      .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.Mobile == input.Filter)
                      .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => jobnumberlist.Contains(e.Id))
                      .WhereIf(input.UserId != 0, e => e.AssignToUserID == input.UserId)

                       .WhereIf(input.DateType == "Assign" && input.StartDate != null, e => e.LeadGenTransferDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                       .WhereIf(input.DateType == "Assign" && input.EndDate != null, e => e.LeadGenTransferDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                       .WhereIf(input.DateType == "ReAssign" && input.StartDate != null, e => e.ReAssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                       .WhereIf(input.DateType == "ReAssign" && input.EndDate != null, e => e.ReAssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                       .WhereIf(input.DateType == "Creation" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                       .WhereIf(input.DateType == "Creation" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                       //.WhereIf(input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))

                       //.WhereIf(input.DateType == "NextFollowup" && input.StartDate != null, e => e.ActivityDate.Value.Date >= SDate.Value.Date)
                       //.WhereIf(input.DateType == "NextFollowup" && input.EndDate != null, e => e.ActivityDate.Value.Date <= EDate.Value.Date)
                       ;


            //var LeadidList = job_list.Where(e => e.DepositeRecceivedDate != null).Select(e => e.LeadId).ToList();

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var leads = (from o in pagedAndFilteredLeads

                             //join o1 in _lookup_rejectReasonRepository.GetAll() on o.RejectReasonId equals o1.Id into j1
                             //from s1 in j1.DefaultIfEmpty()

                             //join o2 in _lookup_cancelReasonRepository.GetAll() on o.LeadStatusId equals o2.Id into j2
                             //from s2 in j2.DefaultIfEmpty()

                             //let isSoldOut = job_list.Where(x => x.FirstDepositDate != null && x.LeadId == o.Id).Any()

                             //join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                             //from s3 in j3.DefaultIfEmpty()

                             //join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                             //from s4 in j4.DefaultIfEmpty()

                             //join o5 in leadactivityALL on o.Id equals o5.LeadId into j5
                             //from s5 in j5.DefaultIfEmpty()

                             //join o6 in job_list on o.Id equals o6.LeadId into j6
                             //from s6 in j6.DefaultIfEmpty()

                         select new GetLeadGenerationForViewDto()
                         {
                             Id = o.Id,
                             CompanyName = o.CompanyName,
                             IsGoogle = o.IsGoogle,
                             ReAssignDate = o.ReAssignDate,
                             LeadStatusId = o.LeadStatusId,
                             LeadStatus = o.LeadStatusFk.Status,
                             LeadStatusColorClass = o.LeadStatusFk.ColorClass,
                             JobStatus = job_list.Where(e => e.LeadId == o.Id).Select(e => e.JobStatusFk.Name).FirstOrDefault(),
                             Mobile = o.Mobile,
                             Address = o.Address,
                             Suburb = o.Suburb,
                             State = o.State,
                             PostCode = o.PostCode,
                             LeadSource = o.LeadSource,
                             LeadSourceColorClass = _leadSourceRepository.GetAll().Where(e => e.Id == o.LeadSourceId).Select(e => e.ColorClass).FirstOrDefault(),
                             //NextFollowup = o.ActivityDate != null ? o.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss") : null,
                             NextFollowup = leadactivity_list.Where(e => e.ActionId == 8 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                             //Description = o.ActivityDescription,
                             Description = leadactivity_list.Where(e => e.ActionId == 8 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                             //Comment = o.ActivityNote,
                             Comment = leadactivity_list.Where(e => e.ActionId == 24 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                             AssignDate = o.LeadGenTransferDate,
                             CurrentLeadOwner = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                             CreationDate = o.CreationTime,
                             LastComment = Lastleadactivity_list.Where(e => e.ActionId == 24 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                             LastDescription = Lastleadactivity_list.Where(e => e.ActionId == 8 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault()
                         });

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetLeadGenerationForViewDto>(totalCount, await leads.ToListAsync());
        }

         public async Task<List<GetInstallerMapDto>> GetLeadGenMap(GetAllInputMapDto input)
        {
            try
            {
                var InstallerUser = _userRepository.GetAll().Select(e => e.Id).ToList();
                var installerpostcode = _installerDetailRepository.GetAll().Where(j => InstallerUser.Contains(j.UserId)).Select(e => Convert.ToInt32(e.PostCode)).ToList();

                List<InstalllerWithPostCode> installlerWithPostCodes = (from o in _installerDetailRepository.GetAll().Where(j => InstallerUser.Contains(j.UserId))
                                                                        select new InstalllerWithPostCode()
                                                                        {
                                                                            PostCode = Convert.ToInt32(o.PostCode),
                                                                            InstallerName = o.UserFk.FullName
                                                                        }).ToList();

                var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
                var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

                var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
                IList<string> role = await _userManager.GetRolesAsync(User);
                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
                var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

                //bool GrantedPermissionNames = _userPermissionRepository.GetAll().Where(e => e.Name == "Pages.JobBooking" && e.UserId == AbpSession.UserId && e.IsGranted == true).Any();

                DateTime currentDate = DateTime.Now;
                DateTime currentnewDate = currentDate.Date.AddDays(-30);

                List<int?> jobst = new List<int?> { 4, 5, 6, 8 };

                var job_list = _jobsRepository.GetAll().Include(e => e.LeadFk).Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobNumber.Contains(input.FilterText))
                      .WhereIf(input.HousetypeId != 0, e => e.HouseTypeId == input.HousetypeId)
                    .WhereIf(input.Rooftypeid != 0, e => e.RoofTypeId == input.Rooftypeid)
                    .WhereIf(input.Jobstatusid != 0, e => e.JobStatusId == input.Jobstatusid)
                    .WhereIf(input.Paymenttypeid != 0, e => e.PaymentOptionId == input.Paymenttypeid)
                   
                    .WhereIf(input.Jobstatusid == 0 && input.Priority == 0, e => e.JobStatusId == 4 || e.JobStatusId == 5 )
                    .WhereIf(input.Priority == 1, e => e.JobStatusId == 5 && e.ActiveDate <= currentnewDate)
                    .WhereIf(input.Priority == 2, e => e.JobStatusId == 4 || (e.JobStatusId == 5 && e.ActiveDate >= currentnewDate))
                    .Select(e => e.LeadId).ToList();


                var filteredJobs = _leadRepository.GetAll()
                     //.WhereIf(!string.IsNullOrWhiteSpace(input.FilterText), e => false || e.JobNumber.Contains(input.FilterText) || e.LeadFk.Mobile.Contains(input.FilterText) || e.LeadFk.Email.Contains(input.FilterText) || e.LeadFk.Phone.Contains(input.FilterText) || e.LeadFk.CompanyName.Contains(input.FilterText))
                    .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.FilterText), e => job_list.Contains(e.Id))
                    .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.Mobile.Contains(input.FilterText))
                    .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.CompanyName.Contains(input.FilterText))
                    .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.Phone.Contains(input.FilterText))
                    .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.Email.Contains(input.FilterText))
                    .WhereIf(input.HousetypeId != 0, e => job_list.Contains(e.Id))
                    .WhereIf(input.Rooftypeid != 0, e => job_list.Contains(e.Id))
                    .WhereIf(input.Jobstatusid != 0, e => job_list.Contains(e.Id))
                    .WhereIf(input.Paymenttypeid != 0, e => job_list.Contains(e.Id))
                     .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.PostCode, input.PostalCodeFrom) >= 0)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.PostCode, input.PostalCodeTo) <= 0)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.SteetAddressFilter), e => e.Address == input.SteetAddressFilter)
                      .WhereIf(input.DateFilterType == "Assign" && input.StartDate != null, e => e.LeadGenTransferDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                       .WhereIf(input.DateFilterType == "Assign" && input.EndDate != null, e => e.LeadGenTransferDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                       .WhereIf(input.DateFilterType == "ReAssign" && input.StartDate != null, e => e.ReAssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                       .WhereIf(input.DateFilterType == "ReAssign" && input.EndDate != null, e => e.ReAssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                       .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                       .WhereIf(input.DateFilterType == "Creation" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                    //.WhereIf(input.DateFilterType == "Active" && input.StartDate != null && input.EndDate != null, e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
                    //.WhereIf(input.DateFilterType == "DepositeReceived" && input.StartDate != null && input.EndDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                    .WhereIf(input.Jobstatusid == 0 && input.Priority == 0, e => job_list.Contains(e.Id))
                    .WhereIf(input.Priority == 1, e => job_list.Contains(e.Id))
                    .WhereIf(input.Priority == 2, e => job_list.Contains(e.Id))
                    .WhereIf(role.Contains("Leadgen Manager"), e => UserList.Contains(e.AssignToUserID))
                    .WhereIf(role.Contains("Leadgen SalesRep"), e => e.AssignToUserID == User.Id)
                    .Where(e => e.OrganizationId == input.OrganizationUnit  && e.AssignToUserID != null && e.LeadGenTransferDate != null);

                //var _TotalMapData = filteredJobs.Where(e => e.JobStatusId == 4 || e.JobStatusId == 5 || e.JobStatusId == 6 || e.JobStatusId == 8).Select(e => e.LeadId).Count();
                //var _TotalDepRcvMapData = filteredJobs.Where(e => e.JobStatusId == 4 && (e.Longitude == null ? e.LeadFk.longitude : e.Longitude) != null
                //&& (e.Latitude == null ? e.LeadFk.latitude : e.Latitude) != null).Select(e => e.LeadId).Count();

                //var _TotalActiveMapData = filteredJobs.Where(e => e.JobStatusId == 5 && (e.Longitude == null ? e.LeadFk.longitude : e.Longitude) != null
                //&& (e.Latitude == null ? e.LeadFk.latitude : e.Latitude) != null).Select(e => e.LeadId).Count();

                //var _TotalJobBookedMapData = filteredJobs.Where(e => e.JobStatusId == 6 && (e.Longitude == null ? e.LeadFk.longitude : e.Longitude) != null
                //&& (e.Latitude == null ? e.LeadFk.latitude : e.Latitude) != null).Select(e => e.LeadId).Count();

                //var _TotalJobInstallMapData = filteredJobs.Where(e => e.JobStatusId == 8 && (e.Longitude == null ? e.LeadFk.longitude : e.Longitude) != null
                //&& (e.Latitude == null ? e.LeadFk.latitude : e.Latitude) != null).Select(e => e.LeadId).Count();

                //var _Other = filteredJobs.Where(e => (e.Longitude == null ? e.LeadFk.longitude : e.Longitude) == null
                //&& (e.Latitude == null ? e.LeadFk.latitude : e.Latitude) == null).Select(e => e.LeadId).Count();

                //var _ListOfmissingLatlong = filteredJobs.Where(e => (e.Longitude == null ? e.LeadFk.longitude : e.Longitude) == null
                //&& (e.Latitude == null ? e.LeadFk.latitude : e.Latitude) == null).Select(e => e.Id).ToList();

                var jobs = (from o in filteredJobs

                            join o6 in _jobsRepository.GetAll() on o.Id equals o6.LeadId into j6
                            from s6 in j6.DefaultIfEmpty()

                            let test =  s6.JobStatusId == 8 ? 0 : getDistanceFromLatLonInKm(o.latitude, o.longitude, o.State)
                            let Icons = getIcon(s6.JobStatusId, s6.ActiveDate.Value.Date)
                            let nearest2postcode = s6.JobStatusId == 8 ? null : getNearest(o.PostCode, installlerWithPostCodes)
                            let first = nearest2postcode != null ? nearest2postcode.First() : ""
                            let last = nearest2postcode != null ? nearest2postcode.Last() : ""

                            where (o.latitude != null && o.longitude != null) 

                            select new GetInstallerMapDto()
                            {
                                LeadId = o.Id,
                                label = s6.JobNumber,
                                Company = o.CompanyName + " - ",
                                Status = s6.JobStatusFk.Name,
                                Adress = o.Address + " " + o.Suburb + " " + o.State + " " + o.PostCode,
                                State = o.State,
                                PhoneEmail = o.Mobile + '/' + o.Email,
                                DepRecDate = s6.DepositeRecceivedDate,
                                ActiveDate = s6.ActiveDate,
                                HouseType = s6.HouseTypeFk.Name,
                                RoofType = s6.RoofTypeFk.Name,
                                AreaType = s6.LeadFk.Area,
                                Latitude = o.latitude,
                                Longitude = o.longitude,
                                Icon = Icons,
                                Distance = test,
                                //ProductIteams = (from j in _jobProductItemRepository.GetAll().Where(e => e.JobId == s6.Id)
                                //                 join o2 in _lookup_productItemRepository.GetAll() on j.ProductItemId equals o2.Id
                                //                 select new GetJobProductItemForEditOutput()
                                //                 {
                                //                     JobNote = o2.ProductTypeFk.Name,
                                //                     ProductItemName = o2.Name,
                                //                     Model = o2.Model,
                                //                     Quantity = j.Quantity
                                //                 }).ToList(),
                                ProductIteams = (from j in _jobProductItemRepository.GetAll().Where(e => e.JobId == s6.Id)
                                                     //join o2 in _lookup_productItemRepository.GetAll() on o.ProductItemId equals o2.Id
                                                 select new GetJobProductItemForEditOutput()
                                                 {
                                                     JobNote = j.ProductItemFk.ProductTypeFk.Name,
                                                     ProductItemName = j.ProductItemFk.Name,
                                                     Model = j.ProductItemFk.Model,
                                                     Quantity = j.Quantity
                                                 }).ToList(),
                                Id = s6.Id,
                                JobStatusId = s6.JobStatusId,

                                FirstNearestInstallerName = first,
                                SecondNearestInstallerName = last,

                                //TotalMapData = _TotalMapData,
                                //TotalDepRcvMapData = _TotalDepRcvMapData,
                                //TotalActiveMapData = _TotalActiveMapData,
                                //TotalJobBookedMapData = _TotalJobBookedMapData,
                                //TotalJobInstallMapData = _TotalJobInstallMapData,
                                //Other = _Other,
                                //ListOfmissingLatlong = _ListOfmissingLatlong

                            }).ToList();

                return jobs;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<LeadsAppointmentCalendarDto>> GetAppointmentCalendar(int appointentFor, DateTime calendarStartDate, int? orgID, string areaNameFilter, string state, int createdBy)
        {
            var User_List = _userRepository.GetAll();

            var User = User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var dates = new List<DateTime>();
            var startDate = (_timeZoneConverter.Convert(calendarStartDate, (int)AbpSession.TenantId));
            var endDate = startDate.Value.AddDays(6).Date;
            for (var dt = startDate; dt <= endDate; dt = dt.Value.AddDays(1))
            {
                dates.Add(dt.Value.Date);
            }

            var installationList = (from dt in dates
                                    select new LeadsAppointmentCalendarDto
                                    {
                                        AppointmentDate = dt.Date,
                                        //AppointmentsCount = _leadGenAppointmentRepository.GetAll()
                                        //        .WhereIf(appointentFor > 0, x => x.AppointmentFor == appointentFor)
                                        //        .WhereIf(!string.IsNullOrWhiteSpace(state), e => e.LeadFK.State == state)
                                        //        .WhereIf(!string.IsNullOrWhiteSpace(areaNameFilter), e => e.LeadFK.Area == areaNameFilter)
                                        //        .WhereIf(orgID != 0, x => x.LeadFK.OrganizationId == orgID)
                                        //        .Where(x => x.AppointmentDate.Date == Convert.ToDateTime(dt.Date.ToShortDateString())).Count(),

                                        Appointments = (from appointments in _leadGenAppointmentRepository.GetAll().Include(e => e.LeadFK).Where(x => x.AppointmentDate.Date == Convert.ToDateTime(dt.Date.ToShortDateString()))
                                                .WhereIf(appointentFor > 0, x => x.AppointmentFor == appointentFor)
                                                .WhereIf(!string.IsNullOrWhiteSpace(areaNameFilter), e => e.LeadFK.Area == areaNameFilter)
                                                .WhereIf(!string.IsNullOrWhiteSpace(state), e => e.LeadFK.State == state)
                                                .WhereIf(orgID != 0, x => x.LeadFK.OrganizationId == orgID)
                                                .WhereIf(createdBy > 0 , e => e.CreatorUserId == createdBy)
                                                //.WhereIf(role.Contains("Leadgen SalesRep"), e => e.AppointmentFor == User.Id)
                                                .WhereIf(role.Contains("Leadgen SalesRep"), e => e.CreatorUserId == User.Id)
                                                            //.WhereIf(role.Contains("Leadgen Manager"), e => UserList.Contains(e.AppointmentFor))

                                                        select new InstallationLeadsAppointmentDto
                                                        {
                                                            Id = appointments.Id,
                                                            LeadId = appointments.LeadId,
                                                            CustomerName = appointments.LeadFK.CompanyName,
                                                            Suburb = appointments.LeadFK.Suburb,
                                                            State = appointments.LeadFK.State,
                                                            PostalCode = appointments.LeadFK.PostCode,
                                                            Address = appointments.LeadFK.Address,
                                                            AppointmentDateTime = appointments.AppointmentDate,
                                                            AppointmentFor = appointments.AppointmentFor,
                                                            AppointmentUserName = appointments.UserFK.FullName.ToString()
                                                        }).ToList()
                                    });

            return new List<LeadsAppointmentCalendarDto>(installationList.ToList());
        }

        public async Task TransferToSalesRep(List<int> leadIds, int userId, int sectionId)
        {
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == userId).FirstOrDefault();
            var assignedFromUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).Select(e => e.FullName).FirstOrDefault();

            foreach (var id in leadIds)
            {
                var lead = await _leadRepository.FirstOrDefaultAsync((int)id);

                lead.AssignToUserID = userId;
                await _leadRepository.UpdateAsync(lead);

                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionNote = "Lead Assign To" + assignedToUser.FullName.ToString();
                leadactivity.ActionId = 27;
                leadactivity.SectionId = sectionId;
                leadactivity.LeadId = (int)lead.Id;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                leadactivity.CreatorUserId = AbpSession.UserId;
                await _leadactivityRepository.InsertAsync(leadactivity);
            }

            string msg = string.Format(leadIds.Count + " Leads Assign By " + assignedFromUser);
            await _appNotifier.LeadAssiged(assignedToUser, msg, NotificationSeverity.Info);
        }

        public bool CheckAppointmentExist(string appointmentDate, int? appointmentFor)
        {
            bool existdata = false;

            if (!string.IsNullOrWhiteSpace(appointmentDate) && appointmentFor != null)
            {
                var AppointmentDate = Convert.ToDateTime(appointmentDate);
                existdata = _leadGenAppointmentRepository.GetAll().Where(e => e.AppointmentDate == AppointmentDate && e.AppointmentFor == appointmentFor).Any();
            }
            return existdata;
        }

        public async Task CreateLeadGenAppointment(CreateOrEditLeadGenAppointmentDto input)
        {

            var AppointmentDate = Convert.ToDateTime(input.AppointmentDate);

            AppointmentDate = (DateTime)_timeZoneConverter.Convert(AppointmentDate, (int)AbpSession.TenantId);

            var leadGenAppointment = new LeadGenAppointment();
            leadGenAppointment.AppointmentDate = AppointmentDate;
            leadGenAppointment.AppointmentFor = input.AppointmentFor;
            leadGenAppointment.LeadId = input.LeadId;
            leadGenAppointment.Notes = input.Notes;
            if (AbpSession.TenantId != null)
            {
                //Prevent tenants to get other tenant's users.
                input.TenantId = AbpSession.TenantId;
            }
            var user = _userRepository.GetAll().Where(e => e.Id == input.AppointmentFor).FirstOrDefault();
            await _leadGenAppointmentRepository.InsertAndGetIdAsync(leadGenAppointment);

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 43;
            leadactivity.SectionId = input.SectionId;
            leadactivity.ActionNote = "New Appintment Created for " + user.FullName;
            leadactivity.LeadId = Convert.ToInt32(input.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        public async Task Delete(EntityDto input)
        {
            //var appointment = await _leadGenAppointmentRepository.FirstOrDefaultAsync((int)input.Id);

            //LeadActivityLog leadactivity = new LeadActivityLog();
            //leadactivity.ActionId = 15;
            //leadactivity.SectionId = 0;
            //leadactivity.ActionNote = "LeadGenAppointment Deleted";
            //leadactivity.LeadId = Convert.ToInt32(appointment.LeadId);
            //if (AbpSession.TenantId != null)
            //{
            //    leadactivity.TenantId = (int)AbpSession.TenantId;
            //}
            //await _leadactivityRepository.InsertAsync(leadactivity);

            await _leadGenAppointmentRepository.DeleteAsync(input.Id);
        }

        public async Task<PagedResultDto<GetAllCommisionViewDto>> GetCommissionJobs(GellAllCommissionInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User_List = _userRepository.GetAll();
            List<int> batteryInverter = new List<int>() { 1, 3, 4, 6 };

            var job_list = _jobsRepository.GetAll().Include(e => e.LeadFk).Where(e => e.JobStatusId == 8 && e.LeadFk.OrganizationId == input.OrganizationUnit)
                .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5))

                        .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                        .WhereIf(input.BatteryFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                        .WhereIf(input.BatteryFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                        .WhereIf(input.BatteryFilter == 5, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 2).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && batteryInverter.Contains(j.ProductItemFk.ProductTypeId)).Count() == 0) //For Only Inverter and Battery
                        .WhereIf(input.BatteryFilter == 6, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5)) //For with battery

                .WhereIf(input.StartDate != null && input.DateType == "InstallDate", e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                 .WhereIf(input.EndDate != null && input.DateType == "InstallDate", e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                 .WhereIf(input.DateType == "InstallBook" && input.StartDate != null, e => e.InstallationDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "InstallBook" && input.EndDate != null, e => e.InstallationDate.Value.AddHours(diffHour).Date <= EDate.Value.Date);

            var leadList = job_list.Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit).WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter).Select(e => e.LeadId);


            var Jobs = _jobsRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit)
                .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5))

                        .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                        .WhereIf(input.BatteryFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                        .WhereIf(input.BatteryFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                        .WhereIf(input.BatteryFilter == 5, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 2).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && batteryInverter.Contains(j.ProductItemFk.ProductTypeId)).Count() == 0) //For Only Inverter and Battery
                        .WhereIf(input.BatteryFilter == 6, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5)) //For with battery

                .WhereIf(input.StartDate != null && input.DateType == "InstallDate", e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                 .WhereIf(input.EndDate != null && input.DateType == "InstallDate", e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                 .WhereIf(input.DateType == "InstallBook" && input.StartDate != null, e => e.InstallationDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "InstallBook" && input.EndDate != null, e => e.InstallationDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                 .AsNoTracking().Select(e => new {e.Id, e.LeadId, e.JobNumber, JobStatus = e.JobStatusFk.Name,StatusClass = e.JobStatusFk.ColorClass}).ToList();

            var Leads = _leadGenAppointmentRepository.GetAll()
                .Where(e => leadList.Any(l => l.Value == e.LeadId))
                .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFK.CompanyName == input.Filter)
                .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFK.Email == input.Filter)
                .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFK.Mobile == input.Filter)
                .WhereIf(input.StartDate != null && input.DateType == "AppointmentDate", e => e.AppointmentDate.Date >= SDate.Value.Date)
                 .WhereIf(input.EndDate != null && input.DateType == "AppointmentDate", e => e.AppointmentDate.Date <= EDate.Value.Date)
                .WhereIf(input.UserId != 0, e => e.CreatorUserId == input.UserId)
                .WhereIf(input.ApointmentFor > 0, e => e.AppointmentFor == input.ApointmentFor)
                .Where(e =>e.CommitionDate != null);

            var AppointmentIds = Leads.GroupBy(e => e.LeadId).Select(e => new { AppointmentDate = e.Max(m => m.AppointmentDate), LeadId = e.Key }).Select(e => e.AppointmentDate).ToList();

            var filteredLeads = Leads
                .WhereIf(AppointmentIds.Count() > 0,e => AppointmentIds.Contains(e.AppointmentDate));
                //.WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFK.CompanyName == input.Filter)
                //.WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFK.Email == input.Filter)
                //.WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFK.Mobile == input.Filter)
                //.WhereIf(input.StartDate != null && input.DateType == "AppointmentDate", e => e.AppointmentDate.Date >= SDate.Value.Date)
                // .WhereIf(input.EndDate != null && input.DateType == "AppointmentDate", e => e.AppointmentDate.Date <= EDate.Value.Date)
                //.WhereIf(input.UserId != 0, e => e.CreatorUserId == input.UserId)
                //.WhereIf(input.ApointmentFor > 0, e => e.AppointmentFor == input.ApointmentFor)
                //.Where(e => e.CommitionDate != null);

            //.WhereIf(input.DateType == "Assign" && input.StartDate != null, e => e.LeadFK.LeadGenTransferDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
            // .WhereIf(input.DateType == "Assign" && input.EndDate != null, e => e.LeadFK.LeadGenTransferDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

            // .WhereIf(input.DateType == "ReAssign" && input.StartDate != null, e => e.LeadFK.ReAssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
            // .WhereIf(input.DateType == "ReAssign" && input.EndDate != null, e => e.LeadFK.ReAssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

            // .WhereIf(input.DateType == "Creation" && input.StartDate != null, e => e.LeadFK.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
            // .WhereIf(input.DateType == "Creation" && input.EndDate != null, e => e.LeadFK.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
            //.WhereIf(input.UserId != 0, e => e.AppointmentFor == input.UserId);


            //var LeadidList = job_list.Where(e => e.DepositeRecceivedDate != null).Select(e => e.LeadId).ToList();

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var leads = (from o in pagedAndFilteredLeads

                             //join o1 in _lookup_rejectReasonRepository.GetAll() on o.RejectReasonId equals o1.Id into j1
                             //from s1 in j1.DefaultIfEmpty()

                             //join o2 in _lookup_cancelReasonRepository.GetAll() on o.LeadStatusId equals o2.Id into j2
                             //from s2 in j2.DefaultIfEmpty()

                             //let isSoldOut = job_list.Where(x => x.FirstDepositDate != null && x.LeadId == o.Id).Any()

                             //join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                             //from s3 in j3.DefaultIfEmpty()

                             //join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                             //from s4 in j4.DefaultIfEmpty()

                             //join o5 in leadactivityALL on o.Id equals o5.LeadId into j5
                             //from s5 in j5.DefaultIfEmpty()

                             //join o6 in job_list on o.Id equals o6.LeadId into j6
                             //from s6 in j6.DefaultIfEmpty()
                             //let job = Jobs.Where(e => e.LeadId == o.LeadId).Any()

                         select new GetAllCommisionViewDto()
                         {
                             Id = o.Id,
                             LeadId = o.LeadId,
                             CompanyName = o.LeadFK.CompanyName,
                             IsGoogle = o.LeadFK.IsGoogle,
                             ReAssignDate = o.LeadFK.ReAssignDate,
                             LeadStatusId = o.LeadFK.LeadStatusId,
                             LeadStatus = o.LeadFK.LeadStatusFk.Status,
                             LeadStatusColorClass = o.LeadFK.LeadStatusFk.ColorClass,
                             //JobStatus = Jobs.Where(e => e.LeadId == o.LeadId).FirstOrDefault().JobStatus,
                             Mobile = o.LeadFK.Mobile,
                             Address = o.LeadFK.Address,
                             Suburb = o.LeadFK.Suburb,
                             State = o.LeadFK.State,
                             PostCode = o.LeadFK.PostCode,
                             LeadSource = o.LeadFK.LeadSource,
                             LeadSourceColorClass = _leadSourceRepository.GetAll().Where(e => e.Id == o.LeadFK.LeadSourceId).Select(e => e.ColorClass).FirstOrDefault(),
                             AssignDate = o.LeadFK.LeadGenTransferDate,
                             CurrentLeadOwner = User_List.Where(e => e.Id == o.LeadFK.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                             CreationDate = o.LeadFK.CreationTime,
                             AppointementFor = User_List.Where(e => e.Id == o.AppointmentFor).Select(e => e.FullName).FirstOrDefault(),
                             IsSelect = false,
                             CreateBy = User_List.Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                             //JobNumber = job == true ? Jobs.Where(e => e.LeadId == o.Id).Select(e => e.JobNumber).FirstOrDefault() : "",
                             //JobId = Jobs.Where(e => e.LeadId == o.Id).Select(e => e.Id).FirstOrDefault(),

                         }).ToList();

            foreach(var item in leads)
            {
                var job = Jobs.Where(e => e.LeadId == item.LeadId).FirstOrDefault();
                item.JobStatus = job.JobStatus;
                item.JobNumber = job.JobNumber;
                item.JobId = job.Id;
                item.JobStatusColorClass = job.StatusClass;
            }

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetAllCommisionViewDto>(totalCount, leads);
        }

        public async Task<FileDto> GetCommissionJobsExcel(GellAllCommissionExcelInput input)
        {
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            //var User_List = _userRepository.GetAll();

            //var User = User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var job_list = _jobsRepository.GetAll().Include(e => e.LeadFk).Where(e => e.JobStatusId == 8 && e.LeadFk.OrganizationId == input.OrganizationUnit);
            //var leadList = new List<int?>();
            //leadList = job_list
            //      .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
            //        .Select(e => e.LeadId).ToList();



            //var filteredLeads = _leadGenAppointmentRepository.GetAll().Where(e => leadList.Contains(e.LeadId))
            //    .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFK.CompanyName == input.Filter)
            //          .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFK.Email == input.Filter)
            //          .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFK.Mobile == input.Filter);

            //.WhereIf(input.DateType == "Assign" && input.StartDate != null, e => e.LeadFK.LeadGenTransferDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
            // .WhereIf(input.DateType == "Assign" && input.EndDate != null, e => e.LeadFK.LeadGenTransferDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

            // .WhereIf(input.DateType == "ReAssign" && input.StartDate != null, e => e.LeadFK.ReAssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
            // .WhereIf(input.DateType == "ReAssign" && input.EndDate != null, e => e.LeadFK.ReAssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

            // .WhereIf(input.DateType == "Creation" && input.StartDate != null, e => e.LeadFK.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
            // .WhereIf(input.DateType == "Creation" && input.EndDate != null, e => e.LeadFK.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
            // .WhereIf(input.UserId != 0, e => e.AppointmentFor == input.UserId);

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User_List = _userRepository.GetAll();

            var job_list = _jobsRepository.GetAll().Include(e => e.LeadFk).Where(e => e.JobStatusId == 8 && e.LeadFk.OrganizationId == input.OrganizationUnit)
                .WhereIf(input.StartDate != null && input.DateType == "InstallDate", e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date)
                 .WhereIf(input.EndDate != null && input.DateType == "InstallDate", e => e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date);
            var leadList = job_list.Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit).WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter).Select(e => e.LeadId);

            var filteredLeads = _leadGenAppointmentRepository.GetAll()
                .Where(e => leadList.Any(l => l.Value == e.LeadId))
                .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFK.CompanyName == input.Filter)
                .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFK.Email == input.Filter)
                .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFK.Mobile == input.Filter)
                .WhereIf(input.StartDate != null && input.DateType == "AppointmentDate", e => e.AppointmentDate.Date >= SDate.Value.Date)
                 .WhereIf(input.EndDate != null && input.DateType == "AppointmentDate", e => e.AppointmentDate.Date <= EDate.Value.Date)
                .WhereIf(input.UserId != 0, e => e.CreatorUserId == input.UserId)
                .WhereIf(input.ApointmentFor > 0, e => e.AppointmentFor == input.ApointmentFor)
                .Where(e => e.CommitionDate != null);


            var leads = (from o in filteredLeads

                             //join o1 in _lookup_rejectReasonRepository.GetAll() on o.RejectReasonId equals o1.Id into j1
                             //from s1 in j1.DefaultIfEmpty()

                             //join o2 in _lookup_cancelReasonRepository.GetAll() on o.LeadStatusId equals o2.Id into j2
                             //from s2 in j2.DefaultIfEmpty()

                             //let isSoldOut = job_list.Where(x => x.FirstDepositDate != null && x.LeadId == o.Id).Any()

                             //join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                             //from s3 in j3.DefaultIfEmpty()

                             //join o4 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o4.Id into j4
                             //from s4 in j4.DefaultIfEmpty()

                             //join o5 in leadactivityALL on o.Id equals o5.LeadId into j5
                             //from s5 in j5.DefaultIfEmpty()

                             //join o6 in job_list on o.Id equals o6.LeadId into j6
                             //from s6 in j6.DefaultIfEmpty()

                         select new GetAllCommisionViewDto()
                         {
                             Id = o.Id,
                             LeadId = o.LeadId,
                             CompanyName = o.LeadFK.CompanyName,
                             IsGoogle = o.LeadFK.IsGoogle,
                             ReAssignDate = o.LeadFK.ReAssignDate,
                             LeadStatusId = o.LeadFK.LeadStatusId,
                             LeadStatus = o.LeadFK.LeadStatusFk.Status,
                             LeadStatusColorClass = o.LeadFK.LeadStatusFk.ColorClass,
                             JobStatus = job_list.Where(e => e.LeadId == o.Id).Select(e => e.JobStatusFk.Name).FirstOrDefault(),
                             Mobile = o.LeadFK.Mobile,
                             Address = o.LeadFK.Address,
                             Suburb = o.LeadFK.Suburb,
                             State = o.LeadFK.State,
                             PostCode = o.LeadFK.PostCode,
                             LeadSource = o.LeadFK.LeadSource,
                             LeadSourceColorClass = _leadSourceRepository.GetAll().Where(e => e.Id == o.LeadFK.LeadSourceId).Select(e => e.ColorClass).FirstOrDefault(),
                             AssignDate = o.LeadFK.LeadGenTransferDate,
                             CurrentLeadOwner = User_List.Where(e => e.Id == o.LeadFK.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                             CreationDate = o.LeadFK.CreationTime,
                             AppointementFor = _userRepository.GetAll().Where(e => e.Id == o.AppointmentFor).Select(e => e.FullName).FirstOrDefault(),
                             CreateBy = User_List.Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                             JobNumber = job_list.Where(e => e.LeadId == o.Id).Select(e => e.JobNumber).FirstOrDefault(),

                         }).ToList();

            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _leadsExcelExporter.CommissionExport(leads, "commission.xlsx");
            }
            else
            {
                return _leadsExcelExporter.CommissionExport(leads, "commission.csv");
                //return _leadsExcelExporter.LeadTrackerCsvExport(leadtrackerListDtos);
            }
        }

        public async Task<double> getCommissionAmountByUserId(int UserId)
        {
            var p = 0.0;
            var user = await _userRepository.GetAsync(UserId);

            if (user != null)
            {
                p = (double)(user.CommitionAmount != null ? user.CommitionAmount : 0);
            }
            return p;
        }

        public async Task CreateCommission(CreateCommissionDto input)
        {
            input.CommitionDate = (DateTime)_timeZoneConverter.Convert(input.CommitionDate, (int)AbpSession.TenantId);
            foreach (int apointmentId in input.AppointmentIds)
            {
                var appointment = await _leadGenAppointmentRepository.GetAsync(apointmentId);

                appointment.CommitionDate = input.CommitionDate;
                appointment.CommissionNote = input.Note;
                appointment.CommitionAmount = (decimal)input.Amount;

                await _leadGenAppointmentRepository.UpdateAsync(appointment);

                var user = _userRepository.GetAll().Where(e => e.Id == appointment.AppointmentFor).FirstOrDefault();

                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 64;
                leadactivity.SectionId = input.SectionId;
                leadactivity.ActionNote = "Commission for " + user.FullName;
                leadactivity.LeadId = Convert.ToInt32(appointment.LeadId);
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
        }
        private static string getIcon(int? jobstatusid, DateTime? Date)
        {
            DateTime currentDate = DateTime.Now;
            DateTime newDate = currentDate.Date.AddDays(-30);

            var icon3 = "https://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png";
            var Active = "https://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
            var DepRcv = "https://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png";
            var JobBook = "https://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png";
            var JobInstalled = "https://www.google.com/intl/en_us/mapfiles/ms/micons/orange-dot.png";

            var Result = "";
            if (jobstatusid == 4)
            {
                Result = DepRcv;
            }
            if (jobstatusid == 5)
            {
                if (Date <= newDate)
                {
                    Result = icon3;
                }
                else
                {
                    Result = Active;
                }
            }
            if (jobstatusid == 6)
            {
                Result = JobBook;
            }
            if (jobstatusid == 8)
            {
                Result = JobInstalled;
            }

            return Result;
        }

        private static double getDistanceFromLatLonInKm(string lat1, string lon1, string States)
        {
            if (lat1 != "" && lon1 != "")
            {
                var R = 6371; // Radius of the earth in km
                var lat2 = 0.0;
                var lon2 = 0.0;

                if (States == "QLD")
                {
                    lat2 = -27.580041680084136;
                    lon2 = 153.03702238177127;
                }
                if (States == "VIC")
                {
                    lat2 = -37.81759262390686;
                    lon2 = 144.8550323686331;
                }
                if (States == "NSW")
                {
                    lat2 = -33.74722076106781;
                    lon2 = 150.90180287033405;
                }
                if (States == "SA")
                {
                    lat2 = -34.828917615461556;
                    lon2 = 138.600999853177;
                }
                if (States == "WA")
                {
                    lat2 = -32.06865473895976;
                    lon2 = 115.9103034972597;
                }

                var d1 = Convert.ToDouble(lat1) * (Math.PI / 180.0);
                var num1 = Convert.ToDouble(lon1) * (Math.PI / 180.0);
                var d2 = lat2 * (Math.PI / 180.0);
                var num2 = lon2 * (Math.PI / 180.0) - num1;
                var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) +
                         Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);
                var ans = 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
                return Math.Round((ans / 1000), 2);
            }
            else
            {
                return 0.00;
            }
            //var dLat = deg2rad(lat2 - Convert.ToDouble(lat1));  // deg2rad below
            //var dLon = deg2rad(lon2 - Convert.ToDouble(lon1));
            //var a =
            //  Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
            //  Math.Cos(deg2rad(Convert.ToDouble(lat1))) * Math.Cos(deg2rad(lat2)) *
            //  Math.Sin(dLon / 2) * Math.Cos(dLon / 2);
            //var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            //var d = R * c; // Distance in km

            //return Math.Round((d / 1000),2);
        }

        private static List<string> getNearest(string post, List<InstalllerWithPostCode> installerpostcode)
        {
            var s = Convert.ToInt32(post);
            var Listpostcodes = installerpostcode.OrderBy(x => Math.Abs(x.PostCode - s)).Take(2).ToList();
            var nearest2postcode = (from i in Listpostcodes select i.InstallerName.ToString()).ToList();
            return nearest2postcode;
        }


    }


}
