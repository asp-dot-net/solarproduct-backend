﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheSolarProduct.CheckActives.Dtos;
using TheSolarProduct.DataVaults;
using TheSolarProduct.Dto;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.StockOrders.SerialNoStatuses;
using TheSolarProduct.SerialNoStatuses.Dtos;
using Abp.Linq.Extensions;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Abp.Collections.Extensions;
using System.Linq.Dynamic.Core;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace TheSolarProduct.SerialNoStatuses
{
   
    public class SerialNoStatusAppService : TheSolarProductAppServiceBase, ISerialNoStatusAppService
    {
       
        private readonly IRepository<SerialNoStatus> _SerialNoStatusRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public SerialNoStatusAppService(
            
              IRepository<SerialNoStatus> SerialNoStatusRepository,
              IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
              )
        {

            _SerialNoStatusRepository = SerialNoStatusRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task<PagedResultDto<GetSerialNoStatusForViewDto>> GetAll(GetAllSerialNoStatusInput input)
        {

            var SerialNoStatus = _SerialNoStatusRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Status == input.Filter);

            var pagedAndFilteredChcekApplication = SerialNoStatus
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var outputSerialNoStatus = from o in pagedAndFilteredChcekApplication
                                    select new GetSerialNoStatusForViewDto()
                                    {
                                        SerialNoStatus = new SerialNoStatusDto
                                        {
                                            Id = o.Id,
                                            status = o.Status,
                                            IsActive = o.IsActive
                                        }
                                    };

            var totalCount = await SerialNoStatus.CountAsync();

            return new PagedResultDto<GetSerialNoStatusForViewDto>(
                totalCount,
                await outputSerialNoStatus.ToListAsync()
            );
        }

       
        public async Task<GetSerialNoStatusForEditOutput> GetSerialNoStatusForEdit(EntityDto input)
        {
            var SerialNoStatus = await _SerialNoStatusRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetSerialNoStatusForEditOutput { SerialNoStatus = ObjectMapper.Map<CreateOrEditSerialNoStatusDto>(SerialNoStatus) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditSerialNoStatusDto input)
        {


            if (input.Id != null)
            {
                await Update(input);
            }
           
        }

        protected virtual async Task Update(CreateOrEditSerialNoStatusDto input)
        {
            var SerialNoStatus = await _SerialNoStatusRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 58;
            dataVaultLog.ActionNote = "SerialNoStatus Updated : " + SerialNoStatus.Status;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Status != SerialNoStatus.Status)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = SerialNoStatus.Status;
                history.CurValue = input.Status;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != SerialNoStatus.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = SerialNoStatus.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, SerialNoStatus);

            await _SerialNoStatusRepository.UpdateAsync(SerialNoStatus);


        }

        
        public async Task Delete(EntityDto input)
        {
            var Status = _SerialNoStatusRepository.Get(input.Id).Status;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 58;
            dataVaultLog.ActionNote = "SerialNoStatus Deleted : " + Status;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _SerialNoStatusRepository.DeleteAsync(input.Id);
        }

       
    }
}
