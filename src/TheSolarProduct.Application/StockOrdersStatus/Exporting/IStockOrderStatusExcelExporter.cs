﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.StockOrderStatus.Dtos;

namespace TheSolarProduct.StockOrdersStatus.Exporting
{
    public interface IStockOrderStatusExcelExporter
    {
        FileDto ExportToFile(List<GetStockOrderStatusForViewDto> stockOrderStatus);
    }
}
