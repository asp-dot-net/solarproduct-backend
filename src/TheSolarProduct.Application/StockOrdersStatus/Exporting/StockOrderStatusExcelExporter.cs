﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.PurchaseCompanys.Dtos;
using TheSolarProduct.PurchaseCompanys.Exporting;
using TheSolarProduct.StockOrderStatus.Dtos;
using TheSolarProduct.Storage;

namespace TheSolarProduct.StockOrdersStatus.Exporting
{
    
    public class StockOrderStatusExcelExporter : NpoiExcelExporterBase, IStockOrderStatusExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public StockOrderStatusExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetStockOrderStatusForViewDto> stockOrderStatus)
        {
            return CreateExcelPackage(
                "StockOrderStatus.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("StockOrderStatus"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("IsActive")
                    );

                    AddObjects(
                        sheet,
                        2,
                        stockOrderStatus,
                        _ => _.StockOrderStatus.Name,
                        _ => _.StockOrderStatus.IsActive.HasValue ? (_.StockOrderStatus.IsActive.Value ? L("Yes") : L("No")) : L("No")
                    );
                }
            );
        }


    }
}
