﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataVaults;
using TheSolarProduct.Dto;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.PurchaseCompanies;
using TheSolarProduct.PurchaseCompanys.Dtos;
using TheSolarProduct.PurchaseCompanys.Exporting;
using TheSolarProduct.PurchaseCompanys;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.StockOrdersStatus.Exporting;
using TheSolarProduct.StockOrderStatus;
using TheSolarProduct.StockOrderStatus.Dtos;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;

using System.Linq;


namespace TheSolarProduct.StockOrdersStatus
{

    [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockOrderStatus)]
    public class StockOrderStatusAppService : TheSolarProductAppServiceBase, IStockOrderStatusAppService
    {
        private readonly IStockOrderStatusExcelExporter _stockOrderStatusExcelExporter;

        private readonly IRepository<StockOrderStatuses.StockOrderStatus> _stockOrderStatusRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public StockOrderStatusAppService(
              IRepository<StockOrderStatuses.StockOrderStatus> stockOrderStatusRepository,
              IStockOrderStatusExcelExporter IStockOrderStatusExcelExporter,
              IRepository<UserTeam> userTeamRepository,
              IRepository<UserRole, long> userRoleRepository,
              IRepository<Role> roleRepository,
              IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
              )
        {
            _stockOrderStatusRepository = stockOrderStatusRepository;
            _stockOrderStatusExcelExporter = IStockOrderStatusExcelExporter;
            _userTeamRepository = userTeamRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }




        public async Task<PagedResultDto<GetStockOrderStatusForViewDto>> GetAll(GetAllStockOrderStatusInput input)
        {

            var StockOrderStatus = _stockOrderStatusRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFilteredStockOrderStatus = StockOrderStatus
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var outputStockOrderStatus = from o in pagedAndFilteredStockOrderStatus
                                        select new GetStockOrderStatusForViewDto()

                                        {
                                            StockOrderStatus = new StockOrderStatusDto
                                            {
                                                Id = o.Id,
                                                Name = o.Name,
                                                IsActive = o.IsActive
                                            }
                                        };

            var totalCount = await StockOrderStatus.CountAsync();

            return new PagedResultDto<GetStockOrderStatusForViewDto>(
                totalCount,
                await outputStockOrderStatus.ToListAsync()
            );
        }



        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockOrderStatus_Edit)]
        public async Task<GetStockOrderStatusForEditOutput> GetStockOrderStatusForEdit(EntityDto input)
        {
            var StockOrderStatus = await _stockOrderStatusRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetStockOrderStatusForEditOutput { StockOrderStatus = ObjectMapper.Map<CreateOrEditStockOrderStatusDto>(StockOrderStatus) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditStockOrderStatusDto input)
        {


            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockOrderStatus_Create)]

        protected virtual async Task Create(CreateOrEditStockOrderStatusDto input)
        {
            var stockOrderStatus = ObjectMapper.Map<StockOrderStatuses.StockOrderStatus>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 61;
            dataVaultLog.ActionNote = "StockOrderStatus Created : " + input.Name;
            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _stockOrderStatusRepository.InsertAsync(stockOrderStatus);
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockOrderStatus_Edit)]
        protected virtual async Task Update(CreateOrEditStockOrderStatusDto input)
        {
            var stockOrderStatus = await _stockOrderStatusRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 61;
            dataVaultLog.ActionNote = "StockOrderStatus Updated : " + stockOrderStatus.Name;

            dataVaultLog.IsInventory = true;
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != stockOrderStatus.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = stockOrderStatus.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != stockOrderStatus.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = stockOrderStatus.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, stockOrderStatus);

            await _stockOrderStatusRepository.UpdateAsync(stockOrderStatus);


        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockOrderStatus_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _stockOrderStatusRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 61;
            dataVaultLog.ActionNote = "StockOrderStatus Deleted : " + Name;

            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _stockOrderStatusRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_StockOrderStatus_Export)]

        public async Task<FileDto> GetStockOrderStatusToExcel(GetAllStockOrderStatusForExcelInput input)
        {

            var filteredStockOrderStatus = _stockOrderStatusRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredStockOrderStatus
                         select new GetStockOrderStatusForViewDto()
                         {
                             StockOrderStatus = new StockOrderStatusDto
                             {
                                 Name = o.Name,
                                 Id = o.Id,
                                 IsActive = o.IsActive
                             }
                         });


            var ListDtos = await query.ToListAsync();

            return _stockOrderStatusExcelExporter.ExportToFile(ListDtos);
        }
    }
}
