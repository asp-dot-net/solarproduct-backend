﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Organizations;
using TheSolarProduct.Authorization;
using TheSolarProduct.Organizations.Dto;
using System.Linq.Dynamic.Core;
using Abp.Extensions;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Storage;
using Abp.UI;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using TheSolarProduct.MultiTenancy;
using System.Collections.Generic;
using TheSolarProduct.ApplicationSettings;
using static Abp.Zero.Configuration.AbpZeroSettingNames;
using Telerik.Reporting;
using TheSolarProduct.Wholesales.Ecommerces;

namespace TheSolarProduct.Organizations
{
    [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits)]
    public class OrganizationUnitAppService : TheSolarProductAppServiceBase, IOrganizationUnitAppService
    {
        private const int MaxFileBytes = 5242880; //5MB
        private readonly OrganizationUnitManager _organizationUnitManager;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<OrganizationUnitRole, long> _organizationUnitRoleRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendedOrganizationUnitRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly RoleManager _roleManager;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<OrganizationUnitMap> _organizationUnitMapRepository;
        private readonly IRepository<STCProvider> _stcProviderRepository;


        public OrganizationUnitAppService(
            OrganizationUnitManager organizationUnitManager,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            RoleManager roleManager,
            IRepository<OrganizationUnitRole, long> organizationUnitRoleRepository,
            IRepository<ExtendOrganizationUnit, long> extendedOrganizationUnitRepository,
            ITempFileCacheManager tempFileCacheManager,
            IWebHostEnvironment env,
            IRepository<Tenant> tenantRepository,
            IRepository<OrganizationUnitMap> organizationUnitMapRepository,
            IRepository<STCProvider> stcProviderRepository)
        {
            _organizationUnitManager = organizationUnitManager;
            _organizationUnitRepository = organizationUnitRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _roleManager = roleManager;
            _organizationUnitRoleRepository = organizationUnitRoleRepository;
            _extendedOrganizationUnitRepository = extendedOrganizationUnitRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _env = env;
            _tenantRepository = tenantRepository;
            _organizationUnitMapRepository = organizationUnitMapRepository;
            _stcProviderRepository = stcProviderRepository;
        }

        public async Task<ListResultDto<OrganizationUnitDto>> GetOrganizationUnits()
        {
            //var organizationUnits = await _organizationUnitRepository.GetAllListAsync();
            var organizationUnits = await _extendedOrganizationUnitRepository.GetAllListAsync();
            var organizationUnitMemberCounts = await _userOrganizationUnitRepository.GetAll()
                .GroupBy(x => x.OrganizationUnitId)
                .Select(groupedUsers => new
                {
                    organizationUnitId = groupedUsers.Key,
                    count = groupedUsers.Count()
                }).ToDictionaryAsync(x => x.organizationUnitId, y => y.count);

            var organizationUnitRoleCounts = await _organizationUnitRoleRepository.GetAll()
                .GroupBy(x => x.OrganizationUnitId)
                .Select(groupedRoles => new
                {
                    organizationUnitId = groupedRoles.Key,
                    count = groupedRoles.Count()
                }).ToDictionaryAsync(x => x.organizationUnitId, y => y.count);

            return new ListResultDto<OrganizationUnitDto>(
                organizationUnits.Select(ou =>
                {
                    var organizationUnitDto = ObjectMapper.Map<OrganizationUnitDto>(ou);
                    organizationUnitDto.MemberCount = organizationUnitMemberCounts.ContainsKey(ou.Id) ? organizationUnitMemberCounts[ou.Id] : 0;
                    organizationUnitDto.RoleCount = organizationUnitRoleCounts.ContainsKey(ou.Id) ? organizationUnitRoleCounts[ou.Id] : 0;
                    organizationUnitDto.ProjectId = ou.ProjectId;
                    organizationUnitDto.OrganizationCode = ou.OrganizationCode;
                    organizationUnitDto.GreenBoatUsername = ou.GreenBoatUsername;
                    organizationUnitDto.GreenBoatPassword = ou.GreenBoatPassword;
                    organizationUnitDto.defaultFromAddress = ou.defaultFromAddress;
                    organizationUnitDto.defaultFromDisplayName = ou.defaultFromDisplayName;
                    organizationUnitDto.FoneDynamicsPhoneNumber = ou.FoneDynamicsPhoneNumber;
                    organizationUnitDto.FoneDynamicsPropertySid = ou.FoneDynamicsPropertySid;
                    organizationUnitDto.FoneDynamicsAccountSid = ou.FoneDynamicsAccountSid;
                    organizationUnitDto.FoneDynamicsToken = ou.FoneDynamicsToken;
                    organizationUnitDto.Email = ou.Email;
                    organizationUnitDto.Mobile = ou.Mobile;
                    organizationUnitDto.paymentMethod = ou.paymentMethod;
                    organizationUnitDto.cardNumber = ou.cardNumber;
                    organizationUnitDto.expiryDateMonth = ou.expiryDateMonth;
                    organizationUnitDto.expiryDateYear = ou.expiryDateYear;
                    organizationUnitDto.cvn = ou.cvn;
                    organizationUnitDto.cardholderName = ou.cardholderName;
                    organizationUnitDto.MerchantId = ou.MerchantId;
                    organizationUnitDto.PublishKey = ou.PublishKey;
                    organizationUnitDto.SecrateKey = ou.SecrateKey;
                    organizationUnitDto.businesscode = ou.businesscode;
                    organizationUnitDto.AuthorizationKey = ou.AuthorizationKey;
                    organizationUnitDto.WestPacSecreteKey = ou.WestPacSecreteKey;
                    organizationUnitDto.WestPacPublishKey = ou.WestPacPublishKey;
                    organizationUnitDto.SurchargeAuthorizationKey = ou.SurchargeAuthorizationKey;
                    organizationUnitDto.SurchargeSecrateKey = ou.SurchargeSecrateKey;
                    organizationUnitDto.SurchargePublishKey = ou.SurchargePublishKey;
                    organizationUnitDto.GreenBoatPasswordForFetch = ou.GreenBoatPasswordForFetch;
                    organizationUnitDto.GreenBoatUsernameForFetch = ou.GreenBoatUsernameForFetch;
                    organizationUnitDto.ABNNumber = ou.ABNNumber;
                    organizationUnitDto.OrganizationUnitMaps = ObjectMapper.Map<List<OrganizationUnitMapDto>>(_organizationUnitMapRepository.GetAll().Where(e => e.OrganizationUnitId == ou.Id).ToList());
                    organizationUnitDto.IsDefault =ou.IsDefault;
                    organizationUnitDto.AccountId = ou.AccountId;
                    organizationUnitDto.GateWayAccountEmail = ou.GateWayAccountEmail;
                    organizationUnitDto.GateWayAccountPassword = ou.GateWayAccountPassword;
                    organizationUnitDto.smsFrom = ou.smsFrom;
                    organizationUnitDto.AutoAssignLeadUserId = ou.AutoAssignLeadUserId;
                    organizationUnitDto.STCProviderList = ObjectMapper.Map<List<STCProviderDto>>(_stcProviderRepository.GetAll().Where(e => e.OrganizationUnitId == ou.Id).ToList());
                    organizationUnitDto.ProjectOrderNo = ou.ProjectOrderNo;

                    return organizationUnitDto;
                }).ToList());
        }

        public async Task<PagedResultDto<OrganizationUnitUserListDto>> GetOrganizationUnitUsers(GetOrganizationUnitUsersInput input)
        {
            var query = from ouUser in _userOrganizationUnitRepository.GetAll()
                        join ou in _organizationUnitRepository.GetAll() on ouUser.OrganizationUnitId equals ou.Id
                        join user in UserManager.Users on ouUser.UserId equals user.Id
                        where ouUser.OrganizationUnitId == input.Id
                        select new
                        {
                            ouUser,
                            user
                        };

            var totalCount = await query.CountAsync();
            var items = await query.OrderBy(input.Sorting).PageBy(input).ToListAsync();

            return new PagedResultDto<OrganizationUnitUserListDto>(
                totalCount,
                items.Select(item =>
                {
                    var organizationUnitUserDto = ObjectMapper.Map<OrganizationUnitUserListDto>(item.user);
                    organizationUnitUserDto.AddedTime = item.ouUser.CreationTime;
                    return organizationUnitUserDto;
                }).ToList());
        }

        public async Task<PagedResultDto<OrganizationUnitRoleListDto>> GetOrganizationUnitRoles(GetOrganizationUnitRolesInput input)
        {
            var query = from ouRole in _organizationUnitRoleRepository.GetAll()
                        join ou in _organizationUnitRepository.GetAll() on ouRole.OrganizationUnitId equals ou.Id
                        join role in _roleManager.Roles on ouRole.RoleId equals role.Id
                        where ouRole.OrganizationUnitId == input.Id
                        select new
                        {
                            ouRole,
                            role
                        };

            var totalCount = await query.CountAsync();
            var items = await query.OrderBy(input.Sorting).PageBy(input).ToListAsync();

            return new PagedResultDto<OrganizationUnitRoleListDto>(
                totalCount,
                items.Select(item =>
                {
                    var organizationUnitRoleDto = ObjectMapper.Map<OrganizationUnitRoleListDto>(item.role);
                    organizationUnitRoleDto.AddedTime = item.ouRole.CreationTime;
                    return organizationUnitRoleDto;
                }).ToList());
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree)]
        public async Task<OrganizationUnitDto> CreateOrganizationUnit(CreateOrganizationUnitInput input)
        {
            var organizationUnits = await _organizationUnitRepository.GetAllListAsync();
            var Code = organizationUnits.OrderByDescending(e => e.Id).Select(e => e.Id).FirstOrDefault();

            var OrgCode = 00001;
            if(Code != null)
			{
                OrgCode = (int)Code + 1;
            }
            if (input.FileToken != null)
            {
                var ByteArray = _tempFileCacheManager.GetFile(input.FileToken);
                var filename = "";
                if (input.LogoFileName.Contains("+"))
                {
                    filename = input.LogoFileName.Replace("+", "_");
                }
                else
                {
                    filename = input.LogoFileName.Replace(" ", "_");
                }
                if (ByteArray == null)
                {
                    throw new UserFriendlyException("There is no such file with the token: " + input.FileToken);
                }

                if (ByteArray.Length > MaxFileBytes)
                {
                    throw new UserFriendlyException("Document size exceeded");
                }

                var ext = Path.GetExtension(input.LogoFileName);

                //input.FileType = DateTime.Now.Ticks + ext;
                var Path1 = _env.WebRootPath;
                var MainFolder = Path1;
                MainFolder = MainFolder + "\\Documents";
                var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();

               
                string TenantPath = MainFolder + "\\" + TenantName;
                string JobPath = TenantPath;
                string SignaturePath = "";

                SignaturePath = JobPath + "\\OrganizationWiseLogo";

                var FinalFilePath = SignaturePath + "\\" + filename;
                input.LogoFilePath = "\\Documents" + FinalFilePath;
                if (System.IO.Directory.Exists(MainFolder))
                {
                    if (System.IO.Directory.Exists(TenantPath))
                    {
                        if (System.IO.Directory.Exists(JobPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(JobPath);
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(TenantPath);
                        if (System.IO.Directory.Exists(JobPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(JobPath);
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(MainFolder);
                    if (System.IO.Directory.Exists(TenantPath))
                    {
                        if (System.IO.Directory.Exists(JobPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(JobPath);
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(TenantPath);
                        if (System.IO.Directory.Exists(JobPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(JobPath);
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                    }
                }

            }


            ExtendOrganizationUnit extend = new ExtendOrganizationUnit();
            extend.ABN = input.ABN;
            extend.Address = input.Address;
            extend.Code = input.OrganizationCode; //OrgCode.ToString().PadLeft(5, '0');
            extend.OrganizationCode = input.OrganizationCode;
            extend.ContactNo = input.ContactNo;
            extend.DisplayName = input.DisplayName;
            extend.Logo = input.Logo;
            extend.ProjectId = input.ProjectId;
            extend.TenantId = AbpSession.TenantId;
            extend.GreenBoatPassword = input.GreenBoatPassword;
            extend.GreenBoatUsername = input.GreenBoatUsername;
            extend.defaultFromAddress = input.defaultFromAddress;
            extend.defaultFromDisplayName = input.defaultFromDisplayName;
            extend.FoneDynamicsPhoneNumber = input.FoneDynamicsPhoneNumber;
            extend.FoneDynamicsPropertySid = input.FoneDynamicsPropertySid;
            extend.FoneDynamicsAccountSid = input.FoneDynamicsAccountSid;
            extend.FoneDynamicsToken = input.FoneDynamicsToken;
            extend.Mobile = input.Mobile;
            extend.Email = input.Email;
            extend.LogoFileName = input.LogoFileName;
            extend.LogoFilePath = input.LogoFilePath;
            extend.paymentMethod = input.paymentMethod;
            extend.cardNumber = input.cardNumber;
            extend.expiryDateMonth = input.expiryDateMonth;
            extend.expiryDateYear = input.expiryDateYear;
            extend.cvn = input.cvn;
            extend.cardholderName = input.cardholderName;
            extend.MerchantId = input.MerchantId;
            extend.PublishKey = input.PublishKey;
            extend.SecrateKey = input.SecrateKey;
            extend.businesscode = input.businesscode;
            extend.AuthorizationKey = input.AuthorizationKey;
            extend.WestPacSecreteKey = input.WestPacSecreteKey;
            extend.WestPacPublishKey = input.WestPacPublishKey;
            extend.SurchargeAuthorizationKey = input.SurchargeAuthorizationKey;
            extend.SurchargeSecrateKey = input.SurchargeSecrateKey;
            extend.SurchargePublishKey = input.SurchargePublishKey;
            extend.GreenBoatPasswordForFetch = input.GreenBoatPasswordForFetch;
            extend.GreenBoatUsernameForFetch = input.GreenBoatUsernameForFetch;
            extend.ABNNumber = input.ABNNumber;
            extend.IsDefault = input.IsDefault;

            extend.smsFrom = input.smsFrom;
            extend.AccountId = input.AccountId;
            extend.GateWayAccountPassword = input.GateWayAccountPassword;
            extend.GateWayAccountEmail = input.GateWayAccountEmail;
            extend.AutoAssignLeadUserId = input.AutoAssignLeadUserId;
            //extend.MapProvider = input.MapProvider;
            //extend.MapApiKey = input.MapApiKey;
            extend.ProjectOrderNo = input.ProjectOrderNo;

            await _extendedOrganizationUnitRepository.InsertAsync(extend);

            if(extend.Id > 0)
            {
                foreach (var item in input.OrganizationUnitMaps)
                {
                    var dto = ObjectMapper.Map<OrganizationUnitMap>(item);
                    dto.OrganizationUnitId = extend.Id;
                    await _organizationUnitMapRepository.InsertAsync(dto);
                }

                foreach(var item in input.STCProviderList)
                {
                    item.OrganizationUnitId = extend.Id;
                    var stc = ObjectMapper.Map<STCProvider>(item);
                    await _stcProviderRepository.InsertAsync(stc);
                }

            }

			return null;
            //return ObjectMapper.Map<OrganizationUnitDto>(organizationUnit);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree)]
        public async Task<OrganizationUnitDto> UpdateOrganizationUnit(UpdateOrganizationUnitInput input)
        {
            var organizationUnit = await _extendedOrganizationUnitRepository.GetAsync(input.Id);
            if (input.FileToken != null)
            {
                var ByteArray = _tempFileCacheManager.GetFile(input.FileToken);
                var filename = "";
                if (input.LogoFileName.Contains("+"))
                {
                    filename = input.LogoFileName.Replace("+", "_");
                }
                else
                {
                    filename = input.LogoFileName.Replace(" ", "_");
                }
                if (ByteArray == null)
                {
                    throw new UserFriendlyException("There is no such file with the token: " + input.FileToken);
                }

                if (ByteArray.Length > MaxFileBytes)
                {
                    throw new UserFriendlyException("Document size exceeded");
                }

                var ext = ".png";
                input.LogoFileName = input.LogoFileName + ext;

                var MainFolder = ApplicationSettingConsts.DocumentPath;
                
                MainFolder = MainFolder + "\\Documents";
                var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();

                string TenantPath = MainFolder + "\\" + TenantName;
                string JobPath = TenantPath;
                string SignaturePath = "";

                SignaturePath = JobPath + "\\OrganizationWiseLogo\\" + input.OrganizationCode;

                var FinalFilePath = SignaturePath + "\\" + filename +ext;
                input.LogoFilePath = "\\Documents" + "\\" + TenantName + "\\OrganizationWiseLogo\\" + input.OrganizationCode + "\\";

                if (System.IO.Directory.Exists(MainFolder))
                {
                    if (System.IO.Directory.Exists(TenantPath))
                    {
                        if (System.IO.Directory.Exists(JobPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(JobPath);
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(TenantPath);
                        if (System.IO.Directory.Exists(JobPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(JobPath);
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(MainFolder);
                    if (System.IO.Directory.Exists(TenantPath))
                    {
                        if (System.IO.Directory.Exists(JobPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(JobPath);
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(TenantPath);
                        if (System.IO.Directory.Exists(JobPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(JobPath);
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                    }
                }

            }
            organizationUnit.DisplayName = input.DisplayName;
            organizationUnit.Code = input.OrganizationCode;
            organizationUnit.OrganizationCode = input.OrganizationCode;
            organizationUnit.ProjectId = input.ProjectId;
            organizationUnit.GreenBoatPassword = input.GreenBoatPassword;
            organizationUnit.GreenBoatUsername = input.GreenBoatUsername;
            organizationUnit.defaultFromDisplayName = input.defaultFromDisplayName;
            organizationUnit.defaultFromAddress = input.defaultFromAddress;
            organizationUnit.FoneDynamicsPhoneNumber = input.FoneDynamicsPhoneNumber;
            organizationUnit.FoneDynamicsPropertySid = input.FoneDynamicsPropertySid;
            organizationUnit.FoneDynamicsAccountSid = input.FoneDynamicsAccountSid;
            organizationUnit.FoneDynamicsToken = input.FoneDynamicsToken;
            organizationUnit.Mobile = input.Mobile;
            organizationUnit.Email = input.Email;
            organizationUnit.LogoFilePath = string.IsNullOrEmpty(input.LogoFilePath) ? organizationUnit.LogoFilePath : input.LogoFilePath;
            organizationUnit.LogoFileName = string.IsNullOrEmpty(input.LogoFileName) ? organizationUnit.LogoFileName : input.LogoFileName;
            organizationUnit.paymentMethod = input.paymentMethod;
            organizationUnit.cardNumber = input.cardNumber;
            organizationUnit.expiryDateMonth = input.expiryDateMonth;
            organizationUnit.expiryDateYear = input.expiryDateYear;
            organizationUnit.cvn = input.cvn;
            organizationUnit.cardholderName = input.cardholderName;
            organizationUnit.MerchantId = input.MerchantId;
            organizationUnit.PublishKey = input.PublishKey;
            organizationUnit.SecrateKey = input.SecrateKey;
            organizationUnit.businesscode = input.businesscode;
            organizationUnit.AuthorizationKey = input.AuthorizationKey;
            organizationUnit.WestPacSecreteKey = input.WestPacSecreteKey;
            organizationUnit.WestPacPublishKey = input.WestPacPublishKey;
            organizationUnit.SurchargeAuthorizationKey = input.SurchargeAuthorizationKey;
            organizationUnit.SurchargeSecrateKey = input.SurchargeSecrateKey;
            organizationUnit.SurchargePublishKey = input.SurchargePublishKey;
            organizationUnit.GreenBoatPasswordForFetch = input.GreenBoatPasswordForFetch;
            organizationUnit.GreenBoatUsernameForFetch = input.GreenBoatUsernameForFetch;
            organizationUnit.ABNNumber = input.ABNNumber;
            organizationUnit.IsDefault = input.IsDefault; 
            organizationUnit.smsFrom = input.smsFrom;
            organizationUnit.AccountId = input.AccountId;
            organizationUnit.GateWayAccountPassword = input.GateWayAccountPassword;
            organizationUnit.GateWayAccountEmail = input.GateWayAccountEmail;
            //organizationUnit.MapProvider = input.MapProvider;
            //organizationUnit.MapApiKey = input.MapApiKey;
            organizationUnit.AutoAssignLeadUserId = input.AutoAssignLeadUserId;
            organizationUnit.ProjectOrderNo = input.ProjectOrderNo;

            if (input.OrganizationUnitMaps != null)
            {
                //var existData = _organizationUnitMapRepository.GetAll().AsNoTracking().Where(x => x.OrganizationUnitId == organizationUnit.Id).FirstOrDefault();
                var existData = _organizationUnitMapRepository.GetAll().AsNoTracking().Where(x => x.OrganizationUnitId == organizationUnit.Id).Select(e => e.Id).ToList();
                if (existData.Any())
                {
                    var Ids = input.OrganizationUnitMaps.Select(e => e.Id).ToList();
                    foreach (var item in existData)
                    {
                        if(Ids.Count > 0)
                        {
                            bool containsItem = Ids.Any(x => x == item);
                            if(containsItem == false)
                            {
                                await _organizationUnitMapRepository.DeleteAsync(item);
                            }
                        }
                        else
                        {

                            await _organizationUnitMapRepository.DeleteAsync(item);
                        }
                    }
                    //bool exists = existData.co
                    foreach (var item in input.OrganizationUnitMaps)
                    {
                        if(item.Id != null && item.Id != 0)
                        {
                            var dto = await _organizationUnitMapRepository.GetAsync((int)item.Id);
                            dto.MapProvider = item.MapProvider;
                            dto.MapApiKey = item.MapApiKey;
                            await _organizationUnitMapRepository.UpdateAsync(dto);
                        }
                        else
                        {
                            var dto = ObjectMapper.Map<OrganizationUnitMap>(item);
                            dto.OrganizationUnitId = organizationUnit.Id;
                            await _organizationUnitMapRepository.InsertAsync(dto);
                        }
                    }
                }
                else
                {
                    foreach (var item in input.OrganizationUnitMaps)
                    {
                        var dto = ObjectMapper.Map<OrganizationUnitMap>(item);
                        dto.OrganizationUnitId = organizationUnit.Id;
                        await _organizationUnitMapRepository.InsertAsync(dto);
                    }
                }

                
            }

            if (input.STCProviderList != null)
            {
                //var existData = _stcProviderRepository.GetAll().AsNoTracking().Where(x => x.OrganizationUnitId == organizationUnit.Id).FirstOrDefault();
                var existData = _stcProviderRepository.GetAll().AsNoTracking().Where(x => x.OrganizationUnitId == organizationUnit.Id).Select(e => e.Id).ToList();
                if (existData.Any())
                {
                    var Ids = input.STCProviderList.Select(e => e.Id).ToList();
                    foreach (var item in existData)
                    {
                        if (Ids.Count > 0)
                        {
                            bool containsItem = Ids.Any(x => x == item);
                            if (containsItem == false)
                            {
                                await _stcProviderRepository.DeleteAsync(item);
                            }
                        }
                        else
                        {

                            await _stcProviderRepository.DeleteAsync(item);
                        }
                    }
                    //bool exists = existData.co
                    foreach (var item in input.STCProviderList)
                    {
                        if (item.Id != null && item.Id != 0)
                        {
                            var dto = await _stcProviderRepository.GetAsync((int)item.Id);
                            ObjectMapper.Map(item, dto);
                            dto.OrganizationUnitId = organizationUnit.Id;
                            await _stcProviderRepository.UpdateAsync(dto);
                        }
                        else
                        {
                            var dto = ObjectMapper.Map<STCProvider>(item);
                            dto.OrganizationUnitId = organizationUnit.Id;
                            await _stcProviderRepository.InsertAsync(dto);
                        }
                    }
                }
                else
                {
                    foreach (var item in input.STCProviderList)
                    {
                        var dto = ObjectMapper.Map<STCProvider>(item);
                        dto.OrganizationUnitId = organizationUnit.Id;
                        await _stcProviderRepository.InsertAsync(dto);
                    }
                }


            }

            await _extendedOrganizationUnitRepository.UpdateAsync(organizationUnit);

            return await CreateOrganizationUnitDto(organizationUnit);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree)]
        public async Task<OrganizationUnitDto> MoveOrganizationUnit(MoveOrganizationUnitInput input)
        {
            await _organizationUnitManager.MoveAsync(input.Id, input.NewParentId);

            return await CreateOrganizationUnitDto(
                await _organizationUnitRepository.GetAsync(input.Id)
                );
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree)]
        public async Task DeleteOrganizationUnit(EntityDto<long> input)
        {
            await _userOrganizationUnitRepository.DeleteAsync(x => x.OrganizationUnitId == input.Id);
            await _organizationUnitRoleRepository.DeleteAsync(x => x.OrganizationUnitId == input.Id);
            await _organizationUnitManager.DeleteAsync(input.Id);
        }


        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers)]
        public async Task RemoveUserFromOrganizationUnit(UserToOrganizationUnitInput input)
        {
            await UserManager.RemoveFromOrganizationUnitAsync(input.UserId, input.OrganizationUnitId);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageRoles)]
        public async Task RemoveRoleFromOrganizationUnit(RoleToOrganizationUnitInput input)
        {
            await _roleManager.RemoveFromOrganizationUnitAsync(input.RoleId, input.OrganizationUnitId);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers)]
        public async Task AddUsersToOrganizationUnit(UsersToOrganizationUnitInput input)
        {
            foreach (var userId in input.UserIds)
            {
                await UserManager.AddToOrganizationUnitAsync(userId, input.OrganizationUnitId);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageRoles)]
        public async Task AddRolesToOrganizationUnit(RolesToOrganizationUnitInput input)
        {
            foreach (var roleId in input.RoleIds)
            {
                await _roleManager.AddToOrganizationUnitAsync(roleId, input.OrganizationUnitId, AbpSession.TenantId);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers)]
        public async Task<PagedResultDto<NameValueDto>> FindUsers(FindOrganizationUnitUsersInput input)
        {
            var userIdsInOrganizationUnit = _userOrganizationUnitRepository.GetAll()
                .Where(uou => uou.OrganizationUnitId == input.OrganizationUnitId)
                .Select(uou => uou.UserId);

            var query = UserManager.Users
                .Where(u => !userIdsInOrganizationUnit.Contains(u.Id))
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u =>
                        u.Name.Contains(input.Filter) ||
                        u.Surname.Contains(input.Filter) ||
                        u.UserName.Contains(input.Filter) ||
                        u.EmailAddress.Contains(input.Filter)
                );

            var userCount = await query.CountAsync();
            var users = await query
                .OrderBy(u => u.Name)
                .ThenBy(u => u.Surname)
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<NameValueDto>(
                userCount,
                users.Select(u =>
                    new NameValueDto(
                        u.FullName + " (" + u.EmailAddress + ")",
                        u.Id.ToString()
                    )
                ).ToList()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageRoles)]
        public async Task<PagedResultDto<NameValueDto>> FindRoles(FindOrganizationUnitRolesInput input)
        {
            var roleIdsInOrganizationUnit = _organizationUnitRoleRepository.GetAll()
                .Where(uou => uou.OrganizationUnitId == input.OrganizationUnitId)
                .Select(uou => uou.RoleId);

            var query = _roleManager.Roles
                .Where(u => !roleIdsInOrganizationUnit.Contains(u.Id))
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u =>
                        u.DisplayName.Contains(input.Filter) ||
                        u.Name.Contains(input.Filter)
                );

            var roleCount = await query.CountAsync();
            var users = await query
                .OrderBy(u => u.DisplayName)
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<NameValueDto>(
                roleCount,
                users.Select(u =>
                    new NameValueDto(
                        u.DisplayName,
                        u.Id.ToString()
                    )
                ).ToList()
            );
        }

        private async Task<OrganizationUnitDto> CreateOrganizationUnitDto(OrganizationUnit organizationUnit)
        {
            var dto = ObjectMapper.Map<OrganizationUnitDto>(organizationUnit);
            dto.MemberCount = await _userOrganizationUnitRepository.CountAsync(uou => uou.OrganizationUnitId == organizationUnit.Id);
            return dto;
        }
        public async Task<OrganizationUnitDto> GetOrganizationDetailsById(int organizationId)
        {
            var output = await _extendedOrganizationUnitRepository.GetAll().Where(x => x.Id == organizationId).FirstOrDefaultAsync();
            var result = ObjectMapper.Map<OrganizationUnitDto>(output);

            //ExtendOrganizationUnit

            return result;
        }
    }
}