﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.WholesalePropertyTypes.Dtos;
using TheSolarProduct.Wholesales.DataVault;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct.WholesaleTransportTypes.Dtos;
using Abp.Collections.Extensions;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;


namespace TheSolarProduct.WholesalePropertyTypes
{
    public class WholesalePropertyTypeAppService : TheSolarProductAppServiceBase, IWholesalePropertyTypeAppService
    {
        private readonly IRepository<WholesalePropertyType> _wholesalePropertyTypeRepository;
       
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;

        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public WholesalePropertyTypeAppService(

            IRepository<WholesalePropertyType> WholesalePropertyTypeRepository,
             // IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
             IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)

        {


            _wholesalePropertyTypeRepository = WholesalePropertyTypeRepository;
            // _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;

        }


        public async Task<PagedResultDto<GetWholesalePropertyTypeForViewDto>> GetAll(GetAllWholesalePropertyTypeInput input)
        {
            var propertytype = _wholesalePropertyTypeRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFiltered = propertytype
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFiltered
                         select new GetWholesalePropertyTypeForViewDto()
                         {
                             PropertyType = new WholesalePropertTypeDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 IsActive = o.IsActive
                             }
                         };

            var totalCount = await propertytype.CountAsync();

            return new PagedResultDto<GetWholesalePropertyTypeForViewDto>(totalCount, await output.ToListAsync());
        }

        public async Task<GetWholesalePropertyTypeForEditOutput> GetWholesalePropertyTypeForEdit(EntityDto input)
        {
            var propertytype = await _wholesalePropertyTypeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetWholesalePropertyTypeForEditOutput { PropertyType = ObjectMapper.Map<CreateOrEditWholesalePropertyTypeDto>(propertytype) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditWholesalePropertyTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        protected virtual async Task Create(CreateOrEditWholesalePropertyTypeDto input)
        {
            var properytype = ObjectMapper.Map<WholesalePropertyType>(input);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 9;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "WholesalePropertyType Created: " + input.Name;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

            await _wholesalePropertyTypeRepository.InsertAsync(properytype);
        }

        protected virtual async Task Update(CreateOrEditWholesalePropertyTypeDto input)
        {
            var properytype = await _wholesalePropertyTypeRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 9;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "WholesalePropertyType Updated : " + properytype.Name;
            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            var List = new List<WholeSaleDataVaultActivityLogHistory>();
            if (input.Name != properytype.Name)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = properytype.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != properytype.IsActive)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = properytype.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, properytype);

            await _wholesalePropertyTypeRepository.UpdateAsync(properytype);
        }
        public async Task Delete(EntityDto input)
        {
            var Name = _wholesalePropertyTypeRepository.Get(input.Id).Name;

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 9;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "WholesalePropertyType Deleted  : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            await _wholesalePropertyTypeRepository.DeleteAsync(input.Id);
        }

    }
}
