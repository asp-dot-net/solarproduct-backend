﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ServiceSources.Dtos;
using TheSolarProduct.ServiceSources.Exporting;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.Dto;
using TheSolarProduct.DataVaults;
using Abp.EntityFrameworkCore;
using TheSolarProduct.ServiceCategorys;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.ServiceStatuses;

namespace TheSolarProduct.ServiceSources
{
    [AbpAuthorize(AppPermissions.Pages_ServiceSources, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    public class ServiceSourcesAppService : TheSolarProductAppServiceBase, IServiceSourcesAppService
    {
        private readonly IRepository<ServiceSource> _serviceSourcesRepository;
        private readonly IServiceSourcesExcelExporter _serviceSourcesExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public ServiceSourcesAppService(IRepository<ServiceSource> serviceSourcesRepository, IServiceSourcesExcelExporter serviceSourcesExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _serviceSourcesRepository = serviceSourcesRepository;
            _serviceSourcesExcelExporter = serviceSourcesExcelExporter;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;

        }

        public async Task<PagedResultDto<GetServiceSourcesForViewDto>> GetAll(GetAllServiceSourcesInput input)
        {

            var filteredServiceSources = _serviceSourcesRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ServiceSourcesFilter), e => e.Name == input.ServiceSourcesFilter);

            var pagedAndFilteredServiceSources = filteredServiceSources
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var holdReasons = from o in pagedAndFilteredServiceSources
                              select new GetServiceSourcesForViewDto()
                              {
                                  JobServiceSources = new ServiceSourcesDto
                                  {
                                      JobServiceSources = o.Name,
                                      Id = o.Id,
                                      IsActive = o.IsActive
                                  }
                              };

            var totalCount = await filteredServiceSources.CountAsync();

            return new PagedResultDto<GetServiceSourcesForViewDto>(
                totalCount,
                await holdReasons.ToListAsync()
            );
        }

        public async Task<GetServiceSourcesForViewDto> GetServiceSourcesForView(int id)
        {
            var servicesources = await _serviceSourcesRepository.GetAsync(id);

            var output = new GetServiceSourcesForViewDto { JobServiceSources = ObjectMapper.Map<ServiceSourcesDto>(servicesources) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceSources_Edit)]
        public async Task<GetServiceSourcesForEditOutput> GetServiceSourcesForEdit(EntityDto input)
        {
            var servicesources = await _serviceSourcesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetServiceSourcesForEditOutput { JobServiceSources = ObjectMapper.Map<CreateOrEditServiceSourcesDto>(servicesources) };

            return output;
        }
        public async Task CreateOrEdit(CreateOrEditServiceSourcesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceSources_Create)]
        protected virtual async Task Create(CreateOrEditServiceSourcesDto input)
        {
            var servicesources = ObjectMapper.Map<ServiceSource>(input);

            if (AbpSession.TenantId != null)
            {
                servicesources.TenantId = (int)AbpSession.TenantId;
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 33;
            dataVaultLog.ActionNote = "Service Source Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _serviceSourcesRepository.InsertAsync(servicesources);
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceSources_Edit)]
        protected virtual async Task Update(CreateOrEditServiceSourcesDto input)
        {
            var servicesources = await _serviceSourcesRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 33;
            dataVaultLog.ActionNote = "Service Source Updated : " + servicesources.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != servicesources.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = servicesources.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != servicesources.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = servicesources.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, servicesources);
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceSources_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _serviceSourcesRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 33;
            dataVaultLog.ActionNote = "Service Source Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _serviceSourcesRepository.DeleteAsync(input.Id);
        }
        public async Task<FileDto> GetServiceSourcesToExcel(GetAllServiceSourcesForExcelInput input)
        {

            var filteredServiceSources = _serviceSourcesRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ServiceSourcesFilter), e => e.Name == input.ServiceSourcesFilter);

            var query = (from o in filteredServiceSources
                         select new GetServiceSourcesForViewDto()
                         {
                             JobServiceSources = new ServiceSourcesDto
                             {
                                 JobServiceSources = o.Name,
                                 Id = o.Id,
                                 IsActive = o.IsActive,
                             }
                         });

            var serviceSourcesListDtos = await query.ToListAsync();

            return _serviceSourcesExcelExporter.ExportToFile(serviceSourcesListDtos);
        }
    }
}
