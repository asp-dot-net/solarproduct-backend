﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.ServiceSources.Dtos;
using TheSolarProduct.Storage;

namespace TheSolarProduct.ServiceSources.Exporting
{
    public class ServiceSourcesExcelExporter : NpoiExcelExporterBase, IServiceSourcesExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ServiceSourcesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetServiceSourcesForViewDto> serviceSourcesListDtos)
        {
            return CreateExcelPackage(
                "serviceSources.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("serviceSources"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("IsActive")
                        );

                    AddObjects(
                        sheet, 2, serviceSourcesListDtos,
                        _ => _.JobServiceSources.JobServiceSources,
                        _ => _.JobServiceSources.IsActive ? L("Yes") : L("No")
                        );

                });
        }
    }
}
