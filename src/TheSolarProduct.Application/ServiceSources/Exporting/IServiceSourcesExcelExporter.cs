﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.ServiceSources.Dtos;

namespace TheSolarProduct.ServiceSources.Exporting
{
    public interface IServiceSourcesExcelExporter
    {
        FileDto ExportToFile(List<GetServiceSourcesForViewDto> holdReasons);
    }
}
