﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.LeadSources.Exporting;
using TheSolarProduct.LeadSources.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.UI;
using TheSolarProduct.Storage;
using Abp.Organizations;
using TheSolarProduct.CallFlowQueues.Dtos;
using TheSolarProduct.LeadSources;
using TheSolarProduct.TheSolarDemo.Exporting;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Quotations;
using System.Xml.Linq;
using Telerik.Reporting;

namespace TheSolarProduct.CallFlowQueues
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class CallFlowQueueAppService : TheSolarProductAppServiceBase, ICallFlowQueueAppService
    {
        private readonly IRepository<CallFlowQueue> _callFlowQueRepository;
        private readonly IRepository<CallFlowQueueOrganizationUnit> _callFlowQueOrganizationUnitRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public CallFlowQueueAppService(
            IRepository<CallFlowQueue> callFlowQueRepository
            , IRepository<CallFlowQueueOrganizationUnit> callFlowQueOrganizationUnitRepository
            , IRepository<OrganizationUnit, long> organizationUnitRepository
            , IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            )
        {
            _callFlowQueRepository = callFlowQueRepository;
            _callFlowQueOrganizationUnitRepository = callFlowQueOrganizationUnitRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
        }

        public async Task<PagedResultDto<GetCallFlowQueueForViewDto>> GetAll(GetAllCallFlowQueuesInput input)
        {
            var filtered = _callFlowQueRepository.GetAll()
                       .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name == input.Filter);

            var pagedAndFiltered = filtered
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var callFlowQueue = from o in pagedAndFiltered
                              select new
                              {

                                  o.Name,
                                  o.Id,
                                  o.ExtentionNumber,
                                  o.IsActive
                              };

            var totalCount = await filtered.CountAsync();

            var dbList = await callFlowQueue.ToListAsync();
            var results = new List<GetCallFlowQueueForViewDto>();

            foreach (var o in dbList)
            {
                var res = new GetCallFlowQueueForViewDto()
                {
                    CallFlowQueue = new CallFlowQueueDto
                    {
                        Name = o.Name,
                        Id = o.Id,
                        ExtentionNumber = o.ExtentionNumber,
                        IsActive = o.IsActive,
                        OrganizationUnitsName = (from lo in _callFlowQueOrganizationUnitRepository.GetAll()
                                                 join ou in _organizationUnitRepository.GetAll() on lo.OrganizationUnitId equals ou.Id
                                                 where lo.CallFlowQueueId == o.Id
                                                 select ou.DisplayName
                                                 ).ToList()
                    }
                };

                results.Add(res);
            }

            return new PagedResultDto<GetCallFlowQueueForViewDto>(
                totalCount,
                results
            );
        }

        public async Task CreateOrEdit(CreateOrEditCallFlowQueueDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        [AbpAuthorize(AppPermissions.Pages_CallFlowQueues_Create)]
        protected virtual async Task Create(CreateOrEditCallFlowQueueDto input)
        {
            var callFlowQueue = ObjectMapper.Map<CallFlowQueue>(input);

            var Id = await _callFlowQueRepository.InsertAndGetIdAsync(callFlowQueue);

            CallFlowQueueOrganizationUnit callFlowQueueOrganizationUnit;
            if (input.OrganizationUnits != null)
            {
                foreach (var item in input.OrganizationUnits)
                {
                    callFlowQueueOrganizationUnit = new CallFlowQueueOrganizationUnit();
                    callFlowQueueOrganizationUnit.CallFlowQueueId = Id;
                    callFlowQueueOrganizationUnit.OrganizationUnitId = item;
                    _callFlowQueOrganizationUnitRepository.InsertOrUpdate(callFlowQueueOrganizationUnit);
                }
            }
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 43;
            dataVaultLog.ActionNote = "Call Flow Queue Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);
        }

        [AbpAuthorize(AppPermissions.Pages_CallFlowQueues_Edit)]
        protected virtual async Task Update(CreateOrEditCallFlowQueueDto input)
        {
            var callFlowQueue = await _callFlowQueRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 43;
            dataVaultLog.ActionNote = "Call Flow Queue Updated : " + callFlowQueue.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != callFlowQueue.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = callFlowQueue.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.ExtentionNumber != callFlowQueue.ExtentionNumber)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "ExtentionNumber";
                history.PrevValue = callFlowQueue.ExtentionNumber.ToString();
                history.CurValue = input.ExtentionNumber.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != callFlowQueue.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = callFlowQueue.ExtentionNumber.ToString();
                history.CurValue = input.ExtentionNumber.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            //Lead Source OrganizationUnits
            CallFlowQueueOrganizationUnit callFlowQueueOrganizationUnit;

            var orgs = _callFlowQueOrganizationUnitRepository.GetAll().Where(x => x.CallFlowQueueId == callFlowQueue.Id).Select(e => e.OrganizationUnitId).ToList();
            orgs.Sort();
            

            var orgStr = String.Join(",", orgs);


            input.OrganizationUnits = input.OrganizationUnits != null ? input.OrganizationUnits : new List<long>();
            input.OrganizationUnits.Sort();
            var inputOrgIds = String.Join(",", input.OrganizationUnits);

            //foreach (var id in input.OrganizationUnits)
            //{
            //    inputOrgIds = inputOrgIds + ",";
            //}

            if (orgStr != inputOrgIds)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Organizations";
                history.PrevValue = orgs?.Count() > 0 ? string.Join(" ,", _organizationUnitRepository.GetAll().Where(e => orgs.Contains((int)e.Id)).Select(e => e.DisplayName)) : "";
                history.CurValue = input.OrganizationUnits?.Count() > 0 ? string.Join(" ,", _organizationUnitRepository.GetAll().Where(e => input.OrganizationUnits.Contains(e.Id)).Select(e => e.DisplayName)): "";
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }


            if (input.OrganizationUnits != null)
            {
                _callFlowQueOrganizationUnitRepository.Delete(x => x.CallFlowQueueId == callFlowQueue.Id);

                foreach (var item in input.OrganizationUnits)
                {
                    callFlowQueueOrganizationUnit = new CallFlowQueueOrganizationUnit();
                    callFlowQueueOrganizationUnit.CallFlowQueueId = callFlowQueue.Id;
                    callFlowQueueOrganizationUnit.OrganizationUnitId = item;
                    _callFlowQueOrganizationUnitRepository.InsertOrUpdate(callFlowQueueOrganizationUnit);
                }
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
            ObjectMapper.Map(input, callFlowQueue);
            await _callFlowQueRepository.UpdateAsync(callFlowQueue);

        }


        [AbpAuthorize(AppPermissions.Pages_CallFlowQueues_Delete)]
        public async Task Delete(EntityDto input)
        {
            var name = _callFlowQueRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 43;
            dataVaultLog.ActionNote = "Call Flow Queue Deleted : " + name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _callFlowQueRepository.DeleteAsync(input.Id);
        }

        public Task<GetCallFlowQueueForViewDto> GetCallFlowQueueForView(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<GetCallFlowQueuesForEditOutput> GetCallFlowQueueForEdit(EntityDto input)
        {
            var callFlowQueue = await _callFlowQueRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetCallFlowQueuesForEditOutput { CallFlowQueueDto = ObjectMapper.Map<CreateOrEditCallFlowQueueDto>(callFlowQueue) };

            output.CallFlowQueueDto.OrganizationUnits = await _callFlowQueOrganizationUnitRepository.GetAll().Where(e => e.CallFlowQueueId == input.Id).Select(e => (long)e.OrganizationUnitId).ToListAsync();

            return output;
        }

        //public async Task<FileDto> GetLeadSourcesToExcel(GetAllCallFlowQueuesInput input)
        //{
        //    var filtered = _callFlowQueRepository.GetAll()
        //               .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name == input.Filter); ;

        //    var query = (from o in filtered
        //                 select new GetCallFlowQueueForViewDto()
        //                 {
        //                     CallFlowQueue = new CallFlowQueueDto
        //                     {
        //                         Name = o.Name,
        //                         ExtentionNumber = o.ExtentionNumber,
        //                         Id = o.Id
        //                     }
        //                 });

        //    var listDtos = await query.ToListAsync();

        //    return _leadSourcesExcelExporter.ExportToFile(leadSourceListDtos);
        //}
    }
}
