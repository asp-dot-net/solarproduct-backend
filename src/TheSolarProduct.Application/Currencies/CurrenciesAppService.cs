﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Departments.Exporting;
using TheSolarProduct.Currencies;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using TheSolarProduct.Currencies.Dtos;
using Abp.Collections.Extensions;
using System.Linq;
using TheSolarProduct.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using TheSolarProduct.CheckDepositReceived;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using TheSolarProduct.CheckDepositReceived.Dto;
using TheSolarProduct.TransportationCosts;
using TheSolarProduct.TransportCompanies.Dto;
using Abp;
using TheSolarProduct.CheckApplications.Dtos;
using TheSolarProduct.Currencies.Exporting;
using TheSolarProduct.TransportCompanies.Exporting;



namespace TheSolarProduct.Currencies
{
    public class CurrenciesAppService : TheSolarProductAppServiceBase, ICurrenciesAppService
    {
        
    
		 private readonly IRepository<Currency> _currencyRepository;
         private readonly ICurrenciesExcelExporter _currencyExcelExporter;

        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;



        public CurrenciesAppService(IRepository<Currency> currencyRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider, ICurrenciesExcelExporter CurrencyExcelExporter)
        {
            _currencyExcelExporter = CurrencyExcelExporter;
            _currencyRepository = currencyRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;

        }

        public async Task<PagedResultDto<GetCurrenciesForViewDto>> GetAll(GetAllCurrenciesInput input)
        {
            var currency = _currencyRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFiltered = currency
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFiltered
                         select new GetCurrenciesForViewDto()
                         {
                             Currency = new CurrenciesDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 IsActive = o.IsActive
                             }
                         };

            var totalCount = await currency.CountAsync();

            return new PagedResultDto<GetCurrenciesForViewDto>(totalCount, await output.ToListAsync());
        }


        public async Task<GetCurrenciesForEditOutput> GetCurrenciesForEdit(EntityDto input)
        {
            var currency = await _currencyRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetCurrenciesForEditOutput { Currency = ObjectMapper.Map<CreateOrEditCurrenciesDto>(currency) };

            return output;
        }






        public async Task CreateOrEdit(CreateOrEditCurrenciesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }



        protected virtual async Task Create(CreateOrEditCurrenciesDto input)
        {
            var currency = ObjectMapper.Map<Currency>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 66;
            dataVaultLog.ActionNote = "Currency Created: " + input.Name;
            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _currencyRepository.InsertAsync(currency);
        }


        protected virtual async Task Update(CreateOrEditCurrenciesDto input)
        {
            var currency = await _currencyRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 66;
            dataVaultLog.ActionNote = "Currency Updated : " + currency.Name;
            dataVaultLog.IsInventory = true;
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            var List = new List<DataVaultActivityLogHistory>();
            if (input.Name != currency.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = currency.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != currency.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = currency.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, currency);

            await _currencyRepository.UpdateAsync(currency);
        }



        public async Task Delete(EntityDto input)
        {
            var Name = _currencyRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 66;
            dataVaultLog.ActionNote = "Currency Deleted  : " + Name;

            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _currencyRepository.DeleteAsync(input.Id);
        }


        public async Task<FileDto> GetCurrenciesToExcel(GetAllCurrenciesForExcelInput input)
        {

            var filteredCurrency = _currencyRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredCurrency
                         select new GetCurrenciesForViewDto()
                         {
                             Currency = new CurrenciesDto
                             {
                                 Name = o.Name,
                                 Id = o.Id
                             }
                         });


            var CurrencyListDtos = await query.ToListAsync();

            return _currencyExcelExporter.ExportToFile(CurrencyListDtos);
        }


    }
}
