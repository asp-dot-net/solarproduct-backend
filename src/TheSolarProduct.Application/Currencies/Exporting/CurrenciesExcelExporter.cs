﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Currencies.Dtos;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.TransportCompanies.Dto;
using TheSolarProduct.TransportCompanies.Exporting;

namespace TheSolarProduct.Currencies.Exporting
{
    public class CurrenciesExcelExporter : NpoiExcelExporterBase, ICurrenciesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public CurrenciesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }


        public FileDto ExportToFile(List<GetCurrenciesForViewDto> currency)
        {
            return CreateExcelPackage(
                "TransportCompany.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet("Currencies");

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, 2, currency,
                        _ => _.Currency.Name
                        );



                });
        }
    }
}
