﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Currencies.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.TransportCompanies.Dto;

namespace TheSolarProduct.Currencies.Exporting
{
    public interface ICurrenciesExcelExporter
    {
        FileDto ExportToFile(List<GetCurrenciesForViewDto> currency);
    }
}
