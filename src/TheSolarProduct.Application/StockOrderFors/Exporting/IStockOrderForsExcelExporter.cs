﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Currencies.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.StockOrderFors.Dtos;

namespace TheSolarProduct.StockOrderFors.Exporting
{
    public interface IStockOrderForsExcelExporter
    {
        FileDto ExportToFile(List<GetStockOrderForsForViewDto> currency);
    }
}
