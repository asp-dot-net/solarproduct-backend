﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Currencies.Dtos;
using TheSolarProduct.Currencies.Exporting;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.StockOrderFors.Dtos;
using TheSolarProduct.Storage;

namespace TheSolarProduct.StockOrderFors.Exporting
{
    public class StockOrderForsExcelExporter : NpoiExcelExporterBase, IStockOrderForsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public StockOrderForsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }



        public FileDto ExportToFile(List<GetStockOrderForsForViewDto> stockorderfors)
        {
            return CreateExcelPackage(
                "Stockorderfors.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet("Stockorderfors");

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, 2, stockorderfors,
                        _ => _.stockorderfors.Name
                        );



                });
        }

    }
}
