﻿using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckDepositeReceive.Exporting;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.StockOrderFors;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using System.Linq;
using TheSolarProduct.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using TheSolarProduct.StockFroms.Dtos;
using TheSolarProduct.TransportationCosts;
using Abp;
using TheSolarProduct.StockFroms.Exporting;
using TheSolarProduct.StockOrderFors.Dtos;
using TheSolarProduct.StockFroms;
using TheSolarProduct.StockOrderFors.Exporting;






namespace TheSolarProduct.StockOrderFors
{
    public class StockOrderForsAppService : TheSolarProductAppServiceBase, IStockOrderForsAppService
    {

        private readonly IRepository<StockOrderFor> _stockorderforRepository;
        private readonly IStockOrderForsExcelExporter _stockorderforsExcelExporter;
     
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;


        public StockOrderForsAppService(IRepository<StockOrderFor> stockorderforRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider, IStockOrderForsExcelExporter stockorderforsExcelExporter)
        {
            _stockorderforsExcelExporter = stockorderforsExcelExporter;
            _stockorderforRepository = stockorderforRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;

        }


        public async Task<PagedResultDto<GetStockOrderForsForViewDto>> GetAll(GetAllStockOerderForsInput input)
        {
            var stockorderfor = _stockorderforRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFiltered = stockorderfor
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFiltered
                         select new GetStockOrderForsForViewDto()
                         {
                             stockorderfors = new StockOrderForsDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 IsActive = o.IsActive
                             }
                         };

            var totalCount = await stockorderfor.CountAsync();

            return new PagedResultDto<GetStockOrderForsForViewDto>(totalCount, await output.ToListAsync());
        }

        public async Task<GetStockOrderForsForEditOutput> GetStockOrderForsForEdit(EntityDto input)
        {
            var stockorderfor = await _stockorderforRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetStockOrderForsForEditOutput { stockorderfors = ObjectMapper.Map<CreateOrEditStockOrderForsDto>(stockorderfor) };

            return output;
        }


        public async Task CreateOrEdit(CreateOrEditStockOrderForsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }



        protected virtual async Task Create(CreateOrEditStockOrderForsDto input)
        {
            var stockorderfor = ObjectMapper.Map<StockOrderFor>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 69;
            dataVaultLog.ActionNote = "StockOrderFors Created: " + input.Name;
            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _stockorderforRepository.InsertAsync(stockorderfor);
        }


        protected virtual async Task Update(CreateOrEditStockOrderForsDto input)
        {
            var stockorderfor = await _stockorderforRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 69;
            dataVaultLog.ActionNote = "_Stockorderfor Updated : " + stockorderfor.Name;
            dataVaultLog.IsInventory = true;
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            var List = new List<DataVaultActivityLogHistory>();
            if (input.Name != stockorderfor.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = stockorderfor.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != stockorderfor.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = stockorderfor.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, stockorderfor);

            await _stockorderforRepository.UpdateAsync(stockorderfor);
        }

        public async Task Delete(EntityDto input)
        {
            var Name = _stockorderforRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 69;
            dataVaultLog.IsInventory = true;
            dataVaultLog.ActionNote = "Stockorderfor Deleted  : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _stockorderforRepository.DeleteAsync(input.Id);
        }


        public async Task<FileDto> GetStockOrderForsToExcel(GetAllStockOrderForsForExcelInput input)
        {

            var filteredStockFroms = _stockorderforRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredStockFroms
                         select new GetStockOrderForsForViewDto()
                         {
                             stockorderfors = new StockOrderForsDto
                             {
                                 Name = o.Name,
                                 Id = o.Id,
                                 IsActive = o.IsActive
                             }
                         });


            var ListDtos = await query.ToListAsync();

            return _stockorderforsExcelExporter.ExportToFile(ListDtos);
        }








    }



}
