﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Net.Mail;
using Abp.Notifications;
using Abp.Timing.Timezone;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net.Mail;
using System.Threading.Tasks;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.Notifications;
using TheSolarProduct.Sections;
using TheSolarProduct.WholeSaleActivitys.Dtos;
using TheSolarProduct.WholeSaleLeadActivityLogs;
using TheSolarProduct.WholeSaleLeads;
using Abp.Collections.Extensions;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System.Data;

namespace TheSolarProduct.WholeSaleActivitys
{
    [AbpAuthorize]
    public class WholeSaleActivityAppService : TheSolarProductAppServiceBase, IWholeSaleActivityAppService
    {
        private readonly IRepository<WholeSaleLead> _wholeSaleLeadRepository;
        private readonly IRepository<WholeSaleLeadActivityLog> _wholeSaleLeadactivityRepository;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IRepository<Section> _sectionRepository;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<User, long> _userRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly ITimeZoneConverter _timeZoneConverter;

        public WholeSaleActivityAppService(
            IRepository<WholeSaleLead> wholeSaleLeadRepository
            , IRepository<WholeSaleLeadActivityLog> wholeSaleLeadactivityRepository
            , IApplicationSettingsAppService applicationSettings
            , IRepository<Section> sectionRepository
            , IEmailSender emailSender
            , IRepository<User, long> userRepository
            , IAppNotifier appNotifier
            , ITimeZoneConverter timeZoneConverter
            )
        {
            _wholeSaleLeadRepository = wholeSaleLeadRepository;
            _wholeSaleLeadactivityRepository = wholeSaleLeadactivityRepository;
            _applicationSettings = applicationSettings;
            _sectionRepository = sectionRepository;
            _emailSender = emailSender;
            _userRepository = userRepository;
            _appNotifier = appNotifier;
            _timeZoneConverter = timeZoneConverter;
        }

        public async Task SendSms(WholeSaleSmsEmailDto input)
        {

            var WholeSaleLead = await _wholeSaleLeadRepository.FirstOrDefaultAsync((int)input.WholeSaleLeadId);
            var sectionName = _sectionRepository.GetAll().Where(e => e.SectionId == input.SectionId).Select(e => e.SectionName).FirstOrDefault();
           
            if (!string.IsNullOrEmpty(WholeSaleLead.Mobile))
            {
                WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
                leadactivity.ActionId = 6;
                leadactivity.ActionNote = "Sms Send From " + sectionName;
                leadactivity.WholeSaleLeadId = (int)input.WholeSaleLeadId;
                leadactivity.Subject = input.Body;
                leadactivity.ActivityDate = DateTime.UtcNow;
                leadactivity.SectionId = input.SectionId;
                if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                {
                    leadactivity.TemplateId = input.SMSTemplateId;
                }
                else
                {
                    leadactivity.TemplateId = 0;
                }
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                leadactivity.Body = input.Body;
                await _wholeSaleLeadactivityRepository.InsertAndGetIdAsync(leadactivity);

                var sendBulkSMSInput = new SendBulkSMSInput();

                List<SendSMSInput> msg = new List<SendSMSInput>();

                SendSMSInput sendSMSInput = new SendSMSInput();
                sendSMSInput.PhoneNumber = WholeSaleLead.Mobile;
                sendSMSInput.Text = input.Body;
                sendSMSInput.ActivityId = leadactivity.Id;
                sendSMSInput.SectionID = (int)input.SectionId;

                msg.Add(sendSMSInput);
                sendBulkSMSInput.Messages = msg;

                await _applicationSettings.SendWholeSaleBulkSMS(sendBulkSMSInput);
            }
        }

        public async Task SendEmail(WholeSaleSmsEmailDto input)
        {
            var lead = await _wholeSaleLeadRepository.FirstOrDefaultAsync((int)input.WholeSaleLeadId);
            var sectionName = _sectionRepository.GetAll().Where(e => e.SectionId == input.SectionId).Select(e => e.SectionName).FirstOrDefault();
            int? TemplateId;
            if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
            {
                TemplateId = input.EmailTemplateId;
            }
            else
            {
                TemplateId = 0;
            }

            if (!string.IsNullOrEmpty(lead.Email))
            {
                foreach(var Eid in input.ToEmails)
                {
                    if (input.cc != null && input.Bcc != null)
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { Eid }, //{ "viral.jain@meghtechnologies.com" }, //
                            CC = { input.cc },
                            Bcc = { input.Bcc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else if (input.cc != null && input.cc != "" && input.Bcc == null)
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { Eid }, //{ "viral.jain@meghtechnologies.com" }, //
                            CC = { input.cc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else if (input.cc == null && input.Bcc != null && input.Bcc != "")
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { Eid }, //{ "viral.jain@meghtechnologies.com" }, //
                            Bcc = { input.Bcc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else
                    {
                        var tomail = Eid;
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { tomail }, //{ "hiral.prajapati@meghtechnologies.com" }, //

                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                }
            }

            WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
            leadactivity.ActionId = 7;
            leadactivity.ActionNote = "Email Send From " + sectionName;
            leadactivity.WholeSaleLeadId = (int)input.WholeSaleLeadId;
            leadactivity.TemplateId = TemplateId;
            leadactivity.Subject = input.Subject;
            leadactivity.Body = input.Body;
            leadactivity.SectionId = input.SectionId;
            leadactivity.ActivityDate = DateTime.UtcNow;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _wholeSaleLeadactivityRepository.InsertAsync(leadactivity);
        }
    
        public async Task AddNotifyActivityLog(WholeSaleActivitylogInput input)
        {
            var WholeSaleLead = await _wholeSaleLeadRepository.FirstOrDefaultAsync((int)input.WholeSaleLeadId);
            var sectionName = _sectionRepository.GetAll().Where(e => e.SectionId == input.SectionId).Select(e => e.SectionName).FirstOrDefault();
            var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == WholeSaleLead.AssignUserId).FirstOrDefault();

            WholeSaleLeadActivityLog wholeSaleLeadActivityLog = new WholeSaleLeadActivityLog();
            wholeSaleLeadActivityLog.ActionId = 9;
            wholeSaleLeadActivityLog.ActionNote = "Notification";
            wholeSaleLeadActivityLog.SectionId = input.SectionId;
            wholeSaleLeadActivityLog.CreatorUserId = AbpSession.UserId;
            wholeSaleLeadActivityLog.ActivityNote = input.ActivityNote;
            wholeSaleLeadActivityLog.WholeSaleLeadId = (int)input.WholeSaleLeadId;
            await _wholeSaleLeadactivityRepository.InsertAndGetIdAsync(wholeSaleLeadActivityLog);

            string msg = "Notification " + input.ActivityNote + " For " + WholeSaleLead.CompanyName + " By " + CurrentUser.Name + " From " + sectionName;
            await _appNotifier.LeadComment(assignedToUser, msg, NotificationSeverity.Warn);
        }

        public async Task AddReminderActivityLog(WholeSaleActivitylogInput input)
        {
            input.ActivityDate = (_timeZoneConverter.Convert(input.ActivityDate, (int)AbpSession.TenantId));

            var wholeSaleLead = await _wholeSaleLeadRepository.FirstOrDefaultAsync((int)input.WholeSaleLeadId);
            var sectionName = _sectionRepository.GetAll().Where(e => e.SectionId == input.SectionId).Select(e => e.SectionName).FirstOrDefault();
            var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == wholeSaleLead.AssignUserId).FirstOrDefault();

            var CurrentTime = (_timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId));
            if (Convert.ToDateTime(input.ActivityDate) < CurrentTime)
            {
                throw new UserFriendlyException(L("PleaseSelectGreaterDatethanCurrentDate"));
            }

            WholeSaleLeadActivityLog wholeSaleLeadActivityLog = new WholeSaleLeadActivityLog();
            wholeSaleLeadActivityLog.ActionId = 8;
            wholeSaleLeadActivityLog.ActionNote = "Reminder Set";
            wholeSaleLeadActivityLog.SectionId = input.SectionId;
            wholeSaleLeadActivityLog.CreatorUserId = AbpSession.UserId;
            wholeSaleLeadActivityLog.ActivityNote = input.ActivityNote;
            wholeSaleLeadActivityLog.ActivityDate = input.ActivityDate;
            wholeSaleLeadActivityLog.WholeSaleLeadId = (int)input.WholeSaleLeadId;
            await _wholeSaleLeadactivityRepository.InsertAndGetIdAsync(wholeSaleLeadActivityLog);


            wholeSaleLead.ActivityDate = input.ActivityDate;
            wholeSaleLead.ActivityDescription = input.ActivityNote;

            await _wholeSaleLeadRepository.UpdateAsync(wholeSaleLead);


            string msg = string.Format("Reminder Reminder Set " + input.ActivityNote + " For " + wholeSaleLead.CompanyName + " By " + CurrentUser.Name + " From " + sectionName);
            await _appNotifier.LeadComment(assignedToUser, msg, NotificationSeverity.Warn);
        }

        public async Task AddToDoActivityLog(WholeSaleActivitylogInput input)
        {
            var WholeSaleLead = await _wholeSaleLeadRepository.FirstOrDefaultAsync((int)input.WholeSaleLeadId);
            var sectionName = _sectionRepository.GetAll().Where(e => e.SectionId == input.SectionId).Select(e => e.SectionName).FirstOrDefault();
            var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == WholeSaleLead.AssignUserId).FirstOrDefault();


            WholeSaleLeadActivityLog wholeSaleLeadActivityLog = new WholeSaleLeadActivityLog();
            wholeSaleLeadActivityLog.ActionId = 25;
            wholeSaleLeadActivityLog.ActionNote = "ToDo";
            wholeSaleLeadActivityLog.SectionId = input.SectionId;
            wholeSaleLeadActivityLog.CreatorUserId = AbpSession.UserId;
            wholeSaleLeadActivityLog.ActivityNote = input.ActivityNote;
            wholeSaleLeadActivityLog.WholeSaleLeadId = (int)input.WholeSaleLeadId;

            await _wholeSaleLeadactivityRepository.InsertAndGetIdAsync(wholeSaleLeadActivityLog);

            var assignedToUserForTodo = _userRepository.GetAll().Where(u => u.Id == input.UserId).FirstOrDefault();
            string msg = "ToDo " + input.ActivityNote + " For " + WholeSaleLead.CompanyName + " By " + CurrentUser.Name + " From " + sectionName;
            await _appNotifier.LeadComment(assignedToUserForTodo, msg, NotificationSeverity.Warn);
        }

        public async Task AddCommentActivityLog(WholeSaleActivitylogInput input)
        {
            var wholeSaleLead = await _wholeSaleLeadRepository.FirstOrDefaultAsync((int)input.WholeSaleLeadId);

           
            WholeSaleLeadActivityLog wholeSaleLeadActivityLog = new WholeSaleLeadActivityLog();
            wholeSaleLeadActivityLog.ActionId = 24;
            wholeSaleLeadActivityLog.ActionNote = "Comment";
            wholeSaleLeadActivityLog.SectionId = input.SectionId;
            wholeSaleLeadActivityLog.CreatorUserId = AbpSession.UserId;
            wholeSaleLeadActivityLog.ActivityNote = input.ActivityNote;
            wholeSaleLeadActivityLog.WholeSaleLeadId = (int)input.WholeSaleLeadId;

            await _wholeSaleLeadactivityRepository.InsertAndGetIdAsync(wholeSaleLeadActivityLog);

            wholeSaleLead.ActivityComment = input.ActivityNote;
            await _wholeSaleLeadRepository.UpdateAsync(wholeSaleLead);

        }

        public async Task<PagedResultDto<SmsReplyDto>> GetSmsReplyList(SmsReplyInputDto input)
        {
            int UserId = (int)AbpSession.UserId;

            var leadid = 0;

            //if (input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter))
            //{
            //    leadid = (int)_jobRepository.GetAll().Where(e => e.JobNumber == input.Filter).AsNoTracking().Select(e => e.LeadId).FirstOrDefault();
            //}

            //var ids = _leadactivityRepository.GetAll().Where(e => e.ActionId == 6 && e.CreatorUserId == UserId).Select(e => e.Id).ToList();
            var ids = _wholeSaleLeadactivityRepository.GetAll().Where(e => (e.ActionId == 6 ) && e.CreatorUserId == UserId).Select(e => e.Id);

            var filteredLeads = _wholeSaleLeadactivityRepository.GetAll()
                 .Include(e => e.WholeSaleLeadFk)
                 //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter))
                 .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFk.Mobile.Contains(input.Filter))
                 .WhereIf(input.FilterName == "ABNNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFk.ABNNumber.Contains(input.Filter))
                 .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFk.CompanyName.Contains(input.Filter))
                 .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFk.Email.Contains(input.Filter))
                 .WhereIf(leadid > 0, e => e.WholeSaleLeadId == leadid)
                 //.WhereIf(input.Markstatusid != 0 && input.Markstatusid == 1, e => ids.Contains((int)e.ReferanceId) && e.IsMark == true)
                 //.WhereIf(input.Markstatusid != 0 && input.Markstatusid == 2, e => ids.Contains((int)e.ReferanceId) && e.IsMark == false)
                 //.Where(e => ids.Contains((int)e.ReferanceId) && e.LeadFk.OrganizationId == input.OrganizationUnit);
                 .WhereIf(input.Markstatusid != 0 && input.Markstatusid == 1, e => ids.Any(i => i == (int)e.ReferanceId) && e.IsMark == true)
                 .WhereIf(input.Markstatusid != 0 && input.Markstatusid == 2, e => ids.Any(i => i == (int)e.ReferanceId) && e.IsMark == false)
                 .Where(e => ids.Any(i => i == (int)e.ReferanceId) && e.WholeSaleLeadFk.OrganizationId == input.OrganizationUnit);

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var result = from o in pagedAndFilteredLeads
                         join o1 in _wholeSaleLeadRepository.GetAll() on o.WholeSaleLeadId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _wholeSaleLeadactivityRepository.GetAll() on o.ReferanceId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()
                         select new SmsReplyDto
                         {
                             LeadCompanyName = s1.CompanyName,
                             ResponceActionNote = o.ActionNote,
                             ResponceActionId = o.ActionId,
                             ResponceActivityDate = o.ActivityDate,
                             ResponceActivityNote = o.ActivityNote,
                             ResponceCreationTime = o.CreationTime,
                             ResponceBody = o.Body,
                             ActionNote = s2.ActionNote,
                             ActionId = s2.ActionId,
                             ActivityDate = s2.ActivityDate,
                             ActivityNote = s2.ActivityNote,
                             CreationTime = s2.CreationTime,
                             Body = s2.Body,
                             IsMark = o.IsMark,
                             id = o.Id,
                             UserName = s3.Name,
                             LeadId = o.WholeSaleLeadId,
                             SectionName = _sectionRepository.GetAll().Where(e => e.SectionId == o.SectionId).Select(e => e.SectionName).FirstOrDefault()
                         };


            var totalCount = await filteredLeads.CountAsync();
            //var totalCount = filteredLeads.DistinctBy(e => e.LeadId).Count();

            //return new PagedResultDto<SmsReplyDto>(totalCount, result.DistinctBy(e => e.LeadId).ToList());
            return new PagedResultDto<SmsReplyDto>(totalCount, await result.ToListAsync());
        }

        public async Task MarkReadSmsInBulk(int Readorunreadtag, List<int> ids)
        {
            foreach (var item in ids)
            {
                var activityListIds = _wholeSaleLeadactivityRepository.GetAll().Where(e => e.WholeSaleLeadId == (int)item).Select(e => e.Id).ToList();

                foreach (var item1 in activityListIds)
                {
                    var leadactivity = await _wholeSaleLeadactivityRepository.FirstOrDefaultAsync((int)item1);
                    if (Readorunreadtag == 1)
                    {
                        leadactivity.IsMark = true;
                    }
                    else
                    {
                        leadactivity.IsMark = false;
                    }
                    await _wholeSaleLeadactivityRepository.UpdateAsync(leadactivity);
                }
            }
        }

        public async Task MarkReadSms(int id)
        {

            var leadactivity = await _wholeSaleLeadactivityRepository.FirstOrDefaultAsync((int)id);
            leadactivity.IsMark = true;
            await _wholeSaleLeadactivityRepository.UpdateAsync(leadactivity);

        }

        public async Task<List<SmsReplyDto>> GetOldSmsReplyList(int? Id, int? LeadId, int orgId)
        {
            int UserId = (int)AbpSession.UserId;
            //var ids = _leadactivityRepository.GetAll().Where(e => e.ActionId == 6 && e.CreatorUserId == UserId && e.LeadId == LeadId).Select(e => e.Id).ToList();
            var ids = _wholeSaleLeadactivityRepository.GetAll().Where(e => (e.ActionId == 6 ) && e.CreatorUserId == UserId && e.WholeSaleLeadId == LeadId).Select(e => e.Id).ToList();

            var filteredLeads = _wholeSaleLeadactivityRepository.GetAll()
                 .Include(e => e.WholeSaleLeadFk)
                 .Where(e => ids.Contains((int)e.ReferanceId) && e.WholeSaleLeadFk.OrganizationId == orgId && e.WholeSaleLeadId == LeadId);

            var pagedAndFilteredLeads = filteredLeads.OrderByDescending(e => e.Id);
            var result = new List<SmsReplyDto>();

            result = (from o in pagedAndFilteredLeads
                      join o1 in _wholeSaleLeadRepository.GetAll() on o.WholeSaleLeadId equals o1.Id into j1
                      from s1 in j1.DefaultIfEmpty()

                      join o2 in _wholeSaleLeadactivityRepository.GetAll() on o.ReferanceId equals o2.Id into j2
                      from s2 in j2.DefaultIfEmpty()

                      join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                      from s3 in j3.DefaultIfEmpty()

                      select new SmsReplyDto
                      {
                          LeadCompanyName = s1.CompanyName,
                          ResponceActionNote = o.ActionNote,
                          ResponceActionId = o.ActionId,
                          ResponceActivityDate = o.ActivityDate,
                          ResponceActivityNote = o.ActivityNote,
                          ResponceCreationTime = o.CreationTime,
                          ResponceBody = o.Body,
                          ActionNote = s2.ActionNote,
                          ActionId = s2.ActionId,
                          ActivityDate = s2.ActivityDate,
                          ActivityNote = s2.ActivityNote,
                          CreationTime = s2.CreationTime,
                          //Body = o.ActivityNote,
                          Body = s2.ActionNote.EndsWith("Signature Request Sent On SMS") ? s2.ActionNote : s2.Body,
                          IsMark = o.IsMark,
                          id = o.Id,
                          UserName = s3.Name,
                          LeadId = o.WholeSaleLeadId,

                          SectionName = _sectionRepository.GetAll().Where(e => e.SectionId == o.SectionId).Select(e => e.SectionName).FirstOrDefault()

                      }).ToList();
            return result;
        }


    }
}
