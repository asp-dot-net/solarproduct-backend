﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.PostCodes.Dtos;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct.ECommerces.BrandingPartners.Dto;
using TheSolarProduct.Common;
using TheSolarProduct.Storage;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.Wholesales.DataVault;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using System.Collections.Generic;
using TheSolarProduct.ECommerceSliders;
using TheSolarProduct.LeadStatuses;
using TheSolarProduct.WholeSaleLeads;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.ECommerces.BrandingPartners
{
    public class BrandingPartnerAppService : TheSolarProductAppServiceBase, IBrandingPartnerAppService
    {
        private readonly IRepository<BrandingPartner> _brandingPartnerRepository;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly TempFileCacheManager _tempFileCacheManager;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<ProductType> _productTypeRepository;

        public BrandingPartnerAppService(
           IRepository<BrandingPartner> brandingPartnerRepository,
           ICommonLookupAppService CommonDocumentSaveRepository,
           TempFileCacheManager tempFileCacheManager,
           IRepository<Tenant> tenantRepository,
           IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository,
           IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
           IRepository<ProductType> productTypeRepository
            )
        {
            _brandingPartnerRepository = brandingPartnerRepository;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _tenantRepository = tenantRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;
            _dbcontextprovider = dbcontextprovider;
            _productTypeRepository = productTypeRepository;
        }

        public async Task CreateOrEdit(BrandingPartnerDto input)
        {
            if (input.Id == null || input.Id == 0)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        protected virtual async Task Create(BrandingPartnerDto input)
        {
            var FileName = DateTime.Now.Ticks + "_" + input.BrandName.Replace(" ", "") + ".png";
            var InstallerByteArray = _tempFileCacheManager.GetFile(input.FileToken);

            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();

            var filepath = _CommonDocumentSaveRepository.SaveEcommerceDocument(InstallerByteArray, FileName, "BrandingPartnerLogo", AbpSession.TenantId);

            input.Logo = "\\Documents\\" + TenantName + "\\" + "WholeSale" + "\\BrandingPartnerLogo" + "\\" + FileName;


            var brandingPartner = ObjectMapper.Map<BrandingPartner>(input);

            await _brandingPartnerRepository.InsertAsync(brandingPartner);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 14;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "Branding Partner Created : " + input.BrandName;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);
        }

        protected virtual async Task Update(BrandingPartnerDto input)
        {
            if (!string.IsNullOrEmpty(input.FileToken))
            {
                var FileName = DateTime.Now.Ticks + "_" + input.BrandName.Replace(" ", "") + ".png";
                var InstallerByteArray = _tempFileCacheManager.GetFile(input.FileToken);

                var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();

                var filepath = _CommonDocumentSaveRepository.SaveEcommerceDocument(InstallerByteArray, FileName, "BrandingPartnerLogo", AbpSession.TenantId);

                input.Logo = "\\Documents\\" + TenantName + "\\" + "WholeSale" + "\\BrandingPartnerLogo" + "\\" + FileName;
            }
                

            var brandingPartner = await _brandingPartnerRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 14;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "Branding Partner Updated : " + brandingPartner.BrandName;

            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<WholeSaleDataVaultActivityLogHistory>();

            if (input.BrandName != brandingPartner.BrandName)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "BrandName";
                history.PrevValue = brandingPartner.BrandName;
                history.CurValue = input.BrandName;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.ProductCategoryId != brandingPartner.ProductCategoryId)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "ProductCategory";
                history.PrevValue = brandingPartner.ProductCategoryId > 0 ? _productTypeRepository.GetAll().Where(e => e.Id == brandingPartner.ProductCategoryId).FirstOrDefault().Name : "";
                history.CurValue = input.ProductCategoryId > 0 ? _productTypeRepository.GetAll().Where(e => e.Id == input.ProductCategoryId).FirstOrDefault().Name : "";
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Logo != brandingPartner.Logo)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Logo";
                history.PrevValue = brandingPartner.Logo;
                history.CurValue = input.Logo;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            

            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, brandingPartner);
            await _brandingPartnerRepository.UpdateAsync(brandingPartner);
        }

        public async Task Delete(EntityDto input)
        {
            var Name = _brandingPartnerRepository.Get(input.Id).BrandName;
            await _brandingPartnerRepository.DeleteAsync(input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 14;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "Branding Partner Deleted : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);
        }

        public async Task<PagedResultDto<GetAllBrandingPartnerDto>> GetAll(GetBrandingPartnerinputDto input)
        {
            var filteredBrandingPartners = _brandingPartnerRepository.GetAll()
                        .Include(e => e.ProductTypeFK)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.BrandName.Contains(input.Filter));

            var pagedAndFilteredBrandingPartners = filteredBrandingPartners
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var postCodes = from o in pagedAndFilteredBrandingPartners

                            select new GetAllBrandingPartnerDto()
                            {
                                BrandingPartner = new BrandingPartnerDto
                                {
                                    Id = o.Id,
                                    BrandName = o.BrandName,
                                    Logo = o.Logo,
                                    ProductCategory = o.ProductTypeFK.Name,
                                    ProductCategoryId = o.ProductCategoryId
                                }
                            };

            var totalCount = await filteredBrandingPartners.CountAsync();

            return new PagedResultDto<GetAllBrandingPartnerDto>(
                totalCount,
                await postCodes.ToListAsync()
            );
        }

        public async Task<BrandingPartnerDto> GetBusinessForEdit(EntityDto input)
        {
            var postCode = await _brandingPartnerRepository.GetAll()
                .Include(e => e.ProductTypeFK).Where(e => e.Id ==input.Id)
                .FirstOrDefaultAsync();

            var output =  ObjectMapper.Map<BrandingPartnerDto>(postCode) ;
            output.ProductCategory = postCode.ProductTypeFK != null ? postCode.ProductTypeFK.Name : "";
            return output;
        }
    }
}
