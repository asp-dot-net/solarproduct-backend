﻿using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Collections.Extensions;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct.ECommerces.STCRegisters.Dto;
using TheSolarProduct.ECommerces.Client.Dto;

namespace TheSolarProduct.ECommerces.STCRegisters
{
    public class StcRegisterAppService : TheSolarProductAppServiceBase, IStcRegisterAppService
    {
        private readonly IRepository<STCRegister> _stcRegisteRepository;

        public StcRegisterAppService(
            IRepository<STCRegister> stcRegisteRepository
            ) { 
            _stcRegisteRepository = stcRegisteRepository;
        }

        public async Task<PagedResultDto<GetAllStcRegisterDto>> GetAll(GetStcRegisterInputDto input)
        {
            var filteredSTCRegister = _stcRegisteRepository.GetAll()//.Include(e => e.fk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.ABNNumber.Contains(input.Filter))


                        .AsNoTracking();

            var pagedAndFilteredSTCRegister = filteredSTCRegister
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var STCRegisterses = from o in pagedAndFilteredSTCRegister
                              select new GetAllStcRegisterDto()
                              {
                                  RegisterDto = new STCRegisterDto()
                                  {
                                      Id = o.Id,
                                      TradingName = o.TradingName,
                                      ABNNumber = o.ABNNumber,
                                      UnitNo = o.UnitNo,
                                      UnitType = o.UnitType,
                                      StreetNo = o.StreetNo,
                                      StreetName = o.StreetName,
                                      StreetType = o.StreetType,
                                      State = o.StateFK.Name,
                                      PostCode = o.PostCode,
                                      Latitude = o.Latitude,
                                      Longitude = o.Longitude,
                                      Claim = o.Claim,
                                      PublicLiabilityCertificate = o.PublicLiabilityCertificate,
                                      ProductLiabilityCertificate = o.ProductLiabilityCertificate,
                                      PhotoId = o.PhotoId,
                                      ASICCertificate  = o.ASICCertificate,
                                      
                                  }
                              };

            var totalCount = await filteredSTCRegister.CountAsync();

            return new PagedResultDto<GetAllStcRegisterDto>(
                totalCount,
                await STCRegisterses.ToListAsync()
            );
        }
    }
}
