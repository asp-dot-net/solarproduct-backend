﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;

using TheSolarProduct.DocumentTypes.Dtos;
using TheSolarProduct.Quotations;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.ECommerces.ECommerceSliders.Dtos;
using TheSolarProduct.ECommerceSliders;
using TheSolarProduct.Common;
using TheSolarProduct.Storage;
using TheSolarProduct.Wholesales.DataVault;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.WholeSaleStatuses;

namespace TheSolarProduct.ECommerces.ECommerceSliders
{
    public class ECommerceSliderAppService : TheSolarProductAppServiceBase, IECommerceSliderAppService
    {
        private readonly IRepository<ECommerceSlider> _eCommerceSliderrepository;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly TempFileCacheManager _tempFileCacheManager;
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;


        public ECommerceSliderAppService(
            IRepository<ECommerceSlider> eCommerceSliderrepository
            , ICommonLookupAppService CommonDocumentSaveRepository
            , TempFileCacheManager tempFileCacheManager
            , IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository
            , IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            )
        {
            _eCommerceSliderrepository = eCommerceSliderrepository;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task CreateOrEdit(CreateOrEditECommerceSliderDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(CreateOrEditECommerceSliderDto input)
        {
            var FileName = DateTime.Now.Ticks + "_" + input.Name.Replace(" ", "") + ".png"; 

            //var IMage = input.Img.Split;
            //var buffer = new byte[2097152];
            //string converted = input.Img.Replace('-', '+');
            //converted = converted.Replace('_', '/');
            //byte[] ByteArray = Convert.FromBase64String(converted);
            var InstallerByteArray = _tempFileCacheManager.GetFile(input.Img);

            var filepath = _CommonDocumentSaveRepository.SaveEcommerceDocument(InstallerByteArray, FileName, "E-CommerceSlider", AbpSession.TenantId);

            //input.ImgPath = filepath.FinalFilePath;
                input.ImgPath = "\\Documents\\" + filepath.TenantName + "\\" + "WholeSale" + "\\" + "E-CommerceSlider"+ "\\" + FileName;


            var eCommerceSlider = ObjectMapper.Map<ECommerceSlider>(input);
            eCommerceSlider.Image = input.ImgPath;

            if (AbpSession.TenantId != null)
            {
                eCommerceSlider.TenantId = (int)AbpSession.TenantId;
            }
            await _eCommerceSliderrepository.InsertAsync(eCommerceSlider);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 12;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "ECommerce Slider Created : " + input.Name;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);
        }

        protected virtual async Task Update(CreateOrEditECommerceSliderDto input)
        {
            if(!string.IsNullOrEmpty(input.Img))
            {
                var FileName = DateTime.Now.Ticks + "_" + input.Name.Replace(" ","") + ".png";
                var InstallerByteArray = _tempFileCacheManager.GetFile(input.Img);

                var filepath = _CommonDocumentSaveRepository.SaveEcommerceDocument(InstallerByteArray, FileName, "E-CommerceSlider", AbpSession.TenantId);
                //input.ImgPath = filepath.FinalFilePath;
                input.ImgPath = "\\Documents\\" + filepath.TenantName + "\\" + "WholeSale" + "\\" + "E-CommerceSlider"+ "\\" + FileName;
            }
           
            var eCommerceSlider = await _eCommerceSliderrepository.FirstOrDefaultAsync((int)input.Id);
            eCommerceSlider.Image = input.ImgPath;

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 12;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "ECommerce Slider Updated : " + eCommerceSlider.Name;

            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<WholeSaleDataVaultActivityLogHistory>();

            if (input.Name != eCommerceSlider.Name)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = eCommerceSlider.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Header != eCommerceSlider.Header)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Header";
                history.PrevValue = eCommerceSlider.Header;
                history.CurValue = input.Header;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.button != eCommerceSlider.button)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "button";
                history.PrevValue = eCommerceSlider.button;
                history.CurValue = input.button;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.SubHeader != eCommerceSlider.SubHeader)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "SubHeader";
                history.PrevValue = eCommerceSlider.SubHeader;
                history.CurValue = input.SubHeader;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.ImageUrl != eCommerceSlider.ImageUrl)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "ImageUrl";
                history.PrevValue = eCommerceSlider.ImageUrl;
                history.CurValue = input.ImageUrl;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Img != eCommerceSlider.Image)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Image";
                history.PrevValue = eCommerceSlider.Image;
                history.CurValue = input.Img;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.active != eCommerceSlider.active)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "active";
                history.PrevValue = eCommerceSlider.active.ToString();
                history.CurValue = input.active.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, eCommerceSlider);


            await _eCommerceSliderrepository.UpdateAsync(eCommerceSlider);
        }

        public async Task Delete(EntityDto input)
        {
            var Name = _eCommerceSliderrepository.Get(input.Id).Name;
            await _eCommerceSliderrepository.DeleteAsync(input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 12;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "ECommerce Slider Deleted : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);
        }

        public async Task<PagedResultDto<GetECommerceSliderViewDto>> GetAll(GetAllECommerceSliderInput input)
        {
            var filteredECommerceSlider = _eCommerceSliderrepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(input.IsActive == "Yes", e => e.active == true)
                        .WhereIf(input.IsActive == "No", e => e.active != true);

            var pagedAndFilteredECommerceSlider = filteredECommerceSlider
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var eCommerceSlider = from o in pagedAndFilteredECommerceSlider
                              select new GetECommerceSliderViewDto()
                              {
                                 ECommerceSliders = new ECommerceSliderDto()
                                 {
                                     Id = o.Id,
                                     Name = o.Name,
                                     active = o.active,
                                     Img  = o.Image.Substring(o.Image.LastIndexOf("\\")),
                                     ImageUrl = o.ImageUrl,
                                     button = o.button,
                                     Header = o.Header,
                                     SubHeader = o.SubHeader
                                 }
                              };

            var totalCount = await filteredECommerceSlider.CountAsync();

            return new PagedResultDto<GetECommerceSliderViewDto>(
                totalCount,
                await eCommerceSlider.ToListAsync()
            );
        }

        public async Task<GetECommerceSliderForEditOutputDto> GetECommerceSliderForEdit(EntityDto input)
        {
            var eCommerceSlider = await _eCommerceSliderrepository.GetAsync(input.Id);

            var output = new GetECommerceSliderForEditOutputDto { CreateOrEditECommerceSlider = ObjectMapper.Map<CreateOrEditECommerceSliderDto>(eCommerceSlider) };

            output.CreateOrEditECommerceSlider.ImgPath = eCommerceSlider.Image;

            return output;
        }

        public async Task<GetECommerceSliderViewDto> GetECommerceSliderForView(int id)
        {
            var eCommerceSlider = await _eCommerceSliderrepository.GetAsync(id);

            var output = new GetECommerceSliderViewDto { ECommerceSliders = ObjectMapper.Map<ECommerceSliderDto>(eCommerceSlider) };
            output.ECommerceSliders.ImgPath = eCommerceSlider.Image;

            return output;
        }
    }
}
