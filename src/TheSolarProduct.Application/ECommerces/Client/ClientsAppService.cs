﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.Net.Mail;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Businesses;
using TheSolarProduct.EcommerceProducctSpecification.Dtos;
using TheSolarProduct.ECommerces.Businesses.Dtos;
using TheSolarProduct.ECommerces.Client.Dto;
using TheSolarProduct.ECommerceSliders;
using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.Locations.Dtos;
using TheSolarProduct.States;
using TheSolarProduct.WholeSaleLeads;
using TheSolarProduct.Wholesales;
using TheSolarProduct.Wholesales.Ecommerces;

namespace TheSolarProduct.ECommerces.Client
{
    public class ClientsAppService : TheSolarProductAppServiceBase, IClientsAppService
    {
        private readonly IRepository<ECommerceSlider> _eCommerceSliderRepository;
        private readonly IRepository<ProductType> _productTypesRepository;
        private readonly IRepository<ProductItem> _productItemRepository;
        private readonly IRepository<BrandingPartner> _brandingPartnerRepository;
        private readonly IRepository<EcommerceProductItem> _eCommerceProductItemRepository;
        private readonly IRepository<EcommerceProductItemDocument> _ecommerceProductItemDocumentRepository;
        private readonly IRepository<Wholesales.Ecommerces.Series> _seriesRepository;
        private readonly IRepository<SpecialOffer> _specialOfferRepository;
        private readonly IRepository<SpecialOfferProduct> _specialOfferProductRepository;
        private readonly IRepository<EcommerceUserRegisterRequest> _ecommerceUserRegisterRequestRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<EcommerceSolarPackage> _ecommerceSolarPackageRepository;
        private readonly IRepository<EcommerceSolarPackageItem> _ecommerceSolarPackageItemRepository;
        private readonly IRepository<WholeSaleLead> _wholeSaleLeadRepository;
        private readonly IRepository<State> _lookup_stateRepository;
        private readonly IRepository<Business> _businessRepository;
        private readonly IRepository<BuisnessDocumentType> _businessDocumentTypeRepository;
        private readonly IRepository<BuisnessStructure> _buisnessStructureRepository;
        private readonly IRepository<EcommerceProductItemPriceCategory> _ecommerceProductItemPriceCategoryRepository;
        private readonly IRepository<EcommerceSubscriber> _ecommerceSubscriberRepository;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<EcommerceContactUs> _ecommerceContactUsRepository;
        private readonly IRepository<EcomerceSpecialStatus> _ecommerceSpecialStatusRepository;
        private readonly IRepository<EcommerceProductItemImage> _ecommerceProductItemImageRepository;
        private readonly IRepository<STCRegister> _STCRegisterImageRepository;
        private readonly IRepository<EcommerceCartProductItem> _ecommerceCartProductItemRepository;
        private readonly IRepository<WholesaleInvoice> _wholesaleInvoiceRepository;
        private readonly IRepository<WholesaleInvoicetype> _wholesaleInvoicetypeRepository;
        private readonly IRepository<EcommerceProductItemSpecification> _ecommerceProductItemspecificationRepository;
        private readonly IRepository<Warehouselocation> _warehouselocationRepository;
        private readonly IRepository<StcDealPrice> _stcDealPriceRepository;

        public ClientsAppService(
            IRepository<ECommerceSlider> eCommerceSliderRepository,
            IRepository<ProductType> productTypesRepository,
            IRepository<ProductItem> productItemRepository,
            IRepository<BrandingPartner> brandingPartnerRepository,
            IRepository<EcommerceProductItem> eCommerceProductItemRepository,
            IRepository<Wholesales.Ecommerces.Series> seriesRepository,
            IRepository<EcommerceProductItemDocument> ecommerceProductItemDocumentRepository,
            IRepository<SpecialOffer> specialOfferRepository,
            IRepository<SpecialOfferProduct> specialOfferProductRepository,
            IRepository<EcommerceUserRegisterRequest> ecommerceUserRegisterRequestRepository,
            IRepository<User, long> userRepository,
            IRepository<EcommerceSolarPackage> ecommerceSolarPackageRepository,
            IRepository<EcommerceSolarPackageItem> ecommerceSolarPackageItemRepository
            , IRepository<WholeSaleLead> wholeSaleLeadRepository
            , IRepository<State> lookup_stateRepository
            , IRepository<Business> businessRepository
            , IRepository<BuisnessDocumentType> businessDocumentTypeRepository
            , IRepository<BuisnessStructure> buisnessStructureRepository
            , IRepository<EcommerceProductItemPriceCategory> ecommerceProductItemPriceCategoryRepository
            , IRepository<EcommerceSubscriber> ecommerceSubscriberRepository
            , IEmailSender emailSender
            , IRepository<EcommerceContactUs> ecommerceContactUsRepository
            , IRepository<EcomerceSpecialStatus> ecommerceSpecialStatusRepository
            , IRepository<EcommerceProductItemImage> ecommerceProductItemImageRepository
            , IRepository<EcommerceCartProductItem> ecommerceCartProductItemRepository
            , IRepository<WholesaleInvoice> wholesaleInvoiceRepository
            , IRepository<WholesaleInvoicetype> wholesaleInvoicetypeRepository
            , IRepository<EcommerceProductItemSpecification> ecommerceProductItemspecificationRepository
            , IRepository<Warehouselocation> warehouselocationRepository
            , IRepository<StcDealPrice> stcDealPriceRepository

        )
        {
            _eCommerceSliderRepository = eCommerceSliderRepository;
            _productTypesRepository = productTypesRepository;
            _productItemRepository = productItemRepository;
            _brandingPartnerRepository = brandingPartnerRepository;
            _eCommerceProductItemRepository = eCommerceProductItemRepository;
            _seriesRepository = seriesRepository;
            _ecommerceProductItemDocumentRepository = ecommerceProductItemDocumentRepository;
            _specialOfferRepository = specialOfferRepository;
            _specialOfferProductRepository = specialOfferProductRepository;
            _ecommerceUserRegisterRequestRepository = ecommerceUserRegisterRequestRepository;
            _userRepository = userRepository;
            _ecommerceSolarPackageRepository = ecommerceSolarPackageRepository;
            _ecommerceSolarPackageItemRepository = ecommerceSolarPackageItemRepository;
            _wholeSaleLeadRepository = wholeSaleLeadRepository;
            _lookup_stateRepository = lookup_stateRepository;
            _businessRepository = businessRepository;
            _businessDocumentTypeRepository = businessDocumentTypeRepository;
            _buisnessStructureRepository = buisnessStructureRepository;
            _ecommerceProductItemPriceCategoryRepository = ecommerceProductItemPriceCategoryRepository;
            _ecommerceSubscriberRepository = ecommerceSubscriberRepository;
            _emailSender = emailSender;
            _ecommerceContactUsRepository = ecommerceContactUsRepository;
            _ecommerceSpecialStatusRepository = ecommerceSpecialStatusRepository;
            _ecommerceProductItemImageRepository = ecommerceProductItemImageRepository;
            _ecommerceCartProductItemRepository = ecommerceCartProductItemRepository;
            _wholesaleInvoiceRepository = wholesaleInvoiceRepository;
            _wholesaleInvoicetypeRepository = wholesaleInvoicetypeRepository;
            _ecommerceProductItemspecificationRepository = ecommerceProductItemspecificationRepository;
            _warehouselocationRepository = warehouselocationRepository;
            _stcDealPriceRepository = stcDealPriceRepository;
        }

        public async Task<List<GetSliderForClientOutputDto>> GetSlider(int TenantId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var sliderDetails = _eCommerceSliderRepository.GetAll().Where(e => e.active == true).Select(e => new { e.Name, e.Image, e.Header, e.SubHeader, e.button, e.ImageUrl });

                var output = (from slider in sliderDetails
                              select new GetSliderForClientOutputDto()
                              {
                                  Name = slider.Name,
                                  Image = (slider.Image != null ? slider.Image.Replace("\\", "/") : ""),
                                  Header = slider.Header,
                                  SubHeader = slider.SubHeader,
                                  Button = slider.button,
                                  ImageUrl = slider.ImageUrl
                              });

                return await output.ToListAsync();
            }
        }

        public async Task<List<GetProductTypeForDashboardClientOutputDto>> GetProductTypeForDashboard(int TenantId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var productTypes = _productTypesRepository.GetAll().Select(e => new { e.Id, e.Name, e.Image });

                var output = (from o in productTypes
                              select new GetProductTypeForDashboardClientOutputDto()
                              {
                                  Id = o.Id,
                                  Name = o.Name,
                                  ProductItemCount = _eCommerceProductItemRepository.GetAll().Where(e => e.IsActive == true && e.ProductItemFk.ECommerce == true && e.ProductCategoryId == o.Id).Count(),
                                  ImageUrl = (o.Image != null ? o.Image.Replace("\\", "/") : "")
                              });

                return await output.ToListAsync();
            }
        }

        public async Task<List<GetBrandingPartnersForDashboardClientOutputDto>> GetBrandingPartnersForDashboard(int TenantId, int? productCategoryId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var productTypes = _brandingPartnerRepository.GetAll().WhereIf(productCategoryId > 0, e => e.ProductCategoryId == productCategoryId).Select(e => new { e.Id, e.BrandName, e.Logo });

                var output = (from o in productTypes
                              select new GetBrandingPartnersForDashboardClientOutputDto()
                              {
                                  Id = o.Id,
                                  Logo = (o.Logo != null ? o.Logo.Replace("\\", "/") : ""),
                                  Name = o.BrandName
                              });

                return await output.ToListAsync();
            }
        }

        public async Task<List<GetFeaturedProductForClientOutputDto>> GetFeaturedProduct(int TenantId, int? PriceCategoryId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                //productCategoryId = productCategoryId > 0 ? productCategoryId : 0;
                var products = _eCommerceProductItemRepository.GetAll().Include(e => e.ProductItemFk).Include(e => e.SpecialStatusFk).Include(e => e.BrandPartnerFK)
                    .Where(e => e.IsActive == true && e.IsFeatured == true)
                    .Select(e => new { ecomId = e.Id, e.ProductItemFk.Id, e.ProductItemFk.Name, e.ProductItemFk.ImagePath, e.ProductItemFk.ImageName, e.Rating, e.SpecialStatusFk.Status });

                var output = (from o in products

                              let price = _ecommerceProductItemPriceCategoryRepository.GetAll().Where(e => e.EcommerceProductItemId == o.ecomId && e.ECommercePriceCategoryId == PriceCategoryId).Select(e => e.Price).FirstOrDefault()

                              select new GetFeaturedProductForClientOutputDto()
                              {
                                  Id = o.Id,
                                  ProductName = o.Name,
                                  Price = price != null ? (decimal)price : 0.0m,
                                  Rating = o.Rating != null ? (decimal)o.Rating : 0.0m,
                                  ImageUrl = (o.ImagePath != null ? o.ImagePath.Replace("\\", "/") : "") + o.ImageName,
                                  SpecialStatus = o.Status
                              });

                return await output.ToListAsync();
            }
        }

        public async Task<List<CommonLookupDto>> GetAllSeries(int TenantId, int categoryId, List<int?> Brand)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var result = _eCommerceProductItemRepository.GetAll()
                    .Where(e => e.SeriesFK.ProductCategoryId == categoryId)
                    .WhereIf(Brand.Count > 0, e => Brand.Contains(e.BrandPartnerId))
                    .Select(e => new { e.SeriesFK.SeriesName, e.SeriesId }).Distinct();

                var series = (from item in result
                              select new CommonLookupDto
                              {
                                  Id = (int)item.SeriesId,
                                  DisplayName = item.SeriesName,
                              });
                return await series.OrderBy(e => e.DisplayName).ToListAsync();
            }

        }

        public async Task<List<decimal?>> GetAllSizes(int TenantId, int categoryId, List<int?> Brand)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var result = await _eCommerceProductItemRepository.GetAll()
                    .Where(e => e.IsActive == true && e.ProductItemFk.ProductTypeId == categoryId)
                    .WhereIf(Brand.Count > 0, e => Brand.Contains(e.BrandPartnerId))
                    .Select(e => e.ProductItemFk.Size).Distinct().OrderBy(e => e.Value).ToListAsync();
                return result;
            }
        }

        public async Task<GetProductDetailDto> GetProductDetail(int TenantId, int ProductItemId, int? PriceCategoryId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var products = await _eCommerceProductItemRepository.GetAll().Include(e => e.ProductItemFk).Include(e => e.SpecialStatusFk).Include(e => e.BrandPartnerFK)
                    .Where(e => e.ProductItemFk.Id == ProductItemId)
                    .Select(e => new { e.ProductItemFk.Id, ecomid = e.Id, e.ProductItemFk.Name, e.ProductItemFk.ImagePath, e.ProductItemFk.ImageName, e.Rating, e.ProductShortDescription, e.ProductDescription, e.ProductItemFk.Model, e.ProductItemFk.ProductTypeId, e.ProductItemFk.FilePath, e.ProductItemFk.FileName, e.Specification, e.SpecialStatusFk.Status }).FirstOrDefaultAsync();

                var productDoc = await _ecommerceProductItemDocumentRepository.GetAll().Include(e => e.DocumentTypeFK).Where(e => e.EcommerceProductItemId == products.ecomid).ToListAsync();

                var productPrice = _ecommerceProductItemPriceCategoryRepository.GetAll().Where(e => e.EcommerceProductItemId == products.ecomid && e.ECommercePriceCategoryId == PriceCategoryId).Select(e => e.Price).FirstOrDefault();

                var output = new GetProductDetailDto();

                output.Id = products.Id;
                output.ProductName = products.Name;
                output.Price = productPrice != null ? (decimal)productPrice : 0.0m;
                output.Rating = products.Rating != null ? (decimal)products.Rating : 0.0m;
                output.ImageUrl = (products.ImagePath != null ? products.ImagePath.Replace("\\", "/") : "") + products.ImageName;
                output.ProductDescription = products.ProductDescription;
                output.ProductShortDescription = products.ProductShortDescription;
                output.Model = products.Model;
                output.categoryId = products.ProductTypeId;
                output.FileUrl = (products.FilePath != null ? products.FilePath.Replace("\\", "/") : "") + products.FileName;
                output.Specification = products.Specification;
                output.SpecialStatus = products.Status;
                output.EcomProdId = products.ecomid;

                output.Documnets = (from item in productDoc
                                    select new GetProductDocumentDto()
                                    {
                                        Id = item.Id,
                                        DocTypeId = item.DocumentTypeId,
                                        DocType = item.DocumentTypeFK.DocumentType,
                                        DocName = item.DocumentName,
                                        DocUrl = item.Documemt

                                    }).ToList();

                var PRoImages = await _ecommerceProductItemImageRepository.GetAll().Where(e => e.EcommerceProductItemId == products.ecomid).Select(e => e.Image).ToListAsync();
                output.ImageUrlList = new List<string>();
                if (PRoImages.Any())
                {
                    foreach (var i in PRoImages)
                    {
                        output.ImageUrlList.Add(i);
                    }
                }
                else
                {
                    output.ImageUrlList.Add(output.ImageUrl);
                }

                var resultProductItemSpecification = _ecommerceProductItemspecificationRepository.GetAll().Include(e => e.EcommerceSpecificationfk).Where(e => e.EcommerceProductItemFK.ProductItemID == ProductItemId);
                output.EcommerceProductItemSpecification = (from o in resultProductItemSpecification
                                                         select new EcommerceProductItemSpecificationDto()
                                                         {
                                                             Id = o.Id,
                                                             EcommerceProductItemId = o.EcommerceProductItemId,
                                                             SpecificationId = o.SpecificationId,
                                                             Value = o.Value,
                                                             Name = o.EcommerceSpecificationfk.Name

                                                         }).ToList();

                return output;
            }
        }

        public async Task<List<GetSpecialOfferProductForDashboardDto>> GetSpecialOfferProductForDashboard(int TenantId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var today = DateTime.Now;
                var products = await _specialOfferProductRepository.GetAll()
                    .Include(e => e.SpecialOfferFK).Include(e => e.ProductItemFK).Include(e => e.ProductItemFK.ProductItemFk).Include(e => e.ProductItemFK.SpecialStatusFk).AsNoTracking()
                    .Where(e => e.SpecialOfferFK.DealStarts.Value.Date <= today && e.SpecialOfferFK.DealEnds >= today && e.SpecialOfferFK.IsOfferActive == true)
                    .Select(e => new { e.ProductItemFK.ProductItemFk.Id, e.ProductItemFK.ProductItemFk.Name, e.NewPrice, e.OldPrice, e.ProductItemFK.Rating, e.ProductItemFK.ProductItemFk.ImagePath, e.ProductItemFK.ProductItemFk.ImageName, e.ProductItemFK.SpecialStatusFk.Status, e.SpecialOfferFK.DealEnds }).ToListAsync();

                var output = (from o in products
                              select new GetSpecialOfferProductForDashboardDto()
                              {
                                  Id = o.Id,
                                  ProductName = o.Name,
                                  Rating = o.Rating != null ? (decimal)o.Rating : 0.0m,
                                  ImageUrl = (o.ImagePath != null ? o.ImagePath.Replace("\\", "/") : "") + o.ImageName,
                                  SpecialStatus = o.Status,
                                  NewPrice = o.NewPrice,
                                  OldPrice = o.OldPrice,
                                  DealEnd = o.DealEnds
                              }).ToList();
                return output;
            }
        }

        public async Task<string> CreateUserRequest(EcommerceUserRegisterRequestCreateDto input)
        {
            using (UnitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                //var output = new UserInfoClientOutputDto();
                var request = ObjectMapper.Map<EcommerceUserRegisterRequest>(input);

                await _ecommerceUserRegisterRequestRepository.InsertAsync(request);
                var output = "Success";



                return output;
            }
        }

        [HttpGet]
        public async Task<List<int?>> CheckUserExist(int TenantId, string userName, string Email)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var result = new List<int?>();

                var users = await _wholeSaleLeadRepository.GetAll().Where(e => e.Email == Email).AsNoTracking().Select(e => e.Id).ToListAsync();
                foreach (var u in users)
                {
                    result.Add((int?)u);
                }

                var request = await _ecommerceUserRegisterRequestRepository.GetAll().Where(e => e.Email == Email).AsNoTracking().Select(e => e.Id).ToListAsync();
                foreach (var u in request)
                {
                    result.Add((int?)u);
                }

                return result;
            }
        }

        [HttpGet]
        public async Task<List<GetFeaturedProductForClientOutputDto>> GetAllProduct(int TenantId, int? productCategoryId, List<int?> Brand, List<int?> Series, List<decimal?> Size, string filter, int? PriceCategoryId, List<int?> SpecialStatus, List<int?> Types)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                productCategoryId = productCategoryId > 0 ? productCategoryId : 0;
                var products = _eCommerceProductItemRepository.GetAll().Include(e => e.ProductItemFk).Include(e => e.SpecialStatusFk).Include(e => e.BrandPartnerFK)
                    .Where(e => e.IsActive == true && e.ProductItemFk.ECommerce == true)
                    .WhereIf(productCategoryId > 0, e => e.ProductCategoryId == productCategoryId)
                    .WhereIf(Brand.Count > 0, e => Brand.Contains(e.BrandPartnerId))
                    .WhereIf(Series.Count > 0, e => Series.Contains(e.SeriesId))
                    .WhereIf(Size.Count > 0, e => Size.Contains(e.ProductItemFk.Size))
                    .WhereIf(SpecialStatus.Count > 0, e => SpecialStatus.Contains(e.SpecialStatusId))
                    .WhereIf(!string.IsNullOrEmpty(filter), e => e.ProductItemFk.Name.Contains(filter))
                    .WhereIf(Types.Count > 0, e => Types.Contains(e.EcommerceProductTypeId))
                    .Select(e => new { ecomId = e.Id, e.ProductItemFk.Id, e.ProductItemFk.Name, e.ProductItemFk.ImagePath, e.ProductItemFk.ImageName, e.Rating, e.SpecialStatusFk.Status });

                var output = (from o in products

                              let price = _ecommerceProductItemPriceCategoryRepository.GetAll().Where(e => e.EcommerceProductItemId == o.ecomId && e.ECommercePriceCategoryId == PriceCategoryId).Select(e => e.Price).FirstOrDefault()

                              select new GetFeaturedProductForClientOutputDto()
                              {
                                  Id = o.Id,
                                  ProductName = o.Name,
                                  Price = price != null ? (decimal)price : 0.0m,
                                  Rating = o.Rating != null ? (decimal)o.Rating : 0.0m,
                                  ImageUrl = (o.ImagePath != null ? o.ImagePath.Replace("\\", "/") : "") + o.ImageName,
                                  SpecialStatus = o.Status,
                                  EcomProdId = o.ecomId
                              });

                return await output.OrderBy(e => e.ProductName).ToListAsync();
            }
        }

        public async Task<List<GetSolarPackageForDashboardDto>> GetSolarPackageForDashboard(int TenantId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var today = DateTime.Now;
                var products = await _ecommerceSolarPackageRepository.GetAll()
                    //.Include(e => e.EcommerceSolarPackagFK).Include(e => e.ProductItemFk).AsNoTracking()
                    .Where(e => e.IsActive == true)
                    .Select(e => new { e.Id, e.Name, e.Banner, e.Price }).ToListAsync();

                var output = (from o in products

                              select new GetSolarPackageForDashboardDto()
                              {
                                  Id = o.Id,
                                  Name = o.Name,
                                  Banner = o.Banner,
                                  Price = o.Price
                              }).ToList();
                return output;
            }
        }

        public async Task<List<GetSolarPackageProductDetailsDto>> GetSolarPackageProductDetails(int TenantId, int PackageId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var today = DateTime.Now;
                var products = await _ecommerceSolarPackageItemRepository.GetAll()
                    .Include(e => e.EcommerceSolarPackagFK).Include(e => e.ProductItemFk).AsNoTracking()
                    .Where(e => e.EcommerceSolarPackagFK.IsActive == true && e.EcommerceSolarPackageId == PackageId)
                    .Select(e => new { e.Id, e.ProductItemId, e.ProductItemFk.Name, e.ProductItemFk.Model, category = e.ProductItemFk.ProductTypeFk.Name, e.Quantity, e.ProductItemFk.ImagePath, e.ProductItemFk.ImageName }).ToListAsync();

                var output = (from o in products

                              select new GetSolarPackageProductDetailsDto()
                              {
                                  Id = o.Id,
                                  ProductName = o.Name,
                                  ProductType = o.category,
                                  Model = o.Model,
                                  Quantity = o.Quantity,
                                  ProductItemId = o.ProductItemId,
                                  Image = (o.ImagePath != null ? o.ImagePath.Replace("\\", "/") : "") + o.ImageName

                              }).ToList();
                return output;
            }
        }

        [HttpGet]
        public List<int> GetStateId(int TenantId, string StateName)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                return _lookup_stateRepository.GetAll().Where(P => P.Name == StateName).Select(P => P.Id).ToList();
            }
        }

        [HttpGet]
        public async Task<List<LeadStateLookupTableDto>> GetAllStateForTableDropdown(int TenantId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var output = await _lookup_stateRepository.GetAll()
                .Select(state => new LeadStateLookupTableDto
                {
                    Id = state.Id,
                    DisplayName = state == null || state.Name == null ? "" : state.Name.ToString().ToUpper()
                }).OrderBy(e => e.Id).ToListAsync();
                return output;
            }
        }

        public async Task<int> AddToCreditApplication(BusinessDto input)
        {
            using (UnitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                int id = (int)(input.Id != null ? input.Id : 0);
                if (id == 0)
                {
                    var request = ObjectMapper.Map<Business>(input);

                    id = await _businessRepository.InsertAndGetIdAsync(request);
                }
                else
                {
                    var request = _businessRepository.GetAll().Where(e => e.Id == input.Id).FirstOrDefault();

                    ObjectMapper.Map(input, request);
                    await _businessRepository.UpdateAsync(request);
                }
                return id;
            }
        }

        public async Task<UserInfoClientOutputDto> Authentication(AauthenticationModelDto input)
        {
            using (UnitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var output = new UserInfoClientOutputDto();
                var usersIsExists = await _wholeSaleLeadRepository.GetAll().Include(e => e.EcommerceUserRegisterRequestFk).AnyAsync(e => e.EcommerceUserRegisterRequestFk.Email == input.Email);

                if (!usersIsExists)
                {
                    output.SuccessMsg = "UserNotExists";
                    return output;
                }
                else
                {
                    var isUserApproved = await _ecommerceUserRegisterRequestRepository.GetAll().AnyAsync(e => e.Email == input.Email && e.IsApproved == true);

                    if (!isUserApproved)
                    {
                        output.SuccessMsg = "UserIsNotApproved";
                        return output;
                    }
                    else
                    {
                        var isLoggedIn = await _wholeSaleLeadRepository.GetAll().Include(e => e.EcommerceUserRegisterRequestFk).Where(e => e.EcommerceUserRegisterRequestFk.Email == input.Email && e.EcommerceUserRegisterRequestFk.Password == input.Password && e.EcommerceUserRegisterRequestFk.IsApproved == true).FirstOrDefaultAsync();

                        if (isLoggedIn != null)
                        {
                            output.Id = isLoggedIn.Id;
                            output.FirstName = isLoggedIn.FirstName;
                            output.LastName = isLoggedIn.LastName;
                            output.Email = isLoggedIn.Email;
                            output.PriceCategoryId = isLoggedIn.PriceCategoryId;
                            output.CartItemCount = await _ecommerceCartProductItemRepository.GetAll().Where(e => e.UserId == isLoggedIn.Id && e.Purcahse != true).CountAsync();

                            output.SuccessMsg = "Success";
                        }
                        else
                        {
                            output.SuccessMsg = "InvalidUserNameOrPassword";
                            return output;
                        }

                        return output;
                    }
                }
            }
        }

        public async Task<BusinessDto> GetCreditApplicationForEdit(int TenantId, int LoginId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                // var whalesaleId = _wholeSaleLeadRepository.GetAll().Where(e=> e.WholesaleClientId == LoginId).Select(e => e.Id).FirstOrDefault();

                var creditApplication = await _businessRepository.GetAll().Where(e => e.WholeSaleLeadId == LoginId).FirstOrDefaultAsync();

                var output = new BusinessDto();
                if (creditApplication != null)
                {
                    output = ObjectMapper.Map<BusinessDto>(creditApplication);
                }

                return output;
            }
        }

        public async Task<List<LeadStateLookupTableDto>> GetAllBuisnessStructureDropdown(int TenantId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var output = await _buisnessStructureRepository.GetAll()
                .Select(struc => new LeadStateLookupTableDto
                {
                    Id = struc.Id,
                    DisplayName = struc.Structure
                }).OrderBy(e => e.Id).ToListAsync();
                return output;
            }
        }

        public async Task<List<LeadStateLookupTableDto>> GetAllBuisnessDocumentTypeDropdown(int TenantId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var output = await _businessDocumentTypeRepository.GetAll()
                .Select(type => new LeadStateLookupTableDto
                {
                    Id = type.Id,
                    DisplayName = type.DocType
                }).OrderBy(e => e.Id).ToListAsync();
                return output;
            }
        }

        public async Task<string> AddSubscriber(EcommerceSubscriberDto input)
        {
            using (UnitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                //var output = new UserInfoClientOutputDto();
                var request = ObjectMapper.Map<EcommerceSubscriber>(input);

                await _ecommerceSubscriberRepository.InsertAsync(request);
                var output = "Success";

                return output;
            }
        }

        public async Task<string> ContactUs(CuntactUsDto input)
        {
            using (UnitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var output = "Success";
                var FinalBody = "<!doctype html><html lang='en'><body><p>This Message From " + input.Name + "</p><p> Phone : " + input.Phone + "</p><p>" + input.Message + "</p></body></html>";
                var email = "info@arisesolar.com.au";
                var tomail = "suresh.quickforms@gmail.com";

                var request = ObjectMapper.Map<EcommerceContactUs>(input);

                await _ecommerceContactUsRepository.InsertAsync(request);
                //MailMessage mail = new MailMessage
                //{
                //    From = new MailAddress(email),
                //    To = { tomail }, //{ "hiral.prajapati@meghtechnologies.com" }, //

                //    Subject = input.Subject,
                //    Body = input.Body,
                //    IsBodyHtml = true
                //};
                //await _emailSender.SendAsync(mail);

                return output;
            }
        }

        public async Task<CreditApplicationInfo> GetUserCreditApplicationInfo(int tenantId, int loginId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(tenantId))
            {
                var output = new CreditApplicationInfo();
                var creditApp = await _businessRepository.GetAll().Where(e => e.WholeSaleLeadId == loginId).Select(e => new { e.IsConfirm, e.IsApproved }).FirstOrDefaultAsync();

                output.IsConfirm = (creditApp != null ? creditApp.IsConfirm : false);
                output.IsApproved = (creditApp != null ? creditApp.IsApproved : false);
                return output;
            }
        }

        public async Task<List<GetBrandingPartnersForDashboardClientOutputDto>> GetAllCategories(int TenantId, int categoryId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var categoryIds = await _eCommerceProductItemRepository.GetAll().Where(e => e.IsActive == true && e.ProductItemFk.ProductTypeId == categoryId).Select(e => e.BrandPartnerId).Distinct().ToListAsync();

                var productTypes = _brandingPartnerRepository.GetAll().Where(e => categoryIds.Contains(e.Id)).Select(e => new { e.Id, e.BrandName, e.Logo });

                var output = (from o in productTypes
                              select new GetBrandingPartnersForDashboardClientOutputDto()
                              {
                                  Id = o.Id,
                                  Logo = (o.Logo != null ? o.Logo.Replace("\\", "/") : ""),
                                  Name = o.BrandName
                              });

                return output.OrderBy(e => e.Name).ToList();
            }

        }

        public async Task<CreditSummaryForAccountDashboardDto> GetCreditSummaryForAccountDashboard(int TenantId, int LoginId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                // var whalesaleId = _wholeSaleLeadRepository.GetAll().Where(e=> e.WholesaleClientId == LoginId).Select(e => e.Id).FirstOrDefault();

                var creditApplication = await _businessRepository.GetAll().Where(e => e.WholeSaleLeadId == LoginId).FirstOrDefaultAsync();
                var WholeSaleLead = await _wholeSaleLeadRepository.GetAll().Where(e => e.Id == LoginId).FirstOrDefaultAsync();

                var output = new CreditSummaryForAccountDashboardDto();

                if (creditApplication != null)
                {
                    output.Id = creditApplication.Id;
                    output.CreditLimit = WholeSaleLead.CreditAmount;
                    output.PaymentTerrm = WholeSaleLead.CreditDays > 0 ? WholeSaleLead.CreditDays : 0;
                    output.CreditUsed = 0;
                    output.AvailableCredit = output.CreditLimit - output.CreditUsed;
                    output.IsApproved = creditApplication.IsApproved;
                }

                return output;
            }
        }

        public async Task<List<CommonLookupDto>> GetAllSpecialStatus(int TenantId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var output = await _ecommerceSpecialStatusRepository.GetAll()
                .Select(status => new CommonLookupDto
                {
                    Id = status.Id,
                    DisplayName = status.Status
                }).OrderBy(e => e.DisplayName).ToListAsync();
                return output;
            }
        }

        public async Task<string> AddSTCRegister(STCRegisterDto input)
        {
            using (UnitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                //var output = new UserInfoClientOutputDto();
                var request = ObjectMapper.Map<STCRegister>(input);

                await _STCRegisterImageRepository.InsertAsync(request);
                var output = "Success";



                return output;
            }
        }

        public async Task<List<CommonLookupDto>> GetAllTypes(int TenantId, int categoryId, List<int?> Brand)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var result = _eCommerceProductItemRepository.GetAll()
                    .Where(e => e.ProductCategoryId == categoryId)
                    .WhereIf(Brand.Count > 0, e => Brand.Contains(e.BrandPartnerId))
                    .Select(e => new { e.EcommerceProductTypeFK.Type, e.EcommerceProductTypeId }).Distinct();

                var series = (from item in result
                              select new CommonLookupDto
                              {
                                  Id = (int)item.EcommerceProductTypeId,
                                  DisplayName = item.Type,
                              });
                return await series.OrderBy(e => e.DisplayName).Where(e => e.Id > 0).ToListAsync();
            }

        }

        public async Task<int> AddedToEcommerceCart(EcommerceCartProductItemDto input)
        {
            using (UnitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                //var output = new UserInfoClientOutputDto();
                var request = ObjectMapper.Map<EcommerceCartProductItem>(input);

                //await _ecommerceCartProductItemRepository.InsertAsync(request);
                var output = await _ecommerceCartProductItemRepository.InsertAndGetIdAsync(request);

                return output;
            }
        }

        public async Task<List<GetProductDetailDto>> GetAllEcommerceCartProductItem(int TenantId, int UserId, int PriceCategoryId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                //var output = new UserInfoClientOutputDto();
                var EcommerceCart = _ecommerceCartProductItemRepository.GetAll()
                    .Include(e => e.EcommerceProductItemFK)
                    .Include(e => e.EcommerceProductItemFK.ProductItemFk)
                    .Where(e => e.UserId == UserId && e.Purcahse != true);


                var result = from o in EcommerceCart
                             let price = _ecommerceProductItemPriceCategoryRepository.GetAll().Where(e => e.EcommerceProductItemId == o.EcommerceProductItemId && e.ECommercePriceCategoryId == PriceCategoryId).Select(e => e.Price).FirstOrDefault()
                             select new GetProductDetailDto()
                             {
                                 CartId = o.Id,
                                 Price = PriceCategoryId > 0 && price > 0 ? price : 0,
                                 ProductName = o.EcommerceProductItemFK.ProductItemFk.Name,
                                 ImageUrl = (o.EcommerceProductItemFK.ProductItemFk.ImagePath != null ? o.EcommerceProductItemFK.ProductItemFk.ImagePath.Replace("\\", "/") : "") + o.EcommerceProductItemFK.ProductItemFk.ImageName,
                                 QTY = o.QTY,
                                 EcomProdId = (int)o.EcommerceProductItemId,
                                 Id = (int)o.EcommerceProductItemFK.ProductItemID
                             };
                return await result.ToListAsync();
            }
        }

        public async Task<string> DeleteToEcommerceCart(int TenantId, int Id)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                //var output = new UserInfoClientOutputDto();

                await _ecommerceCartProductItemRepository.DeleteAsync(Id);
                var output = "Success";

                return output;
            }
        }

        public async Task<string> AddWholesaleInvoice(WholesaleInvoiceDto input)
        {
            using (UnitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                //var output = new UserInfoClientOutputDto();
                var request = ObjectMapper.Map<WholesaleInvoice>(input);
                request.DeliveryDate = Convert.ToDateTime(input.ETPDeliveryDate);
                var InvoiceType = await _wholesaleInvoicetypeRepository.FirstOrDefaultAsync(e => e.Name == input.InvoiceType);
                request.InvoiceTypeId = InvoiceType != null ? InvoiceType.Id : 0;
                var invId = await _wholesaleInvoiceRepository.InsertAndGetIdAsync(request);

                //var wholesalelead =await _wholeSaleLeadRepository.GetAsync((int)input.WholesaleLeadId);
                //var tomail = wholesalelead.Email;
                var tomail = "poonam.quickforms@gmail.com";
                MailMessage mail = new MailMessage
                {
                    From = new MailAddress("info@achieversenergy.com.au"),
                    To = { tomail }, //{ "hiral.prajapati@meghtechnologies.com" }, //

                    Subject = "Order Confirmd",
                    Body = "Your Order is Confirmed",
                    IsBodyHtml = true
                };
                this._emailSender.SendAsync(mail);

                //foreach (var cart in input.ProductDetailDto)
                //{

                //    var EcomCart = await _ecommerceCartProductItemRepository.FirstOrDefaultAsync((int)cart.CartId);

                //    EcomCart.WholesaleInvoiceId = invId;
                //    EcomCart.Purcahse = true;
                //    EcomCart.QTY = cart.QTY;

                //    await _ecommerceCartProductItemRepository.UpdateAsync(EcomCart);
                //}

                var output = "Success";

                return output;
            }
        }

        public async Task<List<LocationDto>> GetAllWareHouseLocation(int TenantId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var filtered = _warehouselocationRepository.GetAll().Where(e => e.state == "NSW" || e.state == "QLD" || e.state == "SA" || e.state == "VIC" );

                var output = from o in filtered
                             select new LocationDto()
                             {
                                 
                                     location = o.location,
                                     Address = o.Address,
                                     state = o.state,
                                     UnitNo = o.UnitNo,
                                     UnitType = o.UnitType,
                                     StreetNo = o.StreetNo,
                                     StreetName = o.StreetName,
                                     StreetType = o.StreetType,
                                     PostCode = o.PostCode,
                                     Latitude = o.Latitude,
                                     Longitude = o.Longitude,
                                     Suburb = o.Suburb,
                                     Id = o.Id,
                                 
                             };

                return await output.ToListAsync();
            }
        }

        public async Task<decimal?> GetSTCDealPrice(int TenantId)
        {
            using (UnitOfWorkManager.Current.SetTenantId(TenantId))
            {
                return await _stcDealPriceRepository.GetAll().Select(e => e.Value).FirstOrDefaultAsync();
            }
        }

    }
}
