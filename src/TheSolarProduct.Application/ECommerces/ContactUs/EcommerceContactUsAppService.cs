﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.PostCodes.Dtos;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct.ECommerces.BrandingPartners.Dto;
using TheSolarProduct.Common;
using TheSolarProduct.Storage;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.ECommerces.ContactUs.Dtos;
using TheSolarProduct.WholeSaleActivitys.Dtos;
using System.Net.Mail;
using Abp.Net.Mail;
using Newtonsoft.Json;
using static TheSolarProduct.ApplicationSettings.ApplicationSettingsAppService;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.WholeSaleLeads;
using TheSolarProduct.Organizations;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.WholeSaleLeadActivityLogs;
using Abp.EntityFrameworkCore;
using TheSolarProduct.WholeSaleLeadHistory;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.ECommerces.ContactUs
{
    public class EcommerceContactUsAppService : TheSolarProductAppServiceBase, IEcommerceContactUsAppService
    {
        private IRepository<EcommerceContactUs> _ecommerceContactUsRepository;
        private readonly IEmailSender _emailSender;
        private readonly IApplicationSettingsAppService _applicationSettingsAppService;
        private readonly IRepository<WholeSaleLead> _wholeSaleLeadsRepository;
        private readonly IRepository<EcommerceUserRegisterRequest> _ecommerceUserRegisterRequestRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendedOrganizationUnitRepository;
        private readonly IRepository<WholeSaleLeadActivityLog> _leadactivityRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public EcommerceContactUsAppService(
            IRepository<EcommerceContactUs> ecommerceContactUsRepository,
           IEmailSender emailSender,
           IApplicationSettingsAppService applicationSettingsAppService,
           IRepository<WholeSaleLead> wholeSaleLeadsRepository,
           IRepository<EcommerceUserRegisterRequest> ecommerceUserRegisterRequestRepository,
           UserManager userManager,
           IRepository<ExtendOrganizationUnit, long> extendedOrganizationUnitRepository,
           IRepository<WholeSaleLeadActivityLog> leadactivityRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            )
        {
            _ecommerceContactUsRepository = ecommerceContactUsRepository;
            _emailSender = emailSender;
            _applicationSettingsAppService = applicationSettingsAppService;
            _wholeSaleLeadsRepository = wholeSaleLeadsRepository;
            _ecommerceUserRegisterRequestRepository = ecommerceUserRegisterRequestRepository;
            _userManager = userManager;
            _extendedOrganizationUnitRepository = extendedOrganizationUnitRepository;
            _leadactivityRepository = leadactivityRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        
        public async Task<PagedResultDto<GetAllEcommerceContactUsDto>> GetAll(GetEcommerceContactUsInputDto input)
        {
            var filteredeEcommerceContactUs = _ecommerceContactUsRepository.GetAll()
                      // .Include(e => e.ProductTypeFK)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.FirstName.Contains(input.Filter) || e.LastName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter));

            var pagedAndFilteredEcommerceContactUs = filteredeEcommerceContactUs
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var ecommerceContactUs = from o in pagedAndFilteredEcommerceContactUs

                                     select new GetAllEcommerceContactUsDto()
                            {
                                ecommerceContactUs = new EcommerceContactUsDto
                                {
                                    Id = o.Id,
                                    FirstName = o.FirstName,
                                    LastName = o.LastName,
                                    Email = o.Email,
                                    Phone = o.Phone,
                                    Subject = o.Subject,
                                    Message = o.Message,
                                    CreateDate = o.CreationTime,
                                    WholesaleLeadId = o.WholesaleLeadId
                                }
                            };

            var totalCount = await filteredeEcommerceContactUs.CountAsync();

            return new PagedResultDto<GetAllEcommerceContactUsDto>(
                totalCount,
                await ecommerceContactUs.ToListAsync()
            );
        }

        public async Task<EcommerceContactUsDto> GetEcommerceContactUsForEdit(EntityDto input)
        {
            var contact = await _ecommerceContactUsRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<EcommerceContactUsDto>(contact);

            return output;
        }

        public async Task Delete(EntityDto input)
        {
            await _ecommerceContactUsRepository.DeleteAsync(input.Id);
        }

        public async Task Edit(EcommerceContactUsDto input)
        {
            var contact = await _ecommerceContactUsRepository.FirstOrDefaultAsync((int)input.Id);

            ObjectMapper.Map(input, contact);
            await _ecommerceContactUsRepository.UpdateAsync(contact);
        }

        public async Task SendEmail(WholeSaleSmsEmailDto input)
        {
            var tomail = input.EmailTo;
            MailMessage mail = new MailMessage
            {
                From = new MailAddress(input.EmailFrom),
                To = { tomail }, //{ "hiral.prajapati@meghtechnologies.com" }, //

                Subject = input.Subject,
                Body = input.Body,
                IsBodyHtml = true
            };
            await _emailSender.SendAsync(mail);
        }

        public async Task SendSMS(WholeSaleSmsEmailDto input)
        {
            var appsetting = _applicationSettingsAppService.GetApplicationSetting(AbpSession.TenantId);
            if (appsetting != null)
            {
                string authCode = appsetting.FoneDynamicsAccountSid + ":" + appsetting.FoneDynamicsToken;
                byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(authCode);
                string headerToken = "BASIC " + System.Convert.ToBase64String(data);

                var list = new Root();
                List<Message> listData = new List<Message>();

                var listitem = new Message();

                listitem.From = appsetting.FoneDynamicsPhoneNumber;
                listitem.To = input.PhoneNo;
                listitem.Text = input.Body;

                listData.Add(listitem);
                
                list.Messages = listData;

                string restURL = "https://api.fonedynamics.com/v2/Properties/" + appsetting.FoneDynamicsPropertySid + "/BatchMessages"; ;
                using (var httpClient = new HttpClient())
                {
                    StringContent content = new StringContent(JsonConvert.SerializeObject(list), Encoding.UTF8, "application/json");
                    httpClient.DefaultRequestHeaders.Add("Authorization", headerToken);
                    using (var response = await httpClient.PostAsync(restURL, content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                    }
                }
            }
            
        }

        public async Task<string> AddtoLead(int contactId, int OrganizationUnit)
        {
            var request = await _ecommerceContactUsRepository.FirstOrDefaultAsync(contactId);

            var existsUser = _wholeSaleLeadsRepository.GetAll().Any(e => e.Email == request.Email);
            if (!existsUser)
            {
                var UserName = request.Email.Split("@")[0];
                var Password = await _userManager.CreateRandomPassword();

                var user = new EcommerceUserRegisterRequest();
                user.Email = request.Email;
                user.FirstName = request.FirstName;
                user.LastName = request.LastName;
                user.IsApproved = true;
                user.UserName = UserName;
                user.Password = Password;
                var clientid = await _ecommerceUserRegisterRequestRepository.InsertAndGetIdAsync(user);

                var lead = new WholeSaleLead();
                lead.Email = request.Email;
                lead.FirstName = request.FirstName;
                lead.LastName = request.LastName;
                lead.WholesaleClientId = clientid;
                lead.OrganizationId = OrganizationUnit;
                lead.AssignDate = DateTime.UtcNow;
                lead.AssignUserId = (int)AbpSession.UserId;
                lead.FirstAssignDate = DateTime.UtcNow;
                lead.FirstAssignUserId = (int)AbpSession.UserId;

                lead.CreatorUserId = AbpSession.UserId;
                lead.TenantId = (int)AbpSession.TenantId;
                var leadid = await _wholeSaleLeadsRepository.InsertAndGetIdAsync(lead);

                WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
                leadactivity.ActionId = 53;
                leadactivity.SectionId = 44;
                leadactivity.ActionNote = "wholesale Lead Created";
                leadactivity.WholeSaleLeadId = leadid;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                //await _leadactivityRepository.InsertAsync(leadactivity);

                var leadactivityId = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                var List = new List<WholeSaleLeadtrackerHistory>();

                WholeSaleLeadtrackerHistory history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "FirstName";
                history.PrevValue = "";
                history.CurValue = request.FirstName;
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = leadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "LastName";
                history.PrevValue = "";
                history.CurValue = request.LastName;
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = leadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "Email";
                history.PrevValue = "";
                history.CurValue = request.Email;
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = leadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "WholesaleClientId";
                history.PrevValue = "";
                history.CurValue = clientid.ToString();
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = leadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "OrganizationId";
                history.PrevValue = "";
                history.CurValue = OrganizationUnit.ToString();
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = leadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "AssignDate";
                history.PrevValue = "";
                history.CurValue = DateTime.UtcNow.ToString();
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = leadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "AssignUserId";
                history.PrevValue = "";
                history.CurValue = AbpSession.UserId.ToString();
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = leadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "CreatorUserId";
                history.PrevValue = "";
                history.CurValue = AbpSession.UserId.ToString();
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = leadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "FirstAssignDate";
                history.PrevValue = "";
                history.CurValue = DateTime.UtcNow.ToString();
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = leadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "FirstAssignUserId";
                history.PrevValue = "";
                history.CurValue = AbpSession.UserId.ToString();
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = leadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "TenantId";
                history.PrevValue = "";
                history.CurValue = AbpSession.TenantId.ToString();
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = leadid;
                List.Add(history);

                await _dbcontextprovider.GetDbContext().WholeSaleLeadtrackerHistorys.AddRangeAsync(List);
                await _dbcontextprovider.GetDbContext().SaveChangesAsync();

                request.WholesaleLeadId = leadid;
                await _ecommerceContactUsRepository.UpdateAsync(request);

                var Body = "http://achievers.thesolarproduct.com/profile-authentication";
                var OrgLogo = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == OrganizationUnit).Select(e => (e.LogoFilePath + e.LogoFileName)).FirstOrDefault();
                OrgLogo = ApplicationSettingConsts.ViewDocumentPath + (OrgLogo != null ? OrgLogo.Replace("\\", "/") : "");
                var FromEmail = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == OrganizationUnit).Select(e => e.defaultFromAddress).FirstOrDefault();
                var FinalBody = "<!doctype html><html lang='en'><head><meta charset='utf-8'><meta name='viewport' content='width=device-width,initial-scale=1,shrink-to-fit=no'><meta name='viewport' content='width=device-width,initial-scale=1,shrink-to-fit=no'><link rel='preconnect' href='https://fonts.googleapis.com'><link rel='preconnect' href='https://fonts.gstatic.com' crossorigin><link href='https://fonts.googleapis.com/css2?family=Roboto&display=swap' rel='stylesheet'><title>Inline CSS Email</title></head><style>@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu72xKOzY.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu5mxKOzY.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-stle:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7mxKOzY.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu4WxKOzY.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7WxKOzY.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7GxKOzY.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(http://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu4mxK.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;rc:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}body{margin:0;padding:0;background:#48afb5;font-family:Roboto}</style><body style='background:#48afb5;margin:0;padding:0'><div style='background:#48afb5;margin:0;padding:20px 0'><div style='margin:10px auto;max-width:600px;min-width:320px'><div style='margin:0 10px;max-width:600px;min-width:320px;padding:0;background-color:#fff;border:1px solid #e2ebf1;border-radius:10px;overflow:hidden'><table style='width:100%;border:0;border-spacing:0;margin:0 auto'><tr><td style='text-align:center;padding:30px 0 20px'><img src='" + OrgLogo + "' style='max-width:200px'></td></tr><tr><td style='font-size:22px;font-weight:700;font-family:Roboto,sans-serif;color:#000;padding:0 30px;text-align:center;font-weight:900'>Dear " + request.FirstName + " " + request.LastName + ",</td></tr><tr><td style='height:30px'></td></tr><tr><td style='text-align:center;font-size:14px;font-weight:400;font-family:Roboto,sans-serif;color:#1a1a1a;padding:0 0'><div style='font-size:1.1rem;font-family:Roboto,sans-serif;color:#1a1a1a;margin:0 25px 30px;line-height:28px'><br> Your User Request Is Approved.</div><div> UserName : " + UserName + " <br/> PassWord : " + Password + "</div><div style='padding:20px 0'><div style='font-size:.9rem;font-weight:400;font-family:Roboto,sans-serif;margin-bottom:30px;line-height:24px'></div><div style='font-size:14px;font-weight:400;font-family:Roboto,sans-serif;line-height:24px'><a href='" + Body + "' style='text-decoration:none;text-decoration:none;background:#48afb5;font-weight:700;color:#fff;letter-spacing:1px;font-size:16px;text-transform:uppercase;border-radius:25px;height:50px;width:320px;display:inline-block;line-height:50px'>Click here to Login</a></div></div></td></tr></table></div></div></div></body></html>";

                await _emailSender.SendAsync(new MailMessage
                {
                    From = new MailAddress(FromEmail),
                    To = { lead.Email },
                    Subject = "Your Account is Added",
                    Body = FinalBody,
                    IsBodyHtml = true
                });

                //var User = await CreateWholesaleUserAsync(request, 0);

                //if(User.Id > 0)
                //{
                //    request.IsApproved = true;
                //    request.UserName = User.UserName;
                //    //await _ecommerceUserRegisterRequestRepository.UpdateAsync(request);
                //}

                return "Success";
            }
            else
            {
                return "Email Exists";
            }
        }
    }
}
