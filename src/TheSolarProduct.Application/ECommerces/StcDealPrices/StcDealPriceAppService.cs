﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ECommerces.STCRegisters.Dto;
using TheSolarProduct.ECommerces.STCRegisters;
using TheSolarProduct.Wholesales.Ecommerces;
using Abp.Authorization;
using Abp.EntityFrameworkCore;

using TheSolarProduct.ECommerces.StcDealPrices.Dtos;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using Abp.Collections.Extensions;
using System.Linq;
using TheSolarProduct.Currencies.Dtos;
using PayPalCheckoutSdk.Orders;
using TheSolarProduct.Wholesales.DataVault;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.ECommerces.StcDealPrices
{
   
    public class StcDealPriceAppService : TheSolarProductAppServiceBase, IStcDealPriceAppService
    {
        private readonly IRepository<StcDealPrice> _StcDealPriceRepository;
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        public StcDealPriceAppService(
            IRepository<StcDealPrice> StcDealPriceRepository,
            IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            )
        {
            _StcDealPriceRepository = StcDealPriceRepository;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;
            _dbcontextprovider = dbcontextprovider;
        }
        public async Task<GetStcDealPriceForEditOutput> GetStcDeakPriceForEdit(EntityDto input)
        {
            var StcDealPrice = await _StcDealPriceRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetStcDealPriceForEditOutput { StcDealPrice = ObjectMapper.Map<CreateOrEditStcDealPriceDto>(StcDealPrice) };

            return output;
        }


        public async Task<PagedResultDto<GetStcDealPriceForViewDto>> GetAll(GetAllCurrenciesInput input)
        {
            var currency = _StcDealPriceRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Value.ToString() == input.Filter);

            var pagedAndFiltered = currency
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFiltered
                         select new GetStcDealPriceForViewDto()
                         {
                             StcDealPrice = new StcDealPriceDto
                             {
                                 Value = o.Value.ToString(),
                                 Id= o.Id,
                             }
                         };

            var totalCount = await currency.CountAsync();

            return new PagedResultDto<GetStcDealPriceForViewDto>(totalCount, await output.ToListAsync());
        }



        public async Task CreateOrEdit(CreateOrEditStcDealPriceDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }



        protected virtual async Task Create(CreateOrEditStcDealPriceDto input)
        {
            var StcDealPrice = ObjectMapper.Map<StcDealPrice>(input);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 23;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "STC Deal Price Created : " + input.Value;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

            await _StcDealPriceRepository.InsertAsync(StcDealPrice);
        }


        protected virtual async Task Update(CreateOrEditStcDealPriceDto input)
        {
            var StcDealPrice = await _StcDealPriceRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 23;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "STC Deal Price Updated : " + StcDealPrice.Value;

            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<WholeSaleDataVaultActivityLogHistory>();

            if (input.Value != StcDealPrice.Value.ToString())
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Value";
                history.PrevValue = StcDealPrice.Value.ToString();
                history.CurValue = input.Value;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
           
           

            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, StcDealPrice);

            await _StcDealPriceRepository.UpdateAsync(StcDealPrice);
        }



        public async Task Delete(EntityDto input)
        {
            var Name = _StcDealPriceRepository.Get(input.Id).Value;

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 23;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "STC Deal Price Deleted : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

            await _StcDealPriceRepository.DeleteAsync(input.Id);
        }



    }
}
