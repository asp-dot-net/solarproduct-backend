﻿using TheSolarProduct.Jobs;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Linq.Extensions;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Dtos;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Storage;
using TheSolarProduct.Common;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct.ECommerces.EcommerceProductItems.Dtos;
using TheSolarProduct.MultiTenancy;
using NPOI.HPSF;
using TheSolarProduct.ApplicationSettings;
using System.Windows.Forms.Design;
using Microsoft.Azure.KeyVault.Models;
using System.IO;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Migrations;
using TheSolarProduct.Wholesales.DataVault;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.ECommerceSliders;
using TheSolarProduct.EcommerceProducctSpecification.Dtos;

namespace TheSolarProduct.ECommerces.EcommerceProductItems
{
    public class ECommerceProductItemsAppService : TheSolarProductAppServiceBase, IECommerceProductItemsAppService
    {
        private readonly IRepository<EcommerceProductItem> _ecommerceProductItemRepository;
        private readonly IRepository<EcommerceProductItemDocument> _ecommerceProductItemDocumentRepository;
        private readonly IRepository<EcommerceProductItemImage> _ecommerceProductItemImageRepository;
        private readonly IRepository<EcommerceProductItemPriceCategory> _ecommerceProductItemPriceCategoryRepository;
        private readonly IRepository<ProductItem> _productItemRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<ProductType> _productTypeRepository;
        private readonly IRepository<EcomerceSpecialStatus> _ecomerceSpecialStatusRepository;
        private readonly IRepository<BrandingPartner> _brandingPartnerRepository;
        private readonly IRepository<Wholesales.Ecommerces.Series> _SeriesRepository;
        private readonly IRepository<EcommerceProductType> _ecommerceProductTypeRepository;
        private readonly IRepository<EcommerceProductItemSpecification> _ecommerceProductTspecificationRepository;


        public ECommerceProductItemsAppService(
            IRepository<EcommerceProductItem> ecommerceProductItemRepository
            , IRepository<EcommerceProductItemDocument> ecommerceProductItemDocumentRepository
             ,IRepository<EcommerceProductItemImage> ecommerceProductItemImageRepository
            , IRepository<EcommerceProductItemPriceCategory> ecommerceProductItemPriceCategoryRepository
            , IRepository<ProductItem> productItemRepository
            , ITempFileCacheManager tempFileCacheManager
            , ICommonLookupAppService CommonDocumentSaveRepository
            , IRepository<Tenant> tenantRepository
            , IRepository<User, long> userRepository
            , IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository
            , IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            , IRepository<ProductType> productTypeRepository
            , IRepository<EcomerceSpecialStatus> ecomerceSpecialStatusRepository
            , IRepository<BrandingPartner> brandingPartnerRepository
            , IRepository<Wholesales.Ecommerces.Series> SeriesRepository
            , IRepository<EcommerceProductType> ecommerceProductTypeRepository
             , IRepository<EcommerceProductItemSpecification> ecommerceProductTspecificationRepository

            )
        {
            _ecommerceProductItemRepository = ecommerceProductItemRepository;
            _ecommerceProductItemDocumentRepository = ecommerceProductItemDocumentRepository;
            _ecommerceProductItemImageRepository = ecommerceProductItemImageRepository;
            _ecommerceProductItemPriceCategoryRepository = ecommerceProductItemPriceCategoryRepository;
            _productItemRepository = productItemRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _tenantRepository = tenantRepository;
            _userRepository = userRepository;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;
            _dbcontextprovider = dbcontextprovider;
            _productTypeRepository = productTypeRepository;
            _ecomerceSpecialStatusRepository = ecomerceSpecialStatusRepository;
            _brandingPartnerRepository = brandingPartnerRepository;
            _SeriesRepository = SeriesRepository;
            _ecommerceProductTypeRepository = ecommerceProductTypeRepository;
            _ecommerceProductTspecificationRepository= ecommerceProductTspecificationRepository;
        }

        public async Task CreateOrEdit(EcommerceProductItemDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(EcommerceProductItemDto input)
        {
            var ecommerceProductItem = ObjectMapper.Map<EcommerceProductItem>(input);

            var ecommerceProductItemId = await _ecommerceProductItemRepository.InsertAndGetIdAsync(ecommerceProductItem);

            var ProName = _productItemRepository.Get((int)ecommerceProductItem.ProductItemID);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 18;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "Product Item Created : " + ProName.Name;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);
            //foreach (var item in input.EcommerceProductItemDocuments)
            //{
            //    var ecommerceProductItemDocument = ObjectMapper.Map<EcommerceProductItemDocument>(item);
            //    ecommerceProductItemDocument.EcommerceProductItemId = ecommerceProductItemId;

            //    await _ecommerceProductItemDocumentRepository.InsertAndGetIdAsync(ecommerceProductItemDocument);
            //}
            foreach (var item in input.EcommerceProductItemPriceCategories)
            {
                var ecommerceProductItemPriceCategory = ObjectMapper.Map<EcommerceProductItemPriceCategory>(item);
                ecommerceProductItemPriceCategory.EcommerceProductItemId = ecommerceProductItemId;

                await _ecommerceProductItemPriceCategoryRepository.InsertAndGetIdAsync(ecommerceProductItemPriceCategory);
            }
        }

        protected virtual async Task Update(EcommerceProductItemDto input)
        {

            var ecommerceProductItem = await _ecommerceProductItemRepository.GetAll()
                                            .Include(e => e.ProductItemFk)
                                            .Include(e => e.ProductTypeFK)
                                            .Include(e => e.EcommerceProductTypeFK)
                                            .Include(e => e.BrandPartnerFK)
                                            .Include(e => e.SeriesFK)
                                            .Include(e => e.SpecialStatusFk)
                                            .Where(e => e.Id == input.Id).FirstOrDefaultAsync();

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 18;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "Product Item Updated : " + ecommerceProductItem.ProductItemFk.Name;

            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<WholeSaleDataVaultActivityLogHistory>();

            if (input.ProductDescription != ecommerceProductItem.ProductDescription)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "ProductDescription";
                history.PrevValue = ecommerceProductItem.ProductDescription;
                history.CurValue = input.ProductDescription;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.ProductCategoryId != ecommerceProductItem.ProductCategoryId)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "ProductCategoryId";
                history.PrevValue = ecommerceProductItem.ProductCategoryId > 0 ? _productTypeRepository.GetAll().Where(e => e.Id == ecommerceProductItem.ProductCategoryId).Select(e => e.Name).FirstOrDefault() : "";
                history.CurValue = input.ProductCategoryId > 0 ? _productTypeRepository.GetAll().Where(e => e.Id == input.ProductCategoryId).Select(e => e.Name).FirstOrDefault() : "";
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.ProductItemID != ecommerceProductItem.ProductItemID)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "ProductItem";
                history.PrevValue = ecommerceProductItem.ProductItemID > 0 ? _productItemRepository.GetAll().Where(e => e.Id == ecommerceProductItem.ProductItemID).Select(e => e.Name).FirstOrDefault() : "";
                history.CurValue = input.ProductItemID > 0 ? _productItemRepository.GetAll().Where(e => e.Id == input.ProductItemID).Select(e => e.Name).FirstOrDefault() : "";
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsDiscounted != ecommerceProductItem.IsDiscounted)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "IsDiscounted";
                history.PrevValue = ecommerceProductItem.IsDiscounted.ToString();
                history.CurValue = input.IsDiscounted.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsNewArrival != ecommerceProductItem.IsNewArrival)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "IsNewArrival";
                history.PrevValue = ecommerceProductItem.IsNewArrival.ToString();
                history.CurValue = input.IsNewArrival.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.IsFeatured != ecommerceProductItem.IsFeatured)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "IsFeatured";
                history.PrevValue = ecommerceProductItem.IsFeatured.ToString();
                history.CurValue = input.IsFeatured.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Rating != ecommerceProductItem.Rating)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Rating";
                history.PrevValue = ecommerceProductItem.Rating.ToString();
                history.CurValue = input.Rating.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.SpecialStatusId != ecommerceProductItem.SpecialStatusId)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "SpecialStatus";
                history.PrevValue = ecommerceProductItem.SpecialStatusId > 0 ? _ecomerceSpecialStatusRepository.GetAll().Where(e => e.Id == ecommerceProductItem.SpecialStatusId).Select(e => e.Status).FirstOrDefault() : "";
                history.CurValue = input.SpecialStatusId > 0 ? _ecomerceSpecialStatusRepository.GetAll().Where(e => e.Id == input.SpecialStatusId).Select(e => e.Status).FirstOrDefault() : "";
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != ecommerceProductItem.IsActive)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = ecommerceProductItem.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.BrandPartnerId != ecommerceProductItem.BrandPartnerId)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "BrandPartner";
                history.PrevValue = ecommerceProductItem.BrandPartnerId > 0 ? _brandingPartnerRepository.GetAll().Where(e => e.Id == ecommerceProductItem.BrandPartnerId).Select(e => e.BrandName).FirstOrDefault() : "";
                history.CurValue = input.BrandPartnerId > 0 ? _brandingPartnerRepository.GetAll().Where(e => e.Id == input.BrandPartnerId).Select(e => e.BrandName).FirstOrDefault() : "";
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.SeriesId != ecommerceProductItem.SeriesId)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Series";
                history.PrevValue = ecommerceProductItem.SeriesId > 0 ? _SeriesRepository.GetAll().Where(e => e.Id == ecommerceProductItem.SeriesId).Select(e => e.SeriesName).FirstOrDefault() : "";
                history.CurValue = input.SeriesId > 0 ? _SeriesRepository.GetAll().Where(e => e.Id == input.SeriesId).Select(e => e.SeriesName).FirstOrDefault() : "";
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.ProductShortDescription != ecommerceProductItem.ProductShortDescription)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "ProductShortDescription";
                history.PrevValue = ecommerceProductItem.ProductShortDescription;
                history.CurValue = input.ProductShortDescription;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Specification != ecommerceProductItem.Specification)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Specification";
                history.PrevValue = ecommerceProductItem.Specification;
                history.CurValue = input.Specification;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.EcommerceProductTypeId != ecommerceProductItem.EcommerceProductTypeId)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "EcommerceProductType";
                history.PrevValue = ecommerceProductItem.EcommerceProductTypeId > 0 ? _ecommerceProductTypeRepository.GetAll().Where(e => e.Id == ecommerceProductItem.EcommerceProductTypeId).Select(e => e.Type).FirstOrDefault() : "";
                history.CurValue = input.EcommerceProductTypeId > 0 ? _ecommerceProductTypeRepository.GetAll().Where(e => e.Id == input.EcommerceProductTypeId).Select(e => e.Type).FirstOrDefault() : "";
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, ecommerceProductItem);

            //await _ecommerceProductItemDocumentRepository.HardDeleteAsync(e => e.EcommerceProductItemId == input.Id);
            //foreach (var item in input.EcommerceProductItemDocuments)
            //{
            //    var ecommerceProductItemDocument = ObjectMapper.Map<EcommerceProductItemDocument>(item);
            //    ecommerceProductItemDocument.EcommerceProductItemId = ecommerceProductItem.Id;

            //    await _ecommerceProductItemDocumentRepository.InsertAndGetIdAsync(ecommerceProductItemDocument);
            //}

            await _ecommerceProductItemPriceCategoryRepository.HardDeleteAsync(e => e.EcommerceProductItemId == input.Id);
            foreach (var item in input.EcommerceProductItemPriceCategories)
            {
                var ecommerceProductItemPriceCategory = ObjectMapper.Map<EcommerceProductItemPriceCategory>(item);
                ecommerceProductItemPriceCategory.EcommerceProductItemId = ecommerceProductItem.Id;

                await _ecommerceProductItemPriceCategoryRepository.InsertAndGetIdAsync(ecommerceProductItemPriceCategory);
            }
        }

        public async Task Delete(EntityDto input)
        {
            

            var Name = _ecommerceProductItemRepository.Get(input.Id).ProductDescription;
            await _ecommerceProductItemRepository.DeleteAsync(input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 18;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "Product Item Deleted : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

            await _ecommerceProductItemDocumentRepository.DeleteAsync(e => e.EcommerceProductItemId == input.Id);
            await _ecommerceProductItemPriceCategoryRepository.DeleteAsync(e => e.EcommerceProductItemId == input.Id);
        }

        public async Task<PagedResultDto<GetAllEcommerceProductItemDto>> GetAll(GetAllEcommerceProductItemInput input)
        {
            var ecomProdIDs = new List<int?>();
            if(input.IsActive != "All")
            {
                ecomProdIDs = _ecommerceProductItemRepository.GetAll().Where(e => e.IsActive == true).Select(e => e.ProductItemID).ToList();
            }
                            

            var filteredECommerceSlider = _productItemRepository.GetAll()
                          .Include(e => e.ProductTypeFk)
                          .Where(e => e.ECommerce == true)
                          .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter) || e.Model.Contains(input.Filter))
                          .WhereIf(input.ProductCategoryId > 0 , e => e.ProductTypeId == input.ProductCategoryId)
                          .WhereIf(input.IsActive == "Active", e => ecomProdIDs.Contains(e.Id))
                          .WhereIf(input.IsActive == "DeActive", e => !ecomProdIDs.Contains(e.Id))
                          .AsNoTracking()
                          .Select(e => new { e.Id, product = e.Name, e.Model, category = e.ProductTypeFk.Name, e.ProductTypeFk.Image, e.Active, e.ProductTypeId, e.Size });

            var pagedAndFilteredECommerceSlider = filteredECommerceSlider
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var eCommerceSlider = from o in pagedAndFilteredECommerceSlider
                                  let ePro = _ecommerceProductItemRepository.GetAll().Where(e => e.ProductItemID == o.Id).FirstOrDefault()

                                  select new GetAllEcommerceProductItemDto()
                                  {
                                      EcommerceProductItem = new EcommerceProductItemDto()
                                      {
                                          ProductItemID = o.Id,
                                          Id = ePro != null ? ePro.Id : 0,
                                          ProductImage = o.Image,
                                          ProductName = o.product,
                                          ProductCategory = o.category,
                                          Model = o.Model,
                                          SpecialStatus = ePro != null ? (ePro.IsActive == true ? "Active" : "Deactiive") : "Deactiive",
                                          ProductCategoryId= o.ProductTypeId,
                                          Size = o.Size
                                      }
                                  };

            var totalCount = await filteredECommerceSlider.CountAsync();

            return new PagedResultDto<GetAllEcommerceProductItemDto>(
                totalCount,
                await eCommerceSlider.ToListAsync()
            );
        }

        public async Task<EcommerceProductItemDto> GetEcommerceProductItemForView(int id)
        {
            var result = await _ecommerceProductItemRepository.GetAll()
                .Include(e => e.ProductTypeFK)
                .Include(e => e.ProductItemFk)
                .Include(e => e.SpecialStatusFk)
                .Include(e => e.BrandPartnerFK)
                .Where(e => e.Id == id).AsNoTracking()
                .FirstOrDefaultAsync();

            var output = new EcommerceProductItemDto();

                output = ObjectMapper.Map<EcommerceProductItemDto>(result);

                output.ProductCategory = result.ProductTypeFK.Name;
                output.ProductImage = result.ProductTypeFK.Image != null ? ApplicationSettingConsts.ViewDocumentPath + result.ProductTypeFK.Image : "";
                output.ProductName = result.ProductItemFk.Name;
                output.Model = result.ProductItemFk.Model;
                output.Code = result.ProductItemFk.Id.ToString();
                output.Brand = result.ProductItemFk.Manufacturer;
                output.Size = result.ProductItemFk.Size;
                output.StockSeries = result.ProductItemFk.Series;
                output.SpecialStatus = result.SpecialStatusFk.Status;
                output.BrandName = result.BrandPartnerFK.BrandName;

                var resultProductItemDocuments = _ecommerceProductItemDocumentRepository.GetAll().Include(e => e.DocumentTypeFK).Where(e => e.EcommerceProductItemId == id);
                output.EcommerceProductItemDocuments = (from o in resultProductItemDocuments
                                                        select new EcommerceProductItemDocumentDto()
                                                        {
                                                            Id = o.Id,
                                                            EcommerceProductItemId = o.EcommerceProductItemId,
                                                            DocumentTypeId = o.DocumentTypeId,
                                                            Documemt = ApplicationSettingConsts.ViewDocumentPath + o.Documemt,
                                                            DocumentName = o.DocumentName,
                                                            DocumentType = o.DocumentTypeFK.DocumentType
                                                        }).ToList();

                var resultProductItemPriceCategories = _ecommerceProductItemPriceCategoryRepository.GetAll().Include(e => e.ECommercePriceCategoryFk).Where(e => e.EcommerceProductItemId == id);
                output.EcommerceProductItemPriceCategories = (from o in resultProductItemPriceCategories
                                                              select new EcommerceProductItemPriceCategoryDto()
                                                              {
                                                                  Id = o.Id,
                                                                  EcommerceProductItemId = o.EcommerceProductItemId,
                                                                  ECommercePriceCategoryId = o.ECommercePriceCategoryId,
                                                                  Price = o.Price,
                                                                  ECommercePriceCategory = o.ECommercePriceCategoryFk.PriceCategory
                                                              }).ToList();
            

            return output;
        }

        public async Task<EcommerceProductItemDto> GetEcommerceProductItemForEdit(EntityDto input)
        {
            var result = await _ecommerceProductItemRepository.FirstOrDefaultAsync(e => e.ProductItemID == input.Id);
            var output = new EcommerceProductItemDto();

            if (result != null)
            {
                output = ObjectMapper.Map<EcommerceProductItemDto>(result);

                //output.ProductCategory = result.ProductTypeFK.Name;
                //output.ProductImage = result.ProductTypeFK.Image;
                //output.ProductName = result.ProductItemFk.Name;
                //output.Model = result.ProductItemFk.Model;
                //output.Code = result.ProductItemFk.Id.ToString();
                //output.Brand = result.ProductItemFk.Manufacturer;
                //output.Size = result.ProductItemFk.Size;
                //output.StockSeries = result.ProductItemFk.Series;

                //var resultProductItemDocuments = _ecommerceProductItemDocumentRepository.GetAll().Include(e=>e.DocumentTypeFK).Where(e => e.EcommerceProductItemId == input.Id);
                //output.EcommerceProductItemDocuments = (from o in resultProductItemDocuments
                //                                        select new EcommerceProductItemDocumentDto()
                //                                        {
                //                                            Id = o.Id,
                //                                            EcommerceProductItemId = o.EcommerceProductItemId,
                //                                            DocumentTypeId = o.DocumentTypeId,
                //                                            Documemt = o.Documemt,
                //                                            DocumentName = o.DocumentName,
                //                                            DocumentType = o.DocumentTypeFK.DocumentType
                //                                        }).ToList();

                var resultProductItemPriceCategories = _ecommerceProductItemPriceCategoryRepository.GetAll().Where(e => e.EcommerceProductItemId == result.Id);
                output.EcommerceProductItemPriceCategories = (from o in resultProductItemPriceCategories
                                                              select new EcommerceProductItemPriceCategoryDto()
                                                              {
                                                                  Id = o.Id,
                                                                  EcommerceProductItemId = o.EcommerceProductItemId,
                                                                  ECommercePriceCategoryId = o.ECommercePriceCategoryId,
                                                                  Price = o.Price

                                                              }).ToList();
            }
            else
            {
                output.ProductItemID = input.Id;
                output.ProductCategoryId = _productItemRepository.GetAsync(input.Id).Result.ProductTypeId;
            }

            return output;
        }

        public async Task<List<GetProductItemForViewDto>> GetAllProductItems(string Model, int productType)
        {
            var productItems = from o in _productItemRepository.GetAll().Where(e => e.Model.Contains(Model) && e.ProductTypeId == productType).AsNoTracking()

                               select new GetProductItemForViewDto()
                               {
                                   ProductItem = new ProductItemDto
                                   {
                                       Name = o.Name,
                                       Manufacturer = o.Manufacturer,
                                       Model = o.Model,
                                       Series = o.Series,
                                       Size = o.Size,
                                       Code = o.Code,
                                       Id = o.Id,
                                       FileName = o.FileName,
                                       FilePath = o.FilePath,
                                       ExpiryDate = o.ExpiryDate,
                                       Status = o.Active == true ? "Active" : "Deactiive",
                                       Image = o.ProductTypeFk.Image
                                   }
                               };

            return productItems.ToList();
        }

        public async Task SaveDocument(EcommerceProductItemDocumentDto input)
        {
            byte[] ByteArray = _tempFileCacheManager.GetFile(input.FileToken);

            //if (input.FileType != "application/vnd.openxmlformats-officedocument.wordprocessingml.document" && input.DocumentTypeId == 1)
            //{
            //    throw new UserFriendlyException("Ooppps! There is a problem!", "Near Map sould be in word file type!");
            //}
            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
            var filename = DateTime.Now.Ticks + "_" + input.FileName.Replace(" ","");

            var filepath = _CommonDocumentSaveRepository.SaveEcommerceDocument(ByteArray, filename, "EcommereceProductItems", AbpSession.TenantId);

            input.Documemt = "\\Documents\\" + TenantName + "\\" + "WholeSale" + "\\EcommereceProductItems" + "\\" + filename;

            var productType = ObjectMapper.Map<EcommerceProductItemDocument>(input);
            //productType.Image = input.ImgPath;

            //if (AbpSession.TenantId != null)
            //{
            //	productType.TenantId = (int) AbpSession.TenantId;
            //}


            await _ecommerceProductItemDocumentRepository.InsertAsync(productType);

        }

        public async Task<List<EcommerceProductItemDocumentDto>> GetAllEcommerceProductItemDocuments(int productItemId)
        {
            var resultProductItemDocuments = _ecommerceProductItemDocumentRepository.GetAll().Include(e =>e.DocumentTypeFK).Where(e => e.EcommerceProductItemId == productItemId);
            var EcommerceProductItemDocuments = (from o in resultProductItemDocuments
                                                    select new EcommerceProductItemDocumentDto()
                                                    {
                                                        Id = o.Id,
                                                        EcommerceProductItemId = o.EcommerceProductItemId,
                                                        DocumentTypeId = o.DocumentTypeId,
                                                        Documemt = o.Documemt,
                                                        DocumentName = o.DocumentName,
                                                        DocumentType = o.DocumentTypeFK.DocumentType
                                                    }).ToList();

            return EcommerceProductItemDocuments;
        }

        public async Task<List<string>> GetAllCategories()
        {
            var result = _ecommerceProductItemRepository.GetAll().Select(e => e.BrandPartnerFK.BrandName).Distinct().ToList();

            return result;
        }

        public async Task DeleteDocument(EntityDto input)
        {
            await _ecommerceProductItemDocumentRepository.DeleteAsync(input.Id);
        }

        public async Task SaveImage(EcommerceProductItemImageDto input)
        {
            byte[] ByteArray = _tempFileCacheManager.GetFile(input.FileToken);

            //if (input.FileType != "application/vnd.openxmlformats-officedocument.wordprocessingml.document" && input.DocumentTypeId == 1)
            //{
            //    throw new UserFriendlyException("Ooppps! There is a problem!", "Near Map sould be in word file type!");
            //}
            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
            var filename = DateTime.Now.Ticks + "_" + input.FileName.Replace(" ", "");

            var filepath = _CommonDocumentSaveRepository.SaveEcommerceDocument(ByteArray, filename, "EcommereceProductItems", AbpSession.TenantId);

            input.Image = "\\Documents\\" + TenantName + "\\" + "WholeSale" + "\\EcommereceProductItems" + "\\" + filename;

            input.ImageName = filename;

            var productType = ObjectMapper.Map<EcommerceProductItemImage>(input);
            //productType.Image = input.ImgPath;

            //if (AbpSession.TenantId != null)
            //{
            //	productType.TenantId = (int) AbpSession.TenantId;
            //}


            await _ecommerceProductItemImageRepository.InsertAsync(productType);

        }

        public async Task<List<EcommerceProductItemImageDto>> GetAllEcommerceProductItemImages(int productItemId)
        {
            var resultProductItemImages = _ecommerceProductItemImageRepository.GetAll().Where(e => e.EcommerceProductItemId == productItemId);
            var EcommerceProductItemImages = (from o in resultProductItemImages
                                                 select new EcommerceProductItemImageDto()
                                                 {
                                                     Id = o.Id,
                                                     EcommerceProductItemId = o.EcommerceProductItemId,
                                                     ImageName = o.ImageName,
                                                     Image = o.Image,
                                                     Extention = Path.GetExtension(ApplicationSettingConsts.ViewDocumentPath + o.Image),
                                                     CreationTime = o.CreationTime.ToString("dd-MMM-yyyy"),
                                                     CreateBy = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault()
                                                 }).ToList();

            return EcommerceProductItemImages;
        }


        public async Task<List<EcommerceProductItemSpecificationDto>> GetAllEcommerceProductItemSpecification(int productItemId)
        {
            var resultProductItemSpecification = _ecommerceProductTspecificationRepository.GetAll().Include(e => e.EcommerceSpecificationfk).Where(e => e.EcommerceProductItemId == productItemId);
            var EcommerceProductItemSpecification = (from o in resultProductItemSpecification
                                                     select new EcommerceProductItemSpecificationDto()
                                                 {
                                                     Id = o.Id,
                                                     EcommerceProductItemId = o.EcommerceProductItemId,
                                                     SpecificationId = o.SpecificationId,
                                                     Value = o.Value,
                                                     Name = o.EcommerceSpecificationfk.Name
                                                   
                                                 }).ToList();

            return EcommerceProductItemSpecification;
        }


        public async Task SaveSpecification(EcommerceProductItemSpecificationDto input)
        {
            

            var productType = ObjectMapper.Map<EcommerceProductItemSpecification>(input);
            await _ecommerceProductTspecificationRepository.InsertAsync(productType);

        }


        public async Task DeleteD(EntityDto input)
        {
            await _ecommerceProductItemDocumentRepository.DeleteAsync(input.Id);
        }



        public async Task DeleteSpecification(EntityDto input)
        {
            await _ecommerceProductTspecificationRepository.DeleteAsync(input.Id);
        }



        public async Task DeleteImage(EntityDto input)
        {
            await _ecommerceProductItemImageRepository.DeleteAsync(input.Id);
        }


    }
}
