﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.ECommerces.EcommerceDataVaultActivity.Dtos;
using TheSolarProduct.WholeSaleDataVaultActivityLog.Dtos;
using TheSolarProduct.Wholesales.DataVault;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;



namespace TheSolarProduct.ECommerces.EcommerceDataVaultActivity
{
   
    public class EcommerceDataVaultActivityLogAppService : TheSolarProductAppServiceBase, IEcommerceDataVaultActivityLogAppService
    {
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityLogs;
        private readonly IRepository<WholeSaleDataVaultActivityLogHistory> _WholeSaleDataVaultActivityLogHistory;
        private readonly IRepository<User, long> _userRepository;

        public EcommerceDataVaultActivityLogAppService(
            IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityLogs,
            IRepository<WholeSaleDataVaultActivityLogHistory> WholeSaleDataVaultActivityLogHistory,
            IRepository<User, long> userRepository
            )
        {
            _WholeSaleDataVaultActivityLogs = WholeSaleDataVaultActivityLogs;
            _WholeSaleDataVaultActivityLogHistory = WholeSaleDataVaultActivityLogHistory;
            _userRepository = userRepository;
        }

        public async Task<PagedResultDto<GetAllEcommerceActivityLogDto>> GetAllEcommerceDataVaultActivityLog(GetEcommerceActivityLogInputDto input)
        {
            var ActivityLogIds = _WholeSaleDataVaultActivityLogs.GetAll().Where(e => e.Type == "Ecommerce").GroupBy(e => e.SectionId).Select(e => e.Max(m => m.Id)).ToList();



            var datavaultActivityLog = _WholeSaleDataVaultActivityLogs.GetAll().Where(e => ActivityLogIds.Contains(e.Id) && e.Type == "Ecommerce").AsNoTracking().Select(e => new { e.Id, e.SectionId, e.SectionFK.SectionName, e.CreatorUserId, e.CreationTime, e.Action, e.ActionNote });

            var pagedAndFilteredDatavaultActivityLog = datavaultActivityLog
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input).ToList();

            var result = from o in pagedAndFilteredDatavaultActivityLog
                         let user = _userRepository.Get((long)o.CreatorUserId).FullName

                         select new GetAllEcommerceActivityLogDto
                         {
                             Id = o.Id,
                             SectionId = (int)o.SectionId,
                             SectionName = o.SectionName,
                             UserName = user,
                             LastModificationTime = o.CreationTime,
                             ActionName = o.Action,
                             ActionNote = o.ActionNote
                         };

            var totalCount = await datavaultActivityLog.CountAsync();

            return new PagedResultDto<GetAllEcommerceActivityLogDto>(
            totalCount,
                result.ToList()
            );
        }

        public async Task<List<GetAllEcommerceActivityLogDto>> GetEcommerceDataVaultActivityLogDetail(int sectionId)
        {
            var datavaultActivityLog = _WholeSaleDataVaultActivityLogs.GetAll().Where(e => e.SectionId == sectionId).AsNoTracking().Select(e => new { e.Id, e.SectionId, e.SectionFK.SectionName, e.CreatorUserId, e.CreationTime, e.Action, e.ActionNote }).OrderByDescending(e => e.Id).ToList();

            var result = from o in datavaultActivityLog

                         select new GetAllEcommerceActivityLogDto
                         {
                             Id = o.Id,
                             SectionId = (int)o.SectionId,
                             SectionName = o.SectionName,
                             UserName = _userRepository.Get((long)o.CreatorUserId).FullName,
                             LastModificationTime = o.CreationTime,
                             ActionName = o.Action,
                             ActionNote = o.ActionNote
                         };

            return result.ToList();

        }


        public async Task<List<GetEcommerceActivityLogHistoryDto>> GetEcommerceDataVaultActivityLogHistory(int activityLogId)
        {
            var History = _WholeSaleDataVaultActivityLogHistory.GetAll().Where(e => e.ActivityLogId == activityLogId).AsNoTracking().Select(e => new { e.Id, e.Action, e.PrevValue, e.CurValue, e.FieldName });

            var result = from o in History
                         select new GetEcommerceActivityLogHistoryDto
                         {
                             Id = o.Id,
                             ActionName = o.Action,
                             prevValue = o.PrevValue,
                             curValue = o.CurValue,
                             FieldName = o.FieldName
                         };

            return await result.ToListAsync();
        }
    }
}
