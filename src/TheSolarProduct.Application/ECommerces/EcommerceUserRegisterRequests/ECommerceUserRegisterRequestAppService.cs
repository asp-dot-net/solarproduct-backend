﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct.ECommerces.EcommerceUserRegisterRequests.Dto;
using Microsoft.AspNetCore.Mvc;
using NPOI.SS.Formula.Functions;
using TheSolarProduct.Authorization.Users;
using Abp.Authorization.Users;
using Abp.Runtime.Session;
using Abp.UI;
using Microsoft.AspNetCore.Identity;
using System.Collections.ObjectModel;
using TheSolarProduct.Authorization.Users.Dto;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Url;
using TheSolarProduct.UserWiseEmailOrgs;
using TheSolarProduct.Authorization.Roles;
using System.Collections.Generic;
using TheSolarProduct.WholeSaleLeads;
using Abp.Net.Mail;
using System.Net.Mail;
using TheSolarProduct.Quotations;
using TheSolarProduct.Organizations;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.WholeSaleLeadActivityLogs;
using TheSolarProduct.WholeSaleLeadHistory;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Wholesales.DataVault;

namespace TheSolarProduct.ECommerces.EcommerceUserRegisterRequests
{
    public class ECommerceUserRegisterRequestAppService : TheSolarProductAppServiceBase, IECommerceUserRegisterRequestAppService
    {
        public IAppUrlService AppUrlService { get; set; }
        private readonly IRepository<EcommerceUserRegisterRequest> _ecommerceUserRegisterRequestRepository;
        private readonly IRepository<WholeSaleLead> _wholeSaleLeadsRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendedOrganizationUnitRepository;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<WholeSaleLeadActivityLog> _leadactivityRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;


        //private readonly IPasswordHasher<User> _passwordHasher;
        //private readonly RoleManager _roleManager;
        //private readonly IUserEmailer _userEmailer;

        public ECommerceUserRegisterRequestAppService(
            IRepository<EcommerceUserRegisterRequest> ecommerceUserRegisterRequestRepository,
            IRepository<WholeSaleLead> wholeSaleLeadsRepository,
            UserManager userManager,
            IRepository<ExtendOrganizationUnit, long> extendedOrganizationUnitRepository,
            IEmailSender emailSender,
            IRepository<WholeSaleLeadActivityLog> leadactivityRepository,
             IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
             IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository

            //IPasswordHasher<User> passwordHasher,
            //RoleManager roleManager,
            //IUserEmailer userEmailer
            ) {
            _ecommerceUserRegisterRequestRepository = ecommerceUserRegisterRequestRepository;
            _wholeSaleLeadsRepository = wholeSaleLeadsRepository;
            _userManager = userManager;
            _extendedOrganizationUnitRepository = extendedOrganizationUnitRepository;
            _emailSender = emailSender;
            _leadactivityRepository = leadactivityRepository;
            _dbcontextprovider = dbcontextprovider;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;
            //_passwordHasher = passwordHasher;
            //_roleManager = roleManager;
            //_userEmailer = userEmailer;
            //AppUrlService = NullAppUrlService.Instance;
        }

        public async Task CreateOrEdit(EcommerceUserRegisterRequestDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(EcommerceUserRegisterRequestDto input)
        {
            var request = ObjectMapper.Map<EcommerceUserRegisterRequest>(input);
            await _ecommerceUserRegisterRequestRepository.InsertAsync(request);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 15;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "User Request Created : " + input.UserName;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);
        }

        protected virtual async Task Update(EcommerceUserRegisterRequestDto input)
        {
            var request = await _ecommerceUserRegisterRequestRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 15;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "User Request Updated : " + request.UserName;

            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<WholeSaleDataVaultActivityLogHistory>();

            if (input.UserName != request.UserName)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "UserName";
                history.PrevValue = request.UserName;
                history.CurValue = input.UserName;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.FirstName != request.FirstName)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "FirstName";
                history.PrevValue = request.FirstName;
                history.CurValue = input.FirstName;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.LastName != request.LastName)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "LastName";
                history.PrevValue = request.LastName;
                history.CurValue = input.LastName;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Email != request.Email)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Email";
                history.PrevValue = request.Email;
                history.CurValue = input.Email;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Password != request.Password)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Password";
                history.PrevValue = request.Password;
                history.CurValue = input.Password;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsApproved != request.IsApproved)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "IsApproved";
                history.PrevValue = request.IsApproved.ToString();
                history.CurValue = input.IsApproved.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, request);
        }

        public async Task Delete(EntityDto input)
        {
            var Name = _ecommerceUserRegisterRequestRepository.Get(input.Id).UserName;
            await _ecommerceUserRegisterRequestRepository.DeleteAsync(input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 15;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "User Request Deleted : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);
        }

        public async Task<PagedResultDto<GetECommerceUserRegisterRequestViewDto>> GetAll(GetEcommerceUserRegisterRequestInputDto input)
        {
            var filteredRequest = _ecommerceUserRegisterRequestRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.UserName.Contains(input.Filter))
                        .WhereIf(input.IsApproved == "Yes", e => e.IsApproved == true)
                        .WhereIf(input.IsApproved == "No", e => e.IsApproved != true);

            var pagedAndFilteredRequest = filteredRequest
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var result = from o in pagedAndFilteredRequest
                                  select new GetECommerceUserRegisterRequestViewDto()
                                  {
                                      ecommerceUserRegisterRequest = new EcommerceUserRegisterRequestDto()
                                      {
                                          Id = o.Id,
                                          UserName = o.UserName,
                                          FirstName = o.FirstName,
                                          LastName = o.LastName,
                                          Email = o.Email,
                                          Password = o.Password,
                                          IsApproved = o.IsApproved,
                                      }
                                  };

            var totalCount = await filteredRequest.CountAsync();

            return new PagedResultDto<GetECommerceUserRegisterRequestViewDto>(
                totalCount,
                await result.ToListAsync()
            );
        }

        public async Task<GetECommerceUserRegisterRequestViewDto> GetEcommerceUserRegisterRequestForView(int id)
        {
            var request = await _ecommerceUserRegisterRequestRepository.GetAsync(id);

            var output = new GetECommerceUserRegisterRequestViewDto { ecommerceUserRegisterRequest = ObjectMapper.Map<EcommerceUserRegisterRequestDto>(request) };

            return output;
        }

        [HttpPut]
        public async Task<string> ApproveUserRequest(int requestId, int OrganizationUnit)
        {
            var request = await _ecommerceUserRegisterRequestRepository.FirstOrDefaultAsync(requestId);

            var existsUser = _wholeSaleLeadsRepository.GetAll().Any(e => e.Email == request.Email);
            if (!existsUser)
            {
                var UserName = request.Email.Split("@")[0];
                var Password = await _userManager.CreateRandomPassword();

                request.IsApproved = true;
                request.UserName = UserName;
                request.Password = Password;
                await _ecommerceUserRegisterRequestRepository.UpdateAsync(request);

                var lead = new WholeSaleLead();
                lead.Email = request.Email;
                lead.FirstName = request.FirstName;
                lead.LastName = request.LastName;
                lead.WholesaleClientId = requestId;
                lead.OrganizationId = OrganizationUnit;
                lead.AssignDate = DateTime.UtcNow;
                lead.AssignUserId = (int)AbpSession.UserId;
                lead.FirstAssignDate = DateTime.UtcNow;
                lead.FirstAssignUserId = (int)AbpSession.UserId;

                lead.CreatorUserId = AbpSession.UserId;
                lead.TenantId = (int)AbpSession.TenantId;
                //await _wholeSaleLeadsRepository.InsertAsync(lead);
                var wholeSaleLeadid = await _wholeSaleLeadsRepository.InsertAndGetIdAsync(lead);

                WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
                leadactivity.ActionId = 53;
                leadactivity.SectionId = 43;
                leadactivity.ActionNote = "User Request Is Approved and added to wholesale Lead";
                leadactivity.WholeSaleLeadId = wholeSaleLeadid;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                //await _leadactivityRepository.InsertAsync(leadactivity);

                var leadactivityId = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                var List = new List<WholeSaleLeadtrackerHistory>();

                WholeSaleLeadtrackerHistory history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "FirstName";
                history.PrevValue = "";
                history.CurValue = request.FirstName;
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = wholeSaleLeadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "LastName";
                history.PrevValue = "";
                history.CurValue = request.LastName;
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = wholeSaleLeadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "Email";
                history.PrevValue = "";
                history.CurValue = request.Email;
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = wholeSaleLeadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "WholesaleClientId";
                history.PrevValue = "";
                history.CurValue = requestId.ToString();
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = wholeSaleLeadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "OrganizationId";
                history.PrevValue = "";
                history.CurValue = OrganizationUnit.ToString();
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = wholeSaleLeadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "AssignDate";
                history.PrevValue = "";
                history.CurValue = DateTime.UtcNow.ToString(); 
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = wholeSaleLeadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "AssignUserId";
                history.PrevValue = "";
                history.CurValue = AbpSession.UserId.ToString();
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = wholeSaleLeadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "CreatorUserId";
                history.PrevValue = "";
                history.CurValue = AbpSession.UserId.ToString();
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = wholeSaleLeadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "FirstAssignDate";
                history.PrevValue = "";
                history.CurValue = DateTime.UtcNow.ToString();
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = wholeSaleLeadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "FirstAssignUserId";
                history.PrevValue = "";
                history.CurValue = AbpSession.UserId.ToString();
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = wholeSaleLeadid;
                List.Add(history);

                history = new WholeSaleLeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    history.TenantId = (int)AbpSession.TenantId;
                }
                history.FieldName = "TenantId";
                history.PrevValue = "";
                history.CurValue = AbpSession.TenantId.ToString();
                history.Action = "Add";
                history.WholeSaleLeadActivityId = leadactivityId;
                history.LastmodifiedDateTime = DateTime.Now;
                history.WholeSaleLeadId = wholeSaleLeadid;
                List.Add(history);

                await _dbcontextprovider.GetDbContext().WholeSaleLeadtrackerHistorys.AddRangeAsync(List);
                await _dbcontextprovider.GetDbContext().SaveChangesAsync();

                var Body = "http://achievers.thesolarproduct.com/profile-authentication";
                var OrgLogo = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == OrganizationUnit).Select(e => (e.LogoFilePath + e.LogoFileName)).FirstOrDefault();
                OrgLogo = ApplicationSettingConsts.ViewDocumentPath + (OrgLogo != null ? OrgLogo.Replace("\\", "/") : "");
                var FromEmail = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == OrganizationUnit).Select(e => e.defaultFromAddress).FirstOrDefault();
                var FinalBody = "<!doctype html><html lang='en'><head><meta charset='utf-8'><meta name='viewport' content='width=device-width,initial-scale=1,shrink-to-fit=no'><meta name='viewport' content='width=device-width,initial-scale=1,shrink-to-fit=no'><link rel='preconnect' href='https://fonts.googleapis.com'><link rel='preconnect' href='https://fonts.gstatic.com' crossorigin><link href='https://fonts.googleapis.com/css2?family=Roboto&display=swap' rel='stylesheet'><title>Inline CSS Email</title></head><style>@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu72xKOzY.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu5mxKOzY.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-stle:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7mxKOzY.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu4WxKOzY.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7WxKOzY.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7GxKOzY.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(http://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu4mxK.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;rc:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}body{margin:0;padding:0;background:#48afb5;font-family:Roboto}</style><body style='background:#48afb5;margin:0;padding:0'><div style='background:#48afb5;margin:0;padding:20px 0'><div style='margin:10px auto;max-width:600px;min-width:320px'><div style='margin:0 10px;max-width:600px;min-width:320px;padding:0;background-color:#fff;border:1px solid #e2ebf1;border-radius:10px;overflow:hidden'><table style='width:100%;border:0;border-spacing:0;margin:0 auto'><tr><td style='text-align:center;padding:30px 0 20px'><img src='" + OrgLogo + "' style='max-width:200px'></td></tr><tr><td style='font-size:22px;font-weight:700;font-family:Roboto,sans-serif;color:#000;padding:0 30px;text-align:center;font-weight:900'>Dear " + request.FirstName + " " + request.LastName + ",</td></tr><tr><td style='height:30px'></td></tr><tr><td style='text-align:center;font-size:14px;font-weight:400;font-family:Roboto,sans-serif;color:#1a1a1a;padding:0 0'><div style='font-size:1.1rem;font-family:Roboto,sans-serif;color:#1a1a1a;margin:0 25px 30px;line-height:28px'><br> Your User Request Is Approved.</div><div> UserName : " + request.UserName + " <br/> PassWord : " + request.Password + "</div><div style='padding:20px 0'><div style='font-size:.9rem;font-weight:400;font-family:Roboto,sans-serif;margin-bottom:30px;line-height:24px'></div><div style='font-size:14px;font-weight:400;font-family:Roboto,sans-serif;line-height:24px'><a href='" + Body + "' style='text-decoration:none;text-decoration:none;background:#48afb5;font-weight:700;color:#fff;letter-spacing:1px;font-size:16px;text-transform:uppercase;border-radius:25px;height:50px;width:320px;display:inline-block;line-height:50px'>Click here to Login</a></div></div></td></tr></table></div></div></div></body></html>";

                await _emailSender.SendAsync(new MailMessage
                {
                    From = new MailAddress(FromEmail),
                    To = { lead.Email },
                    Subject = "Your User Requset is Approved",
                    Body = FinalBody,
                    IsBodyHtml = true
                });

                //var User = await CreateWholesaleUserAsync(request, 0);

                //if(User.Id > 0)
                //{
                //    request.IsApproved = true;
                //    request.UserName = User.UserName;
                //    //await _ecommerceUserRegisterRequestRepository.UpdateAsync(request);
                //}

                return "Success";
            }
            else
            {
                return "Email Exists";
            }

        }
        //protected virtual async Task<User> CreateWholesaleUserAsync(EcommerceUserRegisterRequest request, int OrganizationId)
        //{
        //    var UserName = request.Email.Split("@")[0];
        //    var Password = "";

        //    var _user = new User();
        //    _user.EmailAddress = request.Email;
        //    _user.UserName = UserName;
        //    _user.Name = request.FirstName;
        //    _user.Surname = request.LastName;
        //    _user.CompanyName = (request.FirstName + " " + request.LastName).Trim();

        //    var user = ObjectMapper.Map<User>(_user); //Passwords is not mapped (see mapping configuration)
        //    user.TenantId = AbpSession.TenantId;

        //    //Set password
        //    var randomPassword = 
        //    user.Password = _passwordHasher.HashPassword(user, randomPassword);
        //    Password = randomPassword;

        //    //Assign roles
        //    user.Roles = new Collection<UserRole>();
        //    var roleName = StaticRoleNames.Tenants.WholesaleClient;
        //    var role = await _roleManager.GetRoleByNameAsync(roleName);
        //    user.Roles.Add(new UserRole(AbpSession.TenantId, user.Id, role.Id));

        //    //CheckErrors(await UserManager.CreateAsync(user));
        //    //await CurrentUnitOfWork.SaveChangesAsync(); //To get new user's Id.

        //    //if (input.UserEmailDetail != null)
        //    //{
        //    //    foreach (var useremailorgdetail in input.UserEmailDetail)
        //    //    {
        //    //        input.OrganizationUnits.Add(useremailorgdetail.OrganizationUnitId);
        //    //        if (useremailorgdetail.OrganizationUnitId != null)
        //    //        {
        //    //            var detail = new UserWiseEmailOrg();
        //    //            detail.UserId = (long)user.Id;
        //    //            if (AbpSession.TenantId != null)
        //    //            {
        //    //                detail.TenantId = (int)AbpSession.TenantId;
        //    //            }
        //    //            detail.EmailFromAdress = useremailorgdetail.EmailFromAdress;
        //    //            detail.OrganizationUnitId = useremailorgdetail.OrganizationUnitId;
        //    //            _UserWiseEmailOrgsRepository.Insert(detail);
        //    //        }
        //    //    }
        //    //}

        //    //Organization Units
        //    var OrganizationUnits = new List<long> { OrganizationId };
        //    //await UserManager.SetOrganizationUnitsAsync(user, OrganizationUnits.ToArray());

        //    //Send activation email
        //    user.SetNewEmailConfirmationCode();
        //    await _userEmailer.SendEmailActivationLinkAsync(
        //        user,
        //        AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId),
        //        Password
        //    );

        //    return user;
        //}
    }
}
