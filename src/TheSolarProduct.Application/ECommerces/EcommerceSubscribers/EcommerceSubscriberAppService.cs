﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using TheSolarProduct.ECommerces.EcommerceSolarPackages.Dto;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct.ECommerces.EcommerceSubscribers.Dto;
using System.Net.Mail;
using Abp.Net.Mail;
using TheSolarProduct.Leads;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Wholesales.DataVault;

namespace TheSolarProduct.ECommerces.EcommerceSubscribers
{
    public class EcommerceSubscriberAppService : TheSolarProductAppServiceBase, IEcommerceSubscriberAppService
    {
        private readonly IRepository<EcommerceSubscriber> _ecommerceSubscriberRepository;
        private readonly IEmailSender _emailSender;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;

        public EcommerceSubscriberAppService(
           IRepository<EcommerceSubscriber> ecommerceSubscriberRepository,
           IEmailSender emailSender,
           IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
           IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository
           ) 
        { 
            _ecommerceSubscriberRepository = ecommerceSubscriberRepository;
            _emailSender = emailSender;
            _dbcontextprovider = dbcontextprovider;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;
        }

        public async Task<PagedResultDto<GetAllEcommerceSubscriberViewDto>> GetAll(GetEcommerceSubscriberInputDto input)
        {
            var filtered = _ecommerceSubscriberRepository.GetAll()
                       .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.SubscriberEmail.Contains(input.Filter));

            var pagedAndFiltered = filtered
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var outputList = from o in pagedAndFiltered
                             select new GetAllEcommerceSubscriberViewDto()
                             {
                                 Id = o.Id,
                                 SubscriberEmail = o.SubscriberEmail,
                                 IsSelected = false
                             };

            var totalCount = await outputList.CountAsync();

            return new PagedResultDto<GetAllEcommerceSubscriberViewDto>(
                totalCount,
                await outputList.ToListAsync()
            );
        }

        public async Task SendEmailBluk(GetEcommerceSubscribeEmailDto input)
        {
            foreach( var o in input.EmailList)
            {
                var tomail = o;
                MailMessage mail = new MailMessage
                {
                    From = new MailAddress(input.EmailFrom),
                    To = { tomail }, //{ "hiral.prajapati@meghtechnologies.com" }, //

                    Subject = input.Subject,
                    Body = input.Body,
                    IsBodyHtml = true
                };
                await this._emailSender.SendAsync(mail);
            }
        }
    }
}
