﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using System;
using System.Collections.Generic;
using TheSolarProduct.ECommerces.EcommerceSolarPackages.Dto;
using TheSolarEcommerceSolar.ECommerces.EcommerceSolarPackages;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct;
using TheSolarProduct.Common;
using TheSolarProduct.Storage;
using TheSolarProduct.MultiTenancy;
using Microsoft.AspNetCore.Mvc;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Wholesales.DataVault;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarEcommerceSolar.EcommerceSolarPackages
{
    [AbpAuthorize]
    public class EcommerceSolarPackageAppService : TheSolarProductAppServiceBase, IEcommerceSolarPackagesAppService
    {
        private readonly IRepository<EcommerceSolarPackage> _EcommerceSolarPackageRepository;
        private readonly IRepository<EcommerceSolarPackageItem> _EcommerceSolarPackageItemRepository;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly TempFileCacheManager _tempFileCacheManager;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<EcommerceProductItem> _ecommerceProductItemRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;

        public EcommerceSolarPackageAppService(IRepository<EcommerceSolarPackage> EcommerceSolarPackageRepository
            , IRepository<EcommerceSolarPackageItem> EcommerceSolarPackageItemRepository,
              ICommonLookupAppService CommonDocumentSaveRepository,
              TempFileCacheManager tempFileCacheManager,
              IRepository<Tenant> tenantRepository,
              IRepository<EcommerceProductItem> ecommerceProductItemRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
              IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository
            )
        {
            _EcommerceSolarPackageRepository = EcommerceSolarPackageRepository;
            _EcommerceSolarPackageItemRepository = EcommerceSolarPackageItemRepository;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _tenantRepository = tenantRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _ecommerceProductItemRepository = ecommerceProductItemRepository;
            _dbcontextprovider = dbcontextprovider;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;
        }

        public async Task<PagedResultDto<GetEcommerceSolarPackageForViewDto>> GetAll(GetAllEcommerceSolarPackageForInput input)
        {
            var filtered = _EcommerceSolarPackageRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var pagedAndFiltered = filtered
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var outputList = from o in pagedAndFiltered
                             select new GetEcommerceSolarPackageForViewDto()
                             {
                                 EcommerceSolarPackage = new EcommerceSolarPackageDto
                                 {
                                     Name = o.Name,
                                     Price = o.Price,
                                     IsActive = o.IsActive,
                                     Id = o.Id
                                 }
                             };

            var totalCount = await outputList.CountAsync();

            return new PagedResultDto<GetEcommerceSolarPackageForViewDto>(
                totalCount,
                await outputList.ToListAsync()
            );
        }

        public async Task<GetEcommerceSolarPackageForEditOutput> GetEcommerceSolarPackageForView(int id)
        {
            var retult = await _EcommerceSolarPackageRepository.GetAsync(id);

            var output = new GetEcommerceSolarPackageForEditOutput { EcommerceSolarPackage = ObjectMapper.Map<CreateOrEditEcommerceSolarPackageDto>(retult) };
            var packageItemResult = _EcommerceSolarPackageItemRepository.GetAll().Include(e => e.ProductItemFk).Include(e => e.ProductItemFk.ProductTypeFk).Where(e => e.EcommerceSolarPackageId == id);

            output.EcommerceSolarPackage.EcommerceSolarPackageItem = (from o in packageItemResult
                                                                      select new CreateOrEditEcommerceSolarPackagesItemDto()
                                                                      {
                                                                          Id = o.Id,
                                                                          ProductItemId = o.ProductItemId,
                                                                          EcommerceSolarPackageId = o.EcommerceSolarPackageId,
                                                                          ProductTypeId = o.ProductItemFk.ProductTypeId,
                                                                          ProductItem = o.ProductItemFk.Name,
                                                                          Model = o.ProductItemFk.Model,
                                                                          Quantity = o.Quantity,
                                                                          ProductType = o.ProductItemFk.ProductTypeFk.Name

                                                                      }).ToList();

            return output;
        }

        //    //[AbpAuthorize(AppPermissions.Pages_EcommerceSolarPackages_Edit)]
        public async Task<GetEcommerceSolarPackageForEditOutput> GetEcommerceSolarPackageForEdit(EntityDto input)
        {
            var result = await _EcommerceSolarPackageRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetEcommerceSolarPackageForEditOutput { EcommerceSolarPackage = ObjectMapper.Map<CreateOrEditEcommerceSolarPackageDto>(result) };


            var packageItemResult = _EcommerceSolarPackageItemRepository.GetAll().Include(e => e.ProductItemFk).Where(e => e.EcommerceSolarPackageId == input.Id);

            output.EcommerceSolarPackage.EcommerceSolarPackageItem = (from o in packageItemResult
                                                                      select new CreateOrEditEcommerceSolarPackagesItemDto()
                                                                      {
                                                                          Id = o.Id,
                                                                          ProductItemId = o.ProductItemId,
                                                                          EcommerceSolarPackageId = o.EcommerceSolarPackageId,
                                                                          ProductTypeId = o.ProductItemFk.ProductTypeId,
                                                                          ProductItem = o.ProductItemFk.Name,
                                                                          Model = o.ProductItemFk.Model,
                                                                          Quantity = o.Quantity

                                                                      }).ToList();

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditEcommerceSolarPackageDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(CreateOrEditEcommerceSolarPackageDto input)
        {
            var FileName = DateTime.Now.Ticks + "_" + input.Name.Replace(" ", "") + ".png";
            var InstallerByteArray = _tempFileCacheManager.GetFile(input.FileToken);

            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();

            var filepath = _CommonDocumentSaveRepository.SaveEcommerceDocument(InstallerByteArray, FileName, "EcommerceSolarPackage", AbpSession.TenantId);

            input.Banner = "\\Documents\\" + TenantName + "\\" + "WholeSale" + "\\EcommerceSolarPackage" + "\\" + FileName;

            var output = ObjectMapper.Map<EcommerceSolarPackage>(input);

            await _EcommerceSolarPackageRepository.InsertAndGetIdAsync(output);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 16;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "Solar Package Created : " + input.Name;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

            if (output.Id > 0)
            {
                foreach (var item in input.EcommerceSolarPackageItem)
                {
                    var outputItem = ObjectMapper.Map<EcommerceSolarPackageItem>(item);
                    outputItem.EcommerceSolarPackageId = output.Id;
                    await _EcommerceSolarPackageItemRepository.InsertAsync(outputItem);
                }
            }
        }


        protected virtual async Task Update(CreateOrEditEcommerceSolarPackageDto input)
        {
            if (!string.IsNullOrEmpty(input.FileToken))
            {
                var FileName = DateTime.Now.Ticks + "_" + input.Name.Replace(" ", "") + ".png";
                var InstallerByteArray = _tempFileCacheManager.GetFile(input.FileToken);

                var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();

                var filepath = _CommonDocumentSaveRepository.SaveEcommerceDocument(InstallerByteArray, FileName, "EcommerceSolarPackage", AbpSession.TenantId);

                input.Banner = "\\Documents\\" + TenantName + "\\" + "WholeSale" + "\\EcommerceSolarPackage" + "\\" + FileName;
            }
            var output = await _EcommerceSolarPackageRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 16;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "Solar Package Updated : " + output.Name;

            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<WholeSaleDataVaultActivityLogHistory>();

            if (input.Name != output.Name)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = output.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Price != output.Price)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Price";
                history.PrevValue = output.Price.ToString();
                history.CurValue = input.Price.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != output.IsActive)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = output.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Banner != output.Banner)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Banner";
                history.PrevValue = output.Banner.ToString();
                history.CurValue = input.Banner.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, output);

            #region EcommerceSolar Package Item
            if (input.EcommerceSolarPackageItem != null)
            {
                var existData = _EcommerceSolarPackageItemRepository.GetAll().AsNoTracking().Where(x => x.EcommerceSolarPackageId == input.Id).Select(e => e.Id).ToList();
                if (existData.Any())
                {
                    var Ids = input.EcommerceSolarPackageItem.Select(e => e.Id).ToList();
                    foreach (var item in existData)
                    {
                        if (Ids.Count > 0)
                        {
                            bool containsItem = Ids.Any(x => x == item);
                            if (containsItem == false)
                            {
                                await _EcommerceSolarPackageItemRepository.DeleteAsync(item);
                            }
                        }
                        else
                        {
                            await _EcommerceSolarPackageItemRepository.DeleteAsync(item);
                        }
                    }

                    foreach (var item in input.EcommerceSolarPackageItem)
                    {
                        if (item.Id != null && item.Id != 0)
                        {
                            var EcommerceSolarPackageItem = await _EcommerceSolarPackageItemRepository.GetAsync((int)item.Id);
                            item.EcommerceSolarPackageId = output.Id;
                            ObjectMapper.Map(item, EcommerceSolarPackageItem);
                        }
                        else
                        {
                            var dto = ObjectMapper.Map<EcommerceSolarPackageItem>(item);
                            dto.EcommerceSolarPackageId = output.Id;
                            await _EcommerceSolarPackageItemRepository.InsertAsync(dto);
                        }
                    }
                }
                else
                {
                    foreach (var item in input.EcommerceSolarPackageItem)
                    {
                        var dto = ObjectMapper.Map<EcommerceSolarPackageItem>(item);
                        dto.EcommerceSolarPackageId = output.Id;
                        await _EcommerceSolarPackageItemRepository.InsertAsync(dto);
                    }
                }
            }
            #endregion
        }

        public async Task Delete(EntityDto input)
        {
            

            var Name = _EcommerceSolarPackageRepository.Get(input.Id).Name;
            await _EcommerceSolarPackageRepository.DeleteAsync(input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 16;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "Solar Package Deleted : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);
        }

        public async Task<List<NameValueDto>> GetEcommerceSolarPackageForDropdown()
        {
            var result = _EcommerceSolarPackageRepository.GetAll().Where(e => e.IsActive == true);

            var output = from o in result
                         select new NameValueDto()
                         {
                             Name = o.Name,
                             Value = o.Id.ToString()
                         };

            return await output.ToListAsync();
        }

        public async Task<List<CreateOrEditEcommerceSolarPackagesItemDto>> GetEcommerceSolarPackageItemByPackageId(int EcommerceSolarPackageId)
        {
            var result = _EcommerceSolarPackageItemRepository.GetAll().Where(e => e.EcommerceSolarPackageId == EcommerceSolarPackageId);

            var output = from o in result
                         select new CreateOrEditEcommerceSolarPackagesItemDto()
                         {
                             Id = o.Id,
                             ProductItemId = o.ProductItemId,
                             EcommerceSolarPackageId = o.EcommerceSolarPackageId,
                             ProductTypeId = o.ProductItemFk.ProductTypeId,
                             Model = o.ProductItemFk.Model,
                             Quantity = o.Quantity,
                             Size = o.ProductItemFk.Size,
                             DatasheetUrl = (o.ProductItemFk.FilePath + o.ProductItemFk.FileName)
                         };

            return await output.ToListAsync();
        }

        //[HttpGet]
        //public async Task<List<GetAllPackageDetailsDto>> GetAllPackageDetails()
        //{
        //    var EcommerceSolarPackages = _EcommerceSolarPackageRepository.GetAll().Where(e => e.IsActive == true);

        //    var output = from o in EcommerceSolarPackages
        //                 select new GetAllPackageDetailsDto()
        //                 {
        //                     Id = o.Id,
        //                     Name = o.Name,
        //                     Price = o.Price,
        //                     PackageItems = (from p in _EcommerceSolarPackageItemRepository.GetAll().Where(e => e.ProductItemId == o.Id)
        //                                     select new EcommerceSolarPackageItems
        //                                     {
        //                                         ProductType = p.ProductItemFk.ProductTypeFk.Name,
        //                                         ProductItem = p.ProductItemFk.Name,
        //                                         Model = p.ProductItemFk.Model,
        //                                         Quantity = p.Quantity
        //                                     }).ToList(),
        //                 };

        //    return await output.ToListAsync();
        //}

        public async Task<List<GetProductItemSearchResult>> GetPicklistProductItemList(int productTypeId, string productItem)
        {

            return await _ecommerceProductItemRepository.GetAll()
                    .Where(e => e.ProductItemFk.ProductTypeId == productTypeId && e.IsActive == true && e.ProductItemFk.ECommerce == true)
                    .WhereIf(!string.IsNullOrWhiteSpace(productItem), c => c.ProductItemFk.Name.ToLower().Contains(productItem.ToLower()))
                    .Select(p => new GetProductItemSearchResult() { Id = p.ProductItemFk.Id, ProductItem = p.ProductItemFk.Name, ProductModel = p.ProductItemFk.Model, Size = p.ProductItemFk.Size }).ToListAsync();
        }
    }
}
