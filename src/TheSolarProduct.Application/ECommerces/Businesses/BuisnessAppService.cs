﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;

using TheSolarProduct.DocumentTypes.Dtos;
using TheSolarProduct.Quotations;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.ECommerces.ECommerceSliders.Dtos;
using TheSolarProduct.ECommerceSliders;
using TheSolarProduct.Common;
using TheSolarProduct.Storage;
using TheSolarProduct.ECommerces.Businesses.Dtos;
using TheSolarProduct.Businesses;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.WholeSaleLeads;
using Abp.Collections.Extensions;

namespace TheSolarProduct.ECommerces.Businesses
{
    public class BuisnessAppService : TheSolarProductAppServiceBase, IBuisnessAppService
    {
        private readonly IRepository<Business> _businessRepository;
        private readonly IRepository<WholeSaleLead> _wholeSaleLeadRepository;
        
        public BuisnessAppService(
            IRepository<Business> businessRepository
            , IRepository<WholeSaleLead> wholeSaleLeadRepository
            )
        {
            _businessRepository = businessRepository;
            _wholeSaleLeadRepository = wholeSaleLeadRepository;
        }

        public async Task CreateOrEdit(BusinessDto input)
        {
            input.Id = input.Id != null ? input.Id : 0;
            if (input.Id == 0)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        protected virtual async Task Create(BusinessDto input)
        {
            var business = ObjectMapper.Map<Business>(input);
            
            await _businessRepository.InsertAsync(business);
        }

        protected virtual async Task Update(BusinessDto input)
        {
            var business = await _businessRepository.FirstOrDefaultAsync((int)input.Id);

            ObjectMapper.Map(input, business);
            await _businessRepository.UpdateAsync(business);
        }
        public async Task Delete(EntityDto input)
        {
            await _businessRepository.DeleteAsync(input.Id);

        }

        public async Task<PagedResultDto<GetAllBusinessDto>> GetAll(GetAllBusinessInput input)
        {

            var ecomBussIDs = new List<int?>();
            

            var filteredBusiness = _businessRepository.GetAll().Include(e => e.BuisnessStructureFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.ABNNumber.Contains(input.Filter) || e.LegalEntityName.Contains(input.Filter) || e.MainTradingName.Contains(input.Filter) || e.OtherTradingNames.Contains(input.Filter))

                         .WhereIf(input.BuisnessStructureId > 0, e => e.BuisnessStructureId == input.BuisnessStructureId)
                          .WhereIf(input.IsApproved != "All" && !string.IsNullOrEmpty(input.IsApproved), e => e.Status == input.IsApproved)
                          
                        //.WhereIf(input.IsApproved == "Approved", e => e.IsApproved == true)
                        //.WhereIf(input.IsApproved == "Not Approved", e => e.IsApproved==false)

                        .AsNoTracking().Select(e => new {e.Id, e.ABNNumber, e.LegalEntityName, e.PrimaryFullName, e.PrimaryContactNo, e.PrimaryEmail, e.CreditLimitRequested, e.IsConfirm, e.IsApproved, e.ApprovedAmount, e.Status, e.Notes});

            var pagedAndFilteredBusiness = filteredBusiness
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var businessses = from o in pagedAndFilteredBusiness
                            select new GetAllBusinessDto()
                                  {
                                      Business = new BusinessDto()
                                      {
                                          Id = o.Id,
                                          ABNNumber = o.ABNNumber,
                                          LegalEntityName = o.LegalEntityName,
                                          PrimaryFullName = o.PrimaryFullName,
                                          PrimaryContactNo = o.PrimaryContactNo,
                                          PrimaryEmail = o.PrimaryEmail,
                                          CreditLimitRequested = o.CreditLimitRequested,
                                          IsConfirm = o.IsConfirm,
                                         IsApproved = o.IsApproved,
                                         ApprovedAmount = o.ApprovedAmount,
                                         Status = o.Status,
                                         Notes = o.Notes
                                      }
                                  };

            var totalCount = await filteredBusiness.CountAsync();

            return new PagedResultDto<GetAllBusinessDto>(
                totalCount,
                await businessses.ToListAsync()
            );
        }

        public async Task<BusinessDto> GetBusinessForEdit(EntityDto input)
        {
            var business = await _businessRepository.GetAll()
                .Include(e => e.BuisnessStructureFk)
                .Include(e => e.DP_BuisnessDocumentTypeFK)
                .Include(e => e.DP_BuisnessDocumentTypeFK2)
                .Include(e => e.B_StateFK)
                .Include(e => e.DP_StateFK)
                .Include(e => e.DP_StateFK2)
                .Include(e => e.R_StateFK)
                .Where(e => e.Id == input.Id).AsNoTracking().FirstOrDefaultAsync();

            var output = ObjectMapper.Map<BusinessDto>(business);
            //output.BuisnessStructure = business.BuisnessStructureFk.Structure;
            //output.DP_BuisnessDocumentType = business.DP_BuisnessDocumentTypeFK.DocType;
            //output.DP_BuisnessDocumentType2 = business.DP_BuisnessDocumentTypeFK2.DocType;
            //output.DP_State = business.DP_StateFK.Name;
            //output.DP_State2 = business.DP_StateFK2.Name;
            //output.B_State2 = business.B_StateFK2.Name;
            //output.R_State2 = business.R_StateFK2.Name;

            return output;
        }

        public async Task<BusinessDto> GetBusinessForView(int id,string  type)
        {
            var business = await _businessRepository.GetAll()
                .Include(e => e.BuisnessStructureFk)
                .Include(e => e.DP_BuisnessDocumentTypeFK)
                .Include(e => e.DP_BuisnessDocumentTypeFK2)
                .Include(e => e.B_StateFK)
                .Include(e => e.DP_StateFK)
                .Include(e => e.DP_StateFK2)
                .Include(e => e.R_StateFK)
                .WhereIf(type== "Creditapplication",e => e.Id == id)//.AsNoTracking().FirstOrDefaultAsync();
                .WhereIf(type == "Wholesale",e => e.WholeSaleLeadId == id).AsNoTracking().FirstOrDefaultAsync();

            var output =  ObjectMapper.Map<BusinessDto>(business) ;
            output.BuisnessStructure = business.BuisnessStructureId > 0 ? business.BuisnessStructureFk.Structure : "";
            output.DP_BuisnessDocumentType = business.DP_BuisnessDocumentTypeId != null ? business.DP_BuisnessDocumentTypeFK.DocType :"";
            output.DP_BuisnessDocumentType2 = business.DP_BuisnessDocumentTypeId2 != null ? business.DP_BuisnessDocumentTypeFK2.DocType : "";
            output.DP_State = business.DP_StateId != null ? business.DP_StateFK.Name : "";
            output.DP_State2 = business.DP_StateId2 != null ? business.DP_StateFK2.Name : "";
            output.B_State = business.B_StateId != null ? business.B_StateFK.Name : "";
            output.R_State = business.R_StateId != null ? business.R_StateFK.Name : "";
            output.LicencePassportDoc = !string.IsNullOrEmpty(business.LicencePassportDoc) ? ApplicationSettingConsts.ViewDocumentPath + business.LicencePassportDoc : "";
            output.AddressDoc = !string.IsNullOrEmpty(business.AddressDoc) ? ApplicationSettingConsts.ViewDocumentPath + business.AddressDoc : "";
            return output;
        }
    
        public async Task IsAprooveBussiness(int id)
        {
            var business = await _businessRepository.FirstOrDefaultAsync(id);
            business.IsApproved = true;
            await _businessRepository.UpdateAsync(business);

        }

        public async Task ApproveAmount(BusinessDto input)
        {
            var business = await _businessRepository.FirstOrDefaultAsync((int)input.Id);

            ObjectMapper.Map(input, business);
            await _businessRepository.UpdateAsync(business);

            var wholesalelead = await _wholeSaleLeadRepository.FirstOrDefaultAsync((int)business.WholeSaleLeadId);

            wholesalelead.CreditAmount = business.ApprovedAmount;
            await _wholeSaleLeadRepository.UpdateAsync(wholesalelead);
        }

    }
}
