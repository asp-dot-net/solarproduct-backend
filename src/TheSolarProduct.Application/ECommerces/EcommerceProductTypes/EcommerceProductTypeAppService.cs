﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheSolarProduct.ECommerces.EcommerceProductTypes.Dto;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Wholesales.DataVault;
using TheSolarProduct.Wholesales.Ecommerces;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.WholeSaleStatuses.Dtos;


namespace TheSolarProduct.ECommerces.EcommerceProductTypes
{
    public class EcommerceProductTypeAppService : TheSolarProductAppServiceBase, IEcommerceProductTypeAppService
    {
        private readonly IRepository<EcommerceProductType> _ecommerceProductTypeRepository;
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        public EcommerceProductTypeAppService(
            IRepository<EcommerceProductType> ecommerceProductTypeRepository
            , IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository
            , IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _ecommerceProductTypeRepository = ecommerceProductTypeRepository;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task CreateOrEdit(EcommerceProductTypeDto input)
        {
            if (input.Id == null || input.Id == 0)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(EcommerceProductTypeDto input)
        {
            var EcommerceProductType = ObjectMapper.Map<EcommerceProductType>(input);

            await _ecommerceProductTypeRepository.InsertAndGetIdAsync(EcommerceProductType);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 19;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "Ecommerce ProductType Created : " + input.Type;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

        }

        protected virtual async Task Update(EcommerceProductTypeDto input)
        {

            var EcommerceProductType = await _ecommerceProductTypeRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 19;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "Ecommerce ProductType Updated : " + EcommerceProductType.Type;

            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<WholeSaleDataVaultActivityLogHistory>();

            if (input.Type != EcommerceProductType.Type)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Type";
                history.PrevValue = EcommerceProductType.Type;
                history.CurValue = input.Type;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }


            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, EcommerceProductType);

        }

        public async Task Delete(EntityDto input)
        {
            var Name = _ecommerceProductTypeRepository.Get(input.Id).Type;
            await _ecommerceProductTypeRepository.DeleteAsync(input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 19;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "Ecommerce ProductType Deleted : " + Name;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);
        }

        public async Task<PagedResultDto<GetAllEcommerceProductTypeDto>> GetAll(GetEcommerceProductTypeInputDto input)
        {
            var filteredEcommerceProductTypes = _ecommerceProductTypeRepository.GetAll()
                       .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Type.Contains(input.Filter));

            var pagedAndFilteredEcommerceProductTypes = filteredEcommerceProductTypes
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var ecommerceProductTypes = from o in pagedAndFilteredEcommerceProductTypes
                                        select new GetAllEcommerceProductTypeDto()
                              {
                                EcommerceProductType = new EcommerceProductTypeDto()
                                {
                                    Id = o.Id,
                                    Type = o.Type,
                                }
                                  
                              };

            var totalCount = await filteredEcommerceProductTypes.CountAsync();

            return new PagedResultDto<GetAllEcommerceProductTypeDto>(
               totalCount,
               await ecommerceProductTypes.ToListAsync()
           );
        }

        public async Task<EcommerceProductTypeDto> GetLeadSourceForEdit(EntityDto input)
        {
            var ecommerceProductType = await _ecommerceProductTypeRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<EcommerceProductTypeDto>(ecommerceProductType);


            return output;
        }
    }
}
