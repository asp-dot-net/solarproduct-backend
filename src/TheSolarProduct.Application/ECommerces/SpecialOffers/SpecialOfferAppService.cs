﻿using TheSolarProduct.Jobs;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Linq.Extensions;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Dtos;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Storage;
using TheSolarProduct.Common;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct.ECommerces.EcommerceProductItems.Dtos;
using TheSolarProduct.MultiTenancy;
using NPOI.HPSF;
using TheSolarProduct.ApplicationSettings;
using System.Windows.Forms.Design;
using TheSolarProduct.ECommerces.SpecialOffers.Dtos;
using Abp.Timing.Timezone;
using TheSolarProduct.ECommerces.BrandingPartners.Dto;
using TheSolarProduct.Authorization.Users;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Wholesales.DataVault;

namespace TheSolarProduct.ECommerces.SpecialOffers
{
    public class SpecialOfferAppService : TheSolarProductAppServiceBase, ISpecialOfferAppService
    {
        private readonly IRepository<SpecialOffer> _specialOfferRepository;
        private readonly IRepository<SpecialOfferProduct> _specialOfferProductRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<EcommerceProductItem> _ecommerceProductItemRepository;
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public SpecialOfferAppService(
            IRepository<SpecialOffer> specialOfferRepository,
            IRepository<SpecialOfferProduct> specialOfferProductRepository,
            ITimeZoneConverter timeZoneConverter,
            IRepository<User, long> userRepository,
            IRepository<EcommerceProductItem> ecommerceProductItemRepository,
            IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            ) 
        {
            _specialOfferRepository = specialOfferRepository;
            _specialOfferProductRepository = specialOfferProductRepository;
            _timeZoneConverter = timeZoneConverter;
            _userRepository = userRepository;
            _ecommerceProductItemRepository = ecommerceProductItemRepository;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task CreateOrEdit(SpecialOfferDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(SpecialOfferDto input)
        {
            input.DealStarts = (_timeZoneConverter.Convert(input.DealStarts, (int)AbpSession.TenantId));
            input.DealEnds = (_timeZoneConverter.Convert(input.DealEnds, (int)AbpSession.TenantId));
            var specialOffer = ObjectMapper.Map<SpecialOffer>(input);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 13;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "Special Offer Created : " + input.OfferTitle;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

            var specialOfferId = await _specialOfferRepository.InsertAndGetIdAsync(specialOffer);

            if (input.specialOfferProducts != null)
            {
                foreach (var item in input.specialOfferProducts)
                {
                    var specialOfferProduct = ObjectMapper.Map<SpecialOfferProduct>(item);
                    specialOfferProduct.SpecialOfferId = specialOfferId;

                    await _specialOfferProductRepository.InsertAndGetIdAsync(specialOfferProduct);
                }
            }
           
        }

        protected virtual async Task Update(SpecialOfferDto input)
        {

            var specialOffer = await _specialOfferRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 13;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "Special Offer Updated : " + specialOffer.OfferTitle;

            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<WholeSaleDataVaultActivityLogHistory>();

            if (input.OfferTitle != specialOffer.OfferTitle)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "OfferTitle";
                history.PrevValue = specialOffer.OfferTitle;
                history.CurValue = input.OfferTitle;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.DealStarts != specialOffer.DealStarts)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "DealStarts";
                history.PrevValue = specialOffer.DealStarts.ToString();
                history.CurValue = input.DealStarts.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.DealEnds != specialOffer.DealEnds)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "DealEnds";
                history.PrevValue = specialOffer.DealEnds.ToString();
                history.CurValue = input.DealEnds.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsOfferActive != specialOffer.IsOfferActive)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "IsOfferActive";
                history.PrevValue = specialOffer.IsOfferActive.ToString();
                history.CurValue = input.IsOfferActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            
            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, specialOffer);


            await _specialOfferProductRepository.HardDeleteAsync(e => e.SpecialOfferId == input.Id);
            foreach (var item in input.specialOfferProducts)
            {
                var specialOfferProduct = ObjectMapper.Map<SpecialOfferProduct>(item);
                specialOfferProduct.SpecialOfferId = specialOffer.Id;

                await _specialOfferProductRepository.InsertAndGetIdAsync(specialOfferProduct);
            }
        }

        public async Task Delete(EntityDto input)
        {
            var Name = _specialOfferRepository.Get(input.Id).OfferTitle;
            await _specialOfferRepository.DeleteAsync(input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 13;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "Special Offer Deleted : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

            await _specialOfferProductRepository.DeleteAsync(e => e.SpecialOfferId == input.Id);
        }

        public async Task<PagedResultDto<GetAllSpecialOfferDto>> GetAll(GetSpecialOfferInputDto input)
        {
            var filteredSpecialOffer = _specialOfferRepository.GetAll().AsNoTracking()
                         .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.OfferTitle.Contains(input.Filter));

            var User_List = _userRepository.GetAll().AsNoTracking();

            var pagedAndFilteredSpecialOffer = filteredSpecialOffer
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var specialOffer = from o in pagedAndFilteredSpecialOffer
                               let products = _specialOfferProductRepository.GetAll().AsNoTracking().Where(e => e.SpecialOfferId == o.Id)
                            select new GetAllSpecialOfferDto()
                            {
                                SpecialOffer = new SpecialOfferDto
                                {
                                    Id = o.Id,
                                    OfferTitle = o.OfferTitle,
                                    DealStarts = o.DealStarts,
                                    DealEnds = o.DealEnds,
                                    OfferCreatedBy = User_List.Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                                    OfferCreatedDate = o.CreationTime,
                                    IsOfferActive = o.IsOfferActive,
                                    ProductCount = products.Count()
                                }
                            };

            var totalCount = await filteredSpecialOffer.CountAsync();

            return new PagedResultDto<GetAllSpecialOfferDto>(
                totalCount,
                await specialOffer.ToListAsync()
            );
        }

        public async Task<SpecialOfferDto> GetSpecialOfferForEdit(EntityDto input)
        {
            var specialOffer = await _specialOfferRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<SpecialOfferDto>(specialOffer);

            var specialOfferProducts = await _specialOfferProductRepository.GetAll().AsNoTracking()
                                    .Include(e => e.ProductItemFK).Include(e=> e.ProductItemFK.ProductItemFk).Include(e => e.ProductItemFK.ProductItemFk.ProductTypeFk).Include(e => e.ProductItemFK.BrandPartnerFK)
                                    .Where(e => e.SpecialOfferId == output.Id).Select(e => new {e.Id,e.SpecialOfferId,e.ProductItemId, proname= e.ProductItemFK.ProductItemFk.Name, e.NewPrice, e.OldPrice, type = e.ProductItemFK.ProductItemFk.ProductTypeFk.Name, e.ProductItemFK.BrandPartnerFK.BrandName }).ToListAsync();
            output.specialOfferProducts = (from p in specialOfferProducts
                                          select new SpecialOfferProductDto()
                                          {
                                            Id = p.Id,
                                            SpecialOfferId = p.SpecialOfferId,
                                            ProductItemId = p.ProductItemId,
                                            ProductName = p.proname,
                                            NewPrice = p.NewPrice,
                                            OldPrice = p.OldPrice,
                                            ProductCategory = p.type,
                                            Brand = p.BrandName

                                          }).ToList();

            return output;

        }

        public async Task<List<GetEcommerceProductsForDropdown>> GetEcommerceProductsForDropdown(string Query)
        {
            var product = await _ecommerceProductItemRepository.GetAll()
                                    .Include(e => e.ProductItemFk).Include(e => e.ProductItemFk.ProductTypeFk).Include(e => e.BrandPartnerFK)
                                    .Where(e => e.IsActive == true && e.ProductItemFk.Name.Contains(Query)).AsNoTracking()
                                    .Select(e => new {e.Id, e.ProductItemFk.Name, e.ProductItemFk.Amount, e.BrandPartnerFK.BrandName,type = e.ProductItemFk.ProductTypeFk.Name}).ToListAsync();

            var output = (from o in product
                          select new GetEcommerceProductsForDropdown
                          {
                              Id= o.Id,
                              ProductName = o.Name,
                              Price = o.Amount != null ? o.Amount : 0,
                              Brand = o.BrandName,
                              ProductCategory = o.type
                          }
                          ).ToList();

            return output;
        }
    }
}
