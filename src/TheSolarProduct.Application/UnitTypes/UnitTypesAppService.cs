﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.UnitTypes.Exporting;
using TheSolarProduct.UnitTypes.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.StreetNames;
using PayPalCheckoutSdk.Orders;
using TheSolarProduct.CheckActives;

namespace TheSolarProduct.UnitTypes
{
	[AbpAuthorize(AppPermissions.Pages_UnitTypes)]
    public class UnitTypesAppService : TheSolarProductAppServiceBase, IUnitTypesAppService
    {
		 private readonly IRepository<UnitType> _unitTypeRepository;
		 private readonly IUnitTypesExcelExporter _unitTypesExcelExporter;
         private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
         private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;


        public UnitTypesAppService(IRepository<UnitType> unitTypeRepository, IUnitTypesExcelExporter unitTypesExcelExporter ,
            IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
             IDbContextProvider<TheSolarProductDbContext> dbcontextprovider) 
		  {
			_unitTypeRepository = unitTypeRepository;
			_unitTypesExcelExporter = unitTypesExcelExporter;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }

		 public async Task<PagedResultDto<GetUnitTypeForViewDto>> GetAll(GetAllUnitTypesInput input)
         {
			
			var filteredUnitTypes = _unitTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter);

			var pagedAndFilteredUnitTypes = filteredUnitTypes
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

			var unitTypes = from o in pagedAndFilteredUnitTypes
                         select new GetUnitTypeForViewDto() {
							UnitType = new UnitTypeDto
							{
                                Name = o.Name,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						};

            var totalCount = await filteredUnitTypes.CountAsync();

            return new PagedResultDto<GetUnitTypeForViewDto>(
                totalCount,
                await unitTypes.ToListAsync()
            );
         }
		 
		 public async Task<GetUnitTypeForViewDto> GetUnitTypeForView(int id)
         {
            var unitType = await _unitTypeRepository.GetAsync(id);

            var output = new GetUnitTypeForViewDto { UnitType = ObjectMapper.Map<UnitTypeDto>(unitType) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_UnitTypes_Edit)]
		 public async Task<GetUnitTypeForEditOutput> GetUnitTypeForEdit(EntityDto input)
         {
            var unitType = await _unitTypeRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetUnitTypeForEditOutput {UnitType = ObjectMapper.Map<CreateOrEditUnitTypeDto>(unitType)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditUnitTypeDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_UnitTypes_Create)]
		 protected virtual async Task Create(CreateOrEditUnitTypeDto input)
         {
            var unitType = ObjectMapper.Map<UnitType>(input);
			unitType.Name = unitType.Name.ToUpper();

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 75;
            dataVaultLog.ActionNote = "UnitType Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _unitTypeRepository.InsertAsync(unitType);
         }

		 [AbpAuthorize(AppPermissions.Pages_UnitTypes_Edit)]
		 protected virtual async Task Update(CreateOrEditUnitTypeDto input)
         {
            var unitType = await _unitTypeRepository.FirstOrDefaultAsync((int)input.Id);
			unitType.Name = unitType.Name.ToUpper();

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 75;
            dataVaultLog.ActionNote = "UnitType Updated : " + unitType.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != unitType.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = unitType.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != unitType.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = unitType.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, unitType);
         }

		 [AbpAuthorize(AppPermissions.Pages_UnitTypes_Delete)]
         public async Task Delete(EntityDto input)
        {
            var Name = _unitTypeRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 75;
            dataVaultLog.ActionNote = "UnitType Deleted : " + Name;
            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _unitTypeRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetUnitTypesToExcel(GetAllUnitTypesForExcelInput input)
         {
			
			var filteredUnitTypes = _unitTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter);

			var query = (from o in filteredUnitTypes
                         select new GetUnitTypeForViewDto() { 
							UnitType = new UnitTypeDto
							{
                                Name = o.Name,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						 });


            var unitTypeListDtos = await query.ToListAsync();

            return _unitTypesExcelExporter.ExportToFile(unitTypeListDtos);
         }


    }
}