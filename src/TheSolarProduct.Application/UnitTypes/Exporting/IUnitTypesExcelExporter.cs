﻿using System.Collections.Generic;
using TheSolarProduct.UnitTypes.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.UnitTypes.Exporting
{
    public interface IUnitTypesExcelExporter
    {
        FileDto ExportToFile(List<GetUnitTypeForViewDto> unitTypes);
    }
}