﻿using TheSolarProduct.Jobs;
using TheSolarProduct.Leads;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Dtos;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Installation.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Jobs.Exporting;
using System.Collections.Generic;
using TheSolarProduct.Authorization.Users;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.LeadActivityLogs;
using Abp.Authorization;
using Abp.Authorization.Users;
using TheSolarProduct.Installer;
using Telerik.Reporting;

using Stripe;
using TheSolarProduct.Authorization;

namespace TheSolarProduct.Installation
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class InstallationAppService : TheSolarProductAppServiceBase, IInstallationAppService
    {
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<JobType, int> _lookup_jobTypeRepository;
        private readonly IRepository<JobStatus, int> _lookup_jobStatusRepository;
        private readonly IRepository<RoofType, int> _lookup_roofTypeRepository;
        private readonly IRepository<ElecDistributor, int> _lookup_elecDistributorRepository;
        private readonly IRepository<Lead, int> _lookup_leadRepository;
        private readonly IRepository<ElecRetailer, int> _lookup_elecRetailerRepository;
        private readonly IRepository<PaymentOption, int> _lookup_paymentOptionRepository;
        private readonly IRepository<DepositOption, int> _lookup_depositOptionRepository;
        private readonly IRepository<MeterUpgrade, int> _lookup_meterUpgradeRepository;
        private readonly IRepository<MeterPhase, int> _lookup_meterPhaseRepository;
        private readonly IRepository<PromotionOffer, int> _lookup_promotionOfferRepository;
        private readonly IRepository<HouseType, int> _lookup_houseTypeRepository;
        private readonly IRepository<FinanceOption, int> _lookup_financeOptionRepository;
        private readonly IRepository<RoofAngle, int> _lookup_roofAngleRepository;
        private readonly IJobsExcelExporter _jobsExcelExporter;
        private readonly IRepository<ProductItem> _productItemRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IAbpSession _abpSession;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<ProductItem, int> _lookup_productItemRepository;
        private readonly IJobProductItemsAppService _jobProductItemsAppServiceAppService;
        private readonly UserManager _userManager;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly PermissionManager _PermissionRepository;
        private readonly IRepository<UserPermissionSetting, long> _userPermissionRepository;
        private readonly IRepository<InstallerDetail> _installerDetailRepository;
        private readonly IRepository<Warehouselocation> _warehouselocationRepository;

        public InstallationAppService(IRepository<Job> jobRepository
          , IRepository<JobType, int> lookup_jobTypeRepository
            , IRepository<JobStatus, int> lookup_jobStatusRepository
            , IRepository<RoofType, int> lookup_roofTypeRepository
             , IRepository<ElecDistributor, int> lookup_elecDistributorRepository
            , IRepository<Lead, int> lookup_leadRepository
            , IRepository<ElecRetailer, int> lookup_elecRetailerRepository
             , IRepository<PaymentOption, int> lookup_paymentOptionRepository
            , IRepository<DepositOption, int> lookup_depositOptionRepository
            , IRepository<MeterUpgrade, int> lookup_meterUpgradeRepository
            , IRepository<MeterPhase, int> lookup_meterPhaseRepository
            , IRepository<PromotionOffer, int> lookup_promotionOfferRepository
            , IRepository<HouseType, int> lookup_houseTypeRepository
            , IRepository<FinanceOption, int> lookup_financeOptionRepository
             , IRepository<RoofAngle, int> lookup_roofAngleRepository,
            IJobsExcelExporter jobsExcelExporter,
            IRepository<User, long> userRepository,
            IRepository<Lead> leadRepository,
            IRepository<ProductItem> productItemRepository,
            IRepository<JobProductItem> jobProductItemRepository,
            IAbpSession abpSession,
            ITimeZoneConverter timeZoneConverter,
            IRepository<ProductItem, int> lookup_productItemRepository,
            IJobProductItemsAppService jobProductItemsAppServiceAppService,
            UserManager userManager,
            IRepository<UserTeam> userTeamRepository,
            IRepository<LeadActivityLog> leadactivityRepository,
            PermissionManager PermissionRepository,
            IRepository<UserPermissionSetting, long> userPermissionRepository,
            IRepository<InstallerDetail> installerDetailRepository
            , IRepository<Warehouselocation> warehouselocationRepository
            )
        {
            _jobRepository = jobRepository;
            _lookup_jobTypeRepository = lookup_jobTypeRepository;
            _lookup_jobStatusRepository = lookup_jobStatusRepository;
            _lookup_roofTypeRepository = lookup_roofTypeRepository;
            _lookup_elecDistributorRepository = lookup_elecDistributorRepository;
            _lookup_leadRepository = lookup_leadRepository;
            _lookup_elecRetailerRepository = lookup_elecRetailerRepository;
            _lookup_paymentOptionRepository = lookup_paymentOptionRepository;
            _lookup_depositOptionRepository = lookup_depositOptionRepository;
            _lookup_meterUpgradeRepository = lookup_meterUpgradeRepository;
            _lookup_meterPhaseRepository = lookup_meterPhaseRepository;
            _lookup_promotionOfferRepository = lookup_promotionOfferRepository;
            _lookup_houseTypeRepository = lookup_houseTypeRepository;
            _lookup_financeOptionRepository = lookup_financeOptionRepository;
            _lookup_roofAngleRepository = lookup_roofAngleRepository;
            _jobsExcelExporter = jobsExcelExporter;
            _userRepository = userRepository;
            _leadRepository = leadRepository;
            _productItemRepository = productItemRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _abpSession = abpSession;
            _timeZoneConverter = timeZoneConverter;
            _lookup_productItemRepository = lookup_productItemRepository;
            _jobProductItemsAppServiceAppService = jobProductItemsAppServiceAppService;
            _userManager = userManager;
            _userTeamRepository = userTeamRepository;
            _leadactivityRepository = leadactivityRepository;
            _PermissionRepository = PermissionRepository;
            _installerDetailRepository = installerDetailRepository;
            _userPermissionRepository = userPermissionRepository;
            _warehouselocationRepository = warehouselocationRepository;
        }
        public async Task<PagedResultDto<GetJobForViewDto>> GetPendingInstallationList(GetAllInstallationInput input)
        {
            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var leadactive_list = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 12 || e.SectionId == 11) && e.LeadFk.OrganizationId == input.OrganizationUnit)
                .AsNoTracking().Select(e => new { e.Id, e.LeadId, e.ActionId, e.ActivityNote, e.ActivityDate });

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var PanelDetails = new List<int?>();
            if (input.Panelmodel != null)
            {
                var Panel = await _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Model.Contains(input.Panelmodel)).Select(e => e.Id).FirstOrDefaultAsync();
                PanelDetails = await _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Panel).Select(e => e.JobId).ToListAsync();
            }

            var InvertDetails = new List<int?>();
            if (input.Invertmodel != null)
            {
                var Inverter = await _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Model.Contains(input.Invertmodel)).Select(e => e.Id).FirstOrDefaultAsync();
                InvertDetails = await _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Inverter).Select(e => e.JobId).ToListAsync();
            }

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobTypeFk)
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.LeadFk)
                        .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit)
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "LotNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LotNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
                        .WhereIf(input.Paymenttypeid != 0, e => e.PaymentOptionId == input.Paymenttypeid)
                        .WhereIf(input.HousetypeId != 0, e => e.HouseTypeId == input.HousetypeId)
                        .WhereIf(input.Rooftypeid != 0, e => e.RoofTypeId == input.Rooftypeid)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.PostalCode, input.PostalCodeFrom) >= 0)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.PostalCode, input.PostalCodeTo) <= 0)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.Panelmodel), e => PanelDetails.Contains(e.Id))
                       .WhereIf(!string.IsNullOrWhiteSpace(input.Invertmodel), e => InvertDetails.Contains(e.Id))
                       .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.SteetAddressFilter), e => e.LeadFk.Address == input.SteetAddressFilter)

                       .WhereIf(input.DateFilterType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                       .WhereIf(input.DateFilterType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                       .WhereIf(input.DateFilterType == "Active" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
                       .WhereIf(input.DateFilterType == "Active" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date)

                       .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                       .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                       .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                       .WhereIf(input.jobTypeId != null && input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
                       .WhereIf(input.JobAssignUser != 0, e => e.BookingManagerId == input.JobAssignUser)
                       .WhereIf(input.AssignJob == 1, e => e.BookingManagerId != 0 && e.BookingManagerId != null)
                       .WhereIf(input.AssignJob == 2, e => (e.BookingManagerId == 0 || e.BookingManagerId == null))
                       .WhereIf((input.status == 0), e => e.JobStatusId == 4 || e.JobStatusId == 5)
                       .WhereIf((input.status == 3), e => e.JobStatusId == 2 && e.DepositeRecceivedDate != null)
                       .WhereIf((input.status != 0 && input.status != 3), e => e.JobStatusId == input.status)
                       .AsNoTracking()
                       .Select(e => new { e.Id, e.LeadId, e.LeadFk, e.JobStatusFk, e.JobTypeFk, e.JobNumber, e.ElecDistributorId, e.ElecRetailerId, e.RegPlanNo, e.LotNumber, e.Suburb, e.State, e.UnitNo, e.UnitType, e.NMINumber, e.ApplicationRefNo, e.DistAppliedDate, e.Note, e.InstallerNotes, e.MeterNumber, e.OldSystemDetails, e.PostalCode, e.StreetName, e.StreetNo, e.StreetType, e.JobStatusId, e.JobTypeId, e.Address, e.BookingManagerId, e.JobAssignDate, e.HouseTypeFk, e.RoofTypeFk })
                       ;

            var pagedAndFilteredJobs = filteredJobs
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var jobs = from o in pagedAndFilteredJobs

                       join o5 in _lookup_elecDistributorRepository.GetAll().AsNoTracking() on o.ElecDistributorId equals o5.Id into j5
                       from s5 in j5.DefaultIfEmpty()

                       //join o6 in _lookup_leadRepository.GetAll().AsNoTracking() on o.LeadId equals o6.Id into j6
                       //from s6 in j6.DefaultIfEmpty()         

                       join o7 in _lookup_elecRetailerRepository.GetAll().AsNoTracking() on o.ElecRetailerId equals o7.Id into j7
                       from s7 in j7.DefaultIfEmpty()

                       select new GetJobForViewDto()
                       {
                           Job = new JobDto
                           {
                               RegPlanNo = o.RegPlanNo,
                               LotNumber = o.LotNumber,
                               Suburb = o.Suburb,
                               State = o.State,
                               UnitNo = o.UnitNo,
                               UnitType = o.UnitType,
                               NMINumber = o.NMINumber,
                               Id = o.Id,
                               ApplicationRefNo = o.ApplicationRefNo,
                               DistAppliedDate = o.DistAppliedDate,
                               Notes = o.Note,
                               InstallerNotes = o.InstallerNotes,
                               JobNumber = o.JobNumber,
                               MeterNumber = o.MeterNumber,
                               LeadId = o.LeadId,
                               OldSystemDetails = o.OldSystemDetails,
                               PostalCode = o.PostalCode,
                               StreetName = o.StreetName,
                               StreetNo = o.StreetNo,
                               StreetType = o.StreetType,
                               ElecDistributorId = o.ElecDistributorId,
                               ElecRetailerId = o.ElecRetailerId,
                               JobStatusId = o.JobStatusId,
                               JobTypeId = o.JobTypeId,
                               Address = o.Address,

                           },
                           JobTypeName = o.JobTypeFk.Name,
                           JobStatusName = o.JobStatusFk.Name,
                           JobStatusColorClass = o.JobStatusFk.ColorClass,
                           ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name,
                           LeadCompanyName = o.LeadFk.CompanyName,
                           Email = o.LeadFk.Email,
                           Mobile = o.LeadFk.Mobile,
                           ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name,
                           JobAssignemployee = User_List.Where(e => e.Id == o.BookingManagerId).Select(e => e.FullName).FirstOrDefault(),
                           JobAssignDate = o.JobAssignDate,
                           HouseType = o.HouseTypeFk.Name,
                           RoofType = o.RoofTypeFk.Name,
                         
                           ActivityReminderTime = leadactive_list.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                           ActivityDescription = leadactive_list.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           ActivityComment = leadactive_list.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                           WarehouseDistance = getDistanceFromLatLonInKm(o.LeadFk.latitude, o.LeadFk.longitude, o.LeadFk.State)
                       };
            var totalCount = await filteredJobs.CountAsync();
            return new PagedResultDto<GetJobForViewDto>(
                totalCount,
                await jobs.ToListAsync()
            );
        }

        public async Task<GetJobForViewDto> GetPendingInstallationcount(GetAllInstallationInput input)
        {
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var PanelDetails = new List<int?>();
            if (input.Panelmodel != null)
            {
                var Panel = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Model.Contains(input.Panelmodel)).Select(e => e.Id).FirstOrDefault();
                PanelDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Panel).Select(e => e.JobId).ToList();
            }

            var InvertDetails = new List<int?>();
            if (input.Invertmodel != null)
            {
                var Inverter = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Model.Contains(input.Invertmodel)).Select(e => e.Id).FirstOrDefault();
                InvertDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Inverter).Select(e => e.JobId).ToList();
            }

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobTypeFk)
                        //.Include(e => e.JobStatusFk)
                        //.Include(e => e.RoofTypeFk)
                        //.Include(e => e.RoofAngleFk)
                        //.Include(e => e.ElecDistributorFk)
                        //.Include(e => e.LeadFk)
                        //.Include(e => e.ElecRetailerFk)
                        //.Include(e => e.PaymentOptionFk)
                        //.Include(e => e.DepositOptionFk)
                        //.Include(e => e.PromotionOfferFk)
                        //.Include(e => e.HouseTypeFk)
                        //.Include(e => e.FinanceOptionFk)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.JobNumber.Contains(input.Filter) || e.LotNumber.Contains(input.Filter) || e.PeakMeterNo.Contains(input.Filter) || e.OffPeakMeter.Contains(input.Filter) || e.Suburb.Contains(input.Filter) || e.State.Contains(input.Filter) || e.PostalCode.Contains(input.Filter) || e.UnitNo.Contains(input.Filter) || e.UnitType.Contains(input.Filter) || e.StreetNo.Contains(input.Filter) || e.StreetName.Contains(input.Filter) || e.StreetType.Contains(input.Filter) || e.Latitude.Contains(input.Filter) || e.Longitude.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Country.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "LotNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LotNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
                        .WhereIf(input.Paymenttypeid != 0, e => e.PaymentOptionId == input.Paymenttypeid)
                        .WhereIf(input.HousetypeId != 0, e => e.HouseTypeId == input.HousetypeId)
                        .WhereIf(input.Rooftypeid != 0, e => e.RoofTypeId == input.Rooftypeid)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.PostalCode, input.PostalCodeFrom) >= 0)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.PostalCode, input.PostalCodeTo) <= 0)
                       /////e => e.PostalCode >= input.PostalCodeFrom && e.PostalCode <= input.PostalCodeTo)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.Panelmodel), e => PanelDetails.Contains(e.Id))
                       .WhereIf(!string.IsNullOrWhiteSpace(input.Invertmodel), e => InvertDetails.Contains(e.Id))
                       .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.SteetAddressFilter), e => e.LeadFk.Address == input.SteetAddressFilter)
                       .WhereIf(input.DateFilterType == "Active" && input.StartDate != null && input.EndDate != null, e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
                       .WhereIf(input.DateFilterType == "DepositeReceived" && input.StartDate != null && input.EndDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                       .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                       .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                       .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                       .WhereIf(input.jobTypeId != null && input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
                       .WhereIf(input.JobAssignUser != 0, e => e.BookingManagerId == input.JobAssignUser)
                       .WhereIf(input.AssignJob == 1, e => e.BookingManagerId != 0 && e.BookingManagerId != null)
                       .WhereIf(input.AssignJob == 2, e => (e.BookingManagerId == 0 || e.BookingManagerId == null))
                       .WhereIf((input.status == 0), e => e.JobStatusId == 4 || e.JobStatusId == 5)
                       .WhereIf((input.status == 3), e => e.JobStatusId == 2 && e.DepositeRecceivedDate != null)
                       .WhereIf((input.status != 0 && input.status != 3), e => e.JobStatusId == input.status)
                       .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);


            var output = new GetJobForViewDto();

            output.TotalData = filteredJobs.Where(e => e.JobStatusId == 4 || e.JobStatusId == 5).Select(e => e.Id).Count();
            output.TotalDepRcvData = filteredJobs.Where(e => e.JobStatusId == 4).Select(e => e.Id).Count();
            output.TotalActiveData = filteredJobs.Where(e => e.JobStatusId == 5).Select(e => e.Id).Count();
            output.TotalJobBookedData = filteredJobs.Where(e => e.JobStatusId == 6).Select(e => e.Id).Count();

            return output;
        }
        public async Task<PagedResultDto<GetJobForViewDto>> GetJobBookingInstallationList(GetAllInstallationInput input)
        {
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var PanelDetails = new List<int?>();
            if (input.Panelmodel != null)
            {
                var Panel = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Model.Contains(input.Panelmodel)).Select(e => e.Id).FirstOrDefault();
                PanelDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Panel).Select(e => e.JobId).ToList();
            }

            var InvertDetails = new List<int?>();
            if (input.Invertmodel != null)
            {
                var Inverter = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Model.Contains(input.Invertmodel)).Select(e => e.Id).FirstOrDefault();
                InvertDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Inverter).Select(e => e.JobId).ToList();
            }
            var filteredJobs = _jobRepository.GetAll()
                          .Include(e => e.JobTypeFk)
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.RoofTypeFk)
                        .Include(e => e.RoofAngleFk)
                        .Include(e => e.ElecDistributorFk)
                        .Include(e => e.LeadFk)
                        .Include(e => e.ElecRetailerFk)
                        .Include(e => e.PaymentOptionFk)
                        .Include(e => e.DepositOptionFk)
                        .Include(e => e.PromotionOfferFk)
                        .Include(e => e.HouseTypeFk)
                        .Include(e => e.FinanceOptionFk)
                         //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.JobNumber.Contains(input.Filter) || e.LotNumber.Contains(input.Filter) || e.PeakMeterNo.Contains(input.Filter) || e.OffPeakMeter.Contains(input.Filter) || e.Suburb.Contains(input.Filter) || e.State.Contains(input.Filter) || e.PostalCode.Contains(input.Filter) || e.UnitNo.Contains(input.Filter) || e.UnitType.Contains(input.Filter) || e.StreetNo.Contains(input.Filter) || e.StreetName.Contains(input.Filter) || e.StreetType.Contains(input.Filter) || e.Latitude.Contains(input.Filter) || e.Longitude.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Country.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                         .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                         .WhereIf(input.FilterName == "LotNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LotNumber == input.Filter)
                         .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                         .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Phone == input.Filter)
                         .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                         .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
                        .WhereIf(input.Paymenttypeid != 0, e => e.PaymentOptionId == input.Paymenttypeid)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.PostalCode, input.PostalCodeFrom) >= 0)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.PostalCode, input.PostalCodeTo) <= 0)
                        .WhereIf(input.HousetypeId != 0, e => e.HouseTypeId == input.HousetypeId)
                        .WhereIf(input.Rooftypeid != 0, e => e.RoofTypeId == input.Rooftypeid)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.Panelmodel), e => PanelDetails.Contains(e.Id))
                       .WhereIf(!string.IsNullOrWhiteSpace(input.Invertmodel), e => InvertDetails.Contains(e.Id))
                       .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
                       .WhereIf(!string.IsNullOrWhiteSpace(input.SteetAddressFilter), e => e.LeadFk.Address == input.SteetAddressFilter)
                       .WhereIf(input.DateFilterType == "Active" && input.StartDate != null && input.EndDate != null, e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
                       .WhereIf(input.DateFilterType == "DepositeReceived" && input.StartDate != null && input.EndDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                       .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                       .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                       .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                       .WhereIf(input.jobTypeId != null && input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
                       .WhereIf((input.status == 0), e => e.JobStatusId == 4 || e.JobStatusId == 5)
                       .WhereIf((input.status == 3), e => e.JobStatusId == 2 && e.DepositeRecceivedDate != null)
                       .WhereIf((input.status != 0), e => e.JobStatusId == input.status)
                       .Where(e => e.JobStatusId <= 6 && e.BookingManagerId == AbpSession.UserId)
                       .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);



            var pagedAndFilteredJobs = filteredJobs
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var jobs = from o in pagedAndFilteredJobs
                       join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                       from s1 in j1.DefaultIfEmpty()

                       join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                       from s2 in j2.DefaultIfEmpty()

                       join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                       from s5 in j5.DefaultIfEmpty()

                       join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                       from s6 in j6.DefaultIfEmpty()

                       join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                       from s7 in j7.DefaultIfEmpty()

                       select new GetJobForViewDto()
                       {
                           Job = new JobDto
                           {
                               RegPlanNo = o.RegPlanNo,
                               LotNumber = o.LotNumber,
                               Suburb = o.Suburb,
                               State = o.State,
                               UnitNo = o.UnitNo,
                               UnitType = o.UnitType,
                               NMINumber = o.NMINumber,
                               Id = o.Id,
                               ApplicationRefNo = o.ApplicationRefNo,
                               DistAppliedDate = o.DistAppliedDate,
                               Notes = o.Note,
                               InstallerNotes = o.InstallerNotes,
                               JobNumber = o.JobNumber,
                               MeterNumber = o.MeterNumber,
                               LeadId = o.LeadId,
                               OldSystemDetails = o.OldSystemDetails,
                               PostalCode = o.PostalCode,
                               StreetName = o.StreetName,
                               StreetNo = o.StreetNo,
                               StreetType = o.StreetType,
                               Address = o.Address,
                               ElecDistributorId = o.ElecDistributorId,
                               ElecRetailerId = o.ElecRetailerId,
                               JobStatusId = o.JobStatusId,
                               JobTypeId = o.JobTypeId
                           },

                           JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                           JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                           ElecDistributorName =  s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                           LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                           Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                           Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                           ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
                           ActivityReminderTime = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 12 || e.SectionId == 11) && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                           ActivityDescription = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 12 || e.SectionId == 11) && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           ActivityComment = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 12 || e.SectionId == 11) && e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           
                           WarehouseDistance = getDistanceFromLatLonInKm(o.LeadFk.latitude, o.LeadFk.longitude, o.LeadFk.State)
                       };

            var totalCount = await filteredJobs.CountAsync();

            return new PagedResultDto<GetJobForViewDto>(
                totalCount,
                await jobs.ToListAsync()
            );
        }

        //public async Task<FileDto> GetJobsToExcel(GetAllJobsForExcelInput input)
        //{

        //    var filteredJobs = _jobRepository.GetAll()
        //                .Include(e => e.JobTypeFk)
        //                .Include(e => e.JobStatusFk)
        //                .Include(e => e.RoofTypeFk)
        //                .Include(e => e.RoofAngleFk)
        //                .Include(e => e.ElecDistributorFk)
        //                .Include(e => e.LeadFk)
        //                .Include(e => e.ElecRetailerFk)
        //                .Include(e => e.PaymentOptionFk)
        //                .Include(e => e.DepositOptionFk)
        //                //.Include(e => e.MeterUpgradeFk)
        //                //.Include(e => e.MeterPhaseFk)
        //                .Include(e => e.PromotionOfferFk)
        //                .Include(e => e.HouseTypeFk)
        //                .Include(e => e.FinanceOptionFk)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) || e.LotNumber.Contains(input.Filter) || e.PeakMeterNo.Contains(input.Filter) || e.OffPeakMeter.Contains(input.Filter) || e.Suburb.Contains(input.Filter) || e.State.Contains(input.Filter) || e.PostalCode.Contains(input.Filter) || e.UnitNo.Contains(input.Filter) || e.UnitType.Contains(input.Filter) || e.StreetNo.Contains(input.Filter) || e.StreetName.Contains(input.Filter) || e.StreetType.Contains(input.Filter) || e.Latitude.Contains(input.Filter) || e.Longitude.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Country.Contains(input.Filter))
        //                .WhereIf(input.MinPanelOnFlatFilter != null, e => e.PanelOnFlat >= input.MinPanelOnFlatFilter)
        //                .WhereIf(input.MaxPanelOnFlatFilter != null, e => e.PanelOnFlat <= input.MaxPanelOnFlatFilter)
        //                .WhereIf(input.MinPanelOnPitchedFilter != null, e => e.PanelOnPitched >= input.MinPanelOnPitchedFilter)
        //                .WhereIf(input.MaxPanelOnPitchedFilter != null, e => e.PanelOnPitched <= input.MaxPanelOnPitchedFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.RegPlanNoFilter), e => e.RegPlanNo == input.RegPlanNoFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.LotNumberFilter), e => e.LotNumber == input.LotNumberFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.PeakMeterNoFilter), e => e.PeakMeterNo == input.PeakMeterNoFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.OffPeakMeterFilter), e => e.OffPeakMeter == input.OffPeakMeterFilter)
        //                .WhereIf(input.EnoughMeterSpaceFilter > -1, e => (input.EnoughMeterSpaceFilter == 1 && e.EnoughMeterSpace) || (input.EnoughMeterSpaceFilter == 0 && !e.EnoughMeterSpace))
        //                .WhereIf(input.IsSystemOffPeakFilter > -1, e => (input.IsSystemOffPeakFilter == 1 && e.IsSystemOffPeak) || (input.IsSystemOffPeakFilter == 0 && !e.IsSystemOffPeak))
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.RoofTypeNameFilter), e => e.RoofTypeFk != null && e.RoofTypeFk.Name == input.RoofTypeNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.RoofAngleNameFilter), e => e.RoofAngleFk != null && e.RoofAngleFk.Name == input.RoofAngleNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.ElecDistributorNameFilter), e => e.ElecDistributorFk != null && e.ElecDistributorFk.Name == input.ElecDistributorNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.ElecRetailerNameFilter), e => e.ElecRetailerFk != null && e.ElecRetailerFk.Name == input.ElecRetailerNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentOptionNameFilter), e => e.PaymentOptionFk != null && e.PaymentOptionFk.Name == input.PaymentOptionNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.DepositOptionNameFilter), e => e.DepositOptionFk != null && e.DepositOptionFk.Name == input.DepositOptionNameFilter)
        //                //.WhereIf(!string.IsNullOrWhiteSpace(input.MeterUpgradeNameFilter), e => e.MeterUpgradeFk != null && e.MeterUpgradeFk.Name == input.MeterUpgradeNameFilter)
        //                //.WhereIf(!string.IsNullOrWhiteSpace(input.MeterPhaseNameFilter), e => e.MeterPhaseFk != null && e.MeterPhaseFk.Name == input.MeterPhaseNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionOfferNameFilter), e => e.PromotionOfferFk != null && e.PromotionOfferFk.Name == input.PromotionOfferNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.HouseTypeNameFilter), e => e.HouseTypeFk != null && e.HouseTypeFk.Name == input.HouseTypeNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter);

        //    var query = (from o in filteredJobs
        //                 join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
        //                 from s1 in j1.DefaultIfEmpty()

        //                 join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
        //                 from s2 in j2.DefaultIfEmpty()

        //                 join o3 in _lookup_roofTypeRepository.GetAll() on o.RoofTypeId equals o3.Id into j3
        //                 from s3 in j3.DefaultIfEmpty()

        //                 join o4 in _lookup_roofAngleRepository.GetAll() on o.RoofAngleId equals o4.Id into j4
        //                 from s4 in j4.DefaultIfEmpty()

        //                 join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
        //                 from s5 in j5.DefaultIfEmpty()

        //                 join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
        //                 from s6 in j6.DefaultIfEmpty()

        //                 join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
        //                 from s7 in j7.DefaultIfEmpty()

        //                 join o8 in _lookup_paymentOptionRepository.GetAll() on o.PaymentOptionId equals o8.Id into j8
        //                 from s8 in j8.DefaultIfEmpty()

        //                 join o9 in _lookup_depositOptionRepository.GetAll() on o.DepositOptionId equals o9.Id into j9
        //                 from s9 in j9.DefaultIfEmpty()

        //                 join o10 in _lookup_meterUpgradeRepository.GetAll() on o.MeterUpgradeId equals o10.Id into j10
        //                 from s10 in j10.DefaultIfEmpty()

        //                 join o11 in _lookup_meterPhaseRepository.GetAll() on o.MeterPhaseId equals o11.Id into j11
        //                 from s11 in j11.DefaultIfEmpty()

        //                 join o12 in _lookup_promotionOfferRepository.GetAll() on o.PromotionOfferId equals o12.Id into j12
        //                 from s12 in j12.DefaultIfEmpty()

        //                 join o13 in _lookup_houseTypeRepository.GetAll() on o.HouseTypeId equals o13.Id into j13
        //                 from s13 in j13.DefaultIfEmpty()

        //                 join o14 in _lookup_financeOptionRepository.GetAll() on o.FinanceOptionId equals o14.Id into j14
        //                 from s14 in j14.DefaultIfEmpty()

        //                 select new GetJobForViewDto()
        //                 {
        //                     Job = new JobDto
        //                     {
        //                         //PanelOnFlat = o.PanelOnFlat,
        //                         //PanelOnPitched = o.PanelOnPitched,
        //                         RegPlanNo = o.RegPlanNo,
        //                         LotNumber = o.LotNumber,
        //                         //PeakMeterNo = o.PeakMeterNo,
        //                         //OffPeakMeter = o.OffPeakMeter,
        //                         //EnoughMeterSpace = o.EnoughMeterSpace,
        //                         //IsSystemOffPeak = o.IsSystemOffPeak,
        //                         //BasicCost = o.BasicCost,
        //                         //SolarVLCRebate = o.SolarVLCRebate,
        //                         //SolarVLCLoan = o.SolarVLCLoan,
        //                         //TotalCost = o.TotalCost,
        //                         Suburb = o.Suburb,
        //                         State = o.State,
        //                         UnitNo = o.UnitNo,
        //                         UnitType = o.UnitType,
        //                         Id = o.Id
        //                     },
        //                     JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
        //                     JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
        //                     //RoofTypeName = s3 == null || s3.Name == null ? "" : s3.Name.ToString(),
        //                     //RoofAngleName = s4 == null || s4.Name == null ? "" : s4.Name.ToString(),
        //                     ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
        //                     LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
        //                     ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
        //                     //PaymentOptionName = s8 == null || s8.Name == null ? "" : s8.Name.ToString(),
        //                     //DepositOptionName = s9 == null || s9.Name == null ? "" : s9.Name.ToString(),
        //                     //MeterUpgradeName = s10 == null || s10.Name == null ? "" : s10.Name.ToString(),
        //                     //MeterPhaseName = s11 == null || s11.Name == null ? "" : s11.Name.ToString(),
        //                     //PromotionOfferName = s12 == null || s12.Name == null ? "" : s12.Name.ToString(),
        //                     //HouseTypeName = s13 == null || s13.Name == null ? "" : s13.Name.ToString(),
        //                     //FinanceOptionName = s14 == null || s14.Name == null ? "" : s14.Name.ToString()
        //                 });


        //    var jobListDtos = await query.ToListAsync();

        //    return _jobsExcelExporter.ExportToFile(jobListDtos);
        //}
        public List<InstallationCalendarDto> GetInstallationCalendar(int installerId, DateTime CalendarStartDate, int? orgID, string areaNameFilter, string state)
        {
            var dates = new List<DateTime>();
            var startDate = (_timeZoneConverter.Convert(CalendarStartDate, (int)AbpSession.TenantId));
            var endDate = startDate.Value.AddDays(6).Date;
            for (var dt = startDate; dt <= endDate; dt = dt.Value.AddDays(1))
            {
                dates.Add(dt.Value.Date);
            }
            var installationList = (from dt in dates
                                    select new InstallationCalendarDto
                                    {
                                        InstallationDate = dt.Date,
                                        InstallationCount = _jobRepository.GetAll()
                                                .WhereIf(installerId > 0, x => x.InstallerId == installerId)
                                                .WhereIf(!string.IsNullOrWhiteSpace(state), e => e.LeadFk.State == state)
                                                .WhereIf(!string.IsNullOrWhiteSpace(areaNameFilter), e => e.LeadFk.Area == areaNameFilter)
                                                .WhereIf(orgID != 0, x => x.LeadFk.OrganizationId == orgID)
                                                .Where(x => x.InstallationDate.Value.Date == Convert.ToDateTime(dt.Date.ToShortDateString())).Count(),

                                        Jobs = (from jobs in _jobRepository.GetAll().Include(e => e.LeadFk).Where(x => x.InstallationDate.Value.Date == Convert.ToDateTime(dt.Date.ToShortDateString()))
                                                .WhereIf(installerId > 0, x => x.InstallerId == installerId)
                                                .WhereIf(!string.IsNullOrWhiteSpace(areaNameFilter), e => e.LeadFk.Area == areaNameFilter)
                                                .WhereIf(!string.IsNullOrWhiteSpace(state), e => e.LeadFk.State == state)
                                                .WhereIf(orgID != 0, x => x.LeadFk.OrganizationId == orgID)
                                                select new InstallationJobsDto
                                                {
                                                    JobId = jobs.Id,
                                                    LeadId = jobs.LeadId,
                                                    JobNumber = jobs.JobNumber,
                                                    Suburb = jobs.Suburb,
                                                    PostalCode = jobs.PostalCode,
                                                    State = jobs.State,
                                                    Address = jobs.Address,
                                                    InstallationTime = jobs.InstallationTime,
                                                    InstallerName = _userRepository.GetAll().Where(x => x.Id == jobs.InstallerId).FirstOrDefault().FullName,
                                                    CustomerName = jobs.LeadFk.CompanyName,
                                                    ColorClass = getCardColorClass(jobs.LeadFk.OrganizationId)
                                                }).ToList()
                                    });


            return new List<InstallationCalendarDto>(
                installationList.ToList()
            );
        }

        //public async Task<TotalSammaryCountDto> getSummaryCountForMap()
        //{
        //    var output = new TotalSammaryCountDto();

        //    output.TotalMapData = _jobRepository.GetAll().Where(e => e.JobStatusId == 4 || e.JobStatusId == 5).Select(e => e.Id).Count();
        //    output.TotalDepRcvMapData = _jobRepository.GetAll().Where(e => e.JobStatusId == 4).Select(e => e.Id).Count();
        //    output.TotalActiveMapData = _jobRepository.GetAll().Where(e => e.JobStatusId == 5).Select(e => e.Id).Count();
        //    output.TotalJobBookedMapData = _jobRepository.GetAll().Where(e => e.JobStatusId == 6).Select(e => e.Id).Count();
        //    return output;
        //}

        public async Task<List<GetInstallerMapDto>> GetInstallerMap(GetAllInputMapDto input)
        {
            try
            {
                var InstallerUser = _userRepository.GetAll().Select(e => e.Id).ToList();
                var installerpostcode = _installerDetailRepository.GetAll().Where(j => InstallerUser.Contains(j.UserId)).Select(e => Convert.ToInt32(e.PostCode)).ToList();

                List<InstalllerWithPostCode> installlerWithPostCodes = (from o in _installerDetailRepository.GetAll().Where(j => InstallerUser.Contains(j.UserId))
                                                                        select new InstalllerWithPostCode()
                                                                        {
                                                                            PostCode = Convert.ToInt32(o.PostCode),
                                                                            InstallerName = o.UserFk.FullName
                                                                        }).ToList();

                var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

                var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
                IList<string> role = await _userManager.GetRolesAsync(User);
                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
                var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

                bool GrantedPermissionNames = _userPermissionRepository.GetAll().Where(e => e.Name == "Pages.JobBooking" && e.UserId == AbpSession.UserId && e.IsGranted == true).Any();

                DateTime currentDate = DateTime.Now;
                DateTime currentnewDate = currentDate.Date.AddDays(-30);

                List<int?> jobst = new List<int?> { 4, 5, 6, 8 };

                var filteredJobs = _jobRepository.GetAll().Include(e => e.LeadFk)
                     //.WhereIf(!string.IsNullOrWhiteSpace(input.FilterText), e => false || e.JobNumber.Contains(input.FilterText) || e.LeadFk.Mobile.Contains(input.FilterText) || e.LeadFk.Email.Contains(input.FilterText) || e.LeadFk.Phone.Contains(input.FilterText) || e.LeadFk.CompanyName.Contains(input.FilterText))
                    .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobNumber.Contains(input.FilterText))
                    .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.LeadFk.Mobile.Contains(input.FilterText))
                    .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.LeadFk.CompanyName.Contains(input.FilterText))
                    .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.LeadFk.Phone.Contains(input.FilterText))
                    .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.LeadFk.Email.Contains(input.FilterText))
                    .WhereIf(input.HousetypeId != 0, e => e.HouseTypeId == input.HousetypeId)
                    .WhereIf(input.Rooftypeid != 0, e => e.RoofTypeId == input.Rooftypeid)
                    .WhereIf(input.Jobstatusid != 0, e => e.JobStatusId == input.Jobstatusid)
                    .WhereIf(input.Paymenttypeid != 0, e => e.PaymentOptionId == input.Paymenttypeid)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.PostalCode, input.PostalCodeFrom) >= 0)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.PostalCode, input.PostalCodeTo) <= 0)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.SteetAddressFilter), e => e.LeadFk.Address == input.SteetAddressFilter)
                    .WhereIf(input.DateFilterType == "Active" && input.StartDate != null && input.EndDate != null, e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
                    .WhereIf(input.DateFilterType == "DepositeReceived" && input.StartDate != null && input.EndDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                    .WhereIf(input.Jobstatusid == 0 && input.Priority == 0, e => e.JobStatusId == 4 || e.JobStatusId == 5)
                    .WhereIf(input.Priority == 1, e => e.JobStatusId == 5 && e.ActiveDate <= currentnewDate)
                    .WhereIf(input.Priority == 2, e => e.JobStatusId == 4 || (e.JobStatusId == 5 && e.ActiveDate >= currentnewDate))
                    .WhereIf(!role.Contains("Admin") && !role.Contains("Installation Manager") && !role.Contains("Pre Installation") && !role.Contains("Post Installation")  && GrantedPermissionNames, e => e.BookingManagerId == AbpSession.UserId)
                    .WhereIf(role.Contains("Sales Rep") , e => e.LeadFk.AssignToUserID == AbpSession.UserId)
                    .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && jobst.Contains(e.JobStatusId));

                var _TotalMapData = filteredJobs.Where(e => e.JobStatusId == 4 || e.JobStatusId == 5 || e.JobStatusId == 6 || e.JobStatusId == 8).Select(e => e.LeadId).Count();
                var _TotalDepRcvMapData = filteredJobs.Where(e => e.JobStatusId == 4 && (e.Longitude == null ? e.LeadFk.longitude : e.Longitude) != null
                && (e.Latitude == null ? e.LeadFk.latitude : e.Latitude) != null).Select(e => e.LeadId).Count();

                var _TotalActiveMapData = filteredJobs.Where(e => e.JobStatusId == 5 && (e.Longitude == null ? e.LeadFk.longitude : e.Longitude) != null
                && (e.Latitude == null ? e.LeadFk.latitude : e.Latitude) != null).Select(e => e.LeadId).Count();

                var _TotalJobBookedMapData = filteredJobs.Where(e => e.JobStatusId == 6 && (e.Longitude == null ? e.LeadFk.longitude : e.Longitude) != null
                && (e.Latitude == null ? e.LeadFk.latitude : e.Latitude) != null).Select(e => e.LeadId).Count();

                var _TotalJobInstallMapData = filteredJobs.Where(e => e.JobStatusId == 8 && (e.Longitude == null ? e.LeadFk.longitude : e.Longitude) != null
                && (e.Latitude == null ? e.LeadFk.latitude : e.Latitude) != null).Select(e => e.LeadId).Count();

                var _Other = filteredJobs.Where(e => (e.Longitude == null ? e.LeadFk.longitude : e.Longitude) == null
                && (e.Latitude == null ? e.LeadFk.latitude : e.Latitude) == null).Select(e => e.LeadId).Count();

                var _ListOfmissingLatlong = filteredJobs.Where(e => (e.Longitude == null ? e.LeadFk.longitude : e.Longitude) == null
                && (e.Latitude == null ? e.LeadFk.latitude : e.Latitude) == null).Select(e => e.Id).ToList();

                var jobs = (from o in filteredJobs

                            //join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                            //from s6 in j6.DefaultIfEmpty()

                            let test = o.JobStatusId == 8 ? 0 : getDistanceFromLatLonInKm(o.Latitude == null ? o.LeadFk.latitude : o.Latitude, o.Longitude == null ? o.LeadFk.longitude : o.Longitude, o.State)
                            let Icons = getIcon(o.JobStatusId, o.ActiveDate.Value.Date)
                            let nearest2postcode = o.JobStatusId == 8 ? null : getNearest(o.PostalCode, installlerWithPostCodes)
                            let first = nearest2postcode != null ? nearest2postcode.First() : ""
                            let last = nearest2postcode != null ? nearest2postcode.Last() : ""

                            where (o.Latitude != null && o.Longitude != null) || (o.LeadFk.latitude != null && o.LeadFk.longitude != null)

                            select new GetInstallerMapDto()
                            {
                                LeadId = o.LeadFk.Id,
                                label = o.JobNumber,
                                Company = o.LeadFk.CompanyName + " - ",
                                Status = o.JobStatusFk.Name,
                                Adress = o.Address + " " + o.Suburb + " " + o.State + " " + o.PostalCode,
                                State = o.State,
                                PhoneEmail = o.LeadFk.Mobile + '/' + o.LeadFk.Email,
                                DepRecDate = o.DepositeRecceivedDate,
                                ActiveDate = o.ActiveDate,
                                HouseType = o.HouseTypeFk.Name,
                                RoofType = o.RoofTypeFk.Name,
                                AreaType = o.LeadFk.Area,
                                Latitude = o.Latitude == null ? o.LeadFk.latitude : o.Latitude,
                                Longitude = o.Longitude == null ? o.LeadFk.longitude : o.Longitude,
                                Icon = Icons,
                                Distance = test,
                                //ProductIteams = (from o in _jobProductItemRepository.GetAll().Where(e => e.JobId == o.Id)
                                //                 join o2 in _lookup_productItemRepository.GetAll() on o.ProductItemId equals o2.Id
                                //                 select new GetJobProductItemForEditOutput()
                                //                 {
                                //                     JobNote = o2.ProductTypeFk.Name,
                                //                     ProductItemName = o2.Name,
                                //                     Model = o2.Model,
                                //                     Quantity = o.Quantity
                                //                 }).ToList(),
                                ProductIteams = (from o in _jobProductItemRepository.GetAll().Where(e => e.JobId == o.Id)
                                                 //join o2 in _lookup_productItemRepository.GetAll() on o.ProductItemId equals o2.Id
                                                 select new GetJobProductItemForEditOutput()
                                                 {
                                                     JobNote = o.ProductItemFk.ProductTypeFk.Name,
                                                     ProductItemName = o.ProductItemFk.Name,
                                                     Model = o.ProductItemFk.Model,
                                                     Quantity = o.Quantity
                                                 }).ToList(),
                                Id = o.Id,
                                JobStatusId = o.JobStatusId,

                                FirstNearestInstallerName = first,
                                SecondNearestInstallerName = last,

                                TotalMapData = _TotalMapData,
                                TotalDepRcvMapData = _TotalDepRcvMapData,
                                TotalActiveMapData = _TotalActiveMapData,
                                TotalJobBookedMapData = _TotalJobBookedMapData,
                                TotalJobInstallMapData = _TotalJobInstallMapData,
                                Other = _Other,
                                ListOfmissingLatlong = _ListOfmissingLatlong

                            }).ToList();

                return jobs;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static string getIcon(int? jobstatusid, DateTime? Date)
        {
            DateTime currentDate = DateTime.Now;
            DateTime newDate = currentDate.Date.AddDays(-30);

            var icon3 = "https://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png";
            var Active = "https://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
            var DepRcv = "https://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png";
            var JobBook = "https://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png";
            var JobInstalled = "https://www.google.com/intl/en_us/mapfiles/ms/micons/orange-dot.png";

            var Result = "";
            if (jobstatusid == 4)
            {
                Result = DepRcv;
            }
            if (jobstatusid == 5)
            {
                if (Date <= newDate)
                {
                    Result = icon3;
                }
                else
                {
                    Result = Active;
                }
            }
            if (jobstatusid == 6)
            {
                Result = JobBook;
            }
            if (jobstatusid == 8)
            {
                Result = JobInstalled;
            }

            return Result;
        }

        private static double getDistanceFromLatLonInKm(string lat1, string lon1, string States)
        {
            if (lat1 != "" && lon1 != "")
            {
                var R = 6371; // Radius of the earth in km
                var lat2 = 0.0;
                var lon2 = 0.0;

                if (States == "QLD")
                {
                    lat2 = -27.580041680084136;
                    lon2 = 153.03702238177127;
                }
                if (States == "VIC")
                {
                    lat2 = -37.81759262390686;
                    lon2 = 144.8550323686331;
                }
                if (States == "NSW")
                {
                    lat2 = -33.74722076106781;
                    lon2 = 150.90180287033405;
                }
                if (States == "SA")
                {
                    lat2 = -34.828917615461556;
                    lon2 = 138.600999853177;
                }
                if (States == "WA")
                {
                    lat2 = -32.06865473895976;
                    lon2 = 115.9103034972597;
                }

                var d1 = Convert.ToDouble(lat1) * (Math.PI / 180.0);
                var num1 = Convert.ToDouble(lon1) * (Math.PI / 180.0);
                var d2 = lat2 * (Math.PI / 180.0);
                var num2 = lon2 * (Math.PI / 180.0) - num1;
                var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) +
                         Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);
                var ans = 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
                return Math.Round((ans / 1000), 2);
            }
            else
            {
                return 0.00;
            }
            //var dLat = deg2rad(lat2 - Convert.ToDouble(lat1));  // deg2rad below
            //var dLon = deg2rad(lon2 - Convert.ToDouble(lon1));
            //var a =
            //  Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
            //  Math.Cos(deg2rad(Convert.ToDouble(lat1))) * Math.Cos(deg2rad(lat2)) *
            //  Math.Sin(dLon / 2) * Math.Cos(dLon / 2);
            //var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            //var d = R * c; // Distance in km

            //return Math.Round((d / 1000),2);
        }

        private static List<string> getNearest(string post, List<InstalllerWithPostCode> installerpostcode)
        {
            var s = Convert.ToInt32(post);
            var Listpostcodes = installerpostcode.OrderBy(x => Math.Abs(x.PostCode - s)).Take(2).ToList();
            var nearest2postcode = (from i in Listpostcodes select i.InstallerName.ToString()).ToList();
            return nearest2postcode;
        }

        private static string getCardColorClass(int orgId)
        {
            string ColorClass = "";
            if (orgId == 1)
            {
                ColorClass = "sb-record";
            }
            else if (orgId == 2)
            {
                ColorClass = "sv-record";
            }
            else if (orgId == 8)
            {
                ColorClass = "sm-record";
            }

            return ColorClass;
        }

        public async Task<PagedResultDto<GetMissingJobDto>> GetMissingJobs(GetMissingJobInput input)
        {
            var filteredJobs = _jobRepository.GetAll().Include(e => e.LeadFk).Include(e => e.JobStatusFk).Where(e => input.JobIds.Contains(e.Id));

            var result = (from o in filteredJobs
                          select new GetMissingJobDto()
                          {
                              Id = o.Id,
                              LeadId = o.LeadFk.Id,
                              JobNumber = o.JobNumber,
                              CompanyName = o.LeadFk.CompanyName,
                              JobStatus = o.JobStatusFk.Name,
                              JobStatusColorClass = o.JobStatusFk.ColorClass
                          });

            var pagedAndFilteredJobs = filteredJobs
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var totalCount = await filteredJobs.CountAsync();

            return new PagedResultDto<GetMissingJobDto>(totalCount, await result.ToListAsync());
        }

        public class InstalllerWithPostCode
        {
            public int PostCode { get; set; }

            public string InstallerName { get; set; }
        }
    }
}
