﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.CancelReasons.Exporting;
using TheSolarProduct.CancelReasons.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.TheSolarDemo;

namespace TheSolarProduct.CancelReasons
{
	[AbpAuthorize(AppPermissions.Pages_CancelReasons)]
    public class CancelReasonsAppService : TheSolarProductAppServiceBase, ICancelReasonsAppService
    {
		 private readonly IRepository<CancelReason> _cancelReasonRepository;
		 private readonly ICancelReasonsExcelExporter _cancelReasonsExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;


        public CancelReasonsAppService(IRepository<CancelReason> cancelReasonRepository, ICancelReasonsExcelExporter cancelReasonsExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider) 
		  {
			_cancelReasonRepository = cancelReasonRepository;
			_cancelReasonsExcelExporter = cancelReasonsExcelExporter;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
			
		  }

		 public async Task<PagedResultDto<GetCancelReasonForViewDto>> GetAll(GetAllCancelReasonsInput input)
         {
			
			var filteredCancelReasons = _cancelReasonRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.CancelReasonName.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.CancelReasonNameFilter),  e => e.CancelReasonName == input.CancelReasonNameFilter);

			var pagedAndFilteredCancelReasons = filteredCancelReasons
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

			var cancelReasons = from o in pagedAndFilteredCancelReasons
                         select new GetCancelReasonForViewDto() {
							CancelReason = new CancelReasonDto
							{
                                CancelReasonName = o.CancelReasonName,
                                Id = o.Id,
                                IsActive = o.IsActive
							}
						};

            var totalCount = await filteredCancelReasons.CountAsync();

            return new PagedResultDto<GetCancelReasonForViewDto>(
                totalCount,
                await cancelReasons.ToListAsync()
            );
         }
		 
		 public async Task<GetCancelReasonForViewDto> GetCancelReasonForView(int id)
         {
            var cancelReason = await _cancelReasonRepository.GetAsync(id);

            var output = new GetCancelReasonForViewDto { CancelReason = ObjectMapper.Map<CancelReasonDto>(cancelReason) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_CancelReasons_Edit)]
		 public async Task<GetCancelReasonForEditOutput> GetCancelReasonForEdit(EntityDto input)
         {
            var cancelReason = await _cancelReasonRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetCancelReasonForEditOutput {CancelReason = ObjectMapper.Map<CreateOrEditCancelReasonDto>(cancelReason)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditCancelReasonDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_CancelReasons_Create)]
		 protected virtual async Task Create(CreateOrEditCancelReasonDto input)
         {
            var cancelReason = ObjectMapper.Map<CancelReason>(input);

			
			if (AbpSession.TenantId != null)
			{
				cancelReason.TenantId = (int) AbpSession.TenantId;
			}
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 5;
            dataVaultLog.ActionNote = "Cancel Reason Created : " + input.CancelReasonName;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _cancelReasonRepository.InsertAsync(cancelReason);
         }

		 [AbpAuthorize(AppPermissions.Pages_CancelReasons_Edit)]
		 protected virtual async Task Update(CreateOrEditCancelReasonDto input)
         {
            var cancelReason = await _cancelReasonRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 5;
            dataVaultLog.ActionNote = "Cancel Reason Updated : " + cancelReason.CancelReasonName;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.CancelReasonName != cancelReason.CancelReasonName)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "CancelReasonName";
                history.PrevValue = cancelReason.CancelReasonName;
                history.CurValue = input.CancelReasonName;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != cancelReason.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = cancelReason.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, cancelReason);
         }

		 [AbpAuthorize(AppPermissions.Pages_CancelReasons_Delete)]
         public async Task Delete(EntityDto input)
         {
            var Name = _cancelReasonRepository.Get(input.Id).CancelReasonName;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 5;
            dataVaultLog.ActionNote = "Cancel Reason Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _cancelReasonRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetCancelReasonsToExcel(GetAllCancelReasonsForExcelInput input)
         {
			
			var filteredCancelReasons = _cancelReasonRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.CancelReasonName.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.CancelReasonNameFilter),  e => e.CancelReasonName == input.CancelReasonNameFilter);

			var query = (from o in filteredCancelReasons
                         select new GetCancelReasonForViewDto() { 
							CancelReason = new CancelReasonDto
							{
                                CancelReasonName = o.CancelReasonName,
                                Id = o.Id
							}
						 });


            var cancelReasonListDtos = await query.ToListAsync();

            return _cancelReasonsExcelExporter.ExportToFile(cancelReasonListDtos);
         }


    }
}