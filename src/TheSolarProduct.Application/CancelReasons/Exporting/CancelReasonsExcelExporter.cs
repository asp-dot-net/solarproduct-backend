﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.CancelReasons.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using Abp.Authorization;
using TheSolarProduct.Authorization;

namespace TheSolarProduct.CancelReasons.Exporting
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class CancelReasonsExcelExporter : NpoiExcelExporterBase, ICancelReasonsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public CancelReasonsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetCancelReasonForViewDto> cancelReasons)
        {
            return CreateExcelPackage(
                "CancelReasons.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("CancelReasons"));

                    AddHeader(
                        sheet,
                        L("CancelReasonName")
                        );

                    AddObjects(
                        sheet, 2, cancelReasons,
                        _ => _.CancelReason.CancelReasonName
                        );

					
					
                });
        }
    }
}
