﻿using System.Collections.Generic;
using TheSolarProduct.CancelReasons.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.CancelReasons.Exporting
{
    public interface ICancelReasonsExcelExporter
    {
        FileDto ExportToFile(List<GetCancelReasonForViewDto> cancelReasons);
    }
}