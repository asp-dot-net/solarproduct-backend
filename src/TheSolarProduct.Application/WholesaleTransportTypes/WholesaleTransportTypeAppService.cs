﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Wholesaleinvoicetype;
using TheSolarProduct.WholesaleinvoicetypeAppService.Exporting;
using TheSolarProduct.Wholesales.DataVault;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct.WholesaleTransportTypes.Dtos;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Abp;
using TheSolarProduct.Wholesaleinvoicetype.Dtos;



namespace TheSolarProduct.WholesaleTransportTypes
{
    public class WholesaleTransportTypeAppService : TheSolarProductAppServiceBase, IWholesaleTransportTypeAppService
    {
        private readonly IRepository<WholesaleTransportType> _wholesaleTransportTypeRepository;
        private readonly IWholesaleinvoicetypeExcelExporter _wholesaleinvoicetypeExcelExporter;
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;

        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public WholesaleTransportTypeAppService(

            IRepository<WholesaleTransportType> WholesaleTransportTypeRepository,
             // IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
             IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
           
        {


            _wholesaleTransportTypeRepository = WholesaleTransportTypeRepository;
            // _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;

        }


        public async Task<PagedResultDto<GetWholesaleTransportTypeForViewDto>> GetAll(GetAllWholesaleTransportTypeInput input)
        {
            var transporttype = _wholesaleTransportTypeRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFiltered = transporttype
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFiltered
                         select new GetWholesaleTransportTypeForViewDto()
                         {
                             TransportType = new TransportTypeDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 IsActive = o.IsActive
                             }
                         };

            var totalCount = await transporttype.CountAsync();

            return new PagedResultDto<GetWholesaleTransportTypeForViewDto>(totalCount, await output.ToListAsync());
        }

        public async Task<GetWholesaleTransportTypeForEditOutput> GetWholesaleTransportTypeForEdit(EntityDto input)
        {
            var transporttype = await _wholesaleTransportTypeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetWholesaleTransportTypeForEditOutput { TransportType = ObjectMapper.Map<CreateOrEditWholesaleTransportTypeDto>(transporttype) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditWholesaleTransportTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        protected virtual async Task Create(CreateOrEditWholesaleTransportTypeDto input)
        {
            var transporttype = ObjectMapper.Map<WholesaleTransportType>(input);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 7;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "WholesaletransportType Created: " + input.Name;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

            await _wholesaleTransportTypeRepository.InsertAsync(transporttype);
        }

        protected virtual async Task Update(CreateOrEditWholesaleTransportTypeDto input)
        {
            var transporttype = await _wholesaleTransportTypeRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 7;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "WholesaletransportType Updated : " + transporttype.Name;
            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            var List = new List<WholeSaleDataVaultActivityLogHistory>();
            if (input.Name != transporttype.Name)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = transporttype.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != transporttype.IsActive)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = transporttype.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, transporttype);

            await _wholesaleTransportTypeRepository.UpdateAsync(transporttype);
        }

        public async Task Delete(EntityDto input)
        {
            var Name = _wholesaleTransportTypeRepository.Get(input.Id).Name;

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 7;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "WholesaletransportType Deleted  : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            await _wholesaleTransportTypeRepository.DeleteAsync(input.Id);
        }

    }
}
