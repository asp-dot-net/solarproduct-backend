﻿using TheSolarProduct.SmsTemplates.Dtos;
using TheSolarProduct.SmsTemplates;
using TheSolarProduct.HoldReasons.Dtos;
using TheSolarProduct.HoldReasons;
using TheSolarProduct.Installer.Dtos;
using TheSolarProduct.Installer;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Invoices;
using TheSolarProduct.CancelReasons.Dtos;
using TheSolarProduct.CancelReasons;
using TheSolarProduct.RejectReasons.Dtos;
using TheSolarProduct.RejectReasons;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Jobs;
using TheSolarProduct.Promotions.Dtos;
using TheSolarProduct.Promotions;
using TheSolarProduct.Departments.Dtos;
using TheSolarProduct.Departments;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.Leads;
using TheSolarProduct.LeadSources.Dtos;
using TheSolarProduct.LeadSources;
using TheSolarProduct.PostalTypes.Dtos;
using TheSolarProduct.PostalTypes;
using TheSolarProduct.PostCodes.Dtos;
using TheSolarProduct.PostCodes;
using TheSolarProduct.StreetNames.Dtos;
using TheSolarProduct.StreetNames;
using TheSolarProduct.StreetTypes.Dtos;
using TheSolarProduct.StreetTypes;
using TheSolarProduct.UnitTypes.Dtos;
using TheSolarProduct.UnitTypes;
using Abp.Application.Editions;
using Abp.Application.Features;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.DynamicEntityParameters;
using Abp.EntityHistory;
using Abp.Localization;
using Abp.Notifications;
using Abp.Organizations;
using Abp.UI.Inputs;
using Abp.Webhooks;
using AutoMapper;
using TheSolarProduct.Auditing.Dto;
using TheSolarProduct.Authorization.Accounts.Dto;
using TheSolarProduct.Authorization.Delegation;
using TheSolarProduct.Authorization.Permissions.Dto;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Roles.Dto;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Authorization.Users.Delegation.Dto;
using TheSolarProduct.Authorization.Users.Dto;
using TheSolarProduct.Authorization.Users.Importing.Dto;
using TheSolarProduct.Authorization.Users.Profile.Dto;
using TheSolarProduct.Chat;
using TheSolarProduct.Chat.Dto;
using TheSolarProduct.DynamicEntityParameters.Dto;
using TheSolarProduct.Editions;
using TheSolarProduct.Editions.Dto;
using TheSolarProduct.Friendships;
using TheSolarProduct.Friendships.Cache;
using TheSolarProduct.Friendships.Dto;
using TheSolarProduct.Localization.Dto;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.MultiTenancy.Dto;
using TheSolarProduct.MultiTenancy.HostDashboard.Dto;
using TheSolarProduct.MultiTenancy.Payments;
using TheSolarProduct.MultiTenancy.Payments.Dto;
using TheSolarProduct.Notifications.Dto;
using TheSolarProduct.Organizations.Dto;
using TheSolarProduct.Sessions.Dto;
using TheSolarProduct.WebHooks.Dto;
using TheSolarProduct.Expense.Dtos;
using TheSolarProduct.Expense;
using TheSolarProduct.TheSolarDemo.Dtos;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.TheSolarProduct.Dtos;
using TheSolarProduct.MyLeads.Dtos;
using TheSolarProduct.UserDashboard.Dtos;
using TheSolarProduct.Quotations.Dtos;
using TheSolarProduct.Quotations;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.EmailTemplates.Dtos;
using TheSolarProduct.EmailTemplates;
using TheSolarProduct.DocumentTypes.Dtos;
using TheSolarProduct.JobCancellationReasons.Dtos;
using TheSolarProduct.Organizations;
using TheSolarProduct.DocumentLibrarys.Dtos;
using TheSolarProduct.PVDStatuses;
using TheSolarProduct.SystemReports.Dto;
using TheSolarProduct.UserWiseEmailOrgs;
using TheSolarProduct.ServiceCategorys.Dtos;
using TheSolarProduct.ServiceCategorys;
using TheSolarProduct.ServiceStatuses.Dtos;
using TheSolarProduct.ServiceStatuses;
using TheSolarProduct.Services.Dtos;
using TheSolarProduct.Services;
using TheSolarProduct.ServiceSources;
using TheSolarProduct.ServiceSources.Dtos;
using TheSolarProduct.ServiceSubCategorys;
using TheSolarProduct.ServiceSubCategorys.Dtos;
using TheSolarProduct.ServiceTypes;
using TheSolarProduct.ServiceTypes.Dtos;
using TheSolarProduct.ReviewTypes;
using TheSolarProduct.ReviewTypes.Dtos;
using TheSolarProduct.PriceItemLists.Dtos;
using TheSolarProduct.PriceItemLists;
using TheSolarProduct.MyInstallerActivityLogs;
using TheSolarProduct.JobAcknowledgement.Dtos;
using TheSolarProduct.QuotationTemplate.Dto;
using TheSolarProduct.PostCodeRange.Dtos;
using TheSolarProduct.Locations.Dtos;
using TheSolarProduct.ProductPackages.Dtos;
using TheSolarProduct.ProductPackages;
using TheSolarProduct.Configuration.Dto;
using TheSolarProduct.EmailSetting;
using TheSolarProduct.InstallationCost.OtherCharges;
using TheSolarProduct.InstallationCost.OtherCharges.Dtos;
using TheSolarProduct.InstallationCost.StateWiseInstallationCosts;
using TheSolarProduct.InstallationCost.StateWiseInstallationCost.Dtos;
using TheSolarProduct.InstallationCost.FixedCostPrice.Dtos;
using TheSolarProduct.InstallationCost.FixedCostPrices;
using TheSolarProduct.InstallationCost.InstallationItemList.Dtos;
using TheSolarProduct.InstallationCost.InstallationItemLists;
using TheSolarProduct.InstallationCost.ExtraInstallationCharge.Dtos;
using TheSolarProduct.InstallationCost.ExtraInstallationCharges;
using TheSolarProduct.InstallationCost.STCCost.Dtos;
using TheSolarProduct.InstallationCost.STCCost;
using TheSolarProduct.InstallationCost.BatteryCost.Dtos;
using TheSolarProduct.InstallationCost.BatteryInstallationCost;
using TheSolarProduct.DeclarationForms.Dtos;
using TheSolarProduct.CallFlowQueues.Dtos;
using TheSolarProduct.CallFlowQueues;
using TheSolarProduct.ServiceDocumentTypes.Dtos;
using TheSolarProduct.ServiceInvoice.Dtos;
using TheSolarProduct.WholeSaleStatuses.Dtos;
using TheSolarProduct.WholeSaleStatuses;
using TheSolarProduct.WholeSaleLeads.Dtos;
using TheSolarProduct.WholeSaleLeads;
using TheSolarProduct.WholeSalePromotions.Dtos;
using TheSolarProduct.WholeSalePromotions;
using TheSolarProduct.WholeSaleLeadDocumentTypes.Dtos;
using TheSolarProduct.WholeSaleSmsTemplates.Dtos;
using TheSolarProduct.WholeSaleSmsTemplates;
using TheSolarProduct.WholeSaleEmailTemplates.Dto;
using TheSolarProduct.WholeSaleEmailTemplates;
using TheSolarProduct.CommissionRanges;
using TheSolarProduct.CommissionRanges.Dtos;
using TheSolarProduct.DashboardMessages.Dtos;
using TheSolarProduct.CategoryInstallationItemLists.Dtos;
using TheSolarProduct.CategoryInstallations;
using TheSolarProduct.ECommerces.ECommerceSliders.Dtos;
using TheSolarProduct.ECommerceSliders;
using TheSolarProduct.ECommerces.EcommerceProductItems.Dtos;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct.ECommerces.Businesses.Dtos;
using TheSolarProduct.Businesses;
using TheSolarProduct.InstallationCost.PostCodeCost;
using TheSolarProduct.InstallationCost.PostCodeCost.Dtos;
using TheSolarProduct.ECommerces.BrandingPartners.Dto;
using TheSolarProduct.ECommerces.SpecialOffers.Dtos;
using TheSolarProduct.ECommerces.EcommerceUserRegisterRequests.Dto;
using TheSolarProduct.ECommerces.Client.Dto;
using TheSolarProduct.ECommerces.EcommerceSolarPackages.Dto;
using TheSolarProduct.ECommerces.ContactUs.Dtos;
using TheSolarProduct.CheckDepositReceived.Dto;
using TheSolarProduct.CheckDepositReceived;

using TheSolarProduct.CheckApplications.Dtos;
using TheSolarProduct.CheckApplications;
using TheSolarProduct.CheckActives.Dtos;
using TheSolarProduct.CheckActives;
using TheSolarProduct.PaymentMethods;
using TheSolarProduct.TransportationCosts.Dtos;
using TheSolarProduct.TransportationCosts;
using TheSolarProduct.TransportCompanies.Dto;
using TheSolarProduct.PurchaseCompanies;
using TheSolarProduct.PurchaseCompanys.Dtos;
using TheSolarProduct.StockOrderStatus.Dtos;
using TheSolarProduct.PaymentStatuses.Dtos;
using TheSolarProduct.PaymentStatuses;
using TheSolarProduct.PaymentMethodes.Dtos;
using TheSolarProduct.PaymentTypes.Dtos;
using TheSolarProduct.PaymentTypes;
using TheSolarProduct.DeliveryTypes.Dtos;
using TheSolarProduct.DeliveryTypes;
using TheSolarProduct.FreightCompanyes.Dtos;
using TheSolarProduct.FreightCompanies;
using TheSolarProduct.PurchaseDocumentLists.Dtos;
using TheSolarProduct.PurchaseDocumentListes;
using TheSolarProduct.Currencies.Dtos;
using TheSolarProduct.Currencies;
using TheSolarProduct.StockFroms.Dtos;
using TheSolarProduct.StockFroms;
using TheSolarProduct.StockOrderFors.Dtos;
using TheSolarProduct.StockOrderFors;
using TheSolarProduct.Vendors.Dtos;
using TheSolarProduct.Vendors;
using TheSolarProduct.IncoTerms.Dtos;
using TheSolarProduct.IncoTerms;
using TheSolarProduct.StockOrder.Dtos;
using TheSolarProduct.StockOrders;
using TheSolarProduct.Series.Dto;
using TheSolarProduct.Wholesaleinvoicetype.Dtos;
using TheSolarProduct.WholesaleDeliveryOption.Dto;
using TheSolarProduct.WholesaleTransportTypes.Dtos;
using TheSolarProduct.WholesaleJobTypes.Dtos;
using TheSolarProduct.WholesalePropertyTypes.Dtos;
using TheSolarProduct.WholeSalePVDStatuses.Dtos;
using TheSolarProduct.BonusList;
using TheSolarProduct.LeadGeneration.Dtos;
using TheSolarProduct.ECommerces.EcommerceProductTypes.Dto;
using TheSolarProduct.WholesaleJobStatuses.Dtos;
using TheSolarProduct.Wholesales;
using TheSolarProduct.EcommerceSpecifications.Dtos;
using TheSolarProduct.EcommerceProducctSpecification.Dtos;
using TheSolarProduct.StockTransfers.Dtos;
using TheSolarProduct.UserActivityLogs;
using TheSolarProduct.UserActivityLogs.Dtos;
using TheSolarProduct.InstallationCost.JobCostFixExpenses.Dto;
using TheSolarProduct.StockPayments.Dtos;
using TheSolarProduct.StockPayments;
using TheSolarProduct.ECommerces.StcDealPrices.Dtos;
using TheSolarProduct.ThirdPartyApis.ExternalApis.Dtos;
using TheSolarProduct.States.Dtos;
using TheSolarProduct.States;
using TheSolarProduct.SerialNoStatuses.Dtos;
using TheSolarProduct.StockOrders.SerialNoStatuses;
using TheSolarProduct.QuickStockApp.StockOrder.Dto;
using TheSolarProduct.Vouchers.Dtos;
using TheSolarProduct.VoucherMasters;

namespace TheSolarProduct
{
    internal static class CustomDtoMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<CreateOrEditSmsTemplateDto, SmsTemplate>().ReverseMap();
            configuration.CreateMap<SmsTemplateDto, SmsTemplate>().ReverseMap();
            configuration.CreateMap<CreateOrEditFreebieTransportDto, FreebieTransport>().ReverseMap();
            configuration.CreateMap<FreebieTransportDto, FreebieTransport>().ReverseMap();
            configuration.CreateMap<CreateOrEditInvoiceStatusDto, InvoiceStatus>().ReverseMap();
            configuration.CreateMap<InvoiceStatusDto, InvoiceStatus>().ReverseMap();
            configuration.CreateMap<CreateOrEditJobInstallerInvoiceDto, InstallerInvoice>().ReverseMap();
            configuration.CreateMap<CreateOrEditPriceItemListsDto, PriceItemList>().ReverseMap();

            configuration.CreateMap<CreateOrEditHoldReasonDto, HoldReason>().ReverseMap();
            configuration.CreateMap<CreateOrEditServiceCategoryDto, ServiceCategory>().ReverseMap();
            configuration.CreateMap<CreateOrEditServiceStatusDto, ServiceStatus>().ReverseMap();
            configuration.CreateMap<CreateOrEditServiceSubCategoryDto, ServiceSubCategory>().ReverseMap();
            configuration.CreateMap<CreateOrEditServiceTypeDto, ServiceType>().ReverseMap();
            configuration.CreateMap<GetDocumentTypeforEditOutput, DocumentType>().ReverseMap();
            configuration.CreateMap<CreateorEditDocumentTypeDto, DocumentType>().ReverseMap();
            configuration.CreateMap<CreateorEditDocumentLibraryDto, DocumentLibrary>().ReverseMap();
            configuration.CreateMap<GetJobCancellationReasonforEditOutput, JobCancellationReason>().ReverseMap();
            configuration.CreateMap<CreateOrEditJobCancellationReasonDto, JobCancellationReason>().ReverseMap();
            configuration.CreateMap<CreateOrEditReviewTypeDto, ReviewType>().ReverseMap();
            //Installer Calendar
            configuration.CreateMap<AddInstallerAvailabilityDto, InstallerAvailability>().ReverseMap();
            configuration.CreateMap<GetInstallerAvailabilityDto, InstallerAvailability>().ReverseMap();

            configuration.CreateMap<CreateOrEditEmailTemplateDto, EmailTemplate>().ReverseMap();
            configuration.CreateMap<GetEmailTemplateDto, EmailTemplate>().ReverseMap();
            configuration.CreateMap<CreateOrEditRefundReasonDto, RefundReason>().ReverseMap();
            configuration.CreateMap<RefundReasonDto, RefundReason>().ReverseMap();
            configuration.CreateMap<CreateOrEditJobRefundDto, JobRefund>().ReverseMap();
            configuration.CreateMap<JobRefundDto, JobRefund>().ReverseMap();
            configuration.CreateMap<CreateOrEditInstallerDetailDto, InstallerDetail>().ReverseMap();
            configuration.CreateMap<InstallerDetailDto, InstallerDetail>().ReverseMap();
            configuration.CreateMap<CreateOrEditInstallerAddressDto, InstallerAddress>().ReverseMap();
            configuration.CreateMap<InstallerAddressDto, InstallerAddress>().ReverseMap();
            configuration.CreateMap<CreateOrEditQuotationDto, Quotation>().ReverseMap();
            configuration.CreateMap<QuotationDto, Quotation>().ReverseMap();
            configuration.CreateMap<UploadDocumentInput, Document>().ReverseMap();
            configuration.CreateMap<CreateOrEditInvoicePaymentMethodDto, InvoicePaymentMethod>().ReverseMap();
            configuration.CreateMap<InvoicePaymentMethodDto, InvoicePaymentMethod>().ReverseMap();
            configuration.CreateMap<CreateOrEditInvoicePaymentDto, InvoicePayment>().ReverseMap();
            configuration.CreateMap<InvoicePaymentDto, InvoicePayment>().ReverseMap();
            configuration.CreateMap<CreateOrEditJobProductItemDto, JobProductItem>().ReverseMap();
            configuration.CreateMap<JobProductItemDto, JobProductItem>().ReverseMap();
            configuration.CreateMap<CreateOrEditPromotionMasterDto, PromotionMaster>().ReverseMap();
            configuration.CreateMap<PromotionMasterDto, PromotionMaster>().ReverseMap();
            configuration.CreateMap<CreateOrEditFinanceOptionDto, FinanceOption>().ReverseMap();
            configuration.CreateMap<FinanceOptionDto, FinanceOption>().ReverseMap();
            configuration.CreateMap<CreateOrEditHouseTypeDto, HouseType>().ReverseMap();
            configuration.CreateMap<HouseTypeDto, HouseType>().ReverseMap();
            configuration.CreateMap<CreateOrEditCancelReasonDto, CancelReason>().ReverseMap();
            configuration.CreateMap<CancelReasonDto, CancelReason>().ReverseMap();
            configuration.CreateMap<CreateOrEditRejectReasonDto, RejectReason>().ReverseMap();
            configuration.CreateMap<RejectReasonDto, RejectReason>().ReverseMap();
            configuration.CreateMap<CreateOrEditMeterPhaseDto, MeterPhase>().ReverseMap();
            configuration.CreateMap<MeterPhaseDto, MeterPhase>().ReverseMap();
            configuration.CreateMap<CreateOrEditMeterUpgradeDto, MeterUpgrade>().ReverseMap();
            configuration.CreateMap<MeterUpgradeDto, MeterUpgrade>().ReverseMap();
            configuration.CreateMap<CreateOrEditElecRetailerDto, ElecRetailer>().ReverseMap();
            configuration.CreateMap<ElecRetailerDto, ElecRetailer>().ReverseMap();
            configuration.CreateMap<CreateOrEditDepositOptionDto, DepositOption>().ReverseMap();
            configuration.CreateMap<DepositOptionDto, DepositOption>().ReverseMap();
            configuration.CreateMap<CreateOrEditPaymentOptionDto, PaymentOption>().ReverseMap();
            configuration.CreateMap<PaymentOptionDto, PaymentOption>().ReverseMap();
            configuration.CreateMap<CreateOrEditPromotionDto, Promotion>().ReverseMap();
            configuration.CreateMap<PromotionDto, Promotion>().ReverseMap();
            configuration.CreateMap<CreateOrEditJobVariationDto, JobVariation>().ReverseMap();
            configuration.CreateMap<CreateOrEditJobOdSysDetails, JobOldSystemDetail>().ReverseMap();
            configuration.CreateMap<CreateOrEditUserOrgDto, UserWiseEmailOrg>().ReverseMap();
            configuration.CreateMap<JobVariationDto, JobVariation>().ReverseMap();
            configuration.CreateMap<CreateOrEditVariationDto, Variation>().ReverseMap();
            configuration.CreateMap<VariationDto, Variation>().ReverseMap();
            configuration.CreateMap<CreateOrEditJobPromotionDto, JobPromotion>().ReverseMap();
            configuration.CreateMap<JobPromotionDto, JobPromotion>().ReverseMap();
            configuration.CreateMap<CreateOrEditJobDto, Job>().ReverseMap();
            configuration.CreateMap<UpdateJobStatusDto, Job>().ReverseMap();
            configuration.CreateMap<JobInstallationDto, Job>().ReverseMap();
            configuration.CreateMap<JobDto, Job>().ReverseMap();
            configuration.CreateMap<CreateOrEditJobStatusDto, JobStatus>().ReverseMap();
            configuration.CreateMap<JobStatusDto, JobStatus>().ReverseMap();
            configuration.CreateMap<CreateOrEditProductItemDto, ProductItem>().ReverseMap();
            //configuration.CreateMap<ProductItemDto, ProductItem>().ReverseMap();
            configuration.CreateMap<CreateOrEditElecDistributorDto, ElecDistributor>().ReverseMap();
            configuration.CreateMap<ElecDistributorDto, ElecDistributor>().ReverseMap();
            configuration.CreateMap<CreateOrEditRoofAngleDto, RoofAngle>().ReverseMap();
            configuration.CreateMap<RoofAngleDto, RoofAngle>().ReverseMap();
            configuration.CreateMap<CreateOrEditRoofTypeDto, RoofType>().ReverseMap();
            configuration.CreateMap<RoofTypeDto, RoofType>().ReverseMap();
            configuration.CreateMap<CreateOrEditProductTypeDto, ProductType>().ReverseMap();
            configuration.CreateMap<ProductTypeDto, ProductType>().ReverseMap();
            configuration.CreateMap<CreateOrEditJobTypeDto, JobType>().ReverseMap();
            configuration.CreateMap<JobTypeDto, JobType>().ReverseMap();
            configuration.CreateMap<CreateOrEditPromotionUserDto, PromotionUser>().ReverseMap();
            configuration.CreateMap<PromotionUserDto, PromotionUser>().ReverseMap();
            configuration.CreateMap<CreateOrEditPromotionOfferDto, Promotion>().ReverseMap();
            configuration.CreateMap<PromotionOfferDto, PromotionOffer>().ReverseMap();
            configuration.CreateMap<CreateOrEditPromotionResponseStatusDto, PromotionResponseStatus>().ReverseMap();
            configuration.CreateMap<PromotionResponseStatusDto, PromotionResponseStatus>().ReverseMap();
            configuration.CreateMap<CreateOrEditPromotionTypeDto, PromotionType>().ReverseMap();
            configuration.CreateMap<PromotionTypeDto, PromotionType>().ReverseMap();
            configuration.CreateMap<CreateOrEditDepartmentDto, Department>().ReverseMap();
            configuration.CreateMap<DepartmentDto, Department>().ReverseMap();
            configuration.CreateMap<CreateOrEditLeadExpenseDto, LeadExpense>().ReverseMap();
            configuration.CreateMap<LeadExpenseDto, LeadExpense>().ReverseMap();
            configuration.CreateMap<CreateOrEditUserTeamDto, UserTeam>().ReverseMap();
            configuration.CreateMap<UserTeamDto, UserTeam>().ReverseMap();
            configuration.CreateMap<CreateOrEditCategoryDto, Category>().ReverseMap();
            configuration.CreateMap<CategoryDto, Category>().ReverseMap();
            configuration.CreateMap<CreateOrEditTeamDto, Team>().ReverseMap();
            configuration.CreateMap<TeamDto, Team>().ReverseMap();

            configuration.CreateMap<CreateOrEditLeadDto, Lead>().ReverseMap();
            configuration.CreateMap<CreateOrEditServiceDto, Service>().ReverseMap();
            configuration.CreateMap<CreateOrEditForAppLeadDto, Lead>().ReverseMap();
            configuration.CreateMap<LeadDto, Lead>().ReverseMap();
            configuration.CreateMap<LeadDashboardDto, Lead>().ReverseMap();
            configuration.CreateMap<CreateOrEditMyLeadDto, Lead>().ReverseMap();
            configuration.CreateMap<MyLeadDto, Lead>().ReverseMap();
            configuration.CreateMap<CreateOrEditForExternalLeadDto, Lead>().ReverseMap();
            configuration.CreateMap<ActivityLogInput, LeadActivityLog>().ReverseMap();

            configuration.CreateMap<CreateOrEditLeadSourceDto, LeadSource>().ReverseMap();
            configuration.CreateMap<LeadSourceDto, LeadSource>().ReverseMap();
            configuration.CreateMap<CreateOrEditPostalTypeDto, PostalType>().ReverseMap();
            configuration.CreateMap<PostalTypeDto, PostalType>().ReverseMap();
            configuration.CreateMap<CreateOrEditPostCodeDto, PostCode>().ReverseMap();
            configuration.CreateMap<PostCodeDto, PostCode>().ReverseMap();
            configuration.CreateMap<CreateOrEditStreetNameDto, StreetName>().ReverseMap();
            configuration.CreateMap<StreetNameDto, StreetName>().ReverseMap();
            configuration.CreateMap<CreateOrEditStreetTypeDto, StreetType>().ReverseMap();
            configuration.CreateMap<StreetTypeDto, StreetType>().ReverseMap();
            configuration.CreateMap<CreateOrEditUnitTypeDto, UnitType>().ReverseMap();
            configuration.CreateMap<UnitTypeDto, UnitType>().ReverseMap();

            configuration.CreateMap<QuotationDataDto, QuotationDetail>().ReverseMap();
            configuration.CreateMap<QunityAndModelList, QuotationQunityAndModelDetail>().ReverseMap();
            configuration.CreateMap<CreateOrEditAcnoDto, Quotations.JobAcknowledgement>().ReverseMap();

            configuration.CreateMap<QuotationTemplateDto, Quotations.QuotationTemplate>().ReverseMap();
            configuration.CreateMap<CreateOrEditQuotationTemplateDto, Quotations.QuotationTemplate>().ReverseMap();

            //Inputs
            configuration.CreateMap<CheckboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<SingleLineStringInputType, FeatureInputTypeDto>();
            configuration.CreateMap<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<IInputType, FeatureInputTypeDto>()
                .Include<CheckboxInputType, FeatureInputTypeDto>()
                .Include<SingleLineStringInputType, FeatureInputTypeDto>()
                .Include<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<ILocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>()
                .Include<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<LocalizableComboboxItem, LocalizableComboboxItemDto>();
            configuration.CreateMap<ILocalizableComboboxItem, LocalizableComboboxItemDto>()
                .Include<LocalizableComboboxItem, LocalizableComboboxItemDto>();

            //Application Setting
            configuration.CreateMap<EditApplicationSettingDto, ApplicationSetting>().ReverseMap();
            configuration.CreateMap<ApplicationSettingListDto, ApplicationSetting>().ReverseMap();

            //Chat
            configuration.CreateMap<ChatMessage, ChatMessageDto>();
            configuration.CreateMap<ChatMessage, ChatMessageExportDto>();

            //Feature
            configuration.CreateMap<FlatFeatureSelectDto, Feature>().ReverseMap();
            configuration.CreateMap<Feature, FlatFeatureDto>();

            //Role
            configuration.CreateMap<RoleEditDto, Role>().ReverseMap();
            configuration.CreateMap<Role, RoleListDto>();
            configuration.CreateMap<UserRole, UserListRoleDto>();

            //Edition
            configuration.CreateMap<EditionEditDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<EditionCreateDto, SubscribableEdition>();
            configuration.CreateMap<EditionSelectDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<Edition, EditionInfoDto>().Include<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<SubscribableEdition, EditionListDto>();
            configuration.CreateMap<Edition, EditionEditDto>();
            configuration.CreateMap<Edition, SubscribableEdition>();
            configuration.CreateMap<Edition, EditionSelectDto>();

            //Payment
            configuration.CreateMap<SubscriptionPaymentDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPaymentListDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPayment, SubscriptionPaymentInfoDto>();

            //Permission
            configuration.CreateMap<Permission, FlatPermissionDto>();
            configuration.CreateMap<Permission, FlatPermissionWithLevelDto>();

            //Language
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageListDto>();
            configuration.CreateMap<NotificationDefinition, NotificationSubscriptionWithDisplayNameDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>()
                .ForMember(ldto => ldto.IsEnabled, options => options.MapFrom(l => !l.IsDisabled));

            //Tenant
            configuration.CreateMap<Tenant, RecentTenant>();
            configuration.CreateMap<Tenant, TenantLoginInfoDto>();
            configuration.CreateMap<Tenant, TenantListDto>();
            configuration.CreateMap<TenantEditDto, Tenant>().ReverseMap();
            configuration.CreateMap<CurrentTenantInfoDto, Tenant>().ReverseMap();

            //User
            configuration.CreateMap<User, UserEditDto>()
                .ForMember(dto => dto.Password, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Password, options => options.Ignore());
            configuration.CreateMap<User, UserLoginInfoDto>();
            configuration.CreateMap<User, UserListDto>();
            configuration.CreateMap<User, ChatUserDto>();
            configuration.CreateMap<User, OrganizationUnitUserListDto>();
            configuration.CreateMap<Role, OrganizationUnitRoleListDto>();
            configuration.CreateMap<CurrentUserProfileEditDto, User>().ReverseMap();
            configuration.CreateMap<UserLoginAttemptDto, UserLoginAttempt>().ReverseMap();
            configuration.CreateMap<ImportUserDto, User>();

            //Installer
            configuration.CreateMap<User, InstallerEditDto>()
                .ForMember(dto => dto.Password, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Password, options => options.Ignore());

            //AuditLog
            configuration.CreateMap<AuditLog, AuditLogListDto>();
            configuration.CreateMap<EntityChange, EntityChangeListDto>();
            configuration.CreateMap<EntityPropertyChange, EntityPropertyChangeDto>();

            //Friendship
            configuration.CreateMap<Friendship, FriendDto>();
            configuration.CreateMap<FriendCacheItem, FriendDto>();

            //OrganizationUnit
            //configuration.CreateMap<ExtendedOrganizationUnit, ExtendOrganizationUnit>().ReverseMap();
            configuration.CreateMap<OrganizationUnit, OrganizationUnitDto>();

            //Webhooks
            configuration.CreateMap<WebhookSubscription, GetAllSubscriptionsOutput>();
            configuration.CreateMap<WebhookSendAttempt, GetAllSendAttemptsOutput>()
                .ForMember(webhookSendAttemptListDto => webhookSendAttemptListDto.WebhookName,
                    options => options.MapFrom(l => l.WebhookEvent.WebhookName))
                .ForMember(webhookSendAttemptListDto => webhookSendAttemptListDto.Data,
                    options => options.MapFrom(l => l.WebhookEvent.Data));

            configuration.CreateMap<WebhookSendAttempt, GetAllSendAttemptsOfWebhookEventOutput>();

            configuration.CreateMap<DynamicParameter, DynamicParameterDto>().ReverseMap();
            configuration.CreateMap<DynamicParameterValue, DynamicParameterValueDto>().ReverseMap();
            configuration.CreateMap<EntityDynamicParameter, EntityDynamicParameterDto>()
                .ForMember(dto => dto.DynamicParameterName,
                    options => options.MapFrom(entity => entity.DynamicParameter.ParameterName));
            configuration.CreateMap<EntityDynamicParameterDto, EntityDynamicParameter>();

            configuration.CreateMap<EntityDynamicParameterValue, EntityDynamicParameterValueDto>().ReverseMap();
            //User Delegations
            configuration.CreateMap<CreateUserDelegationDto, UserDelegation>();

            configuration.CreateMap<CreateEditActivityLogDto, LeadActivityLog>();
            configuration.CreateMap<LeadActivityLogDto, LeadActivityLog>();
            configuration.CreateMap<CreateOrEditStcStatusDto, PVDStatus>();
            configuration.CreateMap<CreateOrEditServiceSourcesDto, ServiceSource>().ReverseMap();

            configuration.CreateMap<CreateOrEditJobReviewDto, JobReview>().ReverseMap();
            configuration.CreateMap<OrganizationUnitDto, ExtendOrganizationUnit>().ReverseMap();
            
            configuration.CreateMap<ActivityLogInput, MyInstallerActivityLog>().ReverseMap();
            configuration.CreateMap<InstallerEditDto, InstallerDetail>().ReverseMap();
            /* ADD YOUR OWN CUSTOM AUTOMAPPER MAPPINGS HERE */

            configuration.CreateMap<OrganizationUnitMapDto, OrganizationUnitMap>().ReverseMap();

            //Post Code Range
            configuration.CreateMap<CreateOrEditPostCodeRangeDto, PostCodeRange.PostCodeRange>().ReverseMap();
            configuration.CreateMap<PostCodeRangeDto, PostCodeRange.PostCodeRange>().ReverseMap();

            //Warehouse Location
            configuration.CreateMap<CreateOrEditLocationDto, Warehouselocation>().ReverseMap();
            configuration.CreateMap<LocationDto, Warehouselocation>().ReverseMap();

            //Product Package
            configuration.CreateMap<CreateOrEditProductPackageDto, ProductPackage>().ReverseMap();
            configuration.CreateMap<ProductPackageDto, ProductPackage>().ReverseMap();

            //Product Package Item
            configuration.CreateMap<CreateOrEditProductPackageItemDto, ProductPackageItem>().ReverseMap();
            //configuration.CreateMap<ProductPackageItemDto, ProductPackageItem>().ReverseMap();

            //Product Package Item
            configuration.CreateMap<EmailSettingsDto, EmailSettings>().ReverseMap();


            configuration.CreateMap<ServiceDocumentTypeDto, ServiceDocumentType>().ReverseMap();
            configuration.CreateMap<CreateOrEditServiceDocumentTypeDto, ServiceDocumentType>().ReverseMap();

            configuration.CreateMap<CreateOrEditServiceInvoDto, ServiceInvo>().ReverseMap();

            configuration.CreateMap<InstallerInvoicePriceListDto, InstallerInvoicePriceList>().ReverseMap();


            #region Installation Cost
            // Other Charges
            configuration.CreateMap<CreateOrEditOtherChargesDto, OtherCharge>().ReverseMap();
            configuration.CreateMap<OtherChargesDto, OtherCharge>().ReverseMap();

            //State Wise Installation Costs
            configuration.CreateMap<CreateOrEditStateWiseInstallationCostDto, StateWiseInstallationCost>().ReverseMap();
            configuration.CreateMap<StateWiseInstallationCostDto, StateWiseInstallationCost>().ReverseMap();

            //Installation Item List
            configuration.CreateMap<CreateOrEditInstallationItemPeriodDto, InstallationItemPeriod>().ReverseMap();
            configuration.CreateMap<CreateOrEditInstallationItemListDto, InstallationItemList>().ReverseMap();

            //Fixed Cost Price
            configuration.CreateMap<CreateOrEditFixedCostPriceDto, FixedCostPrice>().ReverseMap();
            configuration.CreateMap<FixedCostPriceDto, FixedCostPrice>().ReverseMap();

            //Extra Installation Charges
            configuration.CreateMap<CreateOrEditExtraInstallationChargeDto, ExtraInstallationCharge>().ReverseMap();
            configuration.CreateMap<ExtraInstallationChargesDto, ExtraInstallationCharge>().ReverseMap();

            // STC Cost
            configuration.CreateMap<CreateOrEditSTCCostDto, STCCost>().ReverseMap();
            configuration.CreateMap<STCCostDto, STCCost>().ReverseMap(); 
            
            // Battery Cost
            configuration.CreateMap<CreateOrEditBatteryInstallationCostDto, BatteryInstallationCost>().ReverseMap();
            configuration.CreateMap<BatteryInstallationCostDto, BatteryInstallationCost>().ReverseMap();

            configuration.CreateMap<CreateOrEditDeclarationFormDto, Quotations.DeclarationForm>().ReverseMap();
            configuration.CreateMap<DeclarationFormDto, Quotations.DeclarationForm>().ReverseMap();

            configuration.CreateMap<CreateOrEditCallFlowQueueDto, CallFlowQueue>().ReverseMap();
            configuration.CreateMap<CallFlowQueueDto, CallFlowQueue>().ReverseMap();

            #endregion
       
            configuration.CreateMap<WholeSaleStatusDto, WholeSaleStatus>().ReverseMap();
            configuration.CreateMap<CreateOrEditWholeSaleLeadDto, WholeSaleLead>().ReverseMap();

            configuration.CreateMap<CreateOrEditWholeSalePromotionUserDto, WholeSalePromotionUser>().ReverseMap();
            configuration.CreateMap<WholeSalePromotionUserDto, WholeSalePromotionUser>().ReverseMap();
            configuration.CreateMap<CreateOrEditWholeSalePromotionResponseStatusDto, WholeSalePromotionResponseStatus>().ReverseMap();
            configuration.CreateMap<WholeSalePromotionResponseStatusDto, WholeSalePromotionResponseStatus>().ReverseMap();
            configuration.CreateMap<CreateOrEditWholeSalePromotionTypeDto, WholeSalePromotionType>().ReverseMap();
            configuration.CreateMap<WholeSalePromotionTypeDto, WholeSalePromotionType>().ReverseMap();
            configuration.CreateMap<CreateOrEditWholeSalePromotionDto, WholeSalePromotion>().ReverseMap();
            configuration.CreateMap<WholeSalePromotionDto, WholeSalePromotion>().ReverseMap();

            configuration.CreateMap<WholeSaleLeadDocumentTypeDto, WholeSaleLeadDocumentType>().ReverseMap();
            configuration.CreateMap<CreateOrEditWholeSaleLeadDocumentTypeDto, WholeSaleLeadDocumentType>().ReverseMap();

            configuration.CreateMap<CreateOrEditWholeSaleSmsTemplateDto, WholeSaleSmsTemplate>().ReverseMap();
            configuration.CreateMap<WholeSaleSmsTemplateDto, WholeSaleSmsTemplate>().ReverseMap();

            configuration.CreateMap<CreateOrEditWholeSaleEmailTemplateDto, WholeSaleEmailTemplate>().ReverseMap();
            configuration.CreateMap<WholeSaleEmailTamplateDto, WholeSaleEmailTemplate>().ReverseMap();

            configuration.CreateMap<CreateOrEditCommissionRangesDto, CommissionRange>().ReverseMap();

            configuration.CreateMap<CreateOrEditDashboardMessageDto, DashboardMessages.DashboardMessage>().ReverseMap();


            configuration.CreateMap<CreateOrEditCategoryInstallationItemPeriodDto, CategoryInstallationItemPeriod>().ReverseMap();
            configuration.CreateMap<CreateOrEditCategoryInstallationItemListDto, CategoryInstallationItemList>().ReverseMap();

            configuration.CreateMap<ECommerceSliderDto, ECommerceSlider>().ReverseMap();
            configuration.CreateMap<CreateOrEditECommerceSliderDto, ECommerceSlider>().ReverseMap();

            configuration.CreateMap<EcommerceProductItemDto, EcommerceProductItem>().ReverseMap();
            configuration.CreateMap<EcommerceProductItemDocumentDto, EcommerceProductItemDocument>().ReverseMap();
            configuration.CreateMap<EcommerceProductItemPriceCategoryDto, EcommerceProductItemPriceCategory>().ReverseMap();

            configuration.CreateMap<BusinessDto, Business>().ReverseMap();

            configuration.CreateMap<BrandingPartnerDto, BrandingPartner>().ReverseMap();

            configuration.CreateMap<SpecialOfferDto, SpecialOffer>().ReverseMap();
            configuration.CreateMap<SpecialOfferProductDto, SpecialOfferProduct>().ReverseMap();


            configuration.CreateMap<EcommerceUserRegisterRequestDto, EcommerceUserRegisterRequest>().ReverseMap();
            configuration.CreateMap<EcommerceUserRegisterRequestCreateDto, EcommerceUserRegisterRequest>().ReverseMap();

            configuration.CreateMap<EcommerceSolarPackageDto, EcommerceSolarPackage>().ReverseMap();
            configuration.CreateMap<CreateOrEditEcommerceSolarPackageDto, EcommerceSolarPackage>().ReverseMap();
            configuration.CreateMap<CreateOrEditEcommerceSolarPackagesItemDto, EcommerceSolarPackageItem>().ReverseMap();

            configuration.CreateMap<WholeSaleLeadContactDto, WholeSaleLeadContact>().ReverseMap();

            configuration.CreateMap<EcommerceSubscriberDto, EcommerceSubscriber>().ReverseMap();

            configuration.CreateMap<CuntactUsDto, EcommerceContactUs>().ReverseMap();
            configuration.CreateMap<EcommerceContactUsDto, EcommerceContactUs>().ReverseMap();
            configuration.CreateMap<ChcekApplicationDto, CheckApplication>().ReverseMap();
            configuration.CreateMap<CreateOrEditCheckApplicationDto, CheckApplication>().ReverseMap();
            configuration.CreateMap<CreateOrEditCheckActiveDto, CheckActive>().ReverseMap();
            configuration.CreateMap<CheckActiveDto, CheckActive>().ReverseMap();


            configuration.CreateMap<CreateOrEditTransportCost, TransportationCost>().ReverseMap();

            configuration.CreateMap<CreateOrEditCheckDepositReceivedDto, Checkdeposite>().ReverseMap();
            configuration.CreateMap<CreateOrEditTransportCompaniesDto, TransportCompany>().ReverseMap();

            configuration.CreateMap<CreateOrEditDeliveryTypesDto, DeliveryType>().ReverseMap();

            configuration.CreateMap<CreateOrEditPurchaseCompanyDto, PurchaseCompany>().ReverseMap();
            configuration.CreateMap<PurchaseCompanyDto, PurchaseCompany>().ReverseMap();

            configuration.CreateMap<CreateOrEditStockOrderStatusDto, StockOrderStatuses.StockOrderStatus>().ReverseMap();
            configuration.CreateMap<StockOrderStatusDto, StockOrderStatuses.StockOrderStatus>().ReverseMap();

            configuration.CreateMap<CreateOrEditPaymentStatusDto, PaymentStatus>().ReverseMap();
            configuration.CreateMap<PyamentStatusDto, PaymentStatus>().ReverseMap();

            configuration.CreateMap<CreateOrEditPaymentMethod, PaymentMethod>().ReverseMap();
            configuration.CreateMap<PaymentMethodDto, PaymentMethod>().ReverseMap();

            configuration.CreateMap<CreateOrEditPaymentTypeDto, PaymentType>().ReverseMap();
            configuration.CreateMap<PaymentTypeDto, PaymentType>().ReverseMap();

            configuration.CreateMap<CreateOrEditFreightCompanyDto, FreightCompany>().ReverseMap();
            configuration.CreateMap<FreightCompanyDto, FreightCompany>().ReverseMap();

            configuration.CreateMap<CreateOrEditPurchaseDocumentListDto, PurchaseDocumentList>().ReverseMap();
            configuration.CreateMap<PurchaseDocumentListDto, PurchaseDocumentList>().ReverseMap();

            configuration.CreateMap<CreateOrEditCheckDepositReceivedDto, Checkdeposite>().ReverseMap();
            configuration.CreateMap<CreateOrEditTransportCompaniesDto, TransportCompany>().ReverseMap();

            configuration.CreateMap<CreateOrEditDeliveryTypesDto, DeliveryType>().ReverseMap();
            configuration.CreateMap<CreateOrEditCurrenciesDto, Currency>().ReverseMap();

            configuration.CreateMap<CreateOrEditStockFromsDto, StockFrom>().ReverseMap();

            configuration.CreateMap<CreateOrEditStockOrderForsDto, StockOrderFor>().ReverseMap();
            configuration.CreateMap<CreateOrEditVendorsDto, Vendor>().ReverseMap();
            configuration.CreateMap<VendorsDto, Vendor>().ReverseMap();
            configuration.CreateMap<VendorContactDto, VendorContact>().ReverseMap();

            configuration.CreateMap<EcommerceProductItemImageDto, EcommerceProductItemImage>().ReverseMap();

            configuration.CreateMap<STCRegisterDto, STCRegister>().ReverseMap();

            configuration.CreateMap<CreateOrEditPostCodePricePeriodDto, PostCodePricePeriod>().ReverseMap();
            configuration.CreateMap<CreateOrEditPostCodePriceListDto, PostoCodePrice>().ReverseMap();

            configuration.CreateMap<CreateOrEditIncoTermDto, IncoTerm>().ReverseMap();
            configuration.CreateMap<IncoTermDto, IncoTerm>().ReverseMap();

            configuration.CreateMap<STCProviderDto, STCProvider>().ReverseMap();

            configuration.CreateMap<CreateOrEditStockOrderDto, PurchaseOrder>().ReverseMap();
            configuration.CreateMap<StockOrderDto, PurchaseOrder>().ReverseMap();
            configuration.CreateMap<PurchaseOrderItemDto, PurchaseOrderItem>().ReverseMap();

            configuration.CreateMap<CreateOrEditStockPaymentDto, StockPayment>().ReverseMap();
            //configuration.CreateMap<StockOrderDto, StockPayment>().ReverseMap();
            configuration.CreateMap<StockPaymentOrderListDto, StockPaymentOrderList>().ReverseMap();

            configuration.CreateMap<CreateOrEditWholesaleinvoicetypesDto, WholesaleInvoicetype>().ReverseMap();
            configuration.CreateMap<CreateOrEditWholesaleDeliveryOptionDto, Wholesales.Ecommerces.WholesaleDeliveryOption>().ReverseMap();
            configuration.CreateMap<CreateOrEditSeriesDto, Wholesales.Ecommerces.Series>().ReverseMap();

            configuration.CreateMap<CreateOrEditWholesaleTransportTypeDto, WholesaleTransportType>().ReverseMap();

            configuration.CreateMap<CreateOrEditWholesaleJobTypeDto, WholesaleJobType>().ReverseMap();

            configuration.CreateMap<CreateOrEditWholesalePropertyTypeDto, WholesalePropertyType>().ReverseMap();
            configuration.CreateMap<CreateOrEditWholesalePVDStatusDto, WholesalePVDStatus>().ReverseMap();

            configuration.CreateMap<CreateOrEditWholesaleJobStatusDto, WholesaleJobStatus>().ReverseMap();

            configuration.CreateMap<CreateCommissionDto, Bonus>().ReverseMap();
            configuration.CreateMap<BonusCommissionItemDto, BonusCommissionItem>().ReverseMap();

            configuration.CreateMap<EcommerceProductTypeDto, EcommerceProductType>().ReverseMap();

            configuration.CreateMap<EcommerceCartProductItemDto, EcommerceCartProductItem>().ReverseMap();

            configuration.CreateMap<WholesaleInvoiceDto, WholesaleInvoice>().ReverseMap();
            configuration.CreateMap<CreateOrEditSpecificationDto, EcommerceSpecification>().ReverseMap();

            configuration.CreateMap<EcommerceProductItemSpecificationDto, EcommerceProductItemSpecification>().ReverseMap();

            configuration.CreateMap<CreateOrEditStockTransferDto, StockTransfer>().ReverseMap();
            configuration.CreateMap<ProductsItemDto, StockTransferProductItem>().ReverseMap();

            configuration.CreateMap<UserActivityLogDto, UserActivityLog>().ReverseMap();
            
            configuration.CreateMap<CreateOrEditJobCostFixExpensePeriodDto, JobCostFixExpensePeriod>().ReverseMap();
            configuration.CreateMap<CreateOrEditJobCostFixExpenseListDto, JobCostFixExpenseList>().ReverseMap();
            configuration.CreateMap<CreateOrEditJobSuggestedProductDto, JobSuggestedProduct>().ReverseMap();
            configuration.CreateMap<JobSuggestedProductItemDto, JobSuggestedProductItemList>().ReverseMap();

            configuration.CreateMap<StockVariationDto, StockVariation>().ReverseMap();
            configuration.CreateMap<CreateOrEditStockOrderVariation, StockOrderVariation>().ReverseMap();

            configuration.CreateMap<StcDealPriceDto, StcDealPrice>().ReverseMap();
            configuration.CreateMap<CreateOrEditStcDealPriceDto, StcDealPrice>().ReverseMap();

            configuration.CreateMap<CreateOrEditVouchersDto, VoucherMaster>().ReverseMap();
            configuration.CreateMap<MultipleOrganizationDto, MultipleOrganization>().ReverseMap();

            configuration.CreateMap<SerialNoStatusDto, SerialNoStatus>().ReverseMap();
            configuration.CreateMap<CreateOrEditSerialNoStatusDto, SerialNoStatus>().ReverseMap();
            configuration.CreateMap<GetSubmitOtherOrderSerialNosDto, StockSerialNo>().ReverseMap();
            configuration.CreateMap<StatesDto, State>().ReverseMap();
        }
    }
}