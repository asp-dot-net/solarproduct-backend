﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TheSolarProduct.Install.Dto;

namespace TheSolarProduct.Install
{
    public interface IInstallAppService : IApplicationService
    {
        Task Setup(InstallDto input);

        AppSettingsJsonDto GetAppSettingsJson();

        CheckDatabaseOutput CheckDatabase();
    }
}