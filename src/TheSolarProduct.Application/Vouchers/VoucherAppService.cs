﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Vendors.Dtos;
using TheSolarProduct.Vendors.Exporting;
using TheSolarProduct.Vendors;
using TheSolarProduct.VoucherMasters;
using Abp.Linq.Extensions;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Telerik.Reporting;
using TheSolarProduct.Vouchers.Dtos;
using TheSolarProduct.Common;
using TheSolarProduct.Storage;
using TheSolarProduct.MultiTenancy;
using NPOI.POIFS.FileSystem;
using System.IO;

namespace TheSolarProduct.Vouchers
{
   
    public class VoucherAppService : TheSolarProductAppServiceBase, IVoucherAppService
    {
        private readonly IRepository<VoucherMaster> _voucherRepository;
        private readonly IRepository<MultipleOrganization> _multipleOrganizationRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly TempFileCacheManager _tempFileCacheManager;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly IRepository<Tenant> _tenantRepository;

        public VoucherAppService(
              IRepository<VoucherMaster> voucherRepository,
              IRepository<MultipleOrganization> multipleOrganizationRepository,
              IRepository<UserTeam> userTeamRepository,
              IRepository<UserRole, long> userRoleRepository,
              IRepository<Role> roleRepository,
              IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
              TempFileCacheManager tempFileCacheManager,
              ICommonLookupAppService CommonDocumentSaveRepository,
              IRepository<Tenant> tenantRepository
              )
        {
            _voucherRepository = voucherRepository;
            _multipleOrganizationRepository = multipleOrganizationRepository;
            _userTeamRepository = userTeamRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
            _tempFileCacheManager = tempFileCacheManager;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _tenantRepository = tenantRepository;
        }




        public async Task<PagedResultDto<GetVoucherForViewDto>> GetAll(GetAllVouchersInput input)

        {
            var filtered = _voucherRepository.GetAll()
                         .WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.VoucherName == input.Filter)
                          ;

            var pagedAndFiltered = filtered
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var outputList = from o in pagedAndFiltered
                             select new GetVoucherForViewDto()
                             {
                                 Vouchers = new VoucherDto
                                 {
                                     VoucherName = o.VoucherName,
                                     IsActive = o.IsActive,
                                     Id = o.Id
                                 }
                             };

            var totalCount = await outputList.CountAsync();

            return new PagedResultDto<GetVoucherForViewDto>(
                totalCount,
                await outputList.ToListAsync()
            );
        }


        
        public async Task<CreateOrEditVouchersDto> GetVouchersForEdit(EntityDto input)
        {

            var voucher = await _voucherRepository.FirstOrDefaultAsync(input.Id);
            var voucherDto = ObjectMapper.Map<CreateOrEditVouchersDto>(voucher);

            var multipleOrg = await _multipleOrganizationRepository.GetAll().Where(e => e.VoucherId == input.Id).ToListAsync();

            voucherDto.MultipleOrganization = new List<MultipleOrganizationDto>();
            foreach (var org in multipleOrg)
            {
                var multipleOrgs = new MultipleOrganizationDto();
                multipleOrgs.Id = org.Id;
                multipleOrgs.VoucherId = org.VoucherId;
                multipleOrgs.OrgId = org.OrgId;


                voucherDto.MultipleOrganization.Add(multipleOrgs);
            }

            return voucherDto;
        }


        public async Task CreateOrEdit(CreateOrEditVouchersDto input)
        {

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        protected virtual async Task Create(CreateOrEditVouchersDto input)
        {

            var fileExtension = Path.GetExtension(input.FileName);
            var fileName = DateTime.Now.Ticks + "_" + input.VoucherName.Replace(" ", "") + fileExtension;
            var ByteArray = _tempFileCacheManager.GetFile(input.FileToken);
            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();

            var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, fileName, "Voucher", 0, 0, AbpSession.TenantId);

            input.FilePath = "\\Documents\\" + TenantName + "\\Voucher\\";
            input.FileName = fileName;
            var Voucher = ObjectMapper.Map<VoucherMaster>(input);

            var Voucherid = await _voucherRepository.InsertAndGetIdAsync(Voucher);
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 71;
            dataVaultLog.ActionNote = "Voucher Created : " + input.VoucherName;

            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            foreach (var Org in input.MultipleOrganization)
            {
                Org.VoucherId = Voucherid;
                var MultipleOrganizations = ObjectMapper.Map<MultipleOrganization>(Org);

                await _multipleOrganizationRepository.InsertAsync(MultipleOrganizations);
            }
        }
        protected virtual async Task Update(CreateOrEditVouchersDto input)
        {
            var Voucher = await _voucherRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 71;
            dataVaultLog.ActionNote = "Voucher Updated : " + Voucher.VoucherName;
            
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            var List = new List<DataVaultActivityLogHistory>();
            if (input.VoucherName != Voucher.VoucherName)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "VoucherName";
                history.PrevValue = Voucher.VoucherName;
                history.CurValue = input.VoucherName;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != Voucher.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = Voucher.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (!string.IsNullOrEmpty(input.FileToken))
            {
                var fileExtension = Path.GetExtension(input.FileName);
                var fileName = DateTime.Now.Ticks + "_" + input.VoucherName.Replace(" ", "") + fileExtension;
                var ByteArray = _tempFileCacheManager.GetFile(input.FileToken);
                var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();

                var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, fileName, "Voucher", 0, 0, AbpSession.TenantId);

                input.FilePath = "\\Documents\\" + TenantName + "\\Voucher\\";
                input.FileName = fileName;


            }
            var VoucherIds = await _multipleOrganizationRepository.GetAll().Where(e => e.VoucherId == Voucher.Id).ToListAsync();
            var CurrentOrgIds = input.MultipleOrganization != null
    ? input.MultipleOrganization.Select(e => (int?)e.Id).ToList()
    : new List<int?>();
            var listOrg = VoucherIds.Where(e => !CurrentOrgIds.Contains(e.Id));

            foreach (var org in listOrg)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();

                history.FieldName = "org Deleted";
                history.PrevValue = "";
                history.CurValue = org.VoucherId.ToString();
                history.Action = "Edit";
                //history.WholeS = wholeSaleLead.Id;
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);

                await _multipleOrganizationRepository.DeleteAsync(org);
            }
            foreach (var org in input.MultipleOrganization)
            {
                org.VoucherId = Voucher.Id;
                if ((Voucher.Id != null ? Voucher.Id : 0) == 0)
                {
                    DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();

                    history.FieldName = "Voucher Added";
                    history.PrevValue = "";
                    history.CurValue = Voucher.VoucherName;
                    history.Action = "Edit";

                    history.ActivityLogId = dataVaultLogId;
                    List.Add(history);
                    var mulorg = ObjectMapper.Map<MultipleOrganization>(org);
                    await _multipleOrganizationRepository.InsertAsync(mulorg);
                }

                else
                {
                    var mulOrg = await _multipleOrganizationRepository.GetAsync((int)org.Id);
                    if (org.OrgId != mulOrg.OrgId)
                    {
                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                        history.FieldName = "OrgId";
                        history.PrevValue = mulOrg.OrgId.ToString();
                        history.CurValue = org.OrgId.ToString();
                        history.Action = "Update";
                        history.ActivityLogId = dataVaultLogId;
                        List.Add(history);
                    }
                    

                    ObjectMapper.Map(org, mulOrg);
                    await _multipleOrganizationRepository.UpdateAsync(mulOrg);


                }

            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
            ObjectMapper.Map(input, Voucher);
        }
        public async Task Delete(EntityDto input)
        {
            var Name = _voucherRepository.Get(input.Id).VoucherName;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 71;
            dataVaultLog.ActionNote = "Voucher Deleted : " + Name;

            
            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            await _voucherRepository.DeleteAsync(input.Id);

        }


    }
}
