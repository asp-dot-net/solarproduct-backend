﻿using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Organizations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Jobs;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Leads;
using TheSolarProduct.LeadSources;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.UserDashboard.Dtos;

namespace TheSolarProduct.UserDashboard
{
    public class UserDashboardAppService : TheSolarProductAppServiceBase, IUserDashboardAppService
    {
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<OrganizationUnitRole, long> _organizationUnitRoleRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<LeadSource, int> _lookup_leadSourceRepository;
        public UserDashboardAppService(IRepository<Lead> leadRepository
            , IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository
            , IRepository<OrganizationUnitRole, long> organizationUnitRoleRepository
            , IRepository<User, long> userRepository
            , IRepository<UserRole, long> userroleRepository
            , IRepository<Role> roleRepository
            , IRepository<LeadActivityLog> leadactivityRepository,
            IRepository<UserTeam> userTeamRepository,
            IRepository<LeadSource, int> lookup_leadSourceRepository,
            UserManager userManager)
        {
            _leadRepository = leadRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _organizationUnitRoleRepository = organizationUnitRoleRepository;
            _userRepository = userRepository;
            _userRoleRepository = userroleRepository;
            _roleRepository = roleRepository;
            _leadactivityRepository = leadactivityRepository;
            _userTeamRepository = userTeamRepository;
            _userManager = userManager;
            _lookup_leadSourceRepository = lookup_leadSourceRepository;
        }

        public async Task<List<GetLeadForDashboardDto>> GetUserLeadForDashboard()
        {
            int UserId = (int)AbpSession.UserId;

            var Leads = _leadRepository.GetAll()
                        .Where(e => e.AssignToUserID == UserId)
                        .Where(e => e.LeadStatusId == 1).OrderByDescending(e => e.LeadAssignDate);

            var Result = from l in Leads
                         select new GetLeadForDashboardDto()
                         {
                             Id = l.Id,
                             Address = l.Address,
                             CompanyName = l.CompanyName,
                             Email = l.Email,
                             Mobile = l.Mobile,
                             Phone = l.Phone,
                             LeadSourceName = l.LeadSource,
                             LastReminder = _leadactivityRepository.GetAll().Where(e => e.LeadId == l.Id && e.ActionId == 8).OrderByDescending(x => x.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                             LastReminderDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == l.Id && e.ActionId == 8).OrderByDescending(x => x.Id).Select(e => e.CreationTime).FirstOrDefault(),
                             LeadAssignDate = l.LeadAssignDate
                         };

            return new List<GetLeadForDashboardDto>(
                await Result.ToListAsync()
            );
        }

        //public async Task<List<GetLeadForDashboardDto>> GetInactiveLeadForDashboard()
        //{
        //    int UserId = (int)AbpSession.UserId;
        //    var Date = DateTime.Now.AddDays(-7);
        //    var User_List = _userRepository.GetAll();

        //    var RoleName = (from user in User_List
        //                    join ur in _userRoleRepository.GetAll() on user.Id equals ur.UserId into urJoined
        //                    from ur in urJoined.DefaultIfEmpty()
        //                    join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
        //                    from us in usJoined.DefaultIfEmpty()
        //                    where (us != null && user.Id == UserId)
        //                    select (us.DisplayName));
        //    if (RoleName.Contains("admin"))
        //    {
        //        var Leads = _leadRepository.GetAll()
        //                    .Where(e => (e.LastModificationTime == null ? e.CreationTime <= Date : e.LastModificationTime <= Date) && e.AssignToUserID != null).OrderByDescending(e => e.LeadAssignDate);

        //        var Result = new List<GetLeadForDashboardDto>();

        //        Result = (from l in Leads
        //                  select new GetLeadForDashboardDto()
        //                  {
        //                      Id = l.Id,
        //                      Address = l.Address,
        //                      CompanyName = l.CompanyName,
        //                      Email = l.Email,
        //                      Mobile = l.Mobile,
        //                      Phone = l.Phone,
        //                      LeadSourceName = l.LeadSource
        //                  }).ToList();


        //        return Result;
        //    }
        //    else
        //    {
        //        var Leads = _leadRepository.GetAll()
        //                    .Where(e => (e.LastModificationTime == null ? e.CreationTime <= Date : e.LastModificationTime <= Date) && e.AssignToUserID == UserId).OrderByDescending(e => e.LeadAssignDate);

        //        var Result = new List<GetLeadForDashboardDto>();

        //        if (RoleName.Contains("Sales Manager") || RoleName.Contains("Sales Rep"))
        //        {
        //            Result = (from l in Leads
        //                      select new GetLeadForDashboardDto()
        //                      {
        //                          Id = l.Id,
        //                          Address = l.Address,
        //                          CompanyName = l.CompanyName,
        //                          Email = l.Email,
        //                          Mobile = l.Mobile,
        //                          Phone = l.Phone,
        //                          LeadSourceName = l.LeadSource
        //                      }).ToList();
        //        }

        //        return Result;
        //    }
        //}
        public async Task<List<GetLeadForDashboardDto>> GetInactiveLeadForDashboard(int Id)
        {
            try {
                List<int> leadsourcesb = new List<int>() { 2,3 ,5,6,7,8,9,10,11,13,14,15,16,18,19,21 };
                List<int> leadsourcess = new List<int>() { 2,3,5,25,26};
                //var Date = DateTime.Now.AddDays(-7);
                var Date = DateTime.Now;
                var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
                IList<string> role = await _userManager.GetRolesAsync(User);
                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
                var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

                var Leads = (_leadRepository.GetAll()
                        .WhereIf(role.Contains("Admin") && Id == 0, e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Admin") && Id != 0, e => e.AssignToUserID != Id)
                        .WhereIf(role.Contains("Sales Manager") && Id == 0, e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Manager") && Id != 0, e => e.AssignToUserID == Id)
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                 //.Where(e => (e.LastModificationTime == null ? e.CreationTime <= Date : e.LastModificationTime <= Date) && (e.LeadStatusId != 6 && e.LeadStatusId != 7 && e.LeadStatusId != 8 && e.LeadStatusId != 9 && e.LeadStatusId != 10 && e.LeadStatusId != 11)).OrderByDescending(e => e.LeadAssignDate);
                 .Where(e => (e.LastModificationTime == null ? e.CreationTime <= Date : e.LastModificationTime <= Date) && e.OrganizationId == 1
                 && (leadsourcesb.Contains((int)e.LeadSourceId)) && e.LeadSourceId!=22 ).OrderByDescending(e => e.LeadSource))
                 .Union(_leadRepository.GetAll()
                        .WhereIf(role.Contains("Admin") && Id == 0, e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Admin") && Id != 0, e => e.AssignToUserID != Id)
                        .WhereIf(role.Contains("Sales Manager") && Id == 0, e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Manager") && Id != 0, e => e.AssignToUserID == Id)
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                     .Where(e => (e.LastModificationTime == null ? e.CreationTime <= Date : e.LastModificationTime <= Date) 
                     && e.OrganizationId == 2 && (leadsourcess.Contains((int)e.LeadSourceId)) && e.LeadSourceId != 22).OrderByDescending(e => e.LeadSource))
                 ;

                //var Leads = ( (_leadRepository.GetAll()
                //       .WhereIf(role.Contains("Admin") && Id == 0, e => e.AssignToUserID != null)
                //       .WhereIf(role.Contains("Admin") && Id != 0, e => e.AssignToUserID != Id)
                //       .WhereIf(role.Contains("Sales Manager") && Id == 0, e => UserList.Contains(e.AssignToUserID))
                //       .WhereIf(role.Contains("Sales Manager") && Id != 0, e => e.AssignToUserID == Id)
                //       .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                //    .Where(e => (e.LastModificationTime == null ? e.CreationTime <= Date : e.LastModificationTime <= Date) && (leadsourcess.Contains((int)e.LeadSourceId) && e.OrganizationId == 2)).OrderByDescending(e => e.LeadSource)))
                //;

                var Result = new List<GetLeadForDashboardDto>();

                Result = (from l in Leads
                          select new GetLeadForDashboardDto()
                          {
                              Id = l.Id,
                              Address = l.Address,
                              CompanyName = l.CompanyName,
                              Email = l.Email,
                              Mobile = l.Mobile,
                              Phone = l.Phone,
                              LeadSourceName = _lookup_leadSourceRepository.GetAll().Where(e=>e.Id==l.LeadSourceId).Select(e=>e.Name).FirstOrDefault(),
                              //inactive = Convert.ToInt32(((l.LastModificationTime == null ? l.CreationTime : l.LastModificationTime) - l.LeadAssignDate).Value.Days),
                              inactive = Convert.ToInt32((DateTime.Now.Date-(l.LastModificationTime == null ? l.CreationTime.Date : l.LastModificationTime.Value.Date) ).Days),
                              followupdate = (from i in _leadactivityRepository.GetAll()
                                              join lead in _leadRepository.GetAll() on i.LeadId equals lead.Id into l1
                                              from lead in l1
                                              where (lead.Id == l.Id && i.ActionId == 8)
                                              select (i.CreationTime)).FirstOrDefault()
                          }).ToList();


                return Result;
            } catch (Exception e) { throw e; }
            }


        public int GetAllUnAssignedCount()
        {
            int UserId = (int)AbpSession.UserId;

            var User_List = _userRepository
           .GetAll();

            var RoleName = (from user in User_List
                            join ur in _userRoleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            where (us != null && user.Id == UserId)
                            select (us.DisplayName));

            var OId = _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == UserId).Select(e => e.OrganizationUnitId).ToList();
            var TotalCount = 0;
            if (RoleName.Contains("Lead Operator"))
            {
                TotalCount = _leadRepository.GetAll()
                            .Where(e => e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && OId.Contains(e.OrganizationId))
                            .Count();
            }
            else if (RoleName.Contains("Admin"))
            {
                TotalCount = _leadRepository.GetAll()
                            .Where(e => e.AssignToUserID == null && e.IsDuplicate != true && e.IsWebDuplicate != true && OId.Contains(e.OrganizationId))
                            .Count();
            }

            return TotalCount;
        }
    }
}