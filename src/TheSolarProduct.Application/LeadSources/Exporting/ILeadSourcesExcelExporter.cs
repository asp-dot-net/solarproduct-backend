﻿using System.Collections.Generic;
using TheSolarProduct.LeadSources.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.LeadSources.Exporting
{
    public interface ILeadSourcesExcelExporter
    {
        FileDto ExportToFile(List<GetLeadSourceForViewDto> leadSources);
    }
}