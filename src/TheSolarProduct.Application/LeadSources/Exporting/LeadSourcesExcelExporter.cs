﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.LeadSources.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using Abp.Authorization;
using TheSolarProduct.Authorization;

namespace TheSolarProduct.LeadSources.Exporting
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class LeadSourcesExcelExporter : NpoiExcelExporterBase, ILeadSourcesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public LeadSourcesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetLeadSourceForViewDto> leadSources)
        {
            return CreateExcelPackage(
                "LeadSources.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("LeadSources"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("IsActive")
                        );

                    AddObjects(
                        sheet, 2, leadSources,
                        _ => _.LeadSource.Name,
                        _ => _.LeadSource.IsActive
                        );

                });
        }
    }
}