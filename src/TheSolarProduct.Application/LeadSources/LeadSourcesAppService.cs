﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.LeadSources.Exporting;
using TheSolarProduct.LeadSources.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.UI;
using TheSolarProduct.Storage;
using Abp.Organizations;
using System.Windows.Forms;
using TheSolarProduct.DataVaults;
using Telerik.Reporting;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.LeadSources
{
    [AbpAuthorize(AppPermissions.Pages_LeadSources)]
    public class LeadSourcesAppService : TheSolarProductAppServiceBase, ILeadSourcesAppService
    {
        private readonly IRepository<LeadSource> _leadSourceRepository;
        private readonly ILeadSourcesExcelExporter _leadSourcesExcelExporter;
        private readonly IRepository<LeadSourceOrganizationUnit> _leadSourceOrganizationUnitRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public LeadSourcesAppService(IRepository<LeadSource> leadSourceRepository, ILeadSourcesExcelExporter leadSourcesExcelExporter, IRepository<OrganizationUnit, long> organizationUnitRepository, IRepository<LeadSourceOrganizationUnit> leadSourceOrganizationUnitRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _leadSourceRepository = leadSourceRepository;
            _leadSourcesExcelExporter = leadSourcesExcelExporter;
            _leadSourceOrganizationUnitRepository = leadSourceOrganizationUnitRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task<PagedResultDto<GetLeadSourceForViewDto>> GetAll(GetAllLeadSourcesInput input)
        {
            var leadSourceOrganizations = _leadSourceOrganizationUnitRepository.GetAll().Where(e=>e.OrganizationUnitId ==input.OrganizationUnit).Select(e => e.LeadSourceId).ToList();

            var filteredLeadSources = _leadSourceRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter)
                        .Where(e=> leadSourceOrganizations.Contains(e.Id));

            var pagedAndFilteredLeadSources = filteredLeadSources
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var leadSources = from o in pagedAndFilteredLeadSources
                              select new
                              {

                                  o.Name,
                                  o.IsActive,
                                  Id = o.Id,
                                  o.IsActiveSalesRep
                              };

            var totalCount = await filteredLeadSources.CountAsync();

            var dbList = await leadSources.ToListAsync();
            var results = new List<GetLeadSourceForViewDto>();

            foreach (var o in dbList)
            {
                var res = new GetLeadSourceForViewDto()
                {
                    LeadSource = new LeadSourceDto
                    {
                        Name = o.Name,
                        IsActive = o.IsActive,
                        Id = o.Id,
                        IsActiveSalesRep = o.IsActiveSalesRep,
                        OrganizationUnitsName = (from lo in _leadSourceOrganizationUnitRepository.GetAll()
                                                 join ou in _organizationUnitRepository.GetAll() on lo.OrganizationUnitId equals ou.Id
                                                 where lo.LeadSourceId == o.Id 
                                                 select ou.DisplayName
                                                 ).ToList()
                        //IsActiveSalesRep = false,
                        //OrganizationUnitsName = null
                    }
                };

                results.Add(res);
            }

            return new PagedResultDto<GetLeadSourceForViewDto>(
                totalCount,
                results
            );

        }

        public async Task<GetLeadSourceForViewDto> GetLeadSourceForView(int id)
        {
            var leadSource = await _leadSourceRepository.GetAsync(id);

            var output = new GetLeadSourceForViewDto { LeadSource = ObjectMapper.Map<LeadSourceDto>(leadSource) };

            output.LeadSource.OrganizationUnitsName = await _leadSourceOrganizationUnitRepository.GetAll().Where(e => e.LeadSourceId == id).Select(e => e.OrganizationUnitFk.DisplayName).ToListAsync();

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_LeadSources_Edit)]
        public async Task<GetLeadSourceForEditOutput> GetLeadSourceForEdit(EntityDto input)
        {
            var leadSource = await _leadSourceRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLeadSourceForEditOutput { LeadSource = ObjectMapper.Map<CreateOrEditLeadSourceDto>(leadSource) };

            output.LeadSource.OrganizationUnits = await _leadSourceOrganizationUnitRepository.GetAll().Where(e => e.LeadSourceId == input.Id).Select(e => (long)e.OrganizationUnitId).ToListAsync();

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditLeadSourceDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_LeadSources_Create)]
        protected virtual async Task Create(CreateOrEditLeadSourceDto input)
        {
            var leadSource = ObjectMapper.Map<LeadSource>(input);

            //if (AbpSession.TenantId != null)
            //{
            //    leadSource.TenantId = (int)AbpSession.TenantId;
            //}

            var Id = await _leadSourceRepository.InsertAndGetIdAsync(leadSource);

            //Lead Source OrganizationUnits
            LeadSourceOrganizationUnit leadSourceOrganizationUnit;
            if (input.OrganizationUnits != null)
            {
                foreach (var item in input.OrganizationUnits)
                {
                    leadSourceOrganizationUnit = new LeadSourceOrganizationUnit();
                    leadSourceOrganizationUnit.LeadSourceId = Id;
                    leadSourceOrganizationUnit.OrganizationUnitId = item;
                    _leadSourceOrganizationUnitRepository.InsertOrUpdate(leadSourceOrganizationUnit);
                }
            }
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 2;
            dataVaultLog.ActionNote = "Lead Source Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);
        }

        [AbpAuthorize(AppPermissions.Pages_LeadSources_Edit)]
        protected virtual async Task Update(CreateOrEditLeadSourceDto input)
        {
            var leadSource = await _leadSourceRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 2;
            dataVaultLog.ActionNote = "Lead Source Updated : " + leadSource.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != leadSource.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = leadSource.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.MissingPer != leadSource.MissingPer)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "MissingPer";
                history.PrevValue = leadSource.MissingPer.ToString();
                history.CurValue = input.MissingPer.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.IsActive != leadSource.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = leadSource.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.IsActiveSalesRep != leadSource.IsActiveSalesRep)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActiveSalesRep";
                history.PrevValue = leadSource.IsActiveSalesRep.ToString();
                history.CurValue = input.IsActiveSalesRep.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            var Organization = input.OrganizationUnits != null ? string.Join(",",  _organizationUnitRepository.GetAll().Where(e => input.OrganizationUnits.Contains(e.Id)).Select(e => e.DisplayName).ToList()) : "";
            var InputOrganization = _leadSourceOrganizationUnitRepository.GetAll().Where(x => x.LeadSourceId == leadSource.Id).Count() > 0 ? string.Join(",", _leadSourceOrganizationUnitRepository.GetAll().Where(x => x.LeadSourceId == leadSource.Id).Select(e => e.OrganizationUnitFk.DisplayName).ToList()) : "";

            if (Organization != InputOrganization)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Organization";
                history.PrevValue = InputOrganization;
                history.CurValue = Organization;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            ObjectMapper.Map(input, leadSource);

            await _leadSourceRepository.UpdateAsync(leadSource);

            //Lead Source OrganizationUnits
            LeadSourceOrganizationUnit leadSourceOrganizationUnit;
            if (input.OrganizationUnits != null)
            {
                _leadSourceOrganizationUnitRepository.Delete(x => x.LeadSourceId == leadSource.Id);

                foreach (var item in input.OrganizationUnits)
                {
                    leadSourceOrganizationUnit = new LeadSourceOrganizationUnit();
                    leadSourceOrganizationUnit.LeadSourceId = leadSource.Id;
                    leadSourceOrganizationUnit.OrganizationUnitId = item;
                    _leadSourceOrganizationUnitRepository.InsertOrUpdate(leadSourceOrganizationUnit);
                }
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

        }

        [AbpAuthorize(AppPermissions.Pages_LeadSources_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _leadSourceRepository.Get(input.Id).Name;
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 2;
            dataVaultLog.ActionNote = "Lead Source Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _leadSourceRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetLeadSourcesToExcel(GetAllLeadSourcesForExcelInput input)
        {

            var filteredLeadSources = _leadSourceRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

            var query = (from o in filteredLeadSources
                         select new GetLeadSourceForViewDto()
                         {
                             LeadSource = new LeadSourceDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var leadSourceListDtos = await query.ToListAsync();

            return _leadSourcesExcelExporter.ExportToFile(leadSourceListDtos);
        }

    }
}