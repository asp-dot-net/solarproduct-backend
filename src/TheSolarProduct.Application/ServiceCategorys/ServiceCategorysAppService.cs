﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.HoldReasons.Exporting;
using TheSolarProduct.HoldReasons.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.ServiceCategorys.Dtos;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Jobs;
using TheSolarProduct.Migrations;

namespace TheSolarProduct.ServiceCategorys
{
    [AbpAuthorize(AppPermissions.Pages_ServiceCategory, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    public class ServiceCategorysAppService : TheSolarProductAppServiceBase, IServiceCategorysAppService
    {
        private readonly IRepository<ServiceCategory> _serviceCategorysRepository;
        private readonly IHoldReasonsExcelExporter _serviceCategorysExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public ServiceCategorysAppService(IRepository<ServiceCategory> serviceCategorysRepository, IHoldReasonsExcelExporter serviceCategorysExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _serviceCategorysRepository = serviceCategorysRepository;
            _serviceCategorysExcelExporter = serviceCategorysExcelExporter;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;

        }

        public async Task<PagedResultDto<GetServiceCategoryForViewDto>> GetAll(GetAllServiceCategoryInput input)
        {

            var filteredHoldReasons = _serviceCategorysRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

            var pagedAndFilteredHoldReasons = filteredHoldReasons
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var serviceCategory = from o in pagedAndFilteredHoldReasons
                              select new GetServiceCategoryForViewDto()
                              {
                                  serviceCategory = new ServiceCategoryDto
                                  {
                                      Name = o.Name,
                                      Id = o.Id,
                                      IsActive = o.IsActive,    
                                  }
                              };

            var totalCount = await filteredHoldReasons.CountAsync();

            return new PagedResultDto<GetServiceCategoryForViewDto>(
                totalCount,
                await serviceCategory.ToListAsync()
            );
        }

        public async Task<GetServiceCategoryForViewDto> GetServiceCategoryForView(int id)
        {
            var serviceCategory = await _serviceCategorysRepository.GetAsync(id);

            var output = new GetServiceCategoryForViewDto { serviceCategory = ObjectMapper.Map<ServiceCategoryDto>(serviceCategory) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceCategory_Edit)]
        public async Task<GetServiceCategoryForEditOutput> GetServiceCategoryForEdit(EntityDto input)
        {
            var serviceCategory = await _serviceCategorysRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetServiceCategoryForEditOutput { serviceCategorys = ObjectMapper.Map<CreateOrEditServiceCategoryDto>(serviceCategory) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditServiceCategoryDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceCategory_Create)]
        protected virtual async Task Create(CreateOrEditServiceCategoryDto input)
        {
            var servicecategory = ObjectMapper.Map<ServiceCategory>(input);

            if (AbpSession.TenantId != null)
            {
                servicecategory.TenantId = (int)AbpSession.TenantId;
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 32;
            dataVaultLog.ActionNote = "Service Category Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _serviceCategorysRepository.InsertAsync(servicecategory);
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceCategory_Edit)]
        protected virtual async Task Update(CreateOrEditServiceCategoryDto input)
        {
            var servicecategory = await _serviceCategorysRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 32;
            dataVaultLog.ActionNote = "Service Category Updated : " + servicecategory.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != servicecategory.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = servicecategory.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != servicecategory.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = servicecategory.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, servicecategory);
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceCategory_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _serviceCategorysRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 32;
            dataVaultLog.ActionNote = "Service Category Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _serviceCategorysRepository.DeleteAsync(input.Id);
        }

        //public async Task<FileDto> GetHoldReasonsToExcel(GetAllHoldReasonsForExcelInput input)
        //{

        //    var filteredHoldReasons = _serviceCategorysRepository.GetAll()
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobHoldReason.Contains(input.Filter))
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.HoldReasonFilter), e => e.JobHoldReason == input.HoldReasonFilter);

        //    var query = (from o in filteredHoldReasons
        //                 select new GetHoldReasonForViewDto()
        //                 {
        //                     JobHoldReason = new HoldReasonDto
        //                     {
        //                         JobHoldReason = o.JobHoldReason,
        //                         Id = o.Id
        //                     }
        //                 });

        //    var holdReasonListDtos = await query.ToListAsync();

        //    return _holdReasonsExcelExporter.ExportToFile(holdReasonListDtos);
        //}

    }
}