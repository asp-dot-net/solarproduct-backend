﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.WholesaleDeliveryOption;
using TheSolarProduct.WholesaleDeliveryOptions.Exporting;
using TheSolarProduct.WholesaleJobTypes.Dtos;
using TheSolarProduct.Wholesales.DataVault;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct.WholesaleTransportTypes.Dtos;
using Abp.Collections.Extensions;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Abp;
using TheSolarProduct.Wholesaleinvoicetype.Dtos;




namespace TheSolarProduct.WholesaleJobTypes
{
    public class WholesaleJobTypeAppService : TheSolarProductAppServiceBase, IWholesaleJobTypeAppService
    {
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;
        private readonly IRepository<WholesaleJobType> _wholesaleJobtypeRepository;
        
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;



        public WholesaleJobTypeAppService(
             IRepository<WholesaleJobType> jobtypeRepository,
        IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
             IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
           
        {


            _wholesaleJobtypeRepository = jobtypeRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;

        }


        public async Task<PagedResultDto<GetWholesaleJobTypeForViewDto>> GetAll(GetAllWholesaleJobTypeInput input)
        {
            var JobType = _wholesaleJobtypeRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFiltered = JobType
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFiltered
                         select new GetWholesaleJobTypeForViewDto()
                         {
                             JobType = new WholesaleJobTypeDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 IsActive = o.IsActive
                             }
                         };

            var totalCount = await JobType.CountAsync();

            return new PagedResultDto<GetWholesaleJobTypeForViewDto>(totalCount, await output.ToListAsync());
        }



        public async Task<GetWholesaleJobTypeForEditOutput> GetWholesaleJobTypeForEdit(EntityDto input)
        {
            var JobType = await _wholesaleJobtypeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetWholesaleJobTypeForEditOutput { JobType = ObjectMapper.Map<CreateOrEditWholesaleJobTypeDto>(JobType) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditWholesaleJobTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        protected virtual async Task Create(CreateOrEditWholesaleJobTypeDto input)
        {
            var JobType = ObjectMapper.Map<WholesaleJobType>(input);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 8;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "WholesaleJobType Created: " + input.Name;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

            await _wholesaleJobtypeRepository.InsertAsync(JobType);
        }

        protected virtual async Task Update(CreateOrEditWholesaleJobTypeDto input)
        {
            var JobType = await _wholesaleJobtypeRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 8;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "WholesaleJobType Updated : " + JobType.Name;
            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            var List = new List<WholeSaleDataVaultActivityLogHistory>();
            if (input.Name != JobType.Name)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = JobType.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != JobType.IsActive)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = JobType.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, JobType);

            await _wholesaleJobtypeRepository.UpdateAsync(JobType);
        }
        public async Task Delete(EntityDto input)
        {
            var Name = _wholesaleJobtypeRepository.Get(input.Id).Name;

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 8;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "WholesaleJobType Deleted  : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            await _wholesaleJobtypeRepository.DeleteAsync(input.Id);
        }

    }
}
