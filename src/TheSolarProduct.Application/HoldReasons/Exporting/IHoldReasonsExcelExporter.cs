﻿using System.Collections.Generic;
using TheSolarProduct.HoldReasons.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.HoldReasons.Exporting
{
    public interface IHoldReasonsExcelExporter
    {
        FileDto ExportToFile(List<GetHoldReasonForViewDto> holdReasons);
    }
}