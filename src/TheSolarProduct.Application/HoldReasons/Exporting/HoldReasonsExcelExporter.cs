﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.HoldReasons.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.HoldReasons.Exporting
{
    public class HoldReasonsExcelExporter : NpoiExcelExporterBase, IHoldReasonsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public HoldReasonsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetHoldReasonForViewDto> holdReasons)
        {
            return CreateExcelPackage(
                "HoldReasons.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("HoldReasons"));

                    AddHeader(
                        sheet,
                        L("HoldReason"),
                        L("IsActive")
                        );

                    AddObjects(
                        sheet, 2, holdReasons,
                        _ => _.JobHoldReason.JobHoldReason,
                        _ => _.JobHoldReason.IsActive ? L("Yes") : L("No")
                        );

                });
        }
    }
}