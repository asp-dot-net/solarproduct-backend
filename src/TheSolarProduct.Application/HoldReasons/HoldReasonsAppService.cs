﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.HoldReasons.Exporting;
using TheSolarProduct.HoldReasons.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.RejectReasons;

namespace TheSolarProduct.HoldReasons
{
    [AbpAuthorize(AppPermissions.Pages_HoldReasons, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    public class HoldReasonsAppService : TheSolarProductAppServiceBase, IHoldReasonsAppService
    {
        private readonly IRepository<HoldReason> _holdReasonRepository;
        private readonly IHoldReasonsExcelExporter _holdReasonsExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public HoldReasonsAppService(IRepository<HoldReason> holdReasonRepository, IHoldReasonsExcelExporter holdReasonsExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _holdReasonRepository = holdReasonRepository;
            _holdReasonsExcelExporter = holdReasonsExcelExporter;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;

        }

        public async Task<PagedResultDto<GetHoldReasonForViewDto>> GetAll(GetAllHoldReasonsInput input)
        {

            var filteredHoldReasons = _holdReasonRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobHoldReason.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.HoldReasonFilter), e => e.JobHoldReason == input.HoldReasonFilter);

            var pagedAndFilteredHoldReasons = filteredHoldReasons
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var holdReasons = from o in pagedAndFilteredHoldReasons
                              select new GetHoldReasonForViewDto()
                              {
                                  JobHoldReason = new HoldReasonDto
                                  {
                                      JobHoldReason = o.JobHoldReason,
                                      Id = o.Id,
                                      IsActive = o.IsActive,
                                  }
                              };

            var totalCount = await filteredHoldReasons.CountAsync();

            return new PagedResultDto<GetHoldReasonForViewDto>(
                totalCount,
                await holdReasons.ToListAsync()
            );
        }

        public async Task<GetHoldReasonForViewDto> GetHoldReasonForView(int id)
        {
            var holdReason = await _holdReasonRepository.GetAsync(id);

            var output = new GetHoldReasonForViewDto { JobHoldReason = ObjectMapper.Map<HoldReasonDto>(holdReason) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_HoldReasons_Edit)]
        public async Task<GetHoldReasonForEditOutput> GetHoldReasonForEdit(EntityDto input)
        {
            var holdReason = await _holdReasonRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetHoldReasonForEditOutput { JobHoldReason = ObjectMapper.Map<CreateOrEditHoldReasonDto>(holdReason) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditHoldReasonDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_HoldReasons_Create)]
        protected virtual async Task Create(CreateOrEditHoldReasonDto input)
        {
            var holdReason = ObjectMapper.Map<HoldReason>(input);

            if (AbpSession.TenantId != null)
            {
                holdReason.TenantId = (int)AbpSession.TenantId;
            }
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 7;
            dataVaultLog.ActionNote = "Hold Reason Created : " + input.JobHoldReason;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _holdReasonRepository.InsertAsync(holdReason);
        }

        [AbpAuthorize(AppPermissions.Pages_HoldReasons_Edit)]
        protected virtual async Task Update(CreateOrEditHoldReasonDto input)
        {
            var holdReason = await _holdReasonRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 7;
            dataVaultLog.ActionNote = "Hold Reason Updated : " + holdReason.JobHoldReason;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.JobHoldReason != holdReason.JobHoldReason)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "JobHoldReason";
                history.PrevValue = holdReason.JobHoldReason;
                history.CurValue = input.JobHoldReason;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != holdReason.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = holdReason.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();


            ObjectMapper.Map(input, holdReason);
        }

        [AbpAuthorize(AppPermissions.Pages_HoldReasons_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _holdReasonRepository.Get(input.Id).JobHoldReason;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 7;
            dataVaultLog.ActionNote = "Hold Reason Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _holdReasonRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetHoldReasonsToExcel(GetAllHoldReasonsForExcelInput input)
        {

            var filteredHoldReasons = _holdReasonRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobHoldReason.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.HoldReasonFilter), e => e.JobHoldReason == input.HoldReasonFilter);

            var query = (from o in filteredHoldReasons
                         select new GetHoldReasonForViewDto()
                         {
                             JobHoldReason = new HoldReasonDto
                             {
                                 JobHoldReason = o.JobHoldReason,
                                 Id = o.Id,
                                 IsActive = o.IsActive,
                             }
                         });

            var holdReasonListDtos = await query.ToListAsync();

            return _holdReasonsExcelExporter.ExportToFile(holdReasonListDtos);
        }

    }
}