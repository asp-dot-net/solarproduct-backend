﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using TheSolarProduct.StockOrder.Dtos;
using System.Linq.Dynamic.Core;
using TheSolarProduct.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using Abp.Collections.Extensions;
using TheSolarProduct.StockOrders.Exporting;
using Abp.Timing.Timezone;
using TheSolarProduct.StockPaymentTrackers.Dtos;
using TheSolarProduct.StockOrders;
using TheSolarProduct.StockPayments;
using System.Collections.Generic;

namespace TheSolarProduct.StockPaymentTrackers
{
    public class StockPaymentTrackerAppService : TheSolarProductAppServiceBase, IStockPaymentTrackerAppService
    {
        private readonly IStockOrderExcelExporter _stockOrderExcelExporter;
        private readonly IRepository<PurchaseOrder> _purchaseOrderRepository;
        private readonly IRepository<PurchaseOrderItem> _purchaseOrderItemRepository;
        private readonly IRepository<StockPaymentOrderList> _stockPaymentOrderListRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;

        public StockPaymentTrackerAppService(
               IStockOrderExcelExporter stockOrderExcelExporter,
               IRepository<PurchaseOrder> purchaseOrderRepository,
               IRepository<PurchaseOrderItem> purchaseOrderItemRepository,
               IRepository<StockPaymentOrderList> stockPaymentOrderListRepository,
               ITimeZoneConverter timeZoneConverter
            )
        {
            _purchaseOrderRepository = purchaseOrderRepository;
            _purchaseOrderItemRepository = purchaseOrderItemRepository;
            _stockPaymentOrderListRepository = stockPaymentOrderListRepository;
            _timeZoneConverter = timeZoneConverter;
            _stockOrderExcelExporter = stockOrderExcelExporter;
        }

        public async Task<PagedResultDto<GetAllStockPaymentTrackerWithSummary>> GetAll(GetAllStockPaymentTrackerInput input)
        {
            // Convert dates using timezone converter
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            // Query PurchaseOrder with filters
            var purchaseOrderQuery = _purchaseOrderRepository.GetAll()
                .Include(e => e.PurchaseCompanyFk)
                .Include(e => e.StockOrderForFk)
                .Include(e => e.StockFromFk)
                .Include(e => e.PaymentTypeFk)
                .Include(e => e.PaymentStatusFk)
                .Include(e => e.WarehouseLocationFk)
                .Include(e => e.CurrencyFk)
                .Include(e => e.DeliveryTypeFk)
                .Include(e => e.StockOrderStatusFk)
                .Include(e => e.TransportCompanyFk)
                .Include(e => e.VendorFK)
                .WhereIf(input.FilterName == "ContainerNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ContainerNo.Contains(input.Filter))
                .WhereIf(input.FilterName == "ManualOrderNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ManualOrderNo.Contains(input.Filter))
                .WhereIf(input.FilterName == "VendorInoviceNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.VendorInoviceNo.Contains(input.Filter))
                .WhereIf(input.FilterName == "OrderNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.OrderNo.ToString().Contains(input.Filter))
                .WhereIf(input.StockOrderStatusIds != null && input.StockOrderStatusIds.Any(), e => input.StockOrderStatusIds.Contains(e.StockOrderStatusFk.Id))
                .WhereIf(input.WarehouseLocationIdFiletr != null && input.WarehouseLocationIdFiletr.Any(), e => input.WarehouseLocationIdFiletr.Contains(e.WarehouseLocationFk.Id))
                .WhereIf(input.PaymentStatusIdFiletr > 0, e => e.PaymentStatusId == input.PaymentStatusIdFiletr)
                .WhereIf(input.PaymentTypeIdFiletr != null && input.PaymentTypeIdFiletr.Any(), e => input.PaymentTypeIdFiletr.Contains(e.PaymentTypeFk.Id))
                .WhereIf(input.VendorIdFiletr > 0, e => e.VendorId == input.VendorIdFiletr)
                .WhereIf(input.GstTypeIdFilter > 0, e => e.GSTType == input.GstTypeIdFilter)
                .WhereIf(input.DeliveryTypeIdFiletr > 0, e => e.DeliveryTypeId == input.DeliveryTypeIdFiletr)
                .WhereIf(input.StockOrderIdFiletr > 0, e => e.StockOrderForId == input.StockOrderIdFiletr)
                .WhereIf(input.TransportCompanyFilter > 0, e => e.TransportCompanyId == input.TransportCompanyFilter)
                .WhereIf(input.Datefilter == "OrderDate" && input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                .WhereIf(input.Datefilter == "OrderDate" && input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                .Where(e => e.OrganizationId == input.OrganizationId);

            
            var pagedAndFilteredOrders = await purchaseOrderQuery
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input)
                .ToListAsync();

            
            var stockPaymentTrackerList = new List<GetAllStockPaymentTrackerOutputDto>();

            foreach (var order in pagedAndFilteredOrders)
            {
                var products = await _purchaseOrderItemRepository.GetAll()
                    .Where(e => e.PurchaseOrderId == order.Id)
                    .ToListAsync();

                var payments = await _stockPaymentOrderListRepository.GetAll()
                    .Where(e => e.PurchaseOrderId == order.Id)
                    .ToListAsync();

                var paidAmount = payments.Sum(e => e.Amount);
                var totalAmount = products.Sum(e => e.Amount ?? 0);
                var quantity = products.Sum(e => e.Quantity ?? 0);

                stockPaymentTrackerList.Add(new GetAllStockPaymentTrackerOutputDto
                {
                    Id = order.Id,
                    OrderNo = order.OrderNo,
                    VendorInoviceNo = order.VendorInoviceNo,
                    ManualOrderNo = order.ManualOrderNo,
                    PaymentMethod = order.PaymentTypeFk.Name,
                    WarehouseLocation = order.WarehouseLocationFk.location,
                    Currency = order.CurrencyFk.Name,
                    OrderDate = order.CreationTime,
                    DeliveryDate = order.PhysicalDeliveryDate,
                    GSTType = order.GSTType == 1 ? "With GST" : order.GSTType == 2 ? "Without GST" : "",
                    QTY = quantity,
                    Amount = totalAmount,
                    PaidAmount = paidAmount,
                    RemAmount = totalAmount - paidAmount
                });
            }

            
            var summary = new GetAllStockPaymentTrackerWithSummary
            {
                StockPaymentTrackerList = stockPaymentTrackerList,
                Total = stockPaymentTrackerList.Count(),
                Amount = (int?)stockPaymentTrackerList.Sum(x => x.Amount),
                PaidAmount = (int?)stockPaymentTrackerList.Sum(x => x.PaidAmount),
                RemainingAmount = (int?)stockPaymentTrackerList.Sum(x => x.RemAmount)
            };

            var totalCount = await purchaseOrderQuery.CountAsync();

            return new PagedResultDto<GetAllStockPaymentTrackerWithSummary>(
                totalCount,
                new List<GetAllStockPaymentTrackerWithSummary> { summary }
            );
        }

        public async Task<FileDto> GetStockOrderToExcel(GetStockorderForExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var PurchaseOrder = _purchaseOrderRepository.GetAll()
                    .Include(e => e.PurchaseCompanyFk)
                    .Include(e => e.StockOrderForFk)
                    .Include(e => e.StockFromFk)
                    .Include(e => e.PaymentTypeFk)
                    .Include(e => e.PaymentStatusFk)
                    .Include(e => e.WarehouseLocationFk)
                    .Include(e => e.CurrencyFk)
                    .Include(e => e.DeliveryTypeFk)
                    .Include(e => e.StockOrderStatusFk)
                    .Include(e => e.TransportCompanyFk)
                    .Include(e => e.VendorFK)
                    .WhereIf(input.FilterName == "ContainerNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ContainerNo.Contains(input.Filter))
                    .WhereIf(input.FilterName == "ManualOrderNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ManualOrderNo.Contains(input.Filter))
                    .WhereIf(input.FilterName == "VendorInoviceNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.VendorInoviceNo.Contains(input.Filter))
                    .WhereIf(input.FilterName == "OrderNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.OrderNo.ToString().Contains(input.Filter))
                    .WhereIf(input.StockOrderStatusIds != null && input.StockOrderStatusIds.Count() > 0, e => input.StockOrderStatusIds.Contains((e.StockOrderStatusFk.Id)))
                    .WhereIf(input.WarehouseLocationIdFiletr != null && input.WarehouseLocationIdFiletr.Count() > 0, e => input.WarehouseLocationIdFiletr.Contains((e.WarehouseLocationFk.Id)))
                    .WhereIf(input.PaymentStatusIdFiletr > 0, e => e.PaymentStatusId == input.PaymentStatusIdFiletr)
                    .WhereIf(input.PaymentTypeIdFiletr != null && input.PaymentTypeIdFiletr.Count() > 0, e => input.PaymentTypeIdFiletr.Contains((e.PaymentTypeFk.Id)))
                    .WhereIf(input.VendorIdFiletr > 0, e => e.VendorId == input.VendorIdFiletr)
                    .WhereIf(input.GstTypeIdFilter > 0, e => e.GSTType == input.GstTypeIdFilter)
                    .WhereIf(input.DeliveryTypeIdFiletr > 0, e => e.DeliveryTypeId == input.DeliveryTypeIdFiletr)
                    .WhereIf(input.StockOrderIdFiletr > 0, e => e.StockOrderForId == input.StockOrderIdFiletr)
                    .WhereIf(input.TransportCompanyFilter > 0, e => e.TransportCompanyId == input.TransportCompanyFilter)
                    .WhereIf(input.Datefilter == "OrderDate" && input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                    .WhereIf(input.Datefilter == "OrderDate" && input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                    .Where(e => e.OrganizationId == input.OrganizationId);


            var outputstockOrder = from o in PurchaseOrder
                                   let Prod = _purchaseOrderItemRepository.GetAll().Where(e => e.PurchaseOrderId == o.Id)
                                   let paid = _stockPaymentOrderListRepository.GetAll().Where(e => e.PurchaseOrderId == o.Id).Sum(e => e.Amount)
                                   select new GetAllStockPaymentTrackerOutputDto()
                                   {
                                       Id = o.Id,
                                       OrderNo = o.OrderNo,
                                       VendorInoviceNo = o.VendorInoviceNo,
                                       ManualOrderNo = o.ManualOrderNo,
                                       PaymentMethod = o.PaymentTypeFk.Name,
                                       WarehouseLocation = o.WarehouseLocationFk.location,
                                       Currency = o.CurrencyFk.Name,
                                       OrderDate = o.CreationTime,
                                       DeliveryDate = o.PhysicalDeliveryDate,
                                       GSTType = o.GSTType == 1 ? "With GST" : o.GSTType == 2 ? "Without GST" : "",
                                       QTY = Prod.Sum(e => e.Quantity ?? 0),
                                       Amount = Prod.Sum(e => e.Amount ?? 0),
                                       PaidAmount = paid,
                                       RemAmount = Prod.Sum(e => e.Amount ?? 0) - paid
                                   };

            return  _stockOrderExcelExporter.PaymentTrackerExportToFile(await outputstockOrder.ToListAsync());

        }
    }
}
