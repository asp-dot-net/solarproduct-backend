﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;

using TheSolarProduct.DocumentTypes.Dtos;
using TheSolarProduct.Quotations;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.DocumentTypes
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_datavault_DocumentType)]
    public class DocumentTypeAppService : TheSolarProductAppServiceBase, IDocumentTypeAppService
    {
        private readonly IRepository<DocumentType> _documenttypeRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public DocumentTypeAppService(IRepository<DocumentType> documenttypeRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _documenttypeRepository = documenttypeRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
        }

        public async Task<PagedResultDto<GetDocumentTypeForViewDto>> GetAll(GetAllDocumentTypeInput input)
        {

            var filteredDepartments = _documenttypeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Title.Contains(input.Filter));

            var pagedAndFilteredDepartments = filteredDepartments
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var departments = from o in pagedAndFilteredDepartments
                              select new GetDocumentTypeForViewDto()
                              {
                                  Documenttype = new DocumentTypeDto
                                  {
                                      Title = o.Title,
                                      Id = o.Id,
                                     IsActive= o.IsActive
                                  }
                              };

            var totalCount = await filteredDepartments.CountAsync();

            return new PagedResultDto<GetDocumentTypeForViewDto>(
                totalCount,
                await departments.ToListAsync()
            );
        }

        public async Task<GetDocumentTypeForViewDto> GetDepartmentForView(int id)
        {
            var document = await _documenttypeRepository.GetAsync(id);

            var output = new GetDocumentTypeForViewDto { Documenttype = ObjectMapper.Map<DocumentTypeDto>(document) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_datavault_DocumentType_Edit)]
        public async Task<GetDocumentTypeforEditOutput> GetDepartmentForEdit(EntityDto input)
        {
            var documentType = await _documenttypeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetDocumentTypeforEditOutput { DocumenttypeDto = ObjectMapper.Map<CreateorEditDocumentTypeDto>(documentType) };

            return output;
        }

        public async Task CreateOrEdit(CreateorEditDocumentTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_datavault_DocumentType_Create)]
        protected virtual async Task Create(CreateorEditDocumentTypeDto input)
        {

            var documenttype = ObjectMapper.Map<DocumentType>(input);

            if (AbpSession.TenantId != null)
            {
                documenttype.TenantId = (int)AbpSession.TenantId;
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 29;
            dataVaultLog.ActionNote = "Document Type Created : " + input.Title;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _documenttypeRepository.InsertAsync(documenttype);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_datavault_DocumentType_Edit)]
        protected virtual async Task Update(CreateorEditDocumentTypeDto input)
        {
            var documenttype = await _documenttypeRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 29;
            dataVaultLog.ActionNote = "Document Type Updated : " + documenttype.Title;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Title != documenttype.Title)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = documenttype.Title;
                history.CurValue = input.Title;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.IsActive != documenttype.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = documenttype.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, documenttype);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_datavault_DocumentType_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _documenttypeRepository.Get(input.Id).Title;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 29;
            dataVaultLog.ActionNote = "Document Type Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _documenttypeRepository.DeleteAsync(input.Id);
        }
    }
}
