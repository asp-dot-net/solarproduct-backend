﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.WholeSalePVDStatuses;
using TheSolarProduct.WholeSalePVDStatuses.Dtos;
using TheSolarProduct.Wholesales.DataVault;
using TheSolarProduct.Wholesales.Ecommerces;
using Abp.Collections.Extensions;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Abp;
using TheSolarProduct.WholesaleTransportTypes.Dtos;



namespace TheSolarProduct.WholesalePVDStatuses
{
    public class WholesalePVDStatusAppService : TheSolarProductAppServiceBase, IWholesalePVDStatusAppService
    {
        private readonly IRepository<WholesalePVDStatus> _wholesalePVDStatusRepository;
       
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;

        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public WholesalePVDStatusAppService(

            IRepository<WholesalePVDStatus> WholesalePVDStatusRepository,
             // IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
             IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)

        {

            _wholesalePVDStatusRepository = WholesalePVDStatusRepository;
            // _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;

        }

        public async Task<PagedResultDto<GetWholesalePVDStatusForViewDto>> GetAll(GetAllWholesalePVDStatusInput input)
        {
            var pvdstatus = _wholesalePVDStatusRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFiltered = pvdstatus
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFiltered
                         select new GetWholesalePVDStatusForViewDto()
                         {
                             PVDStatus = new WholesalePVDStatusDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 IsActive = o.IsActive
                             }
                         };

            var totalCount = await pvdstatus.CountAsync();

            return new PagedResultDto<GetWholesalePVDStatusForViewDto>(totalCount, await output.ToListAsync());
        }

        public async Task<GetWholesalePVDStatusForEditOutput> GetWholesalePVDStatusForEdit(EntityDto input)
        {
            var pvdstatus = await _wholesalePVDStatusRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetWholesalePVDStatusForEditOutput { PVDStatus = ObjectMapper.Map<CreateOrEditWholesalePVDStatusDto>(pvdstatus) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditWholesalePVDStatusDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        protected virtual async Task Create(CreateOrEditWholesalePVDStatusDto input)
        {
            var pvdstatus = ObjectMapper.Map<WholesalePVDStatus>(input);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 10;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "WholesalePVDStatus Created: " + input.Name;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

            await _wholesalePVDStatusRepository.InsertAsync(pvdstatus);
        }

        protected virtual async Task Update(CreateOrEditWholesalePVDStatusDto input)
        {
            var pvdstatus = await _wholesalePVDStatusRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 10;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "WholesalePVDStatus Updated : " + pvdstatus.Name;
            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            var List = new List<WholeSaleDataVaultActivityLogHistory>();
            if (input.Name != pvdstatus.Name)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = pvdstatus.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != pvdstatus.IsActive)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = pvdstatus.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, pvdstatus);

            await _wholesalePVDStatusRepository.UpdateAsync(pvdstatus);
        }

        public async Task Delete(EntityDto input)
        {
            var Name = _wholesalePVDStatusRepository.Get(input.Id).Name;

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 10;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "WholesalePVDStatus Deleted  : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            await _wholesalePVDStatusRepository.DeleteAsync(input.Id);
        }

    }
}
