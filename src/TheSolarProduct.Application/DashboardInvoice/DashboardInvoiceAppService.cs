﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Timing.Timezone;
using Hangfire.Common;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using NPOI.SS.Formula.Functions;
using Org.BouncyCastle.Crypto.Tls;
using Stripe;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.DashboardInvoice.Dtos;
using TheSolarProduct.DashboardMessages.Dtos;
using TheSolarProduct.DashboardSales.Dtos;
using TheSolarProduct.Invoices;
using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Leads;
using TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos;
using TheSolarProduct.States;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Timing;
using Twilio.TwiML.Fax;
using Job = TheSolarProduct.Jobs.Job;

namespace TheSolarProduct.DashboardInvoice
{
    public class DashboardInvoiceAppService : TheSolarProductAppServiceBase, IDashboardInvoiceAppService
    {
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<InvoiceImportData> _InvoiceImportDataRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly UserManager _userManager;
        private readonly ITimeZoneService _timeZoneService;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
        public DashboardInvoiceAppService(
            IRepository<Job> jobRepository,
            IRepository<InvoiceImportData> InvoiceImportDataRepository,
            IRepository<UserTeam> userTeamRepository,
            IRepository<User, long> userRepository,
            UserManager userManager,
            ITimeZoneService timeZoneService,
            ITimeZoneConverter timeZoneConverter,
            IRepository<InvoicePayment> invoicePaymentRepository
            ) 
        { 
            _jobRepository = jobRepository;
            _InvoiceImportDataRepository = InvoiceImportDataRepository;
            _userTeamRepository = userTeamRepository;
            _userRepository = userRepository;
            _userManager = userManager;
            _timeZoneService = timeZoneService;
            _timeZoneConverter = timeZoneConverter;
            _invoicePaymentRepository = invoicePaymentRepository;
        }

        public async Task<GetInvoiceOverviewDto> GetInvoiceOverview(GetInput input)
        {
            int UserId = (int)AbpSession.UserId;
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var user_teamList = await _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId }).ToListAsync();

            var TeamId = user_teamList.Where(e => e.UserId == UserId).Select(e => e.TeamId).ToList();

            var UserIds = user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var User = await _userRepository.GetAsync(UserId);
            IList<string> RoleName = await _userManager.GetRolesAsync(User);
            var result = await _jobRepository.GetAll()
                                .Include(e => e.LeadFk)
                                .Include(e => e.JobStatusFk)
                                .Where(e => e.LeadFk.OrganizationId == input.OrgId)
                                .WhereIf(RoleName.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                                .WhereIf(RoleName.Contains("Sales Manager"), e => UserIds.Contains(e.LeadFk.AssignToUserID))
                                .WhereIf(RoleName.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                                .WhereIf(input.StartDate != null, e => (_invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.CreationTime).FirstOrDefault().AddHours(diffHour).Date) >= SDate.Value.Date)
                                .WhereIf(input.EndDate != null, e => (_invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.CreationTime).FirstOrDefault().AddHours(diffHour).Date) <= EDate.Value.Date)
                                .GroupBy( e => e.JobStatusId)
                                .Select(e => new {status = e.Key, cost = e.Sum(s => s.TotalCost)})
                                .ToListAsync();

            var invoiceOverview = new GetInvoiceOverviewDto();
            invoiceOverview.Active = result.Where(e => e.status == 5).Any() ? result.Where(e => e.status == 5).FirstOrDefault().cost : 0;
            invoiceOverview.Deposite = result.Where(e => e.status == 4).Any() ? result.Where(e => e.status == 4).FirstOrDefault().cost : 0;
            invoiceOverview.JobBook = result.Where(e => e.status == 6).Any() ? result.Where(e => e.status == 6).FirstOrDefault().cost : 0;
            invoiceOverview.Installed = result.Where(e => e.status == 8).Any() ? result.Where(e => e.status == 8).FirstOrDefault().cost : 0;
            invoiceOverview.InComplete = result.Where(e => e.status == 7).Any() ? result.Where(e => e.status == 7).FirstOrDefault().cost : 0;
            invoiceOverview.Total = result.Sum(e => e.cost);

            return invoiceOverview;
        }

        public async Task<GetCashInvoiceFinanceInvoiceStateWiseOwingDto> GetCashInvoice_financeInvoice_StateWiseOwing(GetInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var filteredInvoiceIssued = _jobRepository.GetAll().Include(e => e.LeadFk).Include(e => e.JobStatusFk).Include(e => e.PaymentOptionFk)
                        .Where(e => e.LeadFk.OrganizationId == input.OrgId && (e.FirstDepositDate != null || e.InstallationDate != null))
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)
                        .AsNoTracking()
                        .Select(e => new {e.Id, e.TotalCost, e.PaymentOptionId, e.LeadFk.State })
                        ;

            var CashInvoice = filteredInvoiceIssued.Where(e => e.PaymentOptionId == 1);
            var FinanceInvoice = filteredInvoiceIssued.Where(e => e.PaymentOptionId != 1);

            var InvoiceImportData = _InvoiceImportDataRepository.GetAll().AsNoTracking().Select(e => new { e.Id, e.JobId, e.PaidAmmount, e.InvoicePaymentmothodName });

            var C_Received = await (from o in CashInvoice
                                                join Invoice in InvoiceImportData on o.Id equals Invoice.JobId
                                                select Invoice.PaidAmmount
                                      ).SumAsync();

            var C_TotalAmount = await CashInvoice.Select(e => e.TotalCost).SumAsync();

            var F_Received = await (from o in FinanceInvoice
                                    join Invoice in InvoiceImportData on o.Id equals Invoice.JobId
                                    select Invoice.PaidAmmount
                                      ).SumAsync();

            var F_TotalAmount = await FinanceInvoice.Select(e => e.TotalCost).SumAsync();

            var NSWInvoice = filteredInvoiceIssued.Where(e => e.State == "NSW");
            var SAInvoice = filteredInvoiceIssued.Where(e => e.State == "SA");
            var QLDInvoice = filteredInvoiceIssued.Where(e => e.State == "QLD");
            var VICInvoice = filteredInvoiceIssued.Where(e => e.State == "VIC");

            var NSW_Received = await (from o in NSWInvoice
                                      join Invoice in InvoiceImportData on o.Id equals Invoice.JobId
                                      select Invoice.PaidAmmount
                                    ).SumAsync();

            var NSW_TotalAmount = await NSWInvoice.Select(e => e.TotalCost).SumAsync();

            var SA_Received = await (from o in SAInvoice
                                     join Invoice in InvoiceImportData on o.Id equals Invoice.JobId
                                     select Invoice.PaidAmmount
                                    ).SumAsync();

            var SA_TotalAmount = await SAInvoice.Select(e => e.TotalCost).SumAsync();

            var QLD_Received = await (from o in QLDInvoice
                                      join Invoice in InvoiceImportData on o.Id equals Invoice.JobId
                                      select Invoice.PaidAmmount
                                    ).SumAsync();

            var QLD_TotalAmount = await QLDInvoice.Select(e => e.TotalCost).SumAsync();

            var VIC_Received = await (from o in VICInvoice
                                      join Invoice in InvoiceImportData on o.Id equals Invoice.JobId
                                      select Invoice.PaidAmmount
                                    ).SumAsync();

            var VIC_TotalAmount = await VICInvoice.Select(e => e.TotalCost).SumAsync();

            var result = new GetCashInvoiceFinanceInvoiceStateWiseOwingDto();
            result.C_Receive = C_Received;
            result.C_Owing = C_TotalAmount - C_Received;
            result.C_Total = result.C_Receive + result.C_Owing;
            result.F_Receive = F_Received;
            result.F_Owing = F_TotalAmount - F_Received;
            result.F_Total = result.F_Receive + result.F_Owing;
            result.SA = SA_TotalAmount - SA_Received;
            result.NSW = NSW_TotalAmount - NSW_Received;
            result.QLD = QLD_TotalAmount - QLD_Received;
            result.VIC = VIC_TotalAmount - VIC_Received;

            return result;
        }
    }
}
