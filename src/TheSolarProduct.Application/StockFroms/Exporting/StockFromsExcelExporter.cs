﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckApplications.Dtos;
using TheSolarProduct.CheckApplications.Exporting;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.StockFroms.Dtos;
using TheSolarProduct.Storage;

namespace TheSolarProduct.StockFroms.Exporting
{
    public class StockFromsExcelExporter : NpoiExcelExporterBase, IStockFromsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public StockFromsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetStockFromsForViewDto> stockfroms)
        {
            return CreateExcelPackage(
                "StockFroms.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("StockFroms");

                    AddHeader(
                        sheet,
                        ("Name"),
                        ("IsActive")
                    );

                    AddObjects(
                        sheet,
                        2,
                        stockfroms,
                        _ => _.stockfroms.Name,
                        _ => _.stockfroms.IsActive 
                    );
                }
            );
        }



    }
}
