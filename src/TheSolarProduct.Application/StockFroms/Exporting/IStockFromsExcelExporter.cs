﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckApplications.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.StockFroms.Dtos;

namespace TheSolarProduct.StockFroms.Exporting
{
    public interface IStockFromsExcelExporter
    {
        FileDto ExportToFile(List<GetStockFromsForViewDto> stockfrom);
    }
}
