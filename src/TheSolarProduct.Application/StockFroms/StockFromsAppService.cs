﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using System.Linq;
using TheSolarProduct.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using TheSolarProduct.StockFroms.Dtos;
using TheSolarProduct.TransportationCosts;
using Abp;
using Abp.Domain.Repositories;
using TheSolarProduct.StockFroms.Exporting;
using TheSolarProduct.CheckDepositeReceive.Exporting;


namespace TheSolarProduct.StockFroms
{
    public class StockFromsAppService : TheSolarProductAppServiceBase, IStockFromsAppService
    {
        private readonly IRepository<StockFrom> _stockfromsRepository;
        private readonly IStockFromsExcelExporter _stockfromsExcelExporter;

        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public StockFromsAppService(IRepository<StockFrom> stockfromsRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider, IStockFromsExcelExporter stockfromsExcelExporter)
        {
             _stockfromsExcelExporter = stockfromsExcelExporter;
            _stockfromsRepository = stockfromsRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;

        }

        public async Task<PagedResultDto<GetStockFromsForViewDto>> GetAll(GetAllStockFromsInput input)
        {
            var stockfrom = _stockfromsRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFiltered = stockfrom
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFiltered
                         select new GetStockFromsForViewDto()
                         {
                             stockfroms = new StockFromsDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 IsActive = o.IsActive
                             }
                         };

            var totalCount = await stockfrom.CountAsync();

            return new PagedResultDto<GetStockFromsForViewDto>(totalCount, await output.ToListAsync());
        }



        public async Task<GetStockFromsForEditOutput> GetStockFromsForEdit(EntityDto input)
        {
            var stockfrom = await _stockfromsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetStockFromsForEditOutput { stockfroms = ObjectMapper.Map<CreateOrEditStockFromsDto>(stockfrom) };

            return output;
        }


        public async Task CreateOrEdit(CreateOrEditStockFromsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }



        protected virtual async Task Create(CreateOrEditStockFromsDto input)
        {
            var stockfrom = ObjectMapper.Map<StockFrom>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 68;
            dataVaultLog.ActionNote = "StockFroms Created: " + input.Name;
            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _stockfromsRepository.InsertAsync(stockfrom);
        }


        protected virtual async Task Update(CreateOrEditStockFromsDto input)
        {
            var stockfrom = await _stockfromsRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 68;
            dataVaultLog.ActionNote = "Stockfrom Updated : " + stockfrom.Name;
            dataVaultLog.IsInventory = true;
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            var List = new List<DataVaultActivityLogHistory>();
            if (input.Name != stockfrom.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = stockfrom.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != stockfrom .IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = stockfrom.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, stockfrom);

            await _stockfromsRepository.UpdateAsync(stockfrom);
        }

        public async Task Delete(EntityDto input)
        {
            var Name = _stockfromsRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 68;
            dataVaultLog.ActionNote = "StockFroms Deleted  : " + Name;

            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _stockfromsRepository.DeleteAsync(input.Id);
        }


        public async Task<FileDto> GetStockFromsToExcel(GetAllStockFromsForExcelInput input)
        {

            var filteredStockFroms = _stockfromsRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredStockFroms
                         select new GetStockFromsForViewDto()
                         {
                             stockfroms = new StockFromsDto
                             {
                                 Name = o.Name,
                                 Id = o.Id,
                                 IsActive = o.IsActive
                             }
                         });


            var ListDtos = await query.ToListAsync();

            return _stockfromsExcelExporter.ExportToFile(ListDtos);
        }



    }

}
