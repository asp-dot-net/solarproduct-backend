﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using System.Linq;
using System.Threading.Tasks;
using TheSolarProduct.QuotationTemplate.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using Abp.Organizations;
using TheSolarProduct.Jobs.Dtos;
using System.Collections.Generic;
using TheSolarProduct.WholeSaleLeads;
using TheSolarProduct.WholeSaleEmailTemplates.Dto;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Wholesales.DataVault;
using TheSolarProduct.WholeSaleSmsTemplates;

namespace TheSolarProduct.WholeSaleEmailTemplates
{
    [AbpAuthorize]
    public class WholeSaleEmailTamplateAppService : TheSolarProductAppServiceBase, IWholeSaleEmailTamplateAppService
    {
        private readonly IRepository<WholeSaleEmailTemplate> _wholeSaleEmailTemplateRepqository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<WholeSaleLead, int> _wholeSaleLeadRepository;
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public WholeSaleEmailTamplateAppService(
            IRepository<WholeSaleEmailTemplate> wholeSaleEmailTemplateRepqository
            , IRepository<OrganizationUnit, long> organizationUnitRepository
            , IRepository<WholeSaleLead, int> wholeSaleLeadRepository,
             IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository,
             IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _wholeSaleEmailTemplateRepqository = wholeSaleEmailTemplateRepqository;
            _organizationUnitRepository = organizationUnitRepository;
            _wholeSaleLeadRepository = wholeSaleLeadRepository;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task CreateOrEdit(CreateOrEditWholeSaleEmailTemplateDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(CreateOrEditWholeSaleEmailTemplateDto input)
        {
            try
            {
                var result = ObjectMapper.Map<WholeSaleEmailTemplate>(input);

                if (AbpSession.TenantId != null)
                {
                    result.TenantId = (int)AbpSession.TenantId;
                }
                var dataVaultLog = new WholeSaleDataVaultActivityLogs();
                dataVaultLog.Action = "Created";
                dataVaultLog.SectionId = 4;
                dataVaultLog.ActionNote = "Email Templates Created : " + input.TemplateName;
                await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

                await _wholeSaleEmailTemplateRepqository.InsertAndGetIdAsync(result);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        protected virtual async Task Update(CreateOrEditWholeSaleEmailTemplateDto input)
        {
            var result = await _wholeSaleEmailTemplateRepqository.GetAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 4;
            dataVaultLog.ActionNote = "Email Templates Updated : " + result.TemplateName;

            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<WholeSaleDataVaultActivityLogHistory>();

            if (input.TemplateName != result.TemplateName)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "TemplateName";
                history.PrevValue = result.TemplateName;
                history.CurValue = input.TemplateName;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.ViewHtml != result.ViewHtml)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "ViewHtml";
                history.PrevValue = result.ViewHtml;
                history.CurValue = input.ViewHtml;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.OrganizationUnitId != result.OrganizationUnitId)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "OrganizationUnitId";
                history.PrevValue = result.OrganizationUnitId.ToString();
                history.CurValue = input.OrganizationUnitId.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != result.IsActive)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = result.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, result);
        }

        public async Task Delete(EntityDto input)
        {
            

            var Name = _wholeSaleEmailTemplateRepqository.Get(input.Id).TemplateName;
            await _wholeSaleEmailTemplateRepqository.DeleteAsync(input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 4;
            dataVaultLog.ActionNote = "Email Templates Deleted : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);
        }

        public async Task<PagedResultDto<GetWholeSaleEmailTemplateForViewDto>> GetAll(GetAllWholeSaleEmailTemplateInput input)
        {
            var filteredResult = _wholeSaleEmailTemplateRepqository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.TemplateName.Contains(input.Filter))
               .WhereIf(input.OrganizationId != 0, e => e.OrganizationUnitId == input.OrganizationId);

            var pagedAndFilteredResult = filteredResult.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var result = from o in pagedAndFilteredResult
                         select new GetWholeSaleEmailTemplateForViewDto()
                         {
                             EmailTamplate = new WholeSaleEmailTamplateDto
                             {
                                 TemplateName = o.TemplateName,
                                 OrganazationUnitsName = o.OrganizationUnitFk.DisplayName,
                                 ViewHtml = o.ViewHtml,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         };

            var totalCount = await filteredResult.CountAsync();

            return new PagedResultDto<GetWholeSaleEmailTemplateForViewDto>(totalCount, await result.ToListAsync());
        }

        public async Task<List<CommonLookupDto>> GetCustomTemplateForTableDropdown(int leadId)
        {
            var OrgId = _wholeSaleEmailTemplateRepqository.GetAll().Where(e => e.Id == leadId).Select(e => e.OrganizationUnitId).FirstOrDefault();

            var filteredResult = _wholeSaleEmailTemplateRepqository.GetAll().Where(e => e.OrganizationUnitId == OrgId);

            var result = from o in filteredResult
                         select new CommonLookupDto()
                         {
                             Id = o.Id,
                             DisplayName = o.TemplateName
                         };

            return await result.ToListAsync();
        }

        public async Task<GetWholeSaleEmailTemplateForEditOutput> GetWholeSaleEmailTemplateForEdit(EntityDto input)
        {
            var result = await _wholeSaleEmailTemplateRepqository.GetAsync(input.Id);

            var output = new GetWholeSaleEmailTemplateForEditOutput { EmailTemplateDto = ObjectMapper.Map<CreateOrEditWholeSaleEmailTemplateDto>(result) };

            return output;
        }
    }
}
