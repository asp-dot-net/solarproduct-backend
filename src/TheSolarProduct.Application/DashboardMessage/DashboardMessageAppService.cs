﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.CommissionRanges.Dtos;
using TheSolarProduct.DashboardMessages;
using TheSolarProduct.DashboardMessages.Dtos;
using TheSolarProduct.DataVaults;
using System.Collections.Generic;
using TheSolarProduct.CommissionRanges;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using Abp.Organizations;

namespace TheSolarProduct.DashboardMessage
{
    public class DashboardMessageAppService : TheSolarProductAppServiceBase, IDashboardMessageAppService
    {
        private readonly IRepository<DashboardMessages.DashboardMessage> _DashboardMessageRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<OrganizationUnit, long> _organizationRepository;

        public DashboardMessageAppService(
            IRepository<DashboardMessages.DashboardMessage> DashboardMessageRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider, IRepository<OrganizationUnit, long> organizationRepository) 
        {
            _DashboardMessageRepository = DashboardMessageRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _organizationRepository = organizationRepository;
        }

        public async Task CreateOrEdit(CreateOrEditDashboardMessageDto input)
        {
            if (input.Id == null)

            {
                await Create(input);
            }

            else
            {
                await Update(input);
            }
        }
        protected virtual async Task Create(CreateOrEditDashboardMessageDto input)
        {
            var dashboardMessage = ObjectMapper.Map<DashboardMessages.DashboardMessage>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 45;
            dataVaultLog.ActionNote = "Dashboard Message Created : " + input.Message;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _DashboardMessageRepository.InsertAsync(dashboardMessage);
        }

        protected virtual async Task Update(CreateOrEditDashboardMessageDto input)
        {
            var dashboardMessage = await _DashboardMessageRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 45;
            dataVaultLog.ActionNote = "Dashboard Message Updated : " + dashboardMessage.Message;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Message != dashboardMessage.Message)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Message";
                history.PrevValue = dashboardMessage.Message;
                history.CurValue = input.Message;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.DashboardType != dashboardMessage.DashboardType)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "DashboardType";
                history.PrevValue = dashboardMessage.DashboardType;
                history.CurValue = input.DashboardType;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.IsActive != dashboardMessage.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = dashboardMessage.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.OrgId != dashboardMessage.OrgId)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Value";
                history.PrevValue = dashboardMessage.OrgId > 0 ? _organizationRepository.Get((long)dashboardMessage.OrgId).DisplayName : "";
                history.CurValue = input.OrgId > 0 ? _organizationRepository.Get((long)input.OrgId).DisplayName : "";
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            ObjectMapper.Map(input, dashboardMessage);

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
        }

        public async Task Delete(EntityDto input)
        {
            var Range = _DashboardMessageRepository.Get(input.Id).Message;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 45;
            dataVaultLog.ActionNote = "Dashboard Message Deleted : " + Range;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _DashboardMessageRepository.DeleteAsync(input.Id);

        }

        public async Task<PagedResultDto<GetAllDashboardMessageViewDto>> GetAll(GetAllDashboardMessageInputDto input)
        {
            var filtereddashDoardMessages = await _DashboardMessageRepository.GetAll()
                         .Where(e => e.OrgId == input.OrgId)
                         .WhereIf(!string.IsNullOrEmpty(input.DashboardType) , e => e.DashboardType == input.DashboardType)
                         .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Message.Contains(input.Filter)).AsNoTracking().OrderBy(e => e.Id).ToListAsync();


            var dashboardMessages = from o in filtereddashDoardMessages
                                    select new GetAllDashboardMessageViewDto()
                                   {
                                       DashboardMessageDto = new CreateOrEditDashboardMessageDto
                                       {
                                           Id = o.Id,
                                           Message = o.Message
                                           , OrgId = o.OrgId
                                           , IsActive = o.IsActive
                                           , DashboardType = o.DashboardType
                                       }
                                   };

            var totalCount = filtereddashDoardMessages.Count();

            return new PagedResultDto<GetAllDashboardMessageViewDto>(
                totalCount,
                dashboardMessages.ToList()
            );
        }

        public async Task<GetAllDashboardMessageViewDto> GetDashboardMessageForView(int id)
        {
            var dashboardMessages = await _DashboardMessageRepository.FirstOrDefaultAsync(id);

            var output = new GetAllDashboardMessageViewDto { DashboardMessageDto = ObjectMapper.Map<CreateOrEditDashboardMessageDto>(dashboardMessages) };

            return output;
        }
    }
}
