﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.DataVaults;
using TheSolarProduct.StockOrder.Dtos;
using System.Linq.Dynamic.Core;
using TheSolarProduct.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using Abp.Collections.Extensions;
using Abp.Timing.Timezone;
using TheSolarProduct.StockTransfers.Exporting;
using TheSolarProduct.StockTransfers.Dtos;
using TheSolarProduct.StockOrders;
using TheSolarProduct.QuickStocks;
using TheSolarProduct.CheckActives.Dtos;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using NPOI.SS.Formula.Functions;
using Telerik.Reporting;
using TheSolarProduct.TransportationCosts;
using TheSolarProduct.Jobs;
using TheSolarProduct.Authorization.Users;
using System;
using Abp.Domain.Entities;



namespace TheSolarProduct.StockTransfers
{

    public class StockTransferAppService : TheSolarProductAppServiceBase, IStockTransferAppService

    {
        private readonly IStockTransferExcelExporter _stockTransferExcelExporter;
        private readonly IRepository<StockTransfer> _stockTransferRepository;
        private readonly IRepository<StockTransferProductItem> _stockTransferProductItemRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<QuickStockActivityLog> _quickStockActivityLogRepository;
        private readonly IRepository<QuickStockActivityLogHistory> _purchaseOrderHistoryRepository;
        private readonly IRepository<TransportCompany> _transportCompanyRepository;
        private readonly IRepository<Warehouselocation> _warehouselocationRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<ProductItem> _productItemRepository;

        public StockTransferAppService(
              ITimeZoneConverter timeZoneConverter,
              IRepository<StockTransfer> stockTransferRepository,
              IRepository<StockTransferProductItem> stockTransferProductItemRepository,
              IStockTransferExcelExporter IStockTransferExcelExporter,
              IRepository<UserTeam> userTeamRepository,
              IRepository<UserRole, long> userRoleRepository,
              IRepository<Role> roleRepository,
              IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
              IRepository<QuickStockActivityLog> quickStockActivityLogRepository,
              IRepository<QuickStockActivityLogHistory> purchaseOrderHistoryRepository,
              IRepository<TransportCompany> transportCompanyRepository,
              IRepository<Warehouselocation> warehouselocationRepository,
              IRepository<User, long> userRepository,
              IRepository<ProductItem> productItemRepository
              )
        {
            _stockTransferRepository = stockTransferRepository;
            _stockTransferProductItemRepository = stockTransferProductItemRepository;
            _timeZoneConverter = timeZoneConverter;
            _stockTransferExcelExporter = IStockTransferExcelExporter;
            _userTeamRepository = userTeamRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
            _quickStockActivityLogRepository = quickStockActivityLogRepository;
            _purchaseOrderHistoryRepository = purchaseOrderHistoryRepository;
            _transportCompanyRepository = transportCompanyRepository;
            _warehouselocationRepository = warehouselocationRepository;
            _userRepository = userRepository;
            _productItemRepository = productItemRepository;
        }

        public async Task<PagedResultDto<GetStockTransferForViewDto>> GetAll(GetAllStockTransferInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var stockTransfer = _stockTransferRepository.GetAll()
                    //.Include(e => e.TransportCompanyFk)
                    //.Include(e => e.WarehouseLocationFromFK)
                    //.Include(e => e.WarehouseLocationToFK)
                    .WhereIf(input.FilterName == "TransportCompany" && !string.IsNullOrWhiteSpace(input.Filter), e => e.TransportCompanyFk.CompanyName.Contains(input.Filter))
                    .WhereIf(input.FilterName == "vehicalNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.VehicalNo.Contains(input.Filter))
                    .WhereIf(input.WarehouseLocationFromIdFilter > 0, e => e.WarehouseLocationFromId == input.WarehouseLocationFromIdFilter)
                    .WhereIf(input.WarehouseLocationToIdFilter > 0, e => e.WarehouseLocationToId == input.WarehouseLocationToIdFilter)
                    .WhereIf(input.Datefilter == "OrderDate" && input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                    .WhereIf(input.Datefilter == "OrderDate" && input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                    .Where(e => e.OrganizationId == input.OrganizationId);

            var pagedAndFiltered = stockTransfer
               .OrderBy(input.Sorting ?? "id desc")
               .PageBy(input);

            var outputstockOrder = from o in pagedAndFiltered
                                   select new GetStockTransferForViewDto()
                                   {
                                       Id = o.Id,
                                       Amount = o.Amount,
                                       TransferNo = o.TransferNo,   
                                       WarehouseLocationFrom = o.WarehouseLocationFromFK.location,
                                       WarehouseLocationTo = o.WarehouseLocationToFK.location,
                                       VehicalNo = o.VehicalNo,
                                       TrackingNumber = o.TrackingNumber,
                                       TransportCompany = o.TransportCompanyFk.CompanyName,
                                       TransferDate=o.CreationTime.Date,
                                       TransferByDate=o.TransferByDate,
                                       Transferd=o.Transferd,
                                       Received =o.Received,
                                       ReceivedByDate=o.ReceivedByDate,
                                       Qty = _stockTransferProductItemRepository.GetAll().Where(e => e.StockTransferId == o.Id).Sum(e => e.Quantity),
                                       CornetNo = o.CornetNo,
                                       ProductItemInfo = _stockTransferProductItemRepository.GetAll().Where(e => e.StockTransferId == o.Id).Select(e => new ProductsItemDto {ProductItem = e.ProductItemFk.Name,}).ToList()
                                   };
            //var stockOrderItems = outputstockOrder.ToList();


            var totalCount = outputstockOrder.Count();

            return new PagedResultDto<GetStockTransferForViewDto>(
                totalCount,
                outputstockOrder.ToList()
            ); ;
        }
        public async Task CreateOrEdit(CreateOrEditStockTransferDto input)
        {


            if (input.Id == null || input.Id==0)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        protected virtual async Task Create(CreateOrEditStockTransferDto input)
        {

            var StockTransfer = ObjectMapper.Map<StockTransfer>(input);

            var StockTransferid = await _stockTransferRepository.InsertAndGetIdAsync(StockTransfer);

            var quickStockLog = new QuickStockActivityLog();
            quickStockLog.Action = "Created";
            quickStockLog.SectionId = 2;
            quickStockLog.ActionId = 76;
            quickStockLog.Type = "StockTransfer";
            quickStockLog.StockTransferId = StockTransferid;
            quickStockLog.ActionNote = "Stock Transfer Created";
            await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);

            foreach (var item in input.ProductItemInfo)
            {
                item.StockTransferId = StockTransferid;
                var StockTransferitem = ObjectMapper.Map<StockTransferProductItem>(item);

                await _stockTransferProductItemRepository.InsertAsync(StockTransferitem);
            }
        }
        protected virtual async Task Update(CreateOrEditStockTransferDto input)
        {
            var stockTransfer = await _stockTransferRepository.FirstOrDefaultAsync((int)input.Id);

            var quickStockLog = new QuickStockActivityLog();
            quickStockLog.Action = "Updated";
            quickStockLog.SectionId = 2;
            quickStockLog.ActionId = 77;
            quickStockLog.StockTransferId = stockTransfer.Id;
            quickStockLog.ActionNote = "Stock Transfer Updated";
            quickStockLog.Type = "StockTransfer";
            var quickStockLogId = await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);

            var List = new List<QuickStockActivityLogHistory>();

            if (input.TransportCompanyId != stockTransfer.TransportCompanyId)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "TransportCompany";
                history.PrevValue = stockTransfer.TransportCompanyId > 0 ? _transportCompanyRepository.GetAll().Where(e => e.Id == stockTransfer.TransportCompanyId).FirstOrDefault().CompanyName : "";
                history.CurValue = input.TransportCompanyId > 0 ? _transportCompanyRepository.GetAll().Where(e => e.Id == input.TransportCompanyId).FirstOrDefault().CompanyName : "";
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.WarehouseLocationFromId != stockTransfer.WarehouseLocationFromId)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "WarehouseLocationFrom";
                history.PrevValue = stockTransfer.WarehouseLocationFromId > 0 ? _warehouselocationRepository.GetAll().Where(e => e.Id == stockTransfer.WarehouseLocationFromId).FirstOrDefault().Address : "";
                history.CurValue = input.WarehouseLocationFromId > 0 ? _warehouselocationRepository.GetAll().Where(e => e.Id == input.WarehouseLocationFromId).FirstOrDefault().Address : "";
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.WarehouseLocationToId != stockTransfer.WarehouseLocationToId)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "WarehouseLocationTo";
                history.PrevValue = stockTransfer.WarehouseLocationToId > 0 ? _warehouselocationRepository.GetAll().Where(e => e.Id == stockTransfer.WarehouseLocationToId).FirstOrDefault().Address : "";
                history.CurValue = input.WarehouseLocationToId > 0 ? _warehouselocationRepository.GetAll().Where(e => e.Id == input.WarehouseLocationToId).FirstOrDefault().Address : "";
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.OrganizationId != stockTransfer.OrganizationId)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "Organization";
                history.PrevValue = stockTransfer.OrganizationId.ToString();
                history.CurValue = input.OrganizationId.ToString();
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.CornetNo != stockTransfer.CornetNo)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "CornetNo";
                history.PrevValue = stockTransfer.CornetNo;
                history.CurValue = input.CornetNo;
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.VehicalNo != stockTransfer.VehicalNo)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "VehicalNo";
                history.PrevValue = stockTransfer.VehicalNo.ToString();
                history.CurValue = input.VehicalNo.ToString();
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.TrackingNumber != stockTransfer.TrackingNumber)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "TrackingNumber";
                history.PrevValue = stockTransfer.TrackingNumber.ToString();
                history.CurValue = input.TrackingNumber.ToString();
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.Amount != stockTransfer.Amount)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "Amount";
                history.PrevValue = stockTransfer.Amount.ToString();
                history.CurValue = input.Amount.ToString();
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.AdditionalNotes != stockTransfer.AdditionalNotes)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "AdditionalNotes";
                history.PrevValue = stockTransfer.AdditionalNotes;
                history.CurValue = input.AdditionalNotes;
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }

            var stockTransferItemIds = await _stockTransferProductItemRepository.GetAll().Where(e => e.StockTransferId == stockTransfer.Id).ToListAsync();
            var CurrentstockTransferItemIds = input.ProductItemInfo != null ? input.ProductItemInfo.Select(e => e.Id).ToList() : new List<int?>();
            var listpurchaseOrder = stockTransferItemIds.Where(e => !CurrentstockTransferItemIds.Contains(e.Id));

            foreach (var item in listpurchaseOrder)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "Item Deleted";
                history.PrevValue = _productItemRepository.Get((int)item.ProductItemId).Name;
                history.CurValue = "";
                history.Action = "Deleted";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);

                await _stockTransferProductItemRepository.DeleteAsync(item);
            }
            foreach (var item in input.ProductItemInfo)
            {
                item.StockTransferId = stockTransfer.Id;
                if ((item.Id != null ? item.Id : 0) == 0)
                {
                    var stockTransferItem = ObjectMapper.Map<StockTransferProductItem>(item);
                    await _stockTransferProductItemRepository.InsertAsync(stockTransferItem);
                    QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                    history.FieldName = "Item Added";
                    history.PrevValue = _productItemRepository.Get((int)item.ProductItemId).Name;
                    history.CurValue = " ";
                    history.Action = "Add";
                    history.ActivityLogId = quickStockLogId;
                    List.Add(history);
                }
                else
                {
                    var stockTransferItem = await _stockTransferProductItemRepository.GetAsync((int)item.Id);

                    if (item.StockTransferId != stockTransferItem.StockTransferId)
                    {
                        QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                        history.FieldName = "StockTransfer";
                        history.PrevValue = stockTransferItem.StockTransferId.ToString();
                        history.CurValue = item.StockTransferId.ToString();
                        history.Action = "Update";
                        history.ActivityLogId = quickStockLogId;
                        List.Add(history);
                    }
                    if (item.ProductItemId != stockTransferItem.ProductItemId)
                    {
                        QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                        history.FieldName = "ProductItem";
                        history.PrevValue = stockTransferItem.ProductItemId.ToString();
                        history.CurValue = item.ProductItemId.ToString();
                        history.Action = "Update";
                        history.ActivityLogId = quickStockLogId;
                        List.Add(history);
                    }
                    if (item.Quantity != stockTransferItem.Quantity)
                    {
                        QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                        history.FieldName = "Quantity";
                        history.PrevValue = stockTransferItem.Quantity.ToString();
                        history.CurValue = item.Quantity.ToString();
                        history.Action = "Update";
                        history.ActivityLogId = quickStockLogId;
                        List.Add(history);
                    }


                    ObjectMapper.Map(item, stockTransferItem);
                    await _stockTransferProductItemRepository.UpdateAsync(stockTransferItem);
                }

            }
            await _dbcontextprovider.GetDbContext().QuickStockActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, stockTransfer);



        }
        public async Task<GetStockTransferForEditOutput> GetStockTransferForEdit(EntityDto input)
        {
            var stockTransfer = await _stockTransferRepository.GetAsync(input.Id);

            var stockTransferDto = ObjectMapper.Map<CreateOrEditStockTransferDto>(stockTransfer);

            var stockTransferItem = await _stockTransferProductItemRepository.GetAll().Include(e => e.ProductItemFk).Where(e => e.StockTransferId == input.Id).ToListAsync();

            stockTransferDto.ProductItemInfo = new List<ProductsItemDto>();
            foreach (var item in stockTransferItem)
            {
                var stockTransferItems = new ProductsItemDto();
                stockTransferItems.Id = item.Id;
                stockTransferItems.ProductTypeId = item.ProductItemFk.ProductTypeId;
                stockTransferItems.ProductItemId = item.ProductItemId;
                stockTransferItems.ProductItem = item.ProductItemFk.Name;
                stockTransferItems.Quantity = item.Quantity;
                stockTransferItems.Size = item.ProductItemFk.Size;
                stockTransferItems.ModelNo = item.ProductItemFk.Model;
                stockTransferDto.ProductItemInfo.Add(stockTransferItems);
            }

            var output = new GetStockTransferForEditOutput
            {
                StockTransfer = stockTransferDto
            };

            return output;
        }
        public async Task Delete(EntityDto input)
        {
            var Name = _stockTransferRepository.Get(input.Id);

            var quickStockLog = new QuickStockActivityLog();
            quickStockLog.Action = "Deleted";
            quickStockLog.SectionId = 2;
            quickStockLog.ActionId = 78;
            quickStockLog.StockTransferId = input.Id;
            quickStockLog.ActionNote = "Stock Transfer Deleted : " + Name;
            quickStockLog.Type = "StockTransfer";
            await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);

            await _stockTransferRepository.DeleteAsync(input.Id);
        }

        public async Task<GetStockTransferForViewDto> GetStockTransferById(int id)
        {
            var stockTransfer = await _stockTransferRepository.GetAll()
                .Include(e => e.TransportCompanyFk)
                .Include(e => e.WarehouseLocationFromFK)
                .Include(e => e.WarehouseLocationToFK)
                .FirstOrDefaultAsync(e => e.Id == id);
            var stockTransferItem = await _stockTransferProductItemRepository.GetAll().Include(e => e.ProductItemFk).Include(poi => poi.ProductItemFk.ProductTypeFk).Where(e => e.StockTransferId == id).ToListAsync();

            var stockTransferDto = new GetStockTransferForViewDto
            {
                Id = stockTransfer.Id,
                TransportCompany = stockTransfer.TransportCompanyFk.CompanyName,
                WarehouseLocationFrom = stockTransfer.WarehouseLocationFromFK.location,
                WarehouseLocationTo = stockTransfer.WarehouseLocationToFK.location,
                VehicalNo = stockTransfer.VehicalNo,
                TrackingNumber = stockTransfer.TrackingNumber,
                Amount = stockTransfer.Amount,
                AdditionalNotes = stockTransfer.AdditionalNotes,
                CornetNo = stockTransfer.CornetNo,
                TransferNo=stockTransfer.TransferNo,
            };
            stockTransferDto.ProductItemInfo = new List<ProductsItemDto>();
            foreach (var item in stockTransferItem)
            {
                var stockTransferItems = new ProductsItemDto();
                stockTransferItems.Id = item.Id;
                stockTransferItems.ProductTypeId = item.ProductItemFk.ProductTypeId;
                stockTransferItems.ProductType = item.ProductItemFk.ProductTypeFk.Name;
                stockTransferItems.ProductItemId = item.ProductItemId;
                stockTransferItems.ProductItem = item.ProductItemFk.Name;
                stockTransferItems.Quantity = item.Quantity;
                stockTransferItems.Size = item.ProductItemFk.Size;
                stockTransferItems.ModelNo = item.ProductItemFk.Model;
                stockTransferDto.ProductItemInfo.Add(stockTransferItems);
            }
            return stockTransferDto;
        }
    
        public async Task<FileDto> GetStockTransferToExcel(GetAllStockTransferForExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var stockTransfer = _stockTransferRepository.GetAll()
                    //.Include(e => e.TransportCompanyFk)
                    //.Include(e => e.WarehouseLocationFromFK)
                    //.Include(e => e.WarehouseLocationToFK)
                    .WhereIf(input.FilterName == "TransportCompany" && !string.IsNullOrWhiteSpace(input.Filter), e => e.TransportCompanyFk.CompanyName.Contains(input.Filter))
                    .WhereIf(input.FilterName == "vehicalNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.VehicalNo.Contains(input.Filter))
                    .WhereIf(input.WarehouseLocationFromIdFilter > 0, e => e.WarehouseLocationFromId == input.WarehouseLocationFromIdFilter)
                    .WhereIf(input.WarehouseLocationToIdFilter > 0, e => e.WarehouseLocationToId == input.WarehouseLocationToIdFilter)
                    .WhereIf(input.Datefilter == "OrderDate" && input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                    .WhereIf(input.Datefilter == "OrderDate" && input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                    .Where(e => e.OrganizationId == input.OrganizationId);



            var outputstockOrder = from o in stockTransfer
                                   select new GetStockTransferForViewDto()
                                   {
                                       Id = o.Id,
                                       TransportCompany = o.TransportCompanyFk.CompanyName,
                                       WarehouseLocationFrom = o.WarehouseLocationFromFK.location,
                                       WarehouseLocationTo = o.WarehouseLocationToFK.location,
                                       VehicalNo = o.VehicalNo,
                                       TrackingNumber = o.TrackingNumber,
                                       Amount = o.Amount,
                                       CornetNo = o.CornetNo,

                                   };
            var stockOrderItems = outputstockOrder.ToList();

           
            var ListDtos = stockOrderItems;
           
            return _stockTransferExcelExporter.ExportToFile(ListDtos);
        }

        public async Task<List<GetStockTransferActivityLogViewDto>> GetStockTransferActivityLog(GetStockTransferActivityLogInput input)
        {

            List<int> stockTransferActionids = new List<int>() { 76,77,78 };
          
            var Result = _quickStockActivityLogRepository.GetAll()
                .Where(L => L.StockTransferId == input.StockTransferId)
                
                .WhereIf(input.CurrentPage != false, L => L.SectionId == input.SectionId)
                ;

            if (!input.AllActivity)
            {
                Result = Result.OrderByDescending(e => e.Id).Take(10);
            }
            else
            {
                Result = Result.OrderByDescending(e => e.Id);
            }
            var stocktransfer = from o in Result


                                 join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                                 from s3 in j3.DefaultIfEmpty()

                                 select new GetStockTransferActivityLogViewDto()
                                 {
                                     Id = o.Id,
                                     ActionName = o.ActionFk.ActionName,
                                     ActionId = o.ActionId,
                                     ActionNote = o.ActionNote,
                                     StockTransferId = o.StockTransferId,
                                     ActivityDate = o.ActivityDate,
                                     Section = o.SectionFK.SectionName,
                                     ActivitysDate = o.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss"),
                                     CreatorUserName = s3 == null || s3.FullName == null ? "" : s3.FullName.ToString(),
                                     CreationTime = o.CreationTime,

                                 };

            return new List<GetStockTransferActivityLogViewDto>(
                   await stocktransfer.ToListAsync()
               );
        }

        public async Task<List<StockTransferHistoryDto>> GetStockTransferActivityLogHistory(GetStockTransferActivityLogInput input)
        {
            try
            {
                var varr = _purchaseOrderHistoryRepository.GetAll().Include(e => e.QuickStockActivityLogFK);
                var Result = (from item in _purchaseOrderHistoryRepository.GetAll()
                              join ur in _userRepository.GetAll() on item.CreatorUserId equals ur.Id into urjoined
                              from ur in urjoined.DefaultIfEmpty()
                              where (item.ActivityLogId == input.StockTransferId)
                              select new StockTransferHistoryDto()
                              {
                                  Id = item.Id,
                                  FieldName = item.FieldName,
                                  PrevValue = item.PrevValue,
                                  CurValue = item.CurValue,
                                  Lastmodifiedbyuser = ur.Name + " " + ur.Surname
                              })
                    .OrderByDescending(e => e.Id).ToList();
                return Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<long?> GetLastTransferNo(int organizationId)
        {
            var TransferNo = await _stockTransferRepository.GetAll().OrderByDescending(e => e.Id).Select(e => e.TransferNo).FirstOrDefaultAsync();

            if (TransferNo == null)
            {
                TransferNo = 1001;
                return (long)TransferNo;
            }

            return TransferNo + 1;
        }

    }
}
