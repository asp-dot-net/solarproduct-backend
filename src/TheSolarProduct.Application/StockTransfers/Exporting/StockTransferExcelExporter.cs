﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.StockTransfers.Dtos;
using TheSolarProduct.StockOrders.Exporting;
using TheSolarProduct.Storage;
using Google.Apis.Sheets.v4.Data;
using Telerik.Reporting;

namespace TheSolarProduct.StockTransfers.Exporting
{
   
    public class StockTransferExcelExporter : NpoiExcelExporterBase, IStockTransferExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public StockTransferExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,

            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetStockTransferForViewDto> StockTransfer)
        {
            return CreateExcelPackage(
                "StockTransfer.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("StockTransfer");

                    AddHeader(
                        sheet,
                        L("TransportCompany"),
                        L("WarehouseLocationFrom"),
                        L("WarehouseLocationTo"),
                        L("VehicalNo"),
                        L("TrackingNumber"),
                        L("Amount")
 
                    );

                    AddObjects(
                        sheet,
                        2,
                        StockTransfer,
                        _ => _.TransportCompany,
                        _ => _.WarehouseLocationFrom,
                        _ => _.WarehouseLocationTo,
                        _ => _.VehicalNo,
                        _ => _.TrackingNumber,
                        _ => _.Amount
                    );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                    for (var i = 1; i <= StockTransfer.Count; i++)
                    {
                        
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[5], "0.00");
                       
                    }


                }
            );

           
           
        }


    }
}
