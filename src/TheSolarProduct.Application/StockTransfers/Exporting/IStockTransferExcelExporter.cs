﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.StockTransfers.Dtos;

namespace TheSolarProduct.StockTransfers.Exporting
{
    public interface IStockTransferExcelExporter
    {
        FileDto ExportToFile(List<GetStockTransferForViewDto> StockTransfer);

    }
}
