﻿using System.Collections.Generic;
using TheSolarProduct.Auditing.Dto;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);

        FileDto ExportToFile(List<EntityChangeListDto> entityChangeListDtos);
    }
}
