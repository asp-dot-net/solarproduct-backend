﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.DocumentLibrarys.Dtos;
using TheSolarProduct.Quotations;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.MultiTenancy;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataVaults;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.DocumentLibrarys
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class DocumentLibraryAppService : TheSolarProductAppServiceBase, IDocumentLibraryAppsnService
    {


        private readonly IRepository<DocumentLibrary> _documentlibraryRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public DocumentLibraryAppService(IRepository<DocumentLibrary> documenttypeRepository,
            IRepository<Tenant> tenantRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _documentlibraryRepository = documenttypeRepository;
            _tenantRepository = tenantRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
        }

        public async Task<PagedResultDto<GetDocumentLibraryForViewDto>> GetAll(GetAllDocumentLibraryInput input)
        {

            var filteredDepartments = _documentlibraryRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Title.Contains(input.Filter));

            var pagedAndFilteredDepartments = filteredDepartments
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var departments = from o in pagedAndFilteredDepartments
                              select new GetDocumentLibraryForViewDto()
                              {
                                  Documentlibrary = new DocumentLibraryDto
                                  {
                                      Title = o.Title,
                                      Id = o.Id,
                                      FilePath = o.FilePath,
                                      FileName = o.FileName
                                  }
                              };

            var totalCount = await filteredDepartments.CountAsync();

            return new PagedResultDto<GetDocumentLibraryForViewDto>(
                totalCount,
                await departments.ToListAsync()
            );
        }

        public async Task<GetDocumentLibraryForViewDto> GetDocumentLibraryForView(int id)
        {
            var document = await _documentlibraryRepository.GetAsync(id);

            var output = new GetDocumentLibraryForViewDto { Documentlibrary = ObjectMapper.Map<DocumentLibraryDto>(document) };

            return output;
        }

        public async Task<GetDocumentLibraryforEditOutput> GetDocumentLibraryForEdit(EntityDto input)
        {
            var DocumentlibraryDto = await _documentlibraryRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetDocumentLibraryforEditOutput { DocumentlibraryDto = ObjectMapper.Map<CreateorEditDocumentLibraryDto>(DocumentlibraryDto) };

            return output;
        }

        public async Task CreateOrEdit(CreateorEditDocumentLibraryDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        protected virtual async Task Create(CreateorEditDocumentLibraryDto input)
        {

            var documenttype = ObjectMapper.Map<DocumentLibrary>(input);

            if (AbpSession.TenantId != null)
            {
                documenttype.TenantId = (int)AbpSession.TenantId;
            }
            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
            documenttype.FilePath = "\\Documents" + "\\" + TenantName + "\\" + "Documents" + "\\" + "DocumentLibrary" + "\\" + input.FileName;
            await _documentlibraryRepository.InsertAsync(documenttype);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 30;
            dataVaultLog.ActionNote = "Document Library Created : " + input.Title;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

        }


        protected virtual async Task Update(CreateorEditDocumentLibraryDto input)
        {
            var documenttype = await _documentlibraryRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 30;
            dataVaultLog.ActionNote = "Document Library Updated : " + documenttype.Title;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Title != documenttype.Title)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = documenttype.Title;
                history.CurValue = input.Title;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.FileName != documenttype.FileName)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "FileName";
                history.PrevValue = documenttype.FileName;
                history.CurValue = input.FileName;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
            documenttype.FilePath = "\\Documents" + "\\" + TenantName + "\\" + "Documents" + "\\" + "DocumentLibrary" + "\\" + input.FileName;

            ObjectMapper.Map(input, documenttype);
        }


        public async Task Delete(EntityDto input)
        {
            var Name = _documentlibraryRepository.Get(input.Id).Title;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 30;
            dataVaultLog.ActionNote = "Document Library Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _documentlibraryRepository.DeleteAsync(input.Id);
        }

    }
}
