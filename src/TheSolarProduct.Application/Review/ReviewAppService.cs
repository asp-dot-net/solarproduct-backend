﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Linq.Extensions;
using Abp.Net.Mail;
using Abp.Notifications;
using Abp.Organizations;
using Abp.Timing.Timezone;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net.Mail;
using System.Threading.Tasks;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.EmailTemplates;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.JobHistory;
using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Leads;
using TheSolarProduct.Notifications;
using TheSolarProduct.Review.Dtos;
using TheSolarProduct.ReviewTypes;
using TheSolarProduct.ServiceCategorys;
using TheSolarProduct.Services.Dtos;
using TheSolarProduct.TheSolarDemo;
using Abp.AutoMapper;
using TheSolarProduct.Sections;
using Abp.Authorization;
using TheSolarProduct.QuickStocks;
using TheSolarProduct.StockOrder.Dtos;

namespace TheSolarProduct.Review
{
    [AbpAuthorize]
    public class ReviewAppService : TheSolarProductAppServiceBase, IReviewAppServicecs
    {

        private readonly IRepository<Lead, int> _lookup_leadRepository;
        private readonly IRepository<Job, int> _lookup_JobRepository;
        private readonly IRepository<ServiceCategory> _lookup_ServiceCategorysRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<User, long> _userRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<ReviewType> _reviewtypeRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<EmailTemplate> _EmailTemplateRepository;
        private readonly IRepository<JobReview> _jobReviewRepository;
        private readonly IRepository<Job, int> _lookup_jobRepository;

        public ReviewAppService(
            IRepository<Lead, int> lookup_leadRepository,
            IRepository<ServiceCategory> lookup_ServiceCategorysRepository,
            ITimeZoneConverter timeZoneConverter,
            IRepository<User, long> userRepository,
            UserManager userManager,
            IRepository<UserTeam> userTeamRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IAppNotifier appNotifier,
            IRepository<Job, int> lookup_JobRepository,
            IApplicationSettingsAppService applicationSettings,
            IRepository<LeadActivityLog> leadactivityRepository,
            IEmailSender emailSender,
            IRepository<UserRole, long> userRoleRepository,
            IRepository<Role> roleRepository,
            IRepository<ReviewType> reviewtypeRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
            IRepository<EmailTemplate> EmailTemplateRepository,
            IRepository<JobReview> jobReviewRepository,
            IRepository<Job, int> lookup_jobRepository
           )
        {
            _lookup_leadRepository = lookup_leadRepository;
            _lookup_ServiceCategorysRepository = lookup_ServiceCategorysRepository;
            _timeZoneConverter = timeZoneConverter;
            _userRepository = userRepository;
            _userManager = userManager;
            _userTeamRepository = userTeamRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _appNotifier = appNotifier;
            _lookup_JobRepository = lookup_JobRepository;
            _applicationSettings = applicationSettings;
            _leadactivityRepository = leadactivityRepository;
            _emailSender = emailSender;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _reviewtypeRepository = reviewtypeRepository;
            _dbcontextprovider = dbcontextprovider;
            _EmailTemplateRepository = EmailTemplateRepository;
            _jobReviewRepository = jobReviewRepository;
            _lookup_jobRepository = lookup_jobRepository;
        }


        public async Task<PagedResultDto<GetReviewViewDto>> GetReviewData(GetAllReviewInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            int UserId = (int)AbpSession.UserId;
            var userList = _userRepository.GetAll();
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            var leadactivitylog = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.SectionId == 23);
            List<int?> jobIdlist = new List<int?>();
            List<int> reviewTemplateList = new List<int>();
            List<int> emailleadids = new List<int>();
            if (input.ReviewType == 1)
            {
                var googlereviewtmpid = _EmailTemplateRepository.GetAll().Where(e => e.TemplateName == "Google Review").Select(e => e.Id).FirstOrDefault();
                emailleadids = leadactivitylog.Where(e => e.TemplateId == googlereviewtmpid).Select(e => e.LeadId).ToList();
                jobIdlist = _jobReviewRepository.GetAll().Where(e => e.ReviewTypeId == input.ReviewType).Select(e => e.JobId).ToList();
            }
            if (input.ReviewType == 2)
            {
                var prouctreviewtmpid = _EmailTemplateRepository.GetAll().Where(e => e.TemplateName == "Product Review").Select(e => e.Id).FirstOrDefault();
                emailleadids = leadactivitylog.Where(e => e.TemplateId == prouctreviewtmpid).Select(e => e.LeadId).ToList();
                jobIdlist = _jobReviewRepository.GetAll().Where(e => e.ReviewTypeId == input.ReviewType).Select(e => e.JobId).ToList();
            }
            var managerusers = new List<long>();
            var jobIdForDateFilter = new List<int?>();
            if (input.Reviewdatefilter == "ReviewDate")
            {
                jobIdForDateFilter = _jobReviewRepository.GetAll().Where(e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date).OrderByDescending(e => e.Id).Select(e => e.JobId).ToList();
            }
            if (role.Contains("Review Manager"))
            {
                managerusers = (from user in userList
                                join ur in _userRoleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                from ur in urJoined.DefaultIfEmpty()
                                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                from us in usJoined.DefaultIfEmpty()
                                where (us != null && us.DisplayName == "Review Manager")
                                select (user.Id)).Distinct().ToList();
            }

            var filteredJobs = _lookup_JobRepository.GetAll()
                        .Include(e => e.JobTypeFk)
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.RoofTypeFk)
                        .Include(e => e.RoofAngleFk)
                        .Include(e => e.ElecDistributorFk)
                        .Include(e => e.LeadFk)
                        .Include(e => e.ElecRetailerFk)
                        .Include(e => e.PaymentOptionFk)
                        .Include(e => e.DepositOptionFk)
                        .Include(e => e.PromotionOfferFk)
                        .Include(e => e.HouseTypeFk)
                        .Include(e => e.FinanceOptionFk)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) ||
                        //e.LotNumber.Contains(input.Filter) || e.JobNumber.Contains(input.Filter) ||
                        //e.OffPeakMeter.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "LotNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LotNumber == input.Filter)
                        .WhereIf(input.FilterName == "NMINumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.NMINumber == input.Filter)
                        .WhereIf(input.FilterName == "RegPlanNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.RegPlanNo == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Phone == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.LeadFk.State == input.stateNameFilter)
                        .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
                        .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
                        .WhereIf(input.IsAssign == 1, e => e.ReviewAssignId != 0 && e.ReviewAssignId != null)
                        .WhereIf(input.IsAssign == 2, e => e.ReviewAssignId == null)
                        .WhereIf(input.emailsend == 1 && input.ReviewType != 0, e => emailleadids.Contains((int)e.LeadId))
                        .WhereIf(input.emailsend == 2 && input.ReviewType != 0, e => !emailleadids.Contains((int)e.LeadId))
                        .WhereIf(input.Reviewrate == 1 && input.emailsend != 0 && input.ReviewType != 0, e => jobIdlist.Contains(e.Id))
                        .WhereIf(input.Reviewrate == 2 && input.emailsend != 0 && input.ReviewType != 0, e => !jobIdlist.Contains(e.Id))
                        
                        .WhereIf(input.Reviewdatefilter == "ReviewDate" && jobIdForDateFilter.Count > 0, e => jobIdForDateFilter.Contains(e.Id))
                        .WhereIf(!role.Contains("Review Manager") && !role.Contains("Admin"), e => e.ReviewAssignId == UserId)
                        .WhereIf(input.Reviewdatefilter == "InstalledCompleteDate" && input.StartDate != null  && input.EndDate != null, e => e.InstalledcompleteDate >= input.StartDate && e.InstalledcompleteDate <= input.EndDate)
                        
                        .Where(e => e.InstalledcompleteDate != null)
                        .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == j.LeadId).Select(e => e.OrganizationId).FirstOrDefault()) == input.OrganizationUnit);
                        

            var pagedAndFilteredJobs = filteredJobs
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var leadlist = filteredJobs.Select(e => e.LeadId).ToList();

            var jobs = from o in pagedAndFilteredJobs
                       join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                       from s6 in j6.DefaultIfEmpty()
                       select new GetReviewViewDto()
                       {
                           Job = new JobDto
                           {
                               RegPlanNo = o.RegPlanNo,
                               LotNumber = o.LotNumber,
                               Address = o.Address + " " + o.Suburb + " " + o.State + " " + o.PostalCode,
                               Suburb = o.Suburb,
                               State = o.State,
                               UnitNo = o.UnitNo,
                               UnitType = o.UnitType,
                               NMINumber = o.NMINumber,
                               Id = o.Id,
                               ApplicationRefNo = o.ApplicationRefNo,
                               DistAppliedDate = o.DistAppliedDate,
                               Notes = o.Note,
                               InstallerNotes = o.InstallerNotes,
                               JobNumber = o.JobNumber,
                               MeterNumber = o.MeterNumber,
                               PostalCode = o.PostalCode,
                               StreetName = o.StreetName,
                               StreetNo = o.StreetNo,
                               StreetType = o.StreetType,
                               LeadId = o.LeadId,
                               JobStatusId = o.JobStatusId,
                               JobTypeId = o.JobTypeId,
                               InstallationDate = o.InstallationDate,
                               InstalledcompleteDate = o.InstalledcompleteDate,
                               PVDNumber = o.PVDNumber,
                               STCNotes = o.STCNotes,
                               SystemCapacity = o.SystemCapacity,
                               MeterApplyRef = o.MeterApplyRef,
                               InspectionDate = o.InspectionDate,
                               //ReviewRating = o.ReviewRating,
                               //ReviewNotes = o.ReviewNotes,
                               ReviewSmsSend = o.ReviewSmsSend,
                               ReviewSmsSendDate = o.ReviewSmsSendDate,
                               ReviewEmailSend = o.ReviewEmailSend,
                               ReviewEmailSendDate = o.ReviewEmailSendDate,
                               CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                               //ReviewType = _reviewtypeRepository.GetAll().Where(e => e.Id == o.ReviewType).Select(e => e.Name).FirstOrDefault(),
                               //ReviewType = _reviewtypeRepository.GetAll().Where(t => t.Id == (_jobReviewRepository.GetAll().OrderByDescending(r => r.Id).Where(r => r.JobId == o.Id).Select(e => e.ReviewTypeId).FirstOrDefault()) && t.Id == input.ReviewType).Select(t => t.Name).FirstOrDefault(),
                               ReviewType = input.ReviewType != 0 ? _reviewtypeRepository.GetAll().Where(t => t.Id == (_jobReviewRepository.GetAll().Where(r => r.JobId == o.Id && r.ReviewTypeId == input.ReviewType).Select(e => e.ReviewTypeId).FirstOrDefault()) && t.Name == "Google Review").Select(t => t.Name).FirstOrDefault()
                               : _reviewtypeRepository.GetAll().Where(t => t.Id == (_jobReviewRepository.GetAll().Where(r => r.JobId == o.Id && r.ReviewTypeId == 1).Select(e => e.ReviewTypeId).FirstOrDefault()) && t.Name == "Google Review").Select(t => t.Name).FirstOrDefault(),
                               ProductReviewType = input.ReviewType != 0 ? _reviewtypeRepository.GetAll().Where(t => t.Id == (_jobReviewRepository.GetAll().Where(r => r.JobId == o.Id && r.ReviewTypeId == input.ReviewType).Select(e => e.ReviewTypeId).FirstOrDefault()) && t.Name == "Prouct Review").Select(t => t.Name).FirstOrDefault()
                               : _reviewtypeRepository.GetAll().Where(t => t.Id == (_jobReviewRepository.GetAll().Where(r => r.JobId == o.Id && r.ReviewTypeId == 2).Select(e => e.ReviewTypeId).FirstOrDefault()) && t.Name == "Prouct Review").Select(t => t.Name).FirstOrDefault(),
                               ReviewRating = _jobReviewRepository.GetAll().Where(t => t.JobId == o.Id && t.ReviewTypeId == input.ReviewType).Select(t => t.Rating).FirstOrDefault(),
                               ReviewNotes = _jobReviewRepository.GetAll().Where(t => t.JobId == o.Id && t.ReviewTypeId == input.ReviewType).Select(t => t.ReviewNotes).FirstOrDefault(),
                           },
                           ReminderTime = leadactivitylog.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                           ActivityDescription = leadactivitylog.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           ActivityComment = leadactivitylog.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                           Yes = filteredJobs.Where(e => e.ReviewRating != null).Count(),
                           No = filteredJobs.Where(e => e.ReviewRating == null).Count(),
                           AssignUserName = userList.Where(e => e.Id == o.ReviewAssignId).Select(e => e.UserName).FirstOrDefault(),
                           AssignDate = o.ReviewAssignDate,
                           TotalEmailSend = leadactivitylog.Where(e => (e.TemplateId == 15 && e.TemplateId == 16)).Count(),
                           EmailSend = filteredJobs.Where(e => emailleadids.Contains((int)e.LeadId)).Count(),
                           EmailPending = leadactivitylog.Where(e => !emailleadids.Contains((int)e.LeadId)).Count(),
                           TotalRating = filteredJobs.Where(e => e.ReviewRating != null).Select(e => e.ReviewRating).Sum(),
                           totalraterecords = filteredJobs.Where(e => e.ReviewRating != null).Count(),
                       };

            var totalCount = await filteredJobs.CountAsync();

            return new PagedResultDto<GetReviewViewDto>(
                totalCount,
                await jobs.ToListAsync()
            );
        }


        //public async Task UpdateReview(List<CreateOrEditJobReviewDto> jobReviewList)
        //{
        //    try
        //    {
        //        var job = await _lookup_JobRepository.FirstOrDefaultAsync((int)jobReviewList[0].JobId);
        //        LeadActivityLog leadactivity = new LeadActivityLog();

        //        leadactivity.ActionId = 28;
        //        leadactivity.SectionId = 23;
        //        leadactivity.ActionNote = "Review Added";
        //        leadactivity.LeadId = Convert.ToInt32(job.LeadId);
        //        if (AbpSession.TenantId != null)
        //        {
        //            leadactivity.TenantId = (int)AbpSession.TenantId;
        //        }
        //        var jobactionid = _leadactivityRepository.InsertAndGetId(leadactivity);

        //        var list = new List<JobTrackerHistory>();

        //        if (jobReviewList != null)
        //        {
        //            var ReviewTypeList = _reviewtypeRepository.GetAll();
        //            var IdList = jobReviewList.Select(e => e.Id).ToList();
        //            var existingData = _jobReviewRepository.GetAll().AsNoTracking().Where(x => x.JobId == job.Id).ToList();

        //            if (existingData != null)
        //            {
        //                foreach (var item in existingData)
        //                {
        //                    if (IdList != null)
        //                    {
        //                        bool containsItem = IdList.Any(x => x == item.Id);
        //                        if (containsItem == false)
        //                        {
        //                            JobTrackerHistory jobhistory1 = new JobTrackerHistory();
        //                            if (AbpSession.TenantId != null)
        //                            {
        //                                jobhistory1.TenantId = (int)AbpSession.TenantId;
        //                            }
        //                            jobhistory1.FieldName = "Review Type";
        //                            jobhistory1.PrevValue = ReviewTypeList.Where(x => x.Id == item.ReviewTypeId).Select(x => x.Name).FirstOrDefault();
        //                            jobhistory1.CurValue = "Deleted";
        //                            jobhistory1.Action = "Review Type Delete";
        //                            jobhistory1.LastmodifiedDateTime = DateTime.Now;
        //                            jobhistory1.JobIDId = job.Id;
        //                            jobhistory1.JobActionId = jobactionid;
        //                            list.Add(jobhistory1);

        //                            JobTrackerHistory jobhistory = new JobTrackerHistory();
        //                            if (AbpSession.TenantId != null)
        //                            {
        //                                jobhistory.TenantId = (int)AbpSession.TenantId;
        //                            }
        //                            jobhistory.FieldName = "Review Rating";
        //                            jobhistory.PrevValue = item.Rating.ToString();
        //                            jobhistory.CurValue = "Deleted";
        //                            jobhistory.Action = "Review Rating Delete";
        //                            jobhistory.LastmodifiedDateTime = DateTime.Now;
        //                            jobhistory.JobIDId = job.Id;
        //                            jobhistory.JobActionId = jobactionid;
        //                            list.Add(jobhistory);

        //                            JobTrackerHistory jobhistory2 = new JobTrackerHistory();
        //                            if (AbpSession.TenantId != null)
        //                            {
        //                                jobhistory2.TenantId = (int)AbpSession.TenantId;
        //                            }
        //                            jobhistory2.FieldName = "Review Notes";
        //                            jobhistory2.PrevValue = item.ReviewNotes;
        //                            jobhistory2.CurValue = "Deleted";
        //                            jobhistory2.Action = "Review Notes Delete";
        //                            jobhistory2.LastmodifiedDateTime = DateTime.Now;
        //                            jobhistory2.JobIDId = job.Id;
        //                            jobhistory2.JobActionId = jobactionid;
        //                            list.Add(jobhistory2);

        //                            _jobReviewRepository.Delete(x => x.Id == item.Id);
        //                        }
        //                    }
        //                }
        //            }

        //            foreach (var jobreview in jobReviewList)
        //            {
        //                if (jobreview.Id != null && jobreview.Id != 0)
        //                {
        //                    var existData = _jobReviewRepository.GetAll().AsNoTracking().Where(x => x.Id == jobreview.Id).FirstOrDefault();
        //                    var olddetail = ObjectMapper.Map<JobReview>(jobreview);
        //                    olddetail.JobId = job.Id;
        //                    if (AbpSession.TenantId != null)
        //                    {
        //                        olddetail.TenantId = (int)AbpSession.TenantId;
        //                    }

        //                    _jobReviewRepository.Update(olddetail);

        //                    if (jobreview.ReviewTypeId != null && jobreview.ReviewTypeId != 0 && existData.ReviewTypeId != jobreview.ReviewTypeId)
        //                    {
        //                        var existVariation = ReviewTypeList.Where(x => x.Id == existData.ReviewTypeId).Select(x => x.Name).FirstOrDefault();
        //                        var currentVariation = ReviewTypeList.Where(x => x.Id == jobreview.ReviewTypeId).Select(x => x.Name).FirstOrDefault();

        //                        if (existVariation != currentVariation)
        //                        {
        //                            JobTrackerHistory jobhistory1 = new JobTrackerHistory();
        //                            if (AbpSession.TenantId != null)
        //                            {
        //                                jobhistory1.TenantId = (int)AbpSession.TenantId;
        //                            }
        //                            jobhistory1.FieldName = "Review Type";
        //                            jobhistory1.PrevValue = existVariation;
        //                            jobhistory1.CurValue = currentVariation;
        //                            jobhistory1.Action = "Review Type Edit";
        //                            jobhistory1.LastmodifiedDateTime = DateTime.Now;
        //                            jobhistory1.JobIDId = job.Id;
        //                            jobhistory1.JobActionId = jobactionid;
        //                            list.Add(jobhistory1);
        //                        }
        //                    }

        //                    if (jobreview.Rating != null && existData.Rating != jobreview.Rating)
        //                    {
        //                        JobTrackerHistory jobhistory = new JobTrackerHistory();
        //                        if (AbpSession.TenantId != null)
        //                        {
        //                            jobhistory.TenantId = (int)AbpSession.TenantId;
        //                        }
        //                        jobhistory.FieldName = "Review Rating";
        //                        jobhistory.PrevValue = existData.Rating.ToString();
        //                        jobhistory.CurValue = jobreview.Rating.ToString();
        //                        jobhistory.Action = "Review Rating Edit";
        //                        jobhistory.LastmodifiedDateTime = DateTime.Now;
        //                        jobhistory.JobIDId = job.Id;
        //                        jobhistory.JobActionId = jobactionid;
        //                        list.Add(jobhistory);
        //                    }

        //                    if (jobreview.ReviewNotes != null && existData.ReviewNotes != jobreview.ReviewNotes)
        //                    {
        //                        JobTrackerHistory jobhistory = new JobTrackerHistory();
        //                        if (AbpSession.TenantId != null)
        //                        {
        //                            jobhistory.TenantId = (int)AbpSession.TenantId;
        //                        }
        //                        jobhistory.FieldName = "Review Notes";
        //                        jobhistory.PrevValue = existData.ReviewNotes;
        //                        jobhistory.CurValue = jobreview.ReviewNotes;
        //                        jobhistory.Action = "Review Notes Edit";
        //                        jobhistory.LastmodifiedDateTime = DateTime.Now;
        //                        jobhistory.JobIDId = job.Id;
        //                        jobhistory.JobActionId = jobactionid;
        //                        list.Add(jobhistory);
        //                    }
        //                }
        //                else
        //                {
        //                    var olddetail = ObjectMapper.Map<JobReview>(jobreview);
        //                    olddetail.JobId = job.Id;
        //                    if (AbpSession.TenantId != null)
        //                    {
        //                        olddetail.TenantId = (int)AbpSession.TenantId;
        //                    }
        //                    _jobReviewRepository.Insert(olddetail);
        //                }
        //            }
        //        }

        //        if (list.Count > 0)
        //        {
        //            await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
        //            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
        //        }

        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //}
        public async Task<ListResultDto<GetJobReviewForEditOutput>> GetAllJobReview(EntityDto input)
        {
            var jobReview = _jobReviewRepository.GetAll().Where(e => e.JobId == input.Id);
           
            var result = jobReview
                .Select(document => new GetJobReviewForEditOutput
                {
                    Id=document.Id,
                    ReviewType= document.ReviewType.Name,
                    ReviewNote=document.ReviewNotes,
                    ReviewRating=document.Rating.ToString(),
                    FileName = document.FileName,
                    FilePath = document.FilePath,
                    
                    
                })
                .ToList();

            return new ListResultDto<GetJobReviewForEditOutput>(result);
        }

        public async Task DeleteJobReview(EntityDto input)
        {
            var jobReview = _jobReviewRepository.Get(input.Id);
 
            await _jobReviewRepository.DeleteAsync(input.Id);

        }
        /// Sms send From allTrackers
        public async Task SendSms(SmsEmailDto input)
        {

            var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)input.LeadId);
            if (!string.IsNullOrEmpty(lead.Mobile))
            {
                var Jobid = _lookup_JobRepository.GetAll().Where(e => e.LeadId == input.LeadId).Select(e => e.Id).FirstOrDefault();
                var job = await _lookup_JobRepository.FirstOrDefaultAsync(Jobid);
                var output = ObjectMapper.Map<CreateOrEditJobDto>(job);
                output.ReviewEmailSend = true;
                output.ReviewEmailSendDate = DateTime.UtcNow;
                ObjectMapper.Map(output, job);

                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 6;
                leadactivity.ActionNote = "Sms Send From Review";
                leadactivity.LeadId = (int)input.LeadId;
                leadactivity.Subject = input.Body;
                leadactivity.ActivityDate = DateTime.UtcNow;
                leadactivity.SectionId = input.TrackerId;
                if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                {
                    leadactivity.TemplateId = input.SMSTemplateId;
                }
                else
                {
                    leadactivity.TemplateId = 0;
                }
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                leadactivity.Body = input.Body;
                await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                SendSMSInput sendSMSInput = new SendSMSInput();
                sendSMSInput.PhoneNumber = lead.Mobile;
                sendSMSInput.Text = input.Body;
                sendSMSInput.ActivityId = leadactivity.Id;
                await _applicationSettings.SendSMS(sendSMSInput);
            }
        }

        /// Email send 
        public async Task SendEmail(SmsEmailDto input)
        {

            var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)input.LeadId);
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
            int? TemplateId;
            if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
            {
                TemplateId = input.EmailTemplateId;
            }
            else
            {
                TemplateId = 0;
            }

            if (!string.IsNullOrEmpty(lead.Email))
            {
                //MailMessage mail = new MailMessage
                //{
                //    From = new MailAddress(input.EmailFrom),
                //    To = { lead.Email }, //{ "hiral.prajapati@meghtechnologies.com" }, //
                //    Subject = input.Subject,
                //    Body = input.Body,
                //    IsBodyHtml = true
                //};
                //await this._emailSender.SendAsync(mail);

                if (input.cc != null && input.Bcc != null)
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                        CC = { input.cc },
                        Bcc = { input.Bcc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                }

                else if (input.cc != null && input.cc != "" && input.Bcc == null)
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                        CC = { input.cc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                }

                else if (input.cc == null && input.Bcc != null && input.Bcc != "")
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                        Bcc = { input.Bcc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                }

                else
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { lead.Email }, //{ "hiral.prajapati@meghtechnologies.com" }, //

                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                }
            }

            var Jobid = _lookup_JobRepository.GetAll().Where(e => e.LeadId == input.LeadId).Select(e => e.Id).FirstOrDefault();
            var job = await _lookup_JobRepository.FirstOrDefaultAsync(Jobid);
            var output = ObjectMapper.Map<CreateOrEditJobDto>(job);
            output.ReviewSmsSend = true;
            output.ReviewSmsSendDate = DateTime.UtcNow;
            ObjectMapper.Map(output, job);

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 7;
            leadactivity.ActionNote = "Email Send From Review";
            leadactivity.LeadId = (int)input.LeadId;
            leadactivity.TemplateId = TemplateId;
            leadactivity.Subject = input.Subject;
            leadactivity.Body = input.Body;
            leadactivity.SectionId = input.TrackerId;
            leadactivity.ActivityDate = DateTime.UtcNow;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }
        public async Task UpdateAssignUserID(List<int> Ids, int? userId)
        {
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == userId).FirstOrDefault();
            var assignedFromUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).Select(e => e.FullName).FirstOrDefault();

            foreach (var id in Ids)
            {
                var service = await _lookup_JobRepository.FirstOrDefaultAsync((int)id);
                service.ReviewAssignId = userId;
                service.ReviewAssignDate = DateTime.UtcNow;
                await _lookup_JobRepository.UpdateAsync(service);
            }
            string msg = string.Format(Ids.Count + " Review Assign" + " BY " + assignedFromUser);
            await _appNotifier.LeadAssiged(assignedToUser, msg, NotificationSeverity.Info);


        }
        public async Task<List<GetJobReviewForEditOutput>> GetJobReviewByJobId(int jobid)
        {
            var jobReview = _jobReviewRepository.GetAll().Where(x => x.JobId == jobid).ToList();

            var output = new List<GetJobReviewForEditOutput>();

            foreach (var item in jobReview)
            {
                var outobj = new GetJobReviewForEditOutput();
                outobj.JobReview = ObjectMapper.Map<CreateOrEditJobReviewDto>(item);
                outobj.JobReview.Id = item.Id;
                if (outobj.JobReview.ReviewTypeId != null)
                {
                    var reviewtype = await _reviewtypeRepository.FirstOrDefaultAsync((int)outobj.JobReview.ReviewTypeId);
                    outobj.ReviewType = reviewtype?.Name?.ToString();
                }

                if (outobj.JobReview.JobId != null)
                {
                    var _lookupJob = await _lookup_jobRepository.FirstOrDefaultAsync((int)outobj.JobReview.JobId);
                    outobj.ReviewNote = _lookupJob?.ReviewNotes?.ToString();
                    outobj.ReviewRating = _lookupJob?.ReviewRating?.ToString();
                }
                output.Add(outobj);
            }

            return output;
        }
    }
}
