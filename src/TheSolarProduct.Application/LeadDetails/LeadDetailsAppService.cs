﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using TheSolarProduct.LeadDetails.Dtos;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Linq;
using System.Linq.Dynamic.Core;
using TheSolarProduct.Jobs;
using TheSolarProduct.Leads;
using TheSolarProduct.Authorization.Users;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using Hangfire.Storage;
using TheSolarProduct.Authorization;
using TheSolarProduct.Organizations;
using Abp.Organizations;
using System;
using TheSolarProduct.PostCodes;
using TheSolarProduct.States;
using TheSolarProduct.LeadActivityLogs;
using Telerik.Reporting;
using TheSolarProduct.JobHistory;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using TheSolarProduct.Quickstock;

namespace TheSolarProduct.LeadDetails
{
    [AbpAuthorize]
    public class LeadDetailsAppService : TheSolarProductAppServiceBase, ILeadDetailsAppService
    {
        private readonly IRepository<Lead, int> _leadRepository;
        private readonly IRepository<Job, int> _jobRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<PylonDocument> _pylonDocumentRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendOrganizationUnitRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<PostCode, int> _postCodeRepository;
        private readonly IRepository<State, int> _stateRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<JobType> _jobTypeRepository;
        private readonly IRepository<Warehouselocation> _warehouselocationRepository;
        private readonly IRepository<JobProductItem> _jobProductItemsRepository;
        private readonly IQuickstockAppService _quickstockAppService;

        public LeadDetailsAppService(
            IRepository<Lead, int> leadRepository,
            IRepository<Job, int> jobRepository,
            IRepository<User, long> userRepository,
            UserManager userManager,
            IRepository<PylonDocument> pylonDocumentRepository,
            IRepository<ExtendOrganizationUnit, long> extendOrganizationUnitRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRepository<PostCode, int> postCodeRepository,
            IRepository<State, int> stateRepository,
            IRepository<LeadActivityLog> leadactivityRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
            IRepository<JobType> jobTypeRepository,
            IRepository<Warehouselocation> warehouselocationRepository,
            IRepository<JobProductItem> jobProductItemsRepository,
            IQuickstockAppService quickstockAppService
            )
        {
            _leadRepository = leadRepository;
            _jobRepository = jobRepository;
            _userRepository = userRepository;
            _userManager = userManager;
            _pylonDocumentRepository = pylonDocumentRepository;
            _extendOrganizationUnitRepository = extendOrganizationUnitRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _postCodeRepository = postCodeRepository;
            _stateRepository = stateRepository;
            _leadactivityRepository = leadactivityRepository;
            _dbcontextprovider = dbcontextprovider;
            _jobTypeRepository = jobTypeRepository;
            _warehouselocationRepository = warehouselocationRepository;
            _jobProductItemsRepository = jobProductItemsRepository;
            _quickstockAppService = quickstockAppService;
        }

        public async Task<GetLeadDetailsForOutput> GetLeadDetailsByLeadId(EntityDto input)
        {
            var output = new GetLeadDetailsForOutput();
            output.LeadDetails = new Dtos.LeadDetails();
            output.JobDetails = new Dtos.JobDetails();

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            output.CurrentUserRole = role[0];

            var lead = await _leadRepository.FirstOrDefaultAsync(input.Id);
            var job = await _jobRepository.GetAll().Include(e => e.JobStatusFk).Where(x => x.LeadId == input.Id).AsNoTracking().FirstOrDefaultAsync();

            output.LeadDetails.LeadId = lead.Id;
            output.LeadDetails.OrganizationId = lead.OrganizationId;
            output.LeadDetails.CompanyName = lead.CompanyName;
            output.LeadDetails.LeadStatusId = lead.LeadStatusId;
            output.LeadDetails.Mobile = lead.Mobile;
            output.LeadDetails.Phone = lead.Phone;
            output.LeadDetails.Email = lead.Email;
            output.LeadDetails.Longitude = lead.longitude;
            output.LeadDetails.Latitude = lead.latitude;

            var curruntUser = await _userRepository.FirstOrDefaultAsync((int)lead.AssignToUserID);
            output.LeadDetails.CurrentAssignUserName = curruntUser.FullName?.ToString();

            var createdUser = await _userRepository.FirstOrDefaultAsync((int)lead.CreatorUserId);
            output.LeadDetails.CreatedByName = createdUser.FullName?.ToString();

            output.LeadDetails.CreatedOn = lead.CreationTime;
            output.LeadDetails.Address = lead.Address;
            output.LeadDetails.UnitNo = lead.UnitNo;
            output.LeadDetails.UnitType = lead.UnitType;
            output.LeadDetails.Suburb = lead.Suburb;
            output.LeadDetails.State = lead.State;
            output.LeadDetails.PostCode = lead.PostCode;
            output.LeadDetails.StreetNo = lead.StreetNo;
            output.LeadDetails.StreetName = lead.StreetName;
            output.LeadDetails.StreetType = lead.StreetType;

            //Other Details
            output.LeadDetails.Area = lead.Area;
            output.LeadDetails.Type = lead.Type;
            output.LeadDetails.LeadSource = lead.LeadSource;
            output.LeadDetails.Requirements = lead.Requirements;
            output.LeadDetails.FormName = lead.FormName;

            output.LeadDetails.IsGoogle = lead.IsGoogle;

            if (job != null)
            {
                output.JobDetails.JobId = job.Id;
                output.JobDetails.JobNumber = job.JobNumber;
                output.JobDetails.JobStatusId = job.JobStatusId;
                output.JobDetails.JobStatus = job.JobStatusFk.Name;
                output.JobDetails.JobStatusColorClass = job.JobStatusFk.ColorClass;
                output.JobDetails.DepositeRecceivedDate = job.DepositeRecceivedDate;
                output.JobDetails.ActiveDate = job.ActiveDate;
                output.JobDetails.IsJobCancelRequest = job.IsJobCancelRequest;

                var pylonDocument = await _pylonDocumentRepository.GetAll().Where(e => e.JobId == job.Id).OrderByDescending(e => e.Id).FirstOrDefaultAsync();
                if (pylonDocument != null)
                {
                    output.JobDetails.PylonUrl = pylonDocument.Url;
                }

                //Jobs
                output.JobDetails.JobTypeId = job.JobTypeId;
                output.JobDetails.ManualQuote = job.ManualQuote;
                output.JobDetails.PriceType = job.PriceType;

                //Address
                output.JobDetails.UnitNo = job.UnitNo;
                output.JobDetails.UnitType = job.UnitType;
                output.JobDetails.StreetNo = job.StreetNo;
                output.JobDetails.StreetName = job.StreetName;
                output.JobDetails.StreetType = job.StreetType;
                output.JobDetails.Suburb = job.Suburb;
                output.JobDetails.State = job.State;
                output.JobDetails.PostCode = job.PostalCode;
                output.JobDetails.Longitude = job.Longitude;
                output.JobDetails.Latitude = job.Latitude;
                output.JobDetails.Country = job.Country;

                //Notes
                output.JobDetails.Note = job.Note;
                output.JobDetails.OldSystemDetails = job.OldSystemDetails;
                output.JobDetails.InstallerNotes = job.InstallerNotes;

                //WarehouseDistance
                if (!string.IsNullOrEmpty(lead.longitude) && !string.IsNullOrEmpty(lead.latitude))
                {
                    var location = await _warehouselocationRepository.GetAll().Where(e => e.state == lead.State).FirstOrDefaultAsync();
                    if (location != null)
                    {
                        //Warehouse Lat lang
                        var warehouseLatitude = !string.IsNullOrEmpty(location.Latitude) ? Convert.ToDouble(location.Latitude) : 0;
                        var warehouseLongitude = !string.IsNullOrEmpty(location.Longitude) ? Convert.ToDouble(location.Longitude) : 0;

                        output.JobDetails.WarehouseDistance = GetDistance(warehouseLatitude, warehouseLongitude, Convert.ToDouble(lead.latitude), Convert.ToDouble(lead.longitude));
                    }
                }

                //Rebate
                output.JobDetails.SystemCapacity = job.SystemCapacity;
                output.JobDetails.STC = job.STC;
                output.JobDetails.STCPrice = 36;
                output.JobDetails.Rebate = job.Rebate;

                //Job Product Items
                var jobProductItems = _jobProductItemsRepository.GetAll().Where(e => e.JobId == job.Id).Select(e => new GetJobProductItemDto { Id = e.Id, ProductTypeId = e.ProductItemFk.ProductTypeId, ProductItemId = e.ProductItemId, Name = e.ProductItemFk.Name, Model = e.ProductItemFk.Model, Size = e.ProductItemFk.Size, Quantity = e.Quantity }).AsNoTracking().ToList();

                if(jobProductItems != null && PermissionChecker.IsGranted(AppPermissions.Pages_LeadDetails_CheckLiveStock))
                {
                    foreach (var item in jobProductItems)
                    {
                        item.LiveStock = _quickstockAppService.GetStockDetailByState((int)item.ProductItemId, job.State).StockOnHand;
                    }

                    output.JobDetails.JobProducts = jobProductItems;
                }

                //output.JobDetails.JobProducts = (from jp in jobProductItems
                //                                 select new GetJobProductItemDto
                //                                 {
                //                                     Id = jp.Id,
                //                                     ProductTypeId = jp.ProductTypeId,
                //                                     ProductItemId = jp.ProductItemId,
                //                                     Name = jp.Name,
                //                                     Model = jp.Model,
                //                                     Size = jp.Size,
                //                                     Quantity = jp.Quantity,
                //                                     LiveStock = _quickstockAppService.GetStockDetailByState((int)jp.ProductItemId, job.State).StockOnHand
                //                                 }).ToList();

                //Installation Details
                output.JobDetails.InstallationDate = job.InstallationDate;
            }

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Jobs_Create)]
        public async Task<string> CreateNewJob(CreateJobDto createJobDto)
        {
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            var lead = await _leadRepository.FirstOrDefaultAsync(createJobDto.LeadId);

            if(lead.AssignToUserID > 0)
            {
                if (role.Contains("Admin"))
                {
                    return await CreateJob(createJobDto, lead);
                }
                else
                {
                    if(createJobDto.JobTypeId == 1)
                    {
                        var duplicateAdd = _jobRepository.GetAll().Where(e => (e.UnitNo == createJobDto.Address.UnitNo && e.UnitType == createJobDto.Address.UnitType && e.StreetNo == createJobDto.Address.StreetNo && e.StreetName == createJobDto.Address.StreetName && e.StreetType == createJobDto.Address.StreetType && e.Suburb == createJobDto.Address.Suburb && e.State == createJobDto.Address.State && e.PostalCode == createJobDto.Address.PostCode) && e.LeadFk.OrganizationId == lead.OrganizationId).Any();

                        if(duplicateAdd)
                        {
                            return "AddreessAlredyExist";
                        }
                        else
                        {
                            return await CreateJob(createJobDto, lead);
                        }
                    }
                    else
                    {
                        return await CreateJob(createJobDto, lead);
                    }
                }
            }
            else
            {
                return "OnlyAssignUserCanCreateJob";
            }
        }

        protected virtual async Task<string> CreateJob(CreateJobDto input, Lead lead)
        {
            var ProjectCode = _extendOrganizationUnitRepository.GetAll().Where(e => e.Id == lead.OrganizationId).Select(e => e.ProjectId).FirstOrDefault();
            var OrganizationCode = _organizationUnitRepository.GetAll().Where(e => e.Id == lead.OrganizationId).Select(e => e.Code).FirstOrDefault();
            var PrevJobNumber = (from j in _jobRepository.GetAll()
                                 where (j.LeadFk.OrganizationId == lead.OrganizationId)
                                 select new { j.JobNumber, j.Id })
                                .OrderByDescending(e => e.Id).FirstOrDefault();

            var JobNumber = ProjectCode;
            if (PrevJobNumber != null)
            {
                if (PrevJobNumber.JobNumber != null)
                {
                    var JobNumberSplit = PrevJobNumber.JobNumber.Replace(OrganizationCode, "");
                    JobNumber = Convert.ToString(Convert.ToInt32(JobNumberSplit) + 1);
                }
            }

            var job = new Jobs.Job();
            job.LeadId = input.LeadId;
            job.JobStatusId = 1;
            job.JobTypeId = input.JobTypeId;
            job.JobNumber = OrganizationCode + JobNumber;
            if (AbpSession.TenantId != null)
            {
                job.TenantId = (int)AbpSession.TenantId;
            }

            job.UnitNo = input.Address.UnitNo;
            job.UnitType = input.Address.UnitType;
            job.StreetNo = input.Address.StreetNo;
            job.StreetName = input.Address.StreetName;
            job.StreetType = input.Address.StreetType;
            job.Address = input.Address.UnitNo + " " + input.Address.UnitType + " " + input.Address.StreetNo + " " + input.Address.StreetName + " " + input.Address.StreetType;
            job.IsRefferalVerify = false;

            if(!string.IsNullOrEmpty(input.Address.Suburb))
            {
                job.Suburb = input.Address.Suburb;
                job.SuburbId = await _postCodeRepository.GetAll().Where(e => e.Suburb == input.Address.Suburb.Trim()).Select(e => e.Id).FirstOrDefaultAsync();
            }

            if (!string.IsNullOrEmpty(input.Address.State))
            {
                job.State = input.Address.State;
                job.StateId = await _stateRepository.GetAll().Where(e => e.Name == input.Address.State.Trim()).Select(e => e.Id).FirstOrDefaultAsync();
            }
            job.PostalCode = input.Address.PostCode;
            job.Country = input.Address.Country;
            job.Longitude = input.Address.Longitude;
            job.Latitude = input.Address.Latitude;

            var jobId = await _jobRepository.InsertAndGetIdAsync(job);

            lead.LeadStatusId = 6;
            await _leadRepository.UpdateAsync(lead);

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 10;
            leadactivity.SectionId = input.SectionId;
            leadactivity.ActionNote = "Job Created";
            leadactivity.LeadId = Convert.ToInt32(input.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);

            return "Success";
        }

        public async Task<GetJobStatusDto> GetJobStatus(EntityDto input)
        {
            var output = await _jobRepository.GetAll().Include(e => e.JobStatusFk).Where(e => e.Id == input.Id).Select(e => new GetJobStatusDto { JobStatusId = e.JobStatusId, JobStatus = e.JobStatusFk.Name, JobStatusColorClass = e.JobStatusFk.ColorClass }).FirstOrDefaultAsync();

            return output;
        }

        public async Task<string> UpdateJobAddress(EditJobAddressDto editJobDto)
        {
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            if (role.Contains("Admin"))
            {
                return await EditJobAddress(editJobDto);
            }
            else
            {
                var duplicateAdd = _jobRepository.GetAll().Where(e => (e.UnitNo == editJobDto.Address.UnitNo && e.UnitType == editJobDto.Address.UnitType && e.StreetNo == editJobDto.Address.StreetNo && e.StreetName == editJobDto.Address.StreetName && e.StreetType == editJobDto.Address.StreetType && e.Suburb == editJobDto.Address.Suburb && e.State == editJobDto.Address.State && e.PostalCode == editJobDto.Address.PostCode) && e.Id != editJobDto.JobId).Any();

                if (duplicateAdd)
                {
                    return "AddreessAlredyExist";
                }
                else
                {
                    return await EditJobAddress(editJobDto);
                }
            }
        }

        protected virtual async Task<string> EditJobAddress(EditJobAddressDto input)
        {
            var job = await _jobRepository.FirstOrDefaultAsync(input.JobId);

            var address = (input.Address.UnitNo + " " + input.Address.UnitType + " " + input.Address.StreetNo + " " + input.Address.StreetName + " " + input.Address.StreetType).Trim();

            #region Edit Activity Log
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 11;
            leadactivity.SectionId = input.SectionId;
            leadactivity.ActionNote = "Job Address Updated";
            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
            leadactivity.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
            var jobActionId = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

            var jobHistoryList = new List<JobTrackerHistory>();
            if (job.Address != address)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "Address";
                jobHistory.PrevValue = job.Address;
                jobHistory.CurValue = address;
                jobHistory.Action = "Site Address Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                jobHistoryList.Add(jobHistory);
            }
            if (job.UnitNo != input.Address.UnitNo)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "UnitNo";
                jobHistory.PrevValue = job.UnitNo;
                jobHistory.CurValue = input.Address.UnitNo;
                jobHistory.Action = "Site Address Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                jobHistoryList.Add(jobHistory);
            }
            if (job.UnitType != input.Address.UnitType)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "UnitType";
                jobHistory.PrevValue = job.UnitType;
                jobHistory.CurValue = input.Address.UnitType;
                jobHistory.Action = "Site Address Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                jobHistoryList.Add(jobHistory);
            }
            if (job.StreetNo != input.Address.StreetNo)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "StreetNo";
                jobHistory.PrevValue = job.StreetNo;
                jobHistory.CurValue = input.Address.StreetNo;
                jobHistory.Action = "Site Address Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                jobHistoryList.Add(jobHistory);
            }
            if (job.StreetName != input.Address.StreetName)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "StreetName";
                jobHistory.PrevValue = job.StreetName;
                jobHistory.CurValue = input.Address.StreetName;
                jobHistory.Action = "Site Address Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                jobHistoryList.Add(jobHistory);
            }
            if (job.StreetType != input.Address.StreetType)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "StreetType";
                jobHistory.PrevValue = job.StreetType;
                jobHistory.CurValue = input.Address.StreetType;
                jobHistory.Action = "Site Address Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                jobHistoryList.Add(jobHistory);
            }
            if (job.Suburb != input.Address.Suburb)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "Suburb";
                jobHistory.PrevValue = job.Suburb;
                jobHistory.CurValue = input.Address.Suburb;
                jobHistory.Action = "Site Address Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                jobHistoryList.Add(jobHistory);
            }
            if (job.State != input.Address.State)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "State";
                jobHistory.PrevValue = job.State;
                jobHistory.CurValue = input.Address.State;
                jobHistory.Action = "Site Address Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                jobHistoryList.Add(jobHistory);
            }
            if (job.PostalCode != input.Address.PostCode)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "PostalCode";
                jobHistory.PrevValue = job.PostalCode;
                jobHistory.CurValue = input.Address.PostCode;
                jobHistory.Action = "Site Address Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                jobHistoryList.Add(jobHistory);
            }
            if (job.Country != input.Address.Country)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "Country";
                jobHistory.PrevValue = job.Country;
                jobHistory.CurValue = input.Address.Country;
                jobHistory.Action = "Site Address Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                jobHistoryList.Add(jobHistory);
            }
            if (job.Longitude != input.Address.Longitude)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "Longitude";
                jobHistory.PrevValue = job.Longitude;
                jobHistory.CurValue = input.Address.Longitude;
                jobHistory.Action = "Site Address Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                jobHistoryList.Add(jobHistory);
            }
            if (job.Latitude != input.Address.Latitude)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "Latitude";
                jobHistory.PrevValue = job.Latitude;
                jobHistory.CurValue = input.Address.Latitude;
                jobHistory.Action = "Site Address Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                jobHistoryList.Add(jobHistory);
            }
            #endregion

            job.Address = address;
            job.UnitNo = input.Address.UnitNo;
            job.UnitType = input.Address.UnitType;
            job.StreetNo = input.Address.StreetNo;
            job.StreetName = input.Address.StreetName;
            job.StreetType = input.Address.StreetType;
            
            if (!string.IsNullOrEmpty(input.Address.Suburb))
            {
                job.Suburb = input.Address.Suburb;
                job.SuburbId = await _postCodeRepository.GetAll().Where(e => e.Suburb == input.Address.Suburb.Trim()).Select(e => e.Id).FirstOrDefaultAsync();
            }

            if (!string.IsNullOrEmpty(input.Address.State))
            {
                job.State = input.Address.State;
                job.StateId = await _stateRepository.GetAll().Where(e => e.Name == input.Address.State.Trim()).Select(e => e.Id).FirstOrDefaultAsync();
            }
            job.PostalCode = input.Address.PostCode;
            job.Country = input.Address.Country;
            job.Longitude = input.Address.Longitude;
            job.Latitude = input.Address.Latitude;

            await _jobRepository.UpdateAsync(job);
            if (jobHistoryList.Count > 0)
            {
                await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(jobHistoryList);
                await _dbcontextprovider.GetDbContext().SaveChangesAsync();
            }

            return "Success";
        }

        [HttpPost]
        public async Task<string> UpdateSalesTab(EditSalesTabDto input)
        {
            var job = await _jobRepository.FirstOrDefaultAsync(input.JobId);

            var jobHistoryList = await SalesEditActivityHistoryAsync(job, input);

            //Job Details
            job.JobTypeId = input.JobTypeId;
            job.ManualQuote = input.ManualQuote;
            job.PriceType = input.PriceType;
            job.Note = input.Note;
            job.InstallerNotes = input.InstallerNotes;
            job.OldSystemDetails = input.OldSystemDetails;

            //Rebare
            job.SystemCapacity = input.SystemCapacity;
            job.STC = input.STC;
            job.Rebate = input.Rebate;


            //await _jobRepository.UpdateAsync(job);
            //if (jobHistoryList.Count > 0)
            //{
            //    await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(jobHistoryList);
            //    await _dbcontextprovider.GetDbContext().SaveChangesAsync();
            //}
            return "Success";
        }

        protected virtual async Task<List<JobTrackerHistory>> SalesEditActivityHistoryAsync(Job job, EditSalesTabDto input)
        {
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 11;
            leadactivity.SectionId = input.SectionId;
            leadactivity.ActionNote = "Job Sales Updated";
            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
            leadactivity.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
            //var jobActionId = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);
            var jobActionId = 0;

            //Job Details
            var historyList = new List<JobTrackerHistory>();
            if (job.JobTypeId != input.JobTypeId)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "JobType";
                jobHistory.PrevValue = _jobTypeRepository.GetAll().Where(e => e.Id == job.JobTypeId).Select(e => e.Name).FirstOrDefault();
                jobHistory.CurValue = _jobTypeRepository.GetAll().Where(e => e.Id == input.JobTypeId).Select(e => e.Name).FirstOrDefault();
                jobHistory.Action = "Job Type Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                historyList.Add(jobHistory);
            }
            if (job.ManualQuote != input.ManualQuote)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "ManualQuote";
                jobHistory.PrevValue = job.ManualQuote;
                jobHistory.CurValue = input.ManualQuote;
                jobHistory.Action = "Manual Quote Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                historyList.Add(jobHistory);
            }
            if (job.PriceType != input.PriceType)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "PriceType";
                jobHistory.PrevValue = job.PriceType;
                jobHistory.CurValue = input.PriceType;
                jobHistory.Action = "Price Type Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                historyList.Add(jobHistory);
            }
            if (job.Note != input.Note)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "Note";
                jobHistory.PrevValue = job.Note;
                jobHistory.CurValue = input.Note;
                jobHistory.Action = "Note Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                historyList.Add(jobHistory);
            }
            if (job.OldSystemDetails != input.OldSystemDetails)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "OldSystemDetails";
                jobHistory.PrevValue = job.OldSystemDetails;
                jobHistory.CurValue = input.OldSystemDetails;
                jobHistory.Action = "Old System Details Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                historyList.Add(jobHistory);
            }
            if (job.InstallerNotes != input.InstallerNotes)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "InstallerNotes";
                jobHistory.PrevValue = job.InstallerNotes;
                jobHistory.CurValue = input.InstallerNotes;
                jobHistory.Action = "Installer Notes Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                historyList.Add(jobHistory);
            }

            //Rebate
            if (job.SystemCapacity != input.SystemCapacity)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "System Capacity";
                jobHistory.PrevValue = job.SystemCapacity != null ? job.SystemCapacity.ToString() : "";
                jobHistory.CurValue = input.SystemCapacity != null ? input.SystemCapacity.ToString() : "";
                jobHistory.Action = "System Capacity Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                historyList.Add(jobHistory);
            }
            if (job.STC != input.STC)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "STC";
                jobHistory.PrevValue = job.STC != null ? job.STC.ToString() : "";
                jobHistory.CurValue = input.STC != null ? input.STC.ToString() : "";
                jobHistory.Action = "STC Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                historyList.Add(jobHistory);
            }
            if (job.Rebate != input.Rebate)
            {
                JobTrackerHistory jobHistory = new JobTrackerHistory();
                jobHistory.TenantId = AbpSession.TenantId != null ? (int)AbpSession.TenantId : 0;
                jobHistory.FieldName = "Rebate";
                jobHistory.PrevValue = job.Rebate != null ? job.Rebate.ToString() : "";
                jobHistory.CurValue = input.Rebate != null ? input.Rebate.ToString() : "";
                jobHistory.Action = "Rebate Updated";
                jobHistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobHistory.JobIDId = job.Id;
                jobHistory.JobActionId = jobActionId;
                historyList.Add(jobHistory);
            }

            return historyList;
        }
    }
}
