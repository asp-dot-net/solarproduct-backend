﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.IncoTerms.Dtos;

namespace TheSolarProduct.IncoTerms.Exporting
{
    public interface IIncoTermExcelExporter
    {
        FileDto ExportToFile(List<GetIncoTermForVIewDto> IncoTerm);
    }
}
