﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.FreightCompanies.Exporting;
using TheSolarProduct.FreightCompanyes.Dtos;
using TheSolarProduct.IncoTerms.Dtos;
using TheSolarProduct.Storage;

namespace TheSolarProduct.IncoTerms.Exporting
{
    
    public class IncoTermExcelExporter : NpoiExcelExporterBase, IIncoTermExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public IncoTermExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetIncoTermForVIewDto> IncoTerm)
        {
            return CreateExcelPackage(
                "IncoTerm.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("IncoTerm"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("IsActive")
                    );

                    AddObjects(
                        sheet,
                        2,
                         IncoTerm,
                        _ => _.IncoTerm.Name,
                        _ => _.IncoTerm.IsActive.HasValue ? (_.IncoTerm.IsActive.Value ? L("Yes") : L("No")) : L("No")
                    );
                }
            );
        }


    }
}
