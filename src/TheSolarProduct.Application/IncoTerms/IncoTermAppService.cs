﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataVaults;
using TheSolarProduct.Dto;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.FreightCompanies.Exporting;
using TheSolarProduct.FreightCompanies;
using TheSolarProduct.FreightCompanyes.Dtos;
using TheSolarProduct.FreightCompanyes;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.IncoTerms.Exporting;
using TheSolarProduct.IncoTerms.Dtos;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace TheSolarProduct.IncoTerms
{
    
    [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_IncoTerm)]
    public class IncoTermAppService : TheSolarProductAppServiceBase, IIncoTermAppService
    {
        private readonly IIncoTermExcelExporter _incoTermExcelExporter;
        private readonly IRepository<IncoTerm> _IncoTermRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public IncoTermAppService(
              IRepository<IncoTerm> incoTermRepositoryRepository,
              IIncoTermExcelExporter IIncoTermExcelExporter,
              IRepository<UserTeam> userTeamRepository,
              IRepository<UserRole, long> userRoleRepository,
              IRepository<Role> roleRepository,
              IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
              )
        {
            _IncoTermRepository = incoTermRepositoryRepository;
            _incoTermExcelExporter = IIncoTermExcelExporter;
            _userTeamRepository = userTeamRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }




        public async Task<PagedResultDto<GetIncoTermForVIewDto>> GetAll(GetAllIncoTermInput input)
        {

            var IncoTerm = _IncoTermRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFilteredIncoTerm = IncoTerm
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var outputIncoTerm = from o in pagedAndFilteredIncoTerm
                                       select new GetIncoTermForVIewDto()

                                       {
                                           IncoTerm = new IncoTermDto
                                           {
                                               Id = o.Id,
                                               Name = o.Name,
                                               IsActive = o.IsActive
                                           }
                                       };

            var totalCount = await IncoTerm.CountAsync();

            return new PagedResultDto<GetIncoTermForVIewDto>(
                totalCount,
                await outputIncoTerm.ToListAsync()
            );
        }



        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_IncoTerm_Edit)]
        public async Task<GetIncoTermForEditOutput> GetIncoTermForEdit(EntityDto input)
        {
            var IncoTerm = await _IncoTermRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetIncoTermForEditOutput { IncoTerm = ObjectMapper.Map<CreateOrEditIncoTermDto>(IncoTerm) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditIncoTermDto input)
        {


            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_IncoTerm_Create)]

        protected virtual async Task Create(CreateOrEditIncoTermDto input)
        {
            var IncoTerm = ObjectMapper.Map<IncoTerm>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 72;
            dataVaultLog.ActionNote = "IncoTerm Created : " + input.Name;
            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _IncoTermRepository.InsertAsync(IncoTerm);
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_IncoTerm_Edit)]
        protected virtual async Task Update(CreateOrEditIncoTermDto input)
        {
            var IncoTerm = await _IncoTermRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 72;
            dataVaultLog.ActionNote = "IncoTerm Updated : " + IncoTerm.Name;

            dataVaultLog.IsInventory = true;
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != IncoTerm.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = IncoTerm.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != IncoTerm.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = IncoTerm.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, IncoTerm);

            await _IncoTermRepository.UpdateAsync(IncoTerm);


        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_IncoTerm_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _IncoTermRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 72;
            dataVaultLog.ActionNote = "IncoTerm Deleted : " + Name;

            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _IncoTermRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_IncoTerm_Export)]

        public async Task<FileDto> GetIncoTermToExcel(GetAllIncoTermForExcelInput input)
        {

            var filteredIncoTerm = _IncoTermRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredIncoTerm
                         select new GetIncoTermForVIewDto()
                         {
                             IncoTerm = new IncoTermDto
                             {
                                 Name = o.Name,
                                 Id = o.Id,
                                 IsActive = o.IsActive
                             }
                         });


            var ListDtos = await query.ToListAsync();

            return _incoTermExcelExporter.ExportToFile(ListDtos);
        }
    }
}
