﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using TheSolarProduct.MultiTenancy.Accounting.Dto;

namespace TheSolarProduct.MultiTenancy.Accounting
{
    public interface IInvoiceAppService
    {
        Task<InvoiceDto> GetInvoiceInfo(EntityDto<long> input);

        Task CreateInvoice(CreateInvoiceDto input);
    }
}
