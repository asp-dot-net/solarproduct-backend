﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheSolarProduct.MultiTenancy.HostDashboard.Dto;

namespace TheSolarProduct.MultiTenancy.HostDashboard
{
    public interface IIncomeStatisticsService
    {
        Task<List<IncomeStastistic>> GetIncomeStatisticsData(DateTime startDate, DateTime endDate,
            ChartDateInterval dateInterval);
    }
}