﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.States.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using TheSolarProduct.StreetNames.Dtos;
using TheSolarProduct.StreetNames;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.StreetTypes.Dtos;
using TheSolarProduct.StreetTypes;

namespace TheSolarProduct.States
{
    public class StateAppService : TheSolarProductAppServiceBase, IStateAppService
    {
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public StateAppService(IRepository<State> stateRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider) 
        {
            _stateRepository = stateRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task<PagedResultDto<GetStateForViewDto>> GetAll(GetAllStateInput input)
        {
            var states = _stateRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFilteredStates = states
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var outputState = from o in pagedAndFilteredStates
                              select new GetStateForViewDto()
                              {
                                  States = new StatesDto
                                  {
                                      Id = o.Id,
                                      Name = o.Name,
                                      IsReport = o.IsReport,
                                      BatteryRebate = o.BatteryRebate,
                                      IsActiveBatteryRebate = o.IsActiveBatteryRebate,
                                      BatteryBess = o.BatteryBess,
                                  }
                              };
            
            var totalCount = await states.CountAsync();

            return new PagedResultDto<GetStateForViewDto>(
                totalCount,
                await outputState.ToListAsync()
            );

        }

        public async Task UpdateRebate(StatesDto input)
        {
            var state = await _stateRepository.FirstOrDefaultAsync((int)input.Id);
            

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 78;
            dataVaultLog.ActionNote = "State Updated : " + state.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != state.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = state.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.BatteryRebate != state.BatteryRebate)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "BatteryRebate";
                history.PrevValue = state.BatteryRebate.ToString();
                history.CurValue = input.BatteryRebate.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActiveBatteryRebate != state.IsActiveBatteryRebate)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActiveBatteryRebate";
                history.PrevValue = state.IsActiveBatteryRebate == true ? "Yes" : "No";
                history.CurValue = input.IsActiveBatteryRebate == true ? "Yes" : "No";
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.BatteryBess != state.BatteryBess)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "BatteryBess";
                history.PrevValue = state.BatteryBess.ToString();
                history.CurValue = input.BatteryBess.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, state);
        }

        public async Task<StatesDto> GetStateForEdit(EntityDto input)
        {
            var state = await _stateRepository.FirstOrDefaultAsync((int)input.Id);

            var output = ObjectMapper.Map<StatesDto>(state) ;

            return output;
        }
    }
}
