﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TheSolarProduct.Podiums
{
    public class PodiumAppService : TheSolarProductAppServiceBase, IPodiumAppService
    {
        private static string clientId = "eddca896-8bcf-4f38-92cb-6dc7a30c5f4e";
        private static string clientSecret = "a45be376ce91031bfc640f1bca218a07e25612fb4d058a45fbcd85c070fd6abe";
        private static string redirectUri = "https://greatsparklykayak89.conveyor.cloud/api/services/app/Podium/Callback";

        public PodiumAppService()
        {
        }

        public async Task<string> GetAuthorizationUrl()
        {
            var authorizationEndpoint = "https://api.podium.com/oauth/authorize";
            var grant_type = "authorization_code";
            var scope = "write_messages";

            var authorizationUrl = $"{authorizationEndpoint}?grant_type={grant_type}&client_id={clientId}&redirect_uri={redirectUri}&scope={scope}";

            return authorizationUrl;
        }

        public async Task Callback(string code)
        {
            var AuthCode = code;
        }
    }
}
