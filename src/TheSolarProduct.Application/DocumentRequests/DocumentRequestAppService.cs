﻿using Abp.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using TheSolarProduct.DocumentRequests.Dtos;
using TheSolarProduct.Quotations;
using Abp.Domain.Repositories;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Jobs;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.ApplicationSettings.Dto;
using System.Net.Mail;
using TheSolarProduct.Leads;
using Abp.Runtime.Security;

using Abp.Domain.Uow;
using Abp.Organizations;
using TheSolarProduct.Organizations;
using Abp.Authorization;
using TheSolarProduct.Authorization;

namespace TheSolarProduct.DocumentRequests
{
    public class DocumentRequestAppService : TheSolarProductAppServiceBase, IDocumentRequestAppService
    {
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Job, int> _jobRepository;
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<OrganizationUnit, long> _OrganizationRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendedOrganizationUnitRepository;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<DocumentRequestLinkHistory> _documentRequestLinkHistory;
        private readonly IRepository<DocumentRequest> _documentRequestRepository;
        private readonly IRepository<DocumentType> _documentTypeRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public DocumentRequestAppService(
              IRepository<User, long> userRepository
            , IRepository<Job, int> jobRepository
            , IRepository<Lead> leadRepository
            , IRepository<OrganizationUnit, long> organizationRepository
            , IRepository<ExtendOrganizationUnit, long> extendedOrganizationUnitRepository
            , IApplicationSettingsAppService applicationSettings
            , IEmailSender emailSender
            , IRepository<LeadActivityLog> leadactivityRepository
            , IRepository<DocumentRequestLinkHistory> documentRequestLinkHistory
            , IRepository<DocumentRequest> documentRequestRepository
            , IRepository<DocumentType> documentTypeRepository
            , IUnitOfWorkManager unitOfWorkManager
            )
        { 
            _userRepository = userRepository;
            _jobRepository = jobRepository;
            _leadRepository = leadRepository;
            _OrganizationRepository = organizationRepository;
            _extendedOrganizationUnitRepository = extendedOrganizationUnitRepository;
            _applicationSettings = applicationSettings;
            _emailSender = emailSender;
            _leadactivityRepository = leadactivityRepository;
            _documentRequestLinkHistory = documentRequestLinkHistory;
            _documentRequestRepository = documentRequestRepository;
            _documentTypeRepository = documentTypeRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task SendDocumentRequestForm(SendDocumentRequestInputDto input)
        {
            DocumentRequest documentRequest = new DocumentRequest();
            documentRequest.JobId = input.JobId;
            documentRequest.DocTypeId = input.DocTypeId;
            documentRequest.IsSubmitted = false;
            var docRequestId = await _documentRequestRepository.InsertAndGetIdAsync(documentRequest);

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var documentsTypes = _documentTypeRepository.FirstOrDefault(input.DocTypeId);

            var job = _jobRepository.GetAll().Where(e => e.Id == input.JobId).FirstOrDefault();
            var lead = _leadRepository.GetAll().Where(e => e.Id == job.LeadId).FirstOrDefault();
            var Body = job.TenantId + "," + job.Id + "," + docRequestId;

            //Token With Tenant Id & Promotion User Primary Id for subscribe & UnSubscribe
            var token = SimpleStringCipher.Instance.Encrypt(Body, AppConsts.DefaultPassPhrase);

            string BaseURL = System.Configuration.ConfigurationManager.AppSettings["ClientRootAddress"];
            Body = "https://thesolarproduct.com/account/request-document?STR=" + HttpUtility.UrlEncode(token);

            DocumentRequestLinkHistory documentRequestLinkHistory = new DocumentRequestLinkHistory();
            documentRequestLinkHistory.Expired = false;
            documentRequestLinkHistory.DocumentRequestId = docRequestId;
            documentRequestLinkHistory.Token = token;
            await _documentRequestLinkHistory.InsertAsync(documentRequestLinkHistory);

            //var LeadAssignUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();

            var orgName = _OrganizationRepository.GetAll().Where(e => e.Id == lead.OrganizationId).Select(e => e.DisplayName).FirstOrDefault();
            var FromEmail = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == lead.OrganizationId).Select(e => e.defaultFromAddress).FirstOrDefault();
            
            var ClickHere = Body;
            var leadassignusername = UserDetail == null ? "" : UserDetail.Name;

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.SectionId = input.SectionId;
            if (input.SendMode == "SMS")
            {
                leadactivity.IsMark = false;
            }
            else
            {
                leadactivity.IsMark = null;
            }
            leadactivity.ActionId = 12;
            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

            var OrgLogo = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == job.LeadFk.OrganizationId).Select(e => (e.LogoFilePath + e.LogoFileName)).FirstOrDefault();
            OrgLogo = ApplicationSettingConsts.ViewDocumentPath + (OrgLogo != null ? OrgLogo.Replace("\\", "/") : "");

            string FinalSMSBody = "Dear " + lead.CompanyName + "\n\nYour Solar Advisor " + leadassignusername + " From " + orgName + " PTY LTD is requesting "+ documentsTypes.Title +" for Job No. " + job.JobNumber + "." + "\n\nTo Upload documents please click below link \n" + ClickHere + "\n\nRegards\n" + orgName;

            string FinalBody = "<!doctype html><html lang='en'><head> <meta charset='utf-8'> <meta name='viewport' content='width=device-width,initial-scale=1,shrink-to-fit=no'> <meta name='viewport' content='width=device-width,initial-scale=1,shrink-to-fit=no'> <link rel='preconnect' href='https://fonts.googleapis.com'> <link rel='preconnect' href='https://fonts.gstatic.com' crossorigin> <link href='https://fonts.googleapis.com/css2?family=Roboto&display=swap' rel='stylesheet'> <title>Inline CSS Email</title></head><style>@font-face{font-family: Roboto; font-style: normal; font-weight: 400; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu72xKOzY.woff2) format('woff2'); unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F}@font-face{font-family: Roboto; font-style: normal; font-weight: 400; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu5mxKOzY.woff2) format('woff2'); unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116}@font-face{font-family: Roboto; font-style: normal; font-weight: 400; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7mxKOzY.woff2) format('woff2'); unicode-range: U+1F00-1FFF}@font-face{font-family: Roboto; font-style: normal; font-weight: 400; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu4WxKOzY.woff2) format('woff2'); unicode-range: U+0370-03FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 400; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7WxKOzY.woff2) format('woff2'); unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB}@font-face{font-family: Roboto; font-style: normal; font-weight: 400; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7GxKOzY.woff2) format('woff2'); unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 400; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu4mxK.woff2) format('woff2'); unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD}@font-face{font-family: Roboto; font-style: normal; font-weight: 500; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCRc4EsA.woff2) format('woff2'); unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F}@font-face{font-family: Roboto; font-style: normal; font-weight: 500; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fABc4EsA.woff2) format('woff2'); unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116}@font-face{font-family: Roboto; font-style: normal; font-weight: 500; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCBc4EsA.woff2) format('woff2'); unicode-range: U+1F00-1FFF}@font-face{font-family: Roboto; font-style: normal; font-weight: 500; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fBxc4EsA.woff2) format('woff2'); unicode-range: U+0370-03FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 500; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCxc4EsA.woff2) format('woff2'); unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB}@font-face{font-family: Roboto; font-style: normal; font-weight: 500; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fChc4EsA.woff2) format('woff2'); unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 500; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fBBc4.woff2) format('woff2'); unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD}@font-face{font-family: Roboto; font-style: normal; font-weight: 700; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCRc4EsA.woff2) format('woff2'); unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F}@font-face{font-family: Roboto; font-style: normal; font-weight: 700; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfABc4EsA.woff2) format('woff2'); unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116}@font-face{font-family: Roboto; font-style: normal; font-weight: 700; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCBc4EsA.woff2) format('woff2'); unicode-range: U+1F00-1FFF}@font-face{font-family: Roboto; font-style: normal; font-weight: 700; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfBxc4EsA.woff2) format('woff2'); unicode-range: U+0370-03FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 700; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCxc4EsA.woff2) format('woff2'); unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB}@font-face{font-family: Roboto; font-style: normal; font-weight: 700; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfChc4EsA.woff2) format('woff2'); unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 700; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfBBc4.woff2) format('woff2'); unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD}@font-face{font-family: Roboto; font-style: normal; font-weight: 900; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCRc4EsA.woff2) format('woff2'); unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F}@font-face{font-family: Roboto; font-style: normal; font-weight: 900; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfABc4EsA.woff2) format('woff2'); unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116}@font-face{font-family: Roboto; font-style: normal; font-weight: 900; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCBc4EsA.woff2) format('woff2'); unicode-range: U+1F00-1FFF}@font-face{font-family: Roboto; font-style: normal; font-weight: 900; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfBxc4EsA.woff2) format('woff2'); unicode-range: U+0370-03FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 900; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCxc4EsA.woff2) format('woff2'); unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB}@font-face{font-family: Roboto; font-style: normal; font-weight: 900; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfChc4EsA.woff2) format('woff2'); unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 900; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfBBc4.woff2) format('woff2'); unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD}@font-face{font-family: Roboto; font-style: normal; font-weight: 300; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCRc4EsA.woff2) format('woff2'); unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F}@font-face{font-family: Roboto; font-style: normal; font-weight: 300; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fABc4EsA.woff2) format('woff2'); unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116}@font-face{font-family: Roboto; font-style: normal; font-weight: 300; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCBc4EsA.woff2) format('woff2'); unicode-range: U+1F00-1FFF}@font-face{font-family: Roboto; font-style: normal; font-weight: 300; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fBxc4EsA.woff2) format('woff2'); unicode-range: U+0370-03FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 300; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCxc4EsA.woff2) format('woff2'); unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB}@font-face{font-family: Roboto; font-style: normal; font-weight: 300; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fChc4EsA.woff2) format('woff2'); unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 300; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fBBc4.woff2) format('woff2'); unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD}body{margin: 0; padding: 0; background: #E8F0FF; font-family: Roboto}</style><body style='background:#E8F0FF;margin:0;padding:0'> <div style='background:#E8F0FF;margin:0;padding:20px 0'> <div style='margin:10px auto;max-width:600px;min-width:320px'> <div style='margin:0 10px;max-width:600px;min-width:320px;padding:0;background-color:#fff;border:1px solid #e1defa;border-radius:10px;overflow:hidden'> <table style='width:100%;border:0;border-spacing:0;margin:0 auto'> <tr> <td style='text-align:center;padding:30px 0 20px'> <img src='" + OrgLogo + "' style='max-width:180px'> </td></tr><tr> <td style='text-align:center;padding:10px 0'><img src='https://thesolarproduct.com/assets/common/images/contract.png' style='width:35%'></td></tr><tr> <td style='font-size:22px;font-weight:700;font-family:Roboto,sans-serif;color:#000;padding:0 30px;text-align:center;font-weight:900'>Hello " + lead.CompanyName + ",</td></tr><tr> <td style='height:20px'></td></tr><tr> <td style='text-align:center;font-size:14px;font-weight:400;font-family:Roboto,sans-serif;color:#1a1a1a;padding:0 0'> <div style='font-size:.9rem;font-family:Roboto,sans-serif;color:#1a1a1a;margin:0 25px 30px;line-height:28px'>Your Solar Advisor <strong style='border-bottom:1px dotted #000'>" + leadassignusername + "</strong> From <strong style='border-bottom:1px dotted #000'>" + orgName + "</strong> is requesting "+ documentsTypes.Title + " for Job No. <strong style='border-bottom:1px dotted #000'>" + job.JobNumber + "</strong>.</div><div style='padding:20px 0'> <div style='font-size:.9rem;font-weight:400;font-family:Roboto,sans-serif;margin-bottom:30px;line-height:24px'><a href='" + Body + "' style='text-decoration:none;text-decoration:none;background:#6A60D4;font-weight:700;color:#fff;letter-spacing:1px;font-size:16px;text-transform:uppercase;border-radius:25px;height:50px;width:320px;display:inline-block;line-height:50px'>Click Here To Upload Document</a></div><div style='font-size:18px;font-weight:400;font-family:Roboto,sans-serif;color:#000;margin-bottom:20px;line-height:24px;font-weight:700'>OR</div><div style='font-size:14px;font-weight:400;font-family:Roboto,sans-serif;line-height:24px'><a href='" + Body + "' style='text-decoration:none;color:#000;font-weight:900'>" + Body + "</a></div></div><div style='font-size:.9rem;font-weight:400;font-family:Roboto,sans-serif;color:#1a1a1a;margin:20px 25px 25px;line-height:24px'><strong>Note: </strong>This link is valid for <strong>24 hour</strong> from the time it was sent to you and can be used to upload the documents once.</div><div style='background:#6a60d452;font-family:Roboto,sans-serif;font-size:12px;color:#000;line-height:22px;border-top:1px solid #e2ebf1;padding:10px 0'>Thanks,<br>" + orgName + "</td></tr></table> </div></div></div></body></html>";

            if (input.SendMode == "SMS")
            {
                if (!string.IsNullOrEmpty(lead.Mobile))
                {
                    SendSMSInput sendSMSInput = new SendSMSInput();
                    sendSMSInput.ActivityId = leadactivity.Id;
                    sendSMSInput.PhoneNumber = lead.Mobile;
                    sendSMSInput.Text = FinalSMSBody;
                    sendSMSInput.SectionID = input.SectionId;
                    await _applicationSettings.SendSMS(sendSMSInput);
                }
                leadactivity.ActionNote = "Document Request Sent On SMS For " + documentsTypes.Title;
            }
            else if (input.SendMode == "Email")
            {
                var sub = "Document Request.";
                var mail = lead.Email;
                await _emailSender.SendAsync(new MailMessage
                {
                    From = new MailAddress(FromEmail),
                    To = { mail },
                    Subject = sub,
                    Body = FinalBody,
                    IsBodyHtml = true
                });
                leadactivity.ActionNote = "Document Request Sent On Email For " + documentsTypes.Title;
            }
        }

        [UnitOfWork]
        public async Task<DocumentRequestDto> DocumentRequestData(string STR)
        {
            DocumentRequestDto documentRequestDto = new DocumentRequestDto();

            if (STR != null)
            {
                var Ids = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(STR, AppConsts.DefaultPassPhrase));
                
                var Id = Ids.Split(",");
                int TenantId = Convert.ToInt32(Id[0]);
                int JobId = Convert.ToInt32(Id[1]);
                int DocRequestId = Convert.ToInt32(Id[2]);

                using (_unitOfWorkManager.Current.SetTenantId(TenantId))
                {
                    bool IsExpiried = false;
                    bool IsSubmitted = false;

                    var IsExpired = _documentRequestLinkHistory.GetAll().Where(e => e.Token == STR && e.DocumentRequestId == DocRequestId).OrderByDescending(e => e.Id).FirstOrDefault();

                    if (IsExpired != null)
                    {
                        if (IsExpired.CreationTime.AddHours(24) <= DateTime.UtcNow)
                        {
                            IsExpiried = true;
                        }
                        if (IsExpired.Expired == true)
                        {
                            IsSubmitted = true;
                        }

                        if (IsExpiried == false && IsSubmitted == false)
                        {
                            var documentRequest = _documentRequestRepository.GetAll().Include(e => e.DocTypeFk).Where(e => e.Id == DocRequestId).FirstOrDefault();
                            var UserDetail = await _userRepository.FirstOrDefaultAsync(u => u.Id == documentRequest.CreatorUserId);
                            documentRequestDto.Expiry = IsExpiried;
                            documentRequestDto.IsSubmitted = IsSubmitted;
                            documentRequestDto.RequestedBy = UserDetail.FullName.ToString();
                            documentRequestDto.DocumnetType = documentRequest.DocTypeFk.Title;
                            
                            return documentRequestDto;
                        }
                        else
                        {
                            documentRequestDto.Expiry = IsExpiried;
                            documentRequestDto.IsSubmitted = IsSubmitted;
                            return documentRequestDto;
                        }
                    }
                    else
                    {
                        documentRequestDto.Expiry = IsExpiried;
                        documentRequestDto.IsSubmitted = IsSubmitted;
                        return documentRequestDto;
                    }
                }
            }
            else
            {
                documentRequestDto.Expiry = true;
                documentRequestDto.IsSubmitted = false;
                return documentRequestDto;
            }
        }
    }
}
