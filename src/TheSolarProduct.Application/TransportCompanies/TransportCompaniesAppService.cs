﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckApplications;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.TransportCompanies;
using TheSolarProduct.TransportCompanies.Dto;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.TransportationCosts;
using TheSolarProduct.CheckDepositeReceive.Exporting;
using TheSolarProduct.CheckDepositReceived;
using Abp.Application.Services.Dto;
using TheSolarProduct.CheckDepositReceived.Dto;
using Abp.Collections.Extensions;
using System.Linq;

using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using TheSolarProduct.Dto;
using TheSolarProduct.TransportCompanies.Exporting;





namespace TheSolarProduct.TransportCompanies
{
    public class TransportCompaniesAppService : TheSolarProductAppServiceBase, ITransportCompaniesAppService
    {
        private readonly IRepository<TransportCompany> _transportcompanyRepository;

        private readonly ITransportCompaniesExcelExporter _transporycompanyExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;



        public TransportCompaniesAppService(IRepository<TransportCompany> transportCompanyRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,ITransportCompaniesExcelExporter TransportCompaniesExcelExporter )
        {

            _transporycompanyExcelExporter = TransportCompaniesExcelExporter;
            _transportcompanyRepository = transportCompanyRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;

        }

        public async Task<PagedResultDto<GetTransportCompaniesForViewDto>>GetAll(GetAllTransportCompaniesInput input)
        {
            var transportCompanies = _transportcompanyRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.CompanyName == input.Filter);

            var pagedAndFiltered = transportCompanies
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFiltered
                         select new GetTransportCompaniesForViewDto()
                         {
                             transportCompanies = new TransportCompaniesDto
                             {
                                 Id = o.Id,
                                 CompanyName = o.CompanyName,
                                
                             }
                         };

            var totalCount = await transportCompanies.CountAsync();

            return new PagedResultDto<GetTransportCompaniesForViewDto>(totalCount, await output.ToListAsync());
        }



        public async Task CreateOrEdit(CreateOrEditTransportCompaniesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

       

        protected virtual async Task Create(CreateOrEditTransportCompaniesDto input)
        {
            var transportCompanies = ObjectMapper.Map<TransportCompany>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 60;
            dataVaultLog.ActionNote = "Transport company Created: " + input.CompanyName;
            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _transportcompanyRepository.InsertAsync(transportCompanies);
        }

        protected virtual async Task Update(CreateOrEditTransportCompaniesDto input)
        {
            var transportCompanies = await _transportcompanyRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 60;
            dataVaultLog.ActionNote = "Transport Company Updated : " + transportCompanies.CompanyName;
            dataVaultLog.IsInventory = true;
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            var List = new List<DataVaultActivityLogHistory>();
            if (input.CompanyName != transportCompanies.CompanyName)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = transportCompanies.CompanyName;
                history.CurValue = input.CompanyName;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
           

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, transportCompanies);

            await _transportcompanyRepository.UpdateAsync(transportCompanies);
        }


        public async Task<GetTransportCompaniesForEditOutput> GetTransportCompaniesForEdit(EntityDto input)
        {
            var transportCompanies = await _transportcompanyRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetTransportCompaniesForEditOutput { Transportcompanies = ObjectMapper.Map<CreateOrEditTransportCompaniesDto>(transportCompanies) };

            return output;
        }

        public async Task Delete(EntityDto input)
        {
            var Name = _transportcompanyRepository.Get(input.Id).CompanyName;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 60;
            dataVaultLog.ActionNote = "Transport Company Deleted : " + Name;

            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _transportcompanyRepository.DeleteAsync(input.Id);
        }



        public async Task<FileDto> GetTransportCompanyToExcel(GetAllTransportCompaniesForExcelInput input)
        {

            var filteredTransportCompanies = _transportcompanyRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter));

            var query = (from o in filteredTransportCompanies
                         select new GetTransportCompaniesForViewDto()
                         {
                             transportCompanies = new TransportCompaniesDto
                             {
                                CompanyName = o.CompanyName,
                                 Id = o.Id
                             }
                         });


            var TransportCompanyListDtos = await query.ToListAsync();

            return _transporycompanyExcelExporter.ExportToFile(TransportCompanyListDtos);
        }




    }
}
