﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.TransportCompanies.Dto;

namespace TheSolarProduct.TransportCompanies.Exporting
{
    public interface ITransportCompaniesExcelExporter
    {
        FileDto ExportToFile(List<GetTransportCompaniesForViewDto> trasportcompany);

    }
}
