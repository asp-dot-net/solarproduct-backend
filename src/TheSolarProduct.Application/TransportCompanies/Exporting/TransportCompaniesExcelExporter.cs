﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckDepositReceived.Dto;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.TransportCompanies.Dto;

namespace TheSolarProduct.TransportCompanies.Exporting
{
    public class TransportCompaniesExcelExporter : NpoiExcelExporterBase, ITransportCompaniesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public TransportCompaniesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetTransportCompaniesForViewDto> transportcompany)
        {
            return CreateExcelPackage(
                "TransportCompany.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet("Transportcompanies");

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, 2, transportcompany,
                        _ => _.transportCompanies.CompanyName
                        );



                });
        }


    }
}
