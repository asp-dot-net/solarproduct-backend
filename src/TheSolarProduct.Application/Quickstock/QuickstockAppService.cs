﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.Jobs;
using TheSolarProduct.Quickstock.Dto;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace TheSolarProduct.Quickstock
{
    [AbpAuthorize]
    public class QuickstockAppService : TheSolarProductAppServiceBase, IQuickstockAppService
    {
        private readonly IRepository<Warehouselocation> _warehouseLocationRepository;

        public QuickstockAppService(IRepository<Warehouselocation> warehouseLocationRepository)
        {
            _warehouseLocationRepository = warehouseLocationRepository;
        }

        public StockDetails GetStockDetails(int StockItemId, int Location)
        {
            //String SqlconString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=sa;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
            String SqlconString = ApplicationSettingConsts.QuickStockSqlconString;
            using (SqlConnection sqlCon = new SqlConnection(SqlconString))
            {
                sqlCon.Open();
                SqlCommand sql_cmnd2 = new SqlCommand("SP_GetStockDetailsByStockItemIdLocationId", sqlCon);
                sql_cmnd2.CommandType = CommandType.StoredProcedure;
                sql_cmnd2.Parameters.AddWithValue("@StockItemId", SqlDbType.Int).Value = StockItemId;
                sql_cmnd2.Parameters.AddWithValue("@LocationId", SqlDbType.Int).Value = Location;
                var table = new DataTable();
                table.Load(sql_cmnd2.ExecuteReader());
                var query = (from pickItem in table.AsEnumerable()
                             select new StockDetails
                             {
                                 StockOnHand = pickItem.Field<int>("StockOnHand")
                             });

                sqlCon.Close();
                return query.FirstOrDefault();
            }
        }

        public StockDetails GetStockDetailByState(int StockItemId, string State)
        {
            var Location = _warehouseLocationRepository.GetAll().Where(e => e.state == State).FirstOrDefault();
            //String SqlconString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=sa;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
            String SqlconString = ApplicationSettingConsts.QuickStockSqlconString;
            using (SqlConnection sqlCon = new SqlConnection(SqlconString))
            {
                sqlCon.Open();
                SqlCommand sql_cmnd2 = new SqlCommand("SP_GetStockDetailsByStockItemIdLocationIdForProduct", sqlCon);
                sql_cmnd2.CommandType = CommandType.StoredProcedure;
                sql_cmnd2.Parameters.AddWithValue("@StockItemId", SqlDbType.Int).Value = StockItemId;
                sql_cmnd2.Parameters.AddWithValue("@LocationId", SqlDbType.Int).Value = Location != null ? Location.Id : 0;
                var table = new DataTable();
                table.Load(sql_cmnd2.ExecuteReader());
                var query = (from pickItem in table.AsEnumerable()
                             select new StockDetails
                             {
                                 //StockOnHand = pickItem.Field<int>("StockOnHand")
                                 StockOnHand = pickItem.Field<int>("NetSituation")
                             });

                sqlCon.Close();
                return query.FirstOrDefault();
            }
        }

        public async Task<bool> GetCheckActualAudit(int InstallerId)
        {
            int CompnyId = 0;
            if (AbpSession.TenantId == 6)
            {
                CompnyId = 1;
            }
            else if (AbpSession.TenantId == 11)
            {
                CompnyId = 2;
            }

            //String SqlconString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=sa;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
            String SqlconString = ApplicationSettingConsts.QuickStockSqlconString;
            using (SqlConnection sqlCon = new SqlConnection(SqlconString))
            {
                sqlCon.Open();
                SqlCommand sql_cmnd2 = new SqlCommand("GetCheckActualAudit", sqlCon);
                sql_cmnd2.CommandType = CommandType.StoredProcedure;
                sql_cmnd2.Parameters.AddWithValue("@CompanyID", SqlDbType.Int).Value = CompnyId;
                sql_cmnd2.Parameters.AddWithValue("@InstallerId", SqlDbType.Int).Value = InstallerId;
                var table = new DataTable();
                table.Load(sql_cmnd2.ExecuteReader());
                var actualAudit = (from pickItem in table.AsEnumerable()
                                   select pickItem.Field<int>("ActualAudit")
                                  ).FirstOrDefault();

                sqlCon.Close();
                
                await Task.CompletedTask;

                return actualAudit < 300 ? true : false;
                
            }
        }

    }
}
