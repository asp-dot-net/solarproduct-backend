﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Organizations;
using Abp.Runtime.Session;
using Abp.UI;
using Api2Pdf;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using NUglify.Helpers;
//using PdfiumViewer;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Common.Dto;
using TheSolarProduct.Editions;
using TheSolarProduct.Editions.Dto;
using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.LeadSources;
using TheSolarProduct.LeadStatuses;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.Organizations.Dto;
using TheSolarProduct.PostCodes;
using TheSolarProduct.PriceItemLists;
using TheSolarProduct.Quotations;
using TheSolarProduct.Quotations.Dtos;
using TheSolarProduct.ReviewTypes;
using TheSolarProduct.ServiceCategorys;
using TheSolarProduct.ServicePrioritys;
using TheSolarProduct.ServiceSources;
using TheSolarProduct.ServiceStatuses;
using TheSolarProduct.ServiceSubCategorys;
using TheSolarProduct.States;
using TheSolarProduct.Storage;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.UserWiseEmailOrgs;
using TheSolarProduct.EmailSetting;
using TheSolarProduct.Organizations;
using Microsoft.AspNetCore.Identity;
using NPOI.SS.Formula.Functions;
using TheSolarProduct.Authorization;
using TheSolarProduct.InstallationCost.OtherCharges;
using TheSolarProduct.CallHistory.Dtos;
using TheSolarProduct.CallHistory;
using TheSolarProduct.WholeSaleStatuses;
using TheSolarProduct.WholeSaleLeads;
using NUglify.JavaScript.Syntax;
using TheSolarProduct.VppConections;
using TheSolarProduct.StreetNames;
using TheSolarProduct.StreetTypes;
using Hangfire.Storage;
using static NPOI.POIFS.Crypt.Dsig.SignatureInfo;
using TheSolarProduct.Wholesales.Ecommerces;
using Abp;
using TheSolarProduct.TransportationCosts;
using TheSolarProduct.PurchaseCompanies;
using TheSolarProduct.DeliveryTypes;
using TheSolarProduct.PaymentTypes;
using TheSolarProduct.PaymentMethods;
using TheSolarProduct.Currencies;
using TheSolarProduct.StockOrderFors;
using TheSolarProduct.StockFroms;
using TheSolarProduct.PaymentStatuses;
using TheSolarProduct.Vendors;
using TheSolarProduct.IncoTerms;
using TheSolarProduct.BonusList;
using TheSolarProduct.WholesaleDeliveryOptions;
using TheSolarProduct.PurchaseDocumentListes;
using TheSolarProduct.StockOrders;
using System.Linq.Dynamic.Core;
using TheSolarProduct.Timing;
using Abp.Timing.Timezone;
using TheSolarProduct.Expense;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using TheSolarProduct.CheckDepositReceived;
using TheSolarProduct.CheckActives;
using TheSolarProduct.CheckApplications;
using TheSolarProduct.StockOrders.SerialNoStatuses;
using TheSolarProduct.VoucherMasters;

namespace TheSolarProduct.Common
{
    public class CommonLookupAppService : TheSolarProductAppServiceBase, ICommonLookupAppService
    {
        private const int MaxFileBytes = 5242880; //5MB
        private readonly EditionManager _editionManager;
        private readonly IRepository<UserWiseEmailOrg> _UserWiseEmailOrgsRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<JobType, int> _lookup_jobTypeRepository;
        private readonly IRepository<LeadSource, int> _lookup_leadSourceRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<State, int> _lookup_stateRepository;
        private readonly IRepository<JobStatus, int> _lookup_jobStatusRepository;
        private readonly IRepository<PaymentOption, int> _lookup_paymentOptionRepository;
        private readonly IRepository<FinanceOption, int> _lookup_financeOptionRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<ReviewType> _reviewtypeRepository;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<LeadStatus, int> _lookup_leadStatusRepository;
        private readonly IRepository<PostCode, int> _lookup_postCodeRepository;
        private readonly IRepository<ServiceCategory> _lookup_serviceCategoryRepository;
        private readonly IRepository<ServiceSubCategory> _lookup_serviceSubCategoryRepository;
        private readonly IRepository<ServicePriority> _lookup_servicePriorityRepository;
        private readonly IRepository<ServiceSource> _lookup_serviceSourceRepository;
        private readonly IRepository<ServiceStatus> _lookup_serviceStatusRepository;
        private readonly IRepository<Variation, int> _lookup_variationRepository;
        private readonly IRepository<ProductItem, int> _lookup_productItemRepository;
        private readonly IRepository<ProductType, int> _lookup_productTypeRepository;
        private readonly IRepository<PriceItemList> _PriceItemListRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Job, int> _jobRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<LeadSourceOrganizationUnit> _leadSourceOrganizationUnitRepository;
        private readonly IRepository<TemplateType> _templateTypeRepository;
        private readonly IRepository<EmailProviders> _emailProvidersRepository;
        private readonly IRepository<PurchaseDocumentList> _purchaseDocumentRepository;
        private readonly IRepository<Quotations.Document> _documentRepository;
        private readonly IRepository<OrganizationUnitMap> _organizationUnitMapRepository;
        private readonly RoleManager _roleManager;
        private readonly PermissionChecker _permissionChecker;
        private readonly IRepository<HouseType> _houseTypeRepository;
        private readonly IRepository<RoofAngle> _roofAngleRepository;
        private readonly IRepository<OtherCharge> _otherChargeRepository;
        private readonly IRepository<RoofType> _roofTypeRepository;
        private readonly IRepository<Bonus> _bonusRepository;
        private readonly IRepository<EnfornicaCallDetails> _enfornicaCallDetailsRepository;
        private readonly IRepository<WholeSaleStatus> _wholeSaleStatusRepository;
        private readonly IRepository<IncoTerm> _IncoTermRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<WholeSaleLead> _wholeSaleLeadRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendOrganizationUnitRepository;
        private readonly IRepository<VppConection> _vppConectionRepository;
        private readonly IRepository<UnitTypes.UnitType, int> _unitTypeRepository;
        private readonly IRepository<StreetName, int> _streetNameRepository;
        private readonly IRepository<StreetType, int> _streetTypeRepository;
        private readonly IRepository<EcomerceSpecialStatus> _ecomerceSpecialStatusRepository;
        private readonly IRepository<ECommerceDocumentType> _ecomerceDocumentTypeRepository;
        private readonly IRepository<ECommercePriceCategory> _ecomercePriceCategoryRepository;
        private readonly IRepository<BrandingPartner> _brandingPartnerRepository;
        private readonly IRepository<Wholesales.Ecommerces.Series> _seriesRepository;
        private readonly IRepository<CustomerType> _customerTypeRepository;
        private readonly IRepository<TradingSTCWhom> _tradingSTCWhomRepository;
        private readonly IRepository<InstallerInvoiceType> _installerInvoiceTypeRepository;
        private readonly IRepository<TransportCompany> _transportCompanyRepository;
        private readonly IRepository<PurchaseCompany, int> _purchaseCompanyRepository;
        private readonly IRepository<StockOrderStatuses.StockOrderStatus> _stockOrderStatusRepository;
        private readonly IRepository<DeliveryType> _deliverytypesRepository;
        private readonly IRepository<PaymentType> _paymentTypeRepository;
        private readonly IRepository<PaymentMethod> _paymentMethodRepository;
        private readonly IRepository<Currency> _currencyRepository;
        private readonly IRepository<Warehouselocation> _warehouseLocationRepository;
        private readonly IRepository<StockOrderFor> _stockorderforRepository;
        private readonly IRepository<StockFrom> _stockfromsRepository;
        private readonly IRepository<PaymentStatus> _paymentStatusRepository;
        private readonly IRepository<Vendor> _vendorRepository;
        private readonly IRepository<STCProvider> _stcProviderRepository;
        private readonly IRepository<EcommerceProductType> _ecommerceProductTypeRepository;
        private readonly IRepository<EcommerceSpecification> _ecommerceSpecificationRepository;
        private readonly IRepository<WholesaleInvoicetype> _wholesaleInvoicetypeRepository;
        private readonly IRepository<WholesaleJobType> _wholesaleJobTypeRepository;
        private readonly IRepository<Wholesales.Ecommerces.WholesaleDeliveryOption> _wholesaleDeliveryOptionRepository;
        private readonly IRepository<WholesaleJobStatus> _wholesaleJobStatusRepository;
        private readonly IRepository<WholesaleTransportType> _wholesaleTransportTypeRepository;
        private readonly IRepository<WholesalePropertyType> _wholesalePropertyTypeRepository;
        private readonly IRepository<WholesalePVDStatus> _wholesalePVDStatusRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly IRepository<EcommerceProductItem> _ecommerceProductItemRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<JobCostFixExpenseType> _jobCostFixExpenseTypeRepository;
        private readonly IRepository<EcommerceProductItemPriceCategory> _ecommerceProductItemPriceCategoryRepository;
        private readonly IRepository<StockVariation> _stockVariationRepository;
        private readonly IRepository<Checkdeposite> _checkdepositeRepository;
        private readonly IRepository<CheckActive> _checkActiveRepository;
        private readonly IRepository<CheckApplication> _checkApplicationRepository;
        private readonly IRepository<SerialNoStatus> _serialNoStatusRepository;
        private readonly IRepository<VoucherMaster> _voucherRepository;
        public CommonLookupAppService(EditionManager editionManager, IRepository<LeadSource, int> lookup_leadSourceRepository,
            IRepository<UserWiseEmailOrg> UserWiseEmailOrgsRepository, IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRepository<JobType, int> lookup_jobTypeRepository, IUnitOfWorkManager unitOfWorkManager, IRepository<State, int> lookup_stateRepository,
            IRepository<JobStatus, int> lookup_jobStatusRepository
            , IRepository<PaymentOption, int> lookup_paymentOptionRepository
            , IRepository<FinanceOption, int> lookup_financeOptionRepository
            , IRepository<User, long> userRepository
            , IRepository<UserTeam> userTeamRepository
            , UserManager userManager
            , IRepository<Team> teamRepository
            , IRepository<UserRole, long> userroleRepository
            , IRepository<Role> roleRepository
            , IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository
            , IRepository<LeadStatus, int> lookup_leadStatusRepository
            , IRepository<PostCode, int> lookup_postCodeRepository,
            IRepository<ServiceCategory> lookup_serviceCategoryRepository,
            IRepository<ServicePriority> lookup_servicePriorityRepository,
            IRepository<ServiceSource> lookup_serviceSourceRepository,
            IRepository<ServiceStatus> lookup_serviceStatusRepository,
            IRepository<Variation, int> lookup_variationRepository,
            IRepository<ProductItem, int> lookup_productItemRepository,
            IRepository<ProductType, int> lookup_productTypeRepository,
            IRepository<ReviewType> reviewtypeRepository,
            IRepository<ServiceSubCategory> lookup_serviceSubCategoryRepository,
            IRepository<PriceItemList> PriceItemListRepository,
            ITempFileCacheManager tempFileCacheManager,
            IWebHostEnvironment env,
            IRepository<Job, int> jobRepository,
            IRepository<Tenant> tenantRepository,
            IRepository<LeadSourceOrganizationUnit> leadSourceOrganizationUnitRepository,
            IRepository<Quotations.Document> documentRepository,
            IRepository<TemplateType> templateTypeRepository,
            IRepository<EmailProviders> emailProvidersRepository,
            IRepository<OrganizationUnitMap> organizationUnitMapRepository,
            RoleManager roleManager,
            PermissionChecker permissionChecker,
            IRepository<HouseType> houseTypeRepository,
            IRepository<RoofAngle> roofAngleRepository,
            IRepository<OtherCharge> otherChargeRepository
            , IRepository<RoofType> roofTypeRepository
            , IRepository<EnfornicaCallDetails> enfornicaCallDetailsRepository
            , IRepository<WholeSaleStatus> wholeSaleStatusRepository
            , IRepository<UserRole, long> userRoleRepository
            , IRepository<WholeSaleLead> wholeSaleLeadRepository
            , IRepository<ExtendOrganizationUnit, long> extendOrganizationUnitRepository
            , IRepository<VppConection> vppConectionRepository
            , IRepository<UnitTypes.UnitType, int> unitTypeRepository
            , IRepository<StreetName, int> streetNameRepository
            , IRepository<StreetType, int> streetTypeRepository
            , IRepository<EcomerceSpecialStatus> ecomerceSpecialStatusRepository
            , IRepository<ECommerceDocumentType> ecomerceDocumentTypeRepository
            , IRepository<ECommercePriceCategory> ecomercePriceCategoryRepository
            , IRepository<BrandingPartner> brandingPartnerRepository
            , IRepository<Wholesales.Ecommerces.Series> seriesRepository
            , IRepository<CustomerType> customerTypeRepository
            , IRepository<TradingSTCWhom> tradingSTCWhomRepository
            , IRepository<InstallerInvoiceType> installerInvoiceTypeRepository
            , IRepository<TransportCompany> transportCompanyRepository,
            IRepository<StockOrderStatuses.StockOrderStatus> stockOrderStatusRepository,
            IRepository<PurchaseCompany, int> purchaseCompanyRepository,
            IRepository<DeliveryType> deliverytypesRepository,
            IRepository<PaymentType> paymentTypeRepository,
             IRepository<PaymentMethod> paymentMethodRepository,
             IRepository<Currency> currencyRepository,
             IRepository<Warehouselocation> warehouseLocationRepository,
             IRepository<StockOrderFor> stockorderforRepository,
             IRepository<StockFrom> stockfromsRepository,
              IRepository<PaymentStatus> paymentStatusRepository,
              IRepository<Vendor> vendorRepository,
              IRepository<STCProvider> stcProviderRepository,
              IRepository<IncoTerm> incoTermRepositoryRepository,
              IRepository<Bonus> bonusRepository
            , IRepository<EcommerceProductType> ecommerceProductTypeRepository
            , IRepository<EcommerceSpecification> ecommerceSpecificationRepository
            , IRepository<WholesaleInvoicetype> wholesaleInvoicetypeRepository
            , IRepository<WholesaleJobType> wholesaleJobTypeRepository
            , IRepository<Wholesales.Ecommerces.WholesaleDeliveryOption> wholesaleDeliveryOptionRepository
            , IRepository<WholesaleJobStatus> wholesaleJobStatusRepository
            , IRepository<WholesaleTransportType> wholesaleTransportTypeRepository
            , IRepository<WholesalePropertyType> wholesalePropertyTypeRepository
            , IRepository<WholesalePVDStatus> wholesalePVDStatusRepository
            , IRepository<PurchaseDocumentList> purchaseDocumentRepository
            , ITimeZoneService timeZoneService
            , IRepository<EcommerceProductItem> ecommerceProductItemRepository
            , ITimeZoneConverter timeZoneConverter
            , IRepository<JobCostFixExpenseType> jobCostFixExpenseTypeRepository
            , IRepository<EcommerceProductItemPriceCategory> ecommerceProductItemPriceCategoryRepository
            , IRepository<StockVariation> stockVariationRepository
            , IRepository<Checkdeposite> checkdepositeRepository
            , IRepository<CheckActive> checkActiveRepository
            , IRepository<CheckApplication> checkApplicationRepository
            , IRepository<SerialNoStatus> serialNoStatusRepository
            , IRepository<VoucherMaster> voucherRepository
            )
        {
            _editionManager = editionManager;
            _UserWiseEmailOrgsRepository = UserWiseEmailOrgsRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _lookup_jobTypeRepository = lookup_jobTypeRepository;
            _lookup_leadSourceRepository = lookup_leadSourceRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _lookup_stateRepository = lookup_stateRepository;
            _lookup_jobStatusRepository = lookup_jobStatusRepository;
            _lookup_paymentOptionRepository = lookup_paymentOptionRepository;
            _lookup_financeOptionRepository = lookup_financeOptionRepository;
            _userRepository = userRepository;
            _userTeamRepository = userTeamRepository;
            _userManager = userManager;
            _teamRepository = teamRepository;
            _userroleRepository = userroleRepository;
            _roleRepository = roleRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _lookup_leadStatusRepository = lookup_leadStatusRepository;
            _lookup_postCodeRepository = lookup_postCodeRepository;
            _lookup_serviceCategoryRepository = lookup_serviceCategoryRepository;
            _lookup_servicePriorityRepository = lookup_servicePriorityRepository;
            _lookup_serviceSourceRepository = lookup_serviceSourceRepository;
            _lookup_serviceStatusRepository = lookup_serviceStatusRepository;
            _lookup_variationRepository = lookup_variationRepository;
            _lookup_productItemRepository = lookup_productItemRepository;
            _lookup_productTypeRepository = lookup_productTypeRepository;
            _reviewtypeRepository = reviewtypeRepository;
            _lookup_serviceSubCategoryRepository = lookup_serviceSubCategoryRepository;
            _PriceItemListRepository = PriceItemListRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _env = env;
            _jobRepository = jobRepository;
            _tenantRepository = tenantRepository;
            _leadSourceOrganizationUnitRepository = leadSourceOrganizationUnitRepository;
            _documentRepository = documentRepository;
            _templateTypeRepository = templateTypeRepository;
            _emailProvidersRepository = emailProvidersRepository;
            _organizationUnitMapRepository = organizationUnitMapRepository;
            _roleManager = roleManager;
            _permissionChecker = permissionChecker;
            _houseTypeRepository = houseTypeRepository;
            _roofAngleRepository = roofAngleRepository;
            _otherChargeRepository = otherChargeRepository;
            _roofTypeRepository = roofTypeRepository;
            _enfornicaCallDetailsRepository = enfornicaCallDetailsRepository;
            _wholeSaleStatusRepository = wholeSaleStatusRepository;
            _userroleRepository = userroleRepository;
            _wholeSaleLeadRepository = wholeSaleLeadRepository;
            _extendOrganizationUnitRepository = extendOrganizationUnitRepository;
            _vppConectionRepository = vppConectionRepository;
            _unitTypeRepository = unitTypeRepository;
            _streetNameRepository = streetNameRepository;
            _streetTypeRepository = streetTypeRepository;
            _ecomerceSpecialStatusRepository = ecomerceSpecialStatusRepository;
            _ecomerceDocumentTypeRepository = ecomerceDocumentTypeRepository;
            _ecomercePriceCategoryRepository = ecomercePriceCategoryRepository;
            _brandingPartnerRepository = brandingPartnerRepository;
            _seriesRepository = seriesRepository;
            _customerTypeRepository = customerTypeRepository;
            _tradingSTCWhomRepository = tradingSTCWhomRepository;
            _installerInvoiceTypeRepository = installerInvoiceTypeRepository;
            _transportCompanyRepository = transportCompanyRepository;
            _stockOrderStatusRepository = stockOrderStatusRepository;
            _purchaseCompanyRepository = purchaseCompanyRepository;
            _deliverytypesRepository = deliverytypesRepository;
            _paymentTypeRepository = paymentTypeRepository;
            _paymentMethodRepository = paymentMethodRepository;
            _currencyRepository = currencyRepository;
            _warehouseLocationRepository = warehouseLocationRepository;
            _stockorderforRepository = stockorderforRepository;
            _stockfromsRepository = stockfromsRepository;
            _paymentStatusRepository = paymentStatusRepository;
            _vendorRepository = vendorRepository;
            _stcProviderRepository = stcProviderRepository;
            _IncoTermRepository = incoTermRepositoryRepository;
            _bonusRepository = bonusRepository;
            _ecommerceProductTypeRepository = ecommerceProductTypeRepository;
            _ecommerceSpecificationRepository = ecommerceSpecificationRepository;
            _wholesaleInvoicetypeRepository = wholesaleInvoicetypeRepository;
            _wholesaleJobTypeRepository = wholesaleJobTypeRepository;
            _wholesaleDeliveryOptionRepository = wholesaleDeliveryOptionRepository;
            _wholesaleJobStatusRepository = wholesaleJobStatusRepository;
            _wholesaleTransportTypeRepository = wholesaleTransportTypeRepository;
            _wholesalePropertyTypeRepository = wholesalePropertyTypeRepository;
            _wholesalePVDStatusRepository = wholesalePVDStatusRepository;
            _purchaseDocumentRepository = purchaseDocumentRepository;
            _timeZoneService = timeZoneService;
            _ecommerceProductItemRepository = ecommerceProductItemRepository;
            _timeZoneConverter = timeZoneConverter;
            _jobCostFixExpenseTypeRepository = jobCostFixExpenseTypeRepository;
            _ecommerceProductItemPriceCategoryRepository = ecommerceProductItemPriceCategoryRepository;
            _stockVariationRepository = stockVariationRepository;
            _checkdepositeRepository = checkdepositeRepository;
            _checkActiveRepository= checkActiveRepository;  
            _checkApplicationRepository = checkApplicationRepository;
            _serialNoStatusRepository = serialNoStatusRepository;
            _voucherRepository=voucherRepository;

        }

        public async Task<ListResultDto<SubscribableEditionComboboxItemDto>> GetEditionsForCombobox(bool onlyFreeItems = false)
        {
            var subscribableEditions = (await _editionManager.Editions.Cast<SubscribableEdition>().ToListAsync())
                .WhereIf(onlyFreeItems, e => e.IsFree)
                .OrderBy(e => e.MonthlyPrice);

            return new ListResultDto<SubscribableEditionComboboxItemDto>(
                subscribableEditions.Select(e => new SubscribableEditionComboboxItemDto(e.Id.ToString(), e.DisplayName, e.IsFree)).ToList()
            );
        }

        public async Task<PagedResultDto<NameValueDto>> FindUsers(FindUsersInput input)
        {
            if (AbpSession.TenantId != null)
            {
                //Prevent tenants to get other tenant's users.
                input.TenantId = AbpSession.TenantId;
            }

            using (CurrentUnitOfWork.SetTenantId(input.TenantId))
            {
                var query = UserManager.Users
                    .WhereIf(
                        !string.IsNullOrEmpty(input.Filter),
                        u =>
                            u.Name.Contains(input.Filter) ||
                            u.Surname.Contains(input.Filter) ||
                            u.UserName.Contains(input.Filter) ||
                            u.EmailAddress.Contains(input.Filter)
                    ).WhereIf(input.ExcludeCurrentUser, u => u.Id != AbpSession.GetUserId());

                var userCount = await query.CountAsync();
                var users = await query
                    .OrderBy(u => u.Name)
                    .ThenBy(u => u.Surname)
                    .PageBy(input)
                    .ToListAsync();

                return new PagedResultDto<NameValueDto>(
                    userCount,
                    users.Select(u =>
                        new NameValueDto(
                            u.FullName + " (" + u.EmailAddress + ")",
                            u.Id.ToString()
                            )
                        ).ToList()
                    );
            }
        }

        public GetDefaultEditionNameOutput GetDefaultEditionName()
        {
            return new GetDefaultEditionNameOutput
            {
                Name = EditionManager.DefaultEditionName
            };
        }

        public async Task<List<OrganizationUnitDto>> GetOrganizationUnit()
        {
            int UserId = (int)AbpSession.UserId;

            var OrganizationList = new List<OrganizationUnitDto>();

            OrganizationList = (from or in _UserWiseEmailOrgsRepository.GetAll()
                                join uo in _extendOrganizationUnitRepository.GetAll() on or.OrganizationUnitId equals uo.Id into uoJoined
                                from uo in uoJoined.DefaultIfEmpty()
                                where (or.UserId == UserId)
                                orderby uo.IsDefault descending
                                select new OrganizationUnitDto
                                {
                                    Id = uo.Id,
                                    Code = uo.Code,
                                    DisplayName = uo.DisplayName,
                                    ParentId = uo.ParentId,
                                    EnableAutoAssignLead = uo.EnableAutoAssignLead,
                                }).ToList();


            return OrganizationList;
        }
        public async Task<List<JobJobTypeLookupTableDto>> GetAllJobTypeForTableDropdown()
        {
            return await _lookup_jobTypeRepository.GetAll()
                .Select(jobType => new JobJobTypeLookupTableDto
                {
                    Id = jobType.Id,
                    DisplayName = jobType == null || jobType.Name == null ? "" : jobType.Name.ToString()
                }).AsNoTracking().ToListAsync();
        }
        public async Task<List<LeadSourceLookupTableDto>> GetAllLeadSourceDropdown()
        {
            return await _lookup_leadSourceRepository.GetAll()
                        .Select(leadSource => new LeadSourceLookupTableDto
                        {
                            Id = leadSource.Id,
                            DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
                        }).ToListAsync();
        }
        public async Task<List<LeadStateLookupTableDto>> GetAllStateForTableDropdown()
        {
            return await _lookup_stateRepository.GetAll()
                .Select(state => new LeadStateLookupTableDto
                {
                    Id = state.Id,
                    DisplayName = state == null || state.Name == null ? "" : state.Name.ToString().ToUpper()
                }).OrderBy(e => e.Id).ToListAsync();
        }

        public async Task<List<JobStatusTableDto>> GetAllJobStatusTableDropdown()
        {
            return await _lookup_jobStatusRepository.GetAll()
            .Select(jobstaus => new JobStatusTableDto
            {
                Id = jobstaus.Id,
                DisplayName = jobstaus == null || jobstaus.Name == null ? "" : jobstaus.Name.ToString()
            }).ToListAsync();
        }
        public async Task<List<JobPaymentOptionLookupTableDto>> GetAllPaymentOptionForTableDropdown()
        {
            return await _lookup_paymentOptionRepository.GetAll()
                .Select(paymentOption => new JobPaymentOptionLookupTableDto
                {
                    Id = paymentOption.Id,
                    DisplayName = paymentOption == null || paymentOption.Name == null ? "" : paymentOption.Name.ToString()
                }).ToListAsync();
        }
        public async Task<List<JobFinanceOptionLookupTableDto>> GetAllFinanceOptionForTableDropdown()
        {
            return await _lookup_financeOptionRepository.GetAll()
                .Select(financeOption => new JobFinanceOptionLookupTableDto
                {
                    Id = financeOption.Id,
                    DisplayName = financeOption == null || financeOption.Name == null ? "" : financeOption.Name.ToString()
                }).ToListAsync();
        }

        /// login wise team display
        public async Task<List<LeadUsersLookupTableDto>> GetTeamForFilter()
        {
            var User = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            //var Team = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).FirstOrDefault();
            var Team = await _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);

            var TeamList = new List<LeadUsersLookupTableDto>();
            if (role.Contains("Admin") || role.Contains("Installation Manager"))
            {
                TeamList = (from user in _teamRepository.GetAll()
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.Name == null || user.Name == null ? "" : user.Name.ToString()
                            }).DistinctBy(d => d.Id).ToList();
            }
            else
            {
                TeamList = (from team in _teamRepository.GetAll()
                                //where (team.Id == Team)
                            where (Team.Contains(team.Id))
                            select new LeadUsersLookupTableDto()
                            {
                                Id = team.Id,
                                DisplayName = team.Name == null || team.Name == null ? "" : team.Name.ToString()
                            }).DistinctBy(d => d.Id).ToList();
            }
            return TeamList;
        }

        public async Task<List<LeadUsersLookupTableDto>> GetSalesRepForFilter(int OId, int? TeamId)
        {
            var User_List = _userRepository
             .GetAll();

            var User = User_List.Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var Team = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId);
            IList<string> role = await _userManager.GetRolesAsync(User);


            var UserList = new List<LeadUsersLookupTableDto>();
            if (role.Contains("Admin") || role.Contains("Installation Manager"))
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Sales Rep" && uo.OrganizationUnitId == OId) //&& ut.TeamId == TeamId
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).OrderBy(d => d.DisplayName).ToList();
            }
            else
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Sales Rep" && uo != null && uo.OrganizationUnitId == OId && Team.Contains(ut.TeamId)) // 
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).OrderBy(d => d.DisplayName).ToList();
            }


            return UserList;
        }

        public async Task<List<LeadSourceLookupTableDto>> GetAllLeadSourceForTableDropdown()
        {
            return await _lookup_leadSourceRepository.GetAll()
                        .Select(leadSource => new LeadSourceLookupTableDto
                        {
                            Id = leadSource.Id,
                            DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
                        }).ToListAsync();
        }

        public async Task<List<LeadStatusLookupTableDto>> GetAllLeadStatusForTableDropdown(string lead)
        {
            return await _lookup_leadStatusRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(lead), c => c.Status.ToLower().Contains(lead.ToLower()))
                .Select(leadStatus => new LeadStatusLookupTableDto
                {
                    Value = leadStatus.Id,
                    Name = leadStatus == null || leadStatus.Status == null ? "" : leadStatus.Status.ToString()
                }).ToListAsync();
        }

        [UnitOfWork]
        public async Task<List<string>> GetOnlySuburb(string suburb)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var result = _lookup_postCodeRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(suburb), c => c.Suburb.ToLower().Contains(suburb.ToLower()));

                var suburbs = from o in result
                              select new LeadStatusLookupTableDto()
                              {
                                  Value = o.Id,
                                  Name = o.Suburb.ToString()
                              };

                return await suburbs
                    .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
            }
        }

        public async Task<List<LeadUsersLookupTableDto>> GetSalesManagerForFilter(int OId, int? TeamId)
        {

            var User_List = _userRepository.GetAll();
            var User = User_List.Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var Team = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            //New Added
            var TeamList = await _teamRepository.GetAll().WhereIf(TeamId != 0, e => e.Id == TeamId).Select(e => (int?)e.Id).ToListAsync();

            var UserList = new List<LeadUsersLookupTableDto>();
            if (role.Contains("Admin"))
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Sales Manager" && uo.OrganizationUnitId == OId && TeamList.Contains(ut.TeamId))
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).ToList();
            }
            else
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Sales Manager" && uo != null && uo.OrganizationUnitId == OId && ut.TeamId == Team) // 
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).ToList();
            }


            return UserList;
        }


        public async Task<List<CommonLookupDto>> GetServieCategoryDropdown()
        {
            var servieCategorylist = (from item in _lookup_serviceCategoryRepository.GetAll()
                                      select new CommonLookupDto
                                      {
                                          Id = item.Id,
                                          DisplayName = item.Name,
                                      }).ToList();
            return servieCategorylist;
        }
        public async Task<List<CommonLookupDto>> GetServiePriorityDropdown()
        {
            var serviePrioritylist = (from item in _lookup_servicePriorityRepository.GetAll()
                                      select new CommonLookupDto
                                      {
                                          Id = item.Id,
                                          DisplayName = item.Name,
                                      }).ToList();
            return serviePrioritylist;
        }
        public async Task<List<CommonLookupDto>> GetServieSourceDropdown()
        {
            var servieSourcelist = (from item in _lookup_serviceSourceRepository.GetAll()
                                    select new CommonLookupDto
                                    {
                                        Id = item.Id,
                                        DisplayName = item.Name,
                                    }).ToList();
            return servieSourcelist;
        }
        public async Task<List<CommonLookupDto>> GetServieStatusDropdown()
        {
            var servieStatuslist = (from item in _lookup_serviceStatusRepository.GetAll()
                                    select new CommonLookupDto
                                    {
                                        Id = item.Id,
                                        DisplayName = item.Name,
                                    }).ToList();
            return servieStatuslist;
        }


        public async Task<List<CommonLookupDto>> GetPriceItemListDropdown()
        {
            var servieStatuslist = (from item in _PriceItemListRepository.GetAll()
                                    select new CommonLookupDto
                                    {
                                        Id = item.Id,
                                        DisplayName = item.Name,
                                    }).ToList();
            return servieStatuslist;
        }
        public async Task<List<LeadUsersLookupTableDto>> GetServiceUserForFilter(int OId)
        {

            var User_List = _userRepository
          .GetAll();
            var User = User_List.Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            var UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Service" && uo.OrganizationUnitId == OId) // 
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).ToList();
            return UserList;
        }

        public async Task<List<LeadUsersLookupTableDto>> GetServiceReviewUserForFilter(int OId)
        {

            var User_List = _userRepository
          .GetAll();
            var User = User_List.Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            var UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Review" && uo.OrganizationUnitId == OId) // 
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).ToList();
            return UserList;
        }

        public async Task<List<LeadUsersLookupTableDto>> GetServiceManagerUserForFilter(int OId)
        {

            var User_List = _userRepository
          .GetAll();
            var User = User_List.Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            var UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Service Manager" && uo.OrganizationUnitId == OId) // 
                            select new LeadUsersLookupTableDto()
                            {
                                Id = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).DistinctBy(d => d.Id).ToList();
            return UserList;
        }

        public async Task<List<JobVariationVariationLookupTableDto>> GetAllVariationForTableDropdown()
        {
            var _per = _permissionChecker.IsGranted(AppPermissions.Pages_Jobs_Variation);

            var variation = _lookup_variationRepository.GetAll()
                            .WhereIf(!_per, e => e.Name != "Manager Discount")
                            .WhereIf(!_per, e => e.Name != "Category D Discount");

            var ret = (from o in variation
                       select new JobVariationVariationLookupTableDto
                       {
                           Id = o.Id,
                           DisplayName = o == null || o.Name == null ? "" : o.Name.ToString(),
                           ActionName = o == null || o.Action == null ? "" : o.Action.ToString()
                       });

            return await ret.ToListAsync();
        }

        public async Task<List<JobProductItemProductItemLookupTableDto>> GetAllProductItemForTableDropdown()
        {
            return await _lookup_productItemRepository.GetAll().Where(x => x.Active == true)
                .Select(productItem => new JobProductItemProductItemLookupTableDto
                {
                    Id = productItem.Id,
                    Size = productItem.Size,
                    ProductTypeId = productItem.ProductTypeId,
                    Model = productItem.Model,
                    DisplayName = productItem == null || productItem.Name == null ? "" : productItem.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobProductItemProductItemLookupTableDto>> GetAllProductItemForTableDropdownForEdit()
        {
            return await _lookup_productItemRepository.GetAll()
                .Select(productItem => new JobProductItemProductItemLookupTableDto
                {
                    Id = productItem.Id,
                    Size = productItem.Size,
                    ProductTypeId = productItem.ProductTypeId,
                    Model = productItem.Model,
                    DisplayName = productItem == null || productItem.Name == null ? "" : productItem.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<ProductItemProductTypeLookupTableDto>> GetAllProductTypeForTableDropdown()
        {
            return await _lookup_productTypeRepository.GetAll()
                .Select(productType => new ProductItemProductTypeLookupTableDto
                {
                    Id = productType.Id,
                    DisplayName = productType == null || productType.Name == null ? "" : productType.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetReviewTypeDropdown()
        {
            var servieStatuslist = (from item in _reviewtypeRepository.GetAll()
                                    select new CommonLookupDto
                                    {
                                        Id = item.Id,
                                        DisplayName = item.Name,
                                    }).ToList();
            return servieStatuslist;
        }

        public async Task<List<CommonLookupDto>> getServieSubCategoryDropdown(string Name)
        {
            var id = _lookup_serviceCategoryRepository.GetAll().Where(e => e.Name == Name).Select(e => e.Id).FirstOrDefault();

            var serviesubCategorylist = (from item in _lookup_serviceSubCategoryRepository.GetAll().WhereIf(id != 0 && id != null && Name != null, e => e.ServiceCategoryId == id)
                                         select new CommonLookupDto
                                         {
                                             Id = item.Id,
                                             DisplayName = item.Name,
                                         }).ToList();
            return serviesubCategorylist;


        }

        public DocumentReturnValuesDto SaveCommonDocument(byte[]? ByteArray, string filename, string SectionName, int? JobId, int? DocumentTypeId, int? TenantId, int wholeSaleleadId = 0)
        {
            if (ByteArray == null)
            {
                throw new UserFriendlyException("There is no such file");
            }

            if (ByteArray.Length > MaxFileBytes)
            {
                throw new UserFriendlyException("Document size exceeded");
            }

            ///var Path1 = _env.WebRootPath;
            var MainFolder = ApplicationSettingConsts.DocumentPath; //From Const
            MainFolder = MainFolder + "\\Documents";

            string TenantPath = "";
            string JobPath = "";
            string SignaturePath = "";
            var FinalFilePath = "";
            var TenantName = "";
            var JobNumber = "";
            var wholeSaleLeadCompanyName = "";
            if (SectionName == "DocumentLibrary" || SectionName == "Installer" || SectionName == "InvoiceImportFiles" || SectionName == "MyInstallerOrganizationDoc" || SectionName == "ProductItem" || SectionName == "ServiceSubCategory" || SectionName == "Service" || SectionName == "PurchaseCompany" || SectionName == "Voucher")
            {
                if (AbpSession.TenantId != null)
                {
                    TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                }
                else
                {
                    TenantName = _tenantRepository.GetAll().Where(e => e.Id == TenantId).Select(e => e.TenancyName).FirstOrDefault();
                }
                TenantPath = MainFolder + "\\" + TenantName;
                JobPath = TenantPath;
                SignaturePath = JobPath + "\\" + SectionName;
                FinalFilePath = SignaturePath + "\\" + filename;
            }
            else if (wholeSaleleadId != 0)
            {
                wholeSaleLeadCompanyName = _wholeSaleLeadRepository.GetAll().Where(e => e.Id == wholeSaleleadId).FirstOrDefault().CompanyName;
                var tenantid = _wholeSaleLeadRepository.GetAll().Where(e => e.Id == wholeSaleleadId).FirstOrDefault().TenantId;
                TenantName = _tenantRepository.GetAll().Where(e => e.Id == tenantid).Select(e => e.TenancyName).FirstOrDefault();

                TenantPath = MainFolder + "\\" + TenantName;
                JobPath = TenantPath + "\\" + SectionName;
                SignaturePath = JobPath + "\\" + wholeSaleLeadCompanyName;
                FinalFilePath = SignaturePath + "\\" + filename;
            }
            else
            {
                JobNumber = _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.JobNumber).FirstOrDefault();
                var tenantid = _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.TenantId).FirstOrDefault();
                TenantName = _tenantRepository.GetAll().Where(e => e.Id == tenantid).Select(e => e.TenancyName).FirstOrDefault();

                TenantPath = MainFolder + "\\" + TenantName;
                JobPath = TenantPath + "\\" + JobNumber;
                SignaturePath = "";
                if (DocumentTypeId == 1 && SectionName == "Quotation")
                {
                    SignaturePath = JobPath + "\\NearMap";
                }
                else if (DocumentTypeId != 1 && SectionName == "Quotation")
                {
                    SignaturePath = JobPath + "\\Documents";
                }
                else
                {
                    SignaturePath = JobPath + "\\" + SectionName;
                }
                FinalFilePath = SignaturePath + "\\" + filename;

            }



            if (System.IO.Directory.Exists(MainFolder))
            {
                if (System.IO.Directory.Exists(TenantPath))
                {
                    if (System.IO.Directory.Exists(JobPath))
                    {
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(JobPath);
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(TenantPath);
                    if (System.IO.Directory.Exists(JobPath))
                    {
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(JobPath);
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
            }
            else
            {
                System.IO.Directory.CreateDirectory(MainFolder);
                if (System.IO.Directory.Exists(TenantPath))
                {
                    if (System.IO.Directory.Exists(JobPath))
                    {
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(JobPath);
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(TenantPath);
                    if (System.IO.Directory.Exists(JobPath))
                    {
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(JobPath);
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
            }

            var doc = new DocumentReturnValuesDto();
            doc.FinalFilePath = FinalFilePath;
            doc.SignaturePath = SignaturePath;
            doc.filename = filename;
            doc.TenantName = TenantName;
            doc.JobNumber = JobNumber;
            return doc;
        }


        public async Task<List<LeadSourceLookupTableDto>> GetAllLeadSourceForTableDropdownByOrg(int Id)
        {
            var User = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            var orgLeadSource = _leadSourceOrganizationUnitRepository.GetAll().Where(e => e.OrganizationUnitId == Id).Select(e => e.LeadSourceId).ToList();

            return await _lookup_leadSourceRepository.GetAll().WhereIf(orgLeadSource != null, u => orgLeadSource.Contains(u.Id)).WhereIf(role != null && role.Contains("Sales Rep"), u => u.IsActiveSalesRep == true)
                        .Select(leadSource => new LeadSourceLookupTableDto
                        {
                            Id = leadSource.Id,
                            DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
                        }).ToListAsync();

            //return await _lookup_leadSourceRepository.GetAll()
            //            .Select(leadSource => new LeadSourceLookupTableDto
            //            {
            //                Id = leadSource.Id,
            //                DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
            //            }).ToListAsync();
        }

        public async Task<List<LeadSourceLookupTableDto>> GetAllLeadSourceForTableDropdownOnlyOrg(int Id)
        {
            var orgLeadSource = _leadSourceOrganizationUnitRepository.GetAll().Where(e => e.OrganizationUnitId == Id).Select(e => e.LeadSourceId).ToList();

            return await _lookup_leadSourceRepository.GetAll().WhereIf(orgLeadSource != null, u => orgLeadSource.Contains(u.Id))
                        .Select(leadSource => new LeadSourceLookupTableDto
                        {
                            Id = leadSource.Id,
                            DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
                        }).ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetTemplateTypeForTableDropdown()
        {
            return await _templateTypeRepository.GetAll()
                .Select(TemplateType => new CommonLookupDto
                {
                    Id = TemplateType.Id,
                    DisplayName = TemplateType == null || TemplateType.Name == null ? "" : TemplateType.Name.ToString()
                }).ToListAsync();
        }

        public string DownloadQuote(string JobNumber, object QuoteString, string FileName)
        {
            var FileUrl = GeneratePdf(JobNumber, QuoteString, FileName);

            //var NearMap = _documentRepository.GetAll().Where(e => (e.DocumentTypeId == 1) && e.JobFk.JobNumber == JobNumber).OrderByDescending(e => e.Id).FirstOrDefault();
            var NearMap = _documentRepository.GetAll().Where(e => (e.DocumentTypeId == 1 || e.DocumentTypeId == 16) && e.JobFk.JobNumber == JobNumber).OrderByDescending(e => e.Id).FirstOrDefault();

            var OrgId = _jobRepository.GetAll().Where(e => e.JobNumber == JobNumber).Select(e => e.LeadFk.OrganizationId).FirstOrDefault();

            var ViewDocPath = ApplicationSettingConsts.ViewDocumentPath;
            if (NearMap != null)
            {
                DateTime dateTime = new DateTime();
                if (OrgId == 7)
                {
                    dateTime = new DateTime(2023, 1, 04);
                }
                else
                {
                    dateTime = new DateTime(2022, 9, 17);
                }

                ViewDocPath = NearMap.CreationTime > dateTime ? ViewDocPath : "https://docs.thesolarproduct.com";

                var NearMapUrl = ViewDocPath + NearMap.FilePath.Replace("\\", "/") + NearMap.FileName;

                var urls = new List<string>();
                urls.Add(FileUrl);
                urls.Add(NearMapUrl);

                FileUrl = GenerateMergedPdf(urls, FileName);
            }

            return FileUrl;
        }

        public string DownloadPdf(string JobNumber, object String, string FileName)
        {
            var FileUrl = GeneratePdf(JobNumber, String, FileName);

            return FileUrl;
        }

        public async Task<List<CommonLookupDto>> GetAllEmailProviderDropdown()
        {
            return await _emailProvidersRepository.GetAll()
                .Select(e => new CommonLookupDto
                {
                    Id = e.Id,
                    DisplayName = e.Name
                }).ToListAsync();
        }

        public async Task<List<OrganizationUnitMapDto>> GetOrganizationMapById(int organizationId)
        {
            var output = await _organizationUnitMapRepository.GetAll().Where(x => x.OrganizationUnitId == organizationId).AsNoTracking().ToListAsync();
            var result = ObjectMapper.Map<List<OrganizationUnitMapDto>>(output);

            return result;
        }

        public async Task<List<CommonLookupDto>> GetAllUsersByRoleNameTableDropdown(string RoleName, int OrgId)
        {
            var roleId = 0;
            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);

            if (!string.IsNullOrWhiteSpace(RoleName) && RoleName != "Without Installer")
            {
                roleId = Convert.ToInt32(_roleManager.GetRoleByName(RoleName).Id);
            }

            var filter = _userManager.Users.Where(e => e.IsActive == true)
                .WhereIf(OrgId != 0, u => u.OrganizationUnits.Any(r => r.OrganizationUnitId == OrgId))
                .WhereIf(roleId != 0, u => u.Roles.Any(r => r.RoleId == roleId));

            if (RoleName == "Without Installer")
            {
                filter = _userManager.Users.Where(e => e.IsActive == true)
                .WhereIf(OrgId != 0, u => u.OrganizationUnits.Any(r => r.OrganizationUnitId == OrgId))
                .WhereIf(roleId != 0, u => u.Roles.Any(r => r.RoleId == roleId)).Where(e => e.ismyinstalleruser != true);
            }

            //if(RoleName == "Wholesale Manager" && role.Contains("Wholesale SalesRep"))
            //{

            //    var Team = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //    var UserIds = _userTeamRepository.GetAll().Where(e => Team.Contains(e.TeamId)).Select(e => e.UserId).ToList();
            //    //var Users = _userRepository.GetAll().Where(e => e.Roles.Any(r => r.RoleId == roleId)).Select(e => e.Id).ToList();

            //    filter = _userManager.Users.Where(e => e.IsActive == true)
            //    .WhereIf(OrgId != 0, u => u.OrganizationUnits.Any(r => r.OrganizationUnitId == OrgId))
            //    .WhereIf(UserIds.Count() != 0, u => UserIds.Contains(u.Id) && u.Roles.Any(r => r.RoleId == roleId));
            //}

            //if (RoleName == "Wholesale SalesRep" && role.Contains("Wholesale Manager"))
            //{

            //    var Team = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //    var Users = _userTeamRepository.GetAll().Where(e => Team.Contains(e.TeamId)).Select(e => e.UserId).ToList();

            //    filter = _userManager.Users.Where(e => e.IsActive == true)
            //    .WhereIf(OrgId != 0, u => u.OrganizationUnits.Any(r => r.OrganizationUnitId == OrgId))
            //    .WhereIf(roleId != 0, u => Users.Contains(u.Id) && u.Roles.Any(r => r.RoleId == roleId));
            //}

            var output = from o in filter
                         select new CommonLookupDto
                         {
                             Id = (int)o.Id,
                             DisplayName = o == null || o.FullName == null ? "" : o.FullName.ToString()
                         };

            return await output.ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetAllUsersByRoleNameContainsTableDropdown(string[] RoleName, int OrgId)
        {
            var role = _roleRepository.GetAll().Where(e => RoleName.Contains(e.Name)).Select(e => e.Id);

            var filter = _userManager.Users
                .WhereIf(OrgId != 0, u => u.OrganizationUnits.Any(r => r.OrganizationUnitId == OrgId))
                .Where(u => u.Roles.Any(r => role.Contains(r.RoleId)));

            var output = from o in filter
                         select new CommonLookupDto
                         {
                             Id = (int)o.Id,
                             DisplayName = o.FullName.ToString()
                         };

            return await output.ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetAllVariationForDropdown()
        {
            var variation = _lookup_variationRepository.GetAll();

            var ret = (from o in variation
                       select new CommonLookupDto
                       {
                           Id = o.Id,
                           DisplayName = o.Name.ToString(),
                       });

            return await ret.ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetAllHouseTypeForDropdown()
        {
            var houseType = _houseTypeRepository.GetAll();

            var ret = (from o in houseType
                       select new CommonLookupDto
                       {
                           Id = o.Id,
                           DisplayName = o.Name.ToString(),
                       });

            return await ret.ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetAllRoofAngleForDropdown()
        {
            var roofAngle = _roofAngleRepository.GetAll();

            var ret = (from o in roofAngle
                       select new CommonLookupDto
                       {
                           Id = o.Id,
                           DisplayName = o.Name.ToString(),
                       });

            return await ret.ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetAllOtherChargeForDropdown()
        {
            var otherCharge = _otherChargeRepository.GetAll();

            var ret = (from o in otherCharge
                       select new CommonLookupDto
                       {
                           Id = o.Id,
                           DisplayName = o.Name.ToString(),
                       });

            return await ret.ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetAllRoofTypeForDropdown()
        {
            var roofType = _roofTypeRepository.GetAll();

            var ret = (from o in roofType
                       select new CommonLookupDto
                       {
                           Id = o.Id,
                           DisplayName = o.Name.ToString(),
                       });

            return await ret.ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetAllUsersForAppointment(int OrgId)
        {
            var filter = _userManager.Users
            .WhereIf(OrgId != 0, u => u.OrganizationUnits.Any(r => r.OrganizationUnitId == OrgId))
            .Where(u => u.IsOutdoor == true);

            var output = from o in filter
                         select new CommonLookupDto
                         {
                             Id = (int)o.Id,
                             DisplayName = o.FullName.ToString()
                         };

            return await output.ToListAsync();
        }
        public async Task<List<LocalityLookupDto>> GetAlLocalityForTableDropdown()
        {
            var filter = _enfornicaCallDetailsRepository.GetAll();

            var output = from o in filter
                         select new LocalityLookupDto
                         {

                             DisplayName = o.FromLocationLocality.ToString()
                         };

            return await output.Distinct().ToListAsync();

        }

        public async Task<List<CommonLookupDto>> GetAllUsersTableDropdownForUserCallHistory(int OrgId)
        {

            var filter = _userManager.Users
                .WhereIf(OrgId != 0, u => u.OrganizationUnits.Any(r => r.OrganizationUnitId == OrgId))
                .Where(u => u.Roles.Any(r => r.RoleId != 6));

            var output = from o in filter
                         select new CommonLookupDto
                         {
                             Id = (int)o.Id,
                             DisplayName = o == null || o.FullName == null ? "" : o.FullName.ToString()
                         };

            return await output.ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetWholesaleStatusDropdown()
        {
            var servieCategorylist = (from item in _wholeSaleStatusRepository.GetAll()
                                      select new CommonLookupDto
                                      {
                                          Id = item.Id,
                                          DisplayName = item.Name,
                                      }).ToList();
            return servieCategorylist;
        }
        public async Task<List<string>> GetCurrentUserRoles()
        {
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var role = await _userManager.GetRolesAsync(User);

            return role.ToList();
        }

        public async Task<List<CommonLookupDto>> GetVppConnection()
        {
            var vppConnections = await (from item in _vppConectionRepository.GetAll()
                                        select new CommonLookupDto
                                        {
                                            Id = item.Id,
                                            DisplayName = item.Name,
                                        }).ToListAsync();
            return vppConnections;
        }

        public async Task<List<string>> GetAllUnitType(string unitType)
        {
            var unitTypes = await _unitTypeRepository.GetAll()
                 .WhereIf(!string.IsNullOrWhiteSpace(unitType), c => c.Name.ToLower().Contains(unitType.ToLower()))
                 .Select(unitTypes => unitTypes == null || unitTypes.Name == null ? "" : unitTypes.Name.ToString()).AsNoTracking().ToListAsync();

            return unitTypes;
        }

        public async Task<List<string>> GetAllStreetName(string streetName)
        {
            return await _streetNameRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(streetName), c => c.Name.ToLower().Contains(streetName.ToLower()))
                .Select(streetNames => streetNames == null || streetNames.Name == null ? "" : streetNames.Name.ToString()).AsNoTracking().ToListAsync();
        }

        public async Task<List<string>> GetAllStreetType(string streetType)
        {
            return await _streetTypeRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(streetType), c => c.Name.ToLower().Contains(streetType.ToLower()))
                .Select(streetTypes => streetTypes == null || streetTypes.Name == null ? "" : streetTypes.Name.ToString()).AsNoTracking().ToListAsync();
        }

        public async Task<List<string>> GetAllSuburb(string suburb)
        {
            var result = _lookup_postCodeRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(suburb), c => c.Suburb.ToLower().Contains(suburb.ToLower())).AsNoTracking().Select(e => new { e.Id, e.StateId, e.Suburb, e.PostalCode });

            var suburbs = from o in result
                          join o1 in _lookup_stateRepository.GetAll().AsNoTracking() on o.StateId equals o1.Id into j1
                          from s1 in j1.DefaultIfEmpty()
                          select new LeadStatusLookupTableDto()
                          {
                              Value = o.Id,
                              Name = o.Suburb.ToString() + " || " + s1.Name.ToString() + " || " + o.PostalCode
                          };

            return await suburbs.Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
        }

        public DocumentReturnValuesDto SaveEcommerceDocument(byte[] ByteArray, string filename, string SectionName, int? TenantId)
        {
            if (ByteArray == null)
            {
                throw new UserFriendlyException("There is no such file");
            }

            if (ByteArray.Length > MaxFileBytes)
            {
                throw new UserFriendlyException("Document size exceeded");
            }

            ///var Path1 = _env.WebRootPath;
            var MainFolder = ApplicationSettingConsts.DocumentPath; //From Const
            MainFolder = MainFolder + "\\Documents";

            string TenantPath = "";
            string FolderPath = "";
            var FinalFilePath = "";
            var TenantName = "";
            var wholesalePath = "";

            if (AbpSession.TenantId != null)
            {
                TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
            }
            else
            {
                TenantName = _tenantRepository.GetAll().Where(e => e.Id == TenantId).Select(e => e.TenancyName).FirstOrDefault();
            }
            TenantPath = MainFolder + "\\" + TenantName;
            wholesalePath = TenantPath + "\\" + "WholeSale";
            FolderPath = wholesalePath + "\\" + SectionName;
            FinalFilePath = FolderPath + "\\" + filename;




            if (System.IO.Directory.Exists(MainFolder))
            {
                if (System.IO.Directory.Exists(TenantPath))
                {
                    if (System.IO.Directory.Exists(wholesalePath))
                    {
                        if (System.IO.Directory.Exists(FolderPath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(FolderPath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(wholesalePath);
                        if (System.IO.Directory.Exists(FolderPath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(FolderPath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(TenantPath);
                    if (System.IO.Directory.Exists(wholesalePath))
                    {
                        if (System.IO.Directory.Exists(FolderPath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(FolderPath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(wholesalePath);
                        if (System.IO.Directory.Exists(FolderPath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(FolderPath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
            }
            else
            {
                System.IO.Directory.CreateDirectory(MainFolder);
                if (System.IO.Directory.Exists(TenantPath))
                {
                    if (System.IO.Directory.Exists(wholesalePath))
                    {
                        if (System.IO.Directory.Exists(FolderPath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(FolderPath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(wholesalePath);
                        if (System.IO.Directory.Exists(FolderPath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(FolderPath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(TenantPath);
                    if (System.IO.Directory.Exists(wholesalePath))
                    {
                        if (System.IO.Directory.Exists(FolderPath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(FolderPath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(wholesalePath);
                        if (System.IO.Directory.Exists(FolderPath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(FolderPath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
            }


            var doc = new DocumentReturnValuesDto();
            doc.FinalFilePath = FinalFilePath;
            doc.SignaturePath = FolderPath;
            doc.filename = filename;
            doc.TenantName = TenantName;
            doc.JobNumber = "";
            return doc;
        }

        public async Task<List<CommonLookupDto>> GetSpecialStatusDropdown()
        {
            var ecomerceSpecialStatus = await (from item in _ecomerceSpecialStatusRepository.GetAll()
                                               select new CommonLookupDto
                                               {
                                                   Id = item.Id,
                                                   DisplayName = item.Status,
                                               }).ToListAsync();
            return ecomerceSpecialStatus;
        }

        public async Task<List<CommonLookupDto>> GetEcommerceDocumentTypeDropdown()
        {
            var ecomerceSpecialStatus = await (from item in _ecomerceDocumentTypeRepository.GetAll()
                                               select new CommonLookupDto
                                               {
                                                   Id = item.Id,
                                                   DisplayName = item.DocumentType,
                                               }).ToListAsync();
            return ecomerceSpecialStatus;
        }

        public async Task<List<CommonLookupDto>> GetEcommercePriceCategoryDropdown()
        {
            var ecomerceSpecialStatus = await (from item in _ecomercePriceCategoryRepository.GetAll()
                                               select new CommonLookupDto
                                               {
                                                   Id = item.Id,
                                                   DisplayName = item.PriceCategory,
                                               }).ToListAsync();
            return ecomerceSpecialStatus;
        }
        public async Task<List<CommonLookupDto>> GetBrandingPartnersDropdown(int? categoryId)
        {
            var ecomerceSpecialStatus = await (from item in _brandingPartnerRepository.GetAll().WhereIf(categoryId > 0, e => e.ProductCategoryId == categoryId)
                                               select new CommonLookupDto
                                               {
                                                   Id = item.Id,
                                                   DisplayName = item.BrandName,
                                               }).ToListAsync();
            return ecomerceSpecialStatus;
        }
        public async Task<List<CommonLookupDto>> GetSeriesDropdown(int? categoryId)
        {
            var series = await (from item in _seriesRepository.GetAll().WhereIf(categoryId > 0, e => e.ProductCategoryId == categoryId)
                                select new CommonLookupDto
                                {
                                    Id = item.Id,
                                    DisplayName = item.SeriesName,
                                }).ToListAsync();
            return series;
        }

        public List<NameValue<string>> GetCustomerTypes(string query)
        {
            var ListOfJobStatus = _customerTypeRepository.GetAll()
                            .Where(c => c.Type.Contains(query))
                            .Select(jobstatus =>
                                new LeadStatusLookupTableDto
                                {
                                    Value = jobstatus.Id,
                                    Name = jobstatus.Type.ToString()
                                });


            //Making NameValue Collection of Multi-Select DropDown

            var jobstatus = new List<NameValue<string>>();
            foreach (var item in ListOfJobStatus)
                jobstatus.Add(new NameValue { Name = item.Name, Value = item.Value.ToString() });


            return
                jobstatus.IsNullOrEmpty() ?
                jobstatus.ToList() :
                jobstatus.Where(c => c.Name.ToLower().Contains(query.ToLower())).ToList();
        }

        public async Task<List<CommonLookupDto>> GetTradingSTCWhomDropdown()
        {
            var ecomerceSpecialStatus = await (from item in _tradingSTCWhomRepository.GetAll()
                                               select new CommonLookupDto
                                               {
                                                   Id = item.Id,
                                                   DisplayName = item.Name,
                                               }).ToListAsync();
            return ecomerceSpecialStatus;
        }

        public async Task<List<string>> GetAllInstallerInvoiceType()
        {
            var output = await _installerInvoiceTypeRepository.GetAll().Select(e => e.Type).ToListAsync();

            return output;
        }

        public async Task<List<CommonLookupDto>> GetTransportCompanyDropdown()
        {
            var company = await (from item in _transportCompanyRepository.GetAll()
                                 select new CommonLookupDto
                                 {
                                     Id = item.Id,
                                     DisplayName = item.CompanyName,
                                 }).ToListAsync();
            return company;
        }



        public async Task<List<CommonLookupDto>> GetJobsDropdown(string searchTerm)
        {
            var company = await (from item in _jobRepository.GetAll().Where(e => e.JobNumber.Contains(searchTerm))
                                 select new CommonLookupDto
                                 {
                                     Id = item.Id,
                                     DisplayName = item.JobNumber,
                                 }).ToListAsync();
            return company;
        }
        public async Task<List<CommonLookupDto>> GetPurchaseCompanyDropdown()
        {
            var PurchaseCompany = await (from item in _purchaseCompanyRepository.GetAll().Where(e => e.IsActive == true)
                                         select new CommonLookupDto
                                         {
                                             Id = item.Id,
                                             DisplayName = item.Name,
                                         }).ToListAsync();
            return PurchaseCompany;
        }
        public async Task<List<CommonLookupDto>> GetStockOrderStatusDropdown()
        {
            var StockOrderStatus = await (from item in _stockOrderStatusRepository.GetAll().Where(e => e.IsActive == true)
                                          select new CommonLookupDto
                                          {
                                              Id = item.Id,
                                              DisplayName = item.Name,
                                          }).ToListAsync();
            return StockOrderStatus;
        }

        public async Task<List<CommonLookupDto>> GetDeliveryTypeDropdown()
        {
            var DeliveryType = await (from item in _deliverytypesRepository.GetAll().Where(e => e.IsActive == true)
                                      select new CommonLookupDto
                                      {
                                          Id = item.Id,
                                          DisplayName = item.Name,
                                      }).ToListAsync();
            return DeliveryType;
        }

        public async Task<List<CommonLookupDto>> GetpaymentTypeDropdown()
        {
            var paymentType = await (from item in _paymentTypeRepository.GetAll().Where(e => e.IsActive == true)
                                     select new CommonLookupDto
                                     {
                                         Id = item.Id,
                                         DisplayName = item.Name,
                                     }).ToListAsync();
            return paymentType;
        }

        public async Task<List<CommonLookupDto>> GetpaymentMethodDropdown()
        {
            var paymentMethod = await (from item in _paymentMethodRepository.GetAll().Where(e => e.IsActive == true)
                                       select new CommonLookupDto
                                       {
                                           Id = item.Id,
                                           DisplayName = item.Name,
                                       }).ToListAsync();
            return paymentMethod;
        }

        public async Task<List<CommonLookupDto>> GetcurrencyDropdown()
        {
            var currency = await (from item in _currencyRepository.GetAll().Where(e => e.IsActive == true)
                                  select new CommonLookupDto
                                  {
                                      Id = item.Id,
                                      DisplayName = item.Name,
                                  }).ToListAsync();
            return currency;
        }
        public async Task<List<CommonLookupDto>> GetLocationDropdown()
        {
            var Location = await (from item in _warehouseLocationRepository.GetAll()
                                  select new CommonLookupDto
                                  {
                                      Id = item.Id,
                                      DisplayName = item.location,
                                  }).ToListAsync();
            return Location;
        }
        public async Task<List<CommonLookupDto>> GetStockorderfoDropdown()
        {
            var Location = await (from item in _stockorderforRepository.GetAll().Where(e => e.IsActive == true)
                                  select new CommonLookupDto
                                  {
                                      Id = item.Id,
                                      DisplayName = item.Name,
                                  }).ToListAsync();
            return Location;
        }
        public async Task<List<CommonLookupDto>> GetStockOrderFromOutsideDropdown()
        {
            var Location = await (from item in _stockfromsRepository.GetAll().Where(e => e.IsActive == true)
                                  select new CommonLookupDto
                                  {
                                      Id = item.Id,
                                      DisplayName = item.Name,
                                  }).ToListAsync();
            return Location;
        }
        public async Task<List<CommonLookupDto>> GetStockOrderPaymentStatusDropdown()
        {
            var Location = await (from item in _paymentStatusRepository.GetAll().Where(e => e.IsActive == true)
                                  select new CommonLookupDto
                                  {
                                      Id = item.Id,
                                      DisplayName = item.Name,
                                  }).ToListAsync();
            return Location;
        }
        public async Task<List<CommonLookupDto>> GetAllVendorDropdown()
        {
            var Location = await (from item in _vendorRepository.GetAll()
                                  select new CommonLookupDto
                                  {
                                      Id = item.Id,
                                      DisplayName = item.CompanyName,
                                  }).ToListAsync();
            return Location;
        }

        public async Task<List<CommonLookupDto>> GetSTCProviderDropdown(int OrgId)
        {
            var Location = await (from item in _stcProviderRepository.GetAll()
                                  select new CommonLookupDto
                                  {
                                      Id = item.Id,
                                      DisplayName = item.Name,
                                  }).ToListAsync();
            return Location;
        }

        public async Task<List<CommonLookupDto>> GetIncoTermDropdown()
        {
            var IncoTerm = await (from item in _IncoTermRepository.GetAll().Where(e => e.IsActive == true)
                                  select new CommonLookupDto
                                  {
                                      Id = item.Id,
                                      DisplayName = item.Name,
                                  }).ToListAsync();
            return IncoTerm;
        }

        public async Task<List<CommonLookupDto>> GetWarehouseLocationForFilter(string str)
        {
            var warehouseLocation = await (from item in _warehouseLocationRepository.GetAll()
                                  .WhereIf(!string.IsNullOrEmpty(str), e => e.state.ToLower().Contains(str.ToLower()))
                                           select new CommonLookupDto
                                           {
                                               Id = item.Id,
                                               DisplayName = item.state,
                                           }).ToListAsync();

            return warehouseLocation;
        }
        public async Task<List<CommonLookupDto>> GetBonusesDropdown()
        {
            var Bonuses = await (from item in _bonusRepository.GetAll().Where(e => e.IsActive == true)
                                 select new CommonLookupDto
                                 {
                                     Id = item.Id,
                                     DisplayName = item.Name,
                                 }).ToListAsync();
            return Bonuses;
        }

        public async Task<List<CommonLookupDto>> GetEcommerceProductTypesDropdown()
        {
            var series = await (from item in _ecommerceProductTypeRepository.GetAll()
                                select new CommonLookupDto
                                {
                                    Id = item.Id,
                                    DisplayName = item.Type,
                                }).ToListAsync();
            return series;
        }
        public async Task<List<CommonLookupDto>> GetSpecification(int? type)
        {
            var specification = _ecommerceSpecificationRepository.GetAll().Where(e => !string.IsNullOrEmpty(e.ProductType) && e.IsActive == true);
            var output = new List<CommonLookupDto>();
            foreach (var Sep in specification)
            {

                var data = Sep.ProductType.Split(',');

                if (data.Where(e => e.Trim() == type.ToString()).Any())
                {
                    var Com = new CommonLookupDto();
                    Com.Id = Sep.Id;
                    Com.DisplayName = Sep.Name;
                    output.Add(Com);
                }
            }

            return output;

        }

        public async Task<List<CommonLookupDto>> GetWholesaleInvoicetypeDropdown()
        {
            var types = await (from item in _wholesaleInvoicetypeRepository.GetAll()
                               select new CommonLookupDto
                               {
                                   Id = item.Id,
                                   DisplayName = item.Name,
                               }).ToListAsync();
            return types;
        }

        public async Task<List<CommonLookupDto>> GetWholesaleJobTypeDropdown()
        {
            var types = await (from item in _wholesaleJobTypeRepository.GetAll()
                               select new CommonLookupDto
                               {
                                   Id = item.Id,
                                   DisplayName = item.Name,
                               }).ToListAsync();
            return types;
        }

        public async Task<List<CommonLookupDto>> GetWholesaleDelivaryOptionDropdown()
        {
            var types = await (from item in _wholesaleDeliveryOptionRepository.GetAll()
                               select new CommonLookupDto
                               {
                                   Id = item.Id,
                                   DisplayName = item.Name,
                               }).ToListAsync();
            return types;
        }

        public async Task<List<CommonLookupDto>> GetWholesaleJobStatusDropdown()
        {
            var types = await (from item in _wholesaleJobStatusRepository.GetAll()
                               select new CommonLookupDto
                               {
                                   Id = item.Id,
                                   DisplayName = item.Name,
                               }).ToListAsync();
            return types;
        }

        public async Task<List<CommonLookupDto>> GetWholesaleTransportTypeDropdown()
        {
            var types = await (from item in _wholesaleTransportTypeRepository.GetAll()
                               select new CommonLookupDto
                               {
                                   Id = item.Id,
                                   DisplayName = item.Name,
                               }).ToListAsync();
            return types;
        }

        public async Task<List<CommonLookupDto>> GetWholesalePropertyTypeDropdown()
        {
            var types = await (from item in _wholesalePropertyTypeRepository.GetAll()
                               select new CommonLookupDto
                               {
                                   Id = item.Id,
                                   DisplayName = item.Name,
                               }).ToListAsync();
            return types;
        }

        public async Task<List<CommonLookupDto>> GetWholesalePVDStatusDropdown()
        {
            var types = await (from item in _wholesalePVDStatusRepository.GetAll()
                               select new CommonLookupDto
                               {
                                   Id = item.Id,
                                   DisplayName = item.Name,
                               }).ToListAsync();
            return types;
        }

        public async Task<List<CommonLookupDto>> GetAllUsersByRoleNameNotContainsTableDropdown(string[] RoleName, int OrgId)
        {
            var role = _roleRepository.GetAll().Where(e => !RoleName.Contains(e.Name)).Select(e => e.Id);

            var filter = _userManager.Users
                .WhereIf(OrgId != 0, u => u.OrganizationUnits.Any(r => r.OrganizationUnitId == OrgId))
                .Where(u => u.Roles.Any(r => role.Contains(r.RoleId)));

            var output = from o in filter
                         select new CommonLookupDto
                         {
                             Id = (int)o.Id,
                             DisplayName = o.FullName.ToString()
                         };

            var result = await output.ToListAsync();

            return result;
        }

        public async Task<List<CommonLookupDto>> GetPurchaseDoucmentDropdown()
        {
            var types = await (from item in _purchaseDocumentRepository.GetAll().Where(e => e.IsActive == true)
                               select new CommonLookupDto
                               {
                                   Id = item.Id,
                                   DisplayName = item.Name,
                               }).ToListAsync();
            return types;
        }

        public async Task<List<GetProductItemSearchResult>> GetEcommerceProductItemList(int productTypeId, string productItem, int? wholesaleId)
        {
            //var productiteamids = _productiteamlocationRepository.GetAll().Where(e => e.WarehouselocationId == warehouseid && e.SalesTag == true).Select(e => e.ProductItemId).Distinct();
            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();

            //IList<string> role = await _userManager.GetRolesAsync(User);

            //if (!role.Contains("Admin"))
            //{
            //    return await _productItemRepository.GetAll()
            //        .Where(e => e.ProductTypeId == productTypeId && e.Active == true)
            //        .WhereIf(salesTag == true, e => productiteamids.Any(o => o == e.Id))
            //        .WhereIf(!string.IsNullOrWhiteSpace(productItem), c => c.Name.ToLower().Contains(productItem.ToLower()))
            //        .Select(p => new GetProductItemSearchResult() { Id = p.Id, ProductItem = p.Name, ProductModel = p.Model, Size = p.Size }).ToListAsync();
            //}
            //else
            //{

            //}

            var TodayDate = (_timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId));
            var priceCat = await _wholeSaleLeadRepository.GetAll().Where(e => e.Id == wholesaleId).Select(e => e.PriceCategoryId).FirstOrDefaultAsync();
            var Item = await _ecommerceProductItemRepository.GetAll()
                    .Where(e => e.ProductItemFk.ProductTypeId == productTypeId && e.ProductItemFk.Active == true)
                    .WhereIf(!string.IsNullOrWhiteSpace(productItem), c => c.ProductItemFk.Name.ToLower().Contains(productItem.ToLower()))
                    .Select(p => new { Id = p.Id, ProductItem = p.ProductItemFk.Name, ProductModel = p.ProductItemFk.Model, Size = p.ProductItemFk.Size, ExpiryDate = p.ProductItemFk.ExpiryDate.Value.Date, IsExpired = (p.ProductItemFk.ExpiryDate.Value.Date < TodayDate.Value.Date) }).ToListAsync();

            var result = from p in Item
                         let price = priceCat > 0 ? _ecommerceProductItemPriceCategoryRepository.GetAll().Where(e => e.EcommerceProductItemId == p.Id && e.ECommercePriceCategoryId == priceCat).Select(e => e.Price).FirstOrDefault() : 0
                         select new GetProductItemSearchResult()
                         {
                             Id = p.Id,
                             ProductItem = p.ProductItem,
                             ProductModel = p.ProductModel,
                             Size = p.Size,
                             ExpiryDate = p.ExpiryDate,
                             IsExpired = p.IsExpired,
                             Price = price
                         };

            return result.ToList();
        }

        public async Task<List<CommonLookupDto>> GetJobCostFixExpenseTypeDropdown()
        {
            var types = await (from item in _jobCostFixExpenseTypeRepository.GetAll()
                               select new CommonLookupDto
                               {
                                   Id = item.Id,
                                   DisplayName = item.Type,
                               }).ToListAsync();
            return types;
        }

        public async Task<List<CommonLookupDto>> GetAllProductItemForSearchable(string query)
        {
            var productItems = from item in _lookup_productItemRepository.GetAll()
                                      .Where(x => x.Active == true)
                                      .WhereIf(!string.IsNullOrWhiteSpace(query), c => c.Name.ToLower().Contains(query.ToLower()))
                                      select new CommonLookupDto
                                      {
                                          Id = item.Id,
                                          DisplayName = item.Name
                                      };

            return await productItems.ToListAsync();
        }

        public async Task<List<JobVariationVariationLookupTableDto>> GetAllStockVariation()
        {
            var variation = _stockVariationRepository.GetAll();

            var ret = (from o in variation
                       select new JobVariationVariationLookupTableDto
                       {
                           Id = o.Id,
                           DisplayName = o == null || o.Name == null ? "" : o.Name.ToString(),
                           ActionName = o == null || o.Action == null ? "" : o.Action.ToString()
                       });

            return await ret.ToListAsync();
        }
        public async Task<List<CommonLookupDto>> GetAllCheckDeposit()
        {
            var types = await (from item in _checkdepositeRepository.GetAll().Where(e => e.IsActive == true)
                               select new CommonLookupDto
                               {
                                   Id = item.Id,
                                   DisplayName = item.Name,
                               }).ToListAsync();
            return types;
        }
        public async Task<List<CommonLookupDto>> GetAllCheckActive()
        {
            var types = await (from item in _checkActiveRepository.GetAll().Where(e => e.IsActive == true)
                               select new CommonLookupDto
                               {
                                   Id = item.Id,
                                   DisplayName = item.Name,
                               }).ToListAsync();
            return types;
        }
        public async Task<List<CommonLookupDto>> GetAllCheckApplication()
        {
            var types = await (from item in _checkApplicationRepository.GetAll().Where(e => e.IsActive == true)
                               select new CommonLookupDto
                               {
                                   Id = item.Id,
                                   DisplayName = item.Name,
                               }).ToListAsync();
            return types;
        }
        public async Task<List<CommonLookupDto>> GetAllSerialNoStatus()
        {
            var types = await (from item in _serialNoStatusRepository.GetAll().Where(e => e.IsActive == true)
                               select new CommonLookupDto
                               {
                                   Id = item.Id,
                                   DisplayName = item.Status,
                               }).ToListAsync();
            return types;
        }
        public async Task<List<CommonLookupDto>> GetAllVoucher()
        {
            var types = await (from item in _voucherRepository.GetAll().Where(e => e.IsActive == true)
                               select new CommonLookupDto
                               {
                                   Id = item.Id,
                                   DisplayName = item.VoucherName,
                               }).ToListAsync();
            return types;
        }
    }
}
