﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.PostalTypes.Exporting;
using TheSolarProduct.PostalTypes.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace TheSolarProduct.PostalTypes
{
	[AbpAuthorize(AppPermissions.Pages_PostalTypes)]
    public class PostalTypesAppService : TheSolarProductAppServiceBase, IPostalTypesAppService
    {
		 private readonly IRepository<PostalType> _postalTypeRepository;
		 private readonly IPostalTypesExcelExporter _postalTypesExcelExporter;
		 

		  public PostalTypesAppService(IRepository<PostalType> postalTypeRepository, IPostalTypesExcelExporter postalTypesExcelExporter ) 
		  {
			_postalTypeRepository = postalTypeRepository;
			_postalTypesExcelExporter = postalTypesExcelExporter;
			
		  }

		 public async Task<PagedResultDto<GetPostalTypeForViewDto>> GetAll(GetAllPostalTypesInput input)
         {
			
			var filteredPostalTypes = _postalTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter) || e.Code.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter),  e => e.Code == input.CodeFilter);

			var pagedAndFilteredPostalTypes = filteredPostalTypes
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

			var postalTypes = from o in pagedAndFilteredPostalTypes
                         select new GetPostalTypeForViewDto() {
							PostalType = new PostalTypeDto
							{
                                Name = o.Name,
                                Code = o.Code,
                                Id = o.Id,
								IsActive = o.IsActive,
							}
						};

            var totalCount = await filteredPostalTypes.CountAsync();

            return new PagedResultDto<GetPostalTypeForViewDto>(
                totalCount,
                await postalTypes.ToListAsync()
            );
         }
		 
		 public async Task<GetPostalTypeForViewDto> GetPostalTypeForView(int id)
         {
            var postalType = await _postalTypeRepository.GetAsync(id);

            var output = new GetPostalTypeForViewDto { PostalType = ObjectMapper.Map<PostalTypeDto>(postalType) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_PostalTypes_Edit)]
		 public async Task<GetPostalTypeForEditOutput> GetPostalTypeForEdit(EntityDto input)
         {
            var postalType = await _postalTypeRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetPostalTypeForEditOutput {PostalType = ObjectMapper.Map<CreateOrEditPostalTypeDto>(postalType)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditPostalTypeDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_PostalTypes_Create)]
		 protected virtual async Task Create(CreateOrEditPostalTypeDto input)
         {
            var postalType = ObjectMapper.Map<PostalType>(input);
			postalType.Name = postalType.Name.ToUpper();
			postalType.Code = postalType.Code.ToUpper();



			await _postalTypeRepository.InsertAsync(postalType);
         }

		 [AbpAuthorize(AppPermissions.Pages_PostalTypes_Edit)]
		 protected virtual async Task Update(CreateOrEditPostalTypeDto input)
         {
            var postalType = await _postalTypeRepository.FirstOrDefaultAsync((int)input.Id);
			postalType.Name = postalType.Name.ToUpper();
			postalType.Code = postalType.Code.ToUpper();
			ObjectMapper.Map(input, postalType);
         }

		 [AbpAuthorize(AppPermissions.Pages_PostalTypes_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _postalTypeRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetPostalTypesToExcel(GetAllPostalTypesForExcelInput input)
         {
			
			var filteredPostalTypes = _postalTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter) || e.Code.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter),  e => e.Code == input.CodeFilter);

			var query = (from o in filteredPostalTypes
                         select new GetPostalTypeForViewDto() { 
							PostalType = new PostalTypeDto
							{
                                Name = o.Name,
                                Code = o.Code,
                                Id = o.Id,
								IsActive = o.IsActive,
							}
						 });


            var postalTypeListDtos = await query.ToListAsync();

            return _postalTypesExcelExporter.ExportToFile(postalTypeListDtos);
         }


    }
}