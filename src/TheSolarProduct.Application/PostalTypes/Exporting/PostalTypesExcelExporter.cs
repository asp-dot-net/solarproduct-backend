﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.PostalTypes.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.PostalTypes.Exporting
{
    public class PostalTypesExcelExporter : NpoiExcelExporterBase, IPostalTypesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PostalTypesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetPostalTypeForViewDto> postalTypes)
        {
            return CreateExcelPackage(
                "PostalTypes.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("PostalTypes"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Code")
                        );

                    AddObjects(
                        sheet, 2, postalTypes,
                        _ => _.PostalType.Name,
                        _ => _.PostalType.Code
                        );

					
					
                });
        }
    }
}
