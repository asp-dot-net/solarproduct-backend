﻿using System.Collections.Generic;
using TheSolarProduct.PostalTypes.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.PostalTypes.Exporting
{
    public interface IPostalTypesExcelExporter
    {
        FileDto ExportToFile(List<GetPostalTypeForViewDto> postalTypes);
    }
}