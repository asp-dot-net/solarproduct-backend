﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.FreightCompanyes.Dtos;

namespace TheSolarProduct.FreightCompanies.Exporting
{
    public interface IFreightCompanyExcelExporter
    {
        FileDto ExportToFile(List<GetFreightCompanyForViewDto> FreightCompany);

    }
}
