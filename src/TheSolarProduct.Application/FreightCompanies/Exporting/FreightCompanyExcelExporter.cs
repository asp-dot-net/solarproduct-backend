﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.FreightCompanyes.Dtos;
using TheSolarProduct.PaymentMethodes.Dtos;
using TheSolarProduct.PaymentMethodes.Exporting;
using TheSolarProduct.Storage;

namespace TheSolarProduct.FreightCompanies.Exporting
{
    
    public class FreightCompanyExcelExporter : NpoiExcelExporterBase, IFreightCompanyExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public FreightCompanyExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetFreightCompanyForViewDto> FreightCompany)
        {
            return CreateExcelPackage(
                "FreightCompany.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("FreightCompany"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("IsActive")
                    );

                    AddObjects(
                        sheet,
                        2,
                         FreightCompany,
                        _ => _.FreightCompany.Name,
                        _ => _.FreightCompany.IsActive.HasValue ? (_.FreightCompany.IsActive.Value ? L("Yes") : L("No")) : L("No")
                    );
                }
            );
        }


    }
}
