﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataVaults;
using TheSolarProduct.Dto;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.PaymentMethodes.Dtos;
using TheSolarProduct.PaymentMethodes.Exporting;
using TheSolarProduct.PaymentMethodes;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.FreightCompanies.Exporting;
using TheSolarProduct.FreightCompanyes;
using TheSolarProduct.FreightCompanyes.Dtos;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace TheSolarProduct.FreightCompanies
{
    
    [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_FreightCompany)]
    public class FreightCompanyAppService : TheSolarProductAppServiceBase, IFreightCompanyAppService
    {
        private readonly IFreightCompanyExcelExporter _freightCompanyExcelExporter;
        private readonly IRepository<FreightCompany> _freightCompanyRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public FreightCompanyAppService(
              IRepository<FreightCompany> freightCompanyRepository,
              IFreightCompanyExcelExporter IFreightCompanyExcelExporter,
              IRepository<UserTeam> userTeamRepository,
              IRepository<UserRole, long> userRoleRepository,
              IRepository<Role> roleRepository,
              IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
              )
        {
            _freightCompanyRepository = freightCompanyRepository;
            _freightCompanyExcelExporter = IFreightCompanyExcelExporter;
            _userTeamRepository = userTeamRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }




        public async Task<PagedResultDto<GetFreightCompanyForViewDto>> GetAll(GetAllFreightCompanyInput input)
        {

            var FreightCompany = _freightCompanyRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFilteredFreightCompany = FreightCompany
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var outputFreightCompany = from o in pagedAndFilteredFreightCompany
                                      select new GetFreightCompanyForViewDto()

                                      {
                                          FreightCompany = new FreightCompanyDto
                                          {
                                              Id = o.Id,
                                              Name = o.Name,
                                              IsActive = o.IsActive
                                          }
                                      };

            var totalCount = await FreightCompany.CountAsync();

            return new PagedResultDto<GetFreightCompanyForViewDto>(
                totalCount,
                await outputFreightCompany.ToListAsync()
            );
        }



        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_FreightCompany_Edit)]
        public async Task<GetFreightCompanyForEditOutput> GetFreightCompanyForEdit(EntityDto input)
        {
            var FreightCompany = await _freightCompanyRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetFreightCompanyForEditOutput { FreightCompany = ObjectMapper.Map<CreateOrEditFreightCompanyDto>(FreightCompany) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditFreightCompanyDto input)
        {


            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_FreightCompany_Create)]

        protected virtual async Task Create(CreateOrEditFreightCompanyDto input)
        {
            var FreightCompany = ObjectMapper.Map<FreightCompany>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 67;
            dataVaultLog.ActionNote = "FreightCompany Created : " + input.Name;
            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _freightCompanyRepository.InsertAsync(FreightCompany);
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_FreightCompany_Edit)]
        protected virtual async Task Update(CreateOrEditFreightCompanyDto input)
        {
            var FreightCompany = await _freightCompanyRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 67;
            dataVaultLog.ActionNote = "FreightCompany Updated : " + FreightCompany.Name;

            dataVaultLog.IsInventory = true;
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != FreightCompany.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = FreightCompany.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != FreightCompany.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = FreightCompany.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, FreightCompany);

            await _freightCompanyRepository.UpdateAsync(FreightCompany);


        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_FreightCompany_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _freightCompanyRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 67;
            dataVaultLog.ActionNote = "FreightCompany Deleted : " + Name;

            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _freightCompanyRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_FreightCompany_Export)]

        public async Task<FileDto> GetFreightCompanyToExcel(GetAllFreightCompanyForExcelInput input)
        {

            var filteredFreightCompany = _freightCompanyRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredFreightCompany
                         select new GetFreightCompanyForViewDto()
                         {
                             FreightCompany = new FreightCompanyDto
                             {
                                 Name = o.Name,
                                 Id = o.Id,
                                 IsActive = o.IsActive
                             }
                         });


            var ListDtos = await query.ToListAsync();

            return _freightCompanyExcelExporter.ExportToFile(ListDtos);
        }
    }
}
