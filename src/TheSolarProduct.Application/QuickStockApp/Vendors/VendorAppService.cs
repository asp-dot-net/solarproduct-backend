﻿using Abp.Domain.Repositories;
using Abp.Timing.Timezone;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheSolarProduct.Jobs;
using TheSolarProduct.QuickStockApp.Installer.Dto;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using System;
using TheSolarProduct.Vendors;
using TheSolarProduct.QuickStockApp.Vendors.Dto;
using Abp.Collections.Extensions;

namespace TheSolarProduct.QuickStockApp.Vendors
{
    public class VendorAppService : TheSolarProductAppServiceBase, IVendorAppService
    {
        private readonly IRepository<Vendor> _vendorRepository;

        public VendorAppService(
            IRepository<Vendor> vendorRepository
            ) { 
        _vendorRepository = vendorRepository;
        }
        public async Task<List<GetAllVendorDtoForOutput>> GetAllVendors(GetAllVendorInputDto input)
        {
            using (UnitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var filtered = _vendorRepository.GetAll()
                         .WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.CompanyName == input.Filter)
                         .WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Notes == input.Filter);

                var outputList = from o in filtered
                                 select new GetAllVendorDtoForOutput()
                                 {
                                     CompanyName = o.CompanyName,
                                     Notes = o.Notes,

                                     Id = o.Id
                                 };

                return new List<GetAllVendorDtoForOutput>(outputList.ToList());
            }

        }
    }
}
