﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore.EFPlus;
using Castle.MicroKernel.Registration;
using Hangfire.Common;
using Microsoft.AspNetCore.Mvc;
using NPOI.HPSF;
using NPOI.SS.Formula.Functions;
using PayPalCheckoutSdk.Orders;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.PaymentStatuses;
using TheSolarProduct.PaymentTypes;
using TheSolarProduct.PurchaseCompanies;
using TheSolarProduct.QuickStockApp.Installer.Dto;
using TheSolarProduct.QuickStockApp.StockOrder.Dto;
using TheSolarProduct.QuickStockApp.Vendors.Dto;
using TheSolarProduct.QuickStocks;
using TheSolarProduct.StockFroms;
using TheSolarProduct.StockOrder.Dtos;
using TheSolarProduct.StockOrders;
using TheSolarProduct.StockOrders.SerialNoStatuses;
using TheSolarProduct.StockOrderStatuses;
using TheSolarProduct.ThirdPartyApis.REC.Dtos;

namespace TheSolarProduct.QuickStockApp.StockOrder
{
    public class StockOrderAppService : TheSolarProductAppServiceBase, IStockOrderAppService
    {
        private readonly IRepository<PurchaseOrder> _purchaseOrderRepository;
        private readonly IRepository<PurchaseOrderItem> _purchaseOrderItemRepository;
        private readonly IRepository<UserWarehouseLocation> _userWarehouseLocationRepository;
        private readonly IRepository<StockSerialNo> _stockSerialNoRepository;
        private readonly IRepository<QuickStockActivityLog> _quickStockActivityLogRepository;
        private readonly IRepository<OtherStockItemQty> _otherStockItemQtyRepository;
        private readonly IRepository<AppDocument> _appDocumentRepository;
        private readonly IRepository<ProductItem> _ProductItemRepository;
        private readonly IRepository<StockSerialNoLogHistory> _stockSerialNoLogHistoryRepository;
        private readonly IRepository<Warehouselocation> _warehouselocationRepository;
        

        public StockOrderAppService(IRepository<PurchaseOrder> purchaseOrderRepository,
              IRepository<PurchaseOrderItem> purchaseOrderItemRepository,
              IRepository<UserWarehouseLocation> userWarehouseLocationRepository,
              IRepository<StockSerialNo> stockSerialNoRepository,
              IRepository<QuickStockActivityLog> quickStockActivityLogRepository,
              IRepository<OtherStockItemQty> otherStockItemQtyRepository,
            IRepository<AppDocument> appDocumentRepository,
             IRepository<ProductItem> productItemRepository,
              IRepository<StockSerialNoLogHistory> stockSerialNoLogHistoryRepository,
              IRepository<Warehouselocation> warehouselocationRepository
)
        {
            _purchaseOrderRepository = purchaseOrderRepository;
            _purchaseOrderItemRepository = purchaseOrderItemRepository;
            _userWarehouseLocationRepository = userWarehouseLocationRepository;
            _stockSerialNoRepository = stockSerialNoRepository;
            _quickStockActivityLogRepository = quickStockActivityLogRepository;
            _otherStockItemQtyRepository = otherStockItemQtyRepository;
            _appDocumentRepository = appDocumentRepository;
            _ProductItemRepository = productItemRepository;
            _stockSerialNoLogHistoryRepository = stockSerialNoLogHistoryRepository;
            _warehouselocationRepository = warehouselocationRepository;
        }

        public async Task<List<GetAllStockOrderOutput>> GetAllStockOrder(GetAllStockOrderInputDto input)
        {
           
                var userLocation = _userWarehouseLocationRepository.GetAll().Where(e => e.UserId == AbpSession.UserId).Select(e => e.WarehouseLocationId).ToList();
                var PurchaseOrder = _purchaseOrderRepository.GetAll().Include(e => e.PurchaseCompanyFk).Include(e => e.WarehouseLocationFk)
                    .Where(e => userLocation.Any(l => l == e.WarehouseLocationId) && e.IsSubmitDate == null);
                var purchaseOrderItems = _purchaseOrderItemRepository.GetAll().Include(e => e.ProductItemFk).Where(e => PurchaseOrder.Any(l => l.Id == e.PurchaseOrderId)).
                    Select(e => new {e.Id, e.PurchaseOrderId,e.ProductItemId, ProductType = e.ProductItemFk.ProductTypeFk.Name, e.ModelNo, e.Quantity, e.ProductItemFk.Name, e.ProductItemFk.IsScanned}).ToList();

                var outputStockOrder = (from o in PurchaseOrder
                                       select new GetAllStockOrderOutput()
                                       {
                                           Id = o.Id,
                                           OrderNo = o.OrderNo,
                                           VendorInoviceNo = o.VendorInoviceNo,
                                           Vendor = o.VendorFK.CompanyName,
                                           PurchaseCompany = o.PurchaseCompanyFk.Name,
                                           ContainerNo = o.ContainerNo,
                                           WarehouseLocation = o.WarehouseLocationFk.location,
                                           TargetArrivalDate = o.TargetArrivalDate != null ? o.TargetArrivalDate.Value.ToString("dd-MMM,yyyy") : "",

                                           
                                       }).ToList();

                foreach(var item in outputStockOrder)
                {
                    item.StockOrderItems = (from I in purchaseOrderItems.Where(e => e.PurchaseOrderId == item.Id)

                                            let scanCount = _stockSerialNoRepository.GetAll().Count(e => e.PurchaseOrderId == I.PurchaseOrderId && e.ProductItemId == I.ProductItemId)

                                            let OtherQty = _otherStockItemQtyRepository.GetAll().Where(e => e.PurchaseOrderId == I.PurchaseOrderId && e.ProductItemId == I.ProductItemId).Sum(e => e.Quantity)



                                            select new GetStockOrderItemDto()
                                            {
                                                ProductType = I.ProductType,
                                                ProductItem = I.Name,
                                                PandingQty = (int)(I.IsScanned ? I.Quantity - scanCount : OtherQty > 0 ? I.Quantity - OtherQty : I.Quantity),
                                                
                                            }).ToList();

                    var scanCountVerify = _stockSerialNoRepository.GetAll().Count(e => e.PurchaseOrderId == item.Id && e.IsVerify == true);
                    var Qty = purchaseOrderItems.Where(e => e.PurchaseOrderId == item.Id).Sum(e => e.Quantity);
                    var OtherQuntity = _otherStockItemQtyRepository.GetAll().Where(e => e.PurchaseOrderId == item.Id).Sum(e => e.Quantity);

                    item.IsSubmit = (((scanCountVerify > 0 ? scanCountVerify : 0) + (OtherQuntity > 0 ? OtherQuntity : 0)) == Qty);
                }

                return outputStockOrder.OrderByDescending(e => e.OrderNo).ToList();

            
        }

        public async Task<List<GetStockOrderItemDtoForOutput>> GetStockOrderItemById(int stockOrderId)
        {
            
                var purchaseOrderItems = _purchaseOrderItemRepository.GetAll().Include(e => e.ProductItemFk).Where(poi => poi.PurchaseOrderId == stockOrderId);

                var output = from o in purchaseOrderItems

                             let scanCount = _stockSerialNoRepository.GetAll().Count(e => e.PurchaseOrderId == o.PurchaseOrderId && e.ProductItemId == o.ProductItemId)

                             let isExcel = _stockSerialNoRepository.GetAll().Any(e => e.PurchaseOrderId == o.PurchaseOrderId && e.ProductItemId == o.ProductItemId && e.IsApp == false)

                             let OtherQty = _otherStockItemQtyRepository.GetAll().Where(e => e.PurchaseOrderId == o.PurchaseOrderId && e.ProductItemId == o.ProductItemId).Sum(e => e.Quantity)

                             select new GetStockOrderItemDtoForOutput()
                             {
                                 Id = o.Id,
                                 ProductItemId = (int)o.ProductItemId,
                                 ProductType = o.ProductItemFk.ProductTypeFk.Name,
                                 ModelNo = o.ModelNo,
                                 PendingQuantity = (int)(o.ProductItemFk.IsScanned ? o.Quantity - scanCount : OtherQty > 0?  o.Quantity - OtherQty :o.Quantity),
                                 ScanQuantity = o.ProductItemFk.IsScanned ? scanCount : OtherQty,
                                 Quantity = (int)o.Quantity,
                                 ProductItem = o.ProductItemFk.Name,
                                 IsExcel = isExcel,
                                 IsScanned = o.ProductItemFk.IsScanned
                             };

                return new List<GetStockOrderItemDtoForOutput>(output.ToList());
            
        }

        //public async Task SaveScanSerialNo(SaveSerialNoListInputDto input)
        //{
        //    using (UnitOfWorkManager.Current.SetTenantId(input.TenantId))
        //    {
        //        var orderid = input.orderid;
        //        var purchaseorderitemid = input.purchaseorderitemid;
        //        var palletno = input.palletno;
        //    }
        //}
        public async Task<int> CheckDuplicateSerialNo( List<string> SerialNo)
        {
           
                var duplicateCount = 0;
                foreach (var item in SerialNo)
                {
                    if (_stockSerialNoRepository.GetAll().Any(e => e.SerialNo.ToLower().Trim() == item.ToLower().Trim()))
                    {
                        duplicateCount++;
                    }
                }

                return duplicateCount;
            
        }

        public async Task ImportSerialNo(ImportSerialNoInputDto input)
        {
              foreach (var item in input.SerialNo)
                {
                    var orderWerehouseLocation = _purchaseOrderRepository.GetAll().Where(e => e.Id == input.PurchaseOrderId).Select(e => e.WarehouseLocationId).FirstOrDefault();
                    var serialNo = new StockSerialNo()
                    {
                        PurchaseOrderId = input.PurchaseOrderId,
                        ProductItemId = input.PurchaseOrderItemId,
                        WarehouseLocationId = (int)orderWerehouseLocation,
                        PalletNo = input.PalletNo,
                        SerialNo = item,
                        IsApp = true,
                        IsVerify = true,
                    };

                    _stockSerialNoRepository.Insert(serialNo);
                }
                var logMessage = $" {input.SerialNo.Count}serial numbers Imported  for Purchase Order {input.PurchaseOrderId}.";

            var quickStockLog = new QuickStockActivityLog()
            {
                Action = "Imported",
                SectionId = 1,
                ActionId = 75,
                Type = "StockOrder",


                IsApp = true,
                PurchaseOrderId = input.PurchaseOrderId,
                ActionNote = logMessage,
                CreatorUserId = AbpSession.UserId,
                };

                await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);
            
        } 
        
        public async Task<List<GetPalletNoOutputDto>> GetPalletNo(GetPalletNoDto input)
        {
            
                var palletNos = _stockSerialNoRepository.GetAll().Where(e => e.PurchaseOrderId == input.StockOrderId && e.ProductItemId == input.ProductItemId && e.IsApp == false && e.IsVerify != true).Select(e => e.PalletNo).Distinct();

                var output = from o in palletNos
                             select new GetPalletNoOutputDto
                             {
                                 PalletNo = o,
                                 IsScanned = _stockSerialNoRepository.GetAll().Count(e => e.PalletNo == o && e.IsVerify == false) == 0 ? false : true
                             };

                return output.ToList();
            
        }

        public async Task<bool> CheckVerifySerialNo( int orderId, int productItemId, string verifyType, string palletNo, string serialNo)
        {
           
                var result = false;
                if (verifyType == "PalletNo")
                {
                    result = _stockSerialNoRepository.GetAll().Any(e => e.PurchaseOrderId == orderId && e.ProductItemId == productItemId && e.PalletNo == palletNo && e.SerialNo == serialNo);
                }
                else if (verifyType == "SerialNo")
                {
                    result = _stockSerialNoRepository.GetAll().Any(e => e.PurchaseOrderId == orderId && e.ProductItemId == productItemId && e.SerialNo == serialNo);
                }

                return result;
            
        }

        public async Task VerifySerialNo( int orderId, int productItemId, string verifyType, List<string> palletNos)
        {
            var ProductName = _ProductItemRepository.GetAll().Where(e => e.Id == productItemId).Select(e => e.Name).FirstOrDefault();
            if (verifyType == "PalletNo")
                {
                    var result = _stockSerialNoRepository.GetAll().Where(e => e.PurchaseOrderId == orderId && e.ProductItemId == productItemId && palletNos.Contains(e.PalletNo));

                    foreach (var item in result)
                    {
                        item.IsVerify = true;
                        item.SerialNoStatusId = 2;

                        _stockSerialNoRepository.Update(item);
                    var SerialNoLogHistroy = new StockSerialNoLogHistory
                    {
                        StockSerialNoId = item.Id,
                        SerialNoStatusId = 2,
                        ActionNote = item.SerialNo + "SerialNo has been stocked in"
                    };
                    await _stockSerialNoLogHistoryRepository.InsertAsync(SerialNoLogHistroy);
                }
                
                var logMessage = $"{result.Count()} serial numbers Verified  for Purchase Order {ProductName}.";
                var quickStockLog = new QuickStockActivityLog()
                {
                    Action = "Verify",
                    SectionId = 1,
                    ActionId = 87,
                    Type = "StockOrder",
                    IsApp = true,
                    PurchaseOrderId = orderId,
                    ActionNote = logMessage,
                    CreatorUserId = AbpSession.UserId,
                };

                await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);
            }
                else if (verifyType == "SerialNo")
                {
                   var result = _stockSerialNoRepository.GetAll().Where(e => e.PurchaseOrderId == orderId && e.ProductItemId == productItemId);
                    foreach (var item in result)
                    {
                        item.IsVerify = true;

                        _stockSerialNoRepository.Update(item);
                    var SerialNoLogHistroy = new StockSerialNoLogHistory
                    {
                        StockSerialNoId = item.Id,
                        SerialNoStatusId = 2,
                        ActionNote = item.SerialNo + "SerialNo has been stocked in"
                    };
                }

                var logMessage = $" {result.Count()} serial numbers Verified  for Purchase Order {ProductName}.";
                var quickStockLog = new QuickStockActivityLog()
                {
                    Action = "Verify",
                    SectionId = 1,
                    ActionId = 87,
                    Type = "StockOrder",
                    IsApp = true,
                    PurchaseOrderId = orderId,
                    ActionNote = logMessage,
                    CreatorUserId = AbpSession.UserId,
                };
            }

            
        } 

        public async Task RevertAll( int orderId, int productItemId)
        {
            

                var result = _stockSerialNoRepository.GetAll().Where(e => e.PurchaseOrderId == orderId && e.ProductItemId == productItemId);
            var ProductName=_ProductItemRepository.GetAll().Where(e => e.Id == productItemId).Select(e => e.Name).FirstOrDefault();
                foreach (var item in result)
                {

                    _stockSerialNoRepository.DeleteAsync(item);
                }

            var quickStockLog = new QuickStockActivityLog();
            quickStockLog.Action = "Revert";
            quickStockLog.SectionId = 1;
            quickStockLog.ActionId = 86;
            quickStockLog.IsApp = true;
            quickStockLog.PurchaseOrderId = orderId;
            quickStockLog.ActionNote = "All Stock Serial Reverted For this " + ProductName;
            quickStockLog.Type = "Purchase Order";
            await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);
        }

        public async Task RevertBySerialNo( int orderId, int productItemId, List<string> SerialNo)
        {

            var ProductName = _ProductItemRepository.GetAll().Where(e => e.Id == productItemId).Select(e => e.Name).FirstOrDefault();
            var result = _stockSerialNoRepository.GetAll().Where(e => e.PurchaseOrderId == orderId && e.ProductItemId == productItemId && SerialNo.Contains(e.SerialNo));
                foreach (var item in result)
                {

                    _stockSerialNoRepository.DeleteAsync(item);
                }
            var quickStockLog = new QuickStockActivityLog();
            quickStockLog.Action = "Revert";
            quickStockLog.SectionId = 1;
            quickStockLog.ActionId = 86;
            quickStockLog.IsApp = true;
            quickStockLog.PurchaseOrderId = orderId;
            quickStockLog.ActionNote = SerialNo.Count+ "Stock Serial Reverted For"+ ProductName;
            quickStockLog.Type = "Purchase Order";
            await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);

        }

        public async Task AddOtherStockItemQty(OtherStockItemQtyDto input)
        {
          
                var ExistQty = _otherStockItemQtyRepository.GetAll().Where(e => e.PurchaseOrderId == input.PurchaseOrderId && e.ProductItemId == input.ProductItemId).FirstOrDefault();
                var PurchaseOrder = _purchaseOrderRepository.Get(input.PurchaseOrderId);
                var logMessage = $" {input.Quantity} Quantity Added  for Purchase Order {PurchaseOrder.OrderNo}.";
                if (ExistQty != null)
                {
                    ExistQty.Quantity = ExistQty.Quantity + input.Quantity;
                    _otherStockItemQtyRepository.Update(ExistQty);

                }
                else
                {
                    var serialNo = new OtherStockItemQty()
                    {
                        PurchaseOrderId = input.PurchaseOrderId,
                        ProductItemId = input.ProductItemId,
                        Quantity = input.Quantity

                    };

                    _otherStockItemQtyRepository.Insert(serialNo);
                }



                var quickStockLog = new QuickStockActivityLog()
                {
                    Action = "Added",
                    SectionId = 1,
                    ActionId = 89,
                    Type = "Purchase Order",
                    IsApp = true,
                    PurchaseOrderId = input.PurchaseOrderId,
                    ActionNote = "Other Qty Added",
                    CreatorUserId = AbpSession.UserId
                };

                await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);
            
        }

        public async Task RevertOtherQuntity( int orderId, int productItemId)
        {
           
                await _otherStockItemQtyRepository.DeleteAsync(e => e.PurchaseOrderId == orderId && e.ProductItemId == productItemId);
            var ProductName = _ProductItemRepository.GetAll().Where(e => e.Id == productItemId).Select(e => e.Name).FirstOrDefault();
            var quickStockLog = new QuickStockActivityLog();
            quickStockLog.Action = "Revert";
            quickStockLog.SectionId = 1;
            quickStockLog.ActionId = 88;
            quickStockLog.IsApp = true;
            quickStockLog.PurchaseOrderId = orderId;
            quickStockLog.ActionNote = " All Other Quntity Reverted For " + ProductName;
            quickStockLog.Type = "Purchase Order";
            await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);
            //var result = _otherStockItemQtyRepository.GetAll().Where(e => e.PurchaseOrderId == orderId && e.ProductItemId == productItemId);
            //foreach (var item in result)
            //{

            //    _otherStockItemQtyRepository.DeleteAsync(item);
            //}


        }

        public async Task RevertOtherQuntityByQuntity( int orderId, int productItemId, int QTY)
        {
           
                var ExistQty = _otherStockItemQtyRepository.GetAll().Where(e => e.PurchaseOrderId == orderId && e.ProductItemId == productItemId).FirstOrDefault();
            var ProductName = _ProductItemRepository.GetAll().Where(e => e.Id == productItemId).Select(e => e.Name).FirstOrDefault();
            ExistQty.Quantity = ExistQty.Quantity - QTY;

                if(ExistQty.Quantity > 0)
                {
                    _otherStockItemQtyRepository.Update(ExistQty);

                var quickStockLog = new QuickStockActivityLog();
                quickStockLog.Action = "Updated";
                quickStockLog.SectionId = 1;
                quickStockLog.IsApp = true;
                quickStockLog.ActionId = 69;
                quickStockLog.PurchaseOrderId = orderId;
                quickStockLog.ActionNote = "Other StockItem Qty Updated for :" + ProductName;
                quickStockLog.Type = "Purchase Order";
                await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);

                }
                else
                {
                    _otherStockItemQtyRepository.Delete(ExistQty);

                var quickStockLog = new QuickStockActivityLog();
                quickStockLog.Action = "revert";
                quickStockLog.SectionId = 1;
                quickStockLog.ActionId = 88;
                quickStockLog.IsApp = true;
                quickStockLog.PurchaseOrderId = orderId;
                quickStockLog.ActionNote = QTY +"Other StockItem Qty Reverted  for :" + ProductName;
                quickStockLog.Type = "Purchase Order";
                await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);
            }
            
        }

        public async Task ImageUpload(List<byte[]> fileBytes, int typeId, int id,int purchaseOrderId)
        {
            var MainFolder = ApplicationSettingConsts.DocumentPath; //From Const
            MainFolder = MainFolder + "\\Documents\\AppDocument";
            var i = 1;

            foreach (var file in fileBytes)
            {
                var filename = DateTime.Now.Ticks + "_" + i + "_Document.png";

                await CommonDocumnet(file, filename);

                i++;
                var appDoc = new AppDocument();
                appDoc.DocumentTypeId = typeId;
                appDoc.DocumentId = id;
                appDoc.DocumentName = filename;
                appDoc.DocumentPath = "\\Documents\\AppDocument";
                await _appDocumentRepository.InsertAsync(appDoc);

            }

            var quickStockLog = new QuickStockActivityLog();
            quickStockLog.Action = "Upload";
            quickStockLog.SectionId = 1;
            quickStockLog.ActionId = 84;
            quickStockLog.IsApp = true;
            quickStockLog.PurchaseOrderId = purchaseOrderId;
            quickStockLog.ActionNote = "Document Uploaded for" + typeId;
            quickStockLog.Type = "Purchase Order";
            await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);


        }

        public async Task<bool> CheckSerialNoIsExist(string serialNo)
        {
            var result = _stockSerialNoRepository.GetAll().Any(e => e.SerialNo == serialNo);

            return result;
        }

        public async Task UpdateStatusFaultyOrDefectSerialNo(List<GetFaultyOrDefectSerialNo> input)
        {
            var MainFolder = ApplicationSettingConsts.DocumentPath; //From Const
            MainFolder = MainFolder + "\\Documents\\AppDocument";
            foreach (var inputItem in input)
            {
                var result = _stockSerialNoRepository.GetAll().Where(e => e.SerialNo == inputItem.SerialNo).FirstOrDefault();

                var i = 1;

                foreach (var file in inputItem.fileBytes)
                {
                    var filename = DateTime.Now.Ticks + "_" + i + "_Document.png";

                    await CommonDocumnet(file, filename);

                    i++;
                    var appDoc = new AppDocument();
                    appDoc.DocumentTypeId = 5;
                    appDoc.DocumentId = result.Id;
                    appDoc.DocumentName = filename;
                    appDoc.DocumentPath = "\\Documents\\AppDocument";
                    await _appDocumentRepository.InsertAsync(appDoc);

                }

                result.SerialNoStatusId = inputItem.StatusId;

                await _stockSerialNoRepository.UpdateAsync(result);

                string actionNote = result.SerialNo + (inputItem.StatusId == 7 ? " Serial No Broken" : " Serial No Faulty");


                var SerialNoLogHistroy = new StockSerialNoLogHistory
                {
                    StockSerialNoId = result.Id,
                    SerialNoStatusId = inputItem.StatusId,
                    ActionNote = actionNote,
                    IsApp = true
                };
                await _stockSerialNoLogHistoryRepository.InsertAsync(SerialNoLogHistroy);
            }
        }

        protected virtual async Task CommonDocumnet(byte[] fileBytes, string filename, string FolderName = "")
        {

            var MainFolder = ApplicationSettingConsts.DocumentPath; 
            MainFolder = MainFolder + "\\Documents\\AppDocument" +(!string.IsNullOrEmpty(FolderName) ? "\\" + FolderName : "");

            var FolderPath = MainFolder  + "\\" + filename;

            if (System.IO.Directory.Exists(MainFolder))
            {
                using (MemoryStream mStream = new MemoryStream(fileBytes))
                {
                    System.IO.File.WriteAllBytes(string.Format("{0}", FolderPath), fileBytes);
                }
            }
            else
            {
                System.IO.Directory.CreateDirectory(MainFolder);
                using (MemoryStream mStream = new MemoryStream(fileBytes))
                {
                    System.IO.File.WriteAllBytes(string.Format("{0}", FolderPath), fileBytes);
                }
            }             

            
        }

        public async Task SubmitOrder(GetSubmitOrderDto GetSubmitOrderDto)
        {
            string filenameForUser = null;
            string filenameforSignature = null;
           
            var purchaseOrder = await _purchaseOrderRepository.GetAsync(GetSubmitOrderDto.orderId);
           
            // Save user image
            if (GetSubmitOrderDto.fileBytesForuser_image != null && GetSubmitOrderDto.fileBytesForuser_image.Length > 0)
            {
                filenameForUser = DateTime.Now.Ticks + "_UserImage.png";

                await CommonDocumnet(GetSubmitOrderDto.fileBytesForuser_image, filenameForUser, "UserImage");

            }

            // Save Signature image
            if (GetSubmitOrderDto.fileBytesForsignature_image != null && GetSubmitOrderDto.fileBytesForsignature_image.Length > 0)
            {
                filenameforSignature = DateTime.Now.Ticks + "_SignatureImage.png";

                await CommonDocumnet(GetSubmitOrderDto.fileBytesForsignature_image, filenameforSignature, "SignatureImage");
            }

            purchaseOrder.user_image = "\\Documents\\AppDocument\\UserImage\\" + filenameForUser;
            purchaseOrder.signature_image = "\\Documents\\AppDocument\\SignatureImage\\" + filenameforSignature;
            purchaseOrder.IsSubmitDate = DateTime.UtcNow;
            purchaseOrder.Latitude = GetSubmitOrderDto.latitude;
            purchaseOrder.Longitude = GetSubmitOrderDto.longitude;
            await _purchaseOrderRepository.UpdateAsync(purchaseOrder);

            var quickStockLog = new QuickStockActivityLog();
            quickStockLog.Action = "Updated";
            quickStockLog.SectionId = 1;
            quickStockLog.IsApp = true;
            quickStockLog.ActionId = 69;
            quickStockLog.PurchaseOrderId = purchaseOrder.Id;
            quickStockLog.ActionNote = "Purchase Order Submitted";
            quickStockLog.Type = "Purchase Order";
            await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);

        }

        public async Task<GetAllSerialNoHistoryAppDto> GetSerialNoHistory(string serialNo)
        {
            var serialNoHistory = _stockSerialNoLogHistoryRepository
    .GetAll()
    .Include(e => e.StockSerialNoFk)
    .Include(e => e.SerialNoStatusFk)
    .Include(e => e.StockSerialNoFk.PurchaseOrderFk.PaymentTypeFk)
    .Include(e => e.StockSerialNoFk.ProductItemFk)
    .Include(e => e.StockSerialNoFk.ProductItemFk.ProductTypeFk)
    .Include(e => e.StockSerialNoFk.WarehouseLocationFk)
    .AsNoTracking() 
    .Where(e => e.StockSerialNoFk.SerialNo == serialNo).Select(e=> new {e.Id,e.ActionNote, e.CreationTime, ModelNo= e.StockSerialNoFk.ProductItemFk.Model,Location= e.StockSerialNoFk.WarehouseLocationFk.location, ModuleName =e.SerialNoStatusFk.Status ,productName= e.StockSerialNoFk.ProductItemFk.Name,CatogoryName=e.StockSerialNoFk.ProductItemFk.ProductTypeFk.Name })
    .ToList();
           
            var firstHistoryItem = serialNoHistory.FirstOrDefault();
            var result = new GetAllSerialNoHistoryAppDto
            {
                ProductName = firstHistoryItem.productName,
                Catogory = firstHistoryItem.CatogoryName,
                SerialNo = serialNo,
                ModelNo = firstHistoryItem.ModelNo,
                SerialNoHistory = serialNoHistory.Select(o => new GetSerialNoHistoryAppDto
                {
                    Id = o.Id,
                    Section = o.ModuleName,
                    ModuleName = o.ModuleName,
                    Location = o.Location,
                    DeductDate = o.CreationTime.ToString("dd-MMM,yyyy"),
                    Message = o.ActionNote,
                }).ToList()
            };

            return result;
        }

        public async Task<List<GetLiveStockItemDto>> GetLiveStockByProductName(string ProductName)
        {
            var stockSerialNo = _stockSerialNoRepository.GetAll()
            .Include(e => e.ProductItemFk)
                 .Where(poi => (poi.ProductItemFk.Name.Contains(ProductName) || poi.ProductItemFk.Model.Contains(ProductName) || poi.SerialNo.Contains(ProductName)) && poi.SerialNoStatusId == 2).Select(e => new { e.Id,  ModelNo = e.ProductItemFk.Model, productName = e.ProductItemFk.Name, CatogoryName = e.ProductItemFk.ProductTypeFk.Name })
        .ToList();

            var output = stockSerialNo
                .GroupBy(o => new
                {
                    ProductItemName = o.productName,  
                    ProductTypeName = o.CatogoryName,  
                    ModelNo = o.ModelNo
                })
                .Select(g => new GetLiveStockItemDto
                {
                    ProductItem = g.Key.ProductItemName,
                    Category = g.Key.ProductTypeName,
                    ModelNo = g.Key.ModelNo,
                    Quantity = g.Count() 
                })
                .ToList();

            return output;
        }

        public async Task<List<GetOtherStockOrderItemDto>> GetOtherStockByProductName(string ProductName)
        {
           
            var stockItem =_ProductItemRepository.GetAll().Where(poi => poi.Name.Contains(ProductName)).Select(e => new { e.Id, ModelNo = e.Model,  productName = e.Name, CatogoryName = e.ProductTypeFk.Name ,e.IsScanned})
        .ToList();
            var outputstockItem = (from o in stockItem
                                    select new GetOtherStockOrderItemDto()
                                    {
                                       Id = o.Id,
                                       ProductItemId=o.Id,
                                       ProductItem=o.productName,
                                       Category = o.CatogoryName,
                                       ModelNo=o.ModelNo,
                                       IsScanned = o.IsScanned

                                    }).ToList();

            return outputstockItem;
        }

        public async Task SubmitOterStockItem(GetSubmitOtherOrderSerialNosDto GetSubmitOrderDto)
        {

            foreach (var serial in GetSubmitOrderDto.SerialNo)
            {
                var stockSerialNo = new StockSerialNo
                {
                    PurchaseOrderId = GetSubmitOrderDto.PurchaseOrderId,
                    ProductItemId = GetSubmitOrderDto.ProductItemId,
                    WarehouseLocationId = GetSubmitOrderDto.WarehouseLocationId,
                    PalletNo = GetSubmitOrderDto.PalletNo,
                    SerialNo = serial, 
                    IsApp = GetSubmitOrderDto.IsApp,
                    
                    SerialNoStatusId = GetSubmitOrderDto.SerialNoStatusId
                };

                await _stockSerialNoRepository.InsertAsync(stockSerialNo);
            }




        }

        public async Task<List<GetAllWarehouseLocationDto>> GetAllWarehouseLocation()
        {
            var warehouseLocations = _warehouselocationRepository.GetAll().ToList();
            return warehouseLocations.Select(e => new GetAllWarehouseLocationDto
            {
                LocationId = e.Id,
                LocationName = e.location
            }).ToList();

        }
    }
}
