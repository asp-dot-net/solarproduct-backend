﻿using Abp.Domain.Repositories;
using Abp.Timing.Timezone;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheSolarProduct.Jobs;
using TheSolarProduct.QuickStockApp.Installer.Dto;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using System;

namespace TheSolarProduct.QuickStockApp.Installer

{
    public class InstallerAppService : TheSolarProductAppServiceBase, IInstallerAppService
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<Job> _jobRepository;

        public InstallerAppService(ITimeZoneConverter timeZoneConverter, IRepository<Job> jobRepository)
        {
            _timeZoneConverter = timeZoneConverter;
            _jobRepository = jobRepository;
        }

        public async Task<List<GetInstallationForViewDto>> GetInstallation(InstalllerInstallationInput input)
        {
            using (UnitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var installationDate = (_timeZoneConverter.Convert(input.InstallationDate, input.TenantId));

                var job = _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.InstallationDate.Value.Date == installationDate.Value.Date && e.InstallerId == input.UserId);
                
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

                var installationList = from j in job
                                       select new GetInstallationForViewDto()
                                       {
                                           Id = j.Id,
                                           JobNumber = j.JobNumber,
                                           CustomerName = j.LeadFk.CompanyName,
                                           InstallationTime = !string.IsNullOrEmpty(j.InstallationTime) ? j.InstallationTime : "AM",
                                           Address = !string.IsNullOrEmpty(j.Address) ? textInfo.ToTitleCase(Convert.ToString(j.Address.ToLower()).Trim()) : "",
                                           Address2 = textInfo.ToTitleCase((j.Suburb + ", " + j.State + " - " + j.PostalCode).ToLower())
                                       };

                return new List<GetInstallationForViewDto>(await installationList.ToListAsync());
            }
        }     
        
        public async Task<List<GetInstallationForViewDto>> SaveInstallation(InstalllerInstallationInput input)
        {
            using (UnitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var installationDate = (_timeZoneConverter.Convert(input.InstallationDate, input.TenantId));

                var job = _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.InstallationDate.Value.Date == installationDate.Value.Date && e.InstallerId == input.UserId);
                
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

                var installationList = from j in job
                                       select new GetInstallationForViewDto()
                                       {
                                           Id = j.Id,
                                           JobNumber = j.JobNumber,
                                           CustomerName = j.LeadFk.CompanyName,
                                           InstallationTime = !string.IsNullOrEmpty(j.InstallationTime) ? j.InstallationTime : "AM",
                                           Address = !string.IsNullOrEmpty(j.Address) ? textInfo.ToTitleCase(Convert.ToString(j.Address.ToLower()).Trim()) : "",
                                           Address2 = textInfo.ToTitleCase((j.Suburb + ", " + j.State + " - " + j.PostalCode).ToLower())
                                       };

                return new List<GetInstallationForViewDto>(await installationList.ToListAsync());
            }
        }
    }
}
