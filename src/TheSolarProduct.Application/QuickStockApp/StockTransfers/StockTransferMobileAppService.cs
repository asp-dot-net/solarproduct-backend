﻿using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Jobs;
using TheSolarProduct.QuickStockApp.StockOrder.Dto;
using TheSolarProduct.QuickStockApp.StockTransfer;
using TheSolarProduct.QuickStocks;
using TheSolarProduct.StockOrders;
using TheSolarProduct.StockTransfers.Exporting;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.TransportationCosts;
using Abp.Application.Services.Dto;
using System.Data;
using System.Linq;
using TheSolarProduct.StockOrder.Dtos;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.QuickStockApp.StockTransfer.Dtos;
using TheSolarProduct.StockTransfers.Dtos;
using System.IO;
using TheSolarProduct.ApplicationSettings;


namespace TheSolarProduct.QuickStockApp.StockTransfers
{
  
    public class StockTransferMobileAppService : TheSolarProductAppServiceBase, IStockTransferMobileAppService
    {
        private readonly IStockTransferExcelExporter _stockTransferExcelExporter;
        private readonly IRepository<StockOrders.StockTransfer> _stockTransferRepository;
        private readonly IRepository<StockTransferProductItem> _stockTransferProductItemRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<StockSerialNo> _StockSerialNoRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<QuickStockActivityLog> _quickStockActivityLogRepository;
        private readonly IRepository<QuickStockActivityLogHistory> _purchaseOrderHistoryRepository;
        private readonly IRepository<TransportCompany> _transportCompanyRepository;
        private readonly IRepository<Warehouselocation> _warehouselocationRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<ProductItem> _productItemRepository;
        private readonly IRepository<UserWarehouseLocation> _userWarehouseLocationRepository;
        private readonly IRepository<AppDocument> _appDocumentRepository;
        private readonly IRepository<StockSerialNo> _stockSerialNoRepository;
        private readonly IRepository<StockSerialNoLogHistory> _stockSerialNoLogHistoryRepository;
        public StockTransferMobileAppService(
            ITimeZoneConverter timeZoneConverter,
              IRepository<StockOrders.StockTransfer> stockTransferRepository,
              IRepository<StockTransferProductItem> stockTransferProductItemRepository,
              IStockTransferExcelExporter IStockTransferExcelExporter,
              IRepository<StockSerialNo> StockSerialNoRepository,
              IRepository<UserRole, long> userRoleRepository,
              IRepository<Role> roleRepository,
              IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
              IRepository<QuickStockActivityLog> quickStockActivityLogRepository,
              IRepository<QuickStockActivityLogHistory> purchaseOrderHistoryRepository,
              IRepository<TransportCompany> transportCompanyRepository,
              IRepository<Warehouselocation> warehouselocationRepository,
              IRepository<User, long> userRepository,
              IRepository<ProductItem> productItemRepository,
              IRepository<UserWarehouseLocation> userWarehouseLocationRepository,
              IRepository<AppDocument> appDocumentRepository,
              IRepository<StockSerialNo> stockSerialNoRepository,
              IRepository<StockSerialNoLogHistory> stockSerialNoLogHistoryRepository
)
        {
            _stockTransferRepository = stockTransferRepository;
            _stockTransferProductItemRepository = stockTransferProductItemRepository;
            _timeZoneConverter = timeZoneConverter;
            _stockTransferExcelExporter = IStockTransferExcelExporter;
            _StockSerialNoRepository = StockSerialNoRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
            _quickStockActivityLogRepository = quickStockActivityLogRepository;
            _purchaseOrderHistoryRepository = purchaseOrderHistoryRepository;
            _transportCompanyRepository = transportCompanyRepository;
            _warehouselocationRepository = warehouselocationRepository;
            _userRepository = userRepository;
            _productItemRepository = productItemRepository;
            _userWarehouseLocationRepository=userWarehouseLocationRepository;
            _appDocumentRepository = appDocumentRepository;
            _stockSerialNoRepository = StockSerialNoRepository;
            _stockSerialNoLogHistoryRepository=stockSerialNoLogHistoryRepository;
        }

        public async Task<List<GetAlllStockTransferInOutDto>> GetAllStockTransferOut(GetAllStockOrderInputDto input)
        {
            
                var userLocation = _userWarehouseLocationRepository.GetAll().Where(e => e.UserId == AbpSession.UserId).Select(e => e.WarehouseLocationId).ToList();

                var stockTransfer = _stockTransferRepository.GetAll().Include(e => e.WarehouseLocationFromFK)
                    .Where(e => userLocation.Any(l => l == e.WarehouseLocationFromId)  && e.Received==0 && e.Transferd==0);
                var User_List = _userRepository.GetAll().AsNoTracking();
                var outputstockOrder = from o in stockTransfer
                                       select new GetAlllStockTransferInOutDto()
                                       {
                                           Id = o.Id,
                                           TransferNo = o.TransferNo,
                                           TrackingNumber = o.TrackingNumber,
                                           TransferByDate = o.TransferByDate.Date,
                                           CreationDate = o.CreationTime.ToString("dd-MM-yyyy"),
                                           TransferdBy = o.Transferd,
                                           RecivedBy = o.Received,
                                           RecivedByDate = o.ReceivedByDate.Date,
                                           WarehouseLocation =o.WarehouseLocationFromFK.location,
                                           Quantity =_stockTransferProductItemRepository.GetAll().Where(e => e.StockTransferId == o.Id).Sum(e => e.Quantity),
                                           StockTransferItem = _stockTransferProductItemRepository.GetAll().Where(e => e.StockTransferId == o.Id).Select(e => new StockTransferItemNameDto { StockItem = e.ProductItemFk.Name, }).ToList()
                                       };
               


                var totalCount = outputstockOrder.Count();

                return outputstockOrder.OrderByDescending(e => e.TransferNo).ToList();



            
        }
        public async Task<List<GetAlllStockTransferInOutDto>> GetAllStockTransferIn(GetAllStockOrderInputDto input)
        {
           
                var userLocation = _userWarehouseLocationRepository.GetAll().Where(e => e.UserId == AbpSession.UserId).Select(e => e.WarehouseLocationId).ToList();
                var User_List = _userRepository.GetAll().AsNoTracking();
                var stockTransfer = _stockTransferRepository.GetAll().Include(e => e.WarehouseLocationToFK)
                    .Where(e => userLocation.Any(l => l == e.WarehouseLocationToId) && e.Received == 0 && e.Transferd !=0);

                var outputstockOrder = from o in stockTransfer
                                       select new GetAlllStockTransferInOutDto()
                                       {
                                           Id = o.Id,
                                           TransferNo = o.TransferNo,
                                           TrackingNumber = o.TrackingNumber,
                                           TransferByDate = o.TransferByDate.Date,
                                           CreationDate = o.CreationTime.ToString("dd-MM-yyyy"),
                                           TransferdBy = o.Transferd,
                                           RecivedBy=o.Received,
                                           RecivedByDate=o.ReceivedByDate.Date,
                                           WarehouseLocation = o.WarehouseLocationToFK.location,
                                           Quantity = _stockTransferProductItemRepository.GetAll().Where(e => e.StockTransferId == o.Id).Sum(e => e.Quantity),
                                           StockTransferItem = _stockTransferProductItemRepository.GetAll().Where(e => e.StockTransferId == o.Id).Select(e => new StockTransferItemNameDto { StockItem = e.ProductItemFk.Name, }).ToList()

                                       };



                var totalCount = outputstockOrder.Count();

                return outputstockOrder.OrderByDescending(e => e.TransferNo).ToList();



            
        }

        public async Task<GetAllStockTransferDto> GetStockTransferById(int id, string type)
        {
            
                var stockTransfer = await _stockTransferRepository.GetAll()
                .Include(e => e.TransportCompanyFk)
                .Include(e => e.WarehouseLocationFromFK)
                .Include(e => e.WarehouseLocationToFK)
                .FirstOrDefaultAsync(e => e.Id == id);
                var stockTransferItem = await _stockTransferProductItemRepository.GetAll().Include(e => e.ProductItemFk).Include(poi => poi.ProductItemFk.ProductTypeFk).Where(e => e.StockTransferId == id).ToListAsync();

                var stockTransferDto = new GetAllStockTransferDto
                {
                    Id = stockTransfer.Id,
                    TransferNo = stockTransfer.TransferNo,
                    WarehouseLocationFromId = stockTransfer.WarehouseLocationFromId,
                      
                    WarehouseLocationToId=stockTransfer.WarehouseLocationToId,


                };
                stockTransferDto.StockTransferItem = new List<StockTransferItemDto>();
                foreach (var item in stockTransferItem)
                {
                    var stockTransferItems = new StockTransferItemDto();
                    stockTransferItems.Id = item.Id;

                    stockTransferItems.StockCategory = item.ProductItemFk.ProductTypeFk.Name;
                    stockTransferItems.StockItemId= item.ProductItemId;
                    stockTransferItems.StockItem = item.ProductItemFk.Name;
                    stockTransferItems.Quantity = item.Quantity;
                    stockTransferItems.PendingQty = 10;
                    stockTransferItems.ScanQty = 5;
                    if (type== "in")
                    {
                        stockTransferItems.AvailableQty = _StockSerialNoRepository.GetAll().Where(e => e.WarehouseLocationId == stockTransfer.WarehouseLocationToId && e.ProductItemId == item.ProductItemId).Count();
                    }
                    else if (type == "out")
                    {
                        stockTransferItems.AvailableQty = _StockSerialNoRepository.GetAll().Where(e => e.WarehouseLocationId == stockTransfer.WarehouseLocationFromId && e.ProductItemId == item.ProductItemId).Count();
                    }
                    
                    stockTransferItems.ModalNo = item.ProductItemFk.Model;
                    stockTransferDto.StockTransferItem.Add(stockTransferItems);
                }   
                return stockTransferDto;
            
        }
        public async Task<bool> CheckVerifySerialNo(  int productItemId, string serialNo,int WarehouseLocationFromId,string type,int WarehouseLocationToId)
        {
           
                var result = false;
                
                   
                if (type == "in")
                {
                    result = _StockSerialNoRepository.GetAll().Any(e => e.ProductItemId == productItemId && e.WarehouseLocationId == WarehouseLocationToId && e.SerialNo == serialNo);
                }
                else if (type == "out")
                {
                    result = _StockSerialNoRepository.GetAll().Any(e => e.ProductItemId == productItemId && e.WarehouseLocationId == WarehouseLocationFromId && e.SerialNo == serialNo);
                }

                return result;
            
        }

        public async Task<List<GetAllSerialNo>> GetAllSerialNo( int productItemId, string palletNo, int WarehouseLocationFromId, string type, int WarehouseLocationToId)
        {
           
                List<string> palletNos = new List<string>();
                if (type == "in")
                {
                    palletNos = _StockSerialNoRepository.GetAll()
                        .Where(e => e.ProductItemId == productItemId && e.WarehouseLocationId == WarehouseLocationToId && e.PalletNo == palletNo)
                        .Select(e => e.SerialNo)
                        .ToList();
                }
                else if (type == "out")
                {
                    palletNos = _StockSerialNoRepository.GetAll()
                        .Where(e => e.ProductItemId == productItemId && e.WarehouseLocationId == WarehouseLocationFromId && e.PalletNo == palletNo)
                        .Select(e => e.SerialNo)
                        .ToList();
                }
                var output = palletNos.Select(o => new GetAllSerialNo
                {
                    SerialNo = o
                });


                return output.ToList();
            
        }

        public async Task ImageUpload(List<byte[]> fileBytes, int typeId, int TransferId)
        {
            var MainFolder = ApplicationSettingConsts.DocumentPath; 
            MainFolder = MainFolder + "\\Documents\\AppDocument";

            var i = 1;
            foreach (var file in fileBytes)
            {
                var filename = DateTime.Now.Ticks + "_" + i + "_Document.png";
                var FolderPath = MainFolder + "\\" + filename;

                if (System.IO.Directory.Exists(MainFolder))
                {
                    using (MemoryStream mStream = new MemoryStream(file))
                    {
                        System.IO.File.WriteAllBytes(string.Format("{0}", FolderPath), file);
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(MainFolder);
                    using (MemoryStream mStream = new MemoryStream(file))
                    {
                        System.IO.File.WriteAllBytes(string.Format("{0}", FolderPath), file);
                    }
                }
                i++;
                var appDoc = new AppDocument();
                appDoc.DocumentTypeId = typeId;
                appDoc.DocumentId = TransferId;
                appDoc.DocumentName = filename;
                appDoc.DocumentPath = MainFolder;
                await _appDocumentRepository.InsertAsync(appDoc);

                var quickStockLog = new QuickStockActivityLog();
                quickStockLog.Action = "Upload";
                quickStockLog.SectionId = 1;
                quickStockLog.ActionId = 90;
                quickStockLog.IsApp = true;
                quickStockLog.StockTransferId = TransferId;
                quickStockLog.ActionNote = "Image Uploaded for" + TransferId;
                quickStockLog.Type = "StockTransfer";
                await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);


            }


        }
        public async Task ReadyTransferSerialNo(int transferId,  List<string> SerialNo)
        {

            var result = _stockSerialNoRepository.GetAll()
                                   .Where(e => SerialNo.Contains(e.SerialNo));


            foreach (var item in result)
            {
               
                item.SerialNoStatusId = 3;

                _stockSerialNoRepository.Update(item);
                var SerialNoLogHistroy = new StockSerialNoLogHistory
                {
                    StockSerialNoId = transferId,
                    SerialNoStatusId = 3,
                    ActionNote = item.SerialNo + " SerialNo Ready To Tranfer"
                };
                await _stockSerialNoLogHistoryRepository.InsertAsync(SerialNoLogHistroy);
            }


        }

        public async Task TranferRevert(int transferId, List<string> SerialNo)
        {

            var result = _stockSerialNoRepository.GetAll()
                                   .Where(e => SerialNo.Contains(e.SerialNo));


            foreach (var item in result)
            {

                item.SerialNoStatusId = 5;

                _stockSerialNoRepository.Update(item);
                var SerialNoLogHistroy = new StockSerialNoLogHistory
                {
                    StockSerialNoId = transferId,
                    SerialNoStatusId = 5,
                    ActionNote = item.SerialNo + " SerialNo Reverted"
                };
                await _stockSerialNoLogHistoryRepository.InsertAsync(SerialNoLogHistroy);
            }


        }

        public async Task TranferOut(int transferId, List<string> SerialNo)
        {

            var result = _stockSerialNoRepository.GetAll()
                                   .Where(e => SerialNo.Contains(e.SerialNo));


            foreach (var item in result)
            {

                item.SerialNoStatusId = 4;

                _stockSerialNoRepository.Update(item);
                var SerialNoLogHistroy = new StockSerialNoLogHistory
                {
                    StockSerialNoId = transferId,
                    SerialNoStatusId = 4,
                    ActionNote = item.SerialNo + " SerialNo Out"
                };
                await _stockSerialNoLogHistoryRepository.InsertAsync(SerialNoLogHistroy);
            }


        }

    }
}
