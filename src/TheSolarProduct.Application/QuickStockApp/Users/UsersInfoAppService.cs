﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.QuickStockApp.Installer.Dto;
using TheSolarProduct.QuickStockApp.Users.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.QuickStockApp.Users
{
    public class UsersInfoAppService : TheSolarProductAppServiceBase, IUsersInfoAppService
    {
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserPermissionSetting, long> _userPermissionRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly UserManager _userManager;
        private readonly IRepository<UserWarehouseLocation, int> _userWarehouseLocationRepository;

        public UsersInfoAppService(
            UserManager userManager,
            IBinaryObjectManager binaryObjectManager,
            IRepository<User, long> userRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<UserPermissionSetting, long> userPermissionRepository,
            ITimeZoneConverter timeZoneConverter,
            IRepository<UserWarehouseLocation, int> userWarehouseLocationRepository
            )
        {
            _userManager = userManager;
            _binaryObjectManager = binaryObjectManager;
            _userRepository = userRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _userPermissionRepository = userPermissionRepository;
            _timeZoneConverter = timeZoneConverter;
            _userWarehouseLocationRepository = userWarehouseLocationRepository;
        }

        public async Task ChangeUserPassword(ChangeUserPasswordInput input)
        {
            using (UnitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var user = await GetCurrentUserAsync();
                if (await _userManager.CheckPasswordAsync(user, input.CurrentPassword))
                {
                    CheckErrors(await _userManager.ChangePasswordAsync(user, input.NewPassword));
                }
                else
                {
                    CheckErrors(IdentityResult.Failed(new IdentityError
                    {
                        Description = "Incorrect password."
                    }));
                }
            }

        }

        public async Task<UserInfo> GetUserInfo(UserInfoInput input)
        {
            using (UnitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var userDetails = await _userRepository.GetAsync(input.UserId);

                var output = new UserInfo();
                if (userDetails != null)
                {
                    var Date = (_timeZoneConverter.Convert(DateTime.UtcNow, input.TenantId)).Value.Date;

                    output.Id = userDetails.Id;
                    output.Name = userDetails.Name;
                    output.FullName = userDetails.FullName.ToString();

                    IList<string> RoleName = await _userManager.GetRolesAsync(userDetails);
                    output.IsInstaller = RoleName.Any(e => e == "Installer");

                    var ProfilePictureId = userDetails.ProfilePictureId != null ? userDetails.ProfilePictureId.Value : Guid.Empty;
                    var file = await _binaryObjectManager.GetOrNullAsync(ProfilePictureId);
                    if (file != null)
                    {
                        output.ProfilePicture = "data:image/jpeg;base64," + Convert.ToBase64String(file.Bytes);
                    }

                    var grantedPermissions = _userPermissionRepository.GetAll().Where(p => p.IsGranted == true && p.UserId == userDetails.Id && p.Name.StartsWith("QuickStockApp"));
                    if (grantedPermissions != null)
                    {
                        output.GrantedPermissions = grantedPermissions.Select(p => p.Name).ToList();
                    }

                    var userWarehouseLocation = _userWarehouseLocationRepository.GetAll().Include(e => e.WarehouseLocationFk).Where(e => e.UserId == userDetails.Id);
                    output.UserWarehouseLocations = (from uwl in userWarehouseLocation
                                                     select new UserWarehouseLocationDto()
                                                     {
                                                         Id = uwl.WarehouseLocationId,
                                                         State = uwl.WarehouseLocationFk.state,
                                                         WarehouseLocation = uwl.WarehouseLocationFk.location,
                                                     }).ToList();
                }

                return output;
            }
        }

        //public async Task SaveInfo(UserInfoInput input)
        //{
        //    using (UnitOfWorkManager.Current.SetTenantId(input.TenantId))
        //    {
        //        var userDetails = await _userRepository.GetAsync(input.UserId);

        //        var output = new UserInfo();
        //        if (userDetails != null)
        //        {
        //            output.Id = userDetails.Id;
        //            output.Name = userDetails.Name;
        //            output.FullName = userDetails.FullName.ToString();
        //        }
        //    }
        //}

        public async Task<List<UserInfo>> SaveUsersList(UserInfoInput input)
        {

            var logInUsers = AbpSession.UserId;

            var userDetails = _userRepository.GetAll();

            using (UnitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
              //var userDetails = _userRepository.GetAll();

                var userList = from j in userDetails
                               select new UserInfo()
                              {
                                  Id = j.Id,
                                  Name = j.Name,
                                   FullName = j.FullName,

                              };

                var output = userList.ToList();

              return output;
           }
        }

    }


}
