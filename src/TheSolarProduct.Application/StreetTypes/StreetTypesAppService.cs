﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.StreetTypes.Exporting;
using TheSolarProduct.StreetTypes.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using Abp.EntityFrameworkCore;
using TheSolarProduct.CheckActives;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.StreetTypes
{
	[AbpAuthorize(AppPermissions.Pages_StreetTypes)]
    public class StreetTypesAppService : TheSolarProductAppServiceBase, IStreetTypesAppService
    {
		 private readonly IRepository<StreetType> _streetTypeRepository;
		 private readonly IStreetTypesExcelExporter _streetTypesExcelExporter;
         private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        public StreetTypesAppService(IRepository<StreetType> streetTypeRepository, 
            IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
            IStreetTypesExcelExporter streetTypesExcelExporter,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            ) 
		  {
			_streetTypeRepository = streetTypeRepository;
			_streetTypesExcelExporter = streetTypesExcelExporter;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
			
		  }

		 public async Task<PagedResultDto<GetStreetTypeForViewDto>> GetAll(GetAllStreetTypesInput input)
         {
			
			var filteredStreetTypes = _streetTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter) || e.Code.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter),  e => e.Code == input.CodeFilter);

			var pagedAndFilteredStreetTypes = filteredStreetTypes
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

			var streetTypes = from o in pagedAndFilteredStreetTypes
                         select new GetStreetTypeForViewDto() {
							StreetType = new StreetTypeDto
							{
                                Name = o.Name,
                                Code = o.Code,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						};

            var totalCount = await filteredStreetTypes.CountAsync();

            return new PagedResultDto<GetStreetTypeForViewDto>(
                totalCount,
                await streetTypes.ToListAsync()
            );
         }
		 
		 public async Task<GetStreetTypeForViewDto> GetStreetTypeForView(int id)
         {
            var streetType = await _streetTypeRepository.GetAsync(id);

            var output = new GetStreetTypeForViewDto { StreetType = ObjectMapper.Map<StreetTypeDto>(streetType) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_StreetTypes_Edit)]
		 public async Task<GetStreetTypeForEditOutput> GetStreetTypeForEdit(EntityDto input)
         {
            var streetType = await _streetTypeRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetStreetTypeForEditOutput {StreetType = ObjectMapper.Map<CreateOrEditStreetTypeDto>(streetType)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditStreetTypeDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_StreetTypes_Create)]
		 protected virtual async Task Create(CreateOrEditStreetTypeDto input)
         {
            var streetType = ObjectMapper.Map<StreetType>(input);
			streetType.Name = streetType.Name.ToUpper();
			streetType.Code = streetType.Code.ToUpper();

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 73;
            dataVaultLog.ActionNote = "StreetType Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _streetTypeRepository.InsertAsync(streetType);
         }

		 [AbpAuthorize(AppPermissions.Pages_StreetTypes_Edit)]
		 protected virtual async Task Update(CreateOrEditStreetTypeDto input)
         {
            var streetType = await _streetTypeRepository.FirstOrDefaultAsync((int)input.Id);
			streetType.Name = streetType.Name.ToUpper();
			streetType.Code = streetType.Code.ToUpper();

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 73;
            dataVaultLog.ActionNote = "StreetType Updated : " + streetType.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != streetType.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = streetType.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Code != streetType.Code)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Code";
                history.PrevValue = streetType.Code.ToString();
                history.CurValue = input.Code.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != streetType.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = streetType.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, streetType);
         }

		 [AbpAuthorize(AppPermissions.Pages_StreetTypes_Delete)]
         public async Task Delete(EntityDto input)
         {
            var Name = _streetTypeRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 73;
            dataVaultLog.ActionNote = "StreetType Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _streetTypeRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetStreetTypesToExcel(GetAllStreetTypesForExcelInput input)
         {
			
			var filteredStreetTypes = _streetTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter) || e.Code.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter),  e => e.Code == input.CodeFilter);

			var query = (from o in filteredStreetTypes
                         select new GetStreetTypeForViewDto() { 
							StreetType = new StreetTypeDto
							{
                                Name = o.Name,
                                Code = o.Code,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						 });


            var streetTypeListDtos = await query.ToListAsync();

            return _streetTypesExcelExporter.ExportToFile(streetTypeListDtos);
         }


    }
}