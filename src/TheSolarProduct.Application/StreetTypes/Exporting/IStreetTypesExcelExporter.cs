﻿using System.Collections.Generic;
using TheSolarProduct.StreetTypes.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.StreetTypes.Exporting
{
    public interface IStreetTypesExcelExporter
    {
        FileDto ExportToFile(List<GetStreetTypeForViewDto> streetTypes);
    }
}