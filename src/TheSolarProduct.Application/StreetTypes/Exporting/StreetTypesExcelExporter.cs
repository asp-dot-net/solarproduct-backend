﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.StreetTypes.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.StreetTypes.Exporting
{
    public class StreetTypesExcelExporter : NpoiExcelExporterBase, IStreetTypesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public StreetTypesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetStreetTypeForViewDto> streetTypes)
        {
            return CreateExcelPackage(
                "StreetTypes.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("StreetTypes"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Code"),
                        L("IsActive")
                        );

                    AddObjects(
                        sheet, 2, streetTypes,
                        _ => _.StreetType.Name,
                        _ => _.StreetType.Code,
                        _ => _.StreetType.IsActive ? L("Yes") : L("No")
                        );

					
					
                });
        }
    }
}
