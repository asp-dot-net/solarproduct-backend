﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.ServiceInvoice;
using TheSolarProduct.Services;
using TheSolarProduct.ServiceInvoice.Dtos;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Installer;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.LeadActivityLogs;
using System.Collections.Generic;
using TheSolarProduct.JobHistory;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Leads;
using Abp.Authorization;
using Abp.Timing.Timezone;
using TheSolarProduct.UserActivityLogs;
using TheSolarProduct.UserActivityLogs.Dtos;

namespace TheSolarProduct.ServiceInvoices
{
    [AbpAuthorize]
    public class ServiceIncoiceAppServices : TheSolarProductAppServiceBase, IServiceIncoiceAppServices
    {
        private readonly IRepository<ServiceInvo> _serviceInvoRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<Service> _serviceRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IUserActivityLogAppService _userActivityLogServiceProxy;


        public ServiceIncoiceAppServices(IRepository<ServiceInvo> serviceInvoRepository,
            IRepository<User, long> userRepository,
            IRepository<LeadActivityLog> leadactivityRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
            IRepository<Service> serviceRepository,
            ITimeZoneConverter timeZoneConverter
            , IUserActivityLogAppService userActivityLogServiceProxy
            )
        {
            _serviceInvoRepository = serviceInvoRepository;
            _userRepository = userRepository;
            _dbcontextprovider = dbcontextprovider;
            _serviceRepository = serviceRepository;
            _leadactivityRepository = leadactivityRepository;
            _timeZoneConverter = timeZoneConverter;
            _userActivityLogServiceProxy = userActivityLogServiceProxy;
        }

        public async Task CreateOrEditService(CreateOrEditServiceInvoDto input, int sectionId, string Section)
        {
            if (input.Id == null || input.Id == 0)
            {
                await Create(input, sectionId, Section);
            }
            else
            {
                await Update(input, sectionId, Section);
            }
        }

        protected virtual async Task Create(CreateOrEditServiceInvoDto input, int sectionId, string Section)
        {
            input.InvoiceDate = (DateTime)_timeZoneConverter.Convert(input.InvoiceDate, (int)AbpSession.TenantId);
            input.PaidDate = (DateTime)_timeZoneConverter.Convert(input.PaidDate, (int)AbpSession.TenantId);

            var invoice = ObjectMapper.Map<ServiceInvo>(input);
            int leadId = (int)_serviceRepository.GetAll().Where(e => e.Id == input.ServiceID).Select(e => e.LeadId).FirstOrDefault();
            if (AbpSession.TenantId != null)
            {
                invoice.TenantId = (int)AbpSession.TenantId;
            }

            await _serviceInvoRepository.InsertAsync(invoice);
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionNote = "Invoice Added";
            leadactivity.ActionId = 49;
            leadactivity.SectionId = sectionId;
            leadactivity.LeadId = leadId;
            leadactivity.ServiceId = input.ServiceID;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            leadactivity.CreatorUserId = AbpSession.UserId;
            await _leadactivityRepository.InsertAsync(leadactivity);

            var log = new UserActivityLogDto();
            log.ActionId = 49;
            log.ActionNote = "Invoice Added";
            log.Section = Section;
            await _userActivityLogServiceProxy.Create(log);

        }


        protected virtual async Task Update(CreateOrEditServiceInvoDto input, int sectionId, string Section)
        {
            var invoice = await _serviceInvoRepository.FirstOrDefaultAsync((int)input.Id);
            int leadId = (int)_serviceRepository.GetAll().Where(e => e.Id == input.ServiceID).Select(e => e.LeadId).FirstOrDefault();

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionNote = "Invoice Updated";
            leadactivity.ActionId = 50;
            leadactivity.SectionId = sectionId;
            leadactivity.LeadId = leadId;
            leadactivity.ServiceId = input.ServiceID;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            leadactivity.CreatorUserId = AbpSession.UserId;

            var jobactionid = _leadactivityRepository.InsertAndGetId(leadactivity);

            var log = new UserActivityLogDto();
            log.ActionId = 50;
            log.ActionNote = "Invoice Updated";
            log.Section = Section;
            await _userActivityLogServiceProxy.Create(log);

            var list = new List<JobTrackerHistory>();
            if (invoice.InvoiceNo != input.InvoiceNo)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "InvoiceNo";
                jobhistory.PrevValue = invoice.InvoiceNo;
                jobhistory.CurValue = input.InvoiceNo;
                jobhistory.Action = "Service InvoiceNo";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = invoice.JobID;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (invoice.InvoiceDate != input.InvoiceDate)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "InvoiceDate";
                jobhistory.PrevValue = invoice.InvoiceDate.ToString();
                jobhistory.CurValue = input.InvoiceDate.ToString();
                jobhistory.Action = "Service InvoiceDate";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = invoice.JobID;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (invoice.Amount != input.Amount)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Amount";
                jobhistory.PrevValue = invoice.Amount.ToString();
                jobhistory.CurValue = input.Amount.ToString();
                jobhistory.Action = "Service Amount";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = invoice.JobID;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (invoice.PaidDate != input.PaidDate)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "PaidDate";
                jobhistory.PrevValue = invoice.PaidDate.ToString();
                jobhistory.CurValue = input.PaidDate.ToString();
                jobhistory.Action = "Service PaidDate";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = invoice.JobID;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (invoice.PaidReferenceNo != input.PaidReferenceNo)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "PaidReferenceNo";
                jobhistory.PrevValue = invoice.PaidReferenceNo;
                jobhistory.CurValue = input.PaidReferenceNo;
                jobhistory.Action = "Service PaidReferenceNo";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = invoice.JobID;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (list.Count > 0)
            {
                await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
                await _dbcontextprovider.GetDbContext().SaveChangesAsync();
            }

            ObjectMapper.Map(input, invoice);

        }

        public async Task<PagedResultDto<GetServiceInvoiceForViewDto>> GetAll(GetAllServiceInvoiceInput input)
        {
            var filteredInvoiceStatuses = _serviceInvoRepository.GetAll()
                         .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFk.JobNumber.Contains(input.Filter));

            var pagedAndFilteredInvoiceStatuses = filteredInvoiceStatuses
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var invoiceStatuses = from o in pagedAndFilteredInvoiceStatuses
                                  select new GetServiceInvoiceForViewDto()
                                  {
                                      serviceInvoice = new ServiceInvoiceDto()
                                      {
                                          Id = o.Id,
                                          ServiceId = o.ServiceID,
                                          JobId = o.JobID,
                                          LeadId = _serviceRepository.GetAll().Where(e => e.Id == o.ServiceID).Select(e => e.LeadId).FirstOrDefault(),
                                          InvoiceNo = o.InvoiceNo,
                                          InvoiceAmount = o.Amount,
                                          InvoiceDate = o.InvoiceDate,
                                          InvoicePaidDate  = o.PaidDate,
                                          InvoicePaidReferenceNo = o.PaidReferenceNo,
                                          ProjectNumber = o.JobFk.JobNumber,
                                          CustomerName = o.JobFk.LeadFk.CompanyName,
                                          ManufacturerTicketNumber = o.ServiceFk.CaseNo,
                                          TicketNumber = o.ServiceFk.TicketNo,
                                          InstallerName = _userRepository.GetAll().Where(e => e.Id == o.JobFk.InstallerId).Select(e=>e.FullName).FirstOrDefault(),
                                          ReplacementAddress = o.ServiceFk.Address
                                      }
                                  };

            return new PagedResultDto<GetServiceInvoiceForViewDto>(
                await pagedAndFilteredInvoiceStatuses.CountAsync(),
                await invoiceStatuses.ToListAsync()
            );
        }

        public async Task<CreateOrEditServiceInvoDto> GetserviceForEdit(EntityDto input)
        {
            var servicess =  _serviceInvoRepository.GetAll().Where(e => e.ServiceID == input.Id).FirstOrDefault();

            var output = new CreateOrEditServiceInvoDto();

            if (servicess != null)
            {
                output = ObjectMapper.Map<CreateOrEditServiceInvoDto>(servicess);
            }
            return output;
        }
    }
}
