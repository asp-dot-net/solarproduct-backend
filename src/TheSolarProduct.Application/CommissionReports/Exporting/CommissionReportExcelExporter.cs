﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CallHistory;
using TheSolarProduct.CheckActives.Dtos;
using TheSolarProduct.CheckActives.Exporting;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.CommissionReports.Dtos;
using TheSolarProduct.StockOrders;
using TheSolarProduct.Storage;

namespace TheSolarProduct.CommissionReports.Exporting
{
   
    public class CommissionReportExcelExporter : NpoiExcelExporterBase, ICommissionReportExcelExporter
    {


        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public CommissionReportExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
                                                base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetAllCommissionReportDto> listDtos)
        {
            return CreateExcelPackage(
                "CommissionReport.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("CommissionReport"));

                    AddHeader(
                        sheet,
                        L("User"),
                        L("Count"),
                         L("NoOfPanel"),
                         L("SystemCapacity"),
                         L("BatteryKw"),
                         
                         L("CommissionDate"),
                         L("Commission"),
                         L("BonusCommission"),
                         L("TotalCommission")
                    );

                    AddObjects(
                        sheet,
                        2,
                        listDtos,
                        _ => _.SalesRap,
                        _ => _.JobCount,
                        _ => _.NoOfPanel,
                        _ => _.SystemCapacity,
                        _ => _.BatteryKw,
                       
                        _ => _.CommissionDate,
                        _ => _.Commission,
                        _ => _.TotalBonusCommission,
                        _ => _.TotalCommission
                    );
                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                    for (var i = 1; i <= listDtos.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[1], "0.00");

                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[2], "0.00");

                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[3], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[4], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[7], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[8], "0.00");
                    }

                }
            );
        }


    }
}

