﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.CommissionReports.Dtos;

namespace TheSolarProduct.CommissionReports.Exporting
{
    public interface ICommissionReportExcelExporter
    {
        FileDto ExportToFile(List<GetAllCommissionReportDto> listDtos);
    }
}
