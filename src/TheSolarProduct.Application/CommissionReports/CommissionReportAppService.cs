﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Timing.Timezone;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Org.BouncyCastle.Crypto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.BonusList;
using TheSolarProduct.CheckActives.Dtos;
using TheSolarProduct.CheckActives.Exporting;
using TheSolarProduct.CommissionRanges;
using TheSolarProduct.CommissionReports.Exporting;
using TheSolarProduct.Dto;
using TheSolarProduct.Jobs;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.LeadGeneration.Dtos;
using TheSolarProduct.Leads;
using TheSolarProduct.MultiTenancy.HostDashboard.Dto;
using TheSolarProduct.Reports.CommissionReports;
using TheSolarProduct.Reports.CommissionReports.Dtos;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Timing;
using TheSolarProduct.Vendors;

namespace TheSolarProduct.CommissionReports
{
    public class CommissionReportAppService : TheSolarProductAppServiceBase, ICommissionReportAppService
    {
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<Job> _jobsRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<CommissionRange> _commissionRangeRepository;
        private readonly IRepository<ProductItem> _productItemRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly UserManager _userManager;
        private readonly ITimeZoneService _timeZoneService;
        private readonly IRepository<BonusCommissionItem> _bonusCommissionItemRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly ICommissionReportExcelExporter _commissionReportExcelRepository;
       
        public CommissionReportAppService(
            IRepository<Lead> leadRepository,
            IRepository<Job> jobsRepository,
            IRepository<User, long> userRepository,
             ITimeZoneConverter timeZoneConverter,
             IRepository<LeadActivityLog> leadactivityRepository,
             IRepository<CommissionRange> commissionRangeRepository
            ,IRepository<ProductItem> productItemRepository
            ,IRepository<JobProductItem> jobProductItemRepository
            , IRepository<UserTeam> userTeamRepository
            , UserManager userManager
            , ITimeZoneService timeZoneService
            , IRepository<BonusCommissionItem> bonusCommissionItemRepository
            , IRepository<UserRole, long> userRoleRepository
            , IRepository<Role> roleRepository
            , ICommissionReportExcelExporter commissionReportExcelRepository
            )
        {
            _leadRepository = leadRepository;
            _jobsRepository = jobsRepository;
            _userRepository = userRepository;
            _timeZoneConverter = timeZoneConverter;
            _leadactivityRepository = leadactivityRepository;
            _commissionRangeRepository = commissionRangeRepository;
            _productItemRepository = productItemRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _userTeamRepository =  userTeamRepository;
            _userManager = userManager;
            _timeZoneService = timeZoneService;
            _bonusCommissionItemRepository = bonusCommissionItemRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _commissionReportExcelRepository= commissionReportExcelRepository;  
        }

        //public async Task<PagedResultDto<GetAllCommissionReportDto>> GetAll(GetAllCommissionReportInputDto input)
        //{
        //    var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
        //    var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
        //    var User_List = _userRepository.GetAll().AsNoTracking();

        //    var job_list = _jobsRepository.GetAll()
        //        .Where(e=>e.LeadFk.OrganizationId == input.Orgid)
        //        .WhereIf(!string.IsNullOrEmpty(input.filter), e => e.JobNumber == input.filter)
        //        .WhereIf(input.StartDate != null && input.dateFilter == "InstallDate", e => e.InstallationDate.Value.Date >= SDate.Value.Date)
        //        .WhereIf(input.EndDate != null && input.dateFilter == "InstallDate", e => e.InstallationDate.Value.Date <= EDate.Value.Date)
        //        .WhereIf(input.StartDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Date >= SDate.Value.Date)
        //        .WhereIf(input.EndDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Date <= EDate.Value.Date)
        //        .WhereIf(input.UserId != 0, e => e.LeadFk.AssignToUserID == input.UserId)
        //        .Where(e => e.CommitionDate !=null);

        //    var pagedAndFilteredLeads = job_list
        //        .OrderBy(input.Sorting ?? "id desc")
        //        .PageBy(input);

        //    var result = (from o in pagedAndFilteredLeads

        //                  select new GetAllCommissionReportDto()
        //                  {
        //                      Id = o.Id,
        //                      ProjectNo = o.JobNumber,
        //                      CustomerName = User_List.Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
        //                      InstallDate = o.InstallationDate,
        //                      State = o.State,
        //                      TotalCost = o.TotalCost,
        //                      CommissionDate = o.CommitionDate,
        //                      IsSelect = false
        //                  }); 

        //   var totalCount = result.Count();

        //    return new PagedResultDto<GetAllCommissionReportDto>(totalCount, result.ToList());

        //}
        public async Task CreateCommission(CreateCommissionDto input)
        {
            input.CommitionDate = ((DateTime)_timeZoneConverter.Convert(input.CommitionDate, (int)AbpSession.TenantId));

            if(input.UserId > 0)
            {
                var user = _userRepository.GetAll().Where(e => e.Id == input.UserId).FirstOrDefault();

                foreach (int apointmentId in input.AppointmentIds)
                {
                    var appointment = await _jobsRepository.GetAsync(apointmentId);

                    var lead = await _leadRepository.GetAsync((int)appointment.LeadId);

                    appointment.CommitionDate = input.CommitionDate;
                    appointment.CommissionNote = input.Note;
                    appointment.CommitionAmount = (decimal)input.Amount;
                    appointment.CommitionActualAmount = input.CommitionActualAmount;
                    appointment.TotalBonusAmount = input.TotalBonusAmount;
                    appointment.IsBonusCommission = input.IsBonusCommission;
                    appointment.CommitionIsApprove = input.CommitionIsApprove;
                    appointment.CommitionIsVerified = input.CommitionIsVerified;

                    await _jobsRepository.UpdateAsync(appointment);
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 65;
                    leadactivity.SectionId = input.SectionId;
                    leadactivity.ActionNote = "Commission for " + user.FullName;
                    leadactivity.LeadId = Convert.ToInt32(appointment.LeadId);
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }
                    if (input.BonusIds != null)
                {
                    var Bonus = _bonusCommissionItemRepository.GetAll().Where(e => input.BonusIds.Contains(e.Id)).ToList();
                    foreach (var b in Bonus)
                    {
                        await _bonusCommissionItemRepository.HardDeleteAsync(b);
                    }
                }
               

                foreach (var item in input.BonusCommissionItemDto)
                {
                    var bonusCommission = new BonusCommissionItem();
                    bonusCommission.BonusId = item.BonusId;
                    bonusCommission.Amount = item.Amount;
                    bonusCommission.UserId = input.UserId;
                    bonusCommission.CommitionDate = input.CommitionDate;
                    bonusCommission.CommissionNote = input.Note;
                    bonusCommission.Notes = item.Notes;
                    bonusCommission.JobId = item.JobId;

                     await _bonusCommissionItemRepository.InsertAsync(bonusCommission);
                }
              
                }
            else
            {
                foreach (int apointmentId in input.AppointmentIds)
                {
                    var appointment = await _jobsRepository.GetAsync(apointmentId);

                    var lead = await _leadRepository.GetAsync((int)appointment.LeadId);

                    appointment.CommitionDate = input.CommitionDate;
                    appointment.CommissionNote = input.Note;
                    appointment.CommitionAmount = (decimal)input.Amount;
                    appointment.CommitionActualAmount = input.CommitionActualAmount;
                    appointment.TotalBonusAmount = input.TotalBonusAmount;
                    appointment.IsBonusCommission = input.IsBonusCommission;
                    appointment.CommitionIsApprove=input.CommitionIsApprove;
                    appointment.CommitionIsVerified=input.CommitionIsVerified;

                    await _jobsRepository.UpdateAsync(appointment);

                    var user = _userRepository.GetAll().Where(e => e.Id == lead.AssignToUserID).FirstOrDefault();

                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 65;
                    leadactivity.SectionId = input.SectionId;
                    leadactivity.ActionNote = "Commission for " + user.FullName;
                    leadactivity.LeadId = Convert.ToInt32(appointment.LeadId);
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);

                    var Bonus = _bonusCommissionItemRepository.GetAll().Where(e => input.BonusIds.Contains(e.Id)).ToList();
                    foreach (var b in Bonus)
                    {
                        await _bonusCommissionItemRepository.HardDeleteAsync(b);
                    }

                    foreach (var item in input.BonusCommissionItemDto)
                    {
                        var bonusCommission = new BonusCommissionItem();
                        bonusCommission.BonusId = item.BonusId;
                        bonusCommission.Amount = item.Amount;
                        bonusCommission.UserId = input.UserId;
                        bonusCommission.CommitionDate = input.CommitionDate;
                        bonusCommission.CommissionNote = input.Note;
                        bonusCommission.JobId = item.JobId;

                        await _bonusCommissionItemRepository.InsertAsync(bonusCommission);
                    }
                }

            }

        }
        public async Task<List<GetCommissionAmountList>> getCommissionAmountByUserId(List<int> jobIds, int orgid)
       {
            var res = new List<GetCommissionAmountList>();
                //var user = await _userRepository.GetAsync(UserId);

            //if (user != null)
            //{
            //    p = (double)(user.CommitionAmount != null ? user.CommitionAmount : 0);
            //}
            decimal totatalSystemCapasity = 0;

            foreach (var id in jobIds)
            {
                var job = await _jobsRepository.GetAsync(id);
                var batteryKw = _jobProductItemRepository.GetAll().Where(e => e.ProductItemFk.ProductTypeId == 5 && e.JobId == id).Sum(e => e.Quantity * e.ProductItemFk.Size);

                totatalSystemCapasity = (decimal)((totatalSystemCapasity + (job.SystemCapacity != null ? job.SystemCapacity : 0)) + batteryKw);
            }
            //totatalSystemCapasity = _jobsRepository.GetAll().Where(e => jobIds.Contains(e.Id)).Sum(e => e.SystemCapacity).Value;

            var commissions = _commissionRangeRepository.GetAll().Where(e =>e.OrgId == orgid).AsNoTracking().ToList();
            decimal min_KW = 0;
            decimal max_KW = 0;
            double price = 0;
            foreach(var commission in commissions)
            {
                var range = commission.Range.Split("-");
                decimal min = 0;
                decimal max = 0;
                if (range.Length > 1) 
                {
                    min = Convert.ToInt32(range[0].Trim());
                    max = Convert.ToInt32(range[1].Trim());
                }
                else
                {
                    min = Convert.ToInt32(range[0].Replace("+","").Trim());
                }


                if (max != 0)
                {

                    if (totatalSystemCapasity > max)
                    {
                        min_KW = min;
                        max_KW = max;
                    }
                    else
                    {
                        min_KW = min;
                        max_KW = totatalSystemCapasity;
                    }
                    if (max_KW >= min_KW)
                    {
                        //price = price + (double)((max_KW- min_KW) * commission.Value);
                        price =  (double)((max_KW- min_KW) * commission.Value);
                        res.Add(new GetCommissionAmountList()
                        {
                            Range = min_KW.ToString() + " - " + max_KW.ToString(),
                            value = price
                        }) ;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    min_KW = min;
                    max_KW = totatalSystemCapasity;

                    if(max_KW >= min_KW)
                    {
                        //price = price + (double)((max_KW - min_KW) * commission.Value);
                        price = (double)((max_KW - min_KW) * commission.Value);
                        res.Add(new GetCommissionAmountList()
                        {
                            Range = min_KW.ToString() + " - " + max_KW.ToString(),
                            value = price
                        });
                    }
                    else
                    {
                        break;
                    }


                }

            }


            return res;
        }

        public async Task<PagedResultDto<GetAllCommissionReportDto>> GetAll(GetAllCommissionReportInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var User_List = _userRepository.GetAll().AsNoTracking();
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();

            var Panels = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1).Select(e => e.Id).ToList();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });
            var TeamId = await team_list.Where(e => e.UserId == AbpSession.UserId).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();


            var totalCount = 0;

            var result = new List<GetAllCommissionReportDto>();

            if(input.dateFilter == "NoDate")
            {
                IList<string> role = await _userManager.GetRolesAsync(User);

                var salesRepRoleId = _roleRepository.GetAll().Where(e => e.Name == "Sales Rep").Select(e => e.Id).FirstOrDefault();

                var BonusesCommisons = _bonusCommissionItemRepository.GetAll().Where(e => e.UserId > 0)
                    .
                    WhereIf(role.Contains("Admin"), e => e.JobFK.LeadFk.AssignToUserID != null)
                    .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFK.LeadFk.AssignToUserID))
                    .WhereIf(role.Contains("Sales Rep"), e => e.JobFK.LeadFk.AssignToUserID == AbpSession.UserId)
                    .WhereIf(input.UserId > 0 , e => e.UserId == input.UserId)
                    .GroupBy(e => new {e.UserId, e.CommissionNote, CommitionDate = (e.CommitionDate.Value.Year > 1 ? e.CommitionDate : null) })
                    .Select(e => new {e.Key.UserId, e.Key.CommitionDate, e.Key.CommissionNote, Amount = e.Sum(s => s.Amount)});

                result = (from o in BonusesCommisons
                         select new GetAllCommissionReportDto()
                         {

                             SalesRap = User_List.Where(e => e.Id == o.UserId).Select(e => e.FullName).FirstOrDefault(),
                             SystemCapacity = 0,
                             JobCount = 0,
                             Id = (int)o.UserId,

                             //Commission = getCommissionAmountByUserId(jobs),
                             IsSelect = false,
                             CommissionDate = (o.CommitionDate.Value.Year) > 1 ? o.CommitionDate.Value.ToString("dd - MMM - yyyy") : "",
                             Commission = 0,
                             TotalCommission = o.Amount,
                             TotalBonusCommission = o.Amount,
                             CommitionDate = o.CommitionDate,
                             IsSalesRap = false
                         }).ToList();

               var CommissionForcreate = (from o in User_List.Where(e => e.Id == input.UserId)
                          select new GetAllCommissionReportDto()
                          {
                              SalesRap = o.FullName,
                              SystemCapacity = 0,
                              JobCount = 0,
                              Id = (int)o.Id,

                              //Commission = getCommissionAmountByUserId(jobs),
                              IsSelect = false,
                              CommissionDate = "",
                              Commission = 0,
                              TotalCommission = 0,
                              TotalBonusCommission = 0,
                              CommitionDate = null,
                              IsSalesRap = false

                          }).ToList();

                result = result != null ? result : new List<GetAllCommissionReportDto>();

                foreach(var item in  CommissionForcreate)
                {
                    result.Add(item);
                }
                totalCount = result.Count;
            }
            else
            {
                IList<string> role = await _userManager.GetRolesAsync(User);

                var salesRepRoleId = _roleRepository.GetAll().Where(e => e.Name == "Sales Rep").Select(e => e.Id).FirstOrDefault();

                var job_list = _jobsRepository.GetAll()
                    .Where(e => e.LeadFk.OrganizationId == input.Orgid && e.LeadFk.AssignToUserID != null)
                    .WhereIf(!string.IsNullOrEmpty(input.filter), e => e.JobNumber == input.filter)
                    .WhereIf(input.StartDate != null && input.dateFilter == "InstallDate", e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                    .WhereIf(input.EndDate != null && input.dateFilter == "InstallDate", e => e.InstallationDate.Value.Date <= EDate.Value.Date)
                    .WhereIf(input.dateFilter == "InstallComplete" && input.StartDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                    .WhereIf(input.dateFilter == "InstallComplete" && input.EndDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                    .WhereIf(input.dateFilter == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                    .WhereIf(input.dateFilter == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)
                    .WhereIf(input.dateFilter == "Active" && input.StartDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                    .WhereIf(input.dateFilter == "Active" && input.EndDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                    //.WhereIf(input.StartDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Value.Date >= SDate.Value.Date)
                    //.WhereIf(input.EndDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Value.Date <= EDate.Value.Date)
                    .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                    .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                    .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == AbpSession.UserId)
                    .WhereIf(input.UserId != 0, e => e.LeadFk.AssignToUserID == input.UserId)

                    // .Where(e => _userRoleRepository.GetAll().Any(ur => ur.UserId == e.LeadFk.AssignToUserID && ur.RoleId == salesRepRoleId))
                    //.Where(e => _userManager.IsInRoleAsync(_userRepository.FirstOrDefault((long)e.LeadFk.AssignToUserID), "Sales Rep"))

                    //.Where(e => e.CommissionNote == null || (e.CommitionDate !=null ? e.CommitionDate.ToString() : "") == "0001-01-01 00:00:00.0000000")
                    .AsNoTracking().GroupBy(e => new { e.LeadFk.AssignToUserID, CommitionDate = (e.CommitionDate.Value.Year > 1 ? e.CommitionDate : null) })//, e.CommitionAmount, e.CommitionActualAmount, e.TotalBonusAmount })
                    .Select(e =>
                            new {
                                // Noofpanel = _jobProductItemRepository.GetAll().Where(j => Panels.Contains((int)j.ProductItemId)).Select(j => j.Quantity).Sum(),
                                SystemCapacity = e.Sum(e => e.SystemCapacity),
                                e.Key.AssignToUserID,
                                e.Key.CommitionDate,
                                CommitionAmount = e.Key.CommitionDate != null ? e.Max(m => m.CommitionAmount) : 0,
                                CommitionActualAmount = e.Key.CommitionDate != null ? e.Max(m => m.CommitionActualAmount) : 0,
                                TotalBonusAmount = e.Key.CommitionDate != null ? e.Max(m => m.TotalBonusAmount) : 0,
                                //e.Key.CommitionAmount,
                                //e.Key.CommitionActualAmount,
                                //e.Key.TotalBonusAmount,
                                jobcountdata = e.Count(),
                                role = _userRoleRepository.GetAll().Any(ur => ur.UserId == e.Key.AssignToUserID && ur.RoleId == salesRepRoleId)
                            });

                var jobids = _jobsRepository.GetAll()
                    .Where(e => e.LeadFk.OrganizationId == input.Orgid && e.LeadFk.AssignToUserID != null)
                    .WhereIf(!string.IsNullOrEmpty(input.filter), e => e.JobNumber == input.filter)
                    .WhereIf(input.StartDate != null && input.dateFilter == "InstallDate", e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                    .WhereIf(input.EndDate != null && input.dateFilter == "InstallDate", e => e.InstallationDate.Value.Date <= EDate.Value.Date)
                    .WhereIf(input.dateFilter == "InstallComplete" && input.StartDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                    .WhereIf(input.dateFilter == "InstallComplete" && input.EndDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                    .WhereIf(input.dateFilter == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                    .WhereIf(input.dateFilter == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)
                    .WhereIf(input.dateFilter == "Active" && input.StartDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                    .WhereIf(input.dateFilter == "Active" && input.EndDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                    //.WhereIf(input.StartDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Value.Date >= SDate.Value.Date)
                    //.WhereIf(input.EndDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Value.Date <= EDate.Value.Date)
                    .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                    .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                    .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == AbpSession.UserId)
                    .WhereIf(input.UserId != 0, e => e.LeadFk.AssignToUserID == input.UserId)
                    //    .Where(e => e.LeadFk.OrganizationId == input.Orgid)
                    //    .WhereIf(!string.IsNullOrEmpty(input.filter), e => e.JobNumber == input.filter)
                    //    .WhereIf(input.StartDate != null && input.dateFilter == "InstallDate", e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                    //    .WhereIf(input.EndDate != null && input.dateFilter == "InstallDate", e => e.InstallationDate.Value.Date <= EDate.Value.Date)
                    //    .WhereIf(input.dateFilter == "InstallComplete" && input.StartDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                    //    .WhereIf(input.dateFilter == "InstallComplete" && input.EndDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                    //    .WhereIf(input.dateFilter == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                    //    .WhereIf(input.dateFilter == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)
                    //    .WhereIf(input.dateFilter == "Active" && input.StartDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                    //    .WhereIf(input.dateFilter == "Active" && input.EndDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                    //    .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                    //    .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                    //    .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == AbpSession.UserId)
                    //    //.WhereIf(input.StartDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Value.Date >= SDate.Value.Date)
                    //    //.WhereIf(input.EndDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Value.Date <= EDate.Value.Date)
                    //    .WhereIf(input.UserId != 0, e => e.LeadFk.AssignToUserID == input.UserId)
                    //.Where(e => e.CommitionDate == null || (e.CommitionDate != null ? e.CommitionDate.ToString() : "") == "0001-01-01 00:00:00.0000000")
                    .AsNoTracking().Select(e => new { e.Id, e.LeadFk.AssignToUserID, CommitionDate = (e.CommitionDate.Value.Year) > 1 ? e.CommitionDate.Value.ToString("dd - MMM - yyyy") : "" })
                    .ToList();




                var pagedAndFilteredLeads = (from o in job_list
                          select new GetAllCommissionReportDto()
                          {

                              SalesRap = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                              SystemCapacity = o.SystemCapacity,
                              JobCount = o.jobcountdata,
                              Id = (int)o.AssignToUserID,

                              //Commission = getCommissionAmountByUserId(jobs),
                              IsSelect = false,
                              CommissionDate = (o.CommitionDate.Value.Year) > 1 ? o.CommitionDate.Value.ToString("dd - MMM - yyyy") : "",
                              Commission = (double?)o.CommitionActualAmount,
                              TotalCommission = o.CommitionAmount,
                              TotalBonusCommission = o.TotalBonusAmount,
                              CommitionDate = o.CommitionDate,
                              IsSalesRap = o.role
                          }).ToList();

                result = pagedAndFilteredLeads.AsQueryable()
                    .OrderBy(input.Sorting ?? "SalesRap Asc")
                    .PageBy(input).ToList();

                foreach (var job in pagedAndFilteredLeads)
                {
                    if (job.CommissionDate == "")
                    {
                        List<int> ids = jobids.Where(e => e.AssignToUserID == job.Id && e.CommitionDate == job.CommissionDate).Select(e => e.Id).ToList();
                        job.CommissionAmountList = await getCommissionAmountByUserId(ids, input.Orgid);
                        job.Commission = job.CommissionAmountList.Sum(e => e.value);
                        job.jobs = ids;
                        job.NoOfPanel = _jobProductItemRepository.GetAll().Where(e => Panels.Contains((int)e.ProductItemId) && ids.Contains((int)e.JobId)).Select(e => e.Quantity).Sum();
                        job.BatteryKw = _jobProductItemRepository.GetAll().Where(e => e.ProductItemFk.ProductTypeId == 5 && ids.Contains((int)e.JobId)).Sum(e => e.Quantity * e.ProductItemFk.Size);
                    }
                    else
                    {
                        List<int> ids = _jobsRepository.GetAll().Where(e => e.LeadFk.AssignToUserID == job.Id && e.CommitionDate == job.CommitionDate).Select(e => e.Id).ToList();

                        job.jobs = ids;
                        job.NoOfPanel = _jobProductItemRepository.GetAll().Where(e => Panels.Contains((int)e.ProductItemId) && ids.Contains((int)e.JobId)).Select(e => e.Quantity).Sum();
                        job.BatteryKw = _jobProductItemRepository.GetAll().Where(e => e.ProductItemFk.ProductTypeId == 5 && ids.Contains((int)e.JobId)).Sum(e => e.Quantity * e.ProductItemFk.Size);

                    }

                }
                totalCount = job_list.Count();

            }


            SummaryCommissionReport commissionReport = new SummaryCommissionReport();
            if(result.Count() > 0)
            {
                commissionReport.TotalJobCount = result.Sum(e => e.JobCount);
                commissionReport.TotalCommission = result.Sum(e => e.Commission);
                commissionReport.TotalNoOfPanel = result.Sum(e => e.NoOfPanel);
                commissionReport.TotalSystemCapacity = result.Sum(e => e.SystemCapacity);
                commissionReport.TotalBatteryKw = result.Sum(e => e.BatteryKw);
                result[0].summary = commissionReport;
            }


            return new PagedResultDto<GetAllCommissionReportDto>(totalCount, result);

        }

        public async Task<List<GetAllCommissionReportDetailsViewDto>> GetAllCommissionReportDetail(GetAllCommissionReportDetailInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var User_List = _userRepository.GetAll().AsNoTracking();
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });
            var TeamId = await team_list.Where(e => e.UserId == AbpSession.UserId).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);

            var job_list = _jobsRepository.GetAll()
                .Where(e => e.LeadFk.OrganizationId == input.Orgid && e.LeadFk.AssignToUserID != null)
                .WhereIf(!string.IsNullOrEmpty(input.filter), e => e.JobNumber == input.filter)
                .WhereIf(input.StartDate != null && input.dateFilter == "InstallDate", e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                .WhereIf(input.EndDate != null && input.dateFilter == "InstallDate", e => e.InstallationDate.Value.Date <= EDate.Value.Date)
                .WhereIf(input.dateFilter == "InstallComplete" && input.StartDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                .WhereIf(input.dateFilter == "InstallComplete" && input.EndDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                .WhereIf(input.dateFilter == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                .WhereIf(input.dateFilter == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)
                .WhereIf(input.dateFilter == "Active" && input.StartDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                .WhereIf(input.dateFilter == "Active" && input.EndDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                //.WhereIf(input.StartDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Value.Date >= SDate.Value.Date)
                //.WhereIf(input.EndDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Value.Date <= EDate.Value.Date)
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == AbpSession.UserId)
                .WhereIf(input.UserId != 0, e => e.LeadFk.AssignToUserID == input.UserId)
                //.Where(e => e.CommitionDate == null ||  e.CommitionDate.ToString() == "0001-01-01 00:00:00.0000000")
                .AsNoTracking().Select(e => new { e.JobNumber, e.InstallationDate, e.State, e.SystemCapacity,e.Id , commissionDate = (e.CommitionDate.Value.Year) > 1 ? e.CommitionDate.Value.ToString("dd - MMM - yyyy") : ""}).ToList();

            var result = (from o in job_list.Where(e => e.commissionDate == (string.IsNullOrEmpty(input.CommissionDate) ? "" : input.CommissionDate))

                          select new GetAllCommissionReportDetailsViewDto()
                          {
                              Id = o.Id,
                              ProjectNo = o.JobNumber,
                              InstallDate = o.InstallationDate,
                              State = o.State,
                              SystemCapacity = o.SystemCapacity,
                              BatteryKW = _jobProductItemRepository.GetAll().Where(e => e.ProductItemFk.ProductTypeId == 5 && e.JobId == o.Id).Sum(e => e.Quantity * e.ProductItemFk.Size)

                          }).ToList();

            return result;

        }

        public async Task<PagedResultDto<GetAllCommissionPaidReportViewDto>> GetAllCommissionPaidReport(GetAllCommissionPaidReportInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var User_List = _userRepository.GetAll().AsNoTracking();

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();

            var Panels = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1).Select(e => e.Id).ToList();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });
            var TeamId = await team_list.Where(e => e.UserId == AbpSession.UserId).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = _userTeamRepository.GetAll().Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }

            var job_list = _jobProductItemRepository.GetAll()
                .Where(e => e.JobFk.LeadFk.OrganizationId == input.Orgid)
                .WhereIf(!string.IsNullOrEmpty(input.filter), e => e.JobFk.JobNumber == input.filter)
                .WhereIf(input.StartDate != null, e => e.JobFk.CommitionDate.Value.Date >= SDate.Value.Date)
                .WhereIf(input.EndDate != null, e => e.JobFk.CommitionDate.Value.Date <= EDate.Value.Date)
                //.WhereIf(input.StartDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Value.Date >= SDate.Value.Date)
                //.WhereIf(input.EndDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Value.Date <= EDate.Value.Date)
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == AbpSession.UserId)
                .WhereIf(input.UserId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.UserId)
                .WhereIf(input.TeamId != 0, e => FilterTeamList.Contains(e.CreatorUserId))

                .Where(e => e.JobFk.CommitionDate != null &&  (e.JobFk.CommitionDate != null ? e.JobFk.CommitionDate.ToString() : "") != "0001-01-01 00:00:00.0000000")
                .AsNoTracking().GroupBy(e => new { e.JobFk.JobNumber, e.JobFk.LeadFk.AssignToUserID, e.JobFk.SystemCapacity, e.JobFk.CommitionDate,e.JobFk.CommitionAmount,e.JobFk.CommitionActualAmount })
                .Select(e => new { jobNumber = e.Key.JobNumber, userId = e.Key.AssignToUserID , sys = e.Key.SystemCapacity, commissionDate = e.Key.CommitionDate, e.Key.CommitionAmount, e.Key.CommitionActualAmount , NOP = e.Sum(s=>s.Quantity) });

            var pagedAndFilteredLeads = job_list
               .OrderBy(input.Sorting ?? "jobNumber Asc")
               .PageBy(input);

            var result = (from o in pagedAndFilteredLeads
                          select new GetAllCommissionPaidReportViewDto()
                          {
                              JobNumber = o.jobNumber,
                              SalesREp = User_List.Where(e => e.Id == o.userId).Select(e => e.FullName).FirstOrDefault(),
                              SystemCapacity = o.sys,
                              NoOfPanel = o.NOP,
                              Id = (int)o.userId,
                              CommissionDate = o.commissionDate,
                              CommissionAmount = o.CommitionAmount,
                              ActualAmount = o.CommitionActualAmount
                          }).ToList();

            var totalCount = result.Count();

            return new PagedResultDto<GetAllCommissionPaidReportViewDto>(totalCount, result);

        }
        
        public async Task<CreateCommissionDto> GetCommissionForDto(int jobId)
        {
            var Commission = new CreateCommissionDto();

            var job = await _jobsRepository.GetAsync(jobId);

            var lead = await _leadRepository.GetAsync((int)job.LeadId);

            Commission.CommitionDate = job.CommitionDate ?? DateTime.Now;
            Commission.Note = job.CommissionNote ?? string.Empty;
            
            Commission.Amount = job.CommitionAmount;
            Commission.CommitionActualAmount = job.CommitionActualAmount ?? 0;
            Commission.TotalBonusAmount = job.TotalBonusAmount ?? 0;

            Commission.IsBonusCommission = job.IsBonusCommission ?? false;
            Commission.CommitionIsApprove = job.CommitionIsApprove ?? false;
            Commission.CommitionIsVerified =  false;

            Commission.BonusCommissionItemDto = _bonusCommissionItemRepository.GetAll()
                .Where(e => e.JobId == jobId)
                .Select(o => new BonusCommissionItemDto
                {
                    Id = o.Id,
                    BonusId = o.BonusId,
                    JobId = o.JobId,
                    Amount = o.Amount ?? 0,
                    Notes = o.Notes ?? string.Empty,
                }).ToList() ?? new List<BonusCommissionItemDto>();

            Commission.BonusIds = Commission.BonusCommissionItemDto != null ? Commission.BonusCommissionItemDto.Select(e => e.Id).ToList() : new List<int?>();

            
            return Commission;
        }


        public async Task<CreateCommissionDto> GetBonusCommissionForDto(DateTime? CommissionDate, int UserId)
        {
            var Commission = new CreateCommissionDto();

            var bonusCommissionItems = _bonusCommissionItemRepository.GetAll()
                .Where(e => e.UserId == UserId)
                .Select(o => new BonusCommissionItemDto()
                {
                    Id = o.Id,
                    BonusId = o.BonusId,
                    JobId = o.JobId,
                    Notes = o.Notes,
                    Amount = o.Amount,
                    CommissionNote = o.CommissionNote,
                    CommitionDate = o.CommitionDate,
                    UserId = o.UserId,
                })
                .ToList();

            Commission.BonusCommissionItemDto = bonusCommissionItems;
            Commission.CommitionDate = CommissionDate ?? DateTime.Now; // Default to current date if null

            // Handle cases where BonusCommissionItemDto is empty
            var firstItem = Commission.BonusCommissionItemDto.FirstOrDefault();
            Commission.Note = firstItem?.CommissionNote ?? string.Empty; // Default empty string if null
            Commission.UserId = firstItem?.UserId ?? 0; // Default user ID to 0 if null
            Commission.Amount = 0;

            // Handle null cases for sums
            Commission.CommitionActualAmount = Commission.BonusCommissionItemDto.Any()
                ? Commission.BonusCommissionItemDto.Sum(e => e.Amount)
                : 0;

            Commission.TotalBonusAmount = Commission.CommitionActualAmount;
            Commission.IsBonusCommission = true;
            Commission.BonusIds = Commission.BonusCommissionItemDto.Any()
                ? Commission.BonusCommissionItemDto.Select(e => e.Id).ToList()
                : new List<int?>();

            Commission.CommitionIsVerified = true;
            Commission.CommitionIsApprove = true;

            return Commission;
        }


        public async Task<FileDto> GetCommissionToExcel(CommissionReportExcelExportDto input)
        {

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var User_List = _userRepository.GetAll().AsNoTracking();
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();

            var Panels = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1).Select(e => e.Id).ToList();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });
            var TeamId = await team_list.Where(e => e.UserId == AbpSession.UserId).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();


            var totalCount = 0;

            var result = new List<GetAllCommissionReportDto>();

            if (input.dateFilter == "NoDate")
            {

                var BonusesCommisons = _bonusCommissionItemRepository.GetAll().Where(e => e.UserId > 0).WhereIf(input.UserId > 0, e => e.UserId == input.UserId)
                    .GroupBy(e => new { e.UserId, e.CommissionNote, CommitionDate = (e.CommitionDate.Value.Year > 1 ? e.CommitionDate : null) })
                    .Select(e => new { e.Key.UserId, e.Key.CommitionDate, e.Key.CommissionNote, Amount = e.Sum(s => s.Amount) });

                result = (from o in BonusesCommisons
                          select new GetAllCommissionReportDto()
                          {

                              SalesRap = User_List.Where(e => e.Id == o.UserId).Select(e => e.FullName).FirstOrDefault(),
                              SystemCapacity = 0,
                              JobCount = 0,
                              Id = (int)o.UserId,

                              //Commission = getCommissionAmountByUserId(jobs),
                              IsSelect = false,
                              CommissionDate = (o.CommitionDate.Value.Year) > 1 ? o.CommitionDate.Value.ToString("dd - MMM - yyyy") : "",
                              Commission = 0,
                              TotalCommission = o.Amount,
                              TotalBonusCommission = o.Amount,
                              CommitionDate = o.CommitionDate,
                              IsSalesRap = false
                          }).ToList();

                var CommissionForcreate = (from o in User_List.Where(e => e.Id == input.UserId)
                                           select new GetAllCommissionReportDto()
                                           {
                                               SalesRap = o.FullName,
                                               SystemCapacity = 0,
                                               JobCount = 0,
                                               Id = (int)o.Id,

                                               //Commission = getCommissionAmountByUserId(jobs),
                                               IsSelect = false,
                                               CommissionDate = "",
                                               Commission = 0,
                                               TotalCommission = 0,
                                               TotalBonusCommission = 0,
                                               CommitionDate = null,
                                               IsSalesRap = false

                                           }).ToList();

                result = result != null ? result : new List<GetAllCommissionReportDto>();

                foreach (var item in CommissionForcreate)
                {
                    result.Add(item);
                }
                totalCount = result.Count;
            }
            else
            {
                IList<string> role = await _userManager.GetRolesAsync(User);

                var salesRepRoleId = _roleRepository.GetAll().Where(e => e.Name == "Sales Rep").Select(e => e.Id).FirstOrDefault();

                var job_list = _jobsRepository.GetAll()
                    .Where(e => e.LeadFk.OrganizationId == input.Orgid && e.LeadFk.AssignToUserID != null)
                    .WhereIf(!string.IsNullOrEmpty(input.filter), e => e.JobNumber == input.filter)
                    .WhereIf(input.StartDate != null && input.dateFilter == "InstallDate", e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                    .WhereIf(input.EndDate != null && input.dateFilter == "InstallDate", e => e.InstallationDate.Value.Date <= EDate.Value.Date)
                    .WhereIf(input.dateFilter == "InstallComplete" && input.StartDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                    .WhereIf(input.dateFilter == "InstallComplete" && input.EndDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                    .WhereIf(input.dateFilter == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                    .WhereIf(input.dateFilter == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)
                    .WhereIf(input.dateFilter == "Active" && input.StartDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                    .WhereIf(input.dateFilter == "Active" && input.EndDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                    //.WhereIf(input.StartDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Value.Date >= SDate.Value.Date)
                    //.WhereIf(input.EndDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Value.Date <= EDate.Value.Date)
                    .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                    .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                    .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == AbpSession.UserId)
                    .WhereIf(input.UserId != 0, e => e.LeadFk.AssignToUserID == input.UserId)

                    // .Where(e => _userRoleRepository.GetAll().Any(ur => ur.UserId == e.LeadFk.AssignToUserID && ur.RoleId == salesRepRoleId))
                    //.Where(e => _userManager.IsInRoleAsync(_userRepository.FirstOrDefault((long)e.LeadFk.AssignToUserID), "Sales Rep"))

                    //.Where(e => e.CommissionNote == null || (e.CommitionDate !=null ? e.CommitionDate.ToString() : "") == "0001-01-01 00:00:00.0000000")
                    .AsNoTracking().GroupBy(e => new { e.LeadFk.AssignToUserID, CommitionDate = (e.CommitionDate.Value.Year > 1 ? e.CommitionDate : null), e.CommitionAmount, e.CommitionActualAmount, e.TotalBonusAmount })
                    .Select(e =>
                            new {
                                // Noofpanel = _jobProductItemRepository.GetAll().Where(j => Panels.Contains((int)j.ProductItemId)).Select(j => j.Quantity).Sum(),
                                SystemCapacity = e.Sum(e => e.SystemCapacity),
                                e.Key.AssignToUserID,
                                e.Key.CommitionDate,
                                e.Key.CommitionAmount,
                                e.Key.CommitionActualAmount,
                                e.Key.TotalBonusAmount,
                                jobcountdata = e.Count(),
                                role = _userRoleRepository.GetAll().Any(ur => ur.UserId == e.Key.AssignToUserID && ur.RoleId == salesRepRoleId)
                            });

                var jobids = _jobsRepository.GetAll()
                    .Where(e => e.LeadFk.OrganizationId == input.Orgid && e.LeadFk.AssignToUserID != null)
                    .WhereIf(!string.IsNullOrEmpty(input.filter), e => e.JobNumber == input.filter)
                    .WhereIf(input.StartDate != null && input.dateFilter == "InstallDate", e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                    .WhereIf(input.EndDate != null && input.dateFilter == "InstallDate", e => e.InstallationDate.Value.Date <= EDate.Value.Date)
                    .WhereIf(input.dateFilter == "InstallComplete" && input.StartDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                    .WhereIf(input.dateFilter == "InstallComplete" && input.EndDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                    .WhereIf(input.dateFilter == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                    .WhereIf(input.dateFilter == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)
                    .WhereIf(input.dateFilter == "Active" && input.StartDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                    .WhereIf(input.dateFilter == "Active" && input.EndDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                    //.WhereIf(input.StartDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Value.Date >= SDate.Value.Date)
                    //.WhereIf(input.EndDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Value.Date <= EDate.Value.Date)
                    .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                    .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                    .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == AbpSession.UserId)
                    .WhereIf(input.UserId != 0, e => e.LeadFk.AssignToUserID == input.UserId)
                    //    .Where(e => e.LeadFk.OrganizationId == input.Orgid)
                    //    .WhereIf(!string.IsNullOrEmpty(input.filter), e => e.JobNumber == input.filter)
                    //    .WhereIf(input.StartDate != null && input.dateFilter == "InstallDate", e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                    //    .WhereIf(input.EndDate != null && input.dateFilter == "InstallDate", e => e.InstallationDate.Value.Date <= EDate.Value.Date)
                    //    .WhereIf(input.dateFilter == "InstallComplete" && input.StartDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                    //    .WhereIf(input.dateFilter == "InstallComplete" && input.EndDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                    //    .WhereIf(input.dateFilter == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                    //    .WhereIf(input.dateFilter == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)
                    //    .WhereIf(input.dateFilter == "Active" && input.StartDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                    //    .WhereIf(input.dateFilter == "Active" && input.EndDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                    //    .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                    //    .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                    //    .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == AbpSession.UserId)
                    //    //.WhereIf(input.StartDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Value.Date >= SDate.Value.Date)
                    //    //.WhereIf(input.EndDate != null && input.dateFilter == "CommissionDate", e => e.CommitionDate.Value.Date <= EDate.Value.Date)
                    //    .WhereIf(input.UserId != 0, e => e.LeadFk.AssignToUserID == input.UserId)
                    //.Where(e => e.CommitionDate == null || (e.CommitionDate != null ? e.CommitionDate.ToString() : "") == "0001-01-01 00:00:00.0000000")
                    .AsNoTracking().Select(e => new { e.Id, e.LeadFk.AssignToUserID, CommitionDate = (e.CommitionDate.Value.Year) > 1 ? e.CommitionDate.Value.ToString("dd - MMM - yyyy") : "" })
                    .ToList();


                var pagedAndFilteredLeads = job_list;

                result = (from o in pagedAndFilteredLeads
                          select new GetAllCommissionReportDto()
                          {

                              SalesRap = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                              SystemCapacity = o.SystemCapacity,
                              JobCount = o.jobcountdata,
                              Id = (int)o.AssignToUserID,

                              //Commission = getCommissionAmountByUserId(jobs),
                              IsSelect = false,
                              CommissionDate = (o.CommitionDate.Value.Year) > 1 ? o.CommitionDate.Value.ToString("dd - MMM - yyyy") : "",
                              Commission = (double?)o.CommitionActualAmount,
                              TotalCommission = o.CommitionAmount,
                              TotalBonusCommission = o.TotalBonusAmount,
                              CommitionDate = o.CommitionDate,
                              IsSalesRap = o.role
                          }).ToList();

                foreach (var job in result)
                {
                    if (job.CommissionDate == "")
                    {
                        List<int> ids = jobids.Where(e => e.AssignToUserID == job.Id && e.CommitionDate == job.CommissionDate).Select(e => e.Id).ToList();
                        job.CommissionAmountList = await getCommissionAmountByUserId(ids, input.Orgid);
                        job.Commission = job.CommissionAmountList.Sum(e => e.value);
                        job.jobs = ids;
                        job.NoOfPanel = _jobProductItemRepository.GetAll().Where(e => Panels.Contains((int)e.ProductItemId) && ids.Contains((int)e.JobId)).Select(e => e.Quantity).Sum();
                        job.BatteryKw = _jobProductItemRepository.GetAll().Where(e => e.ProductItemFk.ProductTypeId == 5 && ids.Contains((int)e.JobId)).Sum(e => e.Quantity * e.ProductItemFk.Size);
                    }
                    else
                    {
                        List<int> ids = _jobsRepository.GetAll().Where(e => e.LeadFk.AssignToUserID == job.Id && e.CommitionDate == job.CommitionDate).Select(e => e.Id).ToList();

                        job.jobs = ids;
                        job.NoOfPanel = _jobProductItemRepository.GetAll().Where(e => Panels.Contains((int)e.ProductItemId) && ids.Contains((int)e.JobId)).Select(e => e.Quantity).Sum();
                        job.BatteryKw = _jobProductItemRepository.GetAll().Where(e => e.ProductItemFk.ProductTypeId == 5 && ids.Contains((int)e.JobId)).Sum(e => e.Quantity * e.ProductItemFk.Size);

                    }

                }
                totalCount = job_list.Count();

            }


            SummaryCommissionReport commissionReport = new SummaryCommissionReport();
            if (result.Count() > 0)
            {
                commissionReport.TotalJobCount = result.Sum(e => e.JobCount);
                commissionReport.TotalCommission = result.Sum(e => e.Commission);
                commissionReport.TotalNoOfPanel = result.Sum(e => e.NoOfPanel);
                commissionReport.TotalSystemCapacity = result.Sum(e => e.SystemCapacity);
                commissionReport.TotalBatteryKw = result.Sum(e => e.BatteryKw);
                result[0].summary = commissionReport;
            }

            
            var ListDtos =  result;

            return _commissionReportExcelRepository.ExportToFile(ListDtos);
        }
    }
}
