﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.HoldReasons.Exporting;
using TheSolarProduct.HoldReasons.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.ServiceCategorys.Dtos;
using TheSolarProduct.ServiceSubCategorys.Dtos;
using TheSolarProduct.ServiceCategorys;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.ServiceCategoryDocs;
using TheSolarProduct.Storage;
using Abp.UI;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.Common;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Migrations;
using TheSolarProduct.ServiceTypes;

namespace TheSolarProduct.ServiceSubCategorys
{
    [AbpAuthorize(AppPermissions.Pages_ServiceSubCategory, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    public class ServiceSubCategorysAppService : TheSolarProductAppServiceBase, IServiceSubCategorysAppService
    {
        private const int MaxFileBytes = 5242880; //5MB
        private readonly IRepository<ServiceSubCategory> _serviceSubCategorysRepository;
        private readonly IHoldReasonsExcelExporter _serviceCategorysExcelExporter;
        private readonly IRepository<ServiceCategory> _serviceCategorysRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<ServiceCategoryDocs.ServiceCategoryDoc> _SubCategoryDocsRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public ServiceSubCategorysAppService(IRepository<ServiceSubCategory> serviceSubCategorysRepository,
            IHoldReasonsExcelExporter serviceCategorysExcelExporter,
            IRepository<ServiceCategory> serviceCategorysRepository,
            IRepository<User, long> userRepository,
            IRepository<ServiceCategoryDocs.ServiceCategoryDoc> SubCategoryDocsRepository,
            ITempFileCacheManager tempFileCacheManager,
            IWebHostEnvironment env,
             IRepository<Tenant> tenantRepository,
             ICommonLookupAppService CommonDocumentSaveRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            )
        {
            _serviceSubCategorysRepository = serviceSubCategorysRepository;
            _serviceCategorysExcelExporter = serviceCategorysExcelExporter;
            _serviceCategorysRepository = serviceCategorysRepository;
            _userRepository = userRepository;
            _SubCategoryDocsRepository = SubCategoryDocsRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _env = env;
            _tenantRepository = tenantRepository;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task<PagedResultDto<GetServiceSubCategoryForViewDto>> GetAll(GetAllServiceSubCategoryInput input)
        {

            var filteredHoldReasons = _serviceSubCategorysRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter)
                        .WhereIf(input.ServiceCategoryFilter != 0, e => e.ServiceCategoryId == input.ServiceCategoryFilter);

            var pagedAndFilteredHoldReasons = filteredHoldReasons
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var serviceSubCategory = from o in pagedAndFilteredHoldReasons
                              select new GetServiceSubCategoryForViewDto()
                              {
                                  serviceSubCategory = new ServiceSubCategoryDto
                                  {
                                      Name = o.Name,
                                      Id = o.Id,
                                      ServiceCategoryName = _serviceCategorysRepository.GetAll().Where(e=>e.Id == o.ServiceCategoryId).Select(e=>e.Name).FirstOrDefault(),
                                      IsActive = o.IsActive,
                                  }
                              };

            var totalCount = await filteredHoldReasons.CountAsync();

            return new PagedResultDto<GetServiceSubCategoryForViewDto>(
                totalCount,
                await serviceSubCategory.ToListAsync()
            );
        }

        public async Task<GetServiceSubCategoryForViewDto> GetServiceSubCategoryForView(int id)
        {
            var serviceSubCategory = await _serviceSubCategorysRepository.GetAsync(id);

            var output = new GetServiceSubCategoryForViewDto { serviceSubCategory = ObjectMapper.Map<ServiceSubCategoryDto>(serviceSubCategory) };
            output.serviceSubCategory.ServiceCategoryName = _serviceCategorysRepository.GetAll().Where(e => e.Id == serviceSubCategory.ServiceCategoryId).Select(e => e.Name).FirstOrDefault();
            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceSubCategory_Edit)]
        public async Task<GetServiceSubCategoryForEditOutput> GetServiceSubCategoryForEdit(EntityDto input)
        {
            var serviceSubCategory = await _serviceSubCategorysRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetServiceSubCategoryForEditOutput { serviceSubCategorys = ObjectMapper.Map<CreateOrEditServiceSubCategoryDto>(serviceSubCategory) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditServiceSubCategoryDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceSubCategory_Create)]
        protected virtual async Task Create(CreateOrEditServiceSubCategoryDto input)
        {
            var serviceSubcategory = ObjectMapper.Map<ServiceSubCategory>(input);

            if (AbpSession.TenantId != null)
            {
                serviceSubcategory.TenantId = (int)AbpSession.TenantId;
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 36;
            dataVaultLog.ActionNote = "Service SubCategory Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _serviceSubCategorysRepository.InsertAsync(serviceSubcategory);
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceSubCategory_Edit)]
        protected virtual async Task Update(CreateOrEditServiceSubCategoryDto input)
        {
            var serviceSubcategory = await _serviceSubCategorysRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 36;
            dataVaultLog.ActionNote = "Service SubCategory Updated : " + serviceSubcategory.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != serviceSubcategory.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = serviceSubcategory.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.ServiceCategoryId != serviceSubcategory.ServiceCategoryId)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "ServiceCategory";
                history.PrevValue = serviceSubcategory.ServiceCategoryId > 0 ?_serviceCategorysRepository.Get(serviceSubcategory.ServiceCategoryId).Name : "";
                history.CurValue = input.ServiceCategoryId > 0 ? _serviceCategorysRepository.Get(input.ServiceCategoryId).Name : "";
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != serviceSubcategory.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = serviceSubcategory.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, serviceSubcategory);
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceSubCategory_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _serviceSubCategorysRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 36;
            dataVaultLog.ActionNote = "Service SubCategory Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _serviceSubCategorysRepository.DeleteAsync(input.Id);
        }

        public async Task<List<GetSubCategoryDocs>> GetallSubCategorydoc(int? subcategoryid)
        {
            var servicess = (from o in _SubCategoryDocsRepository.GetAll().Where(e => e.SubCategoryId == subcategoryid)
                             select new GetSubCategoryDocs
                             {
                                 Id = o.Id,
                                 FileName = o.FileName,
                                 FilePath = o.FilePath,
                                 CreationTime = o.CreationTime,
                                 CreationUser = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                             }).ToList();

            return servicess;
        }

        public async Task SaveSubCategoryDocument(string FileToken, string FileName, int id)
        {
            var ext = Path.GetExtension(FileName);
            string FileNames = DateTime.Now.Ticks + "_" + "ServiceSubCategory" + ext;
            byte[] ByteArray = _tempFileCacheManager.GetFile(FileToken);
            var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, FileNames, "ServiceSubCategory", 0, 0, AbpSession.TenantId);
            ServiceCategoryDocs.ServiceCategoryDoc serviceCategoryDocs = new ServiceCategoryDocs.ServiceCategoryDoc();
            serviceCategoryDocs.FileName = FileNames;
            serviceCategoryDocs.FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\ServiceSubCategory\\";
            serviceCategoryDocs.SubCategoryId = id;
            serviceCategoryDocs.TenantId = (int)AbpSession.TenantId;
            await _SubCategoryDocsRepository.InsertAsync(serviceCategoryDocs);

        }

        public async Task DeleteSubCategoryDoc(int? id)
        {
            var docs = _SubCategoryDocsRepository.GetAll().Where(e => e.Id == id).FirstOrDefault();

            var Path = _env.WebRootPath;
            var MainFolder = Path;
            var fileSavePath = Path + docs.FilePath + docs.FileName;
            if (System.IO.File.Exists(fileSavePath))
            {
                try
                {
                    await _serviceSubCategorysRepository.DeleteAsync((int)id);
                    System.IO.File.Delete(fileSavePath);
                }
                catch (Exception ex)
                {
                    Exception e;
                }
            }
        }

    }
}