﻿using Abp.Authorization.Users;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.TheSolarDemo.Exporting;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.TheSolarDemo.Dtos;
using System.Threading.Tasks;
using TheSolarProduct.CheckApplications.Dtos;
using Abp.Notifications;
using TheSolarProduct.Expense.Dtos;
using TheSolarProduct.Expense;
using Abp.Application.Services.Dto;
using TheSolarProduct.States.Dtos;
using Abp;
using System.Linq;
using TheSolarProduct.TheSolarProduct.Dtos;
using Abp.Linq.Extensions;
using Abp.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using TheSolarDemo.TheSolarDemo.Exporting;
using TheSolarProduct.Dto;
using TheSolarProduct.CheckApplications.Exporting;


namespace TheSolarProduct.CheckApplications
{


    [AbpAuthorize(AppPermissions.Pages_ChcekApplication)]
    public class CheckApplicationsAppService : TheSolarProductAppServiceBase, ICheckApplicationAppService
    {
        private readonly ICheckApplicationExcelExporter _checkApplicationsExcelExporter;

        private readonly IRepository<CheckApplication> _checkApplicationRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public CheckApplicationsAppService(
              IRepository<CheckApplication> checkApplicationRepository,
              ICheckApplicationExcelExporter ICheckApplicationExcelExporter,
              IRepository<UserTeam> userTeamRepository,
              IRepository<UserRole, long> userRoleRepository,
              IRepository<Role> roleRepository,
              IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
              )
        {
            _checkApplicationRepository = checkApplicationRepository;
            _checkApplicationsExcelExporter = ICheckApplicationExcelExporter;
            _userTeamRepository = userTeamRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task<PagedResultDto<GetCheckApplicationForViewDto>> GetAll(GetAllCheckApplicationsInput input)
        {

            var CheckApplication = _checkApplicationRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFilteredChcekApplication = CheckApplication
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var outputCheckApplication = from o in pagedAndFilteredChcekApplication
                                         select new GetCheckApplicationForViewDto()
                                         {
                                             ChcekApplication = new ChcekApplicationDto
                                             {
                                                 Id = o.Id,
                                                 Name = o.Name,
                                                 IsActive = o.IsActive
                                             }
                                         };

            var totalCount = await CheckApplication.CountAsync();

            return new PagedResultDto<GetCheckApplicationForViewDto>(
                totalCount,
                await outputCheckApplication.ToListAsync()
            );
        }

        //public async Task<GetCheckApplicationForViewDto> GetCheckApplicationForView(int id)
        //{
        //    var ChcekApplication = await _checkApplicationRepository.GetAsync(id);

        //    var output = new GetCheckApplicationForViewDto { ChcekApplication = ObjectMapper.Map<ChcekApplicationDto>(ChcekApplication) };

        //    return output;
        //}

        [AbpAuthorize(AppPermissions.Pages_CheckApplication_Edit)]
        public async Task<GetCheckApplicationForEditOutput> GetCheckApplicationForEdit(EntityDto input)
        {
            var CheckApplication = await _checkApplicationRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetCheckApplicationForEditOutput { ChcekApplications = ObjectMapper.Map<CreateOrEditCheckApplicationDto>(CheckApplication) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditCheckApplicationDto input)
        {


            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_CheckApplication_Create)]

        protected virtual async Task Create(CreateOrEditCheckApplicationDto input)
        {
            var checkApplication = ObjectMapper.Map<CheckApplication>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 56;
            dataVaultLog.ActionNote = "CheckApplication Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _checkApplicationRepository.InsertAsync(checkApplication);
        }


        [AbpAuthorize(AppPermissions.Pages_CheckApplication_Edit)]
        protected virtual async Task Update(CreateOrEditCheckApplicationDto input)
        {
            var CheckApplication = await _checkApplicationRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 56;
            dataVaultLog.ActionNote = "CheckApplication Updated : " + CheckApplication.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != CheckApplication.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = CheckApplication.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != CheckApplication.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = CheckApplication.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, CheckApplication);

            await _checkApplicationRepository.UpdateAsync(CheckApplication);


        }

        [AbpAuthorize(AppPermissions.Pages_CheckApplication_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _checkApplicationRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 56;
            dataVaultLog.ActionNote = "CheckApplication Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _checkApplicationRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_CheckApplication_Export)]

        public async Task<FileDto> GetChcekApplicationToExcel(GetAllChcekApplicationForExcelInput input)
        {

            var filteredCheckApplication = _checkApplicationRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredCheckApplication
                         select new GetCheckApplicationForViewDto()
                         {
                             ChcekApplication = new ChcekApplicationDto
                             {
                                 Name = o.Name,
                                 Id = o.Id,
                                 IsActive = o.IsActive
                             }
                         });


            var ListDtos = await query.ToListAsync();

            return _checkApplicationsExcelExporter.ExportToFile(ListDtos);
        }
    }
}