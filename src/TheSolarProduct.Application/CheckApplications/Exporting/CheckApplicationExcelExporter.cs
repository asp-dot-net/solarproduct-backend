﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckApplications.Dtos;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.TheSolarDemo.Dtos;
using TheSolarProduct.TheSolarDemo.Exporting;

namespace TheSolarProduct.CheckApplications.Exporting
{
    public class CheckApplicationExcelExporter : NpoiExcelExporterBase, ICheckApplicationExcelExporter
    {


        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public CheckApplicationExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetCheckApplicationForViewDto> checkApplications)
        {
            return CreateExcelPackage(
                "CheckApplication.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("CheckApplication"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("IsActive")
                    );

                    AddObjects(
                        sheet,
                        2,
                        checkApplications,
                        _ => _.ChcekApplication.Name,
                        _ => _.ChcekApplication.IsActive ? L("Yes") : L("No")
                    );
                }
            );
        }


    }
}
