﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckApplications.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.CheckApplications.Exporting
{
    public interface ICheckApplicationExcelExporter
    {
        FileDto ExportToFile(List<GetCheckApplicationForViewDto> checkApplication);
    }
}
