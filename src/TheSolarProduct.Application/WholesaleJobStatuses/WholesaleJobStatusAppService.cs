﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.WholesaleJobStatuses.Dtos;
using TheSolarProduct.Wholesales.DataVault;
using TheSolarProduct.Wholesales.Ecommerces;
using Abp.Collections.Extensions;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using TheSolarProduct.WholesalePropertyTypes.Dtos;
using TheSolarProduct.Jobs;


namespace TheSolarProduct.WholesaleJobStatuses
{
    public class WholesaleJobStatusAppService : TheSolarProductAppServiceBase, IWholesaleJobStatusAppService
    {
        private readonly IRepository<WholesaleJobStatus> _wholesaleJobStatusRepository;

        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;

        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public WholesaleJobStatusAppService(

            IRepository<WholesaleJobStatus> WholesaleJobStatusRepository,
             // IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
             IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)

        {


            _wholesaleJobStatusRepository = WholesaleJobStatusRepository;
            // _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;

        }

        public async Task<PagedResultDto<GetWholesaleJobStatusForViewDto>> GetAll(GetAllWholesaleJobStatusInput input)
        {
            var jobstatus = _wholesaleJobStatusRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFiltered = jobstatus
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFiltered
                         select new GetWholesaleJobStatusForViewDto()
                         {
                             JobStatus = new WholesaleJobStatusDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 IsActive = o.IsActive
                             }
                         };

            var totalCount = await jobstatus.CountAsync();

            return new PagedResultDto<GetWholesaleJobStatusForViewDto>(totalCount, await output.ToListAsync());
        }

        public async Task<GetWholesaleJobStatusForEditOutput> GetWholesaleJobStatusForEdit(EntityDto input)
        {
            var jobstatus = await _wholesaleJobStatusRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetWholesaleJobStatusForEditOutput { JobStatus = ObjectMapper.Map<CreateOrEditWholesaleJobStatusDto>(jobstatus) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditWholesaleJobStatusDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        protected virtual async Task Create(CreateOrEditWholesaleJobStatusDto input)
        {
            var jobstatus = ObjectMapper.Map<WholesaleJobStatus>(input);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 20;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "WholesaleJobStatus Created: " + input.Name;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

            await _wholesaleJobStatusRepository.InsertAsync(jobstatus);
        }

        protected virtual async Task Update(CreateOrEditWholesaleJobStatusDto input)
        {
            var jobstatus = await _wholesaleJobStatusRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 20;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "WholesaleJobStatus Updated : " + jobstatus.Name;
            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            var List = new List<WholeSaleDataVaultActivityLogHistory>();
            if (input.Name != jobstatus.Name)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = jobstatus.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != jobstatus.IsActive)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = jobstatus.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, jobstatus);

            await _wholesaleJobStatusRepository.UpdateAsync(jobstatus);
        }
        public async Task Delete(EntityDto input)
        {
            var Name = _wholesaleJobStatusRepository.Get(input.Id).Name;

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 20;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "WholesaleJobStatus Deleted  : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            await _wholesaleJobStatusRepository.DeleteAsync(input.Id);
        }

    }

}

