﻿using System.Collections.Generic;
using Abp;
using TheSolarProduct.Chat.Dto;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Chat.Exporting
{
    public interface IChatMessageListExcelExporter
    {
        FileDto ExportToFile(UserIdentifier user, List<ChatMessageExportDto> messages);
    }
}
