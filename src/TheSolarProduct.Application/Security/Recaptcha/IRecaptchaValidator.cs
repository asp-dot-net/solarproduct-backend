using System.Threading.Tasks;

namespace TheSolarProduct.Security.Recaptcha
{
    public interface IRecaptchaValidator
    {
        Task ValidateAsync(string captchaResponse);
    }
}