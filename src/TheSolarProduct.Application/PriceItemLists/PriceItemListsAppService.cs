﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Authorization;
using TheSolarProduct.PriceItemLists.Dtos;
using System.Threading.Tasks;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.PriceItemLists
{
    [AbpAuthorize]
    public class PriceItemListsAppService : TheSolarProductAppServiceBase, IPriceItemListsAppService
    {
        private readonly IRepository<PriceItemList> _priceItemListsRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public PriceItemListsAppService(IRepository<PriceItemList> priceItemListsRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _priceItemListsRepository = priceItemListsRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
        }

        public async Task<PagedResultDto<GetPriceItemListsForViewDto>> GetAll(GetAllPriceItemListsInput input)
        {

            var filteredPriceItemLists = _priceItemListsRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Name), e => e.Name == input.Name);

            var pagedAndFilteredServiceSources = filteredPriceItemLists
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var holdReasons = from o in pagedAndFilteredServiceSources
                              select new GetPriceItemListsForViewDto()
                              {
                                  PriceItemLists = new PriceItemListsDto
                                  {
                                      Name = o.Name,
                                      Id = o.Id,
                                      IsActive = o.IsActive,
                                  }
                              };

            var totalCount = await filteredPriceItemLists.CountAsync();

            return new PagedResultDto<GetPriceItemListsForViewDto>(
                totalCount,
                await holdReasons.ToListAsync()
            );
        }

        public async Task<GetPriceItemListsForViewDto> GetPriceItemListsForView(int id)
        {
            var servicesources = await _priceItemListsRepository.GetAsync(id);

            var output = new GetPriceItemListsForViewDto { PriceItemLists = ObjectMapper.Map<PriceItemListsDto>(servicesources) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_PriceItemLists_Edit)]
        public async Task<GetPriceItemListsForEditOutput> GetPriceItemListsForEdit(EntityDto input)
        {
            var servicesources = await _priceItemListsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPriceItemListsForEditOutput { PriceItemLists = ObjectMapper.Map<CreateOrEditPriceItemListsDto>(servicesources) };

            return output;
        }
        public async Task CreateOrEdit(CreateOrEditPriceItemListsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_PriceItemLists_Create)]
        protected virtual async Task Create(CreateOrEditPriceItemListsDto input)
        {
            var servicesources = ObjectMapper.Map<PriceItemList>(input);

            //if (AbpSession.TenantId != null)
            //{
            //    servicesources.TenantId = (int)AbpSession.TenantId;
            //}
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 38;
            dataVaultLog.ActionNote = "Price Item Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _priceItemListsRepository.InsertAsync(servicesources);
        }

        [AbpAuthorize(AppPermissions.Pages_PriceItemLists_Edit)]
        protected virtual async Task Update(CreateOrEditPriceItemListsDto input)
        {
            var servicesources = await _priceItemListsRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 38;
            dataVaultLog.ActionNote = "Price Item Updated : " + servicesources.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != servicesources.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = servicesources.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.IsActive != servicesources.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = servicesources.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, servicesources);
        }

        [AbpAuthorize(AppPermissions.Pages_PriceItemLists_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _priceItemListsRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 38;
            dataVaultLog.ActionNote = "Price Item Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _priceItemListsRepository.DeleteAsync(input.Id);
        }
    }
}
