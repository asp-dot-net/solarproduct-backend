﻿using Abp;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Net.Mail;
using Abp.Notifications;
using Abp.Organizations;
using Abp.Timing;
using Abp.Timing.Timezone;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using NUglify.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.Authorization;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.CancelReasons;
using TheSolarProduct.Dto;
using TheSolarProduct.EmailTemplates;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Invoices;
using TheSolarProduct.JobHistory;
using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.LeadActions;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.LeadHistory;
using TheSolarProduct.Leads;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.Leads.Exporting;
using TheSolarProduct.LeadSources;
using TheSolarProduct.LeadStatuses;
using TheSolarProduct.MyInstallerActivityLogs;
using TheSolarProduct.Notifications;
using TheSolarProduct.Organizations;
using TheSolarProduct.PostCodes;
using TheSolarProduct.PreviousJobStatuses;
using TheSolarProduct.Promotions;
using TheSolarProduct.Quotations;
using TheSolarProduct.RejectReasons;
using TheSolarProduct.ReviewTypes;
using TheSolarProduct.Sections;
using TheSolarProduct.SmsTemplates;
using TheSolarProduct.States;
using TheSolarProduct.StreetNames;
using TheSolarProduct.StreetTypes;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.ThirdPartyApis.ExternalApis.Dtos;
using TheSolarProduct.Timing;
using TheSolarProduct.UnitTypes;
using TheSolarProduct.UserWiseEmailOrgs;

namespace TheSolarProduct.ThirdPartyApis.ExternalApis
{
    public class ExternalAppService : TheSolarProductAppServiceBase, IExternalAppService
    {
        private readonly IRepository<User, long> _userRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<LeadSource, int> _leadSourceRepository;
        private readonly IRepository<State, int> _stateRepository;
        private readonly IRepository<PostCode, int> _postCodeRepository;
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<Job> _jobRepository;
        private readonly IAppNotifier _appNotifier;

        public ExternalAppService(
            IRepository<User, long> userRepository,
            UserManager userManager,
            IRepository<LeadSource, int> leadSourceRepository,
            IRepository<State, int> stateRepository,
            IRepository<PostCode, int> postCodeRepository,
            IRepository<Lead> leadRepository,
            IRepository<LeadActivityLog> leadactivityRepository,
            IRepository<Job> jobRepository,
            IAppNotifier appNotifier
            )
        {
            _userRepository = userRepository;
            _userManager = userManager;
            _leadSourceRepository = leadSourceRepository;
            _stateRepository = stateRepository;
            _postCodeRepository = postCodeRepository;
            _leadRepository = leadRepository;
            _leadactivityRepository = leadactivityRepository;
            _jobRepository = jobRepository;
            _appNotifier = appNotifier;
        }

        public async Task ImportExternalLead(CreateOrEditForExternalLeadDto input)
        {
            using (CurrentUnitOfWork.SetTenantId(input.TenantId))
            {
                var userId = 1; // Default Admin

                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == userId).FirstOrDefault();

                IList<string> role = await _userManager.GetRolesAsync(assignedToUser);

                input.LeadSourceId = _leadSourceRepository.GetAll().Where(e => e.Name == input.LeadSource).Select(e => e.Id).FirstOrDefault();
                if (input.StateId == 0)
                {
                    input.StateId = _stateRepository.GetAll().Where(e => e.Name == input.State).Select(e => e.Id).FirstOrDefault();
                }

                if (input.SuburbId == null || input.SuburbId == 0)
                {
                    input.StateId = _postCodeRepository.GetAll().Where(e => e.PostalCode == input.PostCode).Select(e => e.Id).FirstOrDefault();
                }

                var lead = ObjectMapper.Map<Lead>(input);
                if (input.IsExternalLead == 3)
                {
                    lead.Address = input.Address;
                }
                else
                {
                    lead.Address = input.UnitNo + " " + input.UnitType + " " + input.StreetNo + " " + input.StreetName + " " + input.StreetType;
                }

                lead.IsPromotion = true;
                if (!string.IsNullOrWhiteSpace(input.Suburb))
                {
                    string[] splitPipeValue = input.Suburb.Split('|');
                    if (splitPipeValue.Length > 0)
                    {
                        lead.Suburb = splitPipeValue[0].ToString();
                    }
                    else
                    {
                        lead.Suburb = input.Suburb;
                    }
                }

                //1. Create
                //2. Excel Import
                //3. External Link
                //4. Copy Lead
                lead.IsExternalLead = input.IsExternalLead;
                lead.LeadAssignDate = DateTime.UtcNow;

                if (input.from == "mylead")
                {
                    lead.AssignToUserID = userId;
                    lead.LeadStatusId = 2;
                }
                else if (input.from == "myleads")
                {
                    lead.AssignToUserID = userId;
                    lead.LeadStatusId = 2;
                }
                else
                {
                    lead.LeadStatusId = 1;
                }

                //Start Check Duplicate
                //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
                var dbDup = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationId && e.AssignToUserID != null && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == input.Mobile && !string.IsNullOrEmpty(e.Mobile)))).FirstOrDefault();
                if (dbDup != null)
                {
                    var dbDupLeads = await _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationId && e.AssignToUserID != null && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == input.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Select(e => e.Id).ToListAsync();

                    if (input.from == null)
                    {
                        var dbDupLeadCount = dbDupLeads.Count();
                        var dbDupLeadInstallCount = await _jobRepository.GetAll().CountAsync(e => e.JobStatusId == 8 && dbDupLeads.Contains((int)e.LeadId));

                        if (dbDupLeadCount != dbDupLeadInstallCount)
                        {
                            lead.IsDuplicate = true;
                            lead.DublicateLeadId = dbDup.Id;
                        }
                        else
                        {
                            lead.IsDuplicate = false;
                        }

                    }
                    else if (input.from == "myleads" || input.from == "mylead")
                    {
                        var dbDupLeadOther = await _leadRepository.GetAll().Where(e => e.AssignToUserID != userId && dbDupLeads.Contains(e.Id)).Select(e => e.Id).ToListAsync();

                        if (dbDupLeadOther.Count > 0)
                        {
                            var dbDupLeadInstallCount = await _jobRepository.GetAll().CountAsync(e => e.JobStatusId == 8 && dbDupLeadOther.Contains((int)e.LeadId));

                            if (dbDupLeadOther.Count != dbDupLeadInstallCount)
                            {
                                lead.IsDuplicate = true;
                                lead.DublicateLeadId = dbDup.Id;
                            }
                            else
                            {
                                lead.IsDuplicate = false;
                            }
                        }
                        else
                        {
                            lead.IsDuplicate = false;
                        }
                    }
                    else
                    {
                        lead.IsDuplicate = true;
                        lead.DublicateLeadId = dbDup.Id;
                    }
                }
                else
                {
                    lead.IsDuplicate = false;
                }

                //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                var webDup = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationId && e.AssignToUserID == null && e.HideDublicate != true && ((e.Email == input.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == input.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Any();
                if (webDup)
                {
                    lead.IsWebDuplicate = true;
                }
                else
                {
                    lead.IsWebDuplicate = false;
                }
                //End Check Duplicate
                await _leadRepository.InsertAndGetIdAsync(lead);

                //PreviousJobStatus jobStstus = new PreviousJobStatus();
                //jobStstus.LeadId = lead.Id;
                //if (AbpSession.TenantId != null)
                //{
                //    jobStstus.TenantId = (int)AbpSession.TenantId;
                //}
                //jobStstus.CurrentID = lead.LeadStatusId;
                //jobStstus.PreviousId = 0;
                //await _previousJobStatusRepository.InsertAsync(jobStstus);

                //1. Create
                //2. Excel Import
                //3. External Link
                //4. Copy Lead
                //if (lead.IsExternalLead == 3)
                //{
                //    var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == lead.CreatorUserId).FirstOrDefault();
                //    string msg = string.Format("New Lead Created {0}.", lead.CompanyName);
                //    await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
                //}

                var UserDetail = _userRepository.GetAll().Where(u => u.Id == lead.CreatorUserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 1;
                leadactivity.SectionId = 0;
                leadactivity.ActionNote = "Lead Created";
                leadactivity.LeadId = lead.Id;
                leadactivity.TenantId = input.TenantId;
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
        }
    }
}
