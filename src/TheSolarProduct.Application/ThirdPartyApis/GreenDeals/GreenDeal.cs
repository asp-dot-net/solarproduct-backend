﻿using Abp.Application.Services;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using IdentityModel.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.Unicode;
using System.Threading.Tasks;

namespace TheSolarProduct.ThirdPartyApis.GreenDeals
{
    public class GreenDeal
    {
        //private static string endPoint = "https://staging.greendeal.com.au/rapi/v1"; // Testing Env
        private static string endPoint = "https://www.greendeal.com.au/rapi/v1"; // Production Env

        private static string clientId = "0ca305bbc3760a640063e091b38662ded10d400a59e8f452defc7a1728385c72";
        private static string clientSecret = "1ec664d97ff86487517f9ed4cab9fb9eb62bd9e0af9953447a8690f86492c9f4";

        public GreenDeal()
        {
        }

        public static string GetEncryptedSignature(string timestamp)
        {
            string signature_string = "GD:" + clientId + timestamp + clientSecret;

            //using (MD5 md5 = MD5.Create())
            //{
            //    byte[] byteHash = md5.ComputeHash(Encoding.UTF8.GetBytes(signatureString));
            //    var hash = BitConverter.ToString(byteHash).Replace("-", "");

            //    return hash;
            //}

            string hash = BitConverter.ToString(MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(signature_string))).Replace("-", "");

            return hash.ToLower();
        }

        public static string GetTimeStamp(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-ddTHH:mm:ss:z");
        }

        public static async Task<string> GetSTCPrice()
        {
            var path = "/dictionaries/get_stc_price.json";

            var timestamp = GetTimeStamp(DateTime.UtcNow);
            var encryptedSignature = GetEncryptedSignature(timestamp);

            var content = new DefautlRequestPer()
            {
                client_id = clientId,
                signature = encryptedSignature,
                timestamp = timestamp
            };

            var temp = JsonConvert.SerializeObject(content, Formatting.Indented);
            HttpContent httpContent = new StringContent(temp.ToString(), Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, endPoint + path);
            request.Headers.Add("Accept", "*/*");
            request.Content = httpContent;
            var response = await client.SendAsync(request);
            
            bool IsSuccessJob = response.IsSuccessStatusCode;

            var res = await response.Content.ReadAsStringAsync();

            return res;
        }

        public class DefautlRequestPer
        {
            public string client_id { get; set; }
            
            public string signature { get; set; }
            
            public string timestamp { get; set; }
        }
    }
}
