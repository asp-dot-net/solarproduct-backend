﻿using Abp.Domain.Repositories;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Organizations;
using System.Linq;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Net.Http;
using TheSolarProduct.ThirdPartyApis.GreenDeals.Dtos;
using System.Security.Policy;
using System.Net.Http.Headers;
using Google.Apis.Auth.OAuth2;
using TheSolarProduct.Jobs;
using TheSolarProduct.Installer;
using System.DirectoryServices.ActiveDirectory;
using Castle.MicroKernel.Registration;
using static Castle.MicroKernel.ModelBuilder.Descriptors.InterceptorDescriptor;
using TheSolarProduct.Jobs.Dtos;
using Telerik.Reporting;
using System.Collections.Generic;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Leads;
using static Google.Apis.Requests.BatchRequest;
using System.Net;
using TheSolarProduct.StreetTypes;

namespace TheSolarProduct.ThirdPartyApis.GreenDeals
{
    public class GreenDealAppService : TheSolarProductAppServiceBase, IGreenDealAppService
    {
        //private static string endPoint = "https://staging.greendeal.com.au/rapi/v1"; // Testing Env
        private static string endPoint = "https://www.greendeal.com.au/rapi/v1"; // Production Env

        private readonly IRepository<STCProvider> _stcProviderRepository;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<InstallerDetail> _installerDetailRepository;
        private readonly IRepository<StreetType, int> _streetTypeRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<STCYearWiseRate> _stcYearWiseRateRepository;

        public GreenDealAppService(
            IRepository<STCProvider> stcProviderRepository,
            IRepository<Job> jobRepository,
            IRepository<InstallerDetail> installerDetailRepository,
            IRepository<StreetType, int> streetTypeRepository,
            IRepository<JobProductItem> jobProductItemRepository,
            IRepository<STCYearWiseRate> stcYearWiseRateRepository
            )
        {
            _stcProviderRepository = stcProviderRepository;
            _jobRepository = jobRepository;
            _installerDetailRepository = installerDetailRepository;
            _streetTypeRepository = streetTypeRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _stcYearWiseRateRepository = stcYearWiseRateRepository;
        }

        public static string GetEncryptedSignature(string timestamp, string clientId, string clientSecret)
        {
            string signature_string = "GD:" + clientId + timestamp + clientSecret;

            string hash = BitConverter.ToString(MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(signature_string))).Replace("-", "");

            return hash.ToLower();
        }

        public static string GetTimeStamp()
        {
            //return dateTime.ToString("yyyy-MM-ddTHH:mm:ss:z");
            return new DateTimeOffset(DateTime.UtcNow).ToString(@"yyyy-MM-ddTHH:mm:ss:z");
        }

        protected virtual async Task<HttpResponseMessage> PostAsync(string url, string content)
        {
            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var oTaskPostAsyncJob = await client.PostAsync(url, new StringContent(content.ToString(), Encoding.UTF8, "application/json"));

            return oTaskPostAsyncJob;
        }

        //public async Task<HttpResponseMessage> GetJobSerialNo()
        //{
        //    var url = endPoint + "/pvds/get_serial_numbers.json";

        //    var stcProvider = await _stcProviderRepository.GetAll().Where(e => e.Provider == "Green Deal").FirstOrDefaultAsync();

        //    var timestamp = GetTimeStamp();
        //    var encryptedSignature = GetEncryptedSignature(timestamp, stcProvider.UserId, stcProvider.Password);

        //    var temp = new
        //    {
        //        client_id = stcProvider.UserId,
        //        signature = encryptedSignature,
        //        timestamp = timestamp,
        //        id = "GWT703202"
        //    };

        //    var content = JsonConvert.SerializeObject(temp, Formatting.Indented);
        //    HttpContent httpContent = new StringContent(content.ToString(), Encoding.UTF8, "application/json");

        //    HttpClient client = new HttpClient();
        //    var request = new HttpRequestMessage(HttpMethod.Get, url);
        //    request.Headers.Add("Accept", "*/*");
        //    request.Content = httpContent;
        //    var response = await client.SendAsync(request);

        //    var jsonResponse = await response.Content.ReadAsStringAsync();

        //    return response;

        //    //bool IsSuccess = response.IsSuccessStatusCode;

        //    //if (IsSuccess)
        //    //{
        //    //    var jsonResponse = await response.Content.ReadAsStringAsync();
        //    //    RootObject rootObject = JsonConvert.DeserializeObject<RootObject>(jsonResponse);
        //    //}
        //}

        //public async Task<HttpResponseMessage> GetCECPanels()
        //{
        //    var url = endPoint + "/dictionaries/panel_brands.json";

        //    var stcProvider = await _stcProviderRepository.GetAll().Where(e => e.Provider == "Green Deal").FirstOrDefaultAsync();

        //    var timestamp = GetTimeStamp(DateTime.UtcNow);
        //    var encryptedSignature = GetEncryptedSignature(timestamp, stcProvider.UserId, stcProvider.Password);

        //    var temp = new
        //    {
        //        client_id = stcProvider.UserId,
        //        signature = encryptedSignature,
        //        timestamp = timestamp
        //    };

        //    var content = JsonConvert.SerializeObject(temp, Formatting.Indented);
        //    HttpContent httpContent = new StringContent(content.ToString(), Encoding.UTF8, "application/json");

        //    HttpClient client = new HttpClient();
        //    var request = new HttpRequestMessage(HttpMethod.Get, url);
        //    request.Headers.Add("Accept", "*/*");
        //    request.Content = httpContent;
        //    var response = await client.SendAsync(request);

        //    return response;

        //    //bool IsSuccess = response.IsSuccessStatusCode;

        //    //if (IsSuccess)
        //    //{
        //    //    var jsonResponse = await response.Content.ReadAsStringAsync();
        //    //    RootObject rootObject = JsonConvert.DeserializeObject<RootObject>(jsonResponse);
        //    //}
        //}

        //public async Task<HttpResponseMessage> GetCECInverters()
        //{
        //    var url = endPoint + "/dictionaries/inverter_brands.json";

        //    var stcProvider = await _stcProviderRepository.GetAll().Where(e => e.Provider == "Green Deal").FirstOrDefaultAsync();

        //    var timestamp = GetTimeStamp(DateTime.UtcNow);
        //    var encryptedSignature = GetEncryptedSignature(timestamp, stcProvider.UserId, stcProvider.Password);

        //    var temp = new
        //    {
        //        client_id = stcProvider.UserId,
        //        signature = encryptedSignature,
        //        timestamp = timestamp
        //    };

        //    var content = JsonConvert.SerializeObject(temp, Formatting.Indented);
        //    HttpContent httpContent = new StringContent(content.ToString(), Encoding.UTF8, "application/json");

        //    HttpClient client = new HttpClient();
        //    var request = new HttpRequestMessage(HttpMethod.Get, url);
        //    request.Headers.Add("Accept", "*/*");
        //    request.Content = httpContent;
        //    var response = await client.SendAsync(request);

        //    return response;
        //    //bool IsSuccess = response.IsSuccessStatusCode;

        //    //if (IsSuccess)
        //    //{
        //    //    var jsonResponse = await response.Content.ReadAsStringAsync();
        //    //    RootObject rootObject = JsonConvert.DeserializeObject<RootObject>(jsonResponse);
        //    //}
        //}

        //public async Task<HttpResponseMessage> GetCECBattteris()
        //{
        //    var url = endPoint + "/dictionaries/battery_storages.json";

        //    var stcProvider = await _stcProviderRepository.GetAll().Where(e => e.Provider == "Green Deal").FirstOrDefaultAsync();

        //    var timestamp = GetTimeStamp(DateTime.UtcNow);
        //    var encryptedSignature = GetEncryptedSignature(timestamp, stcProvider.UserId, stcProvider.Password);

        //    var temp = new
        //    {
        //        client_id = stcProvider.UserId,
        //        signature = encryptedSignature,
        //        timestamp = timestamp
        //    };

        //    var content = JsonConvert.SerializeObject(temp, Formatting.Indented);
        //    HttpContent httpContent = new StringContent(content.ToString(), Encoding.UTF8, "application/json");

        //    HttpClient client = new HttpClient();
        //    var request = new HttpRequestMessage(HttpMethod.Get, url);
        //    request.Headers.Add("Accept", "*/*");
        //    request.Content = httpContent;
        //    var response = await client.SendAsync(request);

        //    return response;
        //    bool IsSuccess = response.IsSuccessStatusCode;

        //    if (IsSuccess)
        //    {
        //        var jsonResponse = await response.Content.ReadAsStringAsync();
        //        RootObject rootObject = JsonConvert.DeserializeObject<RootObject>(jsonResponse);
        //    }
        //}

        public async Task<CreateOrUpdateGreenDealJobOutputDto> CreateOrUpdateGreenDealJob(int jobId, int providerId)
        {
            var output = new CreateOrUpdateGreenDealJobOutputDto();

            var jobs = _jobRepository.GetAll().Where(e => e.Id == jobId).Include(e => e.LeadFk).Include(e => e.HouseTypeFk).Include(e => e.JobTypeFk).FirstOrDefault();
            var installerDetails = _installerDetailRepository.GetAll().Where(e => e.UserId == jobs.InstallerId).FirstOrDefault();
            var designerDetails = _installerDetailRepository.GetAll().Where(e => e.UserId == jobs.DesignerId && e.IsDesi == true).FirstOrDefault();
            var electricianDetails = _installerDetailRepository.GetAll().Where(e => e.UserId == jobs.ElectricianId && e.IsElec == true).FirstOrDefault();
            var jobProductItem = _jobProductItemRepository.GetAll().Where(e => e.JobId == jobs.Id).Include(e => e.ProductItemFk).ToList();

            var isPanel = jobProductItem.Any(e => e.ProductItemFk.ProductTypeId == 1);
            var isInverter = jobProductItem.Any(e => e.ProductItemFk.ProductTypeId == 2);

            var validateJob = ValidateJobsAsync(jobs, installerDetails, designerDetails, electricianDetails, isPanel, isInverter);
            if (validateJob.Count == 0)
            {
                if (!string.IsNullOrEmpty(jobs.GWTId))
                {
                    var oJobSuccess = await UpdateGreenDealJob(jobs, installerDetails, designerDetails, electricianDetails, jobProductItem);
                    output.Code = oJobSuccess.Item1;
                    output.Message = oJobSuccess.Item2;
                }
                else
                {
                    var oJobSuccess = await CreateGreenDealJob(jobs, installerDetails, designerDetails, electricianDetails, jobProductItem);

                    output.Code = oJobSuccess.Item1;
                    output.Message = oJobSuccess.Item2;
                }
            }
            else
            {
                output.ErrorList = validateJob;
                output.Code = HttpStatusCode.Forbidden;
            }

            return output;
        }

        protected virtual List<ValidateJobOutputDto> ValidateJobsAsync(Job jobs, InstallerDetail installerDetail, InstallerDetail designerDetails, InstallerDetail electricianDetails, bool isPanel, bool isInverter)
        {
            var output = new List<ValidateJobOutputDto>();

            #region Validate Green Deal Required Fields
            if (string.IsNullOrEmpty(jobs.LeadFk.Type))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Property Type", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(jobs.HouseTypeFk.Name))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "House Type", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(jobs.NMINumber))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "NMI Number", ErrorMessage = "Required" });
            }
            else if (jobs.NMINumber.Length < 10 || jobs.NMINumber.Length > 11)
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "NMI Number", ErrorMessage = "The NMI Number must be 10-11 bits" });
            }

            if (!string.IsNullOrEmpty(jobs.UnitType) || !string.IsNullOrEmpty(jobs.UnitNo))
            {
                if (string.IsNullOrEmpty(jobs.UnitNo))
                {
                    output.Add(new ValidateJobOutputDto() { FieldName = "Install Address: Unit Unit No", ErrorMessage = "Required" });
                }
                if (string.IsNullOrEmpty(jobs.UnitType))
                {
                    output.Add(new ValidateJobOutputDto() { FieldName = "Install Address: Unit Type", ErrorMessage = "Required" });
                }
            }

            if (string.IsNullOrEmpty(jobs.StreetName))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Install Address: Street Name", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(jobs.StreetType))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Install Address: Street Type", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(jobs.Suburb))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Install Address: Suburb", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(jobs.State))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Install Address: State", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(jobs.PostalCode))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Install Address: Post Code", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(jobs.LeadFk.CompanyName))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Customer Address: Company Name", ErrorMessage = "Required" });
            }

            if (!string.IsNullOrEmpty(jobs.LeadFk.UnitType) || !string.IsNullOrEmpty(jobs.LeadFk.UnitNo))
            {
                if (string.IsNullOrEmpty(jobs.LeadFk.UnitNo))
                {
                    output.Add(new ValidateJobOutputDto() { FieldName = "Customer Address: Unit Unit No", ErrorMessage = "Required" });
                }
                if (string.IsNullOrEmpty(jobs.LeadFk.UnitType))
                {
                    output.Add(new ValidateJobOutputDto() { FieldName = "Customer Address: Unit Type", ErrorMessage = "Required" });
                }
            }

            if (string.IsNullOrEmpty(jobs.LeadFk.StreetName))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Customer Address: Street Name", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(jobs.LeadFk.StreetType))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Customer Address: Street Type", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(jobs.LeadFk.Suburb))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Customer Address: Suburb", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(jobs.LeadFk.State))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Customer Address: State", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(jobs.LeadFk.PostCode))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Customer Address: Post Code", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(jobs.LeadFk.Mobile))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Customer Address: Mobile", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(jobs.LeadFk.Email))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Customer Address: Email", ErrorMessage = "Required" });
            }

            var validJobTypes = new List<string> { "New System", "Replacement System", "Extension System", "Additional System" };
            if (jobs.JobTypeId == null)
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Job Type", ErrorMessage = "Required" });
            }
            else if (!validJobTypes.Contains(jobs.JobTypeFk.Name))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Job Type", ErrorMessage = "Invalid Value", Description = "Valid value are: New System, Replacement System, Extension System, Additional System" });
            }

            if (jobs.InstallationDate == null)
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Installation Date", ErrorMessage = "Required" });
            }

            if (jobs.JobTypeId == null && (jobs.JobTypeFk.Name == "Extension System" || jobs.JobTypeFk.Name == "Additional System"))
            {
                if (string.IsNullOrEmpty(jobs.OldSystemDetails))
                {
                    output.Add(new ValidateJobOutputDto() { FieldName = "Old System Details", ErrorMessage = "Required" });
                }
            }

            if (!string.IsNullOrEmpty(jobs.InstallationNotes))
            {
                if (jobs.InstallationNotes.Length < 7 || jobs.InstallationNotes.Length > 4000)
                {
                    output.Add(new ValidateJobOutputDto() { FieldName = "Installer Notes", ErrorMessage = "The Installer Notes must be between 7 and 4000 digits" });
                }
            }

            if (!isPanel && !isInverter)
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Product Item", ErrorMessage = "Required" });
            }
            else if (!isPanel)
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Product Item - Module", ErrorMessage = "Required" });
            }
            else if (!isInverter)
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Product Item - Inverter", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(installerDetail?.Name))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Installer Name", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(installerDetail?.Surname))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Installer Surname", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(installerDetail?.InstallerAccreditationNumber))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Installer Accreditation No", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(designerDetails?.Name))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Designer Name", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(designerDetails?.Surname))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Designer Surname", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(designerDetails?.InstallerAccreditationNumber))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Designer Accreditation No", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(electricianDetails?.Name))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Electrician Name", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(electricianDetails?.Surname))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Electrician Surname", ErrorMessage = "Required" });
            }

            if (string.IsNullOrEmpty(electricianDetails?.ElectricianLicenseNumber))
            {
                output.Add(new ValidateJobOutputDto() { FieldName = "Electrician License No", ErrorMessage = "Required" });
            }
            #endregion

            return output;
        }

        protected virtual async Task<(HttpStatusCode, string)> CreateGreenDealJob(Job jobs, InstallerDetail installerDetail, InstallerDetail designerDetails, InstallerDetail electricianDetails, List<JobProductItem> jobProductItem)
        {
            var url = endPoint + "/pvds.json";

            var stcProvider = await _stcProviderRepository.GetAll().Where(e => e.Provider == "Green Deal").FirstOrDefaultAsync();

            var timeStamp = GetTimeStamp();
            var encryptedSignature = GetEncryptedSignature(timeStamp, stcProvider.UserId, stcProvider.Password);

            var ownerStreetType = _streetTypeRepository.GetAll().Where(e => e.Name == jobs.LeadFk.StreetType).Select(e => e.Code).FirstOrDefault();
            var installStreetType = _streetTypeRepository.GetAll().Where(e => e.Name == jobs.StreetType).Select(e => e.Code).FirstOrDefault();

            var companyName = jobs.LeadFk.CompanyName.Split(' ');

            var isBattery = jobProductItem.Any(e => e.ProductItemFk.ProductTypeId == 5);

            var deemedYears = _stcYearWiseRateRepository.GetAll().Where(e => e.Year == jobs.InstallationDate.Value.Year).Select(e => e.Rate).FirstOrDefault();

            var panels = (from o in jobProductItem
                          where o.ProductItemFk.ProductTypeId == 1
                          select new
                          {
                              brand_id = o.ProductItemFk.GreenDealBrandId,
                              watts_per = o.ProductItemFk.Size,
                              qty = o.Quantity
                          }).ToList();

            var inverters = (from o in jobProductItem
                             where o.ProductItemFk.ProductTypeId == 2
                             select new
                             {
                                 brand_id = o.ProductItemFk.GreenDealBrandId,
                                 qty = o.Quantity
                             }).ToList();

            var batteries = (from o in jobProductItem
                             where o.ProductItemFk.ProductTypeId == 5
                             select new
                             {
                                 brand_id = o.ProductItemFk.GreenDealBrandId,
                                 qty = o.Quantity
                             }).ToList();

            var temp = new
            {
                client_id = stcProvider.UserId,
                signature = encryptedSignature,
                timestamp = timeStamp,

                //created_by = "",
                trade_mode = "Cash", // Required => (Valid values are: "Creadit" or "Cash")
                po_reference = jobs.JobNumber, // Required
                order_reference = jobs.JobNumber,
                gst = "false", // Required => (Valid values are: "true" or "false")

                install_address = new
                {
                    same_as_postal = "false",
                    property_type = jobs.LeadFk.Type == "Res" ? "Residential" : jobs.LeadFk.Type == "Com" ? "Commercial" : "", // Required => (Valid values are: "Residential", "School" or "Commercial")
                    story = jobs.HouseTypeId == 1 ? "Single story" : "Multi story", // Required => (Valid values are: "Single story" or "Multi story")
                    nmi = jobs.NMINumber, // Required
                    job_number = jobs.JobNumber,
                    unit_type = jobs.UnitType,
                    unit_number = jobs.UnitNo,
                    street_number = jobs.StreetNo,
                    street_name = jobs.StreetName, // Required
                    street_type = installStreetType, // Required
                    suburb = jobs.Suburb, // Required
                    state = jobs.State, // Required
                    post_code = jobs.PostalCode, // Required
                    latitude = jobs.Latitude,
                    longitude = jobs.Longitude,
                    //special_address = ""
                },

                owner_address = new
                {
                    owner_type = "Individual", // Required => (Valid values are: "Individual", "Corporate body", "Government body" or "Trustee")
                    first_name = companyName[0], // (Required when owner_type is "Individual" )
                    last_name = companyName[1], // (Required when owner_type is "Individual" )
                    //owner_type_name = "", // (Required when owner_type is "Corporate body", "Government body" or "Trustee" )
                    //owner_representative_name = "", // (Required when owner_type is "Corporate body", "Government body" or "Trustee" )
                    //owner_representative_position = "", // (Required when owner_type is "Corporate body", "Government body" or "Trustee" )
                    unit_type = jobs.LeadFk.UnitType,
                    unit_number = jobs.LeadFk.UnitNo,
                    street_number = jobs.LeadFk.StreetNo,
                    street_name = jobs.LeadFk.StreetName,
                    street_type = ownerStreetType,
                    suburb = jobs.LeadFk.Suburb, // Required
                    state = jobs.LeadFk.State, // Required
                    post_code = jobs.LeadFk.PostCode, // Required
                    address_type = "physical", // Required => (Valid values are: "physical" or "p_o_box")
                    phone = jobs.LeadFk.Mobile, // Required
                    email = jobs.LeadFk.Email, // Required
                    price_to_the_home_owner = jobs.STCPrice, // Required when owner_type is not "Individual" and property_type is "Commercial" or "School"
                    home_owner_abn = "" // Required when owner_type is not "Individual" and property_type is "Commercial" or "School"
                },

                system = new
                {
                    installation_type = jobs.JobTypeFk.Name.Replace(" ", "_").ToLower(), // Required (Valid values are: "new_system", "replacement_system", "extension_system", "additional_system")
                    connected_type = "on-grid", // Required (Valid values are: "on-grid", "off-grid")
                    is_battery = isBattery.ToString().ToLower(), // Required ("true" then batteries is required, "false" then batteries will not be saved)
                    install_date = jobs.InstallationDate.Value.Date.ToString("yyyy-MM-dd"), // Required Type Date 'yyyy-MM-dd'
                    install_on_building = "true", // Required => (Valid values are: "true" or "false")
                    deemed_years = deemedYears, // default 13
                    additional_capacity_details = jobs.OldSystemDetails, // Required when type is "extension_system" or "additional_system" Old System Detals
                    install_additional_information = jobs.InstallationNotes, //InstallationNotes

                    panels = panels,
                    inverters = inverters,
                    batteries = batteries
                    //panels = new List<dynamic>
                    //{
                    //    new {
                    //        brand_id = 0, // Required
                    //        watts_per = 0, // Required
                    //        qty = 0 // Required
                    //    }

                    //},

                    //inverters = new List<dynamic>
                    //{
                    //    new {
                    //        brand_id = 0, // Required
                    //        qty = 0 // Required
                    //    }
                    //},

                    //batteries = new List<dynamic>
                    //{
                    //    new {
                    //        brand_id = 0, // Required
                    //        qty = 0 // Required
                    //    }
                    //},
                },

                installer = new
                {
                    first_name = installerDetail.Name, // Required
                    last_name = installerDetail.Surname, // Required
                    accreditation = installerDetail.InstallerAccreditationNumber // Required
                },

                designer = new
                {
                    first_name = designerDetails.Name, // Required
                    last_name = designerDetails.Surname, // Required
                    accreditation = designerDetails.InstallerAccreditationNumber // Required
                },

                electrician = new
                {
                    first_name = electricianDetails.Name, // Required
                    last_name = electricianDetails.Surname, // Required
                    license = electricianDetails.ElectricianLicenseNumber // Required
                }
            };

            var content = JsonConvert.SerializeObject(temp, Formatting.Indented);

            var jsonResponse = await PostAsync(url, content);

            bool IsSuccessJob = jsonResponse.IsSuccessStatusCode;
            var JsonString = await jsonResponse.Content.ReadAsStringAsync();

            if (IsSuccessJob)
            {
                StcJobRootObject rootObject = JsonConvert.DeserializeObject<StcJobRootObject>(JsonString);

                jobs.GWTId = rootObject.Response.Job.GWTId.Trim();
                await _jobRepository.UpdateAsync(jobs);

                return ((HttpStatusCode)rootObject.Code, rootObject.Message);
            }
            else
            {
                ErrorRootObject rootObject = new ErrorRootObject();
                try
                {
                    rootObject = JsonConvert.DeserializeObject<ErrorRootObject>(JsonString);
                }
                catch
                {
                    rootObject.Code = 400;
                    rootObject.Error = "Something is wrong. Please contact admin";
                }

                return ((HttpStatusCode)rootObject.Code, rootObject.Message == null ? rootObject.Error : rootObject.Message.Parameter);
            }
        }

        protected virtual async Task<(HttpStatusCode, string)> UpdateGreenDealJob(Job jobs, InstallerDetail installerDetail, InstallerDetail designerDetails, InstallerDetail electricianDetails, List<JobProductItem> jobProductItem)
        {
            var stcProvider = await _stcProviderRepository.GetAll().Where(e => e.Provider == "Green Deal").FirstOrDefaultAsync();

            var timeStamp = GetTimeStamp();
            var encryptedSignature = GetEncryptedSignature(timeStamp, stcProvider.UserId, stcProvider.Password);

            var updateJobSummeryRes = await UpdateJobSummaryAsync(stcProvider.UserId, timeStamp, encryptedSignature, jobs);

            if (updateJobSummeryRes.Item1 == HttpStatusCode.OK)
            {
                var updateJobAddressRes = await UpdateJobAddressAsync(stcProvider.UserId, timeStamp, encryptedSignature, jobs);

                if (updateJobAddressRes.Item1 == HttpStatusCode.OK)
                {
                    var updateJobSystemRes = await UpdateJobSystemAsync(stcProvider.UserId, timeStamp, encryptedSignature, jobs, jobProductItem);
                    return (updateJobSystemRes.Item1, updateJobSystemRes.Item2);
                    //if(updateJobSystemRes.Item1 == HttpStatusCode.OK)
                    //{
                    //    var updateJobInstallerRes = await UpdateJobInstallerAsync(stcProvider.UserId, timeStamp, encryptedSignature, jobs, installerDetail, designerDetails, electricianDetails);

                    //    return (updateJobInstallerRes.Item1, updateJobInstallerRes.Item2);
                    //}
                    //else
                    //{
                    //    return (updateJobSystemRes.Item1, updateJobSystemRes.Item2);
                    //}
                }
                else
                {
                    return (updateJobAddressRes.Item1, updateJobAddressRes.Item2);
                }
            }
            else
            {
                return (updateJobSummeryRes.Item1, updateJobSummeryRes.Item2);
            }
        }

        protected virtual async Task<(HttpStatusCode, string)> UpdateJobSummaryAsync(string clientId, string timeStamp, string encryptedSignature, Job jobs)
        {
            var url = endPoint + "/pvds/update_summary.json";

            var temp = new
            {
                client_id = clientId,
                signature = encryptedSignature,
                timestamp = timeStamp,
                id = jobs.GWTId,

                //created_by = "",
                trade_mode = "Cash", // Required => (Valid values are: "Creadit" or "Cash")
                po_reference = jobs.JobNumber, // Required
                order_reference = jobs.JobNumber,
                gst = "true", // Required => (Valid values are: "true" or "false")
            };

            var content = JsonConvert.SerializeObject(temp, Formatting.Indented);

            var jsonResponse = await PostAsync(url, content);

            bool IsSuccessJob = jsonResponse.IsSuccessStatusCode;
            var JsonString = await jsonResponse.Content.ReadAsStringAsync();

            if (IsSuccessJob)
            {
                StcJobRootObject rootObject = JsonConvert.DeserializeObject<StcJobRootObject>(JsonString);

                return ((HttpStatusCode)rootObject.Code, rootObject.Message);
            }
            else
            {
                ErrorRootObject rootObject = JsonConvert.DeserializeObject<ErrorRootObject>(JsonString);
                return ((HttpStatusCode)rootObject.Code, rootObject.Message == null ? rootObject.Error : rootObject.Message.Parameter);
            }
        }

        protected virtual async Task<(HttpStatusCode, string)> UpdateJobAddressAsync(string clientId, string timeStamp, string encryptedSignature, Job jobs)
        {
            var url = endPoint + "/pvds/update_address.json";

            var ownerStreetType = _streetTypeRepository.GetAll().Where(e => e.Name == jobs.LeadFk.StreetType).Select(e => e.Code).FirstOrDefault();
            var installStreetType = _streetTypeRepository.GetAll().Where(e => e.Name == jobs.StreetType).Select(e => e.Code).FirstOrDefault();

            var companyName = jobs.LeadFk.CompanyName.Split(' ');

            var temp = new
            {
                client_id = clientId,
                signature = encryptedSignature,
                timestamp = timeStamp,
                id = jobs.GWTId,

                install_address = new
                {
                    same_as_postal = "false",
                    property_type = jobs.LeadFk.Type == "Res" ? "Residential" : jobs.LeadFk.Type == "Com" ? "Commercial" : "", // Required => (Valid values are: "Residential", "School" or "Commercial")
                    story = jobs.HouseTypeId == 1 ? "Single story" : "Multi story", // Required => (Valid values are: "Single story" or "Multi story")
                    nmi = jobs.NMINumber, // Required
                    job_number = jobs.JobNumber,
                    unit_type = jobs.UnitType,
                    unit_number = jobs.UnitNo,
                    street_number = jobs.StreetNo,
                    street_name = jobs.StreetName, // Required
                    street_type = installStreetType, // Required
                    suburb = jobs.Suburb, // Required
                    state = jobs.State, // Required
                    post_code = jobs.PostalCode, // Required
                    latitude = jobs.Latitude,
                    longitude = jobs.Longitude,
                },

                owner_address = new
                {
                    owner_type = "Individual", // Required => (Valid values are: "Individual", "Corporate body", "Government body" or "Trustee")
                    first_name = companyName[0], // (Required when owner_type is "Individual" )
                    last_name = companyName[1], // (Required when owner_type is "Individual" )
                    //owner_type_name = "", // (Required when owner_type is "Corporate body", "Government body" or "Trustee" )
                    //owner_representative_name = "", // (Required when owner_type is "Corporate body", "Government body" or "Trustee" )
                    //owner_representative_position = "", // (Required when owner_type is "Corporate body", "Government body" or "Trustee" )
                    unit_type = jobs.LeadFk.UnitType,
                    unit_number = jobs.LeadFk.UnitNo,
                    street_number = jobs.LeadFk.StreetNo,
                    street_name = jobs.LeadFk.StreetName,
                    street_type = ownerStreetType,
                    suburb = jobs.LeadFk.Suburb, // Required
                    state = jobs.LeadFk.State, // Required
                    post_code = jobs.LeadFk.PostCode, // Required
                    address_type = "physical", // Required => (Valid values are: "physical" or "p_o_box")
                    phone = jobs.LeadFk.Mobile, // Required
                    email = jobs.LeadFk.Email, // Required
                    //price_to_the_home_owner = "", // Required when owner_type is not "Individual" and property_type is "Commercial" or "School"
                    //home_owner_abn = "" // Required when owner_type is not "Individual" and property_type is "Commercial" or "School"
                },
            };

            var content = JsonConvert.SerializeObject(temp, Formatting.Indented);

            var jsonResponse = await PostAsync(url, content);

            bool IsSuccessJob = jsonResponse.IsSuccessStatusCode;
            var JsonString = await jsonResponse.Content.ReadAsStringAsync();

            if (IsSuccessJob)
            {
                StcJobRootObject rootObject = JsonConvert.DeserializeObject<StcJobRootObject>(JsonString);

                return ((HttpStatusCode)rootObject.Code, rootObject.Message);
            }
            else
            {
                ErrorRootObject rootObject = JsonConvert.DeserializeObject<ErrorRootObject>(JsonString);
                return ((HttpStatusCode)rootObject.Code, rootObject.Message == null ? rootObject.Error : rootObject.Message.Parameter);
            }
        }

        protected virtual async Task<(HttpStatusCode, string)> UpdateJobSystemAsync(string clientId, string timeStamp, string encryptedSignature, Job jobs, List<JobProductItem> jobProductItem)
        {
            var url = endPoint + "/pvds/update_system.json";

            var isBattery = jobProductItem.Any(e => e.ProductItemFk.ProductTypeId == 5);

            var deemedYears = _stcYearWiseRateRepository.GetAll().Where(e => e.Year == jobs.InstallationDate.Value.Year).Select(e => e.Rate).FirstOrDefault();

            var panelList = (from o in jobProductItem
                             where o.ProductItemFk.ProductTypeId == 1
                             select new
                             {
                                 brand_id = o.ProductItemFk.GreenDealBrandId,
                                 watts_per = o.ProductItemFk.Size,
                                 qty = o.Quantity
                             }).ToList();

            var inverterList = (from o in jobProductItem
                                where o.ProductItemFk.ProductTypeId == 2
                                select new
                                {
                                    brand_id = o.ProductItemFk.GreenDealBrandId,
                                    qty = o.Quantity
                                }).ToList();

            var batteryList = (from o in jobProductItem
                               where o.ProductItemFk.ProductTypeId == 5
                               select new
                               {
                                   brand_id = o.ProductItemFk.GreenDealBrandId,
                                   qty = o.Quantity
                               }).ToList();

            var temp = new
            {
                client_id = clientId,
                signature = encryptedSignature,
                timestamp = timeStamp,
                id = jobs.GWTId,

                system = new
                {
                    installation_type = jobs.JobTypeFk.Name.Replace(" ", "_").ToLower(), // Required (Valid values are: "new_system", "replacement_system", "extension_system", "additional_system")
                    connected_type = "on-grid", // Required (Valid values are: "on-grid", "off-grid")
                    is_battery = isBattery.ToString().ToLower(), // Required ("true" then batteries is required, "false" then batteries will not be saved)
                    install_date = jobs.InstallationDate.Value.Date.ToString("yyyy-MM-dd"), // Required Type Date 'yyyy-MM-dd'
                    install_on_building = "true", // Required => (Valid values are: "true" or "false")
                    deemed_years = deemedYears, // default 13
                    additional_capacity_details = jobs.OldSystemDetails, // Required when type is "extension_system" or "additional_system" Old System Detals
                    install_additional_information = jobs.InstallationNotes, //InstallerNOtes

                    panels = panelList,
                    inverters = inverterList,
                    batteries = batteryList
                },
            };

            var content = JsonConvert.SerializeObject(temp, Formatting.Indented);

            var jsonResponse = await PostAsync(url, content);

            bool IsSuccessJob = jsonResponse.IsSuccessStatusCode;
            var JsonString = await jsonResponse.Content.ReadAsStringAsync();

            if (IsSuccessJob)
            {
                StcJobRootObject rootObject = JsonConvert.DeserializeObject<StcJobRootObject>(JsonString);

                return ((HttpStatusCode)rootObject.Code, rootObject.Message);
            }
            else
            {
                ErrorRootObject rootObject = JsonConvert.DeserializeObject<ErrorRootObject>(JsonString);
                return ((HttpStatusCode)rootObject.Code, rootObject.Message == null ? rootObject.Error : rootObject.Message.Parameter);
            }
        }

        protected virtual async Task<(HttpStatusCode, string)> UpdateJobInstallerAsync(string clientId, string timeStamp, string encryptedSignature, Job jobs, InstallerDetail installerDetail, InstallerDetail designerDetails, InstallerDetail electricianDetails)
        {
            var url = endPoint + "/pvds/update_installer.json";

            var temp = new
            {
                client_id = clientId,
                signature = encryptedSignature,
                timestamp = timeStamp,
                id = jobs.GWTId,

                //installer = new { },
                //designer = new { },
                //electrician = new { }

                installer = new
                {
                    accreditation = installerDetail.InstallerAccreditationNumber, // Required
                    first_name = installerDetail.Name, // Required
                    last_name = installerDetail.Surname // Required
                },

                designer = new
                {
                    accreditation = designerDetails.InstallerAccreditationNumber, // Required
                    first_name = designerDetails.Name, // Required
                    last_name = designerDetails.Surname // Required
                },

                electrician = new
                {
                    license = electricianDetails.ElectricianLicenseNumber, // Required
                    first_name = electricianDetails.Name, // Required
                    last_name = electricianDetails.Surname, // Required
                }
            };

            var content = JsonConvert.SerializeObject(temp, Formatting.Indented);

            var jsonResponse = await PostAsync(url, content);

            bool IsSuccessJob = jsonResponse.IsSuccessStatusCode;
            var JsonString = await jsonResponse.Content.ReadAsStringAsync();

            if (IsSuccessJob)
            {
                StcJobRootObject rootObject = JsonConvert.DeserializeObject<StcJobRootObject>(JsonString);

                return ((HttpStatusCode)rootObject.Code, rootObject.Message);
            }
            else
            {
                ErrorRootObject rootObject = JsonConvert.DeserializeObject<ErrorRootObject>(JsonString);
                return ((HttpStatusCode)rootObject.Code, rootObject.Message == null ? rootObject.Error : rootObject.Message.Parameter);
            }
        }
    }
}
