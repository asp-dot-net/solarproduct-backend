﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EcommerceSpecifications.Dtos;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Wholesales.DataVault;
using TheSolarProduct.Wholesales.Ecommerces;
using Abp.Collections.Extensions;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using TheSolarProduct.Jobs;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using TheSolarProduct.PurchaseDocumentLists.Dtos;
using TheSolarProduct.PurchaseDocumentListes;


namespace TheSolarProduct.EcommerceSpecifications
{
    public class EcommerceSpecificationAppService : TheSolarProductAppServiceBase, IEcommerceSpecificationAppService
    {
        private readonly IRepository<EcommerceSpecification> _EcommerceSpecificationRepository;

        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;

        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<ProductType> _productTypeRepository;

        public EcommerceSpecificationAppService(

            IRepository<EcommerceSpecification> EcommerceSpecificationRepository,
             IRepository<ProductType> productTypeRepository,
             IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)

        {


            _EcommerceSpecificationRepository = EcommerceSpecificationRepository;
            // _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;
            _productTypeRepository = productTypeRepository;

        }


        public async Task<PagedResultDto<GetEcommerceSpecificationForViewDto>> GetAll(GetAllEcommerceSpecificationInput input)
        {
            
            var specifications = _EcommerceSpecificationRepository.GetAll()
                .WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

           
            var pagedAndFiltered = specifications
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var filteredAndPagedResults = await pagedAndFiltered.ToListAsync();

            
            var productTypeIds = filteredAndPagedResults
                .SelectMany(o => o.ProductType.Split(',').Select(int.Parse)) 
                .Distinct()
                .ToList();

            
            var productTypes = await _productTypeRepository.GetAll()
                .Where(pt => productTypeIds.Contains(pt.Id))
                .ToDictionaryAsync(pt => pt.Id, pt => pt.Name);

            
            var output = filteredAndPagedResults.Select(o => new GetEcommerceSpecificationForViewDto
            {
                Specification = new EcommerceSpecificationDto
                {
                    Id = o.Id,
                    Name = o.Name,
                    IsActive = o.IsActive,
                   
                    ProductType = o.ProductType
                        .Split(',')
                        .Select(id => productTypes.ContainsKey(int.Parse(id)) ? productTypes[int.Parse(id)] : string.Empty)
                        .ToList(),
                }
            }).ToList();

           
            var totalCount = await specifications.CountAsync();

            
            return new PagedResultDto<GetEcommerceSpecificationForViewDto>(totalCount, output);
        }


        public async Task<GetEcommerceSpecificationForEditOutput> GetEcommerceSpecificationForEdit(EntityDto input)
        {
            var epsecification = await _EcommerceSpecificationRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetEcommerceSpecificationForEditOutput { Specification = ObjectMapper.Map<CreateOrEditSpecificationDto>(epsecification) };
            output.Specification.ProductType = epsecification.ProductType.Split(',').ToList();
            return output;
        }


        public async Task CreateOrEdit(CreateOrEditSpecificationDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        protected virtual async Task Create(CreateOrEditSpecificationDto input)
        {
            var properytype = ObjectMapper.Map<EcommerceSpecification>(input);
            properytype.ProductType = string.Join(",", input.ProductType);
            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 21;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "Specification Created: " + input.Name;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

            await _EcommerceSpecificationRepository.InsertAsync(properytype);
        }

        protected virtual async Task Update(CreateOrEditSpecificationDto input)

        {
           
            var output = await _EcommerceSpecificationRepository.FirstOrDefaultAsync((int)input.Id);

           

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 21;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "Specification Updated : " + output.Name;
            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            var List = new List<WholeSaleDataVaultActivityLogHistory>();

            if (!Enumerable.SequenceEqual(
                                          input.ProductType.Select(s => s.Trim()),
                                                           output.ProductType.Split(',').Select(s => s.Trim())))
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Formats";
                history.PrevValue = output.ProductType;
                history.CurValue = string.Join(",", input.ProductType);
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }


            if (input.Name != output.Name)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = output.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != output.IsActive)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = output.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
           
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            output.ProductType = string.Join(",", input.ProductType);
            output.Name = input.Name;
            output.IsActive = input.IsActive;
            await _EcommerceSpecificationRepository.UpdateAsync(output);
        }
        public async Task Delete(EntityDto input)
        {
            var Name = _EcommerceSpecificationRepository.Get(input.Id).Name;

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 21;
            dataVaultLog.Type = "ECommerce";
            dataVaultLog.ActionNote = "Specification Deleted  : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            await _EcommerceSpecificationRepository.DeleteAsync(input.Id);
        }

    }


}

