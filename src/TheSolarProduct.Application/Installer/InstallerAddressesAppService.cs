﻿using TheSolarProduct.Authorization.Users;


using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Installer.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace TheSolarProduct.Installer
{
	[AbpAuthorize(AppPermissions.Pages_InstallerAddresses)]
    public class InstallerAddressesAppService : TheSolarProductAppServiceBase, IInstallerAddressesAppService
    {
		 private readonly IRepository<InstallerAddress> _installerAddressRepository;
		 private readonly IRepository<User,long> _lookup_userRepository;
		 

		  public InstallerAddressesAppService(IRepository<InstallerAddress> installerAddressRepository , IRepository<User, long> lookup_userRepository) 
		  {
			_installerAddressRepository = installerAddressRepository;
			_lookup_userRepository = lookup_userRepository;
		
		  }

		 public async Task<PagedResultDto<GetInstallerAddressForViewDto>> GetAll(GetAllInstallerAddressesInput input)
         {
			
			var filteredInstallerAddresses = _installerAddressRepository.GetAll()
						.Include( e => e.UserFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Unit.Contains(input.Filter) || e.UnitType.Contains(input.Filter) || e.StreetNo.Contains(input.Filter) || e.StreetName.Contains(input.Filter) || e.Suburb.Contains(input.Filter) || e.PostCode.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserFk != null && e.UserFk.Name == input.UserNameFilter);

			var pagedAndFilteredInstallerAddresses = filteredInstallerAddresses
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

			var installerAddresses = from o in pagedAndFilteredInstallerAddresses
                         join o1 in _lookup_userRepository.GetAll() on o.UserId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetInstallerAddressForViewDto() {
							InstallerAddress = new InstallerAddressDto
							{
                                Id = o.Id
							},
                         	UserName = s1 == null || s1.Name == null ? "" : s1.Name.ToString()
						};

            var totalCount = await filteredInstallerAddresses.CountAsync();

            return new PagedResultDto<GetInstallerAddressForViewDto>(
                totalCount,
                await installerAddresses.ToListAsync()
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_InstallerAddresses_Edit)]
		 public async Task<GetInstallerAddressForEditOutput> GetInstallerAddressForEdit(EntityDto input)
         {
            var installerAddress = await _installerAddressRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetInstallerAddressForEditOutput {InstallerAddress = ObjectMapper.Map<CreateOrEditInstallerAddressDto>(installerAddress)};

		    if (output.InstallerAddress.UserId != null)
            {
                var _lookupUser = await _lookup_userRepository.FirstOrDefaultAsync((long)output.InstallerAddress.UserId);
                output.UserName = _lookupUser?.Name?.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditInstallerAddressDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_InstallerAddresses_Create)]
		 protected virtual async Task Create(CreateOrEditInstallerAddressDto input)
         {
            var installerAddress = ObjectMapper.Map<InstallerAddress>(input);

			
			if (AbpSession.TenantId != null)
			{
				installerAddress.TenantId = (int) AbpSession.TenantId;
			}
		

            await _installerAddressRepository.InsertAsync(installerAddress);
         }

		 [AbpAuthorize(AppPermissions.Pages_InstallerAddresses_Edit)]
		 protected virtual async Task Update(CreateOrEditInstallerAddressDto input)
         {
            var installerAddress = await _installerAddressRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, installerAddress);
         }

		 [AbpAuthorize(AppPermissions.Pages_InstallerAddresses_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _installerAddressRepository.DeleteAsync(input.Id);
         } 

		[AbpAuthorize(AppPermissions.Pages_InstallerAddresses)]
         public async Task<PagedResultDto<InstallerAddressUserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _lookup_userRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Name != null && e.Name.Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var userList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<InstallerAddressUserLookupTableDto>();
			foreach(var user in userList){
				lookupTableDtoList.Add(new InstallerAddressUserLookupTableDto
				{
					Id = user.Id,
					DisplayName = user.Name?.ToString()
				});
			}

            return new PagedResultDto<InstallerAddressUserLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}