﻿using TheSolarProduct.Authorization.Users;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.IO.Extensions;
using Abp.UI;
using Abp.Web.Models;
using Microsoft.AspNetCore.Mvc;
using TheSolarProduct.Storage;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Abp.Domain.Repositories;
using TheSolarProduct.Installer.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using TheSolarProduct.MultiTenancy;
using System.IO;
using Abp.Authorization.Users;
using Abp.Organizations;
using TheSolarProduct.MyInstallerPriceItemLists;
using TheSolarProduct.PriceItemLists;
using Abp.AutoMapper;
using TheSolarProduct.Common;

namespace TheSolarProduct.Installer
{
    [AbpAuthorize(AppPermissions.Pages_InstallerDetails)]
    public class InstallerDetailsAppService : TheSolarProductAppServiceBase, IInstallerDetailsAppService
    {
        private const int MaxFileBytes = 5242880; //5MB
        private readonly IRepository<InstallerDetail> _installerDetailRepository;
        private readonly IRepository<User, long> _lookup_userRepository;
        private readonly TempFileCacheManager _tempFileCacheManager;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<OrganizationUnit, long> _OrgRepository;
        private readonly IRepository<InstallerContract> _InstallerContract;
        private readonly IRepository<MyInstallerPriceItemList> _MyInstallerPriceListRepository;
        private readonly IRepository<PriceItemList> _PriceItemListsRepository;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;

        public InstallerDetailsAppService(IRepository<InstallerDetail> installerDetailRepository,
            IRepository<User, long> lookup_userRepository,
            TempFileCacheManager tempFileCacheManager,
            IBinaryObjectManager binaryObjectManager,
            IWebHostEnvironment env,
            IRepository<Tenant> tenantRepository,
            IRepository<OrganizationUnit, long> OrgRepository,
            IRepository<InstallerContract> InstallerContract,
            IRepository<MyInstallerPriceItemList> MyInstallerPriceListRepository,
            IRepository<PriceItemList> PriceItemListsRepository,
            ICommonLookupAppService CommonDocumentSaveRepository)
        {
            _installerDetailRepository = installerDetailRepository;
            _lookup_userRepository = lookup_userRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _binaryObjectManager = binaryObjectManager;
            _env = env;
            _tenantRepository = tenantRepository;
            _OrgRepository = OrgRepository;
            _InstallerContract = InstallerContract;
            _MyInstallerPriceListRepository = MyInstallerPriceListRepository;
            _PriceItemListsRepository = PriceItemListsRepository;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
        }

        public async Task<PagedResultDto<GetInstallerDetailForViewDto>> GetAll(GetAllInstallerDetailsInput input)
        {

            var filteredInstallerDetails = _installerDetailRepository.GetAll()
                        .Include(e => e.UserFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.InstallerAccreditationNumber.Contains(input.Filter) || e.DesignerLicenseNumber.Contains(input.Filter) || e.ElectricianLicenseNumber.Contains(input.Filter) || e.DocInstaller.Contains(input.Filter) || e.DocDesigner.Contains(input.Filter) || e.DocElectrician.Contains(input.Filter) || e.OtherDocsCSV.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserFk != null && e.UserFk.Name == input.UserNameFilter);

            var pagedAndFilteredInstallerDetails = filteredInstallerDetails
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var installerDetails = from o in pagedAndFilteredInstallerDetails
                                   join o1 in _lookup_userRepository.GetAll() on o.UserId equals o1.Id into j1
                                   from s1 in j1.DefaultIfEmpty()

                                   select new GetInstallerDetailForViewDto()
                                   {
                                       InstallerDetail = new InstallerDetailDto
                                       {
                                           Id = o.Id
                                       },
                                       UserName = s1 == null || s1.Name == null ? "" : s1.Name.ToString()
                                   };

            var totalCount = await filteredInstallerDetails.CountAsync();

            return new PagedResultDto<GetInstallerDetailForViewDto>(
                totalCount,
                await installerDetails.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_InstallerDetails_Edit)]
        public async Task<GetInstallerDetailForEditOutput> GetInstallerDetailForEdit(EntityDto input)
        {
            var installerDetail = await _installerDetailRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetInstallerDetailForEditOutput { InstallerDetail = ObjectMapper.Map<CreateOrEditInstallerDetailDto>(installerDetail) };

            if (output.InstallerDetail.UserId != null)
            {
                var _lookupUser = await _lookup_userRepository.FirstOrDefaultAsync((long)output.InstallerDetail.UserId);
                output.UserName = _lookupUser?.Name?.ToString();
            }

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditInstallerDetailDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_InstallerDetails_Create)]
        protected virtual async Task Create(CreateOrEditInstallerDetailDto input)
        {
            var installerDetail = ObjectMapper.Map<InstallerDetail>(input);


            if (AbpSession.TenantId != null)
            {
                installerDetail.TenantId = (int)AbpSession.TenantId;
            }


            await _installerDetailRepository.InsertAsync(installerDetail);
        }

        [AbpAuthorize(AppPermissions.Pages_InstallerDetails_Edit)]
        protected virtual async Task Update(CreateOrEditInstallerDetailDto input)
        {
            var installerDetail = await _installerDetailRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, installerDetail);
        }

        [AbpAuthorize(AppPermissions.Pages_InstallerDetails_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _installerDetailRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_InstallerDetails)]
        public async Task<PagedResultDto<InstallerDetailUserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input)
        {
            var query = _lookup_userRepository.GetAll().WhereIf(
                   !string.IsNullOrWhiteSpace(input.Filter),
                  e => e.Name != null && e.Name.Contains(input.Filter)
               );

            var totalCount = await query.CountAsync();

            var userList = await query
                .PageBy(input)
                .ToListAsync();

            var lookupTableDtoList = new List<InstallerDetailUserLookupTableDto>();
            foreach (var user in userList)
            {
                lookupTableDtoList.Add(new InstallerDetailUserLookupTableDto
                {
                    Id = user.Id,
                    DisplayName = user.Name?.ToString()
                });
            }

            return new PagedResultDto<InstallerDetailUserLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
        }

        public async Task CreateOrEditOrganizationDoc(InstallerContractDto input)
        {
            
            //var storedFile = new BinaryObject(AbpSession.TenantId, byteArray);
            //await _binaryObjectManager.SaveAsync(storedFile);
            var extc = Path.GetExtension(input.FileName);
            var OrgName = _OrgRepository.GetAll().Where(e => e.Id == input.OrganizationId).Select(e => e.DisplayName).FirstOrDefault();
            var file = DateTime.Now.Ticks + "_" + OrgName + extc;
           

            byte[] ByteArray = _tempFileCacheManager.GetFile(input.FileToken);
            var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, file, "InstallerOrganizationDoc", 0, 0, AbpSession.TenantId);

          
            var exist = _InstallerContract.GetAll().Where(e => e.UserId == input.UserId && e.OrganizationId == input.OrganizationId).Select(e => e.Id).FirstOrDefault();
            if (exist == 0)
            {
                InstallerContract Installerorgdoc = new InstallerContract();
                if (AbpSession.TenantId != null)
                {
                    Installerorgdoc.TenantId = (int)AbpSession.TenantId;
                }
                Installerorgdoc.UserId = input.UserId;
                Installerorgdoc.OrganizationId = input.OrganizationId;
                Installerorgdoc.FileName = file;
                Installerorgdoc.FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\InstallerOrganizationDoc\\" + file;
                await _InstallerContract.InsertAsync(Installerorgdoc);
            }
            else
            {
                var Doc = await _InstallerContract.GetAsync(exist);
                Doc.UserId = input.UserId;
                Doc.OrganizationId = input.OrganizationId;
                Doc.FileName = file;
                Doc.FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\InstallerOrganizationDoc\\" + file;
                await _InstallerContract.UpdateAsync(Doc);
            }



        }

        public async Task<List<InstallerContractDto>> getUserWiseDocList(int id)
        {
            var result = (from or in _InstallerContract.GetAll()
                          where (or.UserId == id)
                          join uo in _OrgRepository.GetAll() on or.OrganizationId equals uo.Id into uoJoined
                          from uo in uoJoined.DefaultIfEmpty()

                          join user in _lookup_userRepository.GetAll() on or.UserId equals user.Id into userJoined
                          from user in userJoined.DefaultIfEmpty()


                          select new InstallerContractDto
                          {
                              UserName = user.Name,
                              OrganizationName = uo.DisplayName,
                              FilePath = or.FilePath,
                          }).ToList();

            return result;
        }

        public bool checkExistList(int userid, int orgid)
        {
            var Result = false;
            Result = _InstallerContract.GetAll().Where(e => e.UserId == userid && e.OrganizationId == orgid).Any();

            return Result;
        }


        public async Task<List<MyInstallerPriceListDto>> GetPriceItemListEdit(int Id)
        {
            var jobProductItem = _MyInstallerPriceListRepository.GetAll().Where(x => x.MyInstallerId == Id).ToList();
            var output = new List<MyInstallerPriceListDto>();
            if (jobProductItem.Count != 0)
            {
                var installerpricelistids = _MyInstallerPriceListRepository.GetAll().Where(x => x.MyInstallerId == Id).Select(e => e.PrictListItemId).ToList();
                var priceList = _PriceItemListsRepository.GetAll().Where(e => !installerpricelistids.Contains(e.Id)).Select(e => e.Id).ToList();
                foreach (var item in jobProductItem)
                {
                    var outobj = new MyInstallerPriceListDto();
                    outobj.MyInstallerId = item.MyInstallerId;
                    outobj.PriceItemListId = item.PrictListItemId;
                    outobj.Cost = item.Cost;
                    outobj.GST = (item.Cost / 10);
                    outobj.PriceINCGST = item.PriceINCGST;
                    outobj.Id = item.Id;
                    output.Add(outobj);
                }
                if (priceList.Count != 0)
                {
                    foreach (var item in priceList)
                    {
                        var outobj = new MyInstallerPriceListDto();
                        outobj.MyInstallerId = Id;
                        outobj.PriceItemListId = item;
                        outobj.Cost = 0.0M;
                        outobj.GST = 0.0M;
                        outobj.PriceINCGST = 0.0M;
                        outobj.Id = 0;
                        output.Add(outobj);
                    }
                }
            }
            else
            {

                var priceList = _PriceItemListsRepository.GetAll().Select(e => e.Id).ToList();
                foreach (var item in priceList)
                {
                    var outobj = new MyInstallerPriceListDto();

                    outobj.MyInstallerId = Id;
                    outobj.PriceItemListId = item;
                    outobj.Cost = 0.0M;
                    outobj.GST = 10;
                    outobj.PriceINCGST = 0.0M;
                    outobj.Id = 0;
                    output.Add(outobj);
                }
            }

            return output;
        }

        public async Task CreateMyInstallerPriceList(CreateOrEditMyInstallerPriceListDto input)
        {
            if (input.pricelist != null)
            {
                foreach (var item in input.pricelist)
                {
                    if (item.Id != 0)
                    {
                        var leadactivity = await _MyInstallerPriceListRepository.FirstOrDefaultAsync((int)item.Id);
                        leadactivity.Cost = item.Cost;
                        leadactivity.GST = item.GST;
                        leadactivity.PriceINCGST = item.PriceINCGST;
                        await _MyInstallerPriceListRepository.UpdateAsync(leadactivity);
                    }
                    else
                    {
                        MyInstallerPriceItemList price = new MyInstallerPriceItemList();
                        price.MyInstallerId = (int)item.MyInstallerId;
                        price.PrictListItemId = (int)item.PriceItemListId;
                        price.Cost = item.Cost;
                        price.GST = item.GST;
                        price.PriceINCGST = item.PriceINCGST;
                        if (AbpSession.TenantId != null)
                        {
                            price.TenantId = (int)AbpSession.TenantId;
                        }
                        await _MyInstallerPriceListRepository.InsertAsync(price);
                    }
                }
            }
        }
    }
}
