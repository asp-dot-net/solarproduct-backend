﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Timing.Timezone;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using TheSolarProduct.Authorization;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Installer.Dtos;
using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.UserWiseEmailOrgs;

using Abp.Linq.Extensions;

namespace TheSolarProduct.Installer
{
    //[AbpAuthorize(AppPermissions.Pages_Tenant_Installer_Availability, AppPermissions.Pages_Tenant_InstallerManager_InstallerCalendar)]
    [AbpAuthorize(AppPermissions.Pages)]
    public class InstallerAppService : TheSolarProductAppServiceBase, IInstallerAppService
    {
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<InstallerDetail> _installerDetailRepository;
        private readonly IRepository<InstallerAvailability> _installerAvailabilityRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<UserWiseEmailOrg> _userOrgRepository;

        public InstallerAppService(IRepository<InstallerDetail> installerDetailRepository, IRepository<Job> jobRepository,
            IRepository<InstallerAvailability> installerAvailabilityRepository,
            IRepository<User, long> userRepository,
            ITimeZoneConverter timeZoneConverter,
            IRepository<UserWiseEmailOrg> userOrgRepository)
        {
            _installerAvailabilityRepository = installerAvailabilityRepository;
            _userRepository = userRepository;
            _installerDetailRepository = installerDetailRepository;
            _timeZoneConverter = timeZoneConverter;
            _userOrgRepository = userOrgRepository;
            _jobRepository = jobRepository;
        }

        public async Task<int> AddInstallerAvailability(AddInstallerAvailabilityDto input)
        {
            var userid = AbpSession.UserId;
            var Date = (_timeZoneConverter.Convert(input.AvailabilityDate.Date, (int)AbpSession.TenantId));
            var recData = _installerAvailabilityRepository.GetAll().Where(e => (e.UserId == input.UserId) && e.AvailabilityDate == input.AvailabilityDate.Date.AddDays(1)).Count();
            if (recData < 2 && input.AvailabilityDate.Date.AddDays(1) >= DateTime.UtcNow.Date)
            {
                var installerAvailability = ObjectMapper.Map<InstallerAvailability>(input);
                installerAvailability.AvailabilityDate = input.AvailabilityDate.Date.AddDays(1);
                if (AbpSession.TenantId != null)
                {
                    installerAvailability.TenantId = (int)AbpSession.TenantId;
                    //installerAvailability.UserId = AbpSession.UserId;
                }
                await _installerAvailabilityRepository.InsertAsync(installerAvailability);
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public async Task<List<GetInstallerAvailabilityDto>> GetAll(long? userid)
        {
            var recData = _installerAvailabilityRepository.GetAll().Where(e => e.UserId == userid && e.AvailabilityDate.Date >= DateTime.UtcNow.Date)
                .Select(recDataVal => new GetInstallerAvailabilityDto
                {

                    Id = recDataVal.Id,
                    AvailabilityDate = recDataVal.AvailabilityDate
                });

            return new List<GetInstallerAvailabilityDto>(
                 await recData.ToListAsync()
             );
        }


        public async Task<List<GetInstallerAvailabilityDto>> GetAllJobBokingCount(long? userid, int? installerId, int? orgID)
        {
            var dates = new List<DateTime>();
            DateTime dt1 = DateTime.Now.Date;
            var firstDayOfMonth = new DateTime(dt1.Year, dt1.Month, dt1.Day);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            for (var dt = firstDayOfMonth; dt <= lastDayOfMonth; dt = dt.AddDays(1))
            {
                dates.Add(dt.Date);
            }

            //var orgwiseuser = _userOrgRepository.GetAll().WhereIf(orgID != 0, e => e.OrganizationUnitId == orgID).Select(e => e.UserId).ToList();

            var jobs = _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.LeadFk.OrganizationId == orgID && e.InstallationDate != null)
                .WhereIf(installerId != 0 && installerId != null, e => e.InstallerId == installerId);

            var recData = (from dt in dates
                           select new GetInstallerAvailabilityDto
                           {
                               AvailabilityDate = Convert.ToDateTime(dt.Date.ToShortDateString()),
                               //InstallationCount = _installerAvailabilityRepository.GetAll().WhereIf(installerId != 0 , e=> e.UserId == installerId).WhereIf(orgwiseuser.Count() > 0 && installerId == 0, e => orgwiseuser.Contains((long)e.UserId)).Where(x => x.AvailabilityDate.Date == Convert.ToDateTime(dt.Date.ToShortDateString())).Count()
                               InstallationCount = jobs.Where(x => x.InstallationDate.Value.Date == Convert.ToDateTime(dt.Date.ToShortDateString())).Count()
                           }).Where(e => e.InstallationCount != 0).ToList();


            return recData;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_InstallerManager_InstallerCalendar)]
        public async Task<List<CommonLookupDto>> GetAllInstallers(int? orgid)
        {
            var orgwiseuser = _userOrgRepository.GetAll()
                              .WhereIf(orgid != 0, e => e.OrganizationUnitId == orgid).Select(e => e.UserId).ToList();

            var User = _userRepository.GetAll().WhereIf(orgwiseuser.Count() > 0, e => orgwiseuser.Contains(e.Id) && e.IsActive == true).ToList();

            var InstallerList = (from u in User
                                 join o1 in _installerDetailRepository.GetAll() on u.Id equals o1.UserId into j1
                                 from s1 in j1.DefaultIfEmpty()
                                 where (s1 != null && s1.IsInst == true)
                                 select new CommonLookupDto
                                 {
                                     Id = (int)u.Id,
                                     DisplayName = u.FullName
                                 }).ToList();
            return InstallerList;
        }
        
        public async Task RemoveInstallerAvailability(EntityDto input)
        {
            await _installerAvailabilityRepository.DeleteAsync(input.Id);
        }
    
        public async Task<bool> CheckIntallerAccreditation(long? userid, string AccreditationNumber)
        {
            var result = false;
            if (userid == 0)
            {
                result = await _installerDetailRepository.GetAll().Where(e => e.InstallerAccreditationNumber == AccreditationNumber && e.UserFk.IsDeleted == false).AnyAsync();
            }
            else
            {
                result = await _installerDetailRepository.GetAll().Where(e => e.InstallerAccreditationNumber == AccreditationNumber && e.UserFk.IsDeleted == false && e.UserId != userid).AnyAsync();
            }

            return result;
        }

        public async Task<List<CommonLookupInstaller>> GetAllInstallersWithCompnayName(int? orgid)
        {
            var orgwiseuser = await _userOrgRepository.GetAll()
                              .WhereIf(orgid != 0, e => e.OrganizationUnitId == orgid).Select(e => e.UserId).ToListAsync();

            var User = _userRepository.GetAll().WhereIf(orgwiseuser.Count() > 0, e => orgwiseuser.Contains(e.Id) && e.IsActive == true).ToList();

            var InstallerList = (from u in User
                                 join o1 in _installerDetailRepository.GetAll() on u.Id equals o1.UserId into j1
                                 from s1 in j1.DefaultIfEmpty()
                                 where (s1 != null && s1.IsInst == true)
                                 select new CommonLookupInstaller
                                 {
                                     Id = (int)u.Id,
                                     InstallerName = u.FullName.ToString(),
                                     CompanyName = u.CompanyName
                                 }).ToList();
            return InstallerList;
        }
    }
}
