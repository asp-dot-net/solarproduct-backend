﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.HoldReasons.Exporting;
using TheSolarProduct.HoldReasons.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.ServiceCategorys.Dtos;
using TheSolarProduct.ServiceCategorys;
using TheSolarProduct.ServiceStatuses.Dtos;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Migrations;

namespace TheSolarProduct.ServiceStatuses
{
    [AbpAuthorize(AppPermissions.Pages_ServiceStatus, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    public class ServiceStatusesAppService : TheSolarProductAppServiceBase, IServiceStatusesAppService
    {
        private readonly IRepository<ServiceStatus> _serviceStatusesRepository;
        private readonly IHoldReasonsExcelExporter _serviceStatusesExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public ServiceStatusesAppService(IRepository<ServiceStatus> serviceStatusesRepository, IHoldReasonsExcelExporter serviceStatusesExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _serviceStatusesRepository = serviceStatusesRepository;
            _serviceStatusesExcelExporter = serviceStatusesExcelExporter;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task<PagedResultDto<GetServiceStatusForViewDto>> GetAll(GetAllServiceStatusInput input)
        {

            var filteredHoldReasons = _serviceStatusesRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

            var pagedAndFilteredHoldReasons = filteredHoldReasons
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var serviceCategory = from o in pagedAndFilteredHoldReasons
                              select new GetServiceStatusForViewDto()
                              {
                                  serviceStatuses = new ServiceStatusDto
                                  {
                                      Name = o.Name,
                                      Id = o.Id,
                                      IsActive=o.IsActive,
                                  }
                              };

            var totalCount = await filteredHoldReasons.CountAsync();

            return new PagedResultDto<GetServiceStatusForViewDto>(
                totalCount,
                await serviceCategory.ToListAsync()
            );
        }

        public async Task<GetServiceStatusForViewDto> GetServiceStatusForView(int id)
        {
            var serviceCategory = await _serviceStatusesRepository.GetAsync(id);

            var output = new GetServiceStatusForViewDto { serviceStatuses = ObjectMapper.Map<ServiceStatusDto>(serviceCategory) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceStatus_Edit)]
        public async Task<GetServiceStatusForEditOutput> GetServiceStatusForEdit(EntityDto input)
        {
            var serviceStatus = await _serviceStatusesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetServiceStatusForEditOutput { serviceStatuses = ObjectMapper.Map<CreateOrEditServiceStatusDto>(serviceStatus) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditServiceStatusDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceStatus_Create)]
        protected virtual async Task Create(CreateOrEditServiceStatusDto input)
        {
            var serviceStatus = ObjectMapper.Map<ServiceStatus>(input);

            if (AbpSession.TenantId != null)
            {
                serviceStatus.TenantId = (int)AbpSession.TenantId;
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 34;
            dataVaultLog.ActionNote = "Service Status Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _serviceStatusesRepository.InsertAsync(serviceStatus);
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceStatus_Edit)]
        protected virtual async Task Update(CreateOrEditServiceStatusDto input)
        {
            var serviceStatus = await _serviceStatusesRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 34;
            dataVaultLog.ActionNote = "Service Status Updated : " + serviceStatus.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != serviceStatus.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = serviceStatus.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != serviceStatus.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = serviceStatus.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, serviceStatus);
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceStatus_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _serviceStatusesRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 34;
            dataVaultLog.ActionNote = "Service Status Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _serviceStatusesRepository.DeleteAsync(input.Id);
        }

        //public async Task<FileDto> GetHoldReasonsToExcel(GetAllHoldReasonsForExcelInput input)
        //{

        //    var filteredHoldReasons = _serviceCategorysRepository.GetAll()
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobHoldReason.Contains(input.Filter))
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.HoldReasonFilter), e => e.JobHoldReason == input.HoldReasonFilter);

        //    var query = (from o in filteredHoldReasons
        //                 select new GetHoldReasonForViewDto()
        //                 {
        //                     JobHoldReason = new HoldReasonDto
        //                     {
        //                         JobHoldReason = o.JobHoldReason,
        //                         Id = o.Id
        //                     }
        //                 });

        //    var holdReasonListDtos = await query.ToListAsync();

        //    return _holdReasonsExcelExporter.ExportToFile(holdReasonListDtos);
        //}

    }
}