﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Timing.Timezone;
using Hangfire.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using NPOI.SS.Formula.Functions;
using Org.BouncyCastle.Crypto.Tls;
using Stripe;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.DashboardMessages.Dtos;
using TheSolarProduct.DashboardSales.Dtos;
using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Leads;
using TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos;
using TheSolarProduct.States;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Timing;
using Twilio.TwiML.Voice;
using Job = TheSolarProduct.Jobs.Job;

namespace TheSolarProduct.DashboardSales
{
    public class DashboardSalesAppService : TheSolarProductAppServiceBase, IDashboardSalesAppService
    {
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<Lead> _leadRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<CallHistory.CallHistory> _callHistoryRepository;
        private readonly IRepository<DashboardMessages.DashboardMessage> _DashboardMessageRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;

        public DashboardSalesAppService(
            IRepository<UserTeam> userTeamRepository
            , IRepository<User, long> userRepository
            , UserManager userManager
            , IRepository<Lead> leadRepository
            , ITimeZoneService timeZoneService
            , ITimeZoneConverter timeZoneConverter
            , IRepository<Job> jobRepository
            , IRepository<CallHistory.CallHistory> callHistoryRepository
            , IRepository<DashboardMessages.DashboardMessage> DashboardMessageRepository
            , IRepository<JobProductItem> jobProductItemRepository)
        {
            _userTeamRepository = userTeamRepository;
            _userRepository = userRepository;
            _userManager = userManager;
            _leadRepository = leadRepository;
            _timeZoneService = timeZoneService; 
            _timeZoneConverter = timeZoneConverter; 
            _jobRepository = jobRepository;
            _callHistoryRepository = callHistoryRepository;
            _DashboardMessageRepository = DashboardMessageRepository;
            _jobProductItemRepository = jobProductItemRepository;
        }

        public async Task<List<CommonLookupDto>> GetUsers(int org)
       {
            int UserId = (int)AbpSession.UserId;
            var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });
            
            var TeamId = await user_teamList.Where(e => e.UserId == UserId).Select(e => e.TeamId).ToListAsync();

            var UserIds = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var User = await _userRepository.GetAsync(UserId);
            IList<string> RoleName = await _userManager.GetRolesAsync(User);

            var User_List = _userRepository.GetAll()
                .Where(e=>e.OrganizationUnits.Any(r =>r.OrganizationUnitId == org))
                .WhereIf(RoleName.Contains("Sales Manager"), e => UserIds.Contains(e.Id))
                .WhereIf(RoleName.Contains("Sales Rep"), e => e.Id == UserId)
                .AsNoTracking().Select(e => new {e.Id, e.FullName});

            var result = (from o in User_List
                         select new CommonLookupDto()
                         {
                             Id = (int)o.Id,
                             DisplayName = o.FullName
                         }).ToList();


            return result;

        }

        public async Task<GetLeadOverViewDto> GetLeadOverView(GetLeadOverViewInput input)
        {
            int UserId = (int)AbpSession.UserId;
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var TeamId = await user_teamList.Where(e => e.UserId == UserId).Select(e => e.TeamId).ToListAsync();

            var UserIds = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var User = await _userRepository.GetAsync(UserId);
            IList<string> RoleName = await _userManager.GetRolesAsync(User);


            var filteredLeads = _leadRepository.GetAll()
                          .Include(e => e.LeadStatusFk)
                          .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                          .WhereIf(RoleName.Contains("Sales Manager"), e => UserIds.Contains(e.AssignToUserID))
                          .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                          .WhereIf(input.UserId != null && input.UserId != 0, e =>e.AssignToUserID == input.UserId)
                          .WhereIf(input.StartDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                          .WhereIf(input.EndDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                          .Where(e => e.HideDublicate != true && e.OrganizationId == input.OrgId)
                          .AsNoTracking().Select(e=> new { e.LeadStatusId });

            var LeadOverView = new GetLeadOverViewDto();

            LeadOverView.Assigned = Convert.ToInt32(await filteredLeads.Where(e => e.LeadStatusId == 10).CountAsync());
            LeadOverView.Hot = Convert.ToInt32(await filteredLeads.Where(e => e.LeadStatusId == 5).CountAsync());
            LeadOverView.Cold = Convert.ToInt32(await filteredLeads.Where(e => e.LeadStatusId == 3).CountAsync());
            LeadOverView.Warm = Convert.ToInt32(await filteredLeads.Where(e => e.LeadStatusId == 4).CountAsync());
            LeadOverView.UnHandled = Convert.ToInt32(await filteredLeads.Where(e => e.LeadStatusId == 2).CountAsync());
            LeadOverView.Canceled = Convert.ToInt32(await filteredLeads.Where(e => e.LeadStatusId == 8).CountAsync());

            return LeadOverView;

        }


        public async Task<GetSalesOverViewDto> GetSalesOverView(GetLeadOverViewInput input)
        {
            int UserId = (int)AbpSession.UserId;
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var TeamId = await user_teamList.Where(e => e.UserId == UserId).Select(e => e.TeamId).ToListAsync();

            var UserIds = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var User = await _userRepository.GetAsync(UserId);
            IList<string> RoleName = await _userManager.GetRolesAsync(User);

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.LeadFk)
                        .Where(e => e.LeadFk.OrganizationId == input.OrgId)
                        .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                        .WhereIf(RoleName.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(RoleName.Contains("Sales Manager"), e => UserIds.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(RoleName.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.UserId != null && input.UserId != 0, e => e.LeadFk.AssignToUserID == input.UserId)
                        .AsNoTracking().Select(e => new { e.JobStatusId });

            var SalesOverView = new GetSalesOverViewDto();
            SalesOverView.TotalJobs = filteredJobs.Count();
            SalesOverView.Planned = await filteredJobs.Where(e => e.JobStatusId == 1).CountAsync();
            SalesOverView.active = await filteredJobs.Where(e => e.JobStatusId == 5 || e.JobStatusId == 4).CountAsync();
            SalesOverView.JobBook = await filteredJobs.Where(e => e.JobStatusId == 6 || e.JobStatusId == 9).CountAsync();
            SalesOverView.Installed = await filteredJobs.Where(e => e.JobStatusId == 8).CountAsync();
            SalesOverView.Can_Hold = await filteredJobs.Where(e => e.JobStatusId == 2 || e.JobStatusId == 3).CountAsync();
            return SalesOverView;

        }

        public async Task<GetCallHistoryDto> GetCallHistory(GetLeadOverViewInput input)
        {
            int UserId = (int)AbpSession.UserId;
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var TeamId = await user_teamList.Where(e => e.UserId == UserId).Select(e => e.TeamId).ToListAsync();

            var UserIds = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var User = await _userRepository.GetAsync(UserId);
            IList<string> RoleName = await _userManager.GetRolesAsync(User);

            var users = await _userRepository.GetAll().Where(e => e.ExtensionNumber != null && e.OrganizationUnits.Any(o => o.OrganizationUnitId == input.OrgId))
                       .WhereIf(input.UserId != 0 && input.UserId != null, e => e.Id == input.UserId)
                       .WhereIf(RoleName.Contains("Sales Manager"), e => UserIds.Contains(e.Id))
                       .WhereIf(RoleName.Contains("Sales Rep"), e => e.Id == User.Id).AsNoTracking().Select(e => new { e.ExtensionNumber, e.Id }).ToListAsync();

            var filtered = await _callHistoryRepository.GetAll()
               .WhereIf(input.StartDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date >= SDate.Value.Date)
               .WhereIf(input.EndDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date <= EDate.Value.Date)
               .AsNoTracking().Select(e => new {e.CallType, e.Agent }).ToListAsync();

            var res = from o in users

                      select new GetAllUserCallHistoryDto
                      {
                          Missed = filtered.Where(e => e.CallType == "Missed" && e.Agent == o.ExtensionNumber).Count(),
                          Inbound = filtered.Where(e => e.CallType == "Inbound" && e.Agent == o.ExtensionNumber).Count(),
                          Outbound = filtered.Where(e => e.CallType == "Outbound" && e.Agent == o.ExtensionNumber).Count(),
                          Notanswered = filtered.Where(e => e.CallType == "Notanswered" && e.Agent == o.ExtensionNumber).Count(),
                      };


            var callhistory = new GetCallHistoryDto();
            callhistory.Missed = res.Select(e => e.Missed).Sum();
            callhistory.Inbound = res.Select(e => e.Inbound).Sum();
            callhistory.Outbound = res.Select(e => e.Outbound).Sum();
            callhistory.NotAnswerd = res.Select(e => e.Notanswered).Sum();

            return callhistory;

        }
    
        public async Task<GetStateWiseSalesDto> getStateWiseSales(GetLeadOverViewInput input)
        {
            int UserId = (int)AbpSession.UserId;
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var TeamId = await user_teamList.Where(e => e.UserId == UserId).Select(e => e.TeamId).ToListAsync();

            var UserIds = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var User = await _userRepository.GetAsync(UserId);
            IList<string> RoleName = await _userManager.GetRolesAsync(User);

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.LeadFk)
                        .Where(e => e.LeadFk.OrganizationId == input.OrgId)
                        .WhereIf(input.StartDate != null, e => e.FirstDepositDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.EndDate != null, e => e.FirstDepositDate.Value.Date <= EDate.Value.Date)
                        .WhereIf(RoleName.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(RoleName.Contains("Sales Manager"), e => UserIds.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(RoleName.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.UserId != null && input.UserId != 0, e => e.LeadFk.AssignToUserID == input.UserId)
                        .AsNoTracking().Select(e => new { e.State });

            var stateWise = new GetStateWiseSalesDto();
            stateWise.NSW = filteredJobs.Where(e =>e.State == "NSW").Count();
            stateWise.QLD = filteredJobs.Where(e =>e.State == "QLD").Count();
            stateWise.SA = filteredJobs.Where(e =>e.State == "SA").Count();
            stateWise.VIC = filteredJobs.Where(e =>e.State == "VIC").Count();

            return stateWise;
        }

        public Task<List<GetFollowUPDto>> getFollowUP(GetLeadOverViewInput input)
        {
            throw new NotImplementedException();
        }

        public async Task<List<GetLeadStatusDetailsDto>> getLeadStatusDetail(GetLeadOverViewInput input)
        {
            int UserId = (int)AbpSession.UserId;
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var TeamId = await user_teamList.Where(e => e.UserId == UserId).Select(e => e.TeamId).ToListAsync();

            var UserIds = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var User = await _userRepository.GetAsync(UserId);
            IList<string> RoleName = await _userManager.GetRolesAsync(User);

            var FD_Jobs = await _jobRepository.GetAll()
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.LeadFk)
                        .Where(e => e.LeadFk.OrganizationId == input.OrgId)
                        .WhereIf(input.StartDate != null, e => e.FirstDepositDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.EndDate != null, e => e.FirstDepositDate.Value.Date <= EDate.Value.Date)
                        .WhereIf(RoleName.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(RoleName.Contains("Sales Manager"), e => UserIds.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(RoleName.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.UserId != null && input.UserId != 0, e => e.LeadFk.AssignToUserID == input.UserId)
                        .AsNoTracking().Select(e => new { soId = e.LeadFk.LeadSource }).ToListAsync();

            var CT_Jobs = await _jobRepository.GetAll()
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.LeadFk)
                        .Where(e => e.LeadFk.OrganizationId == input.OrgId)
                        .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                        .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(RoleName.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(RoleName.Contains("Sales Manager"), e => UserIds.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(RoleName.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.UserId != null && input.UserId != 0, e => e.LeadFk.AssignToUserID == input.UserId)
                        .AsNoTracking().Select(e => new { soId = e.LeadFk.LeadSource }).ToListAsync(); ;

            var filteredLeads = await _leadRepository.GetAll()
                         .Include(e => e.LeadStatusFk)
                         .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                         .WhereIf(RoleName.Contains("Sales Manager"), e => UserIds.Contains(e.AssignToUserID))
                         .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                         .WhereIf(input.UserId != null && input.UserId != 0, e => e.AssignToUserID == input.UserId)
                         .WhereIf(input.StartDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                         .WhereIf(input.EndDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                         .Where(e => e.HideDublicate != true && e.OrganizationId == input.OrgId)
                         .AsNoTracking().GroupBy(E => E.LeadSource).Select(e => new { Source = e.Key, count = e.Count() }).ToListAsync();

            var leads = from o in filteredLeads
                        select new GetLeadStatusDetailsDto()
                        {
                            LeadSource = o.Source,
                            Lead = o.count,
                            Upgrade = CT_Jobs.Where(e => e.soId == o.Source).Count(),
                            Sales = FD_Jobs.Where(e => e.soId == o.Source).Count(),
                            Ratio = decimal.Round(CT_Jobs.Where(e => e.soId == o.Source).Count()>0 ? ((FD_Jobs.Where(e => e.soId == o.Source).Count() *100)/ CT_Jobs.Where(e => e.soId == o.Source).Count()) : 0,2)
                        };

            return leads.ToList();
        }

        public async Task<GetHeaderValue> getHeaderValue(GetLeadOverViewInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            int UserId = (int)AbpSession.UserId;

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var TeamId = await user_teamList.Where(e => e.UserId == UserId).Select(e => e.TeamId).ToListAsync();

            var UserIds = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var User = await _userRepository.GetAsync(UserId);
            IList<string> RoleName = await _userManager.GetRolesAsync(User);

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.LeadFk)
                        .Where(e => e.LeadFk.OrganizationId == input.OrgId)
                        .WhereIf(input.StartDate != null, e => e.FirstDepositDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.EndDate != null, e => e.FirstDepositDate.Value.Date <= EDate.Value.Date)
                         .WhereIf(RoleName.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                         .WhereIf(RoleName.Contains("Sales Manager"), e => UserIds.Contains(e.LeadFk.AssignToUserID))
                         .WhereIf(RoleName.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .AsNoTracking().GroupBy(e => e.LeadFk.AssignToUserID).Select(e => new { userid = e.Key, sys = e.Sum(s=>s.SystemCapacity), index = 0 }).OrderByDescending(e=>e.sys)
                        .Take(1).ToList();

            //var job = filteredJobs.Where(e => e.userid == AbpSession.UserId).FirstOrDefault();

            //if (RoleName.Contains("Admin") || RoleName.Contains("Sales Manager"))
            //{
            //    job = filteredJobs.FirstOrDefault();

            //}

            var job = filteredJobs.FirstOrDefault();
            var header = new GetHeaderValue();

            if (job != null)
            {
                
            header.Name = await _userRepository.GetAll().Where(e => e.Id == job.userid).AsNoTracking().Select(e=>e.FullName).FirstOrDefaultAsync();
            header.Rank =filteredJobs.FindIndex(e=>e.userid == job.userid) + 1 ;
            header.SystemCapacity = job.sys.ToString();
            }

            return header;
        }

        public async Task<List<GetLeadSalesListDto>> getLeadSalesList(GetLeadOverViewInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            int UserId = (int)AbpSession.UserId;

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var TeamId = await user_teamList.Where(e => e.UserId == UserId).Select(e => e.TeamId).ToListAsync();

            var UserIds = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var User = await _userRepository.GetAsync(UserId);
            IList<string> RoleName = await _userManager.GetRolesAsync(User);

            
            var Jobs = await _jobRepository.GetAll()
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.LeadFk)
                        .Where(e => e.LeadFk.OrganizationId == input.OrgId)
                        .WhereIf(input.StartDate != null, e => e.FirstDepositDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.EndDate != null, e => e.FirstDepositDate.Value.Date <= EDate.Value.Date)
                         .WhereIf(RoleName.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                         .WhereIf(RoleName.Contains("Sales Manager"), e => UserIds.Contains(e.LeadFk.AssignToUserID))
                         .WhereIf(RoleName.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)

                        .AsNoTracking()
                        //.GroupBy(e => e.LeadFk.AssignToUserID)
                        .Select(e => new { e.LeadFk.AssignToUserID, e.SystemCapacity, e.Id })
            .ToListAsync();

            var filteredJobs = Jobs.GroupBy(e => e.AssignToUserID).Select(e => new { userid = e.Key, sys = e.Sum(s=>s.SystemCapacity)}).OrderByDescending(e => e.sys).ToList();

            var jobids = Jobs.Select(e => e.Id).ToList();

            var product = _jobProductItemRepository.GetAll()
                            .Include(e => e.JobFk)
                            .Include(e => e.ProductItemFk)
                            .Where(s => jobids.Contains((int)s.JobId) && s.ProductItemFk.ProductTypeId == 1).AsNoTracking()
                            .GroupBy(e => e.JobFk.LeadFk.AssignToUserID).Select(e => new { uid = e.Key, qty = e.Sum(s =>s.Quantity) }).ToList();


            var result = from o in filteredJobs
                         select new GetLeadSalesListDto()
                         {
                             UserName = _userRepository.Get((long)o.userid).FullName,
                             SystemCapacity = (decimal)o.sys,
                             NoOfPanel = product.Where(e => e.uid == o.userid).Any() ? product.Where(e => e.uid == o.userid).FirstOrDefault().qty : 0,
                         };

            return result.ToList();

        }

        public async Task<List<string>> getMessageBoardDataList(int OrgId)
        {
            var filtereddashDoardMessages = await _DashboardMessageRepository.GetAll()
                         .Where(e => e.OrgId == OrgId && e.DashboardType == "Sales").AsNoTracking().Select(e => e.Message).ToListAsync();
            return filtereddashDoardMessages;
        }
    }
}
