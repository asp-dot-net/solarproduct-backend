﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.LeadActions;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Leads;
using TheSolarProduct.SystemReports.Dto;
using TheSolarProduct.TheSolarDemo;
using Abp.Timing.Timezone;
using Abp.Authorization.Users;
using Abp.Auditing;
using TheSolarProduct.Jobs;
using Abp.AutoMapper;
using Abp.Notifications;
using TheSolarProduct.Notifications;
using TheSolarProduct.Sections;
using TheSolarProduct.Dto;
using TheSolarProduct.Invoices;
using TheSolarProduct.SystemReports.Exporting;
using System.Data;
using System.Globalization;
using TheSolarProduct.Timing;
using Abp.Authorization;
using TheSolarProduct.Authorization;

namespace TheSolarProduct.SystemReports
{
    [AbpAuthorize]
    public class SystemReportsAppService : TheSolarProductAppServiceBase, ISystemReportsAppService
    {
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<LeadAction, int> _lookup_leadActionRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly UserManager _userManager;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<Section> _SectionAppService;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<UserLoginAttempt, long> _userLoginAttemptRepository;
        private readonly IRepository<AuditLog, long> _auditLogRepository;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<InvoiceImportData> _InvoiceImportDataRepository;
        private readonly ISystemReportExcelExport _systemReportExcelExporter;
        private readonly ITimeZoneService _timeZoneService;

        public SystemReportsAppService(IRepository<Lead> leadRepository,
             IRepository<AuditLog, long> auditLogRepository
            , IRepository<LeadActivityLog> leadactivityRepository
            , IRepository<LeadAction, int> lookup_leadActionRepository,
            IRepository<User, long> userRepository,
            IAppNotifier appNotifier,
            IRepository<UserTeam> userTeamRepository,
            UserManager userManager,
            ITimeZoneConverter timeZoneConverter,
            IRepository<UserLoginAttempt, long> userLoginAttemptRepository,
            IRepository<Job> jobRepository,
            IRepository<Section> SectionAppService,
            IRepository<InvoiceImportData> InvoiceImportDataRepository,
            ISystemReportExcelExport systemReportExcelExporter,
            ITimeZoneService timeZoneService
            )
        {
            _leadRepository = leadRepository;
            _leadactivityRepository = leadactivityRepository;
            _lookup_leadActionRepository = lookup_leadActionRepository;
            _userRepository = userRepository;
            _userTeamRepository = userTeamRepository;
            _userManager = userManager;
            _appNotifier = appNotifier;
            _timeZoneConverter = timeZoneConverter;
            _userLoginAttemptRepository = userLoginAttemptRepository;
            _auditLogRepository = auditLogRepository;
            _jobRepository = jobRepository;
            _SectionAppService = SectionAppService;
            _InvoiceImportDataRepository = InvoiceImportDataRepository;
            _systemReportExcelExporter = systemReportExcelExporter;
            _timeZoneService = timeZoneService;
        }

        public PagedResultDto<GetActivityForViewDto> GetAll(GetAllActivityInput input)
        {
            try
            {
                var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

                var FilterTeamList = new List<long?>();
                if (input.TeamId != null && input.TeamId != 0)
                {
                    FilterTeamList = _userTeamRepository.GetAll().Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
                }
                //var loginattempt = _userLoginAttemptRepository.GetAll().Where(e => e.CreationTime.AddHours(5).Date >= SDate.Value.Date && e.UserId == input.UserId);

                var filteredLeads = ((from i in _userLoginAttemptRepository.GetAll()
                                      where (i.CreationTime.AddHours(5).Date >= SDate.Value.Date && i.UserId == input.UserId)
                                      select new GetActivitylistViewDto
                                      {
                                          actvitydate = i.CreationTime,
                                          activitynname = "Log In",
                                          ActionId = 0,
                                          LeadId = 0,
                                          CreatorUserId = (int)i.UserId,
                                          ActionNote = "Log In",
                                          Body = "",
                                          ActivityNote = ""
                                      })
                                   .WhereIf(input.TeamId != 0, e => FilterTeamList.Contains(e.CreatorUserId))
                            .WhereIf(input.UserId != 0, e => e.CreatorUserId == input.UserId)
                            //.WhereIf(input.UserId != 0, e => e.LeadFk.AssignToUserID == input.UserId)
                            .WhereIf(input.StartDate != null, e => e.actvitydate.AddHours(5).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.actvitydate.AddHours(5).Date <= EDate.Value.Date).ToList())
                            .Union
                            ((from i in _auditLogRepository.GetAll()
                              where (i.ExecutionTime.AddHours(5).Date >= SDate.Value.Date && i.UserId == input.UserId && i.MethodName == "LogOut")
                              select new GetActivitylistViewDto
                              {

                                  actvitydate = i.ExecutionTime,
                                  activitynname = i.MethodName,
                                  ActionId = 0,
                                  LeadId = 0,
                                  CreatorUserId = (int)i.UserId,
                                  ActionNote = i.MethodName,
                                  Body = "",
                                  ActivityNote = ""

                              })
                                   .WhereIf(input.TeamId != 0, e => FilterTeamList.Contains(e.CreatorUserId))
                            .WhereIf(input.UserId != 0, e => e.CreatorUserId == input.UserId)
                            //.WhereIf(input.UserId != 0, e => e.LeadFk.AssignToUserID == input.UserId)
                            .WhereIf(input.StartDate != null, e => e.actvitydate.AddHours(5).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.actvitydate.AddHours(5).Date <= EDate.Value.Date).ToList())
                             .Union
                            ((from i in _leadactivityRepository.GetAll()
                              where (i.CreationTime.AddHours(5).Date >= SDate.Value.Date && i.CreatorUserId == input.UserId && i.LeadFk.OrganizationId == input.Organizationid)
                              select new GetActivitylistViewDto
                              {
                                  actvitydate = i.CreationTime,
                                  activitynname = "",
                                  ActionId = i.ActionId,
                                  LeadId = i.LeadId,
                                  CreatorUserId = (int)i.CreatorUserId,
                                  ActionNote = i.ActionNote,
                                  Body = i.Body,
                                  ActivityNote = i.ActivityNote

                              })
                                   .WhereIf(input.TeamId != 0, e => FilterTeamList.Contains(e.CreatorUserId))
                            .WhereIf(input.UserId != 0, e => e.CreatorUserId == input.UserId)
                            //.WhereIf(input.UserId != 0, e => e.LeadFk.AssignToUserID == input.UserId)
                            .WhereIf(input.StartDate != null, e => e.actvitydate.AddHours(5).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.actvitydate.AddHours(5).Date <= EDate.Value.Date).ToList());




                //var logouttime = _auditLogRepository.GetAll().Where(e => e.ExecutionTime.AddHours(5).Date >= SDate.Value.Date && e.UserId == input.UserId && e.MethodName == "LogOut");
                //var filteredLeads = _leadactivityRepository.GetAll()
                //            //.WhereIf(input.TeamId != 0, e => FilterTeamList.Contains(e.LeadFk.AssignToUserID))
                //            .WhereIf(input.TeamId != 0, e => FilterTeamList.Contains(e.CreatorUserId))
                //            .WhereIf(input.UserId != 0, e => e.CreatorUserId == input.UserId)
                //            //.WhereIf(input.UserId != 0, e => e.LeadFk.AssignToUserID == input.UserId)
                //            .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(5).Date >= SDate.Value.Date)
                //            .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(5).Date <= EDate.Value.Date)
                //            .Where(e => e.LeadFk.OrganizationId == input.Organizationid);
                var pagedAndFilteredLeads = filteredLeads.AsQueryable()
                    .OrderBy(input.Sorting ?? "actvitydate desc")
                    .PageBy(input);

                var leads = (from o in pagedAndFilteredLeads
                             join o1 in _lookup_leadActionRepository.GetAll() on o.ActionId equals o1.Id into j1
                             from s1 in j1.DefaultIfEmpty()

                             join o2 in _leadRepository.GetAll() on o.LeadId equals o2.Id into j2
                             from s2 in j2.DefaultIfEmpty()

                             join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                             from s3 in j3.DefaultIfEmpty()

                             select new GetActivityForViewDto()
                             {
                                 ActionName = s1 == null || s1.ActionName == null ? o.activitynname : s1.ActionName.ToString(),
                                 ActionId = o.ActionId,
                                 ActionNote = o.ActionNote,
                                 ActivityNote = o.ActionId == 6 ? o.Body : o.ActivityNote,
                                 LeadId = o.LeadId,
                                 CreationTime = o.actvitydate,
                                 //CreationTime = (_timeZoneConverter.Convert(o.CreationTime.AddHours(-5), (int)AbpSession.TenantId)),
                                 // CreationTime = (_timeZoneConverter.Convert(o.CreationTime, (int)AbpSession.TenantId)),
                                 CreatorUserName = s3 == null || s3.FullName == null ? "" : s3.FullName.ToString(),
                                 LeadCompanyName = s2 == null || s2.CompanyName == null ? "" : s2.CompanyName.ToString()
                             }).OrderByDescending(p => p.CreationTime).ToList();

                var totalCount = filteredLeads.AsQueryable().Count();

                return new PagedResultDto<GetActivityForViewDto>(totalCount, leads);
            }
            catch (Exception e) { throw e; }
        }
        public async Task<PagedResultDto<GetActivityForViewDto>> GetAllTodoActivityList(GetAllActivityInputDto input)
        {

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredLeads = _leadactivityRepository.GetAll()
                        //.WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        //.WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        //.WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        //.WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(5).Date >= SDate.Value.Date)
                        //.WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(5).Date <= EDate.Value.Date)
                        .Where(e => e.LeadFk.OrganizationId == input.Organizationid && e.ActionId == 25 && e.IsTodoComplete != true && e.ReferanceId == User.Id);
            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var leads = (from o in pagedAndFilteredLeads
                         join o1 in _lookup_leadActionRepository.GetAll() on o.ActionId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _leadRepository.GetAll() on o.LeadId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()
                         join o3 in _userRepository.GetAll() on (int)o.ReferanceId equals o3.Id into r3
                         from rf3 in r3.DefaultIfEmpty()

                         join j in _jobRepository.GetAll() on o.LeadId equals j.LeadId into j4
                         from j in j4.DefaultIfEmpty()


                         select new GetActivityForViewDto()
                         {
                             ActionName = s1 == null || s1.ActionName == null ? "" : s1.ActionName.ToString(),
                             ActionId = o.ActionId,
                             ActionNote = o.ActionNote,
                             ActivityNote = o.ActionId == 6 ? o.Body : o.ActivityNote,
                             LeadId = o.LeadId,
                             CreationTime = o.CreationTime,
                             CreatorUserName = s3 == null || s3.FullName == null ? "" : s3.FullName.ToString(),
                             AssignedTo = rf3 == null || rf3.FullName == null ? "" : rf3.FullName.ToString(),
                             LeadCompanyName = s2 == null || s2.CompanyName == null ? "" : s2.CompanyName.ToString(),
                             Body = o.Body,
                             jobnumber = j.JobNumber,
                             Prioritytype = o.Todopriority,
                             Id = o.Id
                         });

            var totalCount = await filteredLeads.CountAsync();
            return new PagedResultDto<GetActivityForViewDto>(
               totalCount,
               await leads.ToListAsync()
           );

            //return new ListResultDto<GetActivityForViewDto>(

            //    await leads.ToListAsync()
            //);
        }
        public async Task<LeadActivityLogDto> GetactivitydetailfromId(int id)
        {
            try
            {
                //var activity = await _leadactivityRepository.GetAll().Where(x => x.Id == id).FirstOrDefaultAsync(); 


                //var output = ObjectMapper.Map<LeadActivityLogDto>(activity);
                //return output;
                var activity = (from i in _leadactivityRepository.GetAll()
                                where (i.Id == id)
                                select new LeadActivityLogDto
                                {

                                    Id = i.Id,
                                    TenantId = i.TenantId,
                                    LeadId = i.LeadId,
                                    ActionId = i.ActionId,
                                    ActionNote = i.ActionNote,

                                    ActivityDate = i.ActivityDate,

                                    Body = i.Body,

                                    ActivityNote = i.ActivityNote,

                                    MessageId = i.MessageId,

                                    ReferanceId = i.ReferanceId,

                                    TodopriorityId = i.TodopriorityId,
                                    Todopriority = i.Todopriority,
                                    IsTodoComplete = i.IsTodoComplete,

                                    TodoResponse = i.TodoResponse,
                                    TodoTag = i.TodoTag,
                                    TodoDueDate = i.TodoDueDate,
                                    TodoresponseTime = i.TodoresponseTime
                                }).FirstOrDefault();
                return activity;

            }
            catch (Exception e) { throw e; }
        }

        public async Task markascompleteActivityLog(int id)
        {
            var activity = await _leadactivityRepository.GetAll().Where(x => x.Id == id).FirstOrDefaultAsync();
            activity.IsTodoComplete = true;
            await _leadactivityRepository.UpdateAsync(activity);
            var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var lead = await _leadRepository.FirstOrDefaultAsync(activity.LeadId);
            var assignedToUserForTodo = _userRepository.GetAll().Where(u => u.Id == activity.CreatorUserId).FirstOrDefault();
            var SectionName = _SectionAppService.GetAll().Where(e => e.SectionId == activity.SectionId).Select(e => e.SectionName).FirstOrDefault();
            string msg = activity.ActionNote + " " + activity.ActivityNote + " From " + SectionName + "Is Completed";
            await _appNotifier.LeadComment(assignedToUserForTodo, msg, NotificationSeverity.Warn);
        }

        public async Task updateActivityLog(LeadActivityLogDto input)
        {
            var activity = await _leadactivityRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefaultAsync();


            activity.ReferanceId = input.ReferanceId;
            activity.ActivityNote = input.ActivityNote;
            activity.ActionNote = input.ActionNote;
            activity.TodopriorityId = input.TodopriorityId;
            activity.Todopriority = input.Todopriority;
            activity.IsTodoComplete = input.IsTodoComplete;

            activity.TodoResponse = input.TodoResponse;
            activity.TodoTag = input.TodoTag;
            activity.TodoDueDate = input.TodoDueDate;
            activity.TodoresponseTime = input.TodoresponseTime;
            await _leadactivityRepository.UpdateAsync(activity);
            //activity.IsTodoComplete = true;

            var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var lead = await _leadRepository.FirstOrDefaultAsync(activity.LeadId);
            if (activity.ReferanceId == AbpSession.UserId)
            {
                var assignedToUserForTodo = _userRepository.GetAll().Where(u => u.Id == activity.CreatorUserId).FirstOrDefault();
                var SectionName = _SectionAppService.GetAll().Where(e => e.SectionId == activity.SectionId).Select(e => e.SectionName).FirstOrDefault();
                string msg = activity.ActionNote + " " + activity.ActivityNote + " From " + SectionName + "Is Updated";
                await _appNotifier.LeadComment(assignedToUserForTodo, msg, NotificationSeverity.Warn);
            }
            else
            {
                var assignedToUserForTodo = _userRepository.GetAll().Where(u => u.Id == activity.ReferanceId).FirstOrDefault();
                var SectionName = _SectionAppService.GetAll().Where(e => e.SectionId == activity.SectionId).Select(e => e.SectionName).FirstOrDefault();
                string msg = activity.ActionNote + " " + activity.ActivityNote + " From " + SectionName + "Is Updated";
                await _appNotifier.LeadComment(assignedToUserForTodo, msg, NotificationSeverity.Warn);
            }
        }

        public async Task SaveActivityLogTask(LeadActivityLogDto input)
        {
            if (AbpSession.TenantId != null)
            {
                input.TenantId = (int)AbpSession.TenantId;
            }
            if (input.Jobdetails != null)
            {
                int firstCommaIndex = input.Jobdetails.IndexOf('|');

                string firstPart = input.Jobdetails.Substring(0, firstCommaIndex);

                var jbid = _jobRepository.GetAll().Where(e => e.JobNumber == firstPart).Select(e => e.LeadId).FirstOrDefault();
                input.LeadId = Convert.ToInt32(jbid);
            }
            if (input.TodopriorityId == 1)
            { input.Todopriority = "Low"; }
            if (input.TodopriorityId == 2)
            { input.Todopriority = "Heigh"; }
            if (input.TodopriorityId == 3)
            { input.Todopriority = "Medium"; }
            input.ActionId = 25;
            input.ActionNote = "ToDo";
            input.SectionId = 21;

            var activity = ObjectMapper.Map<LeadActivityLog>(input);
            activity.CreatorUserId = AbpSession.UserId;
            await _leadactivityRepository.InsertAsync(activity);
            var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var lead = await _leadRepository.FirstOrDefaultAsync(input.LeadId);
            var assignedToUserForTodo = _userRepository.GetAll().Where(u => u.Id == input.ReferanceId).FirstOrDefault();
            var SectionName = _SectionAppService.GetAll().Where(e => e.SectionId == input.SectionId).Select(e => e.SectionName).FirstOrDefault();
            string msg = input.ActionNote + " " + input.ActivityNote + " By " + CurrentUser.Name + " From " + SectionName;
            await _appNotifier.LeadComment(assignedToUserForTodo, msg, NotificationSeverity.Warn);

        }

        public async Task<PagedResultDto<GetActivityForViewDto>> GetAllTodoActivityListFilterwise(GetAllActivityInputDto input)
        {
            try
            {

                var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

                var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
                IList<string> role = await _userManager.GetRolesAsync(User);

                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
                var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

                var filteredLeads = _leadactivityRepository.GetAll()
                    .WhereIf(input.taskid == 1, e => e.ReferanceId == AbpSession.UserId && e.IsTodoComplete != true)
                            .WhereIf(input.taskid == 2, e => e.CreatorUserId == AbpSession.UserId && e.IsTodoComplete != true)
                            .WhereIf(input.taskid == 3, e => e.ReferanceId == User.Id && e.IsTodoComplete == true)
                              .WhereIf(input.taskid == 4, e => e.CreatorUserId == AbpSession.UserId && e.IsTodoComplete == true)
                            //.WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(5).Date >= SDate.Value.Date)
                            //.WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(5).Date <= EDate.Value.Date)
                            .Where(e => e.LeadFk.OrganizationId == input.Organizationid && e.ActionId == 25);
                var pagedAndFilteredLeads = filteredLeads
                    .OrderBy(input.Sorting ?? "id desc")
                    .PageBy(input);

                var leads = (from o in pagedAndFilteredLeads
                             join o1 in _lookup_leadActionRepository.GetAll() on o.ActionId equals o1.Id into j1
                             from s1 in j1.DefaultIfEmpty()

                             join o2 in _leadRepository.GetAll() on o.LeadId equals o2.Id into j2
                             from s2 in j2.DefaultIfEmpty()

                             join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                             from s3 in j3.DefaultIfEmpty()
                             join o3 in _userRepository.GetAll() on (int)o.ReferanceId equals o3.Id into r3
                             from rf3 in r3.DefaultIfEmpty()
                             join j in _jobRepository.GetAll() on o.LeadId equals j.LeadId into j4
                             from j in j4.DefaultIfEmpty()


                             select new GetActivityForViewDto()
                             {
                                 ActionName = s1 == null || s1.ActionName == null ? "" : s1.ActionName.ToString(),
                                 ActionId = o.ActionId,
                                 ActionNote = o.ActionNote,
                                 ActivityNote = o.ActionId == 6 ? o.Body : o.ActivityNote,
                                 LeadId = o.LeadId,
                                 CreationTime = o.CreationTime,
                                 CreatorUserName = s3 == null || s3.FullName == null ? "" : s3.FullName.ToString(),
                                 AssignedTo = rf3 == null || rf3.FullName == null ? "" : rf3.FullName.ToString(),
                                 assignedtoid = (int?)rf3.Id,
                                 LeadCompanyName = s2 == null || s2.CompanyName == null ? "" : s2.CompanyName.ToString(),
                                 Body = o.Body,
                                 jobnumber = j.JobNumber,
                                 Prioritytype = o.Todopriority,
                                 Id = o.Id
                             });

                var totalCount = await filteredLeads.CountAsync();

                return new PagedResultDto<GetActivityForViewDto>(
                  totalCount,
                  await leads.ToListAsync()
              );
            }
            catch (Exception e) { throw e; }
        }

        public async Task<GetActivitytaskcountDto> getAllApplicationTrackerCount(GetAllActivityInputDto input)
        {
            var output = new GetActivitytaskcountDto();
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            if (input.taskid > 0)
            {
                output.taskcount = _leadactivityRepository.GetAll()
                        .WhereIf(input.taskid == 1, e => e.CreatorUserId == AbpSession.UserId && e.IsTodoComplete != true)
                         .WhereIf(input.taskid == 2, e => e.CreatorUserId == AbpSession.UserId && e.IsTodoComplete == true)
                         .WhereIf(input.taskid == 3, e => e.LeadFk.AssignToUserID == User.Id && e.IsTodoComplete == true)

                        //.WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(5).Date >= SDate.Value.Date)
                        //.WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(5).Date <= EDate.Value.Date)
                        .Where(e => e.LeadFk.OrganizationId == input.Organizationid && e.ActionId == 25).Select(e => e.Id).Count();
            }
            else
            {
                output.taskcount = _leadactivityRepository.GetAll()
                           .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                           .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                           .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                           //.WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(5).Date >= SDate.Value.Date)
                           //.WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(5).Date <= EDate.Value.Date)
                           .Where(e => e.LeadFk.OrganizationId == input.Organizationid && e.ActionId == 25 && e.IsTodoComplete != true).Select(e => e.Id).Count();
            }
            return output;
        }


        public async Task deleteActivityLog(int id)
        {
            var activity = await _leadactivityRepository.GetAll().Where(x => x.Id == id).FirstOrDefaultAsync();
            _leadactivityRepository.Delete(activity);
            //activity.IsTodoComplete = true;
            //await _leadactivityRepository.UpdateAsync(activity);
            //var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            //var lead = await _leadRepository.FirstOrDefaultAsync(activity.LeadId);
            //var assignedToUserForTodo = _userRepository.GetAll().Where(u => u.Id == activity.CreatorUserId).FirstOrDefault();
            //var SectionName = _SectionAppService.GetAll().Where(e => e.SectionId == activity.SectionId).Select(e => e.SectionName).FirstOrDefault();
            //string msg = activity.ActionNote + " " + activity.ActivityNote + " From " + SectionName + "Is Completed";
            //await _appNotifier.LeadComment(assignedToUserForTodo, msg, NotificationSeverity.Warn);
        }

        public async Task deleteAllActivityLog(List<int> ids)
        {
            foreach (var item in ids)
            {
                var activity = await _leadactivityRepository.GetAll().Where(x => x.Id == item).FirstOrDefaultAsync();
                _leadactivityRepository.Delete(activity);
                //await _leadactivityRepository.UpdateAsync(activity);
                var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                var lead = await _leadRepository.FirstOrDefaultAsync(activity.LeadId);
                var assignedToUserForTodo = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
                var SectionName = _SectionAppService.GetAll().Where(e => e.SectionId == activity.SectionId).Select(e => e.SectionName).FirstOrDefault();
                string msg = activity.ActionNote + " " + activity.ActivityNote + " From " + SectionName + "Is Deleted";
                await _appNotifier.LeadComment(assignedToUserForTodo, msg, NotificationSeverity.Warn);
            }
        }


        public async Task<FileDto> getSalesReportToExcel(SalesReportDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var filteredLeads = _jobRepository.GetAll()
                      .WhereIf(input.StartDate != null && input.EndDate != null, e => e.InstallationDate.Value >= SDate.Value.Date && e.InstallationDate.Value <= EDate.Value.Date)
                      .WhereIf(input.Paymenttypeid != null && input.Paymenttypeid.Count() > 0, e => input.Paymenttypeid.Contains(e.PaymentOptionId))
                      .Where(j => (_leadRepository.GetAll().Where(e => e.Id == j.LeadId).Select(e => e.OrganizationId).FirstOrDefault()) == input.OrganizationId);


            var query = (from o in filteredLeads
                         join o6 in _leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                         from s6 in j6.DefaultIfEmpty()
                         select new GetSalesReportViewDto()
                         {
                             JobStatusName = o.JobStatusFk.Name,
                             CustomerName = s6.CompanyName,
                             Address = o.UnitNo + ' ' + o.UnitType + ' ' + o.StreetNo + ' ' + o.StreetName + ' ' + o.StreetType,
                             Suburb = o.Suburb,
                             State = o.State,
                             PostalCode = o.PostalCode,
                             InstallationDate = o.InstallationDate,
                             STC = o.STC,
                             TotalCost = o.TotalCost,
                             WaveOff = ((_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == o.Id && e.InvoicePaymentMethodId == 5).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == o.Id && e.InvoicePaymentMethodId == 5).Select(e => e.PaidAmmount).Sum()))
                         });

            var salesreportDtos = await query.ToListAsync();
            return _systemReportExcelExporter.SalesReportExportToFile(salesreportDtos);
        }

        //public DataTable getOutstandingReport(SalesReportDto input)
        //{
        //    var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
        //    var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
        //    var lastmonth = SDate.Value.AddMonths(-1);
        //    var invoiceimport_List = _InvoiceImportDataRepository.GetAll();

        //    // set end-date to end of month
        //    var end = new DateTime(EDate.Value.Date.Year, EDate.Value.Date.Month, DateTime.DaysInMonth(EDate.Value.Date.Year, EDate.Value.Date.Month));

        //    var diff = Enumerable.Range(0, Int32.MaxValue)
        //                         .Select(e => SDate.Value.Date.AddMonths(e))
        //                         .TakeWhile(e => e <= end)
        //                         .Select(e => e.ToString("MMMM-yyyy"));

        //    var Month = diff.ToList();



        //    var filteredLeads = _jobRepository.GetAll()

        //                      .Where(o => (o.TotalCost - ((invoiceimport_List.Where(e => e.JobId == o.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (invoiceimport_List.Where(e => e.JobId == o.Id).Select(e => e.PaidAmmount).Sum())) > 0)
        //                      && o.InstallationDate.Value >= SDate.Value.Date && o.InstallationDate.Value <= EDate.Value.Date).ToList();

        //    if (filteredLeads != null)
        //    {
        //        DataTable dt = new DataTable();
        //        DataRow dr;
        //        dt.Columns.Add("JobNumber");
        //        dt.Columns.Add("CompanyName");
        //        dt.Columns.Add("TotalCost");
        //        dt.Columns.Add("PriviousOutStanding");

        //        for (int i = 0; i < Month.Count; i++)
        //        {
        //            dt.Columns.Add(Month[i]);
        //        }

        //        foreach (var item in filteredLeads)
        //        {
        //            var count = 0;
        //            dr = dt.NewRow();

        //            dr["JobNumber"] = item.JobNumber;
        //            dr["CompanyName"] = "Hiral";
        //            dr["TotalCost"] = item.TotalCost;
        //            var paidammount = ((invoiceimport_List.Where(e => e.JobId == item.Id && e.Date.Value.Date.Month == lastmonth.Date.Month && e.Date.Value.Date.Year == lastmonth.Date.Year).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (invoiceimport_List.Where(e => e.JobId == item.Id && e.Date.Value.Date.Month == lastmonth.Month && e.Date.Value.Date.Year == lastmonth.Date.Year).Select(e => e.PaidAmmount).Sum()));
        //            dr["PriviousOutStanding"] = (item.TotalCost - paidammount);

        //            for (int i = 0; i < Month.Count; i++)
        //            {
        //                string entcode = Month[i];
        //                int month = 0;
        //                int Year = 0;
        //                if (!string.IsNullOrWhiteSpace(entcode))
        //                {
        //                    string[] splitPipeValue = entcode.Split('-');
        //                    if (splitPipeValue.Length > 0)
        //                    {
        //                        month = DateTime.ParseExact(splitPipeValue[0], "MMMM", CultureInfo.CurrentCulture).Month;
        //                        Year = DateTime.ParseExact(splitPipeValue[1], "yyyy", CultureInfo.CurrentCulture).Year;
        //                    }
        //                }
        //                var priviousoutstanding = ((invoiceimport_List.Where(e => e.JobId == item.Id && e.Date.Value.Date.Month < month && e.Date.Value.Date.Year == Year).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (invoiceimport_List.Where(e => e.JobId == item.Id && e.Date.Value.Date.Month < month && e.Date.Value.Date.Year == Year).Select(e => e.PaidAmmount).Sum()));
        //                var monthwiseoutstandin = (item.TotalCost - ((invoiceimport_List.Where(e => e.JobId == item.Id && e.Date.Value.Date.Month == month && e.Date.Value.Date.Year == Year).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (invoiceimport_List.Where(e => e.JobId == item.Id && e.Date.Value.Date.Month == month && e.Date.Value.Date.Year == Year).Select(e => e.PaidAmmount).Sum())));
        //                var finalmonthwseoutsatnding = (monthwiseoutstandin - priviousoutstanding);
        //                dr[Month[i]] = finalmonthwseoutsatnding.ToString();
        //            }
        //            dt.Rows.Add(dr);
        //        }
        //        string FileName = "OutStanding Report" + DateTime.Now.Ticks + ".xls";

        //        GridView gv = new GridView();
        //        gv.DataSource = dt;
        //        gv.DataBind();
        //        Response.ClearContent();
        //        Response.Buffer = true;
        //        Response.AddHeader("content-disposition", "attachment; filename=" + FileName);
        //        Response.ContentEncoding = System.Text.Encoding.Unicode;
        //        Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

        //        Response.ContentType = "application/ms-excel";
        //        Response.Charset = "";
        //        StringWriter sw = new StringWriter();
        //        HtmlTextWriter htw = new HtmlTextWriter(sw);
        //        gv.RenderControl(htw);
        //        byte[] bytes = Encoding.UTF8.GetBytes(sw.ToString());

        //        string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "\\Temp\\", FileName);
        //        System.IO.File.WriteAllBytes(destPath, bytes);
        //        return Json(new { fileName = FileName });
        //    }
        //    else
        //    {
        //        DataTable dt = new DataTable();
        //        return dt;
        //    }



        //}

        public async Task<List<OutstandingReportDto>> getoutstandingsForReport(SalesReportDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var lastmonth = SDate.Value.AddMonths(-1);
            var invoiceimport_List = _InvoiceImportDataRepository.GetAll();
            var lead_List = _leadRepository.GetAll();
            // set end-date to end of month
            var end = new DateTime(EDate.Value.Date.Year, EDate.Value.Date.Month, DateTime.DaysInMonth(EDate.Value.Date.Year, EDate.Value.Date.Month));

            var diff = Enumerable.Range(0, Int32.MaxValue)
                                 .Select(e => SDate.Value.Date.AddMonths(e))
                                 .TakeWhile(e => e <= end)
                                 .Select(e => e.ToString("MMMM-yyyy"));

            var Month = diff.ToList();

            var filteredLeads = _jobRepository.GetAll()
                               .WhereIf(input.Paymenttypeid != null && input.Paymenttypeid.Count() > 0, e => input.Paymenttypeid.Contains(e.PaymentOptionId))
                               .WhereIf(input.OrganizationId != 0, e =>e.LeadFk.OrganizationId == input.OrganizationId)
                               .Where(o => o.InstallationDate.Value >= SDate.Value.Date && o.InstallationDate.Value <= EDate.Value.Date).ToList();

            //for (int i = 0; i < Month.Count; i++)
            //{
            //    dt.Columns.Add(Month[i]);
            //}
            List<OutstandingReportDto> objlst = new List<OutstandingReportDto>();
            foreach (var item in filteredLeads)
            {
                var count = 0;
                OutstandingReportDto obj = new OutstandingReportDto();

                obj.JobNumber= item.JobNumber;
                obj.CompanyName = lead_List.Where(e=>e.Id == item.LeadId).Select(e=>e.CompanyName).FirstOrDefault();
                obj.TotalCost =  item.TotalCost;
                var paidammount = ((invoiceimport_List.Where(e => e.JobId == item.Id && e.Date.Value.Date.Month == lastmonth.Date.Month && e.Date.Value.Date.Year == lastmonth.Date.Year).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (invoiceimport_List.Where(e => e.JobId == item.Id && e.Date.Value.Date.Month == lastmonth.Month && e.Date.Value.Date.Year == lastmonth.Date.Year).Select(e => e.PaidAmmount).Sum()));
                obj.PriviousOutStanding = (item.TotalCost - paidammount);
                obj.InstallationDate = item.InstallationDate;
                List<monthdetail> monthlist = new List<monthdetail>();
                for (int i = 0; i < Month.Count; i++)
                {
                    monthdetail mnt = new monthdetail();
                    string entcode = Month[i];
                    int month = 0;
                    int Year = 0;
                    if (!string.IsNullOrWhiteSpace(entcode))
                    {
                        string[] splitPipeValue = entcode.Split('-');
                        if (splitPipeValue.Length > 0)
                        {
                            month = DateTime.ParseExact(splitPipeValue[0], "MMMM", CultureInfo.CurrentCulture).Month;
                            Year = DateTime.ParseExact(splitPipeValue[1], "yyyy", CultureInfo.CurrentCulture).Year;
                        }
                    }
                    var privious = ((invoiceimport_List.Where(e => e.JobId == item.Id && e.Date.Value.Date.Month <= month && e.Date.Value.Date.Year == Year).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (invoiceimport_List.Where(e => e.JobId == item.Id && e.Date.Value.Date.Month <= month && e.Date.Value.Date.Year == Year).Select(e => e.PaidAmmount).Sum()));
                    if (privious != 0)
                    {
                        var priviousoutstanding = ((invoiceimport_List.Where(e => e.JobId == item.Id && e.Date.Value.Date.Month < month && e.Date.Value.Date.Year == Year).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (invoiceimport_List.Where(e => e.JobId == item.Id && e.Date.Value.Date.Month < month && e.Date.Value.Date.Year == Year).Select(e => e.PaidAmmount).Sum()));
                        var monthwiseoutstandin = (item.TotalCost - ((invoiceimport_List.Where(e => e.JobId == item.Id && e.Date.Value.Date.Month == month && e.Date.Value.Date.Year == Year).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (invoiceimport_List.Where(e => e.JobId == item.Id && e.Date.Value.Date.Month == month && e.Date.Value.Date.Year == Year).Select(e => e.PaidAmmount).Sum())));
                        var finalmonthwseoutsatnding = (monthwiseoutstandin - priviousoutstanding);
                        mnt.months = finalmonthwseoutsatnding;
                    }
                    else {
                        mnt.months = null;
                    }
                    mnt.HeaderName = Month[i];
                    monthlist.Add(mnt);
                }
                obj.months = monthlist;
                objlst.Add(obj);
               // dt.Rows.Add(dr);
            }
             return  objlst.ToList();
        }

        //[AbpAuthorize(AppPermissions.Pages_Report_JobCost)]
        //public async Task<PagedResultDto<GetAllJobCostViewDto>> GetJobCostReport(GetAllJobCostInput input)
        //{
        //    var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
        //    var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

        //    var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

        //    var filteredResult = _jobRepository.GetAll().Include(e => e.LeadFk)
        //                      .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.TotalCost != null);

        //    var filter = filteredResult
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.AreaType), e => e.LeadFk.Area == input.AreaType)
                        
        //                .WhereIf(input.DateType == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
        //                .WhereIf(input.DateType == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

        //                .WhereIf(input.DateType == "InstallationDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
        //                .WhereIf(input.DateType == "InstallationDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date);

        //    var pagedAndFilteredResult = filter.OrderBy(input.Sorting ?? "id desc").PageBy(input);

        //    var results = from o in pagedAndFilteredResult
        //                  select new GetAllJobCostViewDto()
        //                  {
        //                      Id = o.Id,
        //                      LeadId = o.LeadFk.Id,
        //                      JobNumber = o.JobNumber,
        //                      JobStatus = o.JobStatusFk.Name,
        //                      JobStatusColorClass = o.JobStatusFk.ColorClass,
        //                      SystemCapacity = o.SystemCapacity,
        //                      TotalCost = o.TotalCost,
        //                      STCCost = o.STC * 40,
        //                      PanelCost = 0,
        //                      InverterCost = 0,
        //                      BatteryCost = 0,
        //                      OtherCost = 0,
        //                      InstallerCost = 0,
        //                      AdvertisementCost = 0,
        //                      FixCost = 0,
        //                      PLCost = 0
        //                  };

        //    var totalCount = await filter.CountAsync();

        //    return new PagedResultDto<GetAllJobCostViewDto>(totalCount, await results.ToListAsync());
        //}
    }
}
