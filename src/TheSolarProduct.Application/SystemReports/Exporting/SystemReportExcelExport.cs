﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.SystemReports.Dto;

namespace TheSolarProduct.SystemReports.Exporting
{
    public class SystemReportExcelExport : NpoiExcelExporterBase, ISystemReportExcelExport
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public SystemReportExcelExport(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto SalesReportExportToFile(List<GetSalesReportViewDto> salesreportDtos)
        {
            return CreateExcelPackage(
                "InvoiceSalesReport.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet("InvoiceSalesReport");

                    AddHeader(
                        sheet,
                        "JobStatusName",
                        "CustomerName",
                        "Address",
                        "Suburb",
                        "State",
                        "PostalCode",
                        "InstallationDate",
                        "STC",
                        "TotalCost",
                        "WaveOff"
                        );

                    AddObjects(
                       sheet, 2, salesreportDtos,
                       _ => _.JobStatusName,
                       _ => _.CustomerName,
                       _ => _.Address,
                       _ => _.Suburb,
                       _ => _.State,
                       _ => _.PostalCode,
                       _ => _.InstallationDate,
                       _ => _.STC,
                       _ => _.TotalCost,
                       _ => _.WaveOff
                       );

                    for (var i = 1; i <= salesreportDtos.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[6], "yyyy-mm-dd");
                    }
                    sheet.AutoSizeColumn(6);
                    for (var i = 1; i <= salesreportDtos.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[7], "0.00");
                    }
                    sheet.AutoSizeColumn(7);
                    for (var i = 1; i <= salesreportDtos.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[8], "0.00");
                    }
                    sheet.AutoSizeColumn(8);
                    for (var i = 1; i <= salesreportDtos.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "0.00");
                    }
                    sheet.AutoSizeColumn(9);
                });
        }
        public FileDto OutstandingReport(List<OutstandingReportDto> salesreportDtos)
        {
            return CreateExcelPackage(
                "OutstandingReport.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet("OutstandingReport");

                    //AddHeader(
                    //    sheet,
                    //    "JobStatusName",
                    //    "CustomerName",
                    //    "Address",
                    //    "Suburb",
                    //    "State",
                    //    "PostalCode",
                    //    "InstallationDate",
                    //    "STC",
                    //    "TotalCost",
                    //    "WaveOff"
                    //    );

                    AddObjects(
                       sheet, 2, salesreportDtos,
                       _ => _.JobNumber,
                       _ => _.CompanyName,
                       _ => _.TotalCost,
                       _ => _.PriviousOutStanding,
                      _ => _.months.ToArray()
                       );
                });
        }
    }
}
