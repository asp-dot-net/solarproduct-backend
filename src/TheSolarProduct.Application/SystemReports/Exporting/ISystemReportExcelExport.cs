﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.SystemReports.Dto;

namespace TheSolarProduct.SystemReports.Exporting
{
    public interface ISystemReportExcelExport
    {
        FileDto SalesReportExportToFile(List<GetSalesReportViewDto> salesreportDtos);
        FileDto OutstandingReport(List<OutstandingReportDto> salesreportDtos);
    }
}
