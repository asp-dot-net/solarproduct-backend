﻿using System;
using System.Linq;
using TheSolarProduct.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using TheSolarProduct.TransportationCosts;
using TheSolarProduct.Series.Dto;
using TheSolarProduct.Currencies.Exporting;
using TheSolarProduct.Wholesales.Ecommerces;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using System.Threading.Tasks;
using TheSolarProduct.PostCodes.Dtos;
using TheSolarProduct.Common;
using TheSolarProduct.Storage;
using TheSolarProduct.MultiTenancy;
using Abp.Application.Services.Dto;
using TheSolarProduct.Currencies.Dtos;
using TheSolarProduct.Currencies;
using System.Collections.Generic;
using TheSolarProduct.Series;
using TheSolarProduct.Wholesales.DataVault;



namespace TheSolarProduct.Serieses
{
    public class SeriesesAppService : TheSolarProductAppServiceBase, ISeriesAppService
    {
        private readonly IRepository<Wholesales.Ecommerces.Series> _seriesRepository;

        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public SeriesesAppService(
            IRepository<Wholesales.Ecommerces.Series> seriesRepository, 
            IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
            IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            
            _seriesRepository = seriesRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;

        }

        public async Task<PagedResultDto<GetSeriesForViewDto>> GetAll(GetAllSeriesinputDto input)
        {
            var pseries = _seriesRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.SeriesName == input.Filter);

            var pagedAndFiltered = pseries
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFiltered
                         select new GetSeriesForViewDto()
                         {
                           Series = new SeriesDto
                           {
                                 Id = o.Id,
                                SeriesName= o.SeriesName,
                                ProductCategory = o.ProductTypeFK.Name,
                                ProductCategoryId = o.ProductCategoryId
                           }
                         };

            var totalCount = await pseries.CountAsync();

            return new PagedResultDto<GetSeriesForViewDto>(totalCount, await output.ToListAsync());
        }


        public async Task<GetSeriesForEditOutput> GetSeriesForEdit(EntityDto input)
        {
            var pseries = await _seriesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetSeriesForEditOutput { Series = ObjectMapper.Map<CreateOrEditSeriesDto>(pseries) };

            return output;
        }



        public async Task CreateOrEdit(CreateOrEditSeriesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(CreateOrEditSeriesDto input)
        {
            var pseries = ObjectMapper.Map<Wholesales.Ecommerces.Series>(input);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created Series";
            dataVaultLog.SectionId = 11;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "Series Created: " + input.SeriesName;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

            await _seriesRepository.InsertAsync(pseries);
        }

        protected virtual async Task Update(CreateOrEditSeriesDto input)
        {
            var pseries = await _seriesRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 11;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "Series Updated : " + pseries.SeriesName;
            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            var List = new List<WholeSaleDataVaultActivityLogHistory>();
            if (input.SeriesName != pseries.SeriesName)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = pseries.SeriesName;
                history.CurValue = input.SeriesName;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
           

            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, pseries);

            await _seriesRepository.UpdateAsync(pseries);
        }

        public async Task Delete(EntityDto input)
        {
            var Name = _seriesRepository.Get(input.Id).SeriesName;

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 11;
            dataVaultLog.Type = "DataVault";
            dataVaultLog.ActionNote = "Series Deleted  : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);

            await _seriesRepository.DeleteAsync(input.Id);
        }



    }


}
