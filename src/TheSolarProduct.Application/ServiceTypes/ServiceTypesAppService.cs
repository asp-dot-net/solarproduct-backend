﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ServiceSources.Dtos;
using TheSolarProduct.ServiceSources.Exporting;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.Dto;
using TheSolarProduct.ServiceTypes;
using TheSolarProduct.ServiceTypes.Dtos;
using TheSolarProduct.DataVaults;
using Abp.EntityFrameworkCore;
using TheSolarProduct.ServiceStatuses;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.StreetNames;

namespace TheSolarProduct.ServiceSources
{
    [AbpAuthorize(AppPermissions.Pages_ServiceType, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    public class ServiceTypesAppService : TheSolarProductAppServiceBase, IServiceTypesAppService
    {
        private readonly IRepository<ServiceType> _serviceTypesRepository;
        private readonly IServiceSourcesExcelExporter _serviceSourcesExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public ServiceTypesAppService(IRepository<ServiceType> serviceTypesRepository, IServiceSourcesExcelExporter serviceSourcesExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _serviceTypesRepository = serviceTypesRepository;
            _serviceSourcesExcelExporter = serviceSourcesExcelExporter;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;

        }

        public async Task<PagedResultDto<GetServiceTypeForViewDto>> GetAll(GetAllServiceTypeInput input)
        {

            var filteredServicetypes = _serviceTypesRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

            var pagedAndFilteredServicetypes = filteredServicetypes
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var servicetype = from o in pagedAndFilteredServicetypes
                              select new GetServiceTypeForViewDto()
                              {
                                  serviceTypes = new ServiceTypeDto
                                  {
                                      Name = o.Name,
                                      Id = o.Id,
                                      IsActive = o.IsActive,
                                  }
                              };

            var totalCount = await filteredServicetypes.CountAsync();

            return new PagedResultDto<GetServiceTypeForViewDto>(
                totalCount,
                await servicetype.ToListAsync()
            );
        }

        public async Task<GetServiceTypeForViewDto> GetServiceTypeForView(int id)
        {
            var serviceTypes = await _serviceTypesRepository.GetAsync(id);

            var output = new GetServiceTypeForViewDto { serviceTypes = ObjectMapper.Map<ServiceTypeDto>(serviceTypes) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceType_Edit)]
        public async Task<GetServiceTypeForEditOutput> GetServiceTypeForEdit(EntityDto input)
        {
            var serviceTypes = await _serviceTypesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetServiceTypeForEditOutput { serviceTypes = ObjectMapper.Map<CreateOrEditServiceTypeDto>(serviceTypes) };

            return output;
        }
        public async Task CreateOrEdit(CreateOrEditServiceTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceType_Create)]
        protected virtual async Task Create(CreateOrEditServiceTypeDto input)
        {
            var serviceTypes = ObjectMapper.Map<ServiceType>(input);

            if (AbpSession.TenantId != null)
            {
                serviceTypes.TenantId = (int)AbpSession.TenantId;
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 35;
            dataVaultLog.ActionNote = "Service Type Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _serviceTypesRepository.InsertAsync(serviceTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceType_Edit)]
        protected virtual async Task Update(CreateOrEditServiceTypeDto input)
        {
            var servicetypes = await _serviceTypesRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 35;
            dataVaultLog.ActionNote = "Service Type Updated : " + servicetypes.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != servicetypes.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = servicetypes.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != servicetypes.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = servicetypes.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, servicetypes);
        }

        [AbpAuthorize(AppPermissions.Pages_ServiceType_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _serviceTypesRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 35;
            dataVaultLog.ActionNote = "Service Type Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _serviceTypesRepository.DeleteAsync(input.Id);
        }
        //public async Task<FileDto> GetServiceTypeToExcel(GetAllServiceTypeForExcelInput input)
        //{

        //    var filteredServiceSources = _serviceTypesRepository.GetAll()
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

        //    var query = (from o in filteredServiceSources
        //                 select new GetServiceTypeForViewDto()
        //                 {
        //                     serviceTypes = new ServiceTypeDto
        //                     {
        //                         Name = o.Name,
        //                         Id = o.Id
        //                     }
        //                 });

        //    var serviceSourcesListDtos = await query.ToListAsync();

        //    return _serviceSourcesExcelExporter.ExportToFile(serviceSourcesListDtos);
        //}
    }
}
