﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Microsoft.Azure.KeyVault.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.Authorization;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Leads;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.MyInstallerActivityLogs;
using TheSolarProduct.Organizations;
using TheSolarProduct.QuickStocks;
using TheSolarProduct.StockOrders;
using TheSolarProduct.UserWiseEmailOrgs;
using TheSolarProduct.WholeSaleLeadActivityLogs;
using TheSolarProduct.WholeSaleLeads;

namespace TheSolarProduct.ApplicationSettings
{
    public class ApplicationSettingsAppService : TheSolarProductAppServiceBase, IApplicationSettingsAppService
    {
        private readonly IRepository<ApplicationSetting> _applicationSettingRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendedOrganizationUnitRepository;
        private readonly IRepository<MyInstallerActivityLog> _myinstalleractivityRepository;
        private readonly IRepository<UserWiseEmailOrg> _userWiseEmailOrgsRepository;
        private readonly IRepository<WholeSaleLeadActivityLog> _wholeSaleLeadactivityRepository;
        private readonly IRepository<WholeSaleLead> _wholeSaleLeadRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<QuickStockActivityLog> _quickStockActivityLogRepository;
        private readonly IRepository<PurchaseOrder> _purchaseOrderRepository;

        public ApplicationSettingsAppService(IRepository<ApplicationSetting> applicationSettingRepository
            , IRepository<Tenant> tenantRepository
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<LeadActivityLog> leadactivityRepository
            , IRepository<Lead> leadRepository
            , IRepository<ExtendOrganizationUnit, long> extendedOrganizationUnitRepository
            , IRepository<MyInstallerActivityLog> myinstalleractivityRepository
            , IRepository<UserWiseEmailOrg> userWiseEmailOrgsRepository
            , IRepository<WholeSaleLeadActivityLog> wholeSaleLeadactivityRepository
            , IRepository<WholeSaleLead> wholeSaleLeadRepository
            , IRepository<User,long> userRepository
            , IRepository<QuickStockActivityLog> quickStockActivityLogRepository
            , IRepository<PurchaseOrder> purchaseOrderRepository
            )
        {
            _applicationSettingRepository = applicationSettingRepository;
            _tenantRepository = tenantRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _leadactivityRepository = leadactivityRepository;
            _leadRepository = leadRepository;
            _extendedOrganizationUnitRepository = extendedOrganizationUnitRepository;
            _myinstalleractivityRepository = myinstalleractivityRepository;
            _userWiseEmailOrgsRepository = userWiseEmailOrgsRepository;
            _wholeSaleLeadactivityRepository = wholeSaleLeadactivityRepository;
            _wholeSaleLeadRepository = wholeSaleLeadRepository;
            _userRepository = userRepository;
            _quickStockActivityLogRepository = quickStockActivityLogRepository;
            _purchaseOrderRepository=purchaseOrderRepository;
        }

        public ApplicationSettingListDto GetApplicationSetting(int? tenantId)
        {
            var applicationSetting = _applicationSettingRepository.FirstOrDefault(X => X.TenantId == tenantId.Value);
            return ObjectMapper.Map<ApplicationSettingListDto>(applicationSetting);
        }

        public async Task EditApplicationSetting(EditApplicationSettingDto input)
        {
            if (AbpSession.TenantId != null)
            {
                var applicationSetting = _applicationSettingRepository.FirstOrDefault(X => X.TenantId == (int)AbpSession.TenantId);
                if (applicationSetting == null)
                {
                    var appsetting = ObjectMapper.Map<ApplicationSetting>(input);
                    if (AbpSession.TenantId != null)
                    {
                        appsetting.TenantId = (int)AbpSession.TenantId;
                    }
                    await _applicationSettingRepository.InsertAsync(appsetting);
                }
                else
                {
                    applicationSetting.TenantId = (int)AbpSession.TenantId;
                    applicationSetting.FoneDynamicsAccountSid = input.FoneDynamicsAccountSid;
                    applicationSetting.FoneDynamicsEnabled = input.FoneDynamicsEnabled;
                    applicationSetting.FoneDynamicsPhoneNumber = input.FoneDynamicsPhoneNumber;
                    applicationSetting.FoneDynamicsPropertySid = input.FoneDynamicsPropertySid;
                    applicationSetting.FoneDynamicsToken = input.FoneDynamicsToken;
                    await _applicationSettingRepository.UpdateAsync(applicationSetting);
                }
            }
        }

        public async Task SendSMS(SendSMSInput input)
        {

            if (AbpSession.TenantId != null)
            {
                SendSMSRequestInput smsReq = new SendSMSRequestInput();

                ExtendOrganizationUnit orgSmsCriteria = new ExtendOrganizationUnit();
                if (input.IsMyInstallerUser == false)
                {
                    var leadid = _leadactivityRepository.GetAll().Where(e => e.Id == input.ActivityId && e.TenantId == (int)AbpSession.TenantId).Select(e => e.LeadId).FirstOrDefault();
                    var orgID = _leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.OrganizationId).FirstOrDefault();
                    orgSmsCriteria = _extendedOrganizationUnitRepository.FirstOrDefault(X => X.Id == orgID);
                }
                else
                {
                    var myinstallerid = _myinstalleractivityRepository.GetAll().Where(e => e.Id == input.ActivityId && e.TenantId == (int)AbpSession.TenantId).Select(e => e.MyInstallerId).FirstOrDefault();
                    var orgID = _userWiseEmailOrgsRepository.GetAll().Where(e => e.UserId == myinstallerid).Select(e => e.OrganizationUnitId).FirstOrDefault();
                    orgSmsCriteria = _extendedOrganizationUnitRepository.FirstOrDefault(X => X.Id == orgID);
                }

                var applicationSetting = _applicationSettingRepository.FirstOrDefault(X => X.TenantId == (int)AbpSession.TenantId);
                string authCode = orgSmsCriteria.FoneDynamicsAccountSid + ":" + orgSmsCriteria.FoneDynamicsToken;
                byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(authCode);
                string headerToken = "BASIC " + System.Convert.ToBase64String(data);
                smsReq.From = orgSmsCriteria.FoneDynamicsPhoneNumber;
                //smsReq.From = "0488824984";
                smsReq.To = input.PhoneNumber;
                //smsReq.To = "+919924860723";
                smsReq.Text = input.Text;
                //smsReq.ExternalId = "1";

                if (input.promoresponseID > 0)
                {
                    smsReq.WebhookUri = "http://app.thesolarproduct.com/SMS?tenantid=" + AbpSession.TenantId + "&actionId=" + input.ActivityId + "&promoresid=" + input.promoresponseID + "&sectionid=" + input.SectionID;
                    smsReq.WebhookMethod = "POST";
                }
                else if (input.IsMyInstallerUser == true)
                {
                    smsReq.WebhookUri = "http://app.thesolarproduct.com/SMS?tenantid=" + AbpSession.TenantId + "&actionId=" + input.ActivityId + "&isMyInstallerUser=" + input.IsMyInstallerUser + "&sectionid=" + input.SectionID;
                    smsReq.WebhookMethod = "POST";
                }
                else if (input.ActivityId > 0)
                {
                    smsReq.WebhookUri = "http://app.thesolarproduct.com/SMS?tenantid=" + AbpSession.TenantId + "&actionId=" + input.ActivityId + "&sectionid=" + input.SectionID;
                    smsReq.WebhookMethod = "POST";
                }

                string restURL = "https://api.fonedynamics.com/v2/Properties/" + orgSmsCriteria.FoneDynamicsPropertySid + "/Messages";
                using (var httpClient = new HttpClient())
                {
                    StringContent content = new StringContent(JsonConvert.SerializeObject(smsReq), Encoding.UTF8, "application/json");
                    httpClient.DefaultRequestHeaders.Add("Authorization", headerToken);
                    using (var response = await httpClient.PostAsync(restURL, content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        var userObj = JObject.Parse(apiResponse);
                        var userGuid = Convert.ToString(userObj["Message"]["MessageSid"]);
                        if (input.ActivityId != 0)
                        {
                            if (input.IsMyInstallerUser == false)
                            {
                                var Activity = _leadactivityRepository.GetAll().Where(e => e.Id == input.ActivityId).FirstOrDefault();
                                Activity.MessageId = userGuid;
                                await _leadactivityRepository.UpdateAsync(Activity);
                            }
                            else
                            {
                                var Activity = _myinstalleractivityRepository.GetAll().Where(e => e.Id == input.ActivityId).FirstOrDefault();
                                Activity.MessageId = userGuid;
                                await _myinstalleractivityRepository.UpdateAsync(Activity);
                            }
                        }
                    }
                }

                //SendSMSRequestInput smsReq = new SendSMSRequestInput();
                ////var applicationSetting = _applicationSettingRepository.FirstOrDefault(X => X.TenantId == (int)AbpSession.TenantId);
                //string authCode = "AA76IMPVNDSNF6GBRMFZ5TGQOT4S5W2A" + ":" + "T5RVFUBY2QBY567J7DXSIJEIC5SW2FNA";
                //byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(authCode);
                //string headerToken = "BASIC " + System.Convert.ToBase64String(data);
                //smsReq.From = "+919924860723";
                //smsReq.To = "+61451831980";
                //smsReq.Text = "Test SMS By Number";

                //smsReq.WebhookUri = "http://app.thesolarproduct.com/SMS?tenantid=" + AbpSession.TenantId + "&actionId=" + 0 + "&promoresid=" + 0 + "&sectionid=" + 0;
                //smsReq.WebhookMethod = "POST";

                //string restURL = "https://api.fonedynamics.com/v2/Properties/" + "SPBBXC4F7AJKLMSZAUYUL4FPDHMKVUZA" + "/Messages";
                //using (var httpClient = new HttpClient())
                //{
                //    StringContent content = new StringContent(JsonConvert.SerializeObject(smsReq), Encoding.UTF8, "application/json");
                //    httpClient.DefaultRequestHeaders.Add("Authorization", headerToken);
                //    using (var response = await httpClient.PostAsync(restURL, content))
                //    {
                //        string apiResponse = await response.Content.ReadAsStringAsync();
                //        var userObj = JObject.Parse(apiResponse);
                //        var userGuid = Convert.ToString(userObj["Message"]["MessageSid"]);
                //    }
                //}
            }

        }

        public async Task SendBulkSMS(SendBulkSMSInput input)
        {
            if (AbpSession.TenantId != null)
            {
                //SendSMSRequestInput smsReq = new SendSMSRequestInput();
                var leadid = _leadactivityRepository.GetAll().Where(e => e.Id == input.Messages[0].ActivityId && e.TenantId == (int)AbpSession.TenantId).Select(e => e.LeadId).FirstOrDefault();
                var orgID = _leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.OrganizationId).FirstOrDefault();
                var orgSmsCriteria = _extendedOrganizationUnitRepository.FirstOrDefault(X => X.Id == orgID);

                //var applicationSetting = _applicationSettingRepository.FirstOrDefault(X => X.TenantId == (int)AbpSession.TenantId);
                string authCode = orgSmsCriteria.FoneDynamicsAccountSid + ":" + orgSmsCriteria.FoneDynamicsToken;
                byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(authCode);
                string headerToken = "BASIC " + System.Convert.ToBase64String(data);
                //smsReq.From = applicationSetting.FoneDynamicsPhoneNumber;
                //smsReq.To = input.PhoneNumber;
                //smsReq.Text = input.Text;

                var list = new Root();
                List<Message> listData = new List<Message>();

                foreach (var item in input.Messages)
                {
                    var listitem = new Message();

                    listitem.From = orgSmsCriteria.FoneDynamicsPhoneNumber;
                    listitem.To = item.PhoneNumber;
                    listitem.Text = item.Text;

                    if (item.promoresponseID > 0)
                    {
                        listitem.WebhookUri = "http://app.thesolarproduct.com/SMS?tenantid=" + AbpSession.TenantId + "&actionId=" + item.ActivityId + "&promoresid=" + item.promoresponseID;
                        listitem.WebhookMethod = "POST";
                    }
                    else if (item.ActivityId > 0)
                    {
                        listitem.WebhookUri = "http://app.thesolarproduct.com/SMS?tenantid=" + AbpSession.TenantId + "&actionId=" + item.ActivityId;
                        listitem.WebhookMethod = "POST";
                    }

                    listData.Add(listitem);
                }

                list.Messages = listData;

                string restURL = "https://api.fonedynamics.com/v2/Properties/" + orgSmsCriteria.FoneDynamicsPropertySid + "/BatchMessages";
                using (var httpClient = new HttpClient())
                {
                    StringContent content = new StringContent(JsonConvert.SerializeObject(list), Encoding.UTF8, "application/json");
                    httpClient.DefaultRequestHeaders.Add("Authorization", headerToken);
                    using (var response = await httpClient.PostAsync(restURL, content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                    }
                }
            }
        }

        public class Message
        {
            public string From { get; set; }
            public string To { get; set; }
            public string Text { get; set; }
            public string WebhookUri { get; set; }
            public string WebhookMethod { get; set; }
        }

        public class Root
        {
            public List<Message> Messages { get; set; }
        }


        public async Task SendWholeSaleSMS(SendSMSInput input)
        {

            if (AbpSession.TenantId != null)
            {
                SendSMSRequestInput smsReq = new SendSMSRequestInput();

                ExtendOrganizationUnit orgSmsCriteria = new ExtendOrganizationUnit();
                
                var leadid = _wholeSaleLeadactivityRepository.GetAll().Where(e => e.Id == input.ActivityId && e.TenantId == (int)AbpSession.TenantId).Select(e => e.WholeSaleLeadId).FirstOrDefault();
                var orgID = 0;

                if(leadid != 0 && leadid != null)
                {
                    orgID = _wholeSaleLeadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.OrganizationId).FirstOrDefault();

                }
                else { orgID = (int)input.oId; }

                orgSmsCriteria = _extendedOrganizationUnitRepository.FirstOrDefault(X => X.Id == orgID);
               

                var applicationSetting = _applicationSettingRepository.FirstOrDefault(X => X.TenantId == (int)AbpSession.TenantId);
                string authCode = orgSmsCriteria.FoneDynamicsAccountSid + ":" + orgSmsCriteria.FoneDynamicsToken;
                byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(authCode);
                string headerToken = "BASIC " + System.Convert.ToBase64String(data);
                smsReq.From = orgSmsCriteria.FoneDynamicsPhoneNumber;
                smsReq.To = input.PhoneNumber;
                smsReq.Text = input.Text;
                //smsReq.ExternalId = "1";

                if (input.promoresponseID > 0)
                {
                    smsReq.WebhookUri = "http://app.thesolarproduct.com/WholeSaleSMS?tenantid=" + AbpSession.TenantId + "&actionId=" + input.ActivityId + "&promoresid=" + input.promoresponseID + "&sectionid=" + input.SectionID;
                    smsReq.WebhookMethod = "POST";
                }
                //else if (input.IsMyInstallerUser == true)
                //{
                //    smsReq.WebhookUri = "http://app.thesolarproduct.com/WholeSaleSMS?tenantid=" + AbpSession.TenantId + "&actionId=" + input.ActivityId + "&isMyInstallerUser=" + input.IsMyInstallerUser + "&sectionid=" + input.SectionID;
                //    smsReq.WebhookMethod = "POST";
                //}
                else if (input.ActivityId > 0)
                {
                    smsReq.WebhookUri = "http://app.thesolarproduct.com/WholeSaleSMS?tenantid=" + AbpSession.TenantId + "&actionId=" + input.ActivityId + "&sectionid=" + input.SectionID;
                    smsReq.WebhookMethod = "POST";
                }

                string restURL = "https://api.fonedynamics.com/v2/Properties/" + orgSmsCriteria.FoneDynamicsPropertySid + "/Messages";
                using (var httpClient = new HttpClient())
                {
                    StringContent content = new StringContent(JsonConvert.SerializeObject(smsReq), Encoding.UTF8, "application/json");
                    httpClient.DefaultRequestHeaders.Add("Authorization", headerToken);
                    using (var response = await httpClient.PostAsync(restURL, content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        var userObj = JObject.Parse(apiResponse);
                        var userGuid = Convert.ToString(userObj["Message"]["MessageSid"]);
                        if (input.ActivityId != 0)
                        {
                            var Activity = _wholeSaleLeadactivityRepository.GetAll().Where(e => e.Id == input.ActivityId).FirstOrDefault();
                            Activity.MessageId = userGuid.ToString();
                            await _wholeSaleLeadactivityRepository.UpdateAsync(Activity);
                           
                        }
                        //test
                    }
                }
            }

        }

        public async Task SendWholeSaleBulkSMS(SendBulkSMSInput input)
        {
            if (AbpSession.TenantId != null)
            {
                SendSMSRequestInput smsReq = new SendSMSRequestInput();
                var leadid = _wholeSaleLeadactivityRepository.GetAll().Where(e => e.Id == input.Messages[0].ActivityId && e.TenantId == (int)AbpSession.TenantId).Select(e => e.WholeSaleLeadId).FirstOrDefault();
                var lead = _wholeSaleLeadRepository.GetAll().Where(e => e.Id == leadid).FirstOrDefault();
                var orgSmsCriteria = _extendedOrganizationUnitRepository.FirstOrDefault(X => X.Id == lead.OrganizationId);
                var applicationSetting = _applicationSettingRepository.FirstOrDefault(X => X.TenantId == (int)AbpSession.TenantId);

                if (orgSmsCriteria.smsFrom == "Fone Dynamics")
                {
                    string authCode = orgSmsCriteria.FoneDynamicsAccountSid + ":" + orgSmsCriteria.FoneDynamicsToken;
                    byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(authCode);
                    string headerToken = "BASIC " + System.Convert.ToBase64String(data);
                    //smsReq.From = applicationSetting.FoneDynamicsPhoneNumber;
                    //smsReq.To = input.PhoneNumber;
                    //smsReq.Text = input.Text;

                    var list = new Root();
                    List<Message> listData = new List<Message>();

                    foreach (var item in input.Messages)
                    {
                        var listitem = new Message();

                        listitem.From = applicationSetting.FoneDynamicsPhoneNumber;
                        listitem.To = item.PhoneNumber;
                        listitem.Text = item.Text;

                        if (item.promoresponseID > 0)
                        {
                            listitem.WebhookUri = "http://app.thesolarproduct.com/WholeSaleSMS?tenantid=" + AbpSession.TenantId + "&actionId=" + item.ActivityId + "&promoresid=" + item.promoresponseID + "&sectionid=" + item.SectionID;
                            listitem.WebhookMethod = "POST";
                        }
                        else if (item.ActivityId > 0)
                        {
                            listitem.WebhookUri = "http://app.thesolarproduct.com/WholeSaleSMS?tenantid=" + AbpSession.TenantId + "&actionId=" + item.ActivityId + "&sectionid=" + item.SectionID;
                            listitem.WebhookMethod = "POST";
                        }

                        listData.Add(listitem);
                    }

                    list.Messages = listData;

                    string restURL = "https://api.fonedynamics.com/v2/Properties/" + orgSmsCriteria.FoneDynamicsPropertySid + "/BatchMessages"; ;
                    using (var httpClient = new HttpClient())
                    {
                        StringContent content = new StringContent(JsonConvert.SerializeObject(list), Encoding.UTF8, "application/json");
                        httpClient.DefaultRequestHeaders.Add("Authorization", headerToken);
                        using (var response = await httpClient.PostAsync(restURL, content))
                        {
                            string apiResponse = await response.Content.ReadAsStringAsync();
                        }
                    }
                }
                else
                {
                    //string authCode = orgSmsCriteria.GateWayAccountEmail + ":" + orgSmsCriteria.GateWayAccountPassword;
                    //byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(authCode);
                    //string headerToken = "BASIC " + System.Convert.ToBase64String(data);
                    string headerToken = "Basic YWRtaW5Adm9pcHRlbGUuYXU6cVVldWUtcGFwYSMxODc2";

                    var list = new Root();
                    List<Message> listData = new List<Message>();

                    foreach (var item in input.Messages)
                    {
                        var listitem = new Message_3cx();

                        listitem .from_addr = _userRepository.GetAll().Where(e => e.Id == lead.AssignUserId).FirstOrDefault().Mobile;
                        //listitem .from_addr = "61437979565";
                        listitem.account_id = orgSmsCriteria.AccountId;
                        //listitem.account_id = "SRC14318";
                        listitem.to_addr = item.PhoneNumber;  
                        //listitem.to_addr = "61451831980";
                        listitem.content = item.Text;

                        listitem.unicode = false;

                        ////if (item.promoresponseID > 0)
                        ////{
                        ////    listitem.WebhookUri = "http://app.thesolarproduct.com/WholeSaleSMS?tenantid=" + AbpSession.TenantId + "&actionId=" + item.ActivityId + "&promoresid=" + item.promoresponseID + "&sectionid=" + item.SectionID;
                        ////    listitem.WebhookMethod = "POST";
                        ////}
                        ////else if (item.ActivityId > 0)
                        ////{
                        ////    listitem.WebhookUri = "http://app.thesolarproduct.com/WholeSaleSMS?tenantid=" + AbpSession.TenantId + "&actionId=" + item.ActivityId + "&sectionid=" + item.SectionID;
                        ////    listitem.WebhookMethod = "POST";
                        ////}

                        //listData.Add(listitem);



                        var client = new HttpClient();
                        var request = new HttpRequestMessage(HttpMethod.Post, "https://api.overthewire.com.au/api/v1/service/sms/");
                        request.Headers.Add("Authorization",headerToken);
                       
                        var content3cx = new StringContent(JsonConvert.SerializeObject(listitem), null, "application/json");
                        request.Content = content3cx;
                        var response3cx = await client.SendAsync(request);
                        response3cx.EnsureSuccessStatusCode();
                        string apiResponse = await response3cx.Content.ReadAsStringAsync();
                    }
                    //list.Messages = listData;

                    ////string restURL = "https://api.overthewire.com.au/api/v1/service/sms/" + orgSmsCriteria.AccountId + "/BatchMessages"; ;
                    //var request = new HttpRequestMessage(HttpMethod.Post, "https://api.overthewire.com.au/api/v1/service/sms/");

                    //using (var httpClient = new HttpClient())
                    //{
                    //    var content3cx = new StringContent(JsonConvert.SerializeObject(list), Encoding.UTF8, "application/json");
                    //    request.Content = content3cx;
                    //    request.Headers.Add("Authorization", headerToken);

                    //    using (var response = await httpClient.SendAsync(request))
                    //    {
                    //        string apiResponse = await response.Content.ReadAsStringAsync();
                    //    }
                    //}


                }

                //var client = new HttpClient();
                //var request = new HttpRequestMessage(HttpMethod.Post, "https://api.overthewire.com.au/api/v1/service/sms/");
                //request.Headers.Add("Authorization", "Basic YWRtaW5Adm9pcHRlbGUuYXU6cVVldWUtcGFwYSMxODc2");
                //var content = new StringContent("{\r\n\r\n  \"account_id\": \"SRC14318\",\r\n\r\n  \"to_addr\": \"61433770782\",\r\n\r\n  \"from_addr\": \"61437979565\",\r\n\r\n  \"content\": \"Hello Ashok, how are you\",\r\n\r\n  \"unicode\": false\r\n\r\n}", null, "application/json");
                //request.Content = content;
                //var response = await client.SendAsync(request);
                //response.EnsureSuccessStatusCode();
                //Console.WriteLine(await response.Content.ReadAsStringAsync());



            }

           

        }

        public class Message_3cx
        {
            public string account_id { get; set; }
            public string to_addr { get; set; }
            public string from_addr { get; set; }
            public string content { get; set; }
            public bool unicode { get; set; }
        }

        public async Task SendStockOrderSMS(SendSMSInput input)
        {

            if (AbpSession.TenantId != null)
            {
                SendSMSRequestInput smsReq = new SendSMSRequestInput();

                ExtendOrganizationUnit orgSmsCriteria = new ExtendOrganizationUnit();
               
                var purchaseid = _quickStockActivityLogRepository.GetAll().Where(e => e.Id == input.ActivityId && e.TenantId == (int)AbpSession.TenantId).Select(e => e.PurchaseOrderId).FirstOrDefault();
                var orgID = 0;

                if (purchaseid != 0 && purchaseid != null)
                {
                    orgID = _purchaseOrderRepository.GetAll().Where(e => e.Id == purchaseid).Select(e => e.OrganizationId).FirstOrDefault();

                }
                else { orgID = (int)input.oId; }

                orgSmsCriteria = _extendedOrganizationUnitRepository.FirstOrDefault(X => X.Id == orgID);


                var applicationSetting = _applicationSettingRepository.FirstOrDefault(X => X.TenantId == (int)AbpSession.TenantId);
                string authCode = orgSmsCriteria.FoneDynamicsAccountSid + ":" + orgSmsCriteria.FoneDynamicsToken;
                byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(authCode);
                string headerToken = "BASIC " + System.Convert.ToBase64String(data);
                smsReq.From = orgSmsCriteria.FoneDynamicsPhoneNumber;
                smsReq.To = input.PhoneNumber;
                smsReq.Text = input.Text;
                //smsReq.ExternalId = "1";

                //if (input.promoresponseID > 0)
                //{
                //    smsReq.WebhookUri = "http://app.thesolarproduct.com/WholeSaleSMS?tenantid=" + AbpSession.TenantId + "&actionId=" + input.ActivityId + "&promoresid=" + input.promoresponseID + "&sectionid=" + input.SectionID;
                //    smsReq.WebhookMethod = "POST";
                //}
                //else if (input.IsMyInstallerUser == true)
                //{
                //    smsReq.WebhookUri = "http://app.thesolarproduct.com/WholeSaleSMS?tenantid=" + AbpSession.TenantId + "&actionId=" + input.ActivityId + "&isMyInstallerUser=" + input.IsMyInstallerUser + "&sectionid=" + input.SectionID;
                //    smsReq.WebhookMethod = "POST";
                //}
                 if (input.ActivityId > 0)
                {
                    smsReq.WebhookUri = "http://app.thesolarproduct.com/StockOrderSMS?tenantid=" + AbpSession.TenantId + "&actionId=" + input.ActivityId + "&sectionid=" + input.SectionID;
                    smsReq.WebhookMethod = "POST";
                }

                string restURL = "https://api.fonedynamics.com/v2/Properties/" + orgSmsCriteria.FoneDynamicsPropertySid + "/Messages";
                using (var httpClient = new HttpClient())
                {
                    StringContent content = new StringContent(JsonConvert.SerializeObject(smsReq), Encoding.UTF8, "application/json");
                    httpClient.DefaultRequestHeaders.Add("Authorization", headerToken);
                    using (var response = await httpClient.PostAsync(restURL, content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        var userObj = JObject.Parse(apiResponse);
                        var userGuid = Convert.ToString(userObj["Message"]["MessageSid"]);
                        if (input.ActivityId != 0)
                        {
                            var Activity = _quickStockActivityLogRepository.GetAll().Where(e => e.Id == input.ActivityId).FirstOrDefault();
                            Activity.MessageId = userGuid.ToString();
                            await _quickStockActivityLogRepository.UpdateAsync(Activity);

                        }
                        //test
                    }
                }
            }

        }
    }
}
