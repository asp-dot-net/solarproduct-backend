﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.PurchaseDocumentLists.Dtos;

namespace TheSolarProduct.PurchaseDocumentLists.Exporting
{
    public interface IPurchaseDocumentListExcelExporter
    {
        FileDto ExportToFile(List<GetPurchaseDocumentListForViewDto> PurchaseDocumentList);
    }
}
