﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.PurchaseCompanys.Dtos;
using TheSolarProduct.PurchaseCompanys.Exporting;
using TheSolarProduct.PurchaseDocumentLists.Dtos;
using TheSolarProduct.StockPayments;
using TheSolarProduct.Storage;

namespace TheSolarProduct.PurchaseDocumentLists.Exporting
{
   
    public class PurchaseDocumentListExcelExporter : NpoiExcelExporterBase, IPurchaseDocumentListExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PurchaseDocumentListExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetPurchaseDocumentListForViewDto> PurchaseDocumentList)
        {
            return CreateExcelPackage(
                "PurchaseDocumentList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("PurchaseDocumentList"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Size"),
                        L("IsActive"),
                        L("Formats")

                    );

                    AddObjects(
                        sheet,
                        2,
                        PurchaseDocumentList,
                        _ => _.PurchaseDocumentList.Name,
                        _ => _.PurchaseDocumentList.Size,
                       _ => (bool)_.PurchaseDocumentList.IsActive ? L("Yes") : L("No"),
                        _ => string.Join(",", _.PurchaseDocumentList.Formats)
                    );
                    for (var i = 1; i <= PurchaseDocumentList.Count; i++)
                    {

                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[1], "0.00");
                       

                    }
                }
            );
        }


    }
}
