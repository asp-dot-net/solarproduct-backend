﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataVaults;
using TheSolarProduct.Dto;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.PurchaseCompanies;
using TheSolarProduct.PurchaseCompanys.Dtos;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.PurchaseDocumentLists.Exporting;
using TheSolarProduct.PurchaseDocumentListes;
using TheSolarProduct.PurchaseDocumentLists.Dtos;
using Abp.Linq.Extensions;
using System.Linq;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;


namespace TheSolarProduct.PurchaseDocumentLists
{
    
    [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseDocumentList)]
    public class PurchaseDocumentListAppService : TheSolarProductAppServiceBase, IPurchaseDocumentListAppService
    {
        private readonly IPurchaseDocumentListExcelExporter _purchaseDocumentListExcelExporter;

        private readonly IRepository<PurchaseDocumentList> _purchaseDocumentListRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public PurchaseDocumentListAppService(
              IRepository<PurchaseDocumentList> purchaseDocumentListRepository,
              IPurchaseDocumentListExcelExporter IPurchaseDocumentListExcelExporter,
              IRepository<UserTeam> userTeamRepository,
              IRepository<UserRole, long> userRoleRepository,
              IRepository<Role> roleRepository,
              IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
              )
        {
            _purchaseDocumentListRepository = purchaseDocumentListRepository;
            _purchaseDocumentListExcelExporter = IPurchaseDocumentListExcelExporter;
            _userTeamRepository = userTeamRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }




        public async Task<PagedResultDto<GetPurchaseDocumentListForViewDto>> GetAll(GetAllPurchaseDocumentListInput input)
        {
            
            var query = _purchaseDocumentListRepository.GetAll()
                .WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFilteredQuery = query
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var filteredAndPagedResults = await pagedAndFilteredQuery.ToListAsync();

            var output = filteredAndPagedResults.Select(o => new GetPurchaseDocumentListForViewDto
            {
                PurchaseDocumentList = new PurchaseDocumentListDto
                {
                    Id = o.Id,
                    Name = o.Name,
                    IsActive = o.IsActive,
                    Size = o.Size,
                    Formats = o.Format.Split(',').ToList()
                }
            }).ToList();

            var totalCount = await query.CountAsync();

            return new PagedResultDto<GetPurchaseDocumentListForViewDto>(totalCount, output);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseDocumentList_Edit)]
        public async Task<GetPurchaseDocumentListForEditOutput> GetPurchaseDocumentListForEdit(EntityDto input)
        {
            var purchaseDocumentList = await _purchaseDocumentListRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPurchaseDocumentListForEditOutput
            {
                PurchaseDocumentList = ObjectMapper.Map<CreateOrEditPurchaseDocumentListDto>(purchaseDocumentList)
            };

            output.PurchaseDocumentList.Formats = purchaseDocumentList.Format.Split(',').ToList();

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditPurchaseDocumentListDto input)
        {


            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseDocumentList_Create)]

        protected virtual async Task Create(CreateOrEditPurchaseDocumentListDto input)
        {
            var purchaseDocumentList = ObjectMapper.Map<PurchaseDocumentList>(input);

            
            purchaseDocumentList.Format = string.Join(",", input.Formats);

           
            await _purchaseDocumentListRepository.InsertAsync(purchaseDocumentList);


            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 70;
            dataVaultLog.ActionNote = "PurchaseDocumentList Created : " + input.Name;
            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseDocumentList_Edit)]
        protected virtual async Task Update(CreateOrEditPurchaseDocumentListDto input)
        {
            var PurchaseDocumentList = await _purchaseDocumentListRepository.FirstOrDefaultAsync((int)input.Id);
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 70;
            dataVaultLog.ActionNote = "PurchaseDocumentList Updated : " + PurchaseDocumentList.Name;

            dataVaultLog.IsInventory = true;
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != PurchaseDocumentList.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = PurchaseDocumentList.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Size != PurchaseDocumentList.Size)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Size";
                history.PrevValue = PurchaseDocumentList.Size.ToString();
                history.CurValue = input.Size.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (!Enumerable.SequenceEqual(
                              input.Formats.Select(s => s.Trim()),
                                               PurchaseDocumentList.Format.Split(',').Select(s => s.Trim())))
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Formats";
                history.PrevValue = PurchaseDocumentList.Format; 
                history.CurValue = string.Join(",", input.Formats); 
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }




            if (input.IsActive != PurchaseDocumentList.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = PurchaseDocumentList.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            PurchaseDocumentList.Format = string.Join(",", input.Formats);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, PurchaseDocumentList);

            await _purchaseDocumentListRepository.UpdateAsync(PurchaseDocumentList);
        }
        
        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseDocumentList_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _purchaseDocumentListRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 70;
            dataVaultLog.ActionNote = "PurchaseDocumentList Deleted : " + Name;

            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _purchaseDocumentListRepository.DeleteAsync(input.Id);
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseDocumentList_Export)]

        public async Task<FileDto> GetPurchaseDocumentListToExcel(GetAllPurchaseDocumentListForExcelInput input)
        {

         
            var query = _purchaseDocumentListRepository.GetAll()
               .WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

           

            var filteredAndPagedResults = await query.ToListAsync();

            var output = filteredAndPagedResults.Select(o => new GetPurchaseDocumentListForViewDto
            {
                PurchaseDocumentList = new PurchaseDocumentListDto
                {
                    Id = o.Id,
                    Name = o.Name,
                    IsActive = o.IsActive,
                    Size = o.Size,
                    Formats = o.Format.Split(',').ToList()
                }
            }).ToList();

           

            return _purchaseDocumentListExcelExporter.ExportToFile(output);
        }
    }
}
