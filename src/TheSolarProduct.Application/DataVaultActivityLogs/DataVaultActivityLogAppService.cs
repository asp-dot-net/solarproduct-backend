﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;

using TheSolarProduct.DocumentTypes.Dtos;
using TheSolarProduct.Quotations;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Jobs;
using TheSolarProduct.DataVaultActivityLogs.Dtos;
using TheSolarProduct.Departments;
using TheSolarProduct.Authorization.Users;
using Telerik.Reporting;

namespace TheSolarProduct.DataVaultActivityLogs
{
    public class DataVaultActivityLogAppService : TheSolarProductAppServiceBase, IDataVaultActivityLogAppService
    {
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IRepository<DataVaultActivityLogHistory> _dataVaultActivityLogHistoryRepository;
        private readonly IRepository<User, long> _userRepository;

        public DataVaultActivityLogAppService(
            IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
            IRepository<DataVaultActivityLogHistory> dataVaultActivityLogHistoryRepository,
            IRepository<User, long> userRepository
            ) { 
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dataVaultActivityLogHistoryRepository = dataVaultActivityLogHistoryRepository;
            _userRepository = userRepository;
        }

        public async Task<PagedResultDto<GetAllActivityLogDto>> GetAllDataVaultActivityLog(GetActivityLogInputDto input)
        {
            var ActivityLogIds = _dataVaultActivityLogRepository.GetAll().Where(e => e.IsInventory == input.IsInventory).GroupBy(e => e.SectionId).Select(e => e.Max(m => m.Id)).ToList();

           // var ActivityLogIds = Activities.Select(e => e.Id).ToList();

            var datavaultActivityLog = _dataVaultActivityLogRepository.GetAll().Where(e => ActivityLogIds.Contains(e.Id)).AsNoTracking().Select(e => new { e.Id, e.SectionId, e.SectionFK.SectionName, e.CreatorUserId, e.CreationTime, e.Action, e.ActionNote });

            var pagedAndFilteredDatavaultActivityLog = datavaultActivityLog
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input).ToList();

            var result = from o in pagedAndFilteredDatavaultActivityLog
                         let user = _userRepository.Get((long)o.CreatorUserId).FullName

                         select new GetAllActivityLogDto
                         {
                             Id = o.Id,
                             SectionId = (int)o.SectionId,
                             SectionName = o.SectionName,
                             UserName = user,
                             LastModificationTime = o.CreationTime,
                             ActionName = o.Action,
                             ActionNote = o.ActionNote
                         };

            var totalCount = await datavaultActivityLog.CountAsync();

            return new PagedResultDto<GetAllActivityLogDto>(
            totalCount,
                result.ToList()
            );
        }

        public async Task<List<GetAllActivityLogDto>> GetDataVaultActivityLogDetail(int sectionId)
        {
            var datavaultActivityLog = _dataVaultActivityLogRepository.GetAll().Where(e => e.SectionId == sectionId).AsNoTracking().Select(e => new { e.Id, e.SectionId, e.SectionFK.SectionName, e.CreatorUserId, e.CreationTime, e.Action, e.ActionNote }).OrderByDescending(e => e.Id).ToList();

            var result = from o in datavaultActivityLog

                         select new GetAllActivityLogDto
                         {
                             Id = o.Id,
                             SectionId = (int)o.SectionId,
                             SectionName = o.SectionName,
                             UserName = _userRepository.Get((long)o.CreatorUserId).FullName,
                             LastModificationTime = o.CreationTime,
                             ActionName = o.Action,
                             ActionNote = o.ActionNote
                         };

            return result.ToList();

        }


        public async Task<List<GetActivityLogHistoryDto>> GetDataVaultActivityLogHistory(int activityLogId)
        {
            var History  = _dataVaultActivityLogHistoryRepository.GetAll().Where(e => e.ActivityLogId == activityLogId).AsNoTracking().Select(e => new {e.Id, e.Action, e.PrevValue, e.CurValue, e.FieldName});

            var result = from o in History
                         select new GetActivityLogHistoryDto
                         {
                             Id = o.Id,
                             ActionName = o.Action,
                             prevValue = o.PrevValue,
                             curValue = o.CurValue,
                             FieldName = o.FieldName
                         };

            return await result.ToListAsync();
        }
    }
}
