﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataVaults;
using TheSolarProduct.Dto;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.PaymentStatuses.Dtos;
using TheSolarProduct.PaymentStatuses.Exporting;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace TheSolarProduct.PaymentStatuses
{
    
    [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentStatus)]
    public class PaymentStatusAppService : TheSolarProductAppServiceBase, IPaymentStatusAppService
    {
        private readonly IPaymentStatusExcelExporter _paymentStatusExcelExporter;

        private readonly IRepository<PaymentStatus> _paymentStatusRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public PaymentStatusAppService(
              IRepository<PaymentStatus> paymentStatusRepository,
              IPaymentStatusExcelExporter IPaymentStatusExcelExporter,
              IRepository<UserTeam> userTeamRepository,
              IRepository<UserRole, long> userRoleRepository,
              IRepository<Role> roleRepository,
              IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
              )
        {
            _paymentStatusRepository = paymentStatusRepository;
            _paymentStatusExcelExporter = IPaymentStatusExcelExporter;
            _userTeamRepository = userTeamRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }




        public async Task<PagedResultDto<GePaymentStatusForViewDto>> GetAll(GetAllPyamentStatusInput input)
        {

            var PyamentStatus = _paymentStatusRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFilteredPyamentStatus = PyamentStatus
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var outputPyamentStatus = from o in pagedAndFilteredPyamentStatus
                                      select new GePaymentStatusForViewDto()

                                        {
                                            PaymentStatus = new PyamentStatusDto
                                            {
                                                Id = o.Id,
                                                Name = o.Name,
                                                IsActive = o.IsActive
                                            }
                                        };

            var totalCount = await PyamentStatus.CountAsync();

            return new PagedResultDto<GePaymentStatusForViewDto>(
                totalCount,
                await outputPyamentStatus.ToListAsync()
            );
        }



        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentStatus_Edit)]
        public async Task<GetPaymentStatusForEditOutput> GetPaymentStatusForEdit(EntityDto input)
        {
            var PaymentStatus = await _paymentStatusRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPaymentStatusForEditOutput { PaymentStatus = ObjectMapper.Map<CreateOrEditPaymentStatusDto>(PaymentStatus) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditPaymentStatusDto input)
        {


            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentStatus_Create)]

        protected virtual async Task Create(CreateOrEditPaymentStatusDto input)
        {
            var PaymentStatus = ObjectMapper.Map<PaymentStatus>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 63;
            dataVaultLog.ActionNote = "PaymentStatus Created : " + input.Name;
            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _paymentStatusRepository.InsertAsync(PaymentStatus);
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PurchaseCompany_Edit)]
        protected virtual async Task Update(CreateOrEditPaymentStatusDto input)
        {
            var PaymentStatus = await _paymentStatusRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 63;
            dataVaultLog.ActionNote = "PaymentStatus Updated : " + PaymentStatus.Name;

            dataVaultLog.IsInventory = true;
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != PaymentStatus.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = PaymentStatus.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != PaymentStatus.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = PaymentStatus.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, PaymentStatus);

            await _paymentStatusRepository.UpdateAsync(PaymentStatus);


        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentStatus_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _paymentStatusRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 63;
            dataVaultLog.ActionNote = "PaymentStatus Deleted : " + Name;

            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _paymentStatusRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_QuickStock_DataVaults_PaymentStatus_Export)]

        public async Task<FileDto> GetPaymentStatusToExcel(GetAllPaymentStatusForExcelInput input)
        {

            var filteredPurchaseCompany = _paymentStatusRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredPurchaseCompany
                         select new GePaymentStatusForViewDto()
                         {
                             PaymentStatus = new PyamentStatusDto
                             {
                                 Name = o.Name,
                                 Id = o.Id,
                                 IsActive = o.IsActive
                             }
                         });


            var ListDtos = await query.ToListAsync();

            return _paymentStatusExcelExporter.ExportToFile(ListDtos);
        }
    }
}
