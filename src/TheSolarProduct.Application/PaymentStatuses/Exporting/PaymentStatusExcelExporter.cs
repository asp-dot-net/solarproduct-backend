﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.PaymentStatuses.Dtos;
using TheSolarProduct.PurchaseCompanys.Dtos;
using TheSolarProduct.PurchaseCompanys.Exporting;
using TheSolarProduct.Storage;

namespace TheSolarProduct.PaymentStatuses.Exporting
{
    
    public class PaymentStatusExcelExporter : NpoiExcelExporterBase, IPaymentStatusExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PaymentStatusExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GePaymentStatusForViewDto> PaymentStatus)
        {
            return CreateExcelPackage(
                "PaymentStatus.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("PaymentStatus"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("IsActive")
                    );

                    AddObjects(
                        sheet,
                        2,
                        PaymentStatus,
                        _ => _.PaymentStatus.Name,
                        _ => _.PaymentStatus.IsActive.HasValue ? (_.PaymentStatus.IsActive.Value ? L("Yes") : L("No")) : L("No")
                    );
                }
            );
        }


    }
}
