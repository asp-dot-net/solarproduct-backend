﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.PaymentStatuses.Dtos;

namespace TheSolarProduct.PaymentStatuses.Exporting
{
    public interface IPaymentStatusExcelExporter
    {
        FileDto ExportToFile(List<GePaymentStatusForViewDto> PaymentStatus);
    }
}
