﻿using TheSolarProduct.Leads;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheSolarProduct.Authorization;
using TheSolarProduct.Authorization.Users;
using Abp.Domain.Repositories;
using TheSolarProduct.WholeSalePromotions.Exporting;
using TheSolarProduct.WholeSalePromotions.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization.Users;
using TheSolarProduct.LeadStatuses;
using JetBrains.Annotations;
using TheSolarProduct.Jobs;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.WholeSaleLeadActivityLogs;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.ApplicationSettings;
using System;
using Abp.Timing.Timezone;
using TheSolarProduct.Invoices;
using TheSolarProduct.Promotions;
using TheSolarProduct.WholeSaleStatuses;
using TheSolarProduct.WholeSaleLeads;

namespace TheSolarProduct.WholeSalePromotions
{
    //[AbpAuthorize(AppPermissions.Pages_WholeSalePromotionUsers)]
    [AbpAuthorize]
    public class WholeSalePromotionUsersAppService : TheSolarProductAppServiceBase, IWholeSalePromotionUsersAppService
    {
        private readonly IRepository<WholeSalePromotionUser> _WholeSalePromotionUserRepository;
        private readonly IWholeSalePromotionUsersExcelExporter _WholeSalePromotionUsersExcelExporter;
        private readonly IRepository<WholeSalePromotion, int> _lookup_WholeSalePromotionRepository;
        private readonly IRepository<WholeSaleLead, int> _lookup_leadRepository;
        private readonly IRepository<WholeSaleStatus, int> _leadStatusRepository;
        private readonly IRepository<WholeSalePromotionResponseStatus, int> _lookup_WholeSalePromotionResponseStatusRepository;
        private readonly IRepository<PromotionType, int> _lookup_WholeSalePromotionTypeRepository;

        private readonly IRepository<User, long> _userRepository;
        //private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        //private readonly IRepository<JobStatus, int> _lookup_jobStatusRepository;
        private readonly IRepository<WholeSaleLeadActivityLog> _leadactivityRepository;
        private readonly IRepository<Team, int> _teamRepository;
        private readonly UserManager _userManager;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;

        public WholeSalePromotionUsersAppService(
              IRepository<WholeSalePromotionUser> WholeSalePromotionUserRepository,
              IWholeSalePromotionUsersExcelExporter WholeSalePromotionUsersExcelExporter,
              IRepository<WholeSalePromotion, int> lookup_WholeSalePromotionRepository,
              IRepository<WholeSaleLead, int> lookup_leadRepository,
              IRepository<WholeSalePromotionResponseStatus, int> lookup_WholeSalePromotionResponseStatusRepository,
              IRepository<User, long> userRepository,
              IRepository<WholeSaleStatus, int> leadStatusRepository,
              IRepository<PromotionType, int> lookup_WholeSalePromotionTypeRepository,
              IRepository<Job> jobRepository,
              IRepository<UserTeam> userTeamRepository,
              IRepository<JobStatus, int> lookup_jobStatusRepository,
              IRepository<WholeSaleLeadActivityLog> leadactivityRepository,
              IRepository<Team, int> teamRepository,
              UserManager userManager,
              IApplicationSettingsAppService applicationSettings,
              ITimeZoneConverter timeZoneConverter,
              IRepository<InvoicePayment> invoicePaymentRepository

                )
        {
            _WholeSalePromotionUserRepository = WholeSalePromotionUserRepository;
            _WholeSalePromotionUsersExcelExporter = WholeSalePromotionUsersExcelExporter;
            _lookup_WholeSalePromotionRepository = lookup_WholeSalePromotionRepository;
            _lookup_leadRepository = lookup_leadRepository;
            _lookup_WholeSalePromotionResponseStatusRepository = lookup_WholeSalePromotionResponseStatusRepository;
            _userRepository = userRepository;
            _leadStatusRepository = leadStatusRepository;
            _lookup_WholeSalePromotionTypeRepository = lookup_WholeSalePromotionTypeRepository;
            //_jobRepository = jobRepository;
            _userTeamRepository = userTeamRepository;
            //_lookup_jobStatusRepository = lookup_jobStatusRepository;
            _leadactivityRepository = leadactivityRepository;
            _teamRepository = teamRepository;
            _userManager = userManager;
            _applicationSettings = applicationSettings;
            _timeZoneConverter = timeZoneConverter;
            _invoicePaymentRepository = invoicePaymentRepository;
        }

        public async Task<PagedResultDto<GetWholeSalePromotionUserForViewDto>> GetAll(GetAllWholeSalePromotionUsersInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var team_list = _userTeamRepository.GetAll();
            var TeamIds = team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = team_list.Where(e => TeamIds.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            var leadactive_list = _leadactivityRepository.GetAll().Where(e => e.WholeSaleLeadFk.OrganizationId == input.OrganizationUnit);
            var jobnumberlist = new List<int?>();
            //jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

            //var jobstatuss = new List<int?>();
            //if (input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0)
            //{
            //    jobstatuss = _jobRepository.GetAll().Where(e => input.JobStatusIDFilter.Contains((int)e.JobStatusId)).Select(e => e.LeadId).ToList();
            //}

            var UserLists = new List<long?>();
            if (input.TeamId != 0 && input.TeamId != null)
            {
                UserLists = team_list.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }


            var readfilterlist = new List<int?>();
            if (input.ReadUnreadsms == 1)
            {
                var readLedActivityids = leadactive_list.Where(e => e.ActionId == 51 && e.IsMark == true).Select(e => e.ReferanceId).Distinct().ToList();
                readfilterlist = leadactive_list.Where(e => readLedActivityids.Contains(e.Id)).Select(e => e.PromotionUserId).Distinct().ToList();
            }
            var unreadfilterlist = new List<int?>();
            if (input.ReadUnreadsms == 2)
            {
                var unreadLedActivityids = leadactive_list.Where(e => e.ActionId == 51 && e.IsMark == false).Select(e => e.ReferanceId).Distinct().ToList();
                unreadfilterlist = leadactive_list.Where(e => unreadLedActivityids.Contains(e.Id)).Select(e => e.PromotionUserId).Distinct().ToList();
            }
            var filteredWholeSalePromotionUsers = _WholeSalePromotionUserRepository.GetAll()
                        .Include(e => e.WholeSalePromotionFk)
                        .Include(e => e.WholeSaleLeadFk)
                        .Include(e => e.WholeSalePromotionResponseStatusFk)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.ResponseMessage.Contains(input.Filter) || e.WholeSaleLeadFk.CompanyName.Contains(input.Filter) || e.WholeSaleLeadFk.Mobile.Contains(input.Filter) || e.WholeSaleLeadFk.Email.Contains(input.Filter) || e.WholeSaleLeadFk.Talephone.Contains(input.Filter))
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFk.Mobile.Contains(input.Filter))
                        .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFk.Talephone.Contains(input.Filter))
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFk.CompanyName.Contains(input.Filter))
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFk.Email.Contains(input.Filter))
                        .WhereIf(input.FilterName == "ResponseMessage" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ResponseMessage.Contains(input.Filter))
                        .WhereIf(input.ResponseDateFilter == "Responce" && SDate != null && EDate != null, e => e.ResponseDate.AddHours(10).Date >= SDate.Value.Date && e.ResponseDate.AddHours(10).Date <= EDate.Value.Date)
                        //.WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => jobstatuss.Contains(e.WholeSaleLeadId))
                        .WhereIf(input.WholeSalePromotionTitleFilter != 0, e => e.WholeSalePromotionId == input.WholeSalePromotionTitleFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCopanyNameFilter), e => e.WholeSaleLeadFk != null && e.WholeSaleLeadFk.CompanyName == input.LeadCopanyNameFilter)
                        .WhereIf(input.WholeSalePromotionResponseSatusIdFilter != null && input.WholeSalePromotionResponseSatusIdFilter != 0, e => e.WholeSalePromotionResponseStatusFk != null && e.WholeSalePromotionResponseStatusFk.Id == input.WholeSalePromotionResponseSatusIdFilter)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.WholeSaleLeadFk.AssignUserId == input.SalesRepId)
                        //.WhereIf(input.TeamId != null && input.TeamId != 0, e => UserLists.Contains(e.WholeSaleLeadFk.AssignUserId))
                        .WhereIf(input.OrganizationUnit != null, e => e.WholeSalePromotionFk.OrganizationID == input.OrganizationUnit)
                        .WhereIf(input.PromoType != 0, e => e.WholeSalePromotionFk.PromotionTypeId == input.PromoType)
                        .WhereIf(input.ReadUnreadsms == 1, e => readfilterlist.Contains(e.Id))
                        .WhereIf(input.ReadUnreadsms == 2, e => unreadfilterlist.Contains(e.Id))
                        .WhereIf(role.Contains("Admin"), e => e.WholeSaleLeadFk.AssignUserId != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.WholeSaleLeadFk.AssignUserId))
                        .WhereIf(role.Contains("Sales Rep"), e => e.WholeSaleLeadFk.AssignUserId == User.Id);
                        //.Where(e => e.WholeSalePromotionResponseStatusId != null);

            var pagedAndFilteredWholeSalePromotionUsers = filteredWholeSalePromotionUsers
                .OrderBy(input.Sorting ?? "responseDate desc")
                .PageBy(input);

            //var JobStatus = _jobRepository.GetAll().Where(e => e.JobStatusId == 1).Select(e => e.LeadId).ToList();
            //var deposite = _invoicePaymentRepository.GetAll().Where(e => e.InvoicePaymentStatusId == 2).Select(e => e.JobFk.LeadId).Distinct().ToList();
            var WholeSalePromotionUsers = from o in pagedAndFilteredWholeSalePromotionUsers
                                 join o1 in _lookup_WholeSalePromotionRepository.GetAll() on o.WholeSalePromotionId equals o1.Id into j1
                                 from s1 in j1.DefaultIfEmpty()

                                 join o2 in _lookup_leadRepository.GetAll() on o.WholeSaleLeadId equals o2.Id into j2
                                 from s2 in j2.DefaultIfEmpty()

                                 join o3 in _lookup_WholeSalePromotionResponseStatusRepository.GetAll() on o.WholeSalePromotionResponseStatusId equals o3.Id into j3
                                 from s3 in j3.DefaultIfEmpty()

                                 join o4 in _userRepository.GetAll() on s2.AssignUserId.ToString() equals o4.Id.ToString() into j4
                                 from s4 in j4.DefaultIfEmpty()

                                 join o5 in _leadStatusRepository.GetAll() on s2.WholeSaleStatusId.ToString() equals o5.Id.ToString()

                                 //join o6 in _jobRepository.GetAll() on o.WholeSaleLeadId equals o6.LeadId into j6
                                 //from s6 in j6.DefaultIfEmpty()

                                 //join o7 in _lookup_jobStatusRepository.GetAll() on s6.JobStatusId equals o7.Id into j7
                                 //from s7 in j7.DefaultIfEmpty()


                                     //join o8 in _leadactivityRepository.GetAll() on o.Id equals o8.WholeSalePromotionUserId into j8
                                     //from s8 in j8.DefaultIfEmpty()
                                     //join o6 in _lookup_WholeSalePromotionTypeRepository.GetAll() on o.WholeSalePromotionTypeId equals o6.Id into j5
                                     //from s6 in j1.DefaultIfEmpty()

                                 select new GetWholeSalePromotionUserForViewDto()
                                 {
                                     WholeSalePromotionUser = new WholeSalePromotionUserDto
                                     {
                                         ResponseDate = o.ResponseDate,
                                         WholeSalePromotionResponseStatusId = o.WholeSalePromotionResponseStatusId,
                                         ResponseMessage = o.ResponseMessage,
                                         Id = o.Id

                                     },

                                     getWholeSalePromotionForViewDto = new GetWholeSalePromotionForViewDto
                                     {
                                         WholeSalePromotion = new WholeSalePromotionDto
                                         {
                                             Id = s1.Id,
                                             Title = s1.Title,
                                             PromoCharge = s1.PromoCharge,
                                             PromotionTypeId = s1.PromotionTypeId,
                                             Description = s1.Description,
                                             CreationTime = s1.CreationTime,
                                         },
                                         WholeSalePromotionTypeName = s1.PromotionTypeFk.Name

                                     },

                                     WholeSalePromotionTitle = s1 == null || s1.Title == null ? "" : s1.Title.ToString(),
                                     LeadCopanyName = s2 == null || s2.CompanyName == null ? "" : s2.CompanyName.ToString(),
                                     Mobile = s2 == null || s2.Mobile == null ? "" : s2.Mobile.ToString(),
                                     EMail = s2 == null || s2.Email == null ? "" : s2.Email.ToString(),
                                     AssignedToName = s4 == null || s2.AssignUserId == null ? "" : s4.Name,
                                     WholeSalePromotionResponseStatusName = s3 == null || s3.Name == null ? "" : s3.Name.ToString(),
                                     LeadStatus = o5.Name,
                                     //ProjectStatus = s7.Name,
                                     //ProjectNumber = s6.JobNumber,
                                     //Mark = leadactive_list.Where(e => e.ReferanceId == s8.Id).Select(e => e.IsMark).FirstOrDefault(),
                                     LeadId = s2.Id,
                                     ActivityReminderTime = leadactive_list.Where(e => e.SectionId == 34 && e.ActionId == 8 && e.WholeSaleLeadId == s2.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                                     ActivityDescription = leadactive_list.Where(e => e.SectionId == 34 && e.ActionId == 8 && e.WholeSaleLeadId == s2.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                     ActivityComment = leadactive_list.Where(e => e.SectionId == 34 && e.ActionId == 24 && e.WholeSaleLeadId == s2.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                     //TotalWholeSalePromotion = filteredWholeSalePromotionUsers.Select(e => e.Id).Count(),
                                     //InterestedCount = filteredWholeSalePromotionUsers.Where(e => e.WholeSalePromotionResponseStatusId == 1 && e.ResponseMessage != null).Select(e => e.Id).Count(),
                                     //NotInterestedCount = filteredWholeSalePromotionUsers.Where(e => e.WholeSalePromotionResponseStatusId == 2 && e.ResponseMessage != null).Select(e => e.Id).Count(),
                                     //OtherCount = filteredWholeSalePromotionUsers.Where(e => e.WholeSalePromotionResponseStatusId == 3 && e.ResponseMessage != null).Select(e => e.Id).Count(),
                                     //Unhandle = filteredWholeSalePromotionUsers.Where(e => e.LeadFk.LeadStatusId != 6 && e.ResponseMessage != null).Select(e => e.Id).Count(),
                                     //WholeSalePromotionSold = filteredWholeSalePromotionUsers.Where(e => deposite.Contains((int)e.LeadId) && e.ResponseMessage != null).Select(e => e.Id).Count(),
                                     //WholeSalePromotionProject = filteredWholeSalePromotionUsers.Where(e => JobStatus.Contains((int)e.LeadId) && e.ResponseMessage != null).Select(e => e.Id).Count()
                                 };

            var totalCount = await filteredWholeSalePromotionUsers.CountAsync();

            return new PagedResultDto<GetWholeSalePromotionUserForViewDto>(
                totalCount,
                await WholeSalePromotionUsers.ToListAsync()
            );
        }
        public async Task<WholeSalePromotioncount> GetAllCount(GetAllWholeSalePromotionUsersInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var team_list = _userTeamRepository.GetAll();
            var TeamIds = team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = team_list.Where(e => TeamIds.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            var leadactive_list = _leadactivityRepository.GetAll().Where(e => e.WholeSaleLeadFk.OrganizationId == input.OrganizationUnit);
            var jobnumberlist = new List<int?>();
            //jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

            //var jobstatuss = new List<int?>();
            //if (input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0)
            //{
            //    jobstatuss = _jobRepository.GetAll().Where(e => input.JobStatusIDFilter.Contains((int)e.JobStatusId)).Select(e => e.LeadId).ToList();
            //}

            var UserLists = new List<long?>();
            if (input.TeamId != 0 && input.TeamId != null)
            {
                UserLists = team_list.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }

            var readfilterlist = new List<int?>();
            if (input.ReadUnreadsms == 1)
            {
                var readLedActivityids = leadactive_list.Where(e => e.ActionId == 51 && e.IsMark == true).Select(e => e.ReferanceId).Distinct().ToList();
                readfilterlist = leadactive_list.Where(e => readLedActivityids.Contains(e.Id)).Select(e => e.PromotionUserId).Distinct().ToList();
            }
            var unreadfilterlist = new List<int?>();
            if (input.ReadUnreadsms == 2)
            {
                var unreadLedActivityids = leadactive_list.Where(e => e.ActionId == 51 && e.IsMark == false).Select(e => e.ReferanceId).Distinct().ToList();
                unreadfilterlist = leadactive_list.Where(e => unreadLedActivityids.Contains(e.Id)).Select(e => e.PromotionUserId).Distinct().ToList();
            }
            var filteredWholeSalePromotionUsers = _WholeSalePromotionUserRepository.GetAll()
                        .Include(e => e.WholeSalePromotionFk)
                        .Include(e => e.WholeSaleLeadFk)
                        .Include(e => e.WholeSalePromotionResponseStatusFk)
                         //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.ResponseMessage.Contains(input.Filter) || e.WholeSaleLeadFk.CompanyName.Contains(input.Filter) || e.WholeSaleLeadFk.Mobile.Contains(input.Filter) || e.WholeSaleLeadFk.Email.Contains(input.Filter) || e.WholeSaleLeadFk.Talephone.Contains(input.Filter))
                         .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFk.Mobile.Contains(input.Filter))
                        .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFk.Talephone.Contains(input.Filter))
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFk.CompanyName.Contains(input.Filter))
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFk.Email.Contains(input.Filter))
                        .WhereIf(input.FilterName == "ResponseMessage" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ResponseMessage.Contains(input.Filter))
                        .WhereIf(input.ResponseDateFilter == "Responce" && SDate != null && EDate != null, e => e.ResponseDate.AddHours(10).Date >= SDate.Value.Date && e.ResponseDate.AddHours(10).Date <= EDate.Value.Date)
                        //.WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => jobstatuss.Contains(e.WholeSaleLeadId))
                        .WhereIf(input.WholeSalePromotionTitleFilter != 0, e => e.WholeSalePromotionId == input.WholeSalePromotionTitleFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCopanyNameFilter), e => e.WholeSaleLeadFk != null && e.WholeSaleLeadFk.CompanyName == input.LeadCopanyNameFilter)
                        .WhereIf(input.WholeSalePromotionResponseSatusIdFilter != null && input.WholeSalePromotionResponseSatusIdFilter != 0, e => e.WholeSalePromotionResponseStatusFk != null && e.WholeSalePromotionResponseStatusFk.Id == input.WholeSalePromotionResponseSatusIdFilter)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.WholeSaleLeadFk.AssignUserId == input.SalesRepId)
                        .WhereIf(input.TeamId != null && input.TeamId != 0, e => UserLists.Contains(e.WholeSaleLeadFk.AssignUserId))
                        .WhereIf(input.OrganizationUnit != null, e => e.WholeSalePromotionFk.OrganizationID == input.OrganizationUnit)
                        .WhereIf(input.PromoType != 0, e => e.WholeSalePromotionFk.PromotionTypeId == input.PromoType)
                        .WhereIf(input.ReadUnreadsms == 1, e => readfilterlist.Contains(e.Id))
                        .WhereIf(input.ReadUnreadsms == 2, e => unreadfilterlist.Contains(e.Id))
                        .WhereIf(role.Contains("Admin"), e => e.WholeSaleLeadFk.AssignUserId != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.WholeSaleLeadFk.AssignUserId))
                        .WhereIf(role.Contains("Sales Rep"), e => e.WholeSaleLeadFk.AssignUserId == User.Id)
                        .Where(e => e.WholeSalePromotionResponseStatusId != null);

            var pagedAndFilteredWholeSalePromotionUsers = filteredWholeSalePromotionUsers
                .OrderBy(input.Sorting ?? "responseDate desc")
                .PageBy(input);

            //var JobStatus = _jobRepository.GetAll().Where(e => e.JobStatusId == 1).Select(e => e.LeadId).ToList();
            var deposite = _invoicePaymentRepository.GetAll().Where(e => e.InvoicePaymentStatusId == 2).Select(e => e.JobFk.LeadId).Distinct().ToList();
            var output = new WholeSalePromotioncount();
            output.TotalWholeSalePromotion = filteredWholeSalePromotionUsers.Select(e => e.Id).Count();
            output.InterestedCount = filteredWholeSalePromotionUsers.Where(e => e.WholeSalePromotionResponseStatusId == 1 && e.ResponseMessage != null).Select(e => e.Id).Count();
            output.NotInterestedCount = filteredWholeSalePromotionUsers.Where(e => e.WholeSalePromotionResponseStatusId == 2 && e.ResponseMessage != null).Select(e => e.Id).Count();
            output.OtherCount = filteredWholeSalePromotionUsers.Where(e => e.WholeSalePromotionResponseStatusId == 3 && e.ResponseMessage != null).Select(e => e.Id).Count();
            output.Unhandle = filteredWholeSalePromotionUsers.Where(e => e.WholeSaleLeadFk.WholeSaleStatusId != 6 && e.ResponseMessage != null).Select(e => e.Id).Count();
            output.WholeSalePromotionSold = filteredWholeSalePromotionUsers.Where(e => deposite.Contains((int)e.WholeSaleLeadId) && e.ResponseMessage != null).Select(e => e.Id).Count();
            //output.WholeSalePromotionProject = filteredWholeSalePromotionUsers.Where(e => JobStatus.Contains((int)e.WholeSaleLeadId) && e.ResponseMessage != null).Select(e => e.Id).Count();

            return output;
        }
        public async Task<GetWholeSalePromotionUserForViewDto> GetWholeSalePromotionUserForView(int id)
        {
            var WholeSalePromotionUser = await _WholeSalePromotionUserRepository.GetAsync(id);

            var output = new GetWholeSalePromotionUserForViewDto { WholeSalePromotionUser = ObjectMapper.Map<WholeSalePromotionUserDto>(WholeSalePromotionUser) };

            if (output.WholeSalePromotionUser.PromotionId != null)
            {
                var _lookupWholeSalePromotion = await _lookup_WholeSalePromotionRepository.FirstOrDefaultAsync((int)output.WholeSalePromotionUser.PromotionId);
                output.WholeSalePromotionTitle = _lookupWholeSalePromotion?.Title?.ToString();
            }

            if (output.WholeSalePromotionUser.LeadId != null)
            {
                var _lookupLead = await _lookup_leadRepository.FirstOrDefaultAsync((int)output.WholeSalePromotionUser.LeadId);
                output.LeadCopanyName = _lookupLead?.CompanyName?.ToString();
            }

            if (output.WholeSalePromotionUser.WholeSalePromotionResponseStatusId != null)
            {
                var _lookupWholeSalePromotionResponseStatus = await _lookup_WholeSalePromotionResponseStatusRepository.FirstOrDefaultAsync((int)output.WholeSalePromotionUser.WholeSalePromotionResponseStatusId);
                output.WholeSalePromotionResponseStatusName = _lookupWholeSalePromotionResponseStatus?.Name?.ToString();
            }

            return output;
        }

        ///[AbpAuthorize(AppPermissions.Pages_WholeSalePromotionUsers_Edit)]
        public async Task<GetWholeSalePromotionUserForEditOutput> GetWholeSalePromotionUserForEdit(EntityDto input)
        {
            var WholeSalePromotionUser = await _WholeSalePromotionUserRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetWholeSalePromotionUserForEditOutput { WholeSalePromotionUser = ObjectMapper.Map<CreateOrEditWholeSalePromotionUserDto>(WholeSalePromotionUser) };

            if (output.WholeSalePromotionUser.PromotionId != null)
            {
                var _lookupWholeSalePromotion = await _lookup_WholeSalePromotionRepository.FirstOrDefaultAsync((int)output.WholeSalePromotionUser.PromotionId);
                output.WholeSalePromotionTitle = _lookupWholeSalePromotion?.Title?.ToString();
            }

            if (output.WholeSalePromotionUser.LeadId != null)
            {
                var _lookupLead = await _lookup_leadRepository.FirstOrDefaultAsync((int)output.WholeSalePromotionUser.LeadId);
                output.LeadCopanyName = _lookupLead?.CompanyName?.ToString();
            }

            if (output.WholeSalePromotionUser.WholeSalePromotionResponseStatusId != null)
            {
                var _lookupWholeSalePromotionResponseStatus = await _lookup_WholeSalePromotionResponseStatusRepository.FirstOrDefaultAsync((int)output.WholeSalePromotionUser.WholeSalePromotionResponseStatusId);
                output.WholeSalePromotionResponseStatusName = _lookupWholeSalePromotionResponseStatus?.Name?.ToString();
            }

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditWholeSalePromotionUserDto input)
        {
            if (input.PromotionId != null && input.PromotionId != null)
            {
                //If WholeSalePromotionid and leadid already exist it , shoult edit
                var qry = _WholeSalePromotionUserRepository.GetAll()
                    .Where(e => e.WholeSalePromotionId == input.PromotionId && e.WholeSaleLeadId == input.LeadId)
                    .ToList();

                if (qry != null && qry.Count > 0) input.Id = qry[0].Id;
            }

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        ///[AbpAuthorize(AppPermissions.Pages_WholeSalePromotionUsers_Create)]
        protected virtual async Task Create(CreateOrEditWholeSalePromotionUserDto input)
        {
            var WholeSalePromotionUser = ObjectMapper.Map<WholeSalePromotionUser>(input);


            if (AbpSession.TenantId != null)
            {
                WholeSalePromotionUser.TenantId = (int)AbpSession.TenantId;
            }


            await _WholeSalePromotionUserRepository.InsertAsync(WholeSalePromotionUser);
        }

        //	[AbpAuthorize(AppPermissions.Pages_WholeSalePromotionUsers_Edit)]
        protected virtual async Task Update(CreateOrEditWholeSalePromotionUserDto input)
        {
            var WholeSalePromotionUser = await _WholeSalePromotionUserRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, WholeSalePromotionUser);
        }

        public virtual async Task UpdateWholeSalePromotionResponseStatus(int WholeSalePromotionUserId, int value)
        {
            var WholeSalePromotionUser = await _WholeSalePromotionUserRepository.FirstOrDefaultAsync(WholeSalePromotionUserId);
            var Lead = _lookup_leadRepository.GetAll().Where(e => e.Id == WholeSalePromotionUser.WholeSaleLeadId).FirstOrDefault();
            WholeSalePromotionUser.WholeSalePromotionResponseStatusId = value;
            await _WholeSalePromotionUserRepository.UpdateAsync(WholeSalePromotionUser);

            var Activity = _leadactivityRepository.GetAll().Where(e => e.PromotionUserId == WholeSalePromotionUserId).FirstOrDefault();
            //var LeadActivity = _leadactivityRepository.GetAll().Where(e => e.ReferanceId == Activity.Id && e.ActionId == 52).FirstOrDefault();
            Activity.IsMark = true;
            await _leadactivityRepository.UpdateAsync(Activity);

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
            leadactivity.ActionId = 6;
            leadactivity.SectionId = 34;
            leadactivity.ActionNote = "Sms Send From WholeSalePromotion Tacker";
            leadactivity.WholeSaleLeadId = (int)WholeSalePromotionUser.WholeSaleLeadId;
            leadactivity.ActivityDate = DateTime.UtcNow;
            leadactivity.TemplateId = 0;

            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            if (value == 1)
            {
                leadactivity.Body = "Thanks for the reply. We will get back to you soon.";
            }
            else
            {
                leadactivity.Body = "You have been successfully unsubscribed.";
            }
            await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);
            var sendBulkSMSInput = new SendBulkSMSInput();

            List<SendSMSInput> msg = new List<SendSMSInput>();

            if (value == 1)
            {
                if (!string.IsNullOrEmpty(Lead.Mobile))
                {
                    SendSMSInput sendSMSInput = new SendSMSInput();
                    sendSMSInput.PhoneNumber = Lead.Mobile;
                    sendSMSInput.ActivityId = leadactivity.Id;
                    sendSMSInput.Text = "Thanks for the reply. We will get back to you soon.";
                    sendSMSInput.SectionID = 34;
                    msg.Add(sendSMSInput);
                }
            }
            else if (value == 2)
            {
                if (!string.IsNullOrEmpty(Lead.Mobile))
                {
                    SendSMSInput sendSMSInput = new SendSMSInput();
                    sendSMSInput.PhoneNumber = Lead.Mobile;
                    sendSMSInput.ActivityId = leadactivity.Id;
                    sendSMSInput.Text = "You have been successfully unsubscribed.";
                    sendSMSInput.SectionID = 34;
                    msg.Add(sendSMSInput);

                }
            }
            sendBulkSMSInput.Messages = msg;

            await _applicationSettings.SendWholeSaleBulkSMS(sendBulkSMSInput);

        }

        ////[AbpAuthorize(AppPermissions.Pages_WholeSalePromotionUsers_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _WholeSalePromotionUserRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetWholeSalePromotionUsersToExcel(GetAllWholeSalePromotionUsersForExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var team_list = _userTeamRepository.GetAll();
            var TeamIds = team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = team_list.Where(e => TeamIds.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            var leadactive_list = _leadactivityRepository.GetAll().Where(e => e.WholeSaleLeadFk.OrganizationId == input.OrganizationUnit);
            var jobnumberlist = new List<int?>();
            //jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

            //var jobstatuss = new List<int?>();
            //if (input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0)
            //{
            //    jobstatuss = _jobRepository.GetAll().Where(e => input.JobStatusIDFilter.Contains((int)e.JobStatusId)).Select(e => e.LeadId).ToList();
            //}

            var UserLists = new List<long?>();
            if (input.TeamId != 0 && input.TeamId != null)
            {
                UserLists = team_list.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }


            var readfilterlist = new List<int?>();
            if (input.ReadUnreadsms == 1)
            {
                var readLedActivityids = leadactive_list.Where(e => e.ActionId == 51 && e.IsMark == true).Select(e => e.ReferanceId).Distinct().ToList();
                readfilterlist = leadactive_list.Where(e => readLedActivityids.Contains(e.Id)).Select(e => e.PromotionUserId).Distinct().ToList();
            }
            var unreadfilterlist = new List<int?>();
            if (input.ReadUnreadsms == 2)
            {
                var unreadLedActivityids = leadactive_list.Where(e => e.ActionId == 51 && e.IsMark == false).Select(e => e.ReferanceId).Distinct().ToList();
                unreadfilterlist = leadactive_list.Where(e => unreadLedActivityids.Contains(e.Id)).Select(e => e.PromotionUserId).Distinct().ToList();
            }
            var filteredWholeSalePromotionUsers = _WholeSalePromotionUserRepository.GetAll()
                        .Include(e => e.WholeSalePromotionFk)
                        .Include(e => e.WholeSaleLeadFk)
                        .Include(e => e.WholeSalePromotionResponseStatusFk)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.ResponseMessage.Contains(input.Filter) || e.WholeSaleLeadFk.CompanyName.Contains(input.Filter) || e.WholeSaleLeadFk.Mobile.Contains(input.Filter) || e.WholeSaleLeadFk.Email.Contains(input.Filter) || e.WholeSaleLeadFk.Talephone.Contains(input.Filter))
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFk.Mobile.Contains(input.Filter))
                        .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFk.Talephone.Contains(input.Filter))
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFk.CompanyName.Contains(input.Filter))
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.WholeSaleLeadFk.Email.Contains(input.Filter))
                        .WhereIf(input.FilterName == "ResponseMessage" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ResponseMessage.Contains(input.Filter))
                        .WhereIf(input.ResponseDateFilter == "Responce" && SDate != null && EDate != null, e => e.ResponseDate.AddHours(10).Date >= SDate.Value.Date && e.ResponseDate.AddHours(10).Date <= EDate.Value.Date)
                        //.WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => jobstatuss.Contains(e.WholeSaleLeadId))
                        .WhereIf(input.WholeSalePromotionTitleFilter != 0, e => e.WholeSalePromotionId == input.WholeSalePromotionTitleFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCopanyNameFilter), e => e.WholeSaleLeadFk != null && e.WholeSaleLeadFk.CompanyName == input.LeadCopanyNameFilter)
                        .WhereIf(input.WholeSalePromotionResponseSatusIdFilter != null && input.WholeSalePromotionResponseSatusIdFilter != 0, e => e.WholeSalePromotionResponseStatusFk != null && e.WholeSalePromotionResponseStatusFk.Id == input.WholeSalePromotionResponseSatusIdFilter)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.WholeSaleLeadFk.AssignUserId == input.SalesRepId)
                        //.WhereIf(input.TeamId != null && input.TeamId != 0, e => UserLists.Contains(e.WholeSaleLeadFk.AssignUserId))
                        .WhereIf(input.OrganizationUnit != null, e => e.WholeSalePromotionFk.OrganizationID == input.OrganizationUnit)
                        .WhereIf(input.PromoType != 0, e => e.WholeSalePromotionFk.PromotionTypeId == input.PromoType)
                        .WhereIf(input.ReadUnreadsms == 1, e => readfilterlist.Contains(e.Id))
                        .WhereIf(input.ReadUnreadsms == 2, e => unreadfilterlist.Contains(e.Id))
                        .WhereIf(role.Contains("Admin"), e => e.WholeSaleLeadFk.AssignUserId != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.WholeSaleLeadFk.AssignUserId))
                        .WhereIf(role.Contains("Sales Rep"), e => e.WholeSaleLeadFk.AssignUserId == User.Id);
            //.Where(e => e.WholeSalePromotionResponseStatusId != null);


            //var JobStatus = _jobRepository.GetAll().Where(e => e.JobStatusId == 1).Select(e => e.LeadId).ToList();
            //var deposite = _invoicePaymentRepository.GetAll().Where(e => e.InvoicePaymentStatusId == 2).Select(e => e.JobFk.LeadId).Distinct().ToList();
            var WholeSalePromotionUsers = from o in filteredWholeSalePromotionUsers
                                          join o1 in _lookup_WholeSalePromotionRepository.GetAll() on o.WholeSalePromotionId equals o1.Id into j1
                                          from s1 in j1.DefaultIfEmpty()

                                          join o2 in _lookup_leadRepository.GetAll() on o.WholeSaleLeadId equals o2.Id into j2
                                          from s2 in j2.DefaultIfEmpty()

                                          join o3 in _lookup_WholeSalePromotionResponseStatusRepository.GetAll() on o.WholeSalePromotionResponseStatusId equals o3.Id into j3
                                          from s3 in j3.DefaultIfEmpty()

                                          join o4 in _userRepository.GetAll() on s2.AssignUserId.ToString() equals o4.Id.ToString() into j4
                                          from s4 in j4.DefaultIfEmpty()

                                          join o5 in _leadStatusRepository.GetAll() on s2.WholeSaleStatusId.ToString() equals o5.Id.ToString()

                                          //join o6 in _jobRepository.GetAll() on o.WholeSaleLeadId equals o6.LeadId into j6
                                          //from s6 in j6.DefaultIfEmpty()

                                          //join o7 in _lookup_jobStatusRepository.GetAll() on s6.JobStatusId equals o7.Id into j7
                                          //from s7 in j7.DefaultIfEmpty()


                                          //join o8 in _leadactivityRepository.GetAll() on o.Id equals o8.WholeSalePromotionUserId into j8
                                          //from s8 in j8.DefaultIfEmpty()
                                          //join o6 in _lookup_WholeSalePromotionTypeRepository.GetAll() on o.WholeSalePromotionTypeId equals o6.Id into j5
                                          //from s6 in j1.DefaultIfEmpty()

                                          select new GetWholeSalePromotionUserForViewDto()
                                          {
                                              WholeSalePromotionUser = new WholeSalePromotionUserDto
                                              {
                                                  ResponseDate = o.ResponseDate,
                                                  WholeSalePromotionResponseStatusId = o.WholeSalePromotionResponseStatusId,
                                                  ResponseMessage = o.ResponseMessage,
                                                  Id = o.Id

                                              },

                                              getWholeSalePromotionForViewDto = new GetWholeSalePromotionForViewDto
                                              {
                                                  WholeSalePromotion = new WholeSalePromotionDto
                                                  {
                                                      Id = s1.Id,
                                                      Title = s1.Title,
                                                      PromoCharge = s1.PromoCharge,
                                                      PromotionTypeId = s1.PromotionTypeId,
                                                      Description = s1.Description,
                                                      CreationTime = s1.CreationTime,
                                                  },
                                                  WholeSalePromotionTypeName = s1.PromotionTypeFk.Name

                                              },

                                              WholeSalePromotionTitle = s1 == null || s1.Title == null ? "" : s1.Title.ToString(),
                                              LeadCopanyName = s2 == null || s2.CompanyName == null ? "" : s2.CompanyName.ToString(),
                                              Mobile = s2 == null || s2.Mobile == null ? "" : s2.Mobile.ToString(),
                                              EMail = s2 == null || s2.Email == null ? "" : s2.Email.ToString(),
                                              AssignedToName = s4 == null || s2.AssignUserId == null ? "" : s4.Name,
                                              WholeSalePromotionResponseStatusName = s3 == null || s3.Name == null ? "" : s3.Name.ToString(),
                                              LeadStatus = o5.Name,
                                              //ProjectStatus = s7.Name,
                                              //ProjectNumber = s6.JobNumber,
                                              //Mark = leadactive_list.Where(e => e.ReferanceId == s8.Id).Select(e => e.IsMark).FirstOrDefault(),
                                              LeadId = s2.Id,
                                              ActivityReminderTime = leadactive_list.Where(e => e.SectionId == 34 && e.ActionId == 8 && e.WholeSaleLeadId == s2.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                                              ActivityDescription = leadactive_list.Where(e => e.SectionId == 34 && e.ActionId == 8 && e.WholeSaleLeadId == s2.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                              ActivityComment = leadactive_list.Where(e => e.SectionId == 34 && e.ActionId == 24 && e.WholeSaleLeadId == s2.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                              //TotalWholeSalePromotion = filteredWholeSalePromotionUsers.Select(e => e.Id).Count(),
                                              //InterestedCount = filteredWholeSalePromotionUsers.Where(e => e.WholeSalePromotionResponseStatusId == 1 && e.ResponseMessage != null).Select(e => e.Id).Count(),
                                              //NotInterestedCount = filteredWholeSalePromotionUsers.Where(e => e.WholeSalePromotionResponseStatusId == 2 && e.ResponseMessage != null).Select(e => e.Id).Count(),
                                              //OtherCount = filteredWholeSalePromotionUsers.Where(e => e.WholeSalePromotionResponseStatusId == 3 && e.ResponseMessage != null).Select(e => e.Id).Count(),
                                              //Unhandle = filteredWholeSalePromotionUsers.Where(e => e.LeadFk.LeadStatusId != 6 && e.ResponseMessage != null).Select(e => e.Id).Count(),
                                              //WholeSalePromotionSold = filteredWholeSalePromotionUsers.Where(e => deposite.Contains((int)e.LeadId) && e.ResponseMessage != null).Select(e => e.Id).Count(),
                                              //WholeSalePromotionProject = filteredWholeSalePromotionUsers.Where(e => JobStatus.Contains((int)e.LeadId) && e.ResponseMessage != null).Select(e => e.Id).Count()
                                          };
            var WholeSalePromotionUserListDtos = await WholeSalePromotionUsers.ToListAsync();
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _WholeSalePromotionUsersExcelExporter.ExportToFile(WholeSalePromotionUserListDtos, "WholeSalePromotionUsers.xlsx");
            }
            else
            {
                //return _WholeSalePromotionUsersExcelExporter.ExportCsvToFile(WholeSalePromotionUserListDtos);
                return _WholeSalePromotionUsersExcelExporter.ExportToFile(WholeSalePromotionUserListDtos, "WholeSalePromotionUsers.csv");
            }

        }

        public async Task MarkAsReadWholeSalePromotionSms(int id)
        {
            var activityid = _leadactivityRepository.GetAll().Where(e => e.PromotionUserId == id).Select(e => e.Id).FirstOrDefault();
            var referenceLeadid = _leadactivityRepository.GetAll().Where(e => e.ReferanceId == activityid).Select(e => e.Id).FirstOrDefault();
            var leadactivity = await _leadactivityRepository.FirstOrDefaultAsync((int)referenceLeadid);
            leadactivity.IsMark = true;
            await _leadactivityRepository.UpdateAsync(leadactivity);

        }

        public async Task UpdateResponce(int? LeaId, string msg)
        {
            var WholeSalePromotionresponce = _WholeSalePromotionUserRepository.GetAll().Where(e => e.WholeSaleLeadId == LeaId).Select(e => e.Id).ToList();
            if (WholeSalePromotionresponce.Count > 0)
            {
                foreach (var item in WholeSalePromotionresponce)
                {
                    var promo = await _WholeSalePromotionUserRepository.FirstOrDefaultAsync((int)item);
                    promo.WholeSalePromotionResponseStatusId = 2;
                    promo.ResponseMessage = msg;
                    //await _WholeSalePromotionUserRepository.UpdateAsync(promo);

                    WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
                    leadactivity.ActionId = 51;
                    leadactivity.SectionId = 34;
                    leadactivity.ActionNote = "WholeSalePromotion SMS Reply";
                    leadactivity.ActivityNote = msg;
                    leadactivity.WholeSaleLeadId = (int)LeaId;
                    leadactivity.PromotionUserId = promo.Id;
                    leadactivity.CreatorUserId = AbpSession.UserId;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    //await _leadactivityRepository.InsertAsync(leadactivity);
                }
            }
            else
            {
                WholeSalePromotionUser WholeSalePromotionUser = new WholeSalePromotionUser();
                WholeSalePromotionUser.CreatorUserId = AbpSession.UserId;
                WholeSalePromotionUser.CreationTime = DateTime.UtcNow;
                if (AbpSession.TenantId != null)
                {
                    WholeSalePromotionUser.TenantId = (int)AbpSession.TenantId;
                }
                WholeSalePromotionUser.ResponseDate = DateTime.UtcNow;
                WholeSalePromotionUser.ResponseMessage = msg;
                WholeSalePromotionUser.WholeSaleLeadId = LeaId;
                WholeSalePromotionUser.WholeSalePromotionId = _WholeSalePromotionUserRepository.GetAll().OrderByDescending(e => e.Id).Select(e => e.Id).FirstOrDefault();
                //var promoid = await _WholeSalePromotionUserRepository.InsertAndGetIdAsync(WholeSalePromotionUser);

                WholeSaleLeadActivityLog leadactivity = new WholeSaleLeadActivityLog();
                leadactivity.ActionId = 51;
                leadactivity.SectionId = 34;
                leadactivity.ActionNote = "WholeSalePromotion SMS Send";
                leadactivity.ActivityNote = msg;
                leadactivity.WholeSaleLeadId = (int)LeaId;
                //leadactivity.WholeSalePromotionUserId = promoid;
                leadactivity.CreatorUserId = AbpSession.UserId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                //await _leadactivityRepository.InsertAsync(leadactivity);
            }

        }

        public async Task MarkAsReadWholeSalePromotionSmsInBulk(List<int> ids, int Readorunreadtag)
        {
            foreach (var item in ids)
            {
                var activityid = _leadactivityRepository.GetAll().Where(e => e.PromotionUserId == item).Select(e => e.Id).FirstOrDefault();
                var referenceLeadid = _leadactivityRepository.GetAll().Where(e => e.ReferanceId == activityid).Select(e => e.Id).ToList();
                foreach (var i in referenceLeadid)
                {
                    var leadactivity = await _leadactivityRepository.FirstOrDefaultAsync((int)i);
                    if (Readorunreadtag == 1)
                    {
                        leadactivity.IsMark = true;
                    }
                    else
                    {
                        leadactivity.IsMark = false;
                    }
                    await _leadactivityRepository.UpdateAsync(leadactivity);
                }
            }
        }

        //[AbpAuthorize(AppPermissions.Pages_WholeSalePromotionUsers)]
        public async Task<List<WholeSalePromotionUserWholeSalePromotionLookupTableDto>> GetAllWholeSalePromotionForTableDropdown()
        {
            return await _lookup_WholeSalePromotionRepository.GetAll()
                .Select(WholeSalePromotion => new WholeSalePromotionUserWholeSalePromotionLookupTableDto
                {
                    Id = WholeSalePromotion.Id,
                    DisplayName = WholeSalePromotion == null || WholeSalePromotion.Title == null ? "" : WholeSalePromotion.Title.ToString()
                }).ToListAsync();
        }

        //[AbpAuthorize(AppPermissions.Pages_WholeSalePromotionUsers)]
        public async Task<List<WholeSalePromotionUserLeadLookupTableDto>> GetAllWholeSaleLeadForTableDropdown()
        {
            return await _lookup_leadRepository.GetAll()
                .Select(lead => new WholeSalePromotionUserLeadLookupTableDto
                {
                    Id = lead.Id,
                    DisplayName = lead == null || lead.CompanyName == null ? "" : lead.CompanyName.ToString()
                }).ToListAsync();
        }

        //[AbpAuthorize(AppPermissions.Pages_WholeSalePromotionUsers)]
        public async Task<List<WholeSalePromotionUserWholeSalePromotionResponseStatusLookupTableDto>> GetAllWholeSalePromotionResponseStatusForTableDropdown()
        {
            return await _lookup_WholeSalePromotionResponseStatusRepository.GetAll()
                .Select(WholeSalePromotionResponseStatus => new WholeSalePromotionUserWholeSalePromotionResponseStatusLookupTableDto
                {
                    Id = WholeSalePromotionResponseStatus.Id,
                    DisplayName = WholeSalePromotionResponseStatus == null || WholeSalePromotionResponseStatus.Name == null ? "" : WholeSalePromotionResponseStatus.Name.ToString()
                }).ToListAsync();
        }

    }
}