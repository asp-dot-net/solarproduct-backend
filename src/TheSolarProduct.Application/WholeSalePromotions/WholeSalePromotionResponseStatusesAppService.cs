﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.WholeSalePromotions.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace TheSolarProduct.WholeSalePromotions
{
    //[AbpAuthorize(AppPermissions.Pages_WholeSalePromotionResponseStatuses)]
    [AbpAuthorize]
    public class WholeSalePromotionResponseStatusesAppService : TheSolarProductAppServiceBase, IWholeSalePromotionResponseStatusesAppService
    {
        private readonly IRepository<WholeSalePromotionResponseStatus> _WholeSalePromotionResponseStatusRepository;


        public WholeSalePromotionResponseStatusesAppService(IRepository<WholeSalePromotionResponseStatus> WholeSalePromotionResponseStatusRepository)
        {
            _WholeSalePromotionResponseStatusRepository = WholeSalePromotionResponseStatusRepository;

        }

        public async Task<PagedResultDto<GetWholeSalePromotionResponseStatusForViewDto>> GetAll(GetAllWholeSalePromotionResponseStatusesInput input)
        {

            var filteredWholeSalePromotionResponseStatuses = _WholeSalePromotionResponseStatusRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

            var pagedAndFilteredWholeSalePromotionResponseStatuses = filteredWholeSalePromotionResponseStatuses
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var WholeSalePromotionResponseStatuses = from o in pagedAndFilteredWholeSalePromotionResponseStatuses
                                            select new GetWholeSalePromotionResponseStatusForViewDto()
                                            {
                                                WholeSalePromotionResponseStatus = new WholeSalePromotionResponseStatusDto
                                                {
                                                    Name = o.Name,
                                                    Id = o.Id
                                                }
                                            };

            var totalCount = await filteredWholeSalePromotionResponseStatuses.CountAsync();

            return new PagedResultDto<GetWholeSalePromotionResponseStatusForViewDto>(
                totalCount,
                await WholeSalePromotionResponseStatuses.ToListAsync()
            );
        }

        public async Task<GetWholeSalePromotionResponseStatusForViewDto> GetWholeSalePromotionResponseStatusForView(int id)
        {
            var WholeSalePromotionResponseStatus = await _WholeSalePromotionResponseStatusRepository.GetAsync(id);

            var output = new GetWholeSalePromotionResponseStatusForViewDto { WholeSalePromotionResponseStatus = ObjectMapper.Map<WholeSalePromotionResponseStatusDto>(WholeSalePromotionResponseStatus) };

            return output;
        }

        /// [AbpAuthorize(AppPermissions.Pages_WholeSalePromotionResponseStatuses_Edit)]
        public async Task<GetWholeSalePromotionResponseStatusForEditOutput> GetWholeSalePromotionResponseStatusForEdit(EntityDto input)
        {
            var WholeSalePromotionResponseStatus = await _WholeSalePromotionResponseStatusRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetWholeSalePromotionResponseStatusForEditOutput { WholeSalePromotionResponseStatus = ObjectMapper.Map<CreateOrEditWholeSalePromotionResponseStatusDto>(WholeSalePromotionResponseStatus) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditWholeSalePromotionResponseStatusDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        // [AbpAuthorize(AppPermissions.Pages_WholeSalePromotionResponseStatuses_Create)]
        protected virtual async Task Create(CreateOrEditWholeSalePromotionResponseStatusDto input)
        {
            var WholeSalePromotionResponseStatus = ObjectMapper.Map<WholeSalePromotionResponseStatus>(input);


            //if (AbpSession.TenantId != null)
            //{
            //	WholeSalePromotionResponseStatus.TenantId = (int) AbpSession.TenantId;
            //}


            await _WholeSalePromotionResponseStatusRepository.InsertAsync(WholeSalePromotionResponseStatus);
        }

        //[AbpAuthorize(AppPermissions.Pages_WholeSalePromotionResponseStatuses_Edit)]
        protected virtual async Task Update(CreateOrEditWholeSalePromotionResponseStatusDto input)
        {
            var WholeSalePromotionResponseStatus = await _WholeSalePromotionResponseStatusRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, WholeSalePromotionResponseStatus);
        }

        //[AbpAuthorize(AppPermissions.Pages_WholeSalePromotionResponseStatuses_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _WholeSalePromotionResponseStatusRepository.DeleteAsync(input.Id);
        }
    }
}