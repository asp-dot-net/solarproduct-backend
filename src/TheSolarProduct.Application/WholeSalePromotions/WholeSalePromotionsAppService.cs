﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.WholeSalePromotions.Exporting;
using TheSolarProduct.WholeSalePromotions.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization.Users;
using TheSolarProduct.Authorization.Users;
using System.Net;
using TheSolarProduct.WholeSaleLeadActivityLogs;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.Leads;
using TheSolarProduct.ApplicationSettings;
using System.Net.Mail;
using Abp.Net.Mail;
using Abp.Runtime.Security;
using Abp.Domain.Uow;
using TheSolarProduct.EmailTemplates;
using System.Web;
using TheSolarProduct.EntityFrameworkCore;
using Abp.BackgroundJobs;
using Abp.Timing.Timezone;
using Abp.EntityFrameworkCore;
using TheSolarProduct.Notifications;
using Abp.Notifications;
using TheSolarProduct.Invoices;
using TheSolarProduct.Jobs;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.TheSolarDemo;
using Abp.Organizations;
using TheSolarProduct.Promotions;
using TheSolarProduct.WholeSaleLeadHistory;
using TheSolarProduct.LeadActions;
using TheSolarProduct.LeadStatuses;
using TheSolarProduct.WholeSaleLeads;
using TheSolarProduct.PromotionHistorys;
using Abp.Authorization;

namespace TheSolarProduct.WholeSalePromotions
{
    //[AbpAuthorize(AppPermissions.Pages_WholeSalePromotions)]
    [AbpAuthorize]
    public class WholeSalePromotionsAppService : TheSolarProductAppServiceBase, IWholeSalePromotionsAppService
    {
        private readonly IRepository<WholeSalePromotion> _WholeSalePromotionRepository;
        private readonly IWholeSalePromotionsExcelExporter _WholeSalePromotionsExcelExporter;
        private readonly IRepository<PromotionType, int> _lookup_WholeSalePromotionTypeRepository;
        private readonly IRepository<WholeSalePromotionUser> _WholeSalePromotionUsersRepository;
        private readonly IRepository<WholeSalePromotionResponseStatus> _WholeSalePromotionResponseStatusRepository;
        private readonly IRepository<User, long> _lookup_userRepository;
        private readonly IRepository<WholeSaleLeadActivityLog> _leadactivityRepository;
        private readonly IRepository<Lead> _leadRepository;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<WholeSalePromotionUser> _WholeSalePromotionUserRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<EmailTemplate> _emailTemplateRepository;
        protected readonly IBackgroundJobManager BackgroundJobManager;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private TheSolarProductDbContext _database;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<User, long> _userRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<WholeSaleLeadActivityLog> _wholesaleLeadActivityLogRepository;
        private readonly IRepository<PromotionHistory> _promotionHistoryRepository;
        private readonly IRepository<WholeSaleLead> _wholeSaleLeadRepository;

        public object URLEncoder { get; private set; }

        public WholeSalePromotionsAppService(
            IRepository<WholeSalePromotion> WholeSalePromotionRepository
            , IRepository<WholeSaleLeadActivityLog> leadactivityRepository
            , IWholeSalePromotionsExcelExporter WholeSalePromotionsExcelExporter
            , IRepository<PromotionType, int> lookup_WholeSalePromotionTypeRepository
            , IRepository<WholeSalePromotionResponseStatus> WholeSalePromotionResponseStatusRepository
            , IRepository<WholeSalePromotionUser> WholeSalePromotionUsersRepository
            , IRepository<User, long> lookup_userRepository
            , IApplicationSettingsAppService applicationSettings
            , IRepository<Lead> leadRepository
            , IEmailSender emailSender
            , IRepository<WholeSalePromotionUser> WholeSalePromotionUserRepository
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<EmailTemplate> emailTemplateRepository
            , IBackgroundJobManager backgroundJobManager
            , ITimeZoneConverter timeZoneConverter
            , IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            , IRepository<User, long> userRepository
            , IAppNotifier appNotifier 
            , IRepository<InvoicePayment> invoicePaymentRepository
            , IRepository<Job> jobRepository 
            , IRepository<UserRole, long> userRoleRepository
            , IRepository<Role> roleRepository
            , IRepository<UserTeam> userTeamRepository
            , IRepository<OrganizationUnit, long> organizationUnitRepository
            , IRepository<WholeSaleLeadActivityLog> wholesaleLeadActivityLogRepository
            , IRepository<PromotionHistory> promotionHistoryRepository
            , IRepository<WholeSaleLead> wholeSaleLeadRepository
            )
        {
            _WholeSalePromotionRepository = WholeSalePromotionRepository;
            _WholeSalePromotionsExcelExporter = WholeSalePromotionsExcelExporter;
            _lookup_WholeSalePromotionTypeRepository = lookup_WholeSalePromotionTypeRepository;
            _WholeSalePromotionResponseStatusRepository = WholeSalePromotionResponseStatusRepository;
            _WholeSalePromotionUsersRepository = WholeSalePromotionUsersRepository;
            _lookup_userRepository = lookup_userRepository;
            _leadactivityRepository = leadactivityRepository;
            _leadRepository = leadRepository;
            _applicationSettings = applicationSettings;
            _emailSender = emailSender;
            _WholeSalePromotionUserRepository = WholeSalePromotionUserRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _emailTemplateRepository = emailTemplateRepository;
            BackgroundJobManager = backgroundJobManager;
            _timeZoneConverter = timeZoneConverter;
            _dbcontextprovider = dbcontextprovider;
            _userRepository = userRepository;
            _appNotifier = appNotifier;
            _invoicePaymentRepository = invoicePaymentRepository;
            _jobRepository = jobRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _userTeamRepository = userTeamRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _wholesaleLeadActivityLogRepository = wholesaleLeadActivityLogRepository;
            _promotionHistoryRepository = promotionHistoryRepository;
            _wholeSaleLeadRepository = wholeSaleLeadRepository;
        }

        /// <summary>
        /// Get WholeSalePromotion List
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetWholeSalePromotionForViewDto>> GetAll(GetAllWholeSalePromotionsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.FromDateFilter, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.ToDateFilter, (int)AbpSession.TenantId));

            var filteredWholeSalePromotions = _WholeSalePromotionRepository.GetAll()
                .Include(e => e.PromotionTypeFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter), e => e.Title == input.TitleFilter)
                        .WhereIf(input.FromDateFilter != null && input.ToDateFilter != null,
                                    e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date &&
                                    e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.WholeSalePromotionTitleFilter != 0, e => e.Id == input.WholeSalePromotionTitleFilter)
                        .WhereIf(input.MinAmountFilter != null && input.MaxAmountFilter != null,
                                    e => e.PromoCharge >= input.MinAmountFilter &&
                                    e.PromoCharge <= input.MaxAmountFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.Contains(input.DescriptionFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.WholeSalePromotionTypeNameFilter), e => e.PromotionTypeFk != null && e.PromotionTypeFk.Name == input.WholeSalePromotionTypeNameFilter)
                        .WhereIf(input.PromoType != 0, e => e.PromotionTypeId == input.PromoType)
                        .WhereIf(input.OrganizationUnit != null && input.OrganizationUnit != 0, e => e.OrganizationID == input.OrganizationUnit);

            if (filteredWholeSalePromotions != null && filteredWholeSalePromotions.Count() > 0)
            {
                var JobStatus = _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.JobStatusId == 1 && e.LeadFk.OrganizationId == input.OrganizationUnit).Select(e => e.LeadId).ToList();
                var deposite = _invoicePaymentRepository.GetAll().Where(e => e.InvoicePaymentStatusId == 2).Select(e => e.JobFk.LeadId).Distinct().ToList();
                var WholeSalePromotionids = filteredWholeSalePromotions.Select(e => e.Id).Distinct().ToList();
                var WholeSalePromotionuser_list = _WholeSalePromotionUsersRepository.GetAll().Include(e => e.WholeSaleLeadFk).Where(e => e.WholeSaleLeadFk.OrganizationId == input.OrganizationUnit);

                var pagedAndFilteredWholeSalePromotions = filteredWholeSalePromotions
                    .OrderBy(input.Sorting ?? "Id desc")
                    .PageBy(input);

                var tot = (from pu in WholeSalePromotionuser_list
                           join promo in filteredWholeSalePromotions on pu.WholeSalePromotionId equals promo.Id
                           where pu.WholeSaleLeadFk.IsDeleted == false
                           select pu.Id).Count();

                var totPromo = (from pu in WholeSalePromotionuser_list
                                join promo in filteredWholeSalePromotions on pu.WholeSalePromotionId equals promo.Id
                                where pu.WholeSaleLeadFk.IsDeleted == false && pu.ResponseMessage != null
                                select pu.Id).Count();

                var interestedPromo = (from pu in WholeSalePromotionuser_list
                                       join promo in filteredWholeSalePromotions on pu.WholeSalePromotionId equals promo.Id
                                       where pu.WholeSaleLeadFk.IsDeleted == false && pu.WholeSaleLeadFk.IsDeleted == false && pu.ResponseMessage != null && pu.WholeSalePromotionResponseStatusId == 1
                                       select pu.Id).Count();

                var notInterestedPromo = (from pu in WholeSalePromotionuser_list
                                          join promo in filteredWholeSalePromotions on pu.WholeSalePromotionId equals promo.Id
                                          where pu.WholeSaleLeadFk.IsDeleted == false && pu.ResponseMessage != null && pu.WholeSalePromotionResponseStatusId == 2
                                          select pu.Id).Count();

                var otherPromo = (from pu in WholeSalePromotionuser_list
                                  join promo in filteredWholeSalePromotions on pu.WholeSalePromotionId equals promo.Id
                                  where pu.WholeSaleLeadFk.IsDeleted == false && pu.ResponseMessage != null && pu.WholeSalePromotionResponseStatusId == 3
                                  select pu.Id).Count();

                var WholeSalePromotions = from o in pagedAndFilteredWholeSalePromotions
                                     //join o1 in _lookup_WholeSalePromotionTypeRepository.GetAll() on o.WholeSalePromotionTypeId equals o1.Id into j1
                                     //from s1 in j1.DefaultIfEmpty()
                                 join o2 in _lookup_userRepository.GetAll() on o.CreatorUserId equals o2.Id
                                 select new GetWholeSalePromotionForViewDto()
                                 {
                                     WholeSalePromotion = new WholeSalePromotionDto
                                     {
                                         Title = o.Title,
                                         PromoCharge = o.PromoCharge,
                                         Description = o.Description,
                                         CreationTime = o.CreationTime,
                                         OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == o.OrganizationID).Select(e => e.DisplayName).FirstOrDefault(),
                                         Id = o.Id
                                     },
                                     InterestedCount = WholeSalePromotionuser_list.Where(s => s.WholeSalePromotionId == o.Id && s.WholeSalePromotionResponseStatusId == 1).Count(),
                                     NotInterestedCount = WholeSalePromotionuser_list.Where(s => s.WholeSalePromotionId == o.Id && s.WholeSalePromotionResponseStatusId == 2).Count(),
                                     OtherCount = WholeSalePromotionuser_list.Where(s => s.WholeSalePromotionId == o.Id && s.WholeSalePromotionResponseStatusId == 3).Count(),
                                     NoReply = WholeSalePromotionuser_list.Where(s => s.WholeSalePromotionId == o.Id && s.ResponseMessage == null).Count(),
                                     //TotalLeads = _WholeSalePromotionUsersRepository.GetAll().Where(s => s.WholeSalePromotionId == o.Id).Count(),
                                     TotalLeads = o.LeadCount,
                                     //WholeSalePromotionTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                                     WholeSalePromotionTypeName = o.PromotionTypeFk.Name,
                                     CreatorUserName = o2.UserName,

                                     TotalSendWholeSalePromotion = tot,
                                     TotalWholeSalePromotion = totPromo,
                                     summaryInterestedCount = interestedPromo,
                                     summaryNotInterestedCount = notInterestedPromo,
                                     summaryOtherCount = otherPromo,
                                     //TotalSendWholeSalePromotion = tot,
                                     //TotalWholeSalePromotion = WholeSalePromotionuser_list.Where(e => WholeSalePromotionids.Contains((int)e.WholeSalePromotionId) && e.ResponseMessage != null).Select(e => e.Id).Count(),
                                     //                           summaryInterestedCount = WholeSalePromotionuser_list.Where(e => WholeSalePromotionids.Contains((int)e.WholeSalePromotionId) && e.ResponseMessage != null && e.WholeSalePromotionResponseStatusId == 1).Select(e => e.Id).Count(),
                                     //                           summaryNotInterestedCount = WholeSalePromotionuser_list.Where(e => WholeSalePromotionids.Contains((int)e.WholeSalePromotionId) && e.ResponseMessage != null && e.WholeSalePromotionResponseStatusId == 2).Select(e => e.Id).Count(),
                                     //                           summaryOtherCount = WholeSalePromotionuser_list.Where(e => WholeSalePromotionids.Contains((int)e.WholeSalePromotionId) && e.ResponseMessage != null  && e.WholeSalePromotionResponseStatusId == 3).Select(e => e.Id).Count(),
                                     //                           Unhandle = WholeSalePromotionuser_list.Where(e => WholeSalePromotionids.Contains((int)e.WholeSalePromotionId) && e.ResponseMessage != null && e.LeadFk.LeadStatusId != 6).Select(e => e.LeadId).Distinct().Count(),
                                     //WholeSalePromotionSold = WholeSalePromotionuser_list.Where(e => WholeSalePromotionids.Contains((int)e.WholeSalePromotionId) && e.ResponseMessage != null && deposite.Contains((int)e.LeadId)).Select(e => e.LeadId).Distinct().Count(),
                                     //WholeSalePromotionProject = WholeSalePromotionuser_list.Where(e => WholeSalePromotionids.Contains((int)e.WholeSalePromotionId) && e.ResponseMessage != null  && JobStatus.Contains((int)e.LeadId)).Select(e => e.LeadId).Distinct().Count(),

                                 };

                var totalCount = await filteredWholeSalePromotions.CountAsync();

                return new PagedResultDto<GetWholeSalePromotionForViewDto>(
                    totalCount,
                    await WholeSalePromotions.ToListAsync()
                );
            }
            else
            {
                return new PagedResultDto<GetWholeSalePromotionForViewDto>(0, null);
            }
        }

        /// <summary>
        /// WholeSalePromotion Detail View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<GetWholeSalePromotionForViewDto> GetWholeSalePromotionForView(int id)
        {
            var WholeSalePromotion = await _WholeSalePromotionRepository.GetAsync(id);

            var output = new GetWholeSalePromotionForViewDto { WholeSalePromotion = ObjectMapper.Map<WholeSalePromotionDto>(WholeSalePromotion) };

            if (output.WholeSalePromotion.PromotionTypeId != null)
            {
                var _lookupWholeSalePromotionType = await _lookup_WholeSalePromotionTypeRepository.FirstOrDefaultAsync((int)output.WholeSalePromotion.PromotionTypeId);
                output.WholeSalePromotionTypeName = _lookupWholeSalePromotionType?.Name?.ToString();
            }

            output.WholeSalePromotion.Description = WebUtility.HtmlDecode(output.WholeSalePromotion.Description);
            return output;
        }

        /// <summary>
        /// Get WholeSalePromotion Data For Edit ==> Not in Use
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        ///[AbpAuthorize(AppPermissions.Pages_WholeSalePromotions_Edit)]
        public async Task<GetWholeSalePromotionForEditOutput> GetWholeSalePromotionForEdit(EntityDto input)
        {
            var WholeSalePromotion = await _WholeSalePromotionRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetWholeSalePromotionForEditOutput { WholeSalePromotion = ObjectMapper.Map<CreateOrEditWholeSalePromotionDto>(WholeSalePromotion) };

            if (output.WholeSalePromotion.WholeSalePromotionTypeId != null)
            {
                var _lookupWholeSalePromotionType = await _lookup_WholeSalePromotionTypeRepository.FirstOrDefaultAsync((int)output.WholeSalePromotion.WholeSalePromotionTypeId);
                output.WholeSalePromotionTypeName = _lookupWholeSalePromotionType?.Name?.ToString();
            }

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditWholeSalePromotionDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
        }

        /// <summary>
        /// Create WholeSalePromotion
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        //[AbpAuthorize(AppPermissions.Pages_WholeSalePromotions_Create)]
        protected virtual async Task Create(CreateOrEditWholeSalePromotionDto input)
        {
            input.StartDateFilter = (_timeZoneConverter.Convert(input.StartDateFilter, (int)AbpSession.TenantId));
            input.EndDateFilter = (_timeZoneConverter.Convert(input.EndDateFilter, (int)AbpSession.TenantId));
            //SaveOrTest == 1 To Send WholeSalePromotion To All Leads
            //SaveOrTest == 2 To Send WholeSalePromotion To Only Test Users(Data Given in MobileNosList & Emails)

            if (input.SaveOrTest == 1)
            {
                //Send WholeSalePromotion to all Leads
                var WholeSalePromotion = ObjectMapper.Map<WholeSalePromotion>(input);
                WholeSalePromotion.PromotionTypeId = input.WholeSalePromotionTypeId;
                if (AbpSession.TenantId != null)
                {
                    WholeSalePromotion.TenantId = (int)AbpSession.TenantId;
                }

                string LeadStatusIdsFilter = "";
                string LeadSourceIdsFilter = "";
                string StateIdsFilter = "";
                string TeamIdsFilter = "";
                string JobStatusIdsFilter = "";

                if (input.LeadStatusIdsFilter != null)
                {
                    foreach (var item in input.LeadStatusIdsFilter)
                    {
                        LeadStatusIdsFilter = LeadStatusIdsFilter + item;
                    }
                }
                if (input.LeadSourceIdsFilter != null)
                {
                    foreach (var item in input.LeadSourceIdsFilter)
                    {
                        LeadSourceIdsFilter = LeadSourceIdsFilter + item;
                    }
                }
                if (input.StateIdsFilter != null)
                {
                    foreach (var item in input.StateIdsFilter)
                    {
                        StateIdsFilter = StateIdsFilter + item;
                    }
                }
                if (input.TeamIdsFilter != null)
                {
                    foreach (var item in input.TeamIdsFilter)
                    {
                        TeamIdsFilter = TeamIdsFilter + item;
                    }
                }
                if (input.JobStatusIdsFilter != null)
                {
                    foreach (var item in input.JobStatusIdsFilter)
                    {
                        JobStatusIdsFilter = JobStatusIdsFilter + item;
                    }
                }
                WholeSalePromotion.LeadStatusIdsFilter = LeadStatusIdsFilter;
                WholeSalePromotion.LeadSourceIdsFilter = LeadSourceIdsFilter;
                WholeSalePromotion.StateIdsFilter = StateIdsFilter;
                WholeSalePromotion.TeamIdsFilter = TeamIdsFilter;
                WholeSalePromotion.JobStatusIdsFilter = JobStatusIdsFilter;

                //Insert Data in WholeSalePromotion Table With WholeSalePromotion Details & Lead Count
                int newWholeSalePromotionId = _WholeSalePromotionRepository.InsertAndGetId(WholeSalePromotion);


                var promotionHistory = new PromotionHistory();
                if (AbpSession.TenantId != null)
                {
                    promotionHistory.TenantId = (int)AbpSession.TenantId;
                }
                promotionHistory.LeadIds = input.SelectedLeadIdsForWholeSalePromotion;
                promotionHistory.PromotionType = "WholeSale";
                promotionHistory.PromotionId = newWholeSalePromotionId;
                promotionHistory.PromotionSendingId = (int)input.WholeSalePromotionTypeId;
                promotionHistory.OrganizationId = input.OrganizationID;
                promotionHistory.Title = input.Title;
                promotionHistory.Description = input.Description;
                promotionHistory.LeadStatuses = input.LeadStatuses;
                promotionHistory.States = input.States;
                promotionHistory.StartDate = input.StartDateFilter;
                promotionHistory.EndDate = input.EndDateFilter;
                promotionHistory.Charges = input.PromoCharge;
                promotionHistory.TotalCharCount = input.totalCharCount;
                promotionHistory.TotalCredit = input.totalCredit;
               
                await _promotionHistoryRepository.InsertAsync(promotionHistory);


                string[] arrSelectedLeadIds = input.SelectedLeadIdsForWholeSalePromotion.Split(',').Distinct().ToArray();

                var List = new List<WholeSalePromotionUser>();

                var ActivityList = new List<WholeSaleLeadActivityLog>();

                var ActivityHistoryList = new List<WholeSaleLeadtrackerHistory>();

                var sendBulkSMSInput = new SendBulkSMSInput();

                var WholeSalePromotionSMSEmailList = new List<WholeSalePromotionSMSEmailList>();

                List<SendSMSInput> msg = new List<SendSMSInput>();

                if (!string.IsNullOrEmpty(Convert.ToString(newWholeSalePromotionId)))
                {
                    //List for WholeSalePromotion User Table for all leads
                    foreach (string leadId in arrSelectedLeadIds)
                    {
                        if (!string.IsNullOrEmpty(leadId))
                        {
                            int LeadId = Convert.ToInt32(leadId);

                            var ListItem = new WholeSalePromotionUser();
                            ListItem.CreationTime = WholeSalePromotion.CreationTime;
                            ListItem.CreatorUserId = WholeSalePromotion.CreatorUserId;
                            ListItem.IsDeleted = false;
                            ListItem.WholeSalePromotionId = newWholeSalePromotionId;
                            ListItem.WholeSaleLeadId = LeadId;
                            ListItem.CreatorUserId = WholeSalePromotion.CreatorUserId;
                            ListItem.TenantId = WholeSalePromotion.TenantId;
                            List.Add(ListItem);


                        }
                    }

                    //Bulk Insert Data in WholeSalePromotion User Table
                    await _dbcontextprovider.GetDbContext().WholeSalePromotionUsers.AddRangeAsync(List);
                    await _dbcontextprovider.GetDbContext().SaveChangesAsync();
                }

                if (!string.IsNullOrEmpty(Convert.ToString(newWholeSalePromotionId)))
                {
                    var WholeSalePromotionUserList = await _WholeSalePromotionUserRepository.GetAll().Where(e => e.WholeSalePromotionId == WholeSalePromotion.Id).Select(e => new { e.Id, e.WholeSaleLeadId }).ToListAsync();
                    //var actionNote = "WholeSalePromotion " + input.Description + " has been sent.";
                    var actionNote = "WholeSalePromotion has been sent.";
                    //List for Lead Activity Log Table for all leads
                    foreach (var pu in WholeSalePromotionUserList)
                    {
                        if (pu.WholeSaleLeadId > 0)
                        {
                            int LeadId = Convert.ToInt32(pu.WholeSaleLeadId);

                            var ActivityListItem = new WholeSaleLeadActivityLog();
                            ActivityListItem.ActionId = input.WholeSalePromotionTypeId == 1 ? 52 : 63;
                            ActivityListItem.ActionNote = actionNote;
                            ActivityListItem.WholeSaleLeadId = LeadId;
                            ActivityListItem.Body = input.Description;
                            ActivityListItem.TenantId = WholeSalePromotion.TenantId;
                            ActivityListItem.CreatorUserId = WholeSalePromotion.CreatorUserId;
                            ActivityListItem.CreationTime = WholeSalePromotion.CreationTime;
                            ActivityListItem.IsDeleted = false;
                            ActivityListItem.PromotionId = newWholeSalePromotionId;
                            ActivityListItem.PromotionUserId = pu.Id;
                            ActivityListItem.SectionId = 36;
                            ActivityList.Add(ActivityListItem);
                            //var wholesaleId = await  _wholesaleLeadActivityLogRepository.InsertAndGetIdAsync(ActivityListItem);

                            //if(input.WholeSalePromotionTypeId == 1)
                            //{
                            //    var history = new WholeSaleLeadtrackerHistory();
                            //    if (AbpSession.TenantId != null)
                            //    {
                            //        history.TenantId = (int)AbpSession.TenantId;
                            //    }
                            //    history.FieldName = "Total Char Count";
                            //    history.PrevValue = "";
                            //    history.CurValue = input.totalCharCount.ToString();
                            //    history.Action = "Edit";
                            //    history.LastmodifiedDateTime = DateTime.Now;
                            //    history.WholeSaleLeadId = LeadId;
                            //    history.WholeSaleLeadActivityId = wholesaleId;
                            //    ActivityHistoryList.Add(history);

                            //    history = new WholeSaleLeadtrackerHistory();
                            //    if (AbpSession.TenantId != null)
                            //    {
                            //        history.TenantId = (int)AbpSession.TenantId;
                            //    }
                            //    history.FieldName = "Total Credit";
                            //    history.PrevValue = "";
                            //    history.CurValue = input.totalCredit.ToString();
                            //    history.Action = "Edit";
                            //    history.LastmodifiedDateTime = DateTime.Now;
                            //    history.WholeSaleLeadId = LeadId;
                            //    history.WholeSaleLeadActivityId = wholesaleId;
                            //    ActivityHistoryList.Add(history);

                            //    history = new WholeSaleLeadtrackerHistory();
                            //    if (AbpSession.TenantId != null)
                            //    {
                            //        history.TenantId = (int)AbpSession.TenantId;
                            //    }
                            //    history.FieldName = "Charge";
                            //    history.PrevValue = "";
                            //    history.CurValue = input.PromoCharge.ToString();
                            //    history.Action = "Edit";
                            //    history.LastmodifiedDateTime = DateTime.Now;
                            //    history.WholeSaleLeadId = LeadId;
                            //    history.WholeSaleLeadActivityId = wholesaleId;
                            //    ActivityHistoryList.Add(history);
                            //}
                            

                        }
                    }

                    //Bulk Insert Data in Lead Activity Log Table
                    await _dbcontextprovider.GetDbContext().WholeSaleLeadActivityLogs.AddRangeAsync(ActivityList);
                    await _dbcontextprovider.GetDbContext().SaveChangesAsync();
                    //if (ActivityHistoryList.Count > 0)
                    //{
                    //    await _dbcontextprovider.GetDbContext().WholeSaleLeadtrackerHistorys.AddRangeAsync(ActivityHistoryList);
                    //    await _dbcontextprovider.GetDbContext().SaveChangesAsync();
                    //}
                }

                if (!string.IsNullOrEmpty(Convert.ToString(newWholeSalePromotionId)))
                {
                    var ActivityLogList = await _leadactivityRepository.GetAll().Where(e => e.PromotionId == WholeSalePromotion.Id).Select(e => new { e.Id, e.WholeSaleLeadId, e.PromotionUserId }).ToListAsync();

                    //List for Lead Activity Log Table for all leads
                    foreach (var al in ActivityLogList)
                    {
                        if (al.WholeSaleLeadId > 0)
                        {
                            int LeadId = Convert.ToInt32(al.WholeSaleLeadId);
                            var WholeSalePromotionSMSEmailListItem = new WholeSalePromotionSMSEmailList();
                            var LeadDetail = _leadRepository.GetAll().Where(e => e.Id == LeadId).Select(e => new { e.Mobile, e.Email }).FirstOrDefault();

                            //Send WholeSalePromotion to Only Test Users(Data Given in MobileNosList & Emails)
                            if (input.WholeSalePromotionTypeId == 1)
                            {
                                if (!string.IsNullOrEmpty(LeadDetail.Mobile))
                                {
                                    var sendSMSInput = new SendSMSInput();
                                    sendSMSInput.PhoneNumber = LeadDetail.Mobile;
                                    sendSMSInput.Text = input.Description;
                                    sendSMSInput.promoresponseID = (int)al.PromotionUserId;
                                    sendSMSInput.oId = input.OrganizationID;
                                    sendSMSInput.SectionID = 36;
                                    sendSMSInput.ActivityId = 52;
                                    msg.Add(sendSMSInput);
                                }

                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(LeadDetail.Email))
                                {
                                    var KeyValue = Convert.ToString(AbpSession.TenantId) + "," + (int)al.PromotionUserId + "," + 36;


                                    //Token With Tenant Id & WholeSalePromotion User Primary Id for subscribe & UnSubscribe
                                    var token = HttpUtility.UrlEncode(SimpleStringCipher.Instance.Encrypt(KeyValue, AppConsts.DefaultPassPhrase));

                                    //Subscription Link
                                    string SubscribeLink = "https://thesolarproduct.com/account/wholecustomer-subscribe?STR=" + token;
                                    //string SubscribeLink = "http://localhost:4200/account/wholecustomer-subscribe?STR=" + token;

                                    //UnSubscription Link
                                    string UnSubscribeLink = "https://thesolarproduct.com/account/wholecustomer-unsubscribe?STR=" + token;

                                    //string UnSubscribeLink = "http://localhost:4200/account/wholecustomer-unsubscribe?STR=" + token;

                                    //Footer Design for subscribe & UnSubscribe
                                    //var footer = "<div style=\"border:1px solid #ccc;padding:10px; background:#f2f2f2;\">If you are intrested in this offer, please click <a href=\"" + SubscribeLink + "\">" +
                                    //    "<img height=\"20px\" src=\"https://thesolarproduct.com/assets/common/images/like.png\"></a>.<br/>" +
                                    //    "If you do not wish to receive any further communications, please click <a href=\"" + UnSubscribeLink + "\">" +
                                    //    "<img height=\"20px\" src=\"https://thesolarproduct.com/assets/common/images/dislike.png\"></a>.</div>";
                                    //var footer = "<div style=\"border:1px solid #ccc;padding:10px; background:#f2f2f2;\">If you are intrested in this offer, please click <a href=\"" + SubscribeLink + "\">" +
                                    //   "<img height=\"20px\" src=\"http://localhost:4200/assets/common/images/like.png\"></a>.<br/>" +
                                    //   "If you do not wish to receive any further communications, please click <a href=\"" + UnSubscribeLink + "\">" +
                                    //   "<img height=\"20px\" src=\"http://localhost:4200/assets/common/images/dislike.png\"></a>.</div>";
                                    
                                    var footer = "<div style='padding:20px; background: rgb(57,163,247); background: linear-gradient(321deg, rgba(57,163,247,1) 0%, rgba(40,76,224,1) 45%, rgba(118,63,200,1) 100%); color:#fff; display:flex; align-items:center; justify-content: space-between; text-align:center;'><table style='border:0; font-family:arial,helvetica,sans-serif; width:auto; margin:0 auto;'><tr><td style='color:#fff; font-size:1.3rem; vertical-align: middle;'>Are you intrested in this offer?</td><td style='width:10px;'></td><td style='vertical-align: middle;'><table style='border:0; font-family:arial,helvetica,sans-serif;'><tr><td><a href='" + SubscribeLink + "' style='font-family:arial,helvetica,sans-serif;color:#fff; background:#1a1a1a; border:1px solid #1a1a1a; padding:15px 25px; border-radius:28px; font-size: 1rem; margin-right:10px; text-decoration: none; display:flex; align-items:center;'><img src='https://thesolarproduct.com/assets/common/images/thumbs-up.png' style='height:22px; margin-right:8px;'> Yes, Interested</a></td><td><a href='" + UnSubscribeLink + "' style='font-family:arial,helvetica,sans-serif;color:#fff; background:#3293e2; padding:15px 25px; border-radius:28px; font-size: 1rem; text-decoration: none; display:flex; align-items:center;'><img src='https://thesolarproduct.com/assets/common/images/icon-unsubscribe.png' style='height:22px; margin-right:8px;'> Unsubscribe Now</a></td></tr></table></td></tr></table></div>";

                                    //Merge Email Body And Footer for subscribe & UnSubscribe
                                    string FinalEmailBody = input.IsFooterdAttached == true ? input.Description + footer : input.Description;

                                    try
                                    {
                                        var mail = LeadDetail.Email;
                                        await _emailSender.SendAsync(new MailMessage
                                        {
                                            From = new MailAddress(input.EmailFrom),
                                            To = { mail }, ////{ "hiral.prajapati@meghtechnologies.com" },
                                            Subject = input.Title,
                                            Body = FinalEmailBody,
                                            IsBodyHtml = true
                                        });
                                        //MailMessage mail = new MailMessage
                                        //{
                                        //    From = new MailAddress("info@solarproduct.com.au"), //new MailAddress(assignedToUser.EmailAddress),
                                        //    To = { "hiral.prajapati@meghtechnologies.com" },
                                        //    Subject = input.Title,
                                        //    Body = FinalEmailBody,
                                        //    IsBodyHtml = true
                                        //};
                                        //await this._emailSender.SendAsync(mail);
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                        }

                        ////Resolver for HangfireJob(Must have to use before Job Call)
                        //var leadReminderService = IocManager.Instance.Resolve<IClientDomainService>();

                        ////Hangfire Job(Fire And Forget) 
                        //var jobId = Hangfire.BackgroundJob.Enqueue(() => leadReminderService.SendWholeSalePromotion(WholeSalePromotion.TenantId, WholeSalePromotion.Id, (int)AbpSession.UserId));
                    }

                    if (msg.Count > 0)
                    {
                        sendBulkSMSInput.Messages = msg;
                        await _applicationSettings.SendWholeSaleBulkSMS(sendBulkSMSInput);
                    }
                }
            }
            else
            {
                //Send WholeSalePromotion to Only Test Users(Data Given in MobileNosList & Emails)
                if (input.WholeSalePromotionTypeId == 1)
                {
                    string[] MobileNosList = input.MobileNos.Split(',').Distinct().ToArray();
                    foreach (string mobileno in MobileNosList)
                    {
                        if (!string.IsNullOrEmpty(mobileno))
                        {
                            try
                            {
                                SendSMSInput sendSMSInput = new SendSMSInput();
                                sendSMSInput.PhoneNumber = mobileno;
                                sendSMSInput.Text = input.Description;
                                sendSMSInput.oId = input.OrganizationID;
                                await _applicationSettings.SendWholeSaleSMS(sendSMSInput);
                            }
                            catch (Exception e)
                            {

                            }
                        }
                    }
                }
                else
                {

                    if (!string.IsNullOrEmpty(input.Emails))
                    {
                        var KeyValue = Convert.ToString(AbpSession.TenantId);

                        //Token With Tenant Id & WholeSalePromotion User Primary Id for subscribe & UnSubscribe
                        var token = HttpUtility.UrlEncode(SimpleStringCipher.Instance.Encrypt(KeyValue, AppConsts.DefaultPassPhrase));

                        //Subscription Link
                        //string SubscribeLink = "https://thesolarproduct.com/account/wholesalecustomer-subscribe?STR=" + token;
                        string SubscribeLink = "http://localhost:4200/account/wholecustomer-subscribe?STR=" + token;

                        //UnSubscription Link
                        //string UnSubscribeLink = "https://thesolarproduct.com/account/wholesalecustomer-unsubscribe?STR=" + token;
                        string UnSubscribeLink = "https://localhost:4200/account/wholesalecustomer-unsubscribe?STR=" + token;

                        //Footer Design for subscribe & UnSubscribe
                        var footer = "<div style='padding:20px; background: rgb(57,163,247); background: linear-gradient(321deg, rgba(57,163,247,1) 0%, rgba(40,76,224,1) 45%, rgba(118,63,200,1) 100%); color:#fff; display:flex; align-items:center; justify-content: space-between; text-align:center;'><table style='border:0; font-family:arial,helvetica,sans-serif; width:auto; margin:0 auto;'><tr><td style='color:#fff; font-size:1.3rem; vertical-align: middle;'>Are you intrested in this offer?</td><td style='width:10px;'></td><td style='vertical-align: middle;'><table style='border:0; font-family:arial,helvetica,sans-serif;'><tr><td><a href='" + SubscribeLink + "' style='font-family:arial,helvetica,sans-serif;color:#fff; background:#1a1a1a; border:1px solid #1a1a1a; padding:15px 25px; border-radius:28px; font-size: 1rem; margin-right:10px; text-decoration: none; display:flex; align-items:center;'><img src='https://thesolarproduct.com/assets/common/images/thumbs-up.png' style='height:22px; margin-right:8px;'> Yes, Interested</a></td><td><a href='" + UnSubscribeLink + "' style='font-family:arial,helvetica,sans-serif;color:#fff; background:#3293e2; padding:15px 25px; border-radius:28px; font-size: 1rem; text-decoration: none; display:flex; align-items:center;'><img src='https://thesolarproduct.com/assets/common/images/icon-unsubscribe.png' style='height:22px; margin-right:8px;'> Unsubscribe Now</a></td></tr></table></td></tr></table></div>";
                        //var footer = "<div style=\"border:1px solid #ccc;padding:10px; background:#f2f2f2;\">If you are intrested in this offer, please click <a href=\"" + SubscribeLink + "\">" +
                        //    "<img height=\"20px\" src=\"https://thesolarproduct.com/assets/common/images/like.png\"></a>.<br/>" +
                        //    "If you do not wish to receive any further communications, please click <a href=\"" + UnSubscribeLink + "\">" +
                        //    "<img height=\"20px\" src=\"https://thesolarproduct.com/assets/common/images/dislike.png\"></a>.</div>";

                        //Merge Email Body And Footer for subscribe & UnSubscribe
                        string FinalEmailBody = input.Description + footer;

                        try
                        {
                            MailMessage mail = new MailMessage
                            {
                                From = new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                                To = { input.Emails },
                                Subject = input.Title,
                                Body = FinalEmailBody,
                                IsBodyHtml = true
                            };
                            await this._emailSender.SendAsync(mail);
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }
            }

        }

        /// <summary>
        /// Edit WholeSalePromotion Details ==> Not in Use
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        ///[AbpAuthorize(AppPermissions.Pages_WholeSalePromotions_Edit)]
        protected virtual async Task Update(CreateOrEditWholeSalePromotionDto input)
        {
            var WholeSalePromotion = await _WholeSalePromotionRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, WholeSalePromotion);
        }

        /// <summary>
        /// Delete WholeSalePromotion ==> Not in Use
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        //[AbpAuthorize(AppPermissions.Pages_WholeSalePromotions_Delete)]
        public async Task Delete(int promoId)
        {

            var promoreply = _WholeSalePromotionUsersRepository.GetAll().Where(e => e.WholeSalePromotionId == promoId).ToList();
            await _WholeSalePromotionRepository.DeleteAsync(promoId);
            foreach (var item in promoreply)
            {
                await _WholeSalePromotionUserRepository.DeleteAsync(item);
            }

        }

        /// <summary>
        /// Export WholeSalePromotion Data to Excel
        /// </summary>
        /// <param name="input"></param>
        /// <returns>FileDto</returns>
        public async Task<FileDto> GetWholeSalePromotionsToExcel(GetAllWholeSalePromotionsForExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.FromDateFilter, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.ToDateFilter, (int)AbpSession.TenantId));

            var filteredWholeSalePromotions = _WholeSalePromotionRepository.GetAll()
                .Include(e => e.PromotionTypeFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter), e => e.Title == input.TitleFilter)
                        .WhereIf(input.FromDateFilter != null && input.ToDateFilter != null,
                                    e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date &&
                                    e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.WholeSalePromotionTitleFilter != 0, e => e.Id == input.WholeSalePromotionTitleFilter)
                        .WhereIf(input.MinAmountFilter != null && input.MaxAmountFilter != null,
                                    e => e.PromoCharge >= input.MinAmountFilter &&
                                    e.PromoCharge <= input.MaxAmountFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.Contains(input.DescriptionFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.WholeSalePromotionTypeNameFilter), e => e.PromotionTypeFk != null && e.PromotionTypeFk.Name == input.WholeSalePromotionTypeNameFilter)
                        .WhereIf(input.PromoType != 0, e => e.PromotionTypeId == input.PromoType)
                        .WhereIf(input.OrganizationUnit != null && input.OrganizationUnit != 0, e => e.OrganizationID == input.OrganizationUnit);

            if (filteredWholeSalePromotions != null && filteredWholeSalePromotions.Count() > 0)
            {
                var JobStatus = _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.JobStatusId == 1 && e.LeadFk.OrganizationId == input.OrganizationUnit).Select(e => e.LeadId).ToList();
                var deposite = _invoicePaymentRepository.GetAll().Where(e => e.InvoicePaymentStatusId == 2).Select(e => e.JobFk.LeadId).Distinct().ToList();
                var WholeSalePromotionids = filteredWholeSalePromotions.Select(e => e.Id).Distinct().ToList();
                var WholeSalePromotionuser_list = _WholeSalePromotionUsersRepository.GetAll().Include(e => e.WholeSaleLeadFk).Where(e => e.WholeSaleLeadFk.OrganizationId == input.OrganizationUnit);


                var tot = (from pu in WholeSalePromotionuser_list
                           join promo in filteredWholeSalePromotions on pu.WholeSalePromotionId equals promo.Id
                           where pu.WholeSaleLeadFk.IsDeleted == false
                           select pu.Id).Count();

                var totPromo = (from pu in WholeSalePromotionuser_list
                                join promo in filteredWholeSalePromotions on pu.WholeSalePromotionId equals promo.Id
                                where pu.WholeSaleLeadFk.IsDeleted == false && pu.ResponseMessage != null
                                select pu.Id).Count();

                var interestedPromo = (from pu in WholeSalePromotionuser_list
                                       join promo in filteredWholeSalePromotions on pu.WholeSalePromotionId equals promo.Id
                                       where pu.WholeSaleLeadFk.IsDeleted == false && pu.WholeSaleLeadFk.IsDeleted == false && pu.ResponseMessage != null && pu.WholeSalePromotionResponseStatusId == 1
                                       select pu.Id).Count();

                var notInterestedPromo = (from pu in WholeSalePromotionuser_list
                                          join promo in filteredWholeSalePromotions on pu.WholeSalePromotionId equals promo.Id
                                          where pu.WholeSaleLeadFk.IsDeleted == false && pu.ResponseMessage != null && pu.WholeSalePromotionResponseStatusId == 2
                                          select pu.Id).Count();

                var otherPromo = (from pu in WholeSalePromotionuser_list
                                  join promo in filteredWholeSalePromotions on pu.WholeSalePromotionId equals promo.Id
                                  where pu.WholeSaleLeadFk.IsDeleted == false && pu.ResponseMessage != null && pu.WholeSalePromotionResponseStatusId == 3
                                  select pu.Id).Count();

                var WholeSalePromotions = from o in filteredWholeSalePromotions
                                              //join o1 in _lookup_WholeSalePromotionTypeRepository.GetAll() on o.WholeSalePromotionTypeId equals o1.Id into j1
                                              //from s1 in j1.DefaultIfEmpty()
                                          join o2 in _lookup_userRepository.GetAll() on o.CreatorUserId equals o2.Id
                                          select new GetWholeSalePromotionForViewDto()
                                          {
                                              WholeSalePromotion = new WholeSalePromotionDto
                                              {
                                                  Title = o.Title,
                                                  PromoCharge = o.PromoCharge,
                                                  Description = o.Description,
                                                  CreationTime = o.CreationTime,
                                                  OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == o.OrganizationID).Select(e => e.DisplayName).FirstOrDefault(),
                                                  Id = o.Id
                                              },
                                              InterestedCount = WholeSalePromotionuser_list.Where(s => s.WholeSalePromotionId == o.Id && s.WholeSalePromotionResponseStatusId == 1).Count(),
                                              NotInterestedCount = WholeSalePromotionuser_list.Where(s => s.WholeSalePromotionId == o.Id && s.WholeSalePromotionResponseStatusId == 2).Count(),
                                              OtherCount = WholeSalePromotionuser_list.Where(s => s.WholeSalePromotionId == o.Id && s.WholeSalePromotionResponseStatusId == 3).Count(),
                                              NoReply = WholeSalePromotionuser_list.Where(s => s.WholeSalePromotionId == o.Id && s.ResponseMessage == null).Count(),
                                              //TotalLeads = _WholeSalePromotionUsersRepository.GetAll().Where(s => s.WholeSalePromotionId == o.Id).Count(),
                                              TotalLeads = o.LeadCount,
                                              //WholeSalePromotionTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                                              WholeSalePromotionTypeName = o.PromotionTypeFk.Name,
                                              CreatorUserName = o2.UserName,

                                              TotalSendWholeSalePromotion = tot,
                                              TotalWholeSalePromotion = totPromo,
                                              summaryInterestedCount = interestedPromo,
                                              summaryNotInterestedCount = notInterestedPromo,
                                              summaryOtherCount = otherPromo,
                                              //TotalSendWholeSalePromotion = tot,
                                              //TotalWholeSalePromotion = WholeSalePromotionuser_list.Where(e => WholeSalePromotionids.Contains((int)e.WholeSalePromotionId) && e.ResponseMessage != null).Select(e => e.Id).Count(),
                                              //                           summaryInterestedCount = WholeSalePromotionuser_list.Where(e => WholeSalePromotionids.Contains((int)e.WholeSalePromotionId) && e.ResponseMessage != null && e.WholeSalePromotionResponseStatusId == 1).Select(e => e.Id).Count(),
                                              //                           summaryNotInterestedCount = WholeSalePromotionuser_list.Where(e => WholeSalePromotionids.Contains((int)e.WholeSalePromotionId) && e.ResponseMessage != null && e.WholeSalePromotionResponseStatusId == 2).Select(e => e.Id).Count(),
                                              //                           summaryOtherCount = WholeSalePromotionuser_list.Where(e => WholeSalePromotionids.Contains((int)e.WholeSalePromotionId) && e.ResponseMessage != null  && e.WholeSalePromotionResponseStatusId == 3).Select(e => e.Id).Count(),
                                              //                           Unhandle = WholeSalePromotionuser_list.Where(e => WholeSalePromotionids.Contains((int)e.WholeSalePromotionId) && e.ResponseMessage != null && e.LeadFk.LeadStatusId != 6).Select(e => e.LeadId).Distinct().Count(),
                                              //WholeSalePromotionSold = WholeSalePromotionuser_list.Where(e => WholeSalePromotionids.Contains((int)e.WholeSalePromotionId) && e.ResponseMessage != null && deposite.Contains((int)e.LeadId)).Select(e => e.LeadId).Distinct().Count(),
                                              //WholeSalePromotionProject = WholeSalePromotionuser_list.Where(e => WholeSalePromotionids.Contains((int)e.WholeSalePromotionId) && e.ResponseMessage != null  && JobStatus.Contains((int)e.LeadId)).Select(e => e.LeadId).Distinct().Count(),

                                          };

                var totalCount = await filteredWholeSalePromotions.CountAsync();

                return _WholeSalePromotionsExcelExporter.ExportToFile(await WholeSalePromotions.ToListAsync());

            }
            else
            {
            return _WholeSalePromotionsExcelExporter.ExportToFile( new List<GetWholeSalePromotionForViewDto>());
                //return new PagedResultDto<GetWholeSalePromotionForViewDto>(0, null);
            }

        }

        /// <summary>
        /// WholeSalePromotion Type Dropdown
        /// </summary>
        /// <returns></returns>
        //[AbpAuthorize(AppPermissions.Pages_WholeSalePromotions)]
        public async Task<List<WholeSalePromotionTypeLookupTableDto>> GetAllWholeSalePromotionTypeForTableDropdown()
        {
            return await _lookup_WholeSalePromotionTypeRepository.GetAll()
                .Select(WholeSalePromotionType => new WholeSalePromotionTypeLookupTableDto
                {
                    Id = WholeSalePromotionType.Id,
                    DisplayName = WholeSalePromotionType == null || WholeSalePromotionType.Name == null ? "" : WholeSalePromotionType.Name.ToString()
                }).ToListAsync();
        }

        /// <summary>
        /// Subscribe And UnSubscribe for WholeSalePromotion
        /// </summary>
        /// <param name="STR">Encrypted Link (Tenant Id & WholeSalePromotion Id)</param>
        /// <param name="WholeSalePromotionResponseStatusId">1 = Subscribe,2 = UnSubscribe</param>
        /// <returns></returns>
        public async Task SubScribeUnsubscribepromo(string STR, int WholeSalePromotionResponseStatusId)
        {
            //Encrypt TenantId & WholeSalePromotionId
            var IDs = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(STR, AppConsts.DefaultPassPhrase));
            var ID = IDs.Split(",");
            int TenantId = Convert.ToInt32(ID[0]);
            int Promoid = 0;
            int sectionId = 0;

            if (ID.Length > 1)
            {
                Promoid = Convert.ToInt32(ID[1]);
            }
            if(ID.Length >2)
            {
                sectionId = Convert.ToInt32(ID[2]);
            }

            //Get Set from GIven Tenant Id
            using (_unitOfWorkManager.Current.SetTenantId(TenantId))
            {
                string msg = string.Format("WholeSalePromotion Email Replay Received");

                if (Promoid > 0)
                {
                    var WholeSalePromotionUser = await _WholeSalePromotionUserRepository.FirstOrDefaultAsync((int)Promoid);
                    WholeSalePromotionUser.WholeSalePromotionResponseStatusId = WholeSalePromotionResponseStatusId;
                    if (WholeSalePromotionResponseStatusId == 1)
                    {
                        WholeSalePromotionUser.ResponseMessage = "Yes";
                    }
                    if (WholeSalePromotionResponseStatusId == 2)
                    {
                        WholeSalePromotionUser.ResponseMessage = "Stop";
                    }
                    WholeSalePromotionUser.ResponseDate = DateTime.UtcNow;
                    await _WholeSalePromotionUserRepository.UpdateAsync(WholeSalePromotionUser);

                    var prmotionuserid = _leadactivityRepository.GetAll().Where(e => e.PromotionUserId == (int)Promoid).Select(e => e.Id).FirstOrDefault();

                    var activity = new WholeSaleLeadActivityLog();
                    activity.ActionNote = "Prmotion Email Reply Given By Customer";
                    activity.TenantId = TenantId;
                    activity.ActionId = 62;
                    activity.CreatorUserId = 1;
                    activity.WholeSaleLeadId = (int)WholeSalePromotionUser.WholeSaleLeadId;
                    //activity.MessageId = smsID;
                    activity.ReferanceId = prmotionuserid;
                    activity.IsMark = false;
                    activity.SectionId = sectionId;
                    await _leadactivityRepository.InsertAsync(activity);

                    var Activityid = _leadactivityRepository.GetAll().Where(e => e.Id == prmotionuserid).FirstOrDefault();
                    var Lead = _wholeSaleLeadRepository.GetAll().Where(e => e.Id == Activityid.WholeSaleLeadId).FirstOrDefault();
                    //            var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == AssignUserID).FirstOrDefault();
                    //string msg = string.Format("WholeSalePromotion Reply Received");
                    //await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);


                    var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == Lead.AssignUserId).FirstOrDefault();
                    await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);

                    //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == Lead.AssignUserId).Select(e => e.TeamId).ToList();
                    //var ManagerUserid = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
                    //foreach (var id in ManagerUserid)
                    //{
                    //    var NotifyToManager = _userRepository.GetAll().Where(u => u.Id == id).FirstOrDefault();
                    //    await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
                    //}
                }


                //var roleids = _roleRepository.GetAll().Where(e => e.Name == "Admin").Select(e => e.Id).ToList();
                //var userids = _userRoleRepository.GetAll().Where(e => roleids.Contains(e.RoleId)).Select(e => e.UserId).ToList();

                //foreach (var id in userids)
                //{
                //    var NotifyToUsertoadmin = _userRepository.GetAll().Where(u => u.Id == id).FirstOrDefault();
                //    await _appNotifier.NewLead(NotifyToUsertoadmin, msg, NotificationSeverity.Info);
                //}

                
            }
        }

        /// <summary>
        /// Export Excel With Mobile & Email 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<FileDto> CreateWithExcel(CreateOrEditWholeSalePromotionDto input)
        {
            //Send WholeSalePromotion to all Leads
            var WholeSalePromotion = ObjectMapper.Map<WholeSalePromotion>(input);

            if (AbpSession.TenantId != null)
            {
                WholeSalePromotion.TenantId = (int)AbpSession.TenantId;
            }

            string LeadStatusIdsFilter = "";
            string LeadSourceIdsFilter = "";
            string StateIdsFilter = "";
            string TeamIdsFilter = "";
            string JobStatusIdsFilter = "";

            if (input.LeadStatusIdsFilter != null)
            {
                foreach (var item in input.LeadStatusIdsFilter)
                {
                    LeadStatusIdsFilter = LeadStatusIdsFilter + item;
                }
            }
            if (input.LeadSourceIdsFilter != null)
            {
                foreach (var item in input.LeadSourceIdsFilter)
                {
                    LeadSourceIdsFilter = LeadSourceIdsFilter + item;
                }
            }
            if (input.StateIdsFilter != null)
            {
                foreach (var item in input.StateIdsFilter)
                {
                    StateIdsFilter = StateIdsFilter + item;
                }
            }
            if (input.TeamIdsFilter != null)
            {
                foreach (var item in input.TeamIdsFilter)
                {
                    TeamIdsFilter = TeamIdsFilter + item;
                }
            }
            if (input.JobStatusIdsFilter != null)
            {
                foreach (var item in input.JobStatusIdsFilter)
                {
                    JobStatusIdsFilter = JobStatusIdsFilter + item;
                }
            }
            WholeSalePromotion.LeadStatusIdsFilter = LeadStatusIdsFilter;
            WholeSalePromotion.LeadSourceIdsFilter = LeadSourceIdsFilter;
            WholeSalePromotion.StateIdsFilter = StateIdsFilter;
            WholeSalePromotion.TeamIdsFilter = TeamIdsFilter;
            WholeSalePromotion.JobStatusIdsFilter = JobStatusIdsFilter;

            //Insert Data in WholeSalePromotion Table With WholeSalePromotion Details & Lead Count
            int newWholeSalePromotionId = _WholeSalePromotionRepository.InsertAndGetId(WholeSalePromotion);

            string[] arrSelectedLeadIds = input.SelectedLeadIdsForWholeSalePromotion.Split(',').Distinct().ToArray();

            var List = new List<WholeSalePromotionUser>();

            var ActivityList = new List<WholeSaleLeadActivityLog>();

            var sendBulkSMSInput = new SendBulkSMSInput();

            var WholeSalePromotionSMSEmailList = new List<WholeSalePromotionSMSEmailList>();

            //List for WholeSalePromotion User Table for all leads
            foreach (string leadId in arrSelectedLeadIds)
            {
                if (!string.IsNullOrEmpty(leadId))
                {
                    int LeadId = Convert.ToInt32(leadId);

                    var ListItem = new WholeSalePromotionUser();
                    ListItem.CreationTime = WholeSalePromotion.CreationTime;
                    ListItem.CreatorUserId = WholeSalePromotion.CreatorUserId;
                    ListItem.IsDeleted = false;
                    ListItem.WholeSalePromotionId = newWholeSalePromotionId;
                    ListItem.WholeSaleLeadId = LeadId;
                    ListItem.CreatorUserId = WholeSalePromotion.CreatorUserId;
                    ListItem.TenantId = WholeSalePromotion.TenantId;
                    List.Add(ListItem);

                    var ActivityListItem = new WholeSaleLeadActivityLog();
                    ActivityListItem.ActionId = 52;
                    ActivityListItem.ActionNote = "WholeSalePromotion " + input.Description + " has been sent";
                    ActivityListItem.WholeSaleLeadId = LeadId;
                    ActivityListItem.Body = input.Description;
                    ActivityListItem.TenantId = WholeSalePromotion.TenantId;
                    ActivityListItem.CreatorUserId = WholeSalePromotion.CreatorUserId;
                    ActivityListItem.CreationTime = WholeSalePromotion.CreationTime;
                    ActivityListItem.IsDeleted = false;
                    ActivityList.Add(ActivityListItem);

                    var WholeSalePromotionSMSEmailListItem = new WholeSalePromotionSMSEmailList();
                    var LeadData = _leadRepository.GetAll().Where(e => e.Id == LeadId).Select(e => new { e.Mobile, e.Email }).FirstOrDefault();
                    WholeSalePromotionSMSEmailListItem.SMS = LeadData.Mobile;
                    WholeSalePromotionSMSEmailListItem.Email = LeadData.Email;
                    WholeSalePromotionSMSEmailList.Add(WholeSalePromotionSMSEmailListItem);
                }
            }

            //Bulk Insert Data in WholeSalePromotion User Table
            await _dbcontextprovider.GetDbContext().WholeSalePromotionUsers.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ////Bulk Insert Data in Lead Activity Log Table
            await _dbcontextprovider.GetDbContext().WholeSaleLeadActivityLogs.AddRangeAsync(ActivityList);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            return _WholeSalePromotionsExcelExporter.ExportSMSEmailListFile(WholeSalePromotionSMSEmailList);
        }

        public async Task<GetPromotionHistorybyPromotionIdOutPut> GetPromotionHistorybyPromotionId(int promotionId)
        {
            var promotionhistory = _promotionHistoryRepository.GetAll().Where(e => e.PromotionId == promotionId && e.PromotionType == "WholeSale").FirstOrDefault();

            var result = new GetPromotionHistorybyPromotionIdOutPut();

            if (promotionhistory != null)
            {
                result = new GetPromotionHistorybyPromotionIdOutPut
                {
                    Id = promotionhistory.Id,
                    Description = promotionhistory.Description,
                    TotalCredit = promotionhistory.TotalCredit,
                    TotalCharCount = promotionhistory.TotalCharCount,
                    LeadIds = promotionhistory.LeadIds,
                    Title = promotionhistory.Title,
                    PromotionSendingId = promotionhistory.PromotionSendingId,
                    LeadStatuses = promotionhistory.LeadStatuses,
                    States = promotionhistory.States,
                    StartDate = promotionhistory.StartDate,
                    EndDate = promotionhistory.EndDate,
                    Charges = promotionhistory.Charges
                };
            }

            return result;
        }


    }
}