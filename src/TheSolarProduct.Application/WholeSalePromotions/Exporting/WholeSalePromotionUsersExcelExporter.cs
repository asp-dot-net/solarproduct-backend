﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.WholeSalePromotions.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using System;
using TheSolarProduct.Authorization;
using TheSolarProduct.Invoices.Dtos;
using Abp.Authorization;

namespace TheSolarProduct.WholeSalePromotions.Exporting
{
    public class WholeSalePromotionUsersExcelExporter : NpoiExcelExporterBase, IWholeSalePromotionUsersExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly PermissionChecker _permissionChecker;

        public WholeSalePromotionUsersExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager, PermissionChecker permissionChecker) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _permissionChecker = permissionChecker;
        }

        public FileDto ExportToFile(List<GetWholeSalePromotionUserForViewDto> WholeSalePromotionUsers, string fileName)
        {
            string[] Header = null;
            Func<GetWholeSalePromotionUserForViewDto, object>[] Objects = null;

            //var EmailPermission = _permissionChecker.IsGranted(AppPermissions.Pages_WholeSalePromotionUsers_Excel_MobileEmail);

            //if (EmailPermission)
            //{
            //    Header = new string[] { "Customer", "Sales Rep", "Mobile/Email", "Response", "Responce On", "Lead Status", "JobNumber", "Job Status", "Title", "Type", "Send On", "Comment", "Next Followup", "Description" };

                Objects = new Func<GetWholeSalePromotionUserForViewDto, object>[] {
                    _ => _.LeadCopanyName,
                    _ => _.CurrentLeadOwner,
                    _ => string.Concat(_.Mobile, "/", _.EMail),
                    _ => _.WholeSalePromotionUser.ResponseMessage,
                    _ => _timeZoneConverter.Convert(_.WholeSalePromotionUser.ResponseDate, _abpSession.TenantId, _abpSession.GetUserId()),
                    _ => _.LeadStatus,
                    _ => _.ProjectNumber,
                    _ => _.ProjectStatus,
                    _ => _.WholeSalePromotionTitle,
                    _ => _.WholeSalePromotionTypeName,
                    _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId()),
                    _ => _.ActivityComment,
                    _ => _.ActivityReminderTime,
                    _ => _.ActivityDescription
                };
            //}
            //else
            //{
            //    Header = new string[] { "Customer", "Sales Rep", "Response", "Responce On", "Lead Status", "JobNumber", "Job Status", "Title", "Type", "Send On", "Comment", "Next Followup", "Description" };

            //    Objects = new Func<GetWholeSalePromotionUserForViewDto, object>[] {
            //        _ => _.LeadCopanyName,
            //        _ => _.CurrentLeadOwner,
            //        _ => _.WholeSalePromotionUser.ResponseMessage,
            //        _ => _timeZoneConverter.Convert(_.WholeSalePromotionUser.ResponseDate, _abpSession.TenantId, _abpSession.GetUserId()),
            //        _ => _.LeadStatus,
            //        _ => _.ProjectNumber,
            //        _ => _.ProjectStatus,
            //        _ => _.WholeSalePromotionTitle,
            //        _ => _.WholeSalePromotionTypeName,
            //        _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId()),
            //        _ => _.ActivityComment,
            //        _ => _.ActivityReminderTime,
            //        _ => _.ActivityDescription
            //    };
            //}

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("WholeSalePromotionUsers");

                    AddHeader(sheet, Header);

                    AddObjects(sheet, 1, WholeSalePromotionUsers, Objects);

                    for (var i = 1; i <= WholeSalePromotionUsers.Count; i++)
                    {
                        //if (EmailPermission)
                        //{
                            SetCellDataFormat(sheet.GetRow(i).Cells[4], "dd-MM-yyyy");
                            SetCellDataFormat(sheet.GetRow(i).Cells[10], "dd-MM-yyyy");
                            SetCellDataFormat(sheet.GetRow(i).Cells[12], "dd-MM-yyyy");
                        //}
                        //else
                        //{
                            //SetCellDataFormat(sheet.GetRow(i).Cells[3], "dd-MM-yyyy");
                            //SetCellDataFormat(sheet.GetRow(i).Cells[9], "dd-MM-yyyy");
                            //SetCellDataFormat(sheet.GetRow(i).Cells[11], "dd-MM-yyyy");
                        //}
                    }

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);

                        //if (EmailPermission && i != 3)
                        //{
                        //    sheet.AutoSizeColumn(i);
                        //}
                        //else
                        //{
                        //    sheet.DefaultColumnWidth = 15;
                        //}

                        //if (EmailPermission == false && i != 2)
                        //{
                        //    sheet.AutoSizeColumn(i);
                        //}
                        //else
                        //{
                        //    sheet.DefaultColumnWidth = 15;
                        //}
                    }
                });
        }

    }
}
