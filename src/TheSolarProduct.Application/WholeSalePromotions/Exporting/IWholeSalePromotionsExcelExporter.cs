﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.WholeSalePromotions.Dtos;

namespace TheSolarProduct.WholeSalePromotions.Exporting
{
    public interface IWholeSalePromotionsExcelExporter
    {
        FileDto ExportToFile(List<GetWholeSalePromotionForViewDto> WholeSalepromotions);

        FileDto ExportSMSEmailListFile(List<WholeSalePromotionSMSEmailList> WholeSalepromotionSMSEmailList);
    }
}
