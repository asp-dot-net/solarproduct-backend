﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.WholeSalePromotions.Dtos;

namespace TheSolarProduct.WholeSalePromotions.Exporting
{
    public interface IWholeSalePromotionUsersExcelExporter
    {
        FileDto ExportToFile(List<GetWholeSalePromotionUserForViewDto> promotionUsers, string fileName);

    }
}
