﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.WholeSalePromotions.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.WholeSalePromotions.Exporting
{
    public class WholeSalePromotionsExcelExporter : NpoiExcelExporterBase, IWholeSalePromotionsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public WholeSalePromotionsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetWholeSalePromotionForViewDto> WholeSalePromotions)
        {
            return CreateExcelPackage(
                "WholeSalePromotions.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("WholeSalePromotions");

                    AddHeader(
                        sheet,
                        "WholeSalePromotion Type",
                        "Title",
                        "Promo Charge",
                        "Created By",
                        "Created On",
                        "Interested",
                        "Not Interested",
                        "Other",
                        "No Reply",
                        "Total Leads"
                        );

                    AddObjects(
                        sheet, 1, WholeSalePromotions,
                        _ => _.WholeSalePromotionTypeName,
                        _ => _.WholeSalePromotion.Title,
                        _ => string.Concat("$", _.WholeSalePromotion.PromoCharge),
                        _ => _.CreatorUserName,
                        _ => _.WholeSalePromotion.CreationTime,
                        _ => _.InterestedCount,
                        _ => _.NotInterestedCount,
                        _ => _.OtherCount,
                        _ => _.NoReply,
                        _ => _.TotalLeads
                        );

                    for (var i = 1; i <= WholeSalePromotions.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[4], "dd/MM/yyyy");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[5], "00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[6], "00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[7], "00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[8], "00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[9], "00");
                    }
                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto ExportSMSEmailListFile(List<WholeSalePromotionSMSEmailList> WholeSalePromotionSMSEmailList)
        {
            return CreateExcelPackage(
                "WholeSalePromotionsSMSEmailList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("WholeSalePromotionsSMSEmailList"));

                    AddHeader(
                        sheet,
                        L("Mobile"),
                        L("Email")
                        );

                    AddObjects(
                        sheet, 2, WholeSalePromotionSMSEmailList,
                        _ => _.SMS,
                        _ => _.Email
                        );
                });
        }
    }
}
