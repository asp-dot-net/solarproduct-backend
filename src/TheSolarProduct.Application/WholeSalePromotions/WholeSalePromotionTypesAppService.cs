﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.WholeSalePromotions.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace TheSolarProduct.WholeSalePromotions
{
    //[AbpAuthorize(AppPermissions.Pages_WholeSalePromotionTypes)]
    [AbpAuthorize]
    public class WholeSalePromotionTypesAppService : TheSolarProductAppServiceBase, IWholeSalePromotionTypesAppService
    {
        private readonly IRepository<WholeSalePromotionType> _WholeSalePromotionTypeRepository;


        public WholeSalePromotionTypesAppService(IRepository<WholeSalePromotionType> WholeSalePromotionTypeRepository)
        {
            _WholeSalePromotionTypeRepository = WholeSalePromotionTypeRepository;

        }

        public async Task<PagedResultDto<GetWholeSalePromotionTypeForViewDto>> GetAll(GetAllWholeSalePromotionTypesInput input)
        {

            var filteredWholeSalePromotionTypes = _WholeSalePromotionTypeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

            var pagedAndFilteredWholeSalePromotionTypes = filteredWholeSalePromotionTypes
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var WholeSalePromotionTypes = from o in pagedAndFilteredWholeSalePromotionTypes
                                 select new GetWholeSalePromotionTypeForViewDto()
                                 {
                                     WholeSalePromotionType = new WholeSalePromotionTypeDto
                                     {
                                         Name = o.Name,
                                         Id = o.Id
                                     }
                                 };

            var totalCount = await filteredWholeSalePromotionTypes.CountAsync();

            return new PagedResultDto<GetWholeSalePromotionTypeForViewDto>(
                totalCount,
                await WholeSalePromotionTypes.ToListAsync()
            );
        }

        //[AbpAuthorize(AppPermissions.Pages_WholeSalePromotionTypes_Edit)]
        public async Task<GetWholeSalePromotionTypeForEditOutput> GetWholeSalePromotionTypeForEdit(EntityDto input)
        {
            var WholeSalePromotionType = await _WholeSalePromotionTypeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetWholeSalePromotionTypeForEditOutput { PromotionType = ObjectMapper.Map<CreateOrEditWholeSalePromotionTypeDto>(WholeSalePromotionType) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditWholeSalePromotionTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        //[AbpAuthorize(AppPermissions.Pages_WholeSalePromotionTypes_Create)]
        protected virtual async Task Create(CreateOrEditWholeSalePromotionTypeDto input)
        {
            var WholeSalePromotionType = ObjectMapper.Map<WholeSalePromotionType>(input);



            await _WholeSalePromotionTypeRepository.InsertAsync(WholeSalePromotionType);
        }

        //[AbpAuthorize(AppPermissions.Pages_WholeSalePromotionTypes_Edit)]
        protected virtual async Task Update(CreateOrEditWholeSalePromotionTypeDto input)
        {
            var WholeSalePromotionType = await _WholeSalePromotionTypeRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, WholeSalePromotionType);
        }

        //[AbpAuthorize(AppPermissions.Pages_WholeSalePromotionTypes_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _WholeSalePromotionTypeRepository.DeleteAsync(input.Id);
        }
    }
}