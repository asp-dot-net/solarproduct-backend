﻿using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.CheckDepositeReceive.Exporting;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using TheSolarProduct.CheckDepositReceived;
using TheSolarProduct.CheckDepositReceived.Dto;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;


namespace TheSolarProduct.CheckDepositeReceive
{
    
    public class CheckDepositeAppService : TheSolarProductAppServiceBase, ICheckDepositReceivedAppService
    {
        private readonly IRepository<Checkdeposite> _checkDepositReceivedRepository;

        private readonly ICheckDepositeReceivedExcelExporter _checkDepositReceivedExcelExporter;

        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public CheckDepositeAppService(IRepository<Checkdeposite> checkDepositReceivedRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider, ICheckDepositeReceivedExcelExporter checkDepositReceivedExcelExporter)
        {

            _checkDepositReceivedExcelExporter = checkDepositReceivedExcelExporter;
            _checkDepositReceivedRepository = checkDepositReceivedRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;

        }

        public async Task<PagedResultDto<GetCheckDepositReceivedForViewDto>> GetAll(GetAllCheckDepositReceivedInput input)
        {
            var checksDepositReceived = _checkDepositReceivedRepository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter);

            var pagedAndFiltered = checksDepositReceived
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var output = from o in pagedAndFiltered
                         select new GetCheckDepositReceivedForViewDto()
                         {
                             CheckDepositReceived = new CheckDepositReceivedDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 IsActive = o.IsActive
                             }
                         };

            var totalCount = await checksDepositReceived.CountAsync();

            return new PagedResultDto<GetCheckDepositReceivedForViewDto>(totalCount, await output.ToListAsync());
        }



        //public async Task<GetCheckDepositeReceiveForEditOutput> GetCheckDepositeReceiveForEdit(EntityDto input)
        //{
        //    var department = await _checkDepositReceivedRepository.FirstOrDefaultAsync(input.Id);

        //    var output = new GetCheckDepositeReceiveForEditOutput { CheckDepositReceived = ObjectMapper.Map<CreateOrEditCheckDepositReceivedDto>(department) };

        //    return output;
        //}




        public async Task CreateOrEdit(CreateOrEditCheckDepositReceivedDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(CreateOrEditCheckDepositReceivedDto input)
        {
            var checkDepositReceived = ObjectMapper.Map<Checkdeposite>(input);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 57;
            dataVaultLog.ActionNote = "Check Deposite Received Created: " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _checkDepositReceivedRepository.InsertAsync(checkDepositReceived);
        }

        protected virtual async Task Update(CreateOrEditCheckDepositReceivedDto input)
        {
            var check = await _checkDepositReceivedRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 57;
            dataVaultLog.ActionNote = "Check Deposite Received Updated : " + check.Name;
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            var List = new List<DataVaultActivityLogHistory>();
            if (input.Name != check.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = check.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != check.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = check.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, check);

            await _checkDepositReceivedRepository.UpdateAsync(check);
        }

        public async Task<GetCheckDepositReceivedForEditOutput> GetCheckDepositReceivedForEdit(EntityDto input)
        {
            var checkDepositReceeived = await _checkDepositReceivedRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetCheckDepositReceivedForEditOutput { CheckDepositReceived = ObjectMapper.Map<CreateOrEditCheckDepositReceivedDto>(checkDepositReceeived) };

            return output;
        }

        public async Task Delete(EntityDto input)
        {
            var Name = _checkDepositReceivedRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 57;
            dataVaultLog.ActionNote = "Check Deposite Deleted  : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _checkDepositReceivedRepository.DeleteAsync(input.Id);
        }


        public async Task<FileDto> GetCheckDepositeReceiveToExcel(GetAllCheckDepositeReceivedForExcelInput input)
        {

            var filteredCheckDepositeReceive = _checkDepositReceivedRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredCheckDepositeReceive
                         select new GetCheckDepositReceivedForViewDto()
                         {
                             CheckDepositReceived = new CheckDepositReceivedDto
                             {
                                 Name = o.Name,
                                 Id = o.Id
                             }
                         });


            var CheckDepositReceivedListDtos = await query.ToListAsync();

            return _checkDepositReceivedExcelExporter.ExportToFile(CheckDepositReceivedListDtos);
        }


    }
}
