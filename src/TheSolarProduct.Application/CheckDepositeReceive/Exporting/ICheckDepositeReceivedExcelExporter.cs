﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckDepositReceived.Dto;
using TheSolarProduct.Departments.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.CheckDepositeReceive.Exporting
{
    public interface ICheckDepositeReceivedExcelExporter
    {

        FileDto ExportToFile(List<GetCheckDepositReceivedForViewDto> chequedeposite);
    }
}
