﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.CheckDepositReceived.Dto;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.TheSolarDemo.Dtos;
using TheSolarProduct.TheSolarDemo.Exporting;

namespace TheSolarProduct.CheckDepositeReceive.Exporting
{
    public class CheckDepositeReceivedExcelExporter : NpoiExcelExporterBase, ICheckDepositeReceivedExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public CheckDepositeReceivedExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetCheckDepositReceivedForViewDto> checkdeposite)
        {
            return CreateExcelPackage(
                "CheckDepositeReceive.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet("CheckdepositeReceive");

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, 2, checkdeposite,
                        _ => _.CheckDepositReceived.Name
                        );



                });
        }
    }
}
