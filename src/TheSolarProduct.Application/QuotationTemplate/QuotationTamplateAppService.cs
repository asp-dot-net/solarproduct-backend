﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using System.Linq;
using System.Threading.Tasks;
using TheSolarProduct.QuotationTemplate.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using Abp.Organizations;
using TheSolarProduct.Quotations;
using TheSolarProduct.Jobs.Dtos;
using System.Collections.Generic;
using TheSolarProduct.Leads;
using TheSolarProduct.Jobs;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Migrations;

namespace TheSolarProduct.QuotationTemplate
{
    [AbpAuthorize]
    public class QuotationTamplateAppService : TheSolarProductAppServiceBase, IQuotationTamplateAppService
    {
        private readonly IRepository<Quotations.QuotationTemplate> _quotationTemplateRepqository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<TemplateType> _templateTypeRepository;
        private readonly IRepository<Lead, int> _leadRepository;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public QuotationTamplateAppService(IRepository<Quotations.QuotationTemplate> quotationTemplateRepqository, IRepository<OrganizationUnit, long> organizationUnitRepository, IRepository<TemplateType> templateTypeRepository, IRepository<Lead, int> leadRepository, IRepository<Job> jobRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _quotationTemplateRepqository = quotationTemplateRepqository;
            _organizationUnitRepository = organizationUnitRepository;
            _templateTypeRepository = templateTypeRepository;
            _leadRepository = leadRepository;
            _jobRepository = jobRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
        }

        public async Task<PagedResultDto<GetQuotationTemplateForViewDto>> GetAll(GetAllQuotationTemplateInput input)
        {
            var filteredResult = _quotationTemplateRepqository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.TemplateName.Contains(input.Filter))
                .WhereIf(input.OrganizationId != 0, e => e.OrganizationUnitId == input.OrganizationId)
                .WhereIf(input.TemplateTypeId != 0, e => e.TemplateTypeId == input.TemplateTypeId);

            var pagedAndFilteredResult = filteredResult.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var result = from o in pagedAndFilteredResult
                                select new GetQuotationTemplateForViewDto()
                                {
                                    QuotationTemplates = new QuotationTemplateDto
                                    {
                                        TemplateName = o.TemplateName,
                                        OrganazationUnitsName = o.OrganizationUnitFk.DisplayName,
                                        ViewHtml = o.ViewHtml,
                                        IsActive = o.IsActive,
                                        Id = o.Id,
                                        TemplateType = _templateTypeRepository.GetAll().Where(e => e.Id == o.TemplateTypeId).Select(e => e.Name).FirstOrDefault()
                                    }
                                };

            var totalCount = await filteredResult.CountAsync();

            return new PagedResultDto<GetQuotationTemplateForViewDto>(totalCount, await result.ToListAsync());
        }

        //[AbpAuthorize(AppPermissions.Pages_QuotationTemplate_Edit)]
        public async Task<GetQuotationTemplateForEditOutput> GetQuotationTemplateForEdit(EntityDto input)
        {
            var result = await _quotationTemplateRepqository.GetAsync(input.Id);

            var output = new GetQuotationTemplateForEditOutput { QuotationTemplate = ObjectMapper.Map<CreateOrEditQuotationTemplateDto>(result) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditQuotationTemplateDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_QuotationTemplate_Create)]
        protected virtual async Task Create(CreateOrEditQuotationTemplateDto input)
        {
            try
            {
                var result = ObjectMapper.Map<Quotations.QuotationTemplate>(input);

                if (AbpSession.TenantId != null)
                {
                    result.TenantId = (int)AbpSession.TenantId;
                }

                var dataVaultLog = new DataVaultActivityLog();
                dataVaultLog.Action = "Created";
                dataVaultLog.SectionId = 39;
                dataVaultLog.ActionNote = "Html Template Created : " + input.TemplateName;
                await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

                await _quotationTemplateRepqository.InsertAndGetIdAsync(result);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        [AbpAuthorize(AppPermissions.Pages_QuotationTemplate_Edit)]
        protected virtual async Task Update(CreateOrEditQuotationTemplateDto input)
        {
            var result = await _quotationTemplateRepqository.GetAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 39;
            dataVaultLog.ActionNote = "Html Template Updated : " + result.TemplateName;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.TemplateName != result.TemplateName)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "TemplateName";
                history.PrevValue = result.TemplateName;
                history.CurValue = input.TemplateName;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.TemplateTypeId != result.TemplateTypeId)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "TemplateType";
                history.PrevValue = result.TemplateTypeId > 0 ? _templateTypeRepository.Get(result.TemplateTypeId).Name : "";
                history.CurValue = input.TemplateTypeId > 0 ? _templateTypeRepository.Get(input.TemplateTypeId).Name : "";
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.OrganizationUnitId != result.OrganizationUnitId)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Organization";
                history.PrevValue = result.OrganizationUnitId > 0 ? _organizationUnitRepository.Get(result.OrganizationUnitId).DisplayName : "";
                history.CurValue = input.OrganizationUnitId > 0 ? _organizationUnitRepository.Get(input.OrganizationUnitId).DisplayName : "";
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.ViewHtml != result.ViewHtml)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Template Code";
                history.PrevValue = result.ViewHtml;
                history.CurValue = input.ViewHtml;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Subject != result.Subject)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Subject";
                history.PrevValue = result.Subject;
                history.CurValue = input.Subject;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, result);
        }

        [AbpAuthorize(AppPermissions.Pages_QuotationTemplate_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _quotationTemplateRepqository.Get(input.Id).TemplateName;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 39;
            dataVaultLog.ActionNote = "Html Template Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _quotationTemplateRepqository.DeleteAsync(input.Id);
        }

        public async Task<List<CommonLookupDto>> GetCustomTemplateForTableDropdown(int leadId)
        {
            var OrgId = _leadRepository.GetAll().Where(e => e.Id == leadId).Select(e => e.OrganizationId).FirstOrDefault();

            var WithVicRebate = _jobRepository.GetAll().Where(e => e.LeadId == leadId).Select(e => e.VicRebate).FirstOrDefault();

            var TemplateTypeId = 2;
            if(WithVicRebate == "With Rebate Install")
            {
                TemplateTypeId = 10;
            }

            var filteredResult = _quotationTemplateRepqository.GetAll().Where(e => e.OrganizationUnitId == OrgId && (e.TemplateTypeId == TemplateTypeId));

            var result = from o in filteredResult
                         select new CommonLookupDto()
                         {
                             Id = o.Id,
                             DisplayName = o.TemplateName
                         };

            return await result.ToListAsync();
        }
    }
}
