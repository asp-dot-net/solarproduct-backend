﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using NPOI.SS.Formula.Functions;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.DeclarationForms.Dtos;
using TheSolarProduct.Jobs;
using TheSolarProduct.LeadActivityLogs;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Leads;
using System.Web;
using Abp.Runtime.Security;
using TheSolarProduct.Quotations;
using Abp.Organizations;
using TheSolarProduct.Organizations;
using System.Globalization;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.ApplicationSettings.Dto;
using Abp.Net.Mail;
using System.Net.Mail;
using Abp.Domain.Uow;
using Abp.Notifications;
using TheSolarProduct.Common;
using Abp.Authorization.Users;
using TheSolarProduct.Notifications;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Authorization.Roles;
using Abp.UI;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.JobAcknowledgement.Dtos;
using TheSolarProduct.Quotations.Dtos;

namespace TheSolarProduct.JobAcknowledgement
{
    public class JobAcknowledgementAppService : TheSolarProductAppServiceBase, IJobAcnowledgementAppService
    {
        private readonly IRepository<Quotations.JobAcknowledgement> _jobAcknowledgementRepqository;
        private readonly IRepository<Jobs.Job, int> _jobRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<JobAcknowledgementLinkHistory> _jobAcknowledgementLinkHistory;
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<OrganizationUnit, long> _OrganizationRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendedOrganizationUnitRepository;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IEmailSender _emailSender;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<UserTeam> _userTeamRepository;

        public JobAcknowledgementAppService(IRepository<Quotations.JobAcknowledgement> jobAcknowledgementRepqository
            , IRepository<Jobs.Job, int> lookup_jobRepository
            , IRepository<LeadActivityLog> leadactivityRepository
            , IRepository<User, long> userRepository
            , IRepository<JobAcknowledgementLinkHistory> jobAcknowledgementLinkHistory
            , IRepository<Lead> leadRepository
            , IRepository<OrganizationUnit, long> organizationRepository
            , IRepository<ExtendOrganizationUnit, long> extendedOrganizationUnitRepository
            , IApplicationSettingsAppService applicationSettings
            , IEmailSender emailSender, IUnitOfWorkManager unitOfWorkManager
            , ICommonLookupAppService CommonDocumentSaveRepository
            , IRepository<UserTeam> userTeamRepository
            , IRepository<UserRole, long> userroleRepository
            , IRepository<Role> roleRepository
            , IAppNotifier appNotifier
           )
        {
            _jobAcknowledgementRepqository = jobAcknowledgementRepqository;
            _jobRepository = lookup_jobRepository;
            _leadactivityRepository = leadactivityRepository;
            _userRepository = userRepository;
            _jobAcknowledgementLinkHistory = jobAcknowledgementLinkHistory;
            _leadRepository = leadRepository;
            _OrganizationRepository = organizationRepository;
            _extendedOrganizationUnitRepository = extendedOrganizationUnitRepository;
            _applicationSettings = applicationSettings;
            _emailSender = emailSender;
            _unitOfWorkManager = unitOfWorkManager;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _userTeamRepository = userTeamRepository;
            _userroleRepository = userroleRepository;
            _roleRepository = roleRepository;
            _appNotifier = appNotifier;
        }

        [AbpAuthorize]
        public async Task CreateOrEdit(CreateOrEditAcnoDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        //[AbpAuthorize(AppPermissions.Pages_Quotations_Create, AppPermissions.Pages_Jobs)]
        protected virtual async Task Create(CreateOrEditAcnoDto input)
        {
            var jobAcknowledgement = ObjectMapper.Map<Quotations.JobAcknowledgement>(input);
            var job = _jobRepository.GetAll().Where(e => e.Id == jobAcknowledgement.JobId).FirstOrDefault();
            if (AbpSession.TenantId != null)
            {
                //Prevent tenants to get other tenant's users.
                input.TenantId = AbpSession.TenantId;
            }

            await _jobAcknowledgementRepqository.InsertAndGetIdAsync(jobAcknowledgement);

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 12;
            leadactivity.SectionId = input.SectionId;
            leadactivity.ActionNote = "New " + input.DocType + " Form Cretaed";
            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }



        //[AbpAuthorize(AppPermissions.Pages_Quotations_Edit, AppPermissions.Pages_Jobs)]
        protected virtual async Task Update(CreateOrEditAcnoDto input)
        {
            var jobAcknowledgement = await _jobAcknowledgementRepqository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, jobAcknowledgement);
        }

        [AbpAuthorize(AppPermissions.Pages_LeadDetails_Acknowledgement_Delete)]
        public async Task DeletejobAcknowledgement(EntityDto input, int sectionId)
        {
            var leadId = await _jobAcknowledgementRepqository.GetAll().Where(e => e.Id == input.Id).Select(e => e.JobFk.LeadId).FirstOrDefaultAsync();

            await _jobAcknowledgementRepqository.DeleteAsync(input.Id);
           
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 44;
            leadactivity.SectionId = sectionId;
            leadactivity.ActionNote = "Job Acknowledgement Form Deleted";
            leadactivity.LeadId = Convert.ToInt32(leadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }


        [AbpAuthorize]
        public async Task<PagedResultDto<GetJobAcnoForViewDto>> GetAll(GetAllJobAcnoInput input)
        {
            var filteredJobAcknowledgement = _jobAcknowledgementRepqository.GetAll()
                        .WhereIf(input.JobId > 0, e => false || e.JobId == input.JobId && e.DocType == input.DocType);

            var pagedAndFilteredJobAcknowledgements = filteredJobAcknowledgement
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var jobAcknowledgements = from o in pagedAndFilteredJobAcknowledgements
                                      join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                                      from s3 in j3.DefaultIfEmpty()
                                      select new GetJobAcnoForViewDto()
                                      {
                                          JobAcknowledgement = new JobAcnoDto
                                          {
                                              Id = o.Id,
                                              JobId = o.JobId,
                                              isSigned = o.IsSigned,
                                              CreatedDate = o.CreationTime,
                                              CreatedBy = s3.FullName
                                          }
                                      };

            var totalCount = await filteredJobAcknowledgement.CountAsync();

            return new PagedResultDto<GetJobAcnoForViewDto>(
                totalCount,
                await jobAcknowledgements.ToListAsync()
            );
        }

        [AbpAuthorize]
        public async Task SendAcknowledgement(SendAcknowledgementInputDto input)
        {
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.SectionId = (int)input.SectionId;
            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

            var jobAcknowledgement = _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == input.JobId && e.DocType == input.DocType).OrderByDescending(e => e.Id).FirstOrDefault();

            if (jobAcknowledgement.IsSigned == true)
            {
                throw new UserFriendlyException("Last "+ input.DocType + " Already Signed, Please Generate New "+ input.DocType + ".");
            }

            var job = _jobRepository.GetAll().Where(e => e.Id == jobAcknowledgement.JobId).FirstOrDefault();
            var lead = _leadRepository.GetAll().Where(e => e.Id == job.LeadId).FirstOrDefault();
            var Body = job.TenantId + "," + job.Id + "," + jobAcknowledgement.Id;

            //Token With Tenant Id & Promotion User Primary Id for subscribe & UnSubscribe
            var token = HttpUtility.UrlEncode(SimpleStringCipher.Instance.Encrypt(Body, AppConsts.DefaultPassPhrase));
            var token1 = SimpleStringCipher.Instance.Encrypt(Body, AppConsts.DefaultPassPhrase);

            string BaseURL = System.Configuration.ConfigurationManager.AppSettings["ClientRootAddress"];

            var p = input.DocType == "Export Control" ? "EC" : "FIT";
            Body = "https://thesolarproduct.com/account/ackno-signature?p=" + p + "&STR=" + token;
            
            JobAcknowledgementLinkHistory jobAcknowledgementLinkHistory = new JobAcknowledgementLinkHistory();
            jobAcknowledgementLinkHistory.Expired = false;
            jobAcknowledgementLinkHistory.JobAcknowledgementId = jobAcknowledgement.Id;
            jobAcknowledgementLinkHistory.Token = token1;
            await _jobAcknowledgementLinkHistory.InsertAsync(jobAcknowledgementLinkHistory);

            var LeadAssignUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
            var adress = lead.Address + "," + lead.Suburb + "." + lead.State + " " + lead.PostCode;

            var orgName = _OrganizationRepository.GetAll().Where(e => e.Id == lead.OrganizationId).Select(e => e.DisplayName).FirstOrDefault();
            var FromEmail = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == lead.OrganizationId).Select(e => e.defaultFromAddress).FirstOrDefault();
            var ClickHere = Body;
            var leadassignusername = LeadAssignUser == null ? "" : LeadAssignUser.Name;

            if (input.SendMode == "SMS")
            {
                leadactivity.IsMark = false;
            }
            else
            {
                leadactivity.IsMark = null;
            }

            leadactivity.ActionId = 12;
            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

            //string FinalSMSBody = "Dear " + lead.CompanyName + " Your Solar Advisor " + leadassignusername + " From " + orgName + " PTY LTD is requesting a signature for Project No. " + jobAcknowledgement.JobFk.JobNumber + " On " + input.DocType + " Form No. " + jobAcknowledgement.Id + " For Your property " + adress + " To Upload your signature please <a href=\"" + ClickHere + "\"></a>" + " Regards " + orgName;

            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            adress = textInfo.ToTitleCase(adress.ToLower());

            var OrgLogo = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == lead.OrganizationId).Select(e => (e.LogoFilePath + e.LogoFileName)).FirstOrDefault();
            OrgLogo = ApplicationSettingConsts.ViewDocumentPath + (OrgLogo != null ? OrgLogo.Replace("\\", "/") : "");

            string FinalSMSBody = "Dear " + lead.CompanyName + "\n\nYour Solar Advisor " + leadassignusername + " From " + orgName + " PTY LTD is requesting a signature for Project No. " + jobAcknowledgement.JobFk.JobNumber + " On " + input.DocType + " Form No. " + jobAcknowledgement.Id + " @ Your property " + adress + "." + "\n\nTo Upload your signature please click below link \n" + ClickHere + "\n\nRegards\n" + orgName;

            string FinalBody = "<!doctype html><html lang='en'><head><meta charset='utf-8'><meta name='viewport' content='width=device-width,initial-scale=1,shrink-to-fit=no'><meta name='viewport' content='width=device-width,initial-scale=1,shrink-to-fit=no'><link rel='preconnect' href='https://fonts.googleapis.com'><link rel='preconnect' href='https://fonts.gstatic.com' crossorigin><link href='https://fonts.googleapis.com/css2?family=Roboto&display=swap' rel='stylesheet'><title>Inline CSS Email</title></head><style>@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu72xKOzY.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu5mxKOzY.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7mxKOzY.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu4WxKOzY.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7WxKOzY.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7GxKOzY.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu4mxK.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:900;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fABc4EsA.woff2) format('woff2');unicode-range:U+0301,U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Roboto;font-style:normal;font-weight:300;src:url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fBBc4.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}body{margin:0;padding:0;background:#48afb5;font-family:Roboto}</style><body style='background:#48afb5;margin:0;padding:0'><div style='background:#48afb5;margin:0;padding:20px 0'><div style='margin:10px auto;max-width:600px;min-width:320px'><div style='margin:0 10px;max-width:600px;min-width:320px;padding:0;background-color:#fff;border:1px solid #e2ebf1;border-radius:10px;overflow:hidden'><table style='width:100%;border:0;border-spacing:0;margin:0 auto'><tr><td style='text-align:center;padding:30px 0 20px'><img src='" + OrgLogo + "' style='max-width:200px'></td></tr><tr><td style='text-align:center;padding:30px 0 50px'><img src='https://thesolarproduct.com/assets/common/images/e-template-images/customer-signature.png' style='width:25%'></td></tr><tr><td style='font-size:22px;font-weight:700;font-family:Roboto,sans-serif;color:#000;padding:0 30px;text-align:center;font-weight:900'>Hello " + lead.CompanyName + ",</td></tr><tr><td style='height:30px'></td></tr><tr><td style='text-align:center;font-size:14px;font-weight:400;font-family:Roboto,sans-serif;color:#1a1a1a;padding:0 0'><div style='font-size:.9rem;font-family:Roboto,sans-serif;color:#1a1a1a;margin:0 25px 30px;line-height:28px'>Your Solar Advisor <strong style='border-bottom:1px dotted #000'>" + leadassignusername + "</strong> From <strong style='border-bottom:1px dotted #000'>" + orgName + "</strong> is requesting a signature for Project No. <strong style='border-bottom:1px dotted #000'>" + jobAcknowledgement.JobFk.JobNumber + "</strong> on " + input.DocType + " Form No. <strong style='border-bottom:1px dotted #000'>" + jobAcknowledgement.Id + "</strong> for your property @ <strong style='border-bottom:1px dotted #000'>" + adress + "</strong>.</div><div style='padding:20px 0'><div style='font-size:.9rem;font-weight:400;font-family:Roboto,sans-serif;margin-bottom:30px;line-height:24px'><a href='" + Body + "' style='text-decoration:none;text-decoration:none;background:#48afb5;font-weight:700;color:#fff;letter-spacing:1px;font-size:16px;text-transform:uppercase;border-radius:25px;height:50px;width:320px;display:inline-block;line-height:50px'>Click Here To Sign</a></div><div style='font-size:18px;font-weight:400;font-family:Roboto,sans-serif;color:#000;margin-bottom:20px;line-height:24px;font-weight:700'>OR</div><div style='font-size:14px;font-weight:400;font-family:Roboto,sans-serif;line-height:24px'><a href='" + Body + "' style='text-decoration:none;color:#000;font-weight:900'>" + Body + "</a></div></div><div style='font-size:.9rem;font-weight:400;font-family:Roboto,sans-serif;color:#1a1a1a;margin:30px 25px 25px;line-height:24px'><strong>Note: </strong>This link is valid for <strong>24 hour</strong> from the time it was sent to you and can be used to signed the quote only once.</div><div style='background:#f5f5f5;font-family:Roboto,sans-serif;font-size:12px;color:#666;line-height:22px;border-top:1px solid #e2ebf1;padding:10px 0'>Thanks,<br>" + orgName + "</td></tr></table></div></div></div></body></html>";


            if (input.SendMode == "SMS")
            {
                if (!string.IsNullOrEmpty(lead.Mobile))
                {
                    SendSMSInput sendSMSInput = new SendSMSInput();
                    sendSMSInput.ActivityId = leadactivity.Id;
                    sendSMSInput.PhoneNumber = lead.Mobile; //"+919974242686"; //
                    //sendSMSInput.PhoneNumber = "+919924860723";
                    sendSMSInput.Text = FinalSMSBody;
                    sendSMSInput.SectionID = (int)input.SectionId;
                    await _applicationSettings.SendSMS(sendSMSInput);
                }
                    leadactivity.ActionNote = input.DocType + " Form Signature Request Sent On SMS";
            }
            else if (input.SendMode == "Email")
            {
                var sub = "Requesting Signature on " + input.DocType + " Form.";
                await _emailSender.SendAsync(new MailMessage
                {
                    From = new MailAddress(FromEmail),
                    To = { lead.Email }, //{ "hiral.prajapati@meghtechnologies.com" },
                    Subject = sub,
                    Body = FinalBody,
                    IsBodyHtml = true
                });
                leadactivity.ActionNote = input.DocType + " Form Signature Request Sent On Email";
            }

        }

        [UnitOfWork]
        public async Task<AcknoSignatureRequestDto> AcknoData(string STR)
        {
            AcknoSignatureRequestDto acknoSignatureRequestDto = new AcknoSignatureRequestDto();

            if (STR != null)
            {
                var IDs = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(STR, AppConsts.DefaultPassPhrase));
                var ID = IDs.Split(",");
                int TenantId = Convert.ToInt32(ID[0]);
                int JobId = Convert.ToInt32(ID[1]);
                int DocNo = Convert.ToInt32(ID[2]);

                using (_unitOfWorkManager.Current.SetTenantId(TenantId))
                {
                    var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
                    var JobAcknoDetail = _jobAcknowledgementRepqository.GetAll().Where(e => e.Id == DocNo).FirstOrDefault();
                    var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
                    var Organization = _OrganizationRepository.GetAll().Where(e => e.Id == Lead.OrganizationId).Select(e => e.DisplayName).FirstOrDefault();
                    var OrgLogo = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == Lead.OrganizationId).Select(e => (e.LogoFilePath + e.LogoFileName)).FirstOrDefault();

                    acknoSignatureRequestDto.CustName = JobAcknoDetail.Name;
                    bool IsExpiried = false;
                    bool IsSigned = false;
                    var IsExpired = _jobAcknowledgementLinkHistory.GetAll().Where(e => e.Token == STR && e.JobAcknowledgementId == DocNo).OrderByDescending(e => e.Id).FirstOrDefault();
                    acknoSignatureRequestDto.OrgLogo = OrgLogo != null ? OrgLogo.Replace("\\", "/") : "";

                    if (IsExpired != null)
                    {
                        if (IsExpired.CreationTime.AddHours(24) <= DateTime.UtcNow)
                        {
                            IsExpiried = true;
                        }
                        if (IsExpired.Expired == true)
                        {
                            IsSigned = true;
                        }

                        if (IsExpiried == false && IsSigned == false)
                        {
                            var SalesRep = _userRepository.GetAll().Where(e => e.Id == Lead.AssignToUserID).FirstOrDefault();
                            
                            acknoSignatureRequestDto.Expiry = false;
                            acknoSignatureRequestDto.JobNumber = JobDetail.JobNumber;
                            acknoSignatureRequestDto.Address = JobAcknoDetail.Address;
                            acknoSignatureRequestDto.Email = JobAcknoDetail.Email;
                            acknoSignatureRequestDto.Mobile = JobAcknoDetail.Mobile;
                            acknoSignatureRequestDto.Kw = JobAcknoDetail.Kw.ToString();
                            acknoSignatureRequestDto.Date = DateTime.UtcNow.ToLongDateString();

                            if (SalesRep != null)
                            {
                                acknoSignatureRequestDto.InstName = SalesRep.FullName;
                            }
                            acknoSignatureRequestDto.Organization = Organization;
                            acknoSignatureRequestDto.OrganizationId = Lead.OrganizationId;

                            return acknoSignatureRequestDto;
                        }
                        else
                        {
                            acknoSignatureRequestDto.Expiry = IsExpiried;
                            acknoSignatureRequestDto.Signed = IsSigned;
                            return acknoSignatureRequestDto;
                        }
                    }
                    else
                    {
                        acknoSignatureRequestDto.Expiry = IsExpiried;
                        acknoSignatureRequestDto.Signed = IsSigned;
                        return acknoSignatureRequestDto;
                    }
                }
            }
            else
            {
                acknoSignatureRequestDto.Expiry = true;
                acknoSignatureRequestDto.Signed = false;
                return acknoSignatureRequestDto;
            }
        }

        public async Task SaveSignature(SaveSignature input)
        {
            try
            {
                var FileName = "";
                if (input.Page == "Export Control Form")
                {
                    FileName = DateTime.Now.Ticks + "_ExportControlSignature.png";
                }
                else
                {
                    FileName = DateTime.Now.Ticks + "_FeedInTariffSignature.png";
                }

                var IMage = input.ImageData.Split(new[] { ',' }, 21);
                var buffer = new byte[2097152];
                byte[] ByteArray = Convert.FromBase64String(IMage[1]);
                var IDs = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(input.EncString, AppConsts.DefaultPassPhrase));
                ///var IDs = SimpleStringCipher.Instance.Decrypt(input.EncString, AppConsts.DefaultPassPhrase);
                var ID = IDs.Split(",");
                int TenantId = Convert.ToInt32(ID[0]);
                int JobId = Convert.ToInt32(ID[1]);
                int AcknoId = Convert.ToInt32(ID[2]);

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (_unitOfWorkManager.Current.SetTenantId(TenantId))
                    {
                        var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, FileName, "AcknoSignature", JobId, 0, AbpSession.TenantId);

                        var acknowledgement = _jobAcknowledgementRepqository.GetAll().Where(e => e.Id == AcknoId).OrderByDescending(e => e.Id).FirstOrDefault();
                        acknowledgement.CustSignLatitude = input.CustSignLatitude;
                        acknowledgement.CustSignLongitude = input.CustSignLongitude;
                        acknowledgement.CustSignIP = input.CustSignIP;
                        acknowledgement.CustSignFileName = FileName;
                        acknowledgement.IsSigned = true;
                        acknowledgement.SignDate = DateTime.UtcNow;
                        acknowledgement.YN = input.YesNo;
                        acknowledgement.SignFilePath = filepath.FinalFilePath;
                        await _jobAcknowledgementRepqository.UpdateAsync(acknowledgement);

                        var Link = _jobAcknowledgementLinkHistory.GetAll().Where(e => e.Token == input.EncString && e.JobAcknowledgementId == AcknoId);
                        foreach (var item in Link)
                        {
                            item.Expired = true;
                            await _jobAcknowledgementLinkHistory.UpdateAsync(item);
                        }

                        var Lead = _leadRepository.GetAll().Where(e => e.Id == _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.LeadId).FirstOrDefault()).FirstOrDefault();
                        var User_List = _userRepository.GetAll().ToList();
                        var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == Lead.AssignToUserID).Select(e => e.TeamId).FirstOrDefault();
                        var AssignedUser = _userRepository.GetAll().Where(e => e.Id == Lead.AssignToUserID).FirstOrDefault();

                        string msg = string.Format(input.Page + " Siagnture Received For {0} Job", filepath.JobNumber);
                        await _appNotifier.LeadAssiged(AssignedUser, msg, NotificationSeverity.Info);

                        var UserList = (from user in User_List
                                        join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                        from ur in urJoined.DefaultIfEmpty()

                                        join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                        from us in usJoined.DefaultIfEmpty()

                                        join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                                        from ut in utJoined.DefaultIfEmpty()

                                        where us != null && us.DisplayName == "Sales Manager" && ut != null && ut.TeamId == TeamId
                                        select user);

                        foreach (var item in UserList)
                        {
                            await _appNotifier.LeadAssiged(item, msg, NotificationSeverity.Info);
                        }
                        uow.Complete();
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
