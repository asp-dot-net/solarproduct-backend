﻿using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.GreenBoatDatatable;
using TheSolarProduct.Leads;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.Notifications;
using Abp.Timing.Timezone;
using TheSolarProduct.Organizations;
using TheSolarProduct.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.PVDStatuses;
using TheSolarProduct.Jobs;
using Microsoft.AspNetCore.Mvc;
using NPOI.SS.Formula.Functions;
using System.Text.RegularExpressions;
using TheSolarProduct.LeadActivityLogs;
using Abp.Notifications;
using TheSolarProduct.Features;
using Abp.Application.Features;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using System.Security.Cryptography;
using System.Security.Policy;
using Microsoft.AspNetCore.Http;
using System.IO;
using NUglify.Helpers;
using TheSolarProduct.ThirdPartyApis.GreenDeals.Dtos;
using TheSolarProduct.ThirdPartyApis.GreenDeal;
using static Google.Apis.Requests.BatchRequest;
using TheSolarProduct.ThirdPartyApis.REC.Dtos;
using TheSolarProduct.ThirdPartyApis.RECData;
using Abp.EntityFrameworkCore.EFPlus;
using Telerik.Reporting;
using Z.EntityFramework.Extensions;
using TheSolarProduct.ThirdPartyApis.Cimet.Dto;
using TheSolarProduct.ThirdPartyApis.Credit;
using TheSolarProduct.RECDatas;

namespace TheSolarProduct.SysJobs
{
    public class SysJobsAppService : TheSolarProductAppServiceBase, ISysJobsAppService
    {
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IUserEmailer _userEmailer;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendOrganizationUnitRepository;
        private readonly IRepository<GreenBoatData> _greenBoatDataRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<PVDStatus, int> _pvdStatusRepository;
        private readonly IRepository<PostCodeRange.PostCodeRange> _postCodeRangeRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<STCProvider> _stcProviderRepository;
        private readonly IRepository<GreenDealJob> _greenDealJobRepository;
        private readonly IRepository<RECDetail> _recDetailRepository;
        private readonly IRepository<CimetDetail> _cimetDetailRepository;

        public SysJobsAppService(IRepository<Lead> leadRepository
            , IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository
            , IRepository<User, long> userRepository
            , IRepository<UserRole, long> userroleRepository
            , IRepository<Role> roleRepository
            , IAppNotifier appNotifier
            , IUserEmailer userEmailer
            , IApplicationSettingsAppService applicationSettings
            , IRepository<Tenant> tenantRepository
            , IUnitOfWorkManager unitOfWorkManager
            , ITimeZoneConverter timeZoneConverter
            , IRepository<ExtendOrganizationUnit, long> extendOrganizationUnitRepository
            , IRepository<GreenBoatData> greenBoatDataRepository
            , IRepository<Job> jobRepository
            , IRepository<PVDStatus, int> pvdStatusRepository
            , IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            , IRepository<PostCodeRange.PostCodeRange> postCodeRangeRepository
            , IRepository<LeadActivityLog> leadactivityRepository
            , IRepository<STCProvider> stcProviderRepository,
            IRepository<GreenDealJob> greenDealJobRepository,
            IRepository<RECDetail> recDetailRepository,
            IRepository<CimetDetail> cimetDetailRepository)
        {
            _leadRepository = leadRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _userRepository = userRepository;
            _userroleRepository = userroleRepository;
            _roleRepository = roleRepository;
            _appNotifier = appNotifier;
            _userEmailer = userEmailer;
            _applicationSettings = applicationSettings;
            _tenantRepository = tenantRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _timeZoneConverter = timeZoneConverter;
            _extendOrganizationUnitRepository = extendOrganizationUnitRepository;
            _greenBoatDataRepository = greenBoatDataRepository;
            _jobRepository = jobRepository;
            _dbcontextprovider = dbcontextprovider;
            _pvdStatusRepository = pvdStatusRepository;
            _postCodeRangeRepository = postCodeRangeRepository;
            _leadactivityRepository = leadactivityRepository;
            _stcProviderRepository = stcProviderRepository;
            _greenDealJobRepository = greenDealJobRepository;
            _recDetailRepository = recDetailRepository;
            _cimetDetailRepository = cimetDetailRepository;
        }

        public async Task GetGreenbotDetails()
        {
            try
            {
                using (var uow = _unitOfWorkManager.Begin())
                {
                    var tenants = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();

                    foreach (var tenant in tenants)
                    {
                        if (tenant.IsActive == true && tenant.Id != 1)
                        {
                            using (_unitOfWorkManager.Current.SetTenantId(tenant.Id))
                            {
                                await _dbcontextprovider.GetDbContext().Database.ExecuteSqlRawAsync("TRUNCATE TABLE [GreenBoatDatas]");

                                var extOrgDetail = _extendOrganizationUnitRepository.GetAll().ToList();

                                foreach (var org in extOrgDetail)
                                {
                                    List<GreenBoatData> gbDataList = new List<GreenBoatData>();
                                    if (!string.IsNullOrEmpty(org.GreenBoatUsernameForFetch) && !string.IsNullOrEmpty(org.GreenBoatPasswordForFetch))
                                    {
                                        #region Greenbot Login
                                        var client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/Login");
                                        var request = new RestRequest(Method.POST);
                                        request.AddParameter("Username", org.GreenBoatUsernameForFetch);
                                        request.AddParameter("Password", org.GreenBoatPasswordForFetch);
                                        var response = client.Execute(request);
                                        #endregion

                                        Job_Response.RootObject JobDetails = new Job_Response.RootObject();
                                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                        {
                                            Job_Response.RootObject RootObject = JsonConvert.DeserializeObject<Job_Response.RootObject>(response.Content);
                                            if (RootObject.StatusCode == System.Net.HttpStatusCode.OK.ToString())
                                            {
                                                string AccessToken = RootObject.TokenData.access_token; // get Login Access Token

                                                //string sDate = "01/01/2021"; // Data Fetch Start Date
                                                //var date = (_timeZoneConverter.Convert(DateTime.UtcNow, tenant.Id).ToString());
                                                //string eDate = date != null ? Convert.ToDateTime(date).Date.ToString("dd/MM/yyyy") : DateTime.UtcNow.Date.ToString("dd/MM/yyyy"); // Data Fetch End Date

                                                var startYear = Convert.ToDateTime("01/01/2021").Year;
                                                var endYear = DateTime.UtcNow.Year;

                                                for (int i = startYear; i <= endYear; i++)
                                                {
                                                    for (int k = 0; k < 2; k++)
                                                    {
                                                        string sDate = "";
                                                        string eDate = "";

                                                        if (k == 0)
                                                        {
                                                            sDate = ("01/01/" + i);
                                                            eDate = ("30/06/" + i);
                                                        }
                                                        else if (k == 1)
                                                        {
                                                            sDate = ("01/07/" + i);
                                                            eDate = ("31/12/" + i);
                                                        }

                                                        client = new RestClient("https://api.greenbot.com.au/api/Account/GetJobs?FromDate=" + sDate + "&ToDate=" + eDate);
                                                        request = new RestRequest(Method.GET);
                                                        client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", AccessToken));
                                                        client.AddDefaultHeader("Content-Type", "application/json");
                                                        client.AddDefaultHeader("Accept", "application/json");

                                                        IRestResponse<Job_Response.RootObject> JsonData = client.Execute<Job_Response.RootObject>(request);
                                                        if (JsonData.StatusCode == System.Net.HttpStatusCode.OK)
                                                        {
                                                            JobDetails = JsonConvert.DeserializeObject<Job_Response.RootObject>(JsonData.Content);
                                                            if (JobDetails.StatusCode == System.Net.HttpStatusCode.OK.ToString())
                                                            {
                                                                if (JobDetails.lstJobData.Count > 0)
                                                                {
                                                                    foreach (var jobItem in JobDetails.lstJobData)
                                                                    {
                                                                        try
                                                                        {
                                                                            GreenBoatData gbData = new GreenBoatData();
                                                                            gbData.TenantId = tenant.Id;
                                                                            gbData.OrganizationId = (int?)org.Id;
                                                                            gbData.IsDeleted = false;
                                                                            gbData.CreationTime = DateTime.UtcNow;

                                                                            gbData.ProjectNo = jobItem.BasicDetails.JobID;
                                                                            gbData.VendorJobId = jobItem.BasicDetails.VendorJobId;
                                                                            gbData.JobID = jobItem.BasicDetails.JobID;
                                                                            gbData.Title = jobItem.BasicDetails.Title;
                                                                            gbData.RefNumber = jobItem.BasicDetails.RefNumber.Trim();
                                                                            gbData.Description = jobItem.BasicDetails.Description;
                                                                            gbData.JobType = jobItem.BasicDetails.JobType;
                                                                            gbData.JobStage = jobItem.BasicDetails.JobStage;
                                                                            gbData.Priority = jobItem.BasicDetails.Priority;
                                                                            gbData.InstallationDate = jobItem.BasicDetails.InstallationDate;
                                                                            gbData.CreatedDate = jobItem.BasicDetails.CreatedDate;

                                                                            gbData.OwnerType_JOD = jobItem.JobOwnerDetails.OwnerType;
                                                                            gbData.CompanyName_JOD = jobItem.JobOwnerDetails.CompanyName;
                                                                            gbData.FirstName_JOD = jobItem.JobOwnerDetails.FirstName;
                                                                            gbData.LastName_JOD = jobItem.JobOwnerDetails.LastName;
                                                                            gbData.Phone_JOD = jobItem.JobOwnerDetails.Phone;
                                                                            gbData.Mobile_JOD = jobItem.JobOwnerDetails.Mobile;
                                                                            gbData.Email_JOD = jobItem.JobOwnerDetails.Email;
                                                                            gbData.IsPostalAddress_JOD = jobItem.JobOwnerDetails.IsPostalAddress;
                                                                            gbData.UnitTypeID_JOD = jobItem.JobOwnerDetails.UnitTypeID;
                                                                            gbData.UnitNumber_JOD = jobItem.JobOwnerDetails.UnitNumber;
                                                                            gbData.StreetNumber_JOD = jobItem.JobOwnerDetails.StreetNumber;
                                                                            gbData.StreetName_JOD = jobItem.JobOwnerDetails.StreetName;
                                                                            gbData.StreetTypeID_JOD = jobItem.JobOwnerDetails.StreetTypeID;
                                                                            gbData.Town_JOD = jobItem.JobOwnerDetails.Town;
                                                                            gbData.State_JOD = jobItem.JobOwnerDetails.State;
                                                                            gbData.PostCode_JOD = jobItem.JobOwnerDetails.PostCode;

                                                                            gbData.DistributorID_JID = jobItem.JobInstallationDetails.DistributorID;
                                                                            gbData.MeterNumber_JID = jobItem.JobInstallationDetails.MeterNumber;
                                                                            gbData.PhaseProperty_JID = jobItem.JobInstallationDetails.PhaseProperty;
                                                                            gbData.ElectricityProviderID_JID = jobItem.JobInstallationDetails.ElectricityProviderID;
                                                                            gbData.IsSameAsOwnerAddress_JID = jobItem.JobInstallationDetails.IsSameAsOwnerAddress;
                                                                            gbData.IsPostalAddress_JID = jobItem.JobInstallationDetails.IsPostalAddress;
                                                                            gbData.UnitTypeID_JID = jobItem.JobInstallationDetails.UnitTypeID;
                                                                            gbData.UnitNumber_JID = jobItem.JobInstallationDetails.UnitNumber;
                                                                            gbData.StreetNumber_JID = jobItem.JobInstallationDetails.StreetNumber;
                                                                            gbData.StreetName_JID = jobItem.JobInstallationDetails.StreetName;
                                                                            gbData.StreetTypeID_JID = jobItem.JobInstallationDetails.StreetTypeID;
                                                                            gbData.Town_JID = jobItem.JobInstallationDetails.Town;
                                                                            gbData.State_JID = jobItem.JobInstallationDetails.State;
                                                                            gbData.PostCode_JID = jobItem.JobInstallationDetails.PostCode;
                                                                            gbData.NMI_JID = jobItem.JobInstallationDetails.NMI;
                                                                            gbData.SystemSize_JID = jobItem.JobInstallationDetails.SystemSize;
                                                                            gbData.PropertyType_JID = jobItem.JobInstallationDetails.PropertyType;
                                                                            gbData.SingleMultipleStory_JID = jobItem.JobInstallationDetails.SingleMultipleStory;
                                                                            gbData.InstallingNewPanel_JID = jobItem.JobInstallationDetails.InstallingNewPanel;
                                                                            gbData.Location_JID = jobItem.JobInstallationDetails.Location;
                                                                            gbData.ExistingSystem_JID = jobItem.JobInstallationDetails.ExistingSystem;
                                                                            gbData.ExistingSystemSize_JID = jobItem.JobInstallationDetails.ExistingSystemSize;
                                                                            gbData.SystemLocation_JID = jobItem.JobInstallationDetails.SystemLocation;
                                                                            gbData.NoOfPanels_JID = jobItem.JobInstallationDetails.NoOfPanels;
                                                                            gbData.AdditionalInstallationInformation_JID = jobItem.JobInstallationDetails.AdditionalInstallationInformation;

                                                                            gbData.SystemSize_JSD = jobItem.JobSystemDetails.SystemSize;
                                                                            gbData.SerialNumbers_JSD = jobItem.JobSystemDetails.SerialNumbers;
                                                                            gbData.NoOfPanel_JSD = jobItem.JobSystemDetails.NoOfPanel;
                                                                            gbData.AdditionalCapacityNotes_JSTC = jobItem.JobSTCDetails.AdditionalCapacityNotes;
                                                                            gbData.TypeOfConnection_JSTC = jobItem.JobSTCDetails.TypeOfConnection;
                                                                            gbData.SystemMountingType_JSTC = jobItem.JobSTCDetails.SystemMountingType;
                                                                            gbData.DeemingPeriod_JSTC = jobItem.JobSTCDetails.DeemingPeriod;
                                                                            gbData.CertificateCreated_JSTC = jobItem.JobSTCDetails.CertificateCreated;
                                                                            gbData.FailedAccreditationCode_JSTC = jobItem.JobSTCDetails.FailedAccreditationCode;
                                                                            gbData.FailedReason_JSTC = jobItem.JobSTCDetails.FailedReason;
                                                                            gbData.MultipleSGUAddress_JSTC = jobItem.JobSTCDetails.MultipleSGUAddress;
                                                                            gbData.Location_JSTC = jobItem.JobSTCDetails.Location;
                                                                            gbData.AdditionalLocationInformation_JSTC = jobItem.JobSTCDetails.AdditionalLocationInformation;
                                                                            gbData.AdditionalSystemInformation_JSTC = jobItem.JobSTCDetails.AdditionalSystemInformation;

                                                                            gbData.FirstName_IV = jobItem.InstallerView.FirstName;
                                                                            gbData.LastName_IV = jobItem.InstallerView.LastName;
                                                                            gbData.Email_IV = jobItem.InstallerView.Email;
                                                                            gbData.Phone_IV = jobItem.InstallerView.Phone;
                                                                            gbData.Mobile_IV = jobItem.InstallerView.Mobile;
                                                                            gbData.CECAccreditationNumber_IV = jobItem.InstallerView.CECAccreditationNumber;
                                                                            gbData.ElectricalContractorsLicenseNumber_IV = jobItem.InstallerView.ElectricalContractorsLicenseNumber;
                                                                            gbData.IsPostalAddress_IV = jobItem.InstallerView.IsPostalAddress;
                                                                            gbData.UnitTypeID_IV = jobItem.InstallerView.UnitTypeID;
                                                                            gbData.UnitNumber_IV = jobItem.InstallerView.UnitNumber;
                                                                            gbData.StreetNumber_IV = jobItem.InstallerView.StreetNumber;
                                                                            gbData.StreetName_IV = jobItem.InstallerView.StreetName;
                                                                            gbData.StreetTypeID_IV = jobItem.InstallerView.StreetTypeID;
                                                                            gbData.Town_IV = jobItem.InstallerView.Town;
                                                                            gbData.State_IV = jobItem.InstallerView.State;
                                                                            gbData.PostCode_IV = jobItem.InstallerView.PostCode;


                                                                            gbData.FirstName_DV = jobItem.DesignerView.FirstName;
                                                                            gbData.LastName_DV = jobItem.DesignerView.LastName;
                                                                            gbData.Email_DV = jobItem.DesignerView.Email;
                                                                            gbData.Phone_DV = jobItem.DesignerView.Phone;
                                                                            gbData.Mobile_DV = jobItem.DesignerView.Mobile;
                                                                            gbData.CECAccreditationNumber_DV = jobItem.DesignerView.CECAccreditationNumber;
                                                                            gbData.ElectricalContractorsLicenseNumber_DV = jobItem.DesignerView.ElectricalContractorsLicenseNumber;
                                                                            gbData.IsPostalAddress_DV = jobItem.DesignerView.IsPostalAddress;
                                                                            gbData.UnitTypeID_DV = jobItem.DesignerView.UnitTypeID;
                                                                            gbData.UnitNumber_DV = jobItem.DesignerView.UnitNumber;
                                                                            gbData.StreetNumber_DV = jobItem.DesignerView.StreetNumber;
                                                                            gbData.StreetName_DV = jobItem.DesignerView.StreetName;
                                                                            gbData.StreetTypeID_DV = jobItem.DesignerView.StreetTypeID;
                                                                            gbData.Town_DV = jobItem.DesignerView.Town;
                                                                            gbData.State_DV = jobItem.DesignerView.State;
                                                                            gbData.PostCode_DV = jobItem.DesignerView.PostCode;

                                                                            gbData.CompanyName_JE = jobItem.JobElectricians.CompanyName;
                                                                            gbData.FirstName_JE = jobItem.JobElectricians.FirstName;
                                                                            gbData.LastName_JE = jobItem.JobElectricians.LastName;
                                                                            gbData.Email_JE = jobItem.JobElectricians.Email;
                                                                            gbData.Phone_JE = jobItem.JobElectricians.Phone;
                                                                            gbData.Mobile_JE = jobItem.JobElectricians.Mobile;
                                                                            gbData.LicenseNumber_JE = jobItem.JobElectricians.LicenseNumber;
                                                                            gbData.IsPostalAddress_JE = jobItem.JobElectricians.IsPostalAddress;
                                                                            gbData.UnitTypeID_JE = jobItem.JobElectricians.UnitTypeID;
                                                                            gbData.UnitNumber_JE = jobItem.JobElectricians.UnitNumber;
                                                                            gbData.StreetNumber_JE = jobItem.JobElectricians.StreetNumber;
                                                                            gbData.StreetName_JE = jobItem.JobElectricians.StreetName;
                                                                            gbData.StreetTypeID_JE = jobItem.JobElectricians.StreetTypeID;
                                                                            gbData.Town_JE = jobItem.JobElectricians.Town;
                                                                            gbData.State_JE = jobItem.JobElectricians.State;
                                                                            gbData.PostCode_JE = jobItem.JobElectricians.PostCode;

                                                                            //gbData.Brand_LstJID = jobItem.lstJobInverterDetails[0].Brand;
                                                                            //gbData.Model_LstJID = jobItem.lstJobInverterDetails[0].Model;
                                                                            //gbData.Series_LstJID = jobItem.lstJobInverterDetails[0].Series;
                                                                            //gbData.NoOfInverter_LstJID = jobItem.lstJobInverterDetails[0].NoOfInverter;

                                                                            //gbData.Brand_LstJPD = jobItem.lstJobPanelDetails[0].Brand;
                                                                            //gbData.Model_LstJPD = jobItem.lstJobPanelDetails[0].Model;

                                                                            //gbData.NoOfPanel_LstJPD = jobItem.lstJobPanelDetails[0].NoOfPanel;

                                                                            gbData.STCStatus_JSTCD = jobItem.JobSTCStatusData.STCStatus;
                                                                            gbData.CalculatedSTC_JSTCD = jobItem.JobSTCStatusData.CalculatedSTC;
                                                                            gbData.STCPrice_JSTCD = jobItem.JobSTCStatusData.STCPrice;
                                                                            gbData.FailureNotice_JSTCD = jobItem.JobSTCStatusData.FailureNotice;
                                                                            gbData.ComplianceNotes_JSTCD = jobItem.JobSTCStatusData.ComplianceNotes;
                                                                            gbData.STCSubmissionDate_JSTCD = jobItem.JobSTCStatusData.STCSubmissionDate;
                                                                            gbData.STCInvoiceStatus_JSTCD = jobItem.JobSTCStatusData.STCInvoiceStatus;
                                                                            gbData.IsInvoiced_JSTCD = jobItem.JobSTCStatusData.IsInvoiced;
                                                                            gbData.FieldName_LstCD = jobItem.JobSTCStatusData.STCStatus;
                                                                            gbData.FieldValue_LstCD = jobItem.JobSTCStatusData.STCStatus;
                                                                            gbData.UpdatedOn = DateTime.UtcNow;
                                                                            gbData.IsGst = false;

                                                                            gbDataList.Add(gbData);
                                                                            //var id = await _greenBoatDataRepository.InsertAndGetIdAsync(gbData);

                                                                            //if (detaildt.JobSystemDetails.SerialNumbers != "" && detaildt.JobSystemDetails.SerialNumbers != null)
                                                                            //{
                                                                            //    List<string> idList = detaildt.JobSystemDetails.SerialNumbers.Split(new[] { "\r\n" }, StringSplitOptions.None).ToList();
                                                                            //    if (idList.Count > 0)
                                                                            //    {
                                                                            //        GreenBoatSerialNumber obj = new GreenBoatSerialNumber();
                                                                            //        for (int k = 0; k < idList.Count; k++)
                                                                            //        {
                                                                            //            obj.GreenBoatId = id;
                                                                            //            obj.SerialNumbers = idList[k];
                                                                            //            _GreenBoatSerialNumberRepository.Insert(obj);
                                                                            //        }
                                                                            //    }
                                                                            //}
                                                                        }
                                                                        catch
                                                                        {

                                                                        }
                                                                    }

                                                                    //await _dbcontextprovider.GetDbContext().GreenBoatDatas.AddRangeAsync(gbDataList);
                                                                    //await _dbcontextprovider.GetDbContext().SaveChangesAsync();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                await _dbcontextprovider.GetDbContext().GreenBoatDatas.AddRangeAsync(gbDataList);
                                                await _dbcontextprovider.GetDbContext().SaveChangesAsync();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    uow.Complete();
                }
            }
            catch (Exception e) { throw e; }
        }

        public async Task UpdateGreenbotDetails()
        {
            try
            {
                using (var uow = _unitOfWorkManager.Begin())
                {
                    var tenants = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();

                    foreach (var tenant in tenants)
                    {
                        if (tenant.IsActive == true && tenant.Id != 1)
                        {
                            using (_unitOfWorkManager.Current.SetTenantId(tenant.Id))
                            {
                                var extOrgDetail = _extendOrganizationUnitRepository.GetAll().ToList();
                                var pvdstatus = _pvdStatusRepository.GetAll();

                                foreach (var org in extOrgDetail)
                                {
                                    var stcJobList = _jobRepository.GetAll().Where(e => e.JobStatusId >= 4 && e.PVDStatus != 4 & e.InstallationDate != null && e.LeadFk.OrganizationId == org.Id).ToList();
                                    foreach (var item in stcJobList)
                                    {
                                        string JobNumber = item.JobNumber.Substring(2, item.JobNumber.Length - 2);
                                        var gbData = _greenBoatDataRepository.GetAll().Where(e => e.RefNumber == item.JobNumber || e.RefNumber == JobNumber).FirstOrDefault();
                                        if (gbData != null)
                                        {
                                            if (gbData.STCSubmissionDate_JSTCD != "" && gbData.STCSubmissionDate_JSTCD != null)
                                            {
                                                item.STCAppliedDate = Convert.ToDateTime(gbData.STCSubmissionDate_JSTCD);
                                            }
                                            if (gbData.STCStatus_JSTCD != "" && gbData.STCStatus_JSTCD != null)
                                            {
                                                item.PVDStatus = pvdstatus.Where(e => e.Name == gbData.STCStatus_JSTCD).Select(e => e.Id).FirstOrDefault();
                                            }
                                            item.PVDNumber = gbData.FailedAccreditationCode_JSTC;

                                            if (gbData.CalculatedSTC_JSTCD != null && gbData.CalculatedSTC_JSTCD != "")
                                            {
                                                decimal n = 0;
                                                if (decimal.TryParse(gbData.CalculatedSTC_JSTCD, out n))
                                                {
                                                    item.GreenbotSTC = Convert.ToDecimal(gbData.CalculatedSTC_JSTCD);
                                                }
                                            }
                                            //await _jobRepository.UpdateAsync(item);
                                        }

                                        if (!string.IsNullOrEmpty(item.GWTId))
                                        {
                                            var gdData = _greenDealJobRepository.GetAll().Where(e => e.GwtId == item.GWTId).FirstOrDefault();
                                            if (gdData != null)
                                            {
                                                if (gdData.SubmitDate != null)
                                                {
                                                    item.STCAppliedDate = Convert.ToDateTime(gdData.SubmitDate);
                                                }

                                                if (!string.IsNullOrEmpty(gdData.Status))
                                                {
                                                    if (gdData.Status == "Submitted")
                                                    {
                                                        item.PVDStatus = 1;
                                                    }
                                                    else if (gdData.Status == "Rejected")
                                                    {
                                                        item.PVDStatus = 2;
                                                    }
                                                    else if (gdData.Status == "Approved")
                                                    {
                                                        item.PVDStatus = 4;
                                                    }
                                                }

                                                item.PVDNumber = gdData.PvdNumber;

                                                if (gdData.RecQuantity != null)
                                                {
                                                    item.GreenbotSTC = gdData.RecQuantity;
                                                }
                                                //await _jobRepository.UpdateAsync(item);
                                            }
                                        }

                                        await _jobRepository.UpdateAsync(item);
                                    }
                                }
                            }
                        }
                    }
                    uow.Complete();
                }
            }
            catch (Exception e) { throw e; }
        }

        public async Task UpdateFbLeads()
        {
            try
            {
                using (var uow = _unitOfWorkManager.Begin())
                {
                    var tenants = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();

                    foreach (var tenant in tenants)
                    {
                        if (tenant.IsActive == true && tenant.Id != 1)
                        {
                            using (_unitOfWorkManager.Current.SetTenantId(tenant.Id))
                            {
                                var fbLeadSource = new List<int?> { 4, 18, 24, 26 };

                                var fbLeads = _leadRepository.GetAll().Where(e => e.LeadStatusId == 1 && e.AssignToUserID == null && e.HideDublicate == null && e.IsWebDuplicate == null && e.IsDuplicate == null && fbLeadSource.Contains(e.LeadSourceId)).ToList();

                                if (fbLeads != null)
                                {
                                    var IdList = fbLeads.Select(e => e.Id).ToList();
                                    foreach (var item in fbLeads)
                                    {
                                        //Removing Special Char from Mobile Number
                                        item.Mobile = Regex.Replace(item.Mobile, "[^0-9]", "");

                                        if (item.Mobile.Length < 10 && item.Mobile.Length != 0)
                                        {
                                            item.Mobile = item.Mobile.ToString().PadLeft(10, '0');
                                        }
                                        else if (item.Mobile.Length > 9 && item.Mobile.Length != 0)
                                        {
                                            var m = item.Mobile.Substring(item.Mobile.Length - 9);
                                            item.Mobile = m.ToString().PadLeft(10, '0');
                                        }
                                        else
                                        {
                                            item.Mobile = item.Mobile;
                                        }

                                        //Start Check Duplicate
                                        //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
                                        var dbDup = _leadRepository.GetAll().Where(e => !IdList.Contains(e.Id) && e.OrganizationId == item.OrganizationId && e.AssignToUserID != null && ((e.Email == item.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == item.Mobile && !string.IsNullOrEmpty(e.Mobile)))).FirstOrDefault();
                                        if (dbDup != null)
                                        {
                                            var dbDupLeads = await _leadRepository.GetAll().Where(e => e.OrganizationId == item.OrganizationId && e.AssignToUserID != null && ((e.Email == item.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == item.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Select(e => e.Id).ToListAsync();

                                            var dbDupLeadCount = dbDupLeads.Count();
                                            var dbDupLeadInstallCount = await _jobRepository.GetAll().CountAsync(e => e.JobStatusId == 8 && dbDupLeads.Contains((int)e.LeadId));

                                            if (dbDupLeadCount != dbDupLeadInstallCount)
                                            {
                                                item.IsDuplicate = true;
                                                item.DublicateLeadId = dbDup.Id;
                                            }
                                            else
                                            {
                                                item.IsDuplicate = false;
                                            }
                                        }
                                        else
                                        {
                                            item.IsDuplicate = false;
                                        }

                                        //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                                        var webDup = _leadRepository.GetAll().Where(e => !IdList.Contains(e.Id) && e.OrganizationId == item.OrganizationId && e.AssignToUserID == null && e.HideDublicate != true && ((e.Email == item.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == item.Mobile && !string.IsNullOrEmpty(e.Mobile)))).Any();
                                        if (webDup)
                                        {
                                            item.IsWebDuplicate = true;
                                        }
                                        else
                                        {
                                            item.IsWebDuplicate = false;
                                        }
                                        //End Check Duplicate

                                        await _leadRepository.UpdateAsync(item);
                                        IdList.Remove(item.Id);
                                    }
                                }
                            }
                        }
                    }
                    uow.Complete();
                }
            }
            catch (Exception e) { throw e; }
        }

        public async Task AutoAssignLeads()
        {
            try
            {
                using (var uow = _unitOfWorkManager.Begin())
                {
                    var tenants = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();

                    foreach (var tenant in tenants)
                    {
                        if (tenant.IsActive == true && tenant.Id != 1)
                        {
                            using (_unitOfWorkManager.Current.SetTenantId(tenant.Id))
                            {
                                if (await FeatureChecker.IsEnabledAsync(tenant.Id, AppFeatures.AutoAssignLeads))// Checking Features for Auto Assign lead for tenant
                                {
                                    var extOrgDetail = _extendOrganizationUnitRepository.GetAll().ToList();

                                    foreach (var org in extOrgDetail)
                                    {
                                        if (org.AutoAssignLeadUserId != null && org.EnableAutoAssignLead == true) // Check Auto Assign User is Selected or not in Organization
                                        {
                                            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == org.AutoAssignLeadUserId).FirstOrDefault();

                                            // Get all assign leads
                                            var leads = _leadRepository.GetAll().Where(e => e.OrganizationId == org.Id && e.AssignToUserID == null && e.HideDublicate != true && e.IsFakeLead != true && e.IsDuplicate == false && e.IsWebDuplicate == false).ToList();

                                            // Assign Leads
                                            foreach (var lead in leads)
                                            {
                                                lead.LeadStatusId = 10; // for Assign
                                                lead.AssignToUserID = org.AutoAssignLeadUserId;
                                                lead.LeadAssignDate = DateTime.UtcNow;
                                                lead.AssignDate = DateTime.UtcNow;

                                                if (!string.IsNullOrEmpty(lead.PostCode))
                                                {
                                                    var postCode = Convert.ToInt64(lead.PostCode);
                                                    var area = await _postCodeRangeRepository.GetAll().Where(e => e.StartPostCode <= postCode && e.EndPostCode >= postCode).AsNoTracking().Select(e => e.Area).FirstOrDefaultAsync();

                                                    if (!string.IsNullOrEmpty(area))
                                                    {
                                                        lead.Area = area;
                                                    }
                                                }

                                                await _leadRepository.UpdateAsync(lead); //Update Leads Table

                                                LeadActivityLog leadactivity = new LeadActivityLog();
                                                leadactivity.ActionId = 3;
                                                leadactivity.SectionId = 0; //31 for Manage Lead
                                                leadactivity.ActionNote = "Auto assign lead to Sales Manager, " + assignedToUser.FullName;

                                                leadactivity.LeadId = lead.Id;
                                                leadactivity.TenantId = tenant.Id;
                                                leadactivity.CreatorUserId = AbpSession.UserId;
                                                await _leadactivityRepository.InsertAsync(leadactivity); //Add Activity Log for table
                                            }

                                            var totalAssignLead = leads.Count(); //get total lead count
                                            if (totalAssignLead > 0)
                                            {
                                                string msg = string.Format("{0} Lead Assigned To You", totalAssignLead);
                                                await _appNotifier.LeadAssiged(assignedToUser, msg, NotificationSeverity.Info);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    uow.Complete();
                }
            }
            catch (Exception e) { throw e; }
        }

        public async Task GetGreenDealJobInformation()
        {
            try
            {
                using (var uow = _unitOfWorkManager.Begin())
                {
                    var tenants = _tenantRepository.GetAll()
                        //.Where(e => e.Id == 6)
                        .OrderByDescending(p => p.Id).ToList();

                    foreach (var tenant in tenants)
                    {
                        if (tenant.IsActive == true && tenant.Id != 1)
                        {
                            using (_unitOfWorkManager.Current.SetTenantId(tenant.Id))
                            {
                                var extOrgDetail = _stcProviderRepository.GetAll().Where(e => e.Provider == "Green Deal").ToList();

                                List<GreenDealJob> greenDealJobList = new List<GreenDealJob>();
                                List<GreenDealJobSerialNo> greenDealJobSerialNoList = new List<GreenDealJobSerialNo>();

                                foreach (var org in extOrgDetail)
                                {
                                    if (!string.IsNullOrEmpty(org.UserId) && !string.IsNullOrEmpty(org.Password))
                                    {
                                        var gwtIds = _jobRepository.GetAll().Where(e => e.LeadFk.OrganizationId == org.OrganizationUnitId && !string.IsNullOrEmpty(e.GWTId)).Select(e => e.GWTId).ToList();

                                        //var gwtIds = new List<string>() { "GWT701219" };

                                        //var totalPages = 1;

                                        //for (int i = 1; i <= totalPages; i++)
                                        //{
                                        //    var temp = new
                                        //    {
                                        //        client_id = org.UserId,
                                        //        signature = encryptedSignature,
                                        //        timestamp = timeStamp,
                                        //        per_page = 100,
                                        //        page = i
                                        //    };

                                        //    var content = JsonConvert.SerializeObject(temp, Formatting.Indented);
                                        //    var jsonResponse = await GetAsync("", content);

                                        //    var jsonString = await jsonResponse.Content.ReadAsStringAsync();
                                        //}

                                        foreach (var gwtId in gwtIds)
                                        {
                                            var timeStamp = GetTimeStamp();
                                            var encryptedSignature = GetEncryptedSignature(timeStamp, org.UserId, org.Password);

                                            var temp = new
                                            {
                                                client_id = org.UserId,
                                                signature = encryptedSignature,
                                                timestamp = timeStamp,
                                                id = gwtId
                                            };

                                            var content = JsonConvert.SerializeObject(temp, Formatting.Indented);
                                            var jsonResponse = await GetAsync(gwtId, content);

                                            if (jsonResponse.IsSuccessStatusCode)
                                            {
                                                var creationTime = DateTime.UtcNow;
                                                var creatorUserId = 1;

                                                var jsonString = await jsonResponse.Content.ReadAsStringAsync();
                                                var jobsInfoRootObject = JsonConvert.DeserializeObject<JobInfoRootObject>(jsonString);

                                                var jobDetails = jobsInfoRootObject.Response.Body;

                                                GreenDealJob greenDealJob = new GreenDealJob();
                                                greenDealJob.CreationTime = creationTime;
                                                greenDealJob.CreatorUserId = creatorUserId;
                                                greenDealJob.IsDeleted = false;
                                                greenDealJob.OrganizationId = (long)org.OrganizationUnitId;

                                                greenDealJob.GwtId = jobDetails.GwtId;
                                                greenDealJob.PvdNumber = jobDetails.PvdNumber;
                                                greenDealJob.TradeMode = jobDetails.TradeMode;
                                                greenDealJob.ClaimedQuantity = jobDetails.ClaimedQuantity == null ? 0 : (int)jobDetails.ClaimedQuantity;
                                                greenDealJob.Price = jobDetails.Price;
                                                greenDealJob.Gst = jobDetails.Gst;
                                                greenDealJob.Status = jobDetails.Status;
                                                greenDealJob.PaymentStatus = jobDetails.PaymentStatus;
                                                greenDealJob.SubmitDate = jobDetails.SubmitDate;
                                                greenDealJob.PaidDate = jobDetails.PaidDate;
                                                greenDealJob.CreditMemo = jobDetails.CreditMemo;
                                                greenDealJob.UnqualifiedReasons = jobDetails.UnqualifiedReasons;
                                                greenDealJob.FailedReasons = jobDetails.FailedReasons;
                                                greenDealJob.CreatedVia = jobDetails.CreatedVia;
                                                greenDealJob.Note = jobDetails.Note;
                                                greenDealJob.ReceivedDate = jobDetails.ReceivedDate;
                                                greenDealJob.Reference = jobDetails.Reference;
                                                greenDealJob.Amount = jobDetails.Amount;
                                                greenDealJob.CreatedAt = jobDetails.CreatedAt;
                                                greenDealJob.FinishedAt = jobDetails.FinishedAt;
                                                greenDealJob.FinishedLocation = jobDetails.FinishedLocation;
                                                greenDealJob.RecQuantity = jobDetails.RecQuantity;
                                                greenDealJob.PoReference = jobDetails.PoReference;
                                                greenDealJob.OrderReference = jobDetails.OrderReference;

                                                #region Install Details
                                                greenDealJob.ID_UnitType = jobDetails.InstallDetail.UnitType;
                                                greenDealJob.ID_UnitNumber = jobDetails.InstallDetail.UnitNumber;
                                                greenDealJob.ID_PostCode = jobDetails.InstallDetail.PostCode;
                                                greenDealJob.ID_StreetType = jobDetails.InstallDetail.StreetType;
                                                greenDealJob.ID_StreetNumber = jobDetails.InstallDetail.StreetNumber;
                                                greenDealJob.ID_StreetName = jobDetails.InstallDetail.StreetName;
                                                greenDealJob.ID_Suburb = jobDetails.InstallDetail.Suburb;
                                                greenDealJob.ID_State = jobDetails.InstallDetail.State;
                                                greenDealJob.ID_PropertyType = jobDetails.InstallDetail.PropertyType;
                                                greenDealJob.ID_Latitude = jobDetails.InstallDetail.Latitude;
                                                greenDealJob.ID_Longitude = jobDetails.InstallDetail.Longitude;
                                                greenDealJob.ID_SpecialAddress = jobDetails.InstallDetail.SpecialAddress;
                                                greenDealJob.ID_Story = jobDetails.InstallDetail.Story;
                                                greenDealJob.ID_NMINumber = jobDetails.InstallDetail.NMI;
                                                greenDealJob.ID_JobNumber = jobDetails.InstallDetail.JobNumber;
                                                greenDealJob.ID_InstallationAddress = jobDetails.InstallDetail.InstallationAddress;
                                                greenDealJob.ID_SingleOrMultiStory = jobDetails.InstallDetail.SingleOrMultiStory;
                                                greenDealJob.ID_SameAsPostal = jobDetails.InstallDetail.SameAsPostal;
                                                #endregion

                                                #region Owner Details
                                                greenDealJob.OD_FirstName = jobDetails.OwnerDetail.FirstName;
                                                greenDealJob.OD_LastName = jobDetails.OwnerDetail.LastName;
                                                greenDealJob.OD_Phone = jobDetails.OwnerDetail.Phone;
                                                greenDealJob.OD_DeliveryType = jobDetails.OwnerDetail.DeliveryType;
                                                greenDealJob.OD_DeliveryNumber = jobDetails.OwnerDetail.DeliveryNumber;
                                                greenDealJob.OD_UnitType = jobDetails.OwnerDetail.UnitType;
                                                greenDealJob.OD_UnitNumber = jobDetails.OwnerDetail.UnitNumber;
                                                greenDealJob.OD_PostCode = jobDetails.OwnerDetail.PostCode;
                                                greenDealJob.OD_StreetNumber = jobDetails.OwnerDetail.StreetNumber;
                                                greenDealJob.OD_StreetName = jobDetails.OwnerDetail.StreetName;
                                                greenDealJob.OD_Suburb = jobDetails.OwnerDetail.Suburb;
                                                greenDealJob.OD_State = jobDetails.OwnerDetail.State;
                                                greenDealJob.OD_Email = jobDetails.OwnerDetail.Email;
                                                greenDealJob.OD_PriceToTheHomeOwner = jobDetails.OwnerDetail.PriceToTheHomeOwner;
                                                greenDealJob.OD_HomeOwnerAbn = jobDetails.OwnerDetail.HomeOwnerAbn;
                                                greenDealJob.OD_Type = jobDetails.OwnerDetail.Type;
                                                greenDealJob.OD_TypeName = jobDetails.OwnerDetail.TypeName;
                                                greenDealJob.OD_AddressType = jobDetails.OwnerDetail.AddressType;
                                                greenDealJob.OD_Address = jobDetails.OwnerDetail.Address;
                                                greenDealJob.OD_StreetType = jobDetails.OwnerDetail.StreetType;
                                                greenDealJob.OD_OwnerRepresentativeName = jobDetails.OwnerDetail.OwnerRepresentativeName;
                                                greenDealJob.OD_OwnerRepresentativePosition = jobDetails.OwnerDetail.OwnerRepresentativePosition;
                                                #endregion

                                                #region System Details
                                                greenDealJob.SD_DeemedYears = jobDetails.SystemDetail.DeemedYears;
                                                greenDealJob.SD_InstallationDate = jobDetails.SystemDetail.InstallationDate;
                                                greenDealJob.SD_TypeOfConnection = jobDetails.SystemDetail.TypeOfConnection;
                                                greenDealJob.SD_ConnectedType = jobDetails.SystemDetail.ConnectedType;
                                                greenDealJob.SD_IsBattery = jobDetails.SystemDetail.IsBattery;
                                                greenDealJob.SD_SystemMountingType = jobDetails.SystemDetail.SystemMountingType;
                                                greenDealJob.SD_RatedOutput = jobDetails.SystemDetail.RatedOutput;
                                                greenDealJob.SD_CompleteUnit = jobDetails.SystemDetail.CompleteUnit;
                                                greenDealJob.SD_AdditionalUpgradeSystemDetails = jobDetails.SystemDetail.AdditionalUpgradeSystemDetails;
                                                greenDealJob.SD_InstallAdditionalInformation = jobDetails.SystemDetail.InstallAdditionalInformation;
                                                greenDealJob.SD_PanelBrand = jobDetails.SystemDetail.PanelBrand;
                                                greenDealJob.SD_PanelModel = jobDetails.SystemDetail.PanelModel;
                                                greenDealJob.SD_NumberPanels = jobDetails.SystemDetail.NumberPanels;
                                                greenDealJob.SD_NumberInverters = jobDetails.SystemDetail.NumberInverters;
                                                greenDealJob.SD_WattsPerPanel = jobDetails.SystemDetail.WattsPerPanel;
                                                greenDealJob.SD_InverterManufacture = jobDetails.SystemDetail.InverterManufacture;
                                                greenDealJob.SD_InverterSeries = jobDetails.SystemDetail.InverterSeries;
                                                greenDealJob.SD_InverterModel = jobDetails.SystemDetail.InverterModel;
                                                greenDealJob.SD_BatteryManufacture = jobDetails.SystemDetail.BatteryManufacture;
                                                greenDealJob.SD_BatterySeries = jobDetails.SystemDetail.BatterySeries;
                                                greenDealJob.SD_BatteryModel = jobDetails.SystemDetail.BatteryModel;
                                                #endregion

                                                #region Installer Details
                                                greenDealJob.I_FirstName = jobDetails.Installer.FirstName;
                                                greenDealJob.I_LastName = jobDetails.Installer.LastName;
                                                greenDealJob.I_Accreditation = jobDetails.Installer.Accreditation;
                                                greenDealJob.I_UnitType = jobDetails.Installer.UnitType;
                                                greenDealJob.I_UnitNumber = jobDetails.Installer.UnitNumber;
                                                greenDealJob.I_StreetNumber = jobDetails.Installer.StreetNumber;
                                                greenDealJob.I_StreetName = jobDetails.Installer.StreetName;
                                                greenDealJob.I_StreetType = jobDetails.Installer.StreetType;
                                                greenDealJob.I_Suburb = jobDetails.Installer.Suburb;
                                                greenDealJob.I_State = jobDetails.Installer.State;
                                                greenDealJob.I_AddressType = jobDetails.Installer.AddressType;
                                                greenDealJob.I_DeliveryType = jobDetails.Installer.DeliveryType;
                                                greenDealJob.I_DeliveryNumber = jobDetails.Installer.DeliveryNumber;
                                                greenDealJob.I_PostCode = jobDetails.Installer.PostCode;
                                                greenDealJob.I_Phone = jobDetails.Installer.Phone;
                                                greenDealJob.I_PlumberLicenses = jobDetails.Installer.PlumberLicenses;
                                                #endregion

                                                #region Designer Details
                                                greenDealJob.D_FirstName = jobDetails.Designer.FirstName;
                                                greenDealJob.D_LastName = jobDetails.Designer.LastName;
                                                greenDealJob.D_Accreditation = jobDetails.Designer.Accreditation;
                                                greenDealJob.D_UnitType = jobDetails.Designer.UnitType;
                                                greenDealJob.D_UnitNumber = jobDetails.Designer.UnitNumber;
                                                greenDealJob.D_StreetNumber = jobDetails.Designer.StreetNumber;
                                                greenDealJob.D_StreetName = jobDetails.Designer.StreetName;
                                                greenDealJob.D_StreetType = jobDetails.Designer.StreetType;
                                                greenDealJob.D_Suburb = jobDetails.Designer.Suburb;
                                                greenDealJob.D_State = jobDetails.Designer.State;
                                                greenDealJob.D_AddressType = jobDetails.Designer.AddressType;
                                                greenDealJob.D_DeliveryType = jobDetails.Designer.DeliveryType;
                                                greenDealJob.D_DeliveryNumber = jobDetails.Designer.DeliveryNumber;
                                                greenDealJob.D_PostCode = jobDetails.Designer.PostCode;
                                                greenDealJob.D_Phone = jobDetails.Designer.Phone;
                                                #endregion

                                                #region Electrician Details
                                                greenDealJob.E_FirstName = jobDetails.Electrician.FirstName;
                                                greenDealJob.E_LastName = jobDetails.Electrician.LastName;
                                                greenDealJob.E_License = jobDetails.Electrician.License;
                                                greenDealJob.E_UnitType = jobDetails.Electrician.UnitType;
                                                greenDealJob.E_UnitNumber = jobDetails.Electrician.UnitNumber;
                                                greenDealJob.E_StreetNumber = jobDetails.Electrician.StreetNumber;
                                                greenDealJob.E_StreetName = jobDetails.Electrician.StreetName;
                                                greenDealJob.E_StreetType = jobDetails.Electrician.StreetType;
                                                greenDealJob.E_Suburb = jobDetails.Electrician.Suburb;
                                                greenDealJob.E_State = jobDetails.Electrician.State;
                                                greenDealJob.E_AddressType = jobDetails.Electrician.AddressType;
                                                greenDealJob.E_DeliveryType = jobDetails.Electrician.DeliveryType;
                                                greenDealJob.E_DeliveryNumber = jobDetails.Electrician.DeliveryNumber;
                                                greenDealJob.E_PostCode = jobDetails.Electrician.PostCode;
                                                greenDealJob.E_Phone = jobDetails.Electrician.Phone;
                                                #endregion

                                                greenDealJob.InstallerSignedAt = jobDetails.InstallerSignedAt;
                                                greenDealJob.InstallerSignedSignature = jobDetails.InstallerSignedSignature;
                                                greenDealJob.DesignerSignedAt = jobDetails.DesignerSignedAt;
                                                greenDealJob.DesignerSignedSignature = jobDetails.DesignerSignedSignature;
                                                greenDealJob.OwnerSignedAt = jobDetails.OwnerSignedAt;
                                                greenDealJob.OwnerSignedSignature = jobDetails.OwnerSignedSignature;
                                                greenDealJob.WitnessSignedAt = jobDetails.WitnessSignedAt;
                                                greenDealJob.WitnessSignedSignature = jobDetails.WitnessSignedSignature;
                                                greenDealJob.RetailerSignedAt = jobDetails.RetailerSignedAt;
                                                greenDealJob.RetailerSignedSignature = jobDetails.RetailerSignedSignature;
                                                greenDealJob.RetailerWitnessSignedAt = jobDetails.RetailerWitnessSignedAt;
                                                greenDealJob.RetailerWitnessSignedSignature = jobDetails.RetailerWitnessSignedSignature;
                                                greenDealJob.InstallerWitnessSignedAt = jobDetails.InstallerWitnessSignedAt;
                                                greenDealJob.InstallerWitnessSignedSignature = jobDetails.InstallerWitnessSignedSignature;
                                                greenDealJob.DesignerWitnessSignedAt = jobDetails.DesignerWitnessSignedAt;
                                                greenDealJob.DesignerWitnessSignedSignature = jobDetails.DesignerWitnessSignedSignature;

                                                foreach (var item in jobDetails.SerialNumbers)
                                                {
                                                    GreenDealJobSerialNo greenDealJobSerialNo = new GreenDealJobSerialNo();
                                                    greenDealJobSerialNo.CreationTime = creationTime;
                                                    greenDealJobSerialNo.CreatorUserId = creatorUserId;
                                                    greenDealJobSerialNo.IsDeleted = false;
                                                    greenDealJobSerialNo.GwtId = jobDetails.GwtId;
                                                    greenDealJobSerialNo.SerialNo = item;
                                                    greenDealJobSerialNo.ProductItemTypeId = 1; // for Panel

                                                    greenDealJobSerialNoList.Add(greenDealJobSerialNo);
                                                }

                                                greenDealJobList.Add(greenDealJob);
                                            }

                                            //Get Inverter Serial No
                                            var jsonResponseInverter = await GetInverterSerialNoAsync(content);
                                            if (jsonResponseInverter.IsSuccessStatusCode)
                                            {
                                                var creationTime = DateTime.UtcNow;
                                                var creatorUserId = 1;

                                                var jsonString = await jsonResponseInverter.Content.ReadAsStringAsync();
                                                var jobsInfoRootObject = JsonConvert.DeserializeObject<JobSerialNoRootObject>(jsonString);

                                                var jobSerialNos = jobsInfoRootObject.Response.Body;

                                                foreach (var item in jobSerialNos)
                                                {
                                                    GreenDealJobSerialNo greenDealJobSerialNo = new GreenDealJobSerialNo();
                                                    greenDealJobSerialNo.CreationTime = creationTime;
                                                    greenDealJobSerialNo.CreatorUserId = creatorUserId;
                                                    greenDealJobSerialNo.IsDeleted = false;
                                                    greenDealJobSerialNo.GwtId = gwtId;
                                                    greenDealJobSerialNo.SerialNo = item.SerialNo;
                                                    greenDealJobSerialNo.ProductItemTypeId = 2; // for Inverter

                                                    greenDealJobSerialNoList.Add(greenDealJobSerialNo);
                                                }
                                            }
                                        }
                                    }
                                }

                                if (greenDealJobList.Count > 0)
                                {
                                    await _dbcontextprovider.GetDbContext().Database.ExecuteSqlRawAsync("TRUNCATE TABLE [GreenDealJobs]");

                                    await _dbcontextprovider.GetDbContext().GreenDealJobs.AddRangeAsync(greenDealJobList);
                                    await _dbcontextprovider.GetDbContext().SaveChangesAsync();
                                }

                                if (greenDealJobSerialNoList.Count > 0)
                                {
                                    await _dbcontextprovider.GetDbContext().Database.ExecuteSqlRawAsync("TRUNCATE TABLE [GreenDealJobSerialNo]");

                                    await _dbcontextprovider.GetDbContext().GreenDealJobSerialNos.AddRangeAsync(greenDealJobSerialNoList);
                                    await _dbcontextprovider.GetDbContext().SaveChangesAsync();
                                }

                            }
                        }
                    }

                    await uow.CompleteAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected virtual async Task<HttpResponseMessage> GetAsync(string GWTId, string content)
        {
            var url = "https://www.greendeal.com.au/rapi/v1/pvds/" + GWTId;
            //var url = "https://www.greendeal.com.au/rapi/v1/pvds.json";

            HttpContent httpContent = new StringContent(content, Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("Accept", "*/*");
            request.Content = httpContent;

            var oTaskPostAsyncJob = await client.SendAsync(request);

            return oTaskPostAsyncJob;
        }

        public static string GetEncryptedSignature(string timestamp, string clientId, string clientSecret)
        {
            string signature_string = "GD:" + clientId + timestamp + clientSecret;

            string hash = BitConverter.ToString(MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(signature_string))).Replace("-", "");

            return hash.ToLower();
        }

        public static string GetTimeStamp()
        {
            //return dateTime.ToString("yyyy-MM-ddTHH:mm:ss:z");
            return new DateTimeOffset(DateTime.UtcNow).ToString(@"yyyy-MM-ddTHH:mm:ss:z");
        }

        protected virtual async Task<HttpResponseMessage> GetInverterSerialNoAsync(string content)
        {
            var url = "https://www.greendeal.com.au/rapi/v1/pvds/get_inverter_numbers.json";

            HttpContent httpContent = new StringContent(content, Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("Accept", "*/*");
            request.Content = httpContent;

            var oTaskPostAsyncJob = await client.SendAsync(request);

            return oTaskPostAsyncJob;
        }

        public async Task GetAllRECDetails(TimeZoneInfo timeZoneInfo)
        {
            try
            {
                using (var uow = _unitOfWorkManager.Begin())
                {
                    var tenants = _tenantRepository.GetAll()
                                    .Where(e => e.Id == 6)
                                    .OrderByDescending(p => p.Id).ToList();

                    foreach (var tenant in tenants)
                    {
                        if (tenant.IsActive == true && tenant.Id != 1)
                        {
                            using (_unitOfWorkManager.Current.SetTenantId(tenant.Id))
                            {
                                var todayDate = TimeZoneInfo.ConvertTime(DateTime.UtcNow, timeZoneInfo).Date.AddDays(-1).ToString("yyyy-MM-dd");

                                //var startDate = new DateTime(2021, 01, 01);
                                //var endDate = new DateTime(2024, 10, 05);

                                //List<string> calculatedDates = new List<string>();

                                //for (var date = startDate; date <= endDate; date = date.AddDays(1))
                                //{
                                //    var toDay = date.ToString("yyyy-MM-dd");

                                //    var url = "https://rec-registry.gov.au/rec-registry/app/api/public-register/certificate-actions?date=" + toDay;

                                    
                                //}

                                var url = "https://rec-registry.gov.au/rec-registry/app/api/public-register/certificate-actions?date=" + todayDate;
                                HttpClient client = new HttpClient();
                                var request = new HttpRequestMessage(HttpMethod.Get, url);

                                var jsonResponse = await client.SendAsync(request);

                                if (jsonResponse.IsSuccessStatusCode)
                                {
                                    var jsonString = await jsonResponse.Content.ReadAsStringAsync();
                                    var recRootObject = JsonConvert.DeserializeObject<RecRootObject>(jsonString);

                                    var actionTypes = new List<string> { "STC created", "STC audit passed", "STC audit failed" };

                                    var recDatailsList = new List<RECDetail>();
                                    if (recRootObject.Result != null)
                                    {
                                        foreach (var item in recRootObject.Result)
                                        {
                                            if (item.CertificateRanges != null && item.CertificateRanges.Count > 0)
                                            {
                                                if (item.CertificateRanges[0].OwnerAccountId == 43023 && !string.IsNullOrEmpty(item.CertificateRanges[0].AccreditationCode) && actionTypes.Any(e => e == item.ActionType)) //43023=>"QUICKFORM TRADES PTY LTD
                                                {
                                                    var recDatails = new RECDetail();
                                                    recDatails.ActionType = item.ActionType;
                                                    recDatails.CompletedTime = item.CompletedTime;
                                                    recDatails.CertificateType = item.CertificateRanges[0].CertificateType;
                                                    recDatails.RegisteredPersonNumber = item.CertificateRanges[0].RegisteredPersonNumber;
                                                    recDatails.AccreditationCode = item.CertificateRanges[0].AccreditationCode;
                                                    recDatails.GenerationYear = item.CertificateRanges[0].GenerationYear;
                                                    recDatails.GenerationState = item.CertificateRanges[0].GenerationState;
                                                    recDatails.StartSerialNumber = item.CertificateRanges[0].StartSerialNumber;
                                                    recDatails.EndSerialNumber = item.CertificateRanges[0].EndSerialNumber;
                                                    recDatails.FuelSource = item.CertificateRanges[0].FuelSource;
                                                    recDatails.OwnerAccount = item.CertificateRanges[0].OwnerAccount;
                                                    recDatails.OwnerAccountId = item.CertificateRanges[0].OwnerAccountId;
                                                    recDatails.Status = item.CertificateRanges[0].Status;
                                                    if (item.CertificateRanges[0].GenerationMonth != null)
                                                    {
                                                        recDatails.GM_DisplayName = item.CertificateRanges[0].GenerationMonth.DisplayName;
                                                        recDatails.GM_MonthCode = item.CertificateRanges[0].GenerationMonth.MonthCode;
                                                        recDatails.GM_MonthName = item.CertificateRanges[0].GenerationMonth.MonthName;
                                                        recDatails.GM_Name = item.CertificateRanges[0].GenerationMonth.Name;
                                                    }
                                                    else
                                                    {
                                                        recDatails.GM_MonthCode = 0;
                                                    }
                                                    recDatails.CreationDate = item.CertificateRanges[0].CreationDate;

                                                    recDatailsList.Add(recDatails);
                                                }
                                            }
                                        }
                                    }

                                    if (recDatailsList.Count > 0)
                                    {
                                        var res = recDatailsList.GroupBy(x => x.AccreditationCode).Select(g => g.OrderByDescending(o => o.CompletedTime).First()).OrderBy(e => e.CreationDate).ToList();

                                        var recData = new List<RECDetail>();
                                        for (int i = 0; i < res.Count; i++)
                                        {
                                            var existsRec = _recDetailRepository.FirstOrDefault(e => e.AccreditationCode == res[i].AccreditationCode);
                                            if (existsRec != null)
                                            {
                                                existsRec.ActionType = res[i].ActionType;
                                                existsRec.CompletedTime = res[i].CompletedTime;
                                                existsRec.CertificateType = res[i].CertificateType;
                                                existsRec.RegisteredPersonNumber = res[i].RegisteredPersonNumber;
                                                existsRec.AccreditationCode = res[i].AccreditationCode;
                                                existsRec.GenerationYear = res[i].GenerationYear;
                                                existsRec.GenerationState = res[i].GenerationState;
                                                existsRec.StartSerialNumber = res[i].StartSerialNumber;
                                                existsRec.EndSerialNumber = res[i].EndSerialNumber;
                                                existsRec.FuelSource = res[i].FuelSource;
                                                existsRec.OwnerAccount = res[i].OwnerAccount;
                                                existsRec.OwnerAccountId = res[i].OwnerAccountId;
                                                existsRec.Status = res[i].Status;
                                                existsRec.GM_DisplayName = res[i].GM_DisplayName;
                                                existsRec.GM_MonthCode = res[i].GM_MonthCode;
                                                existsRec.GM_MonthName = res[i].GM_MonthName;
                                                existsRec.GM_Name = res[i].GM_Name;
                                                existsRec.CreationDate = res[i].CreationDate;

                                                recData.Add(existsRec);
                                            }
                                            else
                                            {
                                                var newRec = new RECDetail();
                                                newRec.ActionType = res[i].ActionType;
                                                newRec.CompletedTime = res[i].CompletedTime;
                                                newRec.CertificateType = res[i].CertificateType;
                                                newRec.RegisteredPersonNumber = res[i].RegisteredPersonNumber;
                                                newRec.AccreditationCode = res[i].AccreditationCode;
                                                newRec.GenerationYear = res[i].GenerationYear;
                                                newRec.GenerationState = res[i].GenerationState;
                                                newRec.StartSerialNumber = res[i].StartSerialNumber;
                                                newRec.EndSerialNumber = res[i].EndSerialNumber;
                                                newRec.FuelSource = res[i].FuelSource;
                                                newRec.OwnerAccount = res[i].OwnerAccount;
                                                newRec.OwnerAccountId = res[i].OwnerAccountId;
                                                newRec.Status = res[i].Status;
                                                newRec.GM_DisplayName = res[i].GM_DisplayName;
                                                newRec.GM_MonthCode = res[i].GM_MonthCode;
                                                newRec.GM_MonthName = res[i].GM_MonthName;
                                                newRec.GM_Name = res[i].GM_Name;
                                                newRec.CreationDate = res[i].CreationDate;

                                                recData.Add(newRec);
                                            }
                                        }

                                        _dbcontextprovider.GetDbContext().RECDetails.UpdateRange(recData);
                                        _dbcontextprovider.GetDbContext().SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                    uow.Complete();
                }



            }
            catch (Exception e) { throw e; }
        }

        public async Task UpdateRECDetails()
        {
            try
            {
                using (var uow = _unitOfWorkManager.Begin())
                {
                    var tenants = _tenantRepository.GetAll()
                        .Where(e => e.Id == 6)
                        .OrderByDescending(p => p.Id).ToList();

                    foreach (var tenant in tenants)
                    {
                        if (tenant.IsActive == true && tenant.Id != 1)
                        {
                            using (_unitOfWorkManager.Current.SetTenantId(tenant.Id))
                            {
                                var extOrgDetail = _extendOrganizationUnitRepository.GetAll().Where(e => e.Id == 7).ToList();
                                foreach (var org in extOrgDetail)
                                {
                                    var recJobList = _jobRepository.GetAll().Where(e => e.JobStatusId >= 4 && e.InstallationDate != null && e.LeadFk.OrganizationId == org.Id && e.RECPvdStatus != "Rec Approved").ToList();

                                    foreach (var item in recJobList)
                                    {
                                        if (!string.IsNullOrEmpty(item.PVDNumber))
                                        {
                                            var recDetails = await _recDetailRepository.FirstOrDefaultAsync(e => e.AccreditationCode == item.PVDNumber);

                                            if (recDetails != null)
                                            {
                                                item.RECPvdNumber = recDetails.AccreditationCode;
                                                item.LastRECUpdated = recDetails.CompletedTime;

                                                if (recDetails.ActionType == "STC audit passed")
                                                {
                                                    item.RECPvdStatus = "Rec Approved";
                                                }
                                                else if (recDetails.ActionType == "STC audit failed")
                                                {
                                                    item.RECPvdStatus = "Rec Failed";
                                                }
                                                else if (recDetails.ActionType == "STC created")
                                                {
                                                    item.RECPvdStatus = "Rec Pending";
                                                }
                                                await _jobRepository.UpdateAsync(item);
                                            }
                                        }
                                    }
                                }


                            }
                        }
                    }
                    uow.Complete();
                }
            }
            catch (Exception e) { throw e; }
        }
    
        public async Task GetCimetDetails()
        {
            try
            {
                var australianEasternTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                var todayDate = TimeZoneInfo.ConvertTime(DateTime.UtcNow, australianEasternTimeZoneInfo).Date.ToString("dd/MM/yyyy");

                #region Solar Product Cimet Credential
                string APIKey = "3MFXJ-YB15U-W6L7E-17115-33379";
                #endregion
                
                using (var uow = _unitOfWorkManager.Begin())
                {
                    var tenants = _tenantRepository.GetAll()
                                    .Where(e => e.Id == 6)
                                    .OrderByDescending(p => p.Id).ToList();

                    foreach (var tenant in tenants)
                    {
                        if (tenant.IsActive == true && tenant.Id != 1)
                        {
                            using (_unitOfWorkManager.Current.SetTenantId(tenant.Id))
                            {
                                #region Get Token
                                var client = new HttpClient();
                                var tokenRequest = new HttpRequestMessage(HttpMethod.Post, "https://services-lms.cimet.io/v2/generate-token");
                                tokenRequest.Headers.Add("accept", "application/json");
                                tokenRequest.Headers.Add("API-Key", APIKey);

                                var tokenResponse = await client.SendAsync(tokenRequest);
                                #endregion

                                if (tokenResponse.IsSuccessStatusCode)
                                {
                                    var tokenRootObject = JsonConvert.DeserializeObject<TokenRootObject>(await tokenResponse.Content.ReadAsStringAsync());

                                    if(tokenRootObject.Status == true)
                                    {
                                        var content = new
                                        {
                                            start_date = "01/01/2020",
                                            end_date = todayDate
                                        };

                                        var temp = JsonConvert.SerializeObject(content, Formatting.Indented);
                                        HttpContent httpContent = new StringContent(temp, Encoding.UTF8, "application/json");

                                        client = new HttpClient();
                                        var request = new HttpRequestMessage(HttpMethod.Post, "https://services-lms.cimet.io/v2/affiliate-sale-status-report");
                                        request.Headers.Add("API-Key", APIKey);
                                        request.Headers.Add("Auth-Token", tokenRootObject.Data.Token);
                                        request.Content = httpContent;

                                        var response = await client.SendAsync(request);
                                        if (response.IsSuccessStatusCode)
                                        {
                                            var rootObject = JsonConvert.DeserializeObject<ThirdPartyApis.Cimet.Dto.RootObject>(await response.Content.ReadAsStringAsync());

                                            var cimetDataList = new List<CimetDetail>();

                                            if (rootObject.Status == true)
                                            {
                                                if(rootObject.Data != null)
                                                {
                                                    if(rootObject.Data.SaleData != null && rootObject.Data.SaleData.Count > 0)
                                                    {
                                                        foreach(var item in rootObject.Data.SaleData)
                                                        {
                                                            if(item.Cui != null)
                                                            {
                                                                var existsRec = _cimetDetailRepository.FirstOrDefault(e => e.Cui == item.Cui);
                                                                if(existsRec != null)
                                                                {
                                                                    existsRec.ProviderName = item.ProviderName;
                                                                    existsRec.PlanName = item.PlanName;
                                                                    existsRec.AffiliateName = item.AffiliateName;
                                                                    existsRec.SubaffiliateName = item.SubaffiliateName;
                                                                    existsRec.Address = item.Address;
                                                                    existsRec.StatusErrorDescription = item.StatusErrorDescription;
                                                                    existsRec.StatusCreatedAt = item.StatusCreatedAt;
                                                                    existsRec.StatusUpdatedAt = item.StatusUpdatedAt;
                                                                    existsRec.SaleSubStatus = item.SaleSubStatus;
                                                                    existsRec.SaleProductsEnergyServiceId = item.SaleProductsEnergyServiceId;
                                                                    existsRec.CommissionStatus = item.CommissionStatus;
                                                                    existsRec.ReferenceNo = item.ReferenceNo;
                                                                    existsRec.ProductType = item.ProductType;
                                                                    existsRec.SaleStatus = item.SaleStatus;
                                                                    existsRec.CreatedAt = item.CreatedAt;
                                                                    existsRec.ProductId = item.ProductId;
                                                                    existsRec.LeadId = item.LeadId;
                                                                    existsRec.VisitorId = item.VisitorId;
                                                                    existsRec.Status = item.Status;
                                                                    existsRec.VisitorSource = item.VisitorSource;
                                                                    existsRec.AgentName = item.AgentName;
                                                                    existsRec.AgentId = item.AgentId;
                                                                    existsRec.AgentEmail = item.AgentEmail;
                                                                    existsRec.SaleCreated = item.SaleCreated;
                                                                    existsRec.Rc = item.Rc;
                                                                    existsRec.SaleCreatedAt = item.SaleCreatedAt;
                                                                    existsRec.ProductUpdatedAt = item.ProductUpdatedAt;
                                                                    existsRec.ProductStatus = item.ProductStatus;
                                                                    existsRec.BundleId = item.BundleId;
                                                                    existsRec.Cui = item.Cui;
                                                                    existsRec.UtmSource = item.UtmSource;
                                                                    existsRec.UtmMedium = item.UtmMedium;
                                                                    existsRec.UtmCampaign = item.UtmCampaign;
                                                                    existsRec.UtmTerm = item.UtmTerm;
                                                                    existsRec.UtmContent = item.UtmContent;
                                                                    existsRec.Gclid = item.Gclid;
                                                                    existsRec.Fbclid = item.Fbclid;
                                                                    existsRec.Msclkid = item.Msclkid;
                                                                    existsRec.ServiceId = item.ServiceId;
                                                                    existsRec.SaleStatusDescription = item.SaleStatusDescription;
                                                                    existsRec.SubStatusDescription = item.SubStatusDescription;
                                                                    existsRec.ServiceName = item.ServiceName;
                                                                    existsRec.StatusTitle = item.StatusTitle;
                                                                    existsRec.AffiliateCommissionStatus = item.AffiliateCommissionStatus;
                                                                    existsRec.SaleStatusUpdatedAt = item.SaleStatusUpdatedAt;
                                                                    existsRec.SaleStatusCreatedAt = item.SaleStatusCreatedAt;
                                                                    existsRec.PostalAddress = item.PostalAddress;
                                                                    existsRec.SaleType = item.SaleType;
                                                                    existsRec.PropertyType = item.PropertyType;
                                                                    existsRec.MoveinType = item.MoveinType;
                                                                    existsRec.MovinDate = item.MovinDate;
                                                                    existsRec.SolarType = item.SolarType;
                                                                    existsRec.MedicalSupport = item.MedicalSupport;
                                                                    existsRec.EnergyType = item.EnergyType;
                                                                    existsRec.NmiMirnNumber = item.NmiMirnNumber;
                                                                    existsRec.FirstName = item.FirstName;
                                                                    existsRec.LastName = item.LastName;
                                                                    existsRec.Email = item.Email;
                                                                    existsRec.Phone = item.Phone;
                                                                    existsRec.UpdatedAt = item.UpdatedAt;
                                                                    existsRec.TransfersStatus = item.TransfersStatus;
                                                                    existsRec.ConnectionName = item.ConnectionName;
                                                                    existsRec.ConnectionType = item.ConnectionType;
                                                                    existsRec.SaleProductsMobileServiceId = item.SaleProductsMobileServiceId;
                                                                    existsRec.Provider = item.Provider;
                                                                    existsRec.PlanMobile = item.PlanMobile;
                                                                    existsRec.ReferalCode = item.ReferalCode;
                                                                    existsRec.SaleProductsBroadbandServiceId = item.SaleProductsBroadbandServiceId;
                                                                    existsRec.PlanBroadband = item.PlanBroadband;
                                                                    cimetDataList.Add(existsRec);
                                                                }
                                                                else
                                                                {
                                                                    var newRec = new CimetDetail();

                                                                    newRec.ProviderName = item.ProviderName;
                                                                    newRec.PlanName = item.PlanName;
                                                                    newRec.AffiliateName = item.AffiliateName;
                                                                    newRec.SubaffiliateName = item.SubaffiliateName;
                                                                    newRec.Address = item.Address;
                                                                    newRec.StatusErrorDescription = item.StatusErrorDescription;
                                                                    newRec.StatusCreatedAt = item.StatusCreatedAt;
                                                                    newRec.StatusUpdatedAt = item.StatusUpdatedAt;
                                                                    newRec.SaleSubStatus = item.SaleSubStatus;
                                                                    newRec.SaleProductsEnergyServiceId = item.SaleProductsEnergyServiceId;
                                                                    newRec.CommissionStatus = item.CommissionStatus;
                                                                    newRec.ReferenceNo = item.ReferenceNo;
                                                                    newRec.ProductType = item.ProductType;
                                                                    newRec.SaleStatus = item.SaleStatus;
                                                                    newRec.CreatedAt = item.CreatedAt;
                                                                    newRec.ProductId = item.ProductId;
                                                                    newRec.LeadId = item.LeadId;
                                                                    newRec.VisitorId = item.VisitorId;
                                                                    newRec.Status = item.Status;
                                                                    newRec.VisitorSource = item.VisitorSource;
                                                                    newRec.AgentName = item.AgentName;
                                                                    newRec.AgentId = item.AgentId;
                                                                    newRec.AgentEmail = item.AgentEmail;
                                                                    newRec.SaleCreated = item.SaleCreated;
                                                                    newRec.Rc = item.Rc;
                                                                    newRec.SaleCreatedAt = item.SaleCreatedAt;
                                                                    newRec.ProductUpdatedAt = item.ProductUpdatedAt;
                                                                    newRec.ProductStatus = item.ProductStatus;
                                                                    newRec.BundleId = item.BundleId;
                                                                    newRec.Cui = item.Cui;
                                                                    newRec.UtmSource = item.UtmSource;
                                                                    newRec.UtmMedium = item.UtmMedium;
                                                                    newRec.UtmCampaign = item.UtmCampaign;
                                                                    newRec.UtmTerm = item.UtmTerm;
                                                                    newRec.UtmContent = item.UtmContent;
                                                                    newRec.Gclid = item.Gclid;
                                                                    newRec.Fbclid = item.Fbclid;
                                                                    newRec.Msclkid = item.Msclkid;
                                                                    newRec.ServiceId = item.ServiceId;
                                                                    newRec.SaleStatusDescription = item.SaleStatusDescription;
                                                                    newRec.SubStatusDescription = item.SubStatusDescription;
                                                                    newRec.ServiceName = item.ServiceName;
                                                                    newRec.StatusTitle = item.StatusTitle;
                                                                    newRec.AffiliateCommissionStatus = item.AffiliateCommissionStatus;
                                                                    newRec.SaleStatusUpdatedAt = item.SaleStatusUpdatedAt;
                                                                    newRec.SaleStatusCreatedAt = item.SaleStatusCreatedAt;
                                                                    newRec.PostalAddress = item.PostalAddress;
                                                                    newRec.SaleType = item.SaleType;
                                                                    newRec.PropertyType = item.PropertyType;
                                                                    newRec.MoveinType = item.MoveinType;
                                                                    newRec.MovinDate = item.MovinDate;
                                                                    newRec.SolarType = item.SolarType;
                                                                    newRec.MedicalSupport = item.MedicalSupport;
                                                                    newRec.EnergyType = item.EnergyType;
                                                                    newRec.NmiMirnNumber = item.NmiMirnNumber;
                                                                    newRec.FirstName = item.FirstName;
                                                                    newRec.LastName = item.LastName;
                                                                    newRec.Email = item.Email;
                                                                    newRec.Phone = item.Phone;
                                                                    newRec.UpdatedAt = item.UpdatedAt;
                                                                    newRec.TransfersStatus = item.TransfersStatus;
                                                                    newRec.ConnectionName = item.ConnectionName;
                                                                    newRec.ConnectionType = item.ConnectionType;
                                                                    newRec.SaleProductsMobileServiceId = item.SaleProductsMobileServiceId;
                                                                    newRec.Provider = item.Provider;
                                                                    newRec.PlanMobile = item.PlanMobile;
                                                                    newRec.ReferalCode = item.ReferalCode;
                                                                    newRec.SaleProductsBroadbandServiceId = item.SaleProductsBroadbandServiceId;
                                                                    newRec.PlanBroadband = item.PlanBroadband;

                                                                    cimetDataList.Add(newRec);

                                                                }
                                                            }
                                                        }

                                                        
                                                    }
                                                }
                                            }

                                            if (cimetDataList.Count > 0)
                                            {
                                                _dbcontextprovider.GetDbContext().CimetDetails.UpdateRange(cimetDataList);
                                                _dbcontextprovider.GetDbContext().SaveChanges();
                                            }
                                        }


                                    }
                                }


                                //

                                    //var url = "https://rec-registry.gov.au/rec-registry/app/api/public-register/certificate-actions?date=" + todayDate;
                                    //HttpClient client = new HttpClient();
                                    //var request = new HttpRequestMessage(HttpMethod.Get, url);

                                    //var jsonResponse = await client.SendAsync(request);

                                    //if (jsonResponse.IsSuccessStatusCode)
                                    //{
                                    //    var jsonString = await jsonResponse.Content.ReadAsStringAsync();
                                    //    var recRootObject = JsonConvert.DeserializeObject<RecRootObject>(jsonString);

                                    //    var actionTypes = new List<string> { "STC created", "STC audit passed", "STC audit failed" };

                                    //    var recDatailsList = new List<RECDetail>();
                                    //    if (recRootObject.Result != null)
                                    //    {
                                    //        foreach (var item in recRootObject.Result)
                                    //        {
                                    //            if (item.CertificateRanges[0].OwnerAccountId == 43023 && !string.IsNullOrEmpty(item.CertificateRanges[0].AccreditationCode) && actionTypes.Any(e => e == item.ActionType)) //43023=>"QUICKFORM TRADES PTY LTD
                                    //            {
                                    //                var recDatails = new RECDetail();
                                    //                recDatails.ActionType = item.ActionType;
                                    //                recDatails.CompletedTime = item.CompletedTime;
                                    //                recDatails.CertificateType = item.CertificateRanges[0].CertificateType;
                                    //                recDatails.RegisteredPersonNumber = item.CertificateRanges[0].RegisteredPersonNumber;
                                    //                recDatails.AccreditationCode = item.CertificateRanges[0].AccreditationCode;
                                    //                recDatails.GenerationYear = item.CertificateRanges[0].GenerationYear;
                                    //                recDatails.GenerationState = item.CertificateRanges[0].GenerationState;
                                    //                recDatails.StartSerialNumber = item.CertificateRanges[0].StartSerialNumber;
                                    //                recDatails.EndSerialNumber = item.CertificateRanges[0].EndSerialNumber;
                                    //                recDatails.FuelSource = item.CertificateRanges[0].FuelSource;
                                    //                recDatails.OwnerAccount = item.CertificateRanges[0].OwnerAccount;
                                    //                recDatails.OwnerAccountId = item.CertificateRanges[0].OwnerAccountId;
                                    //                recDatails.Status = item.CertificateRanges[0].Status;
                                    //                if (item.CertificateRanges[0].GenerationMonth != null)
                                    //                {
                                    //                    recDatails.GM_DisplayName = item.CertificateRanges[0].GenerationMonth.DisplayName;
                                    //                    recDatails.GM_MonthCode = item.CertificateRanges[0].GenerationMonth.MonthCode;
                                    //                    recDatails.GM_MonthName = item.CertificateRanges[0].GenerationMonth.MonthName;
                                    //                    recDatails.GM_Name = item.CertificateRanges[0].GenerationMonth.Name;
                                    //                }
                                    //                else
                                    //                {
                                    //                    recDatails.GM_MonthCode = 0;
                                    //                }
                                    //                recDatails.CreationDate = item.CertificateRanges[0].CreationDate;

                                    //                recDatailsList.Add(recDatails);
                                    //            }
                                    //        }
                                    //    }

                                    //    if (recDatailsList.Count > 0)
                                    //    {
                                    //        var res = recDatailsList.GroupBy(x => x.AccreditationCode).Select(g => g.OrderByDescending(o => o.CompletedTime).First()).OrderBy(e => e.CreationDate).ToList();

                                    //        var recData = new List<RECDetail>();
                                    //        for (int i = 0; i < res.Count; i++)
                                    //        {
                                    //            var existsRec = _recDetailRepository.FirstOrDefault(e => e.AccreditationCode == res[i].AccreditationCode);
                                    //            if (existsRec != null)
                                    //            {
                                    //                existsRec.ActionType = res[i].ActionType;
                                    //                existsRec.CompletedTime = res[i].CompletedTime;
                                    //                existsRec.CertificateType = res[i].CertificateType;
                                    //                existsRec.RegisteredPersonNumber = res[i].RegisteredPersonNumber;
                                    //                existsRec.AccreditationCode = res[i].AccreditationCode;
                                    //                existsRec.GenerationYear = res[i].GenerationYear;
                                    //                existsRec.GenerationState = res[i].GenerationState;
                                    //                existsRec.StartSerialNumber = res[i].StartSerialNumber;
                                    //                existsRec.EndSerialNumber = res[i].EndSerialNumber;
                                    //                existsRec.FuelSource = res[i].FuelSource;
                                    //                existsRec.OwnerAccount = res[i].OwnerAccount;
                                    //                existsRec.OwnerAccountId = res[i].OwnerAccountId;
                                    //                existsRec.Status = res[i].Status;
                                    //                existsRec.GM_DisplayName = res[i].GM_DisplayName;
                                    //                existsRec.GM_MonthCode = res[i].GM_MonthCode;
                                    //                existsRec.GM_MonthName = res[i].GM_MonthName;
                                    //                existsRec.GM_Name = res[i].GM_Name;
                                    //                existsRec.CreationDate = res[i].CreationDate;

                                    //                recData.Add(existsRec);
                                    //            }
                                    //            else
                                    //            {
                                    //                var newRec = new RECDetail();
                                    //                newRec.ActionType = res[i].ActionType;
                                    //                newRec.CompletedTime = res[i].CompletedTime;
                                    //                newRec.CertificateType = res[i].CertificateType;
                                    //                newRec.RegisteredPersonNumber = res[i].RegisteredPersonNumber;
                                    //                newRec.AccreditationCode = res[i].AccreditationCode;
                                    //                newRec.GenerationYear = res[i].GenerationYear;
                                    //                newRec.GenerationState = res[i].GenerationState;
                                    //                newRec.StartSerialNumber = res[i].StartSerialNumber;
                                    //                newRec.EndSerialNumber = res[i].EndSerialNumber;
                                    //                newRec.FuelSource = res[i].FuelSource;
                                    //                newRec.OwnerAccount = res[i].OwnerAccount;
                                    //                newRec.OwnerAccountId = res[i].OwnerAccountId;
                                    //                newRec.Status = res[i].Status;
                                    //                newRec.GM_DisplayName = res[i].GM_DisplayName;
                                    //                newRec.GM_MonthCode = res[i].GM_MonthCode;
                                    //                newRec.GM_MonthName = res[i].GM_MonthName;
                                    //                newRec.GM_Name = res[i].GM_Name;
                                    //                newRec.CreationDate = res[i].CreationDate;

                                    //                recData.Add(newRec);
                                    //            }
                                    //        }

                                    //        _dbcontextprovider.GetDbContext().RECDetails.UpdateRange(recData);
                                    //        _dbcontextprovider.GetDbContext().SaveChanges();
                                    //    }
                                    //}

                            }
                        }
                    }
                    uow.Complete();
                }



            }
            catch (Exception e) { throw e; }
        }
    }
}
