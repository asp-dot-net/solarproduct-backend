﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.DocumentTypes.Dtos;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.Services;
using TheSolarProduct.ServiceDocumentTypes.Dtos;
using TheSolarProduct.Quotations;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using System.Collections.Generic;
using TheSolarProduct.PostCodes;

namespace TheSolarProduct.ServiceDocumentTypes
{
    [AbpAuthorize]
    public class ServiceDocumentTypeAppService : TheSolarProductAppServiceBase, IServiceDocumentTypeAppService
    {
        private readonly IRepository<ServiceDocumentType> _serviceDocumenttypeRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public ServiceDocumentTypeAppService(IRepository<ServiceDocumentType> serviceDocumenttypeRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _serviceDocumenttypeRepository = serviceDocumenttypeRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_datavault_ServiceDocumentType)]
        public async Task<PagedResultDto<ServiceDocumentTypeDto>> GetAll(GetAllServiceDocumentTypeInput input)
        {
            var filteredServiceDocumentTypes = _serviceDocumenttypeRepository.GetAll()
                         .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Title.Contains(input.Filter));

            var pagedAndFilteredServiceDocumentTypes = filteredServiceDocumentTypes
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var ServiceDocumentTypes = from o in pagedAndFilteredServiceDocumentTypes
                              select new ServiceDocumentTypeDto()
                              {
                                      Title = o.Title,
                                      Id = o.Id,
                                      IsDocumentRequest = o.IsDocumentRequest
                              };

            var totalCount = await filteredServiceDocumentTypes.CountAsync();

            return new PagedResultDto<ServiceDocumentTypeDto>(
                totalCount,
                await ServiceDocumentTypes.ToListAsync()
            );
        }

        public async Task<ServiceDocumentTypeDto> GetDepartmentForView(int id)
        {
            var serviceDocument = await _serviceDocumenttypeRepository.GetAsync(id);

            var output = ObjectMapper.Map<ServiceDocumentTypeDto>(serviceDocument);

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_datavault_ServiceDocumentType_Edit)]
        public async Task<GetServiceDocumentTypeforEditOutput> GetDepartmentForEdit(EntityDto input)
        {
            var serviceDocument = await _serviceDocumenttypeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetServiceDocumentTypeforEditOutput { CreateorEditServiceDocumentTypeDto = ObjectMapper.Map<CreateOrEditServiceDocumentTypeDto>(serviceDocument) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditServiceDocumentTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_datavault_ServiceDocumentType_Create)]
        protected virtual async Task Create(CreateOrEditServiceDocumentTypeDto input)
        {

            var documenttype = ObjectMapper.Map<ServiceDocumentType>(input);

            if (AbpSession.TenantId != null)
            {
                documenttype.TenantId = (int)AbpSession.TenantId;
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 55;
            dataVaultLog.ActionNote = "Service Document Type Created : " + input.Title;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _serviceDocumenttypeRepository.InsertAsync(documenttype);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_datavault_ServiceDocumentType_Edit)]
        protected virtual async Task Update(CreateOrEditServiceDocumentTypeDto input)
        {
            var documenttype = await _serviceDocumenttypeRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 55;
            dataVaultLog.ActionNote = "Service Document Type Updated : " + documenttype.Title;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Title != documenttype.Title)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Title";
                history.PrevValue = documenttype.Title;
                history.CurValue = input.Title;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.IsDocumentRequest != documenttype.IsDocumentRequest)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Price";
                history.PrevValue = documenttype.IsDocumentRequest.ToString();
                history.CurValue = input.IsDocumentRequest.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, documenttype);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_datavault_ServiceDocumentType_Delete)]
        public async Task Delete(EntityDto input)
        {
            var type = _serviceDocumenttypeRepository.Get(input.Id).Title;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 55;
            dataVaultLog.ActionNote = "Service Document Type Deleted : " + type;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _serviceDocumenttypeRepository.DeleteAsync(input.Id);
        }


    }
}
