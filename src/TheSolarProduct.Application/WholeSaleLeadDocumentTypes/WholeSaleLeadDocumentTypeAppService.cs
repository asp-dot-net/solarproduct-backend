﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.DocumentTypes.Dtos;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.Services;
using TheSolarProduct.ServiceDocumentTypes.Dtos;
using TheSolarProduct.Quotations;
using TheSolarProduct.WholeSaleLeads;
using TheSolarProduct.WholeSaleLeadDocumentTypes.Dtos;
using TheSolarProduct.Wholesales.DataVault;
using Abp.EntityFrameworkCore;
using System.Collections.Generic;
using TheSolarProduct.WholeSaleStatuses;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.WholeSaleLeadDocumentTypes
{
    [AbpAuthorize]
    public class WholeSaleLeadDocumentTypeAppService : TheSolarProductAppServiceBase, IWholeSaleLeadDocumentTypeAppService
    {
        private readonly IRepository<WholeSaleLeadDocumentType> _wholeSaleLeadDocumenttypeRepository;
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public WholeSaleLeadDocumentTypeAppService(
            IRepository<WholeSaleLeadDocumentType> wholeSaleLeadDocumenttypeRepository,
             IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository,
             IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            )
        {
            _wholeSaleLeadDocumenttypeRepository = wholeSaleLeadDocumenttypeRepository;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task<PagedResultDto<WholeSaleLeadDocumentTypeDto>> GetAll(GetAllWholeSaleLeadDocumentTypeInput input)
        {
            var filteredWholeSaleLeadDocumentTypes = _wholeSaleLeadDocumenttypeRepository.GetAll()
                         .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Title.Contains(input.Filter));

            var pagedAndFilteredWholeSaleLeadDocumentTypes = filteredWholeSaleLeadDocumentTypes
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var WholeSaleLeadDocumentTypes = from o in pagedAndFilteredWholeSaleLeadDocumentTypes
                                       select new WholeSaleLeadDocumentTypeDto()
                                       {
                                           Title = o.Title,
                                           Id = o.Id,
                                           IsDocumentRequest = o.IsDocumentRequest
                                       };

            var totalCount = await filteredWholeSaleLeadDocumentTypes.CountAsync();

            return new PagedResultDto<WholeSaleLeadDocumentTypeDto>(
                totalCount,
                await WholeSaleLeadDocumentTypes.ToListAsync()
            );
        }

        public async Task<WholeSaleLeadDocumentTypeDto> GetWholeSaleLeadDocumentTypeForView(int id)
        {
            var wholeSaleLeadDocument = await _wholeSaleLeadDocumenttypeRepository.GetAsync(id);

            var output = ObjectMapper.Map<WholeSaleLeadDocumentTypeDto>(wholeSaleLeadDocument);

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditWholeSaleLeadDocumentTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(CreateOrEditWholeSaleLeadDocumentTypeDto input)
        {

            var documenttype = ObjectMapper.Map<WholeSaleLeadDocumentType>(input);

            if (AbpSession.TenantId != null)
            {
                documenttype.TenantId = (int)AbpSession.TenantId;
            }
            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 2;
            dataVaultLog.ActionNote = "Document Type Created : " + input.Title;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

            await _wholeSaleLeadDocumenttypeRepository.InsertAsync(documenttype);
        }

        protected virtual async Task Update(CreateOrEditWholeSaleLeadDocumentTypeDto input)
        {
            var documenttype = await _wholeSaleLeadDocumenttypeRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 2;
            dataVaultLog.ActionNote = "Document Type Updated : " + documenttype.Title;

            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<WholeSaleDataVaultActivityLogHistory>();

            if (input.Title != documenttype.Title)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Tile";
                history.PrevValue = documenttype.Title;
                history.CurValue = input.Title;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsDocumentRequest != documenttype.IsDocumentRequest)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "IsDocumentRequest";
                history.PrevValue = documenttype.IsDocumentRequest.ToString();
                history.CurValue = input.IsDocumentRequest.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, documenttype);

        }

        public async Task Delete(EntityDto input)
        {
            
            var Name = _wholeSaleLeadDocumenttypeRepository.Get(input.Id).Title;
            await _wholeSaleLeadDocumenttypeRepository.DeleteAsync(input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 2;
            dataVaultLog.ActionNote = "Document Type Deleted : " + Name;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);
        }

        public async Task<GetWholeSaleLeadDocumentTypeforEditOutput> GetWholeSaleLeadDocumentTypeForEdit(EntityDto input)
        {
            var wholeSaleLeadDocument = await _wholeSaleLeadDocumenttypeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetWholeSaleLeadDocumentTypeforEditOutput { CreateorEditWholeSaleLeadDocumentTypeDto = ObjectMapper.Map<CreateOrEditWholeSaleLeadDocumentTypeDto>(wholeSaleLeadDocument) };

            return output;
        }
    }
}
