﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.Jobs.Importing.Dto;

namespace TheSolarProduct.Jobs.Importing
{
   public interface IInvalidSTCExporter
    {
        FileDto ExportToFile(List<ImportStcDto> stcListDtos);
    }
}
