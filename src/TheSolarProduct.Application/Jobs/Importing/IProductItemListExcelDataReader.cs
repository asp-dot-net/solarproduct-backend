﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Jobs.Importing.Dto;

namespace TheSolarProduct.Jobs.Importing
{
	public interface IProductItemListExcelDataReader : ITransientDependency
	{
		List<ImportProductItemDto> GetProductItemFromExcel(byte[] fileBytes);

		List<ImportProductItemDto> GetProductItemFromExcelPanel(byte[] fileBytes);
	}
}
