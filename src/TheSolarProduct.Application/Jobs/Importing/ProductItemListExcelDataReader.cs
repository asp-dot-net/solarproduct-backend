﻿using Abp.Localization;
using Abp.Localization.Sources;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Importing.Dto;

namespace TheSolarProduct.Jobs.Importing
{
	public class ProductItemListExcelDataReader : NpoiExcelImporterBase<ImportProductItemDto>, IProductItemListExcelDataReader
	{
		private readonly ILocalizationSource _localizationSource;

		public ProductItemListExcelDataReader(ILocalizationManager localizationManager)
		{
			_localizationSource = localizationManager.GetSource(TheSolarProductConsts.LocalizationSourceName);
		}

		public List<ImportProductItemDto> GetProductItemFromExcel(byte[] fileBytes)
		{
			return ProcessExcelFile(fileBytes, ProcessExcelRow);
		}

		public List<ImportProductItemDto> GetProductItemFromExcelPanel(byte[] fileBytes)
		{
			return ProcessExcelFile(fileBytes, ProcessExcelRowPanel);
		}

		private ImportProductItemDto ProcessExcelRow(ISheet worksheet, int row)
		{
			if (IsRowEmpty(worksheet, row))
			{
				return null;
			}

			var exceptionMessage = new StringBuilder();
			var ProductItem = new ImportProductItemDto();

			IRow roww = worksheet.GetRow(row);
			List<ICell> cells = roww.Cells;
			List<string> rowData = new List<string>();
			List<DateTime> rowDataDate = new List<DateTime>();

			for (int colNumber = 0; colNumber < roww.LastCellNum; colNumber++)
			{
				ICell cell = roww.GetCell(colNumber, MissingCellPolicy.CREATE_NULL_AS_BLANK);
				rowData.Add(cell.ToString());
			}

			try
			{
				ProductItem.Manufacturer = rowData[0];
				ProductItem.Series = rowData[1];
				ProductItem.Model = rowData[2];
				ProductItem.ACPower = rowData[3];
				ProductItem.ApprovedDate = rowDataDate[4];
				ProductItem.ExpiryDate = rowDataDate[5];
			}
			catch (System.Exception exception)
			{
				//ProductItem.Exception = exception.Message;
			}

			return ProductItem;
		}

		private ImportProductItemDto ProcessExcelRowPanel(ISheet worksheet, int row)
		{
			if (IsRowEmpty(worksheet, row))
			{
				return null;
			}

			var exceptionMessage = new StringBuilder();
			var ProductItem = new ImportProductItemDto();

			IRow roww = worksheet.GetRow(row);
			List<ICell> cells = roww.Cells;
			List<string> rowData = new List<string>();
			List<DateTime> rowDataDate = new List<DateTime>();

			for (int colNumber = 0; colNumber < roww.LastCellNum; colNumber++)
			{
				ICell cell = roww.GetCell(colNumber, MissingCellPolicy.CREATE_NULL_AS_BLANK);
				rowData.Add(cell.ToString());
			}

			try
			{
				ProductItem.Manufacturer = rowData[0];
				ProductItem.Model = rowData[1];
				ProductItem.ApprovedDate = rowDataDate[2];
				ProductItem.ApprovedDate = rowDataDate[3];
				ProductItem.FireTested = rowData[4];
			}
			catch (System.Exception exception)
			{
				//ProductItem.Exception = exception.Message;
			}

			return ProductItem;
		}

		private string GetLocalizedExceptionMessagePart(string parameter)
		{
			return _localizationSource.GetString("{0}IsInvalid", _localizationSource.GetString(parameter)) + "; ";
		}

		private string GetRequiredValueFromRowOrNull(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
		{
			var cellValue = worksheet.GetRow(row).Cells[column].StringCellValue;
			if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue))
			{
				return cellValue;
			}

			exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
			return null;
		}

		private double GetValue(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
		{
			var cellValue = worksheet.GetRow(row).Cells[column].NumericCellValue;

			return cellValue;

		}

		private bool IsRowEmpty(ISheet worksheet, int row)
		{
			var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
			return cell == null || string.IsNullOrWhiteSpace(cell.StringCellValue);
		}
	}
}
