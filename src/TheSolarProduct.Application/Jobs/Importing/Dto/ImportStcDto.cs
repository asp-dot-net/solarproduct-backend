﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Jobs.Importing.Dto
{
 public  class ImportStcDto
    {
        public string ManualQuote { get; set; }

        public string PVDNumber { get; set; }
        public string JobNumber { get; set; }

        public string ProjectNumber { get; set; }

        public int? PVDStatus { get; set; }

        public virtual DateTime? STCAppliedDate { get; set; }
        public string Exception { get; set; }

        public bool CanBeImported()
        {
            return string.IsNullOrEmpty(Exception);
        }
    }
}
