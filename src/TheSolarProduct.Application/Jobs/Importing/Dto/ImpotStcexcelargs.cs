﻿using Abp;
using System;
using System.Collections.Generic;
using System.Text;
namespace TheSolarProduct.Jobs.Importing.Dto
{
  public  class ImpotStcexcelargs
	{
		public int? TenantId { get; set; }

		public Guid BinaryObjectId { get; set; }

		public UserIdentifier User { get; set; }
		//public int? User { get; set; }
		//public string UserName { get; set; }

		public int OrganizationId { get; set; }
		public byte[] FileBytes { get; set; }

	}
}
