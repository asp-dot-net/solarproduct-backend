﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheSolarProduct.Jobs.Importing.Dto
{
	public class ImportProductItemDto
	{
		public string Name { get; set; }

		public string Manufacturer { get; set; }

		public string Model { get; set; }

		public string Series { get; set; }

		public DateTime? ApprovedDate { get; set; }

		public DateTime? ExpiryDate { get; set; }

		public string FireTested { get; set; }

		public string ACPower { get; set; }

		public bool IsActive { get; set; }

		public bool AUSSalesTag { get; set; }

		public bool NSWSalesTag { get; set; }

		public bool NTSalesTag { get; set; }

		public bool QLDSalesTag { get; set; }

		public bool SASalesTag { get; set; }

		public bool TASSalesTag { get; set; }

		public bool VICSalesTag { get; set; }

		public bool WASalesTag { get; set; }

		public int ProductTypeId { get; set; }


		public string Exception { get; set; }

		public bool CanBeImported()
		{
			return string.IsNullOrEmpty(Exception);
		}
	}
}
