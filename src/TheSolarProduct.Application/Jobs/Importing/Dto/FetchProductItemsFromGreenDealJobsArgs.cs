﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp;

namespace TheSolarProduct.Jobs.Importing.Dto
{
    public class FetchProductItemsFromGreenDealJobsArgs
    {
        public int? TenantId { get; set; }

        public UserIdentifier User { get; set; }
    }
}
