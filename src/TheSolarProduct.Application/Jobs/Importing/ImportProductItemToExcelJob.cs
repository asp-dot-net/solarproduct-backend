﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.Threading;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Jobs.Importing.Dto;
using TheSolarProduct.Notifications;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Importing
{
	public class ImportProductItemToExcelJob : BackgroundJob<ImportProductItemsFromExcelJobArgs>, ITransientDependency
	{
		private readonly IAppNotifier _appNotifier;
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private readonly IInvalidProductItemExporter _invalidProductItemExporter;
		private readonly IBinaryObjectManager _binaryObjectManager;
		private readonly IProductItemListExcelDataReader _productItemListExcelDataReader;
		private readonly IRepository<ProductItem> _productItemSourceRepository;
		private readonly IRepository<ProductType> _productTypeSourceRepository;

		public ImportProductItemToExcelJob(IAppNotifier appNotifier
			, IUnitOfWorkManager unitOfWorkManager
			, IInvalidProductItemExporter invalidProductItemExporter
			, IBinaryObjectManager binaryObjectManager
			, IProductItemListExcelDataReader productItemListExcelDataReader
			, IRepository<ProductItem> productItemSourceRepository
			, IRepository<ProductType> productTypeSourceRepository)
		{
			_appNotifier = appNotifier;
			_unitOfWorkManager = unitOfWorkManager;
			_invalidProductItemExporter = invalidProductItemExporter;
			_binaryObjectManager = binaryObjectManager;
			_productItemListExcelDataReader = productItemListExcelDataReader;
			_productItemSourceRepository = productItemSourceRepository;
			_productTypeSourceRepository = productTypeSourceRepository;
		}

		[UnitOfWork]
		public override void Execute(ImportProductItemsFromExcelJobArgs args)
		{
			var ProductItems = GetProductItemListFromExcelOrNull(args);
			if (ProductItems == null || !ProductItems.Any())
			{
				SendInvalidExcelNotification(args);
				return;
			}
			else
			{
				var invalidProductItems = new List<ImportProductItemDto>();

				foreach (var ProductItem in ProductItems)
				{
					if (ProductItem.CanBeImported())
					{
						try
						{
							AsyncHelper.RunSync(() => CreateProductItemAsync(ProductItem, args));
						}
						catch (UserFriendlyException exception)
						{
							ProductItem.Exception = exception.Message;
							invalidProductItems.Add(ProductItem);
						}
						catch (Exception e)
						{
							ProductItem.Exception = e.ToString();
							invalidProductItems.Add(ProductItem);
						}
					}
					else
					{
						invalidProductItems.Add(ProductItem);
					}
				}

				using (var uow = _unitOfWorkManager.Begin())
				{
					using (CurrentUnitOfWork.SetTenantId(args.TenantId))
					{
						AsyncHelper.RunSync(() => ProcessImportProductItemsResultAsync(args, invalidProductItems));
					}

					uow.Complete();
				}
			}
		}

		private List<ImportProductItemDto> GetProductItemListFromExcelOrNull(ImportProductItemsFromExcelJobArgs args)
		{
			using (var uow = _unitOfWorkManager.Begin())
			{
				using (CurrentUnitOfWork.SetTenantId(args.TenantId))
				{
					try
					{
						var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
						if(args.ProductTypeId == "panel")
						{
							return _productItemListExcelDataReader.GetProductItemFromExcelPanel(file.Bytes);
						}
						else
						{
							return _productItemListExcelDataReader.GetProductItemFromExcel(file.Bytes);
						}						
					}
					catch (Exception)
					{
						return null;
					}
					finally
					{
						uow.Complete();
					}
				}
			}
		}

		private async Task CreateProductItemAsync(ImportProductItemDto input, ImportProductItemsFromExcelJobArgs args)
		{
			using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
			{
				var Exist = _productItemSourceRepository.GetAll().Where(e => e.Model.ToLower() == input.Model.ToLower()).Select(e => e.Id).FirstOrDefault();
				if (Exist == 0)
				{
					ProductItem productItem = new ProductItem();
					var Id = 0;
					if (args.ProductTypeId == "panel")
					{
						Id = _productTypeSourceRepository.GetAll().Where(e => e.Name.ToLower() == "panel").Select(e => e.Id).FirstOrDefault();
					}
					else
					{
						Id = _productTypeSourceRepository.GetAll().Where(e => e.Name.ToLower() == "inverter").Select(e => e.Id).FirstOrDefault();
					}
					productItem.ProductTypeId = Id;
					productItem.Model = input.Model;
					productItem.Name = input.Model;
					productItem.Series = input.Series;
					productItem.Manufacturer = input.Manufacturer;
					productItem.ApprovedDate = input.ApprovedDate;
					productItem.ExpiryDate = input.ExpiryDate;
				
					productItem.TenantId = (int)args.TenantId;
					await _productItemSourceRepository.InsertAsync(productItem);
				}
				else
				{
					var ExistData = _productItemSourceRepository.GetAll().Where(e => e.Id == Exist).FirstOrDefault();
					ExistData.Model = input.Model;
					ExistData.Name = input.Model;
					ExistData.Series = input.Series;
					ExistData.Manufacturer = input.Manufacturer;
					ExistData.ApprovedDate = input.ApprovedDate;
					ExistData.ExpiryDate = input.ExpiryDate;
					ExistData.TenantId = (int)args.TenantId;
					
					await _productItemSourceRepository.UpdateAsync(ExistData);
				}

			}
		}

		private void SendInvalidExcelNotification(ImportProductItemsFromExcelJobArgs args)
		{
			using (var uow = _unitOfWorkManager.Begin())
			{
				using (CurrentUnitOfWork.SetTenantId(args.TenantId))
				{
					AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
						args.User,
						new LocalizableString("FileCantBeConvertedToProductItem", TheSolarProductConsts.LocalizationSourceName),
						null,
						Abp.Notifications.NotificationSeverity.Warn));
				}
				uow.Complete();
			}
		}

		private async Task ProcessImportProductItemsResultAsync(ImportProductItemsFromExcelJobArgs args,
			List<ImportProductItemDto> invalidProductItems)
		{
			if (invalidProductItems.Any())
			{
				var file = _invalidProductItemExporter.ExportToFile(invalidProductItems);
				await _appNotifier.SomeProductItemsCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
			}
			else
			{
				await _appNotifier.SendMessageAsync(
					args.User,
					new LocalizableString("AllProductItemsSuccessfullyImportedFromExcel", TheSolarProductConsts.LocalizationSourceName),
					null,
					Abp.Notifications.NotificationSeverity.Success);
			}
		}
	}
}
