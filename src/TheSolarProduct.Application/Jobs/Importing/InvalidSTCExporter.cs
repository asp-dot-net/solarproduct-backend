﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Jobs.Importing.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Importing
{
    public class InvalidSTCExporter : NpoiExcelExporterBase, IInvalidSTCExporter, ITransientDependency
    {
        public InvalidSTCExporter(ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager)
        {
        }

        public FileDto ExportToFile(List<ImportStcDto> stcListDtos)
        {

            return CreateExcelPackage(
         "InvalidSTCS.xlsx",
          excelPackage =>
          {
              var sheet = excelPackage.CreateSheet("InvalidSTC");

              AddHeader(
                    sheet,
                    L("PVDNumber"),
                    L("ProjectNumber"),
                    L("ManualQuoteNumber"),
                    L("ProjManual"),
                    L("PVDStatus"),
                    L("STCApplied")
                );

              AddObjects(
                    sheet, 2, stcListDtos,
                    _ => _.PVDNumber,
                    _ => _.ProjectNumber,
                    _ => _.ManualQuote,
                    _ => null,
                    _ => _.PVDStatus,
                    _ => _.STCAppliedDate,
                    _ => _.Exception
                );
          });

        }
    }
}
