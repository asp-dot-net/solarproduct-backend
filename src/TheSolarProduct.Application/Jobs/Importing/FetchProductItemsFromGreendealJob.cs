﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.Threading;
using Abp.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Jobs.Importing.Dto;
using TheSolarProduct.Notifications;
using TheSolarProduct.Organizations;
using TheSolarProduct.Storage;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.ThirdPartyApis.GreenDeals.Dtos;

namespace TheSolarProduct.Jobs.Importing
{
    public class FetchProductItemsFromGreendealJob : BackgroundJob<FetchProductItemsFromGreenDealJobsArgs>, ITransientDependency
    {
        private readonly IAppNotifier _appNotifier;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<STCProvider> _stcProviderRepository;
        private readonly IRepository<ProductItem> _productItemRepository;
        private readonly IRepository<ProductType> _productTypeSourceRepository;

        //private static string endPoint = "https://staging.greendeal.com.au/rapi/v1"; // Testing Env
        private static string endPoint = "https://www.greendeal.com.au/rapi/v1"; // Production Env

        public FetchProductItemsFromGreendealJob(IAppNotifier appNotifier
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<STCProvider> stcProviderRepository
            , IRepository<ProductItem> productItemRepository
            , IRepository<ProductType> productTypeSourceRepository
            )
        {
            _appNotifier = appNotifier;
            _unitOfWorkManager = unitOfWorkManager;
            _stcProviderRepository = stcProviderRepository;
            _productItemRepository = productItemRepository;
            _productTypeSourceRepository = productTypeSourceRepository;
        }

        [UnitOfWork]
        public override void Execute(FetchProductItemsFromGreenDealJobsArgs args)
        {
            var productItems = GetProductItemListFromGreenDeal(args);
            if (productItems.Result == null || !productItems.Result.Any())
            {
                SendErrorNotification(args);
                return;
            }
            else
            {
                bool hasError = false;
                foreach (var productItem in productItems.Result)
                {
                    try
                    {
                        AsyncHelper.RunSync(() => CreateOrUpdateProductItemAsync(productItem, args));
                    }
                    catch(Exception e)
                    {
                        hasError = true;
                    }
                }

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                    {
                        AsyncHelper.RunSync(() => ProcessFetchProductItemsResultAsync(args, hasError));
                    }
                    uow.Complete();
                }
            }
        }

        private async Task<List<RootObject>> GetProductItemListFromGreenDeal(FetchProductItemsFromGreenDealJobsArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    try
                    {
                        var stcProvider = await _stcProviderRepository.GetAll().Where(e => e.Provider == "Green Deal").FirstOrDefaultAsync();

                        List<RootObject> rootObject = new List<RootObject>();
                        var rootPanels = await GetCECPanelsAsync(stcProvider.UserId, stcProvider.Password);
                        var rootInverter = await GetCECInvertersAsync(stcProvider.UserId, stcProvider.Password);
                        var rootBattery = await GetCECBattterisAsync(stcProvider.UserId, stcProvider.Password);

                        if (rootPanels != null)
                        {
                            rootPanels.ProductTypeId = 1;
                            rootObject.Add(rootPanels);
                        }

                        if (rootInverter != null)
                        {
                            rootInverter.ProductTypeId = 2;
                            rootObject.Add(rootInverter);
                        }

                        if (rootBattery != null)
                        {
                            rootBattery.ProductTypeId = 5;
                            rootObject.Add(rootBattery);
                        }

                        return rootObject;
                    }
                    catch (Exception)
                    {
                        return new List<RootObject>();
                    }
                    finally
                    {
                        uow.Complete();
                    }
                }
            }
        }

        private async Task CreateOrUpdateProductItemAsync(RootObject input, FetchProductItemsFromGreenDealJobsArgs args)
        {
            using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
            {
                foreach (var item in input.Response.CECProductItems)
                {
                    var ProductTypeId = input.ProductTypeId;

                    if(!string.IsNullOrEmpty(item.Name))
                    {
                        var Exist = _productItemRepository.GetAll().Any(e => e.Model.ToLower() == item.Model.ToLower() && e.ProductTypeId == ProductTypeId);
                        if (!Exist)
                        {
                            //ProductItem productItem = new ProductItem();

                            //productItem.ProductTypeId = ProductTypeId;
                            //productItem.Model = item.Model;
                            //productItem.Name = item.Name;
                            //productItem.Manufacturer = item.Name;

                            //if (!string.IsNullOrEmpty(item.ExpiryDate))
                            //{
                            //    productItem.ExpiryDate = DateTime.ParseExact(item.ExpiryDate, "yyyy-MM-dd", null);
                            //}

                            //if (!string.IsNullOrEmpty(item.Series))
                            //{
                            //    productItem.Series = item.Series;
                            //}

                            //productItem.Active = false;
                            //productItem.ECommerce = false;
                            //productItem.GreenDealBrandId = item.BrandId;

                            //productItem.TenantId = (int)args.TenantId;
                            //await _productItemRepository.InsertAsync(productItem);
                        }
                        else
                        {
                            var existData = _productItemRepository.GetAll().Where(e => e.Model.ToLower() == item.Model.ToLower() && e.ProductTypeId == ProductTypeId).FirstOrDefault();

                            if (!string.IsNullOrEmpty(item.ExpiryDate))
                            {
                                existData.ExpiryDate = DateTime.ParseExact(item.ExpiryDate, "yyyy-MM-dd", null);
                            }

                            existData.GreenDealBrandId = item.BrandId;

                            await _productItemRepository.UpdateAsync(existData);
                        }
                    }
                }
            }
        }

        private void SendErrorNotification(FetchProductItemsFromGreenDealJobsArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(args.User, "Something is wrong from greendeal.", Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }

        private async Task ProcessFetchProductItemsResultAsync(FetchProductItemsFromGreenDealJobsArgs args, bool hasError)
        {
            if (hasError)
            {
                await _appNotifier.SendMessageAsync(args.User, "Something is wrong.", Abp.Notifications.NotificationSeverity.Error);
            }
            else
            {
                await _appNotifier.SendMessageAsync(args.User, "Product Item Fetched Successfully.", Abp.Notifications.NotificationSeverity.Success);
            }
        }

        public static string GetEncryptedSignature(string timestamp, string clientId, string clientSecret)
        {
            string signature_string = "GD:" + clientId + timestamp + clientSecret;

            string hash = BitConverter.ToString(MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(signature_string))).Replace("-", "");

            return hash.ToLower();
        }

        public static string GetTimeStamp(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-ddTHH:mm:ss:z");
        }

        private async Task<RootObject> GetCECPanelsAsync(string clientId, string clientSecret)
        {
            var url = endPoint + "/dictionaries/panel_brands.json";

            var timestamp = GetTimeStamp(DateTime.UtcNow);
            var encryptedSignature = GetEncryptedSignature(timestamp, clientId, clientSecret);

            var temp = new
            {
                client_id = clientId,
                signature = encryptedSignature,
                timestamp = timestamp
            };

            var content = JsonConvert.SerializeObject(temp, Formatting.Indented);
            HttpContent httpContent = new StringContent(content.ToString(), Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("Accept", "*/*");
            request.Content = httpContent;
            var response = await client.SendAsync(request);

            //return response;

            bool IsSuccess = response.IsSuccessStatusCode;

            if (IsSuccess)
            {
                var jsonResponse = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<RootObject>(jsonResponse);
            }
            else
            {
                return null;
            }
        }

        private async Task<RootObject> GetCECInvertersAsync(string clientId, string clientSecret)
        {
            var url = endPoint + "/dictionaries/inverter_brands.json";

            var timestamp = GetTimeStamp(DateTime.UtcNow);
            var encryptedSignature = GetEncryptedSignature(timestamp, clientId, clientSecret);

            var temp = new
            {
                client_id = clientId,
                signature = encryptedSignature,
                timestamp = timestamp
            };

            var content = JsonConvert.SerializeObject(temp, Formatting.Indented);
            HttpContent httpContent = new StringContent(content.ToString(), Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("Accept", "*/*");
            request.Content = httpContent;
            var response = await client.SendAsync(request);

            bool IsSuccess = response.IsSuccessStatusCode;

            if (IsSuccess)
            {
                var jsonResponse = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<RootObject>(jsonResponse);
            }
            else
            {
                return null;
            }
        }

        private async Task<RootObject> GetCECBattterisAsync(string clientId, string clientSecret)
        {
            var url = endPoint + "/dictionaries/battery_storages.json";

            var timestamp = GetTimeStamp(DateTime.UtcNow);
            var encryptedSignature = GetEncryptedSignature(timestamp, clientId, clientSecret);

            var temp = new
            {
                client_id = clientId,
                signature = encryptedSignature,
                timestamp = timestamp
            };

            var content = JsonConvert.SerializeObject(temp, Formatting.Indented);
            HttpContent httpContent = new StringContent(content.ToString(), Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("Accept", "*/*");
            request.Content = httpContent;
            var response = await client.SendAsync(request);

            bool IsSuccess = response.IsSuccessStatusCode;

            if (IsSuccess)
            {
                var jsonResponse = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<RootObject>(jsonResponse);
            }
            else
            {
                return null;
            }
        }
    }
}
