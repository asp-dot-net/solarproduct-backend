﻿using Abp.Localization;
using Abp.Localization.Sources;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Importing.Dto;

namespace TheSolarProduct.Jobs.Importing
{
    public class ImportStcEcelDataReaderJob : NpoiExcelImporterBase<ImportStcDto>, IImportStcEcelDataReaderJob

    {
        private readonly ILocalizationSource _localizationSource;

        public ImportStcEcelDataReaderJob(ILocalizationManager localizationManager)
        {
            _localizationSource = localizationManager.GetSource(TheSolarProductConsts.LocalizationSourceName);
        }

        public List<ImportStcDto> GetStcFromExcel(byte[] fileBytes)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow);
        }

        private ImportStcDto ProcessExcelRow(ISheet worksheet, int row)
        {
            //if (IsRowEmpty(worksheet, row))
            //{
            //    return null;
            //}

            var exceptionMessage = new StringBuilder();
            var invoice = new ImportStcDto();

            IRow roww = worksheet.GetRow(row);
            List<ICell> cells = roww.Cells;
            List<string> rowData = new List<string>();
            List<DateTime> rowDataDate = new List<DateTime>();

            for (int colNumber = 0; colNumber <= 9; colNumber++)
            {
                ICell cell = roww.GetCell(colNumber, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                rowData.Add(cell.ToString());
            }

            try
            {
                invoice.PVDNumber = Convert.ToString(rowData[0]);
                invoice.ProjectNumber = rowData[1];
                invoice.ManualQuote = rowData[2];
                //invoice. = rowData[3];
                invoice.PVDStatus =  Convert.ToInt32(rowData[4]);
                invoice.STCAppliedDate =  Convert.ToDateTime(rowData[5]);
                 
            }

            catch (System.Exception exception)
            {
                invoice.Exception = exception.Message;
            }

            return invoice;
        }

        private string GetLocalizedExceptionMessagePart(string parameter)
        {
            return _localizationSource.GetString("{0}IsInvalid", _localizationSource.GetString(parameter)) + "; ";
        }

        private string GetRequiredValueFromRowOrNull(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].StringCellValue;
            if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue))
            {
                return cellValue;
            }

            exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
            return null;
        }

        private double GetValue(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].NumericCellValue;

            return cellValue;

        }

        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
            return cell == null || string.IsNullOrWhiteSpace(cell.StringCellValue);
        }
    }
}
