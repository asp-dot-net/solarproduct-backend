﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Jobs.Importing.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Importing
{
	public class InvalidProductTypeExporter : NpoiExcelExporterBase, IInvalidProductItemExporter, ITransientDependency
    {
        public InvalidProductTypeExporter(ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager)
        {
        }

        public FileDto ExportToFile(List<ImportProductItemDto> leadListDtos)
        {
            return CreateExcelPackage(
                "InvalidProductItemImportList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("InvalidProductItemImports"));

                    AddHeader(
                        sheet,
                        L("FirstName"),
                        L("LastName"),
                        L("Email"),
                        L("Phone"),
                        L("Mobile"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("Postcode"),
                        L("LeadSource"),
                        L("System"),
                        L("Roof"),
                        L("Angle"),
                        L("Story"),
                        L("HouseAge"),
                        L("Notes")
                    );

                    AddObjects(
                        sheet, 2, leadListDtos
                    );

                    for (var i = 0; i < 8; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
