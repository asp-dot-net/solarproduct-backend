﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Jobs.Importing.Dto;

namespace TheSolarProduct.Jobs.Importing
{
	public interface IImportStcEcelDataReaderJob : ITransientDependency
	{
		List<ImportStcDto> GetStcFromExcel(byte[] fileBytes);
	}
}

