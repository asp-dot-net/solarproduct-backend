﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.ObjectMapping;
using Abp.Runtime.Session;
using Abp.Threading;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheSolarProduct.Storage;
using TheSolarProduct.Notifications;
using System.Linq.Dynamic.Core;
using TheSolarProduct.Authorization.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.RECDatas;
using TheSolarProduct.Invoices.Importing;
using TheSolarProduct.Invoices.Importing.Dto;
using TheSolarProduct.LeadActivityLogs;

namespace TheSolarProduct.Jobs.Importing
{
    public class ImportInstallationToExcel : BackgroundJob<ImportInstallationFromExcelArgs>, ITransientDependency
    {
        private readonly IInstallationInvoiceListExcelDataReader _installationInvoiceListExcelDataReader;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IObjectMapper _objectMapper;
        private readonly IAppNotifier _appNotifier;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly InvalidInstallationInvoiceExporter _invalidInstallationExporter;
        private readonly IAbpSession _abpSession;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Job> _jobRepository;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<InstallerInvoice> _jobInstallerInvoiceRepository;
        private readonly IRepository<Organizations.ExtendOrganizationUnit, long> _extendOrganizationUnitRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;

        public ImportInstallationToExcel(
            IInstallationInvoiceListExcelDataReader installationInvoiceListExcelDataReader,
            IBinaryObjectManager binaryObjectManager,
            IObjectMapper objectMapper,
            IAppNotifier appNotifier,
            IUnitOfWorkManager unitOfWorkManager,
            InvalidInstallationInvoiceExporter invalidInstallationExporter,
            IAbpSession abpSession,
            IRepository<User, long> userRepository,
            //IRepository<InvoicePayment> invoicePaymentRepository,
            IRepository<Job> jobRepository,
            //IRepository<InvoicePaymentMethod, int> lookup_invoicePaymentMethodRepository,
            //IRepository<InvoiceStatus> invoiceStatusRepository,
            //IRepository<InvoiceImportData> invoiceImportDataRepository,
            IWebHostEnvironment env,
            IRepository<Tenant> tenantRepository,
            //IRepository<InvoiceFile> InvoiceFilesRepository,
            IRepository<Organizations.ExtendOrganizationUnit, long> extendOrganizationUnitRepository
            ,IRepository<InstallerInvoice> jobInstallerInvoiceRepository
            ,IRepository<LeadActivityLog> leadactivityRepository
            )
        {
            _installationInvoiceListExcelDataReader = installationInvoiceListExcelDataReader;
            _binaryObjectManager = binaryObjectManager;
            _objectMapper = objectMapper;
            _appNotifier = appNotifier;
            _invalidInstallationExporter = invalidInstallationExporter;
            _abpSession = abpSession;
            _userRepository = userRepository;
            _unitOfWorkManager = unitOfWorkManager;
            //_invoicePaymentRepository = invoicePaymentRepository;
            _jobRepository = jobRepository;
            //_lookup_invoicePaymentMethodRepository = lookup_invoicePaymentMethodRepository;
            //_invoiceStatusRepository = invoiceStatusRepository;
            //_invoiceImportDataRepository = invoiceImportDataRepository;
            _env = env;
            _tenantRepository = tenantRepository;
            //_InvoiceFilesRepository = InvoiceFilesRepository;
            _extendOrganizationUnitRepository = extendOrganizationUnitRepository;
            _jobInstallerInvoiceRepository = jobInstallerInvoiceRepository;
            _leadactivityRepository = leadactivityRepository;
        }

        [UnitOfWork]
        public override void Execute(ImportInstallationFromExcelArgs args)
        {

            //var fileObject = new BinaryObject(args.TenantId, args.FileBytes);
            //_binaryObjectManager.SaveAsync(fileObject);

            var Installations = GetInstallationListFromExcelOrNull(args);
            if (Installations == null || !Installations.Any())
            {
                SendInvalidExcelNotification(args);
                return;
            }
            else
            {
                var invalidInstallation = new List<ImportInstalltionInvoiceDto>();

                foreach (var Installation in Installations)
                {
                    if (Installation.CanBeImported())
                    {
                        try
                        {
                            AsyncHelper.RunSync(() => CreateInvoicesAsync(Installation, args));
                        }
                        catch (UserFriendlyException exception)
                        {
                            invalidInstallation.Add(Installation);
                        }
                        catch (Exception e)
                        {
                            invalidInstallation.Add(Installation);
                        }
                    }
                    else
                    {
                        invalidInstallation.Add(Installation);
                    }
                }

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                    {
                        AsyncHelper.RunSync(() => ProcessImportInstallationResultAsync(args, invalidInstallation));
                    }
                    uow.Complete();
                }
            }
        }

        private List<ImportInstalltionInvoiceDto> GetInstallationListFromExcelOrNull(ImportInstallationFromExcelArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    try
                    {
                        var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
                        return _installationInvoiceListExcelDataReader.GetInstallationInvoiceFromExcel(file.Bytes);
                    }
                    catch (Exception e)
                    {
                        return null;
                    }
                    finally
                    {
                        uow.Complete();
                    }
                }
            }
        }

        private async Task CreateInvoicesAsync(ImportInstalltionInvoiceDto input, ImportInstallationFromExcelArgs args)
        {
            //Job lead = new Job();
            //var Suburb = 0;
            //var State = 0;
            //var leadSourceId = 0;
            //RECData recData = new RECData();
            using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
            {
                var recData = await _jobInstallerInvoiceRepository.GetAll().Include(e => e.JobFk).Where(e => e.JobFk.JobNumber == input.JobNumber.Trim().ToUpper() && e.InvoiceNo.ToUpper() == input.InvoiceNo.Trim().ToUpper()).FirstOrDefaultAsync();

                if (recData != null)
                {
                    //recData.InvoiceNo = input.InvoiceNo;
                    recData.Date = input.Date;
                    recData.BankRefNo = input.BankRefrenceNo;
                    recData.Remarks = input.Remark;
                    await _jobInstallerInvoiceRepository.UpdateAsync(recData);

                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 26;
                    leadactivity.SectionId = 30;
                    leadactivity.ActionNote = "Installer Invoice Paid";
                    leadactivity.LeadId = (int)recData.JobFk.LeadId;
                    leadactivity.TenantId = (int)args.TenantId;
                    var activityId = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);
                }
               
            }
        }

        private void SendInvalidExcelNotification(ImportInstallationFromExcelArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
                        args.User,
                        new LocalizableString("FileCantBeConvertedToInstallerPayment", TheSolarProductConsts.LocalizationSourceName),
                        null,
                        Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }

        private async Task ProcessImportInstallationResultAsync(ImportInstallationFromExcelArgs args,
          List<ImportInstalltionInvoiceDto> invalidInstallation)
        {
            if (invalidInstallation.Any())
            {
                var file = _invalidInstallationExporter.ExportToFile(invalidInstallation);
                await _appNotifier.SomeInstallationCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
            }
            else
            {
                await _appNotifier.SendMessageAsync(
                    args.User,
                    new LocalizableString("AllInstallerInvoicePaymentSuccessfullyImportedFromExcel", TheSolarProductConsts.LocalizationSourceName),
                    null,
                    Abp.Notifications.NotificationSeverity.Success);
            }
        }
    }
}
