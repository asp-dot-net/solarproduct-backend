﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.Jobs.Importing.Dto;

namespace TheSolarProduct.Jobs.Importing
{
	public interface IInvalidProductItemExporter
	{
		FileDto ExportToFile(List<ImportProductItemDto> productItemListDtos);
	}
}
