﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.ObjectMapping;
using Abp.Runtime.Session;
using Abp.Threading;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Importing.Dto;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Leads;
using TheSolarProduct.Leads.Importing;
using TheSolarProduct.LeadSources;
using TheSolarProduct.Notifications;
using TheSolarProduct.PostCodes;
using TheSolarProduct.States;
using TheSolarProduct.Storage;
using System.Linq.Dynamic.Core;
using TheSolarProduct.Invoices;
using Microsoft.AspNetCore.Hosting;

namespace TheSolarProduct.Jobs.Importing
{
    public class ImportStcExcellJob : BackgroundJob<ImpotStcexcelargs>, ITransientDependency
    {
		 
		private readonly IBinaryObjectManager _binaryObjectManager;
		private readonly IObjectMapper _objectMapper;
		private readonly IAppNotifier _appNotifier;
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private readonly IAbpSession _abpSession;
		private readonly IRepository<User, long> _userRepository;
		private readonly IRepository<Job> _jobRepository;
		private readonly IRepository<Lead> _leadRepository;
		private readonly IRepository<LeadSource> _leadSourceRepository;
		private readonly IRepository<LeadActivityLog> _leadactivityRepository;
		private readonly IRepository<PostCode> _postCodeRepository;
		private readonly IRepository<State> _stateRepository;
		private readonly IImportStcEcelDataReaderJob _ImportStcEcelJobDataReader;  
		private readonly IStcExcelFileExporter _invalidStcExporter; 
		private readonly ILeadsAppService _leadServiceRepository;
        private readonly IInvalidSTCExporter _invalidSTCExporter;

        public ImportStcExcellJob(
			   IRepository<Lead> leadRepository,
			   IRepository<LeadSource> leadSourceRepository,
			   IRepository<LeadActivityLog> leadactivityRepository,
			   IRepository<PostCode> postCodeRepository,
			   IRepository<State> stateRepository,
			   IImportStcEcelDataReaderJob ImportStcEcelJobDataReader,
			   IBinaryObjectManager binaryObjectManager,
			   IObjectMapper objectMapper,
			   IAppNotifier appNotifier,
			   IUnitOfWorkManager unitOfWorkManager,
			   IStcExcelFileExporter invalidStcExporter,
			   IAbpSession abpSession,
			   ILeadsAppService leadServiceRepository
			   , IRepository<User, long> userRepository,
			   IRepository<Job> jobRepository, IInvalidSTCExporter invalidSTCExporter
               )
		{
			_leadRepository = leadRepository;
			_leadSourceRepository = leadSourceRepository;
			_leadactivityRepository = leadactivityRepository;
			_postCodeRepository = postCodeRepository;
			_stateRepository = stateRepository;
			_ImportStcEcelJobDataReader = ImportStcEcelJobDataReader;
			_binaryObjectManager = binaryObjectManager;
			_objectMapper = objectMapper;
			_appNotifier = appNotifier;
			_unitOfWorkManager = unitOfWorkManager;
			_invalidStcExporter =  invalidStcExporter;
			_abpSession = abpSession;
			_leadServiceRepository = leadServiceRepository;
			_userRepository = userRepository;
			_jobRepository = jobRepository;
            _invalidSTCExporter = invalidSTCExporter;

        }

		[UnitOfWork]
        public override void Execute(ImpotStcexcelargs args)
        {
            //var fileObject = new BinaryObject(args.TenantId, args.FileBytes);
            //_binaryObjectManager.SaveAsync(fileObject);

            var stcs = GetSTCListFromExcelOrNull(args);
            if (stcs == null || !stcs.Any())
            {
                SendInvalidExcelNotification(args);
                return;
            }
            else
            {
                var invalidstc = new List<ImportStcDto>();

                foreach (var stc in stcs)
                {
                    if (stc.CanBeImported())
                    {
                        try
                        {
                            AsyncHelper.RunSync(() => CreateInvoicesAsync(stc, args));
                        }
                        catch (UserFriendlyException exception)
                        {
                            invalidstc.Add(stc);
                        }
                        catch (Exception e)
                        {
                            invalidstc.Add(stc);
                        }
                    }
                    else
                    {
                        invalidstc.Add(stc);
                    }
                }



                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                    {
                        AsyncHelper.RunSync(() => ProcessImportSTCResultAsync(args, invalidstc));
                    }
                    uow.Complete();
                }
            }
        }

        private List<ImportStcDto> GetSTCListFromExcelOrNull(ImpotStcexcelargs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    try
                    {
                        var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
                        return _ImportStcEcelJobDataReader.GetStcFromExcel(file.Bytes);
                    }
                    catch (Exception e)
                    {
                        return null;
                    }
                    finally
                    {
                        uow.Complete();
                    }
                }
            }
        }

        private async Task CreateInvoicesAsync(ImportStcDto input, ImpotStcexcelargs args)
        {
            Job lead = new Job();
            //var Suburb = 0;
            //var State = 0;
            var leadSourceId = 0;
            using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
            {

                if (input.ProjectNumber != null)
                {
                    lead = await _jobRepository.GetAll().Where(e => e.JobNumber.ToUpper() == input.ProjectNumber.ToUpper()).FirstOrDefaultAsync();
                }
                if (lead == null)
                {
                    input.Exception = "ProjectNo Not Found";
                    throw new Exception("ProjectNo Not Found");
                }
                lead.TenantId = (int)args.TenantId;
                lead.PVDNumber = input.PVDNumber;
                lead.PVDStatus = input.PVDStatus;


                lead.STCAppliedDate = input.STCAppliedDate;
                await _jobRepository.UpdateAsync(lead);

            }
        }

        private void SendInvalidExcelNotification(ImpotStcexcelargs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
                        args.User,
                        new LocalizableString("FileCantBeConvertedToLead", TheSolarProductConsts.LocalizationSourceName),
                        null,
                        Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }

        private async Task ProcessImportSTCResultAsync(ImpotStcexcelargs args,
          List<ImportStcDto> invalidstc)
        {
            if (invalidstc.Any())
            {
                var file = _invalidSTCExporter.ExportToFile(invalidstc);
                await _appNotifier.SomeSTCCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
            }
            else
            {
                await _appNotifier.SendMessageAsync(
                    args.User,
                    new LocalizableString("AllSTCSuccessfullyImportedFromExcel", TheSolarProductConsts.LocalizationSourceName),
                    null,
                    Abp.Notifications.NotificationSeverity.Success);
            }
        }
    }
}
