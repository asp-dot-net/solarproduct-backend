﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.HoldReasons;

namespace TheSolarProduct.Jobs
{
    [AbpAuthorize(AppPermissions.Pages_RefundReasons, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    public class RefundReasonsAppService : TheSolarProductAppServiceBase, IRefundReasonsAppService
    {
        private readonly IRepository<RefundReason> _refundReasonRepository;
        private readonly IRefundReasonsExcelExporter _refundReasonsExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public RefundReasonsAppService(IRepository<RefundReason> refundReasonRepository, IRefundReasonsExcelExporter refundReasonsExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _refundReasonRepository = refundReasonRepository;
            _refundReasonsExcelExporter = refundReasonsExcelExporter;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;

        }

        public async Task<PagedResultDto<GetRefundReasonForViewDto>> GetAll(GetAllRefundReasonsInput input)
        {

            var filteredRefundReasons = _refundReasonRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

            var pagedAndFilteredRefundReasons = filteredRefundReasons
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var refundReasons = from o in pagedAndFilteredRefundReasons
                                select new GetRefundReasonForViewDto()
                                {
                                    RefundReason = new RefundReasonDto
                                    {
                                        Name = o.Name,
                                        Id = o.Id,
                                        IsActive = o.IsActive,
                                    }
                                };

            var totalCount = await filteredRefundReasons.CountAsync();

            return new PagedResultDto<GetRefundReasonForViewDto>(
                totalCount,
                await refundReasons.ToListAsync()
            );
        }

        public async Task<GetRefundReasonForViewDto> GetRefundReasonForView(int id)
        {
            var refundReason = await _refundReasonRepository.GetAsync(id);

            var output = new GetRefundReasonForViewDto { RefundReason = ObjectMapper.Map<RefundReasonDto>(refundReason) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_RefundReasons_Edit)]
        public async Task<GetRefundReasonForEditOutput> GetRefundReasonForEdit(EntityDto input)
        {
            var refundReason = await _refundReasonRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetRefundReasonForEditOutput { RefundReason = ObjectMapper.Map<CreateOrEditRefundReasonDto>(refundReason) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditRefundReasonDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_RefundReasons_Create)]
        protected virtual async Task Create(CreateOrEditRefundReasonDto input)
        {
            var refundReason = ObjectMapper.Map<RefundReason>(input);

            if (AbpSession.TenantId != null)
            {
                refundReason.TenantId = (int)AbpSession.TenantId;
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 8;
            dataVaultLog.ActionNote = "Refund Reason Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _refundReasonRepository.InsertAsync(refundReason);
        }

        [AbpAuthorize(AppPermissions.Pages_RefundReasons_Edit)]
        protected virtual async Task Update(CreateOrEditRefundReasonDto input)
        {
            var refundReason = await _refundReasonRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 8;
            dataVaultLog.ActionNote = "Refund Reason Updated : " + refundReason.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != refundReason.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = refundReason.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.IsActive != refundReason.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = refundReason.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, refundReason);
        }

        [AbpAuthorize(AppPermissions.Pages_RefundReasons_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _refundReasonRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 8;
            dataVaultLog.ActionNote = "Refund Reason Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _refundReasonRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetRefundReasonsToExcel(GetAllRefundReasonsForExcelInput input)
        {

            var filteredRefundReasons = _refundReasonRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

            var query = (from o in filteredRefundReasons
                         select new GetRefundReasonForViewDto()
                         {
                             RefundReason = new RefundReasonDto
                             {
                                 Name = o.Name,
                                 Id = o.Id,
                                 IsActive = o.IsActive,
                             }
                         });

            var refundReasonListDtos = await query.ToListAsync();

            return _refundReasonsExcelExporter.ExportToFile(refundReasonListDtos);
        }

    }
}