﻿using TheSolarProduct.Jobs;
using System.Collections.Generic;


using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Leads;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Invoices;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.LeadActivityLogs;
using Abp.Net.Mail;
using System.Net.Mail;
using TheSolarProduct.SmsTemplates;
using TheSolarProduct.EmailTemplates;
using Abp.Timing.Timezone;
using TheSolarProduct.JobHistory;
using TheSolarProduct.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using Abp.Domain.Uow;
using TheSolarProduct.VoucherMasters;

namespace TheSolarProduct.Jobs
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class JobPromotionsAppService : TheSolarProductAppServiceBase, IJobPromotionsAppService
	{
		private readonly IRepository<JobPromotion> _jobPromotionRepository;
		private readonly IJobPromotionsExcelExporter _jobPromotionsExcelExporter;
		private readonly IRepository<Job, int> _lookup_jobRepository;
		private readonly IRepository<Lead, int> _lookup_leadRepository;
		private readonly IRepository<PromotionMaster, int> _lookup_promotionMasterRepository;
		private readonly IRepository<JobStatus, int> _lookup_jobStatusRepository;
		private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
		private readonly IRepository<FreebieTransport> _freebieTransportRepository;
		private readonly UserManager _userManager;
		private readonly IRepository<User, long> _userRepository;
		private readonly IRepository<UserTeam> _userTeamRepository;
		private readonly IApplicationSettingsAppService _applicationSettings;
		private readonly IRepository<User, long> _lookup_userRepository;
		private readonly IRepository<LeadActivityLog> _leadactivityRepository;
		private readonly IEmailSender _emailSender;
		private readonly IRepository<SmsTemplate> _smsTemplateRepository;
		private readonly IRepository<EmailTemplate> _emailTemplateRepository;
		private readonly ITimeZoneConverter _timeZoneConverter;
		private readonly IRepository<InvoiceImportData> _invoiceImportDataRepository;
		private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<VoucherMaster> _voucherRepository;

        public JobPromotionsAppService(IRepository<JobPromotion> jobPromotionRepository,
			IJobPromotionsExcelExporter jobPromotionsExcelExporter
			, IRepository<Job, int> lookup_jobRepository
			, IRepository<PromotionMaster, int> lookup_promotionMasterRepository
			, IRepository<Lead, int> lookup_leadRepository
			, IRepository<JobStatus, int> lookup_jobStatusRepository
			, IRepository<InvoicePayment> invoicePaymentRepository
			, IRepository<FreebieTransport> freebieTransportRepository
			, UserManager userManager
			, IRepository<User, long> userRepository
			, IRepository<UserTeam> userTeamRepository
			, IApplicationSettingsAppService applicationSettings
			, IRepository<User, long> lookup_userRepository
			, IRepository<LeadActivityLog> leadactivityRepository,
            IRepository<VoucherMaster> voucherRepository,

            IEmailSender emailSender,
			IRepository<SmsTemplate> smsTemplateRepository,
			IRepository<EmailTemplate> emailTemplateRepository,
			ITimeZoneConverter timeZoneConverter,
			IRepository<InvoiceImportData> invoiceImportDataRepository,
			IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
		{
			_jobPromotionRepository = jobPromotionRepository;
			_jobPromotionsExcelExporter = jobPromotionsExcelExporter;
			_lookup_jobRepository = lookup_jobRepository;
			_lookup_promotionMasterRepository = lookup_promotionMasterRepository;
			_lookup_leadRepository = lookup_leadRepository;
			_lookup_jobStatusRepository = lookup_jobStatusRepository;
			_invoicePaymentRepository = invoicePaymentRepository;
			_freebieTransportRepository = freebieTransportRepository;
			_userManager = userManager;
			_userRepository = userRepository;
			_userTeamRepository = userTeamRepository;
			_applicationSettings = applicationSettings;
			_lookup_userRepository = lookup_userRepository;
			_leadactivityRepository = leadactivityRepository;
			_emailSender = emailSender;
			_smsTemplateRepository = smsTemplateRepository;
			_emailTemplateRepository = emailTemplateRepository;
			_timeZoneConverter = timeZoneConverter;
			_invoiceImportDataRepository = invoiceImportDataRepository;
			_dbcontextprovider = dbcontextprovider;
			_voucherRepository = voucherRepository;
		}

		public async Task<List<GetJobPromotionForEditOutput>> GetJobPromotionByJobId(int jobid)
		{
			var jobProduct = _jobPromotionRepository.GetAll().Where(x => x.JobId == jobid).ToList();

			var output = new List<GetJobPromotionForEditOutput>();

			foreach (var item in jobProduct)
			{
				var outobj = new GetJobPromotionForEditOutput();
				outobj.JobPromotion = ObjectMapper.Map<CreateOrEditJobPromotionDto>(item);
				outobj.JobPromotion.Id = item.Id;
				if (outobj.JobPromotion.PromotionMasterId != null)
				{
					var _lookupProductItem = await _lookup_promotionMasterRepository.FirstOrDefaultAsync((int)outobj.JobPromotion.PromotionMasterId);
					outobj.PromotionMasterName = _lookupProductItem?.Name?.ToString();
				}

				if (outobj.JobPromotion.JobId > 0)
				{
					var _lookupJob = await _lookup_jobRepository.FirstOrDefaultAsync((int)outobj.JobPromotion.JobId);
					outobj.JobStreetName = _lookupJob?.StreetName?.ToString();
				}
				output.Add(outobj);
			}
			return output;
		}

		public async Task<PagedResultDto<GetJobPromotionForViewDto>> GetAll(GetAllJobPromotionsInput input)
		{
			var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
			var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
			var user_list = _userRepository.GetAll();
			var user_teamList = _userTeamRepository.GetAll();
			var User = user_list.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
			IList<string> role = await _userManager.GetRolesAsync(User);
			var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.SectionId == 2);

			var TeamId = user_teamList.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
			var UserList = user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

			var filteredJobPromotions = _jobPromotionRepository.GetAll()
						.Include(e => e.JobFk)
						.Include(e => e.PromotionMasterFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.JobFk.LeadFk.Mobile.Contains(input.Filter) || e.JobFk.JobNumber.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.ProjectNumber), e => e.JobFk != null && e.JobFk.JobNumber.Contains(input.ProjectNumber))
						.WhereIf(!string.IsNullOrWhiteSpace(input.Address), e => e.JobFk != null && e.JobFk.Address.Contains(input.ProjectNumber))
						.WhereIf(!string.IsNullOrWhiteSpace(input.CompanyName), e => e.JobFk != null && e.JobFk.LeadFk.CompanyName.Contains(input.CompanyName))
						.WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.JobFk.State == input.stateNameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Email), e => e.JobFk != null && e.JobFk.LeadFk.Email.Contains(input.Email))
						.WhereIf(!string.IsNullOrWhiteSpace(input.MobileNo), e => e.JobFk != null && e.JobFk.LeadFk.Mobile.Contains(input.MobileNo))
						.WhereIf(!string.IsNullOrWhiteSpace(input.MobileNo), e => e.JobFk != null && e.JobFk.LeadFk.Mobile.Contains(input.MobileNo))
						.WhereIf(!string.IsNullOrWhiteSpace(input.MobileNo), e => e.JobFk != null && e.JobFk.LeadFk.Mobile.Contains(input.MobileNo))
						.WhereIf(input.applicationstatus == 1, e => e.TrackingNumber != null && e.DispatchedDate != null && (e.JobFk.TotalCost - ((_invoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_invoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()))) == 0)
						.WhereIf(input.applicationstatus == 2, e => e.TrackingNumber == null && e.DispatchedDate == null && (e.JobFk.TotalCost - ((_invoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_invoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()))) <= 0)
						.WhereIf(input.applicationstatus == 3, e => ((_invoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_invoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum())) < e.JobFk.TotalCost)
						.WhereIf(input.PromoType != 0, e => e.PromotionMasterId == input.PromoType)
						.WhereIf(input.FreebieTransportId != 0, e => e.FreebieTransportId == input.FreebieTransportId)
						.WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
						.WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
						.WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
						.WhereIf(input.applicationstatus == 1 && input.StartDate != null && input.EndDate != null, e => e.DispatchedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DispatchedDate.Value.AddHours(10).Date <= EDate.Value.Date)
						.Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit && e.JobFk.JobStatusId != 2 && e.JobFk.JobStatusId != 3);

			var pagedAndFilteredJobPromotions = filteredJobPromotions
				.OrderBy(input.Sorting ?? "id asc")
				.PageBy(input);

			var jobPromotions = from o in pagedAndFilteredJobPromotions
								join o1 in _lookup_jobRepository.GetAll() on o.JobId equals o1.Id into j1
								from s1 in j1.DefaultIfEmpty()

								join o2 in _lookup_promotionMasterRepository.GetAll() on o.PromotionMasterId equals o2.Id into j2
								from s2 in j2.DefaultIfEmpty()

								join o3 in _lookup_leadRepository.GetAll() on o.JobFk.LeadId equals o3.Id into j3
								from s3 in j3.DefaultIfEmpty()

								join o4 in _lookup_jobStatusRepository.GetAll() on s1.JobStatusId equals o4.Id into j4
								from s4 in j4.DefaultIfEmpty()

								select new GetJobPromotionForViewDto()
								{
									JobPromotion = new JobPromotionDto
									{
										Id = o.Id,
										TrackingNumber = o.TrackingNumber,
										Description = o.Description,
										JobId = o.JobId,
										SmsSend = o.SmsSend,
										EmailSend = o.EmailSend,
										SmsSendDate = o.SmsSendDate,
										EmailSendDate = o.EmailSendDate,
										FreebieTransportName = o.FreebieTransportIdFk.Name,

									},

									CompanyName = s3 == null || s3.CompanyName == null ? "" : s3.CompanyName.ToString(),
									Email = s3 == null || s3.Email == null ? "" : s3.Email.ToString(),
									Phone = s3 == null || s3.Phone == null ? "" : s3.Phone.ToString(),
									Mobile = s3 == null || s3.Mobile == null ? "" : s3.Mobile.ToString(),
									Address = s1 == null || s1.Address == null ? "" : s1.Address.ToString(),
									Suburb = s1 == null || s1.Suburb == null ? "" : s1.Suburb.ToString(),
									State = s1 == null || s1.State == null ? "" : s1.State.ToString(),
									PostCode = s1 == null || s1.PostalCode == null ? "" : s1.PostalCode.ToString(),
									PromotionMasterName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
									JobNumber = s1.JobNumber,
									ProjectStatus = s4.Name,
									BalQwing = (o.JobFk.TotalCost - (_invoiceImportDataRepository.GetAll().Where(e => e.JobId == s1.Id).Select(e => e.PaidAmmount).Sum())),
									CurrentLeadOwaner = user_list.Where(e => e.Id == s3.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
									LeadId = s3.Id,
									ActivityReminderTime = leadactivity_list.Where(e =>   e.ActionId == 8 && e.ReferanceId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
									ActivityDescription = leadactivity_list.Where(e =>   e.ActionId == 8 && e.ReferanceId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
									ActivityComment = leadactivity_list.Where(e =>   e.ActionId == 24 && e.ReferanceId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
								};

			var totalCount = await filteredJobPromotions.CountAsync();

			return new PagedResultDto<GetJobPromotionForViewDto>(
				totalCount,
				await jobPromotions.ToListAsync()
			);
		}

		[AbpAuthorize(AppPermissions.Pages_JobPromotions_Edit, AppPermissions.Pages_FreebiesTracker_AddTrackingNumber)]
		//public async Task<GetJobPromotionForEditOutput> GetJobPromotionForEdit(EntityDto input)
		//{
		//	var jobPromotion = await _jobPromotionRepository.FirstOrDefaultAsync(input.Id);

		//	var output = new GetJobPromotionForEditOutput { JobPromotion = ObjectMapper.Map<CreateOrEditJobPromotionDto>(jobPromotion) };

		//	var leadid = _lookup_jobRepository.GetAll().Where(e => e.Id == jobPromotion.JobId).Select(e => e.LeadId).FirstOrDefault();
		//	output.JobNumber = _lookup_jobRepository.GetAll().Where(e => e.Id == jobPromotion.JobId).Select(e => e.JobNumber).FirstOrDefault();
		//	output.LeadCompanyName = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.CompanyName).FirstOrDefault();
		//	if (output.JobPromotion.JobId != null)
		//	{
		//		var _lookupJob = await _lookup_jobRepository.FirstOrDefaultAsync((int)output.JobPromotion.JobId);
		//		output.JobStreetName = _lookupJob?.StreetName?.ToString();
		//	}

		//	if (output.JobPromotion.PromotionMasterId != null)
		//	{
		//		var _lookupPromotionMaster = await _lookup_promotionMasterRepository.FirstOrDefaultAsync((int)output.JobPromotion.PromotionMasterId);
		//		output.PromotionMasterName = _lookupPromotionMaster?.Name?.ToString();
		//	}

		//	return output;
		//}

		//public async Task CreateOrEdit(CreateOrEditJobPromotionDto input)
		//{
		//	if (input.Id == null)
		//	{
		//		await Create(input);
		//	}
		//	else
		//	{
		//		await Update(input);
		//	}
		//}

		[AbpAuthorize(AppPermissions.Pages_JobPromotions_Create)]
		protected virtual async Task Create(CreateOrEditJobPromotionDto input)
		{
            input.DispatchedDate = _timeZoneConverter.Convert(input.DispatchedDate, (int)AbpSession.TenantId);

            var jobPromotion = ObjectMapper.Map<JobPromotion>(input);


			if (AbpSession.TenantId != null)
			{
				jobPromotion.TenantId = (int)AbpSession.TenantId;
			}


			await _jobPromotionRepository.InsertAsync(jobPromotion);
		}

		[AbpAuthorize(AppPermissions.Pages_JobPromotions_Edit, AppPermissions.Pages_FreebiesTracker_AddTrackingNumber)]
		protected virtual async Task Update(CreateOrEditJobPromotionDto input)
		{
            input.DispatchedDate = _timeZoneConverter.Convert(input.DispatchedDate, (int)AbpSession.TenantId);
			var jobPromotion = await _jobPromotionRepository.FirstOrDefaultAsync((int)input.Id);
			

			var Jobs = _lookup_jobRepository.GetAll().Where(e => e.Id == jobPromotion.JobId).FirstOrDefault();
			var UserDetail = _lookup_userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

			LeadActivityLog leadactivity = new LeadActivityLog();
			leadactivity.ActionId = 19;
			leadactivity.SectionId = input.SectionId != null ? input.SectionId : 0;
			leadactivity.ActionNote = "Job Promotion Sent With Tracking No : " + input.TrackingNumber;
			leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
			if (AbpSession.TenantId != null)
			{
				leadactivity.TenantId = (int)AbpSession.TenantId;
			}
			var jobactionid = _leadactivityRepository.InsertAndGetId(leadactivity);


			var list = new List<JobTrackerHistory>();

			if (jobPromotion.DispatchedDate != input.DispatchedDate)
			{
				JobTrackerHistory jobhistory = new JobTrackerHistory();
				if (AbpSession.TenantId != null)
				{
					jobhistory.TenantId = (int)AbpSession.TenantId;
				}
				jobhistory.FieldName = "DispatchedDate";
				jobhistory.PrevValue = jobPromotion.DispatchedDate.ToString();
				jobhistory.CurValue = input.DispatchedDate.ToString();
				jobhistory.Action = "Freebies Tracker Edit";
				jobhistory.LastmodifiedDateTime = DateTime.Now;
				jobhistory.JobIDId = jobPromotion.JobId;
				jobhistory.JobActionId = jobactionid;
				list.Add(jobhistory);
			}
			if (jobPromotion.TrackingNumber != input.TrackingNumber)
			{
				JobTrackerHistory jobhistory = new JobTrackerHistory();
				if (AbpSession.TenantId != null)
				{
					jobhistory.TenantId = (int)AbpSession.TenantId;
				}
				jobhistory.FieldName = "TrackingNumber";
				jobhistory.PrevValue = jobPromotion.TrackingNumber;
				jobhistory.CurValue = input.TrackingNumber;
				jobhistory.Action = "Freebies Tracker Edit";
				jobhistory.LastmodifiedDateTime = DateTime.Now;
				jobhistory.JobIDId = jobPromotion.JobId;
				jobhistory.JobActionId = jobactionid;
				list.Add(jobhistory);
			}
			if (jobPromotion.FreebieTransportId != input.FreebieTransportId)
			{
				JobTrackerHistory jobhistory = new JobTrackerHistory();
				if (AbpSession.TenantId != null)
				{
					jobhistory.TenantId = (int)AbpSession.TenantId;
				}
				jobhistory.FieldName = "FreebieTransportId";
				jobhistory.PrevValue =  _freebieTransportRepository.GetAll().Where(e=> e.Id == jobPromotion.FreebieTransportId).Select(e => e.Name).FirstOrDefault();
				jobhistory.CurValue = _freebieTransportRepository.GetAll().Where(e => e.Id == input.FreebieTransportId).Select(e => e.Name).FirstOrDefault();
				jobhistory.Action = "Freebies Tracker Edit";
				jobhistory.LastmodifiedDateTime = DateTime.Now;
				jobhistory.JobIDId = jobPromotion.JobId;
				jobhistory.JobActionId = jobactionid;
				list.Add(jobhistory);
			}
            if (jobPromotion.VoucherId != input.VoucherId)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "VoucherId";
                jobhistory.PrevValue = _voucherRepository.GetAll().Where(e => e.Id == jobPromotion.VoucherId).Select(e => e.VoucherName).FirstOrDefault();
                jobhistory.CurValue = _voucherRepository.GetAll().Where(e => e.Id == input.VoucherId).Select(e => e.VoucherName).FirstOrDefault();
                jobhistory.Action = "Voucher Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = jobPromotion.JobId;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (jobPromotion.Description != input.Description)
			{
				JobTrackerHistory jobhistory = new JobTrackerHistory();
				if (AbpSession.TenantId != null)
				{
					jobhistory.TenantId = (int)AbpSession.TenantId;
				}
				jobhistory.FieldName = "Description";
				jobhistory.PrevValue = jobPromotion.Description;
				jobhistory.CurValue = input.Description;
				jobhistory.Action = "Freebies Tracker Edit";
				jobhistory.LastmodifiedDateTime = DateTime.Now;
				jobhistory.JobIDId = jobPromotion.JobId;
				jobhistory.JobActionId = jobactionid;
				list.Add(jobhistory);
			}
			await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
			await _dbcontextprovider.GetDbContext().SaveChangesAsync();

			ObjectMapper.Map(input, jobPromotion);
		}

		[AbpAuthorize(AppPermissions.Pages_JobPromotions_Delete)]
		public async Task Delete(EntityDto input)
		{
			await _jobPromotionRepository.DeleteAsync(input.Id);
		}

		//public async Task<List<JobPromotionJobLookupTableDto>> GetAllJobForTableDropdown()
		//{
		//	return await _lookup_jobRepository.GetAll()
		//		.Select(job => new JobPromotionJobLookupTableDto
		//		{
		//			Id = job.Id,
		//			DisplayName = job == null || job.StreetName == null ? "" : job.StreetName.ToString()
		//		}).ToListAsync();
		//}

		public async Task<List<JobPromotionPromotionMasterLookupTableDto>> GetAllPromotionMasterForTableDropdown()
		{
			return await _lookup_promotionMasterRepository.GetAll()
				.Select(promotionMaster => new JobPromotionPromotionMasterLookupTableDto
				{
					Id = promotionMaster.Id,
					DisplayName = promotionMaster == null || promotionMaster.Name == null ? "" : promotionMaster.Name.ToString()
				}).ToListAsync();
		}

		public async Task<List<JobPromotionPromotionMasterLookupTableDto>> GetAllFreebieTransportRepositoryForTableDropdown()
		{
			return await _freebieTransportRepository.GetAll()
				.Select(promotionMaster => new JobPromotionPromotionMasterLookupTableDto
				{
					Id = promotionMaster.Id,
					DisplayName = promotionMaster == null || promotionMaster.Name == null ? "" : promotionMaster.Name.ToString()
				}).ToListAsync();
		}

		public async Task<FileDto> GetJobPromotionsToExcel(GetAllJobPromotionsForExcelInput input)
		{
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var filteredJobPromotions = _jobPromotionRepository.GetAll()
            //			.Include(e => e.JobFk)
            //			.Include(e => e.PromotionMasterFk)
            //			.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.JobFk.LeadFk.Phone.Contains(input.Filter) || e.JobFk.JobNumber.Contains(input.Filter))
            //			.WhereIf(!string.IsNullOrWhiteSpace(input.ProjectNumber), e => e.JobFk != null && e.JobFk.JobNumber.Contains(input.ProjectNumber))
            //			.WhereIf(!string.IsNullOrWhiteSpace(input.Address), e => e.JobFk != null && e.JobFk.Address.Contains(input.ProjectNumber))
            //			.WhereIf(!string.IsNullOrWhiteSpace(input.CompanyName), e => e.JobFk != null && e.JobFk.LeadFk.CompanyName.Contains(input.CompanyName))
            //			.WhereIf(!string.IsNullOrWhiteSpace(input.Email), e => e.JobFk != null && e.JobFk.LeadFk.Email.Contains(input.Email))
            //			.WhereIf(!string.IsNullOrWhiteSpace(input.MobileNo), e => e.JobFk != null && e.JobFk.LeadFk.Mobile.Contains(input.MobileNo))
            //			.WhereIf(!string.IsNullOrWhiteSpace(input.MobileNo), e => e.JobFk != null && e.JobFk.LeadFk.Mobile.Contains(input.MobileNo))
            //			.WhereIf(!string.IsNullOrWhiteSpace(input.MobileNo), e => e.JobFk != null && e.JobFk.LeadFk.Mobile.Contains(input.MobileNo))
            //			.WhereIf(input.applicationstatus != 0 && input.applicationstatus == 1, e => e.TrackingNumber != null && e.DispatchedDate != null)
            //			.WhereIf(input.applicationstatus != 0 && input.applicationstatus == 2, e => e.TrackingNumber == null && e.DispatchedDate == null)
            //			.WhereIf(input.PromoType != 0, e => e.PromotionMasterId == input.PromoType)
            //			.WhereIf(input.FreebieTransportId != 0, e => e.FreebieTransportId == input.FreebieTransportId)
            //			.WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
            //			.WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
            //			.WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
            //			.WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesRepId)
            //			.WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesManagerId)
            //			.WhereIf(input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= input.StartDate.Value.Date && e.CreationTime.AddHours(10).Date <= input.EndDate.Value.Date)
            //			.Where(e => e.JobFk.TotalCost == (_invoicePaymentRepository.GetAll().Where(i => i.JobId == e.JobFk.Id).Select(e => e.InvoicePayTotal).Sum()))
            //			.Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit);

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
			var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
			var user_list = _userRepository.GetAll();
			var user_teamList = _userTeamRepository.GetAll();
			var User = user_list.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
			IList<string> role = await _userManager.GetRolesAsync(User);
			var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.SectionId == 2);

			var TeamId = user_teamList.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
			var UserList = user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

			var filteredJobPromotions = _jobPromotionRepository.GetAll()
						.Include(e => e.JobFk)
						.Include(e => e.PromotionMasterFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName == input.Filter || e.JobFk.LeadFk.Email == input.Filter || e.JobFk.LeadFk.Mobile == input.Filter || e.JobFk.JobNumber == input.Filter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.ProjectNumber), e => e.JobFk != null && e.JobFk.JobNumber == input.ProjectNumber)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Address), e => e.JobFk != null && e.JobFk.Address == input.ProjectNumber)
						.WhereIf(!string.IsNullOrWhiteSpace(input.CompanyName), e => e.JobFk != null && e.JobFk.LeadFk.CompanyName == input.CompanyName)
						.WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.JobFk.State == input.stateNameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Email), e => e.JobFk != null && e.JobFk.LeadFk.Email == input.Email)
						.WhereIf(!string.IsNullOrWhiteSpace(input.MobileNo), e => e.JobFk != null && e.JobFk.LeadFk.Mobile == input.MobileNo)
						.WhereIf(input.applicationstatus == 1, e => e.TrackingNumber != null && e.DispatchedDate != null && (e.JobFk.TotalCost - ((_invoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_invoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()))) == 0)
						.WhereIf(input.applicationstatus == 2, e => e.TrackingNumber == null && e.DispatchedDate == null && (e.JobFk.TotalCost - ((_invoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_invoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()))) <= 0)
						.WhereIf(input.applicationstatus == 3, e => ((_invoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_invoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum())) < e.JobFk.TotalCost)
						.WhereIf(input.PromoType != 0, e => e.PromotionMasterId == input.PromoType)
						.WhereIf(input.FreebieTransportId != 0, e => e.FreebieTransportId == input.FreebieTransportId)
						.WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
						.WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
						.WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
						.WhereIf(input.applicationstatus == 1 && input.StartDate != null && input.EndDate != null, e => e.DispatchedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DispatchedDate.Value.AddHours(10).Date <= EDate.Value.Date)
						.Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit && e.JobFk.JobStatusId != 2 && e.JobFk.JobStatusId != 3);

			var jobPromotions = from o in filteredJobPromotions
								join o1 in _lookup_jobRepository.GetAll() on o.JobId equals o1.Id into j1
								from s1 in j1.DefaultIfEmpty()

								join o2 in _lookup_promotionMasterRepository.GetAll() on o.PromotionMasterId equals o2.Id into j2
								from s2 in j2.DefaultIfEmpty()

								join o3 in _lookup_leadRepository.GetAll() on o.JobFk.LeadId equals o3.Id into j3
								from s3 in j3.DefaultIfEmpty()
								join o4 in _lookup_jobStatusRepository.GetAll() on s1.JobStatusId equals o4.Id into j4
								from s4 in j4.DefaultIfEmpty()

								select new GetJobPromotionForViewDto()
								{
									JobPromotion = new JobPromotionDto
									{
										Id = o.Id,
										TrackingNumber = o.TrackingNumber,
										Description = o.Description,
										SmsSend = o.SmsSend
									},

									CompanyName = s3 == null || s3.CompanyName == null ? "" : s3.CompanyName.ToString(),
									Email = s3 == null || s3.Email == null ? "" : s3.Email.ToString(),
									Phone = s3 == null || s3.Phone == null ? "" : s3.Phone.ToString(),
									Mobile = s3 == null || s3.Mobile == null ? "" : s3.Mobile.ToString(),
									Address = s1 == null || s1.Address == null ? "" : s1.Address.ToString(),
									Suburb = s1 == null || s1.Suburb == null ? "" : s1.Suburb.ToString(),
									State = s1 == null || s1.State == null ? "" : s1.State.ToString(),
									PostCode = s1 == null || s1.PostalCode == null ? "" : s1.PostalCode.ToString(),
									PromotionMasterName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
									JobNumber = s1.JobNumber,
									ProjectStatus = s4.Name,
									BalQwing = (s1.TotalCost - (_invoicePaymentRepository.GetAll().Where(e => e.JobId == s1.Id).Select(e => e.InvoicePayTotal).Sum())),
									CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s3.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
								};


			var jobPromotionListDtos = await jobPromotions.ToListAsync();
			if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
			{
				return _jobPromotionsExcelExporter.ExportToFile(jobPromotionListDtos);
			}
			else
			{
				return _jobPromotionsExcelExporter.ExportCsvToFile(jobPromotionListDtos);
			}
			
		}

		public async Task FreebiesSendSms(int id, int smstempid)
		{
			var jobPromotion = await _jobPromotionRepository.FirstOrDefaultAsync(id);
			var smsText = _smsTemplateRepository.GetAll().Where(e => e.Id == smstempid).Select(e => e.Text).FirstOrDefault();
			var output = ObjectMapper.Map<CreateOrEditJobPromotionDto>(jobPromotion);
			var link = _freebieTransportRepository.GetAll().Where(e => e.Id == output.FreebieTransportId).Select(e => e.TransportLink).FirstOrDefault();
			var leadid = _lookup_jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
			var mobilenumber = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Mobile).FirstOrDefault();
			var Body = smsText + "You TransportLink Is: " + link + "/" + output.TrackingNumber + ".";
			if (!string.IsNullOrEmpty(mobilenumber))
			{
				SendSMSInput sendSMSInput = new SendSMSInput();
				sendSMSInput.PhoneNumber = mobilenumber;
				sendSMSInput.Text = Body;
				await _applicationSettings.SendSMS(sendSMSInput);
				output.SmsSend = true;
				output.SmsSendDate = DateTime.UtcNow;
				ObjectMapper.Map(output, jobPromotion);
			}
		}

		public async Task FreebiesSendEmail(int id, int emailtempid)
		{
			var jobPromotion = await _jobPromotionRepository.FirstOrDefaultAsync(id);
			var emailText = _emailTemplateRepository.GetAll().Where(e => e.Id == emailtempid).Select(e => e.TemplateName).FirstOrDefault();
			var output = ObjectMapper.Map<CreateOrEditJobPromotionDto>(jobPromotion);
			var link = _freebieTransportRepository.GetAll().Where(e => e.Id == output.FreebieTransportId).Select(e => e.TransportLink).FirstOrDefault();
			var leadid = _lookup_jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
			var Email = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Email).FirstOrDefault();
			var Body = emailText + "You TransportLink Is: " + link + "/" + output.TrackingNumber + ".";
			if (!string.IsNullOrEmpty(Email))
			{
				MailMessage mail = new MailMessage
				{
					To = { Email },
					Subject = "Status Update",
					Body = Body,
					IsBodyHtml = true
				};
				await this._emailSender.SendAsync(mail);
				output.EmailSend = true;
				output.EmailSendDate = DateTime.UtcNow;
				ObjectMapper.Map(output, jobPromotion);
			}
		}

        public async Task<GetJobPromotionForEditOutput> GetJobPromotionForEdit(EntityDto input)
        {
            var jobPromotion = await _jobPromotionRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetJobPromotionForEditOutput { JobPromotion = ObjectMapper.Map<CreateOrEditJobPromotionDto>(jobPromotion) };

            var leadid = _lookup_jobRepository.GetAll().Where(e => e.Id == jobPromotion.JobId).Select(e => e.LeadId).FirstOrDefault();
            output.JobNumber = _lookup_jobRepository.GetAll().Where(e => e.Id == jobPromotion.JobId).Select(e => e.JobNumber).FirstOrDefault();
            output.LeadCompanyName = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.CompanyName).FirstOrDefault();
            if (output.JobPromotion.JobId != null)
            {
                var _lookupJob = await _lookup_jobRepository.FirstOrDefaultAsync((int)output.JobPromotion.JobId);
                output.JobStreetName = _lookupJob?.StreetName?.ToString();
            }

            if (output.JobPromotion.PromotionMasterId != null)
            {
                var _lookupPromotionMaster = await _lookup_promotionMasterRepository.FirstOrDefaultAsync((int)output.JobPromotion.PromotionMasterId);
                output.PromotionMasterName = _lookupPromotionMaster?.Name?.ToString();
            }
            var voucherCounts = await _jobPromotionRepository
    .GetAll()
    .Where(x => x.VoucherId != null) 
    .GroupBy(x => x.VoucherId)
    .Select(g => new
    {
        VoucherId = g.Key.Value,  
        Count = g.Count()
    })
    .ToDictionaryAsync(x => x.VoucherId, x => x.Count);


            output.VoucherUsageCounts = voucherCounts;
            return output;
        }
        public async Task CreateOrEdit(CreateOrEditJobPromotionDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

    }

}