﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Quotations;

namespace TheSolarProduct.Jobs
{
	[AbpAuthorize(AppPermissions.Pages_ElecRetailers, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
	public class ElecRetailersAppService : TheSolarProductAppServiceBase, IElecRetailersAppService
    {
		 private readonly IRepository<ElecRetailer> _elecRetailerRepository;
		 private readonly IElecRetailersExcelExporter _elecRetailersExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;


        public ElecRetailersAppService(IRepository<ElecRetailer> elecRetailerRepository, IElecRetailersExcelExporter elecRetailersExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider) 
		  {
			_elecRetailerRepository = elecRetailerRepository;
			_elecRetailersExcelExporter = elecRetailersExcelExporter;
			_dataVaultActivityLogRepository = dataVaultActivityLogRepository;
			_dbcontextprovider = dbcontextprovider;
			
		  }

		 public async Task<PagedResultDto<GetElecRetailerForViewDto>> GetAll(GetAllElecRetailersInput input)
         {
			
			var filteredElecRetailers = _elecRetailerRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter)
						.WhereIf(input.NSWFilter > -1,  e => (input.NSWFilter == 1 && e.NSW) || (input.NSWFilter == 0 && !e.NSW) )
						.WhereIf(input.SAFilter > -1,  e => (input.SAFilter == 1 && e.SA) || (input.SAFilter == 0 && !e.SA) )
						.WhereIf(input.QLDFilter > -1,  e => (input.QLDFilter == 1 && e.QLD) || (input.QLDFilter == 0 && !e.QLD) )
						.WhereIf(input.VICFilter > -1,  e => (input.VICFilter == 1 && e.VIC) || (input.VICFilter == 0 && !e.VIC) )
						.WhereIf(input.WAFilter > -1,  e => (input.WAFilter == 1 && e.WA) || (input.WAFilter == 0 && !e.WA) )
						.WhereIf(input.ACTFilter > -1,  e => (input.ACTFilter == 1 && e.ACT) || (input.ACTFilter == 0 && !e.ACT) )
						.WhereIf(input.TASFilter > -1,  e => (input.TASFilter == 1 && e.TAS) || (input.TASFilter == 0 && !e.TAS) )
						.WhereIf(input.NTFilter > -1,  e => (input.NTFilter == 1 && e.NT) || (input.NTFilter == 0 && !e.NT) );

			var pagedAndFilteredElecRetailers = filteredElecRetailers
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

			var elecRetailers = from o in pagedAndFilteredElecRetailers
                         select new GetElecRetailerForViewDto() {
							ElecRetailer = new ElecRetailerDto
							{
                                Name = o.Name,
                                NSW = o.NSW,
                                SA = o.SA,
                                QLD = o.QLD,
                                VIC = o.VIC,
                                WA = o.WA,
                                ACT = o.ACT,
                                TAS = o.TAS,
                                NT = o.NT,
                                ElectricityProviderId = o.ElectricityProviderId,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						};

            var totalCount = await filteredElecRetailers.CountAsync();

            return new PagedResultDto<GetElecRetailerForViewDto>(
                totalCount,
                await elecRetailers.ToListAsync()
            );
         }
		 
		 public async Task<GetElecRetailerForViewDto> GetElecRetailerForView(int id)
         {
            var elecRetailer = await _elecRetailerRepository.GetAsync(id);

            var output = new GetElecRetailerForViewDto { ElecRetailer = ObjectMapper.Map<ElecRetailerDto>(elecRetailer) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_ElecRetailers_Edit)]
		 public async Task<GetElecRetailerForEditOutput> GetElecRetailerForEdit(EntityDto input)
         {
            var elecRetailer = await _elecRetailerRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetElecRetailerForEditOutput {ElecRetailer = ObjectMapper.Map<CreateOrEditElecRetailerDto>(elecRetailer)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditElecRetailerDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_ElecRetailers_Create)]
		 protected virtual async Task Create(CreateOrEditElecRetailerDto input)
         {
            var elecRetailer = ObjectMapper.Map<ElecRetailer>(input);

            //if (AbpSession.TenantId != null)
            //{
            //	elecRetailer.TenantId = (int)AbpSession.TenantId;
            //}

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 11;
            dataVaultLog.ActionNote = "Electricity Retailers Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _elecRetailerRepository.InsertAsync(elecRetailer);
         }

		 [AbpAuthorize(AppPermissions.Pages_ElecRetailers_Edit)]
		 protected virtual async Task Update(CreateOrEditElecRetailerDto input)
         {
            var elecRetailer = await _elecRetailerRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 11;
            dataVaultLog.ActionNote = "Electricity Retailers Updated : " + elecRetailer.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != elecRetailer.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = elecRetailer.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != elecRetailer.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = elecRetailer.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, elecRetailer);
         }

		 [AbpAuthorize(AppPermissions.Pages_ElecRetailers_Delete)]
         public async Task Delete(EntityDto input)
         {
            var Name = _elecRetailerRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 11;
            dataVaultLog.ActionNote = "Electricity Retailers Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _elecRetailerRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetElecRetailersToExcel(GetAllElecRetailersForExcelInput input)
         {
			
			var filteredElecRetailers = _elecRetailerRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter)
						.WhereIf(input.NSWFilter > -1,  e => (input.NSWFilter == 1 && e.NSW) || (input.NSWFilter == 0 && !e.NSW) )
						.WhereIf(input.SAFilter > -1,  e => (input.SAFilter == 1 && e.SA) || (input.SAFilter == 0 && !e.SA) )
						.WhereIf(input.QLDFilter > -1,  e => (input.QLDFilter == 1 && e.QLD) || (input.QLDFilter == 0 && !e.QLD) )
						.WhereIf(input.VICFilter > -1,  e => (input.VICFilter == 1 && e.VIC) || (input.VICFilter == 0 && !e.VIC) )
						.WhereIf(input.WAFilter > -1,  e => (input.WAFilter == 1 && e.WA) || (input.WAFilter == 0 && !e.WA) )
						.WhereIf(input.ACTFilter > -1,  e => (input.ACTFilter == 1 && e.ACT) || (input.ACTFilter == 0 && !e.ACT) )
						.WhereIf(input.TASFilter > -1,  e => (input.TASFilter == 1 && e.TAS) || (input.TASFilter == 0 && !e.TAS) )
						.WhereIf(input.NTFilter > -1,  e => (input.NTFilter == 1 && e.NT) || (input.NTFilter == 0 && !e.NT) );

			var query = (from o in filteredElecRetailers
                         select new GetElecRetailerForViewDto() { 
							ElecRetailer = new ElecRetailerDto
							{
                                Name = o.Name,
                                NSW = o.NSW,
                                SA = o.SA,
                                QLD = o.QLD,
                                VIC = o.VIC,
                                WA = o.WA,
                                ACT = o.ACT,
                                TAS = o.TAS,
                                NT = o.NT,
                                ElectricityProviderId = o.ElectricityProviderId,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						 });


            var elecRetailerListDtos = await query.ToListAsync();

            return _elecRetailersExcelExporter.ExportToFile(elecRetailerListDtos);
         }


    }
}