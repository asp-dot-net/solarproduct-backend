﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.PostCodes;

namespace TheSolarProduct.Jobs
{
	[AbpAuthorize(AppPermissions.Pages_PromotionMasters, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    public class PromotionMastersAppService : TheSolarProductAppServiceBase, IPromotionMastersAppService
    {
		 private readonly IRepository<PromotionMaster> _promotionMasterRepository;
		 private readonly IPromotionMastersExcelExporter _promotionMastersExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;


        public PromotionMastersAppService(IRepository<PromotionMaster> promotionMasterRepository, IPromotionMastersExcelExporter promotionMastersExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider) 
		  {
			_promotionMasterRepository = promotionMasterRepository;
			_promotionMastersExcelExporter = promotionMastersExcelExporter;
			_dbcontextprovider = dbcontextprovider;
			_dataVaultActivityLogRepository = dataVaultActivityLogRepository;			
		  }

		 public async Task<PagedResultDto<GetPromotionMasterForViewDto>> GetAll(GetAllPromotionMastersInput input)
         {
			
			var filteredPromotionMasters = _promotionMasterRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter);

			var pagedAndFilteredPromotionMasters = filteredPromotionMasters
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

			var promotionMasters = from o in pagedAndFilteredPromotionMasters
                         select new GetPromotionMasterForViewDto() {
							PromotionMaster = new PromotionMasterDto
							{
                                Name = o.Name,
                                Id = o.Id,
								DisplayOrder =  o.DisplayOrder,
                                IsActive = o.IsActive,
							}
						};

            var totalCount = await filteredPromotionMasters.CountAsync();

            return new PagedResultDto<GetPromotionMasterForViewDto>(
                totalCount,
                await promotionMasters.ToListAsync()
            );
         }
		 
		 public async Task<GetPromotionMasterForViewDto> GetPromotionMasterForView(int id)
         {
            var promotionMaster = await _promotionMasterRepository.GetAsync(id);

            var output = new GetPromotionMasterForViewDto { PromotionMaster = ObjectMapper.Map<PromotionMasterDto>(promotionMaster) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_PromotionMasters_Edit)]
		 public async Task<GetPromotionMasterForEditOutput> GetPromotionMasterForEdit(EntityDto input)
         {
            var promotionMaster = await _promotionMasterRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetPromotionMasterForEditOutput {PromotionMaster = ObjectMapper.Map<CreateOrEditPromotionMasterDto>(promotionMaster)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditPromotionMasterDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_PromotionMasters_Create)]
		 protected virtual async Task Create(CreateOrEditPromotionMasterDto input)
         {
            var promotionMaster = ObjectMapper.Map<PromotionMaster>(input);

			
			if (AbpSession.TenantId != null)
			{
				promotionMaster.TenantId = (int) AbpSession.TenantId;
			}

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 22;
            dataVaultLog.ActionNote = "Promotion Type Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _promotionMasterRepository.InsertAsync(promotionMaster);
         }

		 [AbpAuthorize(AppPermissions.Pages_PromotionMasters_Edit)]
		 protected virtual async Task Update(CreateOrEditPromotionMasterDto input)
         {
            var promotionMaster = await _promotionMasterRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 22;
            dataVaultLog.ActionNote = "Promotion Type Updated : " + promotionMaster.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != promotionMaster.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = promotionMaster.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != promotionMaster.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = promotionMaster.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, promotionMaster);
         }

		 [AbpAuthorize(AppPermissions.Pages_PromotionMasters_Delete)]
         public async Task Delete(EntityDto input)
         {
            var Name = _promotionMasterRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 22;
            dataVaultLog.ActionNote = "Promotion Type Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _promotionMasterRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetPromotionMastersToExcel(GetAllPromotionMastersForExcelInput input)
         {
			
			var filteredPromotionMasters = _promotionMasterRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter);

			var query = (from o in filteredPromotionMasters
                         select new GetPromotionMasterForViewDto() { 
							PromotionMaster = new PromotionMasterDto
							{
                                Name = o.Name,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						 });


            var promotionMasterListDtos = await query.ToListAsync();

            return _promotionMastersExcelExporter.ExportToFile(promotionMasterListDtos);
         }


    }
}