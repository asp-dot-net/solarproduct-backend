﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.Jobs
{
	[AbpAuthorize(AppPermissions.Pages_PaymentOptions, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
	public class PaymentOptionsAppService : TheSolarProductAppServiceBase, IPaymentOptionsAppService
	{
		private readonly IRepository<PaymentOption> _paymentOptionRepository;
		private readonly IPaymentOptionsExcelExporter _paymentOptionsExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;


        public PaymentOptionsAppService(IRepository<PaymentOption> paymentOptionRepository, IPaymentOptionsExcelExporter paymentOptionsExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
		{
			_paymentOptionRepository = paymentOptionRepository;
			_paymentOptionsExcelExporter = paymentOptionsExcelExporter;
			_dataVaultActivityLogRepository = dataVaultActivityLogRepository;
			_dbcontextprovider = dbcontextprovider;
		}

		public async Task<PagedResultDto<GetPaymentOptionForViewDto>> GetAll(GetAllPaymentOptionsInput input)
		{

			var filteredPaymentOptions = _paymentOptionRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

			var pagedAndFilteredPaymentOptions = filteredPaymentOptions
				.OrderBy(input.Sorting ?? "id desc")
				.PageBy(input);

			var paymentOptions = from o in pagedAndFilteredPaymentOptions
								 select new GetPaymentOptionForViewDto()
								 {
									 PaymentOption = new PaymentOptionDto
									 {
										 Name = o.Name,
										 DisplayOrder = o.DisplayOrder,
										 Id = o.Id,
										 EstablishmentFee = o.EstablishmentFee,
										 MonthlyAccFee = o.MonthlyAccFee,
										 IsActive = o.IsActive,
									 }
								 };

			var totalCount = await filteredPaymentOptions.CountAsync();

			return new PagedResultDto<GetPaymentOptionForViewDto>(
				totalCount,
				await paymentOptions.ToListAsync()
			);
		}

		public async Task<GetPaymentOptionForViewDto> GetPaymentOptionForView(int id)
		{
			var paymentOption = await _paymentOptionRepository.GetAsync(id);

			var output = new GetPaymentOptionForViewDto { PaymentOption = ObjectMapper.Map<PaymentOptionDto>(paymentOption) };

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_PaymentOptions_Edit)]
		public async Task<GetPaymentOptionForEditOutput> GetPaymentOptionForEdit(EntityDto input)
		{
			var paymentOption = await _paymentOptionRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetPaymentOptionForEditOutput { PaymentOption = ObjectMapper.Map<CreateOrEditPaymentOptionDto>(paymentOption) };

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditPaymentOptionDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_PaymentOptions_Create)]
		protected virtual async Task Create(CreateOrEditPaymentOptionDto input)
		{
			var paymentOption = ObjectMapper.Map<PaymentOption>(input);


			if (AbpSession.TenantId != null)
			{
				paymentOption.TenantId = (int)AbpSession.TenantId;
			}

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 19;
            dataVaultLog.ActionNote = "Payment Method Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _paymentOptionRepository.InsertAsync(paymentOption);
		}

		[AbpAuthorize(AppPermissions.Pages_PaymentOptions_Edit)]
		protected virtual async Task Update(CreateOrEditPaymentOptionDto input)
		{
			var paymentOption = await _paymentOptionRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 19;
            dataVaultLog.ActionNote = "Payment Method Updated : " + paymentOption.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != paymentOption.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = paymentOption.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.EstablishmentFee != paymentOption.EstablishmentFee)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "EstablishmentFee";
                history.PrevValue = paymentOption.EstablishmentFee.ToString();
                history.CurValue = input.EstablishmentFee.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.MonthlyAccFee != paymentOption.MonthlyAccFee)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "MonthlyAccFee";
                history.PrevValue = paymentOption.MonthlyAccFee.ToString();
                history.CurValue = input.MonthlyAccFee.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != paymentOption.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = paymentOption.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, paymentOption);
		}

		[AbpAuthorize(AppPermissions.Pages_PaymentOptions_Delete)]
		public async Task Delete(EntityDto input)
		{
            var Name = _paymentOptionRepository.Get(input.Id).Name;
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 19;
            dataVaultLog.ActionNote = "Payment Method Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _paymentOptionRepository.DeleteAsync(input.Id);
		}

        [AbpAuthorize(AppPermissions.Pages_PaymentOptions_Export)]
        public async Task<FileDto> GetPaymentOptionsToExcel(GetAllPaymentOptionsForExcelInput input)
		{

			var filteredPaymentOptions = _paymentOptionRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

			var query = (from o in filteredPaymentOptions
						 select new GetPaymentOptionForViewDto()
						 {
							 PaymentOption = new PaymentOptionDto
							 {
								 Name = o.Name,
								 DisplayOrder = o.DisplayOrder,
								 Id = o.Id,
								 IsActive = o.IsActive,
							 }
						 });


			var paymentOptionListDtos = await query.ToListAsync();

			return _paymentOptionsExcelExporter.ExportToFile(paymentOptionListDtos);
		}


	}
}