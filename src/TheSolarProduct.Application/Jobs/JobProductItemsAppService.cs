﻿using TheSolarProduct.Jobs;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace TheSolarProduct.Jobs
{
    //[AbpAuthorize(AppPermissions.Pages_JobProductItems, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    [AbpAuthorize(AppPermissions.Pages)]
    public class JobProductItemsAppService : TheSolarProductAppServiceBase, IJobProductItemsAppService
	{
		private readonly IRepository<JobProductItem> _jobProductItemRepository;
		private readonly IRepository<Job, int> _lookup_jobRepository;
		private readonly IRepository<ProductItem, int> _lookup_productItemRepository;


		public JobProductItemsAppService(IRepository<JobProductItem> jobProductItemRepository, IRepository<Job, int> lookup_jobRepository, IRepository<ProductItem, int> lookup_productItemRepository)
		{
			_jobProductItemRepository = jobProductItemRepository;
			_lookup_jobRepository = lookup_jobRepository;
			_lookup_productItemRepository = lookup_productItemRepository;

		}
		//public async Task<List<GetJobProductItemForEditOutput>> GetJobProductItemByJobId(int jobid)
		//{
		//	var jobProductItem = _jobProductItemRepository.GetAll().Where(x => x.JobId == jobid).ToList();

		//	var output = new List<GetJobProductItemForEditOutput>();

		//	foreach (var item in jobProductItem)
		//	{
		//		var outobj = new GetJobProductItemForEditOutput();
		//		outobj.JobProductItem = ObjectMapper.Map<CreateOrEditJobProductItemDto>(item);

		//		if (outobj.JobProductItem.ProductItemId != null)
		//		{
		//			var _lookupProductItem = await _lookup_productItemRepository.FirstOrDefaultAsync((int)outobj.JobProductItem.ProductItemId);
		//			outobj.ProductItemName = _lookupProductItem?.Name?.ToString();
		//			outobj.JobProductItem.ProductTypeId = _lookupProductItem?.ProductTypeId;
		//			outobj.Size = _lookupProductItem?.Size;
		//			outobj.Model = _lookupProductItem?.Model?.ToString();
		//		}

		//		if (outobj.JobProductItem.JobId != null)
		//		{
		//			var _lookupJob = await _lookup_jobRepository.FirstOrDefaultAsync((int)outobj.JobProductItem.JobId);
		//			outobj.JobNote = _lookupJob?.Note?.ToString();
		//		}
		//		output.Add(outobj);
		//	}

		//	return output;
		//}

		public async Task<List<GetJobProductItemForEditOutput>> GetJobProductItemByJobId(int jobid)
		{
			var jobProductItem = _jobProductItemRepository.GetAll().Where(x => x.JobId == jobid).OrderBy(e => e.Id).ToList();

			var output = new List<GetJobProductItemForEditOutput>();

			foreach (var item in jobProductItem)
			{
				var outobj = new GetJobProductItemForEditOutput();
				outobj.JobProductItem = ObjectMapper.Map<CreateOrEditJobProductItemDto>(item);

				if (outobj.JobProductItem.ProductItemId != null)
				{
					var _lookupProductItem = await _lookup_productItemRepository.FirstOrDefaultAsync((int)outobj.JobProductItem.ProductItemId);
					outobj.ProductItemName = _lookupProductItem?.Name?.ToString();
					outobj.JobProductItem.ProductTypeId = _lookupProductItem?.ProductTypeId;
					outobj.Size = _lookupProductItem?.Size;
					outobj.Model = _lookupProductItem?.Model?.ToString();
					outobj.ID = item.Id;
					outobj.Stock = 0;
                    outobj.DatasheetUrl = (_lookupProductItem.FilePath + _lookupProductItem.FileName);
                }

				if (outobj.JobProductItem.JobId != null)
				{
					var _lookupJob = await _lookup_jobRepository.FirstOrDefaultAsync((int)outobj.JobProductItem.JobId);
					outobj.JobNote = _lookupJob?.Note?.ToString();
				}
				output.Add(outobj);
			}

			return output;
		}

		public async Task<PagedResultDto<GetJobProductItemForViewDto>> GetAll(GetAllJobProductItemsInput input)
		{

			var filteredJobProductItems = _jobProductItemRepository.GetAll()
						.Include(e => e.JobFk)
						.Include(e => e.ProductItemFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
						.WhereIf(!string.IsNullOrWhiteSpace(input.JobNoteFilter), e => e.JobFk != null && e.JobFk.Note == input.JobNoteFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.ProductItemNameFilter), e => e.ProductItemFk != null && e.ProductItemFk.Name == input.ProductItemNameFilter);

			var pagedAndFilteredJobProductItems = filteredJobProductItems
				.OrderBy(input.Sorting ?? "id desc")
				.PageBy(input);

			var jobProductItems = from o in pagedAndFilteredJobProductItems
								  join o1 in _lookup_jobRepository.GetAll() on o.JobId equals o1.Id into j1
								  from s1 in j1.DefaultIfEmpty()

								  join o2 in _lookup_productItemRepository.GetAll() on o.ProductItemId equals o2.Id into j2
								  from s2 in j2.DefaultIfEmpty()

								  select new GetJobProductItemForViewDto()
								  {
									  JobProductItem = new JobProductItemDto
									  {
										  Id = o.Id
									  },
									  JobNote = s1 == null || s1.Note == null ? "" : s1.Note.ToString(),
									  ProductItemName = s2 == null || s2.Name == null ? "" : s2.Name.ToString()
								  };

			var totalCount = await filteredJobProductItems.CountAsync();

			return new PagedResultDto<GetJobProductItemForViewDto>(
				totalCount,
				await jobProductItems.ToListAsync()
			);
		}

		[AbpAuthorize(AppPermissions.Pages_JobProductItems_Edit)]
		public async Task<GetJobProductItemForEditOutput> GetJobProductItemForEdit(EntityDto input)
		{
			var jobProductItem = await _jobProductItemRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetJobProductItemForEditOutput { JobProductItem = ObjectMapper.Map<CreateOrEditJobProductItemDto>(jobProductItem) };

			if (output.JobProductItem.JobId != null)
			{
				var _lookupJob = await _lookup_jobRepository.FirstOrDefaultAsync((int)output.JobProductItem.JobId);
				output.JobNote = _lookupJob?.Note?.ToString();
			}

			if (output.JobProductItem.ProductItemId != null)
			{
				var _lookupProductItem = await _lookup_productItemRepository.FirstOrDefaultAsync((int)output.JobProductItem.ProductItemId);
				output.ProductItemName = _lookupProductItem?.Name?.ToString();
			}

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditJobProductItemDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_JobProductItems_Create)]
		protected virtual async Task Create(CreateOrEditJobProductItemDto input)
		{
			var jobProductItem = ObjectMapper.Map<JobProductItem>(input);


			if (AbpSession.TenantId != null)
			{
				jobProductItem.TenantId = (int)AbpSession.TenantId;
			}


			await _jobProductItemRepository.InsertAsync(jobProductItem);
		}

		[AbpAuthorize(AppPermissions.Pages_JobProductItems_Edit)]
		protected virtual async Task Update(CreateOrEditJobProductItemDto input)
		{
			var jobProductItem = await _jobProductItemRepository.FirstOrDefaultAsync((int)input.Id);
			ObjectMapper.Map(input, jobProductItem);
		}

		[AbpAuthorize(AppPermissions.Pages_JobProductItems_Delete)]
		public async Task Delete(EntityDto input)
		{
			await _jobProductItemRepository.DeleteAsync(input.Id);
		}

		//public async Task<List<JobProductItemJobLookupTableDto>> GetAllJobForTableDropdown()
		//{
		//	return await _lookup_jobRepository.GetAll()
		//		.Select(job => new JobProductItemJobLookupTableDto
		//		{
		//			Id = job.Id,
		//			DisplayName = job == null || job.Note == null ? "" : job.Note.ToString()
		//		}).ToListAsync();
		//}

		public async Task<List<JobProductItemProductItemLookupTableDto>> GetAllProductItemForTableDropdown()
		{
			return await _lookup_productItemRepository.GetAll().Where(x=>x.Active==true)
				.Select(productItem => new JobProductItemProductItemLookupTableDto
				{
					Id = productItem.Id,
					Size = productItem.Size,
					ProductTypeId = productItem.ProductTypeId,
					Model = productItem.Model,
					DisplayName = productItem == null || productItem.Name == null ? "" : productItem.Name.ToString()
				}).ToListAsync();
		}

		public async Task<List<JobProductItemProductItemLookupTableDto>> GetAllProductItemForTableDropdownForEdit()
		{
			return await _lookup_productItemRepository.GetAll()
				.Select(productItem => new JobProductItemProductItemLookupTableDto
				{
					Id = productItem.Id,
					Size = productItem.Size,
					ProductTypeId = productItem.ProductTypeId,
					Model = productItem.Model,
					DisplayName = productItem == null || productItem.Name == null ? "" : productItem.Name.ToString()
				}).ToListAsync();
		}

	}
}