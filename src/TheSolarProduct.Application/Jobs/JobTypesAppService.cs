﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using PayPalCheckoutSdk.Orders;

namespace TheSolarProduct.Jobs
{
	[AbpAuthorize(AppPermissions.Pages_JobTypes, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
	public class JobTypesAppService : TheSolarProductAppServiceBase, IJobTypesAppService
	{
		private readonly IRepository<JobType> _jobTypeRepository;
		private readonly IJobTypesExcelExporter _jobTypesExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;


        public JobTypesAppService(IRepository<JobType> jobTypeRepository, IJobTypesExcelExporter jobTypesExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
		{
			_jobTypeRepository = jobTypeRepository;
			_jobTypesExcelExporter = jobTypesExcelExporter;
			_dataVaultActivityLogRepository = dataVaultActivityLogRepository;
			_dbcontextprovider = dbcontextprovider;

		}

		public async Task<PagedResultDto<GetJobTypeForViewDto>> GetAll(GetAllJobTypesInput input)
		{

			var filteredJobTypes = _jobTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

			var pagedAndFilteredJobTypes = filteredJobTypes
				.OrderBy(input.Sorting ?? "id desc")
				.PageBy(input);

			var jobTypes = from o in pagedAndFilteredJobTypes
						   select new GetJobTypeForViewDto()
						   {
							   JobType = new JobTypeDto
							   {
								   Name = o.Name,
								   DisplayOrder = o.DisplayOrder,
								   Id = o.Id,
								   IsActive = o.IsActive,
							   }
						   };

			var totalCount = await filteredJobTypes.CountAsync();

			return new PagedResultDto<GetJobTypeForViewDto>(
				totalCount,
				await jobTypes.ToListAsync()
			);
		}

		public async Task<GetJobTypeForViewDto> GetJobTypeForView(int id)
		{
			var jobType = await _jobTypeRepository.GetAsync(id);

			var output = new GetJobTypeForViewDto { JobType = ObjectMapper.Map<JobTypeDto>(jobType) };

			return output;
		}

		//[AbpAuthorize(AppPermissions.Pages_JobTypes_Edit)]
		public async Task<GetJobTypeForEditOutput> GetJobTypeForEdit(EntityDto input)
		{
			var jobType = await _jobTypeRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetJobTypeForEditOutput { JobType = ObjectMapper.Map<CreateOrEditJobTypeDto>(jobType) };

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditJobTypeDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		//[AbpAuthorize(AppPermissions.Pages_JobTypes_Create)]
		protected virtual async Task Create(CreateOrEditJobTypeDto input)
		{
			var jobType = ObjectMapper.Map<JobType>(input);

            //if (AbpSession.TenantId != null)
            //{
            //	jobType.TenantId = (int)AbpSession.TenantId;
            //}
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 12;
            dataVaultLog.ActionNote = "Job Type Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _jobTypeRepository.InsertAsync(jobType);
		}

		//[AbpAuthorize(AppPermissions.Pages_JobTypes_Edit)]
		protected virtual async Task Update(CreateOrEditJobTypeDto input)
		{
			var jobType = await _jobTypeRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 12;
            dataVaultLog.ActionNote = "Job Type Updated : " + jobType.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != jobType.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = jobType.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != jobType.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = jobType.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, jobType);
		}

		//[AbpAuthorize(AppPermissions.Pages_JobTypes_Delete)]
		public async Task Delete(EntityDto input)
		{
            var Name = _jobTypeRepository.Get(input.Id).Name;
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 12;
            dataVaultLog.ActionNote = "Job Type Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _jobTypeRepository.DeleteAsync(input.Id);
		}

		public async Task<FileDto> GetJobTypesToExcel(GetAllJobTypesForExcelInput input)
		{

			var filteredJobTypes = _jobTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

			var query = (from o in filteredJobTypes
						 select new GetJobTypeForViewDto()
						 {
							 JobType = new JobTypeDto
							 {
								 Name = o.Name,
								 DisplayOrder = o.DisplayOrder,
								 Id = o.Id,
								 IsActive = o.IsActive,
							 }
						 });


			var jobTypeListDtos = await query.ToListAsync();

			return _jobTypesExcelExporter.ExportToFile(jobTypeListDtos);
		}


	}
}