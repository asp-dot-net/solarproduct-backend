﻿using TheSolarProduct.Jobs;
using System.Collections.Generic;


using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Leads;
using TheSolarProduct.Invoices;
using Abp.UI;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Authorization.Users;
using Abp.AutoMapper;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.SmsTemplates;
using TheSolarProduct.EmailTemplates;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.ApplicationSettings.Dto;
using System.Net.Mail;
using Abp.Net.Mail;
using TheSolarProduct.PreviousJobStatuses;
using Abp.Timing.Timezone;
using TheSolarProduct.JobHistory;
using TheSolarProduct.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.JobTrackers.Dtos;
using TheSolarProduct.Timing;

namespace TheSolarProduct.Jobs
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class JobRefundsAppService : TheSolarProductAppServiceBase, IJobRefundsAppService
	{
		private readonly IRepository<JobRefund> _jobRefundRepository;
		private readonly IRepository<JobStatus> _jobStatusRepository;
		private readonly IJobRefundsExcelExporter _jobRefundsExcelExporter;
		private readonly IRepository<PaymentOption, int> _lookup_paymentOptionRepository;
		private readonly IRepository<Job, int> _lookup_jobRepository;
		private readonly IRepository<Lead, int> _lookup_leadRepository;
		private readonly IRepository<RefundReason, int> _lookup_refundReasonRepository;
		private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
		private readonly IRepository<User, long> _userRepository;
		private readonly IRepository<LeadActivityLog> _leadactivityRepository;
		private readonly UserManager _userManager;
		private readonly IRepository<UserTeam> _userTeamRepository;
		private readonly IRepository<SmsTemplate> _smsTemplateRepository;
		private readonly IRepository<EmailTemplate> _emailTemplateRepository;
		private readonly IApplicationSettingsAppService _applicationSettings;
		private readonly IEmailSender _emailSender;
		private readonly IRepository<PreviousJobStatus> _previousJobStatusRepository;
		private readonly IRepository<InvoicePaymentMethod, int> _lookup_invoicePaymentMethodRepository;
		private readonly ITimeZoneConverter _timeZoneConverter;
		private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly ITimeZoneService _timeZoneService;
        private readonly IRepository<InvoicePaymentMethod, int> _invoicePaymentMethodRepository;
        public JobRefundsAppService(IRepository<JobRefund> jobRefundRepository
			, IRepository<JobStatus> jobStatusRepository
			, IJobRefundsExcelExporter jobRefundsExcelExporter
			, IRepository<PaymentOption, int> lookup_paymentOptionRepository
			, IRepository<Job, int> lookup_jobRepository
			, IRepository<Lead, int> lookup_leadRepository
			, IRepository<RefundReason, int> lookup_refundReasonRepository
			, IRepository<InvoicePayment> invoicePaymentRepository
			, IRepository<User, long> userRepository
			, IRepository<LeadActivityLog> leadactivityRepository
			, UserManager userManager
			, IRepository<UserTeam> userTeamRepository
			, IRepository<SmsTemplate> smsTemplateRepository
			, IRepository<EmailTemplate> emailTemplateRepository
			, IApplicationSettingsAppService applicationSettings
			, IEmailSender emailSender
			, IRepository<PreviousJobStatus> previousJobStatusRepository
			, IRepository<InvoicePaymentMethod, int> lookup_invoicePaymentMethodRepository
			, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
			, ITimeZoneConverter timeZoneConverter
            , ITimeZoneService timeZoneService
			, IRepository<InvoicePaymentMethod, int> invoicePaymentMethodRepository)
        {
			_jobRefundRepository = jobRefundRepository;
			_jobStatusRepository = jobStatusRepository;
			_jobRefundsExcelExporter = jobRefundsExcelExporter;
			_lookup_paymentOptionRepository = lookup_paymentOptionRepository;
			_lookup_jobRepository = lookup_jobRepository;
			_lookup_leadRepository = lookup_leadRepository;
			_lookup_refundReasonRepository = lookup_refundReasonRepository;
			_invoicePaymentRepository = invoicePaymentRepository;
			_userRepository = userRepository;
			_leadactivityRepository = leadactivityRepository;
			_userManager = userManager;
			_userTeamRepository = userTeamRepository;
			_smsTemplateRepository = smsTemplateRepository;
			_emailTemplateRepository = emailTemplateRepository;
			_applicationSettings = applicationSettings;
			_emailSender = emailSender;
			_previousJobStatusRepository = previousJobStatusRepository;
			_timeZoneConverter = timeZoneConverter; _lookup_invoicePaymentMethodRepository = lookup_invoicePaymentMethodRepository;
			_dbcontextprovider = dbcontextprovider;
			_timeZoneService = timeZoneService;
			_invoicePaymentMethodRepository = invoicePaymentMethodRepository;
		}

		//RefundTracker
		public async Task<PagedResultDto<GetJobRefundForViewDto>> GetAll(GetAllJobRefundsInput input)
		{
			var SDate = (_timeZoneConverter.Convert(input.MinPaidDateFilter, (int)AbpSession.TenantId));
			var EDate = (_timeZoneConverter.Convert(input.MaxPaidDateFilter, (int)AbpSession.TenantId));

			var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
			IList<string> role = await _userManager.GetRolesAsync(User);
			var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
			var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
			var leadactivitylog = _leadactivityRepository.GetAll().Where(e => e.SectionId == 4 && e.LeadFk.OrganizationId == input.OrganizationUnit);
			var filteredJobRefunds = _jobRefundRepository.GetAll()
						.Include(e => e.PaymentOptionFk)
						.Include(e => e.JobFk)
						.Include(e => e.RefundReasonFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFk.JobNumber.Contains(input.Filter) || e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.JobFk.LeadFk.Mobile.Contains(input.Filter) || e.JobFk.LeadFk.Phone.Contains(input.Filter))
						//.WhereIf(input.MinAmountFilter != 0, e => e.JobFk.JobStatusId == input.MinAmountFilter)
						//.WhereIf(input.MaxAmountFilter != 0, e => e.RefundReasonId == input.MaxAmountFilter)
						.WhereIf(input.AccountNoFilter == "Receive" && SDate != null && EDate != null, e => e.CreationTime.AddHours(10).Date <= SDate.Value.Date && e.CreationTime.AddHours(10).Date >= EDate.Value.Date)
						.WhereIf(input.AccountNoFilter == "Complete" && SDate != null && EDate != null, e => e.PaidDate.Value.AddHours(10).Date <= SDate.Value.Date && e.PaidDate.Value.AddHours(10).Date >= EDate.Value.Date)
						.WhereIf(input.applicationstatus != 0 && input.applicationstatus == 1, e => e.PaidDate != null)
						.WhereIf(input.applicationstatus != 0 && input.applicationstatus == 2, e => e.PaidDate == null)
						.WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
						.WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
						.WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
						.WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesRepId)
						//.WhereIf(input.JobStatusID != null && input.JobStatusID != 0, e => e.JobFk.JobStatusId == input.JobStatusID)
						.WhereIf(input.JobStatusID != null && input.JobStatusID.Count() > 0, e => input.JobStatusID.Contains((int)e.JobFk.JobStatusId))
						.WhereIf(input.JobRefundID != null && input.JobRefundID != 0, e => e.RefundReasonId == input.JobRefundID)
						.WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesManagerId)
						.Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit);


			var pagedAndFilteredJobRefunds = filteredJobRefunds
				.OrderBy(input.Sorting ?? "id desc")
				.PageBy(input);

			var jobRefunds = from o in pagedAndFilteredJobRefunds
							 join o1 in _lookup_paymentOptionRepository.GetAll() on o.PaymentOptionId equals o1.Id into j1
							 from s1 in j1.DefaultIfEmpty()

							 join o2 in _lookup_jobRepository.GetAll() on o.JobId equals o2.Id into j2
							 from s2 in j2.DefaultIfEmpty()

							 join o3 in _lookup_refundReasonRepository.GetAll() on o.RefundReasonId equals o3.Id into j3
							 from s3 in j3.DefaultIfEmpty()

							 join o4 in _lookup_leadRepository.GetAll() on o.JobFk.LeadId equals o4.Id into j4
							 from s4 in j4.DefaultIfEmpty()

							 join o5 in _userRepository.GetAll() on o.CreatorUserId equals o5.Id into j5
							 from s5 in j5.DefaultIfEmpty()

                             join o6 in _lookup_invoicePaymentMethodRepository.GetAll() on o.PaymentOptionId equals o6.Id into j6
                             from o6 in j6.DefaultIfEmpty()

                                 //join o6 in _previousJobStatusRepository.GetAll() on o.JobId equals o6.JobId into j6
                                 //from s6 in j6.DefaultIfEmpty()

                             select new GetJobRefundForViewDto()
							 {
								 JobRefund = new JobRefundDto
								 {
									 Amount = o.Amount,
									 BankName = o.BankName,
									 AccountName = o.AccountName,
									 BSBNo = o.BSBNo,
									 AccountNo = o.AccountNo,
									 PaidDate = o.PaidDate,
									 Remarks = o.Remarks,
									 Notes = o.Notes,
									 Id = o.Id,
									 CreationTime = o.CreationTime,
									 JobId = o.JobId,
									 ReceiptNo = o.ReceiptNo,
									 JobRefundSmsSend = o.JobRefundSmsSend,
									 JobRefundSmsSendDate = o.JobRefundSmsSendDate,
									 JobRefundEmailSend = o.JobRefundEmailSend,
									 JobRefundEmailSendDate = o.JobRefundEmailSendDate,
									 JobRefundtype=o6.PaymentMethod,
									 IsVerify = o.IsVerify
								 },
								 PaidBy = s5.FullName,
								 CompanyName = s4 == null || s4.CompanyName == null ? "" : s4.CompanyName.ToString(),
								 ProjectName = s2 == null || s2.JobNumber == null ? "" : s2.JobNumber,
								 PaymentOptionName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
								 ProjectStatus = _jobStatusRepository.GetAll().Where(e => e.Id == o.JobFk.JobStatusId).Select(e => e.Name).FirstOrDefault(),
								 JobNote = s2 == null || s2.Note == null ? "" : s2.Note.ToString(),
								 RefundReasonName = s3 == null || s3.Name == null ? "" : s3.Name.ToString(),
								 CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == o.JobFk.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
								 RequestedBy = s5.FullName,
                                 verifyornot = o.PaidDate != null ? true  : o.IsVerify == true ? true : false,
                                 LeadId = s4.Id,
								 PreviousProjectStatus = _jobStatusRepository.GetAll().Where(e => e.Id == _previousJobStatusRepository.GetAll().Where(e => e.JobId == o.JobId).OrderByDescending(e => e.Id).Select(e => e.PreviousId).FirstOrDefault()).Select(e => e.Name).FirstOrDefault(),
								 ActivityReminderTime = leadactivitylog.Where(e => e.SectionId == 4 && e.ActionId == 8 && e.LeadId == s4.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
								 ActivityDescription = leadactivitylog.Where(e => e.SectionId == 4 && e.ActionId == 8 && e.LeadId == s4.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
								 ActivityComment = leadactivitylog.Where(e => e.SectionId == 4 && e.ActionId == 24 && e.LeadId == s4.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
							 };

			var totalCount = await filteredJobRefunds.CountAsync();

			return new PagedResultDto<GetJobRefundForViewDto>(
				totalCount,
				await jobRefunds.ToListAsync()
			);
		}

		public async Task<GetJobRefundForViewDto> GetJobRefundForView(int id)
		{
			var jobRefund = await _jobRefundRepository.GetAsync(id);

			var output = new GetJobRefundForViewDto { JobRefund = ObjectMapper.Map<JobRefundDto>(jobRefund) };

			if (output.JobRefund.PaymentOptionId != null)
			{
				var _lookupPaymentOption = await _lookup_paymentOptionRepository.FirstOrDefaultAsync((int)output.JobRefund.PaymentOptionId);
				output.PaymentOptionName = _lookupPaymentOption?.Name?.ToString();
			}

			if (output.JobRefund.JobId != null)
			{
				var _lookupJob = await _lookup_jobRepository.FirstOrDefaultAsync((int)output.JobRefund.JobId);
				output.JobNote = _lookupJob?.Note?.ToString();
			}

			if (output.JobRefund.RefundReasonId != null)
			{
				var _lookupRefundReason = await _lookup_refundReasonRepository.FirstOrDefaultAsync((int)output.JobRefund.RefundReasonId);
				output.RefundReasonName = _lookupRefundReason?.Name?.ToString();
			}

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_Job_Refunds_Edit, AppPermissions.Pages_Jobs, AppPermissions.Pages_RefundTracker_Refund)]
		public async Task<GetJobRefundForEditOutput> GetJobRefundForEdit(EntityDto input)
		{
			var jobRefund = await _jobRefundRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetJobRefundForEditOutput { JobRefund = ObjectMapper.Map<CreateOrEditJobRefundDto>(jobRefund) };

			var leadid = _lookup_jobRepository.GetAll().Where(e => e.Id == jobRefund.JobId).Select(e => e.LeadId).FirstOrDefault();
			output.JobNumber = _lookup_jobRepository.GetAll().Where(e => e.Id == jobRefund.JobId).Select(e => e.JobNumber).FirstOrDefault();
			output.LeadCompanyName = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.CompanyName).FirstOrDefault();
			output.XeroInvCreated=jobRefund.XeroInvCreated;
			output.ReceiptNo = jobRefund.ReceiptNo;
			output.Amount = jobRefund.Amount;
			output.PaymentOptionId = jobRefund.PaymentOptionId;
			if (output.JobRefund.PaymentOptionId != null)
			{
				var _lookupPaymentOption = await _lookup_paymentOptionRepository.FirstOrDefaultAsync((int)output.JobRefund.PaymentOptionId);
				output.PaymentOptionName = _lookupPaymentOption?.Name?.ToString();
			}

			if (output.JobRefund.JobId != null)
			{
				var _lookupJob = await _lookup_jobRepository.FirstOrDefaultAsync((int)output.JobRefund.JobId);
				output.JobNote = _lookupJob?.Note?.ToString();
			}

			if (output.JobRefund.RefundReasonId != null)
			{
				var _lookupRefundReason = await _lookup_refundReasonRepository.FirstOrDefaultAsync((int)output.JobRefund.RefundReasonId);
				output.RefundReasonName = _lookupRefundReason?.Name?.ToString();
			}

			return output;
		}

		//[AbpAuthorize(AppPermissions.Pages_Jobs)]
		public async Task<List<CreateOrEditJobRefundDto>> GetJobRefundByJobId(EntityDto input)
		{
			return await _jobRefundRepository.GetAll().Include(e => e.RefundReasonFk)
				.Where(e => e.JobId == input.Id)
			.Select(refund => new CreateOrEditJobRefundDto
			{
				Id = refund.Id,
				AccountName = refund.AccountName,
				AccountNo = refund.AccountNo,
				Amount = refund.Amount,
				BankName = refund.BankName,
				BSBNo = refund.BSBNo,
				Notes = refund.Notes,
				PaidDate = refund.PaidDate,
				PaymentOptionId = refund.PaymentOptionId,
				RefundReasonId = refund.RefundReasonId,
                RefundReason = refund.RefundReasonFk.Name,
                Remarks = refund.Remarks,
				ReceiptNo = refund.ReceiptNo,
				RefundPaymentType = refund.RefundPaymentType
			}).OrderByDescending(e => e.Id).ToListAsync();
		}

		public async Task CreateOrEdit(CreateOrEditJobRefundDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_Job_Refunds_Create, AppPermissions.Pages_Jobs)]
		protected virtual async Task Create(CreateOrEditJobRefundDto input)
		{
			input.PaidDate = _timeZoneConverter.Convert(input.PaidDate, (int)AbpSession.TenantId);

            var jobRefund = ObjectMapper.Map<JobRefund>(input);

			if (AbpSession.TenantId != null)
			{
				jobRefund.TenantId = (int)AbpSession.TenantId;
			}
			jobRefund.IsVerify = false;
			await _jobRefundRepository.InsertAsync(jobRefund);

			var Jobs = _lookup_jobRepository.GetAll().Where(e => e.Id == input.JobId).FirstOrDefault();
			Jobs.RefundProcess = true;
			if(input.RefundReasonId == 1)
			{
                PreviousJobStatus jSt = new PreviousJobStatus();
                jSt.JobId = Jobs.Id;
                if (AbpSession.TenantId != null)
                {
                    jSt.TenantId = (int)AbpSession.TenantId;
                }
                jSt.CurrentID = 3;
                jSt.PreviousId = (int)Jobs.JobStatusId;
                await _previousJobStatusRepository.InsertAsync(jSt);


                Jobs.JobStatusId = 3;
				Jobs.JobCancelReasonId = input.JobCancelReasonId;

            }
			await _lookup_jobRepository.UpdateAsync(Jobs);

			var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
			LeadActivityLog leadactivity = new LeadActivityLog();
			leadactivity.ActionId = 15;
			leadactivity.SectionId = input.SectionId != null ? input.SectionId : 0;
			leadactivity.ActionNote = "Job Refund Request Create";
			leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
			if (AbpSession.TenantId != null)
			{
				leadactivity.TenantId = (int)AbpSession.TenantId;
			}
			await _leadactivityRepository.InsertAsync(leadactivity);

			if (input.RefundReasonId == 1)
			{
				LeadActivityLog leadactivity1 = new LeadActivityLog();
				leadactivity1.ActionId = 14;
                leadactivity1.SectionId = input.SectionId != null ? input.SectionId : 0;
                leadactivity1.ActionNote = "Job Status Change To Cancel";
				leadactivity1.LeadId = Convert.ToInt32(Jobs.LeadId);
				if (AbpSession.TenantId != null)
				{
					leadactivity1.TenantId = (int)AbpSession.TenantId;
				}
				await _leadactivityRepository.InsertAsync(leadactivity1);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_Job_Refunds_Edit, AppPermissions.Pages_Jobs, AppPermissions.Pages_RefundTracker_Refund)]
		protected virtual async Task Update(CreateOrEditJobRefundDto input)
		{
			var jobRefund = await _jobRefundRepository.FirstOrDefaultAsync((int)input.Id);

			var Jobs = _lookup_jobRepository.GetAll().Where(e => e.Id == jobRefund.JobId).FirstOrDefault();

			var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
			LeadActivityLog leadactivity = new LeadActivityLog();
			leadactivity.ActionId = 15;
			leadactivity.SectionId = input.SectionId != null ? input.SectionId : 0;
			leadactivity.ActionNote = "Job Refund Request Completed";
			leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
			if (AbpSession.TenantId != null)
			{
				leadactivity.TenantId = (int)AbpSession.TenantId;
			}
			var jobactionid =  _leadactivityRepository.InsertAndGetId(leadactivity);

			var list = new List<JobTrackerHistory>();
			if (jobRefund.RefundReasonId != input.RefundReasonId)
			{
				var RefundReasonList = _lookup_refundReasonRepository.GetAll();
				JobTrackerHistory jobhistory = new JobTrackerHistory();
				if (AbpSession.TenantId != null)
				{
					jobhistory.TenantId = (int)AbpSession.TenantId;
				}
				jobhistory.FieldName = "Refund Reason";
				jobhistory.PrevValue = RefundReasonList.Where(e=>e.Id == jobRefund.RefundReasonId).Select(e=>e.Name).FirstOrDefault();
				jobhistory.CurValue = RefundReasonList.Where(e => e.Id == input.RefundReasonId).Select(e => e.Name).FirstOrDefault();
				jobhistory.Action = "Refund Detail Edit";
				jobhistory.LastmodifiedDateTime = DateTime.Now;
				jobhistory.JobIDId = (int)jobRefund.JobId;
				jobhistory.JobActionId = jobactionid;
				list.Add(jobhistory);
			}

			if (jobRefund.Amount != input.Amount)
			{
				JobTrackerHistory jobhistory = new JobTrackerHistory();
				if (AbpSession.TenantId != null)
				{
					jobhistory.TenantId = (int)AbpSession.TenantId;
				}
				jobhistory.FieldName = "Amount";
				jobhistory.PrevValue = jobRefund.Amount.ToString();
				jobhistory.CurValue = input.Amount.ToString();
				jobhistory.Action = "Refund Detail Edit";
				jobhistory.LastmodifiedDateTime = DateTime.Now;
				jobhistory.JobIDId = (int)jobRefund.JobId;
				jobhistory.JobActionId = jobactionid;
				list.Add(jobhistory);
			}

            if (jobRefund.PaidAmount != input.PaidAmount)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Paid Amount";
                jobhistory.PrevValue = jobRefund.PaidAmount.ToString();
                jobhistory.CurValue = input.PaidAmount.ToString();
                jobhistory.Action = "Refund Detail Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = (int)jobRefund.JobId;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (jobRefund.BankName != input.BankName)
			{
				JobTrackerHistory jobhistory = new JobTrackerHistory();
				if (AbpSession.TenantId != null)
				{
					jobhistory.TenantId = (int)AbpSession.TenantId;
				}
				jobhistory.FieldName = "Bank Name";
				jobhistory.PrevValue = jobRefund.BankName;
				jobhistory.CurValue = input.BankName;
				jobhistory.Action = "Refund Detail Edit";
				jobhistory.LastmodifiedDateTime = DateTime.Now;
				jobhistory.JobIDId = (int)jobRefund.JobId;
				jobhistory.JobActionId = jobactionid;
				list.Add(jobhistory);
			}

			if (jobRefund.AccountName != input.AccountName)
			{
				JobTrackerHistory jobhistory = new JobTrackerHistory();
				if (AbpSession.TenantId != null)
				{
					jobhistory.TenantId = (int)AbpSession.TenantId;
				}
				jobhistory.FieldName = "AccountName";
				jobhistory.PrevValue = jobRefund.AccountName;
				jobhistory.CurValue = input.AccountName;
				jobhistory.Action = "Refund Detail Edit";
				jobhistory.LastmodifiedDateTime = DateTime.Now;
				jobhistory.JobIDId = (int)jobRefund.JobId;
				jobhistory.JobActionId = jobactionid;
				list.Add(jobhistory);
			}
            if (jobRefund.XeroInvCreated != input.XeroInvCreated)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "XeroInvCreated";
                jobhistory.PrevValue = jobRefund.XeroInvCreated.ToString();
                jobhistory.CurValue = input.XeroInvCreated.ToString();
                jobhistory.Action = "Refund Detail Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = (int)jobRefund.JobId;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (jobRefund.BSBNo != input.BSBNo)
			{
				JobTrackerHistory jobhistory = new JobTrackerHistory();
				if (AbpSession.TenantId != null)
				{
					jobhistory.TenantId = (int)AbpSession.TenantId;
				}
				jobhistory.FieldName = "BSBNo";
				jobhistory.PrevValue = jobRefund.BSBNo;
				jobhistory.CurValue = input.BSBNo;
				jobhistory.Action = "Refund Detail Edit";
				jobhistory.LastmodifiedDateTime = DateTime.Now;
				jobhistory.JobIDId = (int)jobRefund.JobId;
				jobhistory.JobActionId = jobactionid;
				list.Add(jobhistory);
			}

			if (jobRefund.AccountNo != input.AccountNo)
			{
				JobTrackerHistory jobhistory = new JobTrackerHistory();
				if (AbpSession.TenantId != null)
				{
					jobhistory.TenantId = (int)AbpSession.TenantId;
				}
				jobhistory.FieldName = "AccountNo";
				jobhistory.PrevValue = jobRefund.AccountNo;
				jobhistory.CurValue = input.AccountNo;
				jobhistory.Action = "Refund Detail Edit";
				jobhistory.LastmodifiedDateTime = DateTime.Now;
				jobhistory.JobIDId = (int)jobRefund.JobId;
				jobhistory.JobActionId = jobactionid;
				list.Add(jobhistory);
			}
			
			if (jobRefund.Notes != input.Notes)
			{
				JobTrackerHistory jobhistory = new JobTrackerHistory();
				if (AbpSession.TenantId != null)
				{
					jobhistory.TenantId = (int)AbpSession.TenantId;
				}
				jobhistory.FieldName = "Notes";
				jobhistory.PrevValue = jobRefund.Notes;
				jobhistory.CurValue = input.Notes;
				jobhistory.Action = "Refund Detail Edit";
				jobhistory.LastmodifiedDateTime = DateTime.Now;
				jobhistory.JobIDId = (int)jobRefund.JobId;
				jobhistory.JobActionId = jobactionid;
				list.Add(jobhistory);
			}
            if (jobRefund.PaidDate != input.PaidDate)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "PaidDate";
                jobhistory.PrevValue = jobRefund.PaidDate.ToString();
                jobhistory.CurValue = input.PaidDate.ToString();
                jobhistory.Action = "Refund Detail Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = (int)jobRefund.JobId;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (jobRefund.PaymentOptionId != input.PaymentOptionId)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "PaymentOption";
                jobhistory.PrevValue = jobRefund.PaymentOptionId.ToString();
                jobhistory.CurValue = input.PaymentOptionId.ToString();
                jobhistory.Action = "Refund Detail Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = (int)jobRefund.JobId;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (jobRefund.ReceiptNo != input.ReceiptNo)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "ReceiptNo";
                jobhistory.PrevValue = jobRefund.ReceiptNo;
                jobhistory.CurValue = input.ReceiptNo;
                jobhistory.Action = "Refund Detail Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = (int)jobRefund.JobId;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (jobRefund.RefundPaymentType != input.RefundPaymentType)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Refund PaymentType";
                jobhistory.PrevValue = jobRefund.RefundPaymentType;
                jobhistory.CurValue = input.RefundPaymentType;
                jobhistory.Action = "Refund Detail Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = (int)jobRefund.JobId;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
			await _dbcontextprovider.GetDbContext().SaveChangesAsync();
			ObjectMapper.Map(input, jobRefund);
		}

		[AbpAuthorize(AppPermissions.Pages_Job_Refunds_Delete)]
		public async Task Delete(EntityDto input)
		{
			var jobRefund = await _jobRefundRepository.FirstOrDefaultAsync((int)input.Id);
			var Jobs = _lookup_jobRepository.GetAll().Where(e => e.Id == jobRefund.JobId).FirstOrDefault();

			var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
			LeadActivityLog leadactivity = new LeadActivityLog();
			leadactivity.ActionId = 15;
			leadactivity.SectionId = 0;
			leadactivity.ActionNote = "Job Refund Request Deleted";
			leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
			if (AbpSession.TenantId != null)
			{
				leadactivity.TenantId = (int)AbpSession.TenantId;
			}
			await _leadactivityRepository.InsertAsync(leadactivity);

			await _jobRefundRepository.DeleteAsync(input.Id);
		}

		public async Task<FileDto> GetJobRefundsToExcel(GetAllJobRefundsForExcelInput input)
		{
			//var SDate = (_timeZoneConverter.Convert(input.MinPaidDateFilter, (int)AbpSession.TenantId));
			//var EDate = (_timeZoneConverter.Convert(input.MaxPaidDateFilter, (int)AbpSession.TenantId));

			//var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
			//IList<string> role = await _userManager.GetRolesAsync(User);
			//var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
			//var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

			//var filteredJobRefunds = _jobRefundRepository.GetAll()
			//			.Include(e => e.PaymentOptionFk)
			//			.Include(e => e.JobFk)
			//			.Include(e => e.RefundReasonFk)
			//			.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFk.JobNumber.Contains(input.Filter) || e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter))
			//			.WhereIf(input.MinAmountFilter != null, e => e.JobFk.JobStatusId == input.MinAmountFilter)
			//			.WhereIf(input.MaxAmountFilter != null, e => e.RefundReasonId == input.MaxAmountFilter)
			//			.WhereIf(input.AccountNoFilter == "Receive" && SDate != null && EDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
			//			.WhereIf(input.AccountNoFilter == "Complete" && SDate != null && EDate != null, e => e.PaidDate.Value.AddHours(10).Date >= SDate.Value.Date && e.PaidDate.Value.AddHours(10).Date <= EDate.Value.Date)
			//			.WhereIf(input.applicationstatus != 0 && input.applicationstatus == 1, e => e.PaidDate != null)
			//			.WhereIf(input.applicationstatus != 0 && input.applicationstatus == 2, e => e.PaidDate == null)
			//			.WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
			//			.WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
			//			.WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
			//		    .WhereIf(input.JobStatusID != null && input.JobStatusID.Count() > 0, e => input.JobStatusID.Contains((int)e.JobFk.JobStatusId))
			//			.WhereIf(input.JobRefundID != null && input.JobRefundID != 0, e => e.RefundReasonId == input.JobRefundID)
			//			.WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesRepId)
			//			.WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesManagerId)
			//			.Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit);

			//var SDate = (_timeZoneConverter.Convert(input.MinPaidDateFilter, (int)AbpSession.TenantId));
			//var EDate = (_timeZoneConverter.Convert(input.MaxPaidDateFilter, (int)AbpSession.TenantId));

			//var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
			//IList<string> role = await _userManager.GetRolesAsync(User);
			//var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
			//var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
			//var leadactivitylog = _leadactivityRepository.GetAll().Where(e => e.SectionId == 4 && e.LeadFk.OrganizationId == input.OrganizationUnit);
			//var filteredJobRefunds = _jobRefundRepository.GetAll()
			//			.Include(e => e.PaymentOptionFk)
			//			.Include(e => e.JobFk)
			//			.Include(e => e.RefundReasonFk)
			//			.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFk.JobNumber.Contains(input.Filter) || e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.JobFk.LeadFk.Mobile.Contains(input.Filter) || e.JobFk.LeadFk.Phone.Contains(input.Filter))
			//			//.WhereIf(input.MinAmountFilter != 0, e => e.JobFk.JobStatusId == input.MinAmountFilter)
			//			//.WhereIf(input.MaxAmountFilter != 0, e => e.RefundReasonId == input.MaxAmountFilter)
			//			.WhereIf(input.AccountNoFilter == "Receive" && SDate != null && EDate != null, e => e.CreationTime.AddHours(10).Date <= SDate.Value.Date && e.CreationTime.AddHours(10).Date >= EDate.Value.Date)
			//			.WhereIf(input.AccountNoFilter == "Complete" && SDate != null && EDate != null, e => e.PaidDate.Value.AddHours(10).Date <= SDate.Value.Date && e.PaidDate.Value.AddHours(10).Date >= EDate.Value.Date)
			//			.WhereIf(input.applicationstatus != 0 && input.applicationstatus == 1, e => e.PaidDate != null)
			//			.WhereIf(input.applicationstatus != 0 && input.applicationstatus == 2, e => e.PaidDate == null)
			//			.WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
			//			.WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
			//			.WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
			//			.WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesRepId)
			//			//.WhereIf(input.JobStatusID != null && input.JobStatusID != 0, e => e.JobFk.JobStatusId == input.JobStatusID)
			//			.WhereIf(input.JobStatusID != null && input.JobStatusID.Count() > 0, e => input.JobStatusID.Contains((int)e.JobFk.JobStatusId))
			//			.WhereIf(input.JobRefundID != null && input.JobRefundID != 0, e => e.RefundReasonId == input.JobRefundID)
			//			.WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesManagerId)
			//			.Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit);

			//var query = (from o in filteredjobrefunds
			//			 join o1 in _lookup_paymentoptionrepository.getall() on o.paymentoptionid equals o1.id into j1
			//			 from s1 in j1.defaultifempty()

			//			 join o2 in _lookup_jobrepository.getall() on o.jobid equals o2.id into j2
			//			 from s2 in j2.defaultifempty()

			//			 join o3 in _lookup_refundreasonrepository.getall() on o.refundreasonid equals o3.id into j3
			//			 from s3 in j3.defaultifempty()

			//			 join o4 in _lookup_leadrepository.getall() on o.jobfk.leadid equals o4.id into j4
			//			 from s4 in j4.defaultifempty()

			//			 join o5 in _userrepository.getall() on o.creatoruserid equals o5.id into j5
			//			 from s5 in j5.defaultifempty()

			//			 select new getjobrefundforviewdto()
			//			 {
			//				 jobrefund = new jobrefunddto
			//				 {
			//					 amount = o.amount,
			//					 bankname = o.bankname,
			//					 accountname = o.accountname,
			//					 bsbno = o.bsbno,
			//					 accountno = o.accountno,
			//					 paiddate = o.paiddate,
			//					 remarks = o.remarks,
			//					 notes = o.notes,
			//					 id = o.id,
			//					 creationtime = o.creationtime,

			//				 },
			//				 paidby = s5.fullname,
			//				 projectname = s2 == null || s2.jobnumber == null ? "" : s2.jobnumber,
			//				 paymentoptionname = s1 == null || s1.name == null ? "" : s1.name.tostring(),
			//				 projectstatus = _jobstatusrepository.getall().where(e => e.id == o.jobfk.jobstatusid).select(e => e.name).firstordefault(),
			//				 jobnote = s2 == null || s2.note == null ? "" : s2.note.tostring(),
			//				 refundreasonname = s3 == null || s3.name == null ? "" : s3.name.tostring(),
			//				 currentleadowaner = _userrepository.getall().where(e => e.id == o.jobfk.leadfk.assigntouserid).select(e => e.fullname).firstordefault(),
			//				 requestedby = s5.fullname
			//			 });
			var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();
            var Today = _timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId);

            var filteredResult = _jobRefundRepository.GetAll()
                              .Include(e => e.RefundReasonFk)
                              .Include(e => e.JobFk)
                              .Include(e => e.PaymentOptionFk)
                              .Include(e => e.JobFk.LeadFk)
                              .Include(e => e.JobFk.JobStatusFk)
                              .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                              .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit && e.JobFk.FirstDepositDate != null)
                              .AsNoTracking()
                              .Select(e => new { e.Id, e.JobFk, e.JobId, e.RefundReasonId, e.IsVerify, e.CreationTime, e.PaidDate, e.PaymentOptionId, e.Amount, e.PaidAmount, e.BSBNo, e.AccountNo, e.RefundReasonFk, e.JobRefundSmsSend, e.JobRefundSmsSendDate, e.JobRefundEmailSend, e.JobRefundEmailSendDate, e.CreatorUserId, e.JobFk.LeadId, e.JobFk.LeadFk.AssignToUserID, e.Notes, e.RefundPaymentType });

            //#region Summary Count
            //RefundSummaryCountDto summary = new RefundSummaryCountDto();
            //summary.Total = await filteredResult.CountAsync();
            //summary.TotalAmt = await filteredResult.SumAsync(e => e.Amount == null ? 0 : (decimal)e.Amount);

            //summary.Pending = await filteredResult.Where(e => e.PaidDate == null).CountAsync();
            //summary.PendingAmt = await filteredResult.Where(e => e.PaidDate == null).SumAsync(e => e.Amount == null ? 0 : (decimal)e.Amount);

            //summary.Refunded = await filteredResult.Where(e => e.PaidDate != null).CountAsync();
            //summary.RefundedAmt = await filteredResult.Where(e => e.PaidDate != null).SumAsync(e => e.PaidAmount == null ? 0 : (decimal)e.PaidAmount);
            //#endregion

            var filter = filteredResult
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Email == input.Filter)
                        .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Phone == input.Filter)
						.WhereIf(!string.IsNullOrEmpty(input.RefundPaymentType) , e => e.RefundPaymentType == input.RefundPaymentType)
                        .WhereIf(input.SalesRep != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesRep)
                        .WhereIf(input.JobStatus != null && input.JobStatus.Count() > 0, e => input.JobStatus.Contains((int)e.JobFk.JobStatusId))
                        .WhereIf(input.RefundReason != 0, e => e.RefundReasonId == input.RefundReason)

                        .WhereIf(input.IsVerify != null, e => e.IsVerify == input.IsVerify)

                        .WhereIf(input.DateFilter == "Receive" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.DateFilter == "Receive" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.DateFilter == "Complete" && input.StartDate != null, e => e.PaidDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateFilter == "Complete" && input.EndDate != null, e => e.PaidDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.ApplicationStatus == 1, e => e.PaidDate != null) // Refunded
                        .WhereIf(input.ApplicationStatus == 2, e => e.PaidDate == null) //Pending


                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.JobFk.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.JobFk.LeadFk.Area == input.AreaNameFilter)
                        ;

            var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 4 && e.LeadFk.OrganizationId == input.OrganizationUnit).AsNoTracking().Select(e => new { e.SectionId, e.ActionId, e.LeadId, e.Id, e.ActivityDate, e.ActivityNote });
            
			//var activityIds = _leadactivityRepository.GetAll().Where(e => e.ActionNote == "Job Refund Verify").GroupBy(e => e.LeadId).Select(e => e.Max(m => m.Id));

   //         var verifyActivity = _leadactivityRepository.GetAll().Where(e => activityIds.Contains(e.Id)).Select(e => new { e.LeadId, e.CreatorUserId, e.CreationTime});

            var results = from o in filter
						  join o6 in _invoicePaymentMethodRepository.GetAll().AsNoTracking() on o.PaymentOptionId equals o6.Id into j6
						  from o6 in j6.DefaultIfEmpty()

                          let veryfyuser = User_List.Where(e => e.Id == _leadactivityRepository.GetAll().Where(e => e.LeadId == o.LeadId && e.ActionNote == "Job Refund Verify").OrderByDescending(e => e.Id).AsNoTracking().Select(e => e.CreatorUserId).FirstOrDefault()).AsNoTracking().Select(e => e.FullName).FirstOrDefault()

                          let veryfytime = _leadactivityRepository.GetAll().Where(e => e.LeadId == o.LeadId && e.ActionNote == "Job Refund Verify").OrderByDescending(e => e.Id).AsNoTracking().Select(e => e.CreationTime.AddHours(diffHour).Date).FirstOrDefault()

                          select new GetViewRefundTrackerDto()
                          {
                              Id = o.Id,
                              JobId = o.JobFk.Id,
                              LeadId = o.JobFk.LeadFk.Id,
                              JobNumber = o.JobFk.JobNumber,
                              CompanyName = o.JobFk.LeadFk.CompanyName,
                              CreationTime = o.CreationTime,
                              Amount = o.Amount,
                              PaidAmount = o.PaidAmount,
                              BSBNo = o.BSBNo,
                              AccountNo = o.AccountNo,
                              CurrentLeadOwaner = User_List.Where(e => e.Id == o.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                              RefundReasonName = o.RefundReasonFk.Name,
                              PaymentOptionName = o6.PaymentMethod,
                              PaidDate = o.PaidDate,
                              PreviousProjectStatus = _jobStatusRepository.GetAll().Where(e => e.Id == _previousJobStatusRepository.GetAll().Where(e => e.JobId == o.JobId).OrderByDescending(e => e.Id).AsNoTracking().Select(e => e.PreviousId).FirstOrDefault()).AsNoTracking().Select(e => e.Name).FirstOrDefault(),
                              ProjectStatus = o.JobFk.JobStatusFk.Name,
                              JobStatusColorClass = o.JobFk.JobStatusFk.ColorClass,
                              JobRefundSmsSend = o.JobRefundSmsSend,
                              JobRefundSmsSendDate = o.JobRefundSmsSendDate,
                              JobRefundEmailSend = o.JobRefundEmailSend,
                              JobRefundEmailSendDate = o.JobRefundEmailSendDate,
                              RequestedBy = User_List.Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),

                              Verifyornot = o.PaidDate != null ? true : o.IsVerify == true ? true : false,

                              ActivityReminderTime = activity.Where(e => e.SectionId == 4 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                              ActivityDescription = activity.Where(e => e.SectionId == 4 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              ActivityComment = activity.Where(e => e.SectionId == 4 && e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                              RefundNotes = o.Notes,
                              PaymentType = o.RefundPaymentType,
                              VerifyBy = veryfyuser != null ? veryfyuser : "",

                              NotVerifiedDays = o.IsVerify != true && veryfytime == null ? (int)(Today.Value.Date - o.CreationTime.AddHours(diffHour).Date).TotalDays : 0,
							  NotPaidDays = o.PaidAmount == null && veryfytime != null ? (int)(Today.Value.Date - veryfytime).TotalDays : 0,
							  //Summary = summary
                          };


            var jobRefundListDtos = await results.ToListAsync();
			if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
			{
				return _jobRefundsExcelExporter.ExportToFile(jobRefundListDtos,"JobRefunds.xlsx");
			}
			else
			{
				return _jobRefundsExcelExporter.ExportToFile(jobRefundListDtos,"JobRefunds.csv");
			}

			
		}

		public async Task<List<JobRefundPaymentOptionLookupTableDto>> GetAllPaymentOptionForTableDropdown()
		{
			return await _lookup_paymentOptionRepository.GetAll()
				.Select(paymentOption => new JobRefundPaymentOptionLookupTableDto
				{
					Id = paymentOption.Id,
					DisplayName = paymentOption == null || paymentOption.Name == null ? "" : paymentOption.Name.ToString()
				}).ToListAsync();
		}

		//public async Task<List<JobRefundJobLookupTableDto>> GetAllJobForTableDropdown()
		//{
		//	return await _lookup_jobRepository.GetAll()
		//		.Select(job => new JobRefundJobLookupTableDto
		//		{
		//			Id = job.Id,
		//			DisplayName = job == null || job.Note == null ? "" : job.Note.ToString()
		//		}).ToListAsync();
		//}

		public async Task<List<JobRefundRefundReasonLookupTableDto>> GetAllRefundReasonForTableDropdown()
		{
			return await _lookup_refundReasonRepository.GetAll()
				.Select(refundReason => new JobRefundRefundReasonLookupTableDto
				{
					Id = refundReason.Id,
					DisplayName = refundReason == null || refundReason.Name == null ? "" : refundReason.Name.ToString()
				}).ToListAsync();
		}

		public async Task JobRefundSendSms(int id, int smstempid)
		{
			var jobRefund = await _jobRefundRepository.FirstOrDefaultAsync(id);
			var smsText = _smsTemplateRepository.GetAll().Where(e => e.Id == smstempid).Select(e => e.Text).FirstOrDefault();
			var output = ObjectMapper.Map<CreateOrEditJobRefundDto>(jobRefund);
			var leadid = _lookup_jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
			var mobilenumber = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Mobile).FirstOrDefault();
			var Body = smsText;
			if (!string.IsNullOrEmpty(mobilenumber))
			{
				SendSMSInput sendSMSInput = new SendSMSInput();
				sendSMSInput.PhoneNumber = mobilenumber;
				sendSMSInput.Text = Body;
				await _applicationSettings.SendSMS(sendSMSInput);
				output.JobRefundSmsSend = true;
				output.JobRefundSmsSendDate = DateTime.UtcNow;
				ObjectMapper.Map(output, jobRefund);
			}
		}

		public async Task JobRefundSendEmail(int id, int emailtempid)
		{
			var jobRefund = await _jobRefundRepository.FirstOrDefaultAsync(id);
			var emailText = _emailTemplateRepository.GetAll().Where(e => e.Id == emailtempid).Select(e => e.TemplateName).FirstOrDefault();
			var output = ObjectMapper.Map<CreateOrEditJobRefundDto>(jobRefund);
			var leadid = _lookup_jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
			var Email = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Email).FirstOrDefault();
			var Body = emailText;
			if (!string.IsNullOrEmpty(Email))
			{
				MailMessage mail = new MailMessage
				{
					To = { Email },
					Subject = "Status Update",
					Body = Body,
					IsBodyHtml = true
				};
				await this._emailSender.SendAsync(mail);
				output.JobRefundEmailSend = true;
				output.JobRefundSmsSendDate = DateTime.UtcNow;
				ObjectMapper.Map(output, jobRefund);
			}
		}

		public async Task verifyorNotVerifyJobRefund(EntityDto input, Boolean VerifyOrNot,int SectionId)
		{
			var jobRefund = await _jobRefundRepository.FirstOrDefaultAsync((int)input.Id);
			var Jobs = _lookup_jobRepository.GetAll().Where(e => e.Id == jobRefund.JobId).FirstOrDefault();
			if (AbpSession.TenantId != null)
			{
				jobRefund.TenantId = (int)AbpSession.TenantId;
			}
			jobRefund.IsVerify = VerifyOrNot;
			await _jobRefundRepository.UpdateAsync(jobRefund);

			var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
			LeadActivityLog leadactivity = new LeadActivityLog();
			leadactivity.ActionId = 15;
			leadactivity.SectionId = SectionId != null ? SectionId : 0;
			if (VerifyOrNot == true)
			{
				leadactivity.ActionNote = "Job Refund Verify";
			}
			else {
				leadactivity.ActionNote = "Job Refund Not Verify";
			}
			leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
			if (AbpSession.TenantId != null)
			{
				leadactivity.TenantId = (int)AbpSession.TenantId;
			}
			await _leadactivityRepository.InsertAsync(leadactivity);
		}

	}
}