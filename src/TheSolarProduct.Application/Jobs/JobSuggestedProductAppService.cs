﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization;
using TheSolarProduct.DataVaults;
using TheSolarProduct.Dto;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.QuickStocks;
using TheSolarProduct.StockOrders;
using TheSolarProduct.StockTransfers.Dtos;

namespace TheSolarProduct.Jobs
{

    public class JobSuggestedProductAppService : TheSolarProductAppServiceBase, IJobSuggestedProductAppService
    {
        private readonly IRepository<JobSuggestedProduct> _jobSuggestedProductRepository;
        private readonly IRepository<JobSuggestedProductItemList> _jobSuggestedProductItemListRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<ProductItem, int> _lookup_productItemRepository;

        public JobSuggestedProductAppService(
            IRepository<JobSuggestedProduct> jobSuggestedProductRepository,
            IRepository<JobSuggestedProductItemList> jobSuggestedProductItemListRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
            IRepository<ProductItem, int> lookup_productItemRepository
            )
        {
            _jobSuggestedProductRepository = jobSuggestedProductRepository;
            _jobSuggestedProductItemListRepository = jobSuggestedProductItemListRepository;
            _dbcontextprovider = dbcontextprovider;
            _lookup_productItemRepository = lookup_productItemRepository;

        }
        public async Task CreateOrEdit(CreateOrEditJobSuggestedProductDto input)
        {


            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        protected virtual async Task Create(CreateOrEditJobSuggestedProductDto input)
        {

            var SuggestedProduct = ObjectMapper.Map<JobSuggestedProduct>(input);

            var SuggestedProductId = await _jobSuggestedProductRepository.InsertAndGetIdAsync(SuggestedProduct);



            foreach (var item in input.JobSuggestedProductItemDto)
            {
                item.SuggestedProductId = SuggestedProductId;
                var SuggestedProductItem = ObjectMapper.Map<JobSuggestedProductItemList>(item);

                await _jobSuggestedProductItemListRepository.InsertAsync(SuggestedProductItem);
            }
        }
        protected virtual async Task Update(CreateOrEditJobSuggestedProductDto input)
        {
            var SuggestedProduct = await _jobSuggestedProductRepository.FirstOrDefaultAsync((int)input.Id);

            var SuggestedProductItemIds = await _jobSuggestedProductItemListRepository.GetAll().Where(e => e.SuggestedProductId == SuggestedProduct.Id).ToListAsync();
            var CurrentSuggestedProductItemIds = input.JobSuggestedProductItemDto != null ? input.JobSuggestedProductItemDto.Select(e => e.Id).ToList() : new List<int?>();
            var listSuggestedProduct = SuggestedProductItemIds.Where(e => !CurrentSuggestedProductItemIds.Contains(e.Id));

            foreach (var item in listSuggestedProduct)
            {


                await _jobSuggestedProductItemListRepository.DeleteAsync(item);
            }
            foreach (var item in input.JobSuggestedProductItemDto)
            {
                item.SuggestedProductId = SuggestedProduct.Id;
                if ((item.Id != null ? item.Id : 0) == 0)
                {
                    var SuggestedProductItem = ObjectMapper.Map<JobSuggestedProductItemList>(item);
                    await _jobSuggestedProductItemListRepository.InsertAsync(SuggestedProductItem);

                }
                else
                {
                    var SuggestedProductItem = await _jobSuggestedProductItemListRepository.GetAsync((int)item.Id);
                    ObjectMapper.Map(item, SuggestedProductItem);
                    await _jobSuggestedProductItemListRepository.UpdateAsync(SuggestedProductItem);
                }

            }

            ObjectMapper.Map(input, SuggestedProduct);
        }

        public async Task<List<JobSuggestedProductDto>> GetAllByJobId(int jobId)
        {
            var jobSuggestedProducts = await _jobSuggestedProductRepository.GetAllListAsync(x => x.JobId == jobId);
            var result = new List<JobSuggestedProductDto>();

            foreach (var jobSuggestedProduct in jobSuggestedProducts)
            {
                var jobSuggestedProductDto = new JobSuggestedProductDto
                {
                    Id = jobSuggestedProduct.Id,
                    JobId = jobSuggestedProduct.JobId,
                    Name = jobSuggestedProduct.Name,
                    Amount = jobSuggestedProduct.Amount,
                    SysCapacity = 0,
                    JobSuggestedProductItemDto = new List<JobSuggestedProductItemDto>()
                };

                
                decimal totalSysCapacity = 0;

                var relatedItems = await _jobSuggestedProductItemListRepository.GetAllListAsync(x => x.SuggestedProductId == jobSuggestedProduct.Id);

                foreach (var item in relatedItems)
                {
                    var _lookupProductItem = await _lookup_productItemRepository.FirstOrDefaultAsync((int)item.ProductItemId);

                    
                    decimal itemSysCapacity = 0;
                    if (item.Quantity.HasValue && _lookupProductItem?.Size.HasValue == true && _lookupProductItem.ProductTypeId == 1)
                    {
                        itemSysCapacity = (item.Quantity.Value * _lookupProductItem.Size.Value) / 1000;
                    }

                   
                    totalSysCapacity += itemSysCapacity;

                    var jobSuggestedProductItemDto = new JobSuggestedProductItemDto
                    {
                        Id = item.Id,
                        ProductItemId = item.ProductItemId,
                        Quantity = item.Quantity,
                        ProductItem = _lookupProductItem?.Name?.ToString(),
                        ProductTypeId = _lookupProductItem?.ProductTypeId,
                        ModelName = _lookupProductItem?.Model?.ToString(),
                        Size = _lookupProductItem?.Size
                    };

                    jobSuggestedProductDto.JobSuggestedProductItemDto.Add(jobSuggestedProductItemDto);
                }

                
                jobSuggestedProductDto.SysCapacity = totalSysCapacity;

                result.Add(jobSuggestedProductDto);
            }

            return result;
        }


        public async Task<JobSuggestedProductDto> GetJobSuggestedForEdit(int id)
        {
           
             var jobSuggestedProduct = await _jobSuggestedProductRepository.FirstOrDefaultAsync(x => x.Id == id);


            var jobSuggestedProductDto = new JobSuggestedProductDto
            {
                Id = jobSuggestedProduct.Id,
                JobId = jobSuggestedProduct.JobId,
                Name = jobSuggestedProduct.Name,
                Amount = jobSuggestedProduct.Amount,
                JobSuggestedProductItemDto = new List<JobSuggestedProductItemDto>()
            };

           
            var relatedItems = await _jobSuggestedProductItemListRepository.GetAllListAsync(x => x.SuggestedProductId == jobSuggestedProduct.Id);

            foreach (var item in relatedItems)
            {
                var _lookupProductItem = await _lookup_productItemRepository.FirstOrDefaultAsync((int)item.ProductItemId);

                var jobSuggestedProductItemDto = new JobSuggestedProductItemDto
                {
                    Id = item.Id,
                    ProductItemId = item.ProductItemId,
                    Quantity = item.Quantity,
                    ProductItem = _lookupProductItem?.Name?.ToString(),
                    ProductTypeId = _lookupProductItem?.ProductTypeId,
                    ModelName = _lookupProductItem?.Model?.ToString(),
                    Size = _lookupProductItem?.Size,
                };

                jobSuggestedProductDto.JobSuggestedProductItemDto.Add(jobSuggestedProductItemDto);
            }

            return jobSuggestedProductDto;
        }
        public async Task Delete(EntityDto input)
        {
           
            await _jobSuggestedProductRepository.DeleteAsync(input.Id);
        }
    }


}

