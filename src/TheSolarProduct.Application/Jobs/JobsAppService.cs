﻿using TheSolarProduct.Jobs;
using System.Collections.Generic;
using TheSolarProduct.Leads;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using Abp.Linq.Extensions;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Installer;
using Abp.AutoMapper;
using TheSolarProduct.Invoices;
using TheSolarProduct.MultiTenancy;
using Abp.Organizations;
using TheSolarProduct.Installer.Dtos;
using GetAllForLookupTableInput = TheSolarProduct.Jobs.Dtos.GetAllForLookupTableInput;
using TheSolarProduct.HoldReasons;
using TheSolarProduct.PVDStatuses;
using TheSolarProduct.Organizations;
using Abp.Notifications;
using TheSolarProduct.Notifications;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Quotations.Dtos;
using TheSolarProduct.PickList;
using TheSolarProduct.Quotations;
using TheSolarProduct.Leads.Exporting;
using TheSolarProduct.LeadSources;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.SmsTemplates;
using TheSolarProduct.EmailTemplates;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.ApplicationSettings;
using System.Net.Mail;
using Abp.Net.Mail;
using TheSolarProduct.PreviousJobStatuses;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.SmsTemplates.Dtos;
using TheSolarProduct.EmailTemplates.Dtos;
using Abp.Runtime.Security;
using TheSolarProduct.Promotions;
using Abp.UI;
using TheSolarProduct.Authorization.Roles;
using Abp.Authorization.Users;
//using System.Globalization;
using Abp.Timing.Timezone;
using Abp;
using TheSolarProduct.LeadStatuses;
using System.IO;
using TheSolarProduct.Storage;
using Microsoft.AspNetCore.Hosting;
using TheSolarProduct.JobHistory;
using TheSolarProduct.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using TheSolarProduct.StreetTypes;
using TheSolarProduct.States;
using RestSharp;
//using RestSharp.Authenticators;
using TheSolarProduct.Services;
using TheSolarProduct.ServiceCategoryDocs;
using TheSolarProduct.Common;
using TheSolarProduct.Timing;
//using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
//using TheSolarProduct.PickList.Dtos;
//using System.Collections;
using TheSolarProduct.Common.Dto;
//using Abp.Domain.Entities;
using Abp.Domain.Uow;
using System.Transactions;
//using Microsoft.AspNetCore.Mvc;
//using NUglify.JavaScript.Syntax;
using TheSolarProduct.UnitTypes;
using Microsoft.AspNetCore.Http;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Microsoft.Extensions.Hosting.Internal;
using Google.Apis.Sheets.v4.Data;
using NPOI.SS.UserModel;
using TheSolarProduct.InstallationCost.StateWiseInstallationCosts;
using TheSolarProduct.InstallationCost.InstallationItemLists;
using TheSolarProduct.InstallationCost.FixedCostPrices;
using TheSolarProduct.InstallationCost.ExtraInstallationCharges;
using TheSolarProduct.InstallationCost.STCCost;
using TheSolarProduct.InstallationCost.BatteryInstallationCost;
using TheSolarProduct.InstallationCost.TravelCost;
using Hangfire.Common;
using Microsoft.Azure.KeyVault.Models;
using IdentityModel.Client;
using TheSolarProduct.Jobs.Dtos.Pylon;
using Stripe;
using TheSolarProduct.CategoryInstallations;
using TheSolarProduct.InstallationCost.PostCodeCost;
using TheSolarProduct.JobTrackers.Dtos;
using TheSolarProduct.DataVaults;
using NPOI.SS.Formula.Functions;
using TheSolarProduct.Features;
using Org.BouncyCastle.Utilities.Encoders;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
//using Telerik.Reporting;
using TheSolarProduct.LeadHistory;
using TheSolarProduct.ThirdPartyApis.GreenDeals;
using TheSolarProduct.UserActivityLogs.Dtos;
using TheSolarProduct.UserActivityLogs;
using TheSolarProduct.CheckActives;
using TheSolarProduct.CheckApplications;
using TheSolarProduct.CheckDepositReceived;
//using System.Windows.Forms;
//using Hangfire.Common;


namespace TheSolarProduct.Jobs
{
    //[AbpAuthorize(AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    [AbpAuthorize]
    public class JobsAppService : TheSolarProductAppServiceBase, IJobsAppService
    {

        private const int MaxFileBytes = 5242880; //5MB
        private readonly IRepository<Job> _jobRepository;
        private readonly IJobsExcelExporter _jobsExcelExporter;
        private readonly IRepository<JobType, int> _lookup_jobTypeRepository;
        private readonly IRepository<JobStatus, int> _lookup_jobStatusRepository;
        private readonly IRepository<RoofType, int> _lookup_roofTypeRepository;
        private readonly IRepository<RoofAngle, int> _lookup_roofAngleRepository;
        private readonly IRepository<ElecDistributor, int> _lookup_elecDistributorRepository;
        private readonly IRepository<Lead, int> _lookup_leadRepository;
        private readonly IRepository<ElecRetailer, int> _lookup_elecRetailerRepository;
        private readonly IRepository<PaymentOption, int> _lookup_paymentOptionRepository;
        private readonly IRepository<DepositOption, int> _lookup_depositOptionRepository;
        private readonly IRepository<MeterUpgrade, int> _lookup_meterUpgradeRepository;
        private readonly IRepository<MeterPhase, int> _lookup_meterPhaseRepository;
        private readonly IRepository<PromotionOffer, int> _lookup_promotionOfferRepository;
        private readonly IRepository<HouseType, int> _lookup_houseTypeRepository;
        private readonly IRepository<FinanceOption, int> _lookup_financeOptionRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<JobPromotion> _jobPromotionRepository;
        private readonly IRepository<JobVariation> _jobVariationRepository;
        private readonly IRepository<Variation> _variationRepository;
        private readonly IRepository<STCZoneRating> _stcZoneRatingRepository;
        private readonly IRepository<STCPostalCode> _stcPostalCodeRepository;
        private readonly IRepository<STCYearWiseRate> _stcYearWiseRateRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<InstallerDetail> _installerDetailRepository;
        private readonly IRepository<InstallerAvailability> _installerAvailabilityRepository;
        private readonly IRepository<JobCancellationReason> _jobCancellationReasonReasonRepository;
        private readonly IRepository<HoldReason> _jobHoldReasonRepository;
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
        private readonly IRepository<Quotation> _quotationRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendOrganizationUnitRepository;
        private readonly IRepository<Warehouselocation> _warehouselocationRepository;
        private readonly IRepository<PVDStatus, int> _pVDStatusRepository;
        private readonly UserManager _userManager;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<ProductItem> _productItemRepository;
        private readonly IRepository<Document> _documentRepository;
        private readonly IRepository<EmailTempData> _EmailTempDataRepository;
        private readonly IRepository<JobRefund> _jobRefundRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<DocumentType, int> _lookup_documentTypeRepository;
        private readonly ILeadsExcelExporter _leadsExcelExporter;
        private readonly IRepository<LeadSource, int> _lookup_leadSourceRepository;
        private readonly IRepository<FreebieTransport> _freebieTransportRepository;
        private readonly IRepository<SmsTemplate> _smsTemplateRepository;
        private readonly IRepository<EmailTemplate> _emailTemplateRepository;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<PreviousJobStatus> _previousJobStatusRepository;
        private readonly IRepository<FinanceOption> _financeOptionRepository;
        private readonly IRepository<DepositOption> _depositOptionRepository;
        private readonly IRepository<PaymentOption> _paymentOptionRepository;
        private readonly IRepository<PromotionUser> _PromotionUsersRepository;
        private readonly IRepository<Promotion, int> _lookup_promotionRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<ProductItemLocation> _productiteamlocationRepository;
        private readonly IRepository<InstallerInvoice> _installerInvoiceRepository;
        private readonly IRepository<InstInvoiceHistory> _InstInvoiceHistoryRepository;
        private readonly IRepository<InstallerInvoice> _jobInstallerInvoiceRepository;
        private readonly IRepository<LeadStatus, int> _lookup_leadStatusRepository;
        private readonly IRepository<InvoicePayment> _lookup_InvoicepaymentRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IRepository<JobApprovalFileHistory> _JobApprovalFileHistoryRepository;
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<InvoiceImportData> _InvoiceImportDataRepository;
        private readonly IRepository<Jobwarranty> _JobwarrantyRepository;
        private readonly IRepository<JobOldSystemDetail> _JobOldSysDetail;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<StreetType> _StreetTypeRepositery;
        private readonly IRepository<STCYearWiseRate> _STCYearWiseRatesRepositery;
        private readonly IRepository<InstallerDetail, int> _installerDetail;
        private readonly IRepository<State> _StateRepositery;
        private readonly IRepository<HouseType> _HouseTypeRepositery;
        private readonly IRepository<ElecDistributor> _ElecDistributorRepositery;
        private readonly IRepository<ElecRetailer> _ElecRetailerRepositery;
        private readonly IRepository<ProductType> _productTypeRepository;
        private readonly IRepository<PromotionMaster, int> _promotionMasterRepository;
        private readonly IRepository<PayWayData> _PayWayDataRepository;
        private readonly IRepository<Service> _serviceRepository;
        private readonly IRepository<ServiceCategoryDoc> _ServiceCAtegoryDocRepository;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Checkdeposite> _checkdepositeRepository;
        private readonly IRepository<CheckActive> _checkActiveRepository;
        private readonly IRepository<CheckApplication> _checkApplicationRepository;
        private readonly IRepository<UnitType> _unitTypesRepository;

        private const string pathfordoc = ApplicationSettingConsts.DocumentPath;
        private const string viewDocPath = ApplicationSettingConsts.ViewDocumentPath;

        static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
        //static readonly string ApplicationName = "AriseSolar WebDownloadData";
        //static readonly string SpreadsheetId = "19NxOhA3Z1srODefkdzVbb0NJr2bHwJ8roS7rrmk3bMM";
        static SheetsService service;

        private readonly IRepository<StateWiseInstallationCost> _stateWiseInstallationCostRepository;
        //private readonly IRepository<InstallationItemPeriod> _installationItemPeriodRepository;
        private readonly IRepository<InstallationItemList> _installationItemListRepository;
        private readonly IRepository<FixedCostPrice> _fixedCostPriceRepository;
        private readonly IRepository<ExtraInstallationCharge> _extraInstallationChargeRepository;
        private readonly IRepository<STCCost> _stcCostRepository;
        private readonly IRepository<BatteryInstallationCost> _batteryInstallationCostRepository;
        private readonly IRepository<TravelCost> _travelCostRepository;

        private readonly IRepository<OrganizationUnitMap> _organizationUnitMapRepository;
        private readonly IRepository<PylonDocument> _pylonDocumentRepository;
        private readonly IRepository<SolarRebateStatus> _solarRebateStatusRepository;
        private readonly IRepository<CategoryInstallationItemList> _categoryInstallationItemListRepository;
        private readonly IRepository<PostoCodePrice> _postCodePriceRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IRepository<STCProvider> _stcProviderRepository;
        private readonly IRepository<QLDBatteryRebate> _qldBatteryRebateRepository;
        private readonly IRepository<Quotations.JobAcknowledgement> _jobAcknowledgementRepqository;
        private readonly IRepository<Quotations.DeclarationForm> _declarationFormRepqository;
        private readonly IUserActivityLogAppService _userActivityLogServiceProxy;
        private readonly PermissionChecker _permissionChecker;

        public JobsAppService(IRepository<Job> jobRepository
            , IJobsExcelExporter jobsExcelExporter
            , IRepository<JobType, int> lookup_jobTypeRepository
            , IRepository<JobStatus, int> lookup_jobStatusRepository
            , IRepository<RoofType, int> lookup_roofTypeRepository
            , IRepository<RoofAngle, int> lookup_roofAngleRepository
            , IRepository<ElecDistributor, int> lookup_elecDistributorRepository
            , IRepository<Lead, int> lookup_leadRepository
            , IRepository<ElecRetailer, int> lookup_elecRetailerRepository
            , IRepository<PaymentOption, int> lookup_paymentOptionRepository
            , IRepository<DepositOption, int> lookup_depositOptionRepository
            , IRepository<MeterUpgrade, int> lookup_meterUpgradeRepository
            , IRepository<MeterPhase, int> lookup_meterPhaseRepository
            , IRepository<PromotionOffer, int> lookup_promotionOfferRepository
            , IRepository<HouseType, int> lookup_houseTypeRepository
            , IRepository<FinanceOption, int> lookup_financeOptionRepository
            , IRepository<JobProductItem> jobProductItemRepository
            , IRepository<JobPromotion> jobPromotionRepository
            , IRepository<JobVariation> jobVariationRepository
            , IRepository<STCZoneRating> stcZoneRatingRepository
            , IRepository<STCPostalCode> stcPostalCodeRepository
            , IRepository<STCYearWiseRate> stcYearWiseRateRepository
            , IRepository<Variation> variationRepository
            , IRepository<User, long> userRepository
            , IRepository<LeadActivityLog> leadactivityRepository
            , IRepository<InstallerDetail> installerDetailRepository
            , IRepository<InstallerAvailability> installerAvailabilityRepository
            , IRepository<JobCancellationReason> jobCancellationReasonReasonRepository
            , IRepository<HoldReason> jobHoldReasonRepository
            , IRepository<InvoicePayment> invoicePaymentRepository
            , IRepository<Quotation> quotationRepository
            , IRepository<Tenant> tenantRepository
            , IRepository<OrganizationUnit, long> organizationUnitRepository
            , IRepository<ExtendOrganizationUnit, long> extendOrganizationUnitRepository
            , IRepository<Warehouselocation> warehouselocationRepository
            , IRepository<PVDStatus, int> PVDStatusRepository
            , UserManager userManager
            , IAppNotifier appNotifier
            , IRepository<ProductItem> productItemRepository
            , IRepository<Document> documentRepository
            , IRepository<JobRefund> jobRefundRepository
            , IRepository<UserTeam> userTeamRepository
            , IRepository<DocumentType, int> lookup_documentTypeRepository
            , ILeadsExcelExporter leadsExcelExporter
            , IRepository<LeadSource, int> lookup_leadSourceRepository
            , IRepository<FreebieTransport> freebieTransportRepository
            , IRepository<SmsTemplate> smsTemplateRepository
            , IRepository<EmailTemplate> emailTemplateRepository
            , IApplicationSettingsAppService applicationSettings
            , IEmailSender emailSender
            , IRepository<PreviousJobStatus> previousJobStatusRepository
            , IRepository<FinanceOption> financeOptionRepository
            , IRepository<DepositOption> depositOptionRepository
            , IRepository<PaymentOption> paymentOptionRepository
            , IRepository<PromotionUser> PromotionUsersRepository
            , IRepository<Promotion, int> lookup_promotionRepository
            , IRepository<Role> roleRepository
            , IRepository<UserRole, long> userRoleRepository
            , ITimeZoneConverter timeZoneConverter
            , IRepository<ProductItemLocation> productiteamlocationRepository
            , IRepository<InstallerInvoice> installerInvoiceRepository
            , IRepository<InstallerInvoice> jobInstallerInvoiceRepository
            , IRepository<LeadStatus, int> lookup_leadStatusRepository
            , IRepository<InvoicePayment> lookup_InvoicepaymentRepository
            , IRepository<InstInvoiceHistory> InstInvoiceHistoryRepository
            , ITempFileCacheManager tempFileCacheManager
            , IWebHostEnvironment env, IRepository<JobApprovalFileHistory> JobApprovalFileHistoryRepository
            , IRepository<Team> teamRepository
            , IRepository<InvoiceImportData> InvoiceImportDataRepository
            , IRepository<Jobwarranty> JobwarrantyRepository
            , IRepository<EmailTempData> EmailTempDataRepository
            , IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            , IRepository<JobOldSystemDetail> JobOldSysDetail
            , IRepository<StreetType> StreetTypeRepositery
            , IRepository<STCYearWiseRate> STCYearWiseRatesRepositery
            , IRepository<InstallerDetail, int> installerDetail
            , IRepository<State> StateRepositery
            , IRepository<HouseType> HouseTypeRepositery
            , IRepository<ElecDistributor> ElecDistributorRepositery
            , IRepository<ElecRetailer> ElecRetailerRepositery
            , IRepository<Document> DocumentRepository
            , IRepository<ProductType> productTypeRepository
            , IRepository<PromotionMaster, int> promotionMasterRepository
            , IRepository<PayWayData> PayWayDataRepository
            , IRepository<Service> serviceRepository
            , IRepository<ServiceCategoryDoc> ServiceCAtegoryDocRepository
            , ICommonLookupAppService CommonDocumentSaveRepository
            , ITimeZoneService timeZoneService
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<UnitType> unitTypesRepository
            , IRepository<StateWiseInstallationCost> stateWiseInstallationCostRepository
            , IRepository<InstallationItemList> installationItemListRepository
            , IRepository<FixedCostPrice> fixedCostPriceRepository
            , IRepository<ExtraInstallationCharge> extraInstallationChargeRepository
            , IRepository<STCCost> stcCostRepository
            , IRepository<BatteryInstallationCost> batteryInstallationCostRepository
            , IRepository<TravelCost> travelCostRepository
            , IRepository<OrganizationUnitMap> organizationUnitMapRepository
            , IRepository<PylonDocument> pylonDocumentRepository
            , IRepository<SolarRebateStatus> solarRebateStatusRepository
            , IRepository<CategoryInstallationItemList> categoryInstallationItemListRepository
            , IRepository<PostoCodePrice> postCodePriceRepository
            , IRepository<DataVaultActivityLog> dataVaultActivityLogRepository
            , IRepository<STCProvider> stcProviderRepository
            , IRepository<QLDBatteryRebate> qldBatteryRebateRepository
            , IRepository<Quotations.JobAcknowledgement> jobAcknowledgementRepqository
            , IRepository<Quotations.DeclarationForm> declarationFormRepqository
            , IUserActivityLogAppService userActivityLogServiceProxy
            , IRepository<Checkdeposite> checkdepositeRepository
            , IRepository<CheckActive> checkActiveRepository
            , IRepository<CheckApplication> checkApplicationRepository
            , PermissionChecker permissionChecker
            )
        {
            _env = env;
            _tempFileCacheManager = tempFileCacheManager;
            _jobRepository = jobRepository;
            _jobsExcelExporter = jobsExcelExporter;
            _lookup_jobTypeRepository = lookup_jobTypeRepository;
            _lookup_jobStatusRepository = lookup_jobStatusRepository;
            _lookup_roofTypeRepository = lookup_roofTypeRepository;
            _lookup_roofAngleRepository = lookup_roofAngleRepository;
            _lookup_elecDistributorRepository = lookup_elecDistributorRepository;
            _lookup_leadRepository = lookup_leadRepository;
            _lookup_elecRetailerRepository = lookup_elecRetailerRepository;
            _lookup_paymentOptionRepository = lookup_paymentOptionRepository;
            _lookup_depositOptionRepository = lookup_depositOptionRepository;
            _lookup_meterUpgradeRepository = lookup_meterUpgradeRepository;
            _lookup_meterPhaseRepository = lookup_meterPhaseRepository;
            _lookup_promotionOfferRepository = lookup_promotionOfferRepository;
            _lookup_houseTypeRepository = lookup_houseTypeRepository;
            _lookup_financeOptionRepository = lookup_financeOptionRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _jobPromotionRepository = jobPromotionRepository;
            _jobVariationRepository = jobVariationRepository;
            _stcZoneRatingRepository = stcZoneRatingRepository;
            _stcPostalCodeRepository = stcPostalCodeRepository;
            _stcYearWiseRateRepository = stcYearWiseRateRepository;
            _variationRepository = variationRepository;
            _userRepository = userRepository;
            _leadactivityRepository = leadactivityRepository;
            _installerDetailRepository = installerDetailRepository;
            _installerAvailabilityRepository = installerAvailabilityRepository;
            _jobCancellationReasonReasonRepository = jobCancellationReasonReasonRepository;
            _jobHoldReasonRepository = jobHoldReasonRepository;
            _invoicePaymentRepository = invoicePaymentRepository;
            _quotationRepository = quotationRepository;
            _tenantRepository = tenantRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _extendOrganizationUnitRepository = extendOrganizationUnitRepository;
            _warehouselocationRepository = warehouselocationRepository;
            _pVDStatusRepository = PVDStatusRepository;
            _userManager = userManager;
            _appNotifier = appNotifier;
            _productItemRepository = productItemRepository;
            _documentRepository = documentRepository;
            _jobRefundRepository = jobRefundRepository;
            _userTeamRepository = userTeamRepository;
            _lookup_documentTypeRepository = lookup_documentTypeRepository;
            _leadsExcelExporter = leadsExcelExporter;
            _lookup_leadSourceRepository = lookup_leadSourceRepository;
            _freebieTransportRepository = freebieTransportRepository;
            _smsTemplateRepository = smsTemplateRepository;
            _emailTemplateRepository = emailTemplateRepository;
            _applicationSettings = applicationSettings;
            _emailSender = emailSender;
            _previousJobStatusRepository = previousJobStatusRepository;
            _financeOptionRepository = financeOptionRepository;
            _depositOptionRepository = depositOptionRepository;
            _paymentOptionRepository = paymentOptionRepository;
            _PromotionUsersRepository = PromotionUsersRepository;
            _lookup_promotionRepository = lookup_promotionRepository;
            _roleRepository = roleRepository;
            _userRoleRepository = userRoleRepository;
            _timeZoneConverter = timeZoneConverter;
            _productiteamlocationRepository = productiteamlocationRepository;
            _installerInvoiceRepository = installerInvoiceRepository;
            _jobInstallerInvoiceRepository = jobInstallerInvoiceRepository;
            _lookup_leadStatusRepository = lookup_leadStatusRepository;
            _lookup_InvoicepaymentRepository = lookup_InvoicepaymentRepository;
            _InstInvoiceHistoryRepository = InstInvoiceHistoryRepository;
            _JobApprovalFileHistoryRepository = JobApprovalFileHistoryRepository;
            _teamRepository = teamRepository;
            _InvoiceImportDataRepository = InvoiceImportDataRepository;
            _JobwarrantyRepository = JobwarrantyRepository;
            _EmailTempDataRepository = EmailTempDataRepository;
            _dbcontextprovider = dbcontextprovider;
            _JobOldSysDetail = JobOldSysDetail;
            _StreetTypeRepositery = StreetTypeRepositery;
            _STCYearWiseRatesRepositery = STCYearWiseRatesRepositery;
            _installerDetail = installerDetail;
            _StateRepositery = StateRepositery;
            _HouseTypeRepositery = HouseTypeRepositery;
            _ElecDistributorRepositery = ElecDistributorRepositery;
            _ElecRetailerRepositery = ElecRetailerRepositery;
            _productTypeRepository = productTypeRepository;
            _promotionMasterRepository = promotionMasterRepository;
            _PayWayDataRepository = PayWayDataRepository;
            _serviceRepository = serviceRepository;
            _ServiceCAtegoryDocRepository = ServiceCAtegoryDocRepository;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _timeZoneService = timeZoneService;
            _unitOfWorkManager = unitOfWorkManager;
            _unitTypesRepository = unitTypesRepository;
            _stateWiseInstallationCostRepository = stateWiseInstallationCostRepository;
            _installationItemListRepository = installationItemListRepository;
            _fixedCostPriceRepository = fixedCostPriceRepository;
            _extraInstallationChargeRepository = extraInstallationChargeRepository;
            _stcCostRepository = stcCostRepository;
            _batteryInstallationCostRepository = batteryInstallationCostRepository;
            _travelCostRepository = travelCostRepository;
            _organizationUnitMapRepository = organizationUnitMapRepository;
            _pylonDocumentRepository = pylonDocumentRepository;
            _solarRebateStatusRepository = solarRebateStatusRepository;
            _categoryInstallationItemListRepository = categoryInstallationItemListRepository;
            _postCodePriceRepository = postCodePriceRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _stcProviderRepository = stcProviderRepository;
            _qldBatteryRebateRepository = qldBatteryRebateRepository;
            _jobAcknowledgementRepqository = jobAcknowledgementRepqository;
            _declarationFormRepqository = declarationFormRepqository;
            _userActivityLogServiceProxy = userActivityLogServiceProxy;
            _checkdepositeRepository = checkdepositeRepository;
            _checkActiveRepository = checkActiveRepository;
            _checkApplicationRepository = checkApplicationRepository;
            _permissionChecker = permissionChecker;
        }

        //Application Tracker
        //public async Task<PagedResultDto<GetJobForViewDto>> GetAll(GetAllJobsInput input)
        //{
        //    var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
        //    var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

        //    var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
        //    var actvitylog_list = _leadactivityRepository.GetAll().Where(e => e.SectionId == 1 && e.LeadFk.OrganizationId == input.OrganizationUnit);
        //    IList<string> role = await _userManager.GetRolesAsync(User);
        //    var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
        //    var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
        //    var Doclist = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 2).Select(e => e.JobId).ToList();
        //    //var holdnddeporcvdate = _jobRepository.GetAll().Where(e=>e.DepositeRecceivedDate != null && e.JobStatusId == 2).Select(e=>e.Id).ToList();
        //    var filteredJobs = _jobRepository.GetAll()
        //                       .Include(e => e.LeadFk)
        //                      .WhereIf(!string.IsNullOrWhiteSpace(input.projectFilter), e => false || e.JobNumber.Contains(input.projectFilter) || e.LeadFk.Mobile.Contains(input.projectFilter) || e.LeadFk.Email.Contains(input.projectFilter) || e.LeadFk.CompanyName.Contains(input.projectFilter))
        //                      .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.State == input.stateNameFilter)
        //                      .WhereIf(input.elecDistributorId != 0, e => e.ElecDistributorId == input.elecDistributorId)
        //                      .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
        //                      //.WhereIf(input.testfilter != null && input.testfilter.Count() > 0, e => input.testfilter.Contains((int)e.JobStatusId))
        //                      .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
        //                      .WhereIf(input.FinanceOptionNameFilter == "Application" && SDate != null && EDate != null, e => e.DistApplied.Value.AddHours(10).Date >= SDate.Value.Date && e.DistApplied.Value.AddHours(10).Date <= EDate.Value.Date)
        //                      .WhereIf(input.FinanceOptionNameFilter == "Exp" && SDate != null && EDate != null, e => e.DistExpiryDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DistExpiryDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                      .WhereIf(input.FinanceOptionNameFilter == "Dep" && SDate != null && EDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                      .WhereIf(input.FinanceOptionNameFilter == "Active" && SDate != null && EDate != null, e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                      .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
        //                      .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
        //                      .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
        //                      .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
        //                      .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
        //                      .WhereIf(input.applicationstatus == 1, e => e.ApplicationRefNo != null)
        //                      .WhereIf(input.applicationstatus == 2, e => e.ApplicationRefNo == null)
        //                      .WhereIf(input.applicationstatus == 3, e => e.DistExpiryDate >= DateTime.UtcNow && e.DistExpiryDate <= DateTime.UtcNow.AddDays(30) && (e.JobStatusId >= 4 && e.JobStatusId <= 7) && e.MeterPhaseId != 0 && e.MeterPhaseId != null && e.PeakMeterNo != null && e.NMINumber != null && Doclist.Contains(e.Id) && e.ApplicationRefNo != null)

        //                      .WhereIf(input.applicationstatus == 4, e => e.JobStatusId != 3 && e.JobStatusId != 1 && (e.MeterPhaseId == 0 || e.MeterPhaseId == null || e.PeakMeterNo == null || string.IsNullOrEmpty(e.NMINumber) || !Doclist.Contains(e.Id)))
        //                      .WhereIf(input.applicationstatus != 4, e => e.JobStatusId != 3 && e.JobStatusId != 1 && e.MeterPhaseId != 0 && e.MeterPhaseId != null && e.PeakMeterNo != null && !string.IsNullOrEmpty(e.NMINumber) && Doclist.Contains(e.Id))
        //                      .WhereIf(input.applicationstatus == 4, e => string.IsNullOrEmpty(e.ApplicationRefNo))

        //                      .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);

        //    var pagedAndFilteredJobs = filteredJobs
        //        .OrderBy(input.Sorting ?? "id desc")
        //        .PageBy(input);

        //    var jobs = from o in pagedAndFilteredJobs
        //               join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
        //               from s1 in j1.DefaultIfEmpty()

        //               join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
        //               from s2 in j2.DefaultIfEmpty()

        //                   //join o3 in _lookup_roofTypeRepository.GetAll() on o.RoofTypeId equals o3.Id into j3
        //                   //from s3 in j3.DefaultIfEmpty()

        //                   //join o4 in _lookup_roofAngleRepository.GetAll() on o.RoofAngleId equals o4.Id into j4
        //                   //from s4 in j4.DefaultIfEmpty()

        //               join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
        //               from s5 in j5.DefaultIfEmpty()

        //               join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
        //               from s6 in j6.DefaultIfEmpty()

        //               join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
        //               from s7 in j7.DefaultIfEmpty()

        //                   //join o8 in _lookup_paymentOptionRepository.GetAll() on o.PaymentOptionId equals o8.Id into j8
        //                   //from s8 in j8.DefaultIfEmpty()

        //                   //join o9 in _lookup_depositOptionRepository.GetAll() on o.DepositOptionId equals o9.Id into j9
        //                   //from s9 in j9.DefaultIfEmpty()

        //                   //join o10 in _lookup_meterUpgradeRepository.GetAll() on o.MeterUpgradeId equals o10.Id into j10
        //                   //from s10 in j10.DefaultIfEmpty()

        //                   //join o11 in _lookup_meterPhaseRepository.GetAll() on o.MeterPhaseId equals o11.Id into j11
        //                   //from s11 in j11.DefaultIfEmpty()

        //                   //join o12 in _lookup_promotionOfferRepository.GetAll() on o.PromotionOfferId equals o12.Id into j12
        //                   //from s12 in j12.DefaultIfEmpty()

        //                   //join o13 in _lookup_houseTypeRepository.GetAll() on o.HouseTypeId equals o13.Id into j13
        //                   //from s13 in j13.DefaultIfEmpty()

        //                   //join o14 in _lookup_financeOptionRepository.GetAll() on o.FinanceOptionId equals o14.Id into j14
        //                   //from s14 in j14.DefaultIfEmpty()

        //               select new GetJobForViewDto()
        //               {
        //                   Job = new JobDto
        //                   {
        //                       RegPlanNo = o.RegPlanNo,
        //                       LotNumber = o.LotNumber,
        //                       Suburb = o.Suburb,
        //                       State = o.State,
        //                       UnitNo = o.UnitNo,
        //                       UnitType = o.UnitType,
        //                       NMINumber = o.NMINumber,
        //                       Id = o.Id,
        //                       ApplicationRefNo = o.ApplicationRefNo,
        //                       DistAppliedDate = o.DistAppliedDate,
        //                       ExpiryDate = o.ExpiryDate,
        //                       Notes = o.Note,
        //                       InstallerNotes = o.InstallerNotes,
        //                       JobNumber = o.JobNumber,
        //                       MeterNumber = o.PeakMeterNo,
        //                       LeadId = o.LeadId,
        //                       OldSystemDetails = o.OldSystemDetails,
        //                       PostalCode = o.PostalCode,
        //                       StreetName = o.StreetName,
        //                       StreetNo = o.StreetNo,
        //                       StreetType = o.StreetType,
        //                       ElecDistributorId = o.ElecDistributorId,
        //                       ElecRetailerId = o.ElecRetailerId,
        //                       JobStatusId = o.JobStatusId,
        //                       JobTypeId = o.JobTypeId,
        //                       Address = o.Address,
        //                       SmsSend = o.SmsSend,
        //                       SmsSendDate = o.SmsSendDate,
        //                       EmailSend = o.EmailSend,
        //                       EmailSendDate = o.EmailSendDate,
        //                       DistApplied = o.DistApplied,
        //                       DistApproveDate = o.DistApproveDate,
        //                       DistExpiryDate = o.DistExpiryDate,
        //                       CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault()
        //                   },
        //                   JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
        //                   JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
        //                   ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
        //                   LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
        //                   Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
        //                   Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
        //                   ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
        //                   MeterPhadeIDs = _lookup_meterPhaseRepository.GetAll().Where(e => e.Id == o.MeterPhaseId).Any(),
        //                   PeakMeterNos = !string.IsNullOrEmpty(o.PeakMeterNo),
        //                   Nminumbers = !string.IsNullOrEmpty(o.NMINumber),
        //                   ReminderTime = actvitylog_list.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
        //                   ActivityDescription = actvitylog_list.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
        //                   ActivityComment = actvitylog_list.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
        //               };

        //    var totalCount = await filteredJobs.CountAsync();

        //    return new PagedResultDto<GetJobForViewDto>(
        //        totalCount,
        //        await jobs.ToListAsync()
        //    );
        //}

        private int?[] getValuesFromNameValue(List<NameValue<int>> nameValues)
        {

            int?[] arr = new int?[nameValues.Count];


            if (nameValues != null)
            {
                int i = 0;

                foreach (var e in nameValues)
                {
                    arr[i] = e.Value;
                    i++;
                }
            }

            return arr;
        }

        //Application Tracker Excel Export
        public async Task<FileDto> getApplicationTrackerToExcel(GetAllJobApplicationTrackerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var Today = _timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var actvitylog_list = _leadactivityRepository.GetAll().Where(e => e.SectionId == 1 && e.LeadFk.OrganizationId == input.OrganizationUnit);
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            var Doclist = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 2).Select(e => e.JobId).ToList();
            var filteredResult = _jobRepository.GetAll()
                             .Include(e => e.LeadFk)
                             .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                             .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                             .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                             .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.JobStatusId != 3 && e.SolarRebateStatus != 3 && e.FirstDepositDate != null)
                             .AsNoTracking()
                             .Select(e => new { e.Id, e.JobNumber, e.LeadFk, e.State, e.ElecDistributorId, e.JobStatusId, e.JobTypeId, e.DepositeRecceivedDate, e.DistApplied, e.ActiveDate, e.ApplicationRefNo, e.MeterPhaseId, e.NMINumber, e.PeakMeterNo, e.DistExpiryDate, e.JobTypeFk, e.JobStatusFk, e.Address, e.Suburb, e.PostalCode, e.ElecDistributorFk, e.DistApproveDate, e.SmsSend, e.SmsSendDate, e.EmailSend, e.EmailSendDate, e.LeadId, e.FirstDepositDate });

            //var jobidstatus = input.testfilter != null ? input.testfilter : null;
            var filteredJobs = filteredResult
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(input.FilterName == "ApplicationRefNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ApplicationRefNo == input.Filter)
                        .WhereIf(input.FilterName == "NMINumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.NMINumber == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                        .WhereIf(input.ElecDistributorId != 0, e => e.ElecDistributorId == input.ElecDistributorId)
                        .WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => input.JobStatusIDFilter.Contains((int)e.JobStatusId))
                        .WhereIf(input.JobTypeId != 0, e => e.JobTypeId == input.JobTypeId)

                        .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ApplicationDate" && input.StartDate != null, e => e.DistApplied.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ApplicationDate" && input.EndDate != null, e => e.DistApplied.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ExpiryDate" && input.StartDate != null, e => e.DistApplied.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ExpiryDate" && input.EndDate != null, e => e.DistApplied.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ActiveDate" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ActiveDate" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.ApplicationStatus == 1, e => e.ApplicationRefNo == null
                        && (_documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() == false || e.MeterPhaseId == null || e.NMINumber == null || e.PeakMeterNo == null)) //Application Awaiting

                        .WhereIf(input.ApplicationStatus == 2, e => e.ApplicationRefNo == null
                        && _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() && e.MeterPhaseId != null && e.NMINumber != null && e.PeakMeterNo != null) //Application Pending

                        .WhereIf(input.ApplicationStatus == 3, e => e.ApplicationRefNo != null) //Application Complete

                        .WhereIf(input.ApplicationStatus == 4, e => e.ApplicationRefNo != null && (e.DistExpiryDate.Value.AddDays(-30) < Today) && e.JobStatusId < 6) //Application Expiry
                        ;

            var activity = _leadactivityRepository.GetAll().AsNoTracking().Where(e => e.SectionId == 1).Select(e => new { e.ActionId, e.LeadId, e.Id, e.ActivityDate, e.ActivityNote });
            var query = (from o in filteredJobs

                         join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                         from s5 in j5.DefaultIfEmpty()

                         join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                         from s6 in j6.DefaultIfEmpty()

                         //join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                         //from s7 in j7.DefaultIfEmpty()

                         select new GetJobForViewDto()
                         {
                             Job = new JobDto
                             {
                                 //RegPlanNo = o.RegPlanNo,
                                 //LotNumber = o.LotNumber,
                                 Suburb = o.Suburb,
                                 State = o.State,
                                // UnitNo = o.UnitNo,
                                 //UnitType = o.UnitType,
                                 NMINumber = o.NMINumber,
                                 Id = o.Id,
                                 ApplicationRefNo = o.ApplicationRefNo,
                                 DistAppliedDate = o.DistApplied,
                                 //Notes = o.Note,
                                 //InstallerNotes = o.InstallerNotes,
                                 JobNumber = o.JobNumber,
                                 MeterNumber = o.PeakMeterNo,
                                 LeadId = o.LeadId,
                                 //OldSystemDetails = o.OldSystemDetails,
                                 PostalCode = o.PostalCode,
                                 //StreetName = o.StreetName,
                                // StreetNo = o.StreetNo,
                                 //StreetType = o.StreetType,
                                 ElecDistributorId = o.ElecDistributorId,
                                 //ElecRetailerId = o.ElecRetailerId,
                                 JobStatusId = o.JobStatusId,
                                 JobTypeId = o.JobTypeId,
                                 Address = o.Address,
                                 CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                                 SmsSend = o.SmsSend,
                                 EmailSend = o.EmailSend,
                                 DistApproveDate = o.DistApproveDate,
                                 DistExpiryDate = o.DistExpiryDate,
                                 NextFollowup = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy")).FirstOrDefault(),
                                 Discription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                 Comment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                             },
                             JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                             JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                             ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                             LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                             NotAppliedDays = o.FirstDepositDate != null && o.DistApplied == null ? (int)(Today.Value.Date - o.FirstDepositDate.Value.Date).TotalDays : (int)0,
                             NotApprovedDays = o.DistApproveDate == null && o.DistApplied != null ? (int)(Today.Value.Date - o.DistApplied.Value.Date).TotalDays : (int)0,
                             ExpiredDays = o.DistExpiryDate != null ? (int)(o.DistExpiryDate.Value.Date - Today.Value.Date).TotalDays : (int)0
                             //ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
                         });

            var applicationtrackerListDtos = await query.ToListAsync();

            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _leadsExcelExporter.ApplicationTrackerExportToFile(applicationtrackerListDtos);
            }
            else
            {
                return _leadsExcelExporter.ApplicationTrackerCsvExport(applicationtrackerListDtos);
            }
        }

        //FinanceTracker
        public async Task<PagedResultDto<GetJobForViewDto>> GetAllForJobFinanceTracker(GetAllJobsInput input)
        {
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            var leadactivity_repositery = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.SectionId == 3);
            var Deposites = _invoicePaymentRepository.GetAll().Select(e => e.JobId).Distinct().ToList();

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobTypeFk)
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.LeadFk)
                        .Include(e => e.FinanceOptionFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.FinancePurchaseNo.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.LeadFk.Suburb == input.SuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.LeadFk.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.LeadFk.Mobile.Contains(input.Mobile))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Email), e => e.LeadFk.Email.Contains(input.Email))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
                        .WhereIf(input.paymentid != 0, e => e.PaymentOptionId == input.paymentid)
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.IsVerified == 3, e => e.FinanceDocumentVerified == 3)
                        .WhereIf(input.IsVerified == 2, e => e.FinanceDocumentVerified == 2 || e.FinanceDocumentVerified == null)
                        .WhereIf(input.IsVerified == 1, e => e.FinanceDocumentVerified == 1)
                        .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit)
                        .Where(e => e.PaymentOptionId != null && e.PaymentOptionId != 1)
                        .Where(e => Deposites.Contains(e.Id));

            //output.TotalfinanceDatanotVerifyData = _jobRepository.GetAll()
            //    .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            //    .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            //    .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            //    .Where(e => e.PaymentOptionId != null && e.PaymentOptionId != 1 && Deposites.Contains(e.Id) && e.LeadFk.OrganizationId == organizationid)
            //    .Where(e => e.FinanceDocumentVerified == 2 || e.FinanceDocumentVerified == null).Count();.

            var pagedAndFilteredJobs = filteredJobs
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var jobs = from o in pagedAndFilteredJobs
                       join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                       from s1 in j1.DefaultIfEmpty()

                       join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                       from s2 in j2.DefaultIfEmpty()

                           //join o3 in _lookup_roofTypeRepository.GetAll() on o.RoofTypeId equals o3.Id into j3
                           //from s3 in j3.DefaultIfEmpty()

                           //join o4 in _lookup_roofAngleRepository.GetAll() on o.RoofAngleId equals o4.Id into j4
                           //from s4 in j4.DefaultIfEmpty()

                       join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                       from s5 in j5.DefaultIfEmpty()

                       join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                       from s6 in j6.DefaultIfEmpty()

                       join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                       from s7 in j7.DefaultIfEmpty()

                           //join o8 in _lookup_paymentOptionRepository.GetAll() on o.PaymentOptionId equals o8.Id into j8
                           //from s8 in j8.DefaultIfEmpty()

                           //join o9 in _lookup_depositOptionRepository.GetAll() on o.DepositOptionId equals o9.Id into j9
                           //from s9 in j9.DefaultIfEmpty()

                           //join o10 in _lookup_meterUpgradeRepository.GetAll() on o.MeterUpgradeId equals o10.Id into j10
                           //from s10 in j10.DefaultIfEmpty()

                           //join o11 in _lookup_meterPhaseRepository.GetAll() on o.MeterPhaseId equals o11.Id into j11
                           //from s11 in j11.DefaultIfEmpty()

                           //join o12 in _lookup_promotionOfferRepository.GetAll() on o.PromotionOfferId equals o12.Id into j12
                           //from s12 in j12.DefaultIfEmpty()

                           //join o13 in _lookup_houseTypeRepository.GetAll() on o.HouseTypeId equals o13.Id into j13
                           //from s13 in j13.DefaultIfEmpty()

                           //join o14 in _lookup_financeOptionRepository.GetAll() on o.FinanceOptionId equals o14.Id into j14
                           //from s14 in j14.DefaultIfEmpty()

                       select new GetJobForViewDto()
                       {
                           Job = new JobDto
                           {
                               RegPlanNo = o.RegPlanNo,
                               LotNumber = o.LotNumber,
                               Address = o.Address,
                               Suburb = o.Suburb,
                               State = o.State,
                               UnitNo = o.UnitNo,
                               UnitType = o.UnitType,
                               NMINumber = o.NMINumber,
                               Id = o.Id,
                               ApplicationRefNo = o.ApplicationRefNo,
                               DistAppliedDate = o.DistAppliedDate,
                               ExpiryDate = o.ExpiryDate,
                               Notes = o.Note,
                               InstallerNotes = o.InstallerNotes,
                               JobNumber = o.JobNumber,
                               MeterNumber = o.PeakMeterNo,
                               OldSystemDetails = o.OldSystemDetails,
                               PostalCode = o.PostalCode,
                               StreetName = o.StreetName,
                               StreetNo = o.StreetNo,
                               StreetType = o.StreetType,
                               LeadId = o.LeadId,
                               ElecDistributorId = o.ElecDistributorId,
                               ElecRetailerId = o.ElecRetailerId,
                               JobStatusId = o.JobStatusId,
                               JobTypeId = o.JobTypeId,
                               FinancePurchaseNo = o.FinancePurchaseNo,
                               FinanceDocumentVerified = o.FinanceDocumentVerified,
                               FinanceSmsSend = o.FinanceSmsSend,
                               FinanceSmsSendDate = o.FinanceSmsSendDate,
                               FinanceEmailSend = o.FinanceEmailSend,
                               FinanceEmailSendDate = o.FinanceEmailSendDate,
                               CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault()
                           },
                           JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                           JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                           ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                           LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                           Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                           Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                           ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
                           ReminderTime = leadactivity_repositery.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                           ActivityDescription = leadactivity_repositery.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           ActivityComment = leadactivity_repositery.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                       };

            var totalCount = await filteredJobs.CountAsync();

            return new PagedResultDto<GetJobForViewDto>(
                totalCount,
                await jobs.ToListAsync()
            );
        }

        //Finance Tracker Excel Export
        public async Task<FileDto> getFinanaceTrackerToExcel(GetAllFinanceTrackerInput input)
        {

            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var filteredJobs = _jobRepository.GetAll()
            //            .Include(e => e.JobTypeFk)
            //            .Include(e => e.JobStatusFk)
            //            .Include(e => e.LeadFk)
            //            .Include(e => e.FinanceOptionFk)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.FinancePurchaseNo.Contains(input.Filter))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.LeadFk.Mobile.Contains(input.Mobile))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Email), e => e.LeadFk.Email.Contains(input.Email))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
            //            .WhereIf(input.IsVerified != null, e => e.FinanceDocumentVerified == input.IsVerified)
            //            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            //            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            //            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            //            .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
            //            .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
            //            .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit)
            //            .Where(e => e.PaymentOptionId != null && e.PaymentOptionId != 1);

            #region old code
            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            //var leadactivity_repositery = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.SectionId == 3);
            //var Deposites = _invoicePaymentRepository.GetAll().Select(e => e.JobId).Distinct().ToList();

            //var filteredJobs = _jobRepository.GetAll()
            //            .Include(e => e.JobTypeFk)
            //            .Include(e => e.JobStatusFk)
            //            .Include(e => e.LeadFk)
            //            .Include(e => e.FinanceOptionFk)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.FinancePurchaseNo.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.LeadFk.Suburb == input.SuburbFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.LeadFk.State == input.StateFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.LeadFk.Mobile.Contains(input.Mobile))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Email), e => e.LeadFk.Email.Contains(input.Email))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
            //            .WhereIf(input.paymentid != 0, e => e.PaymentOptionId == input.paymentid)
            //            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            //            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            //            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            //            .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
            //            .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
            //            .WhereIf(input.IsVerified == 3, e => e.FinanceDocumentVerified == 3)
            //            .WhereIf(input.IsVerified == 2, e => e.FinanceDocumentVerified == 2 || e.FinanceDocumentVerified == null)
            //            .WhereIf(input.IsVerified == 1, e => e.FinanceDocumentVerified == 1)
            //            .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit)
            //            .Where(e => e.PaymentOptionId != null && e.PaymentOptionId != 1)
            //            .Where(e => Deposites.Contains(e.Id));

            #endregion

            #region new code by Poonam 04-03-2024
            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });
            var Today = _timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId);

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var filteredResult = _jobRepository.GetAll()
                              .Include(e => e.LeadFk)
                              .Include(e => e.JobStatusFk)
                              .Include(e => e.FinanceOptionFk)
                              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                              .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.PaymentOptionId != 1 && e.FirstDepositDate != null)
                              .AsNoTracking()
                              .Select(e => new { e.Id, e.LeadFk, e.JobNumber, e.FinancePurchaseNo, e.PaymentOptionId, e.FinanceDocumentVerified, e.State, e.JobStatusFk, e.Address, e.Suburb, e.PostalCode, e.FinanceSmsSend, e.FinanceSmsSendDate, e.FinanceEmailSend, e.FinanceEmailSendDate, e.LeadId, e.JobTypeId, e.JobStatusId, e.ElecDistributorId, e.FinanceApplicationDate });

            

            var filter = filteredResult
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(input.FilterName == "FinancePurchaseNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.FinancePurchaseNo == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                        .WhereIf(input.PaymentOption != 0, e => e.PaymentOptionId == input.PaymentOption)

                        .WhereIf(input.IsVerified == 1, e => e.FinanceDocumentVerified == 1) // Verified
                        .WhereIf(input.IsVerified == 2, e => (e.FinanceDocumentVerified == 2 || e.FinanceDocumentVerified == null || e.FinanceDocumentVerified == 0) && !string.IsNullOrEmpty(e.FinancePurchaseNo)) //Not Verified
                        .WhereIf(input.IsVerified == 3, e => e.FinanceDocumentVerified == 3) // Query
                        .WhereIf(input.IsVerified == 4, e => (e.FinanceDocumentVerified == 2 || e.FinanceDocumentVerified == null || e.FinanceDocumentVerified == 0) && string.IsNullOrEmpty(e.FinancePurchaseNo)) // Awaiting
                        ;

            var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 3 && e.LeadFk.OrganizationId == input.OrganizationUnit).AsNoTracking().Select(e => new { e.Id, e.ActionId, e.LeadId, e.ActivityDate, e.ActivityNote });
            #endregion

            var query = (from o in filter

                         join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                         from s5 in j5.DefaultIfEmpty()

                         join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                         from s6 in j6.DefaultIfEmpty()

                         select new GetViewFinanceTrackerDto()
                         {
                             Id = o.Id,
                             LeadId = o.LeadFk.Id,
                             JobNumber = o.JobNumber,
                             FinancePurchaseNo = o.FinancePurchaseNo,
                             FinanceDocumentVerified = o.FinanceDocumentVerified == null ? 2 : (int)o.FinanceDocumentVerified,
                             JobStatus = o.JobStatusFk.Name,
                             JobStatusColorClass = o.JobStatusFk.ColorClass,
                             CompanyName = o.LeadFk.CompanyName,
                             Mobile = o.LeadFk.Mobile,
                             Email = o.LeadFk.Email,
                             Address = o.Address,
                             Suburb = o.Suburb,
                             State = o.State,
                             PostalCode = o.PostalCode,
                             FinanceSmsSend = o.FinanceSmsSend,
                             FinanceSmsSendDate = o.FinanceSmsSendDate,
                             FinanceEmailSend = o.FinanceEmailSend,
                             FinanceEmailSendDate = o.FinanceEmailSendDate,
                             CurrentLeadOwaner = User_List.Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                             ActivityReminderTime = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                             ActivityDescription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                             ActivityComment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                             NotVerifiedDays = (o.FinanceDocumentVerified == null || o.FinanceDocumentVerified == 2 || o.FinanceDocumentVerified == 0) && !string.IsNullOrEmpty(o.FinancePurchaseNo) && o.FinanceApplicationDate != null ? (int)(Today.Value.Date - o.FinanceApplicationDate.Value.Date).TotalDays : 0,
                         });

            var financetrackerListDtos = await query.ToListAsync();
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _leadsExcelExporter.FinanaceTrackerExportToFile(financetrackerListDtos, "FinanceTracker.xlsx");
            }
            else
            {
                return _leadsExcelExporter.FinanaceTrackerExportToFile(financetrackerListDtos, "FinanceTracker.csv");
            }

        }



        public async Task<FileDto> getEssentialTrackerToExcel(GetAllJobApplicationTrackerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var Today = _timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var actvitylog_list = _leadactivityRepository.GetAll().Where(e => e.SectionId == 1 && e.LeadFk.OrganizationId == input.OrganizationUnit);
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            var Doclist = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 2).Select(e => e.JobId).ToList();
            var filteredResult = _jobRepository.GetAll()
                             .Include(e => e.LeadFk)
                             .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                             .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                             .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                             .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.JobStatusId != 3 && e.SolarRebateStatus != 3 && e.FirstDepositDate != null && e.ElecDistributorFk.Name == "Essential")
                             .AsNoTracking()
                             .Select(e => new { e.Id, e.JobNumber, e.LeadFk, e.State, e.ElecDistributorId, e.JobStatusId, e.JobTypeId, e.DepositeRecceivedDate, e.DistApplied, e.ActiveDate, e.ApplicationRefNo, e.MeterPhaseId, e.NMINumber, e.PeakMeterNo, e.DistExpiryDate, e.JobTypeFk, e.JobStatusFk, e.Address, e.Suburb, e.PostalCode, e.ElecDistributorFk, e.DistApproveDate, e.SmsSend, e.SmsSendDate, e.EmailSend, e.EmailSendDate, e.LeadId, e.FirstDepositDate });

            //var jobidstatus = input.testfilter != null ? input.testfilter : null;
            var filteredJobs = filteredResult
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(input.FilterName == "ApplicationRefNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ApplicationRefNo == input.Filter)
                        .WhereIf(input.FilterName == "NMINumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.NMINumber == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                        .WhereIf(input.ElecDistributorId != 0, e => e.ElecDistributorId == input.ElecDistributorId)
                        .WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => input.JobStatusIDFilter.Contains((int)e.JobStatusId))
                        .WhereIf(input.JobTypeId != 0, e => e.JobTypeId == input.JobTypeId)

                        .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ApplicationDate" && input.StartDate != null, e => e.DistApplied.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ApplicationDate" && input.EndDate != null, e => e.DistApplied.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ExpiryDate" && input.StartDate != null, e => e.DistApplied.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ExpiryDate" && input.EndDate != null, e => e.DistApplied.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ActiveDate" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ActiveDate" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.ApplicationStatus == 1, e => e.ApplicationRefNo == null
                        && (_documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() == false || e.MeterPhaseId == null || e.NMINumber == null || e.PeakMeterNo == null)) //Application Awaiting

                        .WhereIf(input.ApplicationStatus == 2, e => e.ApplicationRefNo == null
                        && _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() && e.MeterPhaseId != null && e.NMINumber == null && e.PeakMeterNo != null) //Application Pending

                        .WhereIf(input.ApplicationStatus == 3, e => e.ApplicationRefNo != null) //Application Complete

                        .WhereIf(input.ApplicationStatus == 4, e => e.ApplicationRefNo != null && (e.DistExpiryDate.Value.AddDays(-30) < Today) && e.JobStatusId < 6) //Application Expiry
                        ;

            var activity = _leadactivityRepository.GetAll().AsNoTracking().Where(e => e.SectionId == 1).Select(e => new { e.ActionId, e.LeadId, e.Id, e.ActivityDate, e.ActivityNote });
            var query = (from o in filteredJobs

                         join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                         from s5 in j5.DefaultIfEmpty()

                         join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                         from s6 in j6.DefaultIfEmpty()

                             //join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                             //from s7 in j7.DefaultIfEmpty()

                         select new GetJobForViewDto()
                         {
                             Job = new JobDto
                             {
                                 
                                 JobNumber = o.JobNumber,
                                 CompanyName=o.LeadFk.CompanyName,
                                 Email =o.LeadFk.Email,
                                 Mobile=o.LeadFk.Mobile,
                                 Address=o.Address
                             },
                             
                         });

            var EssentialTrackerListDtos = await query.ToListAsync();
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _leadsExcelExporter.EssentialTrackerExportToFile(EssentialTrackerListDtos);
            }
            else
            {
                return _leadsExcelExporter.EssentialTrackerExportToFile(EssentialTrackerListDtos);
            }
        }
        //JobGrid
        [AbpAuthorize(AppPermissions.Pages_JobGrid)]
        public async Task<PagedResultDto<GetJobForViewDto>> GetAllJobGrid(GetAllJobsInput input)
        {
            #region Old Code
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            //var team_list = _userTeamRepository.GetAll();

            //var User_List = _userRepository.GetAll();
            //var User = User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var FilterTeamList = new List<long?>();
            //if (input.TeamId != null && input.TeamId != 0)
            //{
            //    FilterTeamList = team_list.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            //}

            //var ProductItem = _productItemRepository.GetAll();
            //var panel_list = ProductItem.Where(e => e.ProductTypeId == 1);
            ////var inverter_list = ProductItem.Where(e => (e.ProductTypeId == 2 || e.ProductTypeId == 5));
            //var inverter_list = ProductItem.Where(e => (e.ProductTypeId == 2));
            //var Battery_list = ProductItem.Where(e => (e.ProductTypeId == 5));

            //var PanelDetails = new List<int?>();
            //if (input.PanelModel != null)
            //{
            //    var Panel = panel_list.Where(e => e.ProductTypeId == 1 && e.Model.Contains(input.PanelModel)).Select(e => e.Id).FirstOrDefault();
            //    PanelDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Panel).Select(e => e.JobId).ToList();
            //}

            //var InvertDetails = new List<int?>();
            //if (input.InvertModel != null)
            //{
            //    //var Inverter = inverter_list.Where(e => (e.ProductTypeId == 2 || e.ProductTypeId == 5) && e.Model.Contains(input.InvertModel)).Select(e => e.Id).FirstOrDefault();
            //    var Inverter = inverter_list.Where(e => e.ProductTypeId == 2 && e.Model.Contains(input.InvertModel)).Select(e => e.Id).FirstOrDefault();
            //    InvertDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Inverter).Select(e => e.JobId).ToList();
            //}

            //var batteryDetails = new List<int?>();
            //if (input.BatteryModel != null)
            //{
            //    var Battery = Battery_list.Where(e => e.ProductTypeId == 5 && e.Model.Contains(input.BatteryModel)).Select(e => e.Id).FirstOrDefault();
            //    batteryDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Battery).Select(e => e.JobId).ToList();
            //}

            ////IQueryable<int> FilterdCancelJob;
            //var FilterdCancelJob = new List<int>(); ;
            //if (input.JobDateFilter == "CancelDate" && input.StartDate != null && input.EndDate != null)
            //{
            //    FilterdCancelJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Cancel") && x.LeadFk.OrganizationId == input.OrganizationUnit)
            //        .WhereIf(input.JobDateFilter == "CancelDate" && SDate != null && EDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date && x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            //}
            //else if (input.JobDateFilter == "CancelDate" && input.StartDate != null)
            //{
            //    FilterdCancelJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Cancel") && x.LeadFk.OrganizationId == input.OrganizationUnit)
            //        .WhereIf(input.JobDateFilter == "CancelDate" && SDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            //}
            //else if (input.JobDateFilter == "CancelDate" && input.EndDate != null)
            //{
            //    FilterdCancelJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Cancel") && x.LeadFk.OrganizationId == input.OrganizationUnit)
            //        .WhereIf(input.JobDateFilter == "CancelDate" && EDate != null, x => x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            //}

            ////IQueryable<int> FilterdHoldJob;
            //var FilterdHoldJob = new List<int>();
            //if (input.JobDateFilter == "HoldDate" && input.StartDate != null && input.EndDate != null)
            //{
            //    FilterdHoldJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Hold") && x.LeadFk.OrganizationId == input.OrganizationUnit)
            //        .WhereIf(input.JobDateFilter == "HoldDate" && SDate != null && EDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date && x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            //}
            //else if (input.JobDateFilter == "HoldDate" && input.StartDate != null)
            //{
            //    FilterdHoldJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Hold") && x.LeadFk.OrganizationId == input.OrganizationUnit)
            //        .WhereIf(input.JobDateFilter == "HoldDate" && SDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            //}
            //else if (input.JobDateFilter == "HoldDate" && input.EndDate != null)
            //{
            //    FilterdHoldJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Hold") && x.LeadFk.OrganizationId == input.OrganizationUnit)
            //        .WhereIf(input.JobDateFilter == "HoldDate" && SDate != null, x => x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            //}

            //var filteredJobs = _jobRepository.GetAll()
            //            .Include(e => e.JobTypeFk)
            //            .Include(e => e.JobStatusFk)
            //            .Include(e => e.RoofTypeFk)
            //            .Include(e => e.RoofAngleFk)
            //            .Include(e => e.ElecDistributorFk)
            //            .Include(e => e.LeadFk)
            //            .Include(e => e.ElecRetailerFk)
            //            .Include(e => e.PaymentOptionFk)
            //            .Include(e => e.DepositOptionFk)
            //            .Include(e => e.PromotionOfferFk)
            //            .Include(e => e.HouseTypeFk)
            //            .Include(e => e.FinanceOptionFk)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.RoofTypeNameFilter), e => e.RoofTypeFk != null && e.RoofTypeFk.Name == input.RoofTypeNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.RoofAngleNameFilter), e => e.RoofAngleFk != null && e.RoofAngleFk.Name == input.RoofAngleNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.ElecDistributorNameFilter), e => e.ElecDistributorFk != null && e.ElecDistributorFk.Name == input.ElecDistributorNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.ElecRetailerNameFilter), e => e.ElecRetailerFk != null && e.ElecRetailerFk.Name == input.ElecRetailerNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentOptionNameFilter), e => e.PaymentOptionFk != null && e.PaymentOptionFk.Name == input.PaymentOptionNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.DepositOptionNameFilter), e => e.DepositOptionFk != null && e.DepositOptionFk.Name == input.DepositOptionNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionOfferNameFilter), e => e.PromotionOfferFk != null && e.PromotionOfferFk.Name == input.PromotionOfferNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.HouseTypeNameFilter), e => e.HouseTypeFk != null && e.HouseTypeFk.Name == input.HouseTypeNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.PostalCode, input.PostalCodeFrom) >= 0)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.PostalCode, input.PostalCodeTo) <= 0)
            //            .WhereIf(input.paymentid != 0, e => e.PaymentOptionId == input.paymentid)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
            //            .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))

            //            .WhereIf(input.JobDateFilter == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
            //            .WhereIf(input.JobDateFilter == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

            //            .WhereIf(input.JobDateFilter == "FirstDeposite" && input.StartDate != null, e => e.FirstDepositDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.JobDateFilter == "FirstDeposite" && input.EndDate != null, e => e.FirstDepositDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.JobDateFilter == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.JobDateFilter == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.JobDateFilter == "Active" && input.StartDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
            //            .WhereIf(input.JobDateFilter == "Active" && input.EndDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

            //            .WhereIf(input.JobDateFilter == "NextFollowUp" && input.StartDate != null, e => e.JobReminderTime.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.JobDateFilter == "NextFollowUp" && input.EndDate != null, e => e.JobReminderTime.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.JobDateFilter == "InstallBook" && input.StartDate != null, e => e.InstallationDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
            //            .WhereIf(input.JobDateFilter == "InstallBook" && input.EndDate != null, e => e.InstallationDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

            //            .WhereIf(input.JobDateFilter == "InstallComplete" && input.StartDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
            //            .WhereIf(input.JobDateFilter == "InstallComplete" && input.EndDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

            //            .WhereIf(input.JobDateFilter == "CancelDate", e => FilterdCancelJob.Contains((int)e.LeadId))
            //            .WhereIf(input.JobDateFilter == "HoldDate", e => FilterdHoldJob.Contains((int)e.LeadId))

            //            .WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => e.JobStatusId != 1)
            //            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            //            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            //            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            //            .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
            //            .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)

            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PanelModel), e => PanelDetails.Contains(e.Id))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.BatteryModel), e => batteryDetails.Contains(e.Id))

            //            .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.LeadFk.AssignToUserID))
            //            .WhereIf(input.Solarvicstatus != 0, e => e.SolarRebateStatus == input.Solarvicstatus)
            //            .WhereIf(input.Readytoactive != null && input.Readytoactive == 1, e => e.IsActive == true && (e.JobStatusId == 1 || e.JobStatusId == 4))
            //            .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemEndRange != null && input.SystemEndRange != 0, e => e.SystemCapacity >= input.SystemStrtRange && e.SystemCapacity <= input.SystemEndRange)
            //            .WhereIf(input.SystemStrtRange != null && input.SystemStrtRange != 0 && input.SystemEndRange == null && input.SystemEndRange == 0, e => e.SystemCapacity == input.SystemStrtRange)
            //            .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemStrtRange == null && input.SystemStrtRange == 0, e => e.SystemCapacity == input.SystemEndRange)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
            //            .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadFk.LeadSourceId))

            //            .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
            //            .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5))

            //            .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
            //            .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

            //            .WhereIf(input.BatteryFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

            //            .WhereIf(input.BatteryFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

            //            .WhereIf(input.PreviousJobStatusId != null && input.PreviousJobStatusId.Count() > 0, e => e.JobStatusId == 3 && input.PreviousJobStatusId.Contains(_previousJobStatusRepository.GetAll().Where(pr => pr.JobId == e.Id).OrderByDescending(r => r.Id).FirstOrDefault().PreviousId))

            //            .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);

            //var pagedAndFilteredJobs = filteredJobs
            //    .OrderBy(input.Sorting ?? "id desc")
            //    .PageBy(input);

            ////var jobids = filteredJobs.Select(e => e.Id).ToList();
            //var summaryCount = new jobcountdata();

            //summaryCount.TotalNoOfPanel = (from o in _jobProductItemRepository.GetAll()
            //                               join j in filteredJobs on o.JobId equals j.Id
            //                               where o.ProductItemFk.ProductTypeId == 1
            //                               select o.Quantity
            //                               ).Sum();

            //summaryCount.TotalNoOfPanel = summaryCount.TotalNoOfPanel == null ? 0 : summaryCount.TotalNoOfPanel;

            //summaryCount.TotalSystemCapacity = filteredJobs.Select(e => e.SystemCapacity).Sum();
            //summaryCount.TotalCost = filteredJobs.Select(e => e.TotalCost).Sum();
            //summaryCount.DepositeReceived = filteredJobs.Where(e => e.JobStatusId == 4).Count();
            //summaryCount.Active = filteredJobs.Where(e => e.JobStatusId == 5).Count();
            //summaryCount.InstallJob = filteredJobs.Where(e => e.JobStatusId == 8).Count();

            //summaryCount.TotalBatteryKw = (from o in _jobProductItemRepository.GetAll()
            //                               join j in filteredJobs on o.JobId equals j.Id
            //                               where o.ProductItemFk.ProductTypeId == 5
            //                               select (o.ProductItemFk.Size * o.Quantity)
            //                               ).Sum();

            //var leadSource = _lookup_leadSourceRepository.GetAll();

            //var jobs = from o in pagedAndFilteredJobs

            //           let previousJobStatusId = _previousJobStatusRepository.GetAll().Where(e => e.JobId == o.Id).OrderByDescending(e => e.Id).FirstOrDefault().PreviousId

            //           select new GetJobForViewDto()
            //           {
            //               Job = new JobDto
            //               {
            //                   Id = o.Id,
            //                   JobNumber = o.JobNumber,
            //                   PostalCode = o.PostalCode,
            //                   LeadId = o.LeadId,
            //                   InstallationDate = o.InstallationDate,
            //                   CurrentLeadOwaner = User_List.Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
            //                   DepositeRecceivedDate = o.DepositeRecceivedDate
            //               },

            //               JobStatusName = o.JobStatusFk.Name,
            //               JobStatusColorClass = o.JobStatusFk.ColorClass,
            //               LeadCompanyName = o.LeadFk.CompanyName,
            //               Mobile = o.LeadFk.Mobile,
            //               LeadSourceName = o.LeadFk.LeadSource,
            //               LeadSourceColorClass = leadSource.Where(e => e.Id == o.LeadFk.LeadSourceId).Select(e => e.ColorClass).FirstOrDefault(),
            //               JobSolarRebateStatus = o.SolarRebateStatus.ToString(),

            //               ReminderTime = o.JobReminderTime != null ? o.JobReminderTime.Value.ToString("dd-MM-yyyy hh:mm:ss") : null,
            //               ActivityDescription = o.JobReminderDes,
            //               ActivityComment = o.JobComment,

            //               JobCancelReason = _jobCancellationReasonReasonRepository.GetAll().Where(e => e.Id == o.JobCancelReasonId).FirstOrDefault().Name,
            //               JobCancelReasonNotes = o.JobCancelReason,

            //               JobHoldReason = _jobHoldReasonRepository.GetAll().Where(e => e.Id == o.JobHoldReasonId).FirstOrDefault().JobHoldReason,
            //               JobHoldReasonNotes = o.JobHoldReason,

            //               PreviousJobStatus = o.JobStatusId == 3 ? _lookup_jobStatusRepository.GetAll().Where(e => e.Id == previousJobStatusId).FirstOrDefault().Name : "",

            //               SummerCount = summaryCount
            //           };

            //var totalCount = await filteredJobs.CountAsync();

            //return new PagedResultDto<GetJobForViewDto>(
            //    totalCount,
            //    await jobs.ToListAsync()
            //);
            #endregion

            #region New Code
            //var StartDate = string.IsNullOrEmpty(input.StartDate) ? (DateTime?)null : DateTime.Parse(input.StartDate);
            //var EndDate = string.IsNullOrEmpty(input.EndDate) ? (DateTime?)null : DateTime.Parse(input.EndDate);

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = await team_list.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToListAsync();
            }

            var ProductItem = _productItemRepository.GetAll().AsNoTracking().Select(e => new { e.Id, e.ProductTypeId, e.Model });
            var panel_list = ProductItem.Where(e => e.ProductTypeId == 1);
            var inverter_list = ProductItem.Where(e => (e.ProductTypeId == 2));
            var Battery_list = ProductItem.Where(e => (e.ProductTypeId == 5));

            var PanelDetails = new List<int?>();
            if (input.PanelModel != null)
            {
                var Panel = await panel_list.Where(e => e.ProductTypeId == 1 && e.Model.Contains(input.PanelModel)).Select(e => e.Id).FirstOrDefaultAsync();
                PanelDetails = await _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Panel).AsNoTracking().Select(e => e.JobId).ToListAsync();
            }

            var InvertDetails = new List<int?>();
            if (input.InvertModel != null)
            {
                var Inverter = await inverter_list.Where(e => e.ProductTypeId == 2 && e.Model.Contains(input.InvertModel)).Select(e => e.Id).FirstOrDefaultAsync();
                InvertDetails = await _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Inverter).AsNoTracking().Select(e => e.JobId).ToListAsync();
            }

            var batteryDetails = new List<int?>();
            if (input.BatteryModel != null)
            {
                var Battery = await Battery_list.Where(e => e.ProductTypeId == 5 && e.Model.Contains(input.BatteryModel)).Select(e => e.Id).FirstOrDefaultAsync();
                batteryDetails = await _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Battery).AsNoTracking().Select(e => e.JobId).ToListAsync();
            }

            var FilterdCancelJob = new List<int>(); ;
            if (input.JobDateFilter == "CancelDate" && input.StartDate != null && input.EndDate != null)
            {
                FilterdCancelJob = await _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Cancel") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "CancelDate" && SDate != null && EDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date && x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).AsNoTracking().Select(x => x.LeadId).ToListAsync();
            }
            else if (input.JobDateFilter == "CancelDate" && input.StartDate != null)
            {
                FilterdCancelJob = await _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Cancel") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "CancelDate" && SDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).AsNoTracking().Select(x => x.LeadId).ToListAsync();
            }
            else if (input.JobDateFilter == "CancelDate" && input.EndDate != null)
            {
                FilterdCancelJob = await _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Cancel") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "CancelDate" && EDate != null, x => x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).AsNoTracking().Select(x => x.LeadId).ToListAsync();
            }

            var FilterdHoldJob = new List<int>();
            if (input.JobDateFilter == "HoldDate" && input.StartDate != null && input.EndDate != null)
            {
                FilterdHoldJob = await _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Hold") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "HoldDate" && SDate != null && EDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date && x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).AsNoTracking().Select(x => x.LeadId).ToListAsync();
            }
            else if (input.JobDateFilter == "HoldDate" && input.StartDate != null)
            {
                FilterdHoldJob = await _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Hold") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "HoldDate" && SDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).AsNoTracking().Select(x => x.LeadId).ToListAsync();
            }
            else if (input.JobDateFilter == "HoldDate" && input.EndDate != null)
            {
                FilterdHoldJob = await _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Hold") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "HoldDate" && SDate != null, x => x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).AsNoTracking().Select(x => x.LeadId).ToListAsync();
            }

            List<int> batteryInverter = new List<int>() { 1, 3, 4, 6 };
            input.JobTypeNameListFilter = input.JobTypeNameListFilter != null ? input.JobTypeNameListFilter : new List<string>();

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.LeadFk)
                        .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit)
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter.Trim())
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter.Trim())
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter.Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
                        .WhereIf(input.JobTypeNameListFilter.Count() > 0, e => input.JobTypeNameListFilter.Contains(e.JobTypeFk.Name))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofTypeNameFilter), e => e.RoofTypeFk != null && e.RoofTypeFk.Name == input.RoofTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofAngleNameFilter), e => e.RoofAngleFk != null && e.RoofAngleFk.Name == input.RoofAngleNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecDistributorNameFilter), e => e.ElecDistributorFk != null && e.ElecDistributorFk.Name == input.ElecDistributorNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecRetailerNameFilter), e => e.ElecRetailerFk != null && e.ElecRetailerFk.Name == input.ElecRetailerNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentOptionNameFilter), e => e.PaymentOptionFk != null && e.PaymentOptionFk.Name == input.PaymentOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DepositOptionNameFilter), e => e.DepositOptionFk != null && e.DepositOptionFk.Name == input.DepositOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionOfferNameFilter), e => e.PromotionOfferFk != null && e.PromotionOfferFk.Name == input.PromotionOfferNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.HouseTypeNameFilter), e => e.HouseTypeFk != null && e.HouseTypeFk.Name == input.HouseTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.PostalCode, input.PostalCodeFrom) >= 0)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.PostalCode, input.PostalCodeTo) <= 0)
                        .WhereIf(input.paymentid != 0, e => e.PaymentOptionId == input.paymentid)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
                        .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))

                        .WhereIf(input.JobDateFilter == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "FirstDeposite" && input.StartDate != null, e => e.FirstDepositDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "FirstDeposite" && input.EndDate != null, e => e.FirstDepositDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "Active" && input.StartDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "Active" && input.EndDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "NextFollowUp" && input.StartDate != null, e => e.JobReminderTime.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "NextFollowUp" && input.EndDate != null, e => e.JobReminderTime.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "InstallBook" && input.StartDate != null, e => e.InstallationDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallBook" && input.EndDate != null, e => e.InstallationDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "InstallComplete" && input.StartDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallComplete" && input.EndDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "CancelDate", e => FilterdCancelJob.Contains((int)e.LeadId))
                        .WhereIf(input.JobDateFilter == "HoldDate", e => FilterdHoldJob.Contains((int)e.LeadId))

                        //.WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => e.JobStatusId != 1)
                        .WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => (e.JobStatusId != 1 || e.JobTypeId == 6))
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)

                        .WhereIf(!string.IsNullOrWhiteSpace(input.PanelModel), e => PanelDetails.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.BatteryModel), e => batteryDetails.Contains(e.Id))

                        .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(input.Solarvicstatus != 0, e => e.SolarRebateStatus == input.Solarvicstatus)
                        .WhereIf(input.Readytoactive != null && input.Readytoactive == 1, e => e.IsActive == true && (e.JobStatusId == 1 || e.JobStatusId == 4))
                        .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemEndRange != null && input.SystemEndRange != 0, e => e.SystemCapacity >= input.SystemStrtRange && e.SystemCapacity <= input.SystemEndRange)
                        .WhereIf(input.SystemStrtRange != null && input.SystemStrtRange != 0 && input.SystemEndRange == null && input.SystemEndRange == 0, e => e.SystemCapacity == input.SystemStrtRange)
                        .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemStrtRange == null && input.SystemStrtRange == 0, e => e.SystemCapacity == input.SystemEndRange)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadFk.LeadSourceId))

                        .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5))

                        .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                        .WhereIf(input.BatteryFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                        .WhereIf(input.BatteryFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                        .WhereIf(input.BatteryFilter == 5, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 2).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && batteryInverter.Contains(j.ProductItemFk.ProductTypeId)).Count() == 0) //For Only Inverter and Battery
                        .WhereIf(input.BatteryFilter == 6, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5)) //For with battery

                        .WhereIf(input.PreviousJobStatusId != null && input.PreviousJobStatusId.Count() > 0, e => e.JobStatusId == 3 && input.PreviousJobStatusId.Contains(_previousJobStatusRepository.GetAll().Where(pr => pr.JobId == e.Id).AsNoTracking().OrderByDescending(r => r.Id).FirstOrDefault().PreviousId))

                        .WhereIf(!string.IsNullOrEmpty(input.PriceType), e => e.PriceType == input.PriceType)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter))
                        
                        .AsNoTracking()
                        .Select(e => new { e.Id, e.SystemCapacity, e.TotalCost, e.JobStatusId, e.JobNumber, e.PostalCode, e.LeadId, e.InstallationDate, e.LeadFk, e.DepositeRecceivedDate, e.JobStatusFk, e.SolarRebateStatus, e.JobReminderTime, e.JobReminderDes, e.JobComment, e.JobCancelReasonId, e.JobCancelReason, e.JobHoldReasonId, e.JobHoldReason });

            var pagedAndFilteredJobs = filteredJobs
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            //var jobids = filteredJobs.Select(e => e.Id).ToList();
            var summaryCount = new jobcountdata();

            summaryCount.TotalNoOfPanel = await (from o in _jobProductItemRepository.GetAll().AsNoTracking()
                                           join j in filteredJobs on o.JobId equals j.Id
                                           where o.ProductItemFk.ProductTypeId == 1
                                           select o.Quantity
                                           ).SumAsync();

            summaryCount.TotalNoOfPanel = summaryCount.TotalNoOfPanel == null ? 0 : summaryCount.TotalNoOfPanel;

            summaryCount.TotalSystemCapacity = await filteredJobs.Select(e => e.SystemCapacity).SumAsync();
            summaryCount.TotalMetroSystemCapacity = await filteredJobs.Where(e => e.LeadFk.Area == "Metro").Select(e => e.SystemCapacity).SumAsync();
            summaryCount.TotalRegionalSystemCapacity = await filteredJobs.Where(e => e.LeadFk.Area == "Regional").Select(e => e.SystemCapacity).SumAsync();
            summaryCount.TotalCost = await filteredJobs.Select(e => e.TotalCost).SumAsync();
            summaryCount.TotalRegionalCost = await filteredJobs.Where(e => e.LeadFk.Area == "Regional").Select(e => e.TotalCost).SumAsync();
            summaryCount.TotalMetroCost = await filteredJobs.Where(e => e.LeadFk.Area == "Metro").Select(e => e.TotalCost).SumAsync();
            summaryCount.DepositeReceived = await filteredJobs.Where(e => e.JobStatusId == 4).CountAsync();
            summaryCount.Active = await filteredJobs.Where(e => e.JobStatusId == 5).CountAsync();
            summaryCount.InstallJob = await filteredJobs.Where(e => e.JobStatusId == 8).CountAsync();

            summaryCount.TotalBatteryKw = await (from o in _jobProductItemRepository.GetAll().AsNoTracking()
                                           join j in filteredJobs on o.JobId equals j.Id
                                           where o.ProductItemFk.ProductTypeId == 5
                                           select (o.ProductItemFk.Size * o.Quantity)
                                           ).SumAsync();

            summaryCount.TotalMetroBatteryKw = await (from o in _jobProductItemRepository.GetAll().AsNoTracking()
                                           join j in filteredJobs.Where(e => e.LeadFk.Area == "Metro") on o.JobId equals j.Id
                                           where o.ProductItemFk.ProductTypeId == 5
                                           select (o.ProductItemFk.Size * o.Quantity)
                                           ).SumAsync();

            summaryCount.TotalRegionalBatteryKw = await (from o in _jobProductItemRepository.GetAll().AsNoTracking()
                                           join j in filteredJobs.Where(e => e.LeadFk.Area == "Regional") on o.JobId equals j.Id
                                           where o.ProductItemFk.ProductTypeId == 5
                                           select (o.ProductItemFk.Size * o.Quantity)
                                           ).SumAsync();


            var leadSource = _lookup_leadSourceRepository.GetAll().AsNoTracking().Select(e => new { e.Id, e.ColorClass });

            var jobs = from o in pagedAndFilteredJobs

                       let previousJobStatusId = _previousJobStatusRepository.GetAll().Where(e => e.JobId == o.Id).OrderByDescending(e => e.Id).FirstOrDefault().PreviousId

                       select new GetJobForViewDto()
                       {
                           Job = new JobDto
                           {
                               Id = o.Id,
                               JobNumber = o.JobNumber,
                               PostalCode = o.PostalCode,
                               LeadId = o.LeadId,
                               InstallationDate = o.InstallationDate,
                               CurrentLeadOwaner = User_List.Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                               DepositeRecceivedDate = o.DepositeRecceivedDate
                           },

                           JobStatusName = o.JobStatusFk.Name,
                           JobStatusColorClass = o.JobStatusFk.ColorClass,
                           LeadCompanyName = o.LeadFk.CompanyName,
                           Mobile = o.LeadFk.Mobile,
                           LeadSourceName = o.LeadFk.LeadSource,
                           LeadSourceColorClass = leadSource.Where(e => e.Id == o.LeadFk.LeadSourceId).Select(e => e.ColorClass).FirstOrDefault(),
                           JobSolarRebateStatus = o.SolarRebateStatus.ToString(),

                           ReminderTime = o.JobReminderTime != null ? o.JobReminderTime.Value.ToString("dd-MM-yyyy hh:mm:ss") : null,
                           ActivityDescription = o.JobReminderDes,
                           ActivityComment = o.JobComment,

                           JobCancelReason = _jobCancellationReasonReasonRepository.GetAll().Where(e => e.Id == o.JobCancelReasonId).FirstOrDefault().Name,
                           JobCancelReasonNotes = o.JobCancelReason,

                           JobHoldReason = _jobHoldReasonRepository.GetAll().Where(e => e.Id == o.JobHoldReasonId).FirstOrDefault().JobHoldReason,
                           JobHoldReasonNotes = o.JobHoldReason,

                           PreviousJobStatus = o.JobStatusId == 3 ? _lookup_jobStatusRepository.GetAll().Where(e => e.Id == previousJobStatusId).FirstOrDefault().Name : "",

                           SummerCount = summaryCount,
                           IsBattery = _jobProductItemRepository.GetAll().Any(j => j.JobId == o.Id && j.ProductItemFk.ProductTypeId == 5)
                       };

            var totalCount = await filteredJobs.CountAsync();

            return new PagedResultDto<GetJobForViewDto>(totalCount, await jobs.ToListAsync()
            );
            #endregion
        }

        public async Task<FileDto> getJobGirdTrackerToExcel(GetAllJobsForExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId, e.TeamFk });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = await team_list.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToListAsync();
            }

            var ProductItem = _productItemRepository.GetAll().AsNoTracking().Select(e => new { e.Id, e.ProductTypeId, e.Model });
            var panel_list = ProductItem.Where(e => e.ProductTypeId == 1);
            var inverter_list = ProductItem.Where(e => (e.ProductTypeId == 2));
            var Battery_list = ProductItem.Where(e => (e.ProductTypeId == 5));

            var PanelDetails = new List<int?>();
            if (input.PanelModel != null)
            {
                var Panel = await panel_list.Where(e => e.ProductTypeId == 1 && e.Model.Contains(input.PanelModel)).Select(e => e.Id).FirstOrDefaultAsync();
                PanelDetails = await _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Panel).AsNoTracking().Select(e => e.JobId).ToListAsync();
            }

            var InvertDetails = new List<int?>();
            if (input.InvertModel != null)
            {
                var Inverter = await inverter_list.Where(e => e.ProductTypeId == 2 && e.Model.Contains(input.InvertModel)).Select(e => e.Id).FirstOrDefaultAsync();
                InvertDetails = await _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Inverter).AsNoTracking().Select(e => e.JobId).ToListAsync();
            }

            var batteryDetails = new List<int?>();
            if (input.BatteryModel != null)
            {
                var Battery = await Battery_list.Where(e => e.ProductTypeId == 5 && e.Model.Contains(input.BatteryModel)).Select(e => e.Id).FirstOrDefaultAsync();
                batteryDetails = await _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Battery).AsNoTracking().Select(e => e.JobId).ToListAsync();
            }

            var FilterdCancelJob = new List<int>(); ;
            if (input.JobDateFilter == "CancelDate" && input.StartDate != null && input.EndDate != null)
            {
                FilterdCancelJob = await _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Cancel") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "CancelDate" && SDate != null && EDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date && x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).AsNoTracking().Select(x => x.LeadId).ToListAsync();
            }
            else if (input.JobDateFilter == "CancelDate" && input.StartDate != null)
            {
                FilterdCancelJob = await _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Cancel") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "CancelDate" && SDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).AsNoTracking().Select(x => x.LeadId).ToListAsync();
            }
            else if (input.JobDateFilter == "CancelDate" && input.EndDate != null)
            {
                FilterdCancelJob = await _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Cancel") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "CancelDate" && EDate != null, x => x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).AsNoTracking().Select(x => x.LeadId).ToListAsync();
            }

            var FilterdHoldJob = new List<int>();
            if (input.JobDateFilter == "HoldDate" && input.StartDate != null && input.EndDate != null)
            {
                FilterdHoldJob = await _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Hold") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "HoldDate" && SDate != null && EDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date && x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).AsNoTracking().Select(x => x.LeadId).ToListAsync();
            }
            else if (input.JobDateFilter == "HoldDate" && input.StartDate != null)
            {
                FilterdHoldJob = await _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Hold") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "HoldDate" && SDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).AsNoTracking().Select(x => x.LeadId).ToListAsync();
            }
            else if (input.JobDateFilter == "HoldDate" && input.EndDate != null)
            {
                FilterdHoldJob = await _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Hold") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "HoldDate" && SDate != null, x => x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).AsNoTracking().Select(x => x.LeadId).ToListAsync();
            }
            input.JobTypeNameListFilter = input.JobTypeNameListFilter != null ? input.JobTypeNameListFilter : new List<string>();

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.LeadFk)
                        .Include(e => e.RoofAngleFk)
                        .Include(e => e.FinanceOptionFk)
                        .Include(e => e.DepositOptionFk)
                        .Include(e => e.HouseTypeFk)
                        .Include(e => e.RoofTypeFk)
                        .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
                        .WhereIf(input.JobTypeNameListFilter.Count() > 0, e => input.JobTypeNameListFilter.Contains(e.JobTypeFk.Name))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofTypeNameFilter), e => e.RoofTypeFk != null && e.RoofTypeFk.Name == input.RoofTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofAngleNameFilter), e => e.RoofAngleFk != null && e.RoofAngleFk.Name == input.RoofAngleNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecDistributorNameFilter), e => e.ElecDistributorFk != null && e.ElecDistributorFk.Name == input.ElecDistributorNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecRetailerNameFilter), e => e.ElecRetailerFk != null && e.ElecRetailerFk.Name == input.ElecRetailerNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentOptionNameFilter), e => e.PaymentOptionFk != null && e.PaymentOptionFk.Name == input.PaymentOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DepositOptionNameFilter), e => e.DepositOptionFk != null && e.DepositOptionFk.Name == input.DepositOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionOfferNameFilter), e => e.PromotionOfferFk != null && e.PromotionOfferFk.Name == input.PromotionOfferNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.HouseTypeNameFilter), e => e.HouseTypeFk != null && e.HouseTypeFk.Name == input.HouseTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.PostalCode, input.PostalCodeFrom) >= 0)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.PostalCode, input.PostalCodeTo) <= 0)
                        .WhereIf(input.paymentid != 0, e => e.PaymentOptionId == input.paymentid)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
                        .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))

                        .WhereIf(input.JobDateFilter == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "FirstDeposite" && input.StartDate != null, e => e.FirstDepositDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "FirstDeposite" && input.EndDate != null, e => e.FirstDepositDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "Active" && input.StartDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "Active" && input.EndDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "NextFollowUp" && input.StartDate != null, e => e.JobReminderTime.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "NextFollowUp" && input.EndDate != null, e => e.JobReminderTime.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "InstallBook" && input.StartDate != null, e => e.InstallationDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallBook" && input.EndDate != null, e => e.InstallationDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "InstallComplete" && input.StartDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallComplete" && input.EndDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "CancelDate", e => FilterdCancelJob.Contains((int)e.LeadId))
                        .WhereIf(input.JobDateFilter == "HoldDate", e => FilterdHoldJob.Contains((int)e.LeadId))

                        .WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => e.JobStatusId != 1)
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)

                        .WhereIf(!string.IsNullOrWhiteSpace(input.PanelModel), e => PanelDetails.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.BatteryModel), e => batteryDetails.Contains(e.Id))

                        .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(input.Solarvicstatus != 0, e => e.SolarRebateStatus == input.Solarvicstatus)
                        .WhereIf(input.Readytoactive != null && input.Readytoactive == 1, e => e.IsActive == true && (e.JobStatusId == 1 || e.JobStatusId == 4))
                        .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemEndRange != null && input.SystemEndRange != 0, e => e.SystemCapacity >= input.SystemStrtRange && e.SystemCapacity <= input.SystemEndRange)
                        .WhereIf(input.SystemStrtRange != null && input.SystemStrtRange != 0 && input.SystemEndRange == null && input.SystemEndRange == 0, e => e.SystemCapacity == input.SystemStrtRange)
                        .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemStrtRange == null && input.SystemStrtRange == 0, e => e.SystemCapacity == input.SystemEndRange)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadFk.LeadSourceId))

                        .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5))

                        .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                        .WhereIf(input.BatteryFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                        .WhereIf(input.BatteryFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                        .WhereIf(input.PreviousJobStatusId != null && input.PreviousJobStatusId.Count() > 0, e => e.JobStatusId == 3 && input.PreviousJobStatusId.Contains(_previousJobStatusRepository.GetAll().Where(pr => pr.JobId == e.Id).AsNoTracking().OrderByDescending(r => r.Id).FirstOrDefault().PreviousId))

                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter))
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber.Contains(input.Filter))
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile.Contains(input.Filter))
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName.Contains(input.Filter))

                        .AsNoTracking()
                        .Select(e => new { e.Id, e.JobNumber, e.ManualQuote, e.LeadFk, e.State, e.SystemCapacity, e.TotalCost, e.JobStatusId, e.PostalCode, e.LeadId, e.InstallationDate,  e.DepositeRecceivedDate, e.JobStatusFk, e.SolarRebateStatus, e.JobReminderTime, e.JobReminderDes, e.JobComment, e.JobCancelReasonId, e.JobCancelReason, e.JobHoldReasonId, e.JobHoldReason, e.FirstDepositDate, e.CreationTime, e.Note, e.STC, e.PaymentOptionFk, e.JobTypeId, e.PriceType, e.RoofAngleFk, e.MeterUpgradeId, e.MeterPhaseId, e.FinanceOptionFk, e.DepositOptionFk, e.RemoveOldSys, e.HouseTypeFk,e.RoofTypeFk, e.ActiveDate }).OrderByDescending(e => e.Id); 

            var leadSource = _lookup_leadSourceRepository.GetAll();
            //var totalBatteryQuantity = await (from p in _jobProductItemRepository.GetAll().AsNoTracking()
            //                                  join j in filteredJobs on p.JobId equals j.Id
            //                                  where p.ProductItemFk.ProductTypeId == 1
            //                                  select p.Quantity
            //                      ).SumAsync();
            var jobs = from o in filteredJobs
                       
                       let previousJobStatusId = _previousJobStatusRepository.GetAll().Where(e => e.JobId == o.Id).OrderByDescending(e => e.Id).FirstOrDefault().PreviousId
                       select new GetJobForExcelViewDto()
                       {
                           Job = new JobDto
                           {
                               Id = o.Id,
                               JobNumber = o.JobNumber,
                               ManualQuote = o.ManualQuote,
                               CurrentLeadOwaner = User_List.Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                               State = o.State,
                               PostalCode = o.PostalCode,
                               SystemCapacity = o.SystemCapacity,
                               TotalCost = o.TotalCost,
                               DepositeRecceivedDate = o.DepositeRecceivedDate,
                               InstallationDate = o.InstallationDate,
                               FirstDepositDate = o.FirstDepositDate,
                               CreationTime = o.CreationTime,
                               Note = o.Note,
                               Stc = o.STC,
                               Jobtype = _lookup_jobTypeRepository.GetAll().Where(e => e.Id == o.JobTypeId).AsNoTracking().Select(e => e.Name).FirstOrDefault(),
                               PriceType = o.PriceType,
                             // BatteryKw = _jobProductItemRepository.GetAll().Where(e => e.JobId == o.Id && e.ProductItemFk.ProductTypeId == 5).Any()? _jobProductItemRepository.GetAll().Where(e=> e.JobId == o.Id && e.ProductItemFk.ProductTypeId == 5).AsNoTracking().Sum(e => e.ProductItemFk.Size * e.Quantity) : 0

                           },

                           Lead = new LeadDto
                           {
                               Id = o.LeadFk.Id,
                               CompanyName = o.LeadFk.CompanyName,
                               LeadSource = o.LeadFk.LeadSource,
                               CreationTime = o.LeadFk.CreationTime,
                               Type = o.LeadFk.Type,
                               Area = o.LeadFk.Area
                           },

                           JobStatusName = o.JobStatusFk.Name,
                           Team = team_list.Where(e => e.UserId == o.LeadFk.AssignToUserID).Select(e => e.TeamFk.Name).FirstOrDefault(),
                           //HouseType = _jobVariationRepository.GetAll().Where(e => e.JobId == o.Id && e.VariationFk.Name == "House Type").AsNoTracking().Select(e => e.VariationFk.Name).FirstOrDefault(),
                           HouseType = o.HouseTypeFk.Name,
                           //RoofType = _jobVariationRepository.GetAll().Where(e => e.JobId == o.Id && e.VariationFk.Name == "Roof Type").AsNoTracking().Select(e => e.VariationFk.Name).FirstOrDefault(),
                           RoofType = o.RoofTypeFk.Name,
                           PaymentOption = o.PaymentOptionFk.Name,
                           FirstDepositAmount = _invoicePaymentRepository.GetAll().Where(e => e.JobId == o.Id).AsNoTracking().Select(e => e.InvoicePayTotal).FirstOrDefault(),

                           ReminderTime = o.JobReminderTime != null ? o.JobReminderTime.Value.ToString("dd-MM-yyyy") : null,
                           ActivityDescription = o.JobReminderDes,
                           ActivityComment = o.JobComment,
                           noOfPanel = _jobProductItemRepository.GetAll().Where(e => e.ProductItemFk.ProductTypeId == 1 && e.JobId == o.Id).AsNoTracking().Select(e => e.Quantity).FirstOrDefault(),

                           JobCancelReason = _jobCancellationReasonReasonRepository.GetAll().Where(e => e.Id == o.JobCancelReasonId).AsNoTracking().FirstOrDefault().Name,
                           JobCancelReasonNotes = o.JobCancelReason,

                           PreviousJobStatus = o.JobStatusId == 3 ? _lookup_jobStatusRepository.GetAll().Where(e => e.Id == previousJobStatusId).AsNoTracking().FirstOrDefault().Name : "",
                           SalesRapName = User_List.Where(e => e.Id == o.LeadFk.AssignToUserID).AsNoTracking().Select(e => e.FullName).FirstOrDefault(),

                           RoofAngle = o.RoofAngleFk.Name,
                           MeterUpgrade = o.MeterUpgradeId == 1 ? "Yes" : (o.MeterUpgradeId == 2 ? "No" : " "),
                           MeterPhase = o.MeterPhaseId,
                           FinPaymentType = o.FinanceOptionFk.Name,
                           Deposit = o.DepositOptionFk.Name,
                           OldSys = o.RemoveOldSys == 1 ? "Yes" : (o.RemoveOldSys == 2 ? "No" : " "),
                           DiffDays = (o.CreationTime.Date - o.LeadFk.CreationTime.Date).Days,
                           First2Depodit = (o.DepositeRecceivedDate.Value.Date - o.FirstDepositDate.Value.Date).Days,
                           Deposit2Active = (o.ActiveDate.Value.Date - o.DepositeRecceivedDate.Value.Date).Days,
                           Active2Install = (o.InstallationDate.Value.Date - o.ActiveDate.Value.Date).Days,
                       };

            var jobgridrListDtos = await jobs.ToListAsync();

            for (int i = 0; i < jobgridrListDtos.Count; i++)
            {
                if (_jobProductItemRepository.GetAll().Where(e => e.JobId == jobgridrListDtos[i].Job.Id).Any())
                {
                    jobgridrListDtos[i].ProductItems = (from jp in _jobProductItemRepository.GetAll().Where(e => e.JobId == jobgridrListDtos[i].Job.Id)
                                                        select new GetJobProductItemDetailsForExcel()
                                                        {
                                                            Id = jp.ProductItemFk.ProductTypeId,
                                                            ProductType = jp.ProductItemFk.ProductTypeFk.Name,
                                                            ProductItem = jp.ProductItemFk.Name,
                                                            ProductModel = jp.ProductItemFk.Model,
                                                            ProductSize = jp.ProductItemFk.Size,
                                                            Quantity = jp.Quantity,
                                                            //betteryKW = jp.ProductItemFk.ProductTypeId == 5 ? jp.ProductItemFk.Size * jp.Quantity
                                                            betteryKW = jp.ProductItemFk.ProductTypeId == 5 ? jp.ProductItemFk.Size * jp.Quantity : 0

                                                        }).OrderBy(r => r.Id).ToList();
                }
                //if (_jobProductItemRepository.GetAll().Where(e => e.JobId == jobgridrListDtos[i].Job.Id && e.ProductItemFk.ProductTypeId == 5).Any())
                //{
                //    jobgridrListDtos[i].Job.BatteryKw = _jobProductItemRepository.GetAll().Where(e => e.JobId == jobgridrListDtos[i].Job.Id && e.ProductItemFk.ProductTypeId == 5).Sum(e => e.ProductItemFk.Size * e.Quantity);
                //}
            }

             if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _leadsExcelExporter.JobGridExportToFile(jobgridrListDtos, "JobGrid.xlsx");
            }
            else
            {
                return _leadsExcelExporter.JobGridExportToFile(jobgridrListDtos, "JobGrid.csv");
                //return _leadsExcelExporter.JobGridExportToCsvFile(jobgridrListDtos);
            }
        }

        public async Task<FileDto> GetJobGirdTrackerProductToExcel(GetAllJobsForExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var team_list = _userTeamRepository.GetAll();

            var User_List = _userRepository.GetAll();
            var User = User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = team_list.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }

            var ProductItem = _productItemRepository.GetAll();
            var panel_list = ProductItem.Where(e => e.ProductTypeId == 1);
            //var inverter_list = ProductItem.Where(e => (e.ProductTypeId == 2 || e.ProductTypeId == 5));
            var inverter_list = ProductItem.Where(e => (e.ProductTypeId == 2));
            var Battery_list = ProductItem.Where(e => (e.ProductTypeId == 5));

            var PanelDetails = new List<int?>();
            if (input.PanelModel != null)
            {
                var Panel = panel_list.Where(e => e.ProductTypeId == 1 && e.Model.Contains(input.PanelModel)).Select(e => e.Id).FirstOrDefault();
                PanelDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Panel).Select(e => e.JobId).ToList();
            }

            var InvertDetails = new List<int?>();
            if (input.InvertModel != null)
            {
                //var Inverter = inverter_list.Where(e => (e.ProductTypeId == 2 || e.ProductTypeId == 5) && e.Model.Contains(input.InvertModel)).Select(e => e.Id).FirstOrDefault();
                var Inverter = inverter_list.Where(e => e.ProductTypeId == 2 && e.Model.Contains(input.InvertModel)).Select(e => e.Id).FirstOrDefault();
                InvertDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Inverter).Select(e => e.JobId).ToList();
            }

            var batteryDetails = new List<int?>();
            if (input.BatteryModel != null)
            {
                var Battery = Battery_list.Where(e => e.ProductTypeId == 5 && e.Model.Contains(input.BatteryModel)).Select(e => e.Id).FirstOrDefault();
                batteryDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Battery).Select(e => e.JobId).ToList();
            }

            //IQueryable<int> FilterdCancelJob;
            var FilterdCancelJob = new List<int>(); ;
            if (input.JobDateFilter == "CancelDate" && input.StartDate != null && input.EndDate != null)
            {
                FilterdCancelJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Cancel") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "CancelDate" && SDate != null && EDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date && x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            }
            else if (input.JobDateFilter == "CancelDate" && input.StartDate != null)
            {
                FilterdCancelJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Cancel") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "CancelDate" && SDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            }
            else if (input.JobDateFilter == "CancelDate" && input.EndDate != null)
            {
                FilterdCancelJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Cancel") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "CancelDate" && EDate != null, x => x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            }

            //IQueryable<int> FilterdHoldJob;
            var FilterdHoldJob = new List<int>();
            if (input.JobDateFilter == "HoldDate" && input.StartDate != null && input.EndDate != null)
            {
                FilterdHoldJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Hold") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "HoldDate" && SDate != null && EDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date && x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            }
            else if (input.JobDateFilter == "HoldDate" && input.StartDate != null)
            {
                FilterdHoldJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Hold") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "HoldDate" && SDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            }
            else if (input.JobDateFilter == "HoldDate" && input.EndDate != null)
            {
                FilterdHoldJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Hold") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "HoldDate" && SDate != null, x => x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            }

            //var filteredJobs = _jobProductItemRepository.GetAll()
            //            .Include(e => e.JobFk)
            //            .Include(e => e.JobFk.JobStatusFk)
            //            .Include(e => e.JobFk.RoofTypeFk)
            //            .Include(e => e.JobFk.RoofAngleFk)
            //            .Include(e => e.JobFk.ElecDistributorFk)
            //            .Include(e => e.JobFk.LeadFk)
            //            .Include(e => e.JobFk.ElecRetailerFk)
            //            .Include(e => e.JobFk.PaymentOptionFk)
            //            .Include(e => e.JobFk.DepositOptionFk)
            //            .Include(e => e.JobFk.PromotionOfferFk)
            //            .Include(e => e.JobFk.HouseTypeFk)
            //            .Include(e => e.JobFk.FinanceOptionFk)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFk.JobNumber.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Mobile.Contains(input.Filter))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.JobFk.Suburb == input.SuburbFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.JobFk.State == input.StateFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.JobFk.Address.Contains(input.AddressFilter))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobFk.JobTypeFk != null && e.JobFk.JobTypeFk.Name == input.JobTypeNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobFk.JobStatusFk != null && e.JobFk.JobStatusFk.Name == input.JobStatusNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.RoofTypeNameFilter), e => e.JobFk.RoofTypeFk != null && e.JobFk.RoofTypeFk.Name == input.RoofTypeNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.RoofAngleNameFilter), e => e.JobFk.RoofAngleFk != null && e.JobFk.RoofAngleFk.Name == input.RoofAngleNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.ElecDistributorNameFilter), e => e.JobFk.ElecDistributorFk != null && e.JobFk.ElecDistributorFk.Name == input.ElecDistributorNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.JobFk.LeadFk != null && e.JobFk.LeadFk.CompanyName == input.LeadCompanyNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.ElecRetailerNameFilter), e => e.JobFk.ElecRetailerFk != null && e.JobFk.ElecRetailerFk.Name == input.ElecRetailerNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentOptionNameFilter), e => e.JobFk.PaymentOptionFk != null && e.JobFk.PaymentOptionFk.Name == input.PaymentOptionNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.DepositOptionNameFilter), e => e.JobFk.DepositOptionFk != null && e.JobFk.DepositOptionFk.Name == input.DepositOptionNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionOfferNameFilter), e => e.JobFk.PromotionOfferFk != null && e.JobFk.PromotionOfferFk.Name == input.PromotionOfferNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.HouseTypeNameFilter), e => e.JobFk.HouseTypeFk != null && e.JobFk.HouseTypeFk.Name == input.HouseTypeNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.JobFk.FinanceOptionFk != null && e.JobFk.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.JobFk.PostalCode, input.PostalCodeFrom) >= 0)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.JobFk.PostalCode, input.PostalCodeTo) <= 0)
            //            .WhereIf(input.paymentid != 0, e => e.JobFk.PaymentOptionId == input.paymentid)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.JobFk.LeadFk.Area == input.AreaNameFilter)
            //            .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobFk.JobStatusId))

            //            .WhereIf(input.JobDateFilter == "CreationDate" && input.StartDate != null, e => e.JobFk.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
            //            .WhereIf(input.JobDateFilter == "CreationDate" && input.EndDate != null, e => e.JobFk.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

            //            .WhereIf(input.JobDateFilter == "FirstDeposite" && input.StartDate != null, e => e.JobFk.FirstDepositDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.JobDateFilter == "FirstDeposite" && input.EndDate != null, e => e.JobFk.FirstDepositDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.JobDateFilter == "DepositeReceived" && input.StartDate != null, e => e.JobFk.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.JobDateFilter == "DepositeReceived" && input.EndDate != null, e => e.JobFk.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.JobDateFilter == "Active" && input.StartDate != null, e => e.JobFk.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
            //            .WhereIf(input.JobDateFilter == "Active" && input.EndDate != null, e => e.JobFk.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

            //            .WhereIf(input.JobDateFilter == "NextFollowUp" && input.StartDate != null, e => e.JobFk.JobReminderTime.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.JobDateFilter == "NextFollowUp" && input.EndDate != null, e => e.JobFk.JobReminderTime.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.JobDateFilter == "InstallBook" && input.StartDate != null, e => e.JobFk.InstallationDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
            //            .WhereIf(input.JobDateFilter == "InstallBook" && input.EndDate != null, e => e.JobFk.InstallationDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

            //            .WhereIf(input.JobDateFilter == "InstallComplete" && input.StartDate != null, e => e.JobFk.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
            //            .WhereIf(input.JobDateFilter == "InstallComplete" && input.EndDate != null, e => e.JobFk.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

            //            .WhereIf(input.JobDateFilter == "CancelDate", e => FilterdCancelJob.Contains((int)e.JobFk.LeadId))
            //            .WhereIf(input.JobDateFilter == "HoldDate", e => FilterdHoldJob.Contains((int)e.JobFk.LeadId))

            //            .WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => e.JobFk.JobStatusId != 1)
            //            .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
            //            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
            //            .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
            //            .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesRepId)
            //            .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesManagerId)

            //            .WhereIf(!string.IsNullOrWhiteSpace(input.PanelModel), e => PanelDetails.Contains(e.JobFk.Id))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.JobFk.Id))

            //            .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.JobFk.LeadFk.AssignToUserID))
            //            .WhereIf(input.Solarvicstatus != 0, e => e.JobFk.SolarRebateStatus == input.Solarvicstatus)
            //            .WhereIf(input.Readytoactive != null && input.Readytoactive == 1, e => e.JobFk.IsActive == true && (e.JobFk.JobStatusId == 1 || e.JobFk.JobStatusId == 4))
            //            //.WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemEndRange != null && input.SystemEndRange != 0, e => e.JobFk.SystemCapacity >= input.SystemStrtRange && e.JobFk.SystemCapacity <= input.SystemEndRange)
            //            .WhereIf(input.SystemStrtRange != null && input.SystemStrtRange != 0 && input.SystemEndRange == null && input.SystemEndRange == 0, e => e.JobFk.SystemCapacity >= input.SystemStrtRange)
            //            .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemStrtRange == null && input.SystemStrtRange == 0, e => e.JobFk.SystemCapacity <= input.SystemEndRange)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.JobFk.Id))
            //            .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.JobFk.LeadFk.LeadSourceId))

            //            .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 1))
            //            .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5))

            //            .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 1))
            //            .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

            //            .WhereIf(input.BatteryFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

            //            .WhereIf(input.BatteryFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

            //            .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit).OrderByDescending(e => e.JobId).OrderBy(r => r.Id);

            input.JobTypeNameListFilter = input.JobTypeNameListFilter != null ? input.JobTypeNameListFilter : new List<string>();

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.RoofTypeFk)
                        .Include(e => e.RoofAngleFk)
                        .Include(e => e.ElecDistributorFk)
                        .Include(e => e.LeadFk)
                        .Include(e => e.ElecRetailerFk)
                        .Include(e => e.PaymentOptionFk)
                        .Include(e => e.DepositOptionFk)
                        .Include(e => e.PromotionOfferFk)
                        .Include(e => e.HouseTypeFk)
                        .Include(e => e.FinanceOptionFk)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
                        .WhereIf(input.JobTypeNameListFilter.Count() > 0, e => input.JobTypeNameListFilter.Contains(e.JobTypeFk.Name))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofTypeNameFilter), e => e.RoofTypeFk != null && e.RoofTypeFk.Name == input.RoofTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofAngleNameFilter), e => e.RoofAngleFk != null && e.RoofAngleFk.Name == input.RoofAngleNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecDistributorNameFilter), e => e.ElecDistributorFk != null && e.ElecDistributorFk.Name == input.ElecDistributorNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecRetailerNameFilter), e => e.ElecRetailerFk != null && e.ElecRetailerFk.Name == input.ElecRetailerNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentOptionNameFilter), e => e.PaymentOptionFk != null && e.PaymentOptionFk.Name == input.PaymentOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DepositOptionNameFilter), e => e.DepositOptionFk != null && e.DepositOptionFk.Name == input.DepositOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionOfferNameFilter), e => e.PromotionOfferFk != null && e.PromotionOfferFk.Name == input.PromotionOfferNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.HouseTypeNameFilter), e => e.HouseTypeFk != null && e.HouseTypeFk.Name == input.HouseTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.PostalCode, input.PostalCodeFrom) >= 0)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.PostalCode, input.PostalCodeTo) <= 0)
                        .WhereIf(input.paymentid != 0, e => e.PaymentOptionId == input.paymentid)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
                        .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))

                        .WhereIf(input.JobDateFilter == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "FirstDeposite" && input.StartDate != null, e => e.FirstDepositDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "FirstDeposite" && input.EndDate != null, e => e.FirstDepositDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "Active" && input.StartDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "Active" && input.EndDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "NextFollowUp" && input.StartDate != null, e => e.JobReminderTime.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "NextFollowUp" && input.EndDate != null, e => e.JobReminderTime.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "InstallBook" && input.StartDate != null, e => e.InstallationDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallBook" && input.EndDate != null, e => e.InstallationDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "InstallComplete" && input.StartDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallComplete" && input.EndDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "CancelDate", e => FilterdCancelJob.Contains((int)e.LeadId))
                        .WhereIf(input.JobDateFilter == "HoldDate", e => FilterdHoldJob.Contains((int)e.LeadId))

                        .WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => e.JobStatusId != 1)
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)

                        .WhereIf(!string.IsNullOrWhiteSpace(input.PanelModel), e => PanelDetails.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.BatteryModel), e => batteryDetails.Contains(e.Id))

                        .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(input.Solarvicstatus != 0, e => e.SolarRebateStatus == input.Solarvicstatus)
                        .WhereIf(input.Readytoactive != null && input.Readytoactive == 1, e => e.IsActive == true && (e.JobStatusId == 1 || e.JobStatusId == 4))
                        //.WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemEndRange != null && input.SystemEndRange != 0, e => e.JobFk.SystemCapacity >= input.SystemStrtRange && e.JobFk.SystemCapacity <= input.SystemEndRange)
                        .WhereIf(input.SystemStrtRange != null && input.SystemStrtRange != 0 && input.SystemEndRange == null && input.SystemEndRange == 0, e => e.SystemCapacity >= input.SystemStrtRange)
                        .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemStrtRange == null && input.SystemStrtRange == 0, e => e.SystemCapacity <= input.SystemEndRange)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadFk.LeadSourceId))

                        .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5))

                        .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                        .WhereIf(input.BatteryFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                        .WhereIf(input.BatteryFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                        .WhereIf(input.PreviousJobStatusId != null && input.PreviousJobStatusId.Count() > 0, e => e.JobStatusId == 3 && input.PreviousJobStatusId.Contains(_previousJobStatusRepository.GetAll().Where(pr => pr.JobId == e.Id).OrderByDescending(r => r.Id).FirstOrDefault().PreviousId))

                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber.Contains(input.Filter))
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile.Contains(input.Filter))
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName.Contains(input.Filter))

                        .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit).OrderByDescending(e => e.Id);

            //var jobProductItems = _jobPro 

            var jobs = from o in filteredJobs

                           //let JobProductItems = _jobProductItemRepository.GetAll().Where(e => e.JobId == o.Id)

                       select new GetJobProductDetailsForExcel()
                       {
                           Id = o.Id,
                           JobNumber = o.JobNumber,
                           JobStatus = o.JobStatusFk.Name,
                           Team = team_list.Where(e => e.UserId == o.LeadFk.AssignToUserID).Select(e => e.TeamFk.Name).FirstOrDefault(),
                           State = o.State,
                           SystemCapacity = o.SystemCapacity,
                           Amount = o.TotalCost,
                           FinanceType = o.PaymentOptionFk.Name,
                           Region = o.LeadFk.Area,
                           JobType = o.JobTypeFk.Name,
                           SalesRapName = User_List.Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                           //ProductItems = (from jp in JobProductItems
                           //                select new GetJobProductItemDetailsForExcel()
                           //                {
                           //                    Id = jp.ProductItemFk.ProductTypeId,
                           //                    ProductType = jp.ProductItemFk.ProductTypeFk.Name,
                           //                    ProductItem = jp.ProductItemFk.Name,
                           //                    ProductModel = jp.ProductItemFk.Model,
                           //                    ProductSize = jp.ProductItemFk.Size,
                           //                    Quantity = jp.Quantity
                           //                }).OrderBy(r => r.Id).ToList()
                         
                       };

            


            var jobProduct = jobs.ToList();
            for (int i = 0; i < jobProduct.Count; i++)
            {
                if (_jobProductItemRepository.GetAll().Where(e => e.JobId == jobProduct[i].Id).Any())
                {
                    jobProduct[i].ProductItems = (from jp in _jobProductItemRepository.GetAll().Where(e => e.JobId == jobProduct[i].Id)
                                                  select new GetJobProductItemDetailsForExcel()
                                                  {
                                                      Id = jp.ProductItemFk.ProductTypeId,
                                                      ProductType = jp.ProductItemFk.ProductTypeFk.Name,
                                                      ProductItem = jp.ProductItemFk.Name,
                                                      ProductModel = jp.ProductItemFk.Model,
                                                      ProductSize = jp.ProductItemFk.Size,
                                                      Quantity = jp.Quantity
                                                  }).OrderBy(r => r.Id).ToList();
                }
            }
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _leadsExcelExporter.JobGridProductDetailsExport(jobProduct, "JobGridProductDetails.xlsx");
            }
            else
            {
                return _leadsExcelExporter.JobGridProductDetailsExport(jobProduct, "JobGridProductDetails.csv");
                //return _leadsExcelExporter.JobGridExportToCsvFile(jobgridrListDtos);
            }
        }

        public async Task<FileDto> GetJobGirdTrackerProductItemToExcel(GetAllJobsForExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var team_list = _userTeamRepository.GetAll();

            var User_List = _userRepository.GetAll();
            var User = User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = team_list.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }

            //var ProductItem = _productItemRepository.GetAll();
            //var panel_list = ProductItem.Where(e => e.ProductTypeId == 1);
            //var inverter_list = ProductItem.Where(e => (e.ProductTypeId == 2 || e.ProductTypeId == 5));

            //var PanelDetails = new List<int?>();
            //if (input.PanelModel != null)
            //{
            //    var Panel = panel_list.Where(e => e.ProductTypeId == 1 && e.Model.Contains(input.PanelModel)).Select(e => e.Id).FirstOrDefault();
            //    PanelDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Panel).Select(e => e.JobId).ToList();
            //}

            //var InvertDetails = new List<int?>();
            //if (input.InvertModel != null)
            //{
            //    var Inverter = inverter_list.Where(e => (e.ProductTypeId == 2 || e.ProductTypeId == 5) && e.Model.Contains(input.InvertModel)).Select(e => e.Id).FirstOrDefault();
            //    InvertDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Inverter).Select(e => e.JobId).ToList();
            //}

            //IQueryable<int> FilterdCancelJob;
            var FilterdCancelJob = new List<int>(); ;
            if (input.JobDateFilter == "CancelDate" && input.StartDate != null && input.EndDate != null)
            {
                FilterdCancelJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Cancel") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "CancelDate" && SDate != null && EDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date && x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            }
            else if (input.JobDateFilter == "CancelDate" && input.StartDate != null)
            {
                FilterdCancelJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Cancel") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "CancelDate" && SDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            }
            else if (input.JobDateFilter == "CancelDate" && input.EndDate != null)
            {
                FilterdCancelJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Cancel") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "CancelDate" && EDate != null, x => x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            }

            //IQueryable<int> FilterdHoldJob;
            var FilterdHoldJob = new List<int>();
            if (input.JobDateFilter == "HoldDate" && input.StartDate != null && input.EndDate != null)
            {
                FilterdHoldJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Hold") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "HoldDate" && SDate != null && EDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date && x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            }
            else if (input.JobDateFilter == "HoldDate" && input.StartDate != null)
            {
                FilterdHoldJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Hold") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "HoldDate" && SDate != null, x => x.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            }
            else if (input.JobDateFilter == "HoldDate" && input.EndDate != null)
            {
                FilterdHoldJob = _leadactivityRepository.GetAll().Where(x => x.ActionId == 14 && x.ActionNote.Contains("Hold") && x.LeadFk.OrganizationId == input.OrganizationUnit)
                    .WhereIf(input.JobDateFilter == "HoldDate" && SDate != null, x => x.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date).Select(x => new { x.LeadId, x.CreationTime, x.Id }).OrderByDescending(x => x.Id).Select(x => x.LeadId).ToList();
            }

            var filteredJobs = _jobProductItemRepository.GetAll()
                        .Include(e => e.JobFk)
                        .Include(e => e.JobFk.JobStatusFk)
                        .Include(e => e.JobFk.RoofTypeFk)
                        .Include(e => e.JobFk.RoofAngleFk)
                        .Include(e => e.JobFk.ElecDistributorFk)
                        .Include(e => e.JobFk.LeadFk)
                        .Include(e => e.JobFk.ElecRetailerFk)
                        .Include(e => e.JobFk.PaymentOptionFk)
                        .Include(e => e.JobFk.DepositOptionFk)
                        .Include(e => e.JobFk.PromotionOfferFk)
                        .Include(e => e.JobFk.HouseTypeFk)
                        .Include(e => e.JobFk.FinanceOptionFk)
                        .Include(e => e.ProductItemFk)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFk.JobNumber.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Mobile.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.JobFk.Suburb == input.SuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.JobFk.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.JobFk.Address.Contains(input.AddressFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobFk.JobTypeFk != null && e.JobFk.JobTypeFk.Name == input.JobTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobFk.JobStatusFk != null && e.JobFk.JobStatusFk.Name == input.JobStatusNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofTypeNameFilter), e => e.JobFk.RoofTypeFk != null && e.JobFk.RoofTypeFk.Name == input.RoofTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofAngleNameFilter), e => e.JobFk.RoofAngleFk != null && e.JobFk.RoofAngleFk.Name == input.RoofAngleNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecDistributorNameFilter), e => e.JobFk.ElecDistributorFk != null && e.JobFk.ElecDistributorFk.Name == input.ElecDistributorNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.JobFk.LeadFk != null && e.JobFk.LeadFk.CompanyName == input.LeadCompanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecRetailerNameFilter), e => e.JobFk.ElecRetailerFk != null && e.JobFk.ElecRetailerFk.Name == input.ElecRetailerNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentOptionNameFilter), e => e.JobFk.PaymentOptionFk != null && e.JobFk.PaymentOptionFk.Name == input.PaymentOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DepositOptionNameFilter), e => e.JobFk.DepositOptionFk != null && e.JobFk.DepositOptionFk.Name == input.DepositOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionOfferNameFilter), e => e.JobFk.PromotionOfferFk != null && e.JobFk.PromotionOfferFk.Name == input.PromotionOfferNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.HouseTypeNameFilter), e => e.JobFk.HouseTypeFk != null && e.JobFk.HouseTypeFk.Name == input.HouseTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.JobFk.FinanceOptionFk != null && e.JobFk.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.JobFk.PostalCode, input.PostalCodeFrom) >= 0)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.JobFk.PostalCode, input.PostalCodeTo) <= 0)
                        .WhereIf(input.paymentid != 0, e => e.JobFk.PaymentOptionId == input.paymentid)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.JobFk.LeadFk.Area == input.AreaNameFilter)
                        .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobFk.JobStatusId))

                        .WhereIf(input.JobDateFilter == "CreationDate" && input.StartDate != null, e => e.JobFk.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "CreationDate" && input.EndDate != null, e => e.JobFk.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "FirstDeposite" && input.StartDate != null, e => e.JobFk.FirstDepositDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "FirstDeposite" && input.EndDate != null, e => e.JobFk.FirstDepositDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "DepositeReceived" && input.StartDate != null, e => e.JobFk.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "DepositeReceived" && input.EndDate != null, e => e.JobFk.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "Active" && input.StartDate != null, e => e.JobFk.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "Active" && input.EndDate != null, e => e.JobFk.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "NextFollowUp" && input.StartDate != null, e => e.JobFk.JobReminderTime.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "NextFollowUp" && input.EndDate != null, e => e.JobFk.JobReminderTime.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "InstallBook" && input.StartDate != null, e => e.JobFk.InstallationDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallBook" && input.EndDate != null, e => e.JobFk.InstallationDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "InstallComplete" && input.StartDate != null, e => e.JobFk.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallComplete" && input.EndDate != null, e => e.JobFk.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "CancelDate", e => FilterdCancelJob.Contains((int)e.JobFk.LeadId))
                        .WhereIf(input.JobDateFilter == "HoldDate", e => FilterdHoldJob.Contains((int)e.JobFk.LeadId))

                        .WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => e.JobFk.JobStatusId != 1)
                        .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.SalesManagerId)

                        //.WhereIf(!string.IsNullOrWhiteSpace(input.PanelModel), e => PanelDetails.Contains(e.Id))
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))

                        .WhereIf(!string.IsNullOrWhiteSpace(input.PanelModel), e => e.ProductItemFk.Model.Contains(input.PanelModel))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => e.ProductItemFk.Model.Contains(input.PanelModel))

                        .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.JobFk.LeadFk.AssignToUserID))
                        .WhereIf(input.Solarvicstatus != 0, e => e.JobFk.SolarRebateStatus == input.Solarvicstatus)
                        .WhereIf(input.Readytoactive != null && input.Readytoactive == 1, e => e.JobFk.IsActive == true && (e.JobFk.JobStatusId == 1 || e.JobFk.JobStatusId == 4))
                        //.WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemEndRange != null && input.SystemEndRange != 0, e => e.JobFk.SystemCapacity >= input.SystemStrtRange && e.JobFk.SystemCapacity <= input.SystemEndRange)
                        .WhereIf(input.SystemStrtRange != null && input.SystemStrtRange != 0 && input.SystemEndRange == null && input.SystemEndRange == 0, e => e.JobFk.SystemCapacity >= input.SystemStrtRange)
                        .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemStrtRange == null && input.SystemStrtRange == 0, e => e.JobFk.SystemCapacity <= input.SystemEndRange)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.JobFk.LeadFk.LeadSourceId))

                        .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobFk.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobFk.Id && j.ProductItemFk.ProductTypeId == 5))

                        .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobFk.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobFk.Id && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                        .WhereIf(input.BatteryFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobFk.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobFk.Id && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                        .WhereIf(input.BatteryFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobFk.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobFk.Id && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                        .WhereIf(input.PreviousJobStatusId != null && input.PreviousJobStatusId.Count() > 0, e => e.JobFk.JobStatusId == 3 && input.PreviousJobStatusId.Contains(_previousJobStatusRepository.GetAll().Where(pr => pr.JobId == e.JobFk.Id).OrderByDescending(r => r.Id).FirstOrDefault().PreviousId))

                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.JobNumber.Contains(input.Filter))
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Mobile.Contains(input.Filter))
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName.Contains(input.Filter))

                        .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit).OrderByDescending(e => e.JobFk.Id);

            //var jobProductItems = _jobPro 

            var jobs = from o in filteredJobs

                       select new GetJobProductItemsForExcel()
                       {
                           Id = o.Id,
                           JobNumber = o.JobFk.JobNumber,
                           ProductType = o.ProductItemFk.ProductTypeFk.Name,
                           ProductItem = o.ProductItemFk.Name,
                           ProductModel = o.ProductItemFk.Model,
                           ProductSize = o.ProductItemFk.Size,
                           Quantity = o.Quantity,
                           State = o.JobFk.LeadFk.State,
                           Teams = team_list.Where(e => e.UserId == o.JobFk.LeadFk.AssignToUserID).Select(e => e.TeamFk.Name).FirstOrDefault()
                       };

            var jobProduct = await jobs.ToListAsync();
            if (input.excelorcsv == 1) 
            {
                return _leadsExcelExporter.JobGridProductItemExport(jobProduct, "JobGridProductItemDetails.xlsx");
            }
            else
            {
                return _leadsExcelExporter.JobGridProductItemExport(jobProduct, "JobGridProductItemDetails.csv");
            }
        }

        /// <summary>
        /// Get Data For Edit Job From Lead Id
        /// </summary>
        /// <param name="input">Lead Id</param>
        /// <returns></returns>
        public async Task<GetJobForEditOutput> GetJobByLeadId(int LeadId)
        {
            var output = new GetJobForEditOutput();
            try
            {
                var job = await _jobRepository.GetAll().Where(x => x.LeadId == LeadId).FirstOrDefaultAsync();
                output = new GetJobForEditOutput { Job = ObjectMapper.Map<CreateOrEditJobDto>(job) };
                if (output.Job == null)
                {
                    return output;
                }
                if (job.IsBroadbandWifiConnection == null)
                {
                    output.Job.IsBroadbandWifiConnection = null; 
                }
                output.Job.BlackOutProtection = string.IsNullOrEmpty(output.Job.BlackOutProtection) ? "" : output.Job.BlackOutProtection;
               
                decimal paidAmount = job.PaidAmmount ?? 0; 
                decimal totalAmount = await _jobInstallerInvoiceRepository
                    .GetAll()
                    .Where(e => e.JobId == job.Id && e.Installation_Maintenance_Inspection == "Inspection")
                    .SumAsync(e => e.Amount ?? 0); 

                output.ExtraCostValue = 100 + paidAmount + totalAmount;
                if (!string.IsNullOrWhiteSpace(output.Job.Suburb))
                {
                    string[] splitPipeValue = output.Job.Suburb.Split('|');
                    if (splitPipeValue.Length > 0)
                    {
                        output.Job.Suburb = splitPipeValue[0].ToString();
                    }
                    else
                    {
                        output.Job.Suburb = output.Job.Suburb;
                    }
                }
                if(!string.IsNullOrEmpty(job.State))
                {
                   output.Job.State = job.State;
                    var state = _StateRepositery.GetAll().Where(e => e.Name == job.State).FirstOrDefault();
                    output.IsStateBatteryRebate = state.IsActiveBatteryRebate;
                    output.Job.BatteryBessState = state.BatteryBess;
                    output.Job.BatteryBessPerPriceState = state.BatteryRebate;

                    if (!(job.BatteryRebate > 0) && output.IsStateBatteryRebate)
                    {
                        output.Job.BatteryBessPerPrice = state.BatteryRebate;
                        output.Job.IsBatteryRebate = state.IsActiveBatteryRebate;
                    }
                }
                if (output.Job.JobTypeId != null)
                {
                    var _lookupJobType = await _lookup_jobTypeRepository.FirstOrDefaultAsync((int)output.Job.JobTypeId);
                    output.JobTypeName = _lookupJobType?.Name?.ToString();
                }

                if (output.Job.JobStatusId != null)
                {
                    var _lookupJobStatus = await _lookup_jobStatusRepository.FirstOrDefaultAsync((int)output.Job.JobStatusId);
                    output.JobStatusName = _lookupJobStatus?.Name?.ToString();
                }

                if (output.Job.RoofTypeId != null)
                {
                    var _lookupRoofType = await _lookup_roofTypeRepository.FirstOrDefaultAsync((int)output.Job.RoofTypeId);
                    output.RoofTypeName = _lookupRoofType?.Name?.ToString();
                }

                if (output.Job.RoofAngleId != null)
                {
                    var _lookupRoofAngle = await _lookup_roofAngleRepository.FirstOrDefaultAsync((int)output.Job.RoofAngleId);
                    output.RoofAngleName = _lookupRoofAngle?.Name?.ToString();
                }

                if (output.Job.ElecDistributorId != null)
                {
                    var _lookupElecDistributor = await _lookup_elecDistributorRepository.FirstOrDefaultAsync((int)output.Job.ElecDistributorId);
                    output.ElecDistributorName = _lookupElecDistributor?.Name?.ToString();
                }

                if (output.Job.LeadId != null)
                {
                    var _lookupLead = await _lookup_leadRepository.FirstOrDefaultAsync((int)output.Job.LeadId);
                    output.LeadCompanyName = _lookupLead?.CompanyName?.ToString();
                }

                if (output.Job.ElecRetailerId != null)
                {
                    var _lookupElecRetailer = await _lookup_elecRetailerRepository.FirstOrDefaultAsync((int)output.Job.ElecRetailerId);
                    output.ElecRetailerName = _lookupElecRetailer?.Name?.ToString();
                }

                if (output.Job.PaymentOptionId != null)
                {
                    var _lookupPaymentOption = await _lookup_paymentOptionRepository.FirstOrDefaultAsync((int)output.Job.PaymentOptionId);
                    output.PaymentOptionName = _lookupPaymentOption?.Name?.ToString();
                }

                if (output.Job.DepositOptionId != null)
                {
                    var _lookupDepositOption = await _lookup_depositOptionRepository.FirstOrDefaultAsync((int)output.Job.DepositOptionId);
                    output.DepositOptionName = _lookupDepositOption?.Name?.ToString();
                }

                if (output.Job.MeterUpgradeId != null)
                {
                    var _lookupMeterUpgrade = await _lookup_meterUpgradeRepository.FirstOrDefaultAsync((int)output.Job.MeterUpgradeId);
                    output.MeterUpgradeName = _lookupMeterUpgrade?.Name?.ToString();
                }

                if (output.Job.MeterPhaseId != null)
                {
                    var _lookupMeterPhase = await _lookup_meterPhaseRepository.FirstOrDefaultAsync((int)output.Job.MeterPhaseId);
                    output.MeterPhaseName = _lookupMeterPhase?.Name?.ToString();
                }

                if (output.Job.PromotionOfferId != null)
                {
                    var _lookupPromotionOffer = await _lookup_promotionOfferRepository.FirstOrDefaultAsync((int)output.Job.PromotionOfferId);
                    output.PromotionOfferName = _lookupPromotionOffer?.Name?.ToString();
                }
                
                if (!string.IsNullOrEmpty(job.LeadFk.longitude) && !string.IsNullOrEmpty(job.LeadFk.latitude))
                {
                    var location = await _warehouselocationRepository.GetAll().Where(e => e.state == job.LeadFk.State).FirstOrDefaultAsync();
                    if (location != null)
                    {
                        //Warehouse Lat lang
                        var warehouseLatitude = !string.IsNullOrEmpty(location.Latitude) ? Convert.ToDouble(location.Latitude) : 0;
                        var warehouseLongitude = !string.IsNullOrEmpty(location.Longitude) ? Convert.ToDouble(location.Longitude) : 0;

                        output.WarehouseDistance = GetDistance(warehouseLatitude, warehouseLongitude, Convert.ToDouble(job.LeadFk.latitude), Convert.ToDouble(job.LeadFk.longitude));
                    }
                }
                if(job.JobStatusId == 2 && job.FirstDepositDate != null)
                {
                    if (_permissionChecker.IsGranted(AppPermissions.Pages_Jobs_EditOnHold))
                    {
                        output.IsOnHoldEdit = true;

                    }
                    else
                    {
                        output.IsOnHoldEdit = false;
                    }
                }
                else
                {
                    output.IsOnHoldEdit = true;
                }

            }
            catch (Exception ex)
            { };
            return output;
        }

        /// <summary>
        /// Get Data For Edit Job
        /// </summary>
        /// <param name="input">Job Id</param>
        /// <returns></returns>
        //[AbpAuthorize(AppPermissions.Pages_Jobs_Edit, AppPermissions.Pages_ApplicationTracker_AddNotes, AppPermissions.Pages_FinanceTracker_AddDetails
        //    , AppPermissions.Pages_InvoiceTracker_Verify, AppPermissions.Pages_FreebiesTracker_AddTrackingNumber, AppPermissions.Pages_JobActiveTracker_AddDetails, AppPermissions.Pages_Gridconnectiontracker_AddMeterDetail, AppPermissions.Pages_RefundTracker_Refund, AppPermissions.Pages_STCTracker_Edit)]
        public async Task<GetJobForEditOutput> GetJobForEdit(EntityDto input)
        {
            var job = await _jobRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetJobForEditOutput { Job = ObjectMapper.Map<CreateOrEditJobDto>(job) };
            output.Job.ApplicationRefNo = output.Job.ApplicationRefNo;
            output.Job.ApprovalRef = output.Job.ApprovalRef;
            if (output.Job.DocumentsVeridiedBy == null)
            {
                output.Job.DocumentsVeridiedBy = (int)AbpSession.UserId;
            }

            output.TotalCost = output.Job.TotalCost;
            output.FinanceApplicationDates = output.Job.FinanceApplicationDate;
            output.DepPaid = _invoicePaymentRepository.GetAll().Where(e => e.JobId == job.Id).Select(e => e.InvoicePayTotal).Sum();
            output.BalanceDue = output.Job.TotalCost - output.DepPaid;
            output.AssignToUserID = _lookup_leadRepository.GetAll().Where(e => e.Id == job.LeadId).Select(e => e.AssignToUserID).FirstOrDefault();
            output.LeadCompanyName = _lookup_leadRepository.GetAll().Where(e => e.Id == job.LeadId).Select(e => e.CompanyName).FirstOrDefault();
            output.LeadEmail = _lookup_leadRepository.GetAll().Where(e => e.Id == job.LeadId).Select(e => e.Email).FirstOrDefault();
            var Quote = _quotationRepository.GetAll().Where(e => e.JobId == job.Id).Any();
            var Invoice = _invoicePaymentRepository.GetAll().Where(e => e.JobId == job.Id).Any();
            var ProductDetail = _jobProductItemRepository.GetAll().Where(e => e.JobId == job.Id).Any();
            if (output.Job.HouseTypeId != null && output.Job.ElecDistributorId != null && output.Job.RoofAngleId != null && output.Job.RoofTypeId != null && !string.IsNullOrEmpty(output.Job.InstallerNotes)
               && Quote && Invoice && ProductDetail)
            {
                output.displayifdp = true;
            }
            else
            {
                output.displayifdp = false;
            }

            if (output.Job.JobTypeId != null)
            {
                var _lookupJobType = await _lookup_jobTypeRepository.FirstOrDefaultAsync((int)output.Job.JobTypeId);
                output.JobTypeName = _lookupJobType?.Name?.ToString();
            }

            if (output.Job.JobStatusId != null)
            {
                var _lookupJobStatus = await _lookup_jobStatusRepository.FirstOrDefaultAsync((int)output.Job.JobStatusId);
                output.JobStatusName = _lookupJobStatus?.Name?.ToString();
            }

            if (output.Job.RoofTypeId != null)
            {
                var _lookupRoofType = await _lookup_roofTypeRepository.FirstOrDefaultAsync((int)output.Job.RoofTypeId);
                output.RoofTypeName = _lookupRoofType?.Name?.ToString();
            }

            if (output.Job.RoofAngleId != null)
            {
                var _lookupRoofAngle = await _lookup_roofAngleRepository.FirstOrDefaultAsync((int)output.Job.RoofAngleId);
                output.RoofAngleName = _lookupRoofAngle?.Name?.ToString();
            }

            if (output.Job.ElecDistributorId != null)
            {
                var _lookupElecDistributor = await _lookup_elecDistributorRepository.FirstOrDefaultAsync((int)output.Job.ElecDistributorId);
                output.ElecDistributorName = _lookupElecDistributor?.Name?.ToString();
            }

            if (output.Job.LeadId != null)
            {
                var _lookupLead = await _lookup_leadRepository.FirstOrDefaultAsync((int)output.Job.LeadId);
                output.LeadCompanyName = _lookupLead?.CompanyName?.ToString();
            }

            if (output.Job.ElecRetailerId != null)
            {
                var _lookupElecRetailer = await _lookup_elecRetailerRepository.FirstOrDefaultAsync((int)output.Job.ElecRetailerId);
                output.ElecRetailerName = _lookupElecRetailer?.Name?.ToString();
            }

            if (output.Job.PaymentOptionId != null)
            {
                var _lookupPaymentOption = await _lookup_paymentOptionRepository.FirstOrDefaultAsync((int)output.Job.PaymentOptionId);
                output.PaymentOptionName = _lookupPaymentOption?.Name?.ToString();
            }

            if (output.Job.DepositOptionId != null)
            {
                var _lookupDepositOption = await _lookup_depositOptionRepository.FirstOrDefaultAsync((int)output.Job.DepositOptionId);
                output.DepositOptionName = _lookupDepositOption?.Name?.ToString();
            }

            if (output.Job.MeterUpgradeId != null)
            {
                var _lookupMeterUpgrade = await _lookup_meterUpgradeRepository.FirstOrDefaultAsync((int)output.Job.MeterUpgradeId);
                output.MeterUpgradeName = _lookupMeterUpgrade?.Name?.ToString();
            }

            if (output.Job.MeterPhaseId != null)
            {
                var _lookupMeterPhase = await _lookup_meterPhaseRepository.FirstOrDefaultAsync((int)output.Job.MeterPhaseId);
                output.MeterPhaseName = _lookupMeterPhase?.Name?.ToString();
            }

            if (output.Job.PromotionOfferId != null)
            {
                var _lookupPromotionOffer = await _lookup_promotionOfferRepository.FirstOrDefaultAsync((int)output.Job.PromotionOfferId);
                output.PromotionOfferName = _lookupPromotionOffer?.Name?.ToString();
            }

            if (output.Job.HouseTypeId != null)
            {
                var _lookupHouseType = await _lookup_houseTypeRepository.FirstOrDefaultAsync((int)output.Job.HouseTypeId);
                output.HouseTypeName = _lookupHouseType?.Name?.ToString();
            }

            if (output.Job.FinanceOptionId != null)
            {
                var _lookupFinanceOption = await _lookup_financeOptionRepository.FirstOrDefaultAsync((int)output.Job.FinanceOptionId);
                output.FinanceOptionName = _lookupFinanceOption?.Name?.ToString();
            }
            if ( output.Job.Applicationfeespaid == 0)
            {
                
                output.Job.Applicationfeespaidstatus = "No";
            }
            else if (output.Job.Applicationfeespaid == 1)
            {

                output.Job.Applicationfeespaidstatus = "Yes";
            }
            output.RefferedJobId = output.Job.RefferedJobId;
            output.RefferedJobStatusId = output.Job.RefferedJobStatusId;
            output.AccountName = output.Job.AccountName; output.AccountNo = output.Job.AccountNo;
            output.BsbNo = output.Job.BsbNo;
            output.ReferralAmount = output.Job.ReferralAmount;
            output.refferedJobName = _jobRepository.GetAll().Where(e => e.LeadId == job.LeadFk.ReferralLeadId).AsNoTracking().Select(e => e.JobNumber).FirstOrDefault();
            output.ReferralPayDate = output.Job.ReferralPayDate;
            output.ReferralPayment = output.Job.ReferralPayment;
            output.BankReferenceNo = output.Job.BankReferenceNo;
            output.ReferralRemark = output.Job.ReferralRemark;
            
            return output;
        }


        /// <summary>
        /// Create Or Edit Job 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task CreateOrEdit(CreateOrEditJobDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        /// <summary>
        /// Create Job
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Jobs_Create)]
        protected virtual async Task Create(CreateOrEditJobDto input)
        {
            input.FinanceApplicationDate = _timeZoneConverter.Convert(input.FinanceApplicationDate, (int)AbpSession.TenantId);
            input.DocumentsVeridiedDate = _timeZoneConverter.Convert(input.DocumentsVeridiedDate, (int)AbpSession.TenantId);
            input.ReferralPayDate = _timeZoneConverter.Convert(input.ReferralPayDate, (int)AbpSession.TenantId);

            var Lead = _lookup_leadRepository.GetAll().Where(e => e.Id == input.LeadId).FirstOrDefault();
            Lead.LeadStatusId = 6;
            await _lookup_leadRepository.UpdateAsync(Lead);
            var ProjectCode = _extendOrganizationUnitRepository.GetAll().Where(e => e.Id == Lead.OrganizationId).Select(e => e.ProjectId).FirstOrDefault();
            var OrganizationCode = _organizationUnitRepository.GetAll().Where(e => e.Id == Lead.OrganizationId).Select(e => e.Code).FirstOrDefault();
            var PrevJobNumber = (from j in _jobRepository.GetAll()
                                 where (j.LeadFk.OrganizationId == Lead.OrganizationId)
                                 select new { j.JobNumber, j.Id })
                                .OrderByDescending(e => e.Id).FirstOrDefault();
            if (input.PaymentOptionId == 1)
            {
                input.FinancePurchaseNo = null;
                input.DepositOptionId = null;
                input.FinanceOptionId = null;
            }
            var JobNumber = ProjectCode;
            if (PrevJobNumber != null)
            {
                if (PrevJobNumber.JobNumber != null)
                {
                    //var JobNumberSplit = PrevJobNumber.JobNumber.Substring(PrevJobNumber.JobNumber.Length - 6); //PrevJobNumber.JobNumber.Split("_");

                    var JobNumberSplit = PrevJobNumber.JobNumber.Replace(OrganizationCode, "");
                    JobNumber = Convert.ToString(Convert.ToInt32(JobNumberSplit) + 1);
                }
            }
           

            //if (JobNumber.Length <= 6)
            //{
            //    JobNumber = JobNumber.ToString().PadLeft(6, '0');
            //}

            var job = ObjectMapper.Map<Job>(input);
            if (AbpSession.TenantId != null)
            {
                job.TenantId = (int)AbpSession.TenantId;
            }
            job.Address = input.UnitNo + " " + input.UnitType + " " + input.StreetNo + " " + input.StreetName + " " + input.StreetType;
            job.JobNumber = OrganizationCode + JobNumber;
            job.JobStatusId = 1;
            job.IsRefferalVerify = false;
            var jobid = await _jobRepository.InsertAndGetIdAsync(job);

            if (input.JobOldSystemDetail != null)
            {
                foreach (var jobolddetail in input.JobOldSystemDetail)
                {
                    var olddetail = ObjectMapper.Map<JobOldSystemDetail>(jobolddetail);
                    olddetail.JobId = jobid;
                    if (AbpSession.TenantId != null)
                    {
                        olddetail.TenantId = (int)AbpSession.TenantId;
                    }
                    _JobOldSysDetail.Insert(olddetail);
                }
            }

            if (input.JobPromotion != null)
            {
                foreach (var jobpromotion in input.JobPromotion)
                {
                    var jobpromo = ObjectMapper.Map<JobPromotion>(jobpromotion);
                    jobpromo.JobId = jobid;
                    if (AbpSession.TenantId != null)
                    {
                        jobpromo.TenantId = (int)AbpSession.TenantId;
                    }
                    _jobPromotionRepository.Insert(jobpromo);
                }
            }

            if (input.JobVariation != null)
            {
                foreach (var jobvariation in input.JobVariation)
                {
                    var jobvar = ObjectMapper.Map<JobVariation>(jobvariation);
                    jobvar.JobId = jobid;
                    if (AbpSession.TenantId != null)
                    {
                        jobvar.TenantId = (int)AbpSession.TenantId;
                    }
                    _jobVariationRepository.Insert(jobvar);
                }
            }


            PreviousJobStatus jobStstus = new PreviousJobStatus();
            jobStstus.JobId = job.Id;
            if (AbpSession.TenantId != null)
            {
                jobStstus.TenantId = (int)AbpSession.TenantId;
            }
            jobStstus.CurrentID = (int)job.JobStatusId;
            jobStstus.PreviousId = 0;
            await _previousJobStatusRepository.InsertAsync(jobStstus);

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 10;
            leadactivity.SectionId = input.SectionId;
            leadactivity.ActionNote = "Job Created";
            leadactivity.LeadId = Convert.ToInt32(input.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            //await _leadactivityRepository.InsertAsync(leadactivity);
            var jobactionid = _leadactivityRepository.InsertAndGetId(leadactivity);

            var list = new List<JobTrackerHistory>();
            JobTrackerHistory jobhistory = new JobTrackerHistory();
            if (AbpSession.TenantId != null)
            {
                jobhistory.TenantId = (int)AbpSession.TenantId;
            }
            jobhistory.FieldName = "JobType";
            jobhistory.PrevValue = "";
            jobhistory.CurValue = input.JobTypeId > 0 ? _lookup_jobTypeRepository.GetAll().Where(e => e.Id == input.JobTypeId).Select(e => e.Name).FirstOrDefault() :"";
            jobhistory.Action = "Add";
            jobhistory.LastmodifiedDateTime = DateTime.Now;
            jobhistory.JobIDId = job.Id;
            jobhistory.JobActionId = jobactionid;
            list.Add(jobhistory);


            jobhistory = new JobTrackerHistory();
            if (AbpSession.TenantId != null)
            {
                jobhistory.TenantId = (int)AbpSession.TenantId;
            }
            jobhistory.FieldName = "UnitNO";
            jobhistory.PrevValue = "";
            jobhistory.CurValue = input.UnitNo;
            jobhistory.Action = "Add";
            jobhistory.LastmodifiedDateTime = DateTime.Now;
            jobhistory.JobIDId = job.Id;
            jobhistory.JobActionId = jobactionid;
            list.Add(jobhistory);

            jobhistory = new JobTrackerHistory();
            if (AbpSession.TenantId != null)
            {
                jobhistory.TenantId = (int)AbpSession.TenantId;
            }
            jobhistory.FieldName = "UnitType";
            jobhistory.PrevValue = "";
            jobhistory.CurValue = input.UnitType;
            jobhistory.Action = "Add";
            jobhistory.LastmodifiedDateTime = DateTime.Now;
            jobhistory.JobIDId = job.Id;
            jobhistory.JobActionId = jobactionid;
            list.Add(jobhistory);

            jobhistory = new JobTrackerHistory();
            if (AbpSession.TenantId != null)
            {
                jobhistory.TenantId = (int)AbpSession.TenantId;
            }
            jobhistory.FieldName = "StreetNo";
            jobhistory.PrevValue = "";
            jobhistory.CurValue = input.StreetNo;
            jobhistory.Action = "Add";
            jobhistory.LastmodifiedDateTime = DateTime.Now;
            jobhistory.JobIDId = job.Id;
            jobhistory.JobActionId = jobactionid;
            list.Add(jobhistory);

            jobhistory = new JobTrackerHistory();
            if (AbpSession.TenantId != null)
            {
                jobhistory.TenantId = (int)AbpSession.TenantId;
            }
            jobhistory.FieldName = "StreetName";
            jobhistory.PrevValue = "";
            jobhistory.CurValue = input.StreetName;
            jobhistory.Action = "Add";
            jobhistory.LastmodifiedDateTime = DateTime.Now;
            jobhistory.JobIDId = job.Id;
            jobhistory.JobActionId = jobactionid;
            list.Add(jobhistory);

            jobhistory = new JobTrackerHistory();
            if (AbpSession.TenantId != null)
            {
                jobhistory.TenantId = (int)AbpSession.TenantId;
            }
            jobhistory.FieldName = "StreetType";
            jobhistory.PrevValue = "";
            jobhistory.CurValue = input.StreetType;
            jobhistory.Action = "Add";
            jobhistory.LastmodifiedDateTime = DateTime.Now;
            jobhistory.JobIDId = job.Id;
            jobhistory.JobActionId = jobactionid;
            list.Add(jobhistory);

            jobhistory = new JobTrackerHistory();
            if (AbpSession.TenantId != null)
            {
                jobhistory.TenantId = (int)AbpSession.TenantId;
            }
            jobhistory.FieldName = "Suburb";
            jobhistory.PrevValue = "";
            jobhistory.CurValue = input.Suburb;
            jobhistory.Action = "Add";
            jobhistory.LastmodifiedDateTime = DateTime.Now;
            jobhistory.JobIDId = job.Id;
            jobhistory.JobActionId = jobactionid;
            list.Add(jobhistory);

            jobhistory = new JobTrackerHistory();
            if (AbpSession.TenantId != null)
            {
                jobhistory.TenantId = (int)AbpSession.TenantId;
            }
            jobhistory.FieldName = "State";
            jobhistory.PrevValue = "";
            jobhistory.CurValue = input.State;
            jobhistory.Action = "Add";
            jobhistory.LastmodifiedDateTime = DateTime.Now;
            jobhistory.JobIDId = job.Id;
            jobhistory.JobActionId = jobactionid;
            list.Add(jobhistory);

            jobhistory = new JobTrackerHistory();
            if (AbpSession.TenantId != null)
            {
                jobhistory.TenantId = (int)AbpSession.TenantId;
            }
            jobhistory.FieldName = "PostalCode";
            jobhistory.PrevValue = "";
            jobhistory.CurValue = input.PostalCode;
            jobhistory.Action = "Add";
            jobhistory.LastmodifiedDateTime = DateTime.Now;
            jobhistory.JobIDId = job.Id;
            jobhistory.JobActionId = jobactionid;
            list.Add(jobhistory);

            await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            var log = new UserActivityLogDto();
            log.ActionId = 10;
            log.ActionNote = "Job Created : " + job.JobNumber;
            log.Section = input.Section;
            await _userActivityLogServiceProxy.Create(log);
        }

        /// <summary>
        /// Update Job
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Jobs_Edit, AppPermissions.Pages_ApplicationTracker_AddNotes, AppPermissions.Pages_FinanceTracker_AddDetails
            , AppPermissions.Pages_InvoiceTracker_Verify, AppPermissions.Pages_FreebiesTracker_AddTrackingNumber, AppPermissions.Pages_JobActiveTracker_AddDetails, AppPermissions.Pages_RefundTracker_Refund, AppPermissions.Pages_STCTracker_Edit)]
        protected virtual async Task Update(CreateOrEditJobDto input)
        {
            try
            {
                input.FinanceApplicationDate = _timeZoneConverter.Convert(input.FinanceApplicationDate, (int)AbpSession.TenantId);
                input.DocumentsVeridiedDate = _timeZoneConverter.Convert(input.DocumentsVeridiedDate, (int)AbpSession.TenantId);
                input.ReferralPayDate = _timeZoneConverter.Convert(input.ReferralPayDate, (int)AbpSession.TenantId);

                var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);

                input.FirstDepositDate = job.FirstDepositDate;
                input.DepositeRecceivedDate = job.DepositeRecceivedDate;
                input.ActiveDate = job.ActiveDate;
                input.JobStatusId = job.JobStatusId;

                if (input.refferedJobName != null)
                {
                    //int firstCommaIndex = input.refferedJobName.IndexOf('|');

                   // string firstPart = input.refferedJobName.Substring(0, firstCommaIndex);

                    var jbid = _jobRepository.GetAll().Where(e => e.JobNumber == input.refferedJobName).FirstOrDefault();

                    input.RefferedJobId = Convert.ToInt32(jbid.Id);
                    input.RefferedJobStatusId = jbid.JobStatusId;

                }
                if (input.ReferralPayDate != null)
                { input.ReferralPayment = true; }

                input.Address = input.UnitNo + " " + input.UnitType + " " + input.StreetNo + " " + input.StreetName + " " + input.StreetType;
                if (input.PaymentOptionId == 1)
                {
                    input.FinancePurchaseNo = null;
                    input.DepositOptionId = null;
                    input.FinanceOptionId = null;
                }
                job.JobHoldReason = input.JobHoldReason;
                job.JobHoldReasonId = input.JobHoldReasonId;

                var log = new UserActivityLogDto();
                log.ActionId = 11;
                log.ActionNote = "Job Updated : " + job.JobNumber;
                log.Section = input.Section;
                await _userActivityLogServiceProxy.Create(log);

                LeadActivityLog leadactivity = new LeadActivityLog();

                leadactivity.ActionId = 11;
                leadactivity.SectionId = input.SectionId != null ? input.SectionId : 0;
                leadactivity.ActionNote = "Job Updated";
                leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                var jobactionid = _leadactivityRepository.InsertAndGetId(leadactivity);

                var list = new List<JobTrackerHistory>();
                if (job.JobHoldReason != input.JobHoldReason)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "JobHoldReason";
                    jobhistory.PrevValue = job.JobHoldReason;
                    jobhistory.CurValue = input.JobHoldReason;
                    jobhistory.Action = "Job JobHoldReason";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.JobHoldReasonId != input.JobHoldReasonId)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "JobHoldReasonId";
                    jobhistory.PrevValue = _jobHoldReasonRepository.GetAll().Where(e => e.Id == job.JobHoldReasonId).Select(e => e.JobHoldReason).FirstOrDefault();
                    jobhistory.CurValue = _jobHoldReasonRepository.GetAll().Where(e => e.Id == input.JobHoldReasonId).Select(e => e.JobHoldReason).FirstOrDefault();
                    jobhistory.Action = "Job JobHoldReason";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.ReviewRating != input.ReviewRating)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "ReviewRating";
                    jobhistory.PrevValue = job.ReviewRating.ToString();
                    jobhistory.CurValue = input.ReviewRating.ToString();
                    jobhistory.Action = "Job ReviewRating";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                    job.Address = input.UnitNo + " " + input.UnitType + " " + input.StreetNo + " " + input.StreetName + " " + input.StreetType;
                }
                if (job.ReviewNotes != input.ReviewNotes)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "ReviewNotes";
                    jobhistory.PrevValue = job.ReviewNotes;
                    jobhistory.CurValue = input.ReviewNotes;
                    jobhistory.Action = "Job ReviewNotes";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                    job.Address = input.UnitNo + " " + input.UnitType + " " + input.StreetNo + " " + input.StreetName + " " + input.StreetType;
                }
                if (job.Address != input.Address)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Address";
                    jobhistory.PrevValue = job.Address;
                    jobhistory.CurValue = input.Address;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                    job.Address = input.UnitNo + " " + input.UnitType + " " + input.StreetNo + " " + input.StreetName + " " + input.StreetType;
                }
                if (job.ManualQuote != input.ManualQuote)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Manual Quote";
                    jobhistory.PrevValue = job.ManualQuote;
                    jobhistory.CurValue = input.ManualQuote;
                    jobhistory.Action = "Job Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.PriceType != input.PriceType)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Price Type";
                    jobhistory.PrevValue = job.PriceType;
                    jobhistory.CurValue = input.PriceType;
                    jobhistory.Action = "Job Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.UnitNo != input.UnitNo)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "UnitNo";
                    jobhistory.PrevValue = job.UnitNo;
                    jobhistory.CurValue = input.UnitNo;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.UnitType != input.UnitType)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "UnitType";
                    jobhistory.PrevValue = job.UnitType;
                    jobhistory.CurValue = input.UnitType;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.StreetNo != input.StreetNo)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "StreetNo";
                    jobhistory.PrevValue = job.StreetNo;
                    jobhistory.CurValue = input.StreetNo;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.StreetName != input.StreetName)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "StreetName";
                    jobhistory.PrevValue = job.StreetName;
                    jobhistory.CurValue = input.StreetName;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.StreetType != input.StreetType)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "StreetType";
                    jobhistory.PrevValue = job.StreetType;
                    jobhistory.CurValue = input.StreetType;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.Suburb != input.Suburb)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Suburb";
                    jobhistory.PrevValue = job.Suburb;
                    jobhistory.CurValue = input.Suburb;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.PostalCode != input.PostalCode)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "PostalCode";
                    jobhistory.PrevValue = job.PostalCode;
                    jobhistory.CurValue = input.PostalCode;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.State != input.State)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "State";
                    jobhistory.PrevValue = job.State;
                    jobhistory.CurValue = input.State;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.JobTypeId != input.JobTypeId)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Job Type";
                    jobhistory.PrevValue = _lookup_jobTypeRepository.GetAll().Where(e => e.Id == job.JobTypeId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.CurValue = _lookup_jobTypeRepository.GetAll().Where(e => e.Id == input.JobTypeId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.Action = "Job Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.OldSystemDetails != input.OldSystemDetails)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Old System Details";
                    jobhistory.PrevValue = job.OldSystemDetails;
                    jobhistory.CurValue = input.OldSystemDetails;
                    jobhistory.Action = "Notes Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.Note != input.Note)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Note";
                    jobhistory.PrevValue = job.Note;
                    jobhistory.CurValue = input.Note;
                    jobhistory.Action = "Notes Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.InstallerNotes != input.InstallerNotes)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "InstallerNotes";
                    jobhistory.PrevValue = job.InstallerNotes;
                    jobhistory.CurValue = input.InstallerNotes;
                    jobhistory.Action = "Notes Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.SystemCapacity != input.SystemCapacity)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "SystemCapacity";
                    jobhistory.PrevValue = job.SystemCapacity.ToString();
                    jobhistory.CurValue = input.SystemCapacity.ToString();
                    jobhistory.Action = "Rebate Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.STC != input.STC)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "STC";
                    jobhistory.PrevValue = job.STC.ToString();
                    jobhistory.CurValue = input.STC.ToString();
                    jobhistory.Action = "Rebate Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.STCPrice != input.STCPrice)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "STCPrice";
                    jobhistory.PrevValue = job.STCPrice.ToString();
                    jobhistory.CurValue = input.STCPrice.ToString();
                    jobhistory.Action = "Rebate Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.Rebate != input.Rebate)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Rebate";
                    jobhistory.PrevValue = job.Rebate.ToString();
                    jobhistory.CurValue = input.Rebate.ToString();
                    jobhistory.Action = "Rebate Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.RemoveOldSys != input.RemoveOldSys)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Remove Old System";
                    jobhistory.PrevValue = job.RemoveOldSys.ToString();
                    jobhistory.CurValue = input.RemoveOldSys.ToString();
                    jobhistory.Action = "Job Remove Old System Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.HouseTypeId != input.HouseTypeId)
                {
                    var HouseTypeList = _lookup_houseTypeRepository.GetAll();
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "House Type";
                    jobhistory.PrevValue = HouseTypeList.Where(e => e.Id == job.HouseTypeId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.CurValue = HouseTypeList.Where(e => e.Id == input.HouseTypeId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.Action = "Site Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.ElecDistributorId != input.ElecDistributorId)
                {
                    var ElecDistributorList = _lookup_elecDistributorRepository.GetAll();
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Elec Dist.";
                    jobhistory.PrevValue = ElecDistributorList.Where(e => e.Id == job.ElecDistributorId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.CurValue = ElecDistributorList.Where(e => e.Id == input.ElecDistributorId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.Action = "Site Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.NMINumber != input.NMINumber)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "NMI Number";
                    jobhistory.PrevValue = job.NMINumber;
                    jobhistory.CurValue = input.NMINumber;
                    jobhistory.Action = "Site Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.RegPlanNo != input.RegPlanNo)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Reg. Plan No.";
                    jobhistory.PrevValue = job.RegPlanNo;
                    jobhistory.CurValue = input.RegPlanNo;
                    jobhistory.Action = "Site Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.RoofAngleId != input.RoofAngleId)
                {
                    var RoofAngleList = _lookup_roofAngleRepository.GetAll();
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Roof Angle";
                    jobhistory.PrevValue = RoofAngleList.Where(e => e.Id == job.RoofAngleId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.CurValue = RoofAngleList.Where(e => e.Id == input.RoofAngleId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.Action = "Site Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.RoofTypeId != input.RoofTypeId)
                {
                    var RoofTypeList = _lookup_roofTypeRepository.GetAll();
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Roof Type";
                    jobhistory.PrevValue = RoofTypeList.Where(e => e.Id == job.RoofTypeId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.CurValue = RoofTypeList.Where(e => e.Id == input.RoofTypeId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.Action = "Site Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);

                    if (input.RoofTypeId == 5 || input.RoofTypeId == 7 || input.RoofTypeId == 9)
                    {
                        if (job.PanelOnFlat != input.PanelOnFlat)
                        {
                            JobTrackerHistory jobhistorypof = new JobTrackerHistory();
                            if (AbpSession.TenantId != null)
                            {
                                jobhistorypof.TenantId = (int)AbpSession.TenantId;
                            }
                            jobhistorypof.FieldName = "Panel On Flat";
                            jobhistorypof.PrevValue = job.PanelOnFlat.ToString();
                            jobhistorypof.CurValue = input.PanelOnFlat.ToString();
                            jobhistorypof.Action = "Site Detail Edit";
                            jobhistorypof.LastmodifiedDateTime = DateTime.Now;
                            jobhistorypof.JobIDId = job.Id;
                            jobhistorypof.JobActionId = jobactionid;
                            list.Add(jobhistorypof);
                        }
                        if (job.PanelOnPitched != input.PanelOnPitched)
                        {
                            JobTrackerHistory jobhistorypop = new JobTrackerHistory();
                            if (AbpSession.TenantId != null)
                            {
                                jobhistorypop.TenantId = (int)AbpSession.TenantId;
                            }
                            jobhistorypop.FieldName = "Panel On Pitchead";
                            jobhistorypop.PrevValue = job.PanelOnPitched.ToString();
                            jobhistorypop.CurValue = input.PanelOnPitched.ToString();
                            jobhistorypop.Action = "Site Detail Edit";
                            jobhistorypop.LastmodifiedDateTime = DateTime.Now;
                            jobhistorypop.JobIDId = job.Id;
                            jobhistorypop.JobActionId = jobactionid;
                            list.Add(jobhistorypop);
                        }
                    }
                    else
                    {
                        job.PanelOnFlat = null;
                        job.PanelOnPitched = null;
                    }
                }

                if (job.ElecRetailerId != input.ElecRetailerId)
                {
                    var ElecRetailerList = _lookup_elecRetailerRepository.GetAll();
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Elec Retailer";
                    jobhistory.PrevValue = ElecRetailerList.Where(e => e.Id == job.ElecRetailerId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.CurValue = ElecRetailerList.Where(e => e.Id == input.ElecRetailerId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.Action = "Site Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.MeterUpgradeId != input.MeterUpgradeId)
                {
                    var MeterUpgradeList = _lookup_meterUpgradeRepository.GetAll();
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Meter Upgrade";
                    jobhistory.PrevValue = MeterUpgradeList.Where(e => e.Id == job.MeterUpgradeId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.CurValue = MeterUpgradeList.Where(e => e.Id == input.MeterUpgradeId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.Action = "Site Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.LotNumber != input.LotNumber)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Lot No.";
                    jobhistory.PrevValue = job.LotNumber;
                    jobhistory.CurValue = input.LotNumber;
                    jobhistory.Action = "Site Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.ApplicationRefNo != input.ApplicationRefNo)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Application Ref No";
                    jobhistory.PrevValue = job.ApplicationRefNo;
                    jobhistory.CurValue = input.ApplicationRefNo;
                    jobhistory.Action = "Site Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.PeakMeterNo != input.PeakMeterNo)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Peak Meter No.";
                    jobhistory.PrevValue = job.PeakMeterNo;
                    jobhistory.CurValue = input.PeakMeterNo;
                    jobhistory.Action = "Meter Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.OffPeakMeter != input.OffPeakMeter)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Off Peak Meters";
                    jobhistory.PrevValue = job.OffPeakMeter;
                    jobhistory.CurValue = input.OffPeakMeter;
                    jobhistory.Action = "Meter Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.MeterPhaseId != input.MeterPhaseId)
                {
                    var MeterPhaseList = _lookup_meterPhaseRepository.GetAll();
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Meter Phase 1-2-3";
                    jobhistory.PrevValue = MeterPhaseList.Where(e => e.Id == job.MeterPhaseId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.CurValue = MeterPhaseList.Where(e => e.Id == input.MeterPhaseId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.Action = "Meter Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.EnoughMeterSpace != input.EnoughMeterSpace)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Enough Meter Space";
                    jobhistory.PrevValue = Convert.ToString(job.EnoughMeterSpace);
                    jobhistory.CurValue = Convert.ToString(input.EnoughMeterSpace);
                    jobhistory.Action = "Meter Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.IsSystemOffPeak != input.IsSystemOffPeak)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "In System Off Peak";
                    jobhistory.PrevValue = Convert.ToString(job.IsSystemOffPeak);
                    jobhistory.CurValue = Convert.ToString(input.IsSystemOffPeak);
                    jobhistory.Action = "Meter Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.PaymentOptionId != input.PaymentOptionId)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Payment Option";
                    jobhistory.PrevValue = _paymentOptionRepository.GetAll().Where(e => e.Id == job.PaymentOptionId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.CurValue = _paymentOptionRepository.GetAll().Where(e => e.Id == input.PaymentOptionId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.Action = "Payment Method Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);

                }
                if (job.FinanceOptionId != input.FinanceOptionId)
                {
                    JobTrackerHistory jobhistoryfid = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistoryfid.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistoryfid.FieldName = "Fin. Payment Type";
                    jobhistoryfid.PrevValue = _financeOptionRepository.GetAll().Where(e => e.Id == job.FinanceOptionId).Select(e => e.Name).FirstOrDefault();
                    jobhistoryfid.CurValue = _financeOptionRepository.GetAll().Where(e => e.Id == input.FinanceOptionId).Select(e => e.Name).FirstOrDefault();
                    jobhistoryfid.Action = "Payment Method Edit";
                    jobhistoryfid.LastmodifiedDateTime = DateTime.Now;
                    jobhistoryfid.JobIDId = job.Id;
                    jobhistoryfid.JobActionId = jobactionid;
                    list.Add(jobhistoryfid);
                }
                if (job.DepositOptionId != input.DepositOptionId)
                {
                    JobTrackerHistory jobhistorydid = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistorydid.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistorydid.FieldName = "Deposit Option";
                    jobhistorydid.PrevValue = _depositOptionRepository.GetAll().Where(e => e.Id == job.DepositOptionId).Select(e => e.Name).FirstOrDefault();
                    jobhistorydid.CurValue = _depositOptionRepository.GetAll().Where(e => e.Id == input.DepositOptionId).Select(e => e.Name).FirstOrDefault();
                    jobhistorydid.Action = "Payment Method Edit";
                    jobhistorydid.LastmodifiedDateTime = DateTime.Now;
                    jobhistorydid.JobIDId = job.Id;
                    jobhistorydid.JobActionId = jobactionid;
                    list.Add(jobhistorydid);
                }
                if (job.FinancePurchaseNo != input.FinancePurchaseNo)
                {
                    JobTrackerHistory jobhistoryfpn = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistoryfpn.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistoryfpn.FieldName = "Finance Purchase No";
                    jobhistoryfpn.PrevValue = job.FinancePurchaseNo;
                    jobhistoryfpn.CurValue = input.FinancePurchaseNo;
                    jobhistoryfpn.Action = "Payment Method Edit";
                    jobhistoryfpn.LastmodifiedDateTime = DateTime.Now;
                    jobhistoryfpn.JobIDId = job.Id;
                    jobhistoryfpn.JobActionId = jobactionid;
                    list.Add(jobhistoryfpn);
                }
                if (job.FinanceAmount != input.FinanceAmount)
                {
                    JobTrackerHistory jobhistoryfpn = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistoryfpn.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistoryfpn.FieldName = "Finance Amount";
                    jobhistoryfpn.PrevValue = job.FinanceAmount.ToString();
                    jobhistoryfpn.CurValue = input.FinanceAmount.ToString();
                    jobhistoryfpn.Action = "Finance Amount Edit";
                    jobhistoryfpn.LastmodifiedDateTime = DateTime.Now;
                    jobhistoryfpn.JobIDId = job.Id;
                    jobhistoryfpn.JobActionId = jobactionid;
                    list.Add(jobhistoryfpn);
                }
                if (job.FinanceDepositeAmount != input.FinanceDepositeAmount)
                {
                    JobTrackerHistory jobhistoryfpn = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistoryfpn.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistoryfpn.FieldName = "Finance Deposit Amount";
                    jobhistoryfpn.PrevValue = job.FinanceDepositeAmount.ToString();
                    jobhistoryfpn.CurValue = input.FinanceDepositeAmount.ToString();
                    jobhistoryfpn.Action = "Finance Deposit Amount Edit";
                    jobhistoryfpn.LastmodifiedDateTime = DateTime.Now;
                    jobhistoryfpn.JobIDId = job.Id;
                    jobhistoryfpn.JobActionId = jobactionid;
                    list.Add(jobhistoryfpn);
                }
                if (job.FinanceNetAmount != input.FinanceNetAmount)
                {
                    JobTrackerHistory jobhistoryfpn = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistoryfpn.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistoryfpn.FieldName = "Finance Net Amount";
                    jobhistoryfpn.PrevValue = job.FinanceNetAmount.ToString();
                    jobhistoryfpn.CurValue = input.FinanceNetAmount.ToString();
                    jobhistoryfpn.Action = "Finance Net Amount Edit";
                    jobhistoryfpn.LastmodifiedDateTime = DateTime.Now;
                    jobhistoryfpn.JobIDId = job.Id;
                    jobhistoryfpn.JobActionId = jobactionid;
                    list.Add(jobhistoryfpn);
                }
                if (job.FinanceApplicationDate != input.FinanceApplicationDate)
                {
                    JobTrackerHistory jobhistoryfpn = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistoryfpn.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistoryfpn.FieldName = "Finance ApplicationDate";
                    jobhistoryfpn.PrevValue = job.FinanceApplicationDate.ToString();
                    jobhistoryfpn.CurValue = input.FinanceApplicationDate.ToString();
                    jobhistoryfpn.Action = "Finance ApplicationDate Edit";
                    jobhistoryfpn.LastmodifiedDateTime = DateTime.Now;
                    jobhistoryfpn.JobIDId = job.Id;
                    jobhistoryfpn.JobActionId = jobactionid;
                    list.Add(jobhistoryfpn);
                }
                if (job.DocumentsVeridiedDate != input.DocumentsVeridiedDate)
                {
                    JobTrackerHistory jobhistoryfpn = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistoryfpn.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistoryfpn.FieldName = "Documents Veridied Date";
                    jobhistoryfpn.PrevValue = job.DocumentsVeridiedDate.ToString();
                    jobhistoryfpn.CurValue = input.DocumentsVeridiedDate.ToString();
                    jobhistoryfpn.Action = "Documents Veridied Date Edit";
                    jobhistoryfpn.LastmodifiedDateTime = DateTime.Now;
                    jobhistoryfpn.JobIDId = job.Id;
                    jobhistoryfpn.JobActionId = jobactionid;
                    list.Add(jobhistoryfpn);
                }
                if (job.CoolingoffPeriodEnd != input.CoolingoffPeriodEnd)
                {
                    JobTrackerHistory jobhistoryfpn = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistoryfpn.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistoryfpn.FieldName = "Coolingoff Period End";
                    jobhistoryfpn.PrevValue = job.CoolingoffPeriodEnd.ToString();
                    jobhistoryfpn.CurValue = input.CoolingoffPeriodEnd.ToString();
                    jobhistoryfpn.Action = "Coolingoff Period End Edit";
                    jobhistoryfpn.LastmodifiedDateTime = DateTime.Now;
                    jobhistoryfpn.JobIDId = job.Id;
                    jobhistoryfpn.JobActionId = jobactionid;
                    list.Add(jobhistoryfpn);
                }
                if (job.DocumentsVeridiedBy != input.DocumentsVeridiedBy)
                {
                    JobTrackerHistory jobhistoryfpn = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistoryfpn.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistoryfpn.FieldName = "Document Verified By";
                    jobhistoryfpn.PrevValue = job.DocumentsVeridiedBy.ToString();
                    jobhistoryfpn.CurValue = input.DocumentsVeridiedBy.ToString();
                    jobhistoryfpn.Action = "Document Verified By Edit";
                    jobhistoryfpn.LastmodifiedDateTime = DateTime.Now;
                    jobhistoryfpn.JobIDId = job.Id;
                    jobhistoryfpn.JobActionId = jobactionid;
                    list.Add(jobhistoryfpn);
                }
                if (job.FinanceDocumentVerified != input.FinanceDocumentVerified)
                {
                    JobTrackerHistory jobhistoryfpn = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistoryfpn.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistoryfpn.FieldName = "Finance Document Verified";
                    jobhistoryfpn.PrevValue = job.FinanceDocumentVerified.ToString();
                    jobhistoryfpn.CurValue = input.FinanceDocumentVerified.ToString();
                    jobhistoryfpn.Action = "Finance Document Verified Edit";
                    jobhistoryfpn.LastmodifiedDateTime = DateTime.Now;
                    jobhistoryfpn.JobIDId = job.Id;
                    jobhistoryfpn.JobActionId = jobactionid;
                    list.Add(jobhistoryfpn);
                }
                if (job.FinanceNotes != input.FinanceNotes)
                {
                    JobTrackerHistory jobhistoryfpn = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistoryfpn.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistoryfpn.FieldName = "Finance Notes";
                    jobhistoryfpn.PrevValue = job.FinanceNotes == null ? "" : job.FinanceNotes;
                    jobhistoryfpn.CurValue = input.FinanceNotes;
                    jobhistoryfpn.Action = "Finance Notes Edit";
                    jobhistoryfpn.LastmodifiedDateTime = DateTime.Now;
                    jobhistoryfpn.JobIDId = job.Id;
                    jobhistoryfpn.JobActionId = jobactionid;
                    list.Add(jobhistoryfpn);
                }

                if (job.BasicCost != input.BasicCost)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Basic Cost";
                    jobhistory.PrevValue = job.BasicCost.ToString();
                    jobhistory.CurValue = input.BasicCost.ToString();
                    jobhistory.Action = "Price Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.TotalCost != input.TotalCost)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Total Cost";
                    jobhistory.PrevValue = job.TotalCost.ToString();
                    jobhistory.CurValue = input.TotalCost.ToString();
                    jobhistory.Action = "Total Cost Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.DepositRequired != input.DepositRequired)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Deposit Required";
                    jobhistory.PrevValue = job.DepositRequired.ToString();
                    jobhistory.CurValue = input.DepositRequired.ToString();
                    jobhistory.Action = "Deposit Required Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.RebateAppRef != input.RebateAppRef)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Rebate App Ref";
                    jobhistory.PrevValue = job.RebateAppRef;
                    jobhistory.CurValue = input.RebateAppRef;
                    jobhistory.Action = "Rebate App Ref Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.ApprovalDate != input.ApprovalDate)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Approval Date";
                    jobhistory.PrevValue = job.ApprovalDate.ToString();
                    jobhistory.CurValue = input.ApprovalDate.ToString();
                    jobhistory.Action = "ApprovalDate Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.ExpiryDate != input.ExpiryDate)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Expiry Date";
                    jobhistory.PrevValue = job.ExpiryDate.ToString();
                    jobhistory.CurValue = input.ExpiryDate.ToString();
                    jobhistory.Action = "Expiry Date Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.VicRebate != input.VicRebate)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Vic Rebate";
                    jobhistory.PrevValue = job.VicRebate;
                    jobhistory.CurValue = input.VicRebate;
                    jobhistory.Action = "Vic Rebate Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.SolarVICLoanDiscont != input.SolarVICLoanDiscont)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Solar Vic Loan Discont";
                    jobhistory.PrevValue = job.SolarVICLoanDiscont.ToString();
                    jobhistory.CurValue = input.SolarVICLoanDiscont.ToString();
                    jobhistory.Action = "Solar Vic Loan Discont Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.VicRebateNotes != input.VicRebateNotes)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Vic Rebate Notes";
                    jobhistory.PrevValue = job.VicRebateNotes;
                    jobhistory.CurValue = input.VicRebateNotes;
                    jobhistory.Action = "Vic Rebate Notes Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.BlackOutProtection != input.BlackOutProtection)
                {
                    JobTrackerHistory jobhistoryfpn = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistoryfpn.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistoryfpn.FieldName = "Blackout Protection";
                    jobhistoryfpn.PrevValue = job.BlackOutProtection;
                    jobhistoryfpn.CurValue = input.BlackOutProtection;
                    jobhistoryfpn.Action = "Blackout Protection Edit";
                    jobhistoryfpn.LastmodifiedDateTime = DateTime.Now;
                    jobhistoryfpn.JobIDId = job.Id;
                    jobhistoryfpn.JobActionId = jobactionid;
                    list.Add(jobhistoryfpn);
                }
                 if (job.InverterLocation != input.InverterLocation)
                {
                    JobTrackerHistory jobhistoryfpn = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistoryfpn.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistoryfpn.FieldName = "InverterLocation";
                    jobhistoryfpn.PrevValue = job.InverterLocation;
                    jobhistoryfpn.CurValue = input.InverterLocation;
                    jobhistoryfpn.Action = "Blackout Protection Edit";
                    jobhistoryfpn.LastmodifiedDateTime = DateTime.Now;
                    jobhistoryfpn.JobIDId = job.Id;
                    jobhistoryfpn.JobActionId = jobactionid;
                    list.Add(jobhistoryfpn);
                }
                 if (job.PaymentTerm != input.PaymentTerm)
                {
                    JobTrackerHistory jobhistoryfpn = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistoryfpn.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistoryfpn.FieldName = "PaymentTerm";
                    jobhistoryfpn.PrevValue = job.PaymentTerm;
                    jobhistoryfpn.CurValue = input.PaymentTerm;
                    jobhistoryfpn.Action = "Payment Term Edit";
                    jobhistoryfpn.LastmodifiedDateTime = DateTime.Now;
                    jobhistoryfpn.JobIDId = job.Id;
                    jobhistoryfpn.JobActionId = jobactionid;
                    list.Add(jobhistoryfpn);
                }
                 if (job.RepaymentAmt != input.RepaymentAmt)
                {
                    JobTrackerHistory jobhistoryfpn = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistoryfpn.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistoryfpn.FieldName = "RepaymentAmt";
                    jobhistoryfpn.PrevValue = job.RepaymentAmt.ToString();
                    jobhistoryfpn.CurValue = input.RepaymentAmt.ToString();
                    jobhistoryfpn.Action = "Repayment Amount Edit";
                    jobhistoryfpn.LastmodifiedDateTime = DateTime.Now;
                    jobhistoryfpn.JobIDId = job.Id;
                    jobhistoryfpn.JobActionId = jobactionid;
                    list.Add(jobhistoryfpn);
                }
                if (job.CategoryDDiscountAmt != input.CategoryDDiscountAmt)
                {
                    JobTrackerHistory jobhistoryfpn = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistoryfpn.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistoryfpn.FieldName = "CategoryD DiscountAmt";
                    jobhistoryfpn.PrevValue = job.CategoryDDiscountAmt.ToString();
                    jobhistoryfpn.CurValue = input.CategoryDDiscountAmt.ToString();
                    jobhistoryfpn.Action = "CategoryD Discount Amount Edit";
                    jobhistoryfpn.LastmodifiedDateTime = DateTime.Now;
                    jobhistoryfpn.JobIDId = job.Id;
                    jobhistoryfpn.JobActionId = jobactionid;
                    list.Add(jobhistoryfpn);
                }
                if (job.CategoryDNotes != input.CategoryDNotes)
                {
                    JobTrackerHistory jobhistoryfpn = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistoryfpn.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistoryfpn.FieldName = "CategoryD Notes";
                    jobhistoryfpn.PrevValue = job.CategoryDNotes;
                    jobhistoryfpn.CurValue = input.CategoryDNotes;
                    jobhistoryfpn.Action = "CategoryD Notes Edit";
                    jobhistoryfpn.LastmodifiedDateTime = DateTime.Now;
                    jobhistoryfpn.JobIDId = job.Id;
                    jobhistoryfpn.JobActionId = jobactionid;
                    list.Add(jobhistoryfpn);
                }

                //await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
                //await _dbcontextprovider.GetDbContext().SaveChangesAsync();


                // Old input.JobProductItems Code
                //if (input.JobProductItems != null)
                //{
                //    if (input.JobProductItems[0].ProductItemId != null)
                //    {
                //        _jobProductItemRepository.Delete(x => x.JobId == job.Id);
                //    }
                //    foreach (var jobproduct in input.JobProductItems)
                //    {
                //        if (jobproduct.ProductItemId != null)
                //        {
                //            var jobProductItem = ObjectMapper.Map<JobProductItem>(jobproduct);
                //            jobProductItem.JobId = job.Id;
                //            if (AbpSession.TenantId != null)
                //            {
                //                jobProductItem.TenantId = (int)AbpSession.TenantId;
                //            }
                //            _jobProductItemRepository.Insert(jobProductItem);
                //        }
                //    }
                //}

                if (input.JobProductItems != null)
                {

                    var ProductItemList = _productItemRepository.GetAll();
                    var ProductType = _productTypeRepository.GetAll();

                    var IdList = input.JobProductItems.Select(e => e.Id).ToList();
                    var existingData = _jobProductItemRepository.GetAll().AsNoTracking().Where(x => x.JobId == job.Id).ToList();

                    if (existingData != null)
                    {
                        foreach (var item in existingData)
                        {
                            if (IdList != null)
                            {
                                bool containsItem = IdList.Any(x => x == item.Id);
                                if (containsItem == false)
                                {
                                    JobTrackerHistory jobhistory1 = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory1.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory1.FieldName = "Product Name";
                                    jobhistory1.PrevValue = ProductItemList.Where(x => x.Id == item.ProductItemId).Select(x => x.Name).FirstOrDefault();
                                    jobhistory1.CurValue = "Deleted";
                                    jobhistory1.Action = "Products Delete";
                                    jobhistory1.LastmodifiedDateTime = DateTime.Now;
                                    jobhistory1.JobIDId = job.Id;
                                    jobhistory1.JobActionId = jobactionid;
                                    list.Add(jobhistory1);

                                    JobTrackerHistory jobhistory2 = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory2.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory2.FieldName = "Product Model";
                                    jobhistory2.PrevValue = ProductItemList.Where(x => x.Id == item.ProductItemId).Select(x => x.Model).FirstOrDefault();
                                    jobhistory2.CurValue = "Deleted";
                                    jobhistory2.Action = "Products Delete";
                                    jobhistory2.LastmodifiedDateTime = DateTime.Now;
                                    jobhistory2.JobIDId = job.Id;
                                    jobhistory2.JobActionId = jobactionid;
                                    list.Add(jobhistory2);

                                    JobTrackerHistory jobhistory3 = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory3.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory3.FieldName = "Product Quantity";
                                    jobhistory3.PrevValue = item.Quantity.ToString();
                                    jobhistory3.CurValue = "Deleted";
                                    jobhistory3.Action = "Products Delete";
                                    jobhistory3.LastmodifiedDateTime = DateTime.Now;
                                    jobhistory3.JobIDId = job.Id;
                                    jobhistory3.JobActionId = jobactionid;
                                    list.Add(jobhistory3);

                                    JobTrackerHistory jobhistory4 = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory2.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory4.FieldName = "Product Type";
                                    jobhistory4.PrevValue = ProductType.Where(x => x.Id == (ProductItemList.Where(x => x.Id == item.ProductItemId).Select(x => x.ProductTypeId).FirstOrDefault())).Select(x => x.Name).FirstOrDefault();
                                    jobhistory4.CurValue = "Deleted";
                                    jobhistory4.Action = "Products Delete";
                                    jobhistory4.LastmodifiedDateTime = DateTime.Now;
                                    jobhistory4.JobIDId = job.Id;
                                    jobhistory4.JobActionId = jobactionid;
                                    list.Add(jobhistory4);
                                    _jobProductItemRepository.Delete(x => x.Id == item.Id);
                                }
                            }
                        }
                    }

                    foreach (var jobproduct in input.JobProductItems)
                    {
                        if (jobproduct.Id != null && jobproduct.Id != 0)
                        {
                            var existData = _jobProductItemRepository.GetAll().AsNoTracking().Where(x => x.Id == jobproduct.Id).FirstOrDefault();
                            var olddetail = ObjectMapper.Map<JobProductItem>(jobproduct);
                            olddetail.JobId = job.Id;
                            if (AbpSession.TenantId != null)
                            {
                                olddetail.TenantId = (int)AbpSession.TenantId;
                            }

                            _jobProductItemRepository.Update(olddetail);



                            if (jobproduct.ProductItemId != null && jobproduct.ProductItemId != 0 && existData.ProductItemId != jobproduct.ProductItemId)
                            {
                                var existProductName = ProductItemList.Where(x => x.Id == existData.ProductItemId).Select(x => x.Name).FirstOrDefault();
                                var currentProductName = ProductItemList.Where(x => x.Id == jobproduct.ProductItemId).Select(x => x.Name).FirstOrDefault();

                                var existModel = ProductItemList.Where(x => x.Id == existData.ProductItemId).Select(x => x.Model).FirstOrDefault();
                                var currentModel = ProductItemList.Where(x => x.Id == jobproduct.ProductItemId).Select(x => x.Model).FirstOrDefault();

                                var existProductType = ProductType.Where(x => x.Id == (ProductItemList.Where(x => x.Id == existData.ProductItemId).Select(x => x.ProductTypeId).FirstOrDefault())).Select(x => x.Name).FirstOrDefault();
                                var currentProductType = ProductType.Where(x => x.Id == jobproduct.ProductTypeId).Select(x => x.Name).FirstOrDefault();

                                if (existProductName != currentProductName)
                                {
                                    JobTrackerHistory jobhistory1 = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory1.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory1.FieldName = "Product Name";
                                    jobhistory1.PrevValue = existProductName;
                                    jobhistory1.CurValue = currentProductName;
                                    jobhistory1.Action = "Products Edit";
                                    jobhistory1.LastmodifiedDateTime = DateTime.Now;
                                    jobhistory1.JobIDId = job.Id;
                                    jobhistory1.JobActionId = jobactionid;
                                    list.Add(jobhistory1);
                                }

                                if (existModel != currentModel)
                                {
                                    JobTrackerHistory jobhistory2 = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory2.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory2.FieldName = "Product Model";
                                    jobhistory2.PrevValue = existModel;
                                    jobhistory2.CurValue = currentModel;
                                    jobhistory2.Action = "Products Edit";
                                    jobhistory2.LastmodifiedDateTime = DateTime.Now;
                                    jobhistory2.JobIDId = job.Id;
                                    jobhistory2.JobActionId = jobactionid;
                                    list.Add(jobhistory2);
                                }

                                if (existProductType != currentProductType)
                                {
                                    JobTrackerHistory jobhistory3 = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory3.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory3.FieldName = "Product Type";
                                    jobhistory3.PrevValue = existProductType;
                                    jobhistory3.CurValue = currentProductType;
                                    jobhistory3.Action = "Products Edit";
                                    jobhistory3.LastmodifiedDateTime = DateTime.Now;
                                    jobhistory3.JobIDId = job.Id;
                                    jobhistory3.JobActionId = jobactionid;
                                    list.Add(jobhistory3);
                                }
                            }

                            if (jobproduct.Quantity != null && existData.Quantity != jobproduct.Quantity)
                            {
                                JobTrackerHistory jobhistory = new JobTrackerHistory();
                                if (AbpSession.TenantId != null)
                                {
                                    jobhistory.TenantId = (int)AbpSession.TenantId;
                                }
                                jobhistory.FieldName = "Product Item Quantity";
                                jobhistory.PrevValue = existData.Quantity.ToString();
                                jobhistory.CurValue = jobproduct.Quantity.ToString();
                                jobhistory.Action = "Products Edit";
                                jobhistory.LastmodifiedDateTime = DateTime.Now;
                                jobhistory.JobIDId = job.Id;
                                jobhistory.JobActionId = jobactionid;
                                list.Add(jobhistory);
                            }
                       
                           
                        }
                        //else if (jobproduct.ProductItemId != null)
                        else
                        {
                            var olddetail = ObjectMapper.Map<JobProductItem>(jobproduct);
                            olddetail.JobId = job.Id;
                            if (AbpSession.TenantId != null)
                            {
                                olddetail.TenantId = (int)AbpSession.TenantId;
                            }

                            JobTrackerHistory jobhistory = new JobTrackerHistory();
                            if (AbpSession.TenantId != null)
                            {
                                jobhistory.TenantId = (int)AbpSession.TenantId;
                            }
                            var currentProductName = ProductItemList.Where(x => x.Id == jobproduct.ProductItemId).Select(x => x.Name).FirstOrDefault();

                            jobhistory.FieldName = " new Product Item Added " + currentProductName;
                            jobhistory.PrevValue = "";
                            jobhistory.CurValue = "";
                            jobhistory.Action = "Products Add";
                            jobhistory.LastmodifiedDateTime = DateTime.Now;
                            jobhistory.JobIDId = job.Id;
                            jobhistory.JobActionId = jobactionid;
                            list.Add(jobhistory);

                            _jobProductItemRepository.Insert(olddetail);

                            //if (jobproduct.ProductItemId != null && jobproduct.ProductItemId != 0)
                            //{
                            //    JobTrackerHistory jobhistory1 = new JobTrackerHistory();
                            //    if (AbpSession.TenantId != null)
                            //    {
                            //        jobhistory1.TenantId = (int)AbpSession.TenantId;
                            //    }
                            //    jobhistory1.FieldName = "Product Name";
                            //    jobhistory1.PrevValue = null;
                            //    jobhistory1.CurValue = _productItemRepository.GetAll().Where(x => x.Id == jobproduct.ProductItemId).Select(x => x.Name).FirstOrDefault();
                            //    jobhistory1.Action = "Job Product Items Edit";
                            //    jobhistory1.LastmodifiedDateTime = DateTime.Now;
                            //    jobhistory1.JobIDId = job.Id;
                            //    jobhistory1.JobActionId = jobactionid;
                            //    list.Add(jobhistory1);

                            //    JobTrackerHistory jobhistory2 = new JobTrackerHistory();
                            //    if (AbpSession.TenantId != null)
                            //    {
                            //        jobhistory2.TenantId = (int)AbpSession.TenantId;
                            //    }
                            //    jobhistory2.FieldName = "Product Model";
                            //    jobhistory2.PrevValue = null;
                            //    jobhistory2.CurValue = _productItemRepository.GetAll().Where(x => x.Id == jobproduct.ProductItemId).Select(x => x.Model).FirstOrDefault();
                            //    jobhistory2.Action = "Job Product Items Edit";
                            //    jobhistory2.LastmodifiedDateTime = DateTime.Now;
                            //    jobhistory2.JobIDId = job.Id;
                            //    jobhistory2.JobActionId = jobactionid;
                            //    list.Add(jobhistory2);

                            //    JobTrackerHistory jobhistory3 = new JobTrackerHistory();
                            //    if (AbpSession.TenantId != null)
                            //    {
                            //        jobhistory3.TenantId = (int)AbpSession.TenantId;
                            //    }
                            //    jobhistory3.FieldName = "Product Type";
                            //    jobhistory3.PrevValue = null;
                            //    jobhistory3.CurValue = _productTypeRepository.GetAll().Where(x => x.Id == jobproduct.ProductTypeId).Select(x => x.Name).FirstOrDefault();
                            //    jobhistory3.Action = "Job Product Items Edit";
                            //    jobhistory3.LastmodifiedDateTime = DateTime.Now;
                            //    jobhistory3.JobIDId = job.Id;
                            //    jobhistory3.JobActionId = jobactionid;
                            //    list.Add(jobhistory3);
                            //}

                            //if (jobproduct.Quantity != null)
                            //{
                            //    JobTrackerHistory jobhistory = new JobTrackerHistory();
                            //    if (AbpSession.TenantId != null)
                            //    {
                            //        jobhistory.TenantId = (int)AbpSession.TenantId;
                            //    }
                            //    jobhistory.FieldName = "Product Item Quantity";
                            //    jobhistory.PrevValue = null;
                            //    jobhistory.CurValue = jobproduct.Quantity.ToString();
                            //    jobhistory.Action = "Job Product Items Edit";
                            //    jobhistory.LastmodifiedDateTime = DateTime.Now;
                            //    jobhistory.JobIDId = job.Id;
                            //    jobhistory.JobActionId = jobactionid;
                            //    list.Add(jobhistory);
                            //}
                        }
                    }
                }

                // Old input.JobPromotion Code
                //if (input.JobPromotion != null)
                //{
                //    if (input.JobPromotion[0].PromotionMasterId != null)
                //    {
                //        _jobPromotionRepository.Delete(x => x.JobId == job.Id);
                //    }
                //    foreach (var jobpromotion in input.JobPromotion)
                //    {
                //        if (jobpromotion.PromotionMasterId != null)
                //        {
                //            var jobpromo = ObjectMapper.Map<JobPromotion>(jobpromotion);
                //            jobpromo.JobId = job.Id;
                //            if (AbpSession.TenantId != null)
                //            {
                //                jobpromo.TenantId = (int)AbpSession.TenantId;
                //            }
                //            _jobPromotionRepository.Insert(jobpromo);
                //        }
                //    }
                //}

                if (input.JobPromotion != null)
                {
                    var PromotionList = _promotionMasterRepository.GetAll();
                    var IdList = input.JobPromotion.Select(e => e.Id).ToList();
                    var existingData = _jobPromotionRepository.GetAll().AsNoTracking().Where(x => x.JobId == job.Id).ToList();

                    if (existingData != null)
                    {
                        foreach (var item in existingData)
                        {
                            if (IdList != null)
                            {
                                bool containsItem = IdList.Any(x => x == item.Id);
                                if (containsItem == false)
                                {
                                    JobTrackerHistory jobhistory1 = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory1.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory1.FieldName = "Promotion Name";
                                    jobhistory1.PrevValue = PromotionList.Where(x => x.Id == item.PromotionMasterId).Select(x => x.Name).FirstOrDefault();
                                    jobhistory1.CurValue = "Deleted";
                                    jobhistory1.Action = "Promotion Delete";
                                    jobhistory1.LastmodifiedDateTime = DateTime.Now;
                                    jobhistory1.JobIDId = job.Id;
                                    jobhistory1.JobActionId = jobactionid;
                                    list.Add(jobhistory1);
                                    _jobPromotionRepository.Delete(x => x.Id == item.Id);
                                }
                            }
                        }
                    }

                    foreach (var jobpromotion in input.JobPromotion)
                    {
                        if (jobpromotion.Id != null && jobpromotion.Id != 0)
                        {
                            var existData = _jobPromotionRepository.GetAll().AsNoTracking().Where(x => x.Id == jobpromotion.Id).FirstOrDefault();
                            var olddetail = ObjectMapper.Map<JobPromotion>(jobpromotion);
                            olddetail.JobId = job.Id;
                            if (AbpSession.TenantId != null)
                            {
                                olddetail.TenantId = (int)AbpSession.TenantId;
                            }

                            _jobPromotionRepository.Update(olddetail);

                            if (jobpromotion.PromotionMasterId != null && jobpromotion.PromotionMasterId != 0 && existData.PromotionMasterId != jobpromotion.PromotionMasterId)
                            {
                                var existPromotion = PromotionList.Where(x => x.Id == existData.PromotionMasterId).Select(x => x.Name).FirstOrDefault();
                                var currentPromotion = PromotionList.Where(x => x.Id == jobpromotion.PromotionMasterId).Select(x => x.Name).FirstOrDefault();

                                if (existPromotion != currentPromotion)
                                {
                                    JobTrackerHistory jobhistory1 = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory1.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory1.FieldName = "Promotion Name";
                                    jobhistory1.PrevValue = existPromotion;
                                    jobhistory1.CurValue = currentPromotion;
                                    jobhistory1.Action = "Promotion Edit";
                                    jobhistory1.LastmodifiedDateTime = DateTime.Now;
                                    jobhistory1.JobIDId = job.Id;
                                    jobhistory1.JobActionId = jobactionid;
                                    list.Add(jobhistory1);
                                }
                            }
                        }
                        //else if (jobpromotion.PromotionMasterId != null)
                        else
                        {
                            var olddetail = ObjectMapper.Map<JobPromotion>(jobpromotion);
                            olddetail.JobId = job.Id;
                            if (AbpSession.TenantId != null)
                            {
                                olddetail.TenantId = (int)AbpSession.TenantId;
                            }
                            JobTrackerHistory jobhistory1 = new JobTrackerHistory();
                            if (AbpSession.TenantId != null)
                            {
                                jobhistory1.TenantId = (int)AbpSession.TenantId;
                            }
                            jobhistory1.FieldName = "Promotion Name";
                            jobhistory1.PrevValue = "Added";
                            jobhistory1.CurValue = PromotionList.Where(x => x.Id == jobpromotion.PromotionMasterId).Select(x => x.Name).FirstOrDefault();
                            jobhistory1.Action = "Promotion Added";
                            jobhistory1.LastmodifiedDateTime = DateTime.Now;
                            jobhistory1.JobIDId = job.Id;
                            jobhistory1.JobActionId = jobactionid;
                            list.Add(jobhistory1);
                            _jobPromotionRepository.Insert(olddetail);
                        }
                    }
                }

                // Old input.JobVariation Code
                //if (input.JobVariation != null)
                //{
                //    if (input.JobVariation[0].VariationId != null)
                //    {
                //        _jobVariationRepository.Delete(x => x.JobId == job.Id);
                //    }
                //    foreach (var jobvariation in input.JobVariation)
                //    {
                //        if (jobvariation.VariationId != null)
                //        {
                //            var jobvar = ObjectMapper.Map<JobVariation>(jobvariation);
                //            jobvar.JobId = job.Id;
                //            if (AbpSession.TenantId != null)
                //            {
                //                jobvar.TenantId = (int)AbpSession.TenantId;
                //            }
                //            _jobVariationRepository.Insert(jobvar);
                //        }
                //    }
                //}

                if (input.JobVariation != null)
                {
                    var variationList = _variationRepository.GetAll();
                    var IdList = input.JobVariation.Select(e => e.Id).ToList();
                    var existingData = _jobVariationRepository.GetAll().AsNoTracking().Where(x => x.JobId == job.Id).ToList();

                    if (existingData != null)
                    {
                        foreach (var item in existingData)
                        {
                            if (IdList != null)
                            {
                                bool containsItem = IdList.Any(x => x == item.Id);
                                if (containsItem == false)
                                {
                                    JobTrackerHistory jobhistory1 = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory1.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory1.FieldName = "Variation Deleted";
                                    jobhistory1.PrevValue = variationList.Where(x => x.Id == item.VariationId).Select(x => x.Name).FirstOrDefault();
                                    jobhistory1.CurValue = "Deleted";
                                    jobhistory1.Action = "Varition Delete";
                                    jobhistory1.LastmodifiedDateTime = DateTime.UtcNow;
                                    jobhistory1.JobIDId = job.Id;
                                    jobhistory1.JobActionId = jobactionid;
                                    list.Add(jobhistory1);

                                    //JobTrackerHistory jobhistory = new JobTrackerHistory();
                                    //if (AbpSession.TenantId != null)
                                    //{
                                    //    jobhistory.TenantId = (int)AbpSession.TenantId;
                                    //}
                                    //jobhistory.FieldName = "Variation Cost";
                                    //jobhistory.PrevValue = item.Cost.ToString();
                                    //jobhistory.CurValue = "Deleted";
                                    //jobhistory.Action = "Varition Delete";
                                    //jobhistory.LastmodifiedDateTime = DateTime.Now;
                                    //jobhistory.JobIDId = job.Id;
                                    //jobhistory.JobActionId = jobactionid;
                                    //list.Add(jobhistory);

                                    _jobVariationRepository.Delete(x => x.Id == item.Id);
                                }
                            }
                        }
                    }

                    foreach (var jobvariation in input.JobVariation)
                    {
                        if (jobvariation.Id != null && jobvariation.Id != 0)
                        {
                            var existData = _jobVariationRepository.GetAll().AsNoTracking().Where(x => x.Id == jobvariation.Id).FirstOrDefault();
                            var olddetail = ObjectMapper.Map<JobVariation>(jobvariation);
                            olddetail.JobId = job.Id;
                            if (AbpSession.TenantId != null)
                            {
                                olddetail.TenantId = (int)AbpSession.TenantId;
                            }

                            _jobVariationRepository.Update(olddetail);

                            var existVariation = variationList.Where(x => x.Id == existData.VariationId).Select(x => x.Name).FirstOrDefault();
                            var currentVariation = variationList.Where(x => x.Id == jobvariation.VariationId).Select(x => x.Name).FirstOrDefault();

                            if (jobvariation.VariationId != null && jobvariation.VariationId != 0 && existData.VariationId != jobvariation.VariationId)
                            {
                                //var existVariation = variationList.Where(x => x.Id == existData.VariationId).Select(x => x.Name).FirstOrDefault();
                                //var currentVariation = variationList.Where(x => x.Id == jobvariation.VariationId).Select(x => x.Name).FirstOrDefault();

                                if (existVariation != currentVariation)
                                {
                                    JobTrackerHistory jobhistory1 = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory1.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory1.FieldName = "Variation Edit";
                                    jobhistory1.PrevValue = existVariation + " - $" + existData.Cost.ToString() + " " + existData.Notes ;
                                    jobhistory1.CurValue = currentVariation + " - $" + jobvariation.Cost.ToString() + " " + jobvariation.Notes;
                                    jobhistory1.Action = "Varition Edit";
                                    jobhistory1.LastmodifiedDateTime = DateTime.UtcNow;
                                    jobhistory1.JobIDId = job.Id;
                                    jobhistory1.JobActionId = jobactionid;
                                    list.Add(jobhistory1);
                                }
                            }
                            else if (jobvariation.Cost != null && existData.Cost != jobvariation.Cost)
                            {
                                JobTrackerHistory jobhistory = new JobTrackerHistory();
                                if (AbpSession.TenantId != null)
                                {
                                    jobhistory.TenantId = (int)AbpSession.TenantId;
                                }
                                jobhistory.FieldName = "Variation Edit";
                                //jobhistory.PrevValue = existData.Cost.ToString();
                                //jobhistory.CurValue = jobvariation.Cost.ToString();
                                jobhistory.PrevValue = existVariation + " - $" + existData.Cost.ToString();
                                jobhistory.CurValue = currentVariation + " - $" + jobvariation.Cost.ToString();
                                jobhistory.Action = "Varition Edit";
                                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                                jobhistory.JobIDId = job.Id;
                                jobhistory.JobActionId = jobactionid;
                                list.Add(jobhistory);
                            }
                            else if (jobvariation.Notes != null && existData.Notes != jobvariation.Notes)
                            {
                                JobTrackerHistory jobhistory = new JobTrackerHistory();
                                if (AbpSession.TenantId != null)
                                {
                                    jobhistory.TenantId = (int)AbpSession.TenantId;
                                }
                                jobhistory.FieldName = "Variation Notes";
                                //jobhistory.PrevValue = existData.Cost.ToString();
                                //jobhistory.CurValue = jobvariation.Cost.ToString();
                                jobhistory.PrevValue = existData.Notes;
                                jobhistory.CurValue = jobvariation.Notes;
                                jobhistory.Action = "Varition Edit";
                                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                                jobhistory.JobIDId = job.Id;
                                jobhistory.JobActionId = jobactionid;
                                list.Add(jobhistory);
                            }
                        }
                        //else if (jobvariation.VariationId != null)
                        else
                        {
                            var olddetail = ObjectMapper.Map<JobVariation>(jobvariation);
                            olddetail.JobId = job.Id;
                            if (AbpSession.TenantId != null)
                            {
                                olddetail.TenantId = (int)AbpSession.TenantId;
                            }

                            var varName = variationList.Where(x => x.Id == olddetail.VariationId).Select(x => x.Name).FirstOrDefault();
                            JobTrackerHistory jobhistory = new JobTrackerHistory();
                            if (AbpSession.TenantId != null)
                            {
                                jobhistory.TenantId = (int)AbpSession.TenantId;
                            }
                            jobhistory.FieldName = "Variation Added";
                            //jobhistory.PrevValue = existData.Cost.ToString();
                            //jobhistory.CurValue = jobvariation.Cost.ToString();
                            jobhistory.PrevValue = "";
                            jobhistory.CurValue = varName + " - $" + jobvariation.Cost.ToString() + " - " + jobvariation.Notes;
                            jobhistory.Action = "Varition Added";
                            jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                            jobhistory.JobIDId = job.Id;
                            jobhistory.JobActionId = jobactionid;
                            list.Add(jobhistory);

                            _jobVariationRepository.Insert(olddetail);
                        }
                    }
                }
                if (job.ExportDetail != input.ExportDetail)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Export Detail";
                    jobhistory.PrevValue = job.ExportDetail == 1 ? "Fully Export" : (job.ExportDetail == 2 ? "Partial Export" : "");
                    jobhistory.CurValue = input.ExportDetail == 1 ? "Fully Export" : (input.ExportDetail == 2 ? "Partial Export" : "");
                    jobhistory.Action = "Job Export Detail";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                
                if (job.Exportkw != input.Exportkw)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Export kw";
                    jobhistory.PrevValue = job.Exportkw.ToString();
                    jobhistory.CurValue = input.Exportkw.ToString();
                    jobhistory.Action = "Job Export kw";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                
                if (job.NonExportDetail != input.NonExportDetail)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Non Export Detail";
                    jobhistory.PrevValue = job.NonExportDetail.ToString();
                    jobhistory.CurValue = input.NonExportDetail.ToString();
                    jobhistory.Action = "Non Export Detail Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                
                if (job.IsGridConnected != input.IsGridConnected)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "isGridConnected";
                    jobhistory.PrevValue = job.IsGridConnected.ToString();
                    jobhistory.CurValue = input.IsGridConnected.ToString();
                    jobhistory.Action = "Job IsGridConnected";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                
                if (job.ApproxFeedTariff != input.ApproxFeedTariff)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "ApproxFeedTariff";
                    jobhistory.PrevValue = job.ApproxFeedTariff;
                    jobhistory.CurValue = input.ApproxFeedTariff;
                    jobhistory.Action = "Job ApproxFeedTariff";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                
                if (job.ExpectedPaybackPeriod != input.ExpectedPaybackPeriod)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "ExpectedPaybackPeriod";
                    jobhistory.PrevValue = job.ExpectedPaybackPeriod;
                    jobhistory.CurValue = input.ExpectedPaybackPeriod;
                    jobhistory.Action = "Job ExpectedPaybackPeriod";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.FlexibleRange != input.FlexibleRange)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "FlexibleRange";
                    jobhistory.PrevValue = job.FlexibleRange;
                    jobhistory.CurValue = input.FlexibleRange;
                    jobhistory.Action = "Job FlexibleRange";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.SolarVICRebate != input.SolarVICRebate)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "SolarVICRebate";
                    jobhistory.PrevValue = job.SolarVICRebate.ToString();
                    jobhistory.CurValue = input.SolarVICRebate.ToString();
                    jobhistory.Action = "Job SolarVICRebate";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
               
                if (job.SolarRebateStatus != input.SolarRebateStatus)
                {
                    var jobSolarRebateStatus = "";
                    var inputSolarVICRebate = "";

                    switch (job.SolarRebateStatus)
                    {
                        case 1:
                            jobSolarRebateStatus = "Without Rebate";
                            break;
                        case 2:
                            jobSolarRebateStatus = "After Install";
                            break;
                        case 3:
                            jobSolarRebateStatus = "Cancelled";
                            break;
                        case 4:
                            jobSolarRebateStatus = "Installation Completed";
                            break;
                        case 5:
                            jobSolarRebateStatus = "Installation Scheduled";
                            break;
                        case 6:
                            jobSolarRebateStatus = "Payment Approved";
                            break;
                        case 7:
                            jobSolarRebateStatus = "Payment Claim Under Review";
                            break;
                        case 8:
                            jobSolarRebateStatus = "Payment Processed";
                            break;
                        case 9:
                            jobSolarRebateStatus = "Quote Submitted";
                            break;
                        case 10:
                            jobSolarRebateStatus = "Appl Rejected";
                            break;
                        case 11:
                            jobSolarRebateStatus = "Additional Info Required";
                            break;
                        case 12:
                            jobSolarRebateStatus = "App Expired";
                            break;
                        case 13:
                            jobSolarRebateStatus = "Draft quote";
                            break;
                        case 14:
                            jobSolarRebateStatus = "Quote expired";
                            break;
                        case 15:
                            jobSolarRebateStatus = "Appl Started";
                            break;
                        case 16:
                            jobSolarRebateStatus = "Appl under review";
                            break;
                        case 17:
                            jobSolarRebateStatus = "Appl approved";
                            break;
                        case 18:
                            jobSolarRebateStatus = "Quote expired";
                            break;
                    }

                    switch (input.SolarVICRebate)
                    {
                        case 1:
                            inputSolarVICRebate = "Without Rebate";
                            break;
                        case 2:
                            inputSolarVICRebate = "After Install";
                            break;
                        case 3:
                            inputSolarVICRebate = "Cancelled";
                            break;
                        case 4:
                            inputSolarVICRebate = "Installation Completed";
                            break;
                        case 5:
                            inputSolarVICRebate = "Installation Scheduled";
                            break;
                        case 6:
                            inputSolarVICRebate = "Payment Approved";
                            break;
                        case 7:
                            inputSolarVICRebate = "Payment Claim Under Review";
                            break;
                        case 8:
                            inputSolarVICRebate = "Payment Processed";
                            break;
                        case 9:
                            inputSolarVICRebate = "Quote Submitted";
                            break;
                        case 10:
                            inputSolarVICRebate = "Appl Rejected";
                            break;
                        case 11:
                            inputSolarVICRebate = "Additional Info Required";
                            break;
                        case 12:
                            inputSolarVICRebate = "App Expired";
                            break;
                        case 13:
                            inputSolarVICRebate = "Draft quote";
                            break;
                        case 14:
                            inputSolarVICRebate = "Quote expired";
                            break;
                        case 15:
                            inputSolarVICRebate = "Appl Started";
                            break;
                        case 16:
                            inputSolarVICRebate = "Appl under review";
                            break;
                        case 17:
                            inputSolarVICRebate = "Appl approved";
                            break;
                        case 18:
                            inputSolarVICRebate = "Quote expired";
                            break;
                    }

                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "SolarRebateStatus";
                    jobhistory.PrevValue = jobSolarRebateStatus;
                    jobhistory.CurValue = inputSolarVICRebate;
                    jobhistory.Action = "Job SolarRebateStatus";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.HouseAge != input.HouseAge)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "HouseAge";
                    jobhistory.PrevValue = job.HouseAge;
                    jobhistory.CurValue = input.HouseAge;
                    jobhistory.Action = "Job HouseAge";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.SiteVisit != input.SiteVisit)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "SiteVisit";
                    jobhistory.PrevValue = job.SiteVisit == true ? "yes" : "No";
                    jobhistory.CurValue = input.SiteVisit == true ? "yes" : "No";
                    jobhistory.Action = "Job SiteVisit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.LocationOfSwitchBoard != input.LocationOfSwitchBoard)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Location Of Meter And SwitchBoard";
                    jobhistory.PrevValue = job.LocationOfSwitchBoard ;
                    jobhistory.CurValue = input.LocationOfSwitchBoard ;
                    jobhistory.Action = "Job Location Of Meter And SwitchBoard";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if(job.BatteryRebate != input.BatteryRebate)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "BatteryRebate";
                    jobhistory.PrevValue = job.BatteryRebate != null ? job.BatteryRebate.ToString() : "0";
                    jobhistory.CurValue = input.BatteryRebate != null ? input.BatteryRebate.ToString() : "0";
                    jobhistory.Action = "Job BatteryRebate";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.BatteryBess != input.BatteryBess)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Battery BESS";
                    jobhistory.PrevValue = job.BatteryBess != null ? job.BatteryBess.ToString() : "0";
                    jobhistory.CurValue = input.BatteryBess != null ? input.BatteryBess.ToString() : "0";
                    jobhistory.Action = "Job Battery BESS";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.BatteryBessPerPrice != input.BatteryBessPerPrice)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Battery BESS Per Price";
                    jobhistory.PrevValue = job.BatteryBess != null ? job.BatteryBessPerPrice.ToString() : "0";
                    jobhistory.CurValue = input.BatteryBess != null ? input.BatteryBessPerPrice.ToString() : "0" ;
                    jobhistory.Action = "Job Battery BESS Per Price";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                if (job.IsBatteryRebate != input.IsBatteryRebate)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "IsBatteryRebate";
                    jobhistory.PrevValue = job.IsBatteryRebate == true ? "Yes" : "No";
                    jobhistory.CurValue = input.IsBatteryRebate == true ? "Yes" : "No";
                    jobhistory.Action = "Job IsBatteryRebate";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                ObjectMapper.Map(input, job);
                await _jobRepository.UpdateAsync(job);
                await JobActive(job.Id);
                /// await JobDeposite(job.Id);


                if (input.JobOldSystemDetail != null)
                {
                    var IdList = input.JobOldSystemDetail.Select(e => e.Id).ToList();
                    var existingData = _JobOldSysDetail.GetAll().AsNoTracking().Where(x => x.JobId == job.Id).ToList();

                    if (existingData != null)
                    {
                        foreach (var item in existingData)
                        {
                            if (IdList != null)
                            {
                                bool containsItem = IdList.Any(x => x == item.Id);
                                if (containsItem == false)
                                {
                                    JobTrackerHistory jobhistory1 = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory1.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory1.FieldName = "Old System Name";
                                    jobhistory1.PrevValue = item.Name;
                                    jobhistory1.CurValue = "Deleted";
                                    jobhistory1.Action = "Old System Details Delete";
                                    jobhistory1.LastmodifiedDateTime = DateTime.Now;
                                    jobhistory1.JobIDId = job.Id;
                                    jobhistory1.JobActionId = jobactionid;
                                    list.Add(jobhistory1);

                                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory.FieldName = "Old System Quantity";
                                    jobhistory.PrevValue = item.Quantity.ToString();
                                    jobhistory.CurValue = "Deleted";
                                    jobhistory.Action = "Old System Details Delete";
                                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                                    jobhistory.JobIDId = job.Id;
                                    jobhistory.JobActionId = jobactionid;
                                    list.Add(jobhistory);

                                    _JobOldSysDetail.Delete(x => x.Id == item.Id);
                                }
                            }
                        }
                    }

                    foreach (var jobolddetail in input.JobOldSystemDetail)
                    {
                        if (jobolddetail.Id != null && jobolddetail.Id != 0)
                        {
                            var existData = _JobOldSysDetail.GetAll().AsNoTracking().Where(x => x.Id == jobolddetail.Id).FirstOrDefault();
                            var olddetail = ObjectMapper.Map<JobOldSystemDetail>(jobolddetail);
                            olddetail.JobId = job.Id;
                            if (AbpSession.TenantId != null)
                            {
                                olddetail.TenantId = (int)AbpSession.TenantId;
                            }

                            _JobOldSysDetail.Update(olddetail);

                            if (jobolddetail.Name != null && existData.Name != jobolddetail.Name)
                            {
                                JobTrackerHistory jobhistory1 = new JobTrackerHistory();
                                if (AbpSession.TenantId != null)
                                {
                                    jobhistory1.TenantId = (int)AbpSession.TenantId;
                                }
                                jobhistory1.FieldName = "Old System Name";
                                jobhistory1.PrevValue = existData.Name;
                                jobhistory1.CurValue = jobolddetail.Name;
                                jobhistory1.Action = "Old System Details Edit";
                                jobhistory1.LastmodifiedDateTime = DateTime.Now;
                                jobhistory1.JobIDId = job.Id;
                                jobhistory1.JobActionId = jobactionid;
                                list.Add(jobhistory1);
                            }

                            if (jobolddetail.Quantity != null && existData.Quantity != jobolddetail.Quantity)
                            {
                                JobTrackerHistory jobhistory = new JobTrackerHistory();
                                if (AbpSession.TenantId != null)
                                {
                                    jobhistory.TenantId = (int)AbpSession.TenantId;
                                }
                                jobhistory.FieldName = "Old System Quantity";
                                jobhistory.PrevValue = existData.Quantity.ToString();
                                jobhistory.CurValue = jobolddetail.Quantity.ToString();
                                jobhistory.Action = "Old System Details Edit";
                                jobhistory.LastmodifiedDateTime = DateTime.Now;
                                jobhistory.JobIDId = job.Id;
                                jobhistory.JobActionId = jobactionid;
                                list.Add(jobhistory);
                            }
                        }
                        //else if (jobolddetail.Name != null)
                        else
                        {
                            var olddetail = ObjectMapper.Map<JobOldSystemDetail>(jobolddetail);
                            olddetail.JobId = job.Id;
                            if (AbpSession.TenantId != null)
                            {
                                olddetail.TenantId = (int)AbpSession.TenantId;
                            }
                            _JobOldSysDetail.Insert(olddetail);
                        }
                    }
                }
               

                if (list.Count > 0)
                {
                    await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
                    await _dbcontextprovider.GetDbContext().SaveChangesAsync();
                }

                if (input.IsLeadAddresschange)
                {
                    var lead = await _lookup_leadRepository.GetAll().Where(e => e.Id == job.LeadId).FirstOrDefaultAsync();
                    lead.State = job.State;
                    lead.Address = job.Address;
                    lead.Suburb = job.Suburb;
                    lead.UnitNo = job.UnitNo;
                    lead.UnitType = job.UnitType;
                    lead.StreetNo = job.StreetNo;
                    lead.StreetType = job.StreetType;
                    lead.StreetName = job.StreetName;
                    lead.PostCode = job.PostalCode;

                    await _jobRepository.UpdateAsync(job);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task CreateGreenBoatData(int jobid, int ProviderId)
        {
            try
            {
                var stcProvider = await _stcProviderRepository.GetAsync(ProviderId);

                var jobdetail = _jobRepository.GetAll().Where(e => e.Id == jobid).FirstOrDefault();

                var installerDetail = _installerDetail.GetAll().Where(e => e.UserId == jobdetail.InstallerId).FirstOrDefault();
                var installer = _userRepository.GetAll().Where(e => e.Id == jobdetail.InstallerId).FirstOrDefault();

                var inverterid = ((from item in _jobProductItemRepository.GetAll()
                                   join i in _productItemRepository.GetAll() on item.ProductItemId equals i.Id into itmgoup
                                   from i in itmgoup
                                   where (i.ProductTypeId == 2 && item.JobId == jobid)
                                   select (item.ProductItemId)).FirstOrDefault());
                var pannelid = ((from item in _jobProductItemRepository.GetAll()
                                 join i in _productItemRepository.GetAll() on item.ProductItemId equals i.Id into itmgoup
                                 from i in itmgoup
                                 where (i.ProductTypeId == 1 && item.JobId == jobid)
                                 select (item.ProductItemId)).FirstOrDefault());

                var batteryId = ((from item in _jobProductItemRepository.GetAll()
                                  join i in _productItemRepository.GetAll() on item.ProductItemId equals i.Id into itmgoup
                                  from i in itmgoup
                                  where (i.ProductTypeId == 5 && item.JobId == jobid)
                                  select (item.ProductItemId)).FirstOrDefault());

                var designerDetail = _installerDetail.GetAll().Where(e => e.UserId == jobdetail.DesignerId).FirstOrDefault();
                var designer = _userRepository.GetAll().Where(e => e.Id == jobdetail.DesignerId).FirstOrDefault();

                var electricianDetail = _installerDetail.GetAll().Where(e => e.UserId == jobdetail.ElectricianId).FirstOrDefault();
                var electrician = _userRepository.GetAll().Where(e => e.Id == jobdetail.ElectricianId).FirstOrDefault();
                var projecttypeid = jobdetail.JobTypeId;
                string ProjectTypeName = "";
                if (projecttypeid == 1)
                {
                    ProjectTypeName = "New";
                }
                if (projecttypeid == 2)
                {
                    ProjectTypeName = "Adding";
                }
                if (projecttypeid == 3)
                {
                    ProjectTypeName = "Replacing";
                }
                string JobNumber = jobdetail.JobNumber.Substring(2);
                var Leaddetail = _lookup_leadRepository.GetAll().Where(e => e.Id == jobdetail.LeadId).FirstOrDefault();
                var organizationdetail = _extendOrganizationUnitRepository.GetAll().Where(e => e.Id == Leaddetail.OrganizationId).FirstOrDefault();
                var s = Leaddetail.CompanyName;
                var firstSpaceIndex = s.IndexOf(" ");

                var firstname = "";
                var lastname = "";
                if (firstSpaceIndex >= 0)
                {
                    firstname = s.Substring(0, firstSpaceIndex);
                    lastname = s.Substring(firstSpaceIndex);
                }
                else
                {
                    firstname = s;
                }

                string typeofconnection = "Connected to an electricity grid without battery storage";
                string SystemMountingType = "Building or structure";

                HttpClient client = new HttpClient();
                JObject oJsonObject = new JObject();
                //oJsonObject.Add("Username", "solarbridgeregistry");
                //oJsonObject.Add("Password", "solarbridge123");
                //oJsonObject.Add("Username", organizationdetail.GreenBoatUsername);
                //oJsonObject.Add("Password", organizationdetail.GreenBoatPassword); 
                oJsonObject.Add("Username", stcProvider.UserId);
                oJsonObject.Add("Password", stcProvider.Password);


                if (stcProvider.Provider == "Greenboat")
                {

                    var oTaskPostAsync = await client.PostAsync("https://api.greenbot.com.au/api/Account/Login", new StringContent(oJsonObject.ToString(), Encoding.UTF8, "application/json"));
                    bool IsSuccess = oTaskPostAsync.IsSuccessStatusCode;
                    if (IsSuccess)
                    {
                        var tokenJson = await oTaskPostAsync.Content.ReadAsStringAsync();
                        RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(tokenJson);
                        string AccessToken = RootObject.TokenData.access_token;

                        string litmessage = AccessToken.ToString();
                        client = new HttpClient();
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);

                        //Panel Details
                        var panels = _jobProductItemRepository.GetAll().Where(e => e.JobId == jobid && e.ProductItemFk.ProductTypeId == 1).Select(e => e.Quantity).FirstOrDefault();
                        var panelName = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Id == pannelid).Select(e => e.Manufacturer).FirstOrDefault();
                        var PanelModel = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Id == pannelid).Select(e => e.Model).FirstOrDefault();

                        var panellist = new List<Panel>();
                        panellist.Add(new Panel { Brand = panelName, Model = PanelModel, NoOfPanel = Convert.ToString(panels) });

                        //Inverter Details
                        var inverterlist = new List<Inverter>();

                        if (inverterid > 0)
                        {
                            var InverterNname = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == inverterid).Select(e => e.Manufacturer).FirstOrDefault();
                            var InverterModel = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == inverterid).Select(e => e.Model).FirstOrDefault();
                            var InverterSeries = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == inverterid).Select(e => e.Series).FirstOrDefault();
                            var Inverters = _jobProductItemRepository.GetAll().Where(e => e.JobId == jobid && e.ProductItemFk.ProductTypeId == 2).Select(e => e.Quantity).FirstOrDefault();

                            inverterlist.Add(new Inverter { Brand = InverterNname, Model = InverterModel, Series = InverterSeries, noofinverter = (int)Inverters });
                        }


                        //Battery Details
                        var btryName = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 5 && e.Id == batteryId).Select(e => e.Manufacturer).FirstOrDefault();
                        var btryModel = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 5 && e.Id == batteryId).Select(e => e.Model).FirstOrDefault();

                        var batterylist = new List<BatteryManufacturer>();
                        if (btryName != null && btryModel != null)
                        {
                            batterylist.Add(new BatteryManufacturer { Brand = btryName, Model = btryModel, BatterySystemPartOfAnAggregatedControl = "No", ChangedSettingOfBatteryStorageSystem = "No" });
                        }

                        //var testlist = new List<JobStcDetailsPanelBrand>();
                        //testlist.Add(new JobStcDetailsPanelBrand { Brand = stProject.PanelBrand, Model = stProject.PanelModel });
                        //testlist.Add(new JobStcDetailsPanelBrand { Brand = stProject.PanelBrand, Model = stProject.PanelModel });
                        try
                        {
                            var content = new CreateJob
                            {

                                VendorJobId = Convert.ToInt32(JobNumber),
                                BasicDetails = new BasicDetails
                                {

                                    JobType = 1,
                                    Jobstatus = 1,
                                    //JobType = _lookup_jobTypeRepository.GetAll().Where(e=>e.Id==jobdetail.JobTypeId).Select(e=>e.Name).FirstOrDefault(),
                                    RefNumber = JobNumber,
                                    JobStage = 1,
                                    Title = Leaddetail.CompanyName,
                                    StrInstallationDate = Convert.ToDateTime(jobdetail.InstallationDate.Value.ToString("yyyy-MM-dd")),
                                    InstallationDate = Convert.ToDateTime(jobdetail.InstallationDate.Value.ToString("yyyy-MM-dd")),
                                    Priority = 1,
                                    Description = jobdetail.Note

                                },
                                JobOwnerDetails = new JobOwnerDetails
                                {

                                    OwnerType = "Individual",
                                    //CompanyName = Leaddetail.CompanyName,
                                    FirstName = firstname,
                                    LastName = lastname,
                                    Email = Leaddetail.Email,
                                    Phone = Leaddetail.Mobile,
                                    Mobile = Leaddetail.Mobile,
                                    AddressID = 1,
                                    //UnitTypeID = Leaddetail.UnitType,
                                    UnitTypeID = _unitTypesRepository.GetAll().Where(e => e.Name == Leaddetail.UnitType).Select(e => e.Id).FirstOrDefault(),
                                    UnitNumber = Leaddetail.UnitNo,
                                    StreetName = Leaddetail.StreetName,
                                    StreetNumber = Leaddetail.StreetNo,
                                    StreetTypeId = _StreetTypeRepositery.GetAll().Where(e => e.Name == Leaddetail.StreetType).Select(e => (int)e.StreetTypeID).FirstOrDefault(),
                                    Town = Leaddetail.Suburb,
                                    State = Leaddetail.State,
                                    PostCode = Leaddetail.PostCode,
                                    IsPostalAddress = false
                                },
                                JobInstallationDetails = new JobInstallationDetails
                                {
                                    AddressId = 1,
                                    //UnitTypeID = Leaddetail.UnitType,
                                    //UnitNumber = Leaddetail.UnitNo,
                                    //StreetName = Leaddetail.StreetName,
                                    //StreetNumber = Leaddetail.StreetNo,
                                    //StreetTypeId = (int)_StreetTypeRepositery.GetAll().Where(e => e.Name == Leaddetail.StreetType).Select(e => e.StreetTypeID).FirstOrDefault(),
                                    //Town = Leaddetail.Suburb,
                                    //State = Leaddetail.State,
                                    //PostCode = Leaddetail.PostCode,

                                    //DistributorID = (int)_ElecDistributorRepositery.GetAll().Where(e => e.Id == jobdetail.ElecDistributorId).Select(e => e.GreenBoatDistributor).FirstOrDefault(),
                                    //ElectricityProviderID = (int)_ElecRetailerRepositery.GetAll().Where(e => e.Id == jobdetail.ElecRetailerId).Select(e => e.ElectricityProviderId).FirstOrDefault(),
                                    //NMI = jobdetail.NMINumber,
                                    //MeterNumber = jobdetail.MeterNumber,
                                    //PhaseProperty = Convert.ToString(jobdetail.MeterPhaseId),
                                    //PropertyType = Leaddetail.Type,
                                    //SingleMultipleStory = _HouseTypeRepositery.GetAll().Where(e => e.Id == jobdetail.HouseTypeId).Select(e => e.Name).FirstOrDefault(),
                                    //InstallingNewPanel = ProjectTypeName,
                                    //UnitTypeID = jobdetail.UnitType,
                                    UnitTypeID = _unitTypesRepository.GetAll().Where(e => e.Name == jobdetail.UnitType).Select(e => e.Id).FirstOrDefault(),
                                    UnitNumber = jobdetail.UnitNo,
                                    StreetName = jobdetail.StreetName,
                                    StreetNumber = jobdetail.StreetNo,
                                    StreetTypeId = (int)_StreetTypeRepositery.GetAll().Where(e => e.Name == jobdetail.StreetType).Select(e => e.StreetTypeID).FirstOrDefault(),
                                    Town = jobdetail.Suburb,
                                    State = jobdetail.State,
                                    PostCode = jobdetail.PostalCode,

                                    DistributorID = (int)_ElecDistributorRepositery.GetAll().Where(e => e.Id == jobdetail.ElecDistributorId).Select(e => e.GreenBoatDistributor).FirstOrDefault(),
                                    ElectricityProviderID = (int)_ElecRetailerRepositery.GetAll().Where(e => e.Id == jobdetail.ElecRetailerId).Select(e => e.ElectricityProviderId).FirstOrDefault(),
                                    NMI = jobdetail.NMINumber,
                                    MeterNumber = jobdetail.MeterNumber,
                                    PhaseProperty = Convert.ToString(jobdetail.MeterPhaseId),
                                    PropertyType = Leaddetail.Type == "Res" ? "Residential" : Leaddetail.Type == "Com" ? "Commercial" : "",
                                    SingleMultipleStory = _HouseTypeRepositery.GetAll().Where(e => e.Id == jobdetail.HouseTypeId).Select(e => e.Name).FirstOrDefault(),
                                    InstallingNewPanel = ProjectTypeName,

                                    ExistingSystem = false
                                },
                                JobSystemDetails = new JobSystemDetails
                                {
                                    SystemSize = Convert.ToDecimal(jobdetail.SystemCapacity),
                                    //InstallationType = "Other",
                                    //SystemModel = "TestModel",
                                    NoOfPanel = (int)panels
                                },
                                JobStcDetails = new JobStcDetails
                                {
                                    TypeOfConnection = typeofconnection,
                                    SystemMountingType = SystemMountingType,
                                    DeemingPeriod = Convert.ToString(_STCYearWiseRatesRepositery.GetAll().Where(e => e.Year == DateTime.Now.Year).Select(e => e.Rate).FirstOrDefault()),
                                    InstallationDate = Convert.ToDateTime(jobdetail.InstallationDate.Value.ToString("yyyy-MM-dd HH:mm:ss.fff")),
                                    //Systembrand = "",
                                    //SystemModel = "",
                                    //Installationpostcode = "",
                                    MultipleSguAddress = "No"
                                },
                                Panel = panellist.ToArray(),
                                Inverter = inverterlist != null ? inverterlist.ToArray() : new Inverter[0],
                                BatteryManufacturer = batterylist.ToArray(),
                                InstallerView = new InstallerView
                                {
                                    CECAccreditationNumber = installerDetail.InstallerAccreditationNumber,
                                    SeDesignRoleId = (int)jobdetail.DesignerId,
                                    //SeDesignRoleId = (int)installerDetail.DesignerLicenseNumber,
                                    FirstName = installer.Name,
                                    LastName = installer.Surname,
                                    Phone = installer.Mobile,
                                    Mobile = installer.Mobile,

                                    AddressID = 1,

                                    StreetName = installerDetail.StreetName,
                                    StreetNumber = installerDetail.StreetNo,
                                    StreetTypeId = 1,
                                    Town = installerDetail.Suburb,
                                    State = _StateRepositery.GetAll().Where(e => e.Id == installerDetail.StateId).Select(e => e.Name).FirstOrDefault(),
                                    PostCode = installerDetail.PostCode,
                                    IsPostalAddress = true,
                                    IsAutoAddVisit = true
                                },
                                DesignerView = new DesignerView
                                {
                                    CECAccreditationNumber = designerDetail.InstallerAccreditationNumber,
                                    IsUpdate = false,
                                    //Email = "richa@meghtechnologies.com",
                                    FirstName = designer.Name,
                                    LastName = designer.Surname,
                                    Phone = designer.Mobile,
                                    Mobile = designer.Mobile,
                                    IsPostalAddress = true,

                                    AddressID = 1,
                                    //UnitTypeID = 1,
                                    //UnitNumber = "1",
                                    StreetName = designerDetail.StreetName,
                                    StreetNumber = designerDetail.StreetNo,
                                    StreetTypeId = 1,
                                    Town = designerDetail.Suburb,
                                    State = _StateRepositery.GetAll().Where(e => e.Id == designerDetail.StateId).Select(e => e.Name).FirstOrDefault(),
                                    PostCode = designerDetail.PostCode,

                                    SeDesignRoleId = 1,
                                },
                                JobElectricians = new JobElectricians
                                {
                                    CECAccreditationNumber = electricianDetail.InstallerAccreditationNumber,
                                    LicenseNumber = electricianDetail.ElectricianLicenseNumber,
                                    //Email = "richa@meghtechnologies.com",
                                    FirstName = electrician.Name,
                                    LastName = electrician.Surname,
                                    Phone = electrician.Mobile,
                                    Mobile = electrician.Mobile,
                                    IsPostalAddress = true,
                                    //ElectricalContractorsLicenseNumber = "9876543214",
                                    AddressID = 1,
                                    //UnitTypeID = 1,
                                    //UnitNumber = "1",
                                    StreetName = electricianDetail.StreetName,
                                    StreetNumber = electricianDetail.StreetNo,
                                    StreetTypeId = 1,
                                    Town = electricianDetail.Suburb,
                                    State = _StateRepositery.GetAll().Where(e => e.Id == electricianDetail.StateId).Select(e => e.Name).FirstOrDefault(),
                                    PostCode = electricianDetail.PostCode,

                                    SeDesignRoleId = 1,
                                }


                            };

                            if (jobdetail.IsGreenBoatSaved != true)
                            {
                                var temp = JsonConvert.SerializeObject(content, Formatting.Indented);
                                // lbltesting.Text = temp.ToString();
                                var oTaskPostAsyncJob = await client.PostAsync("https://api.greenbot.com.au/api/Account/CreateJob", new StringContent(temp.ToString(), Encoding.UTF8, "application/json"));
                                bool IsSuccessJob = oTaskPostAsyncJob.IsSuccessStatusCode;

                                var JsonString = await oTaskPostAsyncJob.Content.ReadAsStringAsync();
                                RootObj RootObjectJob = JsonConvert.DeserializeObject<RootObj>(JsonString);

                                if (IsSuccessJob == true)
                                {

                                    jobdetail.IsGreenBoatSaved = true;
                                    _jobRepository.Update(jobdetail);
                                }
                                litmessage = RootObjectJob.Message;

                                throw new UserFriendlyException(litmessage);

                            }
                            else
                            {
                                var temp = JsonConvert.SerializeObject(content, Formatting.Indented);
                                var oTaskPostAsyncJob = await client.PostAsync("https://api.greenbot.com.au/api/Account/UpdateJob", new StringContent(temp.ToString(), Encoding.UTF8, "application/json"));
                                bool IsSuccessJob = oTaskPostAsyncJob.IsSuccessStatusCode;

                                var JsonString = await oTaskPostAsyncJob.Content.ReadAsStringAsync();
                                RootObj RootObjectJob = JsonConvert.DeserializeObject<RootObj>(JsonString);

                                litmessage = RootObjectJob.Message;
                                //if (IsSuccessJob != true)
                                //{
                                throw new UserFriendlyException(litmessage);
                                //}
                            }
                        }
                        catch (Exception e)
                        {
                            litmessage = e.Message;
                            // PanError.Visible = true;
                            throw new UserFriendlyException(litmessage);
                        }
                    }

                }
                else if (stcProvider.Provider == "Bridge Select")
                {
                    //var oTaskPostAsync = await client.PostAsync("https://bridgeselect.com.au/scanner/web/login.html", new StringContent(oJsonObject.ToString(), Encoding.UTF8, "application/json"));
                    // bool IsSuccess = oTaskPostAsync.IsSuccessStatusCode;
                    bool IsSuccess = true;
                    if (IsSuccess)
                    {
                        //var tokenJson = await oTaskPostAsync.Content.ReadAsStringAsync();
                        //RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(tokenJson);
                        //string AccessToken = RootObject.TokenData.access_token;

                        //string litmessage = AccessToken.ToString();
                        //client = new HttpClient();
                        //client.DefaultRequestHeaders.Accept.Clear();
                        //client.DefaultRequestHeaders.Accept.Add(
                        //new MediaTypeWithQualityHeaderValue("application/json"));
                        //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);

                        ////Panel Details
                        //var panels = _jobProductItemRepository.GetAll().Where(e => e.JobId == jobid && e.ProductItemFk.ProductTypeId == 1).Select(e => e.Quantity).FirstOrDefault();
                        //var panelName = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Id == pannelid).Select(e => e.Manufacturer).FirstOrDefault();
                        //var PanelModel = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Id == pannelid).Select(e => e.Model).FirstOrDefault();

                        //var panellist = new List<Panel>();
                        //panellist.Add(new Panel { Brand = panelName, Model = PanelModel, NoOfPanel = Convert.ToString(panels) });

                        ////Inverter Details
                        //var inverterlist = new List<Inverter>();

                        //if (inverterid > 0)
                        //{
                        //    var InverterNname = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == inverterid).Select(e => e.Manufacturer).FirstOrDefault();
                        //    var InverterModel = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == inverterid).Select(e => e.Model).FirstOrDefault();
                        //    var InverterSeries = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == inverterid).Select(e => e.Series).FirstOrDefault();
                        //    var Inverters = _jobProductItemRepository.GetAll().Where(e => e.JobId == jobid && e.ProductItemFk.ProductTypeId == 2).Select(e => e.Quantity).FirstOrDefault();

                        //    inverterlist.Add(new Inverter { Brand = InverterNname, Model = InverterModel, Series = InverterSeries, noofinverter = (int)Inverters });
                        //}


                        ////Battery Details
                        //var btryName = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 5 && e.Id == batteryId).Select(e => e.Manufacturer).FirstOrDefault();
                        //var btryModel = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 5 && e.Id == batteryId).Select(e => e.Model).FirstOrDefault();

                        //var batterylist = new List<BatteryManufacturer>();
                        //if (btryName != null && btryModel != null)
                        //{
                        //    batterylist.Add(new BatteryManufacturer { Brand = btryName, Model = btryModel, BatterySystemPartOfAnAggregatedControl = "No", ChangedSettingOfBatteryStorageSystem = "No" });
                        //}

                        //var testlist = new List<JobStcDetailsPanelBrand>();
                        //testlist.Add(new JobStcDetailsPanelBrand { Brand = stProject.PanelBrand, Model = stProject.PanelModel });
                        //testlist.Add(new JobStcDetailsPanelBrand { Brand = stProject.PanelBrand, Model = stProject.PanelModel });
                        try
                        {
                            var content = new BridgeSelectJobDto
                            {
                                crmid = "1234",
                                at = "Physical",
                                e = "aberry@kickstartagile.com",
                                fn = "Anu",
                                id = "06/05/2019",
                                ie = "aberry@kickstartagile.com",
                                ifn = "Anu",
                                ilat = -38.043408,
                                iln = "Berry",
                                ilng = 145.32377999999994,
                                im = "+61411297857",
                                ipa = "2 Grand Arch ",
                                ipc = 3806,
                                iph = "+61411297857",
                                ipra = "Grand Arch ",
                                isb = "Berwick",
                                ist = "VIC",
                                istn = "2",
                                istp = "WAY"
                            };

                            //if (jobdetail.IsGreenBoatSaved != true)
                            //{
                            //    var temp = JsonConvert.SerializeObject(content, Formatting.Indented);
                            //    // lbltesting.Text = temp.ToString();
                            //    var oTaskPostAsyncJob = await client.PostAsync("https://api.greenbot.com.au/api/Account/CreateJob", new StringContent(temp.ToString(), Encoding.UTF8, "application/json"));
                            //    bool IsSuccessJob = oTaskPostAsyncJob.IsSuccessStatusCode;

                            //    var JsonString = await oTaskPostAsyncJob.Content.ReadAsStringAsync();
                            //    RootObj RootObjectJob = JsonConvert.DeserializeObject<RootObj>(JsonString);

                            //    if (IsSuccessJob == true)
                            //    {

                            //        jobdetail.IsGreenBoatSaved = true;
                            //        _jobRepository.Update(jobdetail);
                            //    }
                            //    litmessage = RootObjectJob.Message;

                            //    throw new UserFriendlyException(litmessage);

                            //}
                            //else
                            //{
                            //    var temp = JsonConvert.SerializeObject(content, Formatting.Indented);
                            //    var oTaskPostAsyncJob = await client.PostAsync("https://api.greenbot.com.au/api/Account/UpdateJob", new StringContent(temp.ToString(), Encoding.UTF8, "application/json"));
                            //    bool IsSuccessJob = oTaskPostAsyncJob.IsSuccessStatusCode;

                            //    var JsonString = await oTaskPostAsyncJob.Content.ReadAsStringAsync();
                            //    RootObj RootObjectJob = JsonConvert.DeserializeObject<RootObj>(JsonString);

                            //    litmessage = RootObjectJob.Message;
                            //    //if (IsSuccessJob != true)
                            //    //{
                            //    throw new UserFriendlyException(litmessage);
                            //    //}
                            //}
                            //var serializer = new System.Web.Extensions.JavaScriptSerializer();

                            //var tempdata = serializer.Serialize(content);
                            //var key = "2eb5dfbfdb47097596adbe59ddc5ce293199f83b8dd496378538bc526c901e88ret";
                            //var k = key.replace(/[^A - Z0 - 9 ^ -] +/ ig, "_");
                            var tempdata = JsonConvert.SerializeObject(content);
                            //var data = JsonConvert.ToString(tempdata);
                            //var blob = Convert.FromBase64String();
                            //var json = Encoding.UTF8.GetString(blob);

                            var textBytes = Encoding.UTF8.GetBytes(tempdata);
                            // after: 72 101 108 108 111 32 119 111 114 108 100 33 
                            var base64String = Convert.ToBase64String(textBytes);
                            // string: "{Key : 'abc', isExists: 'true'}"

                            //var obj = JsonConvert.DeserializeObject<BridgeSelectJobDto>(json);
                            var objdata = new
                            {
                                data = base64String,
                                csum = "E2873E5980CBA972873F4F17F9D37D6063A40AB081F6F001626C9BD3016AD639"
                            };
                            var temp = JsonConvert.SerializeObject(objdata, Formatting.Indented);
                            var oTaskPostAsyncJob = await client.PostAsync("https://bridgeselect.com.au/connector/2eb5dfbfdb47097596adbe59ddc5ce293199f83b8dd496378538bc526c901e88ret/job/create-or-edit", new StringContent(temp.ToString(), Encoding.UTF8, "application/json"));
                            bool IsSuccessJob = oTaskPostAsyncJob.IsSuccessStatusCode;

                            var JsonString = await oTaskPostAsyncJob.Content.ReadAsStringAsync();
                            RootObj RootObjectJob = JsonConvert.DeserializeObject<RootObj>(JsonString);


                           // litmessage = RootObjectJob.Message;
                            //if (IsSuccessJob != true)
                            //{
                            //throw new UserFriendlyException(litmessage);
                        }
                        catch (Exception e)
                        {
                            //litmessage = e.Message;
                            // PanError.Visible = true;
                           // throw new UserFriendlyException(litmessage);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                //litmessage  = e.Message;
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task CreatePaywayData(PayWayTokenData jobdata)
        {
            try
            {
                {
                    var jobdetail = _jobRepository.GetAll().Where(e => e.Id == jobdata.jobid).FirstOrDefault();
                    string JobNumber = jobdetail.JobNumber.Substring(2);
                    var expiryDateMonth = "";
                    if (jobdata.expiryDateMonth < 10)
                    {
                        var expmnh = 0 + "" + jobdata.expiryDateMonth;
                        expiryDateMonth = Convert.ToString(expmnh);
                    }
                    else
                    {
                        expiryDateMonth = Convert.ToString(jobdata.expiryDateMonth);
                    }
                    var expiryDateYear = Convert.ToString(jobdata.expiryDateYear);
                    var cvn = Convert.ToString(jobdata.cvn);
                    var client = new RestClient("https://api.payway.com.au/rest/v1/single-use-tokens");
                    client.Timeout = -1;

                    var org = _lookup_leadRepository.GetAll().Where(e => e.Id == jobdetail.LeadId).Select(e => e.OrganizationId).FirstOrDefault();

                    var organizationdetail = _extendOrganizationUnitRepository.GetAll().Where(e => e.Id == org).FirstOrDefault();
                    //WithOrWithoutSurcharge == 1 means without surcharge and 2 means with surcharge
                    var PublishKey = "";
                    var SecrateKey = "";
                    if (jobdata.WithOrWithoutSurcharge == 1)
                    {
                        PublishKey = organizationdetail.PublishKey;
                        SecrateKey = organizationdetail.SecrateKey;
                    }
                    else if (jobdata.WithOrWithoutSurcharge == 2)
                    {
                        PublishKey = organizationdetail.SurchargePublishKey;
                        SecrateKey = organizationdetail.SurchargeSecrateKey;
                    }
                    var request = new RestRequest(Method.POST);

                    ///publisable key 
                    var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(PublishKey);
                    string val = System.Convert.ToBase64String(plainTextBytes);
                    request.AddHeader("Authorization", "Basic " + val);
                    //request.AddHeader("Authorization", "Basic UTI5NDMxX1BVQl9qbXJrMnh6NnRodDRmcjl2Z3g3ODc1czN5bWRrYW4yeDZic3JjdHp6aWZkd2M5dWJxendhcDVrM25zcjU6");
                    request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                    request.AddParameter("paymentMethod", "creditCard");
                    request.AddParameter("cardNumber", jobdata.cardNumber);
                    request.AddParameter("expiryDateMonth", expiryDateMonth);
                    request.AddParameter("expiryDateYear", expiryDateYear);
                    request.AddParameter("cvn", cvn);
                    request.AddParameter("cardholderName", jobdata.cardholderName);
                    IRestResponse response = client.Execute(request);


                    var odosRecord = JsonConvert.DeserializeObject<PayWayRootObject>(response.Content);

                    //var tokenJson =response.Content;
                    //PayWayRootObject RootObject = JsonConvert.DeserializeObject<PayWayRootObject>(tokenJson);
                    string singleToken = odosRecord.singleUseTokenId;
                    //string merchantId = "26241059";
                    string merchantId = organizationdetail.MerchantId;
                    client = new RestClient("https://api.payway.com.au/rest/v1/customers");
                    client.Timeout = -1;
                    var request1 = new RestRequest(Method.POST);
                    ///SecrateKey key 
                    var plainscrtKeyBytes = System.Text.Encoding.UTF8.GetBytes(SecrateKey);
                    string scrtKeyval = System.Convert.ToBase64String(plainscrtKeyBytes);
                    request1.AddHeader("Authorization", "Basic " + scrtKeyval);
                    // request1.AddHeader("Authorization", "Basic UTI5NDMxX1NFQ19zcDg0aXpwbmcyZnp5eXV5bTc2enNyaXdoZmt3ZTU1a3plM2I2bXQ4dGs3YTVtcnRqYnZ3ZmY1eTN0dDQ6");
                    request1.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                    request1.AddParameter("singleUseTokenId", singleToken);
                    request1.AddParameter("merchantId", merchantId);
                    request1.AddParameter("customerName", JobNumber);
                    IRestResponse response1 = client.Execute(request1);
                    var custdata = JsonConvert.DeserializeObject<PayWayRootObject>(response1.Content);
                    string customerNumber = custdata.customerNumber;
                    //string customerNumber = "500000";

                    var client1 = new RestClient("https://api.payway.com.au/rest/v1/transactions");
                    client.Timeout = -1;
                    var request2 = new RestRequest(Method.POST);
                    request2.AddHeader("singleUseTokenId", singleToken);
                    //Secret
                    var plainSecrateKeyBytes = System.Text.Encoding.UTF8.GetBytes(SecrateKey);
                    string Secrateval = System.Convert.ToBase64String(plainSecrateKeyBytes);
                    request2.AddHeader("Authorization", "Basic " + Secrateval);
                    /// request2.AddHeader("Authorization", "Basic UTI5NDMxX1NFQ19zcDg0aXpwbmcyZnp5eXV5bTc2enNyaXdoZmt3ZTU1a3plM2I2bXQ4dGs3YTVtcnRqYnZ3ZmY1eTN0dDQ6");
                    request2.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                    request2.AddParameter("customerNumber", customerNumber);
                    request2.AddParameter("transactionType", jobdata.paymentMethod);
                    request2.AddParameter("principalAmount", jobdata.principalAmount);
                    request2.AddParameter("currency", "aud");
                    request2.AddParameter("merchantId", merchantId);
                    IRestResponse response2 = client1.Execute(request2);
                    var transactiondata = JsonConvert.DeserializeObject<PayWayTransactionObject>(response2.Content);

                    if (transactiondata != null)
                    {
                        PayWayData gbdata = new PayWayData();
                        gbdata.jobId = jobdata.jobid;
                        gbdata.transactionId = transactiondata.transactionId;
                        gbdata.receiptNumber = transactiondata.receiptNumber;
                        gbdata.status = transactiondata.status;
                        gbdata.responseCode = transactiondata.responseCode;
                        gbdata.responseText = transactiondata.responseText;
                        gbdata.transactionType = transactiondata.transactionType;
                        gbdata.customerNumber = transactiondata.customerNumber;
                        gbdata.customerName = transactiondata.customerName;
                        gbdata.currency = transactiondata.currency;
                        gbdata.principalAmount = transactiondata.principalAmount;
                        gbdata.surchargeAmount = transactiondata.surchargeAmount;
                        gbdata.paymentAmount = transactiondata.paymentAmount;
                        gbdata.paymentMethod = transactiondata.paymentMethod;
                        gbdata.PaymentType = jobdata.PaymentType;
                        gbdata.OrganizationId = org;
                        _PayWayDataRepository.Insert(gbdata);

                        if (transactiondata.status == "approved")
                        {
                            using (var uow = _unitOfWorkManager.Begin(TransactionScopeOption.RequiresNew))
                            {
                                var invoicePayment = new InvoicePayment();

                                invoicePayment.UserId = (int)AbpSession.UserId;
                                if (AbpSession.TenantId != null)
                                {
                                    invoicePayment.TenantId = (int)AbpSession.TenantId;
                                }

                                invoicePayment.JobId = jobdata.jobid;
                                invoicePayment.InvoicePayDate = (_timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId));
                                invoicePayment.InvoicePayTotal = transactiondata.paymentAmount;
                                invoicePayment.InvoicePayGST = 0;
                                invoicePayment.InvoicePaymentMethodId = 1; // Credit Card
                                invoicePayment.ReceiptNumber = transactiondata.receiptNumber;
                                invoicePayment.TransactionCode = transactiondata.transactionId;
                                invoicePayment.CCSurcharge = transactiondata.surchargeAmount;
                                invoicePayment.InvoicePaymentStatusId = jobdata.PaymentType;
                                invoicePayment.PaymentNote = "Payment taken from Payway.";
                                invoicePayment.IsVerified = false;
                                invoicePayment.Refund = false;

                                await _invoicePaymentRepository.InsertAsync(invoicePayment);

                                await uow.CompleteAsync();
                            }

                            var Jobs = _jobRepository.GetAll().Where(e => e.Id == jobdata.jobid).FirstOrDefault();
                            var InvoiceCount = _invoicePaymentRepository.GetAll().Where(e => e.JobId == jobdata.jobid).Count();
                            if (InvoiceCount == 1)
                            {
                                Jobs.FirstDepositDate = (_timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId));
                                await _jobRepository.UpdateAsync(Jobs);
                            }
                        }

                        LeadActivityLog leadactivity = new LeadActivityLog();
                        leadactivity.ActionId = 16;
                        leadactivity.SectionId = jobdata.sectionId;
                        leadactivity.ActionNote = "Invoice Payment taken from Payway.";
                        leadactivity.LeadId = Convert.ToInt32(jobdata.jobid);
                        if (AbpSession.TenantId != null)
                        {
                            leadactivity.TenantId = (int)AbpSession.TenantId;
                        }
                        await _leadactivityRepository.InsertAsync(leadactivity);
                    }
                }
            }
            catch (Exception e)

            {
                //litmessage  = e.Message;
                throw new UserFriendlyException(e.Message);
            }

        }
        /// <summary>
        /// Update Job Data From Tracker
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Jobs_Edit, AppPermissions.Pages_ApplicationTracker_AddNotes, AppPermissions.Pages_FinanceTracker_AddDetails
            , AppPermissions.Pages_InvoiceTracker_Verify, AppPermissions.Pages_FreebiesTracker_AddTrackingNumber, AppPermissions.Pages_JobActiveTracker_AddDetails, AppPermissions.Pages_RefundTracker_Refund, AppPermissions.Pages_STCTracker_Edit)]
        public async Task UpdateDataFromTracker(CreateOrEditJobDto input)
        {
            input.DistApproveDate = (_timeZoneConverter.Convert(input.DistApproveDate, (int)AbpSession.TenantId));
            input.DistExpiryDate = (_timeZoneConverter.Convert(input.DistExpiryDate, (int)AbpSession.TenantId));
            input.DistApplied = (_timeZoneConverter.Convert(input.DistApplied, (int)AbpSession.TenantId));
            input.InvPaidDate = (_timeZoneConverter.Convert(input.InvPaidDate, (int)AbpSession.TenantId));

            var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);
            //ObjectMapper.Map(input, job);
            //await _jobRepository.UpdateAsync(job);
            await JobActive(job.Id);
            //   await JobDeposite(job.Id);


            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 11;
            leadactivity.SectionId = input.SectionId != null ? input.SectionId : 0; ;
            leadactivity.ActionNote = "Job Data Updated From Tracker";
            leadactivity.LeadId = Convert.ToInt32(input.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var jobactionid = _leadactivityRepository.InsertAndGetId(leadactivity);

            var list = new List<JobTrackerHistory>();
            if (job.ElecDistributorId != input.ElecDistributorId)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "ElecDistributorId";
                jobhistory.PrevValue = _lookup_elecDistributorRepository.GetAll().Where(e => e.Id == job.ElecDistributorId).Select(e => e.Name).FirstOrDefault();
                jobhistory.CurValue = _lookup_elecDistributorRepository.GetAll().Where(e => e.Id == input.ElecDistributorId).Select(e => e.Name).FirstOrDefault();
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.ElecRetailerId != input.ElecRetailerId)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "ElecRetailerId";
                jobhistory.PrevValue = _lookup_elecRetailerRepository.GetAll().Where(e => e.Id == job.ElecRetailerId).Select(e => e.Name).FirstOrDefault();
                jobhistory.CurValue = _lookup_elecRetailerRepository.GetAll().Where(e => e.Id == input.ElecRetailerId).Select(e => e.Name).FirstOrDefault();
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.MeterUpgradeId != input.MeterUpgradeId)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "MeterUpgradeId";
                jobhistory.PrevValue = _lookup_meterUpgradeRepository.GetAll().Where(e => e.Id == job.MeterUpgradeId).Select(e => e.Name).FirstOrDefault();
                jobhistory.CurValue = _lookup_meterUpgradeRepository.GetAll().Where(e => e.Id == input.MeterUpgradeId).Select(e => e.Name).FirstOrDefault();
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (job.NMINumber != input.NMINumber)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "NMINumber";
                jobhistory.PrevValue = job.NMINumber;
                jobhistory.CurValue = input.NMINumber;
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.LotNumber != input.LotNumber)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "LotNumber";
                jobhistory.PrevValue = job.LotNumber;
                jobhistory.CurValue = input.LotNumber;
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.RegPlanNo != input.RegPlanNo)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "RegPlanNo";
                jobhistory.PrevValue = job.RegPlanNo;
                jobhistory.CurValue = input.RegPlanNo;
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.RegPlanNo != input.RegPlanNo)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "RegPlanNo";
                jobhistory.PrevValue = job.RegPlanNo;
                jobhistory.CurValue = input.RegPlanNo;
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.DistApplied != input.DistApplied)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "DistApplied";
                jobhistory.PrevValue = Convert.ToString(job.DistApplied);
                jobhistory.CurValue = Convert.ToString(input.DistApplied);
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.EssentialDistApplied != input.EssentialDistApplied)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "EssentialDistApplied";
                jobhistory.PrevValue = Convert.ToString(job.EssentialDistApplied);
                jobhistory.CurValue = Convert.ToString(input.EssentialDistApplied);
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.EssentialApprovalRef != input.EssentialApprovalRef)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "EssentialApprovalRef";
                jobhistory.PrevValue =job.EssentialApprovalRef;
                jobhistory.CurValue =input.EssentialApprovalRef;
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (job.DistAppliedDate != input.DistAppliedDate)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "DistAppliedDate";
                jobhistory.PrevValue = Convert.ToString(job.DistAppliedDate);
                jobhistory.CurValue = Convert.ToString(input.DistAppliedDate);
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.ApprovalRef != input.ApprovalRef)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "ApprovalRef";
                jobhistory.PrevValue = job.ApprovalRef;
                jobhistory.CurValue = input.ApprovalRef;
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.DistApproveDate != input.DistApproveDate)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "DistApproveDate";
                jobhistory.PrevValue = Convert.ToString(job.DistApproveDate);
                jobhistory.CurValue = Convert.ToString(input.DistApproveDate);
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.ExpiryDate != input.ExpiryDate)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "ExpiryDate";
                jobhistory.PrevValue = Convert.ToString(job.ExpiryDate);
                jobhistory.CurValue = Convert.ToString(input.ExpiryDate);
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.ApprovedCapacityonExport != input.ApprovedCapacityonExport)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "ApprovedCapacityonExport";
                jobhistory.PrevValue = Convert.ToString(job.ApprovedCapacityonExport);
                jobhistory.CurValue = Convert.ToString(input.ApprovedCapacityonExport);
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.ApprovedCapacityonNonExport != input.ApprovedCapacityonNonExport)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "ApprovedCapacityonNonExport";
                jobhistory.PrevValue = Convert.ToString(job.ApprovedCapacityonNonExport);
                jobhistory.CurValue = Convert.ToString(input.ApprovedCapacityonNonExport);
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.Applicationfeespaid != input.Applicationfeespaid)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "ApprovedCapacityonNonExport";
                jobhistory.PrevValue = Convert.ToString(job.Applicationfeespaid);
                jobhistory.CurValue = Convert.ToString(input.Applicationfeespaid);
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.PeakMeterNo != input.PeakMeterNo)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "PeakMeterNo";
                jobhistory.PrevValue = Convert.ToString(job.PeakMeterNo);
                jobhistory.CurValue = Convert.ToString(input.PeakMeterNo);
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.OffPeakMeter != input.OffPeakMeter)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "OffPeakMeter";
                jobhistory.PrevValue = Convert.ToString(job.OffPeakMeter);
                jobhistory.CurValue = Convert.ToString(input.OffPeakMeter);
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.MeterPhaseId != input.MeterPhaseId)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "MeterPhaseId";
                jobhistory.PrevValue = _lookup_meterPhaseRepository.GetAll().Where(e => e.Id == job.MeterPhaseId).Select(e => e.Name).FirstOrDefault();
                jobhistory.CurValue = _lookup_meterPhaseRepository.GetAll().Where(e => e.Id == input.MeterPhaseId).Select(e => e.Name).FirstOrDefault();
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.ApplicationNotes != input.ApplicationNotes)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "ApplicationNotes";
                jobhistory.PrevValue = job.ApplicationNotes;
                jobhistory.CurValue = input.ApplicationNotes;
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.EnoughMeterSpace != input.EnoughMeterSpace)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "EnoughMeterSpace";
                jobhistory.PrevValue = Convert.ToString(job.EnoughMeterSpace);
                jobhistory.CurValue = Convert.ToString(input.EnoughMeterSpace);
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.IsSystemOffPeak != input.EnoughMeterSpace)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "IsSystemOffPeak";
                jobhistory.PrevValue = Convert.ToString(job.IsSystemOffPeak);
                jobhistory.CurValue = Convert.ToString(input.IsSystemOffPeak);
                jobhistory.Action = "Application Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            var ext = Path.GetExtension(input.FileName);
            string filename = DateTime.Now.Ticks + "_" + "ApprovalLetter" + ext;
            byte[] ByteArray = _tempFileCacheManager.GetFile(input.FileToken);
            var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, filename, "JobApprovalLetter", input.Id, 0, AbpSession.TenantId);

            input.ApprovalLetter_Filename = filename;
            input.ApprovalLetter_FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\" + filepath.JobNumber + "\\" + "JobApprovalLetter" + "\\";

            if (job.ApprovalLetter_Filename != input.ApprovalLetter_Filename)
            {
                JobApprovalFileHistory invoiceStatus = new JobApprovalFileHistory();
                if (AbpSession.TenantId != null)
                {
                    invoiceStatus.TenantId = (int)AbpSession.TenantId;
                }
                invoiceStatus.FieldName = "FileName";
                invoiceStatus.PrevValue = job.ApprovalLetter_Filename;
                invoiceStatus.CurValue = input.ApprovalLetter_Filename;
                invoiceStatus.Action = "Edit";
                invoiceStatus.LastmodifiedDateTime = DateTime.Now;
                invoiceStatus.jobid = job.Id;
                await _JobApprovalFileHistoryRepository.InsertAsync(invoiceStatus);
            }
            if (job.ApprovalLetter_FilePath != input.ApprovalLetter_FilePath)
            {
                JobApprovalFileHistory invoiceStatus = new JobApprovalFileHistory();
                if (AbpSession.TenantId != null)
                {
                    invoiceStatus.TenantId = (int)AbpSession.TenantId;
                }
                invoiceStatus.FieldName = "FilePath";
                invoiceStatus.PrevValue = job.ApprovalLetter_FilePath;
                invoiceStatus.CurValue = input.ApprovalLetter_FilePath;
                invoiceStatus.Action = "Edit";
                invoiceStatus.LastmodifiedDateTime = DateTime.Now;
                invoiceStatus.jobid = job.Id;
                await _JobApprovalFileHistoryRepository.InsertAsync(invoiceStatus);
            }
            ObjectMapper.Map(input, job);
        }

        /// <summary>
        /// Update Application Tracker
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_ApplicationTracker_AddNotes)]
        public async Task UpdateApplicationTracker(CreateOrEditJobDto input)
        {
            var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, job);

            await _jobRepository.UpdateAsync(job);

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 11;
            leadactivity.SectionId = 0;
            leadactivity.ActionNote = "Job Application Details Updated";
            leadactivity.LeadId = Convert.ToInt32(input.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        /// <summary>
        /// Update Finance Tracker
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>		
        [AbpAuthorize(AppPermissions.Pages_FinanceTracker_AddDetails)]
        public async Task UpdateFinanceTracker(CreateOrEditJobDto input)
        {
            var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, job);

            await _jobRepository.UpdateAsync(job);

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 11;
            leadactivity.ActionNote = "Job Finance Details Updated";
            leadactivity.LeadId = Convert.ToInt32(input.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        [AbpAuthorize(AppPermissions.Pages_JobActiveTracker_AddDetails)]
        public async Task ActiveJob(CreateOrEditJobDto input)
        {
            input.FirstDepositDate = _timeZoneConverter.Convert(input.FirstDepositDate, (int)AbpSession.TenantId);
            input.DepositeRecceivedDate = _timeZoneConverter.Convert(input.DepositeRecceivedDate, (int)AbpSession.TenantId);

            //var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);
            var job = await _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.Id == input.Id).FirstOrDefaultAsync();

            input.FirstDepositDate = job.FirstDepositDate;

            if (job.FirstDepositDate != null)
            {
                PreviousJobStatus jobStstus = new PreviousJobStatus();
                jobStstus.JobId = job.Id;
                if (AbpSession.TenantId != null)
                {
                    jobStstus.TenantId = (int)AbpSession.TenantId;
                }
                if (input.ActiveDate != null)
                {
                    jobStstus.CurrentID = 5;
                }
                else
                {
                    jobStstus.CurrentID = 4;
                }

                jobStstus.PreviousId = (int)job.JobStatusId;
                await _previousJobStatusRepository.InsertAsync(jobStstus);
            }

            #region GoogleSheet Code
            if(AbpSession.TenantId == 6 && job.LeadFk.OrganizationId == 7)
            {
                if (job.DepositeRecceivedDate == null && input.DepositeRecceivedDate != null)
                {
                    await ImportInGoogleSheet(job, input.DepositeRecceivedDate);
                }
            }
            #endregion

            var Notes = "";
            if (job.FirstDepositDate != null)
            {
                Notes = "Job Status Change To " + (job.DepositeRecceivedDate != null ? ("Active With Date " + input.ActiveDate.Value.Date) : ("Deposite Received With Date: " + input.DepositeRecceivedDate.Value.Date));
            }
            else
            {
                var dt = Convert.ToDateTime(input.FirstDepositDate);

                Notes = "First Deposit Date Entered: " + input.FirstDepositDate.Value.Date; //input.FirstDepositDate;
            }

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 14;
            leadactivity.SectionId = 0;
            leadactivity.ActionNote = Notes;
            leadactivity.LeadId = Convert.ToInt32(input.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var jobactionid = _leadactivityRepository.InsertAndGetId(leadactivity);

            var log = new UserActivityLogDto();
            log.ActionId = 14;
            log.ActionNote = Notes;
            log.Section = input.Section;
            await _userActivityLogServiceProxy.Create(log);

            var list = new List<JobTrackerHistory>();

            if (job.ActiveDate != input.ActiveDate)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "DispatchedDate";
                jobhistory.PrevValue = job.ActiveDate.ToString();
                jobhistory.CurValue = input.ActiveDate.ToString();
                jobhistory.Action = "Job Active Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
            ObjectMapper.Map(input, job);

            if (input.ActiveDate != null)
            {
                job.JobStatusId = 5;
            }
            else if (input.DepositeRecceivedDate != null)
            {
                job.JobStatusId = 4;
            }

            await _jobRepository.UpdateAsync(job);
        }

        //[AbpAuthorize(AppPermissions.Pages_Jobs_Edit)]
        public async Task<JobInstallationDto> GetJobInstallationDetailsForEdit(EntityDto input)
        {
            var job = (await _jobRepository.GetAsync(input.Id)).MapTo<JobInstallationDto>();
            if (job.InstallationDate != null)
            {
                job.InstallationDate = job.InstallationDate;
            }

            if (job.InstallerId != null)
            {
                job.InstallerName = _userRepository.GetAll().Where(e => e.Id == job.InstallerId).Select(e => e.FullName).FirstOrDefault();
            }
            if (job.ElectricianId != null)
            {
                job.ElectricianName = _userRepository.GetAll().Where(e => e.Id == job.ElectricianId).Select(e => e.FullName).FirstOrDefault();
            }
            if (job.DesignerId != null)
            {
                job.DesignerName = _userRepository.GetAll().Where(e => e.Id == job.DesignerId).Select(e => e.FullName).FirstOrDefault();
            }

            decimal? postcodeprice = 0;
            var date = _timeZoneConverter.Convert(job.InstallationDate, (int)AbpSession.TenantId);
            if (!string.IsNullOrEmpty(job.PostalCode) && date != null)
            {
                postcodeprice = await _postCodePriceRepository.GetAll().Where(e => e.PostCodePricePeriodFk.Month == date.Value.Month && e.PostCodePricePeriodFk.Year == date.Value.Year && e.PostCode == job.PostalCode).Select(e => e.Price).FirstOrDefaultAsync();
            }

            job.EstimateCost = postcodeprice > 0 ? postcodeprice * job.SystemCapacity : 0; 
             

            return job;
        }

        public string GetJobStatus(EntityDto input)
        {
            var StatusId = _jobRepository.GetAll().Where(e => e.Id == input.Id).Select(e => e.JobStatusId).FirstOrDefault();

            return _lookup_jobStatusRepository.GetAll().Where(e => e.Id == StatusId).Select(e => e.Name).FirstOrDefault();
        }

        public async Task UpdateJobStatus(UpdateJobStatusDto input)
        {
            var job = await _jobRepository.GetAll().Include(e => e.JobStatusFk).Where(e => e.Id == input.Id).FirstOrDefaultAsync();
            var StatusName = input.JobStatusId > 0 ? _lookup_jobStatusRepository.GetAll().Where(e => e.Id == input.JobStatusId).Select(e => e.Name).FirstOrDefault() : "";
            var JobStatusID = (int)job.JobStatusId;
            if (input.JobStatusId != 0)
            {
                PreviousJobStatus jobStstus = new PreviousJobStatus();
                jobStstus.JobId = job.Id;
                if (AbpSession.TenantId != null)
                {
                    jobStstus.TenantId = (int)AbpSession.TenantId;
                }
                jobStstus.CurrentID = (int)input.JobStatusId;
                jobStstus.PreviousId = (int)job.JobStatusId;
                await _previousJobStatusRepository.InsertAsync(jobStstus);
            }

            if (input.JobStatusId == 0) // No need to update Jobstatus. Only For Request of cancelation
            {
                job.JobCancelRequestReason = input.JobCancelRequestReason;
                job.IsJobCancelRequest = true;
                job.JobStatusId = JobStatusID;
                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 23;
                leadactivity.SectionId = input.SectionId;
                leadactivity.ActionNote = "Cancel Request Manually";
                leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                leadactivity.CreatorUserId = AbpSession.UserId;
                await _leadactivityRepository.InsertAsync(leadactivity);

                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == UserDetail.Id).Select(e => e.TeamId).ToList();
                var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Distinct().ToList();

                var User_List = (from user in UserList
                                 join ur in _userRoleRepository.GetAll() on user.UserId equals ur.UserId into urJoined
                                 from ur in urJoined.DefaultIfEmpty()

                                 join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                 from us in usJoined.DefaultIfEmpty()

                                 where us != null && us.DisplayName == "Sales Manager"
                                 select user).Distinct();

                foreach (var item in User_List)
                {
                    var jobnumber = _jobRepository.GetAll().Where(e => e.Id == job.Id).Select(e => e.JobNumber).FirstOrDefault();
                    var user = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).Select(e => e.UserName).FirstOrDefault();
                    var canreq = "Manually Cancel Request is Apply for" + jobnumber + "By" + user + ".";

                    var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == item.UserId).FirstOrDefault();
                    string msg = string.Format(canreq);
                    await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);

                }
            }

            var log = new UserActivityLogDto();
            log.ActionId = input.JobStatusId == 0 ? 23 : 14;
            log.ActionNote = input.JobStatusId == 0 ? "Cancel Request Manually" : "Job Status Change From " + job.JobStatusFk.Name + " To " + StatusName + " Manually";
            log.Section = input.SectionName;
            await _userActivityLogServiceProxy.Create(log);

            if (input.JobStatusId == 1)
            {
                //job.JobHoldReason = input.JobHoldReason;
                //job.NextFollowUpDate = input.NextFollowUpDate;
                //job.JobHoldReasonId = input.JobHoldReasonId;
                job.JobStatusId = input.JobStatusId;
                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 14;
                leadactivity.SectionId = input.SectionId;
                leadactivity.ActionNote = "Job Status Change From " + job.JobStatusFk.Name +" To Planned Manually";
                leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
            if (input.JobStatusId == 2)
            {
                job.JobHoldReason = input.JobHoldReason;
                job.NextFollowUpDate = input.NextFollowUpDate;
                job.JobHoldReasonId = input.JobHoldReasonId;
                job.JobStatusId = input.JobStatusId;
                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 14;
                leadactivity.SectionId = input.SectionId;
                leadactivity.ActionNote = "Job Status Change From " + job.JobStatusFk.Name +" To On Hold Manually";
                leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
            if (input.JobStatusId == 3)
            {
                
                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 14;
                leadactivity.SectionId = input.SectionId;
                leadactivity.ActionNote = "Job Status Change From "+ job.JobStatusFk.Name +" To Cancel Manually";
                leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);

                job.JobCancelReason = input.JobCancelReason;
                job.JobCancelReasonId = input.JobCancelReasonId;
                job.IsRefund = input.IsRefund;
                job.JobStatusId = input.JobStatusId;
            }
            if (input.JobStatusId == 4)
            {
                job.JobStatusId = input.JobStatusId;
                job.DepositeStatusNotes = input.DepositeStatusNotes;
                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 14;
                leadactivity.SectionId = input.SectionId;
                leadactivity.ActionNote = "Job Status Change From "+ job.JobStatusFk.Name +" To Deposite Received Manually";
                leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
            if (input.JobStatusId == 5)
            {
                job.ActiveStatusNotes = input.ActiveStatusNotes;
                job.JobStatusId = input.JobStatusId;
                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 14;
                leadactivity.SectionId = input.SectionId;
                leadactivity.ActionNote = "Job Status Change From "+ job.JobStatusFk.Name +" To Active Manually";
                leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
            await _jobRepository.UpdateAsync(job);
        }

        public async Task UpdateCanacelationRequest(CreateOrEditJobDto input)
        {
            var job = await _jobRepository.GetAsync((int)input.Id);
            var jobstatusid = (int)input.JobStatusId;
            if (input.IsJobCancelRequest == true)
            {
                jobstatusid = 3;
            }
            if (input.IsJobCancelRequest == false)
            {
                jobstatusid = (int)input.JobStatusId;
            }
            PreviousJobStatus jobStstus = new PreviousJobStatus();
            jobStstus.JobId = job.Id;
            if (AbpSession.TenantId != null)
            {
                jobStstus.TenantId = (int)AbpSession.TenantId;
            }
            jobStstus.CurrentID = jobstatusid;
            jobStstus.PreviousId = (int)job.JobStatusId;
            await _previousJobStatusRepository.InsertAsync(jobStstus);

            job.JobStatusId = jobstatusid;
            job.JobCancelReason = input.JobCancelReason;
            job.JobCancelReasonId = input.JobCancelReasonId;
            job.IsRefund = input.IsRefund;
            job.JobCancelRequestReason = input.JobCancelRequestReason;
            job.IsJobCancelRequest = input.IsJobCancelRequest;
            job.JobCancelRejectReason = input.JobCancelRejectReason;

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var log = new UserActivityLogDto();
            
           
            LeadActivityLog leadactivity = new LeadActivityLog();
            if (job.IsJobCancelRequest == true)
            {
                leadactivity.ActionId = 14;

                leadactivity.ActionNote = "Job Status Change To Cancel";
                leadactivity.ActivityNote = "Cancelation Request Approved";
                log.ActionId = input.JobStatusId = 14;
                log.ActionNote = "Job Status Change To Cancel";
            }
            else if (job.IsJobCancelRequest == false)
            {
                leadactivity.ActionId = 23;

                leadactivity.ActionNote = "Cancel Request";
                leadactivity.ActivityNote = "Cancelation Request Reject";
                log.ActionId = input.JobStatusId = 23;
                log.ActionNote = "Cancelation Request Reject";
            }

            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            leadactivity.SectionId = input.SectionId;
            leadactivity.CreatorUserId = AbpSession.UserId;
            await _leadactivityRepository.InsertAsync(leadactivity);

            await _jobRepository.UpdateAsync(job);

            log.Section = input.Section;
            await _userActivityLogServiceProxy.Create(log);

        }

        [AbpAuthorize(AppPermissions.Pages_Jobs_Edit)]
        public async Task UpdateJobInstallationDetails(JobInstallationDto input)
        {
            input.InstallationDate = (_timeZoneConverter.Convert(input.InstallationDate, (int)AbpSession.TenantId));

            var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);

            if (job.JobStatusId >= 5 || job.JobStatusId == 3)
            {
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 13;
                leadactivity.ActionNote = "Installation Details Updated";
                leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                leadactivity.SectionId = input.sectionId;
                var jobactionid = _leadactivityRepository.InsertAndGetId(leadactivity);

                var list = new List<JobTrackerHistory>();
                if (job.InstallationDate != input.InstallationDate)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Installation Date";
                    jobhistory.PrevValue = job.InstallationDate.ToString();
                    jobhistory.CurValue = input.InstallationDate.ToString();
                    jobhistory.Action = "Installation Details Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.InstallationTime != input.InstallationTime)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Installation Time";
                    jobhistory.PrevValue = job.InstallationTime;
                    jobhistory.CurValue = input.InstallationTime;
                    jobhistory.Action = "Installation Details Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.InstallerId != input.InstallerId)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Installer";
                    jobhistory.PrevValue = _userRepository.GetAll().Where(e => e.Id == job.InstallerId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.CurValue = _userRepository.GetAll().Where(e => e.Id == input.InstallerId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.Action = "Installation Details Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.DesignerId != input.DesignerId)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Designer";
                    jobhistory.PrevValue = _userRepository.GetAll().Where(e => e.Id == job.DesignerId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.CurValue = _userRepository.GetAll().Where(e => e.Id == input.DesignerId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.Action = "Installation Details Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.ElectricianId != input.ElectricianId)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Electrician";
                    jobhistory.PrevValue = _userRepository.GetAll().Where(e => e.Id == job.ElectricianId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.CurValue = _userRepository.GetAll().Where(e => e.Id == input.ElectricianId).Select(e => e.Name).FirstOrDefault();
                    jobhistory.Action = "Installation Details Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.WarehouseLocation != input.WarehouseLocation)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Warehouse Location";
                    jobhistory.PrevValue = _warehouselocationRepository.GetAll().Where(e => e.Id == job.WarehouseLocation).Select(e => e.location).FirstOrDefault();
                    jobhistory.CurValue = _warehouselocationRepository.GetAll().Where(e => e.Id == input.WarehouseLocation).Select(e => e.location).FirstOrDefault();
                    jobhistory.Action = "Installation Details Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.InstallationNotes != input.InstallationNotes)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Installer Notes";
                    jobhistory.PrevValue = job.InstallationNotes;
                    jobhistory.CurValue = input.InstallationNotes;
                    jobhistory.Action = "Installation Details Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (input.InstallationDate != null)
                {
                    job.InstallationDate = input.InstallationDate.Value.Date;
                }
                else
                {
                    job.InstallationDate = null;
                }
                //job.InstallationDate = input.InstallationDate.Value.Date;
                job.InstallationNotes = input.InstallationNotes;
                job.InstallationTime = input.InstallationTime;
                job.InstallerId = input.InstallerId;
                job.DesignerId = input.DesignerId;
                job.ElectricianId = input.ElectricianId;
                job.WarehouseLocation = input.WarehouseLocation;
                job.STC = input.STC;
                job.Rebate = input.Rebate;
                //await _jobRepository.UpdateAsync(job);

                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

                if (input.InstallationDate != null)
                {

                    if (job.JobStatusId != 6)
                    {
                        JobTrackerHistory jobhistory = new JobTrackerHistory();
                        if (AbpSession.TenantId != null)
                        {
                            jobhistory.TenantId = (int)AbpSession.TenantId;
                        }
                        jobhistory.FieldName = "Job Status";
                        jobhistory.PrevValue = _lookup_jobStatusRepository.GetAll().Where(e => e.Id == job.JobStatusId).Select(e => e.Name).FirstOrDefault();
                        jobhistory.CurValue = "Job Book";
                        jobhistory.Action = "Job Status Edit";
                        jobhistory.LastmodifiedDateTime = DateTime.Now;
                        jobhistory.JobIDId = job.Id;
                        jobhistory.JobActionId = jobactionid;
                        list.Add(jobhistory);
                    }
                    job.JobStatusId = 6;
                    await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
                    await _dbcontextprovider.GetDbContext().SaveChangesAsync();
                    await _jobRepository.UpdateAsync(job);

                    LeadActivityLog leadactivity1 = new LeadActivityLog();
                    leadactivity1.ActionId = 14;
                    leadactivity1.ActionNote = "Job Status Change To Job Book";
                    leadactivity1.LeadId = Convert.ToInt32(job.LeadId);
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity1.TenantId = (int)AbpSession.TenantId;
                    }
                    leadactivity1.SectionId = 0;
                    await _leadactivityRepository.InsertAsync(leadactivity1);
                }
                else
                {
                    if (job.JobStatusId != 5)
                    {
                        JobTrackerHistory jobhistory = new JobTrackerHistory();
                        if (AbpSession.TenantId != null)
                        {
                            jobhistory.TenantId = (int)AbpSession.TenantId;
                        }
                        jobhistory.FieldName = "Job Status";
                        jobhistory.PrevValue = _lookup_jobStatusRepository.GetAll().Where(e => e.Id == job.JobStatusId).Select(e => e.Name).FirstOrDefault();
                        jobhistory.CurValue = "Active";
                        jobhistory.Action = "Job Status Edit";
                        jobhistory.LastmodifiedDateTime = DateTime.Now;
                        jobhistory.JobIDId = job.Id;
                        jobhistory.JobActionId = jobactionid;
                        list.Add(jobhistory);
                    }
                    job.JobStatusId = 5;
                    await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
                    await _dbcontextprovider.GetDbContext().SaveChangesAsync();
                    await _jobRepository.UpdateAsync(job);

                    LeadActivityLog leadactivity2 = new LeadActivityLog();
                    leadactivity2.ActionId = 14;
                    leadactivity2.ActionNote = "Job Status Change To Active";
                    leadactivity2.LeadId = Convert.ToInt32(job.LeadId);
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity2.TenantId = (int)AbpSession.TenantId;
                    }
                    leadactivity2.SectionId = 0;
                    await _leadactivityRepository.InsertAsync(leadactivity2);
                }
            }
            else
            {

                throw new UserFriendlyException(L("JobIsNotActive"));
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Jobs_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _jobRepository.DeleteAsync(input.Id);
        }

        //public async Task<FileDto> v(GetAllJobsForExcelInput input)
        //{

        //    var filteredJobs = _jobRepository.GetAll()
        //                .Include(e => e.JobTypeFk)
        //                .Include(e => e.JobStatusFk)
        //                .Include(e => e.RoofTypeFk)
        //                .Include(e => e.RoofAngleFk)
        //                .Include(e => e.ElecDistributorFk)
        //                .Include(e => e.LeadFk)
        //                .Include(e => e.ElecRetailerFk)
        //                .Include(e => e.PaymentOptionFk)
        //                .Include(e => e.DepositOptionFk)
        //                //.Include(e => e.MeterUpgradeFk)
        //                //.Include(e => e.MeterPhaseFk)
        //                .Include(e => e.PromotionOfferFk)
        //                .Include(e => e.HouseTypeFk)
        //                .Include(e => e.FinanceOptionFk)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) || e.LotNumber.Contains(input.Filter) || e.PeakMeterNo.Contains(input.Filter) || e.OffPeakMeter.Contains(input.Filter) || e.Suburb.Contains(input.Filter) || e.State.Contains(input.Filter) || e.PostalCode.Contains(input.Filter) || e.UnitNo.Contains(input.Filter) || e.UnitType.Contains(input.Filter) || e.StreetNo.Contains(input.Filter) || e.StreetName.Contains(input.Filter) || e.StreetType.Contains(input.Filter) || e.Latitude.Contains(input.Filter) || e.Longitude.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Country.Contains(input.Filter))
        //                .WhereIf(input.MinPanelOnFlatFilter != null, e => e.PanelOnFlat >= input.MinPanelOnFlatFilter)
        //                .WhereIf(input.MaxPanelOnFlatFilter != null, e => e.PanelOnFlat <= input.MaxPanelOnFlatFilter)
        //                .WhereIf(input.MinPanelOnPitchedFilter != null, e => e.PanelOnPitched >= input.MinPanelOnPitchedFilter)
        //                .WhereIf(input.MaxPanelOnPitchedFilter != null, e => e.PanelOnPitched <= input.MaxPanelOnPitchedFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.RegPlanNoFilter), e => e.RegPlanNo == input.RegPlanNoFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.LotNumberFilter), e => e.LotNumber == input.LotNumberFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.PeakMeterNoFilter), e => e.PeakMeterNo == input.PeakMeterNoFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.OffPeakMeterFilter), e => e.OffPeakMeter == input.OffPeakMeterFilter)
        //                .WhereIf(input.EnoughMeterSpaceFilter > -1, e => (input.EnoughMeterSpaceFilter == 1 && e.EnoughMeterSpace) || (input.EnoughMeterSpaceFilter == 0 && !e.EnoughMeterSpace))
        //                .WhereIf(input.IsSystemOffPeakFilter > -1, e => (input.IsSystemOffPeakFilter == 1 && e.IsSystemOffPeak) || (input.IsSystemOffPeakFilter == 0 && !e.IsSystemOffPeak))
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.RoofTypeNameFilter), e => e.RoofTypeFk != null && e.RoofTypeFk.Name == input.RoofTypeNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.RoofAngleNameFilter), e => e.RoofAngleFk != null && e.RoofAngleFk.Name == input.RoofAngleNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.ElecDistributorNameFilter), e => e.ElecDistributorFk != null && e.ElecDistributorFk.Name == input.ElecDistributorNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.ElecRetailerNameFilter), e => e.ElecRetailerFk != null && e.ElecRetailerFk.Name == input.ElecRetailerNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentOptionNameFilter), e => e.PaymentOptionFk != null && e.PaymentOptionFk.Name == input.PaymentOptionNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.DepositOptionNameFilter), e => e.DepositOptionFk != null && e.DepositOptionFk.Name == input.DepositOptionNameFilter)
        //                //.WhereIf(!string.IsNullOrWhiteSpace(input.MeterUpgradeNameFilter), e => e.MeterUpgradeFk != null && e.MeterUpgradeFk.Name == input.MeterUpgradeNameFilter)
        //                //.WhereIf(!string.IsNullOrWhiteSpace(input.MeterPhaseNameFilter), e => e.MeterPhaseFk != null && e.MeterPhaseFk.Name == input.MeterPhaseNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionOfferNameFilter), e => e.PromotionOfferFk != null && e.PromotionOfferFk.Name == input.PromotionOfferNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.HouseTypeNameFilter), e => e.HouseTypeFk != null && e.HouseTypeFk.Name == input.HouseTypeNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter);

        //    var query = (from o in filteredJobs
        //                 join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
        //                 from s1 in j1.DefaultIfEmpty()

        //                 join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
        //                 from s2 in j2.DefaultIfEmpty()

        //                 join o3 in _lookup_roofTypeRepository.GetAll() on o.RoofTypeId equals o3.Id into j3
        //                 from s3 in j3.DefaultIfEmpty()

        //                 join o4 in _lookup_roofAngleRepository.GetAll() on o.RoofAngleId equals o4.Id into j4
        //                 from s4 in j4.DefaultIfEmpty()

        //                 join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
        //                 from s5 in j5.DefaultIfEmpty()

        //                 join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
        //                 from s6 in j6.DefaultIfEmpty()

        //                 join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
        //                 from s7 in j7.DefaultIfEmpty()

        //                 join o8 in _lookup_paymentOptionRepository.GetAll() on o.PaymentOptionId equals o8.Id into j8
        //                 from s8 in j8.DefaultIfEmpty()

        //                 join o9 in _lookup_depositOptionRepository.GetAll() on o.DepositOptionId equals o9.Id into j9
        //                 from s9 in j9.DefaultIfEmpty()

        //                 join o10 in _lookup_meterUpgradeRepository.GetAll() on o.MeterUpgradeId equals o10.Id into j10
        //                 from s10 in j10.DefaultIfEmpty()

        //                 join o11 in _lookup_meterPhaseRepository.GetAll() on o.MeterPhaseId equals o11.Id into j11
        //                 from s11 in j11.DefaultIfEmpty()

        //                 join o12 in _lookup_promotionOfferRepository.GetAll() on o.PromotionOfferId equals o12.Id into j12
        //                 from s12 in j12.DefaultIfEmpty()

        //                 join o13 in _lookup_houseTypeRepository.GetAll() on o.HouseTypeId equals o13.Id into j13
        //                 from s13 in j13.DefaultIfEmpty()

        //                 join o14 in _lookup_financeOptionRepository.GetAll() on o.FinanceOptionId equals o14.Id into j14
        //                 from s14 in j14.DefaultIfEmpty()

        //                 select new GetJobForViewDto()
        //                 {
        //                     Job = new JobDto
        //                     {
        //                         //PanelOnFlat = o.PanelOnFlat,
        //                         //PanelOnPitched = o.PanelOnPitched,
        //                         RegPlanNo = o.RegPlanNo,
        //                         LotNumber = o.LotNumber,
        //                         //PeakMeterNo = o.PeakMeterNo,
        //                         //OffPeakMeter = o.OffPeakMeter,
        //                         //EnoughMeterSpace = o.EnoughMeterSpace,
        //                         //IsSystemOffPeak = o.IsSystemOffPeak,
        //                         //BasicCost = o.BasicCost,
        //                         //SolarVLCRebate = o.SolarVLCRebate,
        //                         //SolarVLCLoan = o.SolarVLCLoan,
        //                         //TotalCost = o.TotalCost,
        //                         Suburb = o.Suburb,
        //                         State = o.State,
        //                         UnitNo = o.UnitNo,
        //                         UnitType = o.UnitType,
        //                         Id = o.Id
        //                     },
        //                     JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
        //                     JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
        //                     //RoofTypeName = s3 == null || s3.Name == null ? "" : s3.Name.ToString(),
        //                     //RoofAngleName = s4 == null || s4.Name == null ? "" : s4.Name.ToString(),
        //                     ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
        //                     LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
        //                     ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
        //                     //PaymentOptionName = s8 == null || s8.Name == null ? "" : s8.Name.ToString(),
        //                     //DepositOptionName = s9 == null || s9.Name == null ? "" : s9.Name.ToString(),
        //                     //MeterUpgradeName = s10 == null || s10.Name == null ? "" : s10.Name.ToString(),
        //                     //MeterPhaseName = s11 == null || s11.Name == null ? "" : s11.Name.ToString(),
        //                     //PromotionOfferName = s12 == null || s12.Name == null ? "" : s12.Name.ToString(),
        //                     //HouseTypeName = s13 == null || s13.Name == null ? "" : s13.Name.ToString(),
        //                     //FinanceOptionName = s14 == null || s14.Name == null ? "" : s14.Name.ToString()
        //                 });


        //    var jobListDtos = await query.ToListAsync();

        //    return _jobsExcelExporter.ExportToFile(jobListDtos);
        //}

        public async Task<List<JobJobTypeLookupTableDto>> GetAllJobTypeForTableDropdown()
        {
            return await _lookup_jobTypeRepository.GetAll()
                .Select(jobType => new JobJobTypeLookupTableDto
                {
                    Id = jobType.Id,
                    DisplayName = jobType == null || jobType.Name == null ? "" : jobType.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobJobStatusLookupTableDto>> GetAllJobStatusForTableDropdown()
        {
            return await _lookup_jobStatusRepository.GetAll()
                .Where(e => e.Name == "On Hold" || e.Name == "Cancel")
                .Select(jobStatus => new JobJobStatusLookupTableDto
                {
                    Id = jobStatus.Id,
                    DisplayName = jobStatus == null || jobStatus.Name == null ? "" : jobStatus.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobRoofTypeLookupTableDto>> GetAllRoofTypeForTableDropdown()
        {
            return await _lookup_roofTypeRepository.GetAll()
                .Select(roofType => new JobRoofTypeLookupTableDto
                {
                    Id = roofType.Id,
                    DisplayName = roofType == null || roofType.Name == null ? "" : roofType.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobRoofAngleLookupTableDto>> GetAllRoofAngleForTableDropdown()
        {
            return await _lookup_roofAngleRepository.GetAll()
                .Select(roofAngle => new JobRoofAngleLookupTableDto
                {
                    Id = roofAngle.Id,
                    DisplayName = roofAngle == null || roofAngle.Name == null ? "" : roofAngle.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobElecDistributorLookupTableDto>> GetAllElecDistributorForTableDropdown()
        {
            return await _lookup_elecDistributorRepository.GetAll()
                .Select(elecDistributor => new JobElecDistributorLookupTableDto
                {
                    Id = elecDistributor.Id,
                    DisplayName = elecDistributor == null || elecDistributor.Name == null ? "" : elecDistributor.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobLeadLookupTableDto>> GetAllLeadForTableDropdown()
        {
            return await _lookup_leadRepository.GetAll()
                .Select(lead => new JobLeadLookupTableDto
                {
                    Id = lead.Id,
                    DisplayName = lead == null || lead.CompanyName == null ? "" : lead.CompanyName.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobElecRetailerLookupTableDto>> GetAllElecRetailerForTableDropdown()
        {
            return await _lookup_elecRetailerRepository.GetAll()
                .Select(elecRetailer => new JobElecRetailerLookupTableDto
                {
                    Id = elecRetailer.Id,
                    DisplayName = elecRetailer == null || elecRetailer.Name == null ? "" : elecRetailer.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobPaymentOptionLookupTableDto>> GetAllPaymentOptionForTableDropdown()
        {
            return await _lookup_paymentOptionRepository.GetAll()
                .Select(paymentOption => new JobPaymentOptionLookupTableDto
                {
                    Id = paymentOption.Id,
                    DisplayName = paymentOption == null || paymentOption.Name == null ? "" : paymentOption.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobDepositOptionLookupTableDto>> GetAllDepositOptionForTableDropdown()
        {
            return await _lookup_depositOptionRepository.GetAll()
                .Select(depositOption => new JobDepositOptionLookupTableDto
                {
                    Id = depositOption.Id,
                    DisplayName = depositOption == null || depositOption.Name == null ? "" : depositOption.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobMeterUpgradeLookupTableDto>> GetAllMeterUpgradeForTableDropdown()
        {
            return await _lookup_meterUpgradeRepository.GetAll()
                .Select(meterUpgrade => new JobMeterUpgradeLookupTableDto
                {
                    Id = meterUpgrade.Id,
                    DisplayName = meterUpgrade == null || meterUpgrade.Name == null ? "" : meterUpgrade.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobMeterPhaseLookupTableDto>> GetAllMeterPhaseForTableDropdown()
        {
            return await _lookup_meterPhaseRepository.GetAll()
                .Select(meterPhase => new JobMeterPhaseLookupTableDto
                {
                    Id = meterPhase.Id,
                    DisplayName = meterPhase == null || meterPhase.Name == null ? "" : meterPhase.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<InstallerAvailableListDto>> GetAllInstallerAvailabilityList(DateTime AvailableDate, int jobid)
        {
            var InstAvailableDate = (_timeZoneConverter.Convert(AvailableDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.IsActive == true);
            ///DateTime date = Convert.ToDateTime(AvailableDate.ToString("yyyy-MM-dd"));
            var InstallerList = (from u in User
                                 join o1 in _installerDetailRepository.GetAll() on u.Id equals o1.UserId into j1
                                 from s1 in j1.DefaultIfEmpty()
                                 join o3 in _installerAvailabilityRepository.GetAll() on u.Id equals o3.UserId into j3
                                 from s3 in j3.DefaultIfEmpty()
                                     //where o3.AvailabilityDate.Date == InstAvailableDate.Value.Date
                                 let AvailableCount =
                                    (
                                      from o2 in _installerAvailabilityRepository.GetAll()
                                      where o2.UserId == u.Id && o2.AvailabilityDate.Date == InstAvailableDate.Value.Date
                                      select o2
                                    ).Count()
                                 let InstallBookCount =
                                     (
                                       from o2 in _jobRepository.GetAll()
                                       where o2.InstallerId == u.Id && o2.InstallationDate == InstAvailableDate.Value.Date && o2.Id != jobid
                                       select o2
                                     ).Count()
                                 where (s1 != null && s1.IsInst == true)
                                 select new InstallerAvailableListDto
                                 {
                                     Id = (int)u.Id,
                                     DisplayName = u.FullName,
                                     AvailabilityCount = AvailableCount,
                                     BookCount = InstallBookCount,
                                     availbale = true
                                 }).Where(x => x.AvailabilityCount > x.BookCount).Distinct().ToList();

            return InstallerList;
            //}
        }

        public async Task<List<InstallerAvailableListDto>> GetAllElectricianAvailabilityList(DateTime AvailableDate, int jobid)
        {
            var ElecAvailableDate = (_timeZoneConverter.Convert(AvailableDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll();

            var ElectricianList = (from u in User
                                   join o1 in _installerDetailRepository.GetAll() on u.Id equals o1.UserId into j1
                                   from s1 in j1.DefaultIfEmpty()
                                       //join o2 in _installerAvailabilityRepository.GetAll() on u.Id equals o2.UserId into j2
                                       //from s2 in j2.DefaultIfEmpty()
                                   let AvailableCount =
                                      (
                                        from o2 in _installerAvailabilityRepository.GetAll()
                                        where o2.UserId == u.Id && o2.AvailabilityDate.AddHours(10).Date == ElecAvailableDate.Value.Date
                                        select o2
                                      ).Count()
                                   let InstallBookCount =
                                       (
                                         from o2 in _jobRepository.GetAll()
                                         where o2.ElectricianId == u.Id && o2.InstallationDate == ElecAvailableDate.Value.Date && o2.Id != jobid
                                         select o2
                                       ).Count()
                                   where (s1 != null && s1.IsElec == true)
                                   select new InstallerAvailableListDto
                                   {
                                       Id = (int)u.Id,
                                       DisplayName = u.FullName,
                                       AvailabilityCount = AvailableCount,
                                       BookCount = InstallBookCount
                                   }).Where(x => x.AvailabilityCount > x.BookCount).ToList();
            return ElectricianList;
        }

        public async Task<List<InstallerAvailableListDto>> GetAllDesignerAvailabilityList(DateTime AvailableDate, int jobid)
        {
            var DescAvailableDate = (_timeZoneConverter.Convert(AvailableDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll();

            var DesignerList = (from u in User
                                join o1 in _installerDetailRepository.GetAll() on u.Id equals o1.UserId into j1
                                from s1 in j1.DefaultIfEmpty()
                                    //join o2 in _installerAvailabilityRepository.GetAll() on u.Id equals o2.UserId into j2
                                    //from s2 in j2.DefaultIfEmpty()
                                let AvailableCount =
                                   (
                                     from o2 in _installerAvailabilityRepository.GetAll()
                                     where o2.UserId == u.Id && o2.AvailabilityDate.AddHours(10).Date == DescAvailableDate.Value.Date
                                     select o2
                                   ).Count()
                                let InstallBookCount =
                                    (
                                      from o2 in _jobRepository.GetAll()
                                      where o2.DesignerId == u.Id && o2.InstallationDate == DescAvailableDate.Value.Date && o2.Id != jobid
                                      select o2
                                    ).Count()
                                where (s1 != null && s1.IsDesi == true)
                                select new InstallerAvailableListDto
                                {
                                    Id = (int)u.Id,
                                    DisplayName = u.FullName,
                                    AvailabilityCount = AvailableCount,
                                    BookCount = InstallBookCount
                                }).Where(x => x.AvailabilityCount > x.BookCount).ToList();
            return DesignerList;
        }

        public async Task<List<CommonLookupDto>> GetWareHouseDropdown(string stateid)
        {
            var warehOuselist = (from item in _warehouselocationRepository.GetAll()
                                     //where (item.state == stateid)
                                 select new CommonLookupDto
                                 {
                                     Id = item.Id,
                                     DisplayName = item.location,
                                 }).ToList();
            return warehOuselist;
        }

        public async Task<PagedResultDto<JobPromotionOfferLookupTableDto>> GetAllPromotionOfferForLookupTable(GetAllForLookupTableInput input)
        {
            var query = _lookup_promotionOfferRepository.GetAll().WhereIf(
                   !string.IsNullOrWhiteSpace(input.Filter),
                  e => e.Name != null && e.Name.Contains(input.Filter)
               );

            var totalCount = await query.CountAsync();

            var promotionOfferList = await query
                .PageBy(input)
                .ToListAsync();

            var lookupTableDtoList = new List<JobPromotionOfferLookupTableDto>();
            foreach (var promotionOffer in promotionOfferList)
            {
                lookupTableDtoList.Add(new JobPromotionOfferLookupTableDto
                {
                    Id = promotionOffer.Id,
                    DisplayName = promotionOffer.Name?.ToString()
                });
            }

            return new PagedResultDto<JobPromotionOfferLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
        }

        public async Task<List<JobHouseTypeLookupTableDto>> GetAllHouseTypeForTableDropdown()
        {
            return await _lookup_houseTypeRepository.GetAll()
                .Select(houseType => new JobHouseTypeLookupTableDto
                {
                    Id = houseType.Id,
                    DisplayName = houseType == null || houseType.Name == null ? "" : houseType.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobFinanceOptionLookupTableDto>> GetAllFinanceOptionForTableDropdown()
        {
            return await _lookup_financeOptionRepository.GetAll()
                .Select(financeOption => new JobFinanceOptionLookupTableDto
                {
                    Id = financeOption.Id,
                    DisplayName = financeOption == null || financeOption.Name == null ? "" : financeOption.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<STCZoneRatingDto>> GetSTCZoneRating()
        {
            return await _stcZoneRatingRepository.GetAll()
                .Select(stcZoneRating => new STCZoneRatingDto
                {
                    Id = stcZoneRating.Id,
                    Rating = stcZoneRating.Rating,
                    UpSizeTs = stcZoneRating.UpSizeTs
                }).ToListAsync();
        }

        public async Task<List<STCPostalCodeDto>> GetSTCPostalCode()
        {
            return await _stcPostalCodeRepository.GetAll()
                .Select(stcPostalCode => new STCPostalCodeDto
                {
                    Id = stcPostalCode.Id,
                    PostCodeFrom = stcPostalCode.PostCodeFrom,
                    PostCodeTo = stcPostalCode.PostCodeTo,
                    ZoneId = stcPostalCode.ZoneId
                }).ToListAsync();
        }

        public async Task<List<STCYearWiseRateDto>> GetSTCYearWiseRate()
        {
            return await _stcYearWiseRateRepository.GetAll()
                .Select(stcYearWise => new STCYearWiseRateDto
                {
                    Id = stcYearWise.Id,
                    Rate = stcYearWise.Rate,
                    Year = stcYearWise.Year
                }).ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetAllCancelReasonForTableDropdown()
        {
            return await _jobCancellationReasonReasonRepository.GetAll()
                .Select(cancelReason => new CommonLookupDto
                {
                    Id = cancelReason.Id,
                    DisplayName = cancelReason == null || cancelReason.Name == null ? "" : cancelReason.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<CommonLookupDto>> GetAllHoldReasonForTableDropdown()
        {
            return await _jobHoldReasonRepository.GetAll()
                .Select(holdReason => new CommonLookupDto
                {
                    Id = holdReason.Id,
                    DisplayName = holdReason == null || holdReason.JobHoldReason == null ? "" : holdReason.JobHoldReason.ToString()
                }).ToListAsync();
        }

        public string GetVariationsAction(int? Id)
        {
            var ActionName = _variationRepository.GetAll().Where(e => e.Id == Id).Select(e => e.Action).FirstOrDefault();

            return ActionName;
        }

        public async Task JobActive(int Id)
        {
            var Jobs = _jobRepository.GetAll().Where(e => e.Id == Id).FirstOrDefault();

            if (Jobs.JobStatusId == 1 || Jobs.JobStatusId == 4)
            {
                var NMI = !string.IsNullOrEmpty(Jobs.NMINumber);
                var ApprovedReferenceNumber = !string.IsNullOrEmpty(Jobs.ApplicationRefNo);
                var Meter = Jobs.MeterUpgradeId != null;
                var MeterPhoto = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 3 && e.JobId == Id).Any();
                var Deposite = _invoicePaymentRepository.GetAll().Where(e => e.JobId == Id).Any();
                var SignedQuote = _quotationRepository.GetAll().Where(e => e.JobId == Id && e.IsSigned == true).Any() || _documentRepository.GetAll().Where(e => e.JobId == Id && e.DocumentTypeId == 5).Any();
                //var QuoteAcceptDate = _quotationRepository.GetAll().Where(e => e.JobId == Id && e.IsSigned == true && e.QuoteAcceptDate != null).Any();
                var Finance = Jobs.PaymentOptionId == 1 ? true : Jobs.FinanceDocumentVerified == 1 ? true : false;
                if (Jobs.State == "VIC")
                {
                    if (NMI && Jobs.ElecRetailerId != null && Jobs.ElecRetailerId != 0 && Meter && Deposite && SignedQuote && Finance && ApprovedReferenceNumber && MeterPhoto && Jobs.ExpiryDate != null && Jobs.VicRebate == "With Rebate Install" && Jobs.SolarRebateStatus == 17)
                    {
                        Jobs.IsActive = true;
                        await _jobRepository.UpdateAsync(Jobs);

                        var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                        LeadActivityLog leadactivity = new LeadActivityLog();
                        leadactivity.ActionId = 14;
                        leadactivity.SectionId = 0;
                        leadactivity.ActionNote = "Job Active Request Created";
                        leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
                        if (AbpSession.TenantId != null)
                        {
                            leadactivity.TenantId = (int)AbpSession.TenantId;
                        }
                        await _leadactivityRepository.InsertAsync(leadactivity);

                        //string msg = string.Format("Job Number {0} Requested To Active  ", Jobs.JobNumber);
                        //await _appNotifier.LeadAssiged(AssignedUser, msg, NotificationSeverity.Info);
                    }
                }
                else
                {
                    if (NMI && Jobs.ElecRetailerId != null && Jobs.ElecRetailerId != 0 && Meter && Deposite && SignedQuote && Finance && ApprovedReferenceNumber && MeterPhoto)
                    {
                        Jobs.IsActive = true;
                        await _jobRepository.UpdateAsync(Jobs);

                        var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                        LeadActivityLog leadactivity = new LeadActivityLog();
                        leadactivity.ActionId = 14;
                        leadactivity.SectionId = 0;
                        leadactivity.ActionNote = "Job Active Request Created";
                        leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
                        if (AbpSession.TenantId != null)
                        {
                            leadactivity.TenantId = (int)AbpSession.TenantId;
                        }
                        await _leadactivityRepository.InsertAsync(leadactivity);

                        //string msg = string.Format("Job Number {0} Requested To Active  ", Jobs.JobNumber);
                        //await _appNotifier.LeadAssiged(AssignedUser, msg, NotificationSeverity.Info);
                    }
                }

            }
        }

        public async Task JobActiveLoop()
        {
            var Job = _jobRepository.GetAll().Select(e => e.Id).ToList();

            foreach (var item in Job)
            {
                JobActive(item);
            }
        }

        public async Task<JobActiveStatusDto> GetJobActiveStatus(EntityDto input)
        {
            var Result = new JobActiveStatusDto();
            var job = _jobRepository.GetAll().Where(e => e.Id == input.Id).FirstOrDefault();

            
            ///// Active

            Result.NMINumber = !string.IsNullOrEmpty(job.NMINumber);
            Result.ApprovedReferenceNumber = !string.IsNullOrEmpty(job.ApplicationRefNo);
            Result.DistApproved = job.DistApproveDate != null ? true : false;
            Result.MeterUpdgrade = job.MeterUpgradeId != null;
            Result.MeterBoxPhoto = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 3 && e.JobId == job.Id).Any();
            Result.SignedQuote = _quotationRepository.GetAll().Where(e => e.JobId == job.Id && e.IsSigned == true).Any() || _documentRepository.GetAll().Where(e => e.JobId == job.Id && e.DocumentTypeId == 5).Any();
            Result.Deposite = _invoicePaymentRepository.GetAll().Where(e => e.JobId == input.Id).Any();
            //Result.QuoteAcceptDate = _quotationRepository.GetAll().Where(e => e.JobId == input.Id && e.IsSigned == true && e.QuoteAcceptDate != null).Any();
            Result.Finance = job.PaymentOptionId == 1 ? true : job.FinanceDocumentVerified == 1 ? true : false;
            Result.payementoptionid = job.PaymentOptionId;
            Result.ExpiryDate = job.ExpiryDate == null ? false : true;
            Result.SolarRebateStatus = (job.SolarRebateStatus == 0 || job.SolarRebateStatus == 0 || job.SolarRebateStatus != 17) && job.VicRebate != "With Rebate Install" ? false : true;
            Result.VicRebate = job.VicRebate;
            Result.State = job.State;


            ///// deposite received
            Result.HouseTypeId = job.HouseTypeId != null;
            Result.ElecDistributorId = job.ElecDistributorId != null;
            Result.RoofAngleId = job.RoofAngleId != null;
            Result.RoofTypeId = job.RoofTypeId != null;
            Result.InstallerNotes = !string.IsNullOrEmpty(job.InstallerNotes);
            Result.Quote = _quotationRepository.GetAll().Where(e => e.JobId == input.Id).Any();
            Result.Invoice = _invoicePaymentRepository.GetAll().Where(e => e.JobId == input.Id).Any();
            Result.ProductDetail = _jobProductItemRepository.GetAll().Where(e => e.JobId == input.Id).Any();
            Result.JobStatusId = job.JobStatusId;
            Result.paymentType = _invoicePaymentRepository.GetAll().Where(e => e.JobId == input.Id && (e.InvoicePaymentMethodFk.PaymentMethod == "EFT" || e.InvoicePaymentMethodFk.PaymentMethod == "Credit Card")).Any() ? true : false;
            Result.PaymentVerified = _invoicePaymentRepository.GetAll().Where(e => e.JobId == input.Id && e.IsVerified == true).Any() == true ? true : false;

            ///// ApplicationTracker
            Result.MeterPhadeIDs = _lookup_meterPhaseRepository.GetAll().Where(e => e.Id == job.MeterPhaseId).Any();
            Result.PeakMeterNos = !string.IsNullOrEmpty(job.PeakMeterNo);
            Result.DocList = _documentRepository.GetAll().Where(e => e.JobId == job.Id).Any();
            Result.elecRetailerId = job.ElecRetailerId != null;
            Result.QuoteSigned = _quotationRepository.GetAll().Where(e => e.JobId == input.Id).Any() ? _quotationRepository.GetAll().Where(e => e.JobId == input.Id).OrderByDescending(e => e.Id).FirstOrDefault().IsSigned : true;
            Result.ExportControlFormSigned = _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Export Control").Any() ? _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Export Control").OrderByDescending(e => e.Id).FirstOrDefault().IsSigned : true; ;
            Result.FeedInTariffSigned = _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Feed In Tariff").Any() ? _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Feed In Tariff").OrderByDescending(e => e.Id).FirstOrDefault().IsSigned: true; 
            Result.ShadingDeclarationSigned = _declarationFormRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Shading Declaration").Any() ? _declarationFormRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Shading Declaration").OrderByDescending(e => e.Id).FirstOrDefault().IsSigned : true; 
            Result.EfficiencyDeclarationSigned = _declarationFormRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Efficiency Declaration").Any() ? _declarationFormRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Efficiency Declaration").OrderByDescending(e => e.Id).FirstOrDefault().IsSigned : true;
            Result.ExportControlForm = _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Export Control").Any();
            Result.FeedInTariff = _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Feed In Tariff").Any();
            Result.ShadingDeclaration = _declarationFormRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Shading Declaration").Any();
            Result.EfficiencyDeclaration = _declarationFormRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Efficiency Declaration").Any();


            if (!Result.QuoteSigned && _quotationRepository.GetAll().Where(e => e.JobId == input.Id).Any())
            {
                var quoteId = await _lookup_documentTypeRepository.GetAll().Where(e => e.Title == "Signed Quote").Select(e => e.Id).FirstOrDefaultAsync();
                var quoteDate = _quotationRepository.GetAll().Where(e => e.JobId == input.Id).OrderByDescending(e => e.Id).FirstOrDefault().CreationTime;

                if(_documentRepository.GetAll().Where(e => e.DocumentTypeId == quoteId && e.CreationTime >= quoteDate && e.JobId == input.Id).Any())
                {
                    Result.QuoteSigned = true;
                }
            }

            if (!Result.ExportControlFormSigned && _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Export Control").Any())
            {
                var exportControlId = await _lookup_documentTypeRepository.GetAll().Where(e => e.Title == "Export Control Form").Select(e => e.Id).FirstOrDefaultAsync();

                var exportControlDate = _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Export Control").OrderByDescending(e => e.Id).FirstOrDefault().CreationTime;

                if (_documentRepository.GetAll().Where(e => e.DocumentTypeId == exportControlId && e.CreationTime >= exportControlDate && e.JobId == input.Id).Any())
                {
                    Result.ExportControlFormSigned = true;
                }
            }

            if (!Result.FeedInTariffSigned && _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Feed In Tariff").Any())
            {
                var feedInTariffId = await _lookup_documentTypeRepository.GetAll().Where(e => e.Title == "Feed In Tariff").Select(e => e.Id).FirstOrDefaultAsync();

                var feedInTariffDate = _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Feed In Tariff").OrderByDescending(e => e.Id).FirstOrDefault().CreationTime;

                if (_documentRepository.GetAll().Where(e => e.DocumentTypeId == feedInTariffId && e.CreationTime >= feedInTariffDate && e.JobId == input.Id).Any())
                {
                    Result.FeedInTariffSigned = true;
                }
            }

            if (!Result.ShadingDeclarationSigned && _declarationFormRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Shading Declaration").Any())
            {
                var shadingDeclarationId = await _lookup_documentTypeRepository.GetAll().Where(e => e.Title == "Shading Declaration").Select(e => e.Id).FirstOrDefaultAsync();

                var shadingDeclarationDate = _declarationFormRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Shading Declaration").OrderByDescending(e => e.Id).FirstOrDefault().CreationTime;

                if (_documentRepository.GetAll().Where(e => e.DocumentTypeId == shadingDeclarationId && e.CreationTime >= shadingDeclarationDate && e.JobId == input.Id).Any())
                {
                    Result.ShadingDeclarationSigned = true;
                }
            }

            if (!Result.EfficiencyDeclarationSigned && _declarationFormRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Efficiency Declaration").Any())
            {
                var efficiencyDeclarationId = await _lookup_documentTypeRepository.GetAll().Where(e => e.Title == "Efficiency Declaration").Select(e => e.Id).FirstOrDefaultAsync();

                var efficiencyDeclarationDate = _declarationFormRepqository.GetAll().Where(e => e.JobId == input.Id && e.DocType == "Efficiency Declaration").OrderByDescending(e => e.Id).FirstOrDefault().CreationTime;

                if (_documentRepository.GetAll().Where(e => e.DocumentTypeId == efficiencyDeclarationId && e.CreationTime >= efficiencyDeclarationDate && e.JobId == input.Id).Any())
                {
                    Result.EfficiencyDeclarationSigned = true;
                }
            }

            return Result;
        }

        public async Task<EssentialTrackerActiveStatusDto> GetEssentialTrackerActiveStatus(EntityDto input)
        {
            var Result = new EssentialTrackerActiveStatusDto();
            var job = _jobRepository.GetAll().Where(e => e.Id == input.Id).FirstOrDefault();

            Result.ElectricityBill = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 2 && e.JobId == job.Id).Any();
            Result.ElectricPoleImg = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 7 && e.JobId == job.Id).Any();
            Result.MeterBox = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 3 && e.JobId == job.Id).Any();
            Result.EssentialGreenBox = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 19 && e.JobId == job.Id).Any();

            return Result;
        }

        public async Task JobDeposite(int Id)
        {
            var Jobs = _jobRepository.GetAll().Where(e => e.Id == Id).FirstOrDefault();

            if ((Jobs.JobStatusId == 1 || Jobs.JobStatusId == 2 || Jobs.JobStatusId == 3) && Jobs.JobStatusId != 5 && Jobs.DepositeRecceivedDate == null)
            {
                var HouseTypeId = Jobs.HouseTypeId != null;
                var ElecDistributorId = Jobs.ElecDistributorId != null;
                var RoofAngleId = Jobs.RoofAngleId != null;
                var RoofTypeId = Jobs.RoofTypeId != null;
                var InstallerNOtes = !string.IsNullOrEmpty(Jobs.InstallerNotes);
                var Quote = _quotationRepository.GetAll().Where(e => e.JobId == Id).Any();
                var Invoice = _invoicePaymentRepository.GetAll().Where(e => e.JobId == Id).Any();
                var ProductDetail = _jobProductItemRepository.GetAll().Where(e => e.JobId == Id).Any();

                if (HouseTypeId && ElecDistributorId && RoofAngleId && RoofTypeId && InstallerNOtes && Quote && Invoice && ProductDetail)
                {
                    PreviousJobStatus jobStstus = new PreviousJobStatus();
                    jobStstus.JobId = Jobs.Id;
                    if (AbpSession.TenantId != null)
                    {
                        jobStstus.TenantId = (int)AbpSession.TenantId;
                    }
                    jobStstus.CurrentID = 4;
                    jobStstus.PreviousId = (int)Jobs.JobStatusId;
                    await _previousJobStatusRepository.InsertAsync(jobStstus);

                    Jobs.JobStatusId = 4;
                    Jobs.DepositeRecceivedDate = DateTime.UtcNow;
                    await _jobRepository.UpdateAsync(Jobs);

                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 14;
                    leadactivity.ActionNote = "Job Status Change To Job Deposite Received";
                    leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    leadactivity.SectionId = 0;
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }
            }
        }

        public async Task Update_PostInstallationDetails(CreateOrEditJobDto input)
        {
            input.InstalledcompleteDate = (_timeZoneConverter.Convert(input.InstalledcompleteDate, (int)AbpSession.TenantId));
            input.InspectionDate = (_timeZoneConverter.Convert(input.InspectionDate, (int)AbpSession.TenantId));
            input.VICQRcodeScanDate = (_timeZoneConverter.Convert(input.VICQRcodeScanDate, (int)AbpSession.TenantId));

            var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);
            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var list = new List<JobTrackerHistory>();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 14;
            leadactivity.SectionId = input.SectionId;
            if (input.PostInstallationStatus == 1)
            {
                PreviousJobStatus jobStstus = new PreviousJobStatus();
                jobStstus.JobId = job.Id;
                if (AbpSession.TenantId != null)
                {
                    jobStstus.TenantId = (int)AbpSession.TenantId;
                }
                jobStstus.CurrentID = 8;
                jobStstus.PreviousId = (int)job.JobStatusId;
                await _previousJobStatusRepository.InsertAsync(jobStstus);


                job.JobStatusId = 8;
                leadactivity.ActionNote = "Job Status Change To Job Installed";

            }
            else if (input.PostInstallationStatus == 2)
            {
                PreviousJobStatus jobStstus = new PreviousJobStatus();
                jobStstus.JobId = job.Id;
                if (AbpSession.TenantId != null)
                {
                    jobStstus.TenantId = (int)AbpSession.TenantId;
                }
                jobStstus.CurrentID = 7;
                jobStstus.PreviousId = (int)job.JobStatusId;
                await _previousJobStatusRepository.InsertAsync(jobStstus);

                job.JobStatusId = 7;
                leadactivity.ActionNote = "Job Status Change To Job Incomplete";
            }
            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var jobactionid = _leadactivityRepository.InsertAndGetId(leadactivity);

            if (job.PostInstallationStatus != input.PostInstallationStatus)
            {
                string exsistPostInstallationStatus = "";
                string currPostInstallationStatus = "";
                if (job.PostInstallationStatus == 1)
                {
                    exsistPostInstallationStatus = "Complete";
                }
                else
                {
                    exsistPostInstallationStatus = "In Complete";
                }

                if (input.PostInstallationStatus == 1)
                {
                    currPostInstallationStatus = "Complete";
                }
                else
                {
                    currPostInstallationStatus = "In Complete";
                }

                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Post Installation Status";
                jobhistory.PrevValue = exsistPostInstallationStatus;
                jobhistory.CurValue = currPostInstallationStatus;
                jobhistory.Action = "Post-Installation Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (job.InstalledcompleteDate != input.InstalledcompleteDate)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Installed Complete Date";
                jobhistory.PrevValue = job.InstalledcompleteDate.ToString();
                jobhistory.CurValue = input.InstalledcompleteDate.ToString();
                jobhistory.Action = "Post-Installation Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (job.IncompleteReason != input.IncompleteReason)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Incomplete Reason";
                jobhistory.PrevValue = job.IncompleteReason;
                jobhistory.CurValue = input.IncompleteReason;
                jobhistory.Action = "Post-Installation Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (job.MeterApplyRef != input.MeterApplyRef)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Meter Apply Ref";
                jobhistory.PrevValue = job.MeterApplyRef;
                jobhistory.CurValue = input.MeterApplyRef;
                jobhistory.Action = "Meter Application Details Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (job.InspectorName != input.InspectorName)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Inspector Name";
                jobhistory.PrevValue = job.InspectorName;
                jobhistory.CurValue = input.InspectorName;
                jobhistory.Action = "Meter Application Details Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (job.InspectionDate != input.InspectionDate)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Inspection Date";
                jobhistory.PrevValue = job.InspectionDate.ToString();
                jobhistory.CurValue = input.InspectionDate.ToString();
                jobhistory.Action = "Meter Application Details Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (job.ComplianceCertificate != input.ComplianceCertificate)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Compliance Certificate";
                jobhistory.PrevValue = job.ComplianceCertificate;
                jobhistory.CurValue = input.ComplianceCertificate;
                jobhistory.Action = "Meter Application Details Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (job.VICQRcodeScanDate != input.VICQRcodeScanDate)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "VIC QRcode Scan Date";
                jobhistory.PrevValue = job.VICQRcodeScanDate.ToString();
                jobhistory.CurValue = input.VICQRcodeScanDate.ToString();
                jobhistory.Action = "Post-Installation Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (job.InstallCost != input.InstallCost)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Install Cost";
                jobhistory.PrevValue = job.InstallCost.ToString();
                jobhistory.CurValue = input.InstallCost.ToString();
                jobhistory.Action = "Post-Installation Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (job.ServiceCost != input.ServiceCost)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Service Cost";
                jobhistory.PrevValue = job.ServiceCost.ToString();
                jobhistory.CurValue = input.ServiceCost.ToString();
                jobhistory.Action = "Post-Installation Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            job.MeterApplyRef = input.MeterApplyRef;
            job.InspectorName = input.InspectorName;
            job.InspectionDate = input.InspectionDate;
            job.ComplianceCertificate = input.ComplianceCertificate;
            job.IncompleteReason = input.IncompleteReason;
            job.InstalledcompleteDate = input.InstalledcompleteDate;
            job.PostInstallationStatus = input.PostInstallationStatus;
            job.VICQRcodeScanDate = input.VICQRcodeScanDate;
            job.InstallCost = input.InstallCost;
            job.ServiceCost = input.ServiceCost;
            await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
            await _jobRepository.UpdateAsync(job);
            // await JobActive(job.Id); 


        }

        //STC Tracker
        //public async Task<PagedResultDto<GetJobForViewDto>> GetAllForSTCTrackerTracker(GetAllJobsInput input)
        //{
        //    int stcBlank = 0;
        //    int withblank = 0;
        //    if (input.pvdStat != null)
        //    {
        //        if (input.pvdStat.Contains(11))
        //        {
        //            stcBlank = 11;
        //            input.pvdStat.Remove(11);
        //            withblank = input.pvdStat.Count;
        //            input.pvdnoStatus = 2;
        //        }
        //    }

        //    var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
        //    var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
        //    var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
        //    IList<string> role = await _userManager.GetRolesAsync(User);
        //    var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
        //    var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

        //    var leadactivitylog = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);
        //    var filteredJobs = _jobRepository.GetAll()
        //                  .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) || e.LotNumber.Contains(input.Filter) || e.PeakMeterNo.Contains(input.Filter) || e.OffPeakMeter.Contains(input.Filter) || e.Suburb.Contains(input.Filter) || e.State.Contains(input.Filter) || e.PostalCode.Contains(input.Filter) || e.UnitNo.Contains(input.Filter) || e.UnitType.Contains(input.Filter) || e.StreetNo.Contains(input.Filter) || e.StreetName.Contains(input.Filter) || e.StreetType.Contains(input.Filter) || e.Latitude.Contains(input.Filter) || e.Longitude.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Country.Contains(input.Filter) || e.PVDNumber.Contains(input.Filter) || e.JobNumber.Contains(input.Filter))
        //                  .WhereIf(input.DateNameFilter == "StcAppliedDate" && SDate != null && EDate != null, e => e.STCAppliedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.STCAppliedDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                  .WhereIf(input.DateNameFilter == "InstallDate" && SDate != null && EDate != null, e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                  .WhereIf(input.DateNameFilter == "InstallCmpltDate" && SDate != null && EDate != null, e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                  .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.LeadFk.State == input.stateNameFilter)
        //                  .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
        //                  .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
        //                  .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
        //                  .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
        //                  .WhereIf(input.InstallerID != null && input.InstallerID != 0, e => e.InstallerId == input.InstallerID)
        //                  .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
        //                  .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
        //                  .WhereIf(input.pvdNo != null && input.pvdNo != "", e => e.PVDNumber == input.pvdNo)
        //                  .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
        //                  .WhereIf(input.pvdnoStatus == 1, e => e.PVDNumber != null && e.PVDNumber != "")
        //                  .WhereIf(input.pvdnoStatus == 2, e => e.PVDNumber == null || e.PVDNumber == "")
        //                  .WhereIf(input.pvdStat != null && input.pvdStat.Count() > 0 && stcBlank == 0, e => input.pvdStat.Contains((int)e.PVDStatus))
        //                  .WhereIf(stcBlank != 0 && withblank == 0, e => e.PVDStatus == null)
        //                  .WhereIf(stcBlank != 0 && withblank > 0, e => (e.PVDStatus == null || input.pvdStat.Contains((int)e.PVDStatus)))
        //                  //.Where(e => e.JobStatusId >= 4 && e.PVDStatus != 1 && e.PVDStatus != 0)
        //                  .Where(e => e.JobStatusId >= 4 && e.PVDStatus != 0)
        //                  .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);

        //    var pagedAndFilteredJobs = filteredJobs
        //        .OrderBy(input.Sorting ?? "id desc")
        //        .PageBy(input);

        //    var jobs = from o in pagedAndFilteredJobs
        //               join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
        //               from s1 in j1.DefaultIfEmpty()
        //               join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
        //               from o2 in j2.DefaultIfEmpty()
        //               join pvdval in _pVDStatusRepository.GetAll() on o.PVDStatus equals pvdval.Id into pvd
        //               from pvdval in pvd.DefaultIfEmpty()
        //               join userreq in _userRepository.GetAll() on (int)o.InstallerId equals userreq.Id into user
        //               from userval in user.DefaultIfEmpty()
        //               join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
        //               from s5 in j5.DefaultIfEmpty()
        //               join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
        //               from s6 in j6.DefaultIfEmpty()
        //               join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
        //               from s7 in j7.DefaultIfEmpty()

        //               select new GetJobForViewDto()
        //               {
        //                   Job = new JobDto
        //                   {
        //                       RegPlanNo = o.RegPlanNo,
        //                       LotNumber = o.LotNumber,
        //                       Suburb = o.Suburb,
        //                       State = o.State,
        //                       UnitNo = o.UnitNo,
        //                       UnitType = o.UnitType,
        //                       NMINumber = o.NMINumber,
        //                       Id = o.Id,
        //                       ApplicationRefNo = o.ApplicationRefNo,
        //                       DistAppliedDate = o.DistAppliedDate,
        //                       Notes = o.Note,
        //                       InstallerNotes = o.InstallerNotes,
        //                       JobNumber = o.JobNumber,
        //                       MeterNumber = o.MeterNumber,
        //                       OldSystemDetails = o.OldSystemDetails,
        //                       PostalCode = o.PostalCode,
        //                       StreetName = o.StreetName,
        //                       StreetNo = o.StreetNo,
        //                       StreetType = o.StreetType,
        //                       LeadId = o.LeadId,
        //                       ElecDistributorId = o.ElecDistributorId,
        //                       ElecRetailerId = o.ElecRetailerId,
        //                       JobStatusId = o.JobStatusId,
        //                       JobTypeId = o.JobTypeId,
        //                       PVDStatus = o.PVDStatus,
        //                       STCAppliedDate = o.STCAppliedDate,
        //                       STCUploaddate = o.STCUploaddate,
        //                       STCUploadNumber = o.STCUploadNumber,
        //                       PVDNumber = o.PVDNumber == null || o.PVDNumber == "" ? "Blank" : o.PVDNumber,
        //                       STCNotes = o.STCNotes,
        //                       StcSmsSend = o.StcSmsSend,
        //                       StcSmsSendDate = o.StcSmsSendDate,
        //                       StcEmailSend = o.StcEmailSend,
        //                       StcEmailSendDate = o.StcEmailSendDate,
        //                       InstallerName = userval == null || userval.FullName == null ? "" : userval.FullName.ToString(),
        //                       PVDStatusName = pvdval == null || pvdval.Name == null ? "Blank" : pvdval.Name.ToString(),
        //                       CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
        //                       Stc = o.STC,
        //                       StcTotalPrice = o.Rebate,
        //                       SystemCapacity = o.SystemCapacity,
        //                       InstallationDate = o.InstallationDate,
        //                       LastUpdatedDate = o.LastModificationTime.Value.ToString("dd-MM-yyyy hh:mm:ss")
        //                   },

        //                   LastComment = leadactivitylog.Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
        //                   LastCommentDate = leadactivitylog.Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),

        //                   JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
        //                   JobStatusName = o2 == null || o2.Name == null ? "" : o2.Name.ToString(),
        //                   ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
        //                   LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
        //                   Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
        //                   Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
        //                   ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
        //                   ReminderTime = leadactivitylog.Where(e => e.SectionId == 7 && e.ActionId == 8 && e.LeadId == s6.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
        //                   ActivityDescription = leadactivitylog.Where(e => e.SectionId == 7 && e.ActionId == 8 && e.LeadId == s6.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
        //                   ActivityComment = leadactivitylog.Where(e => e.SectionId == 7 && e.ActionId == 24 && e.LeadId == s6.Id).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),


        //               };

        //    var totalCount = await filteredJobs.CountAsync();

        //    return new PagedResultDto<GetJobForViewDto>(
        //        totalCount,
        //        await jobs.ToListAsync()
        //    );
        //}

        //public List<TotalStcCountDto> getStcTrackerCount(GetAllJobsInput input)
        //{
        //    int stcBlank = 0;
        //    int withblank = 0;
        //    if (input.pvdStat != null)
        //    {
        //        if (input.pvdStat.Contains(11))
        //        {
        //            stcBlank = 11;
        //            input.pvdStat.Remove(11);
        //            withblank = input.pvdStat.Count;
        //            input.pvdnoStatus = 2;
        //        }
        //    }
        //    var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
        //    var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
        //    var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
        //    var role = (from user in _userRepository.GetAll()
        //                join ur in _userRoleRepository.GetAll() on user.Id equals ur.UserId into urJoined
        //                from ur in urJoined.DefaultIfEmpty()
        //                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
        //                from us in usJoined.DefaultIfEmpty()
        //                where (us != null && user.Id == User.Id)
        //                select (us.DisplayName));
        //    var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
        //    var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
        //    var pvdstatus = _pVDStatusRepository.GetAll().Distinct().ToList();
        //    var leadactivitylog = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);
        //    var job_list = _jobRepository.GetAll()
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) || e.LotNumber.Contains(input.Filter) || e.PeakMeterNo.Contains(input.Filter) || e.OffPeakMeter.Contains(input.Filter) || e.Suburb.Contains(input.Filter) || e.State.Contains(input.Filter) || e.PostalCode.Contains(input.Filter) || e.UnitNo.Contains(input.Filter) || e.UnitType.Contains(input.Filter) || e.StreetNo.Contains(input.Filter) || e.StreetName.Contains(input.Filter) || e.StreetType.Contains(input.Filter) || e.Latitude.Contains(input.Filter) || e.Longitude.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Country.Contains(input.Filter) || e.PVDNumber.Contains(input.Filter) || e.JobNumber.Contains(input.Filter))
        //                  .WhereIf(input.DateNameFilter == "StcAppliedDate" && SDate != null && EDate != null, e => e.STCAppliedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.STCAppliedDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                  .WhereIf(input.DateNameFilter == "InstallDate" && SDate != null && EDate != null, e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                  .WhereIf(input.DateNameFilter == "InstallCmpltDate" && SDate != null && EDate != null, e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                  .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.LeadFk.State == input.stateNameFilter)
        //                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
        //                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
        //                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
        //                .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
        //                .WhereIf(input.InstallerID != null && input.InstallerID != 0, e => e.InstallerId == input.InstallerID)
        //                .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
        //                .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
        //                 .WhereIf(input.pvdNo != null && input.pvdNo != "", e => e.PVDNumber == input.pvdNo)
        //                 .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
        //                 .WhereIf(input.pvdnoStatus == 1, e => e.PVDNumber != null && e.PVDNumber != "")
        //                 .WhereIf(input.pvdnoStatus == 2, e => e.PVDNumber == null || e.PVDNumber == "")
        //                 .WhereIf(input.pvdStat != null && input.pvdStat.Count() > 0 && stcBlank == 0, e => input.pvdStat.Contains((int)e.PVDStatus))
        //                 .WhereIf(stcBlank != 0 && withblank == 0, e => e.PVDStatus == null)
        //                 .WhereIf(stcBlank != 0 && withblank > 0, e => (e.PVDStatus == null || input.pvdStat.Contains((int)e.PVDStatus)))
        //                 .Where(e => e.JobStatusId >= 4 && e.PVDStatus != 1 && e.PVDStatus != 0)
        //                .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);

        //    var output = new List<TotalStcCountDto>();
        //    var stctotaldata = new TotalStcCountDto();
        //    stctotaldata.Stc = "Total";

        //    stctotaldata.totalstcamount = job_list
        //                 .Where(e => e.PVDStatus != null)
        //                 .Select(e => e.STC).Sum();

        //    stctotaldata.totaljob = job_list.Count();




        //    output.Add(stctotaldata);

        //    var stcBlankdata = new TotalStcCountDto();
        //    stcBlankdata.Stc = "Blank";
        //    stcBlankdata.totalstcamount = job_list
        //                 .Where(e => e.PVDStatus == null)
        //                 .Select(e => e.STC).Sum();
        //    stcBlankdata.totaljob = job_list
        //                 .Where(e => e.PVDStatus == null)
        //                 .Select(e => e.Id).Count();
        //    output.Add(stcBlankdata);
        //    for (int k = 0; k < pvdstatus.Count; k++)
        //    {
        //        var stcdata = new TotalStcCountDto();
        //        if (pvdstatus[k].Id != 11)
        //        {
        //            stcdata.Stc = pvdstatus[k].Name;

        //            int? pvdid = pvdstatus[k].Id;
        //            stcdata.totalstcamount = job_list
        //                 .Where(e => e.PVDStatus == pvdid && e.PVDStatus != 11)
        //                 .Select(e => e.STC).Sum();

        //            stcdata.totaljob = job_list
        //                .Where(e => e.PVDStatus == pvdid && e.PVDStatus != 11)
        //                .Select(e => e.Id).Count();



        //            output.Add(stcdata);
        //        }
        //    }

        //    return output;

        //}

        //STC Excel Export
        public async Task<FileDto> getSTCTrackerToExcel(GetAllJobsForExcelInput input)
        {

            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var filteredJobs = _jobRepository.GetAll()
            //            .Include(e => e.JobTypeFk)
            //            .Include(e => e.JobStatusFk)
            //            .Include(e => e.RoofTypeFk)
            //            .Include(e => e.RoofAngleFk)
            //            .Include(e => e.ElecDistributorFk)
            //            .Include(e => e.LeadFk)
            //            .Include(e => e.ElecRetailerFk)
            //            .Include(e => e.PaymentOptionFk)
            //            .Include(e => e.DepositOptionFk)
            //            //.Include(e => e.MeterUpgradeFk)
            //            //.Include(e => e.MeterPhaseFk)
            //            .Include(e => e.PromotionOfferFk)
            //            .Include(e => e.HouseTypeFk)
            //            .Include(e => e.FinanceOptionFk)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) || e.LotNumber.Contains(input.Filter) || e.PeakMeterNo.Contains(input.Filter) || e.OffPeakMeter.Contains(input.Filter) || e.Suburb.Contains(input.Filter) || e.State.Contains(input.Filter) || e.PostalCode.Contains(input.Filter) || e.UnitNo.Contains(input.Filter) || e.UnitType.Contains(input.Filter) || e.StreetNo.Contains(input.Filter) || e.StreetName.Contains(input.Filter) || e.StreetType.Contains(input.Filter) || e.Latitude.Contains(input.Filter) || e.Longitude.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Country.Contains(input.Filter))

            //              .WhereIf(input.DateNameFilter == "StcAppliedDate" && SDate != null && EDate != null, e => e.STCAppliedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.STCAppliedDate.Value.AddHours(10).Date <= EDate.Value.Date)
            //              .WhereIf(input.DateNameFilter == "InstallDate" && SDate != null && EDate != null, e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
            //              .WhereIf(input.DateNameFilter == "InstallCmpltDate" && SDate != null && EDate != null, e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
            //              .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.LeadFk.State == input.stateNameFilter)

            //            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            //            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            //            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            //            .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
            //            .WhereIf(input.InstallerID != null && input.InstallerID != 0, e => e.InstallerId == input.InstallerID)
            //            .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
            //            .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
            //             //.WhereIf(input.pvdStatus != null && input.pvdStatus != 0, e => e.PVDStatus == input.pvdStatus)
            //             .WhereIf(input.pvdNo != null && input.pvdNo != "", e => e.PVDNumber == input.pvdNo)
            //             .WhereIf(input.pvdStat != null && input.pvdStat.Count() > 0, e => input.pvdStat.Contains((int)e.PVDStatus))
            //             .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
            //            .Where(e => e.JobStatusId >= 4 && e.PVDStatus != 1)
            //            .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);

            int stcBlank = 0;
            int withblank = 0;
            if (input.pvdStat != null)
            {
                if (input.pvdStat.Contains(11))
                {
                    stcBlank = 11;
                    input.pvdStat.Remove(11);
                    withblank = input.pvdStat.Count;
                    input.pvdnoStatus = 2;
                }
            }

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var leadactivitylog = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);
            var filteredJobs = _jobRepository.GetAll()
                        //.Include(e => e.JobTypeFk)
                        //.Include(e => e.JobStatusFk)
                        //.Include(e => e.RoofTypeFk)
                        //.Include(e => e.RoofAngleFk)
                        //.Include(e => e.ElecDistributorFk)
                        //.Include(e => e.LeadFk)
                        //.Include(e => e.ElecRetailerFk)
                        //.Include(e => e.PaymentOptionFk)
                        //.Include(e => e.DepositOptionFk)
                        ////.Include(e => e.MeterUpgradeFk)
                        ////.Include(e => e.MeterPhaseFk)
                        //.Include(e => e.PromotionOfferFk)
                        //.Include(e => e.HouseTypeFk)
                        //.Include(e => e.FinanceOptionFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) || e.LotNumber.Contains(input.Filter) || e.PeakMeterNo.Contains(input.Filter) || e.OffPeakMeter.Contains(input.Filter) || e.Suburb.Contains(input.Filter) || e.State.Contains(input.Filter) || e.PostalCode.Contains(input.Filter) || e.UnitNo.Contains(input.Filter) || e.UnitType.Contains(input.Filter) || e.StreetNo.Contains(input.Filter) || e.StreetName.Contains(input.Filter) || e.StreetType.Contains(input.Filter) || e.Latitude.Contains(input.Filter) || e.Longitude.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Country.Contains(input.Filter) || e.PVDNumber.Contains(input.Filter) || e.JobNumber.Contains(input.Filter))
                          .WhereIf(input.DateNameFilter == "StcAppliedDate" && SDate != null && EDate != null, e => e.STCAppliedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.STCAppliedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                          .WhereIf(input.DateNameFilter == "InstallDate" && SDate != null && EDate != null, e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
                          .WhereIf(input.DateNameFilter == "InstallCmpltDate" && SDate != null && EDate != null, e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
                          .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.LeadFk.State == input.stateNameFilter)
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.InstallerID != null && input.InstallerID != 0, e => e.InstallerId == input.InstallerID)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
                         .WhereIf(input.pvdNo != null && input.pvdNo != "", e => e.PVDNumber == input.pvdNo)
                         .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
                         .WhereIf(input.pvdnoStatus == 1, e => e.PVDNumber != null && e.PVDNumber != "")
                         .WhereIf(input.pvdnoStatus == 2, e => e.PVDNumber == null || e.PVDNumber == "")
                       .WhereIf(input.pvdStat != null && input.pvdStat.Count() > 0 && stcBlank == 0, e => input.pvdStat.Contains((int)e.PVDStatus))
                         .WhereIf(stcBlank != 0 && withblank == 0, e => e.PVDStatus == null)
                         .WhereIf(stcBlank != 0 && withblank > 0, e => (e.PVDStatus == null || input.pvdStat.Contains((int)e.PVDStatus)))
                        .Where(e => e.JobStatusId >= 4 && e.PVDStatus != 1)
                        .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);

            var query = (from o in filteredJobs

                         join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o2 in _pVDStatusRepository.GetAll() on o.PVDStatus equals o2.Id into pvd
                         from pvdval in pvd.DefaultIfEmpty()

                         join userreq in _userRepository.GetAll() on (int)o.InstallerId equals userreq.Id into user
                         from userval in user.DefaultIfEmpty()


                         join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                         from s5 in j5.DefaultIfEmpty()

                         join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                         from s6 in j6.DefaultIfEmpty()

                         join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                         from s7 in j7.DefaultIfEmpty()



                         select new GetJobForViewDto()
                         {
                             Job = new JobDto
                             {
                                 RegPlanNo = o.RegPlanNo,
                                 LotNumber = o.LotNumber,
                                 Suburb = o.Suburb,
                                 State = o.State,
                                 UnitNo = o.UnitNo,
                                 UnitType = o.UnitType,
                                 NMINumber = o.NMINumber,
                                 Id = o.Id,
                                 ApplicationRefNo = o.ApplicationRefNo,
                                 DistAppliedDate = o.DistAppliedDate,
                                 Notes = o.Note,
                                 InstallerNotes = o.InstallerNotes,
                                 JobNumber = o.JobNumber,
                                 MeterNumber = o.MeterNumber,
                                 OldSystemDetails = o.OldSystemDetails,
                                 PostalCode = o.PostalCode,
                                 StreetName = o.StreetName,
                                 StreetNo = o.StreetNo,
                                 StreetType = o.StreetType,
                                 LeadId = o.LeadId,
                                 ElecDistributorId = o.ElecDistributorId,
                                 ElecRetailerId = o.ElecRetailerId,
                                 JobStatusId = o.JobStatusId,
                                 JobTypeId = o.JobTypeId,
                                 PVDStatus = o.PVDStatus,
                                 STCAppliedDate = o.STCAppliedDate,
                                 STCUploaddate = o.STCUploaddate,
                                 STCUploadNumber = o.STCUploadNumber,
                                 Stc = o.STC,
                                 StcTotalPrice = o.Rebate,
                                 SystemCapacity = o.SystemCapacity,
                                 PVDNumber = o.PVDNumber,
                                 STCNotes = o.STCNotes,
                                 InstallerName = userval == null || userval.FullName == null ? "" : userval.FullName.ToString(),
                                 PVDStatusName = pvdval == null || pvdval.Name == null ? "" : pvdval.Name.ToString(),
                                 CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                                 InstallationDate = o.InstallationDate
                             },
                             LastComment = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                             LastCommentDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),

                             JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                             JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                             ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                             LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                             Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                             Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                             ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),

                         });

            var stcListDtos = await query.ToListAsync();

            return _leadsExcelExporter.STCExportToFile(stcListDtos);
        }

        public async Task Update_JOb_STCDetails(CreateOrEditJobDto input)
        {
            input.STCUploaddate = _timeZoneConverter.Convert(input.STCUploaddate, (int)AbpSession.TenantId);
            input.STCAppliedDate = _timeZoneConverter.Convert(input.STCAppliedDate, (int)AbpSession.TenantId);

            var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);
            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

            job.STCNotes = input.STCNotes;
            job.STCAppliedDate = input.STCAppliedDate;
            job.STCUploaddate = input.STCUploaddate;
            job.STCUploadNumber = input.STCUploadNumber;
            job.PVDNumber = input.PVDNumber;
            job.PVDStatus = input.PVDStatus;

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 18;
            leadactivity.SectionId = input.SectionId != null ? input.SectionId : 0;
            leadactivity.ActionNote = "Add STC Details by for Project No: " + job.JobNumber;
            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            // await JobActive(job.Id); 
            var jobactionid = _leadactivityRepository.InsertAndGetId(leadactivity);
            var list = new List<JobTrackerHistory>();

            if (job.STCUploaddate != input.STCUploaddate)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "STCUploaddate";
                jobhistory.PrevValue = job.STCUploaddate.ToString();
                jobhistory.CurValue = input.STCUploaddate.ToString();
                jobhistory.Action = "STC Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.STCUploadNumber != input.STCUploadNumber)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "STCUploadNumber";
                jobhistory.PrevValue = job.STCUploadNumber;
                jobhistory.CurValue = input.STCUploadNumber;
                jobhistory.Action = "STC Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            if (job.STCAppliedDate != input.STCAppliedDate)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "STCAppliedDate";
                jobhistory.PrevValue = job.STCAppliedDate.ToString();
                jobhistory.CurValue = input.STCAppliedDate.ToString();
                jobhistory.Action = "STC Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.PVDNumber != input.PVDNumber)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "PVDNumber";
                jobhistory.PrevValue = job.PVDNumber;
                jobhistory.CurValue = input.PVDNumber;
                jobhistory.Action = "STC Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.STCNotes != input.STCNotes)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "STCNotes";
                jobhistory.PrevValue = job.STCNotes;
                jobhistory.CurValue = input.STCNotes;
                jobhistory.Action = "STC Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.PVDStatus != input.PVDStatus)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "PVDStatus";
                jobhistory.PrevValue = _pVDStatusRepository.GetAll().Where(e => e.Id == job.PVDStatus).Select(e => e.Name).FirstOrDefault();
                jobhistory.CurValue = _pVDStatusRepository.GetAll().Where(e => e.Id == input.PVDStatus).Select(e => e.Name).FirstOrDefault();
                jobhistory.Action = "STC Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
            await _jobRepository.UpdateAsync(job);
        }

        public async Task<List<JobLeadLookupTableDto>> GetPVDStatusList()
        {
            return await _pVDStatusRepository.GetAll()
                .Select(item => new JobLeadLookupTableDto
                {
                    Id = item.Id,
                    DisplayName = item.Name
                }).OrderBy(x => x.DisplayName).ToListAsync();
        }

        //GridConnectionTracker
        //public async Task<PagedResultDto<GetJobForViewDto>> GetGridConnectionTracker(GetAllJobsInput input)
        //{
        //    var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
        //    var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

        //    var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
        //    IList<string> role = await _userManager.GetRolesAsync(User);
        //    var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
        //    var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
        //    var leadactivitylog = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);
        //    var filteredJobs = _jobRepository.GetAll()
        //                .Include(e => e.JobTypeFk)
        //                .Include(e => e.JobStatusFk)
        //                .Include(e => e.RoofTypeFk)
        //                .Include(e => e.RoofAngleFk)
        //                .Include(e => e.ElecDistributorFk)
        //                .Include(e => e.LeadFk)
        //                .Include(e => e.ElecRetailerFk)
        //                .Include(e => e.PaymentOptionFk)
        //                .Include(e => e.DepositOptionFk)
        //                //.Include(e => e.MeterUpgradeFk)
        //                //.Include(e => e.MeterPhaseFk)
        //                .Include(e => e.PromotionOfferFk)
        //                .Include(e => e.HouseTypeFk)
        //                .Include(e => e.FinanceOptionFk)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) ||
        //                e.LotNumber.Contains(input.Filter) || e.JobNumber.Contains(input.Filter) ||
        //                e.OffPeakMeter.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
        //                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
        //                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
        //                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
        //                .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
        //                .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
        //                .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.LeadFk.State == input.stateNameFilter)
        //                .WhereIf(input.elecDistributorId != 0, e => e.ElecDistributorId == input.elecDistributorId)
        //                //.WhereIf(input.jobStatusIDFilter != 0, e => e.JobStatusId == input.jobStatusIDFilter)
        //                .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
        //                .WhereIf(input.InstallerID != 0, e => e.InstallerId == input.InstallerID)
        //                .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
        //                .WhereIf(input.applicationstatus == 1, e => e.MeterApplyRef == null && e.InspectionDate == null && e.InstalledcompleteDate != null)
        //                .WhereIf(input.applicationstatus == 2, e => e.MeterApplyRef != null && e.InspectionDate != null && e.InstalledcompleteDate != null)
        //                .WhereIf(input.applicationstatus == 3, e => e.JobStatusId == 4 || e.JobStatusId == 5 || e.JobStatusId == 6 || e.JobStatusId == 7)
        //                .WhereIf(input.DateNameFilter == "DepositDate", e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                .WhereIf(input.DateNameFilter == "ActiveDate", e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                .WhereIf(input.DateNameFilter == "distAppliExpiryDate", e => e.DistExpiryDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DistExpiryDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                .WhereIf(input.DateNameFilter == "InstallDate", e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                .WhereIf(input.DateNameFilter == "InstallCmpltDate", e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                .WhereIf(input.DateNameFilter != "InstallDate", e => e.JobStatusId > 3)
        //                .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == j.LeadId).Select(e => e.OrganizationId).FirstOrDefault()) == input.OrganizationUnit);

        //    var pagedAndFilteredJobs = filteredJobs
        //        .OrderBy(input.Sorting ?? "id desc")
        //        .PageBy(input);

        //    var jobs = from o in pagedAndFilteredJobs
        //               join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
        //               from s1 in j1.DefaultIfEmpty()

        //               join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
        //               from s2 in j2.DefaultIfEmpty()

        //               join o2 in _pVDStatusRepository.GetAll() on o.PVDStatus equals o2.Id into pvd
        //               from pvdval in pvd.DefaultIfEmpty()

        //               join userreq in _userRepository.GetAll() on (int)o.InstallerId equals userreq.Id into user
        //               from userval in user.DefaultIfEmpty()

        //                   //join o3 in _lookup_roofTypeRepository.GetAll() on o.RoofTypeId equals o3.Id into j3
        //                   //from s3 in j3.DefaultIfEmpty()

        //                   //join o4 in _lookup_roofAngleRepository.GetAll() on o.RoofAngleId equals o4.Id into j4
        //                   //from s4 in j4.DefaultIfEmpty()

        //               join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
        //               from s5 in j5.DefaultIfEmpty()

        //               join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
        //               from s6 in j6.DefaultIfEmpty()

        //               join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
        //               from s7 in j7.DefaultIfEmpty()

        //                   //join o8 in _lookup_paymentOptionRepository.GetAll() on o.PaymentOptionId equals o8.Id into j8
        //                   //from s8 in j8.DefaultIfEmpty()

        //                   //join o9 in _lookup_depositOptionRepository.GetAll() on o.DepositOptionId equals o9.Id into j9
        //                   //from s9 in j9.DefaultIfEmpty()

        //                   //join o10 in _lookup_meterUpgradeRepository.GetAll() on o.MeterUpgradeId equals o10.Id into j10
        //                   //from s10 in j10.DefaultIfEmpty()

        //                   //join o11 in _lookup_meterPhaseRepository.GetAll() on o.MeterPhaseId equals o11.Id into j11
        //                   //from s11 in j11.DefaultIfEmpty()

        //                   //join o12 in _lookup_promotionOfferRepository.GetAll() on o.PromotionOfferId equals o12.Id into j12
        //                   //from s12 in j12.DefaultIfEmpty()

        //                   //join o13 in _lookup_houseTypeRepository.GetAll() on o.HouseTypeId equals o13.Id into j13
        //                   //from s13 in j13.DefaultIfEmpty()

        //                   //join o14 in _lookup_financeOptionRepository.GetAll() on o.FinanceOptionId equals o14.Id into j14
        //                   //from s14 in j14.DefaultIfEmpty()

        //               select new GetJobForViewDto()
        //               {
        //                   Job = new JobDto
        //                   {
        //                       RegPlanNo = o.RegPlanNo,
        //                       LotNumber = o.LotNumber,
        //                       Address = o.Address,
        //                       Suburb = o.Suburb,
        //                       State = o.State,
        //                       UnitNo = o.UnitNo,
        //                       UnitType = o.UnitType,
        //                       NMINumber = o.NMINumber,
        //                       Id = o.Id,
        //                       ApplicationRefNo = o.ApplicationRefNo,
        //                       DistAppliedDate = o.DistAppliedDate,
        //                       Notes = o.Note,
        //                       InstallerNotes = o.InstallerNotes,
        //                       JobNumber = o.JobNumber,
        //                       MeterNumber = o.MeterNumber,
        //                       OldSystemDetails = o.OldSystemDetails,
        //                       PostalCode = o.PostalCode,
        //                       StreetName = o.StreetName,
        //                       StreetNo = o.StreetNo,
        //                       StreetType = o.StreetType,
        //                       LeadId = o.LeadId,
        //                       ElecDistributorId = o.ElecDistributorId,
        //                       ElecRetailerId = o.ElecRetailerId,
        //                       JobStatusId = o.JobStatusId,
        //                       JobTypeId = o.JobTypeId,
        //                       PVDStatus = o.PVDStatus,
        //                       STCAppliedDate = o.STCAppliedDate,
        //                       STCUploaddate = o.STCUploaddate,
        //                       STCUploadNumber = o.STCUploadNumber,
        //                       DistExpiryDate = o.DistExpiryDate,
        //                       InstallationDate = o.InstallationDate,
        //                       InstalledcompleteDate = o.InstalledcompleteDate,
        //                       PVDNumber = o.PVDNumber,
        //                       STCNotes = o.STCNotes,
        //                       SystemCapacity = o.SystemCapacity,
        //                       MeterApplyRef = o.MeterApplyRef,
        //                       InspectionDate = o.InspectionDate,
        //                       GridConnectionSmsSend = o.GridConnectionSmsSend,
        //                       GridConnectionEmailSendDate = o.GridConnectionSmsSendDate,
        //                       GridConnectionEmailSend = o.GridConnectionEmailSend,
        //                       GridConnectionSmsSendDate = o.GridConnectionEmailSendDate,
        //                       InstallerName = userval == null || userval.FullName == null ? "" : userval.FullName.ToString(),
        //                       PVDStatusName = pvdval == null || pvdval.Name == null ? "" : pvdval.Name.ToString(),
        //                       CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault()
        //                   },

        //                   LastComment = leadactivitylog.Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
        //                   LastCommentDate = leadactivitylog.Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),

        //                   JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
        //                   JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
        //                   ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
        //                   LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
        //                   Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
        //                   Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
        //                   ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
        //                   ReminderTime = leadactivitylog.Where(e => e.SectionId == 6 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
        //                   ActivityDescription = leadactivitylog.Where(e => e.SectionId == 6 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
        //                   ActivityComment = leadactivitylog.Where(e => e.SectionId == 6 && e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

        //               };

        //    var totalCount = await filteredJobs.CountAsync();

        //    return new PagedResultDto<GetJobForViewDto>(
        //        totalCount,
        //        await jobs.ToListAsync()
        //    );
        //}

        //STC Excel Export
        //public async Task<FileDto> getGridConnectionTrackerToExcel(GetAllJobsForExcelInput input)
        //{
        //    //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
        //    //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

        //    //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
        //    //IList<string> role = await _userManager.GetRolesAsync(User);
        //    //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
        //    //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

        //    //var filteredJobs = _jobRepository.GetAll()
        //    //            .Include(e => e.JobTypeFk)
        //    //            .Include(e => e.JobStatusFk)
        //    //            .Include(e => e.RoofTypeFk)
        //    //            .Include(e => e.RoofAngleFk)
        //    //            .Include(e => e.ElecDistributorFk)
        //    //            .Include(e => e.LeadFk)
        //    //            .Include(e => e.ElecRetailerFk)
        //    //            .Include(e => e.PaymentOptionFk)
        //    //            .Include(e => e.DepositOptionFk)
        //    //            //.Include(e => e.MeterUpgradeFk)
        //    //            //.Include(e => e.MeterPhaseFk)
        //    //            .Include(e => e.PromotionOfferFk)
        //    //            .Include(e => e.HouseTypeFk)
        //    //            .Include(e => e.FinanceOptionFk)
        //    //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) ||
        //    //            e.LotNumber.Contains(input.Filter) || e.JobNumber.Contains(input.Filter) ||
        //    //            e.OffPeakMeter.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
        //    //           .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
        //    //            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
        //    //            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
        //    //            .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
        //    //            .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
        //    //            .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
        //    //            .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.LeadFk.State == input.stateNameFilter)
        //    //            .WhereIf(input.elecDistributorId != 0, e => e.ElecDistributorId == input.elecDistributorId)
        //    //            //.WhereIf(input.jobStatusIDFilter != 0, e => e.JobStatusId == input.jobStatusIDFilter)
        //    //            .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
        //    //            .WhereIf(input.InstallerID != 0, e => e.InstallerId == input.InstallerID)
        //    //            .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
        //    //            .WhereIf(input.applicationstatus == 1, e => e.MeterApplyRef == null && e.InspectionDate == null && e.InstalledcompleteDate != null)
        //    //            .WhereIf(input.applicationstatus == 2, e => e.MeterApplyRef != null && e.InspectionDate != null && e.InstalledcompleteDate != null)
        //    //            .WhereIf(input.applicationstatus == 3, e => e.JobStatusId == 4 || e.JobStatusId == 5 || e.JobStatusId == 6 || e.JobStatusId == 7)
        //    //            .WhereIf(input.DateNameFilter == "DepositDate", e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //    //            .WhereIf(input.DateNameFilter == "ActiveDate", e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //    //            .WhereIf(input.DateNameFilter == "distAppliExpiryDate", e => e.DistExpiryDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DistExpiryDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //    //            .WhereIf(input.DateNameFilter == "InstallDate", e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //    //            .WhereIf(input.DateNameFilter == "InstallCmpltDate", e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //    //            .Where(e => e.JobStatusId > 3)
        //    //            .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == j.LeadId).Select(e => e.OrganizationId).FirstOrDefault()) == input.OrganizationUnit);

        //    var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
        //    var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

        //    var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
        //    IList<string> role = await _userManager.GetRolesAsync(User);
        //    var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
        //    var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
        //    var leadactivitylog = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);
        //    var filteredJobs = _jobRepository.GetAll()
        //                .Include(e => e.JobTypeFk)
        //                .Include(e => e.JobStatusFk)
        //                .Include(e => e.RoofTypeFk)
        //                .Include(e => e.RoofAngleFk)
        //                .Include(e => e.ElecDistributorFk)
        //                .Include(e => e.LeadFk)
        //                .Include(e => e.ElecRetailerFk)
        //                .Include(e => e.PaymentOptionFk)
        //                .Include(e => e.DepositOptionFk)
        //                //.Include(e => e.MeterUpgradeFk)
        //                //.Include(e => e.MeterPhaseFk)
        //                .Include(e => e.PromotionOfferFk)
        //                .Include(e => e.HouseTypeFk)
        //                .Include(e => e.FinanceOptionFk)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Note.Contains(input.Filter) || e.NMINumber.Contains(input.Filter) || e.RegPlanNo.Contains(input.Filter) ||
        //                e.LotNumber.Contains(input.Filter) || e.JobNumber.Contains(input.Filter) ||
        //                e.OffPeakMeter.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
        //                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
        //                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
        //                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
        //                .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
        //                .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
        //                .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.stateNameFilter), e => e.LeadFk.State == input.stateNameFilter)
        //                .WhereIf(input.elecDistributorId != 0, e => e.ElecDistributorId == input.elecDistributorId)
        //                //.WhereIf(input.jobStatusIDFilter != 0, e => e.JobStatusId == input.jobStatusIDFilter)
        //                .WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
        //                .WhereIf(input.InstallerID != 0, e => e.InstallerId == input.InstallerID)
        //                .WhereIf(input.jobTypeId != 0, e => e.JobTypeId == input.jobTypeId)
        //                .WhereIf(input.applicationstatus == 1, e => e.MeterApplyRef == null && e.InspectionDate == null && e.InstalledcompleteDate != null)
        //                .WhereIf(input.applicationstatus == 2, e => e.MeterApplyRef != null && e.InspectionDate != null && e.InstalledcompleteDate != null)
        //                .WhereIf(input.applicationstatus == 3, e => e.JobStatusId == 4 || e.JobStatusId == 5 || e.JobStatusId == 6 || e.JobStatusId == 7)
        //                .WhereIf(input.DateNameFilter == "DepositDate", e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                .WhereIf(input.DateNameFilter == "ActiveDate", e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                .WhereIf(input.DateNameFilter == "distAppliExpiryDate", e => e.DistExpiryDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DistExpiryDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                .WhereIf(input.DateNameFilter == "InstallDate", e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                .WhereIf(input.DateNameFilter == "InstallCmpltDate", e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //                .WhereIf(input.DateNameFilter != "InstallDate", e => e.JobStatusId > 3)
        //                .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == j.LeadId).Select(e => e.OrganizationId).FirstOrDefault()) == input.OrganizationUnit);

        //    var query = (from o in filteredJobs
        //                 join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
        //                 from s1 in j1.DefaultIfEmpty()

        //                 join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
        //                 from s2 in j2.DefaultIfEmpty()

        //                 join o2 in _pVDStatusRepository.GetAll() on o.PVDStatus equals o2.Id into pvd
        //                 from pvdval in pvd.DefaultIfEmpty()

        //                 join userreq in _userRepository.GetAll() on (int)o.InstallerId equals userreq.Id into user
        //                 from userval in user.DefaultIfEmpty()

        //                     //join o3 in _lookup_roofTypeRepository.GetAll() on o.RoofTypeId equals o3.Id into j3
        //                     //from s3 in j3.DefaultIfEmpty()

        //                     //join o4 in _lookup_roofAngleRepository.GetAll() on o.RoofAngleId equals o4.Id into j4
        //                     //from s4 in j4.DefaultIfEmpty()

        //                 join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
        //                 from s5 in j5.DefaultIfEmpty()

        //                 join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
        //                 from s6 in j6.DefaultIfEmpty()

        //                 join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
        //                 from s7 in j7.DefaultIfEmpty()

        //                     //join o8 in _lookup_paymentOptionRepository.GetAll() on o.PaymentOptionId equals o8.Id into j8
        //                     //from s8 in j8.DefaultIfEmpty()

        //                     //join o9 in _lookup_depositOptionRepository.GetAll() on o.DepositOptionId equals o9.Id into j9
        //                     //from s9 in j9.DefaultIfEmpty()

        //                     //join o10 in _lookup_meterUpgradeRepository.GetAll() on o.MeterUpgradeId equals o10.Id into j10
        //                     //from s10 in j10.DefaultIfEmpty()

        //                     //join o11 in _lookup_meterPhaseRepository.GetAll() on o.MeterPhaseId equals o11.Id into j11
        //                     //from s11 in j11.DefaultIfEmpty()

        //                     //join o12 in _lookup_promotionOfferRepository.GetAll() on o.PromotionOfferId equals o12.Id into j12
        //                     //from s12 in j12.DefaultIfEmpty()

        //                     //join o13 in _lookup_houseTypeRepository.GetAll() on o.HouseTypeId equals o13.Id into j13
        //                     //from s13 in j13.DefaultIfEmpty()

        //                     //join o14 in _lookup_financeOptionRepository.GetAll() on o.FinanceOptionId equals o14.Id into j14
        //                     //from s14 in j14.DefaultIfEmpty()

        //                 select new GetJobForViewDto()
        //                 {
        //                     Job = new JobDto
        //                     {
        //                         RegPlanNo = o.RegPlanNo,
        //                         LotNumber = o.LotNumber,
        //                         Suburb = o.Suburb,
        //                         State = o.State,
        //                         UnitNo = o.UnitNo,
        //                         UnitType = o.UnitType,
        //                         NMINumber = o.NMINumber,
        //                         Id = o.Id,
        //                         ApplicationRefNo = o.ApplicationRefNo,
        //                         DistAppliedDate = o.DistAppliedDate,
        //                         Notes = o.Note,
        //                         InstallerNotes = o.InstallerNotes,
        //                         JobNumber = o.JobNumber,
        //                         MeterNumber = o.MeterNumber,
        //                         OldSystemDetails = o.OldSystemDetails,
        //                         PostalCode = o.PostalCode,
        //                         StreetName = o.StreetName,
        //                         StreetNo = o.StreetNo,
        //                         StreetType = o.StreetType,
        //                         LeadId = o.LeadId,
        //                         ElecDistributorId = o.ElecDistributorId,
        //                         ElecRetailerId = o.ElecRetailerId,
        //                         JobStatusId = o.JobStatusId,
        //                         JobTypeId = o.JobTypeId,
        //                         PVDStatus = o.PVDStatus,
        //                         STCAppliedDate = o.STCAppliedDate,
        //                         STCUploaddate = o.STCUploaddate,
        //                         STCUploadNumber = o.STCUploadNumber,
        //                         PVDNumber = o.PVDNumber,
        //                         STCNotes = o.STCNotes,
        //                         InstallerName = userval == null || userval.FullName == null ? "" : userval.FullName.ToString(),
        //                         PVDStatusName = pvdval == null || pvdval.Name == null ? "" : pvdval.Name.ToString(),
        //                         CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
        //                         SystemCapacity = o.SystemCapacity,
        //                         MeterApplyRef = o.MeterApplyRef,
        //                     },
        //                     LastComment = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
        //                     LastCommentDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),

        //                     JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
        //                     JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
        //                     ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
        //                     LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
        //                     Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
        //                     Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
        //                     ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
        //                 });

        //    var gridconnectionListDtos = await query.ToListAsync();

        //    return _leadsExcelExporter.GridConnectionExportToFile(gridconnectionListDtos);
        //}

        public async Task Update_MeterDetails(CreateOrEditJobDto input)
        {
            //var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);
            ////ObjectMapper.Map(input, job);


            //var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            //LeadActivityLog leadactivity = new LeadActivityLog();
            //leadactivity.ActionId = 14;
            //leadactivity.SectionId = input.SectionId != null ? input.SectionId : 0;

            //var jobPromotion = await _jobPromotionRepository.FirstOrDefaultAsync((int)input.Id);

            input.InspectionDate = _timeZoneConverter.Convert(input.InspectionDate, (int)AbpSession.TenantId);
            input.DistApplied = _timeZoneConverter.Convert(input.DistApplied, (int)AbpSession.TenantId);
            input.DistApproveDate = _timeZoneConverter.Convert(input.DistApproveDate, (int)AbpSession.TenantId);
            input.DistExpiryDate = _timeZoneConverter.Convert(input.DistExpiryDate, (int)AbpSession.TenantId);
            input.InvPaidDate = _timeZoneConverter.Convert(input.InvPaidDate, (int)AbpSession.TenantId);

            var job = _jobRepository.GetAll().Where(e => e.Id == input.Id).FirstOrDefault();
            //var UserDetail = _lookup_userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 14;
            leadactivity.SectionId = input.SectionId != null ? input.SectionId : 0;
            leadactivity.ActionNote = "Meter Details are updated : " + input.JobNumber;
            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var jobactionid = _leadactivityRepository.InsertAndGetId(leadactivity);


            var list = new List<JobTrackerHistory>();


            if (job.JobStatusId != input.JobStatusId)
            {
                PreviousJobStatus jobStstus = new PreviousJobStatus();
                jobStstus.JobId = job.Id;
                if (AbpSession.TenantId != null)
                {
                    jobStstus.TenantId = (int)AbpSession.TenantId;
                }
                jobStstus.CurrentID = 8;
                jobStstus.PreviousId = (int)job.JobStatusId;
                await _previousJobStatusRepository.InsertAsync(jobStstus);

                job.JobStatusId = 8;
            }
            //leadactivity.ActionNote = "Meter Details are updated";

            //leadactivity.LeadId = Convert.ToInt32(job.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }


            if (job.ElecDistributorId != input.ElecDistributorId)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "ElecDistributor";
                jobhistory.PrevValue = job.ElecDistributorId.ToString();
                jobhistory.CurValue = input.ElecDistributorId.ToString();
                jobhistory.Action = "Grid Connection Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.ElecRetailerId != input.ElecRetailerId)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "ElecRetailer";
                jobhistory.PrevValue = job.ElecRetailerId.ToString();
                jobhistory.CurValue = input.ElecRetailerId.ToString();
                jobhistory.Action = "Grid Connection Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.NMINumber != input.NMINumber)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "NMINumber";
                jobhistory.PrevValue = job.NMINumber;
                jobhistory.CurValue = input.NMINumber;
                jobhistory.Action = "Grid Connection Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.PeakMeterNo != input.PeakMeterNo)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "PeakMeterNo";
                jobhistory.PrevValue = job.PeakMeterNo;
                jobhistory.CurValue = input.PeakMeterNo;
                jobhistory.Action = "Grid Connection Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.OffPeakMeter != input.OffPeakMeter)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "OffPeakMeter";
                jobhistory.PrevValue = job.OffPeakMeter;
                jobhistory.CurValue = input.OffPeakMeter;
                jobhistory.Action = "Grid Connection Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.MeterPhaseId != input.MeterPhaseId)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "MeterPhase";
                jobhistory.PrevValue = job.MeterPhaseId.ToString();
                jobhistory.CurValue = input.MeterPhaseId.ToString();
                jobhistory.Action = "Grid Connection Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.LotNumber != input.LotNumber)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "LotNumber";
                jobhistory.PrevValue = job.LotNumber;
                jobhistory.CurValue = input.LotNumber;
                jobhistory.Action = "Grid Connection Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.RegPlanNo != input.RegPlanNo)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "RegPlanNo";
                jobhistory.PrevValue = job.RegPlanNo;
                jobhistory.CurValue = input.RegPlanNo;
                jobhistory.Action = "Grid Connection Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.MeterApplyRef != input.MeterApplyRef)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "MeterApplyRef";
                jobhistory.PrevValue = job.MeterApplyRef;
                jobhistory.CurValue = input.MeterApplyRef;
                jobhistory.Action = "Grid Connection Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.InspectorName != input.InspectorName)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "InspectorName";
                jobhistory.PrevValue = job.InspectorName;
                jobhistory.CurValue = input.InspectorName;
                jobhistory.Action = "Grid Connection Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.InspectionDate != input.InspectionDate)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "MeterAppliedRefDate";
                jobhistory.PrevValue = job.InspectionDate.ToString();
                jobhistory.CurValue = input.InspectionDate.ToString();
                jobhistory.Action = "Grid Connection Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.DistApplied != input.DistApplied)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "DistApplied";
                jobhistory.PrevValue = job.DistApplied.ToString();
                jobhistory.CurValue = input.DistApplied.ToString();
                jobhistory.Action = "Grid Connection Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.DistApproveDate != input.DistApproveDate)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "DistApproveDate";
                jobhistory.PrevValue = job.DistApproveDate.ToString();
                jobhistory.CurValue = input.DistApproveDate.ToString();
                jobhistory.Action = "Grid Connection Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }
            if (job.InvPaidDate != input.InvPaidDate)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "InvPaidDate";
                jobhistory.PrevValue = job.InvPaidDate.ToString();
                jobhistory.CurValue = input.InvPaidDate.ToString();
                jobhistory.Action = "Grid Connection Tracker Edit";
                jobhistory.LastmodifiedDateTime = DateTime.Now;
                jobhistory.JobIDId = job.Id;
                jobhistory.JobActionId = jobactionid;
                list.Add(jobhistory);
            }

            await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, job);
            //job.MeterTime = input.MeterTime;
            //job.MeterApplyRef = input.MeterApplyRef;
            //job.InspectorName = input.InspectorName;
            //job.InspectionDate = input.InspectionDate;
            //await _jobRepository.UpdateAsync(job);
            // await JobActive(job.Id); 
            //await _leadactivityRepository.InsertAsync(leadactivity);

        }

        public async Task<List<CommonLookupDto>> GetAllUsersTableDropdown()
        {
            var list = await _userRepository.GetAll()
               .Select(users => new CommonLookupDto
               {
                   Id = (int)users.Id,
                   DisplayName = users == null || users.FullName == null ? "" : users.FullName.ToString()
               }).ToListAsync();

            return list;
        }

        public async Task<JobApplicationViewDto> GetDataForQuickViewByJobID(int Id, int? sectionid)
        {
            try
            {
                var job = await _jobRepository.FirstOrDefaultAsync(Id);

                var output = new JobApplicationViewDto { Job = ObjectMapper.Map<CreateOrEditJobDto>(job) };
                //var ProductItemList = _jobProductItemRepository.GetAll().Where(j => j.JobId == Id).Select(e => e.ProductItemId).ToList();
                //var ProductItemsList = _jobProductItemRepository.GetAll().Where(j => j.JobId == Id).ToList();
                //var PanelDetail = _productItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 1).FirstOrDefault();
                //var InverterDetail = _productItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 2).FirstOrDefault();
                var Map = _documentRepository.GetAll().Where(j => j.JobId == Id).ToList();
                var Finances = _financeOptionRepository.GetAll().Where(e => e.Id == job.FinanceOptionId).Select(e => e.Name).FirstOrDefault();
                var Dep = _depositOptionRepository.GetAll().Where(e => e.Id == job.DepositOptionId).Select(e => e.Name).FirstOrDefault();
                var Payment = _paymentOptionRepository.GetAll().Where(e => e.Id == job.PaymentOptionId).Select(e => e.Name).FirstOrDefault();

                var installerinvoice = _installerInvoiceRepository.GetAll().Where(e => e.JobId == Id).OrderByDescending(e => e.Id).FirstOrDefault();

                output.Doc = await GetJobDocumentsByJobId(job.Id);
                output.InvPaymentList = await GetInvoicesByJobId(job.Id);

                if (output.Job.JobTypeId != null)
                {
                    var _lookupJobType = await _lookup_jobTypeRepository.FirstOrDefaultAsync((int)output.Job.JobTypeId);
                    output.JobTypeName = _lookupJobType?.Name?.ToString();
                }

                if (output.Job.JobStatusId != null)
                {
                    var _lookupJobStatus = await _lookup_jobStatusRepository.FirstOrDefaultAsync((int)output.Job.JobStatusId);
                    output.JobStatusName = _lookupJobStatus?.Name?.ToString();
                }

                if (output.Job.RoofTypeId != null)
                {
                    var _lookupRoofType = await _lookup_roofTypeRepository.FirstOrDefaultAsync((int)output.Job.RoofTypeId);
                    output.RoofTypeName = _lookupRoofType?.Name?.ToString();
                }

                if (output.Job.RoofAngleId != null)
                {
                    var _lookupRoofAngle = await _lookup_roofAngleRepository.FirstOrDefaultAsync((int)output.Job.RoofAngleId);
                    output.RoofAngleName = _lookupRoofAngle?.Name?.ToString();
                }

                if (output.Job.ElecDistributorId != null)
                {
                    var _lookupElecDistributor = await _lookup_elecDistributorRepository.FirstOrDefaultAsync((int)output.Job.ElecDistributorId);
                    output.ElecDistributorName = _lookupElecDistributor?.Name?.ToString();
                }

                if (output.Job.LeadId != null)
                {
                    var _lookupLead = await _lookup_leadRepository.FirstOrDefaultAsync((int)output.Job.LeadId);
                    output.LeadCompanyName = _lookupLead?.CompanyName?.ToString();
                    output.Mobileno = _lookupLead?.Mobile?.ToString();
                    output.Phone = _lookupLead?.Phone?.ToString();
                    output.Email = _lookupLead?.Email?.ToString();

                    output.Job.Latitude = _lookupLead.latitude;
                    output.Job.Longitude = _lookupLead.longitude;
                    output.Job.orgid = _lookupLead.OrganizationId;
                }

                if (output.Job.ElecRetailerId != null)
                {
                    var _lookupElecRetailer = await _lookup_elecRetailerRepository.FirstOrDefaultAsync((int)output.Job.ElecRetailerId);
                    output.ElecRetailerName = _lookupElecRetailer?.Name?.ToString();
                }

                if (output.Job.PaymentOptionId != null)
                {
                    var _lookupPaymentOption = await _lookup_paymentOptionRepository.FirstOrDefaultAsync((int)output.Job.PaymentOptionId);
                    output.PaymentOptionName = _lookupPaymentOption?.Name?.ToString();
                }

                if (output.Job.DepositOptionId != null)
                {
                    var _lookupDepositOption = await _lookup_depositOptionRepository.FirstOrDefaultAsync((int)output.Job.DepositOptionId);
                    output.DepositOptionName = _lookupDepositOption?.Name?.ToString();
                }

                if (output.Job.MeterUpgradeId != null)
                {
                    var _lookupMeterUpgrade = await _lookup_meterUpgradeRepository.FirstOrDefaultAsync((int)output.Job.MeterUpgradeId);
                    output.MeterUpgradeName = _lookupMeterUpgrade?.Name?.ToString();
                }

                if (output.Job.MeterPhaseId != null)
                {
                    var _lookupMeterPhase = await _lookup_meterPhaseRepository.FirstOrDefaultAsync((int)output.Job.MeterPhaseId);
                    output.MeterPhaseName = _lookupMeterPhase?.Name?.ToString();
                }

                if (output.Job.PromotionOfferId != null)
                {
                    var _lookupPromotionOffer = await _lookup_promotionOfferRepository.FirstOrDefaultAsync((int)output.Job.PromotionOfferId);
                    output.PromotionOfferName = _lookupPromotionOffer?.Name?.ToString();
                }

                if (output.Job.HouseTypeId != null)
                {
                    var _lookupHouseType = await _lookup_houseTypeRepository.FirstOrDefaultAsync((int)output.Job.HouseTypeId);
                    output.HouseTypeName = _lookupHouseType?.Name?.ToString();
                }

                if (output.Job.FinanceOptionId != null)
                {
                    var _lookupFinanceOption = await _lookup_financeOptionRepository.FirstOrDefaultAsync((int)output.Job.FinanceOptionId);
                    output.FinanceOptionName = _lookupFinanceOption?.Name?.ToString();
                }

                if (output.Job.RefferedJobId != null)
                {
                    //var _lookupFinanceOption = await _lookup_financeOptionRepository.FirstOrDefaultAsync((int)output.Job.FinanceOptionId);
                    output.ReferralPayDate = job.ReferralPayDate;
                    output.ReferralPayment = job.ReferralPayment;
                    output.ReferralAmount = job.ReferralAmount;
                    output.ReferralRemark = job.ReferralRemark;
                }

                //if (PanelDetail != null)
                //{
                //    var PanelQTY = ProductItemsList.Where(e => e.ProductItemId == PanelDetail.Id).Select(e => e.Quantity).FirstOrDefault();

                //    output.Panel = PanelQTY + " x " + PanelDetail.Name;
                //    output.PanelModelNo = PanelDetail.Model;
                //    output.PanelQTY = PanelQTY;
                //    output.PanelNAme = PanelDetail.Name;
                //}
                //if (InverterDetail != null)
                //{
                //    var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail.Id).Select(e => e.Quantity).FirstOrDefault();

                //    output.Inverter = InverterQTY + " x " + InverterDetail.Name;
                //    output.InverterModelNo = InverterDetail.Model;
                //    output.InverterQTY = InverterQTY;
                //    output.InverterNAme = InverterDetail.Name;
                //    output.InverterOutPut = InverterDetail.Size;
                //}

                if (job.PaymentOptionId == 1)
                {
                    output.Finance = "Cash";
                }
                else
                {
                    output.Finance = Payment + " with " + Dep + " for " + Finances;
                }
                output.RegPlanNo = job.RegPlanNo;
                output.LotNumber = job.LotNumber;
                output.Suburb = job.Suburb;
                output.State = job.State;
                output.UnitNo = job.UnitNo;
                output.UnitType = job.UnitType;
                output.NMINumber = job.NMINumber;
                output.ApplicationRefNo = job.ApplicationRefNo;
                output.DistAppliedDate = job.DistAppliedDate;
                output.Notes = job.Note;
                output.InstallerNotes = job.InstallerNotes;
                output.JobNumber = job.JobNumber;
                output.MeterNumber = job.PeakMeterNo;
                output.OldSystemDetails = job.OldSystemDetails;
                output.PostalCode = job.PostalCode;
                output.StreetName = job.StreetName;
                output.StreetNo = job.StreetNo;
                output.StreetType = job.StreetType;
                output.LeadId = job.LeadId;
                output.ElecDistributorId = job.ElecDistributorId;
                output.ElecRetailerId = job.ElecRetailerId;
                output.JobStatusId = job.JobStatusId;
                output.JobTypeId = job.JobTypeId;
                output.PVDStatus = job.PVDStatus;
                output.STCAppliedDate = job.STCAppliedDate;
                output.STCUploaddate = job.STCUploaddate;
                output.STCUploadNumber = job.STCUploadNumber;
                output.PVDNumber = job.PVDNumber;
                output.STCNotes = job.STCNotes;
                output.DistAppliedDate = job.DistAppliedDate;
                output.Note = job.Note;
                output.NMINumber = job.NMINumber;
                output.InstallerNotes = job.InstallerNotes;
                output.SystemCapacity = job.SystemCapacity;
                output.adress = job.Address;
                output.MeterNumber = job.PeakMeterNo;
                output.JobID = job.Id;
                output.DistApplied = job.DistApplied;
                output.DistExpiryDate = job.DistExpiryDate;
                output.ApplicationNotes = job.ApplicationNotes;
                output.InstallerDepartmentNotes = job.InstallationNotes;
                //output.Price = job.TotalCost;
                output.FinanceNotes = job.FinanceNotes;
                output.RebateAppRef = job.RebateAppRef;
                output.ApprovalDate = job.ApprovalDate;
                output.ExpiryDate = job.ExpiryDate;
                output.VicRebate = job.VicRebate;
                output.SolarVICRebate = job.SolarVICRebate;
                output.SolarVICLoanDiscont = job.SolarVICLoanDiscont;
                output.SolarRebateStatus = job.SolarRebateStatus;
                output.vicRebateNotes = job.VicRebateNotes;
                output.FinanceApplicationDate = job.FinanceApplicationDate;
                output.FinancePurchaseNo = job.FinancePurchaseNo;
                output.DocumentsVeridiedDate = job.DocumentsVeridiedDate;
                output.CoolingoffPeriodEnd = job.CoolingoffPeriodEnd;
                output.PeakMeterNo = job.PeakMeterNo;
                output.OffPeakMeter = job.OffPeakMeter;
                output.MeterPhaseId = job.MeterPhaseId;
                output.EnoughMeterSpace = job.EnoughMeterSpace == true ? "Yes" : "No";
                output.IsSystemOffPeak = job.IsSystemOffPeak == true ? "Yes" : "No";
                if (output.Job.DocumentsVeridiedBy != null)
                {
                    var _lookupDocverify = await _userRepository.FirstOrDefaultAsync((int)output.Job.DocumentsVeridiedBy);
                    output.DocumentsVeridiedByName = _lookupDocverify?.Name?.ToString();
                }
                output.FinanceDocumentVerified = job.FinanceDocumentVerified;
                output.FinanceNotes = job.FinanceNotes;
                output.GridConnectionNotes = job.GridConnectionNotes;
                output.gridFilename = job.ApprovalLetter_Filename;
                output.gridFilePath = job.ApprovalLetter_FilePath;
                output.creationTime = job.CreationTime;
                output.DepositeRecceivedDate = job.DepositeRecceivedDate;
                output.ApplicationDate = job.DistApplied;
                output.ApproveDate = job.DistApproveDate;
                output.ActiveDate = job.ActiveDate;
                output.ApprovalDate = job.ApprovalDate;
                output.FinanceApplicationDate = job.FinanceApplicationDate;
                output.FinanceDocReceivedDate = job.FinanceDocReceivedDate;
                output.QuoteCreated = _quotationRepository.GetAll().Where(e => e.JobId == job.Id).OrderByDescending(e => e.Id).Select(e => e.CreationTime).FirstOrDefault();
                output.createdBy = (_userRepository.GetAll().Where(e => e.Id == job.CreatorUserId).Select(e => e.UserName).FirstOrDefault());
                output.ApprovalFilelist = (from flist in _JobApprovalFileHistoryRepository.GetAll()
                                           where (flist.jobid == Id && flist.FieldName == "FileName" && flist.PrevValue != null && flist.CurValue != null)
                                           select new JobApprovalFileHistoryDto
                                           {
                                               fileName = flist.PrevValue,
                                               documentPath = job.ApprovalLetter_FilePath,
                                               creationTime = flist.CreationTime,
                                               createdBy = _userRepository.GetAll().Where(e => e.Id == flist.CreatorUserId).Select(e => e.UserName).FirstOrDefault(),
                                           }).OrderByDescending(e => e.creationTime).ToList();

                if (sectionid == 22)
                {
                    output.SubCatServieDocFilelist = (from flist in _ServiceCAtegoryDocRepository.GetAll()
                                                      join o1 in _serviceRepository.GetAll() on flist.SubCategoryId equals o1.ServiceSubCategoryId into j1
                                                      from s1 in j1.DefaultIfEmpty()
                                                      where (s1.JobId == Id)
                                                      select new JobApprovalFileHistoryDto
                                                      {
                                                          fileName = flist.FileName,
                                                          documentPath = flist.FilePath,
                                                          creationTime = flist.CreationTime,
                                                          createdBy = _userRepository.GetAll().Where(e => e.Id == flist.CreatorUserId).Select(e => e.UserName).FirstOrDefault(),
                                                      }).OrderByDescending(e => e.creationTime).ToList();
                }

                if (installerinvoice != null)
                {
                    output.ActualPanelsInstalled = installerinvoice.ActualPanels_Installed;
                    output.AllGoodNotGood = installerinvoice.AllGood_NotGood;
                    output.CES = installerinvoice.CES;
                    output.CxSign = installerinvoice.Cx_Sign;
                    output.EmailSent = installerinvoice.EmailSent;
                    output.Frontofproperty = installerinvoice.Front_of_property;
                    output.InstDesElesign = installerinvoice.Inst_Des_Ele_sign;
                    output.InstalaltionPic = installerinvoice.InstalaltionPic;
                    output.InstMaintInspPic = installerinvoice.Installation_Maintenance_Inspection;
                    output.Installerselfie = installerinvoice.Installer_selfie;
                    output.InverterSerialNo = installerinvoice.InverterSerialNo;
                    output.InvoiceAmount = installerinvoice.InvoiceAmount;
                    output.InvoiceNo = installerinvoice.InvoiceNo;
                    output.NoofPanelsinvoice = installerinvoice.Noof_Panels_invoice;
                    output.NoofPanelsPortal = installerinvoice.Noof_Panels_Portal;
                    output.NotesOrReasonforPending = installerinvoice.NotesOrReasonforPending;
                    output.PanelsSerialNo = installerinvoice.PanelsSerialNo;
                    output.Remarkifowing = installerinvoice.Remark_if_owing;
                    output.Splits = installerinvoice.Splits;
                    output.Traded = installerinvoice.Traded;
                    output.TravelKMfromWarehouse = installerinvoice.TravelKMfromWarehouse;
                    output.WiFiDongle = installerinvoice.Wi_FiDongle;
                    output.otherExtraInvoiceNumber = installerinvoice.otherExtraInvoiceNumber;
                    output.Filenameinv = installerinvoice.FileName;
                    output.FilePathinv = installerinvoice.FilePath;
                    output.filelist = (from flist in _InstInvoiceHistoryRepository.GetAll()
                                       where (flist.InvoiceInstallerId == installerinvoice.Id && flist.FieldName == "FileName" && flist.PrevValue != null)
                                       select new installerinvoicefileDto
                                       {
                                           fileName = flist.PrevValue,
                                           documentPath = installerinvoice.FilePath,
                                           creationTime = flist.CreationTime,
                                           createdBy = _userRepository.GetAll().Where(e => e.Id == flist.CreatorUserId).Select(e => e.UserName).FirstOrDefault(),
                                       }).OrderByDescending(e => e.creationTime).ToList();
                }

                if (job.InstallerId != null)
                {
                    var _lookupInstallerName = await _userRepository.FirstOrDefaultAsync((int)job.InstallerId);
                    var installerDetail = _installerDetail.GetAll().Where(e => e.UserId == job.InstallerId).FirstOrDefault();
                    output.InstallerName = _lookupInstallerName?.FullName?.ToString();
                    output.InstallerMobile = _lookupInstallerName?.Mobile?.ToString();
                    output.InstallerEmail = _lookupInstallerName?.EmailAddress?.ToString();
                    if (installerDetail != null)
                    {
                        var state = _StateRepositery.GetAll().Where(e => e.Id == installerDetail.StateId).Select(e => e.Name).FirstOrDefault();
                        output.InstallerAdress = installerDetail.Unit + " " + installerDetail.UnitType + " " + installerDetail.StreetNo + " " + installerDetail.StreetName + " " + installerDetail.StreetType + " " + installerDetail.Suburb + "," + state + "," + installerDetail.PostCode;
                        output.AccExpDate = installerDetail.InstallerAccreditationExpiryDate;
                        output.AccCodeExpDate = installerDetail.InstallerAccreditationNumber;
                        output.ElectCodeExpDate = installerDetail.ElectricianLicenseNumber;
                        output.DesignCodeExpDate = installerDetail.DesignerLicenseNumber;
                        output.ElectExpDate = installerDetail.ElectricianLicenseExpiryDate;
                        output.DesignExpDate = installerDetail.DesignerLicenseExpiryDate;
                    }
                }

                var jobVarition = _jobVariationRepository.GetAll().Where(e => e.JobId == (int?)Id);
                output.Variations = (from o in jobVarition
                                     select new NamingValueDto
                                     {
                                         Name = o.VariationFk.Name,
                                         Value = o.Cost == null ? "0.00" : Convert.ToDecimal(o.Cost).ToString("F")
                                     }).ToList();

                var jobProduct = _jobProductItemRepository.GetAll().Where(e => e.JobId == (int?)Id).OrderBy(e => e.ProductItemFk.ProductTypeId);
                output.JobProducts = (from o in jobProduct
                                      select new JobProductItems
                                      {
                                          Category = o.ProductItemFk.ProductTypeFk.Name,
                                          Name = o.ProductItemFk.Name,
                                          Quantity = o.Quantity
                                      }).ToList();

                return output;
            }
            catch (Exception e) { throw e; }
        }

        public async Task<List<GetJobDocumentForView>> GetJobDocumentsByJobId(int jobId)
        {
            var jobdocuments = _documentRepository.GetAll().Include(e => e.JobFk)
                .Include(e => e.DocumentTypeFk)
                .OrderByDescending(e => e.Id)
                .Where(x => x.JobId == jobId);

            var filteredJobs = from o in jobdocuments
                               join o1 in _jobRepository.GetAll() on o.JobId equals o1.Id into j1
                               from s1 in j1.DefaultIfEmpty()

                               join o2 in _lookup_documentTypeRepository.GetAll() on o.DocumentTypeId equals o2.Id into j2
                               from s2 in j2.DefaultIfEmpty()

                               join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                               from s3 in j3.DefaultIfEmpty()

                               select new GetJobDocumentForView()
                               {
                                   Id = o.Id,
                                   DocumentTypeId = o.DocumentTypeId,
                                   MediaId = o.MediaId,
                                   JobId = o.JobId,
                                   FileName = o.FileName,
                                   FileType = o.FileType,
                                   DocumentPath = o.FilePath,
                                   DocumentTitle = s2 == null || s2.Title == null ? "" : s2.Title.ToString(),
                                   CreatedBy = s3.FullName,
                                   CreationTime = o.CreationTime
                               };

            return filteredJobs.ToList();
        }

        public async Task<Boolean> CheckExistJobsiteAddList(ExistJobSiteAddDto input)
        {
            var User = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

            IList<string> role = await _userManager.GetRolesAsync(User);

            if (role.Contains("Admin"))
            {
                return false;
            }

            if (input.jobTypeId == 2)
            {
                return false;
            }
            if (input.jobid == null)
            {
                if (!string.IsNullOrWhiteSpace(input.Suburb))
                {
                    string[] splitPipeValue = input.Suburb.Split('|');
                    if (splitPipeValue.Length > 0)
                    {
                        input.Suburbs = splitPipeValue[0].ToString().Trim();
                    }
                    else
                    {
                        input.Suburbs = input.Suburb;
                    }
                }
                var duplicateAdd = _jobRepository.GetAll().Where(e => (e.UnitNo == input.UnitNo && e.UnitType == input.UnitType && e.StreetNo == input.StreetNo && e.StreetName == input.StreetName
                               && e.StreetType == input.StreetType && e.Suburb == input.Suburb && e.State == input.State && e.PostalCode == input.PostCode) && e.LeadFk.OrganizationId == input.OrganizationId
                               ).Any();


                return duplicateAdd;

            }
            else
            {
                if (!string.IsNullOrWhiteSpace(input.Suburb))
                {
                    string[] splitPipeValue = input.Suburb.Split('|');
                    if (splitPipeValue.Length > 0)
                    {
                        input.Suburbs = splitPipeValue[0].ToString().Trim();
                    }
                    else
                    {
                        input.Suburbs = input.Suburb;
                    }
                }
                var duplicateAdd = _jobRepository.GetAll().Where(e => (e.UnitNo == input.UnitNo && e.UnitType == input.UnitType && e.StreetNo == input.StreetNo && e.StreetName == input.StreetName
                                && e.StreetType == input.StreetType && e.Suburb == input.Suburbs && e.State == input.State && e.PostalCode == input.PostCode) && e.LeadFk.OrganizationId == input.OrganizationId
                                && e.Id != input.jobid).Any();


                return duplicateAdd;

            }
        }

        /// <summary>
        /// Job Create Permission is only to Assigned User Only
        /// </summary>
        /// <param name="leadid">Lead Id</param>
        /// <returns></returns>
        public int checkjobcreatepermission(int leadid)
        {
            var User_List = _userRepository.GetAll().AsNoTracking();

            var RoleName = (from user in User_List
                            join ur in _userRoleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            where (us != null && user.Id == AbpSession.UserId)
                            select (us.DisplayName));

            int? AssignUserid = _lookup_leadRepository.GetAll().Where(e => e.AssignToUserID == AbpSession.UserId && e.Id == leadid).Select(e => e.AssignToUserID).FirstOrDefault();

            if (RoleName.Contains("Admin"))
            {
                return 1;
            }
            else
            {
                if (AssignUserid > 0)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }

        }

        /// <summary>
        /// Bind All User Dropdown For Admin For Other User Only User Will Bind
        /// </summary>
        /// <returns></returns>
        public async Task<List<CommonLookupDto>> GetAllUsers()
        {
            var User = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            if (role.Contains("Admin"))
            {
                return await _userRepository.GetAll()
                .Select(users => new CommonLookupDto
                {
                    Id = (int)users.Id,
                    DisplayName = users == null || users.FullName == null ? "" : users.FullName.ToString()
                }).ToListAsync();
            }
            else
            {
                return await _userRepository.GetAll()
                    .Where(e => e.Id == AbpSession.UserId)
                .Select(users => new CommonLookupDto
                {
                    Id = (int)users.Id,
                    DisplayName = users == null || users.FullName == null ? "" : users.FullName.ToString()
                }).ToListAsync();
            }

        }




        /// <summary>
        /// Get Total Count For All Tracker
        /// </summary>
        /// <param name="organizationid">Organization Id</param>
        /// <returns></returns>
        public async Task<TotalSammaryCountDto> getAllApplicationTrackerCount(int organizationid)
        {
            var output = new TotalSammaryCountDto();

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var JobList = _jobRepository.GetAll().Where(e => e.LeadFk.OrganizationId == organizationid)
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id);

            //Application Tracker Count
            var Doclist = _documentRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.DocumentTypeId == 2).Select(e => e.JobId).ToList();

            output.TotalAppTractorData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobStatusId != 3 && e.JobStatusId != 1 && e.MeterPhaseId != 0 && e.MeterPhaseId != null && e.PeakMeterNo != null && e.NMINumber != null && Doclist.Contains(e.Id) && e.LeadFk.OrganizationId == organizationid).Count();

            output.TotalAppTractorVerifyData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobStatusId != 3 && e.JobStatusId != 1 && e.MeterPhaseId != 0 && e.MeterPhaseId != null && e.PeakMeterNo != null && e.NMINumber != null && Doclist.Contains(e.Id) && e.LeadFk.OrganizationId == organizationid && e.ApplicationRefNo != null).Count();

            output.TotalAppTractornotVerifyData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobStatusId != 3 && e.JobStatusId != 1 && e.MeterPhaseId != 0 && e.MeterPhaseId != null && e.PeakMeterNo != null && e.NMINumber != null && Doclist.Contains(e.Id) && e.LeadFk.OrganizationId == organizationid && e.ApplicationRefNo == null).Count();

            output.TotalExpiryData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.DistExpiryDate != null && e.ActiveDate != null && e.DistExpiryDate >= DateTime.UtcNow && e.DistExpiryDate <= DateTime.UtcNow.AddDays(30) && (e.JobStatusId >= 4 && e.JobStatusId <= 7) && e.MeterPhaseId != 0 && e.MeterPhaseId != null && e.PeakMeterNo != null && e.NMINumber != null && Doclist.Contains(e.Id) && e.ApplicationRefNo != null && e.LeadFk.OrganizationId == organizationid).Count();

            output.TotalAppTractorAwaitingData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobStatusId != 3 && e.JobStatusId != 1 && (e.MeterPhaseId == 0 || e.MeterPhaseId == null || e.PeakMeterNo == null || e.NMINumber == null || !Doclist.Contains(e.Id)) && e.LeadFk.OrganizationId == organizationid)
                .Where(e => e.ApplicationRefNo == null).Count();

            //Job Active Tracker Count

            var MeterPhoto = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 3).Select(e => e.JobId).Distinct().ToList();
            var Deposites = _invoicePaymentRepository.GetAll().Select(e => e.JobId).Distinct().ToList();
            var SignedQuoteordoc = (_quotationRepository.GetAll().Where(e => e.IsSigned == true).Select(e => e.JobId).Distinct().ToList()).Union(_documentRepository.GetAll().Where(e => e.DocumentTypeId == 5).Select(e => e.JobId).Distinct().ToList());
            var doc = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 5).Select(e => e.JobId).Distinct().ToList();
            //var SignedQuoteordoc = new List<int?>();
            //if (SignedQuote.Count() > 0)
            //{
            //    SignedQuoteordoc = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 5).Select(e => e.JobId).Distinct().ToList();
            //}
            //else
            //{
            //    SignedQuoteordoc = _quotationRepository.GetAll().Where(e => e.IsSigned == true).Select(e => e.JobId).Distinct().ToList();
            //}
            output.TotalActiveData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.DepositeRecceivedDate != null && e.LeadFk.OrganizationId == organizationid && Deposites.Contains(e.Id)).Count();

            output.TotalActiveVerifyData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.IsActive == true && e.ActiveDate != null && e.JobStatusId == 5 && e.LeadFk.OrganizationId == organizationid && e.ActiveDate != null).Count();

            output.TotalActiveDatanotVerifyData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.ActiveDate == null && e.JobStatusId == 4 && Deposites.Contains(e.Id) && (e.NMINumber != null && e.ElecRetailerId != 0 && e.ApplicationRefNo != null && e.MeterUpgradeId != null && e.MeterUpgradeId != 0 && MeterPhoto.Contains(e.Id) && SignedQuoteordoc.Contains(e.Id)) && (e.JobStatusId != 3 && e.JobStatusId != 8) && e.LeadFk.OrganizationId == organizationid && e.ActiveDate == null).Count();

            //output.TotalActiveDatanotAwaitingData = _jobRepository.GetAll()
            //    .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            //    .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            //    .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            //    .Where(e => e.IsActive != true && e.JobStatusId == 4 && Deposites.Contains(e.Id) && (e.NMINumber == null || e.ElecRetailerId == 0 || e.ApplicationRefNo == null || e.MeterUpgradeId == null || e.MeterUpgradeId == 0 || !MeterPhoto.Contains(e.Id) || !SignedQuoteordoc.Contains(e.Id)) && (e.JobStatusId != 3 && e.JobStatusId != 8) && e.LeadFk.OrganizationId == organizationid && Deposites.Contains(e.Id) && (e.NMINumber == null || e.ApplicationRefNo == null || e.MeterUpgradeId == null || e.MeterUpgradeId == 0 || !MeterPhoto.Contains(e.Id) || !SignedQuoteordoc.Contains(e.Id)  /*|| (e.PaymentOptionId == 1 ? true : e.FinanceDocumentVerified)*/) && (e.JobStatusId != 3 && e.JobStatusId != 8)).Count();

            //output.TotalActiveDatanotAwaitingData = JobList
            //    .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            //    .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            //    .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            //    .Where(e => e.LeadFk.OrganizationId == organizationid && e.IsActive != true && e.JobStatusId == 4 && Deposites.Contains(e.Id) && e.ApplicationRefNo == null && (e.NMINumber == null || e.ElecRetailerId == 0 || e.ElecRetailerId == null || e.MeterUpgradeId == null || e.MeterUpgradeId == 0 || !MeterPhoto.Contains(e.Id) || !SignedQuoteordoc.Contains(e.Id)) && (e.JobStatusId != 3 && e.JobStatusId != 8))
            //    .Count();

            output.TotalActiveDatanotAwaitingData = JobList
                .Where(e => e.JobStatusId != 3 && e.JobStatusId != 1 && (e.MeterPhaseId == 0 || e.MeterPhaseId == null || e.PeakMeterNo == null || string.IsNullOrEmpty(e.NMINumber) || !Doclist.Contains(e.Id)))
                .Where(e => e.JobStatusId != 3 && e.JobStatusId != 1 && e.MeterPhaseId != 0 && e.MeterPhaseId != null && e.PeakMeterNo != null && !string.IsNullOrEmpty(e.NMINumber) && Doclist.Contains(e.Id))
                .Where(e => e.ApplicationRefNo == "")
                .Count();


            //Finance Tracker Count
            output.TotalfinanceData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.PaymentOptionId != null && e.PaymentOptionId != 1 && Deposites.Contains(e.Id) && e.LeadFk.OrganizationId == organizationid).Count();
            output.TotalfinanceVerifyData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.FinanceDocumentVerified == 1 && Deposites.Contains(e.Id) && e.LeadFk.OrganizationId == organizationid).Count();
            //output.TotalfinanceDatanotVerifyData = _jobRepository.GetAll()
            //    .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            //    .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            //    .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            //    .Where(e => e.PaymentOptionId != null && e.PaymentOptionId != 1 && e.FinanceDocumentVerified != 1 && Deposites.Contains(e.Id) && e.LeadFk.OrganizationId == organizationid).Count();
            output.TotalfinanceDatanotVerifyData = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.PaymentOptionId != null && e.PaymentOptionId != 1 && Deposites.Contains(e.Id) && e.LeadFk.OrganizationId == organizationid)
                .Where(e => e.FinanceDocumentVerified == 2 || e.FinanceDocumentVerified == null).Count();
            output.TotalfinanceQueryData = _jobRepository.GetAll()
               .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
               .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
               .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
               .Where(e => e.FinanceDocumentVerified == 3 && e.PaymentOptionId != null && e.PaymentOptionId != 1 && Deposites.Contains(e.Id) && e.LeadFk.OrganizationId == organizationid).Count();

            //Job Refund Tracker Count
            output.TotaljobrefundData = _jobRefundRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == organizationid).Count();
            output.TotaljobrefundVerifyData = _jobRefundRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == organizationid).Where(e => e.PaidDate != null).Count();
            output.TotaljobrefundnotVerifyData = _jobRefundRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == organizationid).Where(e => e.PaidDate == null).Count();

            //Invoice Payment Tracker Count
            output.TotalpaymentData = _invoicePaymentRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == organizationid).Count();
            output.TotalpaymentVerifyData = _invoicePaymentRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == (_jobRepository.GetAll().Where(e => e.Id == j.JobId).Select(e => e.LeadId).FirstOrDefault())).Select(e => e.OrganizationId).FirstOrDefault()) == organizationid).Where(e => e.IsVerified == true).Count();
            output.TotalpaymentnotVerifyData = _invoicePaymentRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(j => j.JobFk.LeadFk.OrganizationId == organizationid).Where(e => e.IsVerified == false).Count();


            output.TotalAmountOfInvoice = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.LeadFk.OrganizationId == organizationid && _invoicePaymentRepository.GetAll().Select(e => e.JobId).Contains(e.Id)).Select(e => e.TotalCost).Sum();



            output.totalrefundamount = _jobRefundRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(j => j.JobFk.LeadFk.OrganizationId == organizationid).Select(e => e.Amount).Sum();


            output.TotalAmountReceived = (_invoicePaymentRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(j => j.JobFk.LeadFk.OrganizationId == organizationid).Select(e => e.InvoicePayTotal).Sum()) - (output.totalrefundamount);
            output.BalanceOwning = (output.TotalAmountOfInvoice - output.TotalAmountReceived);

            output.TotalCancelAmount = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.LeadFk.OrganizationId == organizationid && e.JobStatusId == 3 && _invoicePaymentRepository.GetAll().Select(e => e.JobId).Contains(e.Id)).Select(e => e.TotalCost).Sum();

            output.TotalVICAmountOfInvoice = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.LeadFk.OrganizationId == organizationid && e.LeadFk.State == "VIC" && _invoicePaymentRepository.GetAll().Select(e => e.JobId).Contains(e.Id)).Select(e => e.SolarVICRebate).Sum();

            output.TotalVICAmountReceived = 0;
            output.TotalVICBAlOwning = (output.TotalVICAmountOfInvoice - output.TotalVICAmountReceived);

            output.TotalCustAmountOfInvoice = (_jobRepository.GetAll()
            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            .Where(e => e.LeadFk.OrganizationId == organizationid && e.LeadFk.State == "VIC" && _invoicePaymentRepository.GetAll().Select(e => e.JobId).Contains(e.Id)).Select(e => e.TotalCost).Sum()) - (output.TotalVICAmountOfInvoice);

            output.TotalCustAmountReceived = _invoicePaymentRepository.GetAll()
            .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
            .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
            .Where(j => j.JobFk.LeadFk.OrganizationId == organizationid && j.JobFk.LeadFk.State == "VIC").Select(e => e.InvoicePayTotal).Sum();

            output.TotalCustBAlOwning = (output.TotalCustAmountOfInvoice - output.TotalCustAmountReceived);



            //Freebies Tracker Count
            output.TotalfreebieData = _jobPromotionRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid && e.JobFk.JobStatusId != 2 && e.JobFk.JobStatusId != 3).Count();
            output.TotalAwaitingData = _jobPromotionRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid && e.JobFk.JobStatusId != 2 && e.JobFk.JobStatusId != 3 && ((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum())) < e.JobFk.TotalCost).Count();
            output.TotalfreebieVerifyData = _jobPromotionRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid && e.TrackingNumber != null && e.DispatchedDate != null && e.JobFk.JobStatusId != 2 && e.JobFk.JobStatusId != 3 && (e.JobFk.TotalCost - ((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()))) == 0).Count();
            output.TotalfreebienotVerifyData = _jobPromotionRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                //.Where(e => e.JobFk.LeadFk.OrganizationId == organizationid && e.TrackingNumber == null && e.DispatchedDate == null && e.JobFk.TotalCost == ((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) <= 0)).Count();
                .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid && e.TrackingNumber == null && e.DispatchedDate == null && e.JobFk.JobStatusId != 2 && e.JobFk.JobStatusId != 3 && (e.JobFk.TotalCost - ((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.JobId).Select(e => e.PaidAmmount).Sum()))) <= 0).Count();

            /// Job grid
            var Panels = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1).Select(e => e.Id).ToList();
            var Inverts = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2).Select(e => e.Id).ToList();
            output.TotalNoOfPanel = _jobProductItemRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid && Panels.Contains((int)e.ProductItemId)).Select(e => e.Quantity).Sum();

            output.TotalNoOfInvert = _jobProductItemRepository.GetAll()
               .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
               .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
               .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
               .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid && Inverts.Contains((int)e.ProductItemId)).Select(e => e.Quantity).Sum();

            output.TotalSystemCapacity = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.LeadFk.OrganizationId == organizationid).Select(e => e.SystemCapacity).Sum();

            output.TotalCost = _jobRepository.GetAll()
               .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
               .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
               .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
               .Where(e => e.LeadFk.OrganizationId == organizationid).Select(e => e.TotalCost).Sum();

            output.Price = Math.Round(Convert.ToDecimal(output.TotalCost / output.TotalSystemCapacity));

            output.DepositeReceived = _jobRepository.GetAll()
               .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
               .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
               .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
               .Where(e => e.LeadFk.OrganizationId == organizationid && e.JobStatusId == 4).Select(e => e.Id).Count();

            output.Active = _jobRepository.GetAll()
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.LeadFk.OrganizationId == organizationid && e.JobStatusId == 5).Select(e => e.Id).Count();

            output.InstallJob = _jobRepository.GetAll()
               .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
               .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
               .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
               .Where(e => e.LeadFk.OrganizationId == organizationid && e.JobStatusId == 8).Select(e => e.Id).Count();

            /////Promotion
            //var ids = _lookup_promotionRepository.GetAll().Where(e => e.OrganizationID == organizationid).Select(e => e.Id).ToList();
            //var JobStatus = _jobRepository.GetAll().Where(e => e.JobStatusId == 6).Select(e => e.LeadId).ToList();
            //var Leads = _lookup_leadRepository.GetAll().Select(e => e.Id).ToList();
            //var promostionids = _PromotionUsersRepository.GetAll().Select(e => e.PromotionId).ToList();

            //output.TotalPromotion = _lookup_promotionRepository.GetAll().Where(e => e.OrganizationID == organizationid && promostionids.Contains(e.Id)).Select(e => e.LeadCount).Sum();
            //output.NoReplay = _PromotionUsersRepository.GetAll().Where(e => e.ResponseMessage == null && ids.Contains((int)e.PromotionId)).Select(e => (int)e.Id).Count();
            //output.InterestedCount = _PromotionUsersRepository.GetAll().Where(e => e.PromotionResponseStatusId == 1 && ids.Contains((int)e.PromotionId)).Select(e => (int)e.Id).Count();
            //output.NotInterestedCount = _PromotionUsersRepository.GetAll().Where(e => e.PromotionResponseStatusId == 2 && ids.Contains((int)e.PromotionId)).Select(e => (int)e.Id).Count();
            //output.OtherCount = _PromotionUsersRepository.GetAll().Where(e => e.PromotionResponseStatusId == 3 && ids.Contains((int)e.PromotionId)).Select(e => (int)e.Id).Count();
            //output.Unhandle = _PromotionUsersRepository.GetAll().Where(e => e.LeadFk.LeadStatusId == 2 && ids.Contains((int)e.PromotionId)).Select(e => (int)e.Id).Count();
            //output.PromotionSold = _PromotionUsersRepository.GetAll().Where(e => e.LeadFk.LeadStatusId == 9 && ids.Contains((int)e.PromotionId)).Select(e => (int)e.Id).Count();
            //output.PromotionProject = _PromotionUsersRepository.GetAll().Where(e => JobStatus.Contains((int)e.LeadId) && ids.Contains((int)e.PromotionId)).Select(e => (int)e.Id).Count();

            ///InstallerInvoice



            //grid connection tracker count
            output.PendingGrid = _jobRepository.GetAll()
              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
              .Where(e => e.LeadFk.OrganizationId == organizationid && e.MeterApplyRef == null && e.InspectionDate == null && e.InstalledcompleteDate != null).Select(e => e.Id).Count();

            output.AprrovedGrid = _jobRepository.GetAll()
             .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
             .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
             .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
             .Where(e => e.LeadFk.OrganizationId == organizationid && e.MeterApplyRef != null && e.InspectionDate != null && e.InstalledcompleteDate != null).Select(e => e.Id).Count();

            output.TotalGrid = _jobRepository.GetAll()
             .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
             .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
             .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
             .Where(e => e.LeadFk.OrganizationId == organizationid && (e.JobStatusId > 3)).Select(e => e.Id).Count();
            //.Where(e => e.LeadFk.OrganizationId == organizationid && ( e.JobStatusId == 8) || (e.JobStatusId == 6)).Select(e => e.Id).Count();

            output.AwaitingGrid = _jobRepository.GetAll()
             .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
             .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
             .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
             .Where(e => e.LeadFk.OrganizationId == organizationid && (e.JobStatusId == 4 || e.JobStatusId == 5 || e.JobStatusId == 6 || e.JobStatusId == 7)).Select(e => e.Id).Count();


            output.TotalReferral = _jobRepository.GetAll()
            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            .Where(e => e.LeadFk.OrganizationId == organizationid && e.LeadFk.LeadSource == "Referral" && e.LeadFk.LeadSourceId == 2 && e.JobStatusId >= 4).Select(e => e.Id).Distinct().Count();

            output.AwaitingReferral = _jobRepository.GetAll()
            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            .Where(e => e.LeadFk.OrganizationId == organizationid && e.LeadFk.LeadSource == "Referral" && e.LeadFk.LeadSourceId == 2 && e.ReferralPayment == false && e.BsbNo == null && e.AccountNo == null && ((_invoicePaymentRepository.GetAll().Where(e => e.JobId == e.Id).Select(e => e.InvoicePayTotal).Sum()) != e.TotalCost) && e.JobStatusId >= 4).Select(e => e.Id).Count();

            output.readytopayReferral = _jobRepository.GetAll()
            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            .Where(e => e.LeadFk.OrganizationId == organizationid && e.LeadFk.LeadSource == "Referral" && e.LeadFk.LeadSourceId == 2 && e.ReferralPayment == false && e.BsbNo != null && e.AccountNo != null && ((_invoicePaymentRepository.GetAll().Where(f => f.JobId == e.Id).Select(f => f.InvoicePayTotal).Sum()) == e.TotalCost)).Select(e => e.Id).Count();

            output.paidReferral = _jobRepository.GetAll()
            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            .Where(e => e.LeadFk.OrganizationId == organizationid && e.LeadFk.LeadSource == "Referral" && e.LeadFk.LeadSourceId == 2 && e.ReferralPayment == true).Select(e => e.Id).Count();



            output.WarrantyTotalData = _jobRepository.GetAll()
             .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
             .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
             .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
             .Where(e => e.LeadFk.OrganizationId == organizationid && e.InstalledcompleteDate != null && ((_invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.InvoicePayTotal).Sum()) == e.TotalCost)).Select(e => e.Id).Count();

            output.WarrantyTotalEmailSend = _jobRepository.GetAll()
            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            .Where(e => e.LeadFk.OrganizationId == organizationid && e.InstalledcompleteDate != null && e.WarrantyEmailSend == true && ((_invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.InvoicePayTotal).Sum()) == e.TotalCost)).Select(e => e.Id).Count();

            output.WarrantyTotalEmailPending = _jobRepository.GetAll()
          .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
          .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
          .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
          .Where(e => e.LeadFk.OrganizationId == organizationid && e.InstalledcompleteDate != null && e.WarrantyEmailSend != true && ((_invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.InvoicePayTotal).Sum()) == e.TotalCost)).Select(e => e.Id).Count();

            return output;
        }

        public async Task<GetDetailForJobEditOutPut> GetJobDetailEditByJobID(EntityDto input)
        {
            var job = await _jobRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetDetailForJobEditOutPut { Job = ObjectMapper.Map<CreateOrEditJobDto>(job) };
            output.DistApplied = job.DistApplied;
            output.ApplicationRefNo = job.ApplicationRefNo;
            output.DistApproveBy = job.DistApproveBy;
            output.ExpiryDate = job.ExpiryDate;
            output.AppliedBy = job.AppliedBy;
            output.ApprovalDate = job.ApprovalDate;
            output.EmpId = job.EmpId;
            output.PaidAmmount = job.PaidAmmount;
            output.Paidby = job.Paidby;
            output.PaidStatus = job.PaidStatus;
            output.InvRefNo = job.InvRefNo;
            output.InvPaidDate = job.InvPaidDate;
            output.FinanceApplicationDate = job.FinanceApplicationDate;
            output.FinancePurchaseNo = job.FinancePurchaseNo;
            output.DocumentsVeridiedDate = job.DocumentsVeridiedDate;
            output.CoolingoffPeriodEnd = job.CoolingoffPeriodEnd;
            output.FinanceDocumentVerified = job.FinanceDocumentVerified;
            output.DocumentsVeridiedBy = job.DocumentsVeridiedBy;
            output.FinanceNotes = job.FinanceNotes;
            output.StcUploaddate = job.STCUploaddate;
            output.StcUploadNumber = job.STCUploadNumber;
            output.StcAppliedDate = job.STCAppliedDate;
            output.PvdStatus = job.PVDStatus;
            output.PvdNumber = job.PVDNumber;
            output.StcNotes = job.STCNotes;
            output.MeterTime = job.MeterTime;
            output.MeterApplyRef = job.MeterApplyRef;
            output.InspectorName = job.InspectorName;
            output.InspectionDate = job.InspectionDate;
            output.ActiveDate = job.ActiveDate;
            if (output.PvdStatus != null)
            {
                var _lookupFinanceOption = await _pVDStatusRepository.FirstOrDefaultAsync((int)output.PvdStatus);
                output.PvdStatusName = _lookupFinanceOption?.Name?.ToString();
            }
            if (output.DocumentsVeridiedBy != null)
            {
                var _lookupdocverify = await _userRepository.FirstOrDefaultAsync((int)output.DocumentsVeridiedBy);
                output.DocumentsVeridiedByName = _lookupdocverify?.Name?.ToString();
            }

            if (output.DistApproveBy != null)
            {
                var _lookupdistaapprove = await _userRepository.FirstOrDefaultAsync((int)output.DistApproveBy);
                output.DistApproveByName = _lookupdistaapprove?.Name?.ToString();
            }
            if (output.EmpId != null)
            {
                var _lookupemployee = await _userRepository.FirstOrDefaultAsync((int)output.EmpId);
                output.EployeeByName = _lookupemployee?.Name?.ToString();
            }
            if (output.AppliedBy != null)
            {
                var _lookupapplyby = await _userRepository.FirstOrDefaultAsync((int)output.AppliedBy);
                output.ApplyByName = _lookupapplyby?.Name?.ToString();
            }
            output.Refund = await GetJobRefundByJobId(input.Id);
            output.InvoicePayment = await GetInvoicesByJobId(input.Id);
            output.JobPromotion = await GetjobpromotionByJobId(input.Id);

            return output;
        }

        public async Task<List<CreateOrEditJobRefundDto>> GetJobRefundByJobId(int jobid)
        {
            var filteredInvoicePayments = _jobRefundRepository.GetAll()
                        .Where(e => e.JobId == jobid);
            var refunds = from o in filteredInvoicePayments
                          select new CreateOrEditJobRefundDto()
                          {
                              Id = o.Id,
                              PaidDate = o.PaidDate,
                              PaymentOptionId = o.PaymentOptionId,
                              RefundReasonId = o.RefundReasonId,
                              Remarks = o.Remarks,
                              PaymentOptionName = _lookup_paymentOptionRepository.GetAll().Where(e => e.Id == o.PaymentOptionId).Select(e => e.Name).FirstOrDefault(),
                          };
            return refunds.ToList();
        }

        public async Task<List<DetailDto>> GetInvoicesByJobId(int jobid)
        {
            var filteredInvoicePayments = _invoicePaymentRepository.GetAll()
                        .Include(e => e.JobFk)
                        .Include(e => e.UserFk)
                        .Include(e => e.VerifiedByFk)
                        .Include(e => e.RefundByFk)
                        .Include(e => e.InvoicePaymentMethodFk)
                        .OrderByDescending(e => e.Id)
                        .Where(e => e.JobId == jobid);

            var invoicePayments = from o in filteredInvoicePayments
                                  select new DetailDto()
                                  {
                                      InvoicePayExGST = o.InvoicePayExGST,
                                      InvoicePayGST = o.InvoicePayGST,
                                      InvoicePayTotal = o.InvoicePayTotal,
                                      InvoicePayDate = o.InvoicePayDate,
                                      CCSurcharge = o.CCSurcharge,
                                      VerifiedOn = o.VerifiedOn,
                                      PaymentNumber = o.PaymentNumber,
                                      IsVerified = o.IsVerified,
                                      ReceiptNumber = o.ReceiptNumber,
                                      ActualPayDate = o.ActualPayDate,
                                      PaidComment = o.PaidComment,
                                      PaymentNote = o.PaymentNote,
                                      CreationTime = o.CreationTime,
                                      CreatedBy = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault()
                                  };
            return invoicePayments.ToList();
        }

        public async Task<List<DetailJobPromotionDto>> GetjobpromotionByJobId(int jobid)
        {
            var filteredInvoicePayments = _jobPromotionRepository.GetAll()
                        .Where(e => e.JobId == jobid);

            var invoicePayments = from o in filteredInvoicePayments
                                  select new DetailJobPromotionDto()
                                  {
                                      FreebieTransportId = o.FreebieTransportId,
                                      DispatchedDate = o.DispatchedDate,
                                      Description = o.Description,
                                      TrackingNumber = o.TrackingNumber,
                                      JobId = o.JobId,
                                      Name = _freebieTransportRepository.GetAll().Where(e => e.Id == o.FreebieTransportId).Select(e => e.Name).FirstOrDefault()
                                  };
            return invoicePayments.ToList();
        }

        public async Task<bool> CheckValidation(CreateOrEditJobDto input)
        {
            if (input.JobProductItems[0].Quantity != null && (input.BasicCost > 0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// Sms send From allTrackers
        public async Task SendSms(SmsEmailDto input)
        {
            if (input.TrackerId == 2)
            {
                var jobPromotion = await _jobPromotionRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditJobPromotionDto>(jobPromotion);
                var link = _freebieTransportRepository.GetAll().Where(e => e.Id == output.FreebieTransportId).Select(e => e.TransportLink).FirstOrDefault();
                var leadid = _jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                var mobilenumber = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Mobile).FirstOrDefault();
                var Body = input.Body;
                if (!string.IsNullOrEmpty(mobilenumber))
                {
                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 6;
                    leadactivity.ActionNote = "SMS Send From Freebies";
                    leadactivity.LeadId = (int)leadid;
                    leadactivity.Subject = Body;
                    if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                    {
                        leadactivity.TemplateId = input.SMSTemplateId;
                    }
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    else
                    {
                        leadactivity.TemplateId = 0;
                    }
                    leadactivity.Body = Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    leadactivity.SectionId = input.TrackerId;
                    await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                    SendSMSInput sendSMSInput = new SendSMSInput();
                    sendSMSInput.PhoneNumber = mobilenumber;
                    sendSMSInput.Text = Body;
                    sendSMSInput.ActivityId = leadactivity.Id;
                    await _applicationSettings.SendSMS(sendSMSInput);

                    output.SmsSend = true;
                    output.SmsSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobPromotion);
                }
            }
            else if (input.TrackerId == 4)
            {
                var jobRefund = await _jobRefundRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditJobRefundDto>(jobRefund);
                var leadid = _jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                var mobilenumber = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Mobile).FirstOrDefault();
                if (!string.IsNullOrEmpty(mobilenumber))
                {
                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 6;
                    leadactivity.ActionNote = "Sms Send From JobRefund";
                    leadactivity.LeadId = (int)leadid;
                    leadactivity.Subject = input.Body;
                    if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                    {
                        leadactivity.TemplateId = input.SMSTemplateId;
                    }
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    else
                    {
                        leadactivity.TemplateId = 0;
                    }
                    leadactivity.Body = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    leadactivity.SectionId = input.TrackerId;
                    await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                    SendSMSInput sendSMSInput = new SendSMSInput();
                    sendSMSInput.PhoneNumber = mobilenumber;
                    sendSMSInput.Text = input.Body;
                    sendSMSInput.ActivityId = leadactivity.Id;
                    await _applicationSettings.SendSMS(sendSMSInput);

                    output.JobRefundSmsSend = true;
                    output.JobRefundSmsSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobRefund);
                }
            }
            //else if (input.TrackerId == 6)
            //{
            //    var jobinvoice = await _invoicePaymentRepository.FirstOrDefaultAsync((int)input.ID);
            //    var output = ObjectMapper.Map<CreateOrEditInvoicePaymentDto>(jobinvoice);
            //    var leadid = _jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
            //    var mobilenumber = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Mobile).FirstOrDefault();
            //    if (!string.IsNullOrEmpty(mobilenumber))
            //    {
            //        var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            //        LeadActivityLog leadactivity = new LeadActivityLog();
            //        leadactivity.ActionId = 6;
            //        leadactivity.ActionNote = "Sms Send From Invoice";
            //        leadactivity.LeadId = (int)leadid;
            //        leadactivity.Subject = input.Body;
            //        leadactivity.ActivityDate = DateTime.UtcNow;
            //        if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
            //        {
            //            leadactivity.TemplateId = input.SMSTemplateId;
            //        }
            //        if (AbpSession.TenantId != null)
            //        {
            //            leadactivity.TenantId = (int)AbpSession.TenantId;
            //        }
            //        else
            //        {
            //            leadactivity.TemplateId = 0;
            //        }
            //        leadactivity.Body = input.Body;
            //        leadactivity.SectionId = input.TrackerId;
            //        await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

            //        SendSMSInput sendSMSInput = new SendSMSInput();
            //        sendSMSInput.PhoneNumber = mobilenumber;
            //        sendSMSInput.Text = input.Body;
            //        sendSMSInput.ActivityId = leadactivity.Id;
            //        await _applicationSettings.SendSMS(sendSMSInput);

            //        output.InvoiceSmsSend = true;
            //        output.InvoiceSmsSendDate = DateTime.UtcNow;
            //        ObjectMapper.Map(output, jobinvoice);
            //    }
            //}
            else if (input.TrackerId == 11)
            {
                var Jobid = _jobRepository.GetAll().Where(e => e.LeadId == input.LeadId).Select(e => e.Id).FirstOrDefault();
                var job = await _jobRepository.FirstOrDefaultAsync(Jobid);
                var output = ObjectMapper.Map<CreateOrEditJobDto>(job);
                var mobilenumber = _lookup_leadRepository.GetAll().Where(e => e.Id == job.LeadId).Select(e => e.Mobile).FirstOrDefault();
                if (!string.IsNullOrEmpty(mobilenumber))
                {
                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 6;
                    leadactivity.ActionNote = "Sms Send From PendingInstallation";
                    leadactivity.LeadId = (int)job.LeadId;
                    leadactivity.Subject = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    leadactivity.SectionId = input.TrackerId;
                    if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                    {
                        leadactivity.TemplateId = input.SMSTemplateId;
                    }
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    else
                    {
                        leadactivity.TemplateId = 0;
                    }
                    leadactivity.Body = input.Body;
                    await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                    SendSMSInput sendSMSInput = new SendSMSInput();
                    sendSMSInput.PhoneNumber = mobilenumber;
                    sendSMSInput.Text = input.Body;
                    sendSMSInput.ActivityId = leadactivity.Id;
                    await _applicationSettings.SendSMS(sendSMSInput);

                    output.PendingInstallerSmsSend = true;
                    output.PendingInstallerSmsSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, job);
                }
            }
            else if (input.TrackerId == 26)  // InstallerInvoice
            {
                var installerinvoice = await _jobInstallerInvoiceRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditJobInstallerInvoiceDto>(installerinvoice);
                var leadid = _jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                var mobilenumber = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Mobile).FirstOrDefault();
                if (!string.IsNullOrEmpty(mobilenumber))
                {
                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 26;
                    leadactivity.ActionNote = "Sms Send From InstallerInvoice";
                    leadactivity.LeadId = (int)leadid;
                    leadactivity.Subject = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    leadactivity.SectionId = input.TrackerId;
                    if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                    {
                        leadactivity.TemplateId = input.SMSTemplateId;
                    }
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    else
                    {
                        leadactivity.TemplateId = 0;
                    }
                    leadactivity.Body = input.Body;
                    await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                    SendSMSInput sendSMSInput = new SendSMSInput();
                    sendSMSInput.PhoneNumber = mobilenumber;
                    sendSMSInput.Text = input.Body;
                    sendSMSInput.ActivityId = leadactivity.Id;
                    await _applicationSettings.SendSMS(sendSMSInput);

                    output.SmsSend = true;
                    output.SmsSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, installerinvoice);
                }
            }
            else
            {
                var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)input.LeadId);
                if (!string.IsNullOrEmpty(lead.Mobile))
                {
                    var Jobid = _jobRepository.GetAll().Where(e => e.LeadId == input.LeadId).Select(e => e.Id).FirstOrDefault();
                    var job = await _jobRepository.FirstOrDefaultAsync(Jobid);
                    var output = ObjectMapper.Map<CreateOrEditJobDto>(job);
                    if (input.TrackerId == 1 && job.DistApproveDate != null) // ApplicarionTrackerid
                    {
                        output.SmsSend = true;
                        output.SmsSendDate = DateTime.UtcNow;
                    }

                    if (input.TrackerId == 3) // FinanceTrackerid
                    {
                        output.FinanceSmsSend = true;
                        output.FinanceSmsSendDate = DateTime.UtcNow;
                    }

                    if (input.TrackerId == 5)  // JobActiveTrackerid
                    {
                        output.JobActiveSmsSend = true;
                        output.JobActiveSmsSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 6)  // GridConnectionid
                    {
                        output.GridConnectionSmsSend = true;
                        output.GridConnectionSmsSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 7)  // StcTrackerid
                    {
                        output.StcSmsSend = true;
                        output.StcSmsSendDate = DateTime.UtcNow;
                    }
                    if (input.TrackerId == 8)  // JobGridId
                    {
                        output.JobGridSmsSend = true;
                        output.JobGridSmsSendDate = DateTime.UtcNow;
                    }
                    ObjectMapper.Map(output, job);

                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 6;
                    leadactivity.ActionNote = "Sms Send From Tacker";
                    leadactivity.LeadId = (int)input.LeadId;
                    leadactivity.Subject = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    leadactivity.SectionId = input.TrackerId;
                    if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
                    {
                        leadactivity.TemplateId = input.SMSTemplateId;
                    }
                    else
                    {
                        leadactivity.TemplateId = 0;
                    }
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    leadactivity.Body = input.Body;
                    await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                    SendSMSInput sendSMSInput = new SendSMSInput();
                    sendSMSInput.PhoneNumber = lead.Mobile;
                    sendSMSInput.Text = input.Body;
                    sendSMSInput.ActivityId = leadactivity.Id;
                    await _applicationSettings.SendSMS(sendSMSInput);
                }
            }
        }

        /// Email send From allTrackers
        public async Task SendEmail(SmsEmailDto input)
        {

            if (input.TrackerId == 2)
            {
                var jobPromotion = await _jobPromotionRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditJobPromotionDto>(jobPromotion);
                var link = _freebieTransportRepository.GetAll().Where(e => e.Id == output.FreebieTransportId).Select(e => e.TransportLink).FirstOrDefault();
                var leadid = _jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                var Email = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Email).FirstOrDefault();
                var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)leadid);
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();

                int? TemplateId;
                if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
                {
                    TemplateId = input.EmailTemplateId;
                }
                else
                {
                    TemplateId = 0;

                }

                if (!string.IsNullOrEmpty(Email))
                {
                    //MailMessage mail = new MailMessage
                    //{
                    //    From = new MailAddress(input.EmailFrom),////new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                    //    To = { Email },
                    //    Subject = input.Subject,
                    //    Body = input.Body,
                    //    IsBodyHtml = true
                    //};
                    //await this._emailSender.SendAsync(mail);

                    if (input.cc != null && input.Bcc != null)
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                            CC = { input.cc },
                            Bcc = { input.Bcc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else if (input.cc != null && input.cc != "" && input.Bcc == null)
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                            CC = { input.cc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else if (input.cc == null && input.Bcc != null && input.Bcc != "")
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                            Bcc = { input.Bcc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "hiral.prajapati@meghtechnologies.com" }, //

                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }
                }

                output.EmailSend = true;
                output.EmailSendDate = DateTime.UtcNow;
                ObjectMapper.Map(output, jobPromotion);


                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 7;
                leadactivity.ActionNote = "Email Send From Freebies";
                leadactivity.LeadId = (int)leadid;
                leadactivity.TemplateId = TemplateId;
                leadactivity.Subject = input.Subject;
                leadactivity.Body = input.Body;
                leadactivity.ActivityDate = DateTime.UtcNow;
                leadactivity.SectionId = input.TrackerId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
            else if (input.TrackerId == 4)
            {
                var jobRefund = await _jobRefundRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditJobRefundDto>(jobRefund);
                var leadid = _jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                var Email = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Email).FirstOrDefault();
                var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)leadid);
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();


                int? TemplateId;
                if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
                {
                    TemplateId = input.EmailTemplateId;
                }
                else
                {
                    TemplateId = 0;
                }

                if (!string.IsNullOrEmpty(Email))
                {
                    //MailMessage mail = new MailMessage
                    //{
                    //    From = new MailAddress(input.EmailFrom),///new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                    //    To = { Email },
                    //    Subject = input.Subject,
                    //    Body = input.Body,
                    //    IsBodyHtml = true
                    //};
                    //await this._emailSender.SendAsync(mail);

                    if (input.cc != null && input.Bcc != null)
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                            CC = { input.cc },
                            Bcc = { input.Bcc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else if (input.cc != null && input.cc != "" && input.Bcc == null)
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                            CC = { input.cc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else if (input.cc == null && input.Bcc != null && input.Bcc != "")
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                            Bcc = { input.Bcc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "hiral.prajapati@meghtechnologies.com" }, //

                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }
                }

                output.JobRefundEmailSend = true;
                output.JobRefundSmsSendDate = DateTime.UtcNow;
                ObjectMapper.Map(output, jobRefund);

                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 7;
                leadactivity.ActionNote = "Email Send From Refund";
                leadactivity.LeadId = (int)leadid;
                leadactivity.TemplateId = TemplateId;
                leadactivity.Subject = input.Subject;
                leadactivity.Body = input.Body;
                leadactivity.ActivityDate = DateTime.UtcNow;
                leadactivity.SectionId = input.TrackerId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
            //else if (input.TrackerId == 6)
            //{
            //    var jobsss = _invoicePaymentRepository.GetAll().Where(e => e.JobId == input.ID).OrderByDescending(e => e.Id).Select(e => e.Id).FirstOrDefault();
            //    var jobinvoice = await _invoicePaymentRepository.FirstOrDefaultAsync((int)jobsss);
            //    var output = ObjectMapper.Map<CreateOrEditInvoicePaymentDto>(jobinvoice);
            //    var leadid = _jobRepository.GetAll().Where(e => e.Id == input.ID).Select(e => e.LeadId).FirstOrDefault();
            //    var Email = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Email).FirstOrDefault();
            //    var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)leadid);
            //    var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();

            //    int? TemplateId;
            //    if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
            //    {
            //        TemplateId = input.EmailTemplateId;
            //        input.Subject = _emailTemplateRepository.GetAll().Where(e => e.Id == input.EmailTemplateId).Select(e => e.Subject).FirstOrDefault();
            //    }
            //    else
            //    {
            //        TemplateId = 0;
            //    }

            //    //List<SendEmailAttachmentDto> emailattch = new List<SendEmailAttachmentDto>();
            //    //SendEmailAttachmentDto doc = new SendEmailAttachmentDto();
            //    //if (input.TaxInvoiceDocPath != null)
            //    //{
            //    //    doc.FileName = input.TaxInvoiceFileName;
            //    //    doc.FilePath = input.TaxInvoiceDocPath;
            //    //    emailattch.Add(doc);
            //    //}
            //    //var EmailTempData = _EmailTempDataRepository.GetAll().Where(e => e.JobId == output.JobId).ToList();
            //    //if (EmailTempData != null)
            //    //{
            //    //    foreach (var item in EmailTempData)
            //    //    {
            //    //        SendEmailAttachmentDto doc5 = new SendEmailAttachmentDto();
            //    //        doc5.FileName = item.FileName;
            //    //        doc5.FilePath = item.FilePath;
            //    //        emailattch.Add(doc5);
            //    //    }

            //    //}
            //    //var Path = _env.WebRootPath;
            //    //var MainFolder = Path;

            //    //var Path = pathfordoc;
            //    //var MainFolder = Path;

            //    if (input.cc != null && input.Bcc != null)
            //    {
            //        MailMessage mail = new MailMessage
            //        {
            //            From = new MailAddress(input.EmailFrom),
            //            To = { input.EmailTo }, //{ "viral.jain@meghtechnologies.com" }, //
            //            CC = { input.cc },
            //            Bcc = { input.Bcc },
            //            Subject = input.Subject,
            //            Body = input.Body,
            //            IsBodyHtml = true
            //        };

            //        //foreach (var item in emailattch)
            //        //{
            //        //    mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
            //        //}
            //        await this._emailSender.SendAsync(mail);
            //        mail.Dispose();

            //        output.InvoiceEmailSend = true;
            //        output.InvoiceEmailSendDate = DateTime.UtcNow;
            //        ObjectMapper.Map(output, jobinvoice);

            //        var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            //        LeadActivityLog leadactivity = new LeadActivityLog();
            //        leadactivity.ActionId = 7;
            //        leadactivity.ActionNote = "Email Send From Invoice";
            //        leadactivity.LeadId = (int)leadid;
            //        leadactivity.TemplateId = TemplateId;
            //        leadactivity.Subject = input.Subject;
            //        leadactivity.Body = input.Body;
            //        leadactivity.ActivityDate = DateTime.UtcNow;
            //        leadactivity.SectionId = input.TrackerId;
            //        if (AbpSession.TenantId != null)
            //        {
            //            leadactivity.TenantId = (int)AbpSession.TenantId;
            //        }
            //        await _leadactivityRepository.InsertAsync(leadactivity);
            //    }

            //    else if (input.cc != null && input.cc != "" && input.Bcc == null)
            //    {
            //        MailMessage mail = new MailMessage
            //        {
            //            From = new MailAddress(input.EmailFrom),
            //            To = { input.EmailTo }, //{ "viral.jain@meghtechnologies.com" }, //
            //            CC = { input.cc },
            //            Subject = input.Subject,
            //            Body = input.Body,
            //            IsBodyHtml = true
            //        };


            //        //foreach (var item in emailattch)
            //        //{
            //        //    mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
            //        //}
            //        await this._emailSender.SendAsync(mail);
            //        mail.Dispose();

            //        output.InvoiceEmailSend = true;
            //        output.InvoiceEmailSendDate = DateTime.UtcNow;
            //        ObjectMapper.Map(output, jobinvoice);

            //        var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            //        LeadActivityLog leadactivity = new LeadActivityLog();
            //        leadactivity.ActionId = 7;
            //        leadactivity.ActionNote = "Email Send From Invoice";
            //        leadactivity.LeadId = (int)leadid;
            //        leadactivity.TemplateId = TemplateId;
            //        leadactivity.Subject = input.Subject;
            //        leadactivity.Body = input.Body;
            //        leadactivity.ActivityDate = DateTime.UtcNow;
            //        leadactivity.SectionId = input.TrackerId;
            //        if (AbpSession.TenantId != null)
            //        {
            //            leadactivity.TenantId = (int)AbpSession.TenantId;
            //        }
            //        await _leadactivityRepository.InsertAsync(leadactivity);
            //    }

            //    else if (input.cc == null && input.Bcc != null && input.Bcc != "")
            //    {
            //        MailMessage mail = new MailMessage
            //        {
            //            From = new MailAddress(input.EmailFrom),
            //            To = { input.EmailTo }, //{ "viral.jain@meghtechnologies.com" }, //
            //            Bcc = { input.Bcc },
            //            Subject = input.Subject,
            //            Body = input.Body,
            //            IsBodyHtml = true
            //        };


            //        //foreach (var item in emailattch)
            //        //{
            //        //    mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
            //        //}

            //        await this._emailSender.SendAsync(mail);
            //        mail.Dispose();

            //        output.InvoiceEmailSend = true;
            //        output.InvoiceEmailSendDate = DateTime.UtcNow;
            //        ObjectMapper.Map(output, jobinvoice);

            //        var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            //        LeadActivityLog leadactivity = new LeadActivityLog();
            //        leadactivity.ActionId = 7;
            //        leadactivity.ActionNote = "Email Send From Invoice";
            //        leadactivity.LeadId = (int)leadid;
            //        leadactivity.TemplateId = TemplateId;
            //        leadactivity.Subject = input.Subject;
            //        leadactivity.Body = input.Body;
            //        leadactivity.ActivityDate = DateTime.UtcNow;
            //        leadactivity.SectionId = input.TrackerId;
            //        if (AbpSession.TenantId != null)
            //        {
            //            leadactivity.TenantId = (int)AbpSession.TenantId;
            //        }
            //        await _leadactivityRepository.InsertAsync(leadactivity);
            //    }
            //    else
            //    {
            //        MailMessage mail = new MailMessage
            //        {
            //            From = new MailAddress(input.EmailFrom),
            //            To = { input.EmailTo }, //{ "hiral.prajapati@meghtechnologies.com" }, //

            //            Subject = input.Subject,
            //            Body = input.Body,
            //            IsBodyHtml = true
            //        };

            //        //foreach (var item in emailattch)
            //        //{
            //        //    mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
            //        //}

            //        await this._emailSender.SendAsync(mail);
            //        mail.Dispose();

            //        output.InvoiceEmailSend = true;
            //        output.InvoiceEmailSendDate = DateTime.UtcNow;
            //        ObjectMapper.Map(output, jobinvoice);

            //        var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            //        LeadActivityLog leadactivity = new LeadActivityLog();
            //        leadactivity.ActionId = 7;
            //        leadactivity.ActionNote = "Email Send From Invoice";
            //        leadactivity.LeadId = (int)leadid;
            //        leadactivity.TemplateId = TemplateId;
            //        leadactivity.Subject = input.Subject;
            //        leadactivity.Body = input.Body;
            //        leadactivity.ActivityDate = DateTime.UtcNow;
            //        leadactivity.SectionId = input.TrackerId;
            //        if (AbpSession.TenantId != null)
            //        {
            //            leadactivity.TenantId = (int)AbpSession.TenantId;
            //        }
            //        await _leadactivityRepository.InsertAsync(leadactivity);

            //    }

            //    //if (EmailTempData != null)
            //    //{
            //    //    foreach (var item in EmailTempData)
            //    //    {
            //    //        _EmailTempDataRepository.HardDelete(item);
            //    //    }
            //    //}

            //    //var fileSavePath = Path + input.TaxInvoiceDocPath + input.TaxInvoiceFileName;
            //    //if (System.IO.File.Exists(fileSavePath))
            //    //{
            //    //    try
            //    //    {
            //    //        System.IO.File.Delete(fileSavePath);
            //    //    }
            //    //    catch (Exception ex)
            //    //    {
            //    //        Exception e;
            //    //    }
            //    //}
            //}

            else if (input.TrackerId == 10 || input.TrackerId == 9)
            {
                //var inmportdatas = await _InvoiceImportDataRepository.FirstOrDefaultAsync((int)input.ID);
                /// var output = ObjectMapper.Map<CreateOrEditImportDto>(inmportdatas);
                var leadid = _jobRepository.GetAll().Where(e => e.Id == input.LeadId).Select(e => e.LeadId).FirstOrDefault();
                var Email = _lookup_leadRepository.GetAll().Where(e => e.Id == input.LeadId).Select(e => e.Email).FirstOrDefault();
                var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)input.LeadId);
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();

                int? TemplateId;
                if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
                {
                    TemplateId = input.EmailTemplateId;
                    input.Subject = _emailTemplateRepository.GetAll().Where(e => e.Id == input.EmailTemplateId).Select(e => e.Subject).FirstOrDefault();
                }
                else
                {
                    TemplateId = 0;
                }

                List<SendEmailAttachmentDto> emailattch = new List<SendEmailAttachmentDto>();
                SendEmailAttachmentDto doc = new SendEmailAttachmentDto();
                if (input.TaxInvoiceDocPath != null)
                {
                    doc.FileName = input.TaxInvoiceFileName;
                    doc.FilePath = input.TaxInvoiceDocPath;
                    emailattch.Add(doc);
                }
                //var EmailTempData = _EmailTempDataRepository.GetAll().Where(e => e.JobId == inmportdatas.JobId).ToList();
                //if (EmailTempData != null)
                //{
                //    foreach (var item in EmailTempData)
                //    {
                //        SendEmailAttachmentDto doc5 = new SendEmailAttachmentDto();
                //        doc5.FileName = item.FileName;
                //        doc5.FilePath = item.FilePath;
                //        emailattch.Add(doc5);
                //    }

                //}
                var Path = _env.WebRootPath;
                var MainFolder = Path;

                if (input.cc != null && input.Bcc != null)
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { input.EmailTo }, //{ "viral.jain@meghtechnologies.com" }, //
                        CC = { input.cc },
                        Bcc = { input.Bcc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };


                    foreach (var item in emailattch)
                    {
                        mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
                    }
                    await this._emailSender.SendAsync(mail);
                    mail.Dispose();
                    //output.InvoiceEmailSend = true;
                    //output.InvoiceEmailSendDate = DateTime.UtcNow;
                    //ObjectMapper.Map(output, jobinvoice);

                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 7;
                    leadactivity.ActionNote = "Email Send From PaidInvoice";
                    leadactivity.LeadId = (int)leadid;
                    leadactivity.TemplateId = TemplateId;
                    leadactivity.Subject = input.Subject;
                    leadactivity.Body = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    leadactivity.SectionId = input.TrackerId;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);

                }

                else if (input.cc != null && input.cc != "" && input.Bcc == null)
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { input.EmailTo }, //{ "viral.jain@meghtechnologies.com" }, //
                        CC = { input.cc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };


                    foreach (var item in emailattch)
                    {
                        mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
                    }
                    await this._emailSender.SendAsync(mail);
                    mail.Dispose();
                    //output.InvoiceEmailSend = true;
                    //output.InvoiceEmailSendDate = DateTime.UtcNow;
                    //ObjectMapper.Map(output, jobinvoice);

                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 7;
                    leadactivity.ActionNote = "Email Send From Invoice";
                    leadactivity.LeadId = (int)input.LeadId;
                    leadactivity.TemplateId = TemplateId;
                    leadactivity.Subject = input.Subject;
                    leadactivity.Body = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    leadactivity.SectionId = input.TrackerId;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }

                else if (input.cc == null && input.Bcc != null && input.Bcc != "")
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { input.EmailTo }, //{ "viral.jain@meghtechnologies.com" }, //
                        Bcc = { input.Bcc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };


                    foreach (var item in emailattch)
                    {
                        mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
                    }

                    await this._emailSender.SendAsync(mail);
                    mail.Dispose();
                    //output.InvoiceEmailSend = true;
                    //output.InvoiceEmailSendDate = DateTime.UtcNow;
                    //ObjectMapper.Map(output, jobinvoice);
                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 7;
                    leadactivity.ActionNote = "Email Send From Invoice";
                    leadactivity.LeadId = (int)leadid;
                    leadactivity.TemplateId = TemplateId;
                    leadactivity.Subject = input.Subject;
                    leadactivity.Body = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    leadactivity.SectionId = input.TrackerId;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }

                else
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { input.EmailTo }, //{ "hiral.prajapati@meghtechnologies.com" }, //
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };



                    foreach (var item in emailattch)
                    {
                        mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
                    }

                    await this._emailSender.SendAsync(mail);
                    mail.Dispose();
                    //output.InvoiceEmailSend = true;
                    //output.InvoiceEmailSendDate = DateTime.UtcNow;
                    //ObjectMapper.Map(output, jobinvoice);

                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.ActionId = 7;
                    leadactivity.ActionNote = "Email Send From Invoice";
                    leadactivity.LeadId = (int)input.LeadId;
                    leadactivity.TemplateId = TemplateId;
                    leadactivity.Subject = input.Subject;
                    leadactivity.Body = input.Body;
                    leadactivity.ActivityDate = DateTime.UtcNow;
                    leadactivity.SectionId = input.TrackerId;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);

                }


                //if (EmailTempData != null)
                //{
                //    foreach (var item in EmailTempData)
                //    {
                //        _EmailTempDataRepository.HardDelete(item);
                //    }
                //}
                //var fileSavePaths = Path + input.TaxInvoiceDocPath + input.TaxInvoiceFileName;
                //if (System.IO.File.Exists(fileSavePaths))
                //{
                //    try
                //    {
                //        System.IO.File.Delete(fileSavePaths);
                //    }
                //    catch (Exception ex)
                //    {
                //        Exception e;
                //    }
                //}
            }
            else if (input.TrackerId == 11)
            {
                var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)input.LeadId);
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();

                var Jobid = _jobRepository.GetAll().Where(e => e.LeadId == input.LeadId).Select(e => e.Id).FirstOrDefault();
                var job = await _jobRepository.FirstOrDefaultAsync(Jobid);
                var output = ObjectMapper.Map<CreateOrEditJobDto>(job);

                int? TemplateId;
                if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
                {
                    TemplateId = input.EmailTemplateId;
                    input.Subject = _emailTemplateRepository.GetAll().Where(e => e.Id == input.EmailTemplateId).Select(e => e.Subject).FirstOrDefault();
                }
                else
                {
                    TemplateId = 0;
                }

                if (!string.IsNullOrEmpty(lead.Email))
                {
                    //MailMessage mail = new MailMessage
                    //{
                    //    From = new MailAddress(input.EmailFrom),//new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                    //    To = { lead.Email },
                    //    Subject = input.Subject,
                    //    Body = input.Body,
                    //    IsBodyHtml = true
                    //};
                    //await this._emailSender.SendAsync(mail);

                    if (input.cc != null && input.Bcc != null)
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                            CC = { input.cc },
                            Bcc = { input.Bcc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else if (input.cc != null && input.cc != "" && input.Bcc == null)
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                            CC = { input.cc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else if (input.cc == null && input.Bcc != null && input.Bcc != "")
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                            Bcc = { input.Bcc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "hiral.prajapati@meghtechnologies.com" }, //

                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }
                }

                output.PendingInstallerEmailSend = true;
                output.PendingInstallerEmailSendDate = DateTime.UtcNow;
                ObjectMapper.Map(output, job);

                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 7;
                leadactivity.ActionNote = "Email Send From PendingInstalltion";
                leadactivity.LeadId = (int)job.LeadId;
                leadactivity.TemplateId = TemplateId;
                leadactivity.Subject = input.Subject;
                leadactivity.Body = input.Body;
                leadactivity.ActivityDate = DateTime.UtcNow;
                leadactivity.SectionId = input.TrackerId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
            else if (input.TrackerId == 26)
            {
                var installerinvoice = await _jobInstallerInvoiceRepository.FirstOrDefaultAsync((int)input.ID);
                var output = ObjectMapper.Map<CreateOrEditJobInstallerInvoiceDto>(installerinvoice);
                var leadid = _jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
                var Email = _lookup_leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Email).FirstOrDefault();
                var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)leadid);
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();


                int? TemplateId;
                if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
                {
                    TemplateId = input.EmailTemplateId;
                }
                else
                {
                    TemplateId = 0;
                }

                if (!string.IsNullOrEmpty(Email))
                {
                    //MailMessage mail = new MailMessage
                    //{
                    //    From = new MailAddress(input.EmailFrom),//new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                    //    To = { Email },
                    //    Subject = input.Subject,
                    //    Body = input.Body,
                    //    IsBodyHtml = true
                    //};
                    //await this._emailSender.SendAsync(mail);

                    if (input.cc != null && input.Bcc != null)
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                            CC = { input.cc },
                            Bcc = { input.Bcc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else if (input.cc != null && input.cc != "" && input.Bcc == null)
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                            CC = { input.cc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else if (input.cc == null && input.Bcc != null && input.Bcc != "")
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                            Bcc = { input.Bcc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "hiral.prajapati@meghtechnologies.com" }, //

                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }
                }

                output.installerEmailSend = true;
                output.EmaiSendDate = DateTime.UtcNow;
                ObjectMapper.Map(output, installerinvoice);

                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 26;
                leadactivity.ActionNote = "Email Send From InstallerInvoice";
                leadactivity.LeadId = (int)leadid;
                leadactivity.TemplateId = TemplateId;
                leadactivity.Subject = input.Subject;
                leadactivity.Body = input.Body;
                leadactivity.ActivityDate = DateTime.UtcNow;
                leadactivity.SectionId = input.TrackerId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
            else
            {
                var lead = await _lookup_leadRepository.FirstOrDefaultAsync((int)input.LeadId);
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
                int? TemplateId;
                if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
                {
                    TemplateId = input.EmailTemplateId;
                }
                else
                {
                    TemplateId = 0;
                }

                if (!string.IsNullOrEmpty(lead.Email))
                {
                    ////MailMessage mail = new MailMessage
                    ////{
                    ////    From = new MailAddress(input.EmailFrom),//new MailAddress(GetCurrentUser().EmailAddress), //new MailAddress(assignedToUser.EmailAddress),
                    ////    To = { lead.Email },
                    ////    Subject = input.Subject,//EmailBody.TemplateName + "The Solar Product Lead Notification",
                    ////    Body = input.Body,
                    ////    IsBodyHtml = true
                    ////};
                    //await this._emailSender.SendAsync(mail);

                    if (input.cc != null && input.Bcc != null)
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                            CC = { input.cc },
                            Bcc = { input.Bcc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else if (input.cc != null && input.cc != "" && input.Bcc == null)
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                            CC = { input.cc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else if (input.cc == null && input.Bcc != null && input.Bcc != "")
                    {
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { lead.Email }, //{ "viral.jain@meghtechnologies.com" }, //
                            Bcc = { input.Bcc },
                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }

                    else
                    {
                        var tomail = lead.Email;
                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress(input.EmailFrom),
                            To = { tomail }, //{ "hiral.prajapati@meghtechnologies.com" }, //

                            Subject = input.Subject,
                            Body = input.Body,
                            IsBodyHtml = true
                        };
                        await this._emailSender.SendAsync(mail);
                    }
                }

                var Jobid = _jobRepository.GetAll().Where(e => e.LeadId == input.LeadId).Select(e => e.Id).FirstOrDefault();
                var job = await _jobRepository.FirstOrDefaultAsync(Jobid);
                var output = ObjectMapper.Map<CreateOrEditJobDto>(job);
                if (input.TrackerId == 1 && job.DistApproveDate != null)
                {
                    output.EmailSend = true;
                    output.EmailSendDate = DateTime.UtcNow;
                }
                if (input.TrackerId == 3)
                {
                    output.FinanceEmailSend = true;
                    output.FinanceEmailSendDate = DateTime.UtcNow;
                }
                if (input.TrackerId == 5)
                {
                    output.JobActiveEmailSend = true;
                    output.JobActiveEmailSendDate = DateTime.UtcNow;
                }
                if (input.TrackerId == 6)
                {
                    output.GridConnectionEmailSend = true;
                    output.GridConnectionEmailSendDate = DateTime.UtcNow;
                }
                if (input.TrackerId == 7)
                {
                    output.StcEmailSend = true;
                    output.StcEmailSendDate = DateTime.UtcNow;
                }
                if (input.TrackerId == 8)
                {
                    output.JobGridEmailSend = true;
                    output.JobGridEmailSendDate = DateTime.UtcNow;
                }
                ObjectMapper.Map(output, job);

                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 7;
                leadactivity.ActionNote = "Email Send From Invoice";
                leadactivity.LeadId = (int)input.LeadId;
                leadactivity.TemplateId = TemplateId;
                leadactivity.Subject = input.Subject;
                leadactivity.Body = input.Body;
                leadactivity.SectionId = input.TrackerId;
                leadactivity.ActivityDate = DateTime.UtcNow;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
        }

        /// get smsTemplateData By ID for send sms Functionality
        public async Task<GetSmsTemplateForEditOutput> GetSmsTemplateForEditForSms(EntityDto input)
        {
            var smsTemplate = await _smsTemplateRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetSmsTemplateForEditOutput { SmsTemplate = ObjectMapper.Map<CreateOrEditSmsTemplateDto>(smsTemplate) };

            return output;
        }

        /// get EmailTemplateData By ID for send email Functionality
        public async Task<GetEmailTemplateForEdit> GetEmailTemplateForEditForEmail(EntityDto input)
        {
            var Email = await _emailTemplateRepository.FirstOrDefaultAsync(input.Id);
            var output = new GetEmailTemplateForEdit { EmailTemplate = ObjectMapper.Map<CreateOrEditEmailTemplateDto>(Email) };
            //GetEmailTemplateForEdit output = new GetEmailTemplateForEdit();
            //output.EmailTemplate.Body == Email.Body;
            //output.EmailTemplate.Subject == Email.Subject;
            //output.EmailTemplate.Id == Email.Id;
            //output.EmailTemplate.Body == Email.Body;
            return output;
        }

        /// <summary>
        /// AutoComplete For Product Names
        /// </summary>
        /// <param name="productTypeId">Product Type Master Id</param>
        /// <param name="productItem">Product Name</param>
        /// <returns></returns>
        public async Task<List<GetProductItemSearchResult>> GetProductItemList(int productTypeId, string productItem, string state, bool salesTag)
        {
            var warehouseid = _warehouselocationRepository.GetAll().Where(e => e.state == state).Select(e => e.Id).FirstOrDefault();
            var productiteamids = _productiteamlocationRepository.GetAll().Where(e => e.WarehouselocationId == warehouseid && e.SalesTag == true).Select(e => e.ProductItemId).Distinct();
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();

            IList<string> role = await _userManager.GetRolesAsync(User);

            if (!role.Contains("Admin"))
            {
                return await _productItemRepository.GetAll()
                    .Where(e => e.ProductTypeId == productTypeId && e.Active == true)
                    .WhereIf(salesTag == true, e => productiteamids.Any(o => o == e.Id))
                    .WhereIf(!string.IsNullOrWhiteSpace(productItem), c => c.Name.ToLower().Contains(productItem.ToLower()))
                    .Select(p => new GetProductItemSearchResult() { 
                        Id = p.Id, 
                        ProductItem = p.Name, 
                        ProductModel = p.Model, 
                        Size = p.Size, 
                        DatasheetUrl = (p.FilePath + p.FileName),
                        PerformanceWarranty = p.PerformanceWarranty,
                        ProductWarranty = p.ProductWarranty,
                        ExtendedWarranty = p.ExtendedWarranty
                    }).ToListAsync();
            }
            else
            {
                return await _productItemRepository.GetAll()
                    .Where(e => e.ProductTypeId == productTypeId && e.Active == true)
                    .WhereIf(!string.IsNullOrWhiteSpace(productItem), c => c.Name.ToLower().Contains(productItem.ToLower()))
                    .Select(p => new GetProductItemSearchResult() { 
                        Id = p.Id, 
                        ProductItem = p.Name, 
                        ProductModel = p.Model, 
                        Size = p.Size, 
                        DatasheetUrl = (p.FilePath + p.FileName),
                        PerformanceWarranty = p.PerformanceWarranty,
                        ProductWarranty = p.ProductWarranty,
                        ExtendedWarranty = p.ExtendedWarranty
                    }).ToListAsync();
            }
        }

        public async Task<List<GetProductItemSearchResult>> GetPicklistProductItemList(int productTypeId, string productItem)
        {
            var TodayDate = (_timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId));

            return await _productItemRepository.GetAll()
                    .Where(e => e.ProductTypeId == productTypeId && e.Active == true)
                    .WhereIf(!string.IsNullOrWhiteSpace(productItem), c => c.Name.ToLower().Contains(productItem.ToLower()))
                    .Select(p => new GetProductItemSearchResult() { Id = p.Id, ProductItem = p.Name, ProductModel = p.Model, 
                        Size = p.Size, ExpiryDate = p.ExpiryDate.Value.Date,
                        IsExpired = (p.ExpiryDate.Value.Date < TodayDate.Value.Date) }).ToListAsync();
            //var productiteamids = _productiteamlocationRepository.GetAll().Where(e => e.WarehouselocationId == warehouseid && e.SalesTag == true).Select(e => e.ProductItemId).Distinct();
            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();

            //IList<string> role = await _userManager.GetRolesAsync(User);

            //if (!role.Contains("Admin"))
            //{
            //    return await _productItemRepository.GetAll()
            //        .Where(e => e.ProductTypeId == productTypeId && e.Active == true)
            //        .WhereIf(salesTag == true, e => productiteamids.Any(o => o == e.Id))
            //        .WhereIf(!string.IsNullOrWhiteSpace(productItem), c => c.Name.ToLower().Contains(productItem.ToLower()))
            //        .Select(p => new GetProductItemSearchResult() { Id = p.Id, ProductItem = p.Name, ProductModel = p.Model, Size = p.Size }).ToListAsync();
            //}
            //else
            //{

            //}


        }

        public async Task<List<string>> getinvertmodel(string invertmodel)
        {
            var result = _productItemRepository.GetAll()
            //.WhereIf(!string.IsNullOrWhiteSpace(invertmodel), c => c.Model.ToLower().Contains(invertmodel.ToLower()) && (c.ProductTypeId == 2 || c.ProductTypeId == 5));
            .WhereIf(!string.IsNullOrWhiteSpace(invertmodel), c => c.Model.ToLower().Contains(invertmodel.ToLower()) && (c.ProductTypeId == 2));

            var invertmodels = from o in result
                               select new LeadStatusLookupTableDto()
                               {
                                   Value = o.Id,
                                   Name = o.Model.ToString()
                               };

            return await invertmodels
                .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
        }

        public async Task<List<string>> GetBatteryModel(string betterymodel)
        {
            var result = _productItemRepository.GetAll()
            .WhereIf(!string.IsNullOrWhiteSpace(betterymodel), c => c.Model.ToLower().Contains(betterymodel.ToLower()) && ( c.ProductTypeId == 5));

            var batteryModel = from o in result
                               select new LeadStatusLookupTableDto()
                               {
                                   Value = o.Id,
                                   Name = o.Model.ToString()
                               };

            return await batteryModel
                .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
        }

        public async Task<List<string>> getpanelmodel(string panelmodel)
        {
            var result = _productItemRepository.GetAll()
            .WhereIf(!string.IsNullOrWhiteSpace(panelmodel), c => c.Model.ToLower().Contains(panelmodel.ToLower()) && c.ProductTypeId == 1);

            var invertmodels = from o in result
                               select new LeadStatusLookupTableDto()
                               {
                                   Value = o.Id,
                                   Name = o.Model.ToString()
                               };

            return await invertmodels
                .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();

        }

        /// <summary>
        /// Send Document Request for Document Upload Using Camera/Gallary 
        /// </summary>
        /// <param name="JobId">Job Id</param>
        /// <param name="DocumentTypeId">Requested Document Type Id</param>
        /// <returns></returns>
        //public async Task SendDocumentRequestLinkSMS(int JobId, int DocumentTypeId)
        //{
        //    var lead = _lookup_leadRepository.GetAll().Where(e => e.Id == _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.LeadId).FirstOrDefault()).FirstOrDefault();

        //    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
        //    LeadActivityLog leadactivity = new LeadActivityLog();
        //    leadactivity.ActionId = 6;
        //    leadactivity.ActionNote = "Document Request Created";
        //    leadactivity.LeadId = lead.Id;
        //    if (AbpSession.TenantId != null)
        //    {
        //        leadactivity.TenantId = (int)AbpSession.TenantId;
        //    }
        //    await _leadactivityRepository.InsertAsync(leadactivity);

        //    if (!string.IsNullOrEmpty(lead.Mobile))
        //    {
        //        string STR = Convert.ToString(DocumentTypeId);
        //        var token = SimpleStringCipher.Instance.Encrypt(STR, AppConsts.DefaultPassPhrase);

        //        SendSMSInput sendSMSInput = new SendSMSInput();
        //        sendSMSInput.PhoneNumber = "+919974242686"; //lead.Mobile;
        //        sendSMSInput.Text = "http://thesolarproduct.com/account/document-request?STR=" + token; //input.Body;
        //        await _applicationSettings.SendSMS(sendSMSInput);
        //    }

        //}

        /// <summary>
        /// Get Attached Documents With Product Items Used in Job
        /// </summary>
        /// <param name="JobId">Job Id</param>
        /// <returns></returns>
        public async Task<List<ProductItemAttachmentListDto>> ProductItemAttachmentList(int JobId)
        {
            var List = _jobProductItemRepository.GetAll().Where(e => e.JobId == JobId).Select(e => e.ProductItemId).ToList();
            var NearMap = _documentRepository.GetAll().Where(e => e.JobId == JobId && (e.DocumentTypeId == 1 || e.DocumentTypeId == 16 || e.DocumentTypeId == 15)).OrderByDescending(e => e.Id).Select(e => new { e.FileName, e.FilePath }).FirstOrDefault();

            var output = await _productItemRepository.GetAll()
                .Where(e => List.Contains(e.Id) && !string.IsNullOrEmpty(e.FileName))
                .Select(item => new ProductItemAttachmentListDto
                {
                    FileName = item.FileName,
                    FilePath = item.FilePath
                }).OrderBy(x => x.FileName).ToListAsync();

            var NearMapObj = new ProductItemAttachmentListDto
            {
                FileName = NearMap.FileName,
                FilePath = NearMap.FilePath
            };
            output.Add(NearMapObj);

            return output;
        }

        public async Task<List<PvdStatusDto>> GetAllPVDStatusDropdown()
        {
            return await _pVDStatusRepository.GetAll()
                        .Select(pVDStatus => new PvdStatusDto
                        {
                            Id = pVDStatus.Id,
                            DisplayName = pVDStatus == null || pVDStatus.Name == null ? "" : pVDStatus.Name.ToString()
                        }).ToListAsync();
        }

        public async Task UpdateBookingManagerID(List<int> Jobids, int? userId)
        {
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == userId).FirstOrDefault();
            var assignedFromUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).Select(e => e.FullName).FirstOrDefault();

            foreach (var id in Jobids)
            {
                var job = await _jobRepository.FirstOrDefaultAsync((int)id);
                job.BookingManagerId = userId;
                job.JobAssignDate = DateTime.UtcNow;
                await _jobRepository.UpdateAsync(job);

                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionNote = "JobAssign To" + assignedToUser.FullName;
                leadactivity.ActionId = 27;
                leadactivity.SectionId = 0;
                leadactivity.LeadId = (int)job.LeadId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                leadactivity.CreatorUserId = AbpSession.UserId;
                await _leadactivityRepository.InsertAsync(leadactivity);
            }



            string msg = string.Format(Jobids.Count + "Job Assign" + " BY " + assignedFromUser);
            await _appNotifier.LeadAssiged(assignedToUser, msg, NotificationSeverity.Info);




        }

        public async Task<List<CommonLookupDto>> GetUsers()
        {
            var user = await _userRepository.GetAll()
            .Select(users => new CommonLookupDto
            {
                Id = (int)users.Id,
                DisplayName = users == null || users.FullName == null ? "" : users.FullName.ToString()
            }).ToListAsync();
            return user;
        }
        public async Task<List<CommonLookupDto>> GetUsersListForToDo()
        {
            var user = (from o in _userRepository.GetAll()
                        join ur in _userRoleRepository.GetAll() on o.Id equals ur.UserId into urJoined
                        from ur in urJoined.DefaultIfEmpty()
                        where (ur.RoleId != 6)
                        select new CommonLookupDto
                        {
                            Id = (int)o.Id,
                            DisplayName = o.FullName.ToString()
                        });

            return await user.ToListAsync();
        }
        public async Task<PagedResultDto<PvdStatusDto>> GetAllPvdStatusForGrid(GetAllRefundReasonsInput input)
        {

            //return await _pVDStatusRepository.GetAll()
            //			.Select(pVDStatus => new PvdStatusDto
            //			{
            //				Id = pVDStatus.Id,
            //				DisplayName = pVDStatus == null || pVDStatus.Name == null ? "" : pVDStatus.Name.ToString()
            //			}).ToListAsync();

            var filteredRefundReasons = _pVDStatusRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

            var pagedAndFilteredRefundReasons = filteredRefundReasons
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var refundReasons = from o in pagedAndFilteredRefundReasons
                                select new PvdStatusDto
                                {

                                    DisplayName = o.Name,
                                    Id = o.Id

                                };

            var totalCount = await filteredRefundReasons.CountAsync();

            return new PagedResultDto<PvdStatusDto>(
                totalCount,
                await refundReasons.ToListAsync()
            );
        }

        public async Task<FileDto> GetAllPvdStatusForGridExcel(GetAllPvdStatusForGridExcelInput input)
        {

            //return await _pVDStatusRepository.GetAll()
            //			.Select(pVDStatus => new PvdStatusDto
            //			{
            //				Id = pVDStatus.Id,
            //				DisplayName = pVDStatus == null || pVDStatus.Name == null ? "" : pVDStatus.Name.ToString()
            //			}).ToListAsync();

            var filteredRefundReasons = _pVDStatusRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);


            var refundReasons = from o in filteredRefundReasons
                                select new PvdStatusDto
                                {

                                    DisplayName = o.Name,
                                    Id = o.Id

                                };

            //var totalCount = await filteredRefundReasons.CountAsync();

            return _leadsExcelExporter.GetAllPvdStatusForGridExcel(refundReasons.ToList(), input.Filename);

        }

        public async Task CreateOrEditStcStatus(CreateOrEditStcStatusDto input)
        {
            if (input.Id == null)
            {
                await CreateStcStatus(input);
            }
            else
            {
                await UpdateStcStatus(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_StcPvdStatus_Create)]
        protected virtual async Task CreateStcStatus(CreateOrEditStcStatusDto input)
        {
            var refundReason = ObjectMapper.Map<PVDStatus>(input);

            //if (AbpSession.TenantId != null)
            //{
            //	refundReason.TenantId = (int)AbpSession.TenantId;
            //}

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 31;
            dataVaultLog.ActionNote = "Stc Status Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _pVDStatusRepository.InsertAsync(refundReason);
        }

        [AbpAuthorize(AppPermissions.Pages_StcPvdStatus_Edit)]
        protected virtual async Task UpdateStcStatus(CreateOrEditStcStatusDto input)
        {
            var refundReason = await _pVDStatusRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 31;
            dataVaultLog.ActionNote = "Stc Status Updated : " + refundReason.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != refundReason.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = refundReason.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, refundReason);
        }


        [AbpAuthorize(AppPermissions.Pages_StcPvdStatus_Edit)]
        public async Task<CreateOrEditStcStatusDto> GetstatusForEdit(EntityDto input)
        {
            var refundReason = await _pVDStatusRepository.FirstOrDefaultAsync(input.Id);

            var output = new CreateOrEditStcStatusDto { Name = refundReason.Name, Id = refundReason.Id };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_StcPvdStatus_Delete)]
        public async Task DeleteStcPvdStatus(EntityDto input)
        {

            var pvddata = _jobRepository.GetAll().Where(e => e.PVDStatus == input.Id).Any();
            if (!pvddata)
            {
                var Name = _pVDStatusRepository.Get(input.Id).Name;

                var dataVaultLog = new DataVaultActivityLog();
                dataVaultLog.Action = "Deleted";
                dataVaultLog.SectionId = 31;
                dataVaultLog.ActionNote = "Stc Status Deleted : " + Name;

                await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

                await _pVDStatusRepository.DeleteAsync(input.Id);

            }
        }



        public GetJobForEditOutput getJobdetailbylead(int? leadid)
        {

            var Installerid = _jobRepository.GetAll().Where(e => e.LeadId == leadid).Select(e => e.InstallerId).FirstOrDefault();
            if (Installerid != null && Installerid != 0)
            {


                ///  var InstallerEmailPhone = GetJobForEditOutput();
                var InstallerEmailPhone = (from o in _userRepository.GetAll()
                                           join o2 in _installerDetailRepository.GetAll() on o.Id equals o2.UserId
                                           where (o2.UserId == Installerid)
                                           select new GetJobForEditOutput
                                           {
                                               InstallerEmail = o.EmailAddress,
                                               InstallerMobile = o.Mobile,
                                               InstallerID = Installerid,
                                           }).FirstOrDefault();

                return InstallerEmailPhone;
            }
            else
            {
                return null;
            }
        }


        public async Task<PagedResultDto<ReferralGridDto>> GetReferralTrackerList(ReferralInputDto input)
        {
            try
            {
                var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

                var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
                IList<string> role = await _userManager.GetRolesAsync(User);
                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
                var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

                var filteredLeads = _jobRepository.GetAll()
                    .Include(e => e.LeadFk)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) ||
                     e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                    .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                    .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                    .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                    .WhereIf(input.DateType == "DepositeDate" && SDate != null && EDate != null, e => e.DistApplied.Value.AddHours(10).Date >= SDate.Value.Date && e.DistApplied.Value.AddHours(10).Date <= EDate.Value.Date)
                    .WhereIf(input.DateType == "ActiveDate" && SDate != null && EDate != null, e => e.DistExpiryDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DistExpiryDate.Value.AddHours(10).Date <= EDate.Value.Date)
                    .WhereIf(input.DateType == "InstallationDate" && SDate != null && EDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                    .WhereIf(input.PaymentStatus == 1, e => e.ReferralPayment == true)
                    .WhereIf(input.PaymentStatus == 2, e => e.ReferralPayment == false && e.BsbNo != null && e.AccountNo != null && e.TotalCost == ((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum())))
                    .WhereIf(input.PaymentStatus == 3, e => e.ReferralPayment == false && ((e.BsbNo == null && e.AccountNo == null) || e.TotalCost != ((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum()))))
                    .WhereIf(input.SalesRep != null && input.SalesRep != 0, e => e.LeadFk.AssignToUserID == input.SalesRep)
                    .WhereIf(!input.State.IsNullOrEmpty(), e => e.LeadFk.State == input.State)
                    .Where(e => e.LeadFk.LeadSource == "Referral" && e.LeadFk.LeadSourceId == 2 && e.JobStatusId >= 4 && e.LeadFk.OrganizationId == input.OrganizationUnit)
                    .Distinct();

                var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

                var result = (from master in pagedAndFilteredLeads
                              join lead in _lookup_leadRepository.GetAll() on master.LeadId equals lead.Id
                              //join jie in _jobInstallerInvoiceRepository.GetAll() on job.Id equals jie.JobId
                              join user in _userRepository.GetAll() on (int)master.InstallerId equals (int)user.Id into userjoined
                              from user in userjoined.DefaultIfEmpty()
                              let refid = master.RefferedJobId == null ? 0 : master.RefferedJobId
                              let refstatusid = master.RefferedJobStatusId == null ? 0 : master.RefferedJobStatusId
                              select new ReferralGridDto
                              {
                                  ProjectName = master.JobNumber,
                                  Address = master.Address,
                                  Suburb = master.Suburb,
                                  State = master.State,
                                  PCode = lead.PostCode,
                                  InstallDate = master.InstallationDate,
                                  InstallComplate = master.InstalledcompleteDate,
                                  id = master.Id,
                                  companyName = lead.CompanyName,
                                  jobstatus = master.JobStatusFk.Name,
                                  TotalCost = master.TotalCost,
                                  owningAmt = Convert.ToDecimal(master.TotalCost - ((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == master.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == master.Id).Select(e => e.PaidAmmount).Sum()))),
                                  leadid = master.LeadId,
                                  RefferedProjectName = _jobRepository.GetAll().Where(e => e.Id == refid).Select(e => e.JobNumber).FirstOrDefault(),
                                  RefferedProjectStatus = _lookup_jobStatusRepository.GetAll().Where(e => e.Id == refstatusid).Select(e => e.Name).FirstOrDefault(),
                                  RefferedTotalCost = _jobRepository.GetAll().Where(e => e.Id == refid).Select(e => e.TotalCost).FirstOrDefault(),
                                  RefferedowningAmt = Convert.ToDecimal(_jobRepository.GetAll().Where(e => e.Id == refid).Select(e => e.TotalCost).FirstOrDefault() - ((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == refid).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == refid).Select(e => e.PaidAmmount).Sum()))),
                                  AccountName = master.AccountName,
                                  BsbNo = master.BsbNo,
                                  AccountNo = master.AccountNo,
                                  ReferralAmount = master.ReferralAmount,
                                  ReminderTime = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 8 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                                  ActivityDescription = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 8 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                  ActivityComment = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 24 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                  ReferralPayment = master.ReferralPayment,
                                  IsRefferalVerify = master.IsRefferalVerify,
                                  isverifyornot = master.ReferralPayment == false && master.BsbNo != null && master.AccountNo != null && master.TotalCost == ((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == master.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == master.Id).Select(e => e.PaidAmmount).Sum())) ? true : false,
                                  TotalReferral = filteredLeads.Count(),
                                  AwaitingReferral = filteredLeads.Where(e => e.ReferralPayment == false && ((e.BsbNo == null && e.AccountNo == null) || e.TotalCost != ((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum())))).Count(),
                                  ReadytopayReferral = filteredLeads.Where(e => e.ReferralPayment == false && e.BsbNo != null && e.AccountNo != null && e.TotalCost == ((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum()))).Count(),
                                  PaidReferral = filteredLeads.Where(e => e.ReferralPayment == true).Count(),
                              }).Distinct().ToList();


                var Count = filteredLeads.Count();

                return new PagedResultDto<ReferralGridDto>(
                    Count, result.ToList()
                );
            }
            catch (Exception e) { throw e; }

        }

        public async Task<FileDto> getRefferalTrackerToExcel(ReferralInputDto input)
        {
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var filteredLeads = _jobRepository.GetAll()
            //     .Include(e => e.LeadFk)
            //        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) ||
            //         e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
            //        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            //        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            //        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            //        .WhereIf(input.DateFilterType == "DepositeDate" && SDate != null && EDate != null, e => e.DistApplied.Value.AddHours(10).Date >= SDate.Value.Date && e.DistApplied.Value.AddHours(10).Date <= EDate.Value.Date)
            //        .WhereIf(input.DateFilterType == "ActiveDate" && SDate != null && EDate != null, e => e.DistExpiryDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DistExpiryDate.Value.AddHours(10).Date <= EDate.Value.Date)
            //        .WhereIf(input.DateFilterType == "InstallationDate" && SDate != null && EDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
            //        .WhereIf(input.PaymentStatus == 1, e => e.ReferralPayment == true)
            //        .WhereIf(input.PaymentStatus == 2, e => e.ReferralPayment == false && e.BsbNo != null && e.AccountNo != null && e.TotalCost == ((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum())))
            //        .WhereIf(input.PaymentStatus == 3, e => e.ReferralPayment == false && e.BsbNo == null && e.AccountNo == null && e.TotalCost != ((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum())))
            //        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
            //        .WhereIf(!input.StateId.IsNullOrEmpty(), e => e.LeadFk.State == input.StateId)
            //        .Where(e => e.LeadFk.LeadSource == "Referral" && e.LeadFk.LeadSourceId == 2 && e.JobStatusId >= 4 && e.LeadFk.OrganizationId == input.OrganizationUnit)
            //        .Distinct();

            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var filteredLeads = _jobRepository.GetAll()
            //    .Include(e => e.LeadFk)
            //    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) ||
            //     e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
            //    .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            //    .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            //    .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            //    .WhereIf(input.DateFilterType == "DepositeDate" && SDate != null && EDate != null, e => e.DistApplied.Value.AddHours(10).Date >= SDate.Value.Date && e.DistApplied.Value.AddHours(10).Date <= EDate.Value.Date)
            //    .WhereIf(input.DateFilterType == "ActiveDate" && SDate != null && EDate != null, e => e.DistExpiryDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DistExpiryDate.Value.AddHours(10).Date <= EDate.Value.Date)
            //    .WhereIf(input.DateFilterType == "InstallationDate" && SDate != null && EDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
            //    .WhereIf(input.PaymentStatus == 1, e => e.ReferralPayment == true)
            //    .WhereIf(input.PaymentStatus == 2, e => e.ReferralPayment == false && e.BsbNo != null && e.AccountNo != null && e.TotalCost == ((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum())))
            //    .WhereIf(input.PaymentStatus == 3, e => e.ReferralPayment == false && ((e.BsbNo == null && e.AccountNo == null) || e.TotalCost != ((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum()))))
            //    .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
            //    .WhereIf(!input.StateId.IsNullOrEmpty(), e => e.LeadFk.State == input.StateId)
            //    .Where(e => e.LeadFk.LeadSource == "Referral" && e.LeadFk.LeadSourceId == 2 && e.JobStatusId >= 4 && e.LeadFk.OrganizationId == input.OrganizationUnit)
            //    .Distinct();

            //var query = (from master in filteredLeads
            //             join lead in _lookup_leadRepository.GetAll() on master.LeadId equals lead.Id
            //             //join jie in _jobInstallerInvoiceRepository.GetAll() on job.Id equals jie.JobId
            //             join user in _userRepository.GetAll() on (int)master.InstallerId equals (int)user.Id into userjoined
            //             from user in userjoined.DefaultIfEmpty()
            //             let refid = master.RefferedJobId == null ? 0 : master.RefferedJobId
            //             let refstatusid = master.RefferedJobStatusId == null ? 0 : master.RefferedJobStatusId
            //             select new ReferralGridDto
            //             {
            //                 ProjectName = master.JobNumber,
            //                 Address = master.Address,
            //                 Suburb = master.Suburb,
            //                 State = master.State,
            //                 PCode = lead.PostCode,
            //                 InstallDate = master.InstallationDate,
            //                 InstallComplate = master.InstalledcompleteDate,
            //                 id = master.Id,
            //                 companyName = lead.CompanyName,
            //                 jobstatus = master.JobStatusFk.Name,
            //                 TotalCost = master.TotalCost,
            //                 owningAmt = Convert.ToDecimal(master.TotalCost - ((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == refid).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == refid).Select(e => e.PaidAmmount).Sum()))),
            //                 leadid = master.LeadId,
            //                 RefferedProjectName = _jobRepository.GetAll().Where(e => e.Id == refid).Select(e => e.JobNumber).FirstOrDefault(),
            //                 RefferedProjectStatus = _lookup_jobStatusRepository.GetAll().Where(e => e.Id == refstatusid).Select(e => e.Name).FirstOrDefault(),
            //                 RefferedTotalCost = _jobRepository.GetAll().Where(e => e.Id == refid).Select(e => e.TotalCost).FirstOrDefault(),
            //                 RefferedowningAmt = Convert.ToDecimal(_jobRepository.GetAll().Where(e => e.Id == refid).Select(e => e.TotalCost).FirstOrDefault() - ((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == refid).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == refid).Select(e => e.PaidAmmount).Sum()))),
            //                 AccountName = master.AccountName,
            //                 BsbNo = master.BsbNo,
            //                 AccountNo = master.AccountNo,
            //                 ReferralAmount = master.ReferralAmount,
            //                 ReminderTime = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 8 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
            //                 ActivityDescription = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 8 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
            //                 ActivityComment = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 24 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
            //                 ReferralPayment = master.ReferralPayment,
            //                 IsRefferalVerify = master.IsRefferalVerify,
            //                 TotalReferral = filteredLeads.Count(),
            //                 AwaitingReferral = filteredLeads.Where(e => e.ReferralPayment == false && e.BsbNo == null && e.AccountNo == null && e.TotalCost != ((_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == e.Id).Select(e => e.PaidAmmount).Sum()))).Count(),
            //                 ReadytopayReferral = filteredLeads.Where(e => e.ReferralPayment == false && e.BsbNo != null && e.AccountNo != null && e.TotalCost == ((_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == e.Id).Select(e => e.PaidAmmount).Sum()))).Count(),
            //                 PaidReferral = filteredLeads.Where(e => e.ReferralPayment == true).Count(),
            //             });
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var filteredResult = _jobRepository.GetAll()
                              .Include(e => e.LeadFk)
                              .Include(e => e.JobStatusFk)
                              .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                              .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                              .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.FirstDepositDate != null && e.LeadFk.LeadSourceId == 2)
                              .AsNoTracking()
                              .Select(e => new { e.Id, e.LeadId, e.LeadFk, e.JobNumber, e.JobStatusFk, e.State, e.TotalCost, e.ReferralPayment, e.AccountName, e.AccountNo, e.BsbNo, e.ReferralAmount, e.DepositeRecceivedDate, e.ActiveDate, e.InstallationDate, e.InstalledcompleteDate, e.IsRefferalVerify, e.ReferralPayDate, e.ReferralRemark, e.BankReferenceNo });

            var invoiceInportData = _InvoiceImportDataRepository.GetAll().AsNoTracking().Select(e => new { e.JobId, e.PaidAmmount });


            var ReferralLeadId = 0;
            if (input.FilterName == "RefferedProjectNo" && !string.IsNullOrWhiteSpace(input.Filter))
            {
                ReferralLeadId = (int)_jobRepository.GetAll().Where(e => e.JobNumber == input.Filter).AsNoTracking().Select(e => e.LeadId).FirstOrDefault();
            }

            var filter = filteredResult
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(input.FilterName == "RefferedProjectNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.ReferralLeadId == ReferralLeadId)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                        .WhereIf(input.SalesRep != 0, e => e.LeadFk.AssignToUserID == input.SalesRep)

                        .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "ActiveDate" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "ActiveDate" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.DateType == "InstallationDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.DateType == "InstallationDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.PaymentStatus == 1, e => e.ReferralPayment == false && ((e.TotalCost - (invoiceInportData.Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Any() ? (invoiceInportData.Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) : 0) > 0) || e.LeadFk.ReferralLeadId == null || e.AccountName == null || e.AccountNo == null && e.BsbNo == null || e.ReferralAmount == null)) // Awaiting

                        .WhereIf(input.PaymentStatus == 2, e => e.ReferralPayment == false && (e.TotalCost - (invoiceInportData.Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Any() ? (invoiceInportData.Where(o => o.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) : 0)) <= 0 && e.LeadFk.ReferralLeadId != null && e.AccountName != null && e.AccountNo != null && e.BsbNo != null && e.ReferralAmount != null) // ReadyToPay

                        .WhereIf(input.PaymentStatus == 3, e => e.ReferralPayment == true) // Paid
                        ;


            var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 16 && e.LeadFk.OrganizationId == input.OrganizationUnit).AsNoTracking()
                .Select(e => new { e.Id, e.SectionId, e.ActionId, e.LeadId, e.ActivityDate, e.ActivityNote });

            var results = from o in filter
                          let refLeadId = o.LeadFk.ReferralLeadId == null ? 0 : o.LeadFk.ReferralLeadId
                          let refJobId = o.LeadFk.ReferralLeadId == null ? 0 : o.LeadFk.ReferralLeadId

                          select new GetViewReferralTrackerDto()
                          {
                              Id = o.Id,
                              LeadId = o.LeadFk.Id,
                              JobNumber = o.JobNumber,
                              CompanyName = o.LeadFk.CompanyName,
                              JobStatus = o.JobStatusFk.Name,
                              JobStatusColorClass = o.JobStatusFk.ColorClass,
                              InstallComplate = o.InstalledcompleteDate,
                              TotalCost = o.TotalCost,
                              OwningAmt = Convert.ToDecimal(o.TotalCost - ((invoiceInportData.Where(x => x.JobId == o.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (invoiceInportData.Where(x => x.JobId == o.Id).Select(e => e.PaidAmmount).Sum()))),
                              RefferedProjectName = _jobRepository.GetAll().Where(e => e.LeadId == refLeadId).AsNoTracking().Select(e => e.JobNumber).FirstOrDefault(),
                              RefferedProjectStatus = _jobRepository.GetAll().Where(e => e.LeadId == refLeadId).AsNoTracking().Select(e => e.JobStatusFk.Name).FirstOrDefault(),
                              RefferedProjectStatusColorClass = _jobRepository.GetAll().Where(e => e.LeadId == refLeadId).AsNoTracking().Select(e => e.JobStatusFk.ColorClass).FirstOrDefault(),
                              RefferedTotalCost = _jobRepository.GetAll().Where(e => e.LeadId == refLeadId).AsNoTracking().Select(e => e.TotalCost).FirstOrDefault(),
                              RefferedowningAmt = Convert.ToDecimal(_jobRepository.GetAll().Where(e => e.LeadId == refLeadId).AsNoTracking().Select(e => e.TotalCost).FirstOrDefault() - ((invoiceInportData.Where(x => x.JobId == _jobRepository.GetAll().AsNoTracking().Where(e => e.LeadId == refLeadId).Select(e => e.Id).FirstOrDefault()).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (invoiceInportData.Where(x => x.JobId == _jobRepository.GetAll().Where(e => e.LeadId == refLeadId).AsNoTracking().Select(e => e.Id).FirstOrDefault()).Select(e => e.PaidAmmount).Sum()))),
                              AccountName = o.AccountName,
                              BSBNo = o.BsbNo,
                              AccountNo = o.AccountNo,
                              ActivityReminderTime = activity.Where(e => e.SectionId == 4 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                              ActivityDescription = activity.Where(e => e.SectionId == 4 && e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              ActivityComment = activity.Where(e => e.SectionId == 4 && e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                              ReferralAmount = o.ReferralAmount,
                              ReferralPayment = o.ReferralPayment,
                              IsRefferalVerify = o.IsRefferalVerify,

                              Isverifyornot = o.ReferralPayment == false && o.BsbNo != null && o.AccountNo != null && o.TotalCost == ((invoiceInportData.Where(x => x.JobId == o.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (invoiceInportData.Where(x => x.JobId == o.Id).Select(e => e.PaidAmmount).Sum())) ? true : false,

                              PaymentDate = o.ReferralPayDate,
                              BankRefNo = o.BankReferenceNo,
                              PaymentRemark = o.ReferralRemark,
                          };

            var refferallist = await results.ToListAsync();
            if (input.excelorcsvfile == 1)
            {
                return _leadsExcelExporter.reffreallistExportToFile(refferallist, "ReferralTracker.xlsx");
            }
            else
            {
                return _leadsExcelExporter.reffreallistExportToFile(refferallist, "ReferralTracker.csv");
            }

        }

        public async Task<List<jobnoLookupTableDto>> GetAlljobNumberforAdd(string leadSourceName)
        {
            var ListOfLeadSource = (from job in _jobRepository.GetAll()
                                    join lead in _lookup_leadRepository.GetAll() on job.LeadId equals lead.Id into leadjoined
                                    from lead in leadjoined
                                    where (job.JobNumber.Contains(leadSourceName) || lead.CompanyName.Contains(leadSourceName) || job.Address.Contains(leadSourceName))
                                    select new jobnoLookupTableDto
                                    {
                                        Value = job.Id,
                                        jobNumber = Convert.ToString(job.JobNumber),
                                        Name = Convert.ToString(lead.CompanyName) + "| " + Convert.ToString(job.Address)
                                    });
            return await ListOfLeadSource
                    .ToListAsync();
        }
        //public async Task<GetReferralEditDto> GetReferralForEdit(EntityDto input)
        //{
        //    //var job = await _jobRepository.FirstOrDefaultAsync(input.Id);
        //    //return job;
        //}
        public async Task<PagedResultDto<WarrantyGridDto>> GetWarrantyTrackerList(WarrantyInputDto input)
        {
            try
            {
                var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

                var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
                IList<string> role = await _userManager.GetRolesAsync(User);
                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
                var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
                var SignedQuotedDoc = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 5).OrderByDescending(e => e.Id).Select(e => e.JobId).Distinct().OrderBy(e => e).ToList();

                var PanelBrocherDoc = _jobProductItemRepository.GetAll().Select(e => e.JobId).Distinct().OrderBy(e => e).ToList();

                var InverterBrocherDoc = (from item in _jobProductItemRepository.GetAll()
                                          join i in _productItemRepository.GetAll() on item.ProductItemId equals i.Id into itmgoup
                                          from i in itmgoup
                                          where (i.ProductTypeId == 2)
                                          select (item.JobId)).Distinct().OrderBy(e => e).ToList();
                var PanelWarrantyDoc = _JobwarrantyRepository.GetAll().Where(e => e.ProductTypeId == 12).OrderByDescending(e => e.Id).Select(e => e.JobId).Distinct().OrderBy(e => e).ToList();
                var InverterWarrantyDoc = _JobwarrantyRepository.GetAll().Where(e => e.ProductTypeId == 13).OrderByDescending(e => e.Id).Select(e => e.JobId).Distinct().OrderBy(e => e).ToList();

                var filteredLeads = _jobRepository.GetAll()
                                    .Include(e => e.LeadFk)
                                    //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) ||
                                    // e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
                                    .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                                    .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                                    .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                                    .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                                    .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                                    .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                                    .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                                    .WhereIf(!input.StateId.IsNullOrEmpty(), e => e.LeadFk.State == input.StateId)
                                    .WhereIf(input.PaymentStatus == 1, e => e.WarrantyEmailSend == true)
                                    .WhereIf(input.PaymentStatus == 2, e => e.WarrantyEmailSend != true)

                                     .WhereIf(input.documentStatus == 1, e => !SignedQuotedDoc.Contains(e.Id) || !PanelBrocherDoc.Contains(e.Id) || !InverterBrocherDoc.Contains(e.Id) || !PanelWarrantyDoc.Contains(e.Id)
                                      || !InverterWarrantyDoc.Contains(e.Id) || PanelWarrantyDoc.Contains(e.Id) || !InverterWarrantyDoc.Contains(e.Id) ||
                                      ((_invoicePaymentRepository.GetAll().Where(e => e.JobId == e.Id).Select(e => e.InvoicePayTotal).Sum()) < e.TotalCost))

                                     .WhereIf(input.documentStatus == 2, e => SignedQuotedDoc.Contains(e.Id) && PanelBrocherDoc.Contains(e.Id) && InverterBrocherDoc.Contains(e.Id) && PanelWarrantyDoc.Contains(e.Id)
                                      && InverterWarrantyDoc.Contains(e.Id) && PanelWarrantyDoc.Contains(e.Id) && InverterWarrantyDoc.Contains(e.Id)
                                      && ((_invoicePaymentRepository.GetAll().Where(e => e.JobId == e.Id).Select(e => e.InvoicePayTotal).Sum()) >= e.TotalCost))

                                     .WhereIf(input.DateFilterType == "InstalledcompleteDate" && SDate != null && EDate != null, e => e.InstalledcompleteDate.Value.Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.Date <= EDate.Value.Date)
                                     .Where(e => e.InstalledcompleteDate != null
                                    && e.LeadFk.OrganizationId == input.OrganizationUnit)
                                    //.Where(e => e.InstalledcompleteDate != null && (((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum())) == e.TotalCost)
                                    //&& e.LeadFk.OrganizationId == input.OrganizationUnit)
                                    .Distinct();

                var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);



                //var inverter = (from item in _jobProductItemRepository.GetAll()
                //                join i in _productItemRepository.GetAll() on item.ProductItemId equals i.Id into itmgoup
                //                from i in itmgoup
                //                where (i.ProductTypeId == 2 && item.JobId == 1)
                //                select (item.ProductItemId)).FirstOrDefault();

                //var ijobids = _jobProductItemRepository.GetAll().Where(e => inverter.Contains((int)e.ProductItemId)).Select(e => e.JobId).ToList();
                var result = (from master in pagedAndFilteredLeads
                              join lead in _lookup_leadRepository.GetAll() on master.LeadId equals lead.Id into leadjoined
                              from lead in leadjoined.DefaultIfEmpty()
                                  //join jie in _jobInstallerInvoiceRepository.GetAll() on job.Id equals jie.JobId
                              join user in _userRepository.GetAll() on (int)lead.AssignToUserID equals (int)user.Id into userjoined
                              from user in userjoined.DefaultIfEmpty()
                              let inverterid = ((from item in _jobProductItemRepository.GetAll()
                                                 join i in _productItemRepository.GetAll() on item.ProductItemId equals i.Id into itmgoup
                                                 from i in itmgoup
                                                 where (i.ProductTypeId == 2 && item.JobId == master.Id)
                                                 select (item.ProductItemId)).FirstOrDefault())
                              let panelid = ((from item in _jobProductItemRepository.GetAll()
                                              join i in _productItemRepository.GetAll() on item.ProductItemId equals i.Id into itmgoup
                                              from i in itmgoup
                                              where (i.ProductTypeId == 1 && item.JobId == master.Id)
                                              select (item.ProductItemId)).FirstOrDefault())
                              select new WarrantyGridDto
                              {
                                  ProjectName = master.JobNumber,
                                  Customer = lead.CompanyName,
                                  Address = master.Address,
                                  Mobile = lead.Mobile,
                                  Email = lead.Email,
                                  panelName = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Id == panelid).Select(e => e.Name).FirstOrDefault(),
                                  PanelModel = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Id == panelid).Select(e => e.Model).FirstOrDefault(),
                                  PanelDocu = _JobwarrantyRepository.GetAll().Where(e => e.ProductTypeId == 12 && e.JobId == master.Id).OrderByDescending(e => e.Id).Select(e => e.Filename).FirstOrDefault(),
                                  PanelDocuPath = _JobwarrantyRepository.GetAll().Where(e => e.ProductTypeId == 12 && e.JobId == master.Id).OrderByDescending(e => e.Id).Select(e => e.Filepath).FirstOrDefault(),
                                  PanelFiletype = _JobwarrantyRepository.GetAll().Where(e => e.ProductTypeId == 12 && e.JobId == master.Id).OrderByDescending(e => e.Id).Select(e => e.FileType).FirstOrDefault(),
                                  InverterNname = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == inverterid).Select(e => e.Name).FirstOrDefault(),
                                  InverterModel = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == inverterid).Select(e => e.Model).FirstOrDefault(),
                                  InverterDoc = _JobwarrantyRepository.GetAll().Where(e => e.ProductTypeId == 13 && e.JobId == master.Id).OrderByDescending(e => e.Id).Select(e => e.Filename).FirstOrDefault(),
                                  InverterDocPath = _JobwarrantyRepository.GetAll().Where(e => e.ProductTypeId == 13 && e.JobId == master.Id).OrderByDescending(e => e.Id).Select(e => e.Filepath).FirstOrDefault(),
                                  InverterFiletype = _JobwarrantyRepository.GetAll().Where(e => e.ProductTypeId == 13 && e.JobId == master.Id).OrderByDescending(e => e.Id).Select(e => e.FileType).FirstOrDefault(),
                                  BalanceOwing = Convert.ToDecimal(master.TotalCost - (_invoicePaymentRepository.GetAll().Where(e => e.JobId == master.Id).Select(e => e.InvoicePayTotal).Sum())),
                                  //BalanceOwing = Convert.ToDecimal(master.TotalCost - (((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == master.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == master.Id).Select(e => e.PaidAmmount).Sum())))),


                                  SalesRep = user.Name,
                                  Sms = "N",
                                  Emailsend = "N",
                                  WarrantySmsSend = master.WarrantySmsSend,
                                  WarrantySmsSendDate = master.WarrantySmsSendDate,
                                  WarrantyEmailSend = master.WarrantyEmailSend,
                                  WarrantyEmailSendDate = master.WarrantyEmailSendDate,
                                  ReminderTime = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 8 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                                  ActivityDescription = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 8 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                  ActivityComment = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 24 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                                  jobid = master.Id,
                                  Leadid = master.LeadId,
                                  Total = filteredLeads.Count(),
                                  Send = filteredLeads.Where(e => e.WarrantyEmailSend == true).Count(),
                                  Pending = filteredLeads.Where(e => e.WarrantyEmailSend != true).Count(),
                              }).Distinct().ToList();


                var Count = filteredLeads.Count();

                return new PagedResultDto<WarrantyGridDto>(
                    Count, result.ToList()
                );

            }
            catch (Exception e) { throw e; }
        }

        public async Task SaveDocument(UploadDocumentInput input)
        {
            var filename = "";
            if (input.FileName.Contains("+"))
            {
                filename = input.FileName.Replace("+", "_");
            }
            else
            {
                filename = input.FileName.Replace(" ", "_");
            }
            byte[] ByteArray = _tempFileCacheManager.GetFile(input.FileToken);
            var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, filename, "Warrantys", input.JobId, 0, AbpSession.TenantId);

            var document = new Jobwarranty()
            {
                Id = input.Id,
                ProductTypeId = input.DocumentTypeId,
                JobId = input.JobId,
                Filename = filename,
                FileType = input.FileType,
                Filepath = "\\Documents" + "\\" + filepath.TenantName + "\\" + filepath.JobNumber + "\\Warrantys\\"
            };
            await _JobwarrantyRepository.InsertAsync(document);
            //}

            // await _jobAppService.JobActive((int)input.JobId);

            var Jobs = _jobRepository.GetAll().Where(e => e.Id == input.JobId).FirstOrDefault();
            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.SectionId = 0;
            leadactivity.ActionId = 17;
            leadactivity.ActionNote = "Job Document Uploaded";
            leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }
        public async Task<List<GetDocumentTypeForView>> GetAllDocumentType()
        {
            List<int> typeid = new List<int>();
            typeid.Add(12);
            typeid.Add(13);
            var documentType = _lookup_documentTypeRepository.GetAll().Where(e => typeid.Contains(e.Id));
            var filtereddocType = from o in documentType
                                  select new GetDocumentTypeForView()
                                  {
                                      Id = o.Id,
                                      DocumentTitle = o.Title
                                  };
            return filtereddocType.ToList();

        }
        public async Task<List<GetDocumentTypeForView>> getRemainingDocumentType(int id)
        {
            List<GetDocumentTypeForView> filtereddocType = new List<GetDocumentTypeForView>();
            var jobid = _jobRepository.GetAll().Where(e => e.LeadId == id).Select(e => e.Id).FirstOrDefault();
            var SignedQuotedDoc = _documentRepository.GetAll().Where(e => e.JobId == jobid && e.DocumentTypeId == 5).Select(e => e.FileName).Distinct().FirstOrDefault();

            var PanelBrocherDoc = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Id == (_jobProductItemRepository.GetAll().Where(e => e.JobId == jobid).Select(e => e.ProductItemId).FirstOrDefault())).Select(e => e.FileName).FirstOrDefault();
            var InverterBrocherDoc = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == (from item in _jobProductItemRepository.GetAll()
                                                                                                                 join i in _productItemRepository.GetAll() on item.ProductItemId equals i.Id into itmgoup
                                                                                                                 from i in itmgoup
                                                                                                                 where (i.ProductTypeId == 2 && item.JobId == jobid)
                                                                                                                 select (item.ProductItemId)).FirstOrDefault()).Select(e => e.FileName).FirstOrDefault();
            var PanelWarrantyDoc = _JobwarrantyRepository.GetAll().Where(e => e.JobId == jobid && e.ProductTypeId == 12).Select(e => e.Filename).FirstOrDefault();
            var InverterWarrantyDoc = _JobwarrantyRepository.GetAll().Where(e => e.JobId == jobid && e.ProductTypeId == 13).Select(e => e.Filename).FirstOrDefault();
            if (SignedQuotedDoc == null)

            {
                GetDocumentTypeForView doc = new GetDocumentTypeForView();
                doc.Id = 5;
                doc.DocumentTitle = "Signed Quoted";
                filtereddocType.Add(doc);
            }


            if (PanelBrocherDoc == null)

            {
                GetDocumentTypeForView doc = new GetDocumentTypeForView();
                doc.Id = 2;
                doc.DocumentTitle = "Panel Brocher";
                filtereddocType.Add(doc);
            }
            if (InverterBrocherDoc == null)

            {
                GetDocumentTypeForView doc = new GetDocumentTypeForView();
                doc.Id = 3;
                doc.DocumentTitle = "Inverter Brocher";
                filtereddocType.Add(doc);
            }
            if (PanelWarrantyDoc == null)

            {
                GetDocumentTypeForView doc = new GetDocumentTypeForView();
                doc.Id = 12;
                doc.DocumentTitle = "Panel Warranty";
                filtereddocType.Add(doc);
            }
            if (InverterWarrantyDoc == null)

            {
                GetDocumentTypeForView doc = new GetDocumentTypeForView();
                doc.Id = 13;
                doc.DocumentTitle = "Inverter Warranty";
                filtereddocType.Add(doc);
            }
            return filtereddocType.ToList();

        }
        public async Task SaveDocumentforwarrantymail(UploadDocumentInput input)
        {
            try
            {
                var jobid = _jobRepository.GetAll().Where(e => e.LeadId == input.JobId).Select(e => e.Id).FirstOrDefault();
                var filename = "";
                if (input.FileName.Contains("+"))
                {
                    filename = input.FileName.Replace("+", "_");
                }
                else
                {
                    filename = input.FileName.Replace(" ", "_");
                }
                var ByteArray = _tempFileCacheManager.GetFile(input.FileToken);

                if (input.DocumentTypeId == 1)
                {


                    var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, filename, "Documents", jobid, 0, AbpSession.TenantId);


                    if (input.DocumentTypeId == 1)
                    {
                        var documentsign = new Document()
                        {
                            JobId = jobid,
                            DocumentTypeId = input.DocumentTypeId,
                            FileName = filename,
                            FileType = input.FileType,
                            FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\" + filepath.JobNumber + "\\Documents\\",
                        };
                        await _documentRepository.InsertAsync(documentsign);
                        var Jobs = _jobRepository.GetAll().Where(e => e.Id == jobid).FirstOrDefault();
                        var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                        LeadActivityLog leadactivity = new LeadActivityLog();
                        leadactivity.SectionId = 0;
                        leadactivity.ActionId = 17;
                        leadactivity.ActionNote = "Job Document Uploaded";
                        leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
                        if (AbpSession.TenantId != null)
                        {
                            leadactivity.TenantId = (int)AbpSession.TenantId;
                        }
                        await _leadactivityRepository.InsertAsync(leadactivity);
                    }

                }

                if (input.DocumentTypeId > 1)
                {
                    var id = 0;
                    if (input.DocumentTypeId == 2)
                    {
                        id = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Id == (_jobProductItemRepository.GetAll().Where(e => e.JobId == jobid).Select(e => e.ProductItemId).FirstOrDefault())).Select(e => e.Id).FirstOrDefault();
                    }
                    if (input.DocumentTypeId == 3)
                    {
                        id = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == (from item in _jobProductItemRepository.GetAll()
                                                                                                         join i in _productItemRepository.GetAll() on item.ProductItemId equals i.Id into itmgoup
                                                                                                         from i in itmgoup
                                                                                                         where (i.ProductTypeId == 2 && item.JobId == jobid)
                                                                                                         select (item.ProductItemId)).FirstOrDefault()).Select(e => e.Id).FirstOrDefault();

                    }
                    var ProductItems = _productItemRepository.GetAll().Where(e => e.Id == id).FirstOrDefault();
                    string FileName = ProductItems.Model + DateTime.Now.Ticks;

                    var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, FileName, "ProductItem", 0, 0, AbpSession.TenantId);

                    ProductItems.FileName = FileName;
                    ProductItems.FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\ProductItem\\";

                    await _productItemRepository.UpdateAsync(ProductItems);
                    var Jobs = _jobRepository.GetAll().Where(e => e.Id == jobid).FirstOrDefault();
                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    LeadActivityLog leadactivity = new LeadActivityLog();
                    leadactivity.SectionId = 0;
                    leadactivity.ActionId = 17;
                    leadactivity.ActionNote = "Job Document Uploaded";
                    leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }

                if (input.DocumentTypeId == 0)
                {
                    var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, filename, "Documents", jobid, 0, AbpSession.TenantId);
                    if (input.DocumentTypeId == 0)
                    {
                        var documentsign = new EmailTempData()
                        {
                            JobId = jobid,
                            DocumentTypeId = input.DocumentTypeId,
                            FileName = filename,
                            FileType = input.FileType,
                            FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\" + filepath.JobNumber + "\\" + "Documents" + "\\",
                        };
                        await _EmailTempDataRepository.InsertAsync(documentsign);
                    }

                }

            }
            catch (Exception e) { throw e; }
        }
        public async Task SendWarrantymail(SmsEmailDto input)
        {
            try
            {
                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.SectionId = 0;
                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                //input.EmailTo = "kalpana.pawar@meghtechnologies.com";

                var job = _jobRepository.GetAll().Where(e => e.Id == input.LeadId).FirstOrDefault();
                var lead = _lookup_leadRepository.GetAll().Where(e => e.Id == job.LeadId).FirstOrDefault();
                var jobs = await _jobRepository.FirstOrDefaultAsync(job.Id);
                var output = ObjectMapper.Map<CreateOrEditJobDto>(jobs);
                List<SendEmailAttachmentDto> emailattch = new List<SendEmailAttachmentDto>();
                SendEmailAttachmentDto doc = new SendEmailAttachmentDto();
                //var SignedQuotedDoc = _documentRepository.GetAll().Where(e => e.JobId == job.Id && e.DocumentTypeId == 5).Select(e => e.FileName).Distinct().FirstOrDefault();
                //var SignedQuotedpath = _documentRepository.GetAll().Where(e => e.JobId == job.Id && e.DocumentTypeId == 5).Select(e => e.FilePath).Distinct().FirstOrDefault();
                if (input.SignedQuotedDoc != null)
                {
                    doc.FileName = input.SignedQuotedDoc;
                    doc.FilePath = input.SignedQuotedDocPath;
                    emailattch.Add(doc);
                }
                SendEmailAttachmentDto doc1 = new SendEmailAttachmentDto();
                //var PanelBrocherDoc = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Id == (_jobProductItemRepository.GetAll().Where(e => e.JobId == job.Id).Select(e => e.ProductItemId).FirstOrDefault())).Select(e => e.FileName).FirstOrDefault();
                //var PanelBrocherDocpath = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Id == (_jobProductItemRepository.GetAll().Where(e => e.JobId == job.Id).Select(e => e.ProductItemId).FirstOrDefault())).Select(e => e.FilePath).FirstOrDefault();
                if (input.PanelBrocherDoc != null)
                {
                    doc1.FileName = input.PanelBrocherDoc;
                    doc1.FilePath = input.PanelBrocherDocPath;
                    emailattch.Add(doc1);
                }
                SendEmailAttachmentDto doc2 = new SendEmailAttachmentDto();
                //var InverterBrocherDoc = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == (from item in _jobProductItemRepository.GetAll()
                //                                                                                                     join i in _productItemRepository.GetAll() on item.ProductItemId equals i.Id into itmgoup
                //                                                                                                     from i in itmgoup
                //                                                                                                     where (i.ProductTypeId == 2 && item.JobId == job.Id)
                //                                                                                                     select (item.ProductItemId)).FirstOrDefault()).Select(e => e.FileName).FirstOrDefault();

                //var InverterBrocherDocpath = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == (from item in _jobProductItemRepository.GetAll()
                //                                                                                                         join i in _productItemRepository.GetAll() on item.ProductItemId equals i.Id into itmgoup
                //                                                                                                         from i in itmgoup
                //                                                                                                         where (i.ProductTypeId == 2 && item.JobId == job.Id)
                //                                                                                                         select (item.ProductItemId)).FirstOrDefault()).Select(e => e.FilePath).FirstOrDefault();

                if (input.InverterBrocherDoc != null)
                {
                    doc2.FileName = input.InverterBrocherDoc;
                    doc2.FilePath = input.InverterBrocherDocPath;
                    emailattch.Add(doc2);
                }
                SendEmailAttachmentDto doc3 = new SendEmailAttachmentDto();
                //var PanelWarrantyDoc = _JobwarrantyRepository.GetAll().Where(e => e.JobId == job.Id && e.ProductTypeId == 12).Select(e => e.Filename).FirstOrDefault();
                //var PanelWarrantypath = _JobwarrantyRepository.GetAll().Where(e => e.JobId == job.Id && e.ProductTypeId == 12).Select(e => e.Filepath).FirstOrDefault();

                if (input.PanelWarrantyDoc != null)
                {
                    doc3.FileName = input.PanelWarrantyDoc;
                    doc3.FilePath = input.PanelWarrantyDocPath;
                    emailattch.Add(doc3);
                }
                SendEmailAttachmentDto doc4 = new SendEmailAttachmentDto();
                //var InverterWarrantyDoc = _JobwarrantyRepository.GetAll().Where(e => e.JobId == job.Id && e.ProductTypeId == 13).Select(e => e.Filename).FirstOrDefault();
                //var InverterWarrantypath = _JobwarrantyRepository.GetAll().Where(e => e.JobId == job.Id && e.ProductTypeId == 13).Select(e => e.Filepath).FirstOrDefault();
                if (input.InverterWarrantyDoc != null)
                {
                    doc4.FileName = input.InverterWarrantyDoc;
                    doc4.FilePath = input.InverterWarrantyDocPath;
                    emailattch.Add(doc4);
                }

                var EmailTempData = _EmailTempDataRepository.GetAll().Where(e => e.JobId == job.Id).ToList();
                if (EmailTempData != null)
                {
                    foreach (var item in EmailTempData)
                    {
                        SendEmailAttachmentDto doc5 = new SendEmailAttachmentDto();
                        doc5.FileName = item.FileName;
                        doc5.FilePath = item.FilePath;
                        emailattch.Add(doc5);
                    }

                }


                var Path = _env.WebRootPath;
                var MainFolder = Path;


                if (input.cc != null && input.Bcc != null)
                {
                    MailMessage mail = new MailMessage
                    {
                        To = { input.EmailTo }, //{ "viral.jain@meghtechnologies.com" }, //
                        CC = { input.cc },
                        Bcc = { input.Bcc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };


                    foreach (var item in emailattch)
                    {
                        mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
                    }


                    await this._emailSender.SendAsync(mail);
                    jobs.WarrantyEmailSend = true;
                    jobs.WarrantyEmailSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobs);
                }
                else if (input.cc != null && input.cc != "" && input.Bcc == null)
                {
                    MailMessage mail = new MailMessage
                    {
                        To = { input.EmailTo }, //{ "viral.jain@meghtechnologies.com" }, //
                        CC = { input.cc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };


                    foreach (var item in emailattch)
                    {
                        mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
                    }
                    await this._emailSender.SendAsync(mail);
                    jobs.WarrantyEmailSend = true;
                    jobs.WarrantyEmailSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobs);
                }
                else if (input.cc == null && input.Bcc != null && input.Bcc != "")
                {
                    MailMessage mail = new MailMessage
                    {
                        To = { input.EmailTo }, //{ "viral.jain@meghtechnologies.com" }, //
                        Bcc = { input.Bcc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };


                    foreach (var item in emailattch)
                    {
                        mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
                    }

                    await this._emailSender.SendAsync(mail);
                    jobs.WarrantyEmailSend = true;
                    jobs.WarrantyEmailSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobs);
                }
                else
                {
                    MailMessage mail = new MailMessage
                    {
                        To = { input.EmailTo }, //{ "viral.jain@meghtechnologies.com" }, //

                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };



                    foreach (var item in emailattch)
                    {
                        mail.Attachments.Add(new Attachment(Path + item.FilePath + item.FileName));
                    }

                    await this._emailSender.SendAsync(mail);
                    jobs.WarrantyEmailSend = true;
                    jobs.WarrantyEmailSendDate = DateTime.UtcNow;
                    ObjectMapper.Map(output, jobs);
                }


                if (EmailTempData != null)
                {
                    foreach (var item in EmailTempData)
                    {
                        _EmailTempDataRepository.HardDelete(item);
                    }
                }
                //leadactivity.ActionId = 12;
                //leadactivity.LeadId = Convert.ToInt32(job.LeadId);
                //if (AbpSession.TenantId != null)
                //{
                //    leadactivity.TenantId = (int)AbpSession.TenantId;
                //}
                //await _leadactivityRepository.InsertAsync(leadactivity);
            }
            catch (Exception e) { throw e; }
        }
        public async Task deleteWarrantyattachment(int id)
        {
            var item = _EmailTempDataRepository.GetAll().Where(e => e.Id == id).FirstOrDefault();
            _EmailTempDataRepository.HardDelete(item);
        }

        public async Task RevertApprovedInvoice(int id)
        {
            var jobType = await _jobRepository.FirstOrDefaultAsync((int)id);
            if (AbpSession.TenantId != null)
            {
                jobType.TenantId = (int)AbpSession.TenantId;
            }

            jobType.ReferralPayDate = null;
            jobType.ReferralPayment = false;
            jobType.BankReferenceNo = null;
            jobType.ReferralRemark = null;
            await _jobRepository.UpdateAsync(jobType);
        }

        public async Task<ListResultDto<SendEmailAttachmentDto>> getWarrantyattachment(int id)
        {
            var jobType = (from item in _EmailTempDataRepository.GetAll().Where(e => e.JobId == id)
                           select new SendEmailAttachmentDto
                           {
                               FileName = item.FileName,
                               FilePath = item.FilePath,
                               id = item.Id
                           }).ToList();

            return new ListResultDto<SendEmailAttachmentDto>(jobType.MapTo<List<SendEmailAttachmentDto>>());
        }

        public async Task<TotalSammaryCountDto> getWarrantyTrackerCount(int organizationid)
        {
            var output = new TotalSammaryCountDto();

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            output.WarrantyTotalData = _jobRepository.GetAll()
            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            .Where(e => e.LeadFk.OrganizationId == organizationid && e.InstalledcompleteDate != null && (((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum())) == e.TotalCost)).Select(e => e.Id).Distinct().Count();

            output.WarrantyTotalEmailSend = _jobRepository.GetAll()
            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            .Where(e => e.LeadFk.OrganizationId == organizationid && e.InstalledcompleteDate != null && e.WarrantyEmailSend == true && (((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum())) == e.TotalCost)).Distinct().Select(e => e.Id).Count();

            output.WarrantyTotalEmailPending = _jobRepository.GetAll()
            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
            .Where(e => e.LeadFk.OrganizationId == organizationid && e.InstalledcompleteDate != null && e.WarrantyEmailSend != true && (((_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum()) == null ? 0 : (_InvoiceImportDataRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.PaidAmmount).Sum())) == e.TotalCost)).Distinct().Select(e => e.Id).Count();

            return output;
        }

        public async Task<List<GetJobOldSysDetaiilForEditOutpu>> getJobOldSystemDetailByJobId(int jobid)
        {
            var joboldsysdetail = _JobOldSysDetail.GetAll().Where(x => x.JobFk.Id == jobid).ToList();

            var output = new List<GetJobOldSysDetaiilForEditOutpu>();

            foreach (var item in joboldsysdetail)
            {
                var outobj = new GetJobOldSysDetaiilForEditOutpu();
                outobj.JobOldSysDetail = ObjectMapper.Map<CreateOrEditJobOdSysDetails>(item);
                output.Add(outobj);
            }

            return output;
        }
        //public async Task<FileDto> GetReferralTrackerListExcel(ReferralInputDto input)
        //{

        //    var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
        //    IList<string> role = await _userManager.GetRolesAsync(User);
        //    var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
        //    var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

        //        var filteredLeads = _jobRepository.GetAll()
        //            .Include(e => e.LeadFk)
        //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) ||
        //                    e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.Phone.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter))
        //            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
        //                    .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
        //                    .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
        //                    .WhereIf(input.PaymentStatus == 1, e => e.ReferralPayment == true)
        //                     .WhereIf(input.PaymentStatus == 2, e => e.ReferralPayment == false && e.BsbNo != null && e.AccountNo != null && ((_invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.InvoicePayTotal).Sum()) == e.TotalCost))
        //                     .WhereIf(input.PaymentStatus == 3, e => e.ReferralPayment == false && e.BsbNo == null && e.AccountNo == null && ((_invoicePaymentRepository.GetAll().Where(x => x.JobId == e.Id).Select(e => e.InvoicePayTotal).Sum()) != e.TotalCost))
        //                     .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
        //                     .WhereIf(!input.StateId.IsNullOrEmpty(), e => e.LeadFk.State == input.StateId)
        //                    .Where(e => e.LeadFk.LeadSource == "Referral" && e.LeadFk.LeadSourceId == 2 && e.JobStatusId >= 4)
        //            .Distinct();
        //    var query = (from master in filteredLeads
        //                 join lead in _lookup_leadRepository.GetAll() on master.LeadId equals lead.Id
        //                 //join jie in _jobInstallerInvoiceRepository.GetAll() on job.Id equals jie.JobId
        //                 join user in _userRepository.GetAll() on (int)master.InstallerId equals (int)user.Id into userjoined
        //                 from user in userjoined.DefaultIfEmpty()
        //                 let refid = master.RefferedJobId == null ? 0 : master.RefferedJobId
        //                 let refstatusid = master.RefferedJobStatusId == null ? 0 : master.RefferedJobStatusId
        //                 select new ReferralGridDto
        //                 {
        //                     ProjectName = master.JobNumber,
        //                     //Customer = user.Name +" " + user.FullName,
        //                     Address = master.Address,
        //                     Suburb = master.Suburb,
        //                     State = master.State,
        //                     PCode = lead.PostCode,
        //                     InstallDate = master.InstallationDate,
        //                     InstallComplate = master.InstalledcompleteDate,

        //                     id = master.Id,
        //                     companyName = lead.CompanyName,
        //                     jobstatus = master.JobStatusFk.Name,
        //                     TotalCost = master.TotalCost,
        //                     owningAmt = Convert.ToDecimal(master.TotalCost - (_invoicePaymentRepository.GetAll().Where(e => e.JobId == master.Id).Select(e => e.InvoicePayTotal).Sum())),
        //                     leadid = master.LeadId,

        //                     RefferedProjectName = _jobRepository.GetAll().Where(e => e.Id == refid).Select(e => e.JobNumber).FirstOrDefault(),
        //                     RefferedProjectStatus = _lookup_jobStatusRepository.GetAll().Where(e => e.Id == refstatusid).Select(e => e.Name).FirstOrDefault(),
        //                     RefferedTotalCost = _jobRepository.GetAll().Where(e => e.Id == refid).Select(e => e.TotalCost).FirstOrDefault(),
        //                     RefferedowningAmt = Convert.ToDecimal(_jobRepository.GetAll().Where(e => e.Id == refid).Select(e => e.TotalCost).FirstOrDefault() - (_invoicePaymentRepository.GetAll().Where(e => e.JobId == refid).Select(e => e.InvoicePayTotal).Sum())),

        //                     AccountName = master.AccountName,

        //                     BsbNo = master.BsbNo,
        //                     AccountNo = master.AccountNo,
        //                     ReferralAmount = master.ReferralAmount,
        //                     ActivityReminderTime = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 8 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
        //                     ActivityDescription = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 8 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
        //                     ActivityComment = _leadactivityRepository.GetAll().Where(e => (e.SectionId == 16) && e.ActionId == 24 && e.LeadId == master.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault()
        //                    ,
        //                     ReferralPayment = master.ReferralPayment
        //                 });

        //    var stcListDtos = await query.ToListAsync();

        //    return _leadsExcelExporter.STCExportToFile(stcListDtos);
        //}

        public async Task verifyorNotVerifyJobRefund(int id, Boolean VerifyOrNot)
        {
            var Jobs = _jobRepository.GetAll().Where(e => e.Id == id).FirstOrDefault();
            if (AbpSession.TenantId != null)
            {
                Jobs.TenantId = (int)AbpSession.TenantId;
            }
            Jobs.IsRefferalVerify = VerifyOrNot;
            await _jobRepository.UpdateAsync(Jobs);

            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 11;
            leadactivity.SectionId = 0;
            if (VerifyOrNot == true)
            {
                leadactivity.ActionNote = "Job Refferal Verify";
            }
            else
            {
                leadactivity.ActionNote = "Job Refferal Not Verify";
            }
            leadactivity.LeadId = Convert.ToInt32(Jobs.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        //JobGrid
        public async Task<PagedResultDto<GetJobForViewDto>> GetAllJobHoldTrackerData(GetAllHoldJobInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var team_list = _userTeamRepository.GetAll();
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = team_list.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }
            var panel_list = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1);
            var inverter_list = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2);
            var PanelDetails = new List<int?>();
            if (input.PanelModel != null)
            {
                var Panel = panel_list.Where(e => e.ProductTypeId == 1 && e.Model.Contains(input.PanelModel)).Select(e => e.Id).FirstOrDefault();
                PanelDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Panel).Select(e => e.JobId).ToList();
            }

            var InvertDetails = new List<int?>();
            if (input.InvertModel != null)
            {
                var Inverter = inverter_list.Where(e => e.ProductTypeId == 2 && e.Model.Contains(input.InvertModel)).Select(e => e.Id).FirstOrDefault();
                InvertDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Inverter).Select(e => e.JobId).ToList();
            }
            var Panels = panel_list.Where(e => e.ProductTypeId == 1).Select(e => e.Id).ToList();
            var Inverts = inverter_list.Where(e => e.ProductTypeId == 2).Select(e => e.Id).ToList();
            var leadactivityLog = _leadactivityRepository.GetAll().Where(e => e.ActionId == 8 && e.SectionId == 27 && e.LeadFk.OrganizationId == input.OrganizationUnit);

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobTypeFk)
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.RoofTypeFk)
                        .Include(e => e.RoofAngleFk)
                        .Include(e => e.ElecDistributorFk)
                        .Include(e => e.LeadFk)
                        .Include(e => e.ElecRetailerFk)
                        .Include(e => e.PaymentOptionFk)
                        .Include(e => e.DepositOptionFk)
                        .Include(e => e.PromotionOfferFk)
                        .Include(e => e.HouseTypeFk)
                        .Include(e => e.FinanceOptionFk)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter))
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofTypeNameFilter), e => e.RoofTypeFk != null && e.RoofTypeFk.Name == input.RoofTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofAngleNameFilter), e => e.RoofAngleFk != null && e.RoofAngleFk.Name == input.RoofAngleNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecDistributorNameFilter), e => e.ElecDistributorFk != null && e.ElecDistributorFk.Name == input.ElecDistributorNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecRetailerNameFilter), e => e.ElecRetailerFk != null && e.ElecRetailerFk.Name == input.ElecRetailerNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentOptionNameFilter), e => e.PaymentOptionFk != null && e.PaymentOptionFk.Name == input.PaymentOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DepositOptionNameFilter), e => e.DepositOptionFk != null && e.DepositOptionFk.Name == input.DepositOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionOfferNameFilter), e => e.PromotionOfferFk != null && e.PromotionOfferFk.Name == input.PromotionOfferNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.HouseTypeNameFilter), e => e.HouseTypeFk != null && e.HouseTypeFk.Name == input.HouseTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.PostalCode, input.PostalCodeFrom) >= 0)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.PostalCode, input.PostalCodeTo) <= 0)
                        .WhereIf(input.paymentid != 0, e => e.PaymentOptionId == input.paymentid)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
                        .WhereIf(input.HoldReasonId != 0, e => e.JobHoldReasonId == input.HoldReasonId)
                        .WhereIf(input.JobDateFilter == "CreationDate" && SDate != null && EDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "FirstDeposite" && SDate != null && EDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "DepositeReceived" && SDate != null && EDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "Active" && SDate != null && EDate != null, e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "NextFollowUp" && SDate != null && EDate != null, e => e.NextFollowUpDate.Value.AddHours(10).Date >= SDate.Value.Date && e.NextFollowUpDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallBook" && SDate != null && EDate != null, e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallComplete" && SDate != null && EDate != null, e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => e.JobStatusId != 1)
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PanelModel), e => PanelDetails.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(input.Solarvicstatus != 0, e => e.SolarRebateStatus == input.Solarvicstatus)
                        .WhereIf(input.Readytoactive != null && input.Readytoactive == 1, e => e.IsActive == true && (e.JobStatusId == 1 || e.JobStatusId == 4))
                        .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemEndRange != null && input.SystemEndRange != 0, e => e.SystemCapacity >= input.SystemStrtRange && e.SystemCapacity <= input.SystemEndRange)
                        .WhereIf(input.SystemStrtRange != null && input.SystemStrtRange != 0 && input.SystemEndRange == null && input.SystemEndRange == 0, e => e.SystemCapacity == input.SystemStrtRange)
                        .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemStrtRange == null && input.SystemStrtRange == 0, e => e.SystemCapacity == input.SystemEndRange)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadFk.LeadSourceId))
                        .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == j.LeadId).Select(e => e.OrganizationId).FirstOrDefault()) == input.OrganizationUnit && j.DepositeRecceivedDate != null && j.JobStatusId == 2);


            var pagedAndFilteredJobs = filteredJobs
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var jobids = filteredJobs.Select(e => e.Id).ToList();

            var jobs = from o in pagedAndFilteredJobs
                       join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                       from s1 in j1.DefaultIfEmpty()

                       join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                       from s2 in j2.DefaultIfEmpty()

                       join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                       from s5 in j5.DefaultIfEmpty()

                       join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                       from s6 in j6.DefaultIfEmpty()

                       join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                       from s7 in j7.DefaultIfEmpty()

                       select new GetJobForViewDto()
                       {
                           Job = new JobDto
                           {
                               RegPlanNo = o.RegPlanNo,
                               LotNumber = o.LotNumber,
                               Address = o.Address,
                               Suburb = o.Suburb,
                               State = o.State,
                               UnitNo = o.UnitNo,
                               UnitType = o.UnitType,
                               NMINumber = o.NMINumber,
                               Id = o.Id,
                               ApplicationRefNo = o.ApplicationRefNo,
                               DistAppliedDate = o.DistAppliedDate,
                               Notes = o.Note,
                               InstallerNotes = o.InstallerNotes,
                               JobNumber = o.JobNumber,
                               MeterNumber = o.PeakMeterNo,
                               OldSystemDetails = o.OldSystemDetails,
                               PostalCode = o.PostalCode,
                               StreetName = o.StreetName,
                               StreetNo = o.StreetNo,
                               StreetType = o.StreetType,
                               ElecDistributorId = o.ElecDistributorId,
                               ElecRetailerId = o.ElecRetailerId,
                               JobStatusId = o.JobStatusId,
                               LeadId = o.LeadId,
                               JobTypeId = o.JobTypeId,
                               InstallationDate = o.InstallationDate,
                               JobGridSmsSend = o.JobGridSmsSend,
                               JobGridSmsSendDate = o.JobGridSmsSendDate,
                               JobGridEmailSend = o.JobGridEmailSend,
                               JobGridEmailSendDate = o.JobGridEmailSendDate,
                               CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                               DepositeRecceivedDate = o.DepositeRecceivedDate,
                               JobHoldReasonName = _jobHoldReasonRepository.GetAll().Where(e => e.Id == o.JobHoldReasonId).Select(e => e.JobHoldReason).FirstOrDefault(),
                               HoldNotes = o.JobHoldReason,
                           },

                           JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                           JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                           ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                           LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                           Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                           Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                           ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
                           LastComment = leadactivityLog.Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(x => x.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           LastCommentDate = leadactivityLog.Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(x => x.Id).Select(e => e.CreationTime).FirstOrDefault(),
                           LeadSourceName = _lookup_leadSourceRepository.GetAll().Where(e => (e.Id == s6.LeadSourceId)).Select(e => e.Name).FirstOrDefault(),
                           JobSolarRebateStatus = o.SolarRebateStatus.ToString(),
                           ActivityReminderTime = leadactivityLog.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate).FirstOrDefault(),
                           ActivityDescription = leadactivityLog.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                           ActivityComment = _leadactivityRepository.GetAll().Where(e => e.SectionId == 8 && e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                       };

            var totalCount = await filteredJobs.CountAsync();

            return new PagedResultDto<GetJobForViewDto>(
                totalCount,
                await jobs.ToListAsync()
            );
        }

        public async Task<jobcountdata> getAllJobHoldTrackerCount(GetAllHoldJobInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            //    var user_list = _userRepository.GetAll();
            var team_list = _userTeamRepository.GetAll();
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = team_list.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }
            var panel_list = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1);
            var inverter_list = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2);
            var PanelDetails = new List<int?>();
            if (input.PanelModel != null)
            {
                var Panel = panel_list.Where(e => e.ProductTypeId == 1 && e.Model.Contains(input.PanelModel)).Select(e => e.Id).FirstOrDefault();
                PanelDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Panel).Select(e => e.JobId).ToList();
            }

            var InvertDetails = new List<int?>();
            if (input.InvertModel != null)
            {
                var Inverter = inverter_list.Where(e => e.ProductTypeId == 2 && e.Model.Contains(input.InvertModel)).Select(e => e.Id).FirstOrDefault();
                InvertDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Inverter).Select(e => e.JobId).ToList();
            }
            var Panels = panel_list.Where(e => e.ProductTypeId == 1).Select(e => e.Id).ToList();
            var Inverts = inverter_list.Where(e => e.ProductTypeId == 2).Select(e => e.Id).ToList();
            var leadactivityLog = _leadactivityRepository.GetAll().Where(e => e.ActionId == 8 && e.LeadFk.OrganizationId == input.OrganizationUnit);
            //// var abc = JobVicStatusList.Where(e => e.Id == null).Select(e => e.DisplayName).FirstOrDefault();

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobTypeFk)
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.RoofTypeFk)
                        .Include(e => e.RoofAngleFk)
                        .Include(e => e.ElecDistributorFk)
                        .Include(e => e.LeadFk)
                        .Include(e => e.ElecRetailerFk)
                        .Include(e => e.PaymentOptionFk)
                        .Include(e => e.DepositOptionFk)
                        .Include(e => e.PromotionOfferFk)
                        .Include(e => e.HouseTypeFk)
                        .Include(e => e.FinanceOptionFk)
                       .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofTypeNameFilter), e => e.RoofTypeFk != null && e.RoofTypeFk.Name == input.RoofTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofAngleNameFilter), e => e.RoofAngleFk != null && e.RoofAngleFk.Name == input.RoofAngleNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecDistributorNameFilter), e => e.ElecDistributorFk != null && e.ElecDistributorFk.Name == input.ElecDistributorNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecRetailerNameFilter), e => e.ElecRetailerFk != null && e.ElecRetailerFk.Name == input.ElecRetailerNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentOptionNameFilter), e => e.PaymentOptionFk != null && e.PaymentOptionFk.Name == input.PaymentOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DepositOptionNameFilter), e => e.DepositOptionFk != null && e.DepositOptionFk.Name == input.DepositOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionOfferNameFilter), e => e.PromotionOfferFk != null && e.PromotionOfferFk.Name == input.PromotionOfferNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.HouseTypeNameFilter), e => e.HouseTypeFk != null && e.HouseTypeFk.Name == input.HouseTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.PostalCode, input.PostalCodeFrom) >= 0)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.PostalCode, input.PostalCodeTo) <= 0)
                        .WhereIf(input.paymentid != 0, e => e.PaymentOptionId == input.paymentid)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
                        .WhereIf(input.HoldReasonId != 0, e => e.JobHoldReasonId == input.HoldReasonId)
                        .WhereIf(input.JobDateFilter == "CreationDate" && SDate != null && EDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "FirstDeposite" && SDate != null && EDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "DepositeReceived" && SDate != null && EDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "Active" && SDate != null && EDate != null, e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "NextFollowUp" && SDate != null && EDate != null, e => e.NextFollowUpDate.Value.AddHours(10).Date >= SDate.Value.Date && e.NextFollowUpDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallBook" && SDate != null && EDate != null, e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallComplete" && SDate != null && EDate != null, e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => e.JobStatusId != 1)
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PanelModel), e => PanelDetails.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(input.Solarvicstatus != 0, e => e.SolarRebateStatus == input.Solarvicstatus)
                        .WhereIf(input.Readytoactive != null && input.Readytoactive == 1, e => e.IsActive == true && (e.JobStatusId == 1 || e.JobStatusId == 4))
                        .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemEndRange != null && input.SystemEndRange != 0, e => e.SystemCapacity >= input.SystemStrtRange && e.SystemCapacity <= input.SystemEndRange)
                        .WhereIf(input.SystemStrtRange != null && input.SystemStrtRange != 0 && input.SystemEndRange == null && input.SystemEndRange == 0, e => e.SystemCapacity == input.SystemStrtRange)
                        .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemStrtRange == null && input.SystemStrtRange == 0, e => e.SystemCapacity == input.SystemEndRange)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadFk.LeadSourceId))
                        //.WhereIf(input.LeadSourceIdFilter != 0 && input.LeadSourceIdFilter != null, e => e.LeadFk.LeadSourceId == input.LeadSourceIdFilter)
                        .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == j.LeadId).Select(e => e.OrganizationId).FirstOrDefault()) == input.OrganizationUnit && j.DepositeRecceivedDate != null && j.JobStatusId == 2);
            var jobids = filteredJobs.Select(e => e.Id).ToList();

            var output = new jobcountdata();

            output.TotalNoOfPanel = _jobProductItemRepository.GetAll().Where(e => Panels.Contains((int)e.ProductItemId) && jobids.Contains((int)e.JobId)).Select(e => e.Quantity).Sum();

            output.TotalNoOfInvert = _jobProductItemRepository.GetAll().Where(e => Inverts.Contains((int)e.ProductItemId) && jobids.Contains((int)e.JobId)).Select(e => e.Quantity).Sum();


            output.TotalSystemCapacity = filteredJobs.Select(e => e.SystemCapacity).Sum();
            output.TotalCost = filteredJobs.Select(e => e.TotalCost).Sum();
            output.DepositeReceived = filteredJobs.Where(e => e.JobStatusId == 4).Count();
            output.Active = filteredJobs.Where(e => e.JobStatusId == 5).Count();
            output.InstallJob = filteredJobs.Where(e => e.JobStatusId == 8).Count();
            return output;

        }

        //Job Tracker Excel Export
        public async Task<FileDto> getJobHoldTrackerToExcel(GetAllHoldJobInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var team_list = _userTeamRepository.GetAll();
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = team_list.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
            }
            var panel_list = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1);
            var inverter_list = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2);
            var PanelDetails = new List<int?>();
            if (input.PanelModel != null)
            {
                var Panel = panel_list.Where(e => e.ProductTypeId == 1 && e.Model.Contains(input.PanelModel)).Select(e => e.Id).FirstOrDefault();
                PanelDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Panel).Select(e => e.JobId).ToList();
            }

            var InvertDetails = new List<int?>();
            if (input.InvertModel != null)
            {
                var Inverter = inverter_list.Where(e => e.ProductTypeId == 2 && e.Model.Contains(input.InvertModel)).Select(e => e.Id).FirstOrDefault();
                InvertDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Inverter).Select(e => e.JobId).ToList();
            }
            var Panels = panel_list.Where(e => e.ProductTypeId == 1).Select(e => e.Id).ToList();
            var Inverts = inverter_list.Where(e => e.ProductTypeId == 2).Select(e => e.Id).ToList();
            var leadactivityLog = _leadactivityRepository.GetAll().Where(e => e.ActionId == 8 && e.LeadFk.OrganizationId == input.OrganizationUnit);
            //// var abc = JobVicStatusList.Where(e => e.Id == null).Select(e => e.DisplayName).FirstOrDefault();

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobTypeFk)
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.RoofTypeFk)
                        .Include(e => e.RoofAngleFk)
                        .Include(e => e.ElecDistributorFk)
                        .Include(e => e.LeadFk)
                        .Include(e => e.ElecRetailerFk)
                        .Include(e => e.PaymentOptionFk)
                        .Include(e => e.DepositOptionFk)
                        .Include(e => e.PromotionOfferFk)
                        .Include(e => e.HouseTypeFk)
                        .Include(e => e.FinanceOptionFk)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.Email.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter))
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                        .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofTypeNameFilter), e => e.RoofTypeFk != null && e.RoofTypeFk.Name == input.RoofTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RoofAngleNameFilter), e => e.RoofAngleFk != null && e.RoofAngleFk.Name == input.RoofAngleNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecDistributorNameFilter), e => e.ElecDistributorFk != null && e.ElecDistributorFk.Name == input.ElecDistributorNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ElecRetailerNameFilter), e => e.ElecRetailerFk != null && e.ElecRetailerFk.Name == input.ElecRetailerNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentOptionNameFilter), e => e.PaymentOptionFk != null && e.PaymentOptionFk.Name == input.PaymentOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DepositOptionNameFilter), e => e.DepositOptionFk != null && e.DepositOptionFk.Name == input.DepositOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PromotionOfferNameFilter), e => e.PromotionOfferFk != null && e.PromotionOfferFk.Name == input.PromotionOfferNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.HouseTypeNameFilter), e => e.HouseTypeFk != null && e.HouseTypeFk.Name == input.HouseTypeNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.PostalCode, input.PostalCodeFrom) >= 0)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.PostalCode, input.PostalCodeTo) <= 0)
                        .WhereIf(input.paymentid != 0, e => e.PaymentOptionId == input.paymentid)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
                        .WhereIf(input.HoldReasonId != 0, e => e.JobHoldReasonId == input.HoldReasonId)
                        .WhereIf(input.JobDateFilter == "CreationDate" && SDate != null && EDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "FirstDeposite" && SDate != null && EDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "DepositeReceived" && SDate != null && EDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "Active" && SDate != null && EDate != null, e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "NextFollowUp" && SDate != null && EDate != null, e => e.NextFollowUpDate.Value.AddHours(10).Date >= SDate.Value.Date && e.NextFollowUpDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallBook" && SDate != null && EDate != null, e => e.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallComplete" && SDate != null && EDate != null, e => e.InstalledcompleteDate.Value.AddHours(10).Date >= SDate.Value.Date && e.InstalledcompleteDate.Value.AddHours(10).Date <= EDate.Value.Date)
                        .WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => e.JobStatusId != 1)
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PanelModel), e => PanelDetails.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(input.Solarvicstatus != 0, e => e.SolarRebateStatus == input.Solarvicstatus)
                        .WhereIf(input.Readytoactive != null && input.Readytoactive == 1, e => e.IsActive == true && (e.JobStatusId == 1 || e.JobStatusId == 4))
                        .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemEndRange != null && input.SystemEndRange != 0, e => e.SystemCapacity >= input.SystemStrtRange && e.SystemCapacity <= input.SystemEndRange)
                        .WhereIf(input.SystemStrtRange != null && input.SystemStrtRange != 0 && input.SystemEndRange == null && input.SystemEndRange == 0, e => e.SystemCapacity == input.SystemStrtRange)
                        .WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemStrtRange == null && input.SystemStrtRange == 0, e => e.SystemCapacity == input.SystemEndRange)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadFk.LeadSourceId))
                        .Where(j => (_lookup_leadRepository.GetAll().Where(e => e.Id == j.LeadId).Select(e => e.OrganizationId).FirstOrDefault()) == input.OrganizationUnit && j.DepositeRecceivedDate != null && j.JobStatusId == 2);

            var jobs = filteredJobs.Select(e => e.Id).ToList();
            var jobproductiteamlist = _jobProductItemRepository.GetAll().Where(e => jobs.Contains((int)e.JobId)).Select(e => e.ProductItemId).ToList();
            var query = (from o in filteredJobs

                         join o1 in _lookup_jobTypeRepository.GetAll() on o.JobTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _lookup_jobStatusRepository.GetAll() on o.JobStatusId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o3 in _lookup_roofTypeRepository.GetAll() on o.RoofTypeId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         join o5 in _lookup_elecDistributorRepository.GetAll() on o.ElecDistributorId equals o5.Id into j5
                         from s5 in j5.DefaultIfEmpty()

                         join o6 in _lookup_leadRepository.GetAll() on o.LeadId equals o6.Id into j6
                         from s6 in j6.DefaultIfEmpty()

                         join o7 in _lookup_elecRetailerRepository.GetAll() on o.ElecRetailerId equals o7.Id into j7
                         from s7 in j7.DefaultIfEmpty()

                         join o8 in _lookup_paymentOptionRepository.GetAll() on o.PaymentOptionId equals o8.Id into j8
                         from s8 in j8.DefaultIfEmpty()

                         join o13 in _lookup_houseTypeRepository.GetAll() on o.HouseTypeId equals o13.Id into j13
                         from s13 in j13.DefaultIfEmpty()

                         join o14 in _lookup_financeOptionRepository.GetAll() on o.FinanceOptionId equals o14.Id into j14
                         from s14 in j14.DefaultIfEmpty()

                         let inverterid = ((from item in _jobProductItemRepository.GetAll()
                                            join i in _productItemRepository.GetAll() on item.ProductItemId equals i.Id into itmgoup
                                            from i in itmgoup
                                            where (i.ProductTypeId == 2 && item.JobId == o.Id)
                                            select (item.ProductItemId)).FirstOrDefault())

                         let Pannelid = ((from item in _jobProductItemRepository.GetAll()
                                          join i in _productItemRepository.GetAll() on item.ProductItemId equals i.Id into itmgoup
                                          from i in itmgoup
                                          where (i.ProductTypeId == 1 && item.JobId == o.Id)
                                          select (item.ProductItemId)).FirstOrDefault())

                         select new GetJobForViewDto()
                         {
                             Job = new JobDto
                             {
                                 RegPlanNo = o.RegPlanNo,
                                 LotNumber = o.LotNumber,
                                 Address = o.Address,
                                 Suburb = o.Suburb,
                                 State = o.State,
                                 UnitNo = o.UnitNo,
                                 UnitType = o.UnitType,
                                 NMINumber = o.NMINumber,
                                 Id = o.Id,
                                 ApplicationRefNo = o.ApplicationRefNo,
                                 DistAppliedDate = o.DistAppliedDate,
                                 Notes = o.Note,
                                 InstallerNotes = o.InstallerNotes,
                                 JobNumber = o.JobNumber,
                                 MeterNumber = o.MeterNumber,
                                 OldSystemDetails = o.OldSystemDetails,
                                 PostalCode = o.PostalCode,
                                 StreetName = o.StreetName,
                                 StreetNo = o.StreetNo,
                                 StreetType = o.StreetType,
                                 ElecDistributorId = o.ElecDistributorId,
                                 ElecRetailerId = o.ElecRetailerId,
                                 JobStatusId = o.JobStatusId,
                                 LeadId = o.LeadId,
                                 JobTypeId = o.JobTypeId,
                                 InstallationDate = o.InstallationDate,
                                 TotalCost = o.TotalCost,
                                 SystemCapacity = o.SystemCapacity,
                                 DistExpiryDate = o.DepositeRecceivedDate,
                                 CreationTime = o.CreationTime,
                                 CurrentLeadOwaner = _userRepository.GetAll().Where(e => e.Id == s6.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
                                 JobHoldReasonName = _jobHoldReasonRepository.GetAll().Where(e => e.Id == o.JobHoldReasonId).Select(e => e.JobHoldReason).FirstOrDefault(),
                                 HoldNotes = o.JobHoldReason,
                                 ManualQuote = o.ManualQuote,
                                 Note = o.Note
                             },

                             FinanceWith = s8 == null || s8.Name == null ? "" : s8.Name.ToString(),
                             HouseType = s13 == null || s13.Name == null ? "" : s13.Name.ToString(),
                             RoofType = s3 == null || s3.Name == null ? "" : s3.Name.ToString(),
                             JobTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                             JobStatusName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
                             ElecDistributorName = s5 == null || s5.Name == null ? "" : s5.Name.ToString(),
                             LeadCompanyName = s6 == null || s6.CompanyName == null ? "" : s6.CompanyName.ToString(),
                             Email = s6 == null || s6.Email == null ? "" : s6.Email.ToString(),
                             Mobile = s6 == null || s6.Mobile == null ? "" : s6.Mobile.ToString(),
                             ElecRetailerName = s7 == null || s7.Name == null ? "" : s7.Name.ToString(),
                             LastComment = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(x => x.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                             LastCommentDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == s6.Id && e.ActionId == 8).OrderByDescending(x => x.Id).Select(e => e.CreationTime).FirstOrDefault(),
                             LeadSourceName = _lookup_leadSourceRepository.GetAll().Where(e => (e.Id == s6.LeadSourceId)).Select(e => e.Name).FirstOrDefault(),
                             Team = _userTeamRepository.GetAll().Where(e => (e.UserId == s6.AssignToUserID)).Select(e => e.TeamFk.Name).FirstOrDefault(),
                             DateDiff = Convert.ToInt32((o.CreationTime - s6.CreationTime).TotalDays),
                             LeadCreationDate = s6.CreationTime,
                             TotalNoOfPanel = _jobProductItemRepository.GetAll().Where(e => e.JobId == o.Id && Panels.Contains((int)e.ProductItemId)).Select(e => e.Quantity).Sum(),
                             TotalNoOfInvert = _jobProductItemRepository.GetAll().Where(e => e.JobId == o.Id && Inverts.Contains((int)e.ProductItemId)).Select(e => e.Quantity).Sum(),
                             Panels = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Id == Pannelid).Select(e => e.Name).FirstOrDefault(),
                             Inverters = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Id == inverterid).Select(e => e.Name).FirstOrDefault(),
                             LeadStatus = s6.LeadStatusId != 6 ? _lookup_leadStatusRepository.GetAll().Where(e => e.Id == s6.LeadStatusId).Select(e => e.Status).FirstOrDefault() : s2.Name.ToString(),
                             FirstDepDate = _lookup_InvoicepaymentRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.InvoicePayDate).FirstOrDefault(),
                             FirstDepAmmount = _lookup_InvoicepaymentRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.InvoicePayTotal).FirstOrDefault(),
                         });

            var jobgridrListDtos = await query.ToListAsync();
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _leadsExcelExporter.HoldJobTrackerExportToFile(jobgridrListDtos);
            }
            else
            {
                return _leadsExcelExporter.HoldJobTrackerExportToCsvFile(jobgridrListDtos);
            }
        }

        public async Task<List<CalculateJobProductItemDeatilDto>> GetCalculateJobProductItemDeatils(List<CalculateJobProductItemDeatilDto> ItemsList)
        {
            //var JobProduct = _jobProductItemRepository.GetAll().Where(e => e.JobId == JobId);

            //var output = from o in JobProduct
            //             select new CalculateJobProductItemDeatilDto()
            //             {
            //                 ProductType = o.ProductItemFk.ProductTypeFk.Name,
            //                 ProductItem = o.ProductItemFk.Name,
            //                 Model = o.ProductItemFk.Model,
            //                 Quantity = (o.Quantity != null ? Convert.ToInt32(o.Quantity) : 0),
            //                 Amount = (o.Quantity != null ? Convert.ToDecimal(o.Quantity) : 0) * (o.ProductItemFk.Amount != null ? Convert.ToDecimal(o.ProductItemFk.Amount) : 0)
            //             };

            var output = from o in ItemsList
                         join pi in _productItemRepository.GetAll() on o.ProductItemId equals pi.Id into PItem
                         from s8 in PItem.DefaultIfEmpty()

                         join pt in _productTypeRepository.GetAll() on s8.ProductTypeId equals pt.Id into PType
                         from s9 in PType.DefaultIfEmpty()

                         select new CalculateJobProductItemDeatilDto()
                         {
                             ProductType = s9.Name,
                             ProductItem = s8.Name,
                             Model = s8.Model,
                             Quantity = (o.Quantity != null ? Convert.ToInt32(o.Quantity) : 0),
                             Amount = (o.Quantity != null ? Convert.ToDecimal(o.Quantity) : 0) * (s8.Amount != null ? Convert.ToDecimal(s8.Amount) : 0)
                         };

            return await Task.FromResult(output.ToList());
        }

        public async Task UpdateAddress(CreateOrEditJobDto input)
        {
            try
            {
                var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);

                input.Address = input.UnitNo + " " + input.UnitType + " " + input.StreetNo + " " + input.StreetName + " " + input.StreetType;

                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 11;
                leadactivity.SectionId = 0;
                leadactivity.ActionNote = "Job Updated";
                leadactivity.LeadId = Convert.ToInt32(job.LeadId);

                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }

                var jobactionid = _leadactivityRepository.InsertAndGetId(leadactivity);

                var list = new List<JobTrackerHistory>();
                if (job.Address != input.Address)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Address";
                    jobhistory.PrevValue = job.Address;
                    jobhistory.CurValue = input.Address;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                    job.Address = input.UnitNo + " " + input.UnitType + " " + input.StreetNo + " " + input.StreetName + " " + input.StreetType;
                }

                if (job.UnitNo != input.UnitNo)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "UnitNo";
                    jobhistory.PrevValue = job.UnitNo;
                    jobhistory.CurValue = input.UnitNo;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.UnitType != input.UnitType)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "UnitType";
                    jobhistory.PrevValue = job.UnitType;
                    jobhistory.CurValue = input.UnitType;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.StreetNo != input.StreetNo)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "StreetNo";
                    jobhistory.PrevValue = job.StreetNo;
                    jobhistory.CurValue = input.StreetNo;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.StreetName != input.StreetName)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "StreetName";
                    jobhistory.PrevValue = job.StreetName;
                    jobhistory.CurValue = input.StreetName;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.StreetType != input.StreetType)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "StreetType";
                    jobhistory.PrevValue = job.StreetType;
                    jobhistory.CurValue = input.StreetType;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.Suburb != input.Suburb)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Suburb";
                    jobhistory.PrevValue = job.Suburb;
                    jobhistory.CurValue = input.Suburb;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.PostalCode != input.PostalCode)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "PostalCode";
                    jobhistory.PrevValue = job.PostalCode;
                    jobhistory.CurValue = input.PostalCode;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.State != input.State)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "State";
                    jobhistory.PrevValue = job.State;
                    jobhistory.CurValue = input.State;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.Latitude != input.Latitude)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Latitude";
                    jobhistory.PrevValue = job.Latitude;
                    jobhistory.CurValue = input.Latitude;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (job.Longitude != input.Longitude)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Longitude";
                    jobhistory.PrevValue = job.Longitude;
                    jobhistory.CurValue = input.Longitude;
                    jobhistory.Action = "Site Address Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                    jobhistory.JobIDId = job.Id;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                job.Address = input.Address;
                job.UnitNo = input.UnitNo;
                job.UnitType = input.UnitType;
                job.StreetNo = input.StreetNo;
                job.StreetName = input.StreetName;
                job.StreetType = input.StreetType;
                job.Suburb = input.Suburb;
                job.State = input.State;
                job.PostalCode = input.PostalCode;
                job.Latitude = input.Latitude;
                job.Longitude = input.Longitude;

                await _jobRepository.UpdateAsync(job);

                if (list.Count > 0)
                {
                    await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
                    await _dbcontextprovider.GetDbContext().SaveChangesAsync();
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        protected virtual async Task ImportInGoogleSheet(Job job, DateTime? depositeDate)
        {
            try
            {
                var ApplicationName = "Google_Ads_GEC_for_Leads_OCT_Sales_Data";
                var SpreadsheetId = "1kL6LjelQkI9sUoqwzpT3c9FuKLvmaP_AX8r2OHI_CiA";

                Google.Apis.Auth.OAuth2.GoogleCredential credential;

                //var filepath = Path.Combine(_env.ContentRootPath, "SupportJson/AriseSolar-692588a0321d.json"); // Old
                var filepath = Path.Combine(_env.ContentRootPath, "SupportJson/splendid-skill-419306-4e4b32191ef2.json");

                using (var stream = new FileStream(filepath, FileMode.Open, FileAccess.Read))
                {
                    credential = GoogleCredential.FromStream(stream)
                        .CreateScoped(Scopes);
                }

                ////Creating Google Sheets API service...
                service = new Google.Apis.Sheets.v4.SheetsService(new Google.Apis.Services.BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = ApplicationName,
                });

                //var sheetName = await _organizationUnitRepository.GetAll().Where(e => e.Id == job.LeadFk.OrganizationId).Select(e => e.DisplayName).FirstOrDefaultAsync();
                var sheetName = "Arise Solar";

                string range = $"{sheetName}!A:F";
                //string range = "A:F";
                var valueRange = new ValueRange();

                string dt = "";
                if (depositeDate != null)
                {
                    dt = Convert.ToDateTime(depositeDate.Value.Date.ToString()).ToString("MMM dd,yyyy hh:mm:ss tt");
                }

                //List<object> list = new List<object>() { job.LeadFk.GCLId, "Offline Sales FINAL", dt, job.TotalCost, "", "" };

                var m = job.LeadFk.Mobile.Substring(job.LeadFk.Mobile.Length - 9);
                var Mobile = "+61" + m.ToString();

                List<object> list = new List<object>() { job.LeadFk.Email, Mobile, "Sales", dt, job.TotalCost, "AUD" };

                valueRange.Values = new List<IList<object>> { list };
                var appendRequest = service.Spreadsheets.Values.Append(valueRange, SpreadsheetId, range);
                appendRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
                var appendReponse = await appendRequest.ExecuteAsync();
            }
            catch
            {
                throw new UserFriendlyException("Google Sheet Data Issue.");
            }
        }

        public async Task<GetActualCostOutput> GetActualCost(GetActualCostInput input)
        {
            decimal actualCost = 0;

            //var jobs = await _jobRepository.GetAsync(input.JobId);
            var jobs = await _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.Id == input.JobId).FirstOrDefaultAsync();
            
            var CreationTime = (_timeZoneConverter.Convert(jobs.CreationTime, (int)AbpSession.TenantId)).Value.Date;

            var noOfPanel = 0;
            if (input.ProductItemList != null)
            {
                noOfPanel = input.ProductItemList.Where(e => e.ProductTypeId == 1).Sum(e => e.Qty == null ? 0 : (int)e.Qty);
            }

            #region Installation Cost
            //var insCost = _stateWiseInstallationCostRepository.GetAll().Where(e => e.StateFk.Name == input.StateName).FirstOrDefault();
            //decimal InstallationCost = 0;
            //if (insCost != null)
            //{
            //    InstallationCost = input.AreaName == "Metro" ? (decimal)insCost.Metro : input.AreaName == "Regional" ? (decimal)insCost.Regional : 0;
            //}

            decimal actualInstallationCost = 0;
            var InsInvoice = _installerInvoiceRepository.GetAll().Any(e => e.JobId == jobs.Id && e.Installation_Maintenance_Inspection == "Installation");
            var installerInvoiceAmount = _installerInvoiceRepository.GetAll().Where(e => e.JobId == jobs.Id).Sum(e => e.Amount);
            if ((installerInvoiceAmount == null || installerInvoiceAmount == 0) && InsInvoice == false)
            {
                var insCost = _postCodePriceRepository.GetAll().Where(e => e.PostCodePricePeriodFk.Month == CreationTime.Month && e.PostCodePricePeriodFk.Year == CreationTime.Year && e.PostCode == jobs.PostalCode).FirstOrDefault();
                decimal InstallationCost = 0;
                if (insCost != null)
                {
                    InstallationCost = (decimal)insCost.Price;
                }

                if (input.SystemKw == null)
                {
                    input.SystemKw = 0;
                }
                actualInstallationCost = InstallationCost * (decimal)input.SystemKw;
            }
            else
            {
                actualInstallationCost = Convert.ToDecimal(installerInvoiceAmount);
            }
            #endregion

            #region Installation Product Item List
            bool ItemVal = false;
            decimal ProductItemCost = 0;
            
            decimal panelCost = 0.0m;
            decimal inverterCost = 0.0m;
            decimal batteryCost = 0.0m;
            decimal PricePerWatt = 0.0m;
            if (input.ProductItemList != null)
            {
                foreach (var item in input.ProductItemList)
                {
                    var ItemObj = _installationItemListRepository.GetAll().Where(e => e.ProductItemId == item.ProductItemId && e.InstallationItemPeriodFk.StartDate.Date <= CreationTime && e.InstallationItemPeriodFk.EndDate.Date >= CreationTime && e.InstallationItemPeriodFk.OrganizationUnit == jobs.LeadFk.OrganizationId).FirstOrDefault();

                    if (ItemObj != null)
                    {
                        var ItemCost = ItemObj.UnitPrice * (item.Qty == null ? 0 : item.Qty);

                        ProductItemCost += (decimal)ItemCost;

                        if ((decimal)ItemCost == 0)
                        {
                            if (item.ProductTypeId == 1 || item.ProductTypeId == 2 || item.ProductTypeId == 5)
                            {
                                ItemVal = true;
                            }
                        }

                        if (item.ProductTypeId == 1)
                        {
                            PricePerWatt = ItemObj.PricePerWatt != null ? (decimal)ItemObj.PricePerWatt : 0.0m;
                            panelCost += (decimal)ItemCost;
                        }
                        if (item.ProductTypeId == 2)
                        {
                            inverterCost += (decimal)ItemCost;
                        }
                        if (item.ProductTypeId == 5)
                        {
                            batteryCost += (decimal)ItemCost;
                        }
                    }
                    else
                    {
                        if (item.ProductTypeId == 1 || item.ProductTypeId == 2 || item.ProductTypeId == 5)
                        {
                            ItemVal = true;
                        }
                    }

                    
                }
            }
            #endregion

            #region Fixed Cost Price
            decimal FixedCostPrice = 0;
            var fixedCostPriceList = _fixedCostPriceRepository.GetAllList();

            if (fixedCostPriceList != null)
            {
                foreach (var item in fixedCostPriceList)
                {
                    if (item.Type == "Panel")
                    {
                        FixedCostPrice += (decimal)item.Cost * noOfPanel;
                    }
                    else if (item.Type == "Kw")
                    {
                        FixedCostPrice += (decimal)item.Cost * (decimal)input.SystemKw;
                    }
                    else if (item.Type == "Job")
                    {
                        FixedCostPrice += (decimal)item.Cost * 1;
                    }
                }
            }
            #endregion

            #region Extra Installation Charges
            decimal ExtraInsCost = 0;
            var extraInstallationCharges = _extraInstallationChargeRepository.GetAll().Where(e => e.StateFk.Name == input.StateName);

            //House Type
            decimal houseTypeCost = 0;
            if (input.HouseType != null)
            {
                var houseType = extraInstallationCharges.Where(e => e.Category == "House Type" && e.NameId == input.HouseType).FirstOrDefault();

                if (houseType != null)
                {
                    var cost = input.AreaName == "Metro" ? (decimal)houseType.MetroAmount : input.AreaName == "Regional" ? (decimal)houseType.RegionalAmount : 0;

                    if (houseType.Type == "Panel")
                    {
                        houseTypeCost = cost * noOfPanel;
                    }
                    else if (houseType.Type == "Kw")
                    {
                        houseTypeCost = cost * (decimal)input.SystemKw;
                    }
                    else if (houseType.Type == "Job")
                    {
                        houseTypeCost = cost * 1;
                    }
                }
            }

            //Roof Type
            decimal roofTypeCost = 0;
            if (input.RoofType != null)
            {
                var roofType = extraInstallationCharges.Where(e => e.Category == "Roof Type" && e.NameId == input.RoofType).FirstOrDefault();

                if (roofType != null)
                {
                    var cost = input.AreaName == "Metro" ? (decimal)roofType.MetroAmount : input.AreaName == "Regional" ? (decimal)roofType.RegionalAmount : 0;

                    if (roofType.Type == "Panel")
                    {
                        roofTypeCost = cost * noOfPanel;
                    }
                    else if (roofType.Type == "Kw")
                    {
                        roofTypeCost = cost * (decimal)input.SystemKw;
                    }
                    else if (roofType.Type == "Job")
                    {
                        roofTypeCost = cost * 1;
                    }
                }
            }

            //Roof Angle
            decimal roofAngleCost = 0;
            if (input.RoofAngle != null)
            {
                var roofAngle = extraInstallationCharges.Where(e => e.Category == "Roof Angle" && e.NameId == input.RoofAngle).FirstOrDefault();

                if (roofAngle != null)
                {
                    var cost = input.AreaName == "Metro" ? (decimal)roofAngle.MetroAmount : input.AreaName == "Regional" ? (decimal)roofAngle.RegionalAmount : 0;

                    if (roofAngle.Type == "Panel")
                    {
                        roofAngleCost = cost * noOfPanel;
                    }
                    else if (roofAngle.Type == "Kw")
                    {
                        roofAngleCost = cost * (decimal)input.SystemKw;
                    }
                    else if (roofAngle.Type == "Job")
                    {
                        roofAngleCost = cost * 1;
                    }
                }
            }

            //Price Variation
            decimal variationCost = 0;
            if (input.VariationList != null)
            {
                foreach (var item in input.VariationList)
                {
                    var variation = extraInstallationCharges.Where(e => e.Category == "Price Variation" && e.NameId == item).FirstOrDefault();
                    if (variation != null)
                    {
                        var cost = input.AreaName == "Metro" ? (decimal)variation.MetroAmount : input.AreaName == "Regional" ? (decimal)variation.RegionalAmount : 0;

                        if (variation.Type == "Panel")
                        {
                            variationCost += cost * noOfPanel;
                        }
                        else if (variation.Type == "Kw")
                        {
                            variationCost += cost * (decimal)input.SystemKw;
                        }
                        else if (variation.Type == "Job")
                        {
                            variationCost += cost * 1;
                        }
                    }
                }
            }

            ExtraInsCost = (houseTypeCost + roofTypeCost + roofAngleCost + variationCost);
            #endregion

            #region Bettery Installation Cost
            decimal BetteryInstallationCost = 0;

            decimal? batterySize = 0;
            //var noOfBattery = 0;
            if (input.ProductItemList != null)
            {
                batterySize = input.ProductItemList.Where(e => e.ProductTypeId == 5).Sum(e => e.Size * (e.Qty == null ? 0 : (int)e.Qty));
                //noOfBattery = input.ProductItemList.Where(e => e.ProductTypeId == 5).Sum(e => e.Qty == null ? 0 : (int)e.Qty);
            }

            if (batterySize != null && batterySize != 0)
            {
                //batterySize = batterySize * noOfBattery;
                var btryCost = _batteryInstallationCostRepository.GetAll().FirstOrDefault();

                if (btryCost != null)
                {
                    var fixCost = btryCost.FixCost;

                    decimal extraCost = 0;
                    if (batterySize > btryCost.Kw)
                    {
                        extraCost = ((decimal)batterySize - btryCost.Kw) * btryCost.ExtraCost;
                    }

                    BetteryInstallationCost = fixCost + extraCost;
                }
            }
            #endregion

            #region Travel Cost
            decimal ActualTravelCost = 0;
            //if(input.Distance != null)
            //{
            //    var travelCostList = _travelCostRepository.GetAll().OrderBy(e => e.StartKm).ToList();

            //    foreach (var item in travelCostList)
            //    {
            //        if(input.Distance > 0)
            //        {

            //        }
            //    }
            //}
            #endregion

            #region STC Value
            decimal StcCostPrice = 0;
            var STCCost = _stcCostRepository.GetAll().FirstOrDefault();
            if (STCCost != null)
            {
                StcCostPrice = STCCost.Cost * (input.STC == null ? 0 : (decimal)input.STC);
            }
            #endregion

            #region Finace Payment Type
            var finType = _financeOptionRepository.GetAll().Where(e => e.Id == input.FinPaymentType).FirstOrDefault();
            decimal percentageValue = 0;
            var totalVal = input.TotalCost != null ? Convert.ToDecimal(input.TotalCost) : 0;
            if (finType != null)
            {
                var per = finType.Percentage != null ? Convert.ToDecimal(finType.Percentage) : 0;
                if (per != 0 && totalVal != 0)
                {
                    percentageValue = (totalVal * per) / 100;
                }
            }
            #endregion

            actualCost = (actualInstallationCost + ProductItemCost + FixedCostPrice + ExtraInsCost + BetteryInstallationCost + ActualTravelCost + percentageValue) - StcCostPrice;

            if (ItemVal == true)
            {
                actualCost = 0;
            }

            if (actualInstallationCost == 0 && BetteryInstallationCost == 0 && noOfPanel > 0)
            {
                actualCost = 0;
            }

            actualCost = actualCost - (input.CategoryDDiscountAmt != null ? (decimal)input.CategoryDDiscountAmt : 0);

            var np = input.TotalCost - actualCost;
            var np_sys = panelCost != 0 && inverterCost != 0 && input.SystemKw > 0 ? np / input.SystemKw : 0;

            var _category = _categoryInstallationItemListRepository.GetAll().Where(e => e.CategoryInstallationItemPeriodFk.StartDate.Date <= CreationTime && e.CategoryInstallationItemPeriodFk.EndDate >= CreationTime && e.CategoryInstallationItemPeriodFk.OrganizationUnit == jobs.LeadFk.OrganizationId && e.StartValue <= np_sys && e.EndValue >= np_sys).FirstOrDefault();

            var output = new GetActualCostOutput {
                ActualCost = actualCost,
                Category = (_category != null ? _category.CategoryName : ""),
                PanelCost = panelCost,
                PricePerWatt = PricePerWatt,
                InverterCost = inverterCost,
                BatteryCost = batteryCost,
                BetteryInstallationCost = BetteryInstallationCost,
                InstallationCost = actualInstallationCost
            };

            return output;
        }

        public async Task GetPylon(EntityDto input, int SectionId)
        {
            try
            {
                var jobs = await _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.Id == input.Id).FirstOrDefaultAsync();
                var jobOrg = _organizationUnitRepository.GetAll().Where(e => e.Id == jobs.LeadFk.OrganizationId).FirstOrDefault();
                //var RefNumber = jobs.JobNumber.Replace(jobOrg.Code, "");
                var RefNumber = jobs.JobNumber;

                var orgMap = _organizationUnitMapRepository.GetAll().Where(e => e.OrganizationUnitId == jobOrg.Id && e.MapProvider == "Pylon").FirstOrDefault();

                if (orgMap != null)
                {
                    //HttpClient client = new HttpClient();
                    //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", orgMap.MapApiKey);

                    HttpClient client = new HttpClient();
                    var request = new HttpRequestMessage(HttpMethod.Get, "https://api.getpylon.com/v1/solar_projects?filter[reference_number]=" + RefNumber);
                    request.Headers.Add("Accept", "application/vnd.api+json");
                    request.Headers.Add("Authorization", "Bearer " + orgMap.MapApiKey);
                    var response = await client.SendAsync(request);
                    response.EnsureSuccessStatusCode();

                    var res = await response.Content.ReadAsStringAsync();

                    Root root = JsonConvert.DeserializeObject<Root>(res);

                    if (root.data.Count > 0)
                    {
                        var designId = root.data[0].relationships.designs.data[root.data[0].relationships.designs.data.Count - 1].id;
                        if (!string.IsNullOrEmpty(designId))
                        {
                            var client1 = new HttpClient();
                            var request1 = new HttpRequestMessage(HttpMethod.Get, "https://api.getpylon.com/v1/solar_designs/" + designId + "?fields[solar_designs]=summary,created_at");
                            request1.Headers.Add("Accept", "application/vnd.api+json");
                            request1.Headers.Add("Authorization", "Bearer " + orgMap.MapApiKey);
                            var response1 = await client.SendAsync(request1);
                            response1.EnsureSuccessStatusCode();
                            var res1 = await response1.Content.ReadAsStringAsync();

                            SRoot sRoot = JsonConvert.DeserializeObject<SRoot>(res1);

                            if (sRoot != null)
                            {
                                var pdfUrl = sRoot.data.attributes.summary.pdf_proposal_url;
                                var imgUrl = sRoot.data.attributes.summary.latest_snapshot_url;

                                using (var webClient = new System.Net.WebClient())
                                {
                                    var content = webClient.DownloadData(pdfUrl);

                                    var FName = DateTime.UtcNow.Ticks + "_" + jobs.JobNumber + "_Pylon.pdf";
                                    var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(content, FName, "Pylon", jobs.Id, 16, AbpSession.TenantId);

                                    var document = new Document()
                                    {
                                        DocumentTypeId = 16, // Pylon
                                        JobId = jobs.Id,
                                        FileName = FName,
                                        FileType = "application/pdf",
                                        FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\" + filepath.JobNumber + "\\Pylon\\",
                                    };
                                    await _documentRepository.InsertAsync(document);

                                    LeadActivityLog leadactivity = new LeadActivityLog();
                                    leadactivity.SectionId = SectionId;
                                    leadactivity.ActionId = 17;
                                    leadactivity.ActionNote = "Pylon Document Updated.";
                                    leadactivity.LeadId = (int)jobs.LeadId;
                                    if (AbpSession.TenantId != null)
                                    {
                                        leadactivity.TenantId = (int)AbpSession.TenantId;
                                    }
                                    await _leadactivityRepository.InsertAsync(leadactivity);

                                    var pylonDocument = new PylonDocument()
                                    {
                                        JobId = jobs.Id,
                                        Url = imgUrl
                                    };
                                    await _pylonDocumentRepository.InsertAsync(pylonDocument);
                                }


                            }
                            else
                            {
                                throw new UserFriendlyException("File Not Found");
                            }
                        }
                        else
                        {
                            throw new UserFriendlyException("Design Not Found");
                        }
                    }
                    else
                    {
                        throw new UserFriendlyException("Job Not Found");
                    }

                }
                else
                {
                    throw new UserFriendlyException("Map Provider Not Found. Please Contact Your Admin.");
                }


                //JObject oJsonObject = new JObject();
                ////oJsonObject.Add("Username", "solarbridgeregistry");
                ////oJsonObject.Add("Password", "solarbridge123");
                //oJsonObject.Add("Username", organizationdetail.GreenBoatUsername);
                //oJsonObject.Add("Password", organizationdetail.GreenBoatPassword);
                //var oTaskPostAsync = await client.PostAsync("https://api.greenbot.com.au/api/Account/Login", new StringContent(oJsonObject.ToString(), Encoding.UTF8, "application/json"));
                //bool IsSuccess = oTaskPostAsync.IsSuccessStatusCode;
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }
        
        public async Task<string> GetRefNoForItme(int itemId, int qty)
        {
            var refNo = await _qldBatteryRebateRepository.GetAll().Where(e => e.ItemId == itemId && e.Qty == qty).Select(e => e.RefNo).FirstOrDefaultAsync();

            return refNo;
        }

        public async Task<List<GetActualCostOutput>> GetActualCostList(GetActualCostInput input)
        {
            decimal actualCost = 0;

            //var jobs = await _jobRepository.GetAsync(input.JobId);
            var jobs = await _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.Id == input.JobId).FirstOrDefaultAsync();

            var Output = new List<GetActualCostOutput>();

            List<string> DateType = new List<string>() { "Project Open", "First Deposite Date", "Deposite Received Date", "Active Date", "Install Book Date", "Install Complete Date" };

            foreach(var dateItem in DateType)
            {
                var CreationTime = new DateTime();

                if(dateItem == "First Deposite Date")
                {
                    if(jobs.FirstDepositDate != null)
                    {
                        CreationTime = (_timeZoneConverter.Convert(jobs.FirstDepositDate, (int)AbpSession.TenantId)).Value.Date;
                    }
                }
                else if (dateItem == "Deposite Received Date")
                {
                    if (jobs.DepositeRecceivedDate != null)
                    {
                        CreationTime = (_timeZoneConverter.Convert(jobs.DepositeRecceivedDate, (int)AbpSession.TenantId)).Value.Date;
                    }
                }
                else if (dateItem == "Active Date")
                {
                    if (jobs.ActiveDate != null)
                    {
                        CreationTime = (_timeZoneConverter.Convert(jobs.ActiveDate, (int)AbpSession.TenantId)).Value.Date;
                    }
                }
                else if (dateItem == "Install Book Date")
                {
                    if (jobs.InstallationDate != null)
                    {
                        CreationTime = (_timeZoneConverter.Convert(jobs.InstallationDate, (int)AbpSession.TenantId)).Value.Date;
                    }
                }
                else if (dateItem == "Install Complete Date")
                {
                    if (jobs.InstalledcompleteDate != null)
                    {
                        CreationTime = (_timeZoneConverter.Convert(jobs.InstalledcompleteDate, (int)AbpSession.TenantId)).Value.Date;
                    }
                }
                else
                {
                    CreationTime = (_timeZoneConverter.Convert(jobs.CreationTime, (int)AbpSession.TenantId)).Value.Date;

                }

                if(CreationTime.Year > 1)
                {
                    var noOfPanel = 0;
                    if (input.ProductItemList != null)
                    {
                        noOfPanel = input.ProductItemList.Where(e => e.ProductTypeId == 1).Sum(e => e.Qty == null ? 0 : (int)e.Qty);
                    }

                    #region Installation Cost
                    //var insCost = _stateWiseInstallationCostRepository.GetAll().Where(e => e.StateFk.Name == input.StateName).FirstOrDefault();
                    //decimal InstallationCost = 0;
                    //if (insCost != null)
                    //{
                    //    InstallationCost = input.AreaName == "Metro" ? (decimal)insCost.Metro : input.AreaName == "Regional" ? (decimal)insCost.Regional : 0;
                    //}

                    decimal actualInstallationCost = 0;
                    var InsInvoice = _installerInvoiceRepository.GetAll().Any(e => e.JobId == jobs.Id && e.Installation_Maintenance_Inspection == "Installation");
                    var installerInvoiceAmount = _installerInvoiceRepository.GetAll().Where(e => e.JobId == jobs.Id).Sum(e => e.Amount);
                    if ((installerInvoiceAmount == null || installerInvoiceAmount == 0) && InsInvoice == false)
                    {
                        var insCost = _postCodePriceRepository.GetAll().Where(e => e.PostCodePricePeriodFk.Month == CreationTime.Month && e.PostCodePricePeriodFk.Year == CreationTime.Year && e.PostCode == jobs.PostalCode).FirstOrDefault();
                        decimal InstallationCost = 0;
                        if (insCost != null)
                        {
                            InstallationCost = (decimal)insCost.Price;
                        }

                        if (input.SystemKw == null)
                        {
                            input.SystemKw = 0;
                        }
                        actualInstallationCost = InstallationCost * (decimal)input.SystemKw;
                    }
                    else
                    {
                        actualInstallationCost = Convert.ToDecimal(installerInvoiceAmount);
                    }
                    #endregion

                    #region Installation Product Item List
                    bool ItemVal = false;
                    decimal ProductItemCost = 0;

                    decimal panelCost = 0.0m;
                    decimal inverterCost = 0.0m;
                    decimal batteryCost = 0.0m;
                    decimal PricePerWatt = 0.0m;
                    if (input.ProductItemList != null)
                    {
                        foreach (var item in input.ProductItemList)
                        {
                            var ItemObj = _installationItemListRepository.GetAll().Where(e => e.ProductItemId == item.ProductItemId && e.InstallationItemPeriodFk.StartDate.Date <= CreationTime && e.InstallationItemPeriodFk.EndDate.Date >= CreationTime && e.InstallationItemPeriodFk.OrganizationUnit == jobs.LeadFk.OrganizationId).FirstOrDefault();

                            if (ItemObj != null)
                            {
                                var ItemCost = ItemObj.UnitPrice * (item.Qty == null ? 0 : item.Qty);

                                ProductItemCost += (decimal)ItemCost;

                                if ((decimal)ItemCost == 0)
                                {
                                    if (item.ProductTypeId == 1 || item.ProductTypeId == 2 || item.ProductTypeId == 5)
                                    {
                                        ItemVal = true;
                                    }
                                }

                                if (item.ProductTypeId == 1)
                                {
                                    PricePerWatt = ItemObj.PricePerWatt != null ? (decimal)ItemObj.PricePerWatt : 0.0m;
                                    panelCost = (decimal)ItemCost;
                                }
                                if (item.ProductTypeId == 2)
                                {
                                    inverterCost = (decimal)ItemCost;
                                }
                                if (item.ProductTypeId == 5)
                                {
                                    batteryCost = (decimal)ItemCost;
                                }
                            }
                            else
                            {
                                if (item.ProductTypeId == 1 || item.ProductTypeId == 2 || item.ProductTypeId == 5)
                                {
                                    ItemVal = true;
                                }
                            }


                        }
                    }
                    #endregion

                    #region Fixed Cost Price
                    decimal FixedCostPrice = 0;
                    var fixedCostPriceList = _fixedCostPriceRepository.GetAllList();

                    if (fixedCostPriceList != null)
                    {
                        foreach (var item in fixedCostPriceList)
                        {
                            if (item.Type == "Panel")
                            {
                                FixedCostPrice += (decimal)item.Cost * noOfPanel;
                            }
                            else if (item.Type == "Kw")
                            {
                                FixedCostPrice += (decimal)item.Cost * (decimal)input.SystemKw;
                            }
                            else if (item.Type == "Job")
                            {
                                FixedCostPrice += (decimal)item.Cost * 1;
                            }
                        }
                    }
                    #endregion

                    #region Extra Installation Charges
                    decimal ExtraInsCost = 0;
                    var extraInstallationCharges = _extraInstallationChargeRepository.GetAll().Where(e => e.StateFk.Name == input.StateName);

                    //House Type
                    decimal houseTypeCost = 0;
                    if (input.HouseType != null)
                    {
                        var houseType = extraInstallationCharges.Where(e => e.Category == "House Type" && e.NameId == input.HouseType).FirstOrDefault();

                        if (houseType != null)
                        {
                            var cost = input.AreaName == "Metro" ? (decimal)houseType.MetroAmount : input.AreaName == "Regional" ? (decimal)houseType.RegionalAmount : 0;

                            if (houseType.Type == "Panel")
                            {
                                houseTypeCost = cost * noOfPanel;
                            }
                            else if (houseType.Type == "Kw")
                            {
                                houseTypeCost = cost * (decimal)input.SystemKw;
                            }
                            else if (houseType.Type == "Job")
                            {
                                houseTypeCost = cost * 1;
                            }
                        }
                    }

                    //Roof Type
                    decimal roofTypeCost = 0;
                    if (input.RoofType != null)
                    {
                        var roofType = extraInstallationCharges.Where(e => e.Category == "Roof Type" && e.NameId == input.RoofType).FirstOrDefault();

                        if (roofType != null)
                        {
                            var cost = input.AreaName == "Metro" ? (decimal)roofType.MetroAmount : input.AreaName == "Regional" ? (decimal)roofType.RegionalAmount : 0;

                            if (roofType.Type == "Panel")
                            {
                                roofTypeCost = cost * noOfPanel;
                            }
                            else if (roofType.Type == "Kw")
                            {
                                roofTypeCost = cost * (decimal)input.SystemKw;
                            }
                            else if (roofType.Type == "Job")
                            {
                                roofTypeCost = cost * 1;
                            }
                        }
                    }

                    //Roof Angle
                    decimal roofAngleCost = 0;
                    if (input.RoofAngle != null)
                    {
                        var roofAngle = extraInstallationCharges.Where(e => e.Category == "Roof Angle" && e.NameId == input.RoofAngle).FirstOrDefault();

                        if (roofAngle != null)
                        {
                            var cost = input.AreaName == "Metro" ? (decimal)roofAngle.MetroAmount : input.AreaName == "Regional" ? (decimal)roofAngle.RegionalAmount : 0;

                            if (roofAngle.Type == "Panel")
                            {
                                roofAngleCost = cost * noOfPanel;
                            }
                            else if (roofAngle.Type == "Kw")
                            {
                                roofAngleCost = cost * (decimal)input.SystemKw;
                            }
                            else if (roofAngle.Type == "Job")
                            {
                                roofAngleCost = cost * 1;
                            }
                        }
                    }

                    //Price Variation
                    decimal variationCost = 0;
                    if (input.VariationList != null)
                    {
                        foreach (var item in input.VariationList)
                        {
                            var variation = extraInstallationCharges.Where(e => e.Category == "Price Variation" && e.NameId == item).FirstOrDefault();
                            if (variation != null)
                            {
                                var cost = input.AreaName == "Metro" ? (decimal)variation.MetroAmount : input.AreaName == "Regional" ? (decimal)variation.RegionalAmount : 0;

                                if (variation.Type == "Panel")
                                {
                                    variationCost += cost * noOfPanel;
                                }
                                else if (variation.Type == "Kw")
                                {
                                    variationCost += cost * (decimal)input.SystemKw;
                                }
                                else if (variation.Type == "Job")
                                {
                                    variationCost += cost * 1;
                                }
                            }
                        }
                    }

                    ExtraInsCost = (houseTypeCost + roofTypeCost + roofAngleCost + variationCost);
                    #endregion

                    #region Bettery Installation Cost
                    decimal BetteryInstallationCost = 0;

                    decimal? batterySize = 0;
                    var noOfBattery = 0;
                    if (input.ProductItemList != null)
                    {
                        batterySize = input.ProductItemList.Where(e => e.ProductTypeId == 5).Sum(e => e.Size);
                        noOfBattery = input.ProductItemList.Where(e => e.ProductTypeId == 5).Sum(e => e.Qty == null ? 0 : (int)e.Qty);
                    }

                    if (batterySize != null && batterySize != 0)
                    {
                        batterySize = batterySize * noOfBattery;
                        var btryCost = _batteryInstallationCostRepository.GetAll().FirstOrDefault();

                        if (btryCost != null)
                        {
                            var fixCost = btryCost.FixCost;

                            decimal extraCost = 0;
                            if (batterySize > btryCost.Kw)
                            {
                                extraCost = ((decimal)batterySize - btryCost.Kw) * btryCost.ExtraCost;
                            }

                            BetteryInstallationCost = fixCost + extraCost;
                        }
                    }
                    #endregion

                    #region Travel Cost
                    decimal ActualTravelCost = 0;
                    //if(input.Distance != null)
                    //{
                    //    var travelCostList = _travelCostRepository.GetAll().OrderBy(e => e.StartKm).ToList();

                    //    foreach (var item in travelCostList)
                    //    {
                    //        if(input.Distance > 0)
                    //        {

                    //        }
                    //    }
                    //}
                    #endregion

                    #region STC Value
                    decimal StcCostPrice = 0;
                    var STCCost = _stcCostRepository.GetAll().FirstOrDefault();
                    if (STCCost != null)
                    {
                        StcCostPrice = STCCost.Cost * (input.STC == null ? 0 : (decimal)input.STC);
                    }
                    #endregion

                    #region Finace Payment Type
                    var finType = _financeOptionRepository.GetAll().Where(e => e.Id == input.FinPaymentType).FirstOrDefault();
                    decimal percentageValue = 0;
                    var totalVal = input.TotalCost != null ? Convert.ToDecimal(input.TotalCost) : 0;
                    if (finType != null)
                    {
                        var per = finType.Percentage != null ? Convert.ToDecimal(finType.Percentage) : 0;
                        if (per != 0 && totalVal != 0)
                        {
                            percentageValue = (totalVal * per) / 100;
                        }
                    }
                    #endregion

                    actualCost = (actualInstallationCost + ProductItemCost + FixedCostPrice + ExtraInsCost + BetteryInstallationCost + ActualTravelCost + percentageValue) - StcCostPrice;

                    if (ItemVal == true)
                    {
                        actualCost = 0;
                    }

                    if (actualInstallationCost == 0 && BetteryInstallationCost == 0)
                    {
                        actualCost = 0;
                    }

                    actualCost = actualCost - (input.CategoryDDiscountAmt != null ? (decimal)input.CategoryDDiscountAmt : 0);

                    var np = input.TotalCost - actualCost;
                    var np_sys = panelCost != 0 && inverterCost != 0 && input.SystemKw > 0 ? np / input.SystemKw : 0;

                    var _category = _categoryInstallationItemListRepository.GetAll().Where(e => e.CategoryInstallationItemPeriodFk.StartDate.Date <= CreationTime && e.CategoryInstallationItemPeriodFk.EndDate >= CreationTime && e.CategoryInstallationItemPeriodFk.OrganizationUnit == jobs.LeadFk.OrganizationId && e.StartValue <= np_sys && e.EndValue >= np_sys).FirstOrDefault();

                    var ActualCostOutput = new GetActualCostOutput
                    {
                        ActualCost = actualCost,
                        Category = (_category != null ? _category.CategoryName : ""),
                        PanelCost = panelCost,
                        PricePerWatt = PricePerWatt,
                        InverterCost = inverterCost,
                        BatteryCost = batteryCost,
                        BetteryInstallationCost = BetteryInstallationCost,
                        InstallationCost = actualInstallationCost,
                        Datetype = dateItem
                    };
                    Output.Add(ActualCostOutput);
                }

                
            }

            return Output;
        }

        public async Task<JobAwaitingActiveSatusDto> GetJobAwaitingActiveSatus( int Id)
        {
            var Result = new JobAwaitingActiveSatusDto();
            var job = _jobRepository.GetAll().Where(e => e.Id ==Id).FirstOrDefault();

            ///// deposite received
            Result.Housetype = job.HouseTypeId != null;
            Result.Elecdistributor = job.ElecDistributorId != null;
            Result.Roofangle = job.RoofAngleId != null;
            Result.Rooftype = job.RoofTypeId != null;
            Result.Quote = _quotationRepository.GetAll().Where(e => e.JobId == Id).Any();
            Result.Invoice = _invoicePaymentRepository.GetAll().Where(e => e.JobId == Id).Any();
            Result.Productdetail = _jobProductItemRepository.GetAll().Where(e => e.JobId == Id).Any();
            Result.paymenttype = _invoicePaymentRepository.GetAll().Where(e => e.JobId == Id && (e.InvoicePaymentMethodFk.PaymentMethod == "EFT" || e.InvoicePaymentMethodFk.PaymentMethod == "Credit Card")).Any() ? true : false;
            Result.Paymentverified = _invoicePaymentRepository.GetAll().Where(e => e.JobId == Id && e.IsVerified == true).Any() == true ? true : false;
            Result.payementoptionid = job.PaymentOptionId;
            Result.Expirydate = job.ExpiryDate == null ? false : true;
            Result.Solarrebatestatus = (job.SolarRebateStatus == 0 || job.SolarRebateStatus == 0 || job.SolarRebateStatus != 17) && job.VicRebate != "With Rebate Install" ? false : true;
            Result.Vicrebate = job.VicRebate;
            Result.State = job.State;
            Result.Finance = job.PaymentOptionId == 1 ? true : job.FinanceDocumentVerified == 1 ? true : false;
            Result.Bonusadded = (job.Bonus == true  && job.BonusAmt.Value > 0);
            ///// Application
            Result.Meterphase = _lookup_meterPhaseRepository.GetAll().Where(e => e.Id == job.MeterPhaseId).Any();
            Result.Nminumber = !string.IsNullOrEmpty(job.NMINumber);
            Result.Peakmeterno = !string.IsNullOrEmpty(job.PeakMeterNo);
            Result.Doclist = _documentRepository.GetAll().Where(e => e.JobId == job.Id).Any();

            ///// Application
            Result.Approvedreferencenumber = !string.IsNullOrEmpty(job.ApplicationRefNo);
            Result.Distapprovedate = job.DistApproveDate != null ? true : false;
            Result.Meterupgrade = job.MeterUpgradeId != null;
            Result.Meterboxphoto = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 3 && e.JobId == job.Id).Any();
            Result.Signedquote = _quotationRepository.GetAll().Where(e => e.JobId == job.Id && e.IsSigned == true).Any() || _documentRepository.GetAll().Where(e => e.JobId == job.Id && e.DocumentTypeId == 5).Any();
            Result.Deposit = _invoicePaymentRepository.GetAll().Where(e => e.JobId == Id).Any();
            Result.Elecretailer = job.ElecRetailerId != null;
            Result.Lastquotesigned = _quotationRepository.GetAll().Where(e => e.JobId == Id).Any() ? _quotationRepository.GetAll().Where(e => e.JobId == Id).OrderByDescending(e => e.Id).FirstOrDefault().IsSigned : true;

            Result.Exportcontrolformsigned = _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Export Control").Any() ? _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Export Control").OrderByDescending(e => e.Id).FirstOrDefault().IsSigned : true; ;
            Result.Feedintariffsigned = _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Feed In Tariff").Any() ? _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Feed In Tariff").OrderByDescending(e => e.Id).FirstOrDefault().IsSigned : true;
            Result.Shadingdeclarationsigned = _declarationFormRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Shading Declaration").Any() ? _declarationFormRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Shading Declaration").OrderByDescending(e => e.Id).FirstOrDefault().IsSigned : true;
            Result.Efficiencydeclarationsigned = _declarationFormRepqository.GetAll().Where(e => e.JobId ==Id && e.DocType == "Efficiency Declaration").Any() ? _declarationFormRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Efficiency Declaration").OrderByDescending(e => e.Id).FirstOrDefault().IsSigned : true;
            Result.Exportcontrolform = _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Export Control").Any();
            Result.Feedintariff = _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Feed In Tariff").Any();
            Result.Shadingdeclaration = _declarationFormRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Shading Declaration").Any();
            Result.Efficiencydeclaration = _declarationFormRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Efficiency Declaration").Any();
            if (!Result.Lastquotesigned && _quotationRepository.GetAll().Where(e => e.JobId == Id).Any())
            {
                var quoteId = await _lookup_documentTypeRepository.GetAll().Where(e => e.Title == "Signed Quote").Select(e => e.Id).FirstOrDefaultAsync();
                var quoteDate = _quotationRepository.GetAll().Where(e => e.JobId == Id).OrderByDescending(e => e.Id).FirstOrDefault().CreationTime;

                if (_documentRepository.GetAll().Where(e => e.DocumentTypeId == quoteId && e.CreationTime >= quoteDate && e.JobId == Id).Any())
                {
                    Result.Lastquotesigned = true;
                }
            }

            if (!Result.Exportcontrolformsigned && _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Export Control").Any())
            {
                var exportControlId = await _lookup_documentTypeRepository.GetAll().Where(e => e.Title == "Export Control Form").Select(e => e.Id).FirstOrDefaultAsync();

                var exportControlDate = _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Export Control").OrderByDescending(e => e.Id).FirstOrDefault().CreationTime;

                if (_documentRepository.GetAll().Where(e => e.DocumentTypeId == exportControlId && e.CreationTime >= exportControlDate && e.JobId == Id).Any())
                {
                    Result.Exportcontrolformsigned = true;
                }
            }

            if (!Result.Feedintariffsigned && _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Feed In Tariff").Any())
            {
                var feedInTariffId = await _lookup_documentTypeRepository.GetAll().Where(e => e.Title == "Feed In Tariff").Select(e => e.Id).FirstOrDefaultAsync();

                var feedInTariffDate = _jobAcknowledgementRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Feed In Tariff").OrderByDescending(e => e.Id).FirstOrDefault().CreationTime;

                if (_documentRepository.GetAll().Where(e => e.DocumentTypeId == feedInTariffId && e.CreationTime >= feedInTariffDate && e.JobId == Id).Any())
                {
                    Result.Feedintariffsigned = true;
                }
            }

            if (!Result.Shadingdeclarationsigned && _declarationFormRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Shading Declaration").Any())
            {
                var shadingDeclarationId = await _lookup_documentTypeRepository.GetAll().Where(e => e.Title == "Shading Declaration").Select(e => e.Id).FirstOrDefaultAsync();

                var shadingDeclarationDate = _declarationFormRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Shading Declaration").OrderByDescending(e => e.Id).FirstOrDefault().CreationTime;

                if (_documentRepository.GetAll().Where(e => e.DocumentTypeId == shadingDeclarationId && e.CreationTime >= shadingDeclarationDate && e.JobId == Id).Any())
                {
                    Result.Shadingdeclarationsigned = true;
                }
            }

            if (!Result.Efficiencydeclarationsigned && _declarationFormRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Efficiency Declaration").Any())
            {
                var efficiencyDeclarationId = await _lookup_documentTypeRepository.GetAll().Where(e => e.Title == "Efficiency Declaration").Select(e => e.Id).FirstOrDefaultAsync();

                var efficiencyDeclarationDate = _declarationFormRepqository.GetAll().Where(e => e.JobId == Id && e.DocType == "Efficiency Declaration").OrderByDescending(e => e.Id).FirstOrDefault().CreationTime;

                if (_documentRepository.GetAll().Where(e => e.DocumentTypeId == efficiencyDeclarationId && e.CreationTime >= efficiencyDeclarationDate && e.JobId == Id).Any())
                {
                    Result.Efficiencydeclarationsigned = true;
                }
            }

            return Result;
        }

        public async Task<DisplayNameStatusResultDto> GetDisplayNameStatuses(EntityDto input)
        {

            var depositResult = new List<DisplayNameStatusDto>();
            var activeResult = new List<DisplayNameStatusDto>();
            var applicationResult = new List<DisplayNameStatusDto>();


            var depositTitles = await (from item in _checkdepositeRepository.GetAll().Where(e => e.IsActive)
                                       select new CommonLookupDto
                                       {
                                           Id = item.Id,
                                           DisplayName = item.Name,
                                       }).ToListAsync();


            var activeTitles = await (from item in _checkActiveRepository.GetAll().Where(e => e.IsActive)
                                      select new CommonLookupDto
                                      {
                                          Id = item.Id,
                                          DisplayName = item.Name,
                                      }).ToListAsync();


            var applicationTitles = await (from item in _checkApplicationRepository.GetAll().Where(e => e.IsActive)
                                           select new CommonLookupDto
                                           {
                                               Id = item.Id,
                                               DisplayName = item.Name,
                                           }).ToListAsync();


            var jobStatuses = await GetJobAwaitingActiveSatus(input.Id);

            if (jobStatuses.Efficiencydeclaration == false)
            {
                activeTitles = activeTitles.Where(title => title.DisplayName != "Efficiency Declaration Signed").ToList();

            }
            if (jobStatuses.Shadingdeclaration == false)
            {
                activeTitles = activeTitles.Where(title => title.DisplayName != "Shading Declaration Signed").ToList();

            }
            if (jobStatuses.Feedintariff == false)
            {
                activeTitles = activeTitles.Where(title => title.DisplayName != "Feed In Tariff Signed").ToList();

            }
            if (jobStatuses.Exportcontrolform == false)
            {
                activeTitles = activeTitles.Where(title => title.DisplayName != "Export Control Form Signed").ToList();

            }
            if (jobStatuses.Quote == false)
            {
                activeTitles = activeTitles.Where(title => title.DisplayName != "Last Quote Signed").ToList();

            }
            if (jobStatuses.paymenttype == false && jobStatuses.payementoptionid != 1)
            {

                activeTitles = activeTitles.Where(title => title.DisplayName != "Payment Verified").ToList();
            }

            if (jobStatuses.State != "VIC" && jobStatuses.Vicrebate != "With Rebate Install")
            {

                activeTitles = activeTitles.Where(title => title.DisplayName != "Solar Rebate Status").ToList();

            }
            if ( jobStatuses.payementoptionid == 1)

            {

                activeTitles = activeTitles.Where(title => title.DisplayName != "Finance").ToList();
            }
            if (jobStatuses.State != "VIC")
            {

                activeTitles = activeTitles.Where(title => title.DisplayName != "Expiry Date").ToList();

            }

            foreach (var title in depositTitles)
            {
                var propertyName = char.ToUpper(title.DisplayName.Trim().Replace(" ", "")[0]) + title.DisplayName.Trim().Replace(" ", "").Substring(1).ToLower();
                bool isActive = jobStatuses.GetType().GetProperty(propertyName)?.GetValue(jobStatuses) is bool status && status;

                depositResult.Add(new DisplayNameStatusDto
                {
                    DisplayName = title.DisplayName,
                    IsActive = isActive
                });
            }

            foreach (var title in activeTitles)
            {
                var propertyName = char.ToUpper(title.DisplayName.Trim().Replace(" ", "")[0]) + title.DisplayName.Trim().Replace(" ", "").Substring(1).ToLower();
                bool isActive = jobStatuses.GetType().GetProperty(propertyName)?.GetValue(jobStatuses) is bool status && status;

                activeResult.Add(new DisplayNameStatusDto
                {
                    DisplayName = title.DisplayName,
                    IsActive = isActive
                });
            }


            foreach (var title in applicationTitles)
            {
                var propertyName = char.ToUpper(title.DisplayName.Trim().Replace(" ", "")[0]) + title.DisplayName.Trim().Replace(" ", "").Substring(1).ToLower();
                bool isActive = jobStatuses.GetType().GetProperty(propertyName)?.GetValue(jobStatuses) is bool status && status;

                applicationResult.Add(new DisplayNameStatusDto
                {
                    DisplayName = title.DisplayName,
                    IsActive = isActive
                });
            }


            return new DisplayNameStatusResultDto
            {
                DepositStatuses = depositResult,
                ActiveStatuses = activeResult,
                ApplicationStatuses = applicationResult
            };
        }

        public async Task RevertNote(int id)
        {
            var jobType = await _jobRepository.FirstOrDefaultAsync((int)id);
            if (AbpSession.TenantId != null)
            {
                jobType.TenantId = (int)AbpSession.TenantId;
            }

            jobType.ReferralAmount = null;
           
            jobType.BsbNo = null;
            jobType.AccountNo = null;
            jobType.AccountName = null;
            await _jobRepository.UpdateAsync(jobType);
        }

        public async Task<FileDto> getApplicationFeeTrackerToExcel(GetAllJobApplicationFeeTrackerExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var Today = _timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var actvitylog_list = _leadactivityRepository.GetAll().Where(e => e.SectionId == 1 && e.LeadFk.OrganizationId == input.OrganizationUnit);
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            var Doclist = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 2).Select(e => e.JobId).ToList();
            var filteredResult = _jobRepository.GetAll()
                             .Include(e => e.LeadFk)
                             .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                             .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                             .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                             .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.JobStatusId != 3 && e.SolarRebateStatus != 3 && e.FirstDepositDate != null)
                             .AsNoTracking()
                             .Select(e => new { e.Id, e.JobNumber, e.LeadFk, e.State, e.Applicationfeespaid, e.PaidAmmount, e.Paidby, e.PaidStatus, e.InvRefNo ,e.InvPaidDate, e.ElecDistributorId, e.JobStatusId, e.JobTypeId, e.DepositeRecceivedDate, e.DistApplied, e.ActiveDate, e.ApplicationRefNo, e.MeterPhaseId, e.NMINumber, e.PeakMeterNo, e.DistExpiryDate, e.JobTypeFk, e.JobStatusFk, e.Address, e.Suburb, e.PostalCode, e.ElecDistributorFk, e.DistApproveDate, e.SmsSend, e.SmsSendDate, e.EmailSend, e.EmailSendDate, e.LeadId, e.FirstDepositDate,e.InstallationDate });

            //var jobidstatus = input.testfilter != null ? input.testfilter : null;
            var filter = filteredResult
                         .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                         .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                         .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                         .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                         .WhereIf(input.FilterName == "ApplicationRefNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ApplicationRefNo == input.Filter)
                         .WhereIf(input.FilterName == "NMINumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.NMINumber == input.Filter)
                         .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
                         .WhereIf(input.ElecDistributorId != 0, e => e.ElecDistributorId == input.ElecDistributorId)
                         .WhereIf(input.JobStatusIDFilter != null && input.JobStatusIDFilter.Count() > 0, e => input.JobStatusIDFilter.Contains((int)e.JobStatusId))
                         .WhereIf(input.JobTypeId != 0, e => e.JobTypeId == input.JobTypeId)
                         .WhereIf(input.PaidStatus != 0, e => e.PaidStatus == input.PaidStatus)
                         .WhereIf(input.FeesPaid != 0, e => e.Applicationfeespaid == input.FeesPaid)
                         .WhereIf(input.PaidById != 0, e => e.Paidby == input.PaidById)
                         .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                         .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                         .WhereIf(input.DateType == "ApplicationDate" && input.StartDate != null, e => e.DistApplied.Value.Date >= SDate.Value.Date)
                         .WhereIf(input.DateType == "ApplicationDate" && input.EndDate != null, e => e.DistApplied.Value.Date <= EDate.Value.Date)

                         .WhereIf(input.DateType == "InstallationDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                         .WhereIf(input.DateType == "InstallationDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)

                         .WhereIf(input.DateType == "ActiveDate" && input.StartDate != null, e => e.ActiveDate.Value.Date >= SDate.Value.Date)
                         .WhereIf(input.DateType == "ActiveDate" && input.EndDate != null, e => e.ActiveDate.Value.Date <= EDate.Value.Date)

                         .WhereIf(input.ApplicationStatus == 1, e => e.ApplicationRefNo == null
                         && (_documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() == false || e.MeterPhaseId == null || e.NMINumber == null || e.PeakMeterNo == null)) //Application Awaiting

                         .WhereIf(input.ApplicationStatus == 2, e => e.ApplicationRefNo == null
                         && _documentRepository.GetAll().AsNoTracking().Where(j => j.JobId == e.Id).Any() && e.MeterPhaseId != null && e.NMINumber == null && e.PeakMeterNo != null) //Application Pending

                         .WhereIf(input.ApplicationStatus == 3, e => e.ApplicationRefNo != null) //Application Complete

                         .WhereIf(input.ApplicationStatus == 4, e => e.ApplicationRefNo != null && (e.DistExpiryDate.Value.AddDays(-30) < Today) && e.JobStatusId < 6) //Application Expiry
                         ;

            //var pagedAndFilteredResult = filter
            //    .OrderBy(input.Sorting ?? "id desc")
            //    .PageBy(input);
            var activity = _leadactivityRepository.GetAll().AsNoTracking().Where(e => e.SectionId == 1).Select(e => new { e.ActionId, e.LeadId, e.Id, e.ActivityDate, e.ActivityNote });

            var results = from o in filter
                          select new GetViewApplicationFeeTrackerDto()
                          {
                              Id = o.Id,
                              LeadId = o.LeadFk.Id,
                              JobNumber = o.JobNumber,
                              JobType = o.JobTypeFk.Name,
                              JobStatus = o.JobStatusFk.Name,
                              JobStatusColorClass = o.JobStatusFk.ColorClass,
                              CompanyName = o.LeadFk.CompanyName,
                              Address = o.Address,
                              Suburb = o.Suburb,
                              State = o.State,
                              PostalCode = o.PostalCode,
                              ElecDistributor = o.ElecDistributorFk.Name,

                              ApplicationFeesPaid = o.Applicationfeespaid == 1 ? "Yes" : (o.Applicationfeespaid == 0 ? "No" : ""),
                              PaidAmount = o.PaidAmmount,
                              PaidBy = o.Paidby == 1 ? "Customer" : (o.Paidby == 2 ? "Company" : ""),
                              PaidStatus = o.PaidStatus == 1 ? "Paid" : (o.PaidStatus == 2 ? "Pending" : ""),
                              InvRefNo = o.InvRefNo,
                              InvVerifyDate = o.InvPaidDate,

                              SmsSend = o.SmsSend,
                              SmsSendDate = o.SmsSendDate,
                              EmailSend = o.EmailSend,
                              EmailSendDate = o.EmailSendDate,
                             
                              NextFollowup = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                              Discription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              Comment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                             
                              IsExpired = (o.ApplicationRefNo != null && (o.DistExpiryDate.Value.AddDays(-30) < Today) && o.JobStatusId < 6) ? true : false
                          };
            var applicationFeetrackerListDtos = await results.ToListAsync();

            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _leadsExcelExporter.ApplicationFeeTrackerExportToFile(applicationFeetrackerListDtos);
            }
            else
            {
                return _leadsExcelExporter.ApplicationFeeTrackerExportToFile(applicationFeetrackerListDtos);
            }
        }

        public async Task UpdateGWTID(int jobid, string GWTID)
        {
            try
            {
                

                var jobdetail = _jobRepository.GetAll().Where(e => e.Id == jobid).FirstOrDefault();
                jobdetail.GWTId = GWTID;
                await _jobRepository.UpdateAsync(jobdetail);



            }
            catch (Exception e)
            {
                
                throw new UserFriendlyException(e.Message);
            }
        }
    }
}