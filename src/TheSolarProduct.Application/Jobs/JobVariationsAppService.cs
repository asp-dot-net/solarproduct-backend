﻿using TheSolarProduct.Jobs;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace TheSolarProduct.Jobs
{
    //[AbpAuthorize(AppPermissions.Pages_JobVariations, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    [AbpAuthorize(AppPermissions.Pages)]
    public class JobVariationsAppService : TheSolarProductAppServiceBase, IJobVariationsAppService
	{
		private readonly IRepository<JobVariation> _jobVariationRepository;
		private readonly IRepository<Variation, int> _lookup_variationRepository;
		private readonly IRepository<Job, int> _lookup_jobRepository;


		public JobVariationsAppService(IRepository<JobVariation> jobVariationRepository, IRepository<Variation, int> lookup_variationRepository, IRepository<Job, int> lookup_jobRepository)
		{
			_jobVariationRepository = jobVariationRepository;
			_lookup_variationRepository = lookup_variationRepository;
			_lookup_jobRepository = lookup_jobRepository;

		}

		public async Task<PagedResultDto<GetJobVariationForViewDto>> GetAll(GetAllJobVariationsInput input)
		{

			var filteredJobVariations = _jobVariationRepository.GetAll()
						.Include(e => e.VariationFk)
						.Include(e => e.JobFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
						.WhereIf(!string.IsNullOrWhiteSpace(input.VariationNameFilter), e => e.VariationFk != null && e.VariationFk.Name == input.VariationNameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.JobNoteFilter), e => e.JobFk != null && e.JobFk.Note == input.JobNoteFilter)
						.WhereIf(input.JobId > 0, e => e.JobId == input.JobId);

			var pagedAndFilteredJobVariations = filteredJobVariations
				.OrderBy(input.Sorting ?? "id desc")
				.PageBy(input);

			var jobVariations = from o in pagedAndFilteredJobVariations
								join o1 in _lookup_variationRepository.GetAll() on o.VariationId equals o1.Id into j1
								from s1 in j1.DefaultIfEmpty()

								join o2 in _lookup_jobRepository.GetAll() on o.JobId equals o2.Id into j2
								from s2 in j2.DefaultIfEmpty()

								select new GetJobVariationForViewDto()
								{
									JobVariation = new JobVariationDto
									{
										Id = o.Id,
										Cost = o.Cost
									},
									VariationName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
									JobNote = s2 == null || s2.Note == null ? "" : s2.Note.ToString()
								};

			var totalCount = await filteredJobVariations.CountAsync();

			return new PagedResultDto<GetJobVariationForViewDto>(
				totalCount,
				await jobVariations.ToListAsync()
			);
		}
		public async Task<List<GetJobVariationForEditOutput>> GetJobVariationByJobId(int jobid)
		{
			var jobVariation = _jobVariationRepository.GetAll().Where(x => x.JobId == jobid).ToList();

			var output = new List<GetJobVariationForEditOutput>();

			foreach (var item in jobVariation)
			{
				var outobj = new GetJobVariationForEditOutput();
				outobj.JobVariation = ObjectMapper.Map<CreateOrEditJobVariationDto>(item);
				outobj.JobVariation.Id = item.Id;
				outobj.JobVariation.Notes = item.Notes;
				if (outobj.JobVariation.VariationId != null)
				{
					var _lookupVariation = await _lookup_variationRepository.FirstOrDefaultAsync((int)outobj.JobVariation.VariationId);
					outobj.VariationName = _lookupVariation?.Name?.ToString();
				}

				if (outobj.JobVariation.JobId != null)
				{
					var _lookupJob = await _lookup_jobRepository.FirstOrDefaultAsync((int)outobj.JobVariation.JobId);
					outobj.JobNote = _lookupJob?.Note?.ToString();
				}
				output.Add(outobj);
			}

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_JobVariations_Edit)]
		public async Task<GetJobVariationForEditOutput> GetJobVariationForEdit(EntityDto input)
		{
			var jobVariation = await _jobVariationRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetJobVariationForEditOutput { JobVariation = ObjectMapper.Map<CreateOrEditJobVariationDto>(jobVariation) };

			if (output.JobVariation.VariationId != null)
			{
				var _lookupVariation = await _lookup_variationRepository.FirstOrDefaultAsync((int)output.JobVariation.VariationId);
				output.VariationName = _lookupVariation?.Name?.ToString();
			}

			if (output.JobVariation.JobId != null)
			{
				var _lookupJob = await _lookup_jobRepository.FirstOrDefaultAsync((int)output.JobVariation.JobId);
				output.JobNote = _lookupJob?.Note?.ToString();
			}

			return output;
		}



		public async Task CreateOrEdit(CreateOrEditJobVariationDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_JobVariations_Create)]
		protected virtual async Task Create(CreateOrEditJobVariationDto input)
		{
			var jobVariation = ObjectMapper.Map<JobVariation>(input);


			if (AbpSession.TenantId != null)
			{
				jobVariation.TenantId = (int)AbpSession.TenantId;
			}


			await _jobVariationRepository.InsertAsync(jobVariation);
		}

		[AbpAuthorize(AppPermissions.Pages_JobVariations_Edit)]
		protected virtual async Task Update(CreateOrEditJobVariationDto input)
		{
			var jobVariation = await _jobVariationRepository.FirstOrDefaultAsync((int)input.Id);
			ObjectMapper.Map(input, jobVariation);
		}

		[AbpAuthorize(AppPermissions.Pages_JobVariations_Delete)]
		public async Task Delete(EntityDto input)
		{
			await _jobVariationRepository.DeleteAsync(input.Id);
		}

		public async Task<List<JobVariationVariationLookupTableDto>> GetAllVariationForTableDropdown()
		{
			return await _lookup_variationRepository.GetAll()
				.Select(variation => new JobVariationVariationLookupTableDto
				{
					Id = variation.Id,
					DisplayName = variation == null || variation.Name == null ? "" : variation.Name.ToString(),
					ActionName = variation == null || variation.Action == null ? "" : variation.Action.ToString()
				}).ToListAsync();
		}

		//public async Task<List<JobVariationJobLookupTableDto>> GetAllJobForTableDropdown()
		//{
		//	return await _lookup_jobRepository.GetAll()
		//		.Select(job => new JobVariationJobLookupTableDto
		//		{
		//			Id = job.Id,
		//			DisplayName = job == null || job.Note == null ? "" : job.Note.ToString()
		//		}).ToListAsync();
		//}

	}
}