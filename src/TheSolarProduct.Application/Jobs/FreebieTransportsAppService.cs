﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.UI;
using TheSolarProduct.Storage;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.SmsTemplates;

namespace TheSolarProduct.Jobs
{
    [AbpAuthorize(AppPermissions.Pages_FreebieTransports)]
    public class FreebieTransportsAppService : TheSolarProductAppServiceBase, IFreebieTransportsAppService
    {
        private readonly IRepository<FreebieTransport> _freebieTransportRepository;
        private readonly IFreebieTransportsExcelExporter _freebieTransportsExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public FreebieTransportsAppService(IRepository<FreebieTransport> freebieTransportRepository, IFreebieTransportsExcelExporter freebieTransportsExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _freebieTransportRepository = freebieTransportRepository;
            _freebieTransportsExcelExporter = freebieTransportsExcelExporter;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task<PagedResultDto<GetFreebieTransportForViewDto>> GetAll(GetAllFreebieTransportsInput input)
        {

            var filteredFreebieTransports = _freebieTransportRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var pagedAndFilteredFreebieTransports = filteredFreebieTransports
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var freebieTransports = from o in pagedAndFilteredFreebieTransports
                                    select new
                                    {

                                        o.Name,
                                        Id = o.Id,
                                        o.TransportLink,
                                        o.IsActive,
                                    };

            var totalCount = await filteredFreebieTransports.CountAsync();

            var dbList = await freebieTransports.ToListAsync();
            var results = new List<GetFreebieTransportForViewDto>();

            foreach (var o in dbList)
            {
                var res = new GetFreebieTransportForViewDto()
                {
                    FreebieTransport = new FreebieTransportDto
                    {

                        Name = o.Name,
                        Id = o.Id,
                        TransportLink = o.TransportLink,
                        IsActive = o.IsActive,
                    }
                };

                results.Add(res);
            }

            return new PagedResultDto<GetFreebieTransportForViewDto>(
                totalCount,
                results
            );

        }

        public async Task<GetFreebieTransportForViewDto> GetFreebieTransportForView(int id)
        {
            var freebieTransport = await _freebieTransportRepository.GetAsync(id);

            var output = new GetFreebieTransportForViewDto { FreebieTransport = ObjectMapper.Map<FreebieTransportDto>(freebieTransport) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_FreebieTransports_Edit)]
        public async Task<GetFreebieTransportForEditOutput> GetFreebieTransportForEdit(EntityDto input)
        {
            var freebieTransport = await _freebieTransportRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetFreebieTransportForEditOutput { FreebieTransport = ObjectMapper.Map<CreateOrEditFreebieTransportDto>(freebieTransport) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditFreebieTransportDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_FreebieTransports_Create)]
        protected virtual async Task Create(CreateOrEditFreebieTransportDto input)
        {
            var freebieTransport = ObjectMapper.Map<FreebieTransport>(input);

            if (AbpSession.TenantId != null)
            {
                freebieTransport.TenantId = (int)AbpSession.TenantId;
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 28;
            dataVaultLog.ActionNote = "Freebie Transports Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _freebieTransportRepository.InsertAsync(freebieTransport);

        }

        [AbpAuthorize(AppPermissions.Pages_FreebieTransports_Edit)]
        protected virtual async Task Update(CreateOrEditFreebieTransportDto input)
        {
            var freebieTransport = await _freebieTransportRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 28;
            dataVaultLog.ActionNote = "Freebie Transports Updated : " + freebieTransport.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != freebieTransport.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = freebieTransport.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != freebieTransport.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = freebieTransport.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.TransportLink != freebieTransport.TransportLink)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "TransportLink";
                history.PrevValue = freebieTransport.TransportLink;
                history.CurValue = input.TransportLink;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, freebieTransport);

        }

        [AbpAuthorize(AppPermissions.Pages_FreebieTransports_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _freebieTransportRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 28;
            dataVaultLog.ActionNote = "Freebie Transports Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _freebieTransportRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetFreebieTransportsToExcel(GetAllFreebieTransportsForExcelInput input)
        {

            var filteredFreebieTransports = _freebieTransportRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredFreebieTransports
                         select new GetFreebieTransportForViewDto()
                         {
                             FreebieTransport = new FreebieTransportDto
                             {
                                 Name = o.Name,
                                 Id = o.Id,
                                 TransportLink = o.TransportLink,
                                 IsActive = o.IsActive,

                             }
                         });

            var freebieTransportListDtos = await query.ToListAsync();

            return _freebieTransportsExcelExporter.ExportToFile(freebieTransportListDtos);
        }

    }
}