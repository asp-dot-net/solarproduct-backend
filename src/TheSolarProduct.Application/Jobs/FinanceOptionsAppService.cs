﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.Jobs
{
    [AbpAuthorize(AppPermissions.Pages_FinanceOptions, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    public class FinanceOptionsAppService : TheSolarProductAppServiceBase, IFinanceOptionsAppService
    {
        private readonly IRepository<FinanceOption> _financeOptionRepository;
        private readonly IFinanceOptionsExcelExporter _financeOptionsExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public FinanceOptionsAppService(IRepository<FinanceOption> financeOptionRepository, IFinanceOptionsExcelExporter financeOptionsExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _financeOptionRepository = financeOptionRepository;
            _financeOptionsExcelExporter = financeOptionsExcelExporter;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
        }

        public async Task<PagedResultDto<GetFinanceOptionForViewDto>> GetAll(GetAllFinanceOptionsInput input)
        {
            var filteredFinanceOptions = _financeOptionRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

            var pagedAndFilteredFinanceOptions = filteredFinanceOptions
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var financeOptions = from o in pagedAndFilteredFinanceOptions
                                 select new GetFinanceOptionForViewDto()
                                 {
                                     FinanceOption = new FinanceOptionDto
                                     {
                                         Name = o.Name,
                                         Id = o.Id,
                                         DisplayOrder = o.DisplayOrder,
                                         Percentage = o.Percentage,
                                         IsActive = o.IsActive,
                                     }
                                 };

            var totalCount = await filteredFinanceOptions.CountAsync();

            return new PagedResultDto<GetFinanceOptionForViewDto>(
                totalCount,
                await financeOptions.ToListAsync()
            );
        }

        public async Task<GetFinanceOptionForViewDto> GetFinanceOptionForView(int id)
        {
            var financeOption = await _financeOptionRepository.GetAsync(id);

            var output = new GetFinanceOptionForViewDto { FinanceOption = ObjectMapper.Map<FinanceOptionDto>(financeOption) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_FinanceOptions_Edit)]
        public async Task<GetFinanceOptionForEditOutput> GetFinanceOptionForEdit(EntityDto input)
        {
            var financeOption = await _financeOptionRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetFinanceOptionForEditOutput { FinanceOption = ObjectMapper.Map<CreateOrEditFinanceOptionDto>(financeOption) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditFinanceOptionDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_FinanceOptions_Create)]
        protected virtual async Task Create(CreateOrEditFinanceOptionDto input)
        {
            var financeOption = ObjectMapper.Map<FinanceOption>(input);

            if (AbpSession.TenantId != null)
            {
                financeOption.TenantId = (int)AbpSession.TenantId;
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 21;
            dataVaultLog.ActionNote = "Payment Term Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);


            await _financeOptionRepository.InsertAsync(financeOption);
        }

        [AbpAuthorize(AppPermissions.Pages_FinanceOptions_Edit)]
        protected virtual async Task Update(CreateOrEditFinanceOptionDto input)
        {
            var financeOption = await _financeOptionRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 21;
            dataVaultLog.ActionNote = "Payment Term Updated : " + financeOption.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != financeOption.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = financeOption.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Percentage != financeOption.Percentage)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Percentage";
                history.PrevValue = financeOption.Percentage.ToString();
                history.CurValue = input.Percentage.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != financeOption.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = financeOption.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, financeOption);
        }

        [AbpAuthorize(AppPermissions.Pages_FinanceOptions_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _financeOptionRepository.Get(input.Id).Name;
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 21;
            dataVaultLog.ActionNote = "Payment Term Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _financeOptionRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetFinanceOptionsToExcel(GetAllFinanceOptionsForExcelInput input)
        {

            var filteredFinanceOptions = _financeOptionRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

            var query = (from o in filteredFinanceOptions
                         select new GetFinanceOptionForViewDto()
                         {
                             FinanceOption = new FinanceOptionDto
                             {
                                 Name = o.Name,
                                 Percentage = o.Percentage,
                                 Id = o.Id,
                                 IsActive = o.IsActive,
                             }
                         });


            var financeOptionListDtos = await query.ToListAsync();

            return _financeOptionsExcelExporter.ExportToFile(financeOptionListDtos);
        }


    }
}