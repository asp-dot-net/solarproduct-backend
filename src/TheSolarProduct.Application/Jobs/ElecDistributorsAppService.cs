﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Quotations;

namespace TheSolarProduct.Jobs
{
	[AbpAuthorize(AppPermissions.Pages_ElecDistributors, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
	public class ElecDistributorsAppService : TheSolarProductAppServiceBase, IElecDistributorsAppService
    {
		 private readonly IRepository<ElecDistributor> _elecDistributorRepository;
		 private readonly IElecDistributorsExcelExporter _elecDistributorsExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;


        public ElecDistributorsAppService(IRepository<ElecDistributor> elecDistributorRepository, IElecDistributorsExcelExporter elecDistributorsExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider) 
		  {
			_elecDistributorRepository = elecDistributorRepository;
			_elecDistributorsExcelExporter = elecDistributorsExcelExporter;
			_dataVaultActivityLogRepository = dataVaultActivityLogRepository;
			_dbcontextprovider = dbcontextprovider;
			
		  }

		 public async Task<PagedResultDto<GetElecDistributorForViewDto>> GetAll(GetAllElecDistributorsInput input)
         {
			
			var filteredElecDistributors = _elecDistributorRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter) || e.TAS.Contains(input.Filter) || e.ElectDistABB.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter)
						.WhereIf(input.MinSeqFilter != null, e => e.Seq >= input.MinSeqFilter)
						.WhereIf(input.MaxSeqFilter != null, e => e.Seq <= input.MaxSeqFilter)
						.WhereIf(input.NSWFilter > -1,  e => (input.NSWFilter == 1 && e.NSW) || (input.NSWFilter == 0 && !e.NSW) )
						.WhereIf(input.SAFilter > -1,  e => (input.SAFilter == 1 && e.SA) || (input.SAFilter == 0 && !e.SA) )
						.WhereIf(input.QLDFilter > -1,  e => (input.QLDFilter == 1 && e.QLD) || (input.QLDFilter == 0 && !e.QLD) )
						.WhereIf(input.VICFilter > -1,  e => (input.VICFilter == 1 && e.VIC) || (input.VICFilter == 0 && !e.VIC) )
						.WhereIf(input.WAFilter > -1,  e => (input.WAFilter == 1 && e.WA) || (input.WAFilter == 0 && !e.WA) )
						.WhereIf(input.ACTFilter > -1,  e => (input.ACTFilter == 1 && e.ACT) || (input.ACTFilter == 0 && !e.ACT) )
						.WhereIf(!string.IsNullOrWhiteSpace(input.TASFilter),  e => e.TAS == input.TASFilter)
						.WhereIf(input.NTFilter > -1,  e => (input.NTFilter == 1 && e.NT) || (input.NTFilter == 0 && !e.NT) )
						.WhereIf(!string.IsNullOrWhiteSpace(input.ElectDistABBFilter),  e => e.ElectDistABB == input.ElectDistABBFilter)
						.WhereIf(input.ElecDistAppReqFilter > -1,  e => (input.ElecDistAppReqFilter == 1 && e.ElecDistAppReq) || (input.ElecDistAppReqFilter == 0 && !e.ElecDistAppReq) )
						.WhereIf(input.MinGreenBoatDistributorFilter != null, e => e.GreenBoatDistributor >= input.MinGreenBoatDistributorFilter)
						.WhereIf(input.MaxGreenBoatDistributorFilter != null, e => e.GreenBoatDistributor <= input.MaxGreenBoatDistributorFilter);

			var pagedAndFilteredElecDistributors = filteredElecDistributors
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

			var elecDistributors = from o in pagedAndFilteredElecDistributors
                         select new GetElecDistributorForViewDto() {
							ElecDistributor = new ElecDistributorDto
							{
                                Name = o.Name,
                                Seq = o.Seq,
                                NSW = o.NSW,
                                SA = o.SA,
                                QLD = o.QLD,
                                VIC = o.VIC,
                                WA = o.WA,
                                ACT = o.ACT,
                                TAS = o.TAS,
                                NT = o.NT,
                                ElectDistABB = o.ElectDistABB,
                                ElecDistAppReq = o.ElecDistAppReq,
                                GreenBoatDistributor = o.GreenBoatDistributor,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						};

            var totalCount = await filteredElecDistributors.CountAsync();

            return new PagedResultDto<GetElecDistributorForViewDto>(
                totalCount,
                await elecDistributors.ToListAsync()
            );
         }
		 
		 public async Task<GetElecDistributorForViewDto> GetElecDistributorForView(int id)
         {
            var elecDistributor = await _elecDistributorRepository.GetAsync(id);

            var output = new GetElecDistributorForViewDto { ElecDistributor = ObjectMapper.Map<ElecDistributorDto>(elecDistributor) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_ElecDistributors_Edit)]
		 public async Task<GetElecDistributorForEditOutput> GetElecDistributorForEdit(EntityDto input)
         {
            var elecDistributor = await _elecDistributorRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetElecDistributorForEditOutput {ElecDistributor = ObjectMapper.Map<CreateOrEditElecDistributorDto>(elecDistributor)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditElecDistributorDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_ElecDistributors_Create)]
		 protected virtual async Task Create(CreateOrEditElecDistributorDto input)
         {
            var elecDistributor = ObjectMapper.Map<ElecDistributor>(input);

            //if (AbpSession.TenantId != null)
            //{
            //	elecDistributor.TenantId = (int)AbpSession.TenantId;
            //}
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 10;
            dataVaultLog.ActionNote = "Electricity Distributors Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _elecDistributorRepository.InsertAsync(elecDistributor);
         }

		 [AbpAuthorize(AppPermissions.Pages_ElecDistributors_Edit)]
		 protected virtual async Task Update(CreateOrEditElecDistributorDto input)
         {
            var elecDistributor = await _elecDistributorRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 10;
            dataVaultLog.ActionNote = "Electricity Distributors Updated : " + elecDistributor.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != elecDistributor.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = elecDistributor.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.ElectDistABB != elecDistributor.ElectDistABB)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "ElectDistABB";
                history.PrevValue = elecDistributor.ElectDistABB;
                history.CurValue = input.ElectDistABB;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != elecDistributor.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = elecDistributor.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, elecDistributor);
         }

		 [AbpAuthorize(AppPermissions.Pages_ElecDistributors_Delete)]
         public async Task Delete(EntityDto input)
         {
            var Name = _elecDistributorRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 10;
            dataVaultLog.ActionNote = "Electricity Distributors Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _elecDistributorRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetElecDistributorsToExcel(GetAllElecDistributorsForExcelInput input)
         {
			
			var filteredElecDistributors = _elecDistributorRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter) || e.TAS.Contains(input.Filter) || e.ElectDistABB.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter)
						.WhereIf(input.MinSeqFilter != null, e => e.Seq >= input.MinSeqFilter)
						.WhereIf(input.MaxSeqFilter != null, e => e.Seq <= input.MaxSeqFilter)
						.WhereIf(input.NSWFilter > -1,  e => (input.NSWFilter == 1 && e.NSW) || (input.NSWFilter == 0 && !e.NSW) )
						.WhereIf(input.SAFilter > -1,  e => (input.SAFilter == 1 && e.SA) || (input.SAFilter == 0 && !e.SA) )
						.WhereIf(input.QLDFilter > -1,  e => (input.QLDFilter == 1 && e.QLD) || (input.QLDFilter == 0 && !e.QLD) )
						.WhereIf(input.VICFilter > -1,  e => (input.VICFilter == 1 && e.VIC) || (input.VICFilter == 0 && !e.VIC) )
						.WhereIf(input.WAFilter > -1,  e => (input.WAFilter == 1 && e.WA) || (input.WAFilter == 0 && !e.WA) )
						.WhereIf(input.ACTFilter > -1,  e => (input.ACTFilter == 1 && e.ACT) || (input.ACTFilter == 0 && !e.ACT) )
						.WhereIf(!string.IsNullOrWhiteSpace(input.TASFilter),  e => e.TAS == input.TASFilter)
						.WhereIf(input.NTFilter > -1,  e => (input.NTFilter == 1 && e.NT) || (input.NTFilter == 0 && !e.NT) )
						.WhereIf(!string.IsNullOrWhiteSpace(input.ElectDistABBFilter),  e => e.ElectDistABB == input.ElectDistABBFilter)
						.WhereIf(input.ElecDistAppReqFilter > -1,  e => (input.ElecDistAppReqFilter == 1 && e.ElecDistAppReq) || (input.ElecDistAppReqFilter == 0 && !e.ElecDistAppReq) )
						.WhereIf(input.MinGreenBoatDistributorFilter != null, e => e.GreenBoatDistributor >= input.MinGreenBoatDistributorFilter)
						.WhereIf(input.MaxGreenBoatDistributorFilter != null, e => e.GreenBoatDistributor <= input.MaxGreenBoatDistributorFilter);

			var query = (from o in filteredElecDistributors
                         select new GetElecDistributorForViewDto() { 
							ElecDistributor = new ElecDistributorDto
							{
                                Name = o.Name,
                                Seq = o.Seq,
                                NSW = o.NSW,
                                SA = o.SA,
                                QLD = o.QLD,
                                VIC = o.VIC,
                                WA = o.WA,
                                ACT = o.ACT,
                                TAS = o.TAS,
                                NT = o.NT,
                                ElectDistABB = o.ElectDistABB,
                                ElecDistAppReq = o.ElecDistAppReq,
                                GreenBoatDistributor = o.GreenBoatDistributor,
                                Id = o.Id
							}
						 });


            var elecDistributorListDtos = await query.ToListAsync();

            return _elecDistributorsExcelExporter.ExportToFile(elecDistributorListDtos);
         }


    }
}