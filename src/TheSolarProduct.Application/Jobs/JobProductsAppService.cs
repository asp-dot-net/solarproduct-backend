﻿using TheSolarProduct.Jobs;
using System.Collections.Generic;


using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace TheSolarProduct.Jobs
{
	[AbpAuthorize(AppPermissions.Pages_JobProducts, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
	public class JobProductsAppService : TheSolarProductAppServiceBase, IJobProductsAppService
	{
		private readonly IRepository<JobProduct> _jobProductRepository;
		private readonly IRepository<ProductItem, int> _lookup_productItemRepository;
		private readonly IRepository<Job, int> _lookup_jobRepository;


		public JobProductsAppService(IRepository<JobProduct> jobProductRepository, IRepository<ProductItem, int> lookup_productItemRepository, IRepository<Job, int> lookup_jobRepository)
		{
			_jobProductRepository = jobProductRepository;
			_lookup_productItemRepository = lookup_productItemRepository;
			_lookup_jobRepository = lookup_jobRepository;

		}

		public async Task<PagedResultDto<GetJobProductForViewDto>> GetAll(GetAllJobProductsInput input)
		{
			var filteredJobProducts = _jobProductRepository.GetAll()
						.Include(e => e.ProductItemFk)
						.Include(e => e.JobFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
						.WhereIf(!string.IsNullOrWhiteSpace(input.ProductItemNameFilter), e => e.ProductItemFk != null && e.ProductItemFk.Name == input.ProductItemNameFilter)
						.WhereIf(input.JobId > 0, e => e.JobId == input.JobId)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Model), e => e.Model == input.Model)
						.WhereIf(!string.IsNullOrWhiteSpace(input.JobNoteFilter), e => e.JobFk != null && e.JobFk.Note == input.JobNoteFilter);

			var pagedAndFilteredJobProducts = filteredJobProducts
				.OrderBy(input.Sorting ?? "id desc")
				.PageBy(input);

			var jobProducts = from o in pagedAndFilteredJobProducts
							  join o1 in _lookup_productItemRepository.GetAll() on o.ProductItemId equals o1.Id into j1
							  from s1 in j1.DefaultIfEmpty()

							  join o2 in _lookup_jobRepository.GetAll() on o.JobId equals o2.Id into j2
							  from s2 in j2.DefaultIfEmpty()

							  select new GetJobProductForViewDto()
							  {
								  JobProduct = new JobProductDto
								  {
									  Id = o.Id,
									  Quantity = o.Quantity
								  },
								  ProductItemName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
								  JobNote = s2 == null || s2.Note == null ? "" : s2.Note.ToString()
							  };

			var totalCount = await filteredJobProducts.CountAsync();

			return new PagedResultDto<GetJobProductForViewDto>(
				totalCount,
				await jobProducts.ToListAsync()
			);
		}

		public async Task<List<GetJobProductForEditOutput>> GetJobProductByJobId(int jobid)
		{
			var jobProduct = _jobProductRepository.GetAll().Where(x => x.JobId == jobid).ToList();

			var output = new List<GetJobProductForEditOutput>();

			foreach (var item in jobProduct)
			{
				var outobj = new GetJobProductForEditOutput();
				outobj.JobProduct = ObjectMapper.Map<CreateOrEditJobProductDto>(item);
				if (outobj.JobProduct.ProductItemId != null)
				{
					var _lookupProductItem = await _lookup_productItemRepository.FirstOrDefaultAsync((int)outobj.JobProduct.ProductItemId);
					outobj.ProductItemName = _lookupProductItem?.Name?.ToString();
				}

				if (outobj.JobProduct.JobId != null)
				{
					var _lookupJob = await _lookup_jobRepository.FirstOrDefaultAsync((int)outobj.JobProduct.JobId);
					outobj.JobNote = _lookupJob?.Note?.ToString();
				}
				output.Add(outobj);
			}
			return output;
		}

		public async Task<GetJobProductForViewDto> GetJobProductForView(int id)
		{
			var jobProduct = await _jobProductRepository.GetAsync(id);

			var output = new GetJobProductForViewDto { JobProduct = ObjectMapper.Map<JobProductDto>(jobProduct) };

			if (output.JobProduct.ProductItemId != null)
			{
				var _lookupProductItem = await _lookup_productItemRepository.FirstOrDefaultAsync((int)output.JobProduct.ProductItemId);
				output.ProductItemName = _lookupProductItem?.Name?.ToString();
			}

			if (output.JobProduct.JobId != null)
			{
				var _lookupJob = await _lookup_jobRepository.FirstOrDefaultAsync((int)output.JobProduct.JobId);
				output.JobNote = _lookupJob?.Note?.ToString();
			}

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_JobProducts_Edit)]
		public async Task<GetJobProductForEditOutput> GetJobProductForEdit(EntityDto input)
		{
			var jobProduct = await _jobProductRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetJobProductForEditOutput { JobProduct = ObjectMapper.Map<CreateOrEditJobProductDto>(jobProduct) };

			if (output.JobProduct.ProductItemId != null)
			{
				var _lookupProductItem = await _lookup_productItemRepository.FirstOrDefaultAsync((int)output.JobProduct.ProductItemId);
				output.ProductItemName = _lookupProductItem?.Name?.ToString();
			}

			if (output.JobProduct.JobId != null)
			{
				var _lookupJob = await _lookup_jobRepository.FirstOrDefaultAsync((int)output.JobProduct.JobId);
				output.JobNote = _lookupJob?.Note?.ToString();
			}

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditJobProductDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_JobProducts_Create)]
		protected virtual async Task Create(CreateOrEditJobProductDto input)
		{
			var jobProduct = ObjectMapper.Map<JobProduct>(input);


			if (AbpSession.TenantId != null)
			{
				jobProduct.TenantId = (int)AbpSession.TenantId;
			}


			await _jobProductRepository.InsertAsync(jobProduct);
		}

		[AbpAuthorize(AppPermissions.Pages_JobProducts_Edit)]
		protected virtual async Task Update(CreateOrEditJobProductDto input)
		{
			var jobProduct = await _jobProductRepository.FirstOrDefaultAsync((int)input.Id);
			ObjectMapper.Map(input, jobProduct);
		}

		[AbpAuthorize(AppPermissions.Pages_JobProducts_Delete)]
		public async Task Delete(EntityDto input)
		{
			await _jobProductRepository.DeleteAsync(input.Id);
		}

		public async Task<List<JobProductProductItemLookupTableDto>> GetAllProductItemForTableDropdown()
		{
			return await _lookup_productItemRepository.GetAll().Where(x => x.Active == true)
				.Select(productItem => new JobProductProductItemLookupTableDto
				{
					Id = productItem.Id,
					DisplayName = productItem == null || productItem.Name == null ? "" : productItem.Name.ToString()
				}).ToListAsync();
		}

        public async Task<List<string>> GetAllProductItemname(string Name)
        {
            
                var result = _lookup_productItemRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(Name), c => c.Name.ToLower().Contains(Name.ToLower()));

                var name = from o in result
                             
                              select new JobProductProductItemLookupTableDto()
                              {
								  Id = o.Id,
								  DisplayName = o.Name.ToString()
                              };

                return await name
					.Select(JobProductProductItemLookupTableDto => JobProductProductItemLookupTableDto == null || JobProductProductItemLookupTableDto.DisplayName == null ? "" : JobProductProductItemLookupTableDto.DisplayName.ToString()).ToListAsync();
            
        }

  //      public async Task<List<string>> GetAllProductItemname(string name)
		//{
		//	return await _lookup_productItemRepository.GetAll().Where(x => x.Active == true)
		//			.WhereIf(!string.IsNullOrWhiteSpace(name), c => c.Name.ToLower().Contains(name.ToLower()))
		//		.Select(Name => Name == null || Name.Name == null ? "" : Name.Name.ToString()).ToListAsync();


		//}
		//public async Task<List<JobProductJobLookupTableDto>> GetAllJobForTableDropdown()
		//{
		//	return await _lookup_jobRepository.GetAll()
		//		.Select(job => new JobProductJobLookupTableDto
		//		{
		//			Id = job.Id,
		//			DisplayName = job == null || job.Note == null ? "" : job.Note.ToString()
		//		}).ToListAsync();
		//}

	}
}